#!/usr/bin/python3

if __name__ == '__main__':
	import sys
	sys.path.append('bin')

	import forge
	forge.determine_api()
	print('set PLATFORM=' + forge.api)
