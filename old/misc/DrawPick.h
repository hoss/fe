/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "signal/signal.h"
#include "window/window.h"
#include "draw/draw.h"
#include "viewer/DrawView.h"
#include "misc/misc.h"

namespace fe
{

/** Draw forward pick.

	@copydoc DrawPick_info
	*/
class FE_DL_EXPORT DrawPick:
	virtual public fe::HandlerI
{
	public:
				DrawPick(void);
virtual			~DrawPick(void);

virtual void	handleBind(	fe::sp<fe::SignalerI> spSignalerI,
						fe::sp<fe::Layout> l_sig);
virtual void	handle(	fe::Record &record);

	private:
		AsPick					m_asPick;
		DrawView				m_drawview;
};

} /* namespace */

