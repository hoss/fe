/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __misc_BroydenInertial_h__
#define __misc_BroydenInertial_h__

namespace fe
{

/**	Broyden time integration

	@copydoc BroydenInertial_info
	*/
class BroydenInertial:
	virtual public HandlerI,
	virtual public Config,
	virtual public Dispatch,
	virtual public Initialize<BroydenInertial>
{
	public:
				BroydenInertial(void);
virtual			~BroydenInertial(void);
virtual	void	initialize(void);

		// AS HandlerI
virtual	void	handleBind(sp<SignalerI> spSignaler,
					sp<Layout> l_sig);
virtual	void	handle(Record &r_sig);

		// AS DispatchI
virtual	bool	call(const String &a_name, std::vector<Instance> a_argv);

	private:
		AsParticle			m_asParticle;
		hp<SignalerI>		m_hpSignaler;
		sp<Scope>			m_spScope;
		Record				r_accum;
};


} /* namespace */

#endif /* __misc_BroydenInertial_h__ */

