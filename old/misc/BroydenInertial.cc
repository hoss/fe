/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "misc.pmh"
#include "BroydenInertial.h"

namespace fe
{

BroydenInertial::BroydenInertial(void)
{
}

BroydenInertial::~BroydenInertial(void)
{
}

void BroydenInertial::initialize(void)
{
	dispatch< Record >("setAccumulateRecord");
}

void BroydenInertial::handleBind(sp<SignalerI> spSignaler, sp<Layout> l_sig)
{
	m_hpSignaler = spSignaler;
	m_spScope = l_sig->scope();

	m_asParticle.bind(m_spScope);
}

void BroydenInertial::handle(Record &r_sig)
{

feLog("BroydenInertial\n");

	sp<RecordGroup> rg_input =
		cfg< sp<RecordGroup> >("input", r_sig); // input group

	SpatialVector zero(0.0f, 0.0f, 0.0f);

	if(!r_accum.isValid())
	{
		sp<Layout> l_accumulate;
		l_accumulate = m_spScope->declare("BroydenInertial_accumulate");
		r_accum = m_spScope->createRecord(l_accumulate);
	}

	unsigned int particle_count = 0;
	// clear force accumulators and get particle count
	for(RecordGroup::iterator i_rg = rg_input->begin();
		i_rg != rg_input->end(); i_rg++)
	{
		sp<RecordArray> spRA = *i_rg;
		if(m_asParticle.check(spRA))
		{
			for(int i = 0; i < spRA->length(); i++)
			{
				m_asParticle.force(spRA, i) = zero;
				particle_count++;
			}
		}
	}

	// store initial vector and set euler approx into location
	std::vector<SpatialVector>	x0(particle_count);
	std::vector<SpatialVector>	v0(particle_count);
	unsigned int i_particle = 0;
	for(RecordGroup::iterator i_rg = rg_input->begin();
		i_rg != rg_input->end(); i_rg++)
	{
		sp<RecordArray> spRA = *i_rg;
		if(m_asParticle.check(spRA))
		{
			for(int i = 0; i < spRA->length(); i++)
			{
				x0[i_particle] = m_asParticle.location(spRA,i);
				v0[i_particle] = m_asParticle.velocity(spRA,i);
				m_asParticle.location(spRA,i) += m_asParticle.velocity(spRA,i)
					* timestep;
				i_particle++;
			}
		}
	}

	// compute s0
	m_hpSignaler->signal(r_accum);

	unsigned int max_inner_iter = 10;
	std::vector< std::vector<SpatialVector> >	s(max_inner_iter);
	for(unsigned int i = 0; i < max_inner_iter; i++)
	{
		s[i].resize(i_particle);
	}

	i_particle = 0;
	for(RecordGroup::iterator i_rg = rg_input->begin();
		i_rg != rg_input->end(); i_rg++)
	{
		sp<RecordArray> spRA = *i_rg;
		if(m_asParticle.check(spRA))
		{
			for(int i = 0; i < spRA->length(); i++)
			{
				s[0][i_particle] = (timestep/m_asParticle.mass(spRA,i))
					* m_asParticle.force(spRA,i);
			}
		}
	}

	int i_inner = 0;
	std::vector<Real> s_norm_sq(max_inner_iter);
	s_norm_sq[0] = 0.0;
	for(unsigned i = 0; i < particle_count; i++)
	{
		s_norm_sq[0] += s[0][i].magnitudeSquared();
	}

	bool converged = false;

	if(s_norm_sq[0] < tol) { converged = true; }

	// while converge
	while(!converged && i_inner < max_inner_iter)
	{
		std::vector<SpatialVector>	z(particle_count);
		// increment state, clear accumulators
		i_particle = 0;
		for(RecordGroup::iterator i_rg = rg_input->begin();
		i_rg != rg_input->end(); i_rg++)
		{
			sp<RecordArray> spRA = *i_rg;
			if(m_asParticle.check(spRA))
			{
				for(int i = 0; i < spRA->length(); i++)
				{
					m_asParticle.velocity(spRA,i) += s[i_inner];
					m_asParticle.location(spRA,i) = x0[i_particle]
						+ m_asParticle.velocity(spRA,i);
					m_asParticle.force(spRA,i) = 0.0;
					i_particle++;
				}
			}
		}

		for(unsigned i = 0; i < particle_count; i++)
		{
			s_norm_sq[i_inner] += s[i_inner][i].magnitudeSquared();
		}

		// compute z
		m_hpSignaler->signal(r_accum);

		i_particle = 0;
		for(RecordGroup::iterator i_rg = rg_input->begin();
		i_rg != rg_input->end(); i_rg++)
		{
			sp<RecordArray> spRA = *i_rg;
			if(m_asParticle.check(spRA))
			{
				for(int i = 0; i < spRA->length(); i++)
				{
					z[i_particle] = v0[i_particle]
						- m_asParticle.velocity(spRA,i)
						+ (timestep/m_asParticle.mass(spRA,i))
							* m_asParticle.force(spRA,i);
					i_particle++;
				}
			}
		}

		Real norm_sq = 0.0;
		for(unsigned i = 0; i < particle_count; i++)
		{
			norm_sq += z[i].magnitudeSquared();
		}
		if(norm_sq < tol)
		{
			converged = true;
			break;
		}

		std::vector<SpatialVector>	tmp_z(particle_count);
		for(unsigned int j = 1; j <= i_inner; j++)
		{
			for(i_particle = 0; i_particle < particle_count; i_particle++)
			{
				tmp_z[i_particle] = z[i_particle];
			}
			for(i_particle = 0; i_particle < particle_count; i_particle++)
			{
				for(unsigned int i = 0; i < particle_count; i++)
				{
					for(unsigned in k = 0; k < 3; k++)
					{
						z[i_particle][k] += (tmp_z[i][k] * s[j][i_particle][k]
							* s[j-1][i][k]) / s_norm[j-1];
					}
				}
			}
		}

		if(i_inner < max_inner_iter -1 )
		{
			Real div = 0.0;
			for(i_particle = 0; i_particle < particle_count; i_particle++)
			{
				div += s[i_inner] * z[i_inner];
			}
			div /= s_norm_sq[i_inner];
			div += 1.0;
			div = 1.0/div;
			for(i_particle = 0; i_particle < particle_count; i_particle++)
			{
				s[i_inner+1][i_particle] = z[i_particle] * div ;
			}
		}

		i_inner++;
	}

	i_particle = 0;
	for(RecordGroup::iterator i_rg = rg_input->begin();
		i_rg != rg_input->end(); i_rg++)
	{
		sp<RecordArray> spRA = *i_rg;
		if(m_asParticle.check(spRA))
		{
			for(int i = 0; i < spRA->length(); i++)
			{
				m_asParticle.force(spRA,i).location =
					x0[i_particle] + timestep*m_asParticle.velocity(spRA,i)
			}
		}
	}

}

bool BroydenInertial::call(const String &a_name, std::vector<Instance> a_argv)
{
	if(a_name == FE_DISPATCH("setAccumulateRecord", "[record]"))
	{
		r_accum = a_argv[0].cast< Record >();
	}
	return true;
}



} /* namespace */

