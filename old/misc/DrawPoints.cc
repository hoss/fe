/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "DrawPoints.h"

namespace fe
{

DrawPoints::DrawPoints(void)
{
}

DrawPoints::~DrawPoints(void)
{
}

void DrawPoints::initialize(void)
{
}

void DrawPoints::handle(Record &r_sig)
{
	if(!m_drawview.handle(r_sig)) { return; }

	sp<RecordGroup> rg_input =
		cfg< sp<RecordGroup> >("input"); // input group

	hp<DrawI> spDraw(m_drawview.drawI());
	const Color red(1.0f,0.0f,0.0f,1.0f);
	SpatialVector scale(0.2,0.2,0.2);

	Matrix3x4f transform;

	for(RecordGroup::iterator it = rg_input->begin();
		it != rg_input->end(); it++)
	{
		sp<RecordArray> spRA(*it);
		m_asPoint.bind(spRA->layout()->scope());
		if(m_asPoint.check(spRA))
		{
			for(int i = 0; i < spRA->length(); i++)
			{
				transform = Matrix3x4f::identity();
				translate(transform, m_asPoint.location(spRA, i));
				spDraw->drawSphere(transform,&scale,red);
			}
		}
	}
}

bool DrawPoints::call(const String &a_name, std::vector<Instance> a_argv)
{
	return true;
}

} /* namespace */

