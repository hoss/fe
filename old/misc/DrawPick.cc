/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "DrawPick.h"

namespace fe
{

DrawPick::DrawPick(void)
{
}

DrawPick::~DrawPick(void)
{
}

void DrawPick::handleBind(fe::sp<fe::SignalerI> spSignalerI,
	fe::sp<fe::Layout> l_sig)
{
	m_asPick.bind(l_sig->scope());
}

void DrawPick::handle(fe::Record &r_sig)
{
	const Color blue(0.0f,0.0f,1.0f);
	const Color cyan(0.0f,1.0f,1.0f);
	const Color red(1.0f,0.0f,0.0f);
	const Color orange(1.0f,0.5f,0.0f);
	const Color white(1.0f,1.0f,1.0f);
	const Color yellow(1.0f,1.0f,0.0f);

	Color startc(blue);

	if(!m_drawview.handle(r_sig))
	{
		return;
	}

	hp<DrawI> spDraw(m_drawview.drawI());

	Record r_windata = m_drawview.asSignal().winData(r_sig);

	if(m_asPick.check(r_windata))
	{
		Matrix3x4f xform;

		if(m_asPick.selecting(r_windata))
		{
			startc = white;
		}

		SpatialVector v = m_asPick.start(r_windata);
		xform = Matrix3x4f::identity();
		translate(xform, v);
		spDraw->drawTransformedMarker(xform, 1.0, startc);

		v = m_asPick.start(r_windata) + m_asPick.startv(r_windata);
		xform = Matrix3x4f::identity();
		translate(xform, v);
		spDraw->drawTransformedMarker(xform, 1.0, cyan);

		SpatialVector l[2];
		l[0] = m_asPick.start(r_windata);
		l[1] = m_asPick.start(r_windata) + m_asPick.startv(r_windata);
		spDraw->drawLines(l, NULL, 2, DrawI::e_strip, false, &red);

		v = m_asPick.end(r_windata);
		xform = Matrix3x4f::identity();
		translate(xform, v);
		spDraw->drawTransformedMarker(xform, 1.0, red);

		v = m_asPick.end(r_windata) + m_asPick.endv(r_windata);
		xform = Matrix3x4f::identity();
		translate(xform, v);
		spDraw->drawTransformedMarker(xform, 1.0, orange);

		l[0] = m_asPick.end(r_windata);
		l[1] = m_asPick.end(r_windata) + m_asPick.endv(r_windata);
		spDraw->drawLines(l, NULL, 2, DrawI::e_strip, false, &yellow);

		xform = Matrix3x4f::identity();
		v = m_asPick.focus(r_windata);
		translate(xform, v);
		spDraw->drawTransformedMarker(xform, 1.0, orange);
	}
}

} /* namespace */

