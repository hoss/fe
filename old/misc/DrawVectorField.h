/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __misc_DrawVectorField_h__
#define __misc_DrawVectorField_h__

#include "signal/signal.h"
#include "draw/draw.h"
#include "window/window.h"
#include "shape/shape.h"
#include "spatial/spatial.h"
#include "viewer/DrawView.h"
#include "misc/misc.h"

namespace fe
{

/**	draw a vector field

	@copydoc DrawVectorField_info
	*/
class FE_DL_EXPORT DrawVectorField :
		public Initialize<DrawVectorField>,
		virtual public HandlerI,
		virtual public Dispatch
{
	public:
				DrawVectorField(void);
virtual			~DrawVectorField(void);

		void	initialize(void);

				// AS HandlerI
virtual void	handleBind(sp<SignalerI> spSignalerI,
						sp<Layout> l_sig);
virtual void	handle(Record &r_sig);

				// AS DispatchI
virtual	bool	call(const String &a_name, std::vector<Instance> a_argv);

		typedef enum
		{
			e_none			= 0,
			e_line			= 1<<0,
			e_sphere		= 1<<1,
			e_rod			= 1<<2,
			e_cone			= 1<<3
		} t_mode;

		void	setMode(unsigned int a_mode);

	private:
		void	setColors(sp<RecordGroup> a_winGroup);
		void	draw(sp<DrawI> spDraw, sp<VectorFieldI> spField);

		PathAccessor<sp<Component> >		m_aVectorField;
		DrawView							m_drawview;
		AsColor								m_asColor;
		unsigned int						m_mode;
		Color								m_color;
};

} /* namespace */

#endif /* __misc_DrawVectorField_h__ */
