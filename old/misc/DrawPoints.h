/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __misc_DrawPoints_h__
#define __misc_DrawPoints_h__

#include "signal/signal.h"
#include "draw/draw.h"
#include "window/window.h"
#include "shape/shape.h"
#include "viewer/DrawView.h"
#include "ai/aiAS.h"
#include "misc/misc.h"

namespace fe
{

class FE_DL_EXPORT DrawPoints :
	public Initialize<DrawPoints>,
	virtual public HandlerI,
	virtual public Dispatch,
	virtual public Config
{
	public:
				DrawPoints(void);
virtual			~DrawPoints(void);

		void	initialize(void);

				// AS HandlerI
virtual void	handle(	Record &record);

				// AS DispatchI
virtual	bool	call(const String &a_name, std::vector<Instance> a_argv);

	private:
		AsPoint								m_asPoint;
		DrawView							m_drawview;
};

} /* namespace */

#endif /* __misc_DrawPoints_h__ */

