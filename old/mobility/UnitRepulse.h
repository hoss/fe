/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __mobility_UnitRepulse_h__
#define __mobility_UnitRepulse_h__

#include "signal/signal.h"
#include "math/math.h"
#include "datatool/datatool.h"
#include "shape/shape.h"
#include "mobilityAS.h"

namespace fe
{

class FE_DL_EXPORT UnitRepulse :
	public Initialize<UnitRepulse>,
	virtual public HandlerI,
	virtual public Config
{
	public:
				UnitRepulse(void);
virtual			~UnitRepulse(void);

		void	initialize(void);

				// AS HandlerI
virtual void	handle(	Record &r_sig);

	private:
		AsMobile		m_asMobile;
		AsParticle		m_asParticle;
		AsPair			m_asPair;
		AsBounded		m_asBounded;
};

} /* namespace */

#endif /* __mobility_UnitRepulse_h__ */

