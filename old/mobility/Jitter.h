/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __mobility_Jitter_h__
#define __mobility_Jitter_h__

#include "signal/signal.h"
#include "math/math.h"
#include "datatool/datatool.h"
#include "shape/shape.h"
#include "mobilityAS.h"

namespace fe
{

/**	randomly displace a particle

	@copydoc Jitter_info
	*/
class FE_DL_EXPORT Jitter :
	public Initialize<Jitter>,
	virtual public HandlerI,
	virtual public Config
{
	public:
				Jitter(void);
virtual			~Jitter(void);

		void	initialize(void);

				// AS HandlerI
virtual void	handle(	Record &r_sig);

	private:
		AsMobile		m_asMobile;
		AsParticle		m_asParticle;
};

} /* namespace */

#endif /* __mobility_Jitter_h__ */

