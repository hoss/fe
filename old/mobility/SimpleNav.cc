/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "mobility/SimpleNav.h"

namespace fe
{

SimpleNav::SimpleNav(void)
{
}

SimpleNav::~SimpleNav(void)
{
	cfg<String>("nearby") = "0.0";
}

void SimpleNav::handle(Record &r_sig)
{

	sp<RecordGroup> rg_input =
		cfg< sp<RecordGroup> >("input"); // input group

	Real nearby = atof(cfg<String>("nearby").c_str());

	for(RecordGroup::iterator i_rg = rg_input->begin();
		i_rg != rg_input->end(); i_rg++)
	{
		sp<RecordArray> spRA = *i_rg;
		AsMobile asMobile;
		AsParticle asParticle;
		asMobile.bind(spRA->layout()->scope());
		asParticle.bind(spRA->layout()->scope());
		if(asMobile.check(spRA) && asParticle.check(spRA))
		{
			for(int i = 0; i < spRA->length(); i++)
			{
				asMobile.direction(spRA,i) =
					asMobile.destination(spRA,i) -
					asParticle.location(spRA,i);
				if(nearby > 0.0)
				{
					Real distance = magnitude(asMobile.direction(spRA,i));
					if(distance < nearby)
					{
						set(asMobile.direction(spRA,i), 0.0, 0.0, 0.0);
					}
				}
			}
		}
	}
}

} /* namespace */

