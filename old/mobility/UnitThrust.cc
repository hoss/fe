/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "UnitThrust.h"

namespace fe
{

UnitThrust::UnitThrust(void)
{
}

UnitThrust::~UnitThrust(void)
{
}

void UnitThrust::initialize(void)
{
}

void UnitThrust::handle(Record &r_sig)
{
	m_asMobile.bind(r_sig.layout()->scope());
	m_asParticle.bind(r_sig.layout()->scope());

	sp<RecordGroup> rg_input =
		cfg< sp<RecordGroup> >("input"); // input group

	SpatialVector zero(0.0f, 0.0f, 0.0f, 1.0f);

	for(RecordGroup::iterator i_rg = rg_input->begin();
		i_rg != rg_input->end(); i_rg++)
	{
		sp<RecordArray> spRA = *i_rg;
		if(m_asParticle.check(spRA) && m_asMobile.check(spRA))
		{
			for(int i = 0; i < spRA->length(); i++)
			{
				SpatialVector force = m_asMobile.direction(spRA, i);
				if(!(force == zero))
				{
					normalize(force);
					force *= m_asMobile.thrust(spRA,i);
					m_asParticle.force(spRA, i) += force;
				}
			}
		}
	}
}

} /* namespace */

