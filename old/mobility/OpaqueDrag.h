/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __mobility_OpaqueDrag_h__
#define __mobility_OpaqueDrag_h__

#include "signal/signal.h"
#include "math/math.h"
#include "datatool/datatool.h"
#include "shape/shape.h"

namespace fe
{

class FE_DL_EXPORT OpaqueDrag :
	virtual public HandlerI,
	virtual public Config
{
	public:
				OpaqueDrag(void);
virtual			~OpaqueDrag(void);

				// AS HandlerI
virtual void	handle(	Record &r_sig);
};


} /* namespace */

#endif /* __mobility_OpaqueDrag_h__ */

