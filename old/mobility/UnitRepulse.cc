/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "UnitRepulse.h"

namespace fe
{

UnitRepulse::UnitRepulse(void)
{
}

UnitRepulse::~UnitRepulse(void)
{
}

void UnitRepulse::initialize(void)
{
}

void UnitRepulse::handle(Record &r_sig)
{
	m_asMobile.bind(r_sig.layout()->scope());
	m_asParticle.bind(r_sig.layout()->scope());
	m_asBounded.bind(r_sig.layout()->scope());
	m_asPair.bind(r_sig.layout()->scope());

	sp<RecordGroup> rg_input =
		cfg< sp<RecordGroup> >("input"); // input group

	for(RecordGroup::iterator i_rg = rg_input->begin();
		i_rg != rg_input->end(); i_rg++)
	{
		sp<RecordArray> spRA = *i_rg;
		if(m_asPair.check(spRA))
		{
			for(int i = 0; i < spRA->length(); i++)
			{
				Record r_left = m_asPair.left(spRA, i);
				Record r_right = m_asPair.right(spRA, i);
				if(	m_asMobile.check(r_left)	&&
					m_asBounded.check(r_left)	&&
					m_asBounded.check(r_right)	&&
					m_asParticle.check(r_left)	&&
					m_asMobile.check(r_right)	&&
					m_asParticle.check(r_right))
				{
					SpatialVector delta = m_asParticle.location(r_left) -
						m_asParticle.location(r_right);

					Real touch_dist = m_asBounded.radius(r_left)
						+ m_asBounded.radius(r_right);

					Real dist = magnitude(delta);

					Real scale = touch_dist - dist;

					normalize(delta);

					delta *= 10.0*scale;

					m_asParticle.force(r_left) += delta;
					delta *= (Real)(-1.0);
					m_asParticle.force(r_right) += delta;
#if 0
					Real scale = magnitudeSquared(delta);
					if (scale < 0.00001) { scale = 0.00001; }
					scale = (Real)1.0/scale;
					delta *= scale;
					m_asParticle.force(r_left) += delta;
					delta *= (Real)(-1.0);
					m_asParticle.force(r_right) += delta;
#endif
				}
			}
		}
	}
}

} /* namespace */

