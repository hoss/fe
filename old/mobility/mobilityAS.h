/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __mobility_mobilityAS_h__
#define __mobility_mobilityAS_h__

#include "signal/signal.h"
#include "math/math.h"
#include "datatool/datatool.h"
#include "shape/shape.h"

namespace fe
{

class AsMobile
	: public AccessorSet, public Initialize<AsMobile>
{
	public:
		AsMobile(void){}
		void initialize(void)
		{
			add(direction,		FE_SPEC("mbl:dir",		"direction to move"));
			add(destination,	FE_SPEC("mbl:dst",		"desired destination"));
			add(thrust,			FE_SPEC("mbl:thrust",	"thrust magnitude"));
		}
		/// direction to move
		Accessor<SpatialVector>		direction;
		/// desired destination
		Accessor<SpatialVector>		destination;
		/// thrust magnitude
		Accessor<Real>				thrust;
};


} /* namespace */

#endif /* __mobility_mobilityAS_h__ */

