/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __mobility_SimpleNav_h__
#define __mobility_SimpleNav_h__

#include "signal/signal.h"

#include "mobility/mobility.h"

namespace fe
{

class SimpleNav :
	virtual public HandlerI,
	virtual public Config
{
	public:
				SimpleNav(void);
virtual			~SimpleNav(void);

				// AS HandlerI
virtual void	handle(Record &r_sig);

};

} /* namespace */

#endif /* __mobility_SimpleNav_h__ */

