/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __armadillo_h__
#define __armadillo_h__

#include "armadillo"

#include "fe/core.h"
#include <armadillo.h>

#ifdef MODULE_armadillo
#define FE_ARMADILLO_PORT FE_DL_EXPORT
#else
#define FE_ARMADILLO_PORT FE_DL_IMPORT
#endif

namespace fe
{
};

#endif // __armadillo_h__

