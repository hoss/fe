/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "fex/math.h"
#include "fex/pyfe.h"
#include "pyfe/Convertor.h"
#include <boost/python.hpp>
using namespace boost::python;
#include "pyfe/fe_math.h"

void mathExceptionTranslator(fe::Exception const& e)
{
	PyErr_SetString(PyExc_UserWarning, e.getMessage().c_str());
}

template<class rawtype, class bindtype>
class math_class_ : public class_<bindtype>
{
	public:
		math_class_(	fe::sp<fe::TypeMaster> spTypeMaster,
						const char *py_name,
						const char *fe_name)
				: class_<bindtype>(py_name)
		{
			object o_bindtype = *this;

			fe::sp<fe::BaseType> spBT;
			spBT = fe::assertType<rawtype>(spTypeMaster, fe_name);

			fe::sp<pyfe::BaseConvertor> spC;
			spC = new pyfe::Convertor<rawtype,bindtype>(o_bindtype);
			pyfe::addConvertor(spBT, spC);
		}
};

template<class rawtype, class bindtype>
class math_array_class_ : public math_class_<rawtype,bindtype>
{
	public:
		math_array_class_(	fe::sp<fe::TypeMaster> spTypeMaster,
						const char *py_name,
						const char *fe_name)
				: math_class_<rawtype,bindtype>(spTypeMaster,py_name,fe_name)
		{
			this->def("__getitem__",			&bindtype::getitem);
			this->def("__setitem__",			&bindtype::setitem);
		}
};

BOOST_PYTHON_MODULE(math)
{
	fe::sp<fe::TypeMaster> spTM(pyfe::feMaster().typeMaster());
	register_exception_translator<fe::Exception>(&mathExceptionTranslator);

	math_array_class_<fe::Vector2f,Vector2f>(spTM,"vector2","py_vector2f")
		.def(float() * self)
		.def(self * float())
		.def(self + self)
		.def(self - self)
	;

	math_array_class_<fe::Vector3f,Vector3f>(spTM,"vector3","py_vector3f")
		.def("cross",			&Vector3f::cross)
		.def("magnitude",		&Vector3f::magnitude)
		.def("normalize",		&Vector3f::normalize)
		.def(float() * self)
		.def(self * float())
		.def(self + self)
		.def(self - self)
	;

	math_array_class_<fe::Vector4f,Vector4f>(spTM,"vector4","py_vector4f")
		.def("cross",			&Vector4f::cross)
		.def(float() * self)
		.def(self * float())
		.def(self + self)
		.def(self - self)
	;

}



