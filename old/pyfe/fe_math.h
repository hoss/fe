/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __pyfe_fe_math_h__
#define __pyfe_fe_math_h__

template<class rawtype>
class MathType
{
	public:
		MathType(void) { m_pRaw = &m_raw; }
virtual	~MathType(void) { /* NOOP */ }
		MathType(const MathType<rawtype> &other)
		{
			m_pRaw = &m_raw;
			*m_pRaw = *(other.m_pRaw);
		}

		MathType<rawtype>	&operator=(const MathType<rawtype> &other)
		{
			*m_pRaw = *(other.m_pRaw);
			return *this;
		}
		void				assign(rawtype &raw) { m_pRaw = &raw; }
		rawtype				*rawptr(void) { return m_pRaw; }
		rawtype				*rawptr(void) const { return m_pRaw; }


	protected:
		rawtype		*m_pRaw;
		rawtype		m_raw;
};


template<class rawtype, int N>
class MathArrayType : public MathType<rawtype>
{
	public:
virtual	~MathArrayType(void) { }
		float			getitem(unsigned long index)
		{
			if(index >= N)
			{
				feX(fe::e_invalidRange,
					"pyfe",
					"array index out of range");
			}
			return (*m_pRaw)[index];
		}
		void			setitem(unsigned long index, float value)
		{
			if(index >= N)
			{
				feX(fe::e_invalidRange,
					"pyfe",
					"array index out of range");
			}
//			(*m_pRaw)[index] = value;
			fe::setAt(*m_pRaw,index,value);
		}
};

class Vector2f : public MathArrayType<fe::Vector2f, 2>
{
	public:
};

Vector2f	operator*(const float &lhs, const Vector2f &rhs)
{
	Vector2f r;
	*(r.rawptr()) = *(rhs.rawptr()) * lhs;
	return r;
}

Vector2f	operator*(const Vector2f &lhs, const float &rhs)
{
	Vector2f r;
	*(r.rawptr()) = *(lhs.rawptr()) * rhs;
	return r;
}

Vector2f	operator+(const Vector2f &lhs, const Vector2f &rhs)
{
	Vector2f r;
	*(r.rawptr()) = *(lhs.rawptr()) + *(rhs.rawptr());
	return r;
}

Vector2f	operator-(const Vector2f &lhs, const Vector2f &rhs)
{
	Vector2f r;
	*(r.rawptr()) = *(lhs.rawptr()) - *(rhs.rawptr());
	return r;
}

class Vector3f : public MathArrayType<fe::Vector3f, 3>
{
	public:
		Vector3f	cross(Vector3f &rhs)
		{
			Vector3f r;
			fe::cross3(*(r.m_pRaw),*m_pRaw,*(rhs.m_pRaw));
			return r;
		}
		float		magnitude(void)
		{
			return fe::magnitude(*m_pRaw);
		}
		Vector3f	normalize(void)
		{
			fe::normalize(*m_pRaw);
			return *this;
		}

};

Vector3f	operator*(const float &lhs, const Vector3f &rhs)
{
	Vector3f r;
	*(r.rawptr()) = *(rhs.rawptr()) * lhs;
	return r;
}

Vector3f	operator*(const Vector3f &lhs, const float &rhs)
{
	Vector3f r;
	*(r.rawptr()) = *(lhs.rawptr()) * rhs;
	return r;
}

Vector3f	operator+(const Vector3f &lhs, const Vector3f &rhs)
{
	Vector3f r;
	*(r.rawptr()) = *(lhs.rawptr()) + *(rhs.rawptr());
	return r;
}

Vector3f	operator-(const Vector3f &lhs, const Vector3f &rhs)
{
	Vector3f r;
	*(r.rawptr()) = *(lhs.rawptr()) - *(rhs.rawptr());
	return r;
}

class Vector4f : public MathArrayType<fe::Vector4f, 4>
{
	public:
		Vector4f	cross(Vector4f &rhs)
		{
			Vector4f r;
			fe::cross3(*(r.m_pRaw),*m_pRaw,*(rhs.m_pRaw));
			return r;
		}
};

Vector4f	operator*(const float &lhs, const Vector4f &rhs)
{
	Vector4f r;
	*(r.rawptr()) = *(rhs.rawptr()) * lhs;
	return r;
}

Vector4f	operator*(const Vector4f &lhs, const float &rhs)
{
	Vector4f r;
	*(r.rawptr()) = *(lhs.rawptr()) * rhs;
	return r;
}

Vector4f	operator+(const Vector4f &lhs, const Vector4f &rhs)
{
	Vector4f r;
	*(r.rawptr()) = *(lhs.rawptr()) + *(rhs.rawptr());
	return r;
}

Vector4f	operator-(const Vector4f &lhs, const Vector4f &rhs)
{
	Vector4f r;
	*(r.rawptr()) = *(lhs.rawptr()) - *(rhs.rawptr());
	return r;
}



#endif /* __pyfe_fe_math_h__ */

