/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "pyfe.pmh"
#include "internal.h"

namespace pyfe
{

#define PyFeObject_NEW(t,pt)	(t*)pyfe::PyFeObjectNew(sizeof(t), pt)
#define PyFeObject_DEL(pObj)	PyMem_Free(pObj)

typedef std::map<fe::sp<fe::BaseType>, fe::sp<BaseConvertor> > convertors_t;

Master *pyfeMaster(void);

PyObject *PyFeObjectNew(int size, PyTypeObject *type)
{
	PyObject *pObj = (PyObject *)PyMem_Malloc(size);
	PyObject_Init(pObj, type);
	return pObj;
}

static PyObject *instanceToPyObject(UWORD index,
const fe::Record &record, const fe::sp<fe::Attribute> spAttribute)
{
	fe::sp<fe::BaseType> spBT = spAttribute->type();
	void *instance =
		(void *)((char *)(record.data())+record.layout()->offsetTable()[index]);
	PyObject *returnVal = NULL;
	fe::sp<fe::TypeMaster> spTM = record.layout()->scope()->typeMaster();
	if(spBT == spTM->lookupType<int>())
	{
		returnVal = Py_BuildValue("l", *((int *)instance));
	}
	else if(spBT == spTM->lookupType<float>())
	{
		returnVal = Py_BuildValue("f", *((float *)instance));
	}
	else if(spBT == spTM->lookupType<fe::String>())
	{
		returnVal = Py_BuildValue("s", ((fe::String *)instance)->c_str());
	}
	else if(spBT == spTM->lookupType<fe::Record>())
	{
		Record *pRecord;
		pRecord = PyFeObject_NEW(Record, &Record_T);
		fe::Record *record = (fe::Record *)instance;
		pRecord->Init(*record);
		returnVal = (PyObject *)pRecord;
	}
	else if(spBT == spTM->lookupType<fe::sp<fe::Component> >())
	{
		Component *pComponent;
		pComponent = PyFeObject_NEW(Component, &Component_T);
		fe::sp<fe::Component> *component = (fe::sp<fe::Component> *)instance;
		pComponent->Init(*component);
		returnVal = (PyObject *)pComponent;
	}
	else
	{
		int *pRefCnt = NULL;
		if(record.layout()->scope()->refCount().check(record))
		{
			pRefCnt = &(record.layout()->scope()->refCount()(record));
		}
		fe::Instance instance(instance, spBT, pRefCnt);

		convertors_t *pConvertors = pyfeMaster()->m_pConvertors;
		if(pConvertors->find(spBT) != pConvertors->end())
		{
			returnVal = (*pConvertors)[spBT]
				->instanceToPyObject(spBT,instance);
		}
		else
		{
			Instance *pInstance;
			pInstance = PyFeObject_NEW(Instance, &Instance_T);
			pInstance->Init(instance);
			returnVal = (PyObject *)pInstance;
		}
	}

	return returnVal;
}

bool PyObjectToInstance(const fe::Record &record, void *instance, const fe::sp<fe::BaseType> spBT, PyObject *pobj)
{
	fe::sp<fe::TypeMaster> spTM = record.layout()->scope()->typeMaster();
	if(PyInt_Check(pobj))
	{
		if(spBT == spTM->lookupType<int>())
		{
			*((int *)instance) = PyInt_AsLong(pobj);
			return true;
		}
	}
	else if(PyFloat_Check(pobj))
	{
		if(spBT == spTM->lookupType<float>())
		{
			*((float *)instance) = (float)PyFloat_AsDouble(pobj);
			return true;
		}
	}
	else if(PyString_Check(pobj))
	{
		if(spBT == spTM->lookupType<fe::String>())
		{
			*((fe::String *)instance) = PyString_AsString(pobj);
			return true;
		}
	}
	else if(pobj->ob_type == &Instance_T)
	{
		fe::Instance *rvalue = ((Instance *)pobj)->getInstance();
		int *pRefCnt = NULL;
		if(record.layout()->scope()->refCount().check(record))
		{
			pRefCnt = &(record.layout()->scope()->refCount()(record));
		}
		if(pRefCnt == NULL)
		{
			throw fe::Exception("attempt to assign non reference counted instance.  avoid instance() method");
		}
		fe::Instance lvalue(instance, spBT, pRefCnt);
		lvalue.assign(*rvalue);
		return true;
	}
	else if(pobj->ob_type == &Record_T)
	{
		if(spBT == spTM->lookupType<fe::Record>())
		{
			Record *pR = (Record *)pobj;
			*((fe::Record *)instance) = *(pR->getRecord());
			return true;
		}
	}
	else if(pobj->ob_type == &Component_T)
	{
		if(spBT == spTM->lookupType<fe::sp<fe::Component> >())
		{
			Component *pC = (Component *)pobj;
			*((fe::sp<fe::Component> *)instance) = *(pC->getComponent());
			return true;
		}
	}
	else
	{
		convertors_t *pConvertors = pyfeMaster()->m_pConvertors;
		if(pConvertors->find(spBT) != pConvertors->end())
		{
			fe::Instance rvalue;
			if(!(*pConvertors)[spBT]->PyObjectToInstance(spBT,pobj,rvalue))
			{
				return false;
			}
			int *pRefCnt = NULL;
			if(record.layout()->scope()->refCount().check(record))
			{
				pRefCnt = &(record.layout()->scope()->refCount()(record));
			}
			if(pRefCnt == NULL)
			{
				throw fe::Exception("attempt to assign non reference counted instance.  avoid instance() method");
			}
			fe::Instance lvalue(instance, spBT, pRefCnt);
			lvalue.assign(rvalue);
			return true;
		}
	}
	return false;
}

// ---------------------------------------------------------------------------
/* Master ********************************************************************/
static void Master_dealloc(PyObject* self)
{
	((Master *)self)->Fini();
	PyFeObject_DEL(self);
}

// ---------------------------------------------------------------------------
/* Record ********************************************************************/
static void Record_dealloc(PyObject* self)
{
	((Record *)self)->Fini();
	PyFeObject_DEL(self);
}

static PyObject *Record_getattr(PyObject *self, char *name)
{
	PyObject *ptr = NULL;
	Record *pRecord;
	pRecord = (Record *)self;

	fe::Record *pFeRecord = pRecord->getRecord();
	if(!pFeRecord->isValid())
	{
		ptr = Py_FindMethod(Record_Methods, self, name);
		if(!ptr)
		{
			fe::String string;
			string.sPrintf("getattr: \'%s\' not found", name);
			PyErr_SetString(PyExc_AttributeError, string.c_str());
		}
		return ptr;
	}


	fe::sp<fe::Layout> pFeLayout = pFeRecord->layout();

	UWORD index;
	fe::sp<fe::Attribute> spAttribute = pFeLayout->scope()->findAttribute(name, index);
	if(!spAttribute.isValid())
	{
		ptr = Py_FindMethod(Record_Methods, self, name);
		if(!ptr)
		{
			fe::String string;
			string.sPrintf("getattr: attribute \'%s\' not in layout \'%s\'",
				name, pFeRecord->layout()->name().c_str());
			PyErr_SetString(PyExc_AttributeError, string.c_str());
		}
		return ptr;
	}

	// check
	if(pFeRecord->layout()->offsetTable()[index] == fe::offsetNone)
	{
		fe::String string;
		string.sPrintf("getattr: attribute \'%s\' not in layout \'%s\'",
			spAttribute->name().c_str(), pFeRecord->layout()->name().c_str());
		PyErr_SetString(PyExc_AttributeError, string.c_str());
		return NULL;
	}

	ptr = instanceToPyObject(index, *pFeRecord, spAttribute);
	if(!ptr)
	{
		PyErr_SetString(PyExc_Exception,
			"attribute type not supported");
	}
	return ptr;
}

static int Record_setattr(PyObject *self, char *name, PyObject *value)
{
	Record *pRecord;
	pRecord = (Record *)self;

	fe::Record *pFeRecord = pRecord->getRecord();
	fe::sp<fe::Layout> pFeLayout = pFeRecord->layout();

	UWORD index;
	fe::sp<fe::Attribute> spAttribute =
			pFeLayout->scope()->findAttribute(name, index);
	if(!spAttribute.isValid())
	{
		fe::String string;
		string.sPrintf("attribute not in scope: %s", name);
		PyErr_SetString(PyExc_AttributeError, string.c_str());
		return true;
	}

	// check
	if(pFeRecord->layout()->offsetTable()[index] == fe::offsetNone)
	{
		fe::String string;
		string.sPrintf("setattr: attribute \'%s\' not in layout \'%s\'",
			spAttribute->name().c_str(), pFeRecord->layout()->name().c_str());
		PyErr_SetString(PyExc_AttributeError, string.c_str());
		return true;
	}

	void *instance =
		(void *)((char *)(pFeRecord->data())
		+ pFeRecord->layout()->offsetTable()[index]);
	bool rv = PyObjectToInstance(*pFeRecord, instance, spAttribute->type(),
			value);

	if(!rv)
	{
		fe::String string;
		string.sPrintf(
				"setattr: attribute \'%s\' in layout \'%s\' not converted",
				spAttribute->name().c_str(),
				pFeRecord->layout()->name().c_str());
		PyErr_SetString(PyExc_AttributeError, string.c_str());
		return true;
	}

	return false;
}

static PyObject *Record_check(PyObject* self, PyObject* args)
{
	Record *pRecord;
	pRecord = (Record *)self;
	fe::Record *pR = pRecord->getRecord();
	char *str;
	PyObject *arg;

	for(UWORD i = 0; i < (U32)(PyTuple_Size(args)); i++)
	{
		arg = PyTuple_GetItem(args, i);
		if(PyString_Check(arg))
		{
			str = PyString_AsString(arg);
			if(!fe::checkAttribute(str, *pR))
			{
				return Py_BuildValue("");
			}
		}
	}
	return Py_BuildValue("i",1);
}

static PyObject *Record_isValid(PyObject* self, PyObject* args)
{
	Record *pRecord;
	pRecord = (Record *)self;
	fe::Record *pR = pRecord->getRecord();

	if(pR->isValid())
	{
		return Py_BuildValue("i",1);
	}
	return Py_BuildValue("i",0);
}


// ---------------------------------------------------------------------------
/* RecordArray ***************************************************************/
static void RecordArray_dealloc(PyObject* self)
{
	((RecordArray *)self)->Fini();
	PyFeObject_DEL(self);
}

static int RecordArray_len(PyObject* self)
{
	RecordArray *pRA;
	pRA = (RecordArray *)self;
	fe::sp<fe::RecordArray> *pspRA = pRA->getRecordArray();

	return (*pspRA)->length();
}

static PyObject *RecordArray_getitem(PyObject* self, int index)
{
	RecordArray *pRA;
	pRA = (RecordArray *)self;
	fe::sp<fe::RecordArray> *pspRA = pRA->getRecordArray();

	if(index >= (*pspRA)->length() || index < 0)
	{
		PyErr_SetString(PyExc_IndexError, "index out of range");
		return NULL;
	}

	Record *pRecord;
	pRecord = PyFeObject_NEW(Record, &Record_T);
	fe::Record record = (*pspRA)->getRecord(index);
	pRecord->Init(record);

	return (PyObject *)pRecord;
}

static PyObject *RecordArray_getslice(PyObject* self, int start, int end)
{
	RecordArray *pRA;
	pRA = (RecordArray *)self;
	fe::sp<fe::RecordArray> *pspRA = pRA->getRecordArray();

	if(end < start) { end = start; }

	if(start >= (*pspRA)->length() || start < 0)
	{
		PyErr_SetString(PyExc_IndexError, "index out of range");
		return NULL;
	}

	if(end > (*pspRA)->length() || end < 0)
	{
		PyErr_SetString(PyExc_IndexError, "index out of range");
		return NULL;
	}

	PyObject *pList = PyList_New(0);

	Record *pRecord;
	for(int i = start; i < end; i++)
	{
		pRecord = PyFeObject_NEW(Record, &Record_T);
		fe::Record record = (*pspRA)->getRecord(i);
		pRecord->Init(record);
		PyList_Append(pList, (PyObject *)pRecord);
	}

	return (PyObject *)pList;
}

static int RecordArray_setitem(PyObject* self, int index, PyObject* obj)
{
	RecordArray *pRA;
	pRA = (RecordArray *)self;
	fe::sp<fe::RecordArray> *pspRA = pRA->getRecordArray();

	if(index >= (*pspRA)->length() || index < 0)
	{
		PyErr_SetString(PyExc_IndexError, "index out of range");
		return -1;
	}

	fe::Record *pR;
	if(obj->ob_type == &Record_T)
	{
		pR = ((Record *)obj)->getRecord();
	}
	else
	{
		PyErr_SetString(PyExc_TypeError,
			"record array element must be of type record");
		return -1;
	}

	fe::Record record = (*pspRA)->getRecord(index);
	record = *pR;
	return 0;
}

static PyObject *RecordArray_getattr(PyObject *self, char *name)
{
	return Py_FindMethod(RecordArray_Methods, self, name);
}

static PyObject *RecordArray_check(PyObject* self, PyObject* args)
{
	RecordArray *pRA;
	pRA = (RecordArray *)self;
	fe::sp<fe::RecordArray> *pspRA = pRA->getRecordArray();
	char *str;
	PyObject *arg;

	for(UWORD i = 0; i < (U32)(PyTuple_Size(args)); i++)
	{
		arg = PyTuple_GetItem(args, i);
		if(PyString_Check(arg))
		{
			str = PyString_AsString(arg);
			if(!fe::checkAttribute(str, *((*pspRA)->layout())))
			{
				return Py_BuildValue("");
			}
		}
	}
	return Py_BuildValue("i",1);
}

static PyObject *RecordArray_add(PyObject* self, PyObject* args)
{
	RecordArray *pRA;
	pRA = (RecordArray *)self;
	fe::sp<fe::RecordArray> *pspRA = pRA->getRecordArray();
	PyObject *arg;

	for(UWORD i = 0; i < (U32)(PyTuple_Size(args)); i++)
	{
		arg = PyTuple_GetItem(args, i);
		fe::Record *pR;
		if(arg->ob_type == &Record_T)
		{
			pR = ((Record *)arg)->getRecord();
		}
		else
		{
			PyErr_SetString(PyExc_TypeError,
				"record array element must be of type record");
			return Py_BuildValue("");
		}

		(*pspRA)->add(*pR, NULL);
	}
	return Py_BuildValue("i",1);
}


// ---------------------------------------------------------------------------
/* Instance ******************************************************************/
static void Instance_dealloc(PyObject* self)
{
	((Instance *)self)->Fini();
	PyFeObject_DEL(self);
}

// ---------------------------------------------------------------------------
/* Component *****************************************************************/
static void Component_dealloc(PyObject* self)
{
	((Component *)self)->Fini();
	PyFeObject_DEL(self);
}

// ---------------------------------------------------------------------------
/* pyfe Module Methods *******************************************************/
static PyObject *registry_manage(PyObject* self, PyObject* args)
{
	if(PyTuple_Size(args) == 1)
	{
		PyObject *arg;
		arg = PyTuple_GetItem(args, 0);
		if(PyString_Check(arg))
		{
			try
			{
				fe::String s = PyString_AsString(arg);
				fe::sp<fe::Registry> spRegistry(pyfe::feMaster().registry());
				if(spRegistry->manage(s))
				{
					return Py_BuildValue("i", 1);
				}
				return Py_BuildValue("i", 0);
			}
			catch(fe::Exception &e)
			{
				PyErr_SetString(PyExc_SyntaxError,e.getMessage().c_str());
				return NULL;
			}
		}
	}

	PyErr_SetString(PyExc_SyntaxError,"requires single string argument");
	return NULL;
}

static PyObject *registry_create(PyObject* self, PyObject* args)
{
	if(PyTuple_Size(args) == 1)
	{
		PyObject *arg;
		arg = PyTuple_GetItem(args, 0);
		if(PyString_Check(arg))
		{
			try
			{
				fe::String s = PyString_AsString(arg);
				fe::sp<fe::Registry> spRegistry(pyfe::feMaster().registry());
				fe::sp<fe::Component> spComponent;
				spComponent = spRegistry->create(s);

				return bindComponent(spComponent);
			}
			catch(fe::Exception &e)
			{
				PyErr_SetString(PyExc_SyntaxError,e.getMessage().c_str());
				return NULL;
			}
		}
	}

	PyErr_SetString(PyExc_SyntaxError,"requires single string argument");
	return NULL;
}

static PyObject *registry_prune(PyObject* self, PyObject* args)
{
	if(PyTuple_Size(args) == 0)
	{
		fe::sp<fe::Registry> spRegistry(pyfe::feMaster().registry());
		return Py_BuildValue("i", spRegistry->prune());
	}

	PyErr_SetString(PyExc_SyntaxError,"requires no arguments");
	return NULL;
}


// ---------------------------------------------------------------------------
/* C++ fe hooks **************************************************************/

FE_DL_EXPORT PyObject *bindRecord(const fe::Record &record)
{
	Record *pRecord = (Record *)PyFeObject_NEW(pyfe::Record, &pyfe::Record_T);
	pRecord->Init(record);
	return (PyObject *)pRecord;
}

FE_DL_EXPORT PyObject *bindRecordArray(const fe::sp<fe::RecordArray> spRA)
{
	RecordArray *pRA = (RecordArray *)
		PyFeObject_NEW(pyfe::RecordArray, &pyfe::RecordArray_T);
	pRA->Init(spRA);
	return (PyObject *)pRA;
}

FE_DL_EXPORT fe::Record extractRecord(PyObject *pRecord)
{
	if(pRecord->ob_type != &Record_T)
	{
		throw fe::Exception(
			"attempt to extract fe::Record from non Record python object");
	}
	fe::Record *pR = ((Record *)pRecord)->getRecord();
	return *pR;
}

FE_DL_EXPORT fe::sp<fe::RecordArray> extractRecordArray(PyObject *pRecordArray)
{
	if(pRecordArray->ob_type != &RecordArray_T)
	{
		throw fe::Exception(
			"attempt to extract fe::RecordArray from non RecordArray "
			"python object");
	}
	fe::sp<fe::RecordArray> *pspRA =
		((RecordArray *)pRecordArray)->getRecordArray();
	return *pspRA;
}

FE_DL_EXPORT PyObject *bindComponent(const fe::sp<fe::Component> spComponent)
{
	Component *pC = (Component *)
		PyFeObject_NEW(pyfe::Component, &pyfe::Component_T);
	pC->Init(spComponent);
	return (PyObject *)pC;
}

FE_DL_EXPORT fe::sp<fe::Component> extractComponent(PyObject *pComponent)
{
	if(pComponent->ob_type != &Component_T)
	{
		throw fe::Exception(
			"attempt to extract fe::Component from non Component "
			"python object");
	}
	fe::sp<fe::Component> *pspComponent =
		((Component *)pComponent)->getComponent();
	return *pspComponent;
}

FE_DL_EXPORT fe::Instance extractInstance(PyObject *pInstance)
{
	if(pInstance->ob_type != &Instance_T)
	{
		throw fe::Exception(
			"attempt to extract instance from non Instance python object");
	}
	fe::Instance *pI = ((Instance *)pInstance)->getInstance();
	return *pI;
}

FE_DL_EXPORT PyObject *bindInstance(fe::Instance &instance)
{
	Instance *pInstance;
	pInstance = PyFeObject_NEW(Instance, &Instance_T);
	pInstance->Init(instance);
	return (PyObject *)pInstance;
}

FE_DL_EXPORT void addConvertor(fe::sp<fe::BaseType> spBT, fe::sp<BaseConvertor> spConvertor)
{
	convertors_t *pConvertors = pyfeMaster()->m_pConvertors;
	(*pConvertors)[spBT] = spConvertor;
}

Master *pyfeMaster(void)
{
	return (Master *)lookupPyObject("fe.pyfe", "PyFeMaster");
}

PyObject *lookupPyObject(char *module_name, char *object_name)
{
	PyObject *modules = PyImport_GetModuleDict();
	if(!modules)
	{
		throw fe::Exception("could not get python modules dict");
	}
	PyObject *module = PyDict_GetItemString(modules, module_name);
	if(!module)
	{
		throw fe::Exception("could not find %s module", module_name);
	}
	PyObject *dict = PyModule_GetDict(module);
	if(!module)
	{
		throw fe::Exception("could not find %s's dict", module_name);
	}
	PyObject *obj = PyDict_GetItemString(dict, object_name);
	if(!obj)
	{
		throw fe::Exception("could not find %s.%s", module_name, object_name);
	}
	return obj;
}

FE_DL_EXPORT fe::Master &feMaster(void)
{
	return *(pyfeMaster()->m_pMaster);
}

} /* namespace */



// ---------------------------------------------------------------------------
// ---------------------------------------------------------------------------
namespace pyfe
{
FE_DL_EXPORT PyInterpreterState *ms_pInterp = NULL;
FE_DL_EXPORT boost::thread_specific_sp<PyThreadState *> tss_tstate;
}

FE_DL_EXPORT void initialize_pyfe(void)
{
	PyEval_InitThreads();
	PyThreadState *pThreadState = PyThreadState_Get();
	pyfe::tss_tstate.reset(new PyThreadState *);
	*pyfe::tss_tstate = pThreadState;
	pyfe::ms_pInterp = pThreadState->interp;
	pyfe::Master *pMaster =
		(pyfe::Master *)PyFeObject_NEW(pyfe::Master, &pyfe::Master_T);
	pMaster->Init();
	PyObject *pModule = Py_InitModule("pyfe", pyfe::PyFeMethods);
	int n = PyModule_AddObject(pModule,
		"PyFeMaster", (PyObject *)pMaster);
	if(n!=0)
	{
		throw fe::Exception("pyfe Master initialization error");
	}
}


