/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __pyfe_Convertor_h__
#define __pyfe_Convertor_h__
/*	This is separated from BaseConvertor and the rest of pyfe since it is
	dependent on boost.python and is just for convienience.
	*/
#include <boost/python.hpp>
using namespace boost::python;

namespace pyfe
{
template<class rawtype, class bindtype>
class Convertor : public BaseConvertor
{
	public:
		Convertor(object boostclass)
		{
			m_boostclass = boostclass;
		}
virtual	~Convertor(void) { }
virtual	PyObject		*instanceToPyObject(fe::sp<fe::BaseType> spBT, fe::Instance &instance)
		{
			if(instance.type()->typeinfo() == spBT->typeinfo())
			{
				object o = m_boostclass();
				extract<bindtype&> ex(o);
				bindtype &bt = ex();

				bt.assign( *(reinterpret_cast<rawtype *>(instance.data())));
				Py_INCREF(o.ptr());
				return o.ptr();
			}
			return Py_BuildValue("");
		}
virtual	bool	PyObjectToInstance(fe::sp<fe::BaseType> spBT, PyObject *pObject, fe::Instance &instance)
		{
			handle<> h(borrowed(pObject));
			object o(h);
			extract<bindtype&> x(o);
			if(!x.check())
			{
				return false;
			}
			bindtype &bt = x();
			rawtype *pR = bt.rawptr();
			fe::Instance i(reinterpret_cast<void *>(pR),spBT,NULL);
			instance = i;
			return true;
		}
	private:
		object m_boostclass;
};

} /* namepsace */

#endif /* __pyfe_Convertor_h__ */
