/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "pyfe.pmh"

namespace pyfe
{

Master::Master(void)
{
	throw fe::Exception("cannot C++ construct pyfe::Master");
}

void Master::Init(void)
{
	m_pConvertors =new std::map<fe::sp<fe::BaseType>, fe::sp<BaseConvertor> >;
	m_pMaster = new fe::Master;
	m_pspTypeMaster = new fe::sp<fe::TypeMaster>;
	*m_pspTypeMaster = m_pMaster->typeMaster();
	(*m_pspTypeMaster)->assertType<int>("py_int");
	(*m_pspTypeMaster)->assertType<float>("py_float");
	(*m_pspTypeMaster)->assertType<fe::String>("py_string");
}

void Master::Fini(void)
{
	*m_pspTypeMaster=NULL;
	delete m_pspTypeMaster;
	delete m_pMaster;
	delete m_pConvertors;
}

void Record::Init(const fe::Record &record)
{
	m_pRecord = new fe::Record();
	*m_pRecord = record;
}

void Record::Init(void)
{
	m_pRecord = new fe::Record();
}

void Record::Fini(void)
{
	FEASSERT(m_pRecord);
	if(m_pRecord) { delete m_pRecord; }
}

fe::Record *Record::getRecord(void)
{
	return m_pRecord;
}

void RecordArray::Init(fe::sp<fe::RecordArray>	recordarray)
{
	m_pspRecordArray = new fe::sp<fe::RecordArray>;
	*m_pspRecordArray = recordarray;
}

void RecordArray::Init(void)
{
	m_pspRecordArray = new fe::sp<fe::RecordArray>;
}

void RecordArray::Fini(void)
{
	*m_pspRecordArray=NULL;
	delete m_pspRecordArray;
}

fe::sp<fe::RecordArray> *RecordArray::getRecordArray(void)
{
	return m_pspRecordArray;
}

void Instance::Init(void)
{
	m_pInstance = new fe::Instance();
}

void Instance::Init(fe::Instance instance)
{
	m_pInstance = new fe::Instance(instance);
}

void Instance::Fini(void)
{
	delete m_pInstance;
}

fe::Instance *Instance::getInstance(void)
{
	return m_pInstance;
}

void Component::Init(fe::sp<fe::Component>	spComponent)
{
	m_pspComponent = new fe::sp<fe::Component>;
	*m_pspComponent = spComponent;
}

void Component::Init(void)
{
	m_pspComponent = new fe::sp<fe::Component>;
}

void Component::Fini(void)
{
	*m_pspComponent = NULL;
	delete m_pspComponent;
}

fe::sp<fe::Component> *Component::getComponent(void)
{
	return m_pspComponent;
}


} /* namespace */

