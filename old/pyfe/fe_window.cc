/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "signal/signal.h"
#include "fex/math.h"
#include "fex/pyfe.h"
#include "fex/window.h"
#include "fex/viewer.h"
#include "window/WindowEvent.h"

#include <boost/python.hpp>
using namespace boost::python;
#include "pyfe/fe_data.h"
#include "pyfe/fe_signal.h"
#include "pyfe/fe_window.h"

void windowExceptionTranslator(fe::Exception const& e)
{
	PyErr_SetString(PyExc_UserWarning, e.getMessage().c_str());
}

BOOST_PYTHON_MODULE(window)
{
	register_exception_translator<fe::Exception>(&windowExceptionTranslator);
	class_<WindowFS>("WindowFS", init<PyObject *>())
		.def("open", &WindowFS::open)
		.def("close", &WindowFS::close)
		.def("setSize", &WindowFS::setSize)
		.def("makeCurrent", &WindowFS::makeCurrent)
		.def("clear", &WindowFS::clear)
		.def("swapBuffers", &WindowFS::swapBuffers)
		.def("getEventContextI", &WindowFS::getEventContextI)
		.def("object", &WindowFS::object)
	;

	class_<ViewerFS>("ViewerFS", init<PyObject *>())
		.def("bind", &ViewerFS::bind)
		.def("object", &ViewerFS::object)
	;

	class_<EventMapFS>("EventMapFS", init<PyObject *>())
		.def("bind", &EventMapFS::bind)
	;

	class_<QuickViewerFS>("QuickViewerFS", init<PyObject *>())
		.def("insertDrawHandler", &QuickViewerFS::insertDrawHandler)
		.def("run", &QuickViewerFS::run)
		.def("object", &QuickViewerFS::object)
	;

	enum_<fe::WindowEvent::Source>("source")
		.value("null",			fe::WindowEvent::e_sourceNull)
		.value("position",		fe::WindowEvent::e_sourceMousePosition)
		.value("button",		fe::WindowEvent::e_sourceMouseButton)
		.value("kb",			fe::WindowEvent::e_sourceKeyboard)
		.value("system",		fe::WindowEvent::e_sourceSystem)
		.value("any",			fe::WindowEvent::e_sourceAny)
	;

	enum_<fe::WindowEvent::Item>("item")
		.value("null",			fe::WindowEvent::e_itemNull)
		.value("any",			fe::WindowEvent::e_itemAny)
		.value("move",			fe::WindowEvent::e_itemMove)
		.value("drag",			fe::WindowEvent::e_itemDrag)
		.value("left",			fe::WindowEvent::e_itemLeft)
		.value("middle",		fe::WindowEvent::e_itemMiddle)
		.value("right",			fe::WindowEvent::e_itemRight)
		.value("wheel",			fe::WindowEvent::e_itemWheel)
		.value("backspace",		fe::WindowEvent::e_keyBackspace)
		.value("escape",		fe::WindowEvent::e_keyEscape)
		.value("delete",		fe::WindowEvent::e_keyDelete)
		.value("asciiMask",		fe::WindowEvent::e_keyAsciiMask)
		.value("cursor",		fe::WindowEvent::e_keyCursor)
		.value("cursorUp",		fe::WindowEvent::e_keyCursorUp)
		.value("cursorDown",	fe::WindowEvent::e_keyCursorDown)
		.value("cursorLeft",	fe::WindowEvent::e_keyCursorLeft)
		.value("cursorRight",	fe::WindowEvent::e_keyCursorRight)
		.value("cursorMask",	fe::WindowEvent::e_keyCursorMask)
		.value("keyShift",		fe::WindowEvent::e_keyShift)
		.value("keyCapsLock",	fe::WindowEvent::e_keyCapLock)
		.value("keyControl",	fe::WindowEvent::e_keyControl)
		.value("keyUsed",		fe::WindowEvent::e_keyUsed)
		.value("idle",			fe::WindowEvent::e_itemIdle)
		.value("resize",		fe::WindowEvent::e_itemResize)
		.value("expose",		fe::WindowEvent::e_itemExpose)
		.value("closeWindow",	fe::WindowEvent::e_itemCloseWindow)
		.value("quit",			fe::WindowEvent::e_itemQuit)
	;

	enum_<fe::WindowEvent::State>("state")
		.value("null",			fe::WindowEvent::e_stateNull)
		.value("press",			fe::WindowEvent::e_statePress)
		.value("release",		fe::WindowEvent::e_stateRelease)
		.value("repeat",		fe::WindowEvent::e_stateRepeat)
		.value("any",			fe::WindowEvent::e_stateAny)
	;

	enum_<fe::WindowEvent::MouseButtons>("mb")
		.value("none",			fe::WindowEvent::e_mbNone)
		.value("left",			fe::WindowEvent::e_mbLeft)
		.value("middle",		fe::WindowEvent::e_mbMiddle)
		.value("right",			fe::WindowEvent::e_mbRight)
		.value("all",			fe::WindowEvent::e_mbAll)
	;

}

bool WindowFS::open(const std::string &title)
{
	fe::String t = title.c_str();
	return !(ptr()->open(t) & FE_FAILURE);
}

void WindowFS::close(void)
{
	ptr()->close();
}

void WindowFS::setSize(unsigned int x, unsigned int y)
{
	ptr()->setSize(x,y);
}

void WindowFS::makeCurrent(void)
{
	ptr()->makeCurrent();
}

void WindowFS::clear(float r, float g, float b, float a)
{
	fe::Vector4f v(r,g,b,a);
	return ptr()->clear(v);
}

void WindowFS::swapBuffers(void)
{
	ptr()->swapBuffers();
}

PyObject *WindowFS::getEventContextI(void)
{
	return pyfe::bindComponent(ptr()->getEventContextI());
}

void ViewerFS::bind(PyObject *pyObject)
{
	fe::sp<fe::WindowI> spWindowI = pyfe::extractComponent(pyObject);
	ptr()->bind(spWindowI);
}

void QuickViewerFS::insertDrawHandler(HandlerFS &handlerPtr)
{
	ptr()->insertDrawHandler(handlerPtr.ptr());
}

void QuickViewerFS::run(unsigned int frames)
{
	ptr()->run(frames);
}

void EventMapFS::bind(PyObject *pyObjectRecord, const LayoutPtr &layoutPtr)
{
	fe::Record record = pyfe::extractRecord(pyObjectRecord);
	ptr()->bind(record, layoutPtr.ptr());
	
}

