/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "fex/pyfe.h"
#include "pyfe/Convertor.h"

#include "signal/signal.h"

using namespace boost::python;
#include "pyfe/fe_data.h"
#include "pyfe/fe_signal.h"

namespace pyfe
{
FE_DL_IMPORT extern PyInterpreterState *ms_pInterp;
FE_DL_IMPORT extern boost::thread_specific_sp<PyThreadState *> tss_tstate;
}

PyHandler::PyHandler(PyObject *pPyObject)
{
	m_pObject = pPyObject;
	PyObject *pType = PyObject_Type(pPyObject);
	PyObject *pTypeName = PyObject_Str(pType);
	setName(PyString_AsString(pTypeName));
	Py_DECREF(pType);
	Py_DECREF(pTypeName);
	Py_INCREF(m_pObject);
}

PyHandler::~PyHandler(void)
{
	Py_DECREF(m_pObject);
}


void PyHandler::handle(fe::Record &signal)
{
	//static int count = 0;
//fprintf(stderr,"A %d %p\n", Py_IsInitialized(), pyfe::tss_tstate.get());
	PyThreadState *tstate = NULL;
	//PyObject *dict = (PyObject *)NULL;
	//dict = PyThreadState_GetDict();
	PyThreadState *_save;
	PyInterpreterState *interp;
	if(!pyfe::tss_tstate.get())
	{
//fprintf(stderr,"B\n");
		//interp = PyInterpreterState_New();
		tstate = PyThreadState_New(pyfe::ms_pInterp);
//fprintf(stderr,"C\n");
		//tstate = PyThreadState_New(interp);
		//pyfe::tss_tstate.reset(new PyThreadState *);
		//*pyfe::tss_tstate = tstate;
//fprintf(stderr,"*** acquire %d \n", count++);
		PyEval_AcquireThread(tstate);
	}
#if 0
	else if(!fe::tid.get())
	{
		PyEval_AcquireThread(*pyfe::tss_tstate);
		//_save = PyEval_SaveThread();
	}
#endif


	PyObject *pSignal = pyfe::bindRecord(signal);
	boost::python::handle<PyObject> h(pSignal);
	call_method<void>(m_pObject, "handle", h);

	if(!pyfe::tss_tstate.get())
	{
		PyEval_ReleaseThread(tstate);
//fprintf(stderr,"*** release %d\n", --count);
		PyThreadState_Delete(tstate);
		//PyInterpreterState_Clear(interp);
		//PyInterpreterState_Delete(interp);
	}
#if 0
	else if(!fe::tid.get())
	{
//fprintf(stderr,"*** pre release \n");
		PyEval_ReleaseThread(*pyfe::tss_tstate);
//fprintf(stderr,"*** release %d\n", --count);
		//PyEval_RestoreThread(_save);
	}
#endif
}

void PyHandler::bind(fe::sp<fe::SignalerI> spS, fe::sp<fe::Layout> spL)
{
	PyObject *layoutClass = pyfe::lookupPyObject("fe.data", "Layout");
	object layout = call<object>(layoutClass);

	extract<LayoutPtr&> ex(layout);
	LayoutPtr &layoutPtr = ex();

	layoutPtr.assign(spL);

	boost::python::handle<> h(pyfe::bindComponent(spS));
	PyObject *signalerClass = pyfe::lookupPyObject("fe.signal", "SignalerFS");
	object signaler = call<object>(signalerClass, h);

	call_method<void>(m_pObject, "bind", signaler, layout);
}

HandlerBase::HandlerBase(void)
{
}

HandlerBase::HandlerBase(PyObject *pP)
{
}

HandlerBase::~HandlerBase(void)
{
}

HandlerFS::HandlerFS(PyObject *self_)
	: FunctionSet<fe::HandlerI>(
		fe::sp<fe::Component>(new PyHandler(self_)))
{
}

HandlerFS::~HandlerFS(void)
{
}

PyObject *HandlerFS::object(void)
{
	return FunctionSet<fe::HandlerI>::object();
}

void ParserFS::parse(PyObject *pTokens)
{
	fprintf(stderr,"HandlerFS::setup\n");
	if(!PyList_Check(pTokens))
	{
		feX("HandlerFS::setup",
			"argument must be a list of strings");
	}
	std::vector<fe::String> tokens;
	int sz = PyList_Size(pTokens);
	for(int i = 0; i < sz; i++)
	{
		PyObject *pStr = PyList_GetItem(pTokens, i);
		if(!PyString_Check(pStr))
		{
			feX("HandlerFS::setup",
				"argument must be a list of strings");
		}
		std::string std_string(PyString_AsString(pStr));
		tokens.push_back(PyString_AsString(pStr));
	}

	ptr()->parse(tokens);
}

void SignalerFS::remove(HandlerFS &handlerPtr, const LayoutPtr &layoutPtr)
{
	ptr()->remove(handlerPtr.ptr(), layoutPtr.ptr());
}

void SignalerFS::insert(HandlerFS &handlerPtr, const LayoutPtr &layoutPtr)
{
	ptr()->insert(handlerPtr.ptr(), layoutPtr.ptr());
}

void SignalerFS::signal(PyObject *pRecord)
{
	fe::Record sig = pyfe::extractRecord(pRecord);
	ptr()->signal(sig);
}

void SignalerFS::dump(void)
{
	fe::Peeker peek;
	peek(*ptr());
	feLog(peek.output().c_str());
}

U32 SequencerFS::getCurrentTime(void)
{
	return ptr()->getCurrentTime();
}

void SequencerFS::setTime(U32 counter)
{
	return ptr()->setTime(counter);
}

PyObject *SequencerFS::add(const LayoutPtr &layoutPtr, U32 startTime, U32 interval)
{
	fe::Record record = ptr()->add(layoutPtr.ptr(), startTime, interval);
	return pyfe::bindRecord(record);
}

PyObject *SequencerFS::lookup(const LayoutPtr &layoutPtr, U32 interval)
{
	fe::Record record = ptr()->lookup(layoutPtr.ptr(), interval);
	return pyfe::bindRecord(record);
}

bool SequencerFS::remove(PyObject *pyobject)
{
	fe::Record record = pyfe::extractRecord(pyobject);
	return ptr()->remove(record);
}



