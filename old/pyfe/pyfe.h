/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __fe_pyfe_h__
#define __fe_pyfe_h__

#include "fe/data.h"

#if FE_CODEGEN<=FE_DEBUG && (FE_OS==FE_WIN32 || FE_OS==FE_WIN64)
#include <boost/python/detail/wrap_python.hpp>
//#include <Python.h>
#else
#include <Python.h>
#endif

#if FE_CODEGEN<=FE_DEBUG && (FE_OS==FE_WIN32 || FE_OS==FE_WIN64)
#define FE_PYTHON_MODULE(mod) BOOST_PYTHON_MODULE(mod)
#else
#define FE_PYTHON_MODULE(mod) BOOST_PYTHON_MODULE(mod)
#endif

namespace pyfe
{

//extern PyInterpreterState *ms_pInterp;
//extern boost::thread_specific_sp<PyThreadState *> tss_tstate;

class BaseConvertor : public fe::Counted
{
	public:
virtual				~BaseConvertor(void) {}
virtual	PyObject	*instanceToPyObject(fe::sp<fe::BaseType> spBT,
							fe::Instance &instance)							= 0;
virtual	bool		PyObjectToInstance(fe::sp<fe::BaseType> spBT,
							PyObject *pObject, fe::Instance &instance)		= 0;
};

FE_DL_EXPORT void addConvertor(fe::sp<fe::BaseType> spBT, fe::sp<BaseConvertor> spCnvrtr);
FE_DL_EXPORT PyObject *bindRecord(const fe::Record &record);
FE_DL_EXPORT PyObject *bindRecordArray(const fe::sp<fe::RecordArray> spRA);
FE_DL_EXPORT fe::Record extractRecord(PyObject *pRecord);
FE_DL_EXPORT fe::sp<fe::RecordArray> extractRecordArray(PyObject *pRecord);
FE_DL_EXPORT fe::Instance extractInstance(PyObject *pInstance);
FE_DL_EXPORT PyObject *bindInstance(fe::Instance &instance);
FE_DL_EXPORT PyObject *lookupPyObject(char *module_name, char *object_name);
FE_DL_EXPORT fe::Master &feMaster(void);
FE_DL_EXPORT PyObject *bindComponent(const fe::sp<fe::Component> spComponent);
FE_DL_EXPORT fe::sp<fe::Component> extractComponent(PyObject *pComponent);

} /* namespace */

class BaseFunctionSet
{
	public:
virtual				~BaseFunctionSet(void) {}
virtual	PyObject	*object(void)											= 0;
};

template<class T>
class FunctionSet
{
	public:
#if 0
		FunctionSet(ComponentPtr cptr)
		{
			m_sp = cptr.ptr();
		}
#endif
		FunctionSet(PyObject *pPyObject)
		{
			m_sp = pyfe::extractComponent(pPyObject);
		}
		FunctionSet(fe::sp<fe::Component> spComponent)
		{
			m_sp = spComponent;
		}
virtual	~FunctionSet(void) {}

		fe::sp<T>	ptr(void)
		{
			assertValid();
			return m_sp;
		}
		fe::sp<T>	*rawptr(void)
		{
			assertValid();
			return &m_sp;
		}
		void		assign(fe::sp<T> tptr)
		{
			m_sp = tptr;
		}
virtual	PyObject	*object(void)
		{
			return pyfe::bindComponent(m_sp);
		}

	protected:
		void	assertValid(void)
		{
			if(!m_sp.isValid())
			{
				throw fe::Exception("invalid function set");
			}
		}
	private:
		fe::sp<T> m_sp;
};



#endif /* __fe_pyfe_h__ */
