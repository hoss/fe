/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __pyfe_init_h__
#define __pyfe_init_h__


extern "C"
{
	void	initpyfe(void);
}


#endif /* __pyfe_init_h__ */

