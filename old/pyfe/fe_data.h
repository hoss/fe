/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __data_fe_data_h__
#define __data_fe_data_h__

class ScopePtr;

class LayoutPtr
{
	public:
							LayoutPtr(void);
							LayoutPtr(PyObject *pObj);
		LayoutPtr			&operator=(fe::sp<fe::Layout> spLayout);
		LayoutPtr			populate(const std::string &attr,
								const std::string &typ);
		ScopePtr			scope(void);
		fe::sp<fe::Layout>	ptr(void) const;
		void				assign(fe::sp<fe::Layout> &spL);
		std::string			name(void);
	private:
		fe::sp<fe::Layout>	m_spLayout;
};

class PyWatcher : public fe::WatcherI
{
	public:
		PyWatcher(PyObject *pPyObject);
virtual	~PyWatcher(void);
virtual	void		add(const fe::Record &record);
virtual	void		remove(const fe::Record &record);
virtual	void		clear(void);
	private:
		PyObject *m_pObject;
};

class WatcherBase
{
	public:
		WatcherBase(void);
		WatcherBase(PyObject *pP);
virtual	~WatcherBase(void);
};

class WatcherFS : public WatcherBase, public FunctionSet<fe::WatcherI>
{
	public:
		WatcherFS(PyObject *self_, PyObject *pP):
				FunctionSet<fe::WatcherI>(pP)								{}
		WatcherFS(PyObject *self_);
		PyObject	*object(void);
virtual	~WatcherFS(void);
};


class ScopePtr
{
	public:
							ScopePtr(void);
							ScopePtr(fe::sp<fe::Scope> spScope);
							~ScopePtr(void);
		LayoutPtr			declare(const std::string &name);
		LayoutPtr			lookupLayout(const std::string &name);
		PyObject			*createRecord(const LayoutPtr &layoutPtr);
		PyObject			*createRecordArray(const LayoutPtr &layoutPtr);
		bool				clear(const std::string &name);
		void				dump(void);
		void				support(	const std::string &mayHave,
										const std::string &ofType);
		void				enforce(	const std::string &ifHas,
										const std::string &mustHave);
		void				populate(	const std::string &ifIs,
										const std::string &mustHave);
		void				addWatcher(	WatcherFS &watcher);

		fe::sp<fe::Scope>	ptr(void) const;
	private:
		fe::sp<fe::Scope>	m_spScope;
};


#endif /* __data_fe_data_h__ */

