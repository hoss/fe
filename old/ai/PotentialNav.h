/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __ai_PotentialNav_h__
#define __ai_PotentialNav_h__

#include "signal/signal.h"
#include "math/math.h"
#include "datatool/datatool.h"
#include "shape/shape.h"
#include "mobility/mobilityAS.h"
#include "aiAS.h"

namespace fe
{

/**	
	@copydoc PotentialNav_info
	*/
class FE_DL_EXPORT PotentialNav :
	public Initialize<PotentialNav>,
	virtual public HandlerI,
	virtual public Config
{
	public:
				PotentialNav(void);
virtual			~PotentialNav(void);

		void	initialize(void);

				// AS HandlerI
virtual void	handle(	Record &r_sig);

	private:
		AsMobile		m_asMobile;
		AsParticle		m_asParticle;
		AsNav			m_asNav;
		AsNavable		m_asNavable;
};

} /* namespace */

#endif /* __ai_PotentialNav_h__ */

