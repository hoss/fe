/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __ai_GotoOrigin_h__
#define __ai_GotoOrigin_h__

#include "signal/signal.h"
#include "math/math.h"
#include "datatool/datatool.h"
#include "shape/shape.h"
#include "mobility/mobilityAS.h"

namespace fe
{

/**	dummy test object

	@copydoc GotoOrigin_info
	*/
class FE_DL_EXPORT GotoOrigin :
	public Initialize<GotoOrigin>,
	virtual public HandlerI,
	virtual public Config
{
	public:
				GotoOrigin(void);
virtual			~GotoOrigin(void);

		void	initialize(void);

				// AS HandlerI
virtual void	handle(	Record &r_sig);

	private:
		AsMobile		m_asMobile;
		AsParticle		m_asParticle;
};


} /* namespace */

#endif /* __ai_GotoOrigin_h__ */

