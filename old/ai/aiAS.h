/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __ai_aiAS_h__
#define __ai_aiAS_h__

#include "spatial/spatial.h"

namespace fe
{

class AsNav
	: public AsPoint, public Initialize<AsNav>
{
	public:
		void initialize(void)
		{
			add(field,		FE_SPEC("ai:field",			"navigation field"));
			add(calculated,	FE_SPEC("_ai:target",		"calculated target"));
		}
		/// navigation field
		Accessor< sp<Component> >				field;
		/// calculated target (internal)
		Accessor<SpatialVector>				calculated;
};

class AsNavDebug
	: public AccessorSet, public Initialize<AsNavDebug>
{
	public:
		void initialize(void)
		{
			add(potential,	FE_SPEC("ai:potential",	"potential field"));
		}
		/// potential field
		Accessor<sp<Component> >				potential;
};

class AsNavable
	: public AccessorSet, public Initialize<AsNavable>
{
	public:
		void initialize(void)
		{
			add(nav,	FE_SPEC("ai:nav", "nav record"));
		}
		/// nav record
		Accessor<Record>		nav;
};

} /* namespace */

#endif /* __ai_aiAS_h__ */

