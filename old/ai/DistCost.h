/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __ai_DistCost_h__
#define __ai_DistCost_h__

#include "fe/plugin.h"
#include "math/math.h"
#include "StepCostI.h"

namespace fe
{

class FE_DL_EXPORT DistCost :
	virtual public StepCostI
{
	public:
				DistCost(void);
virtual			~DistCost(void);

				// AS StepCostI
virtual	Real	cost(const SpatialVector &a_from, const SpatialVector &a_to);
};

} /* namespace */

#endif /* __ai_DistCost_h__ */

