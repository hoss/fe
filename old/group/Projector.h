/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __group_Projector_h__
#define __group_Projector_h__

namespace fe
{
namespace group
{

class Projector : public HandlerI, public ParserI
{
	public:
		Projector(void);
virtual	~Projector(void);

		// AS HandlerI
virtual	void	handle(Record &signal);
virtual	void	bind(sp<SignalerI> spSignalerI, sp<Layout> spLayout);

		// AS ParserI
virtual	void	parse(std::vector<String> &attributeNames);
	private:
		void	doSetup(void);
	private:
		Accessor<sp<RecordGroup> >		m_aInput;
		Accessor<sp<RecordGroup> >		m_aOutput;
		Accessor<String>				m_aLayout;
		std::vector<String>				m_attributeNames;
		sp<Scope>						m_spScope;
};

} /* namespace */
} /* namespace */

#endif /* __group_Projector_h__ */

