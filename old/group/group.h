/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __fex_group_h__
#define __fex_group_h__

#include "signal/signal.h"

#include "group/Projector.h"
#include "group/Insert.h"

#endif /* __fex_group_h__ */

