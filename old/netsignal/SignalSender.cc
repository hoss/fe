/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <netsignal/netsignal.pmh>

namespace fe
{
namespace ext
{

SignalSender::SignalSender(int sid, sp<Scope> spScope,
	sp<ChunkSpool> spChunkSpool)
{
	m_spScope = spScope;
	m_spStream = new data::BinaryStream(m_spScope);
	m_spChunkSpool = spChunkSpool;
	m_spRG = new RecordGroup();
	m_sid = sid;
	m_aSID.setup(spScope, "_SID_", "integer");
	m_maxspoolsize = 0;
}

SignalSender::~SignalSender(void)
{
}

void SignalSender::initialize(void)
{
}

void SignalSender::handle(Record &signal)
{
//	feLog("SignalSender::handle \"%s\"\n",signal.layout()->name().c_str());

	int *sid = m_aSID.queryAttribute(signal);
//	feLog("SignalSender::handle sid %d vs %d\n",sid? *sid: -1,m_sid);

	if(sid)
	{
		if(*sid == 0)
		{
			*sid = m_sid;
		}
		else if(*sid != (int)m_sid)
		{
			return;
		}
	}
	std::ostringstream ost;
	m_spRG->add(signal);
	m_spStream->output(ost, m_spRG);
	m_spRG->clear();

	sp<Chunk> spChunk(new Chunk(ost.str()));

	if(m_maxspoolsize>0)
	{
		I32 waitCount(1000);
		while((waitCount--)>0 && m_spChunkSpool->size()>=m_maxspoolsize)
		{
//			minimumSleep();
			nanoSpin(10e3);
		}
	}

	if((m_maxspoolsize == 0) || (m_spChunkSpool->size() < m_maxspoolsize))
	{
//		feLog("SignalSender::handle putting Chunk for \"%s\"\n",
//				signal.layout()->name().c_str());
		m_spChunkSpool->put(spChunk.abandon());
	}
	else
	{
		feLog("SignalSender::handle dumped Chunk for \"%s\"\n",
				signal.layout()->name().c_str());
	}
}

} /* namespace ext */
} /* namespace fe */

