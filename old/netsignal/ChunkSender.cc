/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <netsignal/netsignal.pmh>

#define FE_CHS_DEBUG	FALSE

namespace fe
{
namespace ext
{

ChunkSender::ChunkSender(void)
{
	m_operating = false;
	m_live = false;
}

ChunkSender::~ChunkSender(void)
{
	stop();
}

void ChunkSender::start(sp<ChunkSpool> spChunkSpool, sp<Socket> spSocket,
	String ioPriority)
{
#if FE_CHS_DEBUG
	feLog("ChunkSender::start\n");
#endif

	m_spChunkSpool = spChunkSpool;
	m_chunkSenderTask.m_spChunkSpool = spChunkSpool;
	m_chunkSenderTask.m_spSocket = spSocket;
	m_chunkSenderTask.m_pChunkSender = this;
	m_live = true;
	m_pChunkSenderThread = new Thread(&m_chunkSenderTask);
	m_pChunkSenderThread->config("priority",ioPriority);
	m_operating = true;
}

void ChunkSender::stop(void)
{
	if(m_operating)
	{
		m_spChunkSpool->interrupt();
		m_pChunkSenderThread->join();
		delete m_pChunkSenderThread;
		m_operating = false;
	}
}

bool ChunkSender::live(void)
{
	Mutex::Guard guard(m_liveMutex);
	return m_live;
}

void ChunkSender::setLive(bool setting)
{
	Mutex::Guard guard(m_liveMutex);
	m_live = setting;
}


ChunkSenderTask::ChunkSenderTask(void)
{
	m_pChunkSender = NULL;
}

void ChunkSenderTask::operate(void)
{
	try
	{
		m_pChunkSender->setLive(true);
		sp<Chunk> spChunk;
		while(true)
		{
#if FE_CHS_DEBUG
			feLogGroup("netsignal","ChunkSenderTask waiting to write chunk\n");
#endif
			spChunk = m_spChunkSpool->wait();
#if FE_CHS_DEBUG
			feLogGroup("netsignal","chunk send size %u\n", spChunk->size());
#endif
			if(spChunk->size() == 0)
			{
				feX(e_writeFailed,
					"ChunkSenderTask",
					"todo: decide on what to do with zero size chunks");
			}
			int n;

			//* NOTE length is now embeded at start of message
/*
			U32 nsize = htonl(spChunk->size());
			n = m_spSocket->write((const void *)&nsize, sizeof(U32));
			if(n== -1)
			{
				feLog("ChunkSenderThread::operate socket error:\n    %s\n",
						errorString(FE_ERRNO).c_str());

				feX(e_cancel, "ChunkSenderThread socket error");
			}
			if(n != sizeof(U32))
			{
				feX(e_writeFailed,
					"ChunkSenderTask","connection failed in size write");
			}
*/

			n = m_spSocket->write(spChunk->buffer(), spChunk->size());
			if(n== -1)
			{
				feLog("ChunkSenderThread::operate socket error:\n    %s\n",
						errorString(FE_ERRNO).c_str());

				feX(e_cancel, "ChunkSenderThread socket error");
			}
			if(n != (IWORD)(spChunk->size()))
			{
				feX(e_writeFailed,
					"ChunkSenderTask","connection failed in chunk write");
			}
		}
	}
	catch(Poisoned &p)
	{
		m_pChunkSender->setLive(false);

#if FE_CHS_DEBUG
		feLogGroup("netsignal","ChunkSenderTask poisoned\n");
#endif
	}
	catch(Exception &e)
	{
		m_pChunkSender->setLive(false);
		if(e.getResult() == e_cancel)
		{
			feLogGroup("netsignal", "ChunkSenderTask thread terminating due to"
					" closed socket:\n");
			feLogGroup("netsignal","  %s\n", e.getMessage().c_str());
		}
		else if(e.getResult() == e_writeFailed)
		{
			feLogGroup("netsignal","thread dying due to write failure\n");
		}
		else
		{
			feLogGroup("netsignal","thread dying due to unknown failure\n");

//			exit(1);
			throw e;
		}
	}
}

} /* namespace ext */
} /* namespace fe */


