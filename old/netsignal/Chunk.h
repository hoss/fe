/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __netsignal_ChunkSpool_h__
#define __netsignal_ChunkSpool_h__
namespace fe
{
namespace ext
{

/**
*/
class FE_DL_EXPORT Chunk : public Counted
{
	public:
		Chunk(const std::string &s);
		Chunk(FE_UWORD size);
virtual	~Chunk(void);

		const void			*buffer(void);
		void				resize(FE_UWORD a_size);
		FE_UWORD			size(void);
		std::string			string(void);

	private:
		std::string			m_string;
		FE_UWORD			m_size;
};

typedef Spool<Chunk> ChunkSpool;

} /* namespace ext */
} /* namespace fe */

#endif /* __netsignal_ChunkSpool_h__ */

