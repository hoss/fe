/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "netsignal/netsignal.h"

namespace fe
{
namespace ext
{

BSDServer::BSDServer(void)
{
	m_operating = false;
}

BSDServer::~BSDServer(void)
{
	stop();
}

void BSDServer::addLayout(sp<Layout> spLayout)
{
	m_layouts.push_back(spLayout);
}

void BSDServer::start(sp<SignalerI> spSignaler, unsigned short a_port,
	sp<Layout> spPollLayout,Socket::Transport transport,String ioPriority)
{
//	feLog("BSDServer:start %d\n",a_port);

	fe::ext::SockAddr server_addr;
	server_addr.setPort(a_port);

	if(m_operating)
	{
		feX(e_unsupported,
			"fe::BSDServer::start",
			"cannot start already started Server");
	}
	m_operating = true;
	m_spSocketHandler = new SocketHandler(server_addr, transport, ioPriority);
	m_spSignaler = spSignaler;
	m_spPollLayout = spPollLayout;
	for(std::list<sp<Layout> >::iterator it = m_layouts.begin();
		it != m_layouts.end(); it++)
	{
		m_spSocketHandler->addSenderLayout(*it);
	}
	m_spSignaler->insert(m_spSocketHandler, m_spPollLayout);
}

void BSDServer::stop(void)
{
	if(m_operating)
	{
		if(m_spSignaler.isValid())
		{
			m_spSignaler->remove(m_spSocketHandler, m_spPollLayout);
		}
		m_spSocketHandler = NULL;
		m_operating = false;
	}
}

} /* namespace ext */
} /* namespace fe */

