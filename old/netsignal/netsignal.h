/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __fe_netsignal_h__
#define __fe_netsignal_h__

#include "signal/signal.h"
#include "datatool/datatool.h"
#include "network/network.h"

#include "netsignal/Socket.h"
#include "netsignal/Spool.h"
#include "netsignal/Chunk.h"
#include "netsignal/ChunkSender.h"
#include "netsignal/ChunkReceiver.h"
#include "netsignal/SignalReceiver.h"
#include "netsignal/SignalSender.h"
#include "netsignal/Listener.h"
#include "netsignal/SocketHandler.h"
#include "netsignal/Server.h"
#include "netsignal/Client.h"

namespace fe
{
namespace ext
{

Library *CreateNetSignalLibrary(sp<Master>);

} /* namespace ext */
} /* namespace fe */

#endif /* __fe_netsignal_h__ */

