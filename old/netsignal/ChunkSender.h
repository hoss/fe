/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __netsignal_ChunkSender_h__
#define __netsignal_ChunkSender_h__

namespace fe
{
namespace ext
{

class ChunkSender;

class FE_DL_EXPORT ChunkSenderTask: public Thread::Functor
{
	public:
						ChunkSenderTask(void);
virtual	void			operate(void);

		sp<ChunkSpool>	m_spChunkSpool;
		sp<Socket>		m_spSocket;
		ChunkSender*	m_pChunkSender;
};

class FE_DL_EXPORT ChunkSender : public Component
{
	public:
				ChunkSender(void);
virtual			~ChunkSender(void);

virtual	void	start(sp<ChunkSpool> spChunkSpool, sp<Socket> spSocket,
					String ioPriority="normal");
virtual	void	stop(void);
virtual	bool	live(void);

virtual	void	setLive(bool setting);

	private:
		Thread*				m_pChunkSenderThread;
		ChunkSenderTask		m_chunkSenderTask;
		bool				m_operating;
		sp<ChunkSpool>		m_spChunkSpool;
		bool				m_live;
		Mutex				m_liveMutex;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __netsignal_ChunkSender_h__ */

