/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <netsignal/netsignal.pmh>

namespace fe
{
namespace ext
{

Chunk::Chunk(FE_UWORD size):
	m_string(size, '\0'),
	m_size(size)
{
}

Chunk::Chunk(const std::string &s)
{
	m_string = s;
	m_size=m_string.length();
}

Chunk::~Chunk(void)
{
}

const void *Chunk::buffer(void)
{
	return (const void *)(m_string.data());
}

void Chunk::resize(FE_UWORD a_size)
{
	m_size=a_size;

	if(m_string.capacity()<a_size)
	{
		m_string.resize(a_size);
	}

	const FE_UWORD capacity=m_string.capacity();
	m_string.replace(0,capacity,capacity,'\0');
}

FE_UWORD Chunk::size(void)
{
	return m_size;
}

std::string Chunk::string(void)
{
	return m_string;
}

} /* namespace ext */
} /* namespace fe */

