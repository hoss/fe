/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "netsignal/netsignal.pmh"

#define FE_NTC_VERBOSE			FALSE
#define FE_NTC_ROLE_CHECK		FALSE	//* filter out any echoed signals

namespace fe
{
namespace ext
{

NetworkCatalog::NetworkCatalog(void)
{
#if FE_NTC_VERBOSE
	feLog("NetworkCatalog::NetworkCatalog\n");
#endif

//	m_lockOnUpdate=FALSE;
}

NetworkCatalog::~NetworkCatalog(void)
{
#if FE_NTC_VERBOSE
	feLog("NetworkCatalog::~NetworkCatalog\n");
#endif

	disconnect();
}

void NetworkCatalog::initialize(void)
{
//	m_spScope=registry()->create("Scope");
	m_spScope=new fe::Scope(*registry()->master().raw());
	m_spScope->setName("NetworkCatalog.Scope");

	m_ttl.setup(m_spScope,FE_USE(":TTL"));
	m_command.setup(m_spScope,"command");
	m_key.setup(m_spScope,"key");
	m_property.setup(m_spScope,"property");
	m_type.setup(m_spScope,"type");
	m_text.setup(m_spScope,"text");
	m_byteBlock.setup(m_spScope,"byteBlock");

#if FE_NTC_ROLE_CHECK
	m_source.setup(m_spScope,"source");
#endif

	m_spLayoutHB=m_spScope->declare("heartbeat");

	m_signalHB=m_spScope->createRecord(m_spLayoutHB);

	m_spHandler=new Handler;
	m_spHandler->setName("NetworkCatalog.Handler");
	m_spHandler->bind(sp<NetworkCatalog>(this));

	m_spSignaler=registry()->create("SignalerI");
	m_spSignaler->setName("NetworkCatalog.Signaler");

	Socket::startup();
}

Result NetworkCatalog::start(void)
{
	const BWORD isClient=
			(catalogOrDefault<String>("net:role","client")=="client");

	//* NOTE the same outgoing layout name on both client and server will loop

	if(isClient)
	{
		m_spLayoutSend=m_spScope->declare("client_update");
		m_spLayoutReceive=m_spScope->declare("server_update");
	}
	else
	{
		m_spLayoutSend=m_spScope->declare("server_update");
		m_spLayoutReceive=m_spScope->declare("client_update");
	}

	m_spLayoutSend->populate(m_ttl);
	m_spLayoutSend->populate(m_command);
	m_spLayoutSend->populate(m_key);
	m_spLayoutSend->populate(m_property);
	m_spLayoutSend->populate(m_type);
	m_spLayoutSend->populate(m_text);
	m_spLayoutSend->populate(m_byteBlock);

	m_spLayoutReceive->populate(m_ttl);
	m_spLayoutReceive->populate(m_command);
	m_spLayoutReceive->populate(m_key);
	m_spLayoutReceive->populate(m_property);
	m_spLayoutReceive->populate(m_type);
	m_spLayoutReceive->populate(m_text);
	m_spLayoutReceive->populate(m_byteBlock);

#if FE_NTC_ROLE_CHECK
	m_spLayoutSend->populate(m_source);
	m_spLayoutReceive->populate(m_source);
#endif

	m_spSignaler->insert(m_spHandler,m_spLayoutReceive);

	return ConnectedCatalog::start();
}

Result NetworkCatalog::flush(void)
{
	Mutex::Guard guard(m_signalMutex);

	return ConnectedCatalog::flush();
}

void NetworkCatalog::heartbeat(void)
{
	ConnectedCatalog::heartbeat();

#if FE_NTC_VERBOSE
	feLog("NetworkCatalog::heartbeat signal\n");
#endif

//	feLog("NetworkCatalog::heartbeat signalQueue %d\n",m_signalQueue.size());
	while(m_signalQueue.size())
	{
		Record signal=m_signalQueue.front();
		m_signalQueue.pop_front();

		m_spSignaler->signal(signal);
	}

	m_spSignaler->signal(m_signalHB);
}

void NetworkCatalog::service(void)
{
	ConnectedCatalog::service();

	Mutex::Guard guard(m_signalMutex);

	heartbeat();
}

Result NetworkCatalog::connectAsServer(String a_address,U16 a_port)
{
#if FE_NTC_VERBOSE
	feLog("NetworkCatalog::connectAsServer port %d\n",a_port);
#endif

	const Socket::Transport transportType=
			(catalogOrDefault<String>("net:transport","udp") == "udp")?
			Socket::e_udp: Socket::e_tcp;
	const String ioPriority=
			catalogOrDefault<String>("net:ioPriority","normal");

	m_spServer=registry()->create("ServerI");
	if(!m_spServer.isValid())
	{
		feX("NetworkCatalog::connectAsServer","could not create ServerI");
	}

	m_spServer->addLayout(m_spLayoutSend);

	try
	{
#if FE_NTC_VERBOSE
		feLog("NetworkCatalog::connectAsServer starting server\n");
#endif

		m_spServer->start(m_spSignaler,a_port,m_spLayoutHB,
				transportType,ioPriority);

#if FE_NTC_VERBOSE
		feLog("NetworkCatalog::connectAsServer started server\n");
#endif
	}
	catch(...)
	{
		feLog("NetworkCatalog::connectAsServer server failed\n");
		m_spServer->stop();
		return e_refused;
	}

	return ConnectedCatalog::connectAsServer(a_address,a_port);
}

Result NetworkCatalog::connectAsClient(String a_address,U16 a_port)
{
#if FE_NTC_VERBOSE
	feLog("NetworkCatalog::connectAsClient \"%s\" port %d\n",
			a_address.c_str(),a_port);
#endif

	const Socket::Transport transportType=
			(catalogOrDefault<String>("net:transport","udp") == "udp")?
			Socket::e_udp: Socket::e_tcp;
	const String ioPriority=
			catalogOrDefault<String>("net:ioPriority","normal");

	m_spClient=registry()->create("ClientI");
	if(!m_spClient.isValid())
	{
		feX("NetworkCatalog::handle","could not create ClientI");
	}

	m_spClient->addLayout(m_spLayoutSend);
	for(I32 attempt=0;attempt<10;attempt++)
	{
		try
		{
#if FE_NTC_VERBOSE
			feLog("NetworkCatalog::connectAsClient starting client\n");
#endif

			m_spClient->start(m_spSignaler,a_address,a_port,
					m_spLayoutHB,transportType,ioPriority);

#if FE_NTC_VERBOSE
			feLog("NetworkCatalog::connectAsClient started client\n");
#endif
			break;
		}
		catch(...)
		{
			feLog("NetworkCatalog::handle client no connection\n");
			m_spClient->stop();
			stall();
		}
	}

	return ConnectedCatalog::connectAsClient(a_address,a_port);
}

Result NetworkCatalog::disconnect(void)
{
#if FE_NTC_VERBOSE
	feLog("NetworkCatalog::disconnect\n");
#endif

	if(connected() && m_spClient.isValid() && !m_spClient->receiving())
	{
		feLog("NetworkCatalog::disconnect client not receiving\n");
		return e_readFailed;
	}

	Result result=ConnectedCatalog::disconnect();

	m_spServer=NULL;
	m_spClient=NULL;

	return result;
}

void NetworkCatalog::broadcastSelect(String a_name,String a_property,
	String a_message,
	I32 a_includeCount,const String* a_pIncludes,
	I32 a_excludeCount,const String* a_pExcludes,
	const U8* a_pRawBytes,I32 a_byteCount)
{
#if FE_NTC_VERBOSE
	feLog("NetworkCatalog::broadcastSelect \"%s\" \"%s\"\n",
			a_name.c_str(),a_property.c_str());
#endif

	if(m_spSignaler.isNull())
	{
		feLog("NetworkCatalog::broadcastSelect null Signaler\n");
		return;
	}

	const BWORD removing=(a_message.empty() &&
			!cataloged(a_name,a_property));
	const BWORD isSignal=catalogOrDefault<bool>(a_name,"signal",false);
	const BWORD isTick=(a_name=="net:tick");

	Record signalUpdate=m_spScope->createRecord(m_spLayoutSend);
	m_ttl(signalUpdate)=2;
	m_key(signalUpdate)=a_name;
	m_property(signalUpdate)=a_property;

	if(isTick)
	{
		m_command(signalUpdate)="tick";
		m_key(signalUpdate)="";
		m_property(signalUpdate)="";
		m_type(signalUpdate)="";
		m_text(signalUpdate)="";
	}
	else if(removing)
	{
		m_command(signalUpdate)="remove";
		m_type(signalUpdate)="";
		m_text(signalUpdate)="";
	}
	else
	{
		m_command(signalUpdate)=isSignal? "signal": "update";

		if(!a_message.empty())
		{
			m_type(signalUpdate)="string";
			m_text(signalUpdate)=a_message;
		}
		else
		{
			const String typeString=catalogTypeName(a_name,a_property);
			m_type(signalUpdate)=typeString;

			if(typeString=="bytearray")
			{
				Array<U8>& rByteArray=catalog< Array<U8> >(a_name,a_property);

				m_byteBlock(signalUpdate)=rByteArray;
			}
			else
			{
				m_text(signalUpdate)=catalogValue(a_name,a_property);
			}
		}
	}

#if FE_NTC_ROLE_CHECK
	m_source(signalUpdate)=role();
#endif

#if FE_NTC_VERBOSE
	feLog("NetworkCatalog::broadcastSelect '%s' sending signal\n",
			m_command(signalUpdate).c_str());
#endif

	m_signalQueue.push_back(signalUpdate);
//	m_spSignaler->signal(signalUpdate);

//	heartbeat();
}

void NetworkCatalog::Handler::handle(Record &a_signal)
{
#if FE_NTC_VERBOSE
	feLog("NetworkCatalog::Handler::handle\n");
#endif

#if FE_NTC_VERBOSE
	if(m_hpNetworkCatalog->disconnecting())
	{
		feLog("NetworkCatalog::Handler::handle disconnecting\n");
	}
#endif

#if FE_NTC_VERBOSE
	if(m_hpNetworkCatalog->m_command.check(a_signal))
	{
		feLog("  command:  \"%s\"\n",
				m_hpNetworkCatalog->m_command(a_signal).c_str());
	}
	if(m_hpNetworkCatalog->m_key.check(a_signal))
	{
		feLog("  key:  \"%s\"\n",
				m_hpNetworkCatalog->m_key(a_signal).c_str());
	}
	if(m_hpNetworkCatalog->m_property.check(a_signal))
	{
		feLog("  prop: \"%s\"\n",
				m_hpNetworkCatalog->m_property(a_signal).c_str());
	}
	if(m_hpNetworkCatalog->m_type.check(a_signal))
	{
		feLog("  type: \"%s\"\n",
				m_hpNetworkCatalog->m_type(a_signal).c_str());
	}
	if(m_hpNetworkCatalog->m_text.check(a_signal))
	{
		feLog("  text: \"%s\"\n",
				m_hpNetworkCatalog->m_text(a_signal).c_str());
	}
#endif

#if FE_NTC_ROLE_CHECK
	String source="";
	if(m_hpNetworkCatalog->m_source.check(a_signal))
	{
		source=m_hpNetworkCatalog->m_source(a_signal);

#if FE_NTC_VERBOSE
		feLog("  source:  \"%s\"\n",source.c_str());
#endif
	}

	//* no circular self-setting
	if(source==m_hpNetworkCatalog->role())
	{
#if FE_NTC_VERBOSE
		feLog("NetworkCatalog::Handler::handle ignore source \"%s\"\n",
				source.c_str());
#endif
		return;
	}
#endif

	if(!m_hpNetworkCatalog->m_command.check(a_signal) ||
			!m_hpNetworkCatalog->m_key.check(a_signal) ||
			!m_hpNetworkCatalog->m_property.check(a_signal) ||
			!m_hpNetworkCatalog->m_type.check(a_signal) ||
			!m_hpNetworkCatalog->m_text.check(a_signal) ||
			!m_hpNetworkCatalog->m_byteBlock.check(a_signal))
	{
		return;
	}

	String source;
	Array<U8>& rByteArray=m_hpNetworkCatalog->m_byteBlock(a_signal);
	const I32 byteCount=rByteArray.size();
	const U8* bRawBytes=byteCount? rByteArray.data(): NULL;

	const String command=m_hpNetworkCatalog->m_command(a_signal);
	const BWORD isSignal=(command=="signal");
	if(isSignal || command=="update")
	{
		m_hpNetworkCatalog->update(isSignal? e_signal: e_update,source,
				m_hpNetworkCatalog->m_key(a_signal),
				m_hpNetworkCatalog->m_property(a_signal),
				m_hpNetworkCatalog->m_type(a_signal),
				m_hpNetworkCatalog->m_text(a_signal),
				bRawBytes,byteCount);
	}
	else if(command=="remove")
	{
		m_hpNetworkCatalog->update(e_remove,source,
				m_hpNetworkCatalog->m_key(a_signal),
				m_hpNetworkCatalog->m_property(a_signal),
				"",
				"");
	}
	else if(command=="tick")
	{
//		feLog("TICK\n");
	}
	else
	{
		feLog("NetworkCatalog::Handler::handle"
				" unknown command \"%s\"\n",command.c_str());
	}
}

} /* namespace ext */
} /* namespace fe */

