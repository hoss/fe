/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <netsignal/netsignal.pmh>

#define FE_SCK_VERBOSE	FALSE

namespace fe
{
namespace ext
{

#if FE_OS==FE_WIN32 || FE_OS==FE_WIN64
bool Socket::m_wsaStarted = false;
#endif

Socket::Socket(void):
	m_sock_handle(-1),
	m_transportType(e_tcp),
	m_server(FALSE)
{
	m_open = false;
	m_original = true;
	m_pPoison = NULL;
	m_id = 0;
	m_nextID = 1;
#if FE_OS==FE_WIN32 || FE_OS==FE_WIN64
	if(!Socket::m_wsaStarted)
	{
		startup();
	}
#endif
}

Socket::Socket(Poison *pPoison):
	m_sock_handle(-1),
	m_transportType(e_tcp),
	m_server(FALSE)
{
	m_open = false;
	m_original = true;
	m_pPoison = pPoison;
	m_id = 0;
	m_nextID = 1;
#if FE_OS==FE_WIN32 || FE_OS==FE_WIN64
	if(!Socket::m_wsaStarted)
	{
		startup();
	}
#endif
}

Socket::~Socket(void)
{
	close();
}

bool Socket::isOpen(void)
{
	return m_open;
}

U32 Socket::id(void)
{
	return m_id;
}

void SockAddr::setAddress(const String &host)
{
	if(host == "0.0.0.0")
	{
		m_sockaddr.sin_addr.s_addr = INADDR_ANY;
		return;
	}

	m_sockaddr.sin_addr.s_addr = getIp4Address(host);
	if(m_sockaddr.sin_addr.s_addr == 0)
	{
		feX("fe::SockAddr::setAddress","unknown host: %s", host.c_str());
	}
}

// static
U32 SockAddr::getIp4Address(const String &host)
{
	U32 address;

	if (host.empty())
	{
		return 0;
	}

	// If host is in dotted notation...
	address = inet_addr(host.c_str());

	if (INADDR_NONE != address)
	{
		return address;
	}

	// If host is *not* in dotted notation...
	struct hostent *h = gethostbyname(host.c_str());
	if ((struct hostent *) 0 != h)
	{
		address = *((U32 *) (h->h_addr_list[0]));
		return address;
	}

	return 0;
}

String SockAddr::hostname(void)
{
	String name;

	struct sockaddr_in sa;
	sa.sin_family = AF_INET;
	sa.sin_addr=m_sockaddr.sin_addr;

	char node[NI_MAXHOST];
	if(getnameinfo((struct sockaddr*)&sa, sizeof(sa), node, sizeof(node),
			NULL, 0, NI_NUMERICHOST) == 0)
	{
//		feLog("SockAddr::hostname, node %s\n", node);
		return node;
	}

	//* NOTE gethostbyaddr is flaky
	struct hostent *hostent_ptr =
			gethostbyaddr((const char *)&m_sockaddr.sin_addr.s_addr,
			sizeof(struct in_addr), AF_INET);
	if(hostent_ptr != NULL)
	{
		return (char *) hostent_ptr->h_name;
	}

	return "_unknown_";
}


SockHandle Socket::socket(int domain, int type, int protocol)
{
	if((m_sock_handle = ::socket(domain, type, protocol)) < 0)
	{
		feX(e_system,
			"fe::Socket::socket",
			errorString(FE_ERRNO).c_str());
	}
	return m_sock_handle;
}

void Socket::openStream(Transport transport,BWORD server)
{
	m_transportType = transport;
	m_server = server;
	if(m_transportType == Socket::e_tcp)
	{
#if FE_SCK_VERBOSE
		feLog("Socket::openStream TCP as %s\n", server? "server": "client");
#endif
		socket(AF_INET, SOCK_STREAM, 0);
	}
	else
	{
#if FE_SCK_VERBOSE
		feLog("Socket::openStream UDP as %s\n", server? "server": "client");
#endif
		socket(AF_INET, SOCK_DGRAM, 0);
	}
}

void Socket::bind(SockAddr &sockaddr)
{
#if FE_SCK_VERBOSE
	feLog("Socket::bind \"%s\" %d\n",
			sockaddr.hostname().c_str(),sockaddr.port());
#endif

	if(::bind(m_sock_handle, sockaddr.ptr(), sockaddr.size()) < 0)
	{
		feLogGroup("netsignal","Socket::bind failed\n");
		feX(e_system,
			"fe::Socket::bind",
			errorString(FE_ERRNO).c_str());
	}
}

void Socket::listen(int backlog)
{
	if(m_transportType == Socket::e_udp)
	{
		return;
	}

	if(::listen(m_sock_handle, backlog) < 0)
	{
		feX(e_system,
			"fe::Socket::listen",
			errorString(FE_ERRNO).c_str());
	}
}

sp<Socket> Socket::accept(SockAddr &sockaddr)
{
	sp<Socket> new_sock(new Socket);

	while(true)
	{
#if FE_OS==FE_WIN32 || FE_OS==FE_WIN64
		int size;
		size = sockaddr.size();
#else
		socklen_t size;
		size = (socklen_t)(sockaddr.size());
#endif

		if(m_transportType == Socket::e_tcp)
		{
#if FE_SCK_VERBOSE
			feLog("Socket::accept waiting\n");
#endif
			wait();
		}

#if FE_SCK_VERBOSE
		feLog("Socket::accept accepting\n");
#endif

		if(m_transportType == Socket::e_udp)
		{
			new_sock->m_original=false;
			new_sock->m_sock_handle=m_sock_handle;
			new_sock->m_udpSockAddrOwn=m_udpSockAddrOwn;
		}

		if(m_transportType == Socket::e_tcp &&
			(new_sock->m_sock_handle =
			::accept(m_sock_handle, sockaddr.ptr(), &size) ) < 0)
		{
			if(FE_ERRNO == FE_EINTR)
			{
				continue;
			}
			else
			{
				feX(e_system,
					"fe::Socket::accept",
					errorString(FE_ERRNO).c_str());
			}
		}
		else
		{
			new_sock->m_open = true;
			new_sock->m_transportType = m_transportType;

			U32 net_id;
			{
				SAFEGUARD;
				net_id = htonl(m_nextID);
				new_sock->m_id = m_nextID;
				m_nextID++;
			}

#if FE_SCK_VERBOSE
			feLog("Socket::accept write ID 0x%x (%d)\n",
					net_id,ntohl(net_id));
#endif

			int n = new_sock->write((void *)(&net_id), sizeof(U32));
			if(n != sizeof(U32))
			{
				feLog("Socket::accept wrote %d/%d\n",n,sizeof(U32));

				feX(e_system,
					"fe::Socket::accept",
					errorString(FE_ERRNO).c_str());
			}

#if FE_SCK_VERBOSE
			feLog("Socket::accept accepted\n");
#endif

			return new_sock;
		}
	}
}

void Socket::connect(SockAddr &sockaddr)
{
	if(m_transportType == e_udp)
	{
#if FE_SCK_VERBOSE
		feLog("Socket::connect UDP as \"%s\" %d\n",
				sockaddr.hostname().c_str(),sockaddr.port());
#endif

		// UDP does not connect, just save the address.
		m_udpSockAddrOwn.setPort(sockaddr.port());
		m_udpSockAddrOwn.setAddress(sockaddr.hostname());

		m_open = true;
	}
	else
	{
		if(::connect(m_sock_handle, sockaddr.ptr(), sockaddr.size()) < 0)
		{
			feX(e_system,
				"fe::Socket::connect",
				errorString(FE_ERRNO).c_str());
		}
		m_open = true;
	}

	if(m_server)
	{
		if(m_transportType == e_udp)
		{
#if FE_SCK_VERBOSE
			feLogGroup("netsignal","Socket::connect waiting for response\n");
#endif

			U32 deed=0x0;
			int n = read((void *)(&deed), sizeof(U32));
			if(n != sizeof(U32))
			{
				feLogGroup("netsignal","Socket::connect read %d/%d\n",
						n,sizeof(U32));
				feX(e_system,
					"fe::Socket::connect",
					errorString(FE_ERRNO).c_str());
			}
			if(deed!=0xDEED)
			{
				feLogGroup("netsignal",
						"Socket::connect wrong response token 0x%x\n",deed);
			}
		}

		return;
	}

	if(m_transportType == e_udp)
	{
#if FE_SCK_VERBOSE
		feLogGroup("netsignal","Socket::connect sending token response\n");
#endif

		while(TRUE)
		{
			U32 deed=0xDEED;
			int n = write((void *)(&deed), sizeof(U32));
			if(n != sizeof(U32))
			{
				feLogGroup("netsignal","Socket::connect wrote %d/%d\n",
						n,sizeof(U32));
				feX(e_system,
					"fe::Socket::connect",
					errorString(FE_ERRNO).c_str());
			}

			//* NOTE need to give the server a chance to respond
			I32 tryCount=4;
			while(tryCount)
			{
				const ReadMode readMode=ReadMode(e_readPeek|e_readNoWait);
				U32 word=0x0;
				n = read((void *)(&word), sizeof(U32), readMode);
//				feLog("Socket::connect n %d word 0x%x\n",n,word);

				if(n>0)
				{
					break;
				}

				feLog("Socket::connect waiting for the server\n");
				milliSleep(1000);
				tryCount--;
			}

			if(n>0)
			{
				break;
			}

#if FE_OS==FE_WIN32 || FE_OS==FE_WIN64
			feLog("Socket::connect reopening socket\n");
			close();
			openStream(m_transportType,m_server);
			m_open=TRUE;
#endif
		}
	}

#if FE_SCK_VERBOSE
	feLogGroup("netsignal","Socket::connect waiting for id\n");
#endif

	U32 net_id;
	int n = read((void *)(&net_id), sizeof(U32));
	if(n != sizeof(U32))
	{
		feLogGroup("netsignal","Socket::connect read %d/%d\n",
				n,sizeof(U32));
		feX(e_system,
			"fe::Socket::connect",
			errorString(FE_ERRNO).c_str());
	}

	m_id = ntohl(net_id);

#if FE_SCK_VERBOSE
	feLogGroup("netsignal","Socket::connect id %u\n", m_id);
#endif
}

void Socket::close(void)
{
//	feLog("Socket::close %d open %d original %d\n",
//			m_sock_handle,m_open,m_original);

	if(m_open)
	{
		if(m_original)
		{
#if FE_OS==FE_WIN32 || FE_OS==FE_WIN64
			if(closesocket(m_sock_handle) != 0)
#else
			if(::close(m_sock_handle) < 0)
#endif
			{
				feX(e_system,
					"fe::Socket::close",
					errorString(FE_ERRNO).c_str());
			}
		}
		m_sock_handle = -1;
		m_open = false;
	}
}

void Socket::setPoison(Poison *pPoison)
{
	m_pPoison = pPoison;
}

void Socket::wait(void)
{
	if(m_pPoison == NULL)
	{
#if FE_SCK_VERBOSE
		feLogGroup("netsignal","Socket::wait NULL poison\n");
#endif

		return;
	}
	fd_set rfds;
	struct timeval tv;
	int ready = 0;
	while(!ready)
	{
		if(m_pPoison->active())
		{
			close();

#if FE_SCK_VERBOSE
#endif
			feLogGroup("netsignal","Socket::wait throwing poison\n");

			throw Poisoned();
		}
		tv.tv_sec = 1;
		tv.tv_usec = 0;
		FD_ZERO(&rfds);
		FD_SET(m_sock_handle, &rfds);

#if FE_SCK_VERBOSE
		feLogGroup("netsignal","Socket::wait selecting %d\n",m_sock_handle);
#endif
		ready = select(m_sock_handle+1, &rfds, NULL, NULL, &tv);
	}
}

IWORD Socket::read(const void *pBuffer, IWORD count, ReadMode readMode)
{
	if(!m_open)
	{
		feX(e_readFailed,
			"fe::Socket::read",
			"attempt to read from closed socket");
	}

	if(m_transportType == Socket::e_udp)
	{
		return udpRead(pBuffer, count, readMode);
	}

#if FE_SCK_VERBOSE
	feLog("Socket::read TCP %p count %d readMode %d from socket %d\n",
			pBuffer,count,readMode,m_sock_handle);
#endif

#if FE_OS==FE_WIN32 || FE_OS==FE_WIN64 || FE_OS==FE_OSX
	int flags = 0;
#else
	int flags = MSG_NOSIGNAL;
#endif

	IWORD offset=0;
	U32 newCount=count;
	char* pNewBuffer=(char*)pBuffer;
	if(readMode&e_readPeek)
	{
		flags |= MSG_PEEK;
	}
	else
	{
		offset=sizeof(U32);
		newCount+=offset;
		pNewBuffer=new char[newCount];
	}

	IWORD nleft;
	IWORD nread;
	char *ptr = pNewBuffer;
	nleft = newCount;
	while(nleft > 0)
	{
		wait();
		if(( nread = ::recv(m_sock_handle, ptr, nleft, flags) ) < 0 )
		{

			feLog("Socket::read bytes read %d/%d\n", nread, nleft);
			if(FE_ERRNO == FE_EINTR)
			{
				nread = 0;
			}
			else
			{
				feLogGroup("netsignal","%u %s\n", FE_ERRNO,
						errorString(FE_ERRNO).c_str());
				return -1;
			}
		}
		else if(nread == 0)
		{
			break;
		}
#if FE_SCK_VERBOSE
		feLog("Socket::read TCP bytes read %d\n",nread);
#endif
		//feLog("Socket: read+ newCount: %d\n", nread);
		nleft -= nread;
		ptr += nread;
	}

	if(!(readMode&e_readPeek))
	{
		memcpy((void*)pBuffer,pNewBuffer+offset,count);
		delete[] pNewBuffer;
	}

	return count - nleft;
}

IWORD Socket::udpRead(const void *pBuffer, IWORD count, ReadMode readMode)
{
#if FE_SCK_VERBOSE
	feLog("Socket::udpRead %p count %d readMode %d from socket %d \"%s\" %d\n",
			pBuffer,count,readMode,m_sock_handle,
			m_udpSockAddrOwn.hostname().c_str(),m_udpSockAddrOwn.port());
#endif

	IWORD offset=0;
	U32 newCount=count;
	char* pNewBuffer=(char*)pBuffer;
	U32 headerCount=0;
	if(!(readMode&e_readPeek))
	{
		offset=sizeof(U32);
		newCount+=offset;
		pNewBuffer=new char[newCount];
	}

	IWORD nleft;
	IWORD nread;
	char *ptr = pNewBuffer;
	nleft = newCount;
	socklen_t slen = m_udpSockAddrOwn.size();
	while(nleft > 0)
	{
		if(readMode&e_readNoWait)
		{
			fd_set rfds;
			FD_ZERO(&rfds);
			FD_SET(m_sock_handle, &rfds);

//			struct timeval tv;
//			tv.tv_sec = 0;
//			tv.tv_usec = 0;
//			const int ready=select(m_sock_handle+1, &rfds, NULL, NULL, &tv);

			const int ready=select(m_sock_handle+1, &rfds, NULL, NULL, NULL);
			if(ready<=0)
			{
				return -1;
			}
		}
		else
		{
			wait();
		}
//		feLog("Socket::udpRead %s\n",
//				m_udpSockAddrOwn.hostname().c_str());

		//* win32 doesn't support MSG_DONTWAIT
//		const int readFlags=
//				((readMode&e_readPeek)? MSG_PEEK: 0) +
//				((readMode&e_readNoWait)? MSG_DONTWAIT: 0);
		const int readFlags=(readMode&e_readPeek)? MSG_PEEK: 0;

		nread = ::recvfrom(m_sock_handle, ptr, nleft, readFlags,
				m_udpSockAddrOwn.ptr(), &slen);

//		feLog("Socket::udpRead return %s\n",
//				m_udpSockAddrOwn.hostname().c_str());

#if FE_OS==FE_WIN32 || FE_OS==FE_WIN64
		//* win32 recvfrom returns -1 if buffer was too small for whole message
		if((readMode&e_readPeek) && nread<0 && WSAGetLastError()==WSAEMSGSIZE)
		{
			nread=count;
		}
#endif

		if(!(readMode&e_readPeek) && nleft==newCount)
		{
			headerCount=ntohl(*(U32*)ptr);

			if(headerCount!=count)
			{
				feLog("Socket::udpRead header %d expected %d <<<<<<<<<<<<<<\n",
						headerCount,count);
			}
		}

#if FE_SCK_VERBOSE
		feLog("Socket::udpRead read %d/%d header %d\n",
				nread,newCount,headerCount);
		if(count==4)
		{
			feLog("  read U32 0x%x (%d)\n",
					*(U32*)(ptr+offset),
					htonl(*(U32*)(ptr+offset)));
		}
		else if(count==1)
		{
			feLog("  read U8 0x%x (%d)\n",
					*(U8*)(ptr+offset),
					*(U8*)(ptr+offset));
		}
#endif

		if( nread < 0)
		{
			feLogGroup("netsignal","%u %s\n", FE_ERRNO,
						errorString(FE_ERRNO).c_str());
			return -1;
		}
		else if(nread == 0)
		{
			break;
		}
		else
		{
			nleft -= nread;
			ptr += nread;

#if FE_SCK_VERBOSE
			feLog("Socket::udpRead part %d/%d nleft %d\n",
					nread,newCount,nleft);
#endif
		}
	}

	if(nleft<0)
	{
		feLog("Socket::udpRead unfinished read %d/%d nleft %d\n",
				nread,newCount,nleft);
	}

	if(!(readMode&e_readPeek))
	{
		memcpy((void*)pBuffer,pNewBuffer+offset,count);
		delete[] pNewBuffer;
	}

	return count - nleft;
}

IWORD Socket::write(const void *pBuffer, IWORD count)
{
	if(!m_open)
	{
		feX(e_writeFailed,
			"fe::Socket::write",
			"attempt to write to closed socket");
	}

	const U32 offset=sizeof(U32);
	const U32 newCount=count+offset;
	char* pNewBuffer=new char[newCount];
	*(U32*)pNewBuffer=htonl(count);
	memcpy(pNewBuffer+offset,pBuffer,count);

	if(m_transportType == Socket::e_udp)
	{
		const IWORD bytes_written=udpWrite(pNewBuffer, newCount);

		delete[] pNewBuffer;

		return bytes_written-offset;
	}

#if FE_SCK_VERBOSE
	feLog("Socket::write TCP %p count %d to socket %d\n",
			pBuffer,count,m_sock_handle);
#endif

	IWORD nleft;
	IWORD nwritten;
	const char *ptr;
	ptr = (const char *)pNewBuffer;
	nleft = newCount;
	while(nleft > 0)
	{
#if FE_OS==FE_WIN32 || FE_OS==FE_WIN64 || FE_OS==FE_OSX
		if((nwritten = ::send(m_sock_handle, ptr, nleft,0)) <= 0)
#else
		if((nwritten = ::send(m_sock_handle, ptr, nleft,MSG_NOSIGNAL)) <= 0)
#endif
		{
			feLog("Socket: bytes written %d/%d\n", nwritten, nleft);
			switch(FE_ERRNO)
			{
				case FE_EINTR:
					nwritten = 0;
					break;
				default:
					feLogGroup("netsignal","%u %s\n", FE_ERRNO,
							errorString(FE_ERRNO).c_str());
					return -1;
			}
		}

//		feLog("Socket: bytes written %d\n", nwritten);

		nleft -= nwritten;
		ptr += nwritten;
	}

	delete[] pNewBuffer;

	return count;
}

IWORD Socket::udpWrite(const void *pBuffer, IWORD count)
{
#if FE_SCK_VERBOSE
	feLog("Socket::udpWrite %p count %d to socket %d \"%s\" %d\n",
			pBuffer,count,m_sock_handle,
			m_udpSockAddrOwn.hostname().c_str(),m_udpSockAddrOwn.port());
#endif

	struct sockaddr_in localAddress;
	socklen_t addressLength = sizeof(localAddress);
	getsockname(m_sock_handle,(struct sockaddr*)&localAddress,&addressLength);

#if FE_SCK_VERBOSE
	feLog("Socket::udpWrite local address \"%s\" %d\n",
			inet_ntoa(localAddress.sin_addr),
			(int) ntohs(localAddress.sin_port));

	const U32 offset=sizeof(U32);
	const char* insideBuffer=(char*)pBuffer+offset;
	if(count==4+offset)
	{
		feLog("  write U32 0x%x (%d)\n",
				*(U32*)insideBuffer,ntohl(*(U32*)insideBuffer));
	}
	else if(count==1+offset)
	{
		feLog("  write U8 0x%x (%d)\n",*(U8*)insideBuffer,*(U8*)insideBuffer);
	}
#endif

	const IWORD rx = ::sendto(m_sock_handle, (char*)pBuffer, count, 0,
			m_udpSockAddrOwn.ptr(), m_udpSockAddrOwn.size());

#if FE_SCK_VERBOSE
	feLog("Socket::udpWrite wrote %d/%d\n",rx,count);
#endif

	return rx;
}

void Socket::startup(void)
{
#if FE_OS==FE_WIN32 || FE_OS==FE_WIN64
	if(Socket::m_wsaStarted)
	{
		return;
	}

	WORD wVersionRequested = MAKEWORD(2,2);
	WSADATA wsaData;
	int err = WSAStartup(wVersionRequested, &wsaData);

	if(0 != err)
	{
		feX(e_system,
				"fe::Socket::startup",
				errorString(FE_ERRNO).c_str());
	}

	if(2 != LOBYTE(wsaData.wVersion) ||
		2 != HIBYTE(wsaData.wVersion))
	{
		WSACleanup();
		feX(e_system,
				"fe::Socket::startup",
				errorString(FE_ERRNO).c_str());
	}
	Socket::m_wsaStarted = true;
#endif
}

void Socket::shutdown(void)
{
#if FE_OS==FE_WIN32 || FE_OS==FE_WIN64
	int err = WSACleanup();
	Socket::m_wsaStarted = false;
	if(err)
	{
		feX(e_system,
				"fe::Socket::shutdown",
				errorString(FE_ERRNO).c_str());
	}
#endif
}

} /* namespace ext */
} /* namespace fe */

