/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __platform_Socket_h__
#define __platform_Socket_h__
namespace fe
{
namespace ext
{

typedef int SockHandle;

/**	BSD socket address wrapper. */
class FE_DL_EXPORT SockAddr
{
	public:
		SockAddr(void)
		{
			memset(&m_sockaddr, 0, sizeof(m_sockaddr));
			m_sockaddr.sin_family = AF_INET;
			setAddress(INADDR_ANY);
		}

		/** Access pointer to underlying (struct sockaddr *) */
		struct sockaddr *	ptr(void)
		{
			return (struct sockaddr *)&m_sockaddr;
		}
		/** Return size of address. */
		int					size(void)
		{
			return sizeof(m_sockaddr);
		}
		/** Set the port.  @todo add a by-service-name port set. */
		void				setPort(FE_UWORD port)
		{
			m_sockaddr.sin_port = htons((short)port);
		}
		/** Set the address. */
		void				setAddress(const U32 address)
		{
			m_sockaddr.sin_addr.s_addr = htonl(address);
		}
		/** Set the address.  This may either be the hostname or the
			dot notation address. */
		void				setAddress(const String &host);
		/** Return the hostname for the address. */
		String				hostname(void);
		FE_UWORD			port(void)
		{
			return (long)ntohs(m_sockaddr.sin_port);
		}
		/** Return the address. */
		static	U32			getIp4Address(const String &host);

	protected:
		struct sockaddr_in	m_sockaddr;
};

/**	A socket wrapper.  The premise of this wrapper is to be simple and
	to the point.  Flexibility and extensibility are not explicitly considered.
	The intent is that all network communication will go through class and does
	not care about TCP/IP details.

	A concept of thread poison is supported by this class due to the blocking
	calls (accept, read, connect).  If Poison is associated with the Socket,
	then blocking calls will be aborted if the Poison becomes active before
	the calls can return normally.  If this happens then a Poisoned exception
	is thrown.

	A concept of network identifier is also supported by this class to provide
	a notion of site uniqueness in a networked application.  It is implemented
	in this lower level abstraction to assure simplicity in maintaining
	uniqueness.  A socket's unique id is 0 until it does a connect() in which
	case it will be given a unique id from the other side of the connection
	(presumably a 'server').
	*/
class FE_DL_EXPORT Socket : public Counted, ObjectSafe<Socket>
{
	public:	

			enum Transport
			{
				e_tcp = 0,
				e_udp = 1
			};

			//* bit field
			enum ReadMode
			{
				e_readNormal = 0,
				e_readPeek = 1,
				e_readNoWait = 2
			};

			/** Constructor. */
						Socket(void);
			/** Construct a socket with the given poison. */
						Socket(Poison *pPoison);
virtual					~Socket(void);
			/** Wraps bsd socket(AF_INET, SOCK_STREAM, 0) call. */
virtual		void		openStream(Transport transport=e_tcp,
								BWORD server=FALSE);
			/** Wraps bsd socket() call. */
virtual		SockHandle	socket(int domain, int type, int protocol);

			/** Wraps bsd bind() call. */
virtual		void		bind(SockAddr &sockaddr);
			/** Wraps bsd listen() call. */
virtual		void		listen(int backlog);
			/** Wraps bsd accept() call. */
virtual		sp<Socket>	accept(SockAddr &sockaddr);
			/** Wraps bsd connect() call. */
virtual		void		connect(SockAddr &sockaddr);
			/** Wraps bsd recv() call. */
virtual		IWORD		read(const void *pBuffer, IWORD count,
								ReadMode readMode=e_readNormal);
			/** Wraps bsd send() call. */
virtual		IWORD		write(const void *pBuffer, IWORD count);

			/** Set a Poison object. */
virtual		void		setPoison(Poison *pPoison);
			/** Wait for the socket to be readable (via select), checking at
				a regular interval for Poison to be active.  Mainly for
				internal use by read(), accept(), and connect() */
virtual		void		wait(void);

			/** Close the underlying socket. */
virtual		void		close(void);

			/** Return true if the socket is open, otherwise return false. */
virtual		bool		isOpen(void);

			/** Return a network identifier unique among Sockets connected
				to the same accept()ing 'server' Socket.  Note that this has
				nothing to do with a socket descriptor. */
virtual		U32			id(void);

			/**	Startup socket networking.  Only required on Win32, but should
				always be done for portability. */
static		void		startup(void);
			/**	Shutdown socket networking. */
static		void		shutdown(void);
			SockHandle	sockHandle() const {return m_sock_handle;}

			// WORKAROUND to UDP read socket. This may go away soon.
			Transport	transportType() const {return m_transportType;}
			SockAddr	sockAddrOwn()const {return m_udpSockAddrOwn;}
			//------------------------------------------------------------
	private:
			IWORD		udpRead(const void *pBuffer, IWORD count,
								ReadMode readMode);
			IWORD		udpWrite(const void *pBuffer, IWORD count);

		SockHandle		m_sock_handle;
		bool			m_open;
		bool			m_original;
		Poison			*m_pPoison;
		U32				m_id;
		U32				m_nextID;
		Transport		m_transportType;
		BWORD			m_server;
		SockAddr		m_udpSockAddrOwn;
#if FE_OS==FE_WIN32 || FE_OS==FE_WIN64
		static	bool	m_wsaStarted;
#endif
};

} /* namespace ext */
} /* namespace fe */

#endif /* __platform_Socket_h__ */

