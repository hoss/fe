/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <netsignal/netsignal.pmh>

namespace fe
{
namespace ext
{

Listener::Listener(void)
{
	m_operating = false;
}

Listener::~Listener(void)
{
}

void Listener::start(sp<SocketSpool> spSocketSpool, sp<Socket> spSocket,
	String ioPriority)
{
	m_listenerTask.m_spSocketSpool = spSocketSpool;
	m_listenerTask.m_spSocket = spSocket;
	spSocket->setPoison(&m_poison);
	m_pListenerThread = new Thread(&m_listenerTask);
	m_pListenerThread->config("priority",ioPriority);
	m_operating = true;
}

void Listener::stop(void)
{
	if(m_operating)
	{
		m_poison.start();
		m_pListenerThread->join();
		delete m_pListenerThread;
		m_operating = false;
		m_poison.stop();
	}
}

ListenerTask::ListenerTask(void)
{
}

void ListenerTask::operate(void)
{
//	feLog("ListenerTask::operate\n");

	try
	{
		while(true)
		{
			sp<Socket> spNewSocket;
			SockAddr client_addr;
			spNewSocket = m_spSocket->accept(client_addr);
			m_spSocketSpool->put(spNewSocket.abandon());

			//* HACK only one client for UDP?
			if(m_spSocket->transportType()==Socket::e_udp)
			{
				return;
			}
		}
	}
	catch(Poisoned &p)
	{
		feLogGroup("netsignal","ListenerTask poisoned\n");
	}
	catch(Exception &e)
	{
		e.log();
		feLogGroup("netsignal",
				"todo: decide how to handle this thread failure\n");
		exit(1);
	}
}


} /* namespace ext */
} /* namespace fe */

