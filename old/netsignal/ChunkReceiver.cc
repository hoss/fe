/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <netsignal/netsignal.pmh>

#define FE_CHR_DEBUG		FALSE

namespace fe
{
namespace ext
{

ChunkReceiver::ChunkReceiver(void)
{
	m_operating = false;
	m_live = false;
}

ChunkReceiver::~ChunkReceiver(void)
{
#if FE_CHR_DEBUG
	fprintf(stderr, "pre stop\n");
#endif
	stop();
#if FE_CHR_DEBUG
	fprintf(stderr, "post stop\n");
#endif
}

void ChunkReceiver::start(sp<ChunkSpool> spChunkSpool, sp<Socket> spSocket,
	String ioPriority)
{
#if FE_CHR_DEBUG
	feLog("ChunkReceiver::start\n");
#endif

	m_chunkReceiverTask.m_spChunkSpool = spChunkSpool;
	m_chunkReceiverTask.m_spSocket = spSocket;
	spSocket->setPoison(&m_poison);
	m_chunkReceiverTask.m_pChunkReceiver = this;
	m_live = true;
	m_pChunkReceiverThread = new Thread(&m_chunkReceiverTask);
	m_pChunkReceiverThread->config("priority",ioPriority);
	m_operating = true;
}

void ChunkReceiver::stop(void)
{
	if(m_operating)
	{
		m_poison.start();

#if FE_CHR_DEBUG
		fprintf(stderr, "pre join\n");
#endif

		m_pChunkReceiverThread->join();

#if FE_CHR_DEBUG
		fprintf(stderr, "post join\n");
#endif
		delete m_pChunkReceiverThread;
		m_operating = false;
		m_poison.stop();
	}
}

bool ChunkReceiver::live(void)
{
	Mutex::Guard guard(m_liveMutex);
	return m_live;
}

void ChunkReceiver::setLive(bool setting)
{
	Mutex::Guard guard(m_liveMutex);
	m_live = setting;
}

void ChunkReceiver::setPolling(sp<SignalerI> a_spSignaler,
	sp<Layout> a_spPollLayout)
{
	m_spSignaler=a_spSignaler;
	m_poll=(m_spSignaler.isValid() && a_spPollLayout.isValid())?
			a_spPollLayout->scope()->createRecord(a_spPollLayout):
			Record();
}

void ChunkReceiver::poll(void)
{
	if(m_spSignaler.isValid())
	{
#if FE_CHR_DEBUG
		feLog("ChunkReceiver::poll\n");
#endif
		m_spSignaler->signal(m_poll);
	}
}

ChunkReceiverTask::ChunkReceiverTask(void)
{
	m_pChunkReceiver = NULL;
	m_spChunkPool.resize(4);
	m_poolIndex=0;
}

void ChunkReceiverTask::operate(void)
{
	try
	{
		m_pChunkReceiver->setLive(true);
		while(true)
		{
#if FE_CHR_DEBUG
			feLogGroup("netsignal","ChunkReceiverTask waiting to read chunk\n");
#endif
			IWORD n;
			U32 chunkLength;
			const Socket::ReadMode readMode=Socket::e_readPeek;
			n = m_spSocket->read((void *)&chunkLength, sizeof(U32), readMode);
			if(!n)
			{
				feX(e_cancel, "ChunkReceiverTask socket disconnected");
			}
			if(n== -1)
			{
				feLog("ChunkReceiverTask::operate socket error:\n    %s\n",
						errorString(FE_ERRNO).c_str());

				feX(e_cancel, "ChunkReceiverTask socket error");
			}
			if(n != sizeof(U32))
			{
				feX(e_readFailed, "ChunkReceiverTask",
						"connection failed in size read %d vs %u",
						n,sizeof(U32));
			}
			chunkLength = ntohl(chunkLength);

			sp<Chunk> spChunk;

			spChunk = new Chunk(chunkLength);

#if FE_CHR_DEBUG
			feLogGroup("netsignal",
					"ChunkReceiverTask chunk length to read %u %u\n",
					chunkLength, spChunk->size());
#endif
			n = m_spSocket->read(spChunk->buffer(), spChunk->size());
#if FE_CHR_DEBUG
			feLogGroup("netsignal","ChunkReceiverTask size read %u\n", n);
#endif
			if(n != (IWORD)chunkLength)
			{
				feLogGroup("netsignal",
						"ChunkReceiverTask read %u/%u size %u\n",
						n,chunkLength, spChunk->size());
				feX(e_readFailed, "ChunkReceiverTask",
					"connection failed in chunk read");
			}
			m_spChunkSpool->put(spChunk.abandon());
			spChunk = NULL;
			m_pChunkReceiver->poll();
		}
	}
	catch(Poisoned &p)
	{
		m_pChunkReceiver->setLive(false);

#if FE_CHR_DEBUG
		feLogGroup("netsignal","ChunkReceiverTask poisoned\n");
#endif
	}
	catch(Exception &e)
	{
		m_pChunkReceiver->setLive(false);
		if(e.getResult() == e_cancel)
		{
			feLogGroup("netsignal",
					"ChunkReceiverTask thread terminating due to"
					" closed socket:\n");
			feLogGroup("netsignal","  %s\n", e.getMessage().c_str());
		}
		else if(e.getResult() == e_readFailed)
		{
			feLogGroup("netsignal",
					"ChunkReceiverTask thread terminating due to"
					" read failure:\n");
			feLogGroup("netsignal","  %s\n", e.getMessage().c_str());
		}
		else
		{
			feLogGroup("netsignal",
					"ChunkReceiverTask thread terminating due to"
					" unknown failure\n");
			throw e;
		}
	}
}

} /* namespace ext */
} /* namespace fe */

