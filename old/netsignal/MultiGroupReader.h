/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __netsignal_MultiGroupReader_h__
#define __netsignal_MultiGroupReader_h__

namespace fe
{
namespace ext
{

/**
	@ingroup netsignal

*/
class FE_DL_EXPORT MultiGroupReader:
		virtual public HandlerI,
		virtual public Config,
		public Initialize<MultiGroupReader>
{
	public:
				MultiGroupReader(void);
virtual			~MultiGroupReader(void);
		void	initialize(void);

				//* as HandlerI
virtual void	handle(Record &a_signal);

	private:
		std::map< int, sp<RecordGroup> >			m_loaded;
		bool										m_initialized;
		std::map< int, sp<RecordGroup> >::iterator	m_i_m_loaded;
};


} /* namespace ext */
} /* namespace fe */

#endif /* __netsignal_MultiGroupReader_h__ */


