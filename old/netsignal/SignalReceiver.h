/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __netsignal_SignalReceiver_h__
#define __netsignal_SignalReceiver_h__

#include <sstream>
namespace fe
{
namespace ext
{

/**	Receives signals over a given Socket.

	This class is intended to be used in conjuction with SignalSender.

	There are two types of messages,
	layout descriptions and record data.  If a layout description
	is read, internal setup for receiving record of that layout is done.
	If record data is read, and the layout description has been received
	then the record is created and used as a signal to the associated
	signaler.

	@copydoc SignalReceiver_info
	*/
class SignalReceiver :
	virtual public HandlerI
{
	public:
				SignalReceiver(sp<Scope> spScope, sp<ChunkSpool> spChunkSpool);
virtual			~SignalReceiver(void);

virtual	void	handle(Record &signal);
virtual	void	handleBind(sp<SignalerI> spSignalerI, sp<Layout> spLayout);

	private:
		sp<SignalerI>		m_spSignalerI;
		sp<Scope>			m_spScope;
		sp<ChunkSpool>		m_spChunkSpool;
		sp<data::StreamI>	m_spStream;
		sp<RecordGroup>		m_spRG;

};

} /* namespace ext */
} /* namespace fe */

#endif /* __netsignal_SignalReceiver_h__ */

