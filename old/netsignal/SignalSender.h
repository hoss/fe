/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __netsignal_SignalSender_h__
#define __netsignal_SignalSender_h__
namespace fe
{
namespace ext
{

/**	Sends signals over a given Socket.  This class is a signal handler
	and simply sends record data over the associated Socket when it handles
	a signal.

	This class is intended to be used in conjuction with SignalReceiver.

	@copydoc SignalSender_info
	*/
class SignalSender :
	virtual public HandlerI,
	public Initialize<SignalSender>
{
	public:
				SignalSender(int sid, sp<Scope> spScope,
					sp<ChunkSpool> spChunkSpool);
virtual			~SignalSender(void);
		void	initialize(void);

virtual	void	handle(Record &signal);

		void	setMaxSpoolSize(unsigned int a_size) {m_maxspoolsize=a_size;}

	private:
		sp<RecordGroup>		m_spRG;
		sp<data::StreamI>	m_spStream;
		sp<Scope>			m_spScope;
		sp<ChunkSpool>		m_spChunkSpool;
		FE_UWORD			m_sid;
		Accessor<int>		m_aSID;
		unsigned int		m_maxspoolsize;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __netsignal_SignalSender_h__ */

