/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __netsignal_Client_h__
#define __netsignal_Client_h__

namespace fe
{
namespace ext
{

class FE_DL_EXPORT ClientI:
	virtual public Component,
	public CastableAs<ClientI>
{
	public:
virtual	void	setScope(sp<Scope> spScope)									= 0;
virtual	void	addLayout(sp<Layout> spLayout)								= 0;
virtual	void	start(sp<SignalerI> spSignalerI,
					String a_host,
					unsigned short a_port,
					sp<Layout> spPollLayout,
					Socket::Transport transport=Socket::e_tcp,
					String ioPriority="normal")								= 0;
virtual	void	stop(void)													= 0;
virtual	BWORD	receiving(void)												= 0;
};

class BSDClient : public ClientI
{
	public:
		BSDClient(void);
virtual	~BSDClient(void);

virtual	void	setScope(sp<Scope> spScope);
virtual	void	addLayout(sp<Layout> spLayout);
virtual	void	start(sp<SignalerI> spSignalerI,
						String a_host,
						unsigned short a_port,
						sp<Layout> spPollLayout,
						Socket::Transport transport=Socket::e_tcp,
					String ioPriority="normal");
virtual	void	stop(void);
virtual	bool	receiving(void)
				{	return (m_spChunkReceiver.isValid() &&
							m_spChunkReceiver->live()); }

	private:
		sp<SignalerI>				m_spSignalerI;
		sp<Scope>					m_spScope;
		sp<HandlerI>				m_spSignalSender;
		sp<HandlerI>				m_spSignalReceiver;
		SockAddr					m_serverAddr;
		std::list<sp<Layout> >		m_layouts;
		bool						m_operating;
		sp<Layout>					m_spPollLayout;
		sp<ChunkReceiver>			m_spChunkReceiver;
		sp<ChunkSender>				m_spChunkSender;


};

} /* namespace ext */
} /* namespace fe */

#endif /* __netsignal_Client_h__ */

