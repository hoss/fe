/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __network_MultiGroupWriter_h__
#define __network_MultiGroupWriter_h__

#include <network/network.h>
namespace fe
{
namespace ext
{

class MultiGroupWriter;

struct MultiGroupWriterTask
{
	MultiGroupWriterTask(void);
	void operator()();
	sp<ChunkSpool>		m_spChunkSpool;
	MultiGroupWriter	*m_pMultiGroupWriter;
	std::string			m_prefix;
	unsigned int		m_count;
	unsigned int		m_loop;
};

/**
	@ingroup network

*/
class FE_DL_EXPORT MultiGroupWriter
	:	virtual public HandlerI,
		virtual public Config,
		public Initialize<MultiGroupWriter>
{
	public:
				MultiGroupWriter(void);
virtual			~MultiGroupWriter(void);
		void	initialize(void);

				//* as HandlerI
virtual void	handle(Record &a_signal);

	private:

		unsigned int					m_count;
		bool							m_initialized;
		std::string						m_prefix;
		unsigned int					m_loop;
		unsigned int					m_rate;

		sp<Scope>						m_spScope;
		boost::thread					*m_pThread;
		sp<ChunkSpool>					m_spChunkSpool;
		MultiGroupWriterTask			m_chunkSenderTask;
};


} /* namespace ext */
} /* namespace fe */

#endif /* __network_MultiGroupWriter_h__ */

