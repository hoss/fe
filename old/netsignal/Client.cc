/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <netsignal/netsignal.pmh>

namespace fe
{
namespace ext
{

BSDClient::BSDClient(void): m_operating(false)
{
}

BSDClient::~BSDClient(void)
{
	stop();
}

void BSDClient::setScope(sp<Scope> spScope)
{
	m_spScope = spScope;
}

void BSDClient::addLayout(sp<Layout> spLayout)
{
	if(m_spScope.isValid())
	{
		if(spLayout->scope() != m_spScope)
		{
			feX(e_unsupported,
				"fe::BSDClient::addLayout",
				"Client can only handle layouts from a single Scope");
		}
	}
	else
	{
		m_spScope = spLayout->scope();
	}
	m_layouts.push_back(spLayout);
}

void BSDClient::start(sp<SignalerI> spSignalerI, String a_host,
	unsigned short a_port, sp<Layout> spPollLayout,
	Socket::Transport transport,String ioPriority)
{
	fe::ext::SockAddr server_addr;
	server_addr.setAddress(a_host);
	server_addr.setPort(a_port);
	if(m_operating)
	{
		feX(e_unsupported,
			"fe::BSDClient::start",
			"cannot start already started Client");
	}
	if(!m_spScope.isValid())
	{
		feX(e_notInitialized,
			"fe::BSDClient::start",
			"attempt to start client with no scope set");
	}
	m_operating = true;
	m_spSignalerI = spSignalerI;
	m_serverAddr = server_addr;
	m_spPollLayout = spPollLayout;

//	feLog("BSDClient::start create socket\n");
	sp<Socket> spConnection;
	spConnection = new fe::ext::Socket;
	spConnection->openStream(transport);
	spConnection->connect(server_addr);

	U8 code;
	code = SocketHandler::e_create_signal_sender;
//	feLog("BSDClient::start send signal\n");
	spConnection->write((void *)&code, 1);

//	feLogGroup("netsignal","sent Server::e_sender\n");

	sp<ChunkSpool> spRecvChunkSpool(new ChunkSpool());

	m_spSignalReceiver = new SignalReceiver(m_spScope, spRecvChunkSpool);
	m_spSignalerI->insert(m_spSignalReceiver, m_spPollLayout);

//	feLog("BSDClient::start create ChunkRX\n");
	m_spChunkReceiver = new ChunkReceiver();
	m_spChunkReceiver->start(spRecvChunkSpool, spConnection, ioPriority);
	m_spChunkReceiver->setPolling(m_spSignalerI, m_spPollLayout);

#if 1
	code = SocketHandler::e_create_signal_receiver;
//	feLog("BSDClient::start send signal\n");
	spConnection->write((void *)&code, 1);

//	feLogGroup("netsignal","sent Server::e_receiver\n");

	sp<ChunkSpool> spSendChunkSpool(new ChunkSpool());
	m_spSignalSender = new SignalSender(spConnection->id(),
			m_spScope, spSendChunkSpool);
	m_spChunkSender = new ChunkSender();
	for(std::list<sp<Layout> >::iterator it = m_layouts.begin();
		it != m_layouts.end(); it++)
	{
		m_spSignalerI->insert(m_spSignalSender, *it);
	}

	m_spChunkSender->start(spSendChunkSpool, spConnection, ioPriority);
#endif

	code = SocketHandler::e_end_op;
//	feLog("BSDClient::start send signal\n");
	spConnection->write((void *)&code, 1);
}

void BSDClient::stop(void)
{
	if(m_operating)
	{
//		feLogGroup("netsignal","BSDClient::stop\n");

		/* stop sender before receiver to flush that pipe */
		if(m_spChunkSender.isValid())
		{
			m_spChunkSender->stop();
		}

		if(m_spChunkSender.isValid())
		{
			m_spChunkSender->stop();
		}

		if(m_spSignalerI.isValid())
		{
			for(std::list<sp<Layout> >::iterator it = m_layouts.begin();
				it != m_layouts.end(); it++)
			{
				m_spSignalerI->remove(m_spSignalSender, *it);
			}
			m_spSignalerI->remove(m_spSignalReceiver, m_spPollLayout);
		}

		m_operating = false;
	}
}


} /* namespace ext */
} /* namespace fe */

