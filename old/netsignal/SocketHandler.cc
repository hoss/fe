/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <netsignal/netsignal.pmh>

#define FE_SKH_DEBUG		FALSE

namespace fe
{
namespace ext
{

SocketHandler::SocketHandler(SockAddr server_addr,
	Socket::Transport transport,String ioPriority):
	m_serverAddr(server_addr),
	m_sockTransportType(transport),
	m_ioPriority(ioPriority)
{
	m_spSocketSpool = new SocketSpool();
	m_spListener = new Listener();
}

SocketHandler::~SocketHandler(void)
{
	stop();
}

void SocketHandler::handle(Record &signal)
{
	sp<Socket> spSocket = m_spSocketSpool->poll();

#if	FE_SKH_DEBUG
	feLogGroup("netsignal","SocketHandler::handle valid %d\n",
			spSocket.isValid());
#endif

	while(spSocket.isValid())
	{
		std::list<U8> ops;
		U8 socket_op;
		//feLog("SocketHandler::handle read signal\n");
		spSocket->read((void *)(&socket_op),1);

#if	FE_SKH_DEBUG
		feLogGroup("netsignal","SocketHandler::handle socket op %d\n",
				socket_op);
#endif

		while(socket_op != e_end_op)
		{
			ops.push_back(socket_op);
			spSocket->read((void *)(&socket_op),1);

#if	FE_SKH_DEBUG
			feLogGroup("netsignal","SocketHandler::handle  socket op %d\n",
					socket_op);
#endif
		}

		for(std::list<U8>::iterator it = ops.begin(); it != ops.end(); it++)
		{
			switch(*it)
			{
				case e_create_signal_sender:
					createSignalSender(spSocket);
					break;
				case e_create_signal_receiver:
					createSignalReceiver(spSocket);
					break;
				default:
					feX(e_impossible,
						"SocketHandler::handle",
						"corrupt socket op");
			}
		}

		spSocket = m_spSocketSpool->poll();
	}
	cleanDeadConnections();
}

void SocketHandler::assertScope(sp<Scope> spScope)
{
	if(m_spScope.isValid())
	{
		if(spScope != m_spScope)
		{
			feX(e_unsupported,
				"fe::SocketHandler::assertScope",
				"SocketHandler can only handle layouts from a single Scope");
		}
	}
	else
	{
		m_spScope = spScope;
	}
}


void SocketHandler::handleBind(sp<SignalerI> spSignaler, sp<Layout> spLayout)
{
#if	FE_SKH_DEBUG
	feLog("SocketHandler::handleBind\n");
#endif

	if(m_spLayout.isValid())
	{
		feX(e_unsupported,
			"SocketHandler::handleBind",
			"cannot bind more than once");
	}
	m_spSignaler = spSignaler;
	m_spLayout = spLayout;
	assertScope(spLayout->scope());

	sp<Socket> listenSocket(new Socket());

	const BWORD server=TRUE;
	listenSocket->openStream(m_sockTransportType,server);

	if(m_sockTransportType == Socket::e_udp)
	{
		listenSocket->bind(m_serverAddr);
		listenSocket->connect(m_serverAddr);
	}

	if(m_sockTransportType == Socket::e_tcp)
	{
		//printf("[SocketHandler] starting listener\n");
		listenSocket->bind(m_serverAddr);
		listenSocket->listen(5);
	}
	m_spListener->start(m_spSocketSpool, listenSocket, m_ioPriority);
}

void SocketHandler::addSenderLayout(sp<Layout> spLayout)
{
	assertScope(spLayout->scope());
	m_layouts.push_back(spLayout);
}

void SocketHandler::createSignalSender(sp<Socket> spSocket)
{
#if	FE_SKH_DEBUG
	feLog("SocketHandler::createSignalSender\n");
#endif

	sp<ChunkSpool> spChunkSpool(new ChunkSpool);

	SenderUndoStruct sender;
	sp<SignalSender> spSignalSender(new SignalSender(spSocket->id(),
		m_spScope, spChunkSpool));
	sender.m_spSignalSender = spSignalSender;
	spSignalSender->setMaxSpoolSize(1);
	sender.m_spChunkSender = new ChunkSender();

	for(std::list<sp<Layout> >::iterator it = m_layouts.begin();
		it != m_layouts.end(); it++)
	{
		sender.m_layouts.push_back(*it);
		m_spSignaler->insert(sender.m_spSignalSender, *it);
	}

	m_senders.push_back(sender);
	sender.m_spChunkSender->start(spChunkSpool, spSocket, m_ioPriority);
}

void SocketHandler::createSignalReceiver(sp<Socket> spSocket)
{
	sp<ChunkSpool> spChunkSpool(new ChunkSpool);

	ReceiverUndoStruct receiver;
	receiver.m_spSignalReceiver = new SignalReceiver(m_spScope, spChunkSpool);
	receiver.m_spChunkReceiver = new ChunkReceiver();

	//printf("[SocketHandler] createSignalReceiver_in\n");
	m_spSignaler->insert(receiver.m_spSignalReceiver, m_spLayout);
	//printf("[SocketHandler] createSignalReceiver_out\n");

	m_receivers.push_back(receiver);
	receiver.m_spChunkReceiver->start(spChunkSpool, spSocket, m_ioPriority);
}

void SocketHandler::stop(void)
{
	m_spListener->stop();

	for(std::list<SenderUndoStruct>::iterator it = m_senders.begin();
		it != m_senders.end(); it++)
	{
		it->m_spChunkSender->stop();
		for(std::list<sp<Layout> >::iterator jt = it->m_layouts.begin();
			jt != it->m_layouts.end(); jt++)
		{
			m_spSignaler->remove(it->m_spSignalSender, *jt);
		}
	}
	m_senders.clear();

	for(std::list<ReceiverUndoStruct>::iterator it = m_receivers.begin();
		it != m_receivers.end(); it++)
	{
		it->m_spChunkReceiver->stop();
		m_spSignaler->remove(it->m_spSignalReceiver, m_spLayout);
	}
	m_receivers.clear();
}

void SocketHandler::cleanDeadConnections(void)
{
	typedef std::list<SenderUndoStruct> t_s_undo;
	std::list<t_s_undo::iterator> remove_senders;
	for(t_s_undo::iterator it = m_senders.begin();
		it != m_senders.end(); it++)
	{
		if(!it->m_spChunkSender->live())
		{
			remove_senders.push_back(it);
			it->m_spChunkSender->stop();
			for(std::list<sp<Layout> >::iterator jt = it->m_layouts.begin();
				jt != it->m_layouts.end(); jt++)
			{
				m_spSignaler->remove(it->m_spSignalSender, *jt);
			}
		}
	}
	for(std::list<t_s_undo::iterator>::iterator it = remove_senders.begin();
		it != remove_senders.end(); it++)
	{
		m_senders.erase(*it);
	}

	typedef std::list<ReceiverUndoStruct> t_r_undo;
	std::list<t_r_undo::iterator> remove_receivers;
	for(t_r_undo::iterator it = m_receivers.begin();
		it != m_receivers.end(); it++)
	{
		if(!it->m_spChunkReceiver->live())
		{
			remove_receivers.push_back(it);
			it->m_spChunkReceiver->stop();
			m_spSignaler->remove(it->m_spSignalReceiver, m_spLayout);
		}
	}
	for(std::list<t_r_undo::iterator>::iterator it = remove_receivers.begin();
		it != remove_receivers.end(); it++)
	{
		m_receivers.erase(*it);
	}
}

} /* namespace ext */
} /* namespace fe */

