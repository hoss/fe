/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __netsignal_Listener_h__
#define __netsignal_Listener_h__
namespace fe
{
namespace ext
{

typedef Spool<Socket> SocketSpool;

class ListenerTask: public Thread::Functor
{
	public:
						ListenerTask(void);
virtual	void			operate(void);

		sp<SocketSpool>	m_spSocketSpool;
		sp<Socket>		m_spSocket;
};

class Listener : public Component
{
	public:
				Listener(void);
virtual			~Listener(void);

virtual	void	start(sp<SocketSpool> spSocketSpool, sp<Socket> spSocket,
					String ioPriority);
virtual	void	stop(void);

	private:
		Thread*			m_pListenerThread;
		ListenerTask	m_listenerTask;
		bool			m_operating;
		Poison			m_poison;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __netsignal_Listener_h__ */

