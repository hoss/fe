/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "netsignal/netsignal.h"

#define SERV_PORT	7876

const char *txt = "hello";

int main(void)
{
	fe::UnitTest unitTest;

	try
	{
		feLogGroup("netsignal","test echo client\n");

		fe::sp<fe::ext::Socket>		connection(new fe::ext::Socket);
		fe::ext::SockAddr			server_addr;

		connection->openStream();
		server_addr.setPort(SERV_PORT);
		server_addr.setAddress("127.0.0.1");

		connection->connect(server_addr);

		long size = htonl(strlen(txt)+1);
		connection->write((void *)&size, 4);
		connection->write((void *)txt,strlen(txt)+1);

		connection->close();

	}
	catch(fe::Exception &e)
	{
		e.log();
	}

	return unitTest.failures();
}
