/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "netsignal/netsignal.h"

#define LISTENQ		5
#define SERV_PORT	7876

#define SENDER



void Receive();
void Send();
fe::sp<fe::ext::ChunkSpool> spBuffer;
fe::sp<fe::ext::ChunkSender> spSender;
fe::sp<fe::ext::ChunkReceiver> spReceiver;

int main(void)
{
	fe::UnitTest unitTest;

	try
	{
        fe::ext::Socket::startup();
		fe::sp<fe::ext::Socket>		_socket(new fe::ext::Socket);
		fe::ext::SockAddr			server_addr;

		_socket->openStream(fe::ext::Socket::e_udp);
		server_addr.setPort(SERV_PORT);
		server_addr.setAddress("127.0.0.1");  
		_socket->connect(server_addr);
		spBuffer = new fe::ext::ChunkSpool();

#ifdef SENDER
		spSender = new fe::ext::ChunkSender();
		spSender->start(spBuffer, _socket);
		Send();
		spSender->stop();
#else
		spReceiver = new fe::ext::ChunkReceiver();
		spReceiver->start(spBuffer, _socket);
		Receive();
		spReceiver->stop();
#endif
	}
	catch(fe::Exception &e)
	{
		e.log();
	}

	return unitTest.failures();
}

//-----------------------------------------------------------------
// Receive()
//-----------------------------------------------------------------
void Receive()
{
		while(true)
		{
			fe::sp<fe::ext::Chunk> spChunk;
			spChunk = spBuffer->wait();
			feLogGroup("netsignal","received chunk of size %u\n", spChunk->size());
		}
}


//-----------------------------------------------------------------
// Send()
//-----------------------------------------------------------------
void Send()
{
	for(int i = 0; i < 1000; i++)
	{
		fe::milliSleep(10);
		fe::sp<fe::ext::Chunk> spChunk;
		spChunk = new fe::ext::Chunk(40);
		feLogGroup("netsignal","%d %d sz %d\n", spBuffer.isValid(), spChunk.isValid(), spChunk->size());
		spBuffer->put(spChunk.abandon());
	}
}
