/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "netsignal/netsignal.h"

#define LISTENQ		5
#define SERV_PORT	7876

int main(void)
{
	fe::UnitTest unitTest;

	try
	{
		feLogGroup("netsignal","test echo server\n");

		fe::ext::Socket::startup();
        fe::sp<fe::ext::Socket>		_socket(new fe::ext::Socket);
		fe::ext::SockAddr			server_addr;

        _socket->openStream(fe::ext::Socket::e_udp);		// Goes first.
        server_addr.setPort(SERV_PORT);
		_socket->connect(server_addr);
        _socket->bind(server_addr);

		while(true)
		{
			long size=0;
			_socket->read((void *)&size, 4);
			size = ntohl(size);

			char *buffer = new char[size];
			_socket->read((void *)buffer, size);
			feLogGroup("netsignal","[%s]\n", buffer);
			delete [] buffer;
		}
	}
	catch(fe::Exception &e)
	{
		e.log();
	}

	return unitTest.failures();
}
