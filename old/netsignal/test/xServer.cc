/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <signal/signal.pmh>	// direct access to ChainSignaler

#include <netsignal/netsignal.pmh>

class AHandler:
	virtual public fe::ext::HandlerI
{
	public:
		AHandler(void){}
virtual	~AHandler(void){}
virtual	void	handle(fe::Record &signal)
		{
			feLogGroup("netsignal","a: %d\n", a(signal));
			feLogGroup("netsignal","ttl: %d\n", ttl(signal));
		}
virtual	void	handleBind(fe::sp<fe::ext::SignalerI> spSignalerI,
						fe::sp<fe::Layout> spLayout)
		{
			a.setup(spLayout->scope(), "a");
			ttl.setup(spLayout->scope(), FE_USE(":TTL"));
		}
	private:
		fe::Accessor<int>	a;
		fe::Accessor<int>	ttl;
};

int main(int argc, char *argv[])
{
	fe::UnitTest unitTest;

	U32 complete=FALSE;

	try
	{
		if(argc<3)
			feX("Not enough arguments");

		fe::ext::Socket::startup();
		fe::sp<fe::Master> spMaster(new fe::Master);
		fe::sp<fe::Registry> spRegistry=spMaster->registry();

		fe::Result result=spRegistry->manage("fexNetSignalDL");
		if(fe::failure(result))
		{
			feX(argv[0], "could not manage fexNetSignalDL %p", result);
		}

		fe::sp<fe::Scope> spScope(new fe::Scope(*spMaster.raw()));
		fe::sp<fe::ext::SignalerI> spSignaler(new fe::ext::ChainSignaler());
		spSignaler->setName("SignalerI");

		fe::Accessor<int>	a(spScope,"a");
		fe::Accessor<int>	ttl(spScope, FE_USE(":TTL"));
		fe::sp<fe::Layout>	spSignal = spScope->declare("SIGNAL");
		spSignal->populate(a);
		spSignal->populate(ttl);

		fe::sp<fe::Layout>	spHB = spScope->declare("HEARTBEAT");
		fe::Record hb = spScope->createRecord(spHB);

		fe::sp<fe::ext::HandlerI> spHandlerI(new AHandler);
		spHandlerI->setName("HandlerI");
		spSignaler->insert(spHandlerI, spSignal);

		fe::sp<fe::ext::ServerI>	spServer(spRegistry->create("ServerI"));
		if(!spServer.isValid())
		{
			feX(argv[0], "could not create Server component");
		}
		spServer->addLayout(spSignal);

		const fe::ext::Socket::Transport transport=
				(fe::String(argv[1])=="udp")?
				fe::ext::Socket::e_udp: fe::ext::Socket::e_tcp;

		spServer->start(spSignaler, (short)atol(argv[1]), spHB, transport);
		for(int i = 0; i < 60; i++)
		{
			feLog("signal %d\n",i);
			spSignaler->signal(hb);
			fe::milliSleep(100);
		}

		spServer->stop();

		complete=TRUE;
//		spScope->shutdown();
	}
	catch(fe::Exception &e)
	{
		e.log();
	}

	unitTest(complete,"incomplete");
	return unitTest.failures();
	return 0;
}

