/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <signal/signal.pmh>	// direct access to ChainSignaler

#include <netsignal/netsignal.pmh>

#define LISTENQ		5
#define SERV_PORT	7876

int main(void)
{
	fe::UnitTest unitTest;

	try
	{
		fe::sp<fe::ext::Socket>		listener(new fe::ext::Socket);
		fe::sp<fe::ext::Socket>		connection(new fe::ext::Socket);
		fe::ext::SockAddr			server_addr;
		fe::ext::SockAddr			client_addr;

		listener->openStream();
		server_addr.setPort(SERV_PORT);
		listener->bind(server_addr);
		listener->listen(LISTENQ);

		fe::sp<fe::Master> spMaster(new fe::Master);
		fe::sp<fe::Scope> spScope(new fe::Scope(*spMaster.raw()));

		fe::sp<fe::ext::SignalerI> spSignalerI(new fe::ext::ChainSignaler());

		fe::Accessor<int>	a(spScope,"a");
		fe::sp<fe::Layout>	spSignal = spScope->declare("SIGNAL");
		spSignal->populate(a);

		connection = listener->accept(client_addr);

		fe::sp<fe::ext::ChunkSpool> spChunkSpool(new fe::ext::ChunkSpool);

		fe::sp<fe::ext::ChunkSender> spChunkSender(new fe::ext::ChunkSender);

		fe::sp<fe::ext::HandlerI> spHandlerI(new fe::ext::SignalSender(
			connection->id(), spScope, spChunkSpool));
		spSignalerI->insert(spHandlerI, spSignal);

		fe::Record signal = spScope->createRecord(spSignal);
		a(signal) = 42;
		spSignalerI->signal(signal);

		spChunkSender->start(spChunkSpool, connection);

		while(true)
		{
			fe::milliSleep(500);
			spSignalerI->signal(signal);
		}

		spChunkSender->stop();

	}
	catch(fe::Exception &e)
	{
		e.log();
	}
	catch(...)
	{
		feLogGroup("netsignal","uncaught exception");
	}

	return unitTest.failures();
}
