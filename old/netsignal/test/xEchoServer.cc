/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "netsignal/netsignal.h"

#define LISTENQ		5
#define SERV_PORT	7876

int main(void)
{
	fe::UnitTest unitTest;

	try
	{
		feLogGroup("netsignal","test echo server\n");

		fe::sp<fe::ext::Socket>		listener(new fe::ext::Socket);
		fe::sp<fe::ext::Socket>		connection(new fe::ext::Socket);
		fe::ext::SockAddr			server_addr;
		fe::ext::SockAddr			client_addr;

		listener->openStream();
		server_addr.setPort(SERV_PORT);
		listener->bind(server_addr);
		listener->listen(LISTENQ);

		while(true)
		{
			feLogGroup("netsignal","waiting...\n");
			connection = listener->accept(client_addr);

			feLogGroup("netsignal","connected: %s %d\n",
				client_addr.hostname().c_str(),
				client_addr.port());

			long size=0;
			connection->read((void *)&size, 4);
			size = ntohl(size);
			char *buffer = new char[size];
			connection->read((void *)buffer, size);
			feLogGroup("netsignal","[%s]\n", buffer);
			delete [] buffer;
		}
	}
	catch(fe::Exception &e)
	{
		e.log();
	}

	return unitTest.failures();
}


