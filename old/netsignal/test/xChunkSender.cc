/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "netsignal/netsignal.h"

#define LISTENQ		5
#define SERV_PORT	7876

#define CLIENT_ROLE


void ClientRole();
void ServerRole();

int main(void)
{
	fe::UnitTest unitTest;

#ifdef CLIENT_ROLE
	ClientRole();
#else
	ServerRole();
#endif
	return unitTest.failures();
}

//-----------------------------------------------------------------
// ServerRole()
//-----------------------------------------------------------------
void ServerRole()
{
	try
	{
		fe::sp<fe::ext::Socket>		listener(new fe::ext::Socket);
		fe::sp<fe::ext::Socket>		connection(new fe::ext::Socket);
		fe::ext::SockAddr			server_addr;
		fe::ext::SockAddr			client_addr;

		listener->openStream();
		server_addr.setPort(SERV_PORT);
		listener->bind(server_addr);
		listener->listen(LISTENQ);

		connection = listener->accept(client_addr);

		fe::sp<fe::ext::ChunkSpool> spBuffer(new fe::ext::ChunkSpool);
		fe::sp<fe::ext::ChunkSender> spSender(new fe::ext::ChunkSender);

		spSender->start(spBuffer, connection);

		for(int i = 1; i < 1000000; i += 1000)
		{
			fe::milliSleep(10);
			fe::sp<fe::ext::Chunk> spChunk;
			spChunk = new fe::ext::Chunk(i);
			feLogGroup("netsignal","%d %d sz %d\n", spBuffer.isValid(), spChunk.isValid(), spChunk->size());
			spBuffer->put(spChunk.abandon());
		}
		spSender->stop();
	}
	catch(fe::Exception &e)
	{
		e.log();
	}
}


//-----------------------------------------------------------------
// ClientRole()
//-----------------------------------------------------------------
void ClientRole()
{
	try
	{
		fe::sp<fe::ext::Socket>		connection(new fe::ext::Socket);
		fe::ext::SockAddr			server_addr;

		server_addr.setPort(SERV_PORT);
		server_addr.setAddress("127.0.0.1");

		connection->openStream();
		connection->connect(server_addr);

		fe::sp<fe::ext::ChunkSpool> spBuffer(new fe::ext::ChunkSpool);
		fe::sp<fe::ext::ChunkSender> spSender(new fe::ext::ChunkSender);

		spSender->start(spBuffer, connection);

		for(int i = 1; i < 1000000; i += 1000)
		{
			fe::milliSleep(10);
			fe::sp<fe::ext::Chunk> spChunk;
			spChunk = new fe::ext::Chunk(i);
			feLogGroup("netsignal","%d %d sz %d\n", spBuffer.isValid(), spChunk.isValid(), spChunk->size());
			spBuffer->put(spChunk.abandon());
		}
		spSender->stop();
	}
	catch(fe::Exception &e)
	{
		e.log();
	}
}
