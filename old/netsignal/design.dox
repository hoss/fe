/* *

@page network_design Networking Design

@section network_purpose Purpose

The goal of this networking design is to provide signaling between
networked processes.  A signal sent to a networked Signaler should not only
be handled by locally connected Handlers, but by remote Handlers as well.

@image html network_overview.jpg

@image latex network_overview.eps "networking overview" width=10cm


@section network_client_server Client/Server

A client/server approach was taken, mainly in support of the
model/view/controller (MVC) styles of architecture that we envision using
for some multiplayer game designs. However, the client/server design really
only applies to connection setup, so alternative approaches are of course
possible based on how signalers are setup and used.

@subsection network_send_layouts Layout specification

Since it is likely that not all signals sent to a Signaler are intended for
network communication, only signals of certain types (Layouts) will be sent.
Which Layouts are sent are therefore specified during setup (see
Server::addLayout and Client::addLayout).

@subsection network_ttl cycles

If both a Client and a Server are setup to send signals of the same Layout
a cycle is formed.  In some cases it may be desirable to have a Client and
Server send the same Layout, but not have a cycle.  The support for breaking
cycles is by convention, which is support of the Attribute "_TTL" by signalers.
A signaler should check a signal for the Attribute "_TTL", and if it exists
check if it is positive.  If it is positive, decrement it and send the signal.
If it is not positive, do not send the signal.  ChainSignaler supports this.

@section network_data_formatting Data Formatting

The format for data transmission is the same as the file format, with the
signal itself being the first record (state block id 1).
All necessary data to complete the record is serialized and transmitted. So
a signal with a attribute of RecordGroup type will result in the whole record
group, complete with member records, being transmitted.  This is of course
recursive.
See @ref serial_format.

@section network_threading Threading

The networking support is implemented in a multithreaded way that does not
'require' full thread safety in the using code (Handlers, etc).  All application
code is run on the same thread.  Incoming signals from a remote connection
are read off the socket by a separate thread, but that thread does not
execute the Signaler and Handlers.  The 'main' application thread polls for
incoming data in response to a signal (the Handler SignalReceiver).

@subsection network_chunkspools ChunkSpools

ChunkSpools are the thread safe mechanism that is used for communication
between the 'main' thread and the networking specific threads.  They are simply
well protected data chunk (memory blocks) queues.

@section network_diagram Design Diagram

@image html network.jpg

@image latex network.eps "network design" width=10cm

@section network_higher_level Higher Level Networking

@subsection network_coherence Data Coherence

Any sort of data coherence model (shared memory, etc) is (for now) to be
implemented in the application.  At this point it seems that locking in any
kind of infrastructure support would be too complicated to be generally
useful.  In others words, it may be simpler overall to just use the provided
building blocks of networked signalers to implement a data coherence approach
appropriate for a given application.

@image html netstack.jpg

@image latex netstack.eps "network stack" width=10cm

*/
