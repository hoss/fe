/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <netsignal/netsignal.pmh>

namespace fe
{
namespace ext
{

SignalReceiver::SignalReceiver(sp<Scope> spScope, sp<ChunkSpool> spChunkSpool)
{
	m_spScope = spScope;
	m_spChunkSpool = spChunkSpool;
	m_spStream = new data::BinaryStream(m_spScope);
}

SignalReceiver::~SignalReceiver(void)
{
}

void SignalReceiver::handleBind(sp<SignalerI> spSignalerI, sp<Layout> spLayout)
{
	m_spSignalerI = spSignalerI;
}

void SignalReceiver::handle(Record &signal)
{
//	feLog("SignalReceiver::handle \"%s\"\n",signal.layout()->name().c_str());

	sp<Chunk> spChunk = m_spChunkSpool->poll();

	sp<RecordGroup>	spRG;

	while(spChunk.isValid())
	{
		std::istringstream ist(spChunk->string());

		spRG = m_spStream->input(ist);
		if(spRG.isNull())
		{
			feLog("SignalReceiver::handle NULL input from stream\n");
			continue;
		}

		for(RecordGroup::iterator it = spRG->begin(); it != spRG->end(); it++)
		{
			sp<RecordArray> spRA(*it);
			for(int i = 0; i < spRA->length(); i++)
			{
				Record record = spRA->getRecord(i);

//				feLog("SignalReceiver::handle signal \"%s\"\n",
//						record.layout()->name().c_str());

				m_spSignalerI->signal(record);
			}
		}

		spChunk = m_spChunkSpool->poll();
	}
}

} /* namespace ext */
} /* namespace fe */

