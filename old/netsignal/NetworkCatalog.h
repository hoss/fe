/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __netsignal_NetworkCatalog_h__
#define __netsignal_NetworkCatalog_h__

namespace fe
{
namespace ext
{

//* TODO double-buffered client reads
//* TODO wait for state change on a key

/**************************************************************************//**
    @brief ConnectedCatalog with networked signal mirroring

	@ingroup netsignal
*//***************************************************************************/
class FE_DL_EXPORT NetworkCatalog:
	public ConnectedCatalog,
	public Initialize<NetworkCatalog>
{
	public:
	class Handler:
		virtual public HandlerI
	{
		public:
					Handler(void)											{}
	virtual			~Handler(void)											{}

	virtual	void	handle(Record &a_signal);

			void	bind(sp<NetworkCatalog> a_spNetworkCatalog)
					{	m_hpNetworkCatalog=a_spNetworkCatalog; }

		private:
			hp<NetworkCatalog>		m_hpNetworkCatalog;
	};

				NetworkCatalog(void);
virtual			~NetworkCatalog(void);

		void	initialize(void);

	protected:

virtual	Result	start(void) override;
virtual	Result	flush(void) override;
virtual	Result	disconnect(void) override;

virtual	Result	connectAsServer(String a_address,U16 a_port) override;
virtual	Result	connectAsClient(String a_address,U16 a_port) override;

virtual	void	broadcastSelect(String a_name,String a_property,
						String a_message,
						I32 a_includeCount,const String* a_pIncludes,
						I32 a_excludeCount,const String* a_pExcludes,
						const U8* a_pRawBytes=NULL,I32 a_byteCount=0) override;

virtual	void	heartbeat(void);
virtual	void	service(void);

	private:

		sp<ServerI>				m_spServer;
		sp<ClientI>				m_spClient;

		sp<Scope>				m_spScope;
		sp<SignalerI>			m_spSignaler;
		sp<Handler>				m_spHandler;
		sp<Layout>				m_spLayoutHB;
		sp<Layout>				m_spLayoutSend;
		sp<Layout>				m_spLayoutReceive;

		Accessor<int>			m_ttl;
		Accessor<String>		m_source;
		Accessor<String>		m_command;
		Accessor<String>		m_key;
		Accessor<String>		m_property;
		Accessor<String>		m_type;
		Accessor<String>		m_text;
		Accessor< Array<U8>>	m_byteBlock;

		std::deque<Record>		m_signalQueue;

		Record					m_signalHB;

		Mutex					m_signalMutex;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __netsignal_NetworkCatalog_h__ */

