/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "bio/Mitosis.h"

namespace fe
{

Mitosis::Mitosis(void)
{
}

Mitosis::~Mitosis(void)
{
}

void Mitosis::initialize(void)
{
}


void Mitosis::handle(Record &r_sig)
{
	m_asParticle.bind(r_sig.layout()->scope());

	sp<RecordGroup> rg_input =
		cfg< sp<RecordGroup> >("input"); // input group

	sp<RecordGroup> spSpawn(new RecordGroup());

	for(RecordGroup::iterator i_rg = rg_input->begin();
		i_rg != rg_input->end(); i_rg++)
	{
		sp<RecordArray> spRA(*i_rg);
		hp<Scope> spScope = spRA->layout()->scope();
		ScopedPathAccessor<SpatialVector> aRange(spScope,
			cfg<String>("range"));
		ScopedPathAccessor<Real> aAccum(spScope,
			cfg<String>("accumulator"));
		ScopedPathAccessor<Real> aStore(spScope,
			cfg<String>("store"));
		sp<Layout> l_spawn = spRA->layout();
		if(	m_asParticle.location.check(spRA))
		{
			for(int i = 0; i < spRA->length(); i++)
			{
				Record r_i = spRA->getRecord(i);
				SpatialVector *pRange = aRange(r_i);
				Real *pAccum = aAccum(r_i);
				Real *pStore = aStore(r_i);
				if(!pRange || !pAccum || !pStore) { continue; }

				if(*pAccum > 1.0f)
				{
					*pAccum -= 0.5f;
					Record r_spawn = r_i.clone();
					SpatialVector v;
					v[0] = -1.0f+2.0f*((float)rand()/(float)RAND_MAX);
					v[1] = -1.0f+2.0f*((float)rand()/(float)RAND_MAX);
					v[2] = -1.0f+2.0f*((float)rand()/(float)RAND_MAX);
					normalize(v);
					v *= *pRange;
					m_asParticle.location(r_spawn) += v;
					*(aAccum(r_spawn)) = 0.5f;
					*(aStore(r_spawn)) = 0.0f;
					spSpawn->add(r_spawn);
				}
			}
		}
	}

	for(RecordGroup::iterator i_rg = spSpawn->begin();
		i_rg != spSpawn->end(); i_rg++)
	{
		sp<RecordArray> spRA(*i_rg);
		for(int i = 0; i < spRA->length(); i++)
		{
			rg_input->add(spRA->getRecord(i));
		}
	}
}


} /* namespace */

