/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __bio_Metabolism_h__
#define __bio_Metabolism_h__

#include "signal/signal.h"
#include "shape/shape.h"
#include "misc/misc.h"
#include "spatial/spatial.h"
#include "field/DensityField.h"

namespace fe
{

class Metabolism : public Initialize<Metabolism>,
	virtual public HandlerI,
	virtual public Config
{
	public:
				Metabolism(void);
virtual			~Metabolism(void);
		void	initialize(void);

		// AS HandlerI
virtual void	bind(	sp<SignalerI> spSignalerI,
						sp<Layout> l_sig);
virtual void	handle(	Record &r_sig);

	private:
		AsParticle					m_asParticle;

};

} /* namespace */

#endif /* __bio_Metabolism_h__ */


