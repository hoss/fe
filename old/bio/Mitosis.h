/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __bio_Mitosis_h__
#define __bio_Mitosis_h__

#include "signal/signal.h"
#include "shape/shape.h"
#include "misc/misc.h"

namespace fe
{

class Mitosis : public Initialize<Mitosis>,
	virtual public HandlerI,
	virtual public Config
{
	public:
				Mitosis(void);
virtual			~Mitosis(void);
		void	initialize(void);

		// AS HandlerI
virtual void	handle(	Record &r_sig);


	private:
		AsParticle					m_asParticle;
};

} /* namespace */

#endif /* __bio_Mitosis_h__ */

