/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "bio/Metabolism.h"

namespace fe
{

Metabolism::Metabolism(void)
{
}

Metabolism::~Metabolism(void)
{
}

void Metabolism::initialize(void)
{
}

void Metabolism::bind(sp<SignalerI> spSignalerI, sp<Layout> l_sig)
{
	m_asParticle.bind(l_sig->scope());
}

void Metabolism::handle(Record &r_sig)
{
	sp<RecordGroup> rg_input =
		cfg< sp<RecordGroup> >("input"); // input group

	sp<DensityFieldI> spField = cfg< sp<Component> >("field"); // target field
	if(!spField.isValid()) { return; }

	for(RecordGroup::iterator i_rg = rg_input->begin();
		i_rg != rg_input->end(); i_rg++)
	{
		sp<RecordArray> spRA(*i_rg);
		hp<Scope> spScope = spRA->layout()->scope();
		ScopedPathAccessor<Real> aAccum(spScope,
			cfg<String>("accumulator"));
		ScopedPathAccessor<Real> aStore(spScope,
			cfg<String>("store"));
		ScopedPathAccessor<Real> aRate(spScope,
			cfg<String>("rate"));
		sp<Layout> l_spawn = spRA->layout();
		if(m_asParticle.location.check(spRA))
		{
			for(int i = spRA->length()-1; i >= 0; i--)
			{
				Record r_i = spRA->getRecord(i);
				Real *pAccum = aAccum(r_i);
				Real *pStore = aStore(r_i);
				Real *pRate = aRate(r_i);
				if(!pAccum || !pStore || !pRate) { continue; }
				*pAccum -= *pRate;
				*pStore += *pRate;
				if(*pAccum < 0.0)
				{
					spField->inject(1.0 * (*pStore), m_asParticle.location(spRA, i));
					spRA->remove(i);
				}
			}
		}
	}
}



} /* namespace */

