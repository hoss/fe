/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __bio_LinearReaction_h__
#define __bio_LinearReaction_h__

#include "signal/signal.h"
#include "shape/shape.h"
#include "spatial/spatial.h"

namespace fe
{

class LinearReaction : public Initialize<LinearReaction>,
	virtual public HandlerI,
	virtual	public Dispatch,
	virtual public Config
{
	public:
				LinearReaction(void);
virtual			~LinearReaction(void);

		void	initialize(void);

		// AS HandlerI
virtual void	handle(	Record &r_sig);

		// AS DispatchI
virtual	bool	call(const String &a_name, std::vector<Instance> a_argv);

virtual	void	addReactant(const String &a_name,Real a_requisite,Real a_rate);

	private:
		class Reactant
		{
			public:
				String				m_attrname;
				Real				m_requisite;
				Real				m_rate;
		};

		std::vector<Reactant>		m_reactant;

		AsSignal					m_asSignal;
};

} /* namespace */

#endif /* __bio_LinearReaction_h__ */

