/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "bio/LinearReaction.h"

namespace fe
{

LinearReaction::LinearReaction(void)
{
}

LinearReaction::~LinearReaction(void)
{
}

void LinearReaction::initialize(void)
{
	dispatch<String>("addReactant");
	dispatch<Real>("addReactant");
	dispatch<Real>("addReactant");
}

bool LinearReaction::call(const String &a_name, std::vector<Instance> a_argv)
{
	if(a_name == FE_DISPATCH("addReactant", "name requisite rate"))
	{
		addReactant(a_argv[0].cast<String>(), a_argv[1].cast<Real>(),
			a_argv[2].cast<Real>());
	}

	return true;
}

void LinearReaction::addReactant(const String &a_name,Real a_requisite,Real a_rate)
{
	int i = m_reactant.size();
	m_reactant.resize(i+1);
	m_reactant[i].m_attrname = a_name;
	m_reactant[i].m_requisite = a_requisite;
	m_reactant[i].m_rate = a_rate;
}

void LinearReaction::handle(Record &r_sig)
{
	sp<RecordGroup> rg_input =
		cfg< sp<RecordGroup> >("input"); // input group


	for(RecordGroup::iterator i_rg = rg_input->begin();
		i_rg != rg_input->end(); i_rg++)
	{
		sp<RecordArray> spRA(*i_rg);
		sp<Scope> spScope = spRA->layout()->scope();

		unsigned int reactant_cnt = m_reactant.size();
		std::vector<Accessor<Real> > aReactant;
		aReactant.resize(reactant_cnt);
		for(unsigned int i = 0; i < reactant_cnt; i++)
		{
			aReactant[i].setup(spScope, m_reactant[i].m_attrname);
		}

		bool ok = true;
		for(unsigned int i = 0; i < reactant_cnt; i++)
		{
			if(!aReactant[i].check(spRA)) { ok = false; }
		}

		if(ok)
		{
			for(int i = 0; i < spRA->length(); i++)
			{
				// determine limiting condition
				Real multiple = 0.0;
				for(unsigned int j = 0; j < reactant_cnt; j++)
				{
					if(m_reactant[j].m_requisite > 0.0)
					{
						Real tmp_multiple =
							aReactant[j](spRA, i)/m_reactant[j].m_requisite;
						if(tmp_multiple > multiple) { multiple = tmp_multiple; }
					}
				}

				if(multiple > 0.0)
				{
					// apply accumulations
					for(unsigned int j = 0; j < reactant_cnt; j++)
					{
						if(m_reactant[j].m_rate != 0.0)
						{
							aReactant[j](spRA, i) +=
								m_reactant[j].m_rate * multiple;
						}
					}
				}
			}
		}
	}

}


} /* namespace */

