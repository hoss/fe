/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "RouteController.h"

namespace fe
{

RouteController::RouteController(void)
{
	m_gc = new RecordGroup();
}

RouteController::~RouteController(void)
{
}

void RouteController::initialize(void)
{
	dispatch<Record>("prototype");

	WindowEvent::Item button = (WindowEvent::Item)(
		(unsigned int)WindowEvent::e_itemLeft |
		(unsigned int)WindowEvent::e_itemMiddle |
		(unsigned int)WindowEvent::e_itemRight);

	// default event mapping
	maskDefault("fe_route_start",
		WindowEvent::Mask(	WindowEvent::e_sourceKeyboard,
							(WindowEvent::Item)105 /* i */,
							WindowEvent::e_statePress));
	maskDefault("fe_route_end",
		WindowEvent::Mask(	WindowEvent::e_sourceKeyboard,
							(WindowEvent::Item)111 /* o */,
							WindowEvent::e_statePress));
	maskDefault("fe_route_delete",
		WindowEvent::Mask(	WindowEvent::e_sourceKeyboard,
							(WindowEvent::Item)120 /* x */,
							WindowEvent::e_statePress));

	sp<RecordGroup> rg_invalid;
	cfg< sp<RecordGroup> >("output") = rg_invalid;
	cfg< sp<RecordGroup> >("selected") = rg_invalid;
}

void RouteController::handle(Record &r_event)
{
	sp<Scope> spScope = r_event.layout()->scope();
	m_asSel.bind(spScope);
	m_asSignal.bind(spScope);

	Record r_windata = m_asSignal.winData(r_event, "RouteController");

	sp<RecordGroup> rg_input = m_asSel.selected(r_windata, "RouteController");

	sp<RecordGroup> rg_output = cfg< sp<RecordGroup> >("output");
	if(!rg_output.isValid()) { return; }

	sp<RecordGroup> rg_selected = cfg< sp<RecordGroup> >("selected");

	m_start = maskGet("fe_route_start");
	m_end = maskGet("fe_route_end");
	m_delete = maskGet("fe_route_delete");

	WindowEvent wev;
	wev.bind(r_event);

	// garbage collect dangling routes from output
	for(RecordGroup::iterator i_rg = rg_output->begin();
		i_rg != rg_output->end(); i_rg++)
	{
		sp<RecordArray> spRA(*i_rg);
		if(m_asRouter.check(spRA))
		{
			for(int i = 0; i < spRA->length(); i++)
			{
				if( !m_asRouter.start(spRA,i).isValid()	||
					!m_asRouter.end(spRA,i).isValid())
				{
					m_gc->add(spRA->getRecord(i));
				}
			}
		}
	}
	rg_output->remove(m_gc);
	m_gc->clear();


	if(wev.is(m_start))
	{
		m_rg_src.clear();
		for(RecordGroup::iterator i_rg = rg_input->begin();
			i_rg != rg_input->end(); i_rg++)
		{
			sp<RecordArray> spRA(*i_rg);
			m_asWaypoint.bind(spRA->layout()->scope());
			if(m_asWaypoint.check(spRA))
			{
				for(int i = 0; i < spRA->length(); i++)
				{
					m_rg_src.push_back(spRA->getRecord(i));
				}
			}
		}
	}

	if(wev.is(m_end))
	{
		for(RecordGroup::iterator i_rg = rg_input->begin();
			i_rg != rg_input->end(); i_rg++)
		{
			sp<RecordArray> spRA(*i_rg);
			m_asWaypoint.bind(spRA->layout()->scope());
			if(m_asWaypoint.check(spRA))
			{
				for(int i = 0; i < spRA->length(); i++)
				{
					m_rg_dst.push_back(spRA->getRecord(i));
				}
			}
		}

		for(unsigned int i = 0; i < m_rg_src.size(); i++)
		{
			for(unsigned int j = 0; j < m_rg_dst.size(); j++)
			{
				Record &r_src = m_rg_src[i];
				Record &r_dst = m_rg_dst[j];

				if(r_src == r_dst) { continue; }

				Record r_router = m_prototype.deepclone();

				m_asRouter.start(r_router) = r_src;
				m_asRouter.end(r_router) = r_dst;

				rg_output->add(r_router);
			}
		}
		m_rg_dst.clear();
	}

	if(wev.is(m_delete))
	{
		for(RecordGroup::iterator i_rg = rg_selected->begin();
			i_rg != rg_selected->end(); i_rg++)
		{
			sp<RecordArray> spRA(*i_rg);
			if(m_asRouter.check(spRA))
			{
				for(int i = 0; i < spRA->length(); i++)
				{
					m_gc->add(spRA->getRecord(i));
				}
			}
		}
		rg_output->remove(m_gc);
		m_gc->clear();
	}
}

void RouteController::prototype(Record &r_proto)
{
	m_asRouter.bind(r_proto.layout()->scope());
	if(!m_asRouter.check(r_proto))
	{
		feX("RouteController::prototype",
			"not a router");
	}
	m_prototype = r_proto;
}

bool RouteController::call(const String &a_name, std::vector<Instance> a_argv)
{
	if(a_name == FE_DISPATCH("prototype", "[record]"))
	{
		Record r_proto = a_argv[0].cast<Record>();
		prototype(r_proto);
	}

	return true;
}


} /* namespace */

