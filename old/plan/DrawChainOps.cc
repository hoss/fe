/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "DrawChainOps.h"

namespace fe
{

DrawChainOps::DrawChainOps(void)
{
	m_spWidgets = new RecordGroup();
}

DrawChainOps::~DrawChainOps(void)
{
}

void DrawChainOps::initialize(void)
{
}

void DrawChainOps::handle(Record &r_sig)
{
	if(!m_drawview.handle(r_sig)) { return; }

	Color white(1.0, 1.0, 1.0, 1.0);
	Color red(1.0, 0.0, 0.0, 1.0);
	Color green(0.0, 1.0, 0.0, 1.0);
	Color grey(0.5, 0.5, 0.5, 1.0);
	Color yellow(0.3, 0.3, 0.0, 0.1);
	SpatialVector zero(0.0,0.0,0.0);
	Color *pColor = &grey;

	sp<DrawMode> solid(new DrawMode());
	solid->setDrawStyle(DrawMode::e_solid);
	solid->setAntialias(true);
	solid->setLineWidth(0.1);

	sp<DrawMode> outline(new DrawMode());
	outline->setDrawStyle(DrawMode::e_outline);
	outline->setAntialias(true);
	outline->setLineWidth(0.1);

	sp<RecordGroup> rg_input = cfg< sp<RecordGroup> >("input");

	sp<RecordGroup> rg_selected = cfg< sp<RecordGroup> >("selected");

	SpatialTransform rotation;
	m_drawview.drawI()->view()->screenAlignment(rotation);
	SpatialVector planeNormal;
	getColumn(2,rotation,planeNormal);

	sp<ViewI> spView(m_drawview.drawI()->view());

	// clear selection entities
	rg_input->remove(m_spWidgets);
	m_spWidgets->clear();

	for(RecordGroup::iterator it = rg_input->begin();
		it != rg_input->end(); it++)
	{
		sp<RecordArray> spRA(*it);
		m_asChainOp.bind(spRA->layout()->scope());
		m_asPoint.bind(spRA->layout()->scope());
		m_asNav.bind(spRA->layout()->scope());
		m_asSelTri.bind(spRA->layout()->scope());
		m_asSelectable.bind(spRA->layout()->scope());
		m_asSelParent.bind(spRA->layout()->scope());
		m_asBounded.bind(spRA->layout()->scope());
		if(!m_arrowheadLayout.isValid())
		{
			m_arrowheadLayout =
				spRA->layout()->scope()->declare("l_DrawChainOps_arrowhead");
			m_asSelTri.populate(m_arrowheadLayout);
			m_asSelParent.populate(m_arrowheadLayout);
		}
		if(m_asChainOp.check(spRA))
		{
			bool isSel = false;
			if(m_asSelectable.is.check(spRA)) { isSel = true; }
			for(int i = 0; i < spRA->length(); i++)
			{
				Record r_item = spRA->getRecord(i);
				Record r_arrowhead;
				Record r_arrowtail;
				Record r_next = m_asChainOp.next(spRA,i);
				if(isSel)
				{
					if(rg_selected->find(r_item))
					{
						pColor = &red;
					}
					else
					{
						pColor = &green;
					}

					m_asNamed.bind(spRA->layout()->scope());
					if(r_next.isValid())
					{
						r_arrowhead = spRA->layout()->scope()->
							createRecord(m_arrowheadLayout);
						m_asSelParent.group(r_arrowhead) = new RecordGroup();
						m_asSelParent.group(r_arrowhead)->add(r_item);
					}
				}
				else
				{
					pColor = &grey;
				}
				SpatialVector line[2];
				line[0] = m_asChainOp.location(spRA,i);
				SpatialTransform transform;
				transform = rotation;
				Real sc = 1.5;
				if(m_asBounded.check(spRA))
				{
					sc = 1.4 * m_asBounded.radius(spRA, i);
				}
				SpatialVector scale(sc,sc,sc);
				setTranslation(transform, line[0]);
				m_drawview.drawI()->pushDrawMode(outline);
				m_drawview.drawI()->drawCircle(transform,&scale,*pColor);
				m_drawview.drawI()->popDrawMode();


				if(r_next.isValid())
				{
					if(m_asChainOp.check(r_next))
					{

						//m_asSelParent.group(r_arrowhead)->add(r_next);


						line[1] = m_asChainOp.location(r_next);


						SpatialVector sline[2];
						Real scl[2];
						spView->screenInfo(scl[0], sline[0], rotation, line[0]);
						spView->screenInfo(scl[1], sline[1], rotation, line[1]);

						sp<ViewMode> mode = spView->viewMode();
						sp<ViewMode> omode(new ViewMode());
						*omode = *mode;
						F32 zoom; Vector2f center;
						//mode->getOrtho(zoom, center);
						omode->setOrtho(1.0, Vector2f());
						spView->setViewMode(omode);
						spView->use(ViewMode::e_ortho);

						SpatialVector diff = sline[1] - sline[0];
						diff[2] = 0.0;
						if(diff == zero)
						{
							transform = Matrix3x4f::identity();
							setTranslation(transform, sline[0]);
							SpatialVector scale(20.0,10.0,1.0);
							m_drawview.drawI()->drawMode()->setDrawStyle(DrawMode::e_outline);
							m_drawview.drawI()->drawCircle(transform,&scale,red);
							//mode->setOrtho(zoom, center);
							spView->setViewMode(mode);
							spView->use(ViewMode::e_perspective);
							//spView->use(mode);
							continue;
						}
						rg_input->add(r_arrowhead);
						m_spWidgets->add(r_arrowhead);
						normalize(diff);
						sline[0] += diff * (sc/scl[0]);
						sline[1] -= diff * (sc/scl[1]);
						//sline[1] -= (Real)5.0 * diff;

						m_drawview.drawI()->pushDrawMode(solid);
						m_drawview.drawI()->drawLines(sline, NULL, 2,
								DrawI::e_strip, false, pColor);
						m_drawview.drawI()->popDrawMode();


						SpatialVector invdiff;
						invdiff[0] = -diff[1];
						invdiff[1] = diff[0];

						SpatialVector tri[4];
						SpatialVector tri_nrml[4];
						for(int z = 0; z < 4; z++)
						{
							tri_nrml[z][2] = 1.0;
						}
#if 1
#define LN 21.0
#define WD 15.0
#else
#define LN 50.0
#define WD 25.0
#endif
						tri[0] = sline[1];
						tri[1] = sline[1] - (Real)LN * diff + (Real)(WD/2.0) * invdiff;
						tri[2] = tri[1] - (Real)WD * invdiff;
						tri[3] = sline[1];
						if(r_arrowhead.isValid())
						{
							m_asSelTri.vertA(r_arrowhead) = tri[0];
							m_asSelTri.vertB(r_arrowhead) = tri[1];
							m_asSelTri.vertC(r_arrowhead) = tri[2];

							m_drawview.drawI()->pushDrawMode(outline);
							m_drawview.drawI()->drawLines(tri, NULL, 4,
								DrawI::e_strip, false, pColor);
							m_drawview.drawI()->popDrawMode();

							m_drawview.drawI()->pushDrawMode(solid);
							m_drawview.drawI()->drawTriangles(tri,
								tri_nrml,
								NULL, 3, DrawI::e_discrete, false,
								pColor);

							m_drawview.drawI()->popDrawMode();
						}

						tri[0] = sline[0] + (Real)LN * diff;
						tri[1] = sline[0] + (Real)(WD/2.0) * invdiff;
						tri[2] = tri[1] - (Real)WD * invdiff;
						tri[3] = sline[1];
						if(r_arrowtail.isValid())
						{
							m_asSelTri.vertA(r_arrowtail) = tri[0];
							m_asSelTri.vertB(r_arrowtail) = tri[1];
							m_asSelTri.vertC(r_arrowtail) = tri[2];
							m_drawview.drawI()->drawTriangles(tri,
								tri_nrml,
								NULL, 3, DrawI::e_discrete, false,
								&white);
						}


						//mode->setOrtho(zoom, center);
						spView->setViewMode(mode);
						spView->use(ViewMode::e_perspective);
						//spView->use(mode);

					}
				}
			}
		}
	}
}

} /* namespace */

