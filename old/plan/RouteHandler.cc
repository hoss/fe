/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "plan/RouteHandler.h"

namespace fe
{

RouteHandler::RouteHandler(void)
{
}

RouteHandler::~RouteHandler(void)
{
}

void RouteHandler::initialize(void)
{
}

void RouteHandler::handle(Record &r_sig)
{
	SystemTicker ticker("RouteHandler");
	ticker.log();
	sp<RecordGroup> rg_input =
		cfg< sp<RecordGroup> >("input"); // input group
	sp<RecordGroup> rg_collisions =
		cfg< sp<RecordGroup> >("collisions"); // collision group

	// non-intrusive to the objects, but kind of slow
	std::map< Record, std::vector<Record> > routeds;
	std::map< Record, WpInfo > wpinfos;

	ticker.log("pre");

	// process pairs
	for(RecordGroup::iterator it = rg_collisions->begin();
		it != rg_collisions->end(); it++)
	{
		sp<RecordArray> spRA(*it);
		sp<Scope> spScope = spRA->layout()->scope();
		m_asRouter.bind(spScope);
		m_asRouted.bind(spScope);
		m_asWaypoint.bind(spScope);
		m_asPair.bind(spScope);
		m_asBounded.bind(spScope);
		m_asMobile.bind(spScope);
		if(m_asPair.check(spRA))
		{
			for(int i = 0; i < spRA->length(); i++)
			{
				Record r_wp;
				Record r_routed;


				// check if one is a waypoint and the other is a routed
				if(m_asWaypoint.check(m_asPair.left(spRA, i)))
				{
					if(m_asRouted.check(m_asPair.right(spRA, i)))
					{
						r_wp = m_asPair.left(spRA, i);
						r_routed = m_asPair.right(spRA, i);
					}
				}
				else if(m_asWaypoint.check(m_asPair.right(spRA, i)))
				{
					if(m_asRouted.check(m_asPair.left(spRA, i)))
					{
						r_wp = m_asPair.right(spRA, i);
						r_routed = m_asPair.left(spRA, i);
					}
				}

				if(!r_routed.isValid() || !r_wp.isValid()) { continue; }


				// make sure the routed in within the waypoint
				if(r_wp.isValid())
				{
					if(!m_asBounded.check(r_wp)) { continue; }
				}
				SpatialVector delta = m_asWaypoint.location(r_wp) -
					m_asRouted.location(r_routed);
				Real dist = magnitude(delta);
				if(m_asBounded.radius(r_wp) < dist) { continue; }


				// find the start and end routers for the waypoint
				std::map<Record, WpInfo>::iterator i_wpinfo =
					wpinfos.find(r_wp);
				if(i_wpinfo == wpinfos.end())
				{

					WpInfo &wpinfo = wpinfos[r_wp];
					for(RecordGroup::iterator i_rtr = rg_input->begin();
						i_rtr != rg_input->end(); i_rtr++)
					{
						sp<RecordArray> ra_rtr(*i_rtr);
						if(m_asRouter.check(ra_rtr))
						{
							for(int j = 0; j < ra_rtr->length(); j++)
							{
								if(r_wp == m_asRouter.start(ra_rtr, j))
								{
									wpinfo.m_start_routers.
										push_back(ra_rtr->getRecord(j));
								}
								else if(r_wp == m_asRouter.end(ra_rtr, j))
								{
									wpinfo.m_end_routers.
										push_back(ra_rtr->getRecord(j));
								}
							}
						}
					}
				}

				WpInfo &wpi = wpinfos[r_wp];

				// build up routed map
				if(m_asRouted.state(r_routed) == e_hold)
				{
					if(wpi.m_start_routers.size() > 0)
					{
						routeds[r_routed].push_back(r_wp);
					}
				}
				else if(m_asRouted.state(r_routed) == e_linger)
				{
					if(wpi.m_start_routers.size() > 0)
					{
						routeds[r_routed].push_back(r_wp);
					}
				}
				else if(m_asRouted.state(r_routed) == e_move)
				{
					if(wpi.m_end_routers.size() > 0)
					{
						routeds[r_routed].push_back(r_wp);
					}
				}
			}
		}
	}
	ticker.log("pairs");

	// process routed objects
	for(std::map< Record, std::vector<Record> >::iterator i_routeds =
		routeds.begin(); i_routeds != routeds.end(); i_routeds++)
	{
		// find the smallest relevent waypoint
		Record r_routed = i_routeds->first;
		std::vector<Record> &wps = i_routeds->second;
		unsigned long sz = wps.size();
		Real smallest = 1.0e30;
		Record r_smallest;
		for(unsigned int i = 0; i < sz; i++)
		{
			if(m_asBounded.radius(wps[i]) < smallest)
			{
				smallest = m_asBounded.radius(wps[i]);
				r_smallest = wps[i];
			}
		}

		WpInfo &wpi = wpinfos[r_smallest];

		if(m_asRouted.state(r_routed) == e_hold)
		{
			Record r_end;
			if(wpi.m_start_routers.size() > 1)
			{
				int j = rand() % wpi.m_start_routers.size();
				r_end = m_asRouter.end(wpi.m_start_routers[j]);
			}
			else if(wpi.m_start_routers.size() == 1)
			{
				r_end = m_asRouter.end(wpi.m_start_routers[0]);
			}

			if(r_end.isValid())
			{
				m_asRouted.point(r_routed) = r_end;
				m_asRouted.state(r_routed) = e_move;
			}
		}
		else if(m_asRouted.state(r_routed) == e_move)
		{
			for(unsigned int i = 0; i < sz; i++)
			{
				if(wps[i] == m_asRouted.point(r_routed))
				{
					m_asRouted.state(r_routed) = e_linger;
				}
			}
		}
		else if(m_asRouted.state(r_routed) == e_linger)
		{
			if(smallest <= m_asBounded.radius(m_asRouted.point(r_routed)))
			{
				Record r_end;
				if(wpi.m_start_routers.size() > 1)
				{
					int j = rand() % wpi.m_start_routers.size();
					r_end = m_asRouter.end(wpi.m_start_routers[j]);
				}
				else if(wpi.m_start_routers.size() == 1)
				{
					r_end = m_asRouter.end(wpi.m_start_routers[0]);
				}

				if(r_end.isValid())
				{
					m_asRouted.point(r_routed) = r_end;
					m_asRouted.state(r_routed) = e_move;
				}
			}
		}
	}
	ticker.log("routed");

	// process all routed ojects for move and linger
	for(RecordGroup::iterator i_r = rg_input->begin();
		i_r != rg_input->end(); i_r++)
	{
		sp<RecordArray> spRA(*i_r);
		sp<Scope> spScope = spRA->layout()->scope();
		m_asMobile.bind(spScope);
		m_asRouted.bind(spScope);
		m_asWaypoint.bind(spScope);
		m_asPoint.bind(spScope);
		if(m_asMobile.check(spRA) && m_asRouted.check(spRA) && m_asPoint.check(spRA))
		{
			for(unsigned int i = 0; i < spRA->length(); i++)
			{
				if(m_asRouted.state(spRA,i) == e_move)
				{
					if(!m_asRouted.point(spRA,i).isValid())
					{
						m_asRouted.state(spRA,i) = e_hold;
						m_asMobile.destination(spRA, i) =
							m_asPoint.location(spRA, i);
						continue;
					}
					if(m_asWaypoint.check(m_asRouted.point(spRA,i)))
					{
						m_asMobile.destination(spRA, i) =
							m_asWaypoint.location(m_asRouted.point(spRA,i));
					}
				}
				if(m_asRouted.state(spRA,i) == e_linger)
				{
					Record &r_point = m_asRouted.point(spRA,i);

					if(!r_point.isValid())
					{
						m_asRouted.state(spRA,i) = e_hold;
						continue;
					}

					// random walk destination
					Real jitter = 0.1;
					Real v = m_asBounded.radius(r_point) * jitter;
					m_asMobile.destination(spRA, i)[0] += randomReal(-v,v);
					m_asMobile.destination(spRA, i)[1] += randomReal(-v,v);

					// bring destination within waypoint
					SpatialVector delta = m_asMobile.destination(spRA, i)
						- m_asWaypoint.location(r_point);
					Real diff = magnitude(delta);
					if(diff > m_asBounded.radius(r_point))
					{
						Real scale = m_asBounded.radius(r_point)/diff;
						delta *= scale;
						m_asMobile.destination(spRA, i) =
							m_asWaypoint.location(r_point) + delta;
					}
				}
			}
		}
	}
	ticker.log("adjust");
}


} /* namespace */

