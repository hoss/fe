/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __plan_ChainOpController_h__
#define __plan_ChainOpController_h__

#include "signal/signal.h"
#include "draw/draw.h"
#include "window/window.h"
#include "viewer/viewer.h"
#include "viewer/Mask.h"
#include "shape/shape.h"
#include "dataui/datauiAS.h"
#include "plan/planAS.h"

namespace fe
{

class FE_DL_EXPORT ChainOpController :
	virtual public Config,
	virtual public HandlerI,
	virtual public Dispatch,
	virtual public Mask,
	public Initialize<ChainOpController>
{
	public:
				ChainOpController(void);
virtual			~ChainOpController(void);

		void	initialize(void);

				// AS HandlerI
virtual void	handleBind(sp<SignalerI> spSignalerI, sp<Layout> spLayout);
virtual void	handle(Record &r_sig);

				// AS DispatchI
virtual	bool	call(const String &a_name, std::vector<Instance> a_argv);

		void	prototype(Record &r_proto);
	private:
		void	selChild(sp<RecordGroup> rg_added, Record r_op);
		bool				m_active;
		AsSelection			m_asSel;
		AsSignal			m_asSignal;
		//AsWaypoint			m_asWaypoint;
		AsChainOp			m_asChainOp;
		AsSelectable		m_asSelectable;
		AsPick				m_asPick;
		//std::vector<Record>	m_prevops;
		Record				m_prevop;
		Record				m_prototype;

		WindowEvent::Mask	m_start;
		WindowEvent::Mask	m_end;
		WindowEvent::Mask	m_add;
		WindowEvent::Mask	m_destroy;
		WindowEvent::Mask	m_childselect;


		sp<SignalerI>		m_spSignaler;
};

} /* namespace */


#endif /* __plan_ChainOpController_h__ */

