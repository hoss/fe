/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __plan_DrawChainOps_h__
#define __plan_DrawChainOps_h__

#include "signal/signal.h"
#include "draw/draw.h"
#include "window/window.h"
#include "viewer/DrawView.h"
#include "shape/shapeAS.h"
#include "ai/aiAS.h"
#include "plan/planAS.h"

namespace fe
{

class FE_DL_EXPORT DrawChainOps :
		public Initialize<DrawChainOps>,
		virtual public Config,
		virtual public HandlerI
{
	public:
				DrawChainOps(void);
virtual			~DrawChainOps(void);

		void	initialize(void);

virtual void	handle(	Record &r_sig);

	private:
		AsChainOp					m_asChainOp;
		AsPoint						m_asPoint;
		AsNav						m_asNav;
		AsSelScreenTriangle			m_asSelTri;
		AsSelParent					m_asSelParent;
		AsNamed						m_asNamed;
		DrawView					m_drawview;
		AsSelectable				m_asSelectable;
		AsBounded					m_asBounded;
		sp<Layout>					m_arrowheadLayout;
		sp<RecordGroup>				m_spWidgets;
};

} /* namespace */

#endif /* __plan_DrawChainOps_h__ */

