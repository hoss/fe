/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __plan_DrawRouter_h__
#define __plan_DrawRouter_h__

#include "signal/signal.h"
#include "draw/draw.h"
#include "window/window.h"
#include "viewer/DrawView.h"
#include "shape/shapeAS.h"
#include "ai/aiAS.h"
#include "plan/planAS.h"

namespace fe
{

class FE_DL_EXPORT DrawRouter :
	public Initialize<DrawRouter>,
	virtual public Config,
	virtual public HandlerI
{
	public:
				DrawRouter(void);
virtual			~DrawRouter(void);

		void	initialize(void);

virtual void	handle(	Record &r_sig, sp<SignalerI> spSignalerI);

	private:
		sp<RecordGroup>				m_spWidgets;
		AsSelScreenTriangle			m_asSelTri;
		AsSelParent					m_asSelParent;
		AsRouter					m_asRouter;
		AsPoint						m_asPoint;
		AsBounded					m_asBounded;
		String						m_arrowheadLayoutName;
};

} /* namespace */

#endif /* __plan_DrawRouter_h__ */

