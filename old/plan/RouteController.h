/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __plan_RouteController_h__
#define __plan_RouteController_h__

#include "signal/signal.h"
#include "draw/draw.h"
#include "window/window.h"
#include "viewer/viewer.h"
#include "viewer/Mask.h"
#include "shape/shape.h"
#include "dataui/datauiAS.h"
#include "plan/planAS.h"

namespace fe
{

class FE_DL_EXPORT RouteController :
	virtual public Config,
	virtual public HandlerI,
	virtual public Dispatch,
	virtual public Mask,
	virtual	public Reactor,
	public Initialize<RouteController>
{
	public:
				RouteController(void);
virtual			~RouteController(void);

		void	initialize(void);

				// AS HandlerI
virtual void	handle(Record &r_sig);

				// AS DispatchI
virtual	bool	call(const String &a_name, std::vector<Instance> a_argv);

		void	prototype(Record &r_proto);
	private:

		std::vector<Record>		m_rg_src;
		std::vector<Record>		m_rg_dst;
		WindowEvent::Mask		m_start;
		WindowEvent::Mask		m_end;
		WindowEvent::Mask		m_delete;
		Record					m_prototype;
		sp<RecordGroup>			m_gc;

		AsSelection				m_asSel;
		AsSignal				m_asSignal;
		AsRouter				m_asRouter;
		AsWaypoint				m_asWaypoint;
};

} /* namespace */

#endif /* __plan_RouteController_h__ */

