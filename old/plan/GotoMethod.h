/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __plan_GotoMethod_h__
#define __plan_GotoMethod_h__

#include "signal/signal.h"
#include "shape/shape.h"

#include "plan/MethodI.h"
#include "plan/planAS.h"
#include "mobility/mobilityAS.h"
#include "ai/aiAS.h"

namespace fe
{

class FE_DL_EXPORT GotoMethod
	: virtual public MethodI
{
	public:
					GotoMethod(void);
virtual				~GotoMethod(void);

					// AS PlopHandlerI
virtual	bool		method(Record &r_object, Record &r_op,
						sp<RecordGroup> rg_plan);

	private:
		AsGotoOp	m_asGotoOp;
		AsParticle	m_asParticle;
		AsWaypoint	m_asWaypoint;
		//AsNavable	m_asNavable;
		//AsNav		m_asNav;
		AsBounded	m_asBounded;
		AsMobile	m_asMobile;
};

} /* namepsace */

#endif /* __plan_GotoMethod_h__ */

