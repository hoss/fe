/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "ChainOpController.h"

namespace fe
{

ChainOpController::ChainOpController(void)
{
	m_active = false;
}

ChainOpController::~ChainOpController(void)
{
}

void ChainOpController::initialize(void)
{
	dispatch<Record>("prototype");

	// default event mapping
	maskDefault("fe_chainop_add",
		WindowEvent::Mask(	WindowEvent::e_sourceMouseButton,
							WindowEvent::e_itemLeft,
							WindowEvent::e_stateRelease));

	maskDefault("fe_chainop_end",
		WindowEvent::Mask(	WindowEvent::e_sourceKeyboard,
							(WindowEvent::Item)27 /* esc */,
							WindowEvent::e_statePress));

	maskDefault("fe_chainop_start",
		WindowEvent::Mask(	WindowEvent::e_sourceKeyboard,
							(WindowEvent::Item)119 /* 'w' */,
							WindowEvent::e_statePress));

	maskDefault("fe_chainop_childselect",
		WindowEvent::Mask(	WindowEvent::e_sourceKeyboard,
							(WindowEvent::Item)101 /* 'e' */,
							WindowEvent::e_statePress));

	maskDefault("fe_chainop_destroy",
		WindowEvent::Mask(	WindowEvent::e_sourceKeyboard,
							(WindowEvent::Item)120 /* 'x' */,
							WindowEvent::e_statePress));

	sp<RecordGroup> rg_invalid;
	cfg< sp<RecordGroup> >("output") = rg_invalid;
}

void ChainOpController::handleBind(sp<SignalerI> spSignalerI, sp<Layout> l_sig)
{
	m_spSignaler = spSignalerI;
	m_asSel.bind(l_sig->scope());
	m_asSignal.bind(l_sig->scope());
}

void ChainOpController::selChild(sp<RecordGroup> rg_added, Record r_op)
{
	Record r_next = m_asChainOp.next(r_op);
	if(!r_next.isValid()) { return; }

	if(m_asSelectable.is.check(r_next))
	{
		if(!rg_added->find(r_next))
		{
			rg_added->add(r_next);
			if(m_asChainOp.check(r_next))
			{
				selChild(rg_added, r_next);
			}
		}
	}
}

void ChainOpController::handle(Record &r_sig)
{
	Record r_event = r_sig;
	m_asSignal.bind(r_sig.layout()->scope());
	m_asPick.bind(r_sig.layout()->scope());
	Record r_windata = m_asSignal.winData(r_sig, "ChainOpController");

	sp<RecordGroup> rg_input = m_asSel.selected(r_windata, "ChainOpController");
	sp<RecordGroup> rg_output = cfg< sp<RecordGroup> >("output");

	if(!rg_output.isValid())
	{
		return;
	}

	bool do_end = false;

	m_start = maskGet("fe_chainop_start");
	m_end = maskGet("fe_chainop_end");
	m_add = maskGet("fe_chainop_add");
	m_destroy = maskGet("fe_chainop_destroy");
	m_childselect = maskGet("fe_chainop_childselect");

	WindowEvent wev;
	wev.bind(r_event);


	if(wev.is(m_end))
	{
		do_end = true;
	}
	if(wev.is(m_start))
	{
		m_active = true;
	}
	if((wev.is(m_add) || wev.is(m_start)) && m_active)
	{
		std::vector<Record> newops;
		Record r_newop;
		// check for existing chainop
		for(RecordGroup::iterator i_rg = rg_input->begin();
			i_rg != rg_input->end(); i_rg++)
		{
			sp<RecordArray> spRA(*i_rg);
			//m_asWaypoint.bind(spRA->layout()->scope());
			m_asChainOp.bind(spRA->layout()->scope());
			if(m_asChainOp.check(spRA))
			{
				if(spRA->length() > 0)
				{
					r_newop = spRA->getRecord(0);
					//m_asChainOp.next(r_newop) = Record();
					break;
				}
#if 0
				for(int i = 0; i < spRA->length(); i++)
				{
					Record r_op = m_prototype.deepclone();
					m_asChainOp.waypoint(r_op) = spRA->getRecord(i);
					newops.push_back(r_op);
					rg_output->add(r_op);
					m_asChainOp.next(r_op) = Record();
					for(int j = 0; j < m_prevops.size(); j++)
					{
						if(m_asChainOp.waypoint(m_prevops[j]) == m_asChainOp.waypoint(r_op))
						{
							feLog("*************** self ref\n");
						}
						m_asChainOp.next(m_prevops[j]) = r_op;
					}
				}
#endif
			}
		}


#if 1
		// if no existing chainop, create a new one
		if(!r_newop.isValid() && (wev.is(m_add)))
		{
			r_newop = m_prototype.deepclone();
			m_asChainOp.next(r_newop) = Record();
			m_asChainOp.location(r_newop) = m_asPick.focus(r_windata,
				"ChainOpController");
			rg_output->add(r_newop);
		}
#endif


		if(r_newop.isValid() && m_prevop.isValid())
		{
			if(r_newop == m_prevop)
			{
				m_asChainOp.next(r_newop) = Record();
			}
			else
			{
				m_asChainOp.next(m_prevop) = r_newop;
			}
		}


#if 0
		for(RecordGroup::iterator i_rg = rg_input->begin();
			i_rg != rg_input->end(); i_rg++)
		{
			sp<RecordArray> spRA(*i_rg);
			m_asWaypoint.bind(spRA->layout()->scope());
			m_asChainOp.bind(spRA->layout()->scope());
			if(m_asChainOp.check(spRA))
			{
				for(int i = 0; i < spRA->length(); i++)
				{
					newops.push_back(spRA->getRecord(i));
				}
			}
		}
		m_prevops = newops;
#endif

		m_prevop = r_newop;
	}
	if(wev.is(m_childselect))
	{
#if 0
		sp<RecordGroup> rg_added(new RecordGroup);
		for(RecordGroup::iterator i_rg = rg_input->begin();
			i_rg != rg_input->end(); i_rg++)
		{
			sp<RecordArray> spRA(*i_rg);
			m_asWaypoint.bind(spRA->layout()->scope());
			m_asChainOp.bind(spRA->layout()->scope());
			m_asSelectable.bind(spRA->layout()->scope());
			if(m_asChainOp.check(spRA))
			{
				for(int i = 0; i < spRA->length(); i++)
				{
					selChild(rg_added, spRA->getRecord(i));
				}
			}
		}
		rg_input->add(rg_added);
#endif
	}
	if(wev.is(m_destroy))
	{
		for(RecordGroup::iterator i_rg = rg_input->begin();
			i_rg != rg_input->end(); i_rg++)
		{
			sp<RecordArray> spRA(*i_rg);
			if(spRA->layout() == m_prototype.layout())
			{
				for(int i = spRA->length()-1; i >= 0; i--)
				{
					Record r_rm = spRA->getRecord(i);
					rg_output->remove(r_rm);
					rg_output->removeReferences(r_rm, true);
				}
				spRA->clear();
			}
		}
	}


	if(do_end)
	{
		m_active = false;
		//m_prevops.clear();
		m_prevop = Record();
	}
}

void ChainOpController::prototype(Record &r_proto)
{
	m_asChainOp.bind(r_proto.layout()->scope());
	if(!m_asChainOp.check(r_proto))
	{
		feX("ChainOpController::prototype",
			"not a chain op");
	}
	m_prototype = r_proto;
}


bool ChainOpController::call(const String &a_name, std::vector<Instance> a_argv)
{
	if(a_name == FE_DISPATCH("prototype", "[record]"))
	{
		Record r_proto = a_argv[0].cast<Record>();
		prototype(r_proto);
	}

	return true;
}


} /* namespace */

