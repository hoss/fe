/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */


#ifndef __plan_planAS_h__
#define __plan_planAS_h__

#include "ai/aiAS.h"

namespace fe
{

class AsWaypoint
	: public AsPoint, public Initialize<AsWaypoint>
{
	public:
		void initialize(void)
		{
			add(name,				FE_USE("plan:name"));
		}
		/// name
		Accessor<String>		name;
};

class AsChainOp :
	public AsPoint,
	public Initialize<AsChainOp>
{
	public:
		void initialize(void)
		{
			//add(waypoint,			FE_USE("plan:waypoint"));
			add(next,				FE_USE("plan:next"));
		}
#if 0
		// waypoint record
		Accessor<Record>			waypoint;
#endif
		// list implementation
		Accessor<Record>			next;
};

class AsGotoOp :
	public AsChainOp,
	public Initialize<AsGotoOp>
{
	public:
		void initialize(void)
		{
		}
};

class AsRouter :
	public AccessorSet,
	public Initialize<AsRouter>
{
	public:
		void initialize(void)
		{
			add(start,					FE_USE("plan:start"));
			add(end,					FE_USE("plan:end"));
			add(level,					FE_USE("plan:level"));
		}
		// start point
		Accessor<Record>			start;
		// end point
		Accessor<Record>			end;
		// level
		Accessor<int>				level;
};

class AsRouted :
	public AsPoint,
	public Initialize<AsRouted>
{
	public:
		void initialize(void)
		{
			add(point,					FE_USE("plan:routepoint"));
			add(state,					FE_USE("plan:state"));
		}
		// route point
		Accessor<Record>			point;
		// state
		Accessor<int>				state;
};


class AsPlan
	: public AccessorSet, public Initialize<AsPlan>
{
	public:
		void initialize(void)
		{
			add(group,				FE_USE("plan:group"));
		}
		/// operation sequence
		Accessor< sp<RecordGroup> >			group;
};


} /* namespace */


#endif /* __plan_planAS_h__ */

