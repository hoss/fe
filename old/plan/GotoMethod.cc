/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "plan/GotoMethod.h"

namespace fe
{

GotoMethod::GotoMethod(void)
{
}

GotoMethod::~GotoMethod(void)
{
}

bool GotoMethod::method(Record &r_object, Record &r_op, sp<RecordGroup> rg_plan)
{
	if(!m_asGotoOp.bind(r_op)) { return true; }
	if(!m_asParticle.bind(r_object)) { return true; }
	if(!m_asMobile.bind(r_object)) { return true; }
	//if(!m_asWaypoint.bind(m_asGotoOp.waypoint(r_op))) { return true; }
	//if(!m_asNavable.bind(r_object)) { return true; }
	if(!m_asBounded.bind(r_op)) { return true; }

	SpatialVector &waypoint = m_asGotoOp.location(r_op);
	SpatialVector &location = m_asParticle.location(r_object);
	SpatialVector &destination = m_asMobile.destination(r_object);

	destination = waypoint;

#if 0
	if(!m_asNav.bind(m_asNavable.nav(r_object))) { return true; }
	SpatialVector &target = m_asNav.location(m_asNavable.nav(r_object));
	m_asNavable.nav(r_object) = r_op;
#endif

	

	if(magnitude(waypoint - location) < m_asBounded.radius(r_op))
	{
		if(m_asGotoOp.next(r_op).isValid())
		{
			rg_plan->add(m_asGotoOp.next(r_op));
		}
		return true;
	}
	return false;
}

} /* namespace */

