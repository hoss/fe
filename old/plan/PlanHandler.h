/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __plan_PlanHandler_h__
#define __plan_PlanHandler_h__

#include "signal/signal.h"
#include "shape/shape.h"

#include "plan/MethodI.h"
#include "plan/planAS.h"
#include "ai/aiAS.h"

namespace fe
{

/**	Processes plans.

	The ops in the group for each AsPlan record is processed with the
	matching registered MethodI.

	Interprets a true return of MethodI::method() to mean the op
	is complete.  In this case the op is removed from the group.

	*/
class FE_DL_EXPORT PlanHandler :
	virtual public HandlerI,
	virtual public Dispatch,
	virtual public Config,
	virtual public Initialize<PlanHandler>
{
	public:
				PlanHandler(void);
virtual			~PlanHandler(void);
virtual	void	initialize(void);

				// AS HandlerI
virtual void	handle(Record &r_sig);

				// AS DispatchI
virtual	bool	call(const String &a_name, std::vector<Instance> a_argv);


virtual	void	method(sp<Layout> spLayout, sp<MethodI> spMethod);


	private:
		typedef std::map<sp<Layout>, sp<MethodI> >	t_map;

		AsPlan				m_asPlan;
		t_map				m_map;
};

} /* namespace */

#endif /* __plan_PlanHandler_h__ */



