# TEMP: for minimal python2 behavior (enough to complain and quit).
# NOTE: __future__ imports must occur before any other expression.
from __future__ import print_function

import os
import sys
import re
import glob
import shutil
import threading
import getpass
import collections

SUCCESS = 0
FAIL = 1

# -----------------------------------------------------------------------------
# Python Version checking and enforcement.

PYTHON_VERSION_MIN = collections.OrderedDict([
	('major', 3),
	('minor', 7),
	('micro', 3),
])

def PythonVersionSafeguard():

	verkeys = PYTHON_VERSION_MIN.keys()
	actualver = sys.version_info
	desiredver = '.'.join([str(v) for v in PYTHON_VERSION_MIN.values()])
	for actual, desired in [
		(getattr(actualver, k), PYTHON_VERSION_MIN[k]) for k in verkeys
	]:
		if actual > desired:
			break
		elif actual < desired:
			raise EnvironmentError(
				"Insufficient python version detected: {}. " \
					"Must be {} or newer.".format(sys.version, desiredver)
			)

# Conduct python version guard as early as possible.
try:
	PythonVersionSafeguard()
except Exception as e:
	print("[ERROR]", e, file=sys.stderr)
	sys.exit(FAIL)

# -----------------------------------------------------------------------------

coloramaColor = 0
wconioColor = 0

if sys.platform == "win32":
	try:
		import colorama
		colorama.init()
		coloramaColor = 1
	except ImportError:
		print("\ninstall the package colorama to get color output on Win32")

#	try:
#		import WConio
#		wconioColor = 1
#	except ImportError:
#		print("\ninstall the package WConio to get color output on Win32")

# color support
if sys.platform == "win32" and wconioColor == 1:
	RED		= WConio.RED
	GREEN	= WConio.GREEN
	YELLOW	= WConio.YELLOW
	BLUE	= WConio.BLUE
	MAGENTA	= WConio.MAGENTA
	CYAN	= WConio.CYAN
	WHITE	= WConio.WHITE
else:
	RED		= 31
	GREEN	= 32
	YELLOW	= 33
	BLUE	= 34
	MAGENTA	= 35
	CYAN	= 36
	WHITE	= 37
	NORMAL	= 0
	BOLD	= 1

def ansi(c):
	return "[%dm" % c

def color_on(bold, hue):
	if wconioColor and sys.platform == "win32":
		if wconioColor == 1:
			WConio.textcolor(hue)
			if bold == -1:
				# NOTE appears to be the same as normvideo()
				WConio.lowvideo()
			elif bold == 1:
				WConio.highvideo()
			else:
				WConio.normvideo()
	elif coloramaColor or sys.platform != "win32":
		if bold == -1:
			bold = 2
		sys.stdout.write( r"[%dm[%dm" % (bold,hue) )

def dim_on():
	if wconioColor and sys.platform == "win32":
		if wconioColor == 1:
			WConio.lowvideo()
	elif coloramaColor or sys.platform != "win32":
		sys.stdout.write( r"[%dm" % (2) )

def color_off():
	if wconioColor and sys.platform == "win32":
		if wconioColor == 1:
			WConio.normvideo()
	elif coloramaColor or sys.platform != "win32":
		sys.stdout.write( "[0m" )
		sys.stdout.flush()

print_lock = threading.Lock()
def cprint(color, bold, string):
	print_lock.acquire()
	color_on(bold,color)
	sys.stdout.write(string + '\n')
	color_off()
	print_lock.release()

def cprint_no_newline(color, bold, string):
	print_lock.acquire()
	color_on(bold,color)
	sys.stdout.write(string)
	color_off()
	print_lock.release()

def copy_files(fromPattern,toDir):
	filenames = glob.glob(fromPattern)
	for filename in filenames:
		shutil.copy(filename,toDir)

# return list of "subPath/match"
def find_all(rootPath,subPath,pattern):
	result = []
	sourceFiles = os.listdir(os.path.join(rootPath,subPath))
	for sourceFile in sourceFiles:
		if re.compile(pattern).match(sourceFile):
			result += [ os.path.join(subPath,sourceFile) ]
	return result

def copy_all(fromDir,toDir):
	filenames = os.listdir(fromDir)
	for filename in filenames:
		pathSource = os.path.join(fromDir, filename)
		pathDest = os.path.join(toDir, filename)
		if os.path.isdir(pathSource):
			if not os.path.exists(pathDest):
				os.mkdir(pathDest)
			copy_all(pathSource,pathDest)
		else:
			shutil.copyfile(pathSource, pathDest)
			shutil.copymode(pathSource, pathDest)

def findModsetFiles(rootPath, basename, modset_root_list):
	dot_file = re.compile("^\.[^.].*$")
	local_parent_path = []
	for modset_root in modset_root_list:
		modset_root_path = os.path.join(rootPath, modset_root)
		if not os.path.exists(modset_root_path):
			continue
		if modset_root != ".":
			modset_local_path = os.path.join(modset_root_path, basename)
			if os.path.exists(modset_local_path):
				local_parent_path.append(modset_root_path)
		dir_list = sorted(os.listdir(modset_root_path))
		for entry in dir_list:
			if dot_file.match(entry):
				continue
			modset_subdir_path = os.path.join(modset_root_path, entry)
			modset_subdir_local = os.path.join(modset_subdir_path, basename)
			if os.path.exists(modset_subdir_local):
				local_parent_path.append(modset_subdir_path)
	return local_parent_path

def readLocalEnvFiles(rootPath='', auto_env=None):

	if auto_env is None:
		auto_env = {}

	toolenv = findTools(auto_env)

	env_files = ["default.env", "local.env"]

	# update toolenv
	readEnvFiles(env_files, toolenv, os.environ)

	default_env = os.path.join(rootPath, "default.env")
	fe_env = os.path.join(rootPath, "..", "fe.env")
	local_env = os.path.join(rootPath, "local.env")
	local_env_dist = os.path.join(rootPath, "local.env.dist")

	if not os.path.exists(local_env_dist):
		cprint(YELLOW,1,"missing %s" % local_env_dist)
		sys.exit(1)
	elif not os.path.exists(local_env):
		cprint(CYAN,1,'copying local.env.dist to non-existent local.env')
		shutil.copy(local_env_dist, local_env)
	if not os.path.exists(local_env):
		cprint(RED,1,"missing %s" % local_env)
		sys.exit(1)

	env_files = [default_env]

	if os.path.exists(fe_env):
		env_files += [fe_env]
		cprint(CYAN,0,'using ../fe.env')
	else:
		cprint(CYAN,0,'not using ../fe.env (not found)')

	env_files += [local_env]

	# get a reference to a freshly updated toolenv
	env = readEnvFiles(env_files, toolenv, os.environ)

	if "FE_MODSET_ROOTS" in os.environ:
		modset_roots = os.environ["FE_MODSET_ROOTS"]
	elif "FE_MODSET_ROOTS" in env:
		modset_roots = env["FE_MODSET_ROOTS"]
	else:
		modset_roots = "."

	modset_list	= []
	modset_root_list = modset_roots.split(':')

	env_files = [default_env]
	if os.path.exists(fe_env):
		env_files += [fe_env]

	for env_name in ["default.env", "local.env"]:
		if env_name == "local.env":
			env_files += [local_env]
		local_parent_path = findModsetFiles(rootPath, env_name, modset_root_list)
		for local_parent in local_parent_path:
			extra_env = os.path.join(local_parent, env_name)
			env_files += [extra_env]

	# NOTE readEnvFiles() overlays each env file on top of previous ones,
	# excluding keys in os.environ

	# get a reference to a freshly updated toolenv
	env = readEnvFiles(env_files, toolenv, os.environ)

	return (env, modset_roots)

def applyLocalEnvFiles(rootPath='', auto_env=None):
	'''
	A convenience function that takes the results of readLocalEnvFiles() and
	injects them into os.environ.  Any key collision with os.environ results in
	a warning, but the value is updated regardless.
	'''

	if auto_env is None:
		auto_env = {}

	(env, modset_roots) = readLocalEnvFiles(rootPath, auto_env)

	for key in env:
		if key in os.environ:
			print("WARNING: key collision with os.environ '{}'".format(key))
		os.environ[key] = env[key]

	return (env, modset_roots)

def findTools(env=None):
	'''
	Returns env updated with any key:value pairs matching discovered tool
	locations.  If env is not provided, a new dict is used and returned.  Note
	that some tools might have an implicit default location that is provided if
	no explicit discovery occurs.

	Currently the search includes the following tools (and their associated
	variable names):
	- FE_VCPKG_DETECTED: vcpkg
	'''

	if env is None:
		env = {}

	vcpkg_path_txt = os.path.expandvars(r"%appdata%\..\Local\vcpkg\vcpkg.path.txt")

	detected = ""

	if os.path.exists(vcpkg_path_txt):
		with open(vcpkg_path_txt) as vcpkg_path_file:
			line = vcpkg_path_file.readline()
		detected = os.path.normpath(line)

	if not os.path.exists(detected):
		detected = "${FE_WINDOWS_LOCAL}/vcpkg"

	env["FE_VCPKG_DETECTED"] = detected

	return env

def readEnvFiles(env_files, env=None, authoritative_env=None):
	'''
	Returns the updated env (or a new dict if env is not specified)
	that contains key:value pairs from all files in the env_files list.
	Any key that exists in authoritative_env is omitted.
	Also, substitutions occur along the way,
	using authoritative_value as an original basis,
	and thereafter the env itself.
	'''

	for filename in env_files:
		env = readEnvFile(filename, env, authoritative_env)

	for variable in env.keys():
		value = env[variable]

		# replace ${OTHER_VAR}
		while True:
			subs = re.findall(r'\${([^}]*)}', value)
			if not len(subs):
				break
			for sub in subs:
				if sub in authoritative_env:
					replace = authoritative_env[sub]
				elif sub in env:
					replace = env[sub]
				else:
					# unknown variable replace with nothing
					replace = ""
				value = value.replace("${" + sub + "}", replace)

		# Seems like this filtering already occurred in readEnvFile().
		if variable not in authoritative_env:
			env[variable] = value

	return env

def readEnvFile(filename, env=None, authoritative_env=None):
	'''
	Returns a copy of env (or a new dict) that has accumulated the key:value
	pairs in the specified file.  Any key that exists in authoritative_env is
	omitted.  Also, authoritative_env is used as the source of values for any
	substitution expression detected in the value in the file.
	'''

	# Ensure we don't modify or return the param dict.
	env = env.copy() if env else {}

	# TODO is this ok to allow spaces in value without quotes?

	if not os.path.exists(filename):
		print("WARNING: readEnvFile() could not find '{}'".format(filename))
		return env

	with open(filename) as envFile:
		while True:
			line = envFile.readline()
			if line == "":
				break
			multiline = (line[-2:-1] == '\\')
			entry = line.split('#')[0].strip().rstrip("\\").strip()

			while(multiline):
				nextLine = envFile.readline()
				if nextLine == "":
					break
				multiline = (nextLine[-2:-1] == '\\')
				nextEntry = nextLine.split('#')[0].strip().rstrip("\\").strip()
				if nextEntry != "":
					entry = entry + nextEntry

			tokens = entry.split('=')
			if len(tokens) != 2:
				continue

			variable = tokens[0].strip()
			value = tokens[1].strip()

			# potentially remove quotes
			value = re.sub(r'"([^"]*)"',r'\1',value)

			subs = re.findall(r'\${([^}]*)}', value)

			# fill in self-references as they are read
			for sub in subs:
				if sub == variable:
					if sub in authoritative_env:
						replace = authoritative_env[sub]
					elif sub in env:
						replace = env[sub]
					else:
						continue
					value = value.replace("${" + sub + "}", replace)

			if variable not in authoritative_env:
				env[variable] = value

	return env

def env_list(name):
	if not name in os.environ:
		return []
	result = os.environ[name].split(':')
	result = list(filter(None, result))		# remove empty entries
	return result

# NOTE ast.literal_eval() is only single object (can't do "['a']+['b']")
def safer_eval(expression):

	# far from bullet proof
	# https://nedbatchelder.com/blog/201206/eval_really_is_dangerous.html
	return eval(expression, {'__builtins__':{}})

# -----------------------------------------------------------------------------
# Convenience tuple for user credentials.
# Any field set to False will be ignored by the credential supply logic,
# meaning that you can ask for only the field(s) you care about.
Credential = collections.namedtuple('Credential',
	['user', 'password'],
	defaults=[None, None],
)

def getUserCredentials(creds=None):

	if creds is None:
		creds = Credential()

	# Ignore the pathological case (in which the caller wants nothing):
	if all(v is False for v in creds):
		return creds

	cprint(YELLOW,0,"Enter server credentials")

	user, password = creds.user, creds.password

	if user != False:
		# Default user can be obtained implicitly from the system.
		default_user = getpass.getuser()

		user = input("Username [%s]: " % default_user)
		if user == '':
			user = default_user

	if password != False:
		password = getpass.getpass()

	# Supply new credential values, preserving anything else in the tuple.
	creds = creds._replace(user=user, password=password)

	return creds

class Atomic():
	def __init__(self, value=0):
		self.value = int(value)
		self.lock = threading.Lock()

	def increment(self, delta=1):
		with self.lock:
#			print("Atomic %d -> %d" % (self.value, self.value+delta))
			self.value += int(delta)
			return self.value

	def decrement(self, delta=1):
		return self.increment(-delta)

	def exchange(self, replacement):
		with self.lock:
			previous = self.value
			self.value = replacement
		return previous

	def current(self):
		with self.lock:
			return self.value
