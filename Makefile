all:
	./build.py

clean:
	./build.py clean

unit:
	./build.py unit

test: unit

docs:
	./build.py docs
