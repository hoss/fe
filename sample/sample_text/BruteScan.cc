/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <sample_text/sample_text.pmh>

namespace fe
{

void BruteScan::analyze(String a_text)
{
	m_text=a_text;
	m_histogram.clear();
}

U32 BruteScan::wordCount(void)
{
	String buffer=m_text;
	U32 count=0;
	while(buffer.parse()!="")
	{
		count++;
	}

	return count;
}

U32 BruteScan::charCount(void)
{
	String buffer=m_text;
	String token;
	U32 count=0;
	while((token=buffer.parse())!="")
	{
		count+=token.length();
	}

	return count;
}

U32 BruteScan::letterCount(void)
{
	populateHistogram();

	return m_histogram.size();
}

U32 BruteScan::letterFrequency(char m_letter)
{
	if(m_letter<'a' || m_letter>'z')
	{
		return 0;
	}

	populateHistogram();

	return m_histogram[m_letter];
}

void BruteScan::populateHistogram(void)
{
	//* don't rebuild unnecessarily
	if(m_histogram.size())
	{
		return;
	}

	const char *rawbuffer=m_text.c_str();
	const U32 byteCount=strlen(rawbuffer);

	for(U32 byte=0;byte<byteCount;byte++)
	{
		char letter=rawbuffer[byte];
		if(letter<'a' || letter>'z')
		{
			continue;
		}
		if(m_histogram.find(letter)==m_histogram.end())
		{
			m_histogram[letter]=1;
		}
		else
		{
			m_histogram[letter]++;
		}
	}
}

} // namespace
