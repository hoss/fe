/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __sample_text_StringScanI_h__
#define __sample_text_StringScanI_h__

namespace fe
{

/**************************************************************************//**
    @brief Text Analyzer

	@ingroup sample_text

	Evaluate metrics on a given string.
*//***************************************************************************/
class FE_DL_EXPORT StringScanI: virtual public Component
{
	public:
				/// specify a string to process
virtual	void	analyze(String filename)									=0;

				/// number of distinct words
virtual	U32		wordCount(void)												=0;

				/// number of non-whitespace characters
virtual	U32		charCount(void)												=0;

				/// number of unique non-whitespace letters (a-z)
virtual	U32		letterCount(void)											=0;

				/// appearances of a specific character (a-z)
virtual	U32		letterFrequency(char m_letter)								=0;
};

} // namespace

#endif /* __sample_text_StringScanI_h__ */
