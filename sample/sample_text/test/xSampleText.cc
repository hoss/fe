/** @file */

#include "sample_text/sample_text.h"

using namespace fe;

int main(int argc,char** argv)
{
	UNIT_START();

	BWORD complete=FALSE;

	try
	{
		sp<Master> spMaster(new Master);
		sp<Registry> spRegistry=spMaster->registry();

		Result result=spRegistry->manage("feAutoLoadDL");
		UNIT_TEST(successful(result));

		{
			sp<StringScanI> spStringScanI(spRegistry->create("StringScanI"));
			if(!spStringScanI.isValid())
			{
				feX(argv[0], "couldn't create component");
			}

			for(U32 pass=0;pass<2;pass++)
			{
				//*	see https://en.wikipedia.org/wiki/Pangram
				const String text=pass?
						"glib jocks quiz nymph to vex dwarf":
						"the quick brown fox jumps over a lazy dog";

				feLog("\nAnalyzing: \"%s\"\n",text.c_str());
				spStringScanI->analyze(text);

				const U32 wordCount=spStringScanI->wordCount();
				feLog("Word Count: %d\n",wordCount);
				UNIT_TEST(wordCount==(pass? 7: 9));

				const U32 charCount=spStringScanI->charCount();
				feLog("Char Count: %d\n",charCount);
				UNIT_TEST(charCount==(pass? 28: 33));

				const U32 letterCount=spStringScanI->letterCount();
				feLog("Letter Count: %d\n",letterCount);
				UNIT_TEST(letterCount==26);

				const U32 eCount=spStringScanI->letterFrequency('e');
				feLog("'e' Count: %d\n",eCount);
				UNIT_TEST(eCount==(pass? 1: 2));
			}
		}

		complete=TRUE;
	}
	catch(Exception &e) { e.log(); }
	catch(...)			{ feLog("uncaught exception\n"); }

	UNIT_TEST(complete);
	UNIT_TEST(!Counted::trackerCount());
	UNIT_TRACK(11);
	UNIT_RETURN();
}
