/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __operate_OperateCommon_h__
#define __operate_OperateCommon_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
    @brief Common Operator/Manipulator methods

	@ingroup operate
*//***************************************************************************/
class FE_DL_EXPORT OperateCommon:
	public Catalog,
	virtual public HandlerI
{
	public:
		enum	CurveMode
				{
					e_null,
					e_lines,
					e_panels,
					e_cylinders,
					e_tube
				};

	class Edge
	{
		public:
								Edge(void):
									m_count(0)								{}

			SpatialVector		m_pointA;
			SpatialVector		m_pointB;
			SpatialVector		m_normal0;
			SpatialVector		m_normal1;
			String				m_part0;
			String				m_part1;
			I32					m_count;
	};

							OperateCommon(void)								{}
virtual						~OperateCommon(void)							{}

							//* As HandlerI
virtual	void				handleBind(sp<SignalerI> a_spSignalerI,
									sp<Layout> a_spLayout);

		Record				windowEvent(Record& a_rSignal)
							{
								Record* pRecord=
										m_aWindowEvent.queryAttribute(
										a_rSignal);
								if(pRecord)
								{
									return *pRecord;
								}
								return Record();
							}

static	void				drawLabel(sp<DrawI> a_spDrawI,
									const SpatialVector& a_location,
									String a_label,
									BWORD a_centered,
									U32 a_margin,
									const Color a_textColor,
									const Color* a_pBorderColor,
									const Color* a_pBackground);
static	void				drawIndicator(sp<DrawI> a_spDrawI,Box2i& a_box,
									BWORD a_vertical,BWORD a_reverse,
									Real a_value,Real a_loopValue,Real a_grain,
									I32 a_precision,
									I32 a_minChars,I32 a_maxChars,
									Real a_multiplication,
									const Color* a_pBrightColor,
									const Color* a_pDimColor,
									const Color* a_pBackground,
									const Color* a_pOdometerBG);
static	void				drawOdometer(sp<DrawI> a_spDrawI,
									SpatialVector a_location,
									Real a_value,I32 a_precision,
									I32 a_minChars,I32 a_maxChars,
									BWORD a_centered,U32 a_margin,
									const Color a_textColor,
									const Color* a_pBorderColor,
									const Color* a_pBackground);
static	void				drawPicker(sp<DrawI> a_spDrawI,
									const SpatialVector& a_point,
									const SpatialVector& a_up,
									const SpatialVector& a_normal,
									Real a_scale,I32 a_depth,
									const Color* a_pColor);
static	void				drawImpact(sp<DrawI> a_spDrawI,
									sp<SurfaceTriangles::Impact> a_spTriImpact,
									Real a_inset,const Color* a_pColor);
static	void				drawInsetTriangle(sp<DrawI> a_spDrawI,
									const SpatialVector& a_vertex0,
									const SpatialVector& a_vertex1,
									const SpatialVector& a_vertex2,
									Real a_inset,const Color* a_pColor);
static	void				highlightCurve(sp<ViewI> a_spView,
									sp<DrawI> a_spDraw,
									sp<SurfaceAccessorI> a_spVertices,
									I32 a_primitiveIndex,I32 a_vertexIndex,
									const Color* a_pLineColor,
									const Color* a_pPointColor,
									const Color* a_pPointHighlight);
static	void				drawDot(sp<ViewI> a_spView,sp<DrawI> a_spDraw,
									SpatialVector a_point,Real a_radius,
									const Color a_color);
static	void				drawSquare(sp<DrawI>& a_rspDraw,
									const SpatialVector& a_rPoint,
									Real a_rimSize,BWORD a_bevel,
									const Color& a_rColorCenter,
									const Color& a_rColorRim,
									const Color* a_pColorShadow,
									const Color* a_pColorSparkle);
static	void				drawTube(sp<DrawI>& a_rspDraw,
									const SpatialVector* a_point,
									const SpatialVector* a_normal,
									const Real* a_radius,
									U32 a_pointCount,CurveMode a_curveMode,
									BWORD a_multicolor,const Color *a_color,
									const SpatialVector& a_rTowardCamera);
static	void				drawTubeAxes(sp<DrawI>& a_rspDraw,
									const SpatialTransform& a_transform,
									Real a_scale,Real a_alpha,
									const Array<Real>& a_profile,
									CurveMode a_curveMode);
static	void				drawAnchor(sp<DrawI>& a_rspDraw,
									SpatialTransform a_transform,
									Real a_scale,U32 a_resolution,
									Color a_color);
static	void				drawCrossSection(sp<DrawI>& a_rspDraw,
									SpatialVector a_p0,
									SpatialVector a_p1,
									SpatialVector a_p2,
									SpatialVector a_planeCenter,
									SpatialVector a_planeNormal,
									Color a_color);
static	void				drawCrossSection(sp<DrawI>& a_rspDraw,
									sp<SurfaceAccessorI> a_spVertices,
									SpatialVector a_planeCenter,
									SpatialVector a_planeNormal,
									Color a_color);
static	void				drawOutline(sp<DrawI> a_spDrawI,
									sp<SurfaceAccessibleI>
									a_spSurfaceAccessibleI,
									String a_partAttr,String a_partName,
									SpatialVector a_cameraPos,Color a_color);
static	void				createEdges(
									sp<SurfaceAccessibleI>
									a_spSurfaceAccessibleI,
									String a_partAttr,
									std::map<U64,Edge>& a_rEdgeMap);
static	void				drawOutline(sp<DrawI> a_spDrawI,
									std::map<U64,Edge>& a_rEdgeMap,
									String a_partName,
									SpatialVector a_cameraPos,Color a_color);

static	I32					nearestPointIndex(
									const sp<SurfaceAccessorI>
									a_spPrimitiveVertices,
									sp<SurfaceCurves::Impact>&
									a_rspCurveImpact);

static	I32					nearestTriVertexIndex(
									sp<SurfaceTriangles::Impact>&
									a_rspTriImpact);
static	I32					nearestPointIndex(sp<SurfaceTriangles::Impact>&
									a_rspTriImpact);
static	SpatialVector		nearestVertexPoint(sp<SurfaceTriangles::Impact>&
									a_rspTriImpact);

static	Real				nearestPointAlongCurve(const SpatialVector a_point,
									const sp<SurfaceAccessorI>
									a_spPrimitiveVertices,
									const I32 a_primitiveIndex);

static	Real				fractionAtLength(
									const sp<SurfaceAccessorI>
									a_spPrimitiveVertices,
									const I32 a_primitiveIndex,
									Real a_length);
static	Real				lengthAtFraction(
									const sp<SurfaceAccessorI>
									a_spPrimitiveVertices,
									const I32 a_primitiveIndex,
									Real a_fraction);
static	SpatialVector		evaluateAtFraction(
									const sp<SurfaceAccessorI>
									a_spPrimitiveVertices,
									const I32 a_primitiveIndex,
									Real a_fraction,U32 a_derivitive);

static	Real				flameProfile(Real a_peakOffset,Real a_fraction);

	private:

		Accessor<Record>	m_aWindowEvent;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __operate_OperateCommon_h__ */
