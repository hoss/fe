/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __operate_ManipulatorCommon_h__
#define __operate_ManipulatorCommon_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
    @brief Partial Generic ManipulatorI Implemention

	@ingroup operate
*//***************************************************************************/
class FE_DL_EXPORT ManipulatorCommon:
	public OperateCommon,
	virtual public ManipulatorI,
	public Initialize<ManipulatorCommon>
{
	public:
					ManipulatorCommon(void):
						m_hotGrip(-1),
						m_lastGrip(-1),
						m_dragging(FALSE)									{}
virtual				~ManipulatorCommon(void)								{}

		void		initialize(void);

					//* As ManipulatorI
virtual	void		bindOverlay(sp<DrawI>& a_spDrawOverlay);

virtual	String		mode(void) const	{ return ""; }

					//* As HandlerI (window events)
virtual	void		handle(Record& a_rSignal);

					//* As DrawableI
virtual	void		draw(sp<DrawI> a_spDrawI,const Color *color);
virtual	void		draw(sp<DrawI> a_spDrawI,const Color *color) const		{}
virtual void		draw(const SpatialTransform &transform,
							sp<DrawI> a_spDrawI,const Color* color)			{}
virtual void		draw(const SpatialTransform &transform,
							sp<DrawI> a_spDrawI,const Color* color) const	{}
virtual void		draw(const SpatialTransform &transform,
							sp<DrawI> a_spDrawI,const Color* color,
							sp<DrawBufferI> a_spDrawBuffer,
							sp<PartitionI> a_spPartition)					{}
virtual void		draw(const SpatialTransform &transform,
							sp<DrawI> a_spDrawI,const Color* color,
							sp<DrawBufferI> a_spDrawBuffer,
							sp<PartitionI> a_spPartition) const				{}

	protected:

		BWORD		gripLabels(void) const
					{	return catalogOrDefault<bool>("gripLabels",FALSE); }
		void		setGripLabels(BWORD a_gripLabels)
					{	catalog<bool>("gripLabels")=a_gripLabels; }

		Real		anchorScale(void) const
					{	return catalogOrDefault<Real>("anchorScale",1.0); }
		void		setAnchorScale(Real a_anchorScale)
					{	catalog<Real>("anchorScale")=a_anchorScale; }

		Real		gripScale(void) const
					{	return catalogOrDefault<Real>("gripScale",1.0); }
		void		setGripScale(Real a_gripScale)
					{	catalog<Real>("gripScale")=a_gripScale; }

		Real		dimming(void) const
					{	return catalogOrDefault<Real>("dimming",0.0); }
		void		setDimming(Real a_dimming)
					{	catalog<Real>("dimming")=a_dimming; }

		Real		foreshadow(void) const
					{	return catalogOrDefault<Real>("foreshadow",0.0); }
		void		setForeshadow(Real a_foreshadow)
					{	catalog<Real>("foreshadow")=a_foreshadow; }

		Real		aura(void) const
					{	return catalogOrDefault<Real>("aura",0.0); }
		void		setAura(Real a_aura)
					{	catalog<Real>("aura")=a_aura; }

		void		drawTubeWithAura(sp<DrawI>& a_rspDraw,
							const SpatialVector* a_point,
							const SpatialVector* a_normal,
							const Real* a_radius,
							U32 a_pointCount,CurveMode a_curveMode,
							BWORD a_multicolor,const Color *a_color,
							const SpatialVector& a_rTowardCamera,
							const Real a_auraScale);

	class Grip
	{
		public:
							Grip(void):
								m_hotSpot(0.0,0.0,0.0),
								m_radius(0.0)
							{
								set(m_alignment[0],0.0,0.0,0.0);
								set(m_alignment[1],0.0,0.0,0.0);
								set(m_planeCenter,0.0,0.0,0.0);
								set(m_planeFacing,0.0,0.0,0.0);
							}

							//* screen-space
			SpatialVector	m_hotSpot;
			Real			m_radius;
			String			m_key[2];
			String			m_message;
			String			m_label;

							//* world-space
			SpatialVector	m_alignment[2];
			SpatialVector	m_planeCenter;
			SpatialVector	m_planeFacing;
	};

					//* reset hotspots and update the draw cache
virtual	void		updateGrips(void);

		sp<DrawMode>		m_spTubeAura;

		Array<Grip>			m_gripArray;
		I32					m_hotGrip;
		I32					m_lastGrip;
		BWORD				m_dragging;
		Vector2i			m_lastMouse;

		sp<DrawI>			m_spDrawCache;
		sp<DrawI>			m_spOverlayCache;
		WindowEvent			m_event;

		SpatialVector		m_facing;
		SpatialVector		m_upwards;
		SpatialVector		m_rightways;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __operate_ManipulatorCommon_h__ */
