/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <operate/operate.pmh>

#define FE_OST_IO_DEBUG		FALSE
#define FE_OST_UNDO_DEBUG	FALSE

using namespace fe;
using namespace fe::ext;

void OperatorState::initialize(void)
{
	m_spScope=registry()->create("Scope");
	m_spScope->setName(name()+".State");
	m_spState=new RecordGroup();

	setupState();

	catalog<String>("IdAttr")="id";
	catalog<String>("IdAttr","label")="ID Attribute";
	catalog<String>("IdAttr","page")="Data";
	catalog<String>("IdAttr","hint")=
			"If defined, store changes based on an identifier"
			" in this primitive attribute instead of"
			" the index of simple primitive order."
			"  The values should be unique integers.";

	catalog< sp<Component> >("Output Surface");
	catalog<String>("Output Surface","IO")="output";
	catalog<String>("Output Surface","copy")="Input Surface";
	catalog<bool>("Output Surface","temporal")=true;
}

void OperatorState::updateIds(void)
{
	const String idAttr=catalog<String>("IdAttr");
	sp<SurfaceAccessorI>	m_spInputIds;
	m_spInputIds=NULL;
	if(!idAttr.empty())
	{
		access(m_spInputIds,"Input Surface",e_primitive,idAttr,e_quiet);
	}

	m_elementIndexMap.clear();
	if(m_spInputIds.isValid())
	{
		const U32 count=m_spInputIds->count();
		for(U32 primIndex=0;primIndex<count;primIndex++)
		{
			m_elementIndexMap[m_spInputIds->integer(primIndex)]=primIndex;
		}
	}
}

I32 OperatorState::lookupIndex(I32 a_id)
{
	if(a_id<0 || m_spInputIds.isNull())
	{
		return a_id;
	}
	if(m_elementIndexMap.find(a_id)==m_elementIndexMap.end())
	{
		return -1;
	}
	return m_elementIndexMap[a_id];
}

I32 OperatorState::lookupId(I32 a_elementIndex)
{
	return m_spInputIds.isValid()?
			m_spInputIds->integer(a_elementIndex): a_elementIndex;
}

BWORD OperatorState::loadState(const String& rBuffer)
{
#if FE_OST_IO_DEBUG
	feLog("OperatorState::loadState\n");
	feLog("---------\n");
	feLog("%s",rBuffer.c_str());
	feLog("---------\n");
#endif

	try
	{
		std::string stringIn=rBuffer.c_str();
		std::istringstream streamIn(stringIn);

		sp<data::StreamI> spStream(new data::AsciiStream(m_spScope));
		sp<RecordGroup> spLoadedGroup=spStream->input(streamIn);

		if(spLoadedGroup.isValid())
		{
			m_spState=spLoadedGroup;
		}

		return TRUE;
	}
	catch(...)
	{
	}

	return FALSE;
}

BWORD OperatorState::saveState(String& rBuffer)
{
#if FE_OST_IO_DEBUG
	feLog("OperatorState::saveState\n");
#endif

	try
	{
		sp<data::StreamI> spStream(new data::AsciiStream(m_spScope));
		std::ostringstream streamOut;
		spStream->output(streamOut,m_spState);

		std::stringbuf* pStringBuf=streamOut.rdbuf();
		rBuffer=pStringBuf->str().c_str();

#if FE_OST_IO_DEBUG
		feLog("---------\n");
		feLog("%s",rBuffer.c_str());
		feLog("---------\n");
#endif

		return TRUE;
	}
	catch(...)
	{
	}

	return FALSE;
}

BWORD OperatorState::undo(String a_change,sp<Counted> a_spCounted)
{
#if FE_OST_UNDO_DEBUG
	feLog("OperatorState::undo \"%s\" %d\n",
			a_change.c_str(),a_spCounted.isValid());
#endif

	m_spState=a_spCounted;
	return TRUE;
}

BWORD OperatorState::redo(String a_change,sp<Counted> a_spCounted)
{
#if FE_OST_UNDO_DEBUG
	feLog("OperatorState::redo \"%s\" %d\n",
			a_change.c_str(),a_spCounted.isValid());
#endif

	m_spState=a_spCounted;
	return TRUE;
}

BWORD OperatorState::anticipate(String a_change,sp<Counted> a_spCounted)
{
#if FE_OST_UNDO_DEBUG
	feLog("OperatorState::anticipate \"%s\" counted valid %d\n",
			a_change.c_str(),a_spCounted.isValid());
#endif

	sp<Counted> spChecked=a_spCounted;
	if(spChecked.isNull())
	{
		String buffer;
		saveState(buffer);

		//* from loadState()
		std::string stringIn=buffer.c_str();
		std::istringstream streamIn(stringIn);

		sp<data::StreamI> spStream(new data::AsciiStream(m_spScope));
		spChecked=spStream->input(streamIn);
	}

#if FE_OST_UNDO_DEBUG
	feLog("OperatorState::anticipate checked valid %d\n",spChecked.isValid());
#endif

	return OperatorSurfaceCommon::anticipate(a_change,spChecked);
}

BWORD OperatorState::resolve(String a_change,sp<Counted> a_spCounted)
{
#if FE_OST_UNDO_DEBUG
	feLog("OperatorState::resolve \"%s\" counted valid %d\n",
			a_change.c_str(),a_spCounted.isValid());
#endif

	sp<Counted> spChecked=a_spCounted;
	if(spChecked.isNull())
	{
		String buffer;
		saveState(buffer);

		//* from loadState()
		std::string stringIn=buffer.c_str();
		std::istringstream streamIn(stringIn);

		sp<data::StreamI> spStream(new data::AsciiStream(m_spScope));
		spChecked=spStream->input(streamIn);
	}

#if FE_OST_UNDO_DEBUG
	feLog("OperatorState::resolve checked valid %d\n",spChecked.isValid());
#endif

	return OperatorSurfaceCommon::resolve(a_change,spChecked);
}

BWORD OperatorState::pickToggle(I32 a_elementId)
{
	const I32 pickCount=m_pickArray.size();
	for(I32 pickId=0;pickId<pickCount;pickId++)
	{
		const Pick& rPick=m_pickArray[pickId];
		if(a_elementId==rPick.m_elementId)
		{
			m_pickArray.erase(m_pickArray.begin()+pickId);
			return TRUE;
		}
	}
	pick(a_elementId,1.0);
	return FALSE;
}

void OperatorState::pick(I32 a_primitiveId,I32 a_subIndex,Real a_weight)
{
	const I32 pickCount=m_pickArray.size();
	for(I32 pickId=0;pickId<pickCount;pickId++)
	{
		Pick& rPick=m_pickArray[pickId];
		if(a_primitiveId==rPick.m_elementId && a_subIndex==rPick.m_subIndex)
		{
			if(rPick.m_weight<a_weight)
			{
				rPick.m_weight=a_weight;
			}
			return;
		}
	}

	m_pickArray.push_back(Pick(a_primitiveId,a_subIndex,a_weight));
}

void OperatorState::pick(I32 a_elementId,Real a_weight)
{
	const I32 pickCount=m_pickArray.size();
	for(I32 pickId=0;pickId<pickCount;pickId++)
	{
		Pick& rPick=m_pickArray[pickId];
		if(a_elementId==rPick.m_elementId)
		{
			if(rPick.m_weight<a_weight)
			{
				rPick.m_weight=a_weight;
			}
			return;
		}
	}

	m_pickArray.push_back(Pick(a_elementId,a_weight));
}

Real OperatorState::picked(I32 a_elementId)
{
	const I32 pickCount=m_pickArray.size();
	for(I32 pickId=0;pickId<pickCount;pickId++)
	{
		const Pick& rPick=m_pickArray[pickId];
		if(a_elementId==rPick.m_elementId)
		{
			return rPick.m_weight;
		}
	}
	return 0.0;
}

