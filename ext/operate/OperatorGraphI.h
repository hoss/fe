/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __operate_OperatorGraphI_h__
#define __operate_OperatorGraphI_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief General graph navigation

	@ingroup operate
*//***************************************************************************/
class FE_DL_EXPORT OperatorGraphI:
	public Component,
	public CastableAs<OperatorGraphI>
{
	public:

virtual	sp<ImageI>	image(void)												=0;

virtual	U32			inputCount(String a_nodeName) const						=0;
virtual	String		inputConnector(String a_nodeName,U32 a_index) const		=0;
virtual	BWORD		hasInputConnector(String a_nodeName,
							String a_inputConnector) const					=0;
virtual	String		inputNode(String a_nodeName,
							String a_inputConnector) const					=0;
virtual	String		inputNodeOutputConnector(String a_nodeName,
							String a_inputConnector) const					=0;

virtual	U32			outputCount(String a_nodeName) const					=0;
virtual	String		outputConnector(String a_nodeName,U32 a_index) const	=0;
virtual	BWORD		hasOutputConnector(String a_nodeName,
							String a_outputConnector) const					=0;
virtual	String		outputNode(String a_nodeName,
							String a_outputConnector) const					=0;

virtual	BWORD		connect(String a_outputName,String a_outputConnector,
							String a_inputName,String a_inputConnector)		=0;
virtual	BWORD		disconnect(String a_inputName,String a_inputConnector)	=0;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __operate_OperatorGraphI_h__ */
