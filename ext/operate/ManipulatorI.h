/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __operate_ManipulatorI_h__
#define __operate_ManipulatorI_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Interactive collection of grips

	@ingroup operate
*//***************************************************************************/
class FE_DL_EXPORT ManipulatorI:
	virtual public HandlerI,
	virtual public DrawableI,
	public CastableAs<ManipulatorI>
{
	public:

virtual	void	bindOverlay(sp<DrawI>& a_spDrawOverlay)						=0;

virtual	String	mode(void) const											=0;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __operate_ManipulatorI_h__ */
