/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __operate_OperatorState_h__
#define __operate_OperatorState_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Operator base class to save and reload state

	@ingroup operate
*//***************************************************************************/
class FE_DL_EXPORT OperatorState:
	public OperatorSurfaceCommon,
	public Initialize<OperatorState>
{
	public:

								OperatorState(void)							{}
virtual							~OperatorState(void)						{}

		void					initialize(void);

								//* As OperatorSurfaceI
virtual	BWORD					saveState(String& rBuffer);
virtual	BWORD					loadState(const String& rBuffer);

virtual	BWORD					undo(String a_change,sp<Counted> a_spCounted);
virtual	BWORD					redo(String a_change,sp<Counted> a_spCounted);

	protected:

	class FE_DL_EXPORT Pick
	{
		public:
					Pick(void)												{}
					Pick(I32 a_primitiveId,I32 a_subIndex,Real a_weight):
						m_elementId(a_primitiveId),
						m_subIndex(a_subIndex),
						m_weight(a_weight)									{}
					Pick(I32 a_elementId,Real a_weight):
						m_elementId(a_elementId),
						m_subIndex(-1),
						m_weight(a_weight)									{}

			I32		m_elementId;
			I32		m_subIndex;
			Real	m_weight;
	};

virtual	void					setupState(void)							{}

								using OperatorSurfaceCommon::anticipate;
								using OperatorSurfaceCommon::resolve;

								//* As OperatorSurfaceCommon
virtual	BWORD					anticipate(String a_change,
										sp<Counted> a_spCounted);
virtual	BWORD					resolve(String a_change,
										sp<Counted> a_spCounted);

		BWORD					pickToggle(I32 a_elementId);
		Real					picked(I32 a_elementId);
		void					pick(I32 a_primitiveId,I32 a_subIndex,
										Real a_weight);
		void					pick(I32 a_elementId,Real a_weight);

		void					updateIds(void);
		I32						lookupIndex(I32 a_id);
		I32						lookupId(I32 a_elementIndex);

		sp<Scope>				m_spScope;
		sp<RecordGroup>			m_spState;
		Accessor<I32>			m_aIndex;
		Array<Pick>				m_pickArray;

	private:

		sp<SurfaceAccessorI>	m_spInputIds;
		std::map<I32,I32>		m_elementIndexMap;

};

} /* namespace ext */
} /* namespace fe */

#endif /* __operate_OperatorState_h__ */
