/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __operate_h__
#define __operate_h__

#include "surface/surface.h"

#include "operate/OperatorPlugin.h"

#include "operate/ManipulatorI.h"
#include "operate/OperatorGraphI.h"
#include "operate/OperatorSurfaceI.h"

#endif /* __operate_h__ */
