/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <operate/operate.pmh>

using namespace fe;
using namespace fe::ext;

void OperateCommon::handleBind(sp<SignalerI> a_spSignalerI,
									sp<Layout> a_spLayout)
{
	m_aWindowEvent.initialize(a_spLayout->scope(),
			"WindowEvent");
}

//* static
void OperateCommon::drawLabel(sp<DrawI> a_spDrawI,
	const SpatialVector& a_location,String a_label,BWORD a_centered,
	U32 a_margin, const Color a_textColor,const Color* a_pBorderColor,
	const Color* a_pBackground)
{
	I32 ascent(0);
	I32 descent(0);

	I32 width(0);
	I32 height(0);

	sp<FontI> spFont=a_spDrawI->font();
	if(spFont.isValid())
	{
		width=a_spDrawI->font()->pixelWidth(a_label);
		height=a_spDrawI->font()->fontHeight(&ascent,&descent);
	}
	else
	{
		feLog("OperateCommon::drawLabel invalid font\n");
	}

#if FALSE
	width=fe::maximum(width,I32(5*a_label.length()));
	height=fe::maximum(height,I32(7));
#endif

//	feLog("OperateCommon::drawLabel %s %dx%d (%d+%d) \"%s\"\n",
//			c_print(a_location),width,height,ascent,descent,a_label.c_str());

	SpatialVector corner=Vector3i(a_location);
	if(a_centered)
	{
		corner-=SpatialVector(width/2,height/2);
//		corner+=SpatialVector(0.5,0.5);
	}

	const SpatialVector behind=corner-SpatialVector(0.0,0.0,0.5);

	SpatialVector tweak[2];
	tweak[0]=SpatialVector(-0.375,-0.375);
	tweak[1]=SpatialVector(-1.375,0.625);

	SpatialVector rect[2];
	rect[0]=behind-SpatialVector(a_margin,a_margin)+tweak[0];
	rect[1]=behind+SpatialVector(width+a_margin,height+a_margin)+tweak[1];

	Box2 backbox;
	set(backbox,rect[0][0]-0.5,rect[0][1]-0.5,
			rect[1][0]-rect[0][0]+1,rect[1][1]-rect[0][1]+1);

	if(a_pBackground)
	{
		a_spDrawI->drawRectangles(rect,2,false,a_pBackground);
	}

	if(a_pBorderColor)
	{
		a_spDrawI->drawBox(backbox,*a_pBorderColor);
	}

/*	text outline
	const Color black(0.0,0.0,0.0,1.0);
	const SpatialVector offsetX(1.0,0.0,0.0);
	const SpatialVector offsetY(0.0,1.0,0.0);
	a_spDrawI->drawAlignedText(corner+offsetX,a_label,black);
	a_spDrawI->drawAlignedText(corner-offsetX,a_label,black);
	a_spDrawI->drawAlignedText(corner+offsetY,a_label,black);
	a_spDrawI->drawAlignedText(corner-offsetY,a_label,black);
*/

	a_spDrawI->drawAlignedText(corner+SpatialVector(0.0,descent),
			a_label,a_textColor);
}

//* static
void OperateCommon::drawIndicator(sp<DrawI> a_spDrawI,
		Box2i& a_box,BWORD a_vertical,BWORD a_reverse,
		Real a_value,Real a_loopValue,Real a_grain,
		I32 a_precision,I32 a_minChars,I32 a_maxChars,
		Real a_multiplication,
		const Color* a_pBrightColor,const Color* a_pDimColor,
		const Color* a_pBackground,const Color* a_pOdometerBG)
{
//	const I32 digits=a_minChars+(a_precision<0? a_precision-1: 0);
	const I32 digits=a_minChars;

	I32 fontAscent=0;
	I32 fontDescent=0;
	a_spDrawI->font()->fontHeight(&fontAscent,&fontDescent);
	const I32 textHeight=fe::maximum(10,fontAscent+fontDescent);
	const I32 fontWidth=a_spDrawI->font()->pixelWidth("W");
	const I32 characterWidth=fe::maximum(10,fontWidth);

	const I32 boxWidth=width(a_box);
	const I32 boxHeight=height(a_box);
	const I32 mainline=a_vertical?
			a_box[0]+(a_reverse? boxWidth: 0):
			a_box[1]+(a_reverse? boxHeight: 0);
	const SpatialVector textOffset=a_vertical?
			SpatialVector((a_reverse? -7.5: 0.5)*characterWidth,
			-0.5*textHeight):
			SpatialVector(0.0,(a_reverse? -2: 1.5)*textHeight);

	SpatialVector line[2];
	I32 center(0);

	if(a_vertical)
	{
		center=a_box[1]+0.5*boxHeight;

		set(line[0],mainline,a_box[1]);
		set(line[1],line[0][0],line[0][1]+boxHeight);
	}
	else
	{
		set(line[0],a_box[0],mainline);
		set(line[1],line[0][0]+boxWidth,line[0][1]);

		center=a_box[0]+0.5*boxWidth;
	}

	a_spDrawI->drawLines(line,NULL,2,DrawI::e_strip,false,a_pDimColor);

	if(a_grain>0.0)
	{
		const Real spacing=100;

		const Real range=(a_vertical? boxHeight: boxWidth)*a_grain/spacing;

		const F64 minValue=a_value-0.5*range;
		const F64 maxValue=a_value+0.5*range;

		const I32 lineLimit(1000);
		I32 lineCount(0);

		const F64 startValue=I32(minValue/a_grain)*a_grain;
		for(F64 tickValue=startValue;tickValue<maxValue;tickValue+=a_grain)
		{
			if(lineCount>=lineLimit)
			{
				feLog("OperateCommon::drawIndicator too many lines\n");
				break;
			}
			lineCount++;

			if(tickValue<minValue)
			{
				continue;
			}

			if(a_vertical)
			{
				set(line[0],a_box[0],
						center+(tickValue-a_value)/a_grain*spacing);
				set(line[1],line[0][0]+boxWidth,line[0][1]);
			}
			else
			{
				set(line[0],center+(tickValue-a_value)/a_grain*spacing,
						a_box[1]);
				set(line[1],line[0][0],line[0][1]+boxHeight);
			}

			a_spDrawI->drawLines(line,NULL,2,DrawI::e_strip,false,a_pDimColor);

			Real printValue=tickValue;
			if(a_loopValue>0.0)
			{
				while(printValue<0.0)
				{
					printValue+=a_loopValue;
				}
				while(printValue>=a_loopValue)
				{
					printValue-=a_loopValue;
				}
			}

			printValue+=1e-3*a_grain;

			const I32 decimals=fe::maximum(Real(0),Real(0.999-log10(a_grain)));

			const I32 maxDecimal=7;
			const Real expGrain=pow(0.1,maxDecimal);

			String text;
			if(a_grain<expGrain)
			{
				text.sPrintf("%.2E",printValue);
			}
			else if(a_vertical)
			{
				text.sPrintf("%*.*f",digits,decimals,printValue);
			}
			else
			{
				text.sPrintf("%.*f",decimals,printValue);
			}

			drawLabel(a_spDrawI,line[1]+textOffset,
					text,!a_vertical,1,*a_pDimColor,NULL,a_pBackground);
		}
	}

	Real vWidth=(a_vertical? 8: 4)*a_multiplication;
	Real vHeight=(a_vertical? 4: 8)*a_multiplication;

	if(a_vertical)
	{
		set(line[0],mainline-0.375,center-0.375);
		set(line[1],line[0][0]+(a_reverse? -vWidth: vWidth),
				line[0][1]-vHeight);
	}
	else
	{
		set(line[0],center-0.375,mainline-0.375);
		set(line[1],line[0][0]-vWidth,
				line[0][1]+(a_reverse? -vHeight: vHeight));
	}

	a_spDrawI->drawLines(line,NULL,2,DrawI::e_strip,false,a_pBrightColor);

	if(a_vertical)
	{
		line[0][1]-=1;
		line[1][1]=line[0][1]+vHeight;
	}
	else
	{
		line[0][0]-=1;
		line[1][0]=line[0][0]+vWidth;
	}

	a_spDrawI->drawLines(line,NULL,2,DrawI::e_strip,false,a_pBrightColor);

	if(a_vertical)
	{
		set(line[0],a_box[0]+boxWidth+textOffset[0],center-0.5*textHeight);
	}
	else
	{
		set(line[0],center,a_box[1]+boxHeight+textOffset[1]-0.5*textHeight);
	}

	//* avoid printing -0.0
	if(a_value<0.0 && a_value> -0.05*pow(10.0,a_precision))
	{
		a_value=0.0;
	}

//	const Color darkRed(0.4,0.0,0.0);
//	const Color darkBlue(0.0,0.0,0.4);
	drawOdometer(a_spDrawI,line[0],a_value,
			a_precision,a_minChars,a_maxChars,!a_vertical,
			1,*a_pBrightColor,a_pDimColor,a_pOdometerBG);
}

//* static
void OperateCommon::drawOdometer(sp<DrawI> a_spDrawI,
		SpatialVector a_location,Real a_value,I32 a_precision,
		I32 a_minChars,I32 a_maxChars,BWORD a_centered,
		U32 a_margin,const Color a_textColor,
		const Color* a_pBorderColor,const Color* a_pBackground)
{
//	feLog("\ndrawOdometer %.16G\n",a_value);

	I32 fontAscent=0;
	I32 fontDescent=0;
	a_spDrawI->font()->fontHeight(&fontAscent,&fontDescent);
	const I32 textHeight=fe::maximum(10,fontAscent+fontDescent);
	const I32 fontWidth=a_spDrawI->font()->pixelWidth("W");
	const I32 characterWidth=fe::maximum(10,fontWidth);

	const BWORD negative=(a_value<0.0);
	const Real value=(negative? -a_value: a_value);
	const I32 power=fe::maximum(Real(0),Real(log10(value)));
	const I32 chars=fe::minimum(a_maxChars,
			fe::maximum(a_minChars,
			1+power-a_precision+I32(a_precision<0)+negative));

	SpatialVector location=a_location;
	SpatialVector corner=a_location;
	if(a_centered)
	{
		location[0]+=(0.5*chars-1)*characterWidth;
		corner[0]-=0.5*chars*characterWidth;
	}
	else
	{
		location[0]+=(chars-1)*characterWidth;
	}

	Box2i scissor1(Vector2i(corner[0]-a_margin,
		corner[1]-0.5*textHeight-a_margin),
		Vector2i(chars*characterWidth+2*a_margin,2*textHeight+2*a_margin));
	Box2i scissor2(Vector2i(corner[0]-a_margin,corner[1]-a_margin),
		Vector2i(chars*characterWidth+2*a_margin,textHeight+2*a_margin));

	if(a_pBorderColor)
	{
		Box2 backbox;
		set(backbox,corner[0]-a_margin-0.875,
				corner[1]-a_margin-1.875,
				(chars-1)*characterWidth+a_margin-2,
				textHeight+2*a_margin+3);

		a_spDrawI->drawBox(backbox,*a_pBorderColor);

		set(backbox,corner[0]+(chars-1)*characterWidth-1.875,
				corner[1]-0.5*textHeight-a_margin-1.875,
				characterWidth+a_margin+1,
				2*textHeight+2*a_margin+3);

		a_spDrawI->drawBox(backbox,*a_pBorderColor);
	}

	if(a_pBackground)
	{
		SpatialVector rect[2];
		rect[0]=SpatialVector(
				corner[0]-a_margin-0.375,
				corner[1]-a_margin-1.375);
		rect[1]=rect[0]+SpatialVector(
				chars*characterWidth+2*a_margin-1,
				textHeight+2*a_margin+2);

		a_spDrawI->drawRectangles(rect,2,false,a_pBackground);

		rect[0]=SpatialVector(
				corner[0]+(chars-1)*characterWidth-1.375,
				corner[1]-0.5*textHeight-a_margin-1.375);
		rect[1]=rect[0]+SpatialVector(
				characterWidth+a_margin,
				2*textHeight+2*a_margin+2);

		a_spDrawI->drawRectangles(rect,2,false,a_pBackground);
	}

	sp<ViewI> spView=a_spDrawI->view();
	spView->setScissor(&scissor1);

	I32 charCount=0;
	BWORD first=TRUE;
	Real drop(0);
	Real keepDrop(0);
	Real place=pow(10.0,a_precision);
	while(charCount<a_maxChars && (place<value || place<2.0))
	{
		const I32 digit=value/(0.999999*place)-I32(value/(9.99999*place))*10;
		const Real part=value/(0.0999999*place)-I32(value/(0.999999*place))*10;
		const BWORD ending=(place>0.2 && place*10.0>=value);

		if(first)
		{
			drop=0.1*part;
			keepDrop=(!digit)? drop: 0.0;
		}
		else if(digit)
		{
			keepDrop=0;
		}

//		feLog("%d place %.10G part %.6G drop %.6G ending %d\n",
//				digit,place,part,drop,ending);

		const BWORD nearZero=(fabs(value)/place<0.5);
		const Real sign=(negative? -1.0: 1.0);
		const Real offset=-textHeight*drop*sign;
		const Real keepOffset=-textHeight*(keepDrop*sign+1);
		const BWORD useKeepOffset=
				(keepDrop>0.0 && !digit && fabs(place-1.0)<1e-3);

		if(digit<9)
		{
			drop=0.0;
		}

		String text;

		text.sPrintf("%d",nearZero? 1: (digit+9)%10);
		drawLabel(a_spDrawI,location+
				SpatialVector(0.0,offset-sign*textHeight),
				text,FALSE,0,a_textColor,NULL,a_pBackground);

		text.sPrintf("%d",digit);
		drawLabel(a_spDrawI,location+
				SpatialVector(0.0,offset),
				text,FALSE,0,a_textColor,NULL,a_pBackground);

		text.sPrintf("%d",nearZero? 1: (digit+1)%10);
		drawLabel(a_spDrawI,location+
				SpatialVector(0.0,offset+sign*textHeight),
				text,FALSE,0,a_textColor,NULL,a_pBackground);

		if(first)
		{
			text.sPrintf("%d",(digit+2)%10);
			drawLabel(a_spDrawI,location+
					SpatialVector(0.0,offset+2.0*sign*textHeight),
					text,FALSE,0,a_textColor,NULL,a_pBackground);
		}

		if(first)
		{
			scissor1=scissor2;
			spView->setScissor(&scissor1);
		}

		location[0]-=characterWidth;

		if(ending && negative && (digit || charCount))
		{
			drawLabel(a_spDrawI,location+
					SpatialVector(0.0,
					charCount? (useKeepOffset? keepOffset: 0.0): offset),
					"-",FALSE,0,a_textColor,NULL,a_pBackground);
		}

		if(ending && digit==9)
		{
			drawLabel(a_spDrawI,location+
					SpatialVector(0.0,offset+sign*textHeight),
					"1",FALSE,0,a_textColor,NULL,a_pBackground);

			if(negative)
			{
				drawLabel(a_spDrawI,location+
						SpatialVector(0.0,offset),
						"-",FALSE,0,a_textColor,NULL,a_pBackground);
			}

			location[0]-=characterWidth;
		}

		if(fabs(place-0.1)<1e-3)
		{
			drawLabel(a_spDrawI,location,
					".",FALSE,0,a_textColor,NULL,a_pBackground);
			location[0]-=characterWidth;
		}

		if(ending && negative && !charCount)
		{
			drawLabel(a_spDrawI,location+
					SpatialVector(0.0,offset-textHeight),
					"-",FALSE,0,a_textColor,NULL,a_pBackground);
		}

		place*=10.0;
		first=FALSE;
		charCount++;
	}

	spView->setScissor(NULL);
}

//* static
void OperateCommon::drawPicker(sp<DrawI> a_spDrawI,
	const SpatialVector& a_point,const SpatialVector& a_up,
	const SpatialVector& a_normal,Real a_scale,I32 a_depth,
	const Color* a_pColor)
{
	SpatialTransform transform;
	makeFrameNormalY(transform,a_point,a_up,a_normal);
	rotate(transform,-90.0*degToRad,e_xAxis);

	SpatialVector scale(a_scale,a_scale,a_scale);

//	a_spDrawI->drawCircle(transform,&scale,*a_pColor);
	//* HACK arc forces wire
	a_spDrawI->drawArc(transform,&scale,0.0,2.0*fe::pi,*a_pColor);

	for(I32 m=1;m<=a_depth;m++)
	{
		SpatialVector line[4];
		line[0]=SpatialVector(-1.0-0.5*m,-0.5);
		line[1]=SpatialVector(-1.0-0.5*m,0.5);
		line[2]=SpatialVector(1.0+0.5*m,-0.5);
		line[3]=SpatialVector(1.0+0.5*m,0.5);
		a_spDrawI->drawTransformedLines(transform,&scale,line,NULL,4,
				DrawI::e_discrete,false,a_pColor);
	}
}

//* static
void OperateCommon::drawImpact(sp<DrawI> a_spDrawI,
	sp<SurfaceTriangles::Impact> a_spTriImpact,
	Real a_inset,const Color* a_pColor)
{
	drawInsetTriangle(a_spDrawI,
			a_spTriImpact->vertex0(),
			a_spTriImpact->vertex1(),
			a_spTriImpact->vertex2(),
			a_inset,a_pColor);
}

//* static
void OperateCommon::drawInsetTriangle(sp<DrawI> a_spDrawI,
	const SpatialVector& a_vertex0,
	const SpatialVector& a_vertex1,
	const SpatialVector& a_vertex2,
	Real a_inset,const Color* a_pColor)
{
	const Real scale=1.0-2.0*a_inset;

	SpatialVector line[4];
	line[0]=scale*a_vertex0+a_inset*a_vertex1+a_inset*a_vertex2;
	line[1]=a_inset*a_vertex0+scale*a_vertex1+a_inset*a_vertex2;
	line[2]=a_inset*a_vertex0+a_inset*a_vertex1+scale*a_vertex2;
	line[3]=line[0];

	a_spDrawI->drawLines(line,NULL,4,DrawI::e_strip,false,a_pColor);
}

//* static
void OperateCommon::highlightCurve(sp<ViewI> a_spView,
	sp<DrawI> a_spDraw,sp<SurfaceAccessorI> a_spVertices,
	I32 a_primitiveIndex,I32 a_vertexIndex,
	const Color* a_pLineColor,const Color* a_pPointColor,
	const Color* a_pPointHighlight)
{
	const I32 subCount=a_spVertices->subCount(a_primitiveIndex);
	SpatialVector* line=new SpatialVector[subCount];

	for(I32 subIndex=0;subIndex<subCount;subIndex++)
	{
		const SpatialVector point=
				a_spVertices->spatialVector(a_primitiveIndex,subIndex);

		const Vector2i projected=
				a_spView->project(point,ViewI::e_perspective);

		line[subIndex]=projected;	//* NOTE z defaults to 0.0
	}

	if(a_pLineColor)
	{
		a_spDraw->drawLines(line,NULL,subCount,
				DrawI::e_strip,FALSE,a_pLineColor);
	}

	if(a_pPointColor)
	{
		for(I32 subIndex=0;subIndex<subCount;subIndex++)
		{
			line[subIndex][2]=0.5;
		}

		a_spDraw->drawPoints(line,NULL,subCount,FALSE,a_pPointColor);
	}

	if(a_vertexIndex>=0 && a_pPointHighlight)
	{
		const SpatialVector point=
				a_spVertices->spatialVector(a_primitiveIndex,a_vertexIndex);
		drawDot(a_spView,a_spDraw,point,5.0,*a_pPointHighlight);
	}

	delete[] line;
}

//* static
void OperateCommon::drawDot(sp<ViewI> a_spView,
	sp<DrawI> a_spDraw,SpatialVector a_point,Real a_radius,const Color a_color)
{
		const Vector2i projected=
				a_spView->project(a_point,ViewI::e_perspective);
		SpatialVector center(projected[0],projected[1],0.6);

		SpatialTransform transform;
		setIdentity(transform);
		translate(transform,center);

		const SpatialVector scale(a_radius,a_radius,a_radius);
		a_spDraw->drawCircle(transform,&scale,a_color);
}

//* static
void OperateCommon::drawSquare(sp<DrawI>& a_rspDraw,
	const SpatialVector& a_rPoint,Real a_rimSize,BWORD a_bevel,
	const Color& a_rColorCenter,const Color& a_rColorRim,
	const Color* a_pColorShadow,const Color* a_pColorSparkle)
{
	sp<ViewI> spView=a_rspDraw->view();

	const Vector2i projected=spView->project(a_rPoint,ViewI::e_perspective);
	const Vector2i rim(a_rimSize,a_rimSize);

	Box2i box=projected-rim;
	box.size()=2.0*rim+Vector2i(1,1);

	if(a_rColorCenter[3]>0.0)
	{
		const I32 lineWidth=a_rspDraw->drawMode()->lineWidth();

		SpatialVector corner[2];
		corner[0]=SpatialVector(box)+SpatialVector(lineWidth,lineWidth);
		corner[1]=SpatialVector(box)-SpatialVector(lineWidth,lineWidth)+
				SpatialVector(box.size());

		a_rspDraw->drawRectangles(corner,2,FALSE,&a_rColorCenter);
	}

	if(a_rColorRim[3]>0.0)
	{
		if(a_bevel)
		{
			const Color dark(0.0,0.0,0.0,0.5);
			Box2i box2=box;
			box2[0]+=1.0;
			box2[1]-=1.0;
			a_rspDraw->drawBox(box2,dark);
		}

		a_rspDraw->drawBox(box,a_rColorRim);

		if(a_bevel)
		{
			const Color light(
					fe::minimum(1.0,0.3+1.3*a_rColorRim[0]),
					fe::minimum(1.0,0.3+1.3*a_rColorRim[1]),
					fe::minimum(1.0,0.3+1.3*a_rColorRim[2]));
			Box2i box2=box;
			box2[0]-=1.0;
			box2[1]+=1.0;
			a_rspDraw->drawBox(box2,light);
		}
	}

	if(a_pColorShadow && (*a_pColorShadow)[3]>0.0)
	{
		SpatialVector glint=SpatialVector(box)+
				SpatialVector(box.size()[0]+1.0,0.0,1.0);
		a_rspDraw->drawPoints(&glint,NULL,1,FALSE,a_pColorShadow);
	}

	if(a_pColorSparkle && (*a_pColorSparkle)[3]>0.0)
	{
		SpatialVector glint=SpatialVector(box)+
				SpatialVector(0.0,box.size()[1]+1.0,1.0);
		a_rspDraw->drawPoints(&glint,NULL,1,FALSE,a_pColorSparkle);
	}
}

//* static
void OperateCommon::drawTube(sp<DrawI>& a_rspDraw,
	const SpatialVector* a_point,
	const SpatialVector* a_normal,
	const Real* a_radius,
	U32 a_pointCount,CurveMode a_curveMode,
	BWORD a_multicolor,const Color *a_color,
	const SpatialVector& a_rTowardCamera)
{
	const Color black(0.0,0.0,0.0,1.0);
	const Color white(1.0,1.0,1.0,1.0);

	const U32 slices=12;	//* TODO param

	const SpatialVector offset=1e-2*a_rTowardCamera;

	if(a_curveMode==e_lines)
	{
		//* overlapping wide lines

		SpatialVector* line=new SpatialVector[a_pointCount];
		Color* color=new Color[a_pointCount];
		Color* glint=new Color[a_pointCount];

		for(U32 pointIndex=0;pointIndex<a_pointCount;pointIndex++)
		{
			line[pointIndex]=a_point[pointIndex]+offset;

			if(a_color)
			{
				color[pointIndex]=a_color[a_multicolor? pointIndex: 0];
			}
			else
			{
				color[pointIndex]=white;
			}

			glint[pointIndex]=color[pointIndex];

			//* TODO restore
//			glint[pointIndex]+=
//					(1.0-darken)*(white-color[pointIndex]);
			glint[pointIndex]=white;

			glint[pointIndex][3]=1.0;
		}

		for(U32 pass=0;pass<3;pass++)
		{
			//* TODO restore line changing
//			a_rspDraw->pushDrawMode(m_spWideLine[pass*2]);

			a_rspDraw->drawLines(line,NULL,a_pointCount,
					DrawI::e_strip,(pass!=2),
					pass? (pass==2? &black: color): glint);

//			a_rspDraw->popDrawMode();
		}

		delete[] glint;
		delete[] color;
		delete[] line;

		return;
	}

	//* camera-facing tri-strip
	//* continuous tubes
	//* disjoint cylinders

	const U32 vertexCount=(a_curveMode==e_panels)? a_pointCount*2: slices*2+2;
	SpatialVector* vertex=new SpatialVector[vertexCount];
	SpatialVector* vertex2=new SpatialVector[vertexCount];
	SpatialVector* vertexNormal=new SpatialVector[vertexCount];
	SpatialVector* vertexNormal2=new SpatialVector[vertexCount];
	Color* vertexColor=new Color[vertexCount];

	SpatialVector lastPoint=a_point[(a_curveMode==e_panels)? 1: 0];
	for(U32 pointIndex=(a_curveMode==e_panels? 0: 1);
			pointIndex<a_pointCount;pointIndex++)
	{
		const SpatialVector point=a_point[pointIndex];

		const Color color=a_color? a_color[a_multicolor? pointIndex: 0]: white;
		const Real radius=a_radius? a_radius[pointIndex]: 1.0;

		if(a_curveMode==e_cylinders)
		{
			a_rspDraw->drawCylinder(lastPoint,point,radius,radius,color,slices);
		}
		else
		{
			SpatialVector normalY(0.0,1.0,0.0);
			if(a_curveMode==e_panels)
			{
				//* TODO maybe faster in 2D screen space

				normalY=a_rTowardCamera;
			}
			else if(a_normal)
			{
				normalY=a_normal[pointIndex];
			}

			const SpatialVector delta=(point-lastPoint)*(pointIndex? 1: -1);
			const Real segLength=magnitude(delta);
			const SpatialVector tangentX=delta*
					(segLength>0.0? 1.0/segLength: 1.0);
			const SpatialVector offsetPoint=point+offset;

			SpatialTransform segmentFrame;
			makeFrameTangentX(segmentFrame,offsetPoint,tangentX,normalY);
			const SpatialVector sideZ=segmentFrame.column(2);

			if(a_curveMode==e_panels)
			{
				//* TODO use original uncorrected normals if provided

				const SpatialVector renormal=a_normal?
						a_normal[pointIndex]: segmentFrame.column(1);

				U32 vertexIndex=pointIndex*2;
				vertexColor[vertexIndex]=color;

//								transformVector(segmentFrame,
//										-v0,vertex[vertexIndex]);
				vertex[vertexIndex]=offsetPoint-radius*sideZ;

//								rotateVector(segmentFrame,
//										n0,vertexNormal[vertexIndex]);
				vertexNormal[vertexIndex]=sideZ;

				vertex2[vertexIndex]=offsetPoint;

//								rotateVector(segmentFrame,
//										n1,vertexNormal2[vertexIndex]);
				vertexNormal2[vertexIndex]=renormal;

				vertexIndex++;
				vertexColor[vertexIndex]=color;

				vertex[vertexIndex]=offsetPoint;

				vertexNormal[vertexIndex]=renormal;

//								transformVector(segmentFrame,
//										v0,vertex2[vertexIndex]);
				vertex2[vertexIndex]=offsetPoint+radius*sideZ;

//								vertexNormal2[vertexIndex]=
//										-vertexNormal[vertexIndex-1];
				vertexNormal2[vertexIndex]= -sideZ;
/*
				feLog("panel %d/%d %d/%d  %s  %s n %s  %s\n",
						primitiveIndex,primitiveCount,
						pointIndex,a_pointCount,
						c_print(vertex[vertexIndex-1]),
						c_print(vertex[vertexIndex]),
						c_print(vertexNormal[vertexIndex-1]),
						c_print(vertexNormal[vertexIndex]));
*/
			}
			else
			{
				const BWORD copying=(pointIndex>1);

				if(copying)
				{
					for(U32 vertexIndex=0;vertexIndex<vertexCount;
							vertexIndex+=2)
					{
						vertex[vertexIndex]=vertex[vertexIndex+1];
						vertexNormal[vertexIndex]=vertexNormal[vertexIndex+1];
						vertexColor[vertexIndex]=vertexColor[vertexIndex+1];
					}
				}

				for(U32 slice=0;slice<slices;slice++)
				{
					const Real angle=2.0*M_PI*slice/(Real)(slices);
					vertexNormal[slice*2+1]=
							SpatialVector(0.0,sin(angle),cos(angle));
					vertex[slice*2+1]=radius*vertexNormal[slice*2+1];
				}
				vertex[slices*2+1]=vertex[1];
				vertexNormal[slices*2+1]=vertexNormal[1];

				if(!copying)
				{
					const Real firstRadius=a_radius? a_radius[0]: 1.0;
					const Real radiusScale=radius>0.0? firstRadius/radius: 1.0;
					for(U32 slice=0;slice<slices;slice++)
					{
						vertexNormal[slice*2]=vertexNormal[slice*2+1];
						vertex[slice*2]=radiusScale*vertex[slice*2+1]-
								SpatialVector(segLength,0.0,0.0);
					}
					vertex[slices*2]=vertex[0];
					vertexNormal[slices*2]=vertexNormal[0];
				}

				for(U32 vertexIndex=copying;vertexIndex<vertexCount;
						vertexIndex+=(1+copying))
				{
					transformVector(segmentFrame,
							vertex[vertexIndex],
							vertex[vertexIndex]);
					rotateVector(segmentFrame,
							vertexNormal[vertexIndex],
							vertexNormal[vertexIndex]);

					vertexColor[vertexIndex]=color;
				}

				a_rspDraw->drawTriangles(vertex,vertexNormal,NULL,vertexCount,
						DrawI::e_strip,TRUE,vertexColor);
			}
		}

		lastPoint=point;
	}
	if(a_curveMode==e_panels)
	{
		a_rspDraw->drawTriangles(vertex,vertexNormal,
				NULL,vertexCount,DrawI::e_strip,
				TRUE,vertexColor);
		a_rspDraw->drawTriangles(vertex2,vertexNormal2,
				NULL,vertexCount,DrawI::e_strip,
				TRUE,vertexColor);
	}

	delete[] vertexColor;
	delete[] vertexNormal2;
	delete[] vertexNormal;
	delete[] vertex2;
	delete[] vertex;
}

//* static
I32 OperateCommon::nearestPointIndex(
	const sp<SurfaceAccessorI> a_spPrimitiveVertices,
	sp<SurfaceCurves::Impact>& a_rspCurveImpact)
{
	const SpatialBary bary=a_rspCurveImpact->barycenter();
	const I32 primitiveIndex=a_rspCurveImpact->primitiveIndex();

	const I32 subCount=a_spPrimitiveVertices->subCount(primitiveIndex);
	const I32 vertexIndex=bary[0]*(subCount-1)+0.5;

	return a_spPrimitiveVertices->integer(primitiveIndex,vertexIndex);
}

//* static
I32 OperateCommon::nearestTriVertexIndex(
	sp<SurfaceTriangles::Impact>& a_rspTriImpact)
{
	const I32 pointIndex0=a_rspTriImpact->pointIndex0();
	const I32 pointIndex1=a_rspTriImpact->pointIndex1();
	const I32 pointIndex2=a_rspTriImpact->pointIndex2();

	const SpatialBary bary=a_rspTriImpact->barycenter();

	SpatialVector bary3=bary;
	bary3[0]=(pointIndex0<0.0)? 0.0: bary3[0];
	bary3[1]=(pointIndex1<0.0)? 0.0: bary3[1];
	bary3[2]=(pointIndex2<0.0)? 0.0: bary3[2];

	if(bary3[0]>bary3[1])
	{
		return (bary3[0]>bary3[2])? 0: 2;
	}
	return (bary3[1]>bary3[2])? 1: 2;
}

//* static
I32 OperateCommon::nearestPointIndex(
	sp<SurfaceTriangles::Impact>& a_rspTriImpact)
{
	const I32 triVertexIndex=nearestTriVertexIndex(a_rspTriImpact);

	if(triVertexIndex==0)
	{
		return a_rspTriImpact->pointIndex0();
	}
	else if (triVertexIndex==1)
	{
		return a_rspTriImpact->pointIndex1();
	}
	return a_rspTriImpact->pointIndex2();
}

//* static
SpatialVector OperateCommon::nearestVertexPoint(
	sp<SurfaceTriangles::Impact>& a_rspTriImpact)
{
	const I32 triVertexIndex=nearestTriVertexIndex(a_rspTriImpact);

	if(triVertexIndex==0)
	{
		return a_rspTriImpact->vertex0();
	}
	else if (triVertexIndex==1)
	{
		return a_rspTriImpact->vertex1();
	}
	return a_rspTriImpact->vertex2();
}


Real OperateCommon::nearestPointAlongCurve(const SpatialVector a_point,
	const sp<SurfaceAccessorI> a_spPrimitiveVertices,
	const I32 a_primitiveIndex)
{
	const U32 subCount=a_spPrimitiveVertices->subCount(a_primitiveIndex);
	if(subCount<2)
	{
		return 0.0;
	}

#if TRUE
	//* TODO return 'unit along' versus 'fractional CV'

	SpatialVector* vertexArray=new SpatialVector[subCount];
	for(U32 subIndex=0;subIndex<subCount;subIndex++)
	{
		vertexArray[subIndex]=a_spPrimitiveVertices->spatialVector(
				a_primitiveIndex,subIndex);
	}

	const Real radius=0.0;
	SpatialVector direction;
	Real along=0.0;
	SpatialVector intersection;

//	const Real distance=
	PointCurveNearest<Real>::solve(vertexArray,subCount,
			radius,a_point,direction,along,intersection);

	delete[] vertexArray;

	return (subCount-1)*along;
#else

	Real result=0.0;
	Real minDist=0.0;

	SpatialVector lastCV=
			a_spPrimitiveVertices->spatialVector(a_primitiveIndex,0);
	for(U32 subIndex=1;subIndex<subCount;subIndex++)
	{
		const SpatialVector cv=a_spPrimitiveVertices->spatialVector(
				a_primitiveIndex,subIndex);
		const SpatialVector segment=cv-lastCV;
		const Real segLength=magnitude(segment);
		const Real safeLength=segLength>0.0? segLength: 1.0;
		const SpatialVector unitSeg=segment/safeLength;
		const SpatialVector away=a_point-lastCV;
		const Real along=dot(unitSeg,away);
		const Real beyond=
				along>segLength? along-segLength: (along<0.0? -along: 0.0);
		const SpatialVector aside=away-along*unitSeg;
		const Real astray=magnitude(aside);
		const Real dist=sqrt(astray*astray+beyond*beyond);

		if(subIndex==1 || dist<minDist)
		{
			const Real unitAlong=along/safeLength;

			minDist=dist;
			result=subIndex-1+unitAlong;
		}

		lastCV=cv;
	}

	return result;
#endif
}

//* static
Real OperateCommon::fractionAtLength(
	const sp<SurfaceAccessorI> a_spPrimitiveVertices,
	const I32 a_primitiveIndex,Real a_length)
{
	const U32 subCount=a_spPrimitiveVertices->subCount(a_primitiveIndex);
	if(subCount<2)
	{
		return 0.0;
	}

	Real cumulative=0.0;
	Real fraction=1.0;

	SpatialVector lastCV=
			a_spPrimitiveVertices->spatialVector(a_primitiveIndex,0);
	U32 subIndex=1;
	for(;subIndex<subCount;subIndex++)
	{
		const SpatialVector cv=a_spPrimitiveVertices->spatialVector(
				a_primitiveIndex,subIndex);
		const SpatialVector segment=cv-lastCV;
		const Real segLength=magnitude(segment);

		cumulative+=segLength;

		if(cumulative>a_length)
		{
			const Real lastLength=cumulative-segLength;
			const Real between=(a_length-lastLength)/segLength;

			fraction=(subIndex-1.0+between)/(subCount-1.0);
			break;
		}

		lastCV=cv;
	}
	if(subIndex==subCount && cumulative>0.0)
	{
		fraction=a_length/cumulative;
	}

//	feLog("OperateCommon::fractionAtLength"
//			" length %.6G fraction %.6G vs %.6G \n",
//			a_length,fraction,a_length/25.0);

	FEASSERT(fraction>=0.0);

	return fraction;
}

//* static
Real OperateCommon::lengthAtFraction(
	const sp<SurfaceAccessorI> a_spPrimitiveVertices,
	const I32 a_primitiveIndex,Real a_fraction)
{
	if(a_fraction<=0.0)
	{
		return 0.0;
	}

	const U32 subCount=a_spPrimitiveVertices->subCount(a_primitiveIndex);
	if(subCount<2)
	{
		return 0.0;
	}

	const Real subCount1=subCount-1.0;
	const Real realCV=a_fraction*subCount1;
	const U32 stopAt=realCV;
	const Real between=realCV-stopAt;

	Real length=0.0;

	SpatialVector lastCV=
			a_spPrimitiveVertices->spatialVector(a_primitiveIndex,0);
	U32 subIndex=1;
	for(;subIndex<subCount;subIndex++)
	{
		const SpatialVector cv=a_spPrimitiveVertices->spatialVector(
				a_primitiveIndex,subIndex);
		const SpatialVector segment=cv-lastCV;
		const Real segLength=magnitude(segment);

		if(subIndex>stopAt)
		{
			length+=between*segLength;
			break;
		}

		length+=segLength;
		lastCV=cv;
	}
	if(subIndex==subCount)
	{
		FEASSERT(a_fraction>0.999);
		length*=a_fraction;
	}

//	feLog("OperateCommon::lengthAtFraction"
//			" fraction %.6G length %.6G vs %.6G \n",
//			a_fraction,length,a_fraction*25.0);
//	feLog("  subCount %d subCount1 %.6G realCV %.6G stopAt %d between %.6G\n",
//			subCount,subCount1,realCV,stopAt,between);

	FEASSERT(length>=0.0);

	return length;
}


SpatialVector OperateCommon::evaluateAtFraction(
	const sp<SurfaceAccessorI> a_spPrimitiveVertices,
	const I32 a_primitiveIndex,Real a_fraction,U32 a_derivitive)
{
	const U32 subCount=a_spPrimitiveVertices->subCount(a_primitiveIndex);
	if(subCount<2)
	{
		return SpatialVector(0.0,0.0,0.0);
	}

	const Real subCount1=subCount-1.0;
	const Real realCV=a_fraction*subCount1;
	const U32 stopAt=realCV;
	const Real between=realCV-stopAt;

	const U32 sub0=stopAt>0? (stopAt<subCount+1? stopAt-1: subCount-1): 0;
	const U32 sub1=stopAt<subCount? stopAt: subCount-1;
	const U32 sub2=stopAt<subCount-1? stopAt+1: subCount-1;
	const U32 sub3=stopAt<subCount-2? stopAt+2: subCount-1;

	const SpatialVector p0=a_spPrimitiveVertices->spatialVector(
			a_primitiveIndex,sub0);
	const SpatialVector p1=a_spPrimitiveVertices->spatialVector(
			a_primitiveIndex,sub1);
	const SpatialVector p2=a_spPrimitiveVertices->spatialVector(
			a_primitiveIndex,sub2);
	const SpatialVector p3=a_spPrimitiveVertices->spatialVector(
			a_primitiveIndex,sub3);

	SpatialVector result;

	switch(a_derivitive)
	{
		case 0:
			result=Spline<SpatialVector,Real>::Cardinal2D(
					between,p0,p1,p2,p3);
			break;
		case 1:
			result=Spline<SpatialVector,Real>::Cardinal2D_d1(
					between,p0,p1,p2,p3);
			break;
		case 2:
			result=Spline<SpatialVector,Real>::Cardinal2D_d2(
					between,p0,p1,p2,p3);
			break;
		default:
			set(result);
			break;
	}

//	feLog("OperateCommon::evaluateAtFraction fraction %.6G deriv %d\n",
//			a_fraction,a_derivitive);
//	feLog("  subCount %d subCount1 %.6G realCV %.6G stopAt %d between %.6G\n",
//			subCount,subCount1,realCV,stopAt,between);
//	feLog("  p0 %s\n",c_print(p0));
//	feLog("  p1 %s\n",c_print(p1));
//	feLog("  p2 %s\n",c_print(p2));
//	feLog("  p3 %s\n",c_print(p3));
//	feLog("  result %s\n",c_print(result));

	return result;
}

//* static
void OperateCommon::drawTubeAxes(sp<DrawI>& a_rspDraw,
	const SpatialTransform& a_transform,Real a_scale,
	Real a_alpha,const Array<Real>& a_profile,
	CurveMode a_curveMode)
{
	const Color rgb[3]=
	{
		Color(1.0,0.7,0.7,a_alpha),
		Color(0.7,1.0,0.7,a_alpha),
		Color(0.7,0.7,1.0,a_alpha)
	};

	const SpatialVector towardCamera(0.0,0.0,1.0);

	const U32 resolution=a_profile.size();

	SpatialVector* pointArray=new SpatialVector[resolution];
	SpatialVector* normalArray=new SpatialVector[resolution];
	Real* radiusArray=new Real[resolution];
	Color* colorArray=new Color[resolution];

	Real maxRadius=1e-6;

	for(U32 axis=0;axis<3;axis++)
	{
		for(U32 m=0;m<resolution;m++)
		{
			const Real fraction=m/(resolution-1.0);

			set(pointArray[m]);
			pointArray[m][axis]=a_scale*fraction;
			transformVector(a_transform,pointArray[m],pointArray[m]);

			set(normalArray[m]);
			normalArray[m][(axis+1)%3]=1.0;
			rotateVector(a_transform,normalArray[m],normalArray[m]);

			radiusArray[m]=a_profile[m];
			colorArray[m]=rgb[axis];

			if(maxRadius<radiusArray[m])
			{
				maxRadius=radiusArray[m];
			}
		}

		//* darken narrow areas
		for(U32 m=0;m<resolution;m++)
		{
			colorArray[m]*=pow(radiusArray[m]/maxRadius,0.5);
			colorArray[m][3]=a_alpha;
		}

		const BWORD multicolor=TRUE;
		drawTube(a_rspDraw,pointArray,normalArray,radiusArray,
				resolution,a_curveMode,multicolor,colorArray,towardCamera);
	}

	delete[] colorArray;
	delete[] radiusArray;
	delete[] normalArray;
	delete[] pointArray;
}

//* static
void OperateCommon::drawAnchor(sp<DrawI>& a_rspDraw,
	SpatialTransform a_transform,Real a_scale,U32 a_resolution,Color a_color)
{
	//* TODO args
	const OperateCommon::CurveMode curveMode=OperateCommon::e_tube;
	const SpatialVector towardCamera(0.0,0.0,1.0);

	const Color white(1.0,1.0,1.0,a_color[3]);

	SpatialVector* pointArray=new SpatialVector[a_resolution];
	SpatialVector* normalArray=new SpatialVector[a_resolution];
	Real* radiusArray=new Real[a_resolution];
	Color* colorArray=new Color[a_resolution];

	for(U32 m=0;m<a_resolution;m++)
	{
		const Real fraction=m/(a_resolution-1.0);

		set(pointArray[m],0.0,a_scale*fraction,0.0);
		transformVector(a_transform,pointArray[m],pointArray[m]);

		set(normalArray[m],1.0,0.0,0.0);
		rotateVector(a_transform,normalArray[m],normalArray[m]);

		colorArray[m]=a_color*fraction;
		colorArray[m][3]=a_color[3];

		radiusArray[m]=(m==a_resolution-1)?
				0.0: 0.5*a_scale*pow(0.5+0.5*fraction,4.0);
	}

	colorArray[a_resolution-1]=white;

	const BWORD multicolor=TRUE;
	drawTube(a_rspDraw,pointArray,normalArray,radiusArray,
			a_resolution,curveMode,multicolor,colorArray,towardCamera);

	delete[] colorArray;
	delete[] radiusArray;
	delete[] normalArray;
	delete[] pointArray;
}

//* static
void OperateCommon::drawCrossSection(sp<DrawI>& a_rspDraw,
	SpatialVector a_p0,SpatialVector a_p1,SpatialVector a_p2,
	SpatialVector a_planeCenter,SpatialVector a_planeNormal,
	Color a_color)
{
	const SpatialVector diff0=a_p0-a_planeCenter;
	const SpatialVector diff1=a_p1-a_planeCenter;
	const SpatialVector diff2=a_p2-a_planeCenter;

	const Real dot0=dot(diff0,a_planeNormal);
	const Real dot1=dot(diff1,a_planeNormal);
	const Real dot2=dot(diff2,a_planeNormal);

	const BWORD above0=(dot0>=0.0);
	const BWORD above1=(dot1>=0.0);
	const BWORD above2=(dot2>=0.0);

	const I32 aboves=above0+above1+above2;

	if(!aboves || aboves==3)
	{
		//* all above or all below -> no contact
		return;
	}

	SpatialVector pointA;
	SpatialVector pointB;
	SpatialVector pointC;	//* corner

	if(above0!=above1 && above0!=above2)
	{
		pointC=a_p0;

		pointA=a_p1;
		pointB=a_p2;
	}
	else if(above0!=above1 && above1!=above2)
	{
		pointC=a_p1;

		pointA=a_p0;
		pointB=a_p2;
	}
	else
	{
		pointC=a_p2;

		pointA=a_p0;
		pointB=a_p1;
	}

	const SpatialVector unitCA=unitSafe(pointA-pointC);
	const SpatialVector unitCB=unitSafe(pointB-pointC);

	const Real distCA=RayPlaneIntersect<Real>::solve(
			a_planeCenter,a_planeNormal,-1.0,pointC,unitCA);
	const Real distCB=RayPlaneIntersect<Real>::solve(
			a_planeCenter,a_planeNormal,-1.0,pointC,unitCB);

	SpatialVector line[2];
	line[0]=pointC+distCA*unitCA;
	line[1]=pointC+distCB*unitCB;

	a_rspDraw->drawLines(line,NULL,2,DrawI::e_strip,false,&a_color);
}

void OperateCommon::drawCrossSection(sp<DrawI>& a_rspDraw,
	sp<SurfaceAccessorI> a_spVertices,
	SpatialVector a_planeCenter,
	SpatialVector a_planeNormal,
	Color a_color)
{
	if(a_spVertices.isNull())
	{
		return;
	}

	const U32 primitiveCount=a_spVertices->count();
	for(U32 primitiveIndex=0;primitiveIndex<primitiveCount;
			primitiveIndex++)
	{
		const U32 subCount=a_spVertices->subCount(primitiveIndex);
		if(subCount<3 || subCount>4)
		{
			continue;
		}

		const SpatialVector p0=
				a_spVertices->spatialVector(primitiveIndex,0);
		const SpatialVector p1=
				a_spVertices->spatialVector(primitiveIndex,1);
		const SpatialVector p2=
				a_spVertices->spatialVector(primitiveIndex,2);

		OperateCommon::drawCrossSection(a_rspDraw,p0,p1,p2,
				a_planeCenter,a_planeNormal,a_color);

		if(subCount==3)
		{
			continue;
		}

		const SpatialVector p3=
				a_spVertices->spatialVector(primitiveIndex,3);

		OperateCommon::drawCrossSection(a_rspDraw,p1,p2,p3,
				a_planeCenter,a_planeNormal,a_color);
	}
}

//* static
void OperateCommon::drawOutline(sp<DrawI> a_spDrawI,
	sp<SurfaceAccessibleI> a_spSurfaceAccessibleI,
	String a_partAttr,String a_partName,
	SpatialVector a_cameraPos,Color a_color)
{
	std::map<U64,Edge> edgeMap;

	createEdges(a_spSurfaceAccessibleI,a_partAttr,edgeMap);

	drawOutline(a_spDrawI,edgeMap,a_partName,a_cameraPos,a_color);
}

//* static
void OperateCommon::createEdges(
	sp<SurfaceAccessibleI> a_spSurfaceAccessibleI,
	String a_partAttr,std::map<U64,Edge>& a_rEdgeMap)
{
	a_rEdgeMap.clear();

	sp<SurfaceAccessorI> spPrimitiveVertices=
			a_spSurfaceAccessibleI->accessor(
			SurfaceAccessibleI::e_primitive,
			SurfaceAccessibleI::e_vertices,
			SurfaceAccessibleI::e_refuseMissing);
	if(spPrimitiveVertices.isNull() || !spPrimitiveVertices->count())
	{
		feLog("OperateCommon::createEdges no vertices\n");
		return;
	}

	//* NOTE need face normals for sharp facets
	sp<SurfaceAccessorI> spPrimitiveNormal=
			a_spSurfaceAccessibleI->accessor(
			SurfaceAccessibleI::e_primitive,
			SurfaceAccessibleI::e_normal,
			SurfaceAccessibleI::e_refuseMissing);
	if(spPrimitiveNormal.isValid() && !spPrimitiveNormal->count())
	{
		spPrimitiveNormal=NULL;
	}

	sp<SurfaceAccessorI> spPrimitivePart;
	if(!a_partAttr.empty())
	{
		spPrimitivePart=a_spSurfaceAccessibleI->accessor(
				SurfaceAccessibleI::e_primitive,
				a_partAttr,
				SurfaceAccessibleI::e_refuseMissing);
	}
	if(spPrimitivePart.isValid() && !spPrimitivePart->count())
	{
		spPrimitivePart=NULL;
	}

	I32 nonManifold=0;

	const I32 primitiveCount=spPrimitiveVertices->count();
	for(I32 primitiveIndex=0;primitiveIndex<primitiveCount;
			primitiveIndex++)
	{
		const I32 subCount=spPrimitiveVertices->subCount(primitiveIndex);
		if(subCount<3)
		{
			continue;
		}

		if(spPrimitiveNormal.isNull() && subCount>4)
		{
			continue;
		}

		String partName;
		if(spPrimitivePart.isValid())
		{
			partName=spPrimitivePart->string(primitiveIndex);
		}

		SpatialVector faceNormal(0.0,0.0,0.0);

		SpatialVector* position=new SpatialVector[subCount];
		I32* index=new I32[subCount];

		for(I32 subIndex=0;subIndex<subCount;subIndex++)
		{
			position[subIndex]=
					spPrimitiveVertices->spatialVector(primitiveIndex,subIndex);
			index[subIndex]=
					spPrimitiveVertices->integer(primitiveIndex,subIndex);
		}

		if(spPrimitiveNormal.isValid())
		{
			faceNormal=spPrimitiveNormal->spatialVector(primitiveIndex);
		}
		else if(subCount>2)
		{
			//* NOTE no need to normalize
			faceNormal=
					cross(position[2]-position[0],position[1]-position[0]);
		}

		if(magnitudeSquared(faceNormal)<1e-12)
		{
			delete[] index;
			delete[] position;
			continue;
		}

		for(I32 subIndex=0;subIndex<subCount;subIndex++)
		{
			const I32 subIndex1=(subIndex+1)%subCount;

			const I32 pointIndexA=index[subIndex];
			const I32 pointIndexB=index[subIndex1];

			const U64 edgeKey=(pointIndexA>pointIndexB)?
					(U64(pointIndexA)<<32)+pointIndexB:
					(U64(pointIndexB)<<32)+pointIndexA;

			Edge& rEdge=a_rEdgeMap[edgeKey];
			I32& rCount=rEdge.m_count;

			if(!rCount)
			{
				rEdge.m_pointA=position[subIndex];
				rEdge.m_pointB=position[subIndex1];
				rEdge.m_normal0=faceNormal;
				rEdge.m_part0=partName;
			}
			else if(rCount==1)
			{
				rEdge.m_normal1=faceNormal;
				rEdge.m_part1=partName;
			}
			else
			{
				const I32 messageLimit=16;
				if(nonManifold<messageLimit)
				{
					feLog("OperateCommon::createEdges"
							" non-manifold primitive %d\n",
							primitiveIndex);
				}
				else if(nonManifold==messageLimit)
				{
					feLog("OperateCommon::createEdges"
							" non-manifold primitives reached logging limit\n");
				}
				nonManifold++;
			}

			rCount++;
		}

		delete[] index;
		delete[] position;
	}

	if(nonManifold)
	{
		feLog("OperateCommon::createEdges"
				" %d non-manifold primitives\n",nonManifold);
	}
}

//* static
void OperateCommon::drawOutline(sp<DrawI> a_spDrawI,
	std::map<U64,Edge>& a_rEdgeMap,String a_partName,
	SpatialVector a_cameraPos,Color a_color)
{
	const I32 bufferSize=256;
	SpatialVector lineBuffer[bufferSize];
	I32 bufferIndex=0;

	const BWORD checkPartName=(!a_partName.empty());

	for(std::map<U64,Edge>::iterator it=a_rEdgeMap.begin();
			it!=a_rEdgeMap.end(); it++)
	{
		const Edge& rEdge=it->second;

		const String& part0=rEdge.m_part0;
		const String& part1=rEdge.m_part1;

		if(checkPartName && part0!=a_partName && part1!=a_partName)
		{
			continue;
		}

		const I32 count=rEdge.m_count;
		FEASSERT(count);

		const SpatialVector& pointA=rEdge.m_pointA;
		const SpatialVector& pointB=rEdge.m_pointB;

		if(count==2 && (!checkPartName || part0==part1))
		{
			const SpatialVector midpoint=0.5*(pointA+pointB);

			//* NOTE towardDir is not cameraDir

			//* NOTE no need to normalize
			const SpatialVector towardDir=midpoint-a_cameraPos;

			const SpatialVector& normal0=rEdge.m_normal0;
			const SpatialVector& normal1=rEdge.m_normal1;

			const Real towardDot0=dot(towardDir,normal0);
			const Real towardDot1=dot(towardDir,normal1);

			if((towardDot0<0.0) == (towardDot1<0.0))
			{
				continue;
			}
		}

		lineBuffer[bufferIndex++]=pointA;
		lineBuffer[bufferIndex++]=pointB;

		if(bufferIndex+2>bufferSize)
		{
			a_spDrawI->drawLines(lineBuffer,NULL,bufferIndex,
					DrawI::e_discrete,false,&a_color);
			bufferIndex=0;
		}
	}

	if(bufferIndex)
	{
		a_spDrawI->drawLines(lineBuffer,NULL,bufferIndex,
				DrawI::e_discrete,false,&a_color);
	}
}

//* static
Real OperateCommon::flameProfile(Real a_peakOffset,Real a_fraction)
{
	const Real maxOffset=0.9*a_fraction;	//* tweak
	Real offset=a_peakOffset*sin(M_PI*a_fraction);
	if(offset>maxOffset)
	{
		offset=maxOffset;
	}
	return sin(M_PI*(a_fraction-offset));
}

