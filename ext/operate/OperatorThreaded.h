/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __operate_OperatorThreaded_h__
#define __operate_OperatorThreaded_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Handler to move curves away from a collider

	@ingroup operate
*//***************************************************************************/
class FE_DL_EXPORT OperatorThreaded:
	public OperatorSurfaceCommon,
	public Initialize<OperatorThreaded>,
	virtual public WorkI
{
	public:

							OperatorThreaded(void)							{}
virtual						~OperatorThreaded(void)							{}

		void				initialize(void);

		using				OperatorSurfaceCommon::accessOutput;

		BWORD				accessOutput(
									sp<SurfaceAccessibleI>& a_rspAccessible,
									Record& a_rSignal,
									Message a_message=e_error);

							//* As WorkI
virtual	WorkI::Threading	threading(void)
							{
								FEASSERT(m_spThreadingState.isValid());
								return WorkI::Threading(
										m_spThreadingState->threading());
							}
virtual	void				setThreading(WorkI::Threading a_threading)
							{
								FEASSERT(m_spThreadingState.isValid());
								m_spThreadingState->set(
										SurfaceAccessibleI::Threading(
										a_threading));
							}
virtual	void				run(I32 a_id,sp<SpannedRange> a_spSpannedRange)	{}
virtual	void				run(I32 a_id,sp<SpannedRange> a_spSpannedRange,
									String a_stage)
							{	run(a_id,a_spSpannedRange); }

							//* As HandlerI
virtual	void				handle(Record& a_rSignal);

		sp<SpannedRange>	fullRange(void)		{ return m_spFullRange; }
		void				setFullRange(sp<SpannedRange> a_spRange);
		void				setNonAtomicRange(I32 a_start,I32 a_end);
		void				setAtomicRange(
									sp<SurfaceAccessibleI>
									a_spSurfaceAccessibleI,
									OperatorSurfaceCommon::AtomicChange
									a_atomicChange,
									String a_group="");

	protected:
		void				adjustThreads(void);
		void				runStage(String a_stage);

	private:
		sp<WorkForceI>		m_spWorkForceI;
		sp<SpannedRange>	m_spFullRange;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __operate_OperatorThreaded_h__ */

