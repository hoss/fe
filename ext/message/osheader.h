/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

#ifndef __MESSAGE_OSHEADER_H__
#define __MESSAGE_OSHEADER_H__

#include <stdio.h>
#include <string>
#include <cstring>
#include <memory>
#include <exception>

#ifdef _WIN32
//#include <ws2tcpip.h>
#include <winsock2.h>
#include <wininet.h>
#include <Ws2tcpip.h>
#pragma comment(lib, "ws2_32")
#pragma comment(lib, "wininet")
#else
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <errno.h>
typedef __int64_t SOCKET;
#define SOCKET_ERROR -1
#define SOCKADDR sockaddr
#define INVALID_SOCKET  (SOCKET)(~0)
#define closesocket close
#endif

#include "fe/plugin.h"

#endif // __MESSAGE_OSHEADER_H__
