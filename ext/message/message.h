/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __FE_MESSAGE_H__
#define __FE_MESSAGE_H__

#include "fe/plugin.h"

#include "MessageI.h"
#include "MessageReliableUDPI.h"

#endif /* __FE_MESSAGE_H__ */

