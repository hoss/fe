/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <string>
#include "message/message.pmh"
#include "message/test/test.h"

using namespace fe;
using namespace ext;

int main(int argc, char **argv)
{
	sp<Master> master(new Master);
	sp<Registry> registry = master->registry();

	registry->manage("fexMessageDL");

	sp<MessageI> messenger(registry->create("MessageI.MessageUDP"));

	messenger->postInit();

	TestRequest testRequest;
	memset(&testRequest, 0, sizeof(testRequest));

	testRequest.initPosition.x = 1.0;
	testRequest.initPosition.y = 2.0;
	testRequest.initPosition.z = 3.0;
	testRequest.zTrackGripRR = 99.96001;

	// import your connection info from somewhere ...
	std::string host;
	host = "127.0.0.1";
	uint16_t port = 5000;

	// create a Messagegram and copy payload (testRequest) into it
	Messagegram mgRequest;
	memset(&mgRequest, 0, sizeof(mgRequest));
	mgRequest.dataLen = sizeof(testRequest);
#ifdef _WIN32
	memcpy_s(static_cast<void *>(mgRequest.data), sizeof(mgRequest.data),
				static_cast<const void *>(&testRequest), sizeof(testRequest));
#else
	memcpy(mgRequest.data, &testRequest, sizeof(testRequest));
#endif
	printf("Sending struct TestRequest data to [%s:%d]:\n", host.c_str(), port);
	printf("=========================================\n");
	printf("Request Size:%d\n", (int)sizeof(testRequest));
	printf("=========================================\n");
	printf("initialPosition.x=%2.1lf\n", testRequest.initPosition.x);
	printf("initialPosition.y=%2.1lf\n", testRequest.initPosition.y);
	printf("initialPosition.z=%2.1lf\n", testRequest.initPosition.z);
	printf("zTrackGripRR=%3.5lf\n\n", testRequest.zTrackGripRR);

	// send messagegram
	messenger->sendTo(mgRequest, host.c_str(), port);

	Messagegram mgResponse;
	memset(&mgResponse, 0, sizeof(mgResponse));

	// wait for response (this is a blocking call)
	messenger->recvFrom(&mgResponse);

	if (mgResponse.dataLen != sizeof(TestResponse))
	{
		printf("bad response size\n");
		return 0;
	}

	// copy Messagegram payload to response
	TestResponse testResponse;
	memset(&testResponse, 0, sizeof(testResponse));
#ifdef _WIN32
	memcpy_s(static_cast<void *>(&testResponse), sizeof(TestResponse),
		static_cast<const void *>(mgResponse.data), mgResponse.dataLen);
#else
	memcpy(&testResponse, mgResponse.data, mgResponse.dataLen);
#endif
	// process your vhdResponse here ...
	printf("Received struct TestResponse data from [%s:%d]:\n", host.c_str(), port);
	printf("=========================================\n");
	printf("Response Size:%d\n", mgResponse.dataLen);
	printf("Expected Size:%d\n", (int)sizeof(TestResponse));
	printf("=========================================\n");
	printf("position.x=%2.1lf\n", testResponse.position.x);
	printf("position.y=%2.1lf\n", testResponse.position.y);
	printf("position.z=%2.1lf\n", testResponse.position.z);
	printf("nWheelRR=%3.5lf\n\n", testResponse.nWheelRR);

	return 0;
}
