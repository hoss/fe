/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <cstdlib>
#include <chrono>

#include "message/message.h"

#define TEST_MESSAGE_SIZE (10000)

using namespace fe;
using namespace fe::ext;

int main(int argc,char** argv)
{
	UNIT_START();

	const I32 sendCount(10);

	BWORD complete=FALSE;

	try
	{
		{
			sp<Master> spMaster(new Master);
			sp<Registry> spRegistry=spMaster->registry();

			spRegistry->manage("fexMessageReliableUDPDL");

			sp<MessageReliableUDPI> spReliableMessageSender(
					spRegistry->create("*.MessageReliableUDP"));

			if(!spReliableMessageSender.isValid())
			{
				feX(argv[0], "couldn't create MessageReliableUDP component");
				return 1;
			}

			uint16_t receivePort = 9000; // Message receive port

			if(argc >= 2)
			{
				receivePort = atoi(argv[1]);
			}

			feLog("Reliable message system starting\n");
			BWORD success=spReliableMessageSender->start(receivePort);
			UNIT_TEST(success);
			if(!success)
			{
				feLog("Reliable message startup failed\n");
				return 1;
			}

			// Test that double start works
			feLog("Reliable message system starting again (redundant)\n");
			success=spReliableMessageSender->start(receivePort);
			UNIT_TEST(success);
			if(!success)
			{
				feLog("Second reliable message startup failed\n");
				return 2;
			}

			for(I32 sendIndex=0;sendIndex<sendCount;sendIndex++)
			{
				feLog("\nMessage %d/%d\n", sendIndex, sendCount);

				uint8_t message1[TEST_MESSAGE_SIZE];
				uint8_t message2[TEST_MESSAGE_SIZE];

				// use current time as seed for random generator
				std::srand(std::time(nullptr));

				for(int i = 0; i < TEST_MESSAGE_SIZE; i++)
				{
					message1[i] = std::rand();
					message2[i] = std::rand();
				}

				const char ipAddress[] = "127.0.0.1";

				feLog("Sending a Message of size %u\n", TEST_MESSAGE_SIZE);
				spReliableMessageSender->sendReliableTo(
						message1, TEST_MESSAGE_SIZE, ipAddress, receivePort);
				spReliableMessageSender->sendReliableTo(
						message2, TEST_MESSAGE_SIZE, ipAddress, receivePort);

				int numMessagesReceived = 0;
				while(numMessagesReceived < 2)
				{
					uint8_t* recvMsg(nullptr);
					uint32_t msgSize(0);
					char fromIPaddress[16];

					feLog("Receiving message\n");
					while(spReliableMessageSender->recvReliableFrom(
							&recvMsg, msgSize, fromIPaddress) == false)
					{
						milliSleep(100);
					}

					feLog("Received a message of size %u\n", msgSize);
					UNIT_TEST(TEST_MESSAGE_SIZE == msgSize);

					if(TEST_MESSAGE_SIZE == msgSize)
					{
						success=FALSE;

						if(memcmp(message1,recvMsg, msgSize) == 0)
						{
							feLog("message 1 -"
									" message sent matches message received\n");
							numMessagesReceived++;
							success=TRUE;
						}
						else if(memcmp(message2,recvMsg, msgSize) == 0)
						{
							feLog("message 2 -"
									" message sent matches message received\n");
							numMessagesReceived++;
							success=TRUE;
						}
						else
						{
							feLog("Message received did not match any sent\n");
						}
						UNIT_TEST(success);
					}
					else
					{
						feLog("Received message size"
								" did not match sent message\n");
					}

					if(recvMsg)
					{
						delete[] recvMsg;
					}
				}
			}

			feLog("Reliable message system shutting down\n");
			spReliableMessageSender->shutdown();
		}

		complete=TRUE;
	}
	catch(Exception &e) { e.log(); }
	catch(...)			{ feLog("uncaught exception\n"); }

	UNIT_TEST(complete);
	UNIT_TEST(!Counted::trackerCount());
	UNIT_TRACK(sendCount*4+4);
	UNIT_RETURN();
}

