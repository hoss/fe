/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

#ifndef __MESSAGE_TESTHEADER_H__
#define __MESSAGE_TESTHEADER_H__

#include "platform/Thread.h"

#pragma pack(push, 1)

struct Vector3
{
	double x;
	double y;
	double z;
};

struct TestRequest
{
	Vector3  initPosition;
	double   zTrackGripRR;
};

struct TestResponse
{
	Vector3  position;
	double   nWheelRR;
};

#pragma pack(pop)

#endif // __MESSAGE_TESTHEADER_H__
