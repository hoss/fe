/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

#include "messageReliableUDP.pmh"

#define FE_RELIABLE_UDP_VERBOSE		FALSE
#define FE_RELIABLE_UDP_LOCK_CHECK	(FE_CODEGEN<=FE_DEBUG)

namespace fe
{
namespace ext
{

MessageReliableUDP::MessageReliableUDP():
	m_transmitListLockCount(0),
	m_receiveListLockCount(0)
{
    m_initialized = false;
	m_transmitThread = nullptr;
	m_receiveThread = nullptr;
}

MessageReliableUDP::~MessageReliableUDP()
{
    shutdown();
}

bool MessageReliableUDP::start(uint16_t receivePort)
{
	if(m_initialized)
	{
		shutdown();
	}

	m_receivePort = receivePort;

	m_msgID = 0;
	m_transmitThreadDone = false;
	m_receiveThreadDone = false;
	m_running = true;
	m_burstPacketLimit = RELIABLE_UDP_BURST_PACKET_LIMIT;

//~	Result result = registry()->manage("fexMessageDL");
//~    if(failure(result))
//~    {
//~		feLog("MessageReliableUDP::start error loading fexMessageDL\n");
//~		return false;
//~    }

	// Setup transmit system
	m_transmitMsgSystem = registry()->create("MessageI.MessageUDP");

	if(!m_transmitMsgSystem.isValid() || !m_transmitMsgSystem->postInit())
	{
		feLog("MessageReliableUDP::start"
				" initialization of verification UDP system failed\n");
		return false;
	}

	m_verificationPort = m_transmitMsgSystem->bindRandom();
	if(m_verificationPort == 0)
	{
		feLog("MessageReliableUDP::start"
				" error failed trying to bind to a random verification port\n");
		return false;
	}

	// Setup receive system
	m_receiveMsgSystem = registry()->create("MessageI.MessageUDP");

	if(!m_receiveMsgSystem.isValid() || !m_receiveMsgSystem->postInit())
	{
		feLog("MessageReliableUDP::start"
				" initialization of receive UDP system failed\n");
		return false;
	}

	if(m_receivePort == 0)
	{
		m_receivePort = m_receiveMsgSystem->bindRandom();
		if(m_receivePort == 0)
		{
			feLog("MessageReliableUDP::start"
					" error failed trying to bind to a random receive port\n");
			return false;
		}
	}
	else if(!m_receiveMsgSystem->bind(m_receivePort))
	{
		feLog("MessageReliableUDP::start failed bind to receive port\n");
		return false;
	}

	// Setup transmit tread
	if(m_transmitThread == nullptr)
	{
		m_transmitThread =
				new std::thread(&MessageReliableUDP::transmitThread,this);

		if(m_transmitThread == nullptr)
		{
			feLog("MessageReliableUDP::start"
					" failed to start transmit thread\n");
			return false;
		}
	}

	// Setup receive tread
	if(m_receiveThread == nullptr)
	{
		m_receiveThread =
				new std::thread(&MessageReliableUDP::receiveThread,this);

		if(m_receiveThread == nullptr)
		{
			feLog("MessageReliableUDP::start"
					" failed to start receive thread\n");
			return false;
		}
	}

    m_initialized = true;
	return true;
}

uint16_t MessageReliableUDP::getReceivePort()
{
	return m_receivePort;
}

void MessageReliableUDP::shutdown()
{
#if FE_RELIABLE_UDP_VERBOSE
	feLog("MessageReliableUDP::shutdown\n");
#endif
	m_running = false;

	if(m_transmitThread != nullptr)
	{
#if FE_RELIABLE_UDP_VERBOSE
		feLog("MessageReliableUDP::shutdown"
				" wait for transmit thread to shutdown\n");
#endif
		while(!m_transmitThreadDone)
		{
			std::this_thread::sleep_for(std::chrono::milliseconds(20));
		}
		m_transmitThread = nullptr;

#if FE_RELIABLE_UDP_VERBOSE
		feLog("MessageReliableUDP::shutdown Transmit thread shutdown\n");
#endif
	}

	if(m_receiveThread != nullptr)
	{
#if FE_RELIABLE_UDP_VERBOSE
		feLog("MessageReliableUDP::shutdown"
				" wait for receive thread to shutdown\n");
#endif
		while(!m_receiveThreadDone)
		{
			std::this_thread::sleep_for(std::chrono::milliseconds(20));
		}
		m_receiveThread = nullptr;

#if FE_RELIABLE_UDP_VERBOSE
		feLog("MessageReliableUDP::shutdown Receive thread shutdown\n");
#endif
	}

	if(m_transmitMsgSystem.isValid())
	{
#if FE_RELIABLE_UDP_VERBOSE
		feLog("MessageReliableUDP::shutdown closing verification socket\n");
#endif
		m_transmitMsgSystem->shutdown();
		m_transmitMsgSystem = nullptr;
	}

	if(m_receiveMsgSystem.isValid())
	{
#if FE_RELIABLE_UDP_VERBOSE
		feLog("MessageReliableUDP::shutdown closing receive socket\n");
#endif
		m_receiveMsgSystem->shutdown();
		m_receiveMsgSystem = nullptr;
	}

	m_transmitList.clear();
	m_receiveList.clear();
	m_fullyreceivedMsgList.clear();

	m_initialized = false;

#if FE_RELIABLE_UDP_VERBOSE
	feLog("MessageReliableUDP::shutdown complete\n");
#endif
}

void MessageReliableUDP::transmitThread()
{
#if FE_RELIABLE_UDP_VERBOSE
	feLog("MessageReliableUDP::transmitThread thread started\n");
#endif

	m_transmitThreadDone = false;
	int packetBurstCount = 0;

	while(m_running)
	{
		m_transmitListLock.lock();
#if FE_RELIABLE_UDP_LOCK_CHECK
		FEASSERT(!m_transmitListLockCount);
		m_transmitListLockCount++;
#endif

		if(m_transmitList.size() > 0)
		{
			for(auto it = m_transmitList.begin(); it != m_transmitList.end()
					&& packetBurstCount < m_burstPacketLimit; it++)
			{
				if(it->doInitialTransmition)
				{
					// initial transmition of packets up to burst packet limit
					for(size_t i = 0; i < RELIABLE_UDP_MAX_PACKETS &&
							packetBurstCount < m_burstPacketLimit; i++)
					{
						uint32_t currentOffset =
								(uint32_t)i * RELIABLE_UDP_PACKET_SIZE;
						if(currentOffset < it->messageSize &&
								it->packetsSent.test(i) == false)
						{
							sendPacket(*it, i);
							it->packetsSent.set(i,true);
							packetBurstCount++;
						}
					}
				}
				else
				{
#if FE_RELIABLE_UDP_VERBOSE
//					feLog("MessageReliableUDP::transmitThread"
//							" checking if packages need re-transmit\n");
#endif
					// Give receiver time to respond and
					// if we haven't received everything they sent
					if(now() >= (it->lastTransmitTime +
							RELIABLE_UDP_MIN_RESPONSE_TIME) &&
							it->packetsReceived != it->packetsSent)
					{
						for(size_t i = 0; i < RELIABLE_UDP_MAX_PACKETS &&
								packetBurstCount < m_burstPacketLimit; i++)
						{
#if FE_RELIABLE_UDP_VERBOSE
							// If packet sent but not received then re-transmit
							feLog("MessageReliableUDP::transmitThread"
									" re-transmit missing packet"
									" to port %s:%u\n",
									it->ipAddress, it->port);
#endif
							if(it->packetsSent.test(i) &&
									!it->packetsReceived.test(i))
							{
								sendPacket(*it, i);
								packetBurstCount++;
							}
						}
					}
				}
			}
		}

#if FE_RELIABLE_UDP_LOCK_CHECK
		FEASSERT(m_transmitListLockCount==1);
		m_transmitListLockCount--;
#endif
		m_transmitListLock.unlock();

		checkVerifications();

		// Reset burst count after delaying while reading back verifications
		packetBurstCount = 0;
	}

	m_transmitThreadDone = true;

#if FE_RELIABLE_UDP_VERBOSE
	feLog("MessageReliableUDP::transmitThread thread shutting down\n");
#endif
}

int64_t MessageReliableUDP::now()
{
	int64_t currentTime = std::chrono::duration_cast<std::chrono::milliseconds>
			(std::chrono::system_clock::now().time_since_epoch()).count();
	return currentTime;
}

void MessageReliableUDP::checkVerifications()
{
	Messagegram verificationMsg;

	while(m_transmitMsgSystem->recvFrom(&verificationMsg, 0, 100))
	{
		MultiPacketResponseMsg *receivedMsg =
				(MultiPacketResponseMsg *)verificationMsg.data;

		m_transmitListLock.lock();
#if FE_RELIABLE_UDP_LOCK_CHECK
		FEASSERT(!m_transmitListLockCount);
		m_transmitListLockCount++;
#endif

		for(auto it = m_transmitList.begin(); it != m_transmitList.end();)
		{
//			feLog("MessageReliableUDP::checkVerifications messageID %d vs %d\n",
//					it->messageID,receivedMsg->messageID);
			if(it->messageID == receivedMsg->messageID)
			{
				if(it->doInitialTransmition == false &&
					receivedMsg->packetsReceived == it->packetsSent)
				{
//					feLog("MessageReliableUDP::checkVerifications erase\n");
					delete[] it->message;
					m_transmitList.erase(it);
					break;
				}
				else
				{
//					feLog("MessageReliableUDP::checkVerifications"
//							" received %d/%d\n",
//							receivedMsg->packetsReceived,it->packetsSent);
					it->packetsReceived = receivedMsg->packetsReceived;
					it->lastReceivedTime = now();

					//* HACK adding to break infinite loop; is this ok?
					++it;
				}
			}
			else
			{
				++it;
			}
		}

#if FE_RELIABLE_UDP_LOCK_CHECK
		FEASSERT(m_transmitListLockCount==1);
		m_transmitListLockCount--;
#endif
		m_transmitListLock.unlock();
	}
}

void MessageReliableUDP::receiveThread()
{
#if FE_RELIABLE_UDP_VERBOSE
	feLog("MessageReliableUDP::receiveThread thread started\n");
#endif

	m_receiveThreadDone = false;

	Messagegram receiveMsg;
	uint32_t address;
	char receiveAddress[INET_ADDRSTRLEN];

	while(m_running)
	{
		if(m_receiveMsgSystem->recvFrom(&receiveMsg, 0, 100))
		{
			MultiPacketMsg *packetMsg = (MultiPacketMsg *)receiveMsg.data;

			m_receiveMsgSystem->getRecvAddress(address);
			m_receiveMsgSystem->convertAddressToStr(address, receiveAddress);

			bool found = false;

			m_receiveListLock.lock();
#if FE_RELIABLE_UDP_LOCK_CHECK
			FEASSERT(!m_receiveListLockCount);
			m_receiveListLockCount++;
#endif

			// Check to see if this is part of an existing message//
			// to be filled in
			for(auto it = m_receiveList.begin();
					it != m_receiveList.end(); ++it)
			{
				if(it->partOfThisMessage(*packetMsg, receiveAddress))
				{
					uint32_t currentOffset = (uint32_t)packetMsg->offset *
							RELIABLE_UDP_PACKET_SIZE;
					uint32_t msgSize = RELIABLE_UDP_PACKET_SIZE -
							(sizeof(MultiPacketMsg) - receiveMsg.dataLen);
#if FE_RELIABLE_UDP_VERBOSE
					feLog("MessageReliableUDP::receiveThread"
							" filling in message %u of %u bytes"
							" at offset %u\n",
							it->messageID, msgSize, currentOffset);
#endif
					memcpy(&it->message[currentOffset],
							packetMsg->data, msgSize);
					it->packetsReceived.set(packetMsg->offset, true);
					it->lastReceivedTime = now();
					it->updated = true; // Mark message as needing an update
					found = true;

					// If we got the whole message send the response
					if(it->receivedFullMessage())
					{
						sendResponseMsg(*it);
					}
					break;
				}
			}

			// If it is a new message and not late message packets that may
			// have be re-sent then create a new message
			if(!found && !alreadyReceived(*packetMsg, receiveAddress))
			{
				// Add new message to the receive list
				MultiPacketMsgInProgress newMsg;

				// ID + IP address of sender +
				// verification port form the unique identifier
				newMsg.messageID = packetMsg->messageID;
				strcpy(newMsg.ipAddress, receiveAddress);
				newMsg.port = packetMsg->verificationPort;

#if FE_RELIABLE_UDP_VERBOSE
				feLog("MessageReliableUDP::receiveThread"
						" adding new message %u received from %s:%u\n",
						newMsg.messageID, newMsg.ipAddress, newMsg.port);
#endif
				newMsg.messageSize = packetMsg->messageSize;
				newMsg.message = new uint8_t[packetMsg->messageSize];

				uint32_t msgSize = RELIABLE_UDP_PACKET_SIZE
								 -(sizeof(MultiPacketMsg) - receiveMsg.dataLen);

				memcpy(newMsg.message, packetMsg->data, msgSize);

				newMsg.lastReceivedTime = now();
				newMsg.lastTransmitTime = 0;
				newMsg.packetsSent.reset();
				newMsg.packetsReceived.set(packetMsg->offset, true);
				newMsg.doInitialTransmition = true;
				newMsg.updated = true; // Mark message as needing an update

				// If we got the whole message send the response immediately
				if(newMsg.receivedFullMessage())
				{
					sendResponseMsg(newMsg);
				}

				m_receiveList.push_back(newMsg);
			}

#if FE_RELIABLE_UDP_LOCK_CHECK
			FEASSERT(m_receiveListLockCount==1);
			m_receiveListLockCount--;
#endif
			m_receiveListLock.unlock();
		}
		else
		{
			// If the receive queue is empty then update the sender on
			// the status of any partially received messages

			m_receiveListLock.lock();
#if FE_RELIABLE_UDP_LOCK_CHECK
			FEASSERT(!m_receiveListLockCount);
			m_receiveListLockCount++;
#endif

			for(auto it = m_receiveList.begin();
					it != m_receiveList.end(); ++it)
			{
				if(it->updated)
				{
					sendResponseMsg(*it);
				}
			}

#if FE_RELIABLE_UDP_LOCK_CHECK
			FEASSERT(m_receiveListLockCount==1);
			m_receiveListLockCount--;
#endif
			m_receiveListLock.unlock();
		}
	}

	m_receiveThreadDone = true;

#if FE_RELIABLE_UDP_VERBOSE
	feLog("MessageReliableUDP::receiveThread thread shutting down\n");
#endif
}

//* NOTE only called by receieve thread
bool MessageReliableUDP::alreadyReceived(const MultiPacketMsg &msg,
	const char *msgIP)
{
	for(auto it = m_fullyreceivedMsgList.begin();
			it != m_fullyreceivedMsgList.end(); it++)
	{
		if(it->messageID == msg.messageID &&
				it->verificationPort == msg.verificationPort &&
				strcmp(it->ipAddress, msgIP) == 0)
		{
			return true;
		}
	}

	return false;
}

//* NOTE only called by receieve thread
bool MessageReliableUDP::sendResponseMsg(MultiPacketMsgInProgress &receivedMsg)
{
	MultiPacketResponseMsg *msg =
			(MultiPacketResponseMsg *)m_verificationMsg.data;
	m_verificationMsg.dataLen = sizeof(MultiPacketResponseMsg);

	msg->messageID = receivedMsg.messageID;
	msg->packetsReceived = receivedMsg.packetsReceived;
#if FE_RELIABLE_UDP_VERBOSE
	feLog("MessageReliableUDP::sendResponseMsg"
			" sending verify for message %u to %s:%u\n",
			msg->messageID, receivedMsg.ipAddress, receivedMsg.port);
#endif
	if(m_receiveMsgSystem->sendTo(m_verificationMsg,
			receivedMsg.ipAddress, receivedMsg.port))
	{
		if(receivedMsg.receivedFullMessage() && !alreadyReceived(receivedMsg))
		{
#if FE_RELIABLE_UDP_VERBOSE
			feLog("MessageReliableUDP::sendResponseMsg"
					" adding message to list of received messages\n");
#endif
			// Added it to the list of already received messages
			MultiPacketMsgReceived newlyReceived;

			newlyReceived.messageID = receivedMsg.messageID;
			memcpy(newlyReceived.ipAddress, receivedMsg.ipAddress,
					sizeof(newlyReceived.ipAddress));
			newlyReceived.verificationPort = receivedMsg.port;

			m_fullyreceivedMsgList.push_front(newlyReceived);
			if(m_fullyreceivedMsgList.size() > RELIABLE_UDP_RECEIVED_MAX_LIST)
			{
#if FE_RELIABLE_UDP_VERBOSE
				feLog("MessageReliableUDP::sendResponseMsg"
						" reached max messages in received list;"
						" removing old one\n");
#endif
				m_fullyreceivedMsgList.pop_back();
			}
		}

		receivedMsg.updated = false;
		return true;
	}

	return false;
}

//* NOTE only called via receieve thread
bool MessageReliableUDP::alreadyReceived(
	const MultiPacketMsgInProgress &testMsg)
{
	for(auto it = m_fullyreceivedMsgList.begin();
			it != m_fullyreceivedMsgList.end(); it++)
	{
		if(*it == testMsg)
		{
			return true;
		}
	}

	return false;
}

//* NOTE only called by transmit thread
bool MessageReliableUDP::sendPacket(MultiPacketMsgInProgress &fullMsg,
	uint32_t offset)
{
	m_packetMsg.dataLen = sizeof(MultiPacketMsg);

	MultiPacketMsg *mpMsg = (MultiPacketMsg *)m_packetMsg.data;

	mpMsg->messageID = fullMsg.messageID;
	mpMsg->messageSize = fullMsg.messageSize;
	mpMsg->verificationPort = m_verificationPort;
	mpMsg->offset = offset;

	uint32_t currentOffset = offset * RELIABLE_UDP_PACKET_SIZE;

	uint32_t msgSize;

	if((currentOffset+RELIABLE_UDP_PACKET_SIZE) >= fullMsg.messageSize)
	{
		fullMsg.doInitialTransmition = false;

		// Calculate the remain bytes to send in the message
		msgSize = (fullMsg.messageSize - currentOffset);

		// Reduce the message size to what is actually being used
		m_packetMsg.dataLen -= (uint16_t)(RELIABLE_UDP_PACKET_SIZE - msgSize);
	}
	else
	{
		msgSize = RELIABLE_UDP_PACKET_SIZE;
	}

	// Copy over the message data
	memcpy(mpMsg->data, &fullMsg.message[currentOffset], msgSize);

#if FE_RELIABLE_UDP_VERBOSE
	feLog("MessageReliableUDP::sendPacket message %u of size:%u to %s:%u\n",
			mpMsg->messageID, m_packetMsg.dataLen,
			fullMsg.ipAddress, fullMsg.port);
#endif

	if(!m_transmitMsgSystem->sendTo(m_packetMsg, fullMsg.ipAddress,
			fullMsg.port))
	{
		feLog("MessageReliableUDP::sendPacket sendTo failed");
		return false;
	}

	fullMsg.lastTransmitTime  = now();
	return true;
}

bool MessageReliableUDP::recvReliableFrom(uint8_t **msg,
	uint32_t &bytesRecv, char *fromIPaddress)
{
	if(!m_initialized)
	{
		return false;
	}

	bool haveMsg = false;

	m_receiveListLock.lock();
#if FE_RELIABLE_UDP_LOCK_CHECK
	FEASSERT(!m_receiveListLockCount);
	m_receiveListLockCount++;
#endif

	if(m_receiveList.size() > 0)
	{
		auto recvMsg = m_receiveList.begin();
		if(recvMsg->receivedFullMessage())
		{
			*msg = recvMsg->message;
			bytesRecv = recvMsg->messageSize;
			memcpy(fromIPaddress, recvMsg->ipAddress,
					sizeof(recvMsg->ipAddress));

			m_receiveList.pop_front();

			haveMsg = true;
		}
	}

#if FE_RELIABLE_UDP_LOCK_CHECK
	FEASSERT(m_receiveListLockCount==1);
	m_receiveListLockCount--;
#endif
	m_receiveListLock.unlock();

	return haveMsg;
}

bool MessageReliableUDP::sendReliableTo(const uint8_t *msg,
	const uint32_t msgSize, const char *ipAddress, uint16_t port)
{
	if(!m_initialized)
	{
#if FE_RELIABLE_UDP_VERBOSE
		feLog("MessageReliableUDP::sendReliableTo not initialized\n");
#endif
		return false;
	}

	MultiPacketMsgInProgress newMsg;

	++m_msgID;
	// Message ID 0 is reserver so skip it
	if(m_msgID == 0)
	{
		++m_msgID;
	}
	newMsg.messageID = m_msgID;
	newMsg.messageSize = msgSize;
	newMsg.message = new uint8_t[msgSize];
	if(newMsg.message == nullptr)
	{
#if FE_RELIABLE_UDP_VERBOSE
		feLog("MessageReliableUDP::sendReliableTo null message\n");
#endif
		return false;
	}
	memcpy(newMsg.message, msg, msgSize);
	memcpy(newMsg.ipAddress, ipAddress, sizeof(newMsg.ipAddress));
	newMsg.port = port;
	newMsg.lastReceivedTime = 0;
	newMsg.lastTransmitTime = 0;
	newMsg.packetsSent.reset();
	newMsg.packetsReceived.reset();
	newMsg.doInitialTransmition = true;

	m_transmitListLock.lock();
#if FE_RELIABLE_UDP_LOCK_CHECK
	FEASSERT(!m_transmitListLockCount);
	m_transmitListLockCount++;
#endif

#if FE_RELIABLE_UDP_VERBOSE
	feLog("MessageReliableUDP::sendReliableTo"
			" sending message %u of %u bytes to %s:%u\n",
			newMsg.messageID, newMsg.messageSize,
			newMsg.ipAddress, newMsg.port);
#endif
	m_transmitList.push_back(newMsg);

#if FE_RELIABLE_UDP_LOCK_CHECK
	FEASSERT(m_transmitListLockCount==1);
	m_transmitListLockCount--;
#endif
	m_transmitListLock.unlock();

	return true;
}

} /* namespace ext */
} /* namespace fe */
