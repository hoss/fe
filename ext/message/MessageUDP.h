/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

#ifndef __MESSAGEUDP_H__
#define __MESSAGEUDP_H__

#include "osheader.h"

namespace fe
{
namespace ext
{

/**************************************************************************//**
    @brief Message Sender/Receiver over UDP

	@ingroup message
*//***************************************************************************/
class MessageUDP : virtual public MessageI
{
public:
	MessageUDP();
	virtual ~MessageUDP();

	virtual bool recvFrom(Messagegram *) override;
	virtual bool recvFrom(Messagegram *, long seconds, long uSeconds) override;
	virtual bool recvLargeFrom(Messagegram *) override;
	virtual void getRecvAddress(uint32_t &address) override;
	virtual void convertAddressToStr(const uint32_t address,
			char *addStr) override;
	virtual bool replyLast(const Messagegram &) override;
	virtual bool sendTo(const Messagegram &, const char *, uint16_t) override;
	virtual bool sendLargeTo(const Messagegram &,
			const char *, uint16_t) override;
	virtual bool bind(const char *, uint16_t) override;
	virtual bool bind(uint16_t) override;
	virtual uint16_t bindRandom() override;
	virtual bool postInit() override;
	virtual bool setReceiveBufferSize(int bufferSize) override;
	virtual bool setSendBufferSize(int bufferSize) override;
	virtual bool setBroadcast(bool boardcast) override;
	virtual bool setReusePort(bool reuse) override;
	virtual bool setReuseAddress(bool reuse) override;
	virtual void shutdown() override;

private:
	bool receiveFrom(char *mesg, uint16_t *bytesRecv,
			uint16_t maxLen, sockaddr_in *recvFrom,
			long seconds, long uSeconds, int flags = 0);
	bool receiveFrom(char *mesg, uint16_t *bytesRecv, uint16_t maxLen,
			sockaddr_in *recvFrom, int flags = 0);
	bool receiveLargeFrom(char *mesg, uint16_t *bytesRecv, uint16_t maxLen,
			sockaddr_in *recvFrom, int flags = 0);
	bool sendTo(sockaddr_in &addr, const char *mesg, uint16_t mesgLen,
			int flags = 0);
	bool bindTo(const char *host, uint16_t port);
	bool bindTo(uint16_t port);

private:
	class MessageAddress
	{
	public:
		MessageAddress() : m_isValid(false) {}

		MessageAddress(const char *host, unsigned short port)
		: m_host(host), m_port(port)
		{
			int addrLen = sizeof(m_addr);
			memset(&m_addr, 0, addrLen);
			m_addr.sin_family = AF_INET;
			m_addr.sin_port = htons(m_port);
			inet_pton(AF_INET, m_host.c_str(), &m_addr.sin_addr);
			m_isValid = true;
		}

		MessageAddress(struct sockaddr_in addr) : m_addr(addr)
		{
			char ip[256];
			memset(ip, 0, 256);
			inet_ntop(AF_INET, &m_addr.sin_addr, ip, 256);
			m_port = ntohs(m_addr.sin_port);
			m_host = ip;
			m_isValid = true;
		}

		MessageAddress &operator=(const MessageAddress &other)
		{
			m_host = other.m_host;
			m_port = other.m_port;
			m_addr = other.m_addr;
			m_isValid = other.m_isValid;
			return *this;
		}

		const std::string& getHost() const { return m_host; }
		unsigned short getPort() const { return m_port; }
		struct sockaddr_in getNetAddress() const { return m_addr; }
		bool isValid() const { return m_isValid;  }

	private:
		bool m_isValid;
		std::string m_host;
		unsigned short m_port;
		struct sockaddr_in m_addr;
	};

	SOCKET m_socket;
	MessageAddress m_replyAddr;
};

} // namespace ext
} // namespace fe

#endif // __MESSAGEUDP_H__
