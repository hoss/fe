/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

#ifndef __MessageReliableUDPI_h__
#define __MessageReliableUDPI_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
    @brief Message Sender/Receiver multi-packet reliable over UDP

	@ingroup message
*//***************************************************************************/
class FE_DL_EXPORT MessageReliableUDPI :
	virtual public fe::Component,
	public fe::CastableAs<MessageReliableUDPI>
{
public:
	/// @brief	Start receive and transmit thread and initialize the receive port (0 = open port)
	virtual bool start(uint16_t receivePort) = 0;

	/// @brief	Returns the selected receive port
	virtual uint16_t getReceivePort() = 0;

	/// @brief	Shutdown receive and transmit thread and the message systems
	virtual void shutdown() = 0;

	/// @brief	Check to see if a message has been received. The receiver is responsible for deleting the message
	virtual bool recvReliableFrom(uint8_t **msg, uint32_t &msgSize, char *fromIPaddress) = 0;

	/// @brief	Send a reliable message over UDP to the address and port given
	virtual bool sendReliableTo(const uint8_t *msg, const uint32_t msgSize, const char *ipAddress, uint16_t port) = 0;
};

} // namespace ext
} // namespace fe

#endif // __MessageReliableUDPI_h__
