/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <pcre/pcre.pmh>

#include "platform/dlCore.cc"

#define FE_PCRE_DEBUG	FALSE
#define FE_PCRE_REPAIR	FALSE	//* FIX broken version in early RHEL6

namespace fe
{
namespace ext
{

void* pcre_init(const char* a_pattern)
{
	pcrecpp::RE* expression=new pcrecpp::RE(a_pattern);

#if FE_PCRE_DEBUG
	feLogError("pcre_init \"%s\" -> %p\n",a_pattern,expression);
#endif

	return expression;
}

void* pcre_match(void* a_expression,const char* a_candidate)
{
#if FE_PCRE_DEBUG
	feLogError("pcre_match %p \"%s\"\n",a_expression,a_candidate);
#endif

	Array<std::string>* pStringArray=new Array<std::string>(1);
	pcrecpp::RE& re=*(pcrecpp::RE*)a_expression;
	if(!re.FullMatch(a_candidate))
/*
			pcrecpp::Arg(&pString[0]),	pcrecpp::Arg(&pString[1]),
			pcrecpp::Arg(&pString[2]),	pcrecpp::Arg(&pString[3]),
			pcrecpp::Arg(&pString[4]),	pcrecpp::Arg(&pString[5]),
			pcrecpp::Arg(&pString[6]),	pcrecpp::Arg(&pString[7]),
			pcrecpp::Arg(&pString[8]),	pcrecpp::Arg(&pString[9]),
			pcrecpp::Arg(&pString[10]),	pcrecpp::Arg(&pString[11]),
			pcrecpp::Arg(&pString[12]),	pcrecpp::Arg(&pString[13]),
			pcrecpp::Arg(&pString[14]),	pcrecpp::Arg(&pString[15])))
*/
	{
#if FE_PCRE_DEBUG
	feLogError("pcre_match %p failed\n",a_expression);
#endif

		delete pStringArray;
		return NULL;
	}

#if FE_PCRE_DEBUG
	feLogError("pcre_match %p result %p\n",a_expression,pStringArray);
#endif

	return pStringArray;
}

void* pcre_search(void* a_expression,const char* a_candidate)
{
#if FE_PCRE_DEBUG
	feLogError("pcre_search %p \"%s\"\n",a_expression,a_candidate);
#endif

	Array<std::string>* pStringArray=new Array<std::string>(1);
	pcrecpp::RE& re=*(pcrecpp::RE*)a_expression;
	if(!re.PartialMatch(a_candidate))
/*
			pcrecpp::Arg(&pString[0]),	pcrecpp::Arg(&pString[1]),
			pcrecpp::Arg(&pString[2]),	pcrecpp::Arg(&pString[3]),
			pcrecpp::Arg(&pString[4]),	pcrecpp::Arg(&pString[5]),
			pcrecpp::Arg(&pString[6]),	pcrecpp::Arg(&pString[7]),
			pcrecpp::Arg(&pString[8]),	pcrecpp::Arg(&pString[9]),
			pcrecpp::Arg(&pString[10]),	pcrecpp::Arg(&pString[11]),
			pcrecpp::Arg(&pString[12]),	pcrecpp::Arg(&pString[13]),
			pcrecpp::Arg(&pString[14]),	pcrecpp::Arg(&pString[15])))
*/
	{
#if FE_PCRE_DEBUG
	feLogError("pcre_search %p failed\n",a_expression);
#endif

		delete pStringArray;
		return NULL;
	}

#if FE_PCRE_DEBUG
	feLogError("pcre_search %p result %p\n",a_expression,pStringArray);
#endif

	return pStringArray;
}

void* pcre_replace(void* a_expression,const char* a_candidate,
		const char* a_replacement)
{
	//* there is also Replace() for single replacement

	Array<std::string>* pStringArray=new Array<std::string>(1);
	pcrecpp::RE& re=*(pcrecpp::RE*)a_expression;

#if FE_PCRE_DEBUG
	feLogError("pcre_replace %p \"%s\" \"%s\" -> \"%s\"\n",
			a_expression,a_candidate,re.pattern().c_str(),a_replacement);
#endif

	std::string replaced=a_candidate;
	std::string replacement=a_replacement;
	if(!re.GlobalReplace(replacement,&replaced))
	{
		delete pStringArray;
		return NULL;
	}

#if FE_PCRE_DEBUG
	feLogError("pcre_replace result \"%s\" \n",replaced.c_str());
#endif

	(*pStringArray)[0]=replaced;

	return pStringArray;
}

const char* pcre_result(void* a_result,U32 a_index)
{
#if FE_PCRE_DEBUG
	feLogError("pcre_result %p %d\n",a_result,a_index);
#endif

	Array<std::string>* pStringArray=(Array<std::string>*)a_result;
	return (*pStringArray)[a_index].c_str();
}

void pcre_finish(void* a_expression)
{
#if FE_PCRE_DEBUG
	feLogError("pcre_finish %p\n",a_expression);
#endif

	pcrecpp::RE* expression=(pcrecpp::RE*)a_expression;
	delete expression;
}

void pcre_release(void* a_result)
{
#if FE_PCRE_DEBUG
	feLogError("pcre_release %p\n",a_result);
#endif

	Array<std::string>* pStringArray=(Array<std::string>*)a_result;
	delete pStringArray;
}

extern "C"
{

#if FE_PCRE_REPAIR

typedef bool (PcreInitCharFunction)(pcrecpp::RE* re,
					const char* buffer,
					const pcrecpp::RE_Options* a_options);
typedef bool (PcreInitStringFunction)(pcrecpp::RE* re,
					const std::string& a_string,
					const pcrecpp::RE_Options* a_options);

static PcreInitCharFunction* s_fnInitChar=NULL;
static PcreInitStringFunction* s_fnInitString=NULL;

//* dynamically check which signatures of pcre_match are available

//	_ZN7pcrecpp2RE4InitEPKcPKNS_10RE_OptionsE
//	_ZN7pcrecpp2RE4InitERKSsPKNS_10RE_OptionsE

bool _ZN7pcrecpp2RE4InitEPKcPKNS_10RE_OptionsE(
		pcrecpp::RE* re,
		const char* a_pattern,
		const pcrecpp::RE_Options* a_options)
{
#if FE_PCRE_DEBUG
	feLogError("_ZN7pcrecpp2RE4InitEPKcPKNS_10RE_OptionsE \"%s\"\n",
			a_pattern);
#endif

	if(s_fnInitChar)
	{
		return s_fnInitChar(re,a_pattern,a_options);
	}
	else if(s_fnInitString)
	{
		const std::string string=a_pattern;
		return s_fnInitString(re,string,a_options);
	}

	return false;
}

bool _ZN7pcrecpp2RE4InitERKSsPKNS_10RE_OptionsE(
		pcrecpp::RE* re,
		const std::string& a_string,
		const pcrecpp::RE_Options* a_options)
{
#if FE_PCRE_DEBUG
	feLogError("_ZN7pcrecpp2RE4InitERKSsPKNS_10RE_OptionsE \"%s\"\n",
			a_string.c_str());
#endif

	if(s_fnInitString)
	{
		return s_fnInitString(re,a_string,a_options);
	}
	else if(s_fnInitChar)
	{
		return s_fnInitChar(re,a_string.c_str(),a_options);
	}

	return false;
}

#endif // FE_PCRE_REPAIR

FE_DL_EXPORT bool regex_init(void)
{
#if FE_PCRE_DEBUG
	feLogError("pcreDL regex_init\n");
#endif

#if FE_PCRE_REPAIR

	DL_Loader *pLoader=new DL_Loader();
	FEASSERT(pLoader);

	const BWORD adaptname=TRUE;
	BWORD success=pLoader->openDL("pcrecpp",adaptname);
	if(!success)
	{
		feLogError("  PCRE symbols inaccessible\n");
		delete pLoader;

		return false;
	}
	else
	{
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wconditionally-supported"

		s_fnInitChar=
				(PcreInitCharFunction*)pLoader->findDLFunction(
				"_ZN7pcrecpp2RE4InitEPKcPKNS_10RE_OptionsE");

		s_fnInitString=
				(PcreInitStringFunction*)pLoader->findDLFunction(
				"_ZN7pcrecpp2RE4InitERKSsPKNS_10RE_OptionsE");

#pragma GCC diagnostic pop

#if FE_PCRE_DEBUG
		feLogError("fnInitChar %p\n",s_fnInitChar);
		feLogError("fnInitString %p\n",s_fnInitString);
#endif

		//* HACK test
//		s_fnInitChar=NULL;
//		s_fnInitString=NULL;

		if(!s_fnInitChar)
		{
			if(s_fnInitString)
			{
				feLogError("  PCRE RE:Init bypass installed\n");
			}
			else
			{
				feLogError("  PCRE RE:Init symbols not found\n");
				return false;
			}
		}
	}

#endif // FE_PCRE_REPAIR

//	feLogError("  using PCRE\n");

	Regex::replaceInitFunction(pcre_init);
	Regex::replaceMatchFunction(pcre_match);
	Regex::replaceSearchFunction(pcre_search);
	Regex::replaceReplaceFunction(pcre_replace);
	Regex::replaceResultFunction(pcre_result);
	Regex::replaceFinishFunction(pcre_finish);
	Regex::replaceReleaseFunction(pcre_release);

#if FE_PCRE_DEBUG
	feLogError("pcreDL regex_init done\n");
#endif

	return true;
}

}

} /* namespace ext */
} /* namespace fe */
