/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __vegetation_h__
#define __vegetation_h__

#include "datatool/datatool.h"
#include "operate/operate.h"
#include "draw/draw.h"

#ifdef FE_VEG_VIEWER
#include "viewer/viewer.h"
#endif

#ifdef MODULE_vegetation
#define FE_VEGETATION_PORT FE_DL_EXPORT
#else
#define FE_VEGETATION_PORT FE_DL_IMPORT
#endif

#include "vegetation/Plant.h"
#include "vegetation/TreeLevel.h"
#include "vegetation/StickLevel.h"
#include "vegetation/LeafLevel.h"
#include "vegetation/PlantSeed.h"
#include "vegetation/TreeSeed.h"
#include "vegetation/Forest.h"

#include "vegetation/PlantModelI.h"
#include "vegetation/SketchI.h"

#endif // __vegetation_h__

