/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __vegetation_SketchI_h__
#define __vegetation_SketchI_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Generic drawable object

	@ingroup vegetation

	This can be useful for holding internal cached state.
*//***************************************************************************/
class FE_DL_EXPORT SketchI:
	virtual public Component,
	public CastableAs<SketchI>
{
	public:

#if FE_VEG_VIEWER
virtual	void				bind(sp<CameraControllerI> spCameraControllerI)	=0;
#endif
virtual	void				bind(sp<Component> spComponent)					=0;
virtual	void				draw(sp<DrawI> spDrawI)							=0;

virtual	SpatialTransform	transform(void) const							=0;
virtual	void				setTransform(SpatialTransform a_transform)		=0;

virtual	void				setPaused(BWORD paused)							=0;
virtual	BWORD				paused(void) const								=0;

virtual	String				stickForm(void)									=0;
virtual	void				setStickForm(String a_stickForm)				=0;

virtual	String				leafForm(void)									=0;
virtual	void				setLeafForm(String a_leafForm)					=0;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __vegetation_SketchI_h__ */
