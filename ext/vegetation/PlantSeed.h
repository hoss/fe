/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __vegetation_PlantSeed_h__
#define __vegetation_PlantSeed_h__

FE_ATTRIBUTE("veg:modelname",	"Name of PlantModelI Component");

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief PlantSeed RecordView

	@ingroup vegetation
*//***************************************************************************/
class FE_DL_EXPORT PlantSeed: virtual public RecordView
{
	public:
		Functor<String>				modelname;

				PlantSeed(void)		{ setName("PlantSeed"); }
virtual	void	addFunctors(void)
				{
					add(modelname,	FE_USE("veg:modelname"));
				}
virtual	void	initializeRecord(void)
				{
				}
};

} /* namespace ext */
} /* namespace fe */

#endif /* __vegetation_PlantSeed_h__ */


