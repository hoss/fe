/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <vegetation/vegetation.pmh>

namespace fe
{
namespace ext
{

DrawPlant::DrawPlant(void)
{
	setIdentity(m_up[0]);
	rotate(m_up[0],0.5*M_PI,e_yAxis);

	setIdentity(m_up[1]);
	rotate(m_up[1],-0.5*M_PI,e_xAxis);

	setIdentity(m_up[2]);
}

void DrawPlant::handle(Record& record)
{
	m_forestRV.bind(record);
	sp<DrawI> spDrawI=m_forestRV.drawI();
	if(spDrawI.isNull())
	{
		return;
	}

	if(!m_plantRAV.scope().isValid())
	{
		sp<Scope> spScope=record.layout()->scope();
		m_plantRAV.bind(spScope);
	}

	const I32 upAxis=m_forestRV.upAxis();
	const F32 deltaT=m_forestRV.deltaT();
#ifdef FE_VEG_VIEWER
	sp<CameraControllerI> spCameraControllerI=m_forestRV.cameraControllerI();
#endif

	sp<RecordGroup> spRG=m_forestRV.plants();
	FEASSERT(spRG.isValid());

	for(RecordGroup::iterator it=spRG->begin();it!=spRG->end();it++)
	{
		sp<RecordArray> spRA= *it;
		m_plantRAV.bind(spRA);

		if(!m_plantRAV.recordView().plantModel.check(spRA))
		{
			continue;
		}

		for(Plant& plantRV: m_plantRAV)
		{
			if(plantRV.plantModel().isNull())
			{
				continue;
			}

			sp<SketchI> spSketchI=plantRV.plantSketch();
			if(!spSketchI.isValid())
			{
				spSketchI=registry()->create("*.PlantSketch");
				plantRV.plantSketch()=spSketchI;

				spSketchI->bind(plantRV.plantModel());
#ifdef FE_VEG_VIEWER
				spSketchI->bind(spCameraControllerI);
#endif
			}
			FEASSERT(spSketchI.isValid());
			spSketchI->setStickForm(m_forestRV.stickForm());
			spSketchI->setLeafForm(m_forestRV.leafForm());
			spSketchI->setTransform(m_up[upAxis]);
			spSketchI->setPaused(deltaT==0.0f);
			spSketchI->draw(spDrawI);
		}
	}
}

} /* namespace ext */
} /* namespace fe */
