/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __vegetation_LimberOp_h__
#define __vegetation_LimberOp_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Combine disjoint curves into a single hierarchy

	@ingroup vegetation
*//***************************************************************************/
class FE_DL_EXPORT LimberOp:
	public OperatorSurfaceCommon,
	public Initialize<LimberOp>
{
	public:

				LimberOp(void);
virtual			~LimberOp(void);

		void	initialize(void);

				//* As HandlerI
virtual	void	handle(Record& a_rSignal);

	private:
};

} /* namespace ext */
} /* namespace fe */

#endif /* __vegetation_LimberOp_h__ */

