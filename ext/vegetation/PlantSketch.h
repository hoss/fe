/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __vegetation_PlantSketch_h__
#define __vegetation_PlantSketch_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Plant rendering cache

	@ingroup vegetation
*//***************************************************************************/
class FE_DL_EXPORT PlantSketch: virtual public SketchI,
		public Initialize<PlantSketch>
{
	public:
							PlantSketch(void)								{}
		void				initialize(void);
virtual						~PlantSketch(void);

#ifdef FE_VEG_VIEWER
virtual	void				bind(sp<CameraControllerI> spCameraControllerI)
							{	m_spCameraControllerI=spCameraControllerI; }
#endif

virtual	void				bind(sp<Component> spComponent);
virtual	void				draw(sp<DrawI> spDrawI);

virtual	SpatialTransform	transform(void) const		{ return m_transform; }
virtual	void				setTransform(SpatialTransform a_transform)
							{	m_transform=a_transform; }

virtual	BWORD				paused(void) const			{ return m_paused; }
virtual	void				setPaused(BWORD paused)		{ m_paused=paused; }

virtual	String				stickForm(void)				{ return m_stickForm; }
virtual	void				setStickForm(String a_stickForm)
							{	m_stickForm=a_stickForm; }

virtual	String				leafForm(void)				{ return m_leafForm; }
virtual	void				setLeafForm(String a_leafForm)
							{	m_leafForm=a_leafForm; }

	private:
		SpatialTransform		m_transform;

#ifdef FE_VEG_VIEWER
		sp<CameraControllerI>	m_spCameraControllerI;
#endif

		sp<PlantModelI>			m_spPlantModelI;
		sp<DrawMode>			m_spCylinderDrawMode;
		sp<DrawMode>			m_spLineDrawMode;
		sp<DrawMode>			m_spLeafDrawMode;
		SpatialVector*			m_pOldLines;
		SpatialVector*			m_pShadowLines;
		SpatialVector*			m_pLines;
		SpatialVector*			m_pLineNormals;
		Real*					m_pLineRadius;
		BWORD					m_paused;
		String					m_stickForm;
		String					m_leafForm;

		sp<Profiler>			m_spProfiler;
		sp<Profiler::Profile>	m_spProfileStartup;
		sp<Profiler::Profile>	m_spProfileCamera;
		sp<Profiler::Profile>	m_spProfileIterate;
		sp<Profiler::Profile>	m_spProfileSpan;
		sp<Profiler::Profile>	m_spProfileLineNormals;
		sp<Profiler::Profile>	m_spProfileDetails;
		sp<Profiler::Profile>	m_spProfilePoints;
		sp<Profiler::Profile>	m_spProfileCylinders;
		sp<Profiler::Profile>	m_spProfileDrawCylinders;
		sp<Profiler::Profile>	m_spProfileShadows;
		sp<Profiler::Profile>	m_spProfileText;
		sp<Profiler::Profile>	m_spProfileLines;
		sp<Profiler::Profile>	m_spProfileLeaves;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __vegetation_PlantSketch_h__ */

