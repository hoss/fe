/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <vegetation/vegetation.pmh>

#define	FE_LBO_DEBUG	FALSE

using namespace fe;
using namespace fe::ext;

LimberOp::LimberOp(void)
{
}

LimberOp::~LimberOp(void)
{
}

void LimberOp::initialize(void)
{
	catalog<String>("icon")="FE_alpha";

	catalog<Real>("maxDistance")=1.0;
	catalog<Real>("maxDistance","high")=10.0;
	catalog<Real>("maxDistance","max")=1e6;
	catalog<String>("maxDistance","label")="Max Distance";
	catalog<String>("maxDistance","hint")=
			"Limit of how far each curve base looks for an attachment point.";

	catalog<bool>("preserveRoots")=FALSE;
	catalog<String>("preserveRoots","label")="Preserve Roots";
	catalog<String>("preserveRoots","hint")=
			"Prevent certain points from being reattached.";

	catalog<String>("rootGroup")="root";
	catalog<String>("rootGroup","label")="Root Group";
	catalog<String>("rootGroup","enabler")="preserveRoots";
	catalog<String>("rootGroup","suggest")="pointGroups";
	catalog<String>("rootGroup","hint")=
			"Group of points that will never be reattached.";

	catalog< sp<Component> >("Input Surface");
}

void LimberOp::handle(Record& a_rSignal)
{
#if FE_LBO_DEBUG
	feLog("LimberOp::handle\n");
#endif

	const Real maxDistance=catalog<Real>("maxDistance");

	const String rootGroup=catalog<String>("rootGroup");
	const BWORD preserveRoots=
			(catalog<bool>("preserveRoots") && !rootGroup.empty());

	String& rSummary=catalog<String>("summary");
	rSummary="";

	sp<SurfaceAccessibleI> spInputAccessible;
	if(!access(spInputAccessible,"Input Surface")) return;

	sp<SurfaceI> spInput;
	if(!access(spInput,spInputAccessible)) return;

	sp<SurfaceAccessibleI> spOutputAccessible;
	if(!accessOutput(spOutputAccessible,a_rSignal)) return;

	sp<SurfaceAccessorI> spOutputVertices;
	if(!access(spOutputVertices,spOutputAccessible,
			e_primitive,e_vertices)) return;

	sp<SurfaceAccessorI> spOutputAttached;
	if(!access(spOutputAttached,spOutputAccessible,
			e_primitive,"attached")) return;

	const I32 pointCount=spInputAccessible->count(SurfaceAccessibleI::e_point);

	Array<I32> pointRooted(pointCount,FALSE);

	sp<SurfaceAccessorI> spInputRoot;
	if(preserveRoots)
	{
		if(!access(spInputRoot,spInputAccessible,
				e_pointGroup,rootGroup)) return;

		const U32 rootCount=spInputRoot->count();
		for(U32 rootIndex=0;rootIndex<rootCount;rootIndex++)
		{
			const I32 pointIndex=spInputRoot->integer(rootIndex);
			pointRooted[pointIndex]=TRUE;
		}
	}

	Array<I32> isBase(pointCount);
	Array<I32> mapping(pointCount);
	for(I32 pointIndex=0;pointIndex<pointCount;pointIndex++)
	{
		isBase[pointIndex]=FALSE;
		mapping[pointIndex]= -1;
	}

	const I32 primitiveCount=spOutputVertices->count();
	Array<I32> parentPrimitive(primitiveCount);
	for(I32 primitiveIndex=0;primitiveIndex<primitiveCount;primitiveIndex++)
	{
		parentPrimitive[primitiveIndex]= -1;

		const I32 subCount=spOutputVertices->subCount(primitiveIndex);
		if(subCount)
		{
			const I32 baseIndex=spOutputVertices->integer(primitiveIndex,0);
			isBase[baseIndex]=TRUE;
		}
	}

	I32 attachCount=0;

	for(I32 primitiveIndex=0;primitiveIndex<primitiveCount;primitiveIndex++)
	{
		spOutputAttached->set(primitiveIndex,I32(0));

		const I32 pointIndex=
				spOutputVertices->integer(primitiveIndex,0);
		if(pointRooted[pointIndex])
		{
			continue;
		}

		const I32 subCount=spOutputVertices->subCount(primitiveIndex);
		if(!subCount)
		{
			continue;
		}

		const SpatialVector basePoint=
				spOutputVertices->spatialVector(primitiveIndex,0);
		const I32 hitLimit=16;
		sp<PartitionI> spPartition(NULL);

		Array< sp<SurfaceI::ImpactI> > impactArray=spInput->nearestPoints(
				basePoint,maxDistance,hitLimit,spPartition);

		const I32 impactCount=impactArray.size();

#if FE_LBO_DEBUG
		feLog("prim %d/%d pt %d at %s hits %d\n",
				primitiveIndex,primitiveCount,pointIndex,
				c_print(basePoint),impactCount);
#endif

		for(I32 impactIndex=0;impactIndex<impactCount;impactIndex++)
		{
			sp<SurfaceI::ImpactI> spImpact=impactArray[impactIndex];
			const I32 impactPrimitiveIndex=spImpact->primitiveIndex();
			if(impactPrimitiveIndex==primitiveIndex)
			{
				continue;
			}

			BWORD cycle=FALSE;
			I32 checkIndex=impactPrimitiveIndex;
			while(TRUE)
			{
				const I32 attachIndex=parentPrimitive[checkIndex];
				if(attachIndex<0)
				{
					break;
				}
				if(attachIndex==primitiveIndex)
				{
					cycle=TRUE;
					break;
				}
				checkIndex=attachIndex;
			}
			if(cycle)
			{
#if FE_LBO_DEBUG
				feLog("  pr %d cycles\n",impactPrimitiveIndex);
#endif
				continue;
			}

			sp<SurfaceCurves::Impact> spCurveImpact=spImpact;
			if(spCurveImpact.isValid())
			{
				const I32 pointIndex0=spCurveImpact->pointIndex0();
				const I32 pointIndex1=spCurveImpact->pointIndex1();

				//* HACK testing
				if(isBase[pointIndex0])
				{
					continue;
				}

				const SpatialVector point0=spCurveImpact->vertex0();
				const SpatialVector point1=spCurveImpact->vertex1();

				const SpatialVector arm=unitSafe(point1-point0);
				const SpatialVector toBase=unitSafe(basePoint-point0);
				const Real baseDot=dot(arm,toBase);

#if FE_LBO_DEBUG
				const SpatialBary bary=spImpact->barycenter();
				feLog("  pr %d pt %d,%d along %.6G dot %.6G\n",
						impactPrimitiveIndex,
						pointIndex0,pointIndex1,bary[0],baseDot);
#endif

				if(baseDot<0.0)
				{
					continue;
				}

				const BWORD preferLower=TRUE;	//* TODO param

				I32 hitIndex=pointIndex0;
				if(isBase[pointIndex0])
				{
					hitIndex=pointIndex1;
				}
				else if(!preferLower)
				{
					const Real mag20=magnitudeSquared(point0-basePoint);
					const Real mag21=magnitudeSquared(point1-basePoint);
					if(mag21<mag20)
					{
						hitIndex=pointIndex1;
					}
				}

				const I32 mapIndex=mapping[hitIndex];
				if(mapIndex>=0)
				{
					hitIndex=mapIndex;
				}

				spOutputVertices->set(primitiveIndex,0,hitIndex);

				const I32 baseIndex=
						spOutputVertices->integer(primitiveIndex,0);
				mapping[baseIndex]=hitIndex;

				spOutputAttached->set(primitiveIndex,I32(1));
				parentPrimitive[primitiveIndex]=impactPrimitiveIndex;

				attachCount++;

				break;
			}
		}
	}

	rSummary.sPrintf("%d points attached",attachCount);

#if FE_LBO_DEBUG
	feLog("LimberOp::handle done\n");
#endif
}
