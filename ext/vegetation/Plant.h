/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __vegetation_Plant_h__
#define __vegetation_Plant_h__

FE_ATTRIBUTE("veg:plantmodel",	"Physical vegetation sp<Component>");
FE_ATTRIBUTE("veg:plantsketch",	"Drawable SketchI in sp<Component>");
FE_ATTRIBUTE("veg:plantname",	"Growth data lookup key");

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Plant RecordView

	@ingroup vegetation
*//***************************************************************************/
class FE_DL_EXPORT Plant: public RecordView
{
	public:
		Functor< sp<Component> >	plantModel;
		Functor< sp<Component> >	plantSketch;
		Functor<String>				plantName;
		Functor<SpatialVector>		location;	//* collective displacement
		Functor<SpatialVector>		offset;		//* start of separate root
		Functor<Real>				reactivity;
		Functor<Real>				threshold;
//~		Functor<BWORD>				collideSphere;
		Functor<String>				collisionMethod;
		Functor<BWORD>				collideEnd;
		Functor<Real>				repulsion;
		Functor<Real>				repulsionFalloff;
		Functor<Real>				depletion;
		Functor<Real>				depletionFalloff;
		Functor<Real>				windHampering;
		Functor<Real>				windFalloff;
		Functor<Real>				plasticity;

				Plant(void)			{ setName("Plant"); }
virtual	void	addFunctors(void)
				{
					add(plantModel,			FE_USE("veg:plantmodel"));
					add(plantSketch,		FE_USE("veg:plantsketch"));
					add(plantName,			FE_USE("veg:plantname"));
					add(location,			FE_USE("spc:at"));
					add(offset,				FE_USE("spc:offset"));
					add(reactivity,			FE_USE("veg:reactivity"));
					add(threshold,			FE_USE("spc:threshold"));
//~					add(collideSphere,		FE_USE("spc:collideSphere"));
					add(collisionMethod,	FE_USE("spc:collisionMethod"));
					add(collideEnd,			FE_USE("spc:collideEnd"));
					add(repulsion,			FE_USE("spc:repulsion"));
					add(repulsionFalloff,	FE_USE("spc:repulsionFalloff"));
					add(depletion,			FE_USE("spc:depletion"));
					add(depletionFalloff,	FE_USE("spc:depletionFalloff"));
					add(windHampering,		FE_USE("spc:windHampering"));
					add(windFalloff,		FE_USE("spc:windFalloff"));
					add(plasticity,			FE_USE("spc:plasticity"));
				}
virtual	void	initializeRecord(void)
				{
					plantModel.attribute()->setSerialize(FALSE);
					plantSketch.attribute()->setSerialize(FALSE);

					set(location());
					set(offset());

					reactivity()=0.0;
					threshold()=0.0;
//~					collideSphere()=FALSE;
					collisionMethod()="";
					collideEnd()=FALSE;
					repulsion()=1.0;
					repulsionFalloff()=0.0;
					depletion()=1.0;
					depletionFalloff()=0.0;
					windHampering()=0.0;
					windFalloff()=0.0;
					plasticity()=0.0;
				}
};

} /* namespace ext */
} /* namespace fe */

#endif /* __vegetation_Plant_h__ */

