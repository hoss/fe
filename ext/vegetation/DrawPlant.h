/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __vegetation_DrawPlant_h__
#define __vegetation_DrawPlant_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Draw all plants

	@ingroup vegetation
*//***************************************************************************/
class FE_DL_EXPORT DrawPlant: virtual public HandlerI
{
	public:
				DrawPlant(void);

				//* as HandlerI
virtual void	handle(Record &record);

	private:
		Forest					m_forestRV;
		RecordArrayView<Plant>	m_plantRAV;

		SpatialTransform		m_up[3];
};

} /* namespace ext */
} /* namespace fe */

#endif /* __vegetation_DrawPlant_h__ */

