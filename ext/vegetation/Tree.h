/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __vegetation_Tree_h__
#define __vegetation_Tree_h__

namespace fe
{
namespace ext
{

typedef DenseVector<F64> VectorN;
typedef SparseMatrix<F64> MatrixN;

/**************************************************************************//**
	@brief Vegetation model for a Dynamic Tree

	@ingroup vegetation
*//***************************************************************************/
class FE_DL_EXPORT Tree: virtual public PlantModelI, public Initialize<Tree>
{
	public:
							Tree(void)										{}
virtual						~Tree(void);

		void				initialize(void);
virtual	void				reset(void);
		void				start(void);
		void				stop(void);

							//* as PlantModelI
virtual	void				generate(const Record seed);
virtual	void				addBranch(U32 a_fromIndex,U32 a_toIndex,
									U32 a_level,Real a_radius1,Real a_radius2,
									const SpatialVector& a_span,
									Real a_rigidity);
virtual	void				prepare(void);
virtual	void				update(const Real deltaT);

virtual	U32					numSticks(void) const
							{	FEASSERT(m_sticks==m_stickArray.size());
								return m_sticks; }
virtual	PlantModelI::Stick*	stick(U32 index)
							{	FEASSERT(index<m_stickMap.size());
								I32 compactIndex=m_stickMap[index];
								return compactIndex<0? NULL:
										m_stickArray[compactIndex]; }

virtual	U32					numLeaves(void) const
							{	return m_leafArray.size(); }
virtual	PlantModelI::Leaf*	leaf(U32 index)
							{	FEASSERT(index<numLeaves());
								return m_leafArray[index]; }
virtual
const	SpatialVector*		leafPoints(void) const	{ return m_pLeafPoints; }
virtual
const	SpatialVector*		leafNormals(void) const	{ return m_pLeafNormals; }

virtual	U32					stickBundles(void) const
							{	FEASSERT(m_bundles==m_bundleSize.size());
								return m_bundles; }
virtual	U32					stickBundleSize(U32 bundle) const
							{	FEASSERT(bundle<stickBundles());
								return m_bundleSize[bundle]; }
virtual
const	SpatialTransform*	stickTransforms(U32 bundle) const
							{	FEASSERT(bundle<stickBundles());
								return m_stickTransformBundles[bundle]; }
virtual
const	SpatialVector*		stickScale(U32 bundle) const
							{	FEASSERT(bundle<stickBundles());
								return m_stickScaleBundles[bundle]; }
virtual
const	Real*				stickBaseScale(U32 bundle) const
							{	FEASSERT(bundle<stickBundles());
								return m_stickBaseScaleBundles[bundle]; }
virtual
const	U32*				stickSlices(U32 bundle) const
							{	FEASSERT(bundle<stickBundles());
								return m_stickSliceBundles[bundle]; }

virtual	void				setLocation(const SpatialVector& location)
							{	m_location=location; }
virtual	void				setOffset(const SpatialVector& offset)
							{	m_offset=offset; }

virtual	sp<SurfaceI>		collider(void)	{ return m_spCollider; }
virtual	void				setCollider(sp<SurfaceI> a_spCollider)
							{	m_spCollider=a_spCollider; }

virtual	SpatialTransform&	colliderTransform(void)
							{	return m_colliderTransform; }
virtual	void				setColliderTransform(
								const SpatialTransform& a_rColliderTransform)
							{	m_colliderTransform=a_rColliderTransform; }

virtual SpatialVector&		effectorForce(void)
							{	return m_effectorForce; }

virtual	void				setGravity(const SpatialVector& a_gravity)
							{	m_gravity=a_gravity; }
virtual	void				setUniformVelocity(const SpatialVector& a_velocity)
							{	m_uniformVelocity=a_velocity; }
virtual	void				setUniformRigidity(const Real a_rigidity)
							{	m_uniformRigidity=a_rigidity; }
virtual	void				setDamping(const Real a_damping)
							{	m_damping=a_damping; }
virtual	void				setReactivity(const Real a_reactivity)
							{	m_reactivity=a_reactivity; }
virtual	void				setThreshold(const Real a_threshold)
							{	m_threshold=a_threshold; }
virtual	void				setCollisionMethod(const String a_collisionMethod)
							{	m_collisionMethod=a_collisionMethod; }
virtual	void				setCollideEnd(const BWORD a_collideEnd)
							{	m_collideEnd=a_collideEnd; }
virtual	void				setRepulsion(const Real a_repulsion)
							{	m_repulsion=a_repulsion; }
virtual	void				setRepulsionFalloff(const Real a_repulsionFalloff)
							{	m_repulsionFalloff=a_repulsionFalloff; }
virtual	void				setDepletion(const Real a_depletion)
							{	m_depletion=a_depletion; }
virtual	void				setDepletionFalloff(const Real a_depletionFalloff)
							{	m_depletionFalloff=a_depletionFalloff; }
virtual	void				setWindHampering(const Real a_windHampering)
							{	m_windHampering=a_windHampering; }
virtual	void				setWindFalloff(const Real a_windFalloff)
							{	m_windFalloff=a_windFalloff; }
virtual	void				setPlasticity(const Real a_plasticity)
							{	m_plasticity=a_plasticity; }
virtual	void				setCompensation(const BWORD a_compensation)
							{	m_compensation=a_compensation; }
virtual	void				setCorrection(const BWORD a_correction)
							{	m_correction=a_correction; }

	/// @brief Leaf on a Tree
	class Leaf:
		virtual public PlantModelI::Leaf,
		public CastableAs<Leaf>
	{
		public:
							Leaf(void)
							{
								set(m_center);
								set(m_normal);
							}
	virtual							~Leaf(void)								{}
	virtual const SpatialVector&	center(void) const	{ return m_center; }
	virtual const SpatialVector&	normal(void) const	{ return m_normal; }

			SpatialVector	m_center;
			SpatialVector	m_normal;
	};

	/// @brief Segment of a branch
	class Stick:
		virtual public PlantModelI::Stick,
		public CastableAs<Stick>
	{
		public:
							Stick(void)
							{
								m_children.setAutoDestruct(TRUE);
								reset();
							}
	virtual					~Stick(void)									{}

			void			reset(void)
							{
								m_pParent=NULL;
								m_index=0;
								m_level=0;
								m_targeted=0;
								m_mass=1.0f;
								m_length=0.0f;
								m_spring=0.0f;
								m_drag=0.0f;
								m_radius1=1.0f;
								m_radius2=1.0f;
								m_resolution=0;
								m_freedom=1.0;

								set(m_base);
								set(m_span);
								set(m_windVelocity);
								set(m_target);
								set(m_rest);
								set(m_position);
								set(m_velocity);
								set(m_lastAddPosition);
								set(m_lastAddDelta);
								set(m_contact);
								set(m_intensity);

								m_children.deleteAll();
								m_leafIndexArray.clear();
							}

										//* as PlantModelI::Stick
	virtual const SpatialVector&		base(void) const	{ return m_base; }
	virtual const SpatialVector&		span(void) const	{ return m_span; }
	virtual const SpatialQuaternion&	rotation(void) const
										{	return m_rotation; }
	virtual Real						radius1(void) const
										{	return m_radius1; }
	virtual Real						radius2(void) const
										{	return m_radius2; }
	virtual I32							resolution(void) const
										{	return m_resolution; }
	virtual String						stateString(void) const
										{
											String result;
											result.sPrintf(
													" %d %.2f %.2f",
													m_index,
													m_rest[0]+m_position[0],
													m_rest[1]+m_position[1]);
											return result;
										}
	virtual	void						setWindVelocity(
											const SpatialVector& a_windVelocity)
										{	m_windVelocity=a_windVelocity; }
	virtual	void						setTarget(
											const SpatialVector& a_target)
										{	m_target=a_target; }
	virtual	void						setTargeted(
											const I32 a_targeted)
										{	m_targeted=a_targeted; }

			void			attach(Stick* pChild)
							{
								m_children.append(pChild);
								pChild->m_pParent=this;
							}
			void			attachLeaf(U32 leafIndex)
							{
								const U32 size=m_leafIndexArray.size();
								m_leafIndexArray.resize(size+1);
								m_leafIndexArray[size]=leafIndex;
							}

			void			grow(sp<Tree>& rspTree,U32& stick,
									const U32 segment,const U32 level,
									const F32 a_fullLength,const F32 along,
									const F32 baseRotate,const F32 rotate);
			void			populate_static(sp<Tree>& rspTree,
									MatrixN* dfdx,MatrixN* dfdv,
									VectorN& invMass);
			Real			effectOfEffector(sp<Tree>& rspTree,
									const SpatialVector& a_effector,
									SpatialVector& a_effect);
			void			populate_dynamic(sp<Tree>& rspTree,
									VectorN* force,
									VectorN* velocity,
									VectorN* addPosition,
									VectorN* addVelocity,
									bool rekine);
			void			forward_kine(U32 thread,
									sp<Tree>& rspTree,const F32 deltaT,
									const VectorN* deltaV,
									const VectorN* addPosition,
									const VectorN* addVelocity,
									SpatialVector& rEffectorForce,
									BWORD rebundle);
			void			forward_kine_recursive(U32 thread,
									sp<Tree>& rspTree,
									const F32 deltaT,
									const VectorN* deltaV,
									const VectorN* addPosition,
									const VectorN* addVelocity,
									SpatialVector& rEffectorForce,
									BWORD only_zero);

			Stick*				m_pParent;
			List<Stick*>		m_children;
			Array<U32>			m_leafIndexArray;
			U32					m_index;
			U32					m_level;

			SpatialVector		m_base;
			SpatialVector		m_span;
			SpatialVector		m_dir[2];	//* world dir of an angle change
			SpatialVector		m_windVelocity;
			SpatialVector		m_target;
			I32					m_targeted;
			F32					m_mass;
			F32					m_length;
			F32					m_spring;	//* angular spring
			F32					m_drag;		//* bending, not viscosity
			F32					m_radius1;
			F32					m_radius2;
			I32					m_resolution;
			Vector2				m_absRest;	//* for addBranch
			SpatialVector		m_dirRest;	//* for addBranch
			Vector2				m_rest;
			Vector2				m_position;
			Vector2				m_velocity;
			SpatialQuaternion	m_rotation;
			SpatialQuaternion	m_correction;
			SpatialTransform	m_transform;
			Vector2				m_intensity;
			Real				m_freedom;

								//* TODO list of

								//* decay adjustment after release
			Vector2				m_lastAddPosition;
			Vector2				m_lastAddDelta;

								//* debug
			SpatialVector		m_contact;
			SpatialVector		m_contact2;
	};

		void				addKine(Tree::Stick* pStick)
							{
								U32 index=m_kineArray.size();
								m_kineArray.resize(index+1,NULL);
								m_kineArray[index]=pStick;
							}
		void				setStick(U32 index,Tree::Stick* pStick)
							{
								if(index>=m_stickArray.size())
								{
									m_stickArray.resize(index+1,NULL);
								}
								m_stickArray[index]=pStick;
							}

		U32					newLeaf(void)
							{
								const U32 size=m_leafArray.size();
								m_leafArray.resize(size+1);
								m_leafArray[size]=new Leaf();
								return size;
							}

		void				forward_kine_thread(U32 thread,I32 index);

	private:
static	F32							shapeFunction(const F32 along,
											const F32 valleybase,
											const F32 valleytip,
											const F32 peakAlong,
											const F32 powerbase,
											const F32 powertip);
		void						bindLevel(const U32 level);

		TreeSeed					m_seedRV;
		StickLevel					m_levelRV;
		I32							m_boundLevel;

		F32							m_lastDeltaT;
		U32							m_sticks;
		Stick						m_root;
		Array<Tree::Stick*>			m_kineArray;		//* TODO not pointers
		Array<Tree::Stick*>			m_stickArray;		//* TODO not pointers
		Array<I32>					m_stickMap;			//* before compacting
		Array<Tree::Leaf*>			m_leafArray;
		SpatialVector*				m_pLeafPoints;
		SpatialVector*				m_pLeafNormals;

		Array<SpatialTransform*>	m_stickTransformBundles;
		Array<SpatialVector*>		m_stickScaleBundles;
		Array<Real*>				m_stickBaseScaleBundles;
		Array<U32*>					m_stickSliceBundles;
		Array<U32>					m_bundleSize;
		U32							m_bundles;

		SpatialVector				m_location;
		SpatialVector				m_offset;
		SpatialQuaternion			m_rotation;

		sp<SurfaceI>				m_spCollider;
		SpatialTransform			m_colliderTransform;
		SpatialVector				m_effectorForce;	//* TODO list of
		Array<SpatialVector>		m_effectorForceArray;

		SpatialVector				m_gravity;
		SpatialVector				m_uniformVelocity;
		Real						m_uniformRigidity;
		Real						m_damping;
		Real						m_reactivity;
		Real						m_threshold;
		String						m_collisionMethod;
		BWORD						m_collideEnd;
		BWORD						m_compensation;
		BWORD						m_correction;
		I32							m_useCorrection;	//* 0=no 1=store 2=apply
		Real						m_repulsion;
		Real						m_repulsionFalloff;
		Real						m_depletion;
		Real						m_depletionFalloff;
		Real						m_windHampering;
		Real						m_windFalloff;
		Real						m_plasticity;

		sp<Profiler>				m_spProfiler;
		sp<Profiler::Profile>		m_spProfileReset;
		sp<Profiler::Profile>		m_spProfilePopulate;
		sp<Profiler::Profile>		m_spProfileSolve;
		sp<Profiler::Profile>		m_spProfileSolveWait;
		sp<Profiler::Profile>		m_spProfileThreadWait;
		sp<Profiler::Profile>		m_spProfileSetup[2];
		sp<Profiler::Profile>		m_spProfileSolver[2];
		sp<Profiler::Profile>		m_spProfileKine;
		sp<Profiler::Profile>		m_spProfileKineWait;

		Array< sp<Profiler::Profile> >	m_spProfileKineLimbArray;

		VectorN						m_force[2];
		VectorN						m_velocity[2];
		VectorN						m_addPosition[2];
		VectorN						m_addVelocity[2];
		VectorN						m_tempVector1[2];
		VectorN						m_tempVector2[2];
		VectorN						m_deltaV[2];
		VectorN						m_b[2];

		// diagonal matrices represented as vectors
		VectorN						m_mass;
		VectorN						m_im;
		VectorN						m_him;
		VectorN						m_invSqrtA[2];

		MatrixN						m_A[2];
//		MatrixN						m_A2[2];
		MatrixN						m_tempMatrix[2];
		MatrixN						m_dfdx[2];
//		MatrixN						m_dfdx2[2];
//		MatrixN						m_hdfdx[2];
		MatrixN						m_imdfdx[2];
		MatrixN						m_imdfdv[2];
		MatrixN						m_identity;

		ConjugateGradient<MatrixN,VectorN>	m_solver[2];

		void						solve(U32 m);
		void						runSolvers(void);
		void						runKine(void);

	class SolverWorker: public Thread::Functor
	{
		public:
							SolverWorker(sp< JobQueue<I32> > a_spJobQueue,
									U32 a_id,String a_stage):
								m_id(a_id),
								m_hpJobQueue(a_spJobQueue)					{}

	virtual	void			operate(void);

			void			setObject(sp<Counted> spTree)
							{	m_hpTree=sp<Component>(spTree); }

		private:
			U32					m_id;
			hp< JobQueue<I32> > m_hpJobQueue;
			hp<Tree>			m_hpTree;
	};

		sp< JobQueue<I32> >				m_spJobQueue;
		sp< Gang<SolverWorker,I32> >	m_spGang;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __vegetation_Tree_h__ */
