/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __vegetation_LeafLevel_h__
#define __vegetation_LeafLevel_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief LeafLevel RecordView

	@ingroup vegetation
*//***************************************************************************/
class FE_DL_EXPORT LeafLevel: virtual public TreeLevel
{
	public:

				LeafLevel(void)		{ setName("LeafLevel"); }
virtual	void	addFunctors(void)
				{
					TreeLevel::addFunctors();
				}
virtual	void	initializeRecord(void)
				{
					TreeLevel::initializeRecord();
				}
};

} /* namespace ext */
} /* namespace fe */

#endif /* __vegetation_LeafLevel_h__ */



