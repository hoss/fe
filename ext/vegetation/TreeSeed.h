/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __vegetation_TreeSeed_h__
#define __vegetation_TreeSeed_h__

FE_ATTRIBUTE("veg:levelgroup",	"RecordGroup of TreeLevel specifications");
FE_ATTRIBUTE("veg:stiffness",	"Stem springiness");
FE_ATTRIBUTE("veg:stillness",	"Stem structural drag");
FE_ATTRIBUTE("veg:density",		"Mass per volume (kg/m^3)");
FE_ATTRIBUTE("veg:levels",		"Depth of levels generated");

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief TreeSeed RecordView

	@ingroup vegetation
*//***************************************************************************/
class FE_DL_EXPORT TreeSeed: public PlantSeed
{
	public:
		Functor< sp<RecordGroup> >	levelgroup;
		Functor<F32>				stiffness;
		Functor<F32>				stillness;
		Functor<F32>				density;
		Functor<I32>				levels;

				TreeSeed(void)		{ setName("TreeSeed"); }
virtual	void	addFunctors(void)
				{
					PlantSeed::addFunctors();

					add(levelgroup,	FE_USE("veg:levelgroup"));
					add(stiffness,	FE_USE("veg:stiffness"));
					add(stillness,	FE_USE("veg:stillness"));
					add(density,	FE_USE("veg:density"));
					add(levels,		FE_USE("veg:levels"));
				}
virtual	void	initializeRecord(void)
				{
					PlantSeed::initializeRecord();

					stiffness()=1.0f;
					stillness()=1.0f;
					density()=1e3f;
					levels()=1;
				}
};

} /* namespace ext */
} /* namespace fe */

#endif /* __vegetation_TreeSeed_h__ */

