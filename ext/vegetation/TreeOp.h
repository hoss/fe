/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __vegetation_OperatorTree_h__
#define __vegetation_OperatorTree_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Handler to Sim and Draw a Tree

	@ingroup vegetation
*//***************************************************************************/
class FE_DL_EXPORT TreeOp:
	public OperatorThreaded,
	public Initialize<TreeOp>
{
	public:

							TreeOp(void);
virtual						~TreeOp(void);

		void				initialize(void);

							//* As HandlerI
virtual	void				handle(Record& a_rSignal);

							using OperatorThreaded::run;

virtual	void				run(I32 a_id,sp<SpannedRange> a_spSpannedRange,
									String a_stage);

	private:

		SpatialVector		convertY(SpatialVector a_vector);

		SpatialVector		unconvertY(SpatialVector a_vector);

		BWORD				rebuild(void);
		void				rebuildPlants(I32 a_start,I32 a_count);

		BWORD				updateInput(void);
		void				updateInputPlants(I32 a_start,I32 a_count);

		void				replace(void);
		void				replacePlants(I32 a_start,I32 a_count);

		void				regenerate(void);

		sp<SurfaceAccessibleI>		m_spInputAccessible;
		sp<SurfaceAccessibleI>		m_spReplaceAccessible;
		sp<SurfaceAccessibleI>		m_spPayloadAccessible;

		BWORD						m_yUp;
		Real						m_lastFrame;
		I32							m_lastPointCount;
		I32							m_lastPrimitiveCount;

		Real						m_time;
		String						m_treeName;

		Forest						m_forestRV;
		Plant						m_plantRV;
		RecordArrayView<Plant>		m_plantRAV;
		sp<Scope>					m_spScope;
		sp<SignalerI>				m_spSignalerI;

		Array< std::map<U32,U32> >	m_plantPointMap;
		Array<SpatialVector>		m_plantOffset;
		Array< Array<U32> >			m_primitivesOfPlant;
		Array< sp<PlantModelI> >	m_modelOfPlant;

		Array< sp<HandlerI> >		m_modifierArray;

	class TreeWorker: public Thread::Functor
	{
		public:
							TreeWorker(sp< JobQueue<I32> > a_spJobQueue,
									U32 a_id,String a_stage):
								m_id(a_id),
								m_stage(a_stage),
								m_hpJobQueue(a_spJobQueue)					{}

	virtual	void			operate(void);

			void			setObject(sp<Counted> spCounted)
							{	m_hpTreeOp=sp<Component>(spCounted); }

		private:
			U32					m_id;
			String				m_stage;
			hp< JobQueue<I32> >	m_hpJobQueue;
			hp<TreeOp>			m_hpTreeOp;
	};

		sp< Gang<TreeWorker,I32> >	m_spGang;
		I32							m_bundleSize;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __vegetation_OperatorTree_h__ */

