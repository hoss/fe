/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __vegetation_StickLevel_h__
#define __vegetation_StickLevel_h__

FE_ATTRIBUTE("veg:segments",	"Divisions per stem");
FE_ATTRIBUTE("veg:forks",		"Number of segments that fork");
FE_ATTRIBUTE("veg:sideshoots",	"Number of shoots per fork");
FE_ATTRIBUTE("veg:forkrotate",	"Spiral angle between forks");
FE_ATTRIBUTE("veg:forktrench",	"Alternately add/subtract to spiral angle");
FE_ATTRIBUTE("veg:shootdown",	"Down angle of shoot");
FE_ATTRIBUTE("veg:shootramp",	"Added down angle towards end of shoot");
FE_ATTRIBUTE("veg:shootreact",	"Effect of shoot angle on main");
FE_ATTRIBUTE("veg:shootdrain",	"Effect of shoot area on main");
FE_ATTRIBUTE("veg:curvedown",	"Gradual increase in down angle");
FE_ATTRIBUTE("veg:sidedeath",	"Chance of side shoot termination");
FE_ATTRIBUTE("veg:centerdeath",	"Chance of center shoot termination");
FE_ATTRIBUTE("veg:areaRatio",	"Cross-section radius per length");
FE_ATTRIBUTE("veg:taper",		"Cross-section reduction at tip");

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief StickLevel RecordView

	@ingroup vegetation
*//***************************************************************************/
class FE_DL_EXPORT StickLevel: virtual public TreeLevel
{
	public:
		Functor<I32>		segments;
		Functor<I32>		forks;
		Functor<I32>		sideshoots;
		Functor<F32>		forkrotate;
		Functor<F32>		forktrench;
		Functor<F32>		shootdown;
		Functor<F32>		shootramp;
		Functor<F32>		shootreact;
		Functor<F32>		shootdrain;
		Functor<F32>		curvedown;
		Functor<F32>		sidedeath;
		Functor<F32>		centerdeath;
		Functor<F32>		areaRatio;
		Functor<F32>		taper;

				StickLevel(void)		{ setName("StickLevel"); }
virtual	void	addFunctors(void)
				{
					TreeLevel::addFunctors();

					add(segments,		FE_USE("veg:segments"));
					add(forks,			FE_USE("veg:forks"));
					add(sideshoots,		FE_USE("veg:sideshoots"));
					add(forkrotate,		FE_USE("veg:forkrotate"));
					add(forktrench,		FE_USE("veg:forktrench"));
					add(shootdown,		FE_USE("veg:shootdown"));
					add(shootramp,		FE_USE("veg:shootramp"));
					add(shootreact,		FE_USE("veg:shootreact"));
					add(shootdrain,		FE_USE("veg:shootdrain"));
					add(curvedown,		FE_USE("veg:curvedown"));
					add(sidedeath,		FE_USE("veg:sidedeath"));
					add(centerdeath,	FE_USE("veg:centerdeath"));
					add(areaRatio,		FE_USE("veg:areaRatio"));
					add(taper,			FE_USE("veg:taper"));
				}
virtual	void	initializeRecord(void)
				{
					TreeLevel::initializeRecord();

					segments()=1;
					forks()=0;
					sideshoots()=0;
					forkrotate()=0.0f;
					forktrench()=0.0f;
					shootdown()=90.0f;
					shootramp()=0.0f;
					shootreact()=1.0f;
					shootdrain()=1.0f;
					curvedown()=90.0f;
					sidedeath()=0.0f;
					centerdeath()=0.0f;
					areaRatio()=1.0f;
					taper()=1.0f;
				}
};

} /* namespace ext */
} /* namespace fe */

#endif /* __vegetation_StickLevel_h__ */



