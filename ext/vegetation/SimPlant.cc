/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <vegetation/vegetation.pmh>

#define FE_SPL_THREAD_DEBUG		FALSE

namespace fe
{
namespace ext
{

void SimPlant::handle(Record& a_forestRecord)
{
#if FE_SPL_THREAD_DEBUG
		feLog("SimPlant::handle\n");
#endif

	if(!m_plantRV.scope().isValid())
	{
		sp<Scope> spScope=a_forestRecord.layout()->scope();
		m_plantRV.bind(spScope);
		m_seedRV.bind(spScope);
	}

	m_forestRV.bind(a_forestRecord);

	if(m_forestRV.deltaT()==0.0f)
	{
		return;
	}

	sp<RecordGroup> spRG=m_forestRV.plants();
	FEASSERT(spRG.isValid());

	BWORD prepareSearch=TRUE;
	I32 treeCount=0;
	for(RecordGroup::iterator it=spRG->begin();it!=spRG->end();it++)
	{
		sp<RecordArray> spRA= *it;
		if(!m_plantRV.plantModel.check(spRA))
		{
			continue;
		}

		treeCount+=spRA->length();
	}

	const I32 threadCount=Thread::hardwareConcurrency();
//	feLog("treeCount %d threadCount %d\n",treeCount,threadCount);

	const BWORD multiThread=(treeCount>1 && threadCount>1);

	if(!multiThread)
	{
		for(RecordGroup::iterator it=spRG->begin();it!=spRG->end();it++)
		{
			sp<RecordArray> spRA= *it;
			if(!m_plantRV.plantModel.check(spRA))
			{
				continue;
			}

			m_plantRAV.bind(spRA);
			for(Plant& plantRV: m_plantRAV)
			{
				update(plantRV);
			}
		}
	}
	else
	{
		sp<SurfaceI> spCollider=m_forestRV.collider();
		if(spCollider.isValid() && prepareSearch)
		{
			//* force caching
			spCollider->center();

			BWORD prepared=FALSE;

			for(RecordGroup::iterator it=spRG->begin();it!=spRG->end();it++)
			{
				sp<RecordArray> spRA= *it;
				if(!m_plantRV.plantModel.check(spRA))
				{
					continue;
				}

				m_plantRAV.bind(spRA);
				for(Plant& plantRV: m_plantRAV)
				{
					if(plantRV.collisionMethod()=="impactRadial" ||
							plantRV.collisionMethod()=="impactNormal")
					{
						spCollider->prepareForSearch();
						prepared=TRUE;
						break;
					}

					//* HACK currently all the same, so only check first
					prepared=TRUE;
					break;

				}

				if(prepared)
				{
					break;
				}
			}
		}

		m_bundleSize=0.51*treeCount/threadCount;
		if(m_bundleSize<1)
		{
			m_bundleSize=1;
		}

		const BWORD doWork=FALSE;
		if(doWork)
		{
			//* TODO selective
			const BWORD quiet=FALSE;
			sp<WorkForceI> spWorkForceI=
					registry()->create("WorkForceI.WorkTbb",quiet);

			if(spWorkForceI.isValid())
			{
//				feLog("SimPlant::handle using \"%s\"\n",
//						spWorkForceI->name().c_str());

				sp<SpannedRange> spSpannedRange(new SpannedRange());
				spSpannedRange->nonAtomic().add(0,treeCount-1);

				const String stage="sim";
				sp<SpannedRange> spFullRange=
						spSpannedRange->combineAtoms(m_bundleSize);
				spWorkForceI->run(sp<WorkI>(this),threadCount,
						spFullRange,stage);
			}
		}
		else
		{
			if(m_spGang.isNull())
			{
				m_spGang=new Gang<PlantWorker,I32>();
			}

#if FE_SPL_THREAD_DEBUG
			feLog("SimPlant::handle start\n");
#endif

			m_spGang->start(sp<Counted>(this),threadCount);

#if FE_SPL_THREAD_DEBUG
			feLog("SimPlant::handle started\n");
#endif

			const I32 bundleCount=(treeCount+m_bundleSize-1)/m_bundleSize;

			I32 jobCount=0;

//			feLog("bundleSize %d bundleCount %d\n",m_bundleSize,bundleCount);

			for(I32 bundleIndex=0;bundleIndex<bundleCount;bundleIndex++)
			{
				m_spGang->post(jobCount++);
			}

			for(I32 threadIndex=0;threadIndex<threadCount;threadIndex++)
			{
				Record empty;
				m_spGang->post(-1);
				jobCount++;
			}

#if FE_SPL_THREAD_DEBUG
			feLog("SimPlant::handle posted\n");
#endif

			I32 delivered;
			while(jobCount)
			{
//				feLog("SimPlant::handle jobCount %d\n",jobCount);
				while(m_spGang->acceptDelivery(delivered))
				{
					jobCount--;
				}
				m_plantRV.unbind();

				if(jobCount)
				{
					milliSleep(1);
				}
			}

#if FE_SPL_THREAD_DEBUG
			feLog("SimPlant::handle finish\n");
#endif

			if(m_spGang->workers())
			{
				m_spGang->finish();
			}

#if FE_SPL_THREAD_DEBUG
			feLog("SimPlant::handle finished\n");
#endif
		}
	}

#if FE_SPL_THREAD_DEBUG
		feLog("SimPlant::handle done\n");
#endif
}

void SimPlant::run(I32 a_id,sp<SpannedRange> a_spSpannedRange)
{
#if FE_SPL_THREAD_DEBUG
	feLog("SimPlant::run %d\n",a_id);
#endif

	sp<RecordGroup> spRG=m_forestRV.plants();
	if(spRG.isNull())
	{
		return;
	}

	sp<RecordArray> spRA= *(spRG->begin());
	if(spRA.isNull())
	{
		return;
	}

	m_plantRAV.bind(spRA);

	for(SpannedRange::Iterator it=a_spSpannedRange->begin();
			!it.atEnd();it.step())
	{
		const I32 recordIndex=it.value();

		Plant& plantRV=m_plantRAV[recordIndex];

		if(plantRV.plantModel().isValid())
		{
//			feLog("SimPlant::run %d update %d\n",a_id,recordIndex);
			update(plantRV);
		}
	}
}

void SimPlant::PlantWorker::operate(void)
{
#if FE_SPL_THREAD_DEBUG
	feLog("SimPlant::PlantWorker::operate %d\n",m_id);
#endif

	sp<RecordGroup> spRG=m_hpSimPlant->m_forestRV.plants();
	if(spRG.isNull())
	{
		return;
	}

	sp<RecordArray> spRA= *(spRG->begin());
	if(spRA.isNull())
	{
		return;
	}

	RecordArrayView<Plant> plantRAV;
	plantRAV.bind(spRA);

	I32 job= -1;
	while(TRUE)
	{
		if(m_hpJobQueue->take(job))
		{
			if(job<0)
			{
				m_hpJobQueue->deliver(job);
				break;
			}

#if FALSE
			const I32 recordIndex=job;
			plantRV.setIndex(recordIndex);
			m_hpSimPlant->update(plantRV);
#else
			const I32 maxRecordIndex=spRA->length()-1;
			const I32 bundleSize=m_hpSimPlant->m_bundleSize;

			I32 recordIndex=job*bundleSize;

			for(I32 subIndex=0;subIndex<bundleSize;subIndex++)
			{
				if(recordIndex>maxRecordIndex)
				{
					break;
				}

				Plant& plantRV=plantRAV[recordIndex++];
				if(plantRV.plantModel().isValid())
				{
					m_hpSimPlant->update(plantRV);
				}
			}
#endif

			m_hpJobQueue->deliver(job);
		}
		else
		{
#if FE_SPL_THREAD_DEBUG
			feLog("SimPlant::PlantWorker::operate %d no jobs\n",m_id);
#endif
			milliSleep(1);
		}
	}

#if FE_SPL_THREAD_DEBUG
	feLog("SimPlant::PlantWorker::operate %d done\n",m_id);
#endif
}

void SimPlant::update(Plant& a_rPlantRV)
{
	try
	{
		sp<PlantModelI> spPlantModelI=a_rPlantRV.plantModel();
		if(!spPlantModelI.isValid())
		{
			feLog("SimPlant::update plantModel is NULL\n");

			sp<Scope> spScope=
					registry()->master()->catalog()->catalogComponent(
					"Scope","PlantScope");

			const String path=
					registry()->master()->catalog()->catalog<String>(
					"path:media")+
					"/vegetation/"+a_rPlantRV.plantName()+".rg";

			sp<RecordGroup>	spRG=RecordView::loadRecordGroup(spScope,path);
			if(spRG.isNull())
			{
				feX("SimPlant::handle","failed to load \"%s\"",
						path.c_str());
				return;
			}

			RecordGroup::iterator it=spRG->begin();
			sp<RecordArray>& rspRA= *it;
			Record seed=rspRA->getRecord(0);

			const char* modelname=m_seedRV.modelname(seed).c_str();
			String componentName;
			componentName.sPrintf("PlantModelI.%s",modelname);
			spPlantModelI=registry()->create(componentName);
			if(!spPlantModelI.isValid())
			{
				feX("SimPlant::handle", "couldn't create plant model %s",
						modelname);
			}
			a_rPlantRV.plantModel()=spPlantModelI;

			spPlantModelI->generate(seed);
		}

		spPlantModelI->setGravity(m_forestRV.gravity());
		spPlantModelI->setUniformVelocity(m_forestRV.uniformVelocity());
		spPlantModelI->setCollider(m_forestRV.collider());
		spPlantModelI->setColliderTransform(m_forestRV.colliderTransform());
		spPlantModelI->setLocation(a_rPlantRV.location());
		spPlantModelI->setOffset(a_rPlantRV.offset());
		spPlantModelI->setReactivity(a_rPlantRV.reactivity());
		spPlantModelI->setThreshold(a_rPlantRV.threshold());
		spPlantModelI->setCollisionMethod(a_rPlantRV.collisionMethod());
		spPlantModelI->setCollideEnd(a_rPlantRV.collideEnd());
		spPlantModelI->setRepulsion(a_rPlantRV.repulsion());
		spPlantModelI->setRepulsionFalloff(a_rPlantRV.repulsionFalloff());
		spPlantModelI->setDepletion(a_rPlantRV.depletion());
		spPlantModelI->setDepletionFalloff(a_rPlantRV.depletionFalloff());
		spPlantModelI->setWindHampering(a_rPlantRV.windHampering());
		spPlantModelI->setWindFalloff(a_rPlantRV.windFalloff());
		spPlantModelI->setPlasticity(a_rPlantRV.plasticity());

		spPlantModelI->update(m_forestRV.deltaT());

		if(a_rPlantRV.plantModel().isNull())
		{
			feLog("SimPlant::update plantModel went NULL\n");
		}

		//* TODO restore
//		m_forestRV.effectForce()+=spPlantModelI->effectorForce();
	}
	catch(const Exception& e)
	{
		feLog("SimPlant::handle caught fe::Exception %s\n",
				c_print(e));
	}
}

} /* namespace ext */
} /* namespace fe */
