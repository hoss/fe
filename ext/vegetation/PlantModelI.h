/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __vegetation_PlantModelI_h__
#define __vegetation_PlantModelI_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Vegetation subsystem

	@ingroup vegetation
*//***************************************************************************/
class FE_DL_EXPORT PlantModelI:
	virtual public Component,
	public CastableAs<PlantModelI>
{
	public:

	//* TODO consider using a Record
	/// @brief Stem Segment in a PlantModelI
	class Stick: public CastableAs<Stick>
	{
		public:
	virtual							~Stick(void)							{}
	virtual const SpatialVector&	base(void) const						=0;
	virtual const SpatialVector&	span(void) const						=0;
	virtual const SpatialQuaternion&	rotation(void) const				=0;
	virtual Real					radius1(void) const						=0;
	virtual Real					radius2(void) const						=0;
	virtual I32						resolution(void) const					=0;
	virtual String					stateString(void) const					=0;
	virtual	void					setWindVelocity(
										const SpatialVector& a_windVelocity)=0;
	virtual	void					setTarget(
										const SpatialVector& a_target)		=0;
	virtual	void					setTargeted(const I32 a_targeted)		=0;
	};

	/// @brief Organ of photosynthesis and transpiration
	class Leaf: public CastableAs<Leaf>
	{
		public:
	virtual							~Leaf(void)								{}
	virtual const SpatialVector&	center(void) const						=0;
	virtual const SpatialVector&	normal(void) const						=0;
	};

virtual	void			reset(void)											=0;
virtual	void			generate(const Record seed)							=0;
virtual	void			addBranch(U32 a_fromIndex,U32 a_toIndex,
								U32 a_level,Real a_radius1,Real a_radius2,
								const SpatialVector& a_span,
								Real a_rigidity)							=0;
virtual	void			prepare(void)										=0;
virtual	void			update(const Real deltaT)							=0;

virtual	U32				numSticks(void) const								=0;
virtual	Stick*			stick(U32 index)									=0;

virtual	U32				numLeaves(void) const								=0;
virtual	Leaf*			leaf(U32 index)										=0;
virtual
const	SpatialVector*	leafPoints(void) const								=0;
virtual
const	SpatialVector*	leafNormals(void) const								=0;

virtual	U32				stickBundles(void) const							=0;
virtual	U32				stickBundleSize(U32 bundle) const					=0;
virtual
const	SpatialTransform*	stickTransforms(U32 bundle) const				=0;
virtual
const	SpatialVector*	stickScale(U32 bundle) const						=0;
virtual
const	Real*			stickBaseScale(U32 bundle) const					=0;
virtual
const	U32*			stickSlices(U32 bundle) const						=0;

virtual	void			setLocation(const SpatialVector& location)			=0;
virtual	void			setOffset(const SpatialVector& offset)				=0;

virtual	sp<SurfaceI>	collider(void)										=0;
virtual	void			setCollider(sp<SurfaceI> a_spCollider)				=0;

virtual	SpatialTransform&	colliderTransform(void)							=0;
virtual	void			setColliderTransform(
							const SpatialTransform& a_rspColliderTransform)	=0;

virtual SpatialVector&	effectorForce(void)									=0;

virtual	void			setGravity(const SpatialVector& a_gravity)			=0;
virtual	void			setUniformVelocity(const SpatialVector& a_velocity)	=0;
virtual	void			setUniformRigidity(const Real a_rigidity)			=0;
virtual	void			setDamping(const Real a_damping)					=0;
virtual	void			setReactivity(const Real a_reactivity)				=0;
virtual	void			setThreshold(const Real a_threshold)				=0;
virtual	void			setCollisionMethod(
								const String a_collisionMethod)				=0;
virtual	void			setCollideEnd(const BWORD a_collideEnd)				=0;
virtual	void			setRepulsion(const Real a_repulsion)				=0;
virtual	void			setRepulsionFalloff(const Real a_repulsionFalloff)	=0;
virtual	void			setDepletion(const Real a_depletion)				=0;
virtual	void			setDepletionFalloff(const Real a_depletionFalloff)	=0;
virtual	void			setWindHampering(const Real a_windHampering)		=0;
virtual	void			setWindFalloff(const Real a_windFalloff)			=0;
virtual	void			setPlasticity(const Real a_plasticity)				=0;
virtual	void			setCompensation(const BWORD a_compensation)			=0;
virtual	void			setCorrection(const BWORD a_correction)				=0;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __vegetation_PlantModelI_h__ */
