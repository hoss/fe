/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <window/window.pmh>

namespace fe
{
namespace ext
{

#define FE_LWH_DEBUG			FALSE

void LogWindowHandler::handle(Record &record)
{
	m_event.bind(record);

#if FE_LWH_DEBUG
	if(!m_event.isIdleMouse() && !m_event.isPoll())
	{
		feLog("%p LWH::handle %s\n",this,print(m_event).c_str());
	}
#endif
}

} /* namespace ext */
} /* namespace fe */
