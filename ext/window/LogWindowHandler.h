/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __window_LogWindowHandler_h__
#define __window_LogWindowHandler_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief WindowEvent handler that just prints to the feLog

	@ingroup window
*//***************************************************************************/
class LogWindowHandler:
	virtual public HandlerI,
	public CastableAs<LogWindowHandler>
{
	public:
					LogWindowHandler(void)									{}
virtual				~LogWindowHandler(void)									{}

					//* As HandlerI
virtual	void		handle(Record &record);

	private:
		WindowEvent	m_event;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __window_LogWindowHandler_h__ */
