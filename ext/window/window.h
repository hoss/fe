/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __window_h__
#define __window_h__

#include "datatool/datatool.h"
#include "math/math.h"

#include "window/FontI.h"
#include "window/JoyI.h"
#include "window/KeyI.h"
#include "window/WindowI.h"
#include "window/EventContextI.h"
#include "window/windowAS.h"

namespace fe
{
namespace ext
{

Library* CreateWindowLibrary(sp<Master>);

} /* namespace ext */
} /* namespace fe */

#endif /* __window_h__ */
