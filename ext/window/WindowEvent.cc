/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <window/window.pmh>

namespace fe
{
namespace ext
{

WindowEvent::Mask WindowEvent::stringToMask(const String &a_string)
{
	WindowEvent::Mask mask(
		WindowEvent::e_sourceNull,
		WindowEvent::e_itemNull,
		WindowEvent::e_stateNull);

	std::string source_str;
	std::string item_str;
	std::string state_str;
	Array< std::string > fields;

#ifdef FE_BOOST_TOKENIZER
	boost::char_separator<char> cat_sep(" ,");
	boost::char_separator<char> item_sep(" |");

	std::string std_str = a_string.c_str();
	typedef boost::tokenizer<boost::char_separator<char> > t_tokenizer;
	t_tokenizer tokens(std_str, cat_sep);
	for(t_tokenizer::iterator i_t = tokens.begin();
					i_t != tokens.end(); ++i_t)
	{
		fields.push_back((*i_t));
	}
#else
	String buffer=a_string;
	String token;
	while(!(token=buffer.parse(""," ,")).empty())
	{
		fields.push_back(token.c_str());
	}
#endif

	if(fields.size() == 1)
	{
		source_str = "keyboard";
		item_str = fields[0];
		state_str = "press";
	}
	else if(fields.size() == 3)
	{
		source_str = fields[0];
		item_str = fields[1];
		state_str = fields[2];
	}
	else
	{
		feX("WindowEvent::stringToMask",
			"invalid mask: wrong field count: [%s]", a_string.c_str());
	}


#ifdef FE_BOOST_TOKENIZER
	t_tokenizer source_tokens(source_str, item_sep);
	for(t_tokenizer::iterator i_t = source_tokens.begin();
					i_t != source_tokens.end(); ++i_t)
	{
		String token= i_t->c_str();
#else
	buffer=source_str.c_str();
	while(!(token=buffer.parse(""," |")).empty())
	{
#endif
		if(token == "mousePosition")
		{
			mask.orSource(WindowEvent::e_sourceMousePosition);
		}
		if(token == "mouseButton")
		{
			mask.orSource(WindowEvent::e_sourceMouseButton);
		}
		if(token == "keyboard")
		{
			mask.orSource(WindowEvent::e_sourceKeyboard);
		}
		if(token == "system")
		{
			mask.orSource(WindowEvent::e_sourceSystem);
		}
		if(token == "user")
		{
			mask.orSource(WindowEvent::e_sourceUser);
		}
		if(token == "joy0")
		{
			mask.orSource(WindowEvent::e_sourceJoy0);
		}
		if(token == "joy1")
		{
			mask.orSource(WindowEvent::e_sourceJoy1);
		}
		if(token == "joy2")
		{
			mask.orSource(WindowEvent::e_sourceJoy2);
		}
		if(token == "joy3")
		{
			mask.orSource(WindowEvent::e_sourceJoy3);
		}
		if(token == "any")
		{
			mask.orSource(WindowEvent::e_sourceAny);
		}
	}

#ifdef FE_BOOST_TOKENIZER
	t_tokenizer item_tokens(item_str, item_sep);
	for(t_tokenizer::iterator i_t = item_tokens.begin();
					i_t != item_tokens.end(); ++i_t)
	{
		String token= i_t->c_str();
#else
	buffer=item_str.c_str();
	while(!(token=buffer.parse(""," |")).empty())
	{
#endif
		if(token == "any")
		{
			mask.orItem(WindowEvent::e_itemAny);
		}
		if(token == "move")
		{
			mask.orItem(WindowEvent::e_itemMove);
		}
		if(token == "drag")
		{
			mask.orItem(WindowEvent::e_itemDrag);
		}
		if(token == "left")
		{
			mask.orItem(WindowEvent::e_itemLeft);
		}
		if(token == "middle")
		{
			mask.orItem(WindowEvent::e_itemMiddle);
		}
		if(token == "right")
		{
			mask.orItem(WindowEvent::e_itemRight);
		}
		if(token == "wheel")
		{
			mask.orItem(WindowEvent::e_itemWheel);
		}
		if(token == "backspace")
		{
			mask.orItem(WindowEvent::e_keyBackspace);
		}
		if(token == "tab")
		{
			mask.orItem(WindowEvent::e_keyTab);
		}
		if(token == "lineFeed")
		{
			mask.orItem(WindowEvent::e_keyLineFeed);
		}
		if(token == "carriageReturn")
		{
			mask.orItem(WindowEvent::e_keyCarriageReturn);
		}
		if(token == "space")
		{
			mask.orItem(WindowEvent::e_keySpace);
		}
		if(token == "escape")
		{
			mask.orItem(WindowEvent::e_keyEscape);
		}
		if(token == "delete")
		{
			mask.orItem(WindowEvent::e_keyDelete);
		}
		if(token == "asciiMask")
		{
			mask.orItem(WindowEvent::e_keyAsciiMask);
		}
		if(token == "cursor")
		{
			mask.orItem(WindowEvent::e_keyCursor);
		}
		if(token == "cursorUp")
		{
			mask.orItem(WindowEvent::e_keyCursorUp);
		}
		if(token == "cursorDown")
		{
			mask.orItem(WindowEvent::e_keyCursorDown);
		}
		if(token == "cursorLeft")
		{
			mask.orItem(WindowEvent::e_keyCursorLeft);
		}
		if(token == "cursorRight")
		{
			mask.orItem(WindowEvent::e_keyCursorRight);
		}
		if(token == "cursorMask")
		{
			mask.orItem(WindowEvent::e_keyCursorMask);
		}
		if(token == "shift")
		{
			mask.orItem(WindowEvent::e_keyShift);
		}
		if(token == "capLock")
		{
			mask.orItem(WindowEvent::e_keyCapLock);
		}
		if(token == "control")
		{
			mask.orItem(WindowEvent::e_keyControl);
		}
		if(token == "alt")
		{
			mask.orItem(WindowEvent::e_keyAlt);
		}
		if(token == "modifiers")
		{
			mask.orItem(WindowEvent::e_keyModifiers);
		}
		if(token == "used")
		{
			mask.orItem(WindowEvent::e_keyUsed);
		}
		if(token == "idle")
		{
			mask.orItem(WindowEvent::e_itemIdle);
		}
		if(token == "resize")
		{
			mask.orItem(WindowEvent::e_itemResize);
		}
		if(token == "expose")
		{
			mask.orItem(WindowEvent::e_itemExpose);
		}
		if(token == "closeWindow")
		{
			mask.orItem(WindowEvent::e_itemCloseWindow);
		}
		if(token == "quit")
		{
			mask.orItem(WindowEvent::e_itemQuit);
		}
		if(token == "poll")
		{
			mask.orItem(WindowEvent::e_itemPoll);
		}
		if(token == "enter")
		{
			mask.orItem(WindowEvent::e_itemEnter);
		}
		if(token == "exit")
		{
			mask.orItem(WindowEvent::e_itemExit);
		}
		if(token == "clear")
		{
			mask.orItem(WindowEvent::e_windowClear);
		}
		if(token == "swapBuffers")
		{
			mask.orItem(WindowEvent::e_windowSwapBuffers);
		}
		if(token == "leftStickX")
		{
			mask.orItem(WindowEvent::e_joyLeftStickX);
		}
		if(token == "leftStickY")
		{
			mask.orItem(WindowEvent::e_joyLeftStickY);
		}
		if(token == "leftPadX")
		{
			mask.orItem(WindowEvent::e_joyLeftPadX);
		}
		if(token == "leftPadY")
		{
			mask.orItem(WindowEvent::e_joyLeftPadY);
		}
		if(token == "rightStickX")
		{
			mask.orItem(WindowEvent::e_joyRightStickX);
		}
		if(token == "rightStickY")
		{
			mask.orItem(WindowEvent::e_joyRightStickY);
		}
		if(token == "leftTrigger")
		{
			mask.orItem(WindowEvent::e_joyLeftTrigger);
		}
		if(token == "rightTrigger")
		{
			mask.orItem(WindowEvent::e_joyRightTrigger);
		}
		if(token == "select")
		{
			mask.orItem(WindowEvent::e_joySelect);
		}
		if(token == "back")
		{
			mask.orItem(WindowEvent::e_joyBack);
		}
		if(token == "guide")
		{
			mask.orItem(WindowEvent::e_joyGuide);
		}
		if(token == "start")
		{
			mask.orItem(WindowEvent::e_joyStart);
		}
		if(token == "leftUp")
		{
			mask.orItem(WindowEvent::e_joyLeftUp);
		}
		if(token == "leftDown")
		{
			mask.orItem(WindowEvent::e_joyLeftDown);
		}
		if(token == "leftLeft")
		{
			mask.orItem(WindowEvent::e_joyLeftLeft);
		}
		if(token == "leftRight")
		{
			mask.orItem(WindowEvent::e_joyLeftRight);
		}
		if(token == "leftHigh")
		{
			mask.orItem(WindowEvent::e_joyLeftHigh);
		}
		if(token == "leftLow")
		{
			mask.orItem(WindowEvent::e_joyLeftLow);
		}
		if(token == "leftDepress")
		{
			mask.orItem(WindowEvent::e_joyLeftDepress);
		}
		if(token == "rightUp")
		{
			mask.orItem(WindowEvent::e_joyRightUp);
		}
		if(token == "rightDown")
		{
			mask.orItem(WindowEvent::e_joyRightDown);
		}
		if(token == "rightLeft")
		{
			mask.orItem(WindowEvent::e_joyRightLeft);
		}
		if(token == "rightRight")
		{
			mask.orItem(WindowEvent::e_joyRightRight);
		}
		if(token == "rightHigh")
		{
			mask.orItem(WindowEvent::e_joyRightHigh);
		}
		if(token == "rightLow")
		{
			mask.orItem(WindowEvent::e_joyRightLow);
		}
		if(token == "rightDepress")
		{
			mask.orItem(WindowEvent::e_joyRightDepress);
		}
		if(token == "axes")
		{
			mask.orItem(WindowEvent::e_joyAxes);
		}
		if(token == "buttons")
		{
			mask.orItem(WindowEvent::e_joyButtons);
		}
		if(token == "joyAny")
		{
			mask.orItem(WindowEvent::e_joyAny);
		}
		if(token.length() == 1)
		{
			mask.orItem((WindowEvent::Item)token.c_str()[0]);
		}
	}

#ifdef FE_BOOST_TOKENIZER
	t_tokenizer state_tokens(state_str, item_sep);
	for(t_tokenizer::iterator i_t = state_tokens.begin();
					i_t != state_tokens.end(); ++i_t)
	{
		String token= i_t->c_str();
#else
	buffer=state_str.c_str();
	while(!(token=buffer.parse(""," |")).empty())
	{
#endif
		if(token == "press")
		{
			mask.orState(WindowEvent::e_statePress);
		}
		if(token == "release")
		{
			mask.orState(WindowEvent::e_stateRelease);
		}
		if(token == "repeat")
		{
			mask.orState(WindowEvent::e_stateRepeat);
		}
		if(token == "any")
		{
			mask.orState(WindowEvent::e_stateAny);
		}
		if(token == "left")
		{
			mask.orState((State)WindowEvent::e_mbLeft);
		}
		if(token == "middle")
		{
			mask.orState((State)WindowEvent::e_mbMiddle);
		}
		if(token == "right")
		{
			mask.orState((State)WindowEvent::e_mbRight);
		}
		if(token == "all")
		{
			mask.orState((State)WindowEvent::e_mbAll);
		}
		if(token == "mbAny")
		{
			mask.orState((State)WindowEvent::e_mbAny);
		}
	}

	return mask;
}

} /* namespace ext */
} /* namespace fe */

namespace fe
{

String print(const ext::WindowEvent& event)
{
	String string;
#if FALSE
	string.sPrintf("source=%02p item=%02p state=%3d,%3d"
			" mouse=%4d,%4d mb=%p",
			event.source(),event.item(),event.state(),event.state2(),
			event.mouseX(),event.mouseY(),event.mouseButtons());
#endif
	string.sPrintf("src ");
	switch(event.source())
	{
		case fe::ext::WindowEvent::e_sourceNull:
			string.cat("NUL ");
			break;
		case fe::ext::WindowEvent::e_sourceMousePosition:
			string.cat("POS ");
			break;
		case fe::ext::WindowEvent::e_sourceMouseButton:
			string.cat("MB  ");
			break;
		case fe::ext::WindowEvent::e_sourceKeyboard:
			string.cat("KEY ");
			break;
		case fe::ext::WindowEvent::e_sourceSystem:
			string.cat("SYS ");
			break;
		case fe::ext::WindowEvent::e_sourceUser:
			string.cat("USR ");
			break;
		case fe::ext::WindowEvent::e_sourceJoy0:
			string.cat("JY0 ");
			break;
		case fe::ext::WindowEvent::e_sourceJoy1:
			string.cat("JY1 ");
			break;
		case fe::ext::WindowEvent::e_sourceJoy2:
			string.cat("JY2 ");
			break;
		case fe::ext::WindowEvent::e_sourceJoy3:
			string.cat("JY3 ");
			break;
		case fe::ext::WindowEvent::e_sourceWheel0:
			string.cat("WL0 ");
			break;
		case fe::ext::WindowEvent::e_sourceWheel1:
			string.cat("WL1 ");
			break;
		case fe::ext::WindowEvent::e_sourceWheel2:
			string.cat("WL2 ");
			break;
		case fe::ext::WindowEvent::e_sourceWheel3:
			string.cat("WL3 ");
			break;
		case fe::ext::WindowEvent::e_sourceAny:
			string.cat("ANY ");
			break;
		default:
			string.catf("%02p ",(U32)event.source());
	}

	string.cat("item ");

	if(event.item()==fe::ext::WindowEvent::e_itemNull)
		string.cat("NULL   ");
	else if(event.item()==fe::ext::WindowEvent::e_itemAny)
		string.cat("ANY    ");
	else
	{
		String modifiers;
		U32 item2=event.item();
		BWORD cryptic=FALSE;
		if(item2&fe::ext::WindowEvent::e_keyAlt)
		{
			item2^=fe::ext::WindowEvent::e_keyAlt;
			modifiers.cat("A");
//			cryptic=TRUE;
		}
		else
			modifiers.cat(" ");
		if(item2&fe::ext::WindowEvent::e_keyControl)
		{
			item2^=fe::ext::WindowEvent::e_keyControl;
			modifiers.cat("C");
//			cryptic=TRUE;
		}
		else
			modifiers.cat(" ");

		if(item2&fe::ext::WindowEvent::e_keyShift)
		{
			item2^=fe::ext::WindowEvent::e_keyShift;
			modifiers.cat("S");
			cryptic=(item2<33 || item2>127);
		}
		else
			modifiers.cat(" ");
		if(item2&fe::ext::WindowEvent::e_keyCapLock)
		{
			item2^=fe::ext::WindowEvent::e_keyCapLock;
			modifiers.cat("L");
		}
		else
			modifiers.cat(" ");

		switch(event.source())
		{
			case fe::ext::WindowEvent::e_sourceMousePosition:
				switch(item2)
				{
					case fe::ext::WindowEvent::e_itemMove:
						string.catf("%sMV ",modifiers.c_str());
						break;
					case fe::ext::WindowEvent::e_itemDrag:
						string.catf("%sDR ",modifiers.c_str());
						break;
					default:
						string.catf("%03p ",(U32)event.item());
						break;
				}
				break;
			case fe::ext::WindowEvent::e_sourceMouseButton:
				switch(item2)
				{
					case fe::ext::WindowEvent::e_itemLeft:
						string.catf("%sLT ",modifiers.c_str());
						break;
					case fe::ext::WindowEvent::e_itemMiddle:
						string.catf("%sMD ",modifiers.c_str());
						break;
					case fe::ext::WindowEvent::e_itemRight:
						string.catf("%sRT ",modifiers.c_str());
						break;
					case fe::ext::WindowEvent::e_itemWheel:
						string.catf("%sWH ",modifiers.c_str());
						break;
					default:
						string.catf("%03p ",(U32)event.item());
						break;
				}
				break;
			case fe::ext::WindowEvent::e_sourceKeyboard:
			{
				string.cat(modifiers);
				if(item2==fe::ext::WindowEvent::e_keyBackspace)
					string.cat("BS ");
				else if(item2==fe::ext::WindowEvent::e_keyTab)
					string.cat("HT ");
				else if(item2==fe::ext::WindowEvent::e_keyLineFeed)
					string.cat("LF ");
				else if(item2==fe::ext::WindowEvent::e_keyCarriageReturn)
					string.cat("CR ");
				else if(item2==fe::ext::WindowEvent::e_keySpace)
					string.cat("SP ");
				else if(item2==fe::ext::WindowEvent::e_keyEscape)
					string.cat("ES ");
				else if(item2==fe::ext::WindowEvent::e_keyDelete)
					string.cat("DL ");
				else if(!cryptic
						&& item2<=fe::ext::WindowEvent::e_keyAsciiMask)
					string.catf("%c  ",item2);
				else if(!cryptic &&
						(item2&fe::ext::WindowEvent::e_keyCursorMask) &&
						!(item2&(~fe::ext::WindowEvent::e_keyCursorMask)))
				{
					static char direction[4][6]=
							{"UP ","DN ","LT ","RT "};
					string.catf("%s%s ",modifiers.c_str(),
							direction[(item2-1)&3]);
				}
				else if(!cryptic &&
						(item2&fe::ext::WindowEvent::e_keyFunctionMask) &&
						!(item2&(~fe::ext::WindowEvent::e_keyFunctionMask)))
				{
					string.catf("%sF%d ",modifiers.c_str(),item2&0xf);
				}
				else
					string.catf("%03p ",(U32)item2);
			}
				break;
			case fe::ext::WindowEvent::e_sourceSystem:
				switch(event.item())
				{
					case fe::ext::WindowEvent::e_itemIdle:
						string.cat("IDLE   ");
						break;
					case fe::ext::WindowEvent::e_itemResize:
						string.cat("RESIZE ");
						break;
					case fe::ext::WindowEvent::e_itemExpose:
						string.cat("EXPOSE ");
						break;
					case fe::ext::WindowEvent::e_itemCloseWindow:
						string.cat("CLOSE  ");
						break;
					case fe::ext::WindowEvent::e_itemQuit:
						string.cat("QUIT   ");
						break;
					case fe::ext::WindowEvent::e_itemPoll:
						string.cat("POLL   ");
						break;
					case fe::ext::WindowEvent::e_itemEnter:
						string.cat("ENTER ");
						break;
					case fe::ext::WindowEvent::e_itemExit:
						string.cat("EXIT  ");
						break;
					default:
						string.catf("%06p ",(U32)event.item());
						break;
				}
				break;
			case fe::ext::WindowEvent::e_sourceJoy0:
			case fe::ext::WindowEvent::e_sourceJoy1:
			case fe::ext::WindowEvent::e_sourceJoy2:
			case fe::ext::WindowEvent::e_sourceJoy3:
				switch(event.item())
				{
					case fe::ext::WindowEvent::e_joyLeftStickX:
						string.cat("L SX   ");
						break;
					case fe::ext::WindowEvent::e_joyLeftStickY:
						string.cat("L SY   ");
						break;
					case fe::ext::WindowEvent::e_joyLeftPadX:
						string.cat("L PADX ");
						break;
					case fe::ext::WindowEvent::e_joyLeftPadY:
						string.cat("L PADY ");
						break;
					case fe::ext::WindowEvent::e_joyRightStickX:
						string.cat("R SX   ");
						break;
					case fe::ext::WindowEvent::e_joyRightStickY:
						string.cat("R SY   ");
						break;
					case fe::ext::WindowEvent::e_joyLeftTrigger:
						string.cat("L TR   ");
						break;
					case fe::ext::WindowEvent::e_joyRightTrigger:
						string.cat("R TR   ");
						break;

					case fe::ext::WindowEvent::e_joyBack:
						string.cat("BACK   ");
						break;
					case fe::ext::WindowEvent::e_joyGuide:
						string.cat("GUIDE  ");
						break;
					case fe::ext::WindowEvent::e_joyStart:
						string.cat("START  ");
						break;

					case fe::ext::WindowEvent::e_joyLeftUp:
						string.cat("L UP   ");
						break;
					case fe::ext::WindowEvent::e_joyLeftDown:
						string.cat("L DOWN ");
						break;
					case fe::ext::WindowEvent::e_joyLeftLeft:
						string.cat("L LEFT ");
						break;
					case fe::ext::WindowEvent::e_joyLeftRight:
						string.cat("L RGHT ");
						break;

					case fe::ext::WindowEvent::e_joyLeftHigh:
						string.cat("L HIGH ");
						break;
					case fe::ext::WindowEvent::e_joyLeftLow:
						string.cat("L LOW  ");
						break;
					case fe::ext::WindowEvent::e_joyLeftDepress:
						string.cat("L DEPR ");
						break;

					case fe::ext::WindowEvent::e_joyRightUp:
						string.cat("R UP   ");
						break;
					case fe::ext::WindowEvent::e_joyRightDown:
						string.cat("R DOWN ");
						break;
					case fe::ext::WindowEvent::e_joyRightLeft:
						string.cat("R LEFT ");
						break;
					case fe::ext::WindowEvent::e_joyRightRight:
						string.cat("R RGHT ");
						break;

					case fe::ext::WindowEvent::e_joyRightHigh:
						string.cat("R HIGH ");
						break;
					case fe::ext::WindowEvent::e_joyRightLow:
						string.cat("R LOW  ");
						break;
					case fe::ext::WindowEvent::e_joyRightDepress:
						string.cat("R DEPR ");
						break;

					default:
						string.catf("%06p ",(U32)event.item());
						break;
				}
				break;
			case fe::ext::WindowEvent::e_sourceUser:
				switch(event.item())
				{
					case fe::ext::WindowEvent::e_windowClear:
						string.cat("CLEAR  ");
						break;
					case fe::ext::WindowEvent::e_windowSwapBuffers:
						string.cat("SWAP   ");
						break;
					default:
						string.catf("%06p ",(U32)event.item());
						break;
				}
				break;
			default:
				string.catf("%09p ",(U32)event.item());
		}
	}

	string.cat("state ");
	if(event.source()==fe::ext::WindowEvent::e_sourceMouseButton &&
			event.item()==fe::ext::WindowEvent::e_itemWheel)
	{
		string.catf("%-6d ",(I32)event.state());
	}
	else
	{
		switch(event.source())
		{
			case fe::ext::WindowEvent::e_sourceMouseButton:
			case fe::ext::WindowEvent::e_sourceKeyboard:
			case fe::ext::WindowEvent::e_sourceMousePosition:
				switch(event.state())
				{
					case fe::ext::WindowEvent::e_stateNull:
						string.cat("NULL   ");
						break;
					case fe::ext::WindowEvent::e_statePress:
						string.cat("PRESS  ");
						break;
					case fe::ext::WindowEvent::e_stateRelease:
						string.cat("REL    ");
						break;
					case fe::ext::WindowEvent::e_stateRepeat:
						string.cat("RPT+   ");
						break;
					case fe::ext::WindowEvent::e_stateAny:
						string.cat("ANY    ");
						break;
					default:
						string.catf("%-6d ",(I32)event.state());
						break;
				}
				break;
			default:
				string.catf("%-6d ",(I32)event.state());
		}
	}

	if(event.state2())
		string.catf(",%-4d ",(I32)event.state2());
	else
		string.cat("      ");

	string.catf("at %4d,%4d",event.mouseX(),event.mouseY());

	if(event.mouseButtons())
		string.catf(" mb %p",event.mouseButtons());
	else
		string.cat("       ");

	return string;
}

} /* namespace fe */
