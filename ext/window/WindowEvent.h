/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __window_WindowEvent_h__
#define __window_WindowEvent_h__

namespace fe
{
namespace ext
{

#ifndef FE_EVENT_LAYOUT
#define	FE_EVENT_LAYOUT		"WindowEvent"
#endif

/**************************************************************************//**
	@brief Generalized windowing event

	@ingroup window

	This is a convenience class to access a window event Record.

	Instead of a massive enumeration, the events are interpreted into
	a useful triple: source/item/state.  This is not only more
	manageable, but it lets you process events with simple masks.

	The source is where the event originated.

	The item is the aspect of the source that was affected.

	The state is how the aspect was affected.
	A few events merit a second state value, given by state2.

	The mouse positions and button states are also available for most events.
	Not all events necessarily merit mouse information.

	Several simple convenience functions of the form is*() are provided to
	check common events or event masks.
*//***************************************************************************/
class FE_DL_EXPORT WindowEvent: public RecordView
{
	public:
		Functor<I32>	eventSource;
		Functor<I32>	eventItem;
		Functor<I32>	eventState;
		Functor<I32>	eventState2;
		Functor<I32>	eventMouseX;
		Functor<I32>	eventMouseY;
		Functor<I32>	eventMouseButtons;


		/******************************************************************//**
			@brief Source of a window event
		*//*******************************************************************/
		typedef enum
		{
			e_sourceNull=			0x0000,
			e_sourceMousePosition=	0x0001,
			e_sourceMouseButton=	0x0002,
			e_sourceKeyboard=		0x0004,
			e_sourceSystem=			0x0008,
			e_sourceUser=			0x0010,
			e_sourceJoy0=			0x0100,
			e_sourceJoy1=			0x0200,
			e_sourceJoy2=			0x0400,
			e_sourceJoy3=			0x0800,
			e_sourceWheel0=			0x1000,
			e_sourceWheel1=			0x2000,
			e_sourceWheel2=			0x4000,
			e_sourceWheel3=			0x8000,
			e_sourceAny=			0x7fffffff
		} Source;

		/******************************************************************//**
			@brief Aspect of change in a window event
		*//*******************************************************************/
		typedef enum
		{
			e_itemNull=				0x00,
			e_itemAny=				0x7fffffff,

			//* e_sourceMousePosition
			e_itemMove=				0x01,
			e_itemDrag=				0x02,

			//* e_sourceMouseButton
			e_itemLeft=				0x01,
			e_itemMiddle=			0x02,
			e_itemRight=			0x04,
			e_itemWheel=			0x08,

			//* e_sourceKeyboard
			e_keyBackspace=			0x08,
			e_keyTab=				0x09,
			e_keyLineFeed=			0x0A,
			e_keyCarriageReturn=	0x0D,
			e_keySpace=				0x20,
			e_keyEscape=			0x1B,
			e_keyDelete=			0x7F,
			e_keyAsciiMask=			0xFF,
			e_keyCursor=			0x100,
			e_keyCursorUp=			0x101,
			e_keyCursorDown=		0x102,
			e_keyCursorLeft=		0x103,
			e_keyCursorRight=		0x104,
			e_keyCursorMask=		0x107,
			e_keyFunction=			0x200,
			e_keyFunction1=			0x201,
			e_keyFunction2=			0x202,
			e_keyFunction3=			0x203,
			e_keyFunction4=			0x204,
			e_keyFunction5=			0x205,
			e_keyFunction6=			0x206,
			e_keyFunction7=			0x207,
			e_keyFunction8=			0x208,
			e_keyFunction9=			0x209,
			e_keyFunction10=		0x20A,
			e_keyFunction11=		0x20B,
			e_keyFunction12=		0x20C,
			e_keyFunctionMask=		0x20F,
			e_keyShift=				1<<24,
			e_keyCapLock=			1<<25,
			e_keyControl=			1<<26,
			e_keyAlt=				1<<27,
			e_keyModifiers=			(e_keyShift|e_keyCapLock|
									e_keyControl|e_keyAlt),
			e_keyUsed=				0x1FFF,

			//* e_sourceSystem
			e_itemIdle=				0x01,
			e_itemResize=			0x02,
			e_itemExpose=			0x04,
			e_itemCloseWindow=		0x08,
			e_itemQuit=				0x10,	//* TODO Win32
			e_itemPoll=				0x20,	//* allow devices to produce events
			e_itemEnter=			0x40,
			e_itemExit=				0x80,

			//* e_sourceUser
			e_windowClear=			0x01,
			e_windowSwapBuffers=	0x02,

			//* e_sourceJoy
			e_joyLeftStickX=		0x0000001,
			e_joyLeftStickY=		0x0000002,
			e_joyLeftPadX=			0x0000004,
			e_joyLeftPadY=			0x0000008,
			e_joyRightStickX=		0x0000010,
			e_joyRightStickY=		0x0000020,
			e_joyLeftTrigger=		0x0000040,
			e_joyRightTrigger=		0x0000080,

			e_joySelect=			0x0000100,
			e_joyBack=				0x0000100,	//* Back is the new Select
			e_joyGuide=				0x0000200,
			e_joyStart=				0x0000400,

			e_joyLeftUp=			0x0001000,
			e_joyLeftDown=			0x0002000,
			e_joyLeftLeft=			0x0004000,
			e_joyLeftRight=			0x0008000,

			e_joyLeftHigh=			0x0010000,
			e_joyLeftLow=			0x0020000,
			e_joyLeftDepress=		0x0040000,

			e_joyRightUp=			0x0100000,
			e_joyRightDown=			0x0200000,
			e_joyRightLeft=			0x0400000,
			e_joyRightRight=		0x0800000,

			e_joyRightHigh=			0x1000000,
			e_joyRightLow=			0x2000000,
			e_joyRightDepress=		0x4000000,

			e_joyAxes=				0x00000FF,
			e_joyButtons=			0xFF7F700,
			e_joyAny=				0xFF7F7FF,

			e_JoyWheel=				e_joyLeftStickX,
			e_JoyClutch=			e_joyLeftStickY,
			e_JoyGasPedal=			e_joyRightStickX,
			e_JoyBrake=				e_joyRightStickY,
			e_JoyShifterRight=		e_joyLeftLow,
			e_JoyShifterLeft=		e_joyRightLow,
		} Item;

		/******************************************************************//**
			@brief Effect of change in a window event
		*//*******************************************************************/
		typedef enum
		{
			e_stateForceSigned=		-1,			//* force enum to be signed
			e_stateNull=			0x00,
			e_statePress=			0x01,		//* key or mousebutton
			e_stateRelease=			0x02,		//* key or mousebutton
			e_stateRepeat=			0x04,		//* key held down
			e_stateAny=				0x7fffffff,

			e_state2ClickGeneric=	0x00,
			e_state2ClickSingle=	0x01,
			e_state2ClickDouble=	0x02,
			e_state2ClickTriple=	0x03,

			e_state2NoRelease=		0x04,
		} State;

		/******************************************************************//**
			@brief	State of the mouse buttons during an event
		*//*******************************************************************/
		typedef enum
		{
			e_mbNone=				0x00,
			e_mbLeft=				0x01,
			e_mbMiddle=				0x02,
			e_mbRight=				0x04,
			e_mbAll=				0x07,
			e_mbAny=				0x7fffffff,
		} MouseButtons;

		class Mask
		{
			public:
				Mask(	void)
					{
						m_source=e_sourceNull;
						m_item=e_itemNull;
						m_state=e_stateNull;
					}
				Mask(	Source source, Item item, State state)
					{
						m_source=source;
						m_item=item;
						m_state=state;
					}
				bool		operator <(const Mask &a_other) const
					{
						if(a_other.m_source > m_source) { return true; }
						if(a_other.m_source < m_source) { return false; }
						if(a_other.m_item > m_item) { return true; }
						if(a_other.m_item < m_item) { return false; }
						if(a_other.m_state > m_state) { return true; }
						if(a_other.m_state < m_state) { return false; }
						return false;
					}
				Source		source(void) const
					{	return m_source; }
				void		setSource(Source value)
					{	m_source=value; }
				void		orSource(Source value)
					{	m_source=(WindowEvent::Source)(m_source|value); }
				Item		item(void) const
					{	return m_item; }
				void		setItem(Item value)
					{	m_item=value; }
				void		orItem(Item value)
					{	m_item=(WindowEvent::Item)(m_item|value); }
				State		state(void) const
					{	return m_state; }
				void		setState(State value)
					{	m_state=value; }
				void		orState(State value)
					{	m_state=(WindowEvent::State)(m_state|value); }
				void		setSIS(Source source, Item item, State state)
					{
						m_source=source;
						m_item=item;
						m_state=state;
					}
			private:
				Source	m_source;
				Item	m_item;
				State	m_state;
		};
static	WindowEvent::Mask stringToMask(const String &a_string);

					WindowEvent(void)			{ setName(FE_EVENT_LAYOUT); }

virtual	void		addFunctors(void)
					{
						add(eventSource,
								FE_SPEC("evt:source",
								"Origin of change (alleged)"));
						add(eventItem,
								FE_SPEC("evt:item","Aspect of change"));
						add(eventState,
								FE_SPEC("evt:state","Effect of change"));
						add(eventState2,
								FE_SPEC("evt:state2","Orthogonal effect"));
						add(eventMouseX,
								FE_SPEC("evt:mouseX","X of mouse (optional)"));
						add(eventMouseY,
								FE_SPEC("evt:mouseY","Y of mouse (optional)"));
						add(eventMouseButtons,
								FE_SPEC("evt:mButtons",
								"Mouse buttons (optional)"));
					}

virtual	void		initializeRecord(void)		{ reset(); }

		bool		operator==(const WindowEvent &other) const
					{
						if(		source() != e_sourceAny
							&&	other.source() != e_sourceAny
							&&	source() != other.source())
						{
							return false;
						}
						if(		item() != e_itemAny
							&&	other.item() != e_itemAny
							&&	item() != other.item())
						{
							return false;
						}
						if(		state() != e_stateAny
							&&	other.state() != e_stateAny
							&&	state() != other.state())
						{
							return false;
						}
						return true;
					}

		bool		operator<(const WindowEvent &other) const
					{
						if(source() != e_sourceAny
							&&	other.source() != e_sourceAny)
						{
							if(source() < other.source())
							{
								return true;
							}
							if(source() > other.source())
							{
								return false;
							}
						}

						if(item() != e_itemAny
							&&	other.item() != e_itemAny)
						{
							if(item() < other.item())
							{
								return true;
							}
							if(item() > other.item())
							{
								return false;
							}
						}

						if(state() != e_stateAny
							&&	other.state() != e_stateAny)
						{
							if(state() < other.state())
							{
								return true;
							}
							if(state() > other.state())
							{
								return false;
							}
						}

						return false;
					}

		Source		source(void) const
					{	return Source(eventSource.check()?
								eventSource(): e_sourceNull); }
		void		setSource(Source value)
					{	eventSource()=value; }

		Item		item(void) const
					{	return Item(eventItem.check()?
								eventItem(): e_itemNull); }
		void		setItem(Item value)
					{	eventItem()=value; }

		State		state(void) const
					{	return State(eventState.check()?
								eventState(): e_stateNull); }
		void		setState(State value)
					{	eventState()=value; }

		State		state2(void) const
					{	return State(eventState2.check()?
								eventState2(): e_stateNull); }
		void		setState2(State value)
					{	eventState2()=value; }

		I32			mouseX(void) const
					{	return eventMouseX.check()? eventMouseX(): -1; }
		void		setMouseX(I32 value)
					{	eventMouseX()=value; }

		I32			mouseY(void) const
					{	return eventMouseY.check()? eventMouseY(): -1; }
		void		setMouseY(I32 value)
					{	eventMouseY()=value; }

		MouseButtons	mouseButtons(void) const
					{	return MouseButtons(eventMouseButtons.check()?
								eventMouseButtons(): e_mbNone); }
		void		setMouseButtons(MouseButtons value)
					{	eventMouseButtons()=value; }

		void		reset(void)
					{
						setSource(e_sourceNull);
						setItem(e_itemNull);
						setState(e_stateNull);
						setState2(e_stateNull);
						setMousePosition(0,0);
						setMouseButtons(e_mbNone);
					}

		void		getMousePosition(I32 *pMx,I32 *pMy) const
					{	*pMx=mouseX();
						*pMy=mouseY(); }

		void		setMousePosition(I32 mx,I32 my)
					{	setMouseX(mx);
						setMouseY(my); }

		void		setSIS(Source source,Item item,State state)
					{	setSource(source);
						setItem(item);
						setState(state); }

		BWORD		matchesSource(Source sourceMask) const
					{	return sourceMask==source() || sourceMask==e_sourceAny;}
		BWORD		matchesItem(Item itemMask) const
					{	return itemMask==item() || itemMask==e_itemAny ||
							itemMask==(item()&(e_itemAny^e_keyModifiers)); }
		BWORD		matchesItemUnmodified(Item itemMask) const
					{	return itemMask==item() || itemMask==e_itemAny; }
		BWORD		matchesState(State stateMask) const
					{	return stateMask==state() || stateMask==e_stateAny; }

		BWORD		is(Source sourceMask,Item itemMask,State stateMask) const
					{	return	matchesSource(sourceMask) &&
								matchesItem(itemMask) &&
								matchesState(stateMask); }
		BWORD		is(const WindowEvent &mask) const
					{	return	matchesSource(mask.source()) &&
								matchesItem(mask.item()) &&
								matchesState(mask.state()); }
		BWORD		is(const Mask &mask) const
					{	return	matchesSource(mask.source()) &&
								matchesItem(mask.item()) &&
								matchesState(mask.state()); }
		BWORD		isUnmodified(Source sourceMask,Item itemMask,
							State stateMask) const
					{	return	matchesSource(sourceMask) &&
								matchesItemUnmodified(itemMask) &&
								matchesState(stateMask); }
		BWORD		isModified(void)
					{	return (item()&e_keyModifiers)!=0x0; }
		BWORD		isModifiedBy(Item itemMask)
					{	return itemMask==(item()&e_keyModifiers); }

		BWORD		isNull(void) const
					{	return is(e_sourceNull,e_itemAny,e_stateAny); }
		BWORD		isExpose(void) const
					{	return is(e_sourceSystem,e_itemExpose,e_stateAny);}
		BWORD		isResize(void) const
					{	return is(e_sourceSystem,e_itemResize,e_stateAny);}
		BWORD		isCloseWindow(void) const
					{	return is(e_sourceSystem,e_itemCloseWindow,e_stateAny);}
		BWORD		isIdleMouse(void) const
					{	return is(e_sourceSystem,e_itemIdle,e_stateAny); }
		BWORD		isPoll(void) const
					{	return is(e_sourceSystem,e_itemPoll,e_stateAny); }
		BWORD		isContinuous(void) const
					{	return isIdleMouse() || isPoll(); }
		BWORD		isMouseEnter(void) const
					{	return is(e_sourceSystem,e_itemEnter,e_stateNull); }
		BWORD		isMouseExit(void) const
					{	return is(e_sourceSystem,e_itemExit,e_stateNull); }
		BWORD		isMouseMotion(void) const
					{	return is(e_sourceMousePosition,e_itemAny,e_stateAny); }
		BWORD		isMouseMove(void) const
					{	return is(e_sourceMousePosition,e_itemMove,e_stateAny);}
		BWORD		isMouseDrag(void) const
					{	return is(e_sourceMousePosition,e_itemDrag,e_stateAny);}
		BWORD		isKeyboard() const
					{	return is(e_sourceKeyboard,e_itemAny,e_stateAny);}
		BWORD		isKeyPress(Item key) const
					{	return is(e_sourceKeyboard,key,e_statePress); }
		BWORD		isKeyRepeat(Item key) const
					{	return is(e_sourceKeyboard,key,e_stateRepeat); }
		BWORD		isKeyRelease(Item key) const
					{	return is(e_sourceKeyboard,key,e_stateRelease); }
		BWORD		isKeyTyped(Item key) const
					{	return isKeyPress(key) || isKeyRepeat(key); }
		BWORD		isMouseButton(Item item=e_itemAny) const
					{	return is(e_sourceMouseButton,item,e_stateAny); }
		BWORD		isMousePress(Item item=e_itemAny) const
					{	return is(e_sourceMouseButton,item,e_statePress); }
		BWORD		isMouseRelease(Item item=e_itemAny) const
					{	return is(e_sourceMouseButton,item,e_stateRelease); }
		BWORD		isMouseWheel(void) const
					{	return is(e_sourceMouseButton,e_itemWheel,e_stateAny); }
		BWORD		isClear(void) const
					{	return is(e_sourceUser,e_windowClear,e_stateAny); }
		BWORD		isSwap(void) const
					{	return is(e_sourceUser,e_windowSwapBuffers,e_stateAny);}
		BWORD		isSystem(void) const
					{	return matchesSource(e_sourceSystem); }
		BWORD		isUser(void) const
					{	return matchesSource(e_sourceUser); }
		BWORD		isJoy(void) const
					{	return matchesSource(e_sourceJoy0); }
		BWORD		isJoyAxis(void) const
					{	return isJoy() && (item()&e_joyAxes); }
		BWORD		isJoyButton(void) const
					{	return is(e_sourceJoy0,e_joyButtons,e_stateAny);}

					/** @brief Returns TRUE if item can be affected by
						shift/control/caplock */
		BWORD		usesModifiers(void) const
					{	return (source()==e_sourceMousePosition ||
								source()==e_sourceMouseButton ||
								source()==e_sourceKeyboard); }
};

} /* namespace ext */
} /* namespace fe */

namespace fe
{

FE_DL_EXPORT String print(const ext::WindowEvent& event);

} /* namespace fe */

#endif /* __window_WindowEvent_h__ */
