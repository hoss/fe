/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __window_windowAS_h__
#define __window_windowAS_h__

namespace fe
{
namespace ext
{

/// general selection/picking
class AsSelect :
	public AccessorSet,
	public Initialize<AsSelect>
{
	public:
		void initialize(void)
		{
			add(selecting,			FE_USE("sel:selecting"));
			add(marked,				FE_USE("sel:marked"));
			add(selected,			FE_USE("sel:selected"));
		}
		/// state of selection (bits - 0: not  1: replace  2: add  4: mark only)
		Accessor<int>				selecting;
		/// marked (mid selection) records
		Accessor<sp<RecordGroup> >	marked;
		/// selected (post selection) records
		Accessor<sp<RecordGroup> >	selected;
};

/// screen space selection
class AsSelection :
	public AsSelect,
	public Initialize<AsSelection>
{
	public:
		void initialize(void)
		{
			add(start,			FE_USE("sel:start"));
			add(end,			FE_USE("sel:end"));
			add(prev,			FE_USE("sel:prev"));
		}
		/// 2D start point of selection box
		Accessor<Vector2>		start;
		/// 2D end point of selection box
		Accessor<Vector2>		end;
		/// 2D previous end point of selection box
		Accessor<Vector2>		prev;

};

/// world space selection
class AsPick :
	public AsSelect,
	public Initialize<AsPick>
{
	public:
		void initialize(void)
		{
			add(start,			FE_USE("pick:start"));
			add(end,			FE_USE("pick:end"));
			add(startv,			FE_USE("pick:startv"));
			add(endv,			FE_USE("pick:endv"));
			add(focus,			FE_USE("pick:focus"));
		}
		/// 3D start point of selection
		Accessor<Vector3>		start;
		/// 3D direction of selection
		Accessor<Vector3>		startv;
		/// 3D end point of selection
		Accessor<Vector3>		end;
		/// 3D end direction of selection
		Accessor<Vector3>		endv;
		/// 3D point of focus
		Accessor<Vector3>		focus;
};

/// color information
class FE_DL_EXPORT AsColor :
	public AccessorSet,
	public Initialize<AsColor>
{
	public:
		void initialize(void)
		{
			add(name,	FE_USE("col:name"));
			add(rgba,	FE_USE("col:rgba"));
		}
		/// name
		Accessor<String>			name;
		/// RGBA data
		Accessor<Color>				rgba;
};

/// UI selectable
class AsSelectable :
	public AccessorSet,
	public Initialize<AsSelectable>
{
	public:
		void initialize(void)
		{
			add(is,			FE_USE("sel:selectable"));
		}
		/// marker for being selected
		Accessor<void>			is;
};

class AsSelParent :
	public AccessorSet,
	public Initialize<AsSelParent>
{
	public:
		void initialize(void)
		{
			add(is,			FE_USE("sel:parent"));
			add(group,		FE_USE("sel:group"));
		}
		Accessor<void>				is;
		Accessor< sp<RecordGroup> >	group;
};


class AsSelWorldSphere :
	public AccessorSet,
	public Initialize<AsSelWorldSphere>
{
	public:
		void initialize(void)
		{
			add(is,			FE_USE("sel:wld:sphere"));
			add(location,	FE_USE("spc:at"));
			add(radius,		FE_USE("bnd:radius"));
		}
		Accessor<void>			is;
		Accessor<SpatialVector>	location;
		Accessor<Real>			radius;
};

class AsSelWorldTriangle :
	public AccessorSet,
	public Initialize<AsSelWorldTriangle>
{
	public:
		void initialize(void)
		{
			add(is,			FE_USE("sel:wld:tri"));
			add(vertA,		FE_USE("generic:v0"));
			add(vertB,		FE_USE("generic:v1"));
			add(vertC,		FE_USE("generic:v2"));
		}
		Accessor<void>			is;
		Accessor<SpatialVector>	vertA;
		Accessor<SpatialVector>	vertB;
		Accessor<SpatialVector>	vertC;
};

class AsSelScreenTriangle :
	public AccessorSet,
	public Initialize<AsSelScreenTriangle>
{
	public:
		void initialize(void)
		{
			add(is,			FE_USE("sel:scr:tri"));
			add(vertA,		FE_USE("generic:v0"));
			add(vertB,		FE_USE("generic:v1"));
			add(vertC,		FE_USE("generic:v2"));
		}
		Accessor<void>			is;
		Accessor<SpatialVector>	vertA;
		Accessor<SpatialVector>	vertB;
		Accessor<SpatialVector>	vertC;
};

class AsDrawScreenTriangle :
	public AccessorSet,
	public Initialize<AsDrawScreenTriangle>
{
	public:
		void initialize(void)
		{
			add(is,			FE_USE("draw:scr:tri"));
			add(vertA,		FE_USE("generic:v0"));
			add(vertB,		FE_USE("generic:v1"));
			add(vertC,		FE_USE("generic:v2"));
		}
		Accessor<void>			is;
		Accessor<SpatialVector>	vertA;
		Accessor<SpatialVector>	vertB;
		Accessor<SpatialVector>	vertC;
};


#if 0
class AsSelectableQuad :
	public AccessorSet,
	public Initialize<AsSelectable>
{
	public:
		void initialize(void)
		{
			add(vertA,		FE_USE("generic:v0"));
			add(vertB,		FE_USE("generic:v1"));
			add(vertC,		FE_USE("generic:v2"));
			add(vertD,		FE_USE("generic:v3"));
		}
};

class AsSelectableRect :
	public AccessorSet,
	public Initialize<AsSelectable>
{
	public:
		void initialize(void)
		{
			add(vertLo,		FE_USE("generic:v0"));
			add(vertHi,		FE_USE("generic:v1"));
		}
};

class AsSelectableExt :
	public AccessorSet,
	public Initialize<AsSelectable>
{
	public:
		void initialize(void)
		{
			add(data,		FE_USE("generic:r"));
			add(component,	FE_USE("generic:c"));
		}
};
#endif



/// projected
class AsProjected :
	public AccessorSet,
	public Initialize<AsProjected>
{
	public:
		void initialize(void)
		{
			add(location,		FE_USE("proj:location"));
		}
		/// marker for being selected
		Accessor<SpatialVector>		location;
};


/// orthographic projection related
class AsOrtho :
	public AccessorSet,
	public Initialize<AsOrtho>
{
	public:
		void initialize(void)
		{
			add(zoom,				FE_USE("ortho:zoom"));
			add(center,				FE_USE("ortho:center"));
		}
		/// zoom level
		Accessor<Real>				zoom;
		/// view center
		Accessor<Vector2>			center;
};

/// perspective projection related
class AsPerspective :
	public AccessorSet,
	public Initialize<AsPerspective>
{
	public:
		void initialize(void)
		{
			add(matrix,				FE_USE("persp:matrix"));
		}
		/// matrix (to be deprecated in favor of azimuth, incl, etc)
		Accessor<SpatialTransform>		matrix;
};


/** Callback signal wrapper.
	Intended for use in dataui Handlers, following the following guidelines.

	If component is a SignalerI, then the callback signal is sent to the
	signaler.

	Otherwise, if the component is a HandlerI, then the signal is sent directly
	to the Handler.  Note that this does not guaruntee that the HandlerI
	has been bound to a SignalerI, so handlers that depend on being bound
	may fail.
	*/
class AsCallback :
	public AccessorSet,
	public Initialize<AsCallback>
{
	public:
		void initialize(void)
		{
			add(component,		FE_USE("cb:component"));
			add(signal,			FE_USE("cb:signal"));
		}
		/// signaler
		Accessor<sp<Component> >	component;
		/// signal
		Accessor<Record>			signal;
};



} /* namespace ext */
} /* namespace fe */

#endif /* __window_windowAS_h__ */

