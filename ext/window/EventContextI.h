/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __window_EventContextI_h__
#define __window_EventContextI_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Interface for processing queued windowing events

	@ingroup window
*//***************************************************************************/
class FE_DL_EXPORT EventContextI:
	virtual public Component,
	public CastableAs<EventContextI>
{
	public:
virtual	void	loop(BWORD dropout)											=0;
				/// @brief lock the display for the given thread
virtual	void	threadLock(void)											=0;
				/// @brief unlock the display for the given thread
virtual	void	threadUnlock(void)											=0;

				/** @brief request buttonless mouse motion events

					This will can substantially increase the
					number of generated events, so it off
					by default. */
virtual	void	setPointerMotion(BWORD active)								=0;

				/// @brief check if pointer motion events are on
virtual	BWORD	pointerMotion(void)											=0;

virtual	Real	multiplication(void)										=0;

//~virtual U32	fontBase(void)												=0;

				/// @brief pixel height of the current font
//~virtual I32	fontHeight(I32* pAscent=NULL,I32* pDescent=NULL)			=0;
				/// @brief pixel width of string in the current font
//~virtual I32	pixelWidth(String string)									=0;

virtual	void	registerWindow(WindowI *window)								=0;
virtual	void	deregisterWindow(WindowI *window)							=0;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __window_EventContextI_h__ */
