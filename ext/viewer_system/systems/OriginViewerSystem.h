/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

namespace fe
{
namespace ext
{

/**
 * Displays world origin.
 */
class OriginViewerSystem : virtual public ViewerSystem
{
public:
	void draw(fe::Record viewportRecord) override
	{
		// Get the pointer to the draw interface.
		fe::hp<fe::ext::DrawI> draw =
				m_viewportAccessorSet.draw(viewportRecord);

		// draw origin
		const fe::Real pixelSizeOrigin = draw->view()->pixelSize(
			fe::SpatialVector(0,0,0),
			1.0f);
		const fe::Real pixelDesiredOrigin = 48.0f;
		fe::Real radiusOrigin = pixelDesiredOrigin/pixelSizeOrigin;
		fe::SpatialTransform identity;
		setIdentity(identity);

		const fe::Color black(0.0f,0.0f,0.0f);
		draw->drawTransformedMarker(
			identity,
			radiusOrigin,
			black);
	};
};

} /* namespace ext */
} /* namespace fe */
