/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#pragma once

namespace fe
{
namespace ext
{

/**
 * Viewer that runs on it's own thread.
 */
class FE_DL_EXPORT ThreadedViewerI :
    virtual public ViewerI,
    public CastableAs<ThreadedViewerI>
{
public:
    /**
     * Open the viewer window.
     *
     * The viewer will start in a separate thread.
     *
     * @param orchestrator The orchestrator determines the systems that get
     *                     signalled by the render thread. It also provides
     *                     a scope and dataset that we attach ourselves to.
     */
	virtual void open(sp<OrchestratorI> orchestrator) = 0;

    /**
     * Close the viewer window.
     *
     * Kills the viewer thread.
     */
	virtual void close() = 0;
};

} /* namespace ext */
} /* namespace fe */
