/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#pragma once

namespace fe
{
namespace ext
{

/**
 * Base class for Viewer Systems.
 *
 * A Viewer System is a System that reacts to draw notes. It is unique in that
 * it is aware of the thread it is being performed on. It delegates draw calls
 * to different viewports based on the current thread id.
 */
class FE_DL_EXPORT ViewerSystem :
	virtual public SystemI,
	public CastableAs<ViewerSystem>
{
public:
	/**
	 * Draw stuff into the viewport.
	 *
	 * Should be overriden by final classes.
	 *
	 * @param viewportRecord The viewport on this thread.
	 */
	virtual void draw(Record viewportRecord) = 0;

	/**
	 * Updates the Viewer records.
	 */
	void updateViewports(const t_note_id &note_id);

	/**
	 * Perform processing due to having been signaled.
	 *
	 * This will delegate drawing to different viewports based on the current
	 * thread id.
	 */
	void perform(const t_note_id &a_note_id) override;

	void connectOrchestrator(sp<OrchestratorI> orchestrator) override
	{
		m_dataset = orchestrator->dataset();
		m_drawNote = orchestrator->connect(this, FE_NOTE_DRAW);
		orchestrator->connect(this, &ViewerSystem::updateViewports, FE_NOTE_ADDED_VIEWPORT);
		orchestrator->connect(this, &ViewerSystem::updateViewports, FE_NOTE_REMOVED_VIEWPORT);
	};

protected:
	sp<RecordGroup>	m_dataset;

	/** ID of the draw note. */
	t_note_id m_drawNote;

	/** Accessors for viewport data. */
	AsViewport m_viewportAccessorSet;

	/** Available viewports. */
	std::map<std::thread::id, Record> m_viewportRecords;
	/** Mutex for viewportRecords. It is needed since this system has multiple
	 * threads performing different notes. */
	#if FE_CPLUSPLUS >= 201703L
		std::shared_mutex m_viewportRecordsMutex; // C++17
	#else
		Mutex m_viewportRecordsMutex;
	#endif
};

} /* namespace ext */
} /* namespace fe */
