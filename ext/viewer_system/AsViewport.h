/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#pragma once

namespace fe
{
namespace ext
{

/**
 * Accessor Set for a viewport.
 */
class FE_DL_EXPORT AsViewport :
	public AccessorSet,
	public Initialize<AsViewport>
{
public:
    void initialize()
    {
        add(draw, FE_USE("draw"));
        add(threadId, FE_USE("threadId"));
    }

    /**
	 * Draw Interface for actually drawing stuff.
	 *
	 * This is intentionally a weak pointer.
	 * Systems are notified when a viewport
	 * is removed and they MUST react accordingly to stop using the draw.
	 */
    Accessor<hp<DrawI>> draw;

    /** Thread id that this viewport runs in. */
    Accessor<std::thread::id> threadId;
};

} /* namespace ext */
} /* namespace fe */
