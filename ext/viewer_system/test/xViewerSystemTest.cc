/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "viewer_system_test.h"
#include <chrono>
#include <cstdlib>

using namespace fe;
using namespace ext;
using namespace std::chrono;

sp<Master> master;
sp<Registry> registry;

AsBox boxAccessorSet;
AsOrbiter orbiterAccessorSet;

void runSim(sp<OrchestratorI> orchestrator, nanoseconds duration)
{
    // Start time.
    steady_clock::time_point startTimePoint = steady_clock::now();

	// Time at which the last tick was run.
	steady_clock::time_point lastTimePoint = startTimePoint;

    while (lastTimePoint - startTimePoint < duration)
    {
        // Calculate dt.
		steady_clock::time_point currentTimePoint = steady_clock::now();
		std::chrono::duration<Real> dt = currentTimePoint - lastTimePoint;

        // Step.
		orchestrator->step(e_step_at_least, dt.count());

        // Save new time.
		lastTimePoint = currentTimePoint;

        // It'll tick too fast for numerical precision otherwise.
        microSleep(100);
    }
}

int main(int argc, char **argv)
{
    // Create registry.
    master = new Master;
    registry = master->registry();
    registry->manage("feAutoLoadDL");
    registry->manage("fexViewerSystemTestDL"); // I'm just too lazy to make a manifest.

    // Assert record types.
    sp<TypeMaster> typeMaster = master->typeMaster();
    assertData(typeMaster);
	assertMath(typeMaster);

    // Create records.
    sp<Scope> scope = registry->create("Scope");
    sp<RecordGroup> records(new RecordGroup());
    sp<Layout> boxLayout = scope->declare("orbiterbox");
    boxAccessorSet.populate(boxLayout);
    orbiterAccessorSet.populate(boxLayout);

    Record boxRecord = scope->createRecord(boxLayout);
    setIdentity(boxAccessorSet.transform(boxRecord));
    boxAccessorSet.size(boxRecord) = SpatialVector(0.5, 0.5, 0.5);
    orbiterAccessorSet.orbitRadius(boxRecord) = 1;
    orbiterAccessorSet.orbitPoint(boxRecord) = SpatialVector(0, 0, 0);

    records->add(boxRecord);

    // Create orchestrator.
    sp<OrchestratorI> orchestrator = registry->create("OrchestratorI.Orchestrator");
    orchestrator->datasetInitialize(records);
    orchestrator->setDT(1.0/100); // 100 HZ

    // Add the systems we want.
    sp<ViewerSystem> originViewerSystem = registry->create("ViewerSystem.OriginViewerSystem.fe");
    orchestrator->append(originViewerSystem);
    sp<ViewerSystem> viewerSystem = registry->create("ViewerSystem.BoxViewerSystem.fe_test");
    orchestrator->append(viewerSystem);
    sp<SystemI> orbiterSystem = registry->create("Stepper.OrbiterSystem.fe_test");
    orchestrator->append(orbiterSystem);

    orchestrator->compile();

    // // Thread collision test.
    // std::vector<sp<ThreadedViewerI>> viewers;

    // sp<ThreadedViewerI> viewer = registry->create("ThreadedViewerI.MOAViewer");
    // viewer->open(orchestrator);
    // viewers.push_back(viewer);

    // // Just keep spawning thread until there is a memory collision.
    // while (true)
    // {
    //     sp<ThreadedViewerI> viewer = registry->create("ThreadedViewerI.MOAViewer");
    //     viewer->open(orchestrator);
    //     viewers.push_back(viewer);
    //     sp<ThreadedViewerI> viewer2 = registry->create("ThreadedViewerI.MOAViewer");
    //     viewer2->open(orchestrator);
    //     viewers.push_back(viewer2);

    //     runSim(orchestrator, milliseconds{std::rand() * 500/RAND_MAX + 250});

    //     viewers.front()->close();
    //     viewers.erase(viewers.begin());
    //     viewers.front()->close();
    //     viewers.erase(viewers.begin());

	// 	runSim(orchestrator, milliseconds{std::rand() * 500/RAND_MAX + 250});
    // }

    sp<ThreadedViewerI> viewer1 = registry->create("ThreadedViewerI.MOAViewer");
    sp<ThreadedViewerI> viewer2 = registry->create("ThreadedViewerI.MOAViewer");

    feLog("viewer1->open()\n");
    viewer1->open(orchestrator);
    feLog("viewer2->open()\n");
    viewer2->open(orchestrator);
    runSim(orchestrator, seconds{3});

    feLog("viewer1->close()\n");
    viewer1->close();

    runSim(orchestrator, seconds{1});

    feLog("viewer1->open()\n");
    viewer1->open(orchestrator);

    runSim(orchestrator, seconds{3});

    feLog("viewer2 = NULL()\n");
    viewer2 = NULL;

    runSim(orchestrator, seconds{3});

    feLog("viewer1 = NULL()\n");
    viewer1 = NULL;

    return 0;
}
