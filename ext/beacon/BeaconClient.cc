/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "beaconClient.pmh"

#define FE_BEACON_CLIENT_VERBOSE FALSE

#define MILLISECOND_MONITOR_DELAY 20

using namespace fe;
using namespace ext;

namespace beacon
{

BeaconClient::BeaconClient()
{
	initVariables();
}

BeaconClient::~BeaconClient()
{
	shutdown();
}

void BeaconClient::initVariables()
{
	m_timeSynced = false;
	m_registered = false;

	m_monitorThread = nullptr;
	m_listUpdated = false;
	m_running = false;
	m_done = false;

	m_deltaTime = 0;
	m_latency = 0;
	m_myID = 0;
	m_lastSendTime = 0;
	m_lastRecTime = 0;
	m_updateCounter = 0;
}

bool BeaconClient::registerWithBeacon(const BeaconConfig &config,
	bool timeSync = true)
{
	m_config = config;

	if(timeSync)
	{
		if(!syncTimeWithBeacon())
		{
			return false;
		}
	}

	if(registerNode())
	{
		m_lastRecTime = getCurrentTime();

		// Start thread to monitor changes to the registered sims list
#if FE_BEACON_CLIENT_VERBOSE
		feLog("BeaconClient::registerAndTimeSync starting monitor thread\n");
#endif
		m_monitorThread = new std::thread(&BeaconClient::monitorThread,this);

		return true;
	}

	return false;
}

bool BeaconClient::syncTimeWithBeacon()
{
	if(!m_spSingleMaster.isValid())
	{
		m_spSingleMaster=SingleMaster::create();
	}
	sp<Master> spMaster=m_spSingleMaster->master();
	sp<Registry> spRegistry = spMaster->registry();

	Result result = spRegistry->manage("fexBeaconTimeSyncDL");
	if(!successful(result))
	{
		feLog("BeaconClient::syncTimeWithBeacon"
				" error loading fexBeaconTimeSyncDL\n");
		return false;
	}

	fe::sp<beacon::BeaconTimeSyncI>spBeaconTimeSync(
			spRegistry->create("*.BeaconTimeSync"));
	if(!spBeaconTimeSync.isValid())
	{
		feLog("BeaconClient::syncTimeWithBeacon"
				" error Creating BeaconTimeSync\n");
		return false;
	}

	m_timeSynced = spBeaconTimeSync->timeSyncWithBeacon(
			m_config.ipAddress, m_config.requestPort);

	if(m_timeSynced)
	{
		// Save results;
		m_latency = spBeaconTimeSync->getLatency();
		m_deltaTime = spBeaconTimeSync->getTimeOffset();

#if FE_BEACON_CLIENT_VERBOSE
		feLog("Time sync complete\n");
		feLog("Latency: %d\n", m_latency);
		feLog("Delta time: %d\n", m_deltaTime);
#endif
	}

	return m_timeSynced;
}

bool BeaconClient::init()
{
	shutdown();

	if(!m_spSingleMaster.isValid())
	{
		m_spSingleMaster=SingleMaster::create();
	}
	sp<Master> spMaster=m_spSingleMaster->master();
	sp<Registry> spRegistry = spMaster->registry();

	Result result = spRegistry->manage("fexMessageDL");
	if(failure(result))
	{
		feLog("BeaconClient::init error loading fexMessageDL\n");
		return false;
	}

	m_sendMsgSystem = spRegistry->create("MessageI.MessageUDP");
	m_recMsgSystem = spRegistry->create("MessageI.MessageUDP");

	if(!m_sendMsgSystem.isValid() || !m_recMsgSystem.isValid())
	{
		feLog("BeaconClient::init creating MessageUDP failed\n");
		return false;
	}

	if(!m_sendMsgSystem->postInit() || !m_recMsgSystem->postInit())
	{
		feLog("BeaconClient::init initialization of UDP system failed\n");
		return false;
	}

	m_responsePort = m_recMsgSystem->bindRandom();
	if(m_responsePort == 0)
	{
		feLog("BeaconClient::init"
				" failed trying to bind to a random response port\n");
		return false;
	}

	result = spRegistry->manage("fexMessageReliableUDPDL");
	if(failure(result))
	{
		feLog("BeaconClient::Error loading fexMessageReliableUDPDL\n");
		return false;
	}

	m_gdMessageSystem = spRegistry->create("*.MessageReliableUDP");

	if(!m_gdMessageSystem.isValid())
	{
		feLog("BeaconClient::init creating MessageReliableUDP failed\n");
		return false;
	}

	if(!m_gdMessageSystem->start(0))
	{
		feLog("BeaconClient::init"
				" initialization of reliable UDP system failed\n");
		return false;
	}

	return true;
}

void BeaconClient::shutdown()
{
	if(m_sendMsgSystem.isValid())
	{
		if(m_registered)
		{
			// Send shutdown message to Beacon server
			sendShutdown();
			m_registered = false;
		}

		// shutdown Beacon server monitoring thread
		if(m_monitorThread != nullptr)
		{
			m_running = false;
			while(!m_done)
			{
				std::this_thread::sleep_for(
						std::chrono::milliseconds(MILLISECOND_MONITOR_DELAY));
			}
		}

#if FE_BEACON_CLIENT_VERBOSE
		feLog("BeaconClient::shutdown shutting down send message system\n");
#endif
		m_sendMsgSystem->shutdown();
	}

	if(m_recMsgSystem.isValid())
	{
#if FE_BEACON_CLIENT_VERBOSE
		feLog("BeaconClient::shutdown shutting down receive message system\n");
#endif
		m_recMsgSystem->shutdown();
	}

	if(m_gdMessageSystem.isValid())
	{
#if FE_BEACON_CLIENT_VERBOSE
		feLog("BeaconClient::shutdown shutting down gd message system\n");
#endif
		m_gdMessageSystem->shutdown();
	}

	initVariables();
}

bool BeaconClient::registerNode()
{
	if(!init())
	{
		return false;
	}

	Messagegram msg;

	while(!m_registered)
	{
		sendRegisterRequest();
#if FE_BEACON_CLIENT_VERBOSE
		feLog("BeaconClient::registerNode"
				" waiting for message from the Beacon server\n");
#endif
		if(getBeaconMessage(&msg))
		{
			MPPmessageHeader *msgHeader = (MPPmessageHeader *)msg.data;

			switch((BeaconMessageType)msgHeader->msgType)
			{
				case BeaconMessageType::nodeList:
					{
#if FE_BEACON_CLIENT_VERBOSE
						feLog("BeaconClient::registerNode"
								" received node list\n");
#endif
						m_nodeListMutex.lock();
						m_registeredNodes.clear();

						NodeListMsg *simMsg = (NodeListMsg *)msg.data;
						for(uint8_t i = 0; i < simMsg->numNodes; i++)
						{
							m_registeredNodes.emplace_front(
								Node(simMsg->nodeList[i].id,
									 simMsg->nodeList[i].ipAddress));
						}
						m_myID = simMsg->yourID;
						m_listUpdated = true;
						m_nodeListMutex.unlock();

						m_updateCounter = simMsg->globalDictionaryUpdateCounter;

#if FE_BEACON_CLIENT_VERBOSE
						displayList();
#endif
						m_registered = true;
					}
					break;

				case BeaconMessageType::currentTime:
					feLog("BeaconClient::registerNode"
							" received node current time\n");
					break;

				default:
					feLog("BeaconClient::registerNode"
							" invalid message type %d\n", msgHeader->msgType);
					break;
			}
		}
	}

	return m_registered;
}


bool BeaconClient::getBeaconMessage(fe::ext::Messagegram *msg)
{
	bool result = (m_recMsgSystem->recvFrom(msg, 1, 0) &&
			validCRC(msg->data, msg->dataLen));

	if(result)
	{
		m_lastRecTime = getCurrentTime();
	}

	return result;
}

void BeaconClient::monitorThread()
{
#if FE_BEACON_CLIENT_VERBOSE
	feLog("BeaconClient::monitorThread monitor thread started\n");
#endif
	m_running = true;
	m_done = false;

	Messagegram msg;

	while(m_running.load(std::memory_order_relaxed))
	{
#if FE_BEACON_CLIENT_VERBOSE
	feLog(".\n");
#endif
		if(getBeaconMessage(&msg))
		{
			MPPmessageHeader *msgHeader = (MPPmessageHeader *)msg.data;

			switch((BeaconMessageType)msgHeader->msgType)
			{
				case BeaconMessageType::nodeList:
					{
#if FE_BEACON_CLIENT_VERBOSE
						feLog("BeaconClient::monitorThread"
								" received node list update\n");
#endif
						m_nodeListMutex.lock();
						m_registeredNodes.clear();

						NodeListMsg *simMsg = (NodeListMsg *)msg.data;
						for(uint8_t i = 0; i < simMsg->numNodes; i++)
						{
							m_registeredNodes.emplace_front(
								Node(simMsg->nodeList[i].id,
									 simMsg->nodeList[i].ipAddress));
						}
						m_listUpdated = true;
						m_nodeListMutex.unlock();
#if FE_BEACON_CLIENT_VERBOSE
						displayList();
#endif
					}
					break;

				case BeaconMessageType::keepAliveResponse:
					{
#if FE_BEACON_CLIENT_VERBOSE
						feLog("BeaconClient::monitorThread"
								" keepAliveResponse message received\n");
#endif
						m_lastRecTime = getCurrentTime();

						KeepAliveResponseMsg *responeMsg =
								(KeepAliveResponseMsg *)msg.data;

						m_updateCounter =
								responeMsg->globalDictionaryUpdateCounter;
					}
					break;

				default:
					break;
			}
		}

		// Check to see if we need to send a keep alive message to the Beacon
		if((getCurrentTime() - m_lastSendTime) >= KEEP_ALIVE_REFRESH_TIME)
		{
			sendKeepAlive();
		}
	}

	m_done = true;

#if FE_BEACON_CLIENT_VERBOSE
	feLog("BeaconClient::monitorThread Monitor thread shutting down\n");
#endif
}

void BeaconClient::sendRegisterRequest()
{
	Messagegram regMsg;

	RegisterNodeMsg *msg = (RegisterNodeMsg *)regMsg.data;
	regMsg.dataLen = sizeof(RegisterNodeMsg);

	msg->header.msgType = NodeMessageType::registrationRequest;
	msg->header.msgLen = sizeof(RegisterNodeMsg);
	msg->responsePort = m_responsePort;
	msg->CRC = calculateCRC(regMsg.data,
							sizeof(RegisterNodeMsg) - sizeof(msg->CRC),
							0);
#if FE_BEACON_CLIENT_VERBOSE
	feLog("BeaconClient::sendRegisterRequest"
			" sending register request msg to IP:%s Port:%d\n",
			m_config.ipAddress, m_config.requestPort);
#endif

	sendRequest(regMsg);
}

void BeaconClient::sendKeepAlive()
{
	Messagegram keepAliveMsg;

	KeepAliveMsg *msg = (KeepAliveMsg *)keepAliveMsg.data;
	keepAliveMsg.dataLen = sizeof(KeepAliveMsg);

	msg->header.msgType = NodeMessageType::keepAlive;
	msg->header.msgLen = sizeof(KeepAliveMsg);
	msg->responsePort = m_responsePort;
	msg->id = m_myID;
	msg->CRC = calculateCRC(keepAliveMsg.data,
							sizeof(KeepAliveMsg) - sizeof(msg->CRC),
							0);

#if FE_BEACON_CLIENT_VERBOSE
	feLog("BeaconClient::sendKeepAlive sending keep alive msg\n");
#endif

	sendRequest(keepAliveMsg);
}

void BeaconClient::sendShutdown()
{
	Messagegram shutdownMsg;

	ShutdownSimMsg *msg = (ShutdownSimMsg *)shutdownMsg.data;
	shutdownMsg.dataLen = sizeof(ShutdownSimMsg);

	msg->header.msgType = NodeMessageType::shutdown;
	msg->header.msgLen = sizeof(ShutdownSimMsg);
	msg->id = m_myID;
	msg->CRC = calculateCRC(shutdownMsg.data,
							sizeof(ShutdownSimMsg) - sizeof(msg->CRC),
							0);

#if FE_BEACON_CLIENT_VERBOSE
	feLog("BeaconClient::sendShutdown sending shutdown msg\n");
#endif

	sendRequest(shutdownMsg);
}

bool BeaconClient::sendRequest(const Messagegram &msg)
{
	m_lastSendTime = getCurrentTime();

	return m_sendMsgSystem->sendTo(msg,
			m_config.ipAddress, m_config.requestPort);
}

int64_t BeaconClient::getCurrentTime()
{
	int64_t time = std::chrono::duration_cast<std::chrono::milliseconds>
			(std::chrono::system_clock::now().time_since_epoch()).count();
	return time;
}

bool BeaconClient::isListUpdated()
{
	return m_listUpdated;
}

void BeaconClient::getList(std::list<Node> &registeredNodes)
{
	registeredNodes.clear();

	m_nodeListMutex.lock();
	for(Node &peer : m_registeredNodes)
	{
		registeredNodes.emplace_back(peer);
	}
	m_listUpdated = false;
	m_nodeListMutex.unlock();
}

void BeaconClient::displayList()
{
	char ipAddressStr[16];

	feLog("BeaconClient::displayList registered Nodes:\n")
	m_nodeListMutex.lock();
	for(Node &node : m_registeredNodes)
	{
		m_recMsgSystem->convertAddressToStr(node.ipAddress, ipAddressStr);
		feLog("ID:%d IP:%s\n", node.id, ipAddressStr);
	}
	m_nodeListMutex.unlock();
}

int64_t BeaconClient::getTimeOffset()
{
	return (m_timeSynced ? m_deltaTime : 0);
}

int64_t BeaconClient::getLatency()
{
	return m_latency;
}

int64_t BeaconClient::getBeaconTime()
{
	int64_t time = std::chrono::duration_cast<std::chrono::milliseconds>
			(std::chrono::system_clock::now().time_since_epoch()).count();
	return time + getTimeOffset();
}

bool BeaconClient::isBeaconAlive()
{
	const int64_t lastRecTime = m_lastRecTime.load(std::memory_order_relaxed);

	return ((getCurrentTime() - lastRecTime) < BEACON_ALIVE_TIME_LIMIT);
}

bool BeaconClient::dictionaryGet(
	std::vector<std::pair<fe::String,fe::String>> &list)
{
	if(list.size() == 0)
	{
		return true;
	}

#if FE_BEACON_CLIENT_VERBOSE
	feLog("BeaconClient::get sending get msg\n");
#endif
	uint32_t msgSize;
	char m_fromIPaddress[16]; // Address from where the request came from
	uint8_t *msg = nullptr;
	bool result = false;

	const bool includeValues = false;

	if(sendGDRequest(list, includeValues, NodeMessageType::getRequest))
	{
		if(waitForResponse(&msg, msgSize, m_fromIPaddress))
		{
			GDResponseMsg *responseMsg = (GDResponseMsg *)msg;

			if((BeaconMessageType)responseMsg->header.msgType ==
					BeaconMessageType::getResults)
			{
#if FE_BEACON_CLIENT_VERBOSE
				feLog("BeaconClient::get got get response\n");
#endif
				m_updateCounter = responseMsg->updateCounter;

				char *dataPtr = responseMsg->data;
				char *endOfData = dataPtr + responseMsg->header.msgLen;

				unsigned int i = 0;
				while(dataPtr < endOfData)
				{
					if(i >= list.size())
					{
						delete[] msg;
						return false;
					}

					char *value = dataPtr;
#if FE_BEACON_CLIENT_VERBOSE
					if(*value == '\0')
					{
						feLog("  [value not found]\n");
					}
					else
					{
						feLog("  %s\n", value);
					}
#endif
//~					feLog("GET %d \"%s\" as \"%s\"\n",i,
//~							list[i].first.c_str(),value);
					list[i].second = fe::String(value);
					i++;
					dataPtr += strlen(value)+1;
				}

				result = true;
			}
		}
	}

	delete[] msg;
	return result;
}


bool BeaconClient::waitForResponse(uint8_t **msg, uint32_t &msgSize,
	char *fromIPaddress)
{
	// Loop until we have a response
	while(!m_gdMessageSystem->recvReliableFrom(msg, msgSize, fromIPaddress))
	{
		if(!isBeaconAlive())
		{
			delete[] *msg;
			*msg = nullptr;
			return false;
		}

		std::this_thread::sleep_for(
				std::chrono::milliseconds(MILLISECOND_MONITOR_DELAY));
	}


	if(!validCRC(*msg, msgSize))
	{
		feLog("BeaconClient::waitForResponse Invalid CRC\n");
		delete[] msg;
		*msg = nullptr;
		return false;
	}

	return true;
}

bool BeaconClient::dictionarySet(
	const std::vector<std::pair<fe::String,fe::String>> &list)
{
	if(list.size() == 0)
	{
		return true;
	}

#if FE_BEACON_CLIENT_VERBOSE
	feLog("BeaconClient::set sending set msg\n");
#endif
	uint32_t msgSize;
	char m_fromIPaddress[16]; // Address from where the request came from
	uint8_t *msg = nullptr;
	bool result = false;

	const bool includeValues = true;

	if(sendGDRequest(list, includeValues, NodeMessageType::setRequest) &&
			waitForResponse(&msg, msgSize, m_fromIPaddress))
	{
		GDSimpleResponseMsg *responseMsg = (GDSimpleResponseMsg *)msg;
#if FE_BEACON_CLIENT_VERBOSE
		feLog("  Set result %u\n", responseMsg->result);
#endif
		result = ((BeaconMessageType)responseMsg->header.msgType ==
				BeaconMessageType::setResults);
		if(result)
		{
			m_updateCounter = responseMsg->updateCounter;
		}
	}

	delete[] msg;
	return result;
}

bool BeaconClient::dictionaryUnset(
	const std::vector<std::pair<fe::String,fe::String>> &list)
{
	if(list.size() == 0)
	{
		return true;
	}

#if FE_BEACON_CLIENT_VERBOSE
	feLog("BeaconClient::unset sending unset msg\n");
#endif
	uint32_t msgSize;
	char m_fromIPaddress[16]; // Address from where the request came from
	uint8_t *msg = nullptr;
	bool result = false;

	const bool includeValues = false;

	if(sendGDRequest(list, includeValues, NodeMessageType::unsetRequest) &&
			waitForResponse(&msg, msgSize, m_fromIPaddress))
	{
		GDSimpleResponseMsg *responseMsg = (GDSimpleResponseMsg *)msg;
#if FE_BEACON_CLIENT_VERBOSE
		feLog("  Unset result %u\n", responseMsg->result);
#endif
		result = ((BeaconMessageType)responseMsg->header.msgType ==
				BeaconMessageType::unsetResults);
		if(result)
		{
			m_updateCounter = responseMsg->updateCounter;
		}
	}

	delete[] msg;
	return result;
}

bool BeaconClient::dictionaryClear()
{
#if FE_BEACON_CLIENT_VERBOSE
	feLog("BeaconClient::Sending clear msg\n");
#endif
	uint32_t msgSize;
	char m_fromIPaddress[16]; // Address from where the request came from
	uint8_t *msg = nullptr;
	bool result = false;

	GDSimpleRequestMsg requestMsg;
	requestMsg.header.msgType = NodeMessageType::clearRequest;
	requestMsg.header.msgLen = sizeof(GDSimpleRequestMsg);
	requestMsg.responsePort = m_gdMessageSystem->getReceivePort();
	requestMsg.id = m_myID;
	requestMsg.CRC = calculateCRC(&requestMsg, sizeof(requestMsg) -
			sizeof(uint32_t), 0);

	if(m_gdMessageSystem->sendReliableTo((const uint8_t *)&requestMsg,
										 sizeof(requestMsg),
										 m_config.ipAddress,
										 m_config.gdRequestPort) &&
		waitForResponse(&msg, msgSize, m_fromIPaddress))
	{
		GDSimpleResponseMsg *responseMsg = (GDSimpleResponseMsg *)msg;
#if FE_BEACON_CLIENT_VERBOSE
		feLog("  Clear result %u\n", responseMsg->result);
#endif
		result = ((BeaconMessageType)responseMsg->header.msgType ==
				BeaconMessageType::clearResults);
		if(result)
		{
			m_updateCounter = responseMsg->updateCounter;
		}
	}

	delete[] msg;
	return result;
}

bool BeaconClient::dictionaryGetRegex(const fe::String searchString,
						std::vector<std::pair<fe::String,fe::String>> &list)
{
	list.clear();

#if FE_BEACON_CLIENT_VERBOSE
	feLog("BeaconClient::Sending get get all msg\n");
#endif
	uint8_t *msg = nullptr;
	uint32_t msgSize;
	char m_fromIPaddress[16]; // Address from where the request came from
	bool result = false;

	std::vector<std::pair<fe::String,fe::String>> searchList;
	searchList.push_back(std::make_pair(searchString,fe::String("")));

	const bool includeValues = false;

	if(sendGDRequest(searchList, includeValues,
			NodeMessageType::getRegexRequest) &&
			waitForResponse(&msg, msgSize, m_fromIPaddress))
	{
		GDResponseMsg *responseMsg = (GDResponseMsg *)msg;

		if((BeaconMessageType)responseMsg->header.msgType ==
				BeaconMessageType::getRegexResults)
		{
#if FE_BEACON_CLIENT_VERBOSE
			feLog("BeaconClient::Got regex request response\n");
#endif
			m_updateCounter = responseMsg->updateCounter;

			char *dataPtr = responseMsg->data;
			char *endOfData = dataPtr + responseMsg->header.msgLen;

			while(dataPtr < endOfData)
			{
				const char *name = dataPtr;
				dataPtr += strlen(name)+1;

				const char *value = dataPtr;
				dataPtr += strlen(value)+1;

				list.push_back(std::make_pair(fe::String(name),
						fe::String(value)));

#if FE_BEACON_CLIENT_VERBOSE
				feLog("%s : %s\n", name, value);
#endif
			}
			result = true;
		}
	}

	delete[] msg;
	return result;
}

uint64_t BeaconClient::dictionaryGetUpdateCounter()
{
	return m_updateCounter.load(std::memory_order_relaxed);
}

bool BeaconClient::sendGDRequest(
	const std::vector<std::pair<fe::String,fe::String>> &list,
	const bool includeValues,
	const NodeMessageType requestType)
{
	std::vector<uint8_t> requestMsg;
	requestMsg.clear();

	GDRequestMsg msgHeader;
	msgHeader.header.msgType = requestType;
	msgHeader.header.msgLen = 0;
	msgHeader.responsePort = m_gdMessageSystem->getReceivePort();
	msgHeader.id = m_myID;

	// Serialize header into the message
	uint8_t *bytes = (uint8_t *)&msgHeader;
	requestMsg.insert(requestMsg.end(), bytes, bytes + sizeof(GDRequestMsg)-1);

	// Serialize list of names and if values if requested
	uint32_t totalMsgDataSize = 0;

	const size_t listCount=list.size();
	for(size_t i = 0; i < listCount; i++)
	{
		U32 strLen = list[i].first.length()+1;
		uint8_t *bytes = (uint8_t *)list[i].first.c_str();
		requestMsg.insert(requestMsg.end(), bytes, bytes + strLen);

		totalMsgDataSize += strLen;

		if(includeValues)
		{

			U32 strLen = list[i].second.length()+1;
			uint8_t *bytes = (uint8_t *)list[i].second.c_str();
			requestMsg.insert(requestMsg.end(), bytes, bytes + strLen);

			totalMsgDataSize += strLen;
		}

#if FE_BEACON_CLIENT_VERBOSE
		feLog("BeaconClient::sendGDRequest %d/%d %s%s\n",
				i,listCount,list[i].first.c_str(),
				includeValues? (" = "+list[i].second).c_str(): "");
#endif
	}

	// Fill in the whole message length (not include the CRC that will be appended to the end)
	GDRequestMsg *msgPtr = (GDRequestMsg *)requestMsg.data();
	msgPtr->header.msgLen += totalMsgDataSize;

	// Calculate the CRC for the message and append it to the end of the message
	uint32_t crc = calculateCRC(requestMsg.data(), requestMsg.size(), 0);
	bytes = (uint8_t *)&crc;
    requestMsg.insert(requestMsg.end(), bytes, bytes + sizeof(uint32_t));

	return m_gdMessageSystem->sendReliableTo(requestMsg.data(),
			requestMsg.size(), m_config.ipAddress, m_config.gdRequestPort);
}

} // namespace beacon
