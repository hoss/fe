/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __BeaconTimeSync_h__
#define __BeaconTimeSync_h__

namespace beacon
{

class FE_DL_EXPORT BeaconTimeSync : virtual public BeaconTimeSyncI
{
public:
	BeaconTimeSync();
	virtual ~BeaconTimeSync();

	// Only do time sync with the Beacon without registering it
	virtual bool timeSyncWithBeacon(const char *ipAddress,
						const uint16_t requestPort) override;
	virtual int64_t getTimeOffset() override;
	virtual int64_t getBeaconTime()override;
	virtual int64_t getLatency() override;
private:
	char m_ipAddress[16];		// Beacon IP address (xxx.xxx.xxx.xxx\0)
	uint16_t m_requestPort;		// Beacon request port
	uint16_t m_responsePort;	// Response port messages from Beacon

	bool m_timeSynced;
	int64_t m_latency;
	int64_t m_deltaTime;

	fe::sp<fe::SingleMaster> m_spSingleMaster;
	fe::sp<fe::ext::MessageI> m_sendMsgSystem;
	fe::sp<fe::ext::MessageI> m_recMsgSystem;

	bool init();
	bool syncWithBeaconTime();
	int64_t getCurrentTime();

	void sendSyncRequest();
	bool sendRequest(const fe::ext::Messagegram &m);
    void shutdown();
};

} // namespace beacon

#endif // __BeaconTimeSync_h__
