/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __BeaconServerStartupI_h__
#define __BeaconServerStartupI_h__

namespace beacon
{

class FE_DL_EXPORT BeaconServerStartupI:
	virtual public BeaconServerI,
	public fe::CastableAs<BeaconServerStartupI>
{
public:
	/// @brief	Startup the Beacon server using a configuration file
	virtual bool startup(const char *fileName) = 0;
};

}

#endif // __BeaconServerStartupI_h__
