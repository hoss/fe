/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <regex>

#include "beaconServer.pmh"

#define FE_GLOBAL_CATALOG_VERBOSE FALSE

using namespace fe;
using namespace ext;

namespace beacon
{

GlobalDictionary::GlobalDictionary()
{
	m_initialized = false;
	m_running = false;
	m_done	= true;
	m_updateCounter = 0;

	m_requestThread = nullptr;
}

GlobalDictionary::~GlobalDictionary()
{
	stop();
}

void GlobalDictionary::setInitialEntries(
	const std::vector<std::pair<String,String>> &entries)
{
	// Use reservered ID zero for entries created on initialization
	uint8_t id = 0;

	for(size_t i = 0; i < entries.size(); i++)
	{
		m_globalDictionary[entries[i].first.c_str()] =
				std::make_pair(id, entries[i].second.c_str());
	}

#if FE_GLOBAL_CATALOG_VERBOSE
	displayDictionary();
#endif
}

bool GlobalDictionary::start(uint16_t receivePort)
{
	stop();

	if(!m_spSingleMaster.isValid())
	{
		m_spSingleMaster=SingleMaster::create();
	}
	sp<Master> spMaster=m_spSingleMaster->master();
	sp<Registry> spRegistry = spMaster->registry();

	Result result = spRegistry->manage("fexMessageReliableUDPDL");
	if(failure(result))
	{
		feLog("GlobalDictionary::start"
				" error loading fexMessageReliableUDPDL\n");
		return false;
	}

	m_reliableMessageSystem = spRegistry->create("*.MessageReliableUDP");

	if(!m_reliableMessageSystem.isValid())
	{
		feLog("GlobalDictionary::start of UDP send system failed\n");
		return false;
	}

	if(!m_reliableMessageSystem->start(receivePort))
	{
		feLog("GlobalDictionary::start"
				" reliable message system failed to start\n");
		return false;
	}

	if(m_requestThread == nullptr)
	{
		m_requestThread =
				new std::thread(&GlobalDictionary::requestThread,this);
		if(m_requestThread == nullptr)
		{
			feLog("GlobalDictionary::start failed to start request thread\n");
			return false;
		}
	}

	m_initialized = true;
	return true;
}

void GlobalDictionary::stop(void)
{
#if FE_GLOBAL_CATALOG_VERBOSE
	feLog( "GlobalDictionary::stop\n");
#endif
	m_running = false;

	if(m_requestThread != nullptr)
	{
		if(m_done == false)
		{
#if FE_GLOBAL_CATALOG_VERBOSE
			feLog( "GlobalDictionary::stop"
					" telling request thread to shutdown\n");
#endif
			while(m_done == false)
			{
				std::this_thread::sleep_for(std::chrono::milliseconds(100));
			}
		}
		m_requestThread = nullptr;
	}

	if(m_reliableMessageSystem.isValid())
	{
#if FE_GLOBAL_CATALOG_VERBOSE
		feLog( "GlobalDictionary::stop shutting reliable message system\n");
#endif
		m_reliableMessageSystem->shutdown();
		m_reliableMessageSystem = nullptr;
	}

	m_initialized = false;
}


bool GlobalDictionary::running()
{
	return m_running;
}

void GlobalDictionary::requestThread()
{
	feLog("GlobalDictionary:requestThread request thread started\n");

	m_done = false;
	m_running = true;
	uint8_t *msg;
	uint32_t msgSize;

#if FE_GLOBAL_CATALOG_VERBOSE
	feLog("GlobalDictionary:requestThread waiting for msg\n");
#endif

	while(m_running)
	{
		if(m_reliableMessageSystem->recvReliableFrom(&msg, msgSize,
				m_fromIPaddress) && validCRC(msg, msgSize))
		{
			MPPmessageHeader *msgHeader = (MPPmessageHeader *)msg;

			switch((NodeMessageType)msgHeader->msgType)
			{
				case NodeMessageType::getRequest:
					{
#if FE_GLOBAL_CATALOG_VERBOSE
						feLog("GlobalDictionary::requestThread"
								" received get request\n");
#endif
						GDRequestMsg *getRequestMsg = (GDRequestMsg *)msg;
						char *dataEnd = getRequestMsg->data +
								getRequestMsg->header.msgLen;

						dictionaryGet((char *)getRequestMsg->data, dataEnd,
								getRequestMsg->responsePort);
					}
					break;

				case NodeMessageType::setRequest:
					{

						GDRequestMsg *setRequestMsg = (GDRequestMsg *)msg;
						char *dataEnd = setRequestMsg->data +
								setRequestMsg->header.msgLen;
#if FE_GLOBAL_CATALOG_VERBOSE
						feLog("GlobalDictionary::requestThread"
								" received set request\n");
#endif
						dictionarySet(setRequestMsg->id,
								(char *)setRequestMsg->data, dataEnd,
								setRequestMsg->responsePort);
					}
					break;

				case NodeMessageType::unsetRequest:
					{
#if FE_GLOBAL_CATALOG_VERBOSE
						feLog("GlobalDictionary::requestThread"
								" received unset request\n");
#endif
						GDRequestMsg *unsetRequestMsg = (GDRequestMsg *)msg;
						char *dataEnd = unsetRequestMsg->data +
								unsetRequestMsg->header.msgLen;

						dictionaryUnset((char *)unsetRequestMsg->data, dataEnd,
								unsetRequestMsg->responsePort);
					}
					break;

				case NodeMessageType::clearRequest:
					{
						GDSimpleRequestMsg *clearRequestMsg =
								(GDSimpleRequestMsg *)msg;
#if FE_GLOBAL_CATALOG_VERBOSE
						feLog("GlobalDictionary::Received clear request\n");
#endif
						dictionaryClear(clearRequestMsg->responsePort);
					}
					break;

				case NodeMessageType::getRegexRequest:
					{
						GDRequestMsg *getRegexRequestMsg = (GDRequestMsg *)msg;
#if FE_GLOBAL_CATALOG_VERBOSE
						feLog("GlobalDictionary::Received clear request\n");
#endif
						dictionaryGetRegex(getRegexRequestMsg->data,
								getRegexRequestMsg->responsePort);
					}
					break;

				default:
					feLog("GlobalDictionary::requestThread"
							" invalid message type %d\n", msgHeader->msgType);
					break;
			}

			delete[] msg;
		}
		else
		{
#if FE_GLOBAL_CATALOG_VERBOSE
			feLog(".\n");
#endif
			std::this_thread::sleep_for(std::chrono::milliseconds(100));
		}
	}

	m_done = true;
}

void GlobalDictionary::dictionaryGet(char *nameList, char *nameListEnd,
	uint16_t responsePort)
{
	m_responseMsg.clear();

	GDResponseMsg msgHeader;
	msgHeader.header.msgType = BeaconMessageType::getResults;
	msgHeader.header.msgLen = 0;
	msgHeader.responsePort = responsePort;

	// Serialize header into the message
	uint8_t *bytes = (uint8_t *)&msgHeader;
	m_responseMsg.insert(m_responseMsg.end(), bytes,
			bytes + sizeof(GDResponseMsg)-1);

	char *name;
	fe::String value;
	uint32_t totalMsgDataSize = 0;

	while(nameList < nameListEnd)
	{
		name = nameList;
		nameList += strlen(name)+1;

#if FE_GLOBAL_CATALOG_VERBOSE
		feLog("  Search for: %s\n", name);
#endif
		auto it = m_globalDictionary.find(name);
		if(it == m_globalDictionary.end())
		{
#if FE_GLOBAL_CATALOG_VERBOSE
			feLog("  Entry not found\n");
#endif
			value = fe::String("");
		}
		else
		{
#if FE_GLOBAL_CATALOG_VERBOSE
			feLog("  Entry found: %s owned by %d\n",
					(it->second).second.c_str(), (it->second).first);
#endif
			value = (it->second).second;
		}

		// Serialize the value into buffer
		U32 valueLen = value.length()+1;
		bytes = (uint8_t *)value.c_str();
		m_responseMsg.insert(m_responseMsg.end(), bytes, bytes + valueLen);

		totalMsgDataSize += valueLen;
	}

	// Fill in the whole message length
	// (not include the CRC that will be appended to the end)
	GDResponseMsg *msgPtr = (GDResponseMsg *)m_responseMsg.data();
	msgPtr->header.msgLen += totalMsgDataSize;
	msgPtr->updateCounter = m_updateCounter;

	// Calculate the CRC for the message and append it to the end
	uint32_t crc = calculateCRC(m_responseMsg.data(), m_responseMsg.size(), 0);
	bytes = (uint8_t *)&crc;
	m_responseMsg.insert(m_responseMsg.end(), bytes, bytes + sizeof(uint32_t));

#if FE_GLOBAL_CATALOG_VERBOSE
	feLog("GlobalDictionary::get sending get response msg\n");
#endif
	sendResponseMsg(responsePort);
}

void GlobalDictionary::dictionarySet(uint8_t id, char *nameValueList,
	char *nameValueListEnd, uint16_t responsePort)
{
	char *name;
	char *value;
	uint16_t result = 0;

	while(nameValueList < nameValueListEnd)
	{
		name = nameValueList;
		nameValueList += strlen(name)+1;

		if( nameValueList == nameValueListEnd)
		{
#if FE_GLOBAL_CATALOG_VERBOSE
			feLog("GlobalDictionary::dictionarySet"
					" missing value for name %s\n", name);
#endif
			break;
		}

		value = nameValueList;
		nameValueList += strlen(value)+1;

#if FE_GLOBAL_CATALOG_VERBOSE
		feLog("GlobalDictionary::set - name:%s with value:%s\n", name, value);
#endif
		m_globalDictionary[name] = std::make_pair(id, value);
		result++;
	}

	if(result > 0)
	{
		m_updateCounter++;
	}
	setupSimpleResponse(BeaconMessageType::setResults, result);
	sendResponseMsg(responsePort);
}

void GlobalDictionary::dictionaryUnset(char *nameList, char *nameListEnd,
	uint16_t responsePort)
{
	char *name;
	uint16_t result = 0;

	while(nameList < nameListEnd)
	{
		name = nameList;
		nameList += strlen(name)+1;

#if FE_GLOBAL_CATALOG_VERBOSE
		feLog("GlobalDictionary::unset - name:%s\n", name);
#endif
		m_globalDictionary.erase(fe::String(name));
		result++;
	}

	if(result > 0)
	{
		m_updateCounter++;
	}

	setupSimpleResponse(BeaconMessageType::unsetResults, result);
	sendResponseMsg(responsePort);
}


void GlobalDictionary::dictionaryGetRegex(char *searchName,
	uint16_t responsePort)
{
	m_responseMsg.clear();

	GDResponseMsg msgHeader;
	msgHeader.header.msgType = BeaconMessageType::getRegexResults;
	msgHeader.header.msgLen = 0;
	msgHeader.responsePort = responsePort;

	// Serialize header into the message
	uint8_t *bytes = (uint8_t *)&msgHeader;
	m_responseMsg.insert(m_responseMsg.end(), bytes,
			bytes + sizeof(GDResponseMsg)-1);

	const std::regex regex_str(searchName);

#if FE_GLOBAL_CATALOG_VERBOSE
	feLog("  Search for: %s\n", searchName);
#endif

	uint32_t totalMsgDataSize = 0;

	for(auto it = m_globalDictionary.begin();
			it != m_globalDictionary.end(); ++it)
	{
		std::cmatch m;

		if(std::regex_search(it->first.c_str(), m, regex_str))
		{
#if FE_GLOBAL_CATALOG_VERBOSE
			feLog("  Entry found: %s owned by %d\n",
					(it->second).second.c_str(), (it->second).first);
#endif
			// Copy name info buffer
			U32 len = it->first.length()+1;
			uint8_t *bytes = (uint8_t *)it->first.c_str();
			m_responseMsg.insert(m_responseMsg.end(), bytes, bytes + len);
			totalMsgDataSize += len;

			// Copy value into buffer
			len = (it->second).second.length()+1;
			bytes = (uint8_t *)(it->second).second.c_str();
			m_responseMsg.insert(m_responseMsg.end(), bytes, bytes + len);
			totalMsgDataSize += len;
		}
	}

	// Fill in the whole message length
	// (not include the CRC that will be appended to the end)
	GDResponseMsg *msgPtr = (GDResponseMsg *)m_responseMsg.data();
	msgPtr->header.msgLen += totalMsgDataSize;
	msgPtr->updateCounter = m_updateCounter;

	// Calculate the CRC for the message and append it to the end
	uint32_t crc = calculateCRC(m_responseMsg.data(), m_responseMsg.size(), 0);
	bytes = (uint8_t *)&crc;
	m_responseMsg.insert(m_responseMsg.end(), bytes, bytes + sizeof(uint32_t));

#if FE_GLOBAL_CATALOG_VERBOSE
	feLog("GlobalDictionary:: Sending get regex response msg\n");
#endif

	sendResponseMsg(responsePort);
}

void GlobalDictionary::dictionaryClear(uint16_t responsePort)
{
	if(m_globalDictionary.size() > 0)
	{
		m_updateCounter++;
	}

	m_globalDictionary.clear();

	setupSimpleResponse(BeaconMessageType::clearResults, 1);
	sendResponseMsg(responsePort);
}

void GlobalDictionary::setupSimpleResponse(uint8_t type, uint16_t result)
{
	m_responseMsg.clear();
	GDSimpleResponseMsg msg;

	msg.header.msgType = type;
	msg.header.msgLen = sizeof(msg.result);
	msg.updateCounter = m_updateCounter;

	msg.result = result;
	msg.CRC = calculateCRC(&msg,
							sizeof(GDSimpleResponseMsg) - sizeof(msg.CRC),
							0);

	uint8_t *msgPtr = (uint8_t *)&msg;
	m_responseMsg.insert(m_responseMsg.end(),
						 msgPtr,
						 msgPtr+sizeof(GDSimpleResponseMsg));
}

void GlobalDictionary::sendResponseMsg(uint16_t responsePort)
{
	m_reliableMessageSystem->sendReliableTo(m_responseMsg.data(),
			m_responseMsg.size(), m_fromIPaddress, responsePort);
}

void GlobalDictionary::deleteEntriesByID(uint8_t id)
{
#if FE_GLOBAL_CATALOG_VERBOSE
		feLog("GlobalDictionary::Deleting entries with ID: %u\n", id);
#endif

	for(auto it = m_globalDictionary.cbegin(); it != m_globalDictionary.cend();)
	{
		if((it->second).first == id)
		{
			it = m_globalDictionary.erase(it);
		}
		else
		{
			++it;
		}
	}

#if FE_GLOBAL_CATALOG_VERBOSE
	displayDictionary();
#endif
}


void GlobalDictionary::clear()
{
#if FE_GLOBAL_CATALOG_VERBOSE
	feLog("GlobalDictionary::clear:\n");
#endif
	m_globalDictionary.clear();
}

void GlobalDictionary::displayDictionary()
{
#if FE_GLOBAL_CATALOG_VERBOSE
	feLog("GlobalDictionary::Current dictionary:\n");
#endif

	for(auto it = m_globalDictionary.cbegin();
			it != m_globalDictionary.cend(); ++it)
	{
		feLog("ID:%u\t%s\t%s\n", (it->second).first,
				it->first.c_str(), (it->second).second.c_str());
	}
}

} // namespace beacon
