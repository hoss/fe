/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <thread>
#include <chrono>

#include "datatool/datatool.h"
#include "window/WindowEvent.h"
#include "beacon/beacon.h"

using namespace fe;
using namespace fe::ext;


class UserInterface:virtual public HandlerI
{
	public:
		UserInterface()
		{
			setName("UserInterface");
			m_running = true;
		}

		bool isRunning()
		{
			return m_running;
		}

		void handle(Record & a_signal)
		{
			m_event.bind(a_signal);

			if(!m_event.isPoll())
			{
				if(
					(m_event.isKeyPress(WindowEvent::Item('q'))) ||
					(m_event.isKeyPress(WindowEvent::Item('Q'))) )
				{
					m_running = false;
				}
			}
		}

	private:
		WindowEvent m_event;
		bool m_running;
};


int main(int argc,char** argv)
{
	uint16_t beaconPort = 5000;		// Beacon request port
	uint16_t gdPort = 5001;			// Global dictionary request port

	if(argc >= 2)
	{
		beaconPort = atoi(argv[1]);

		if(argc >= 3)
		{
			gdPort = atoi(argv[2]);
		}
	}

	// Provide scope for auto destruction of pointers, etc..
	{
		sp<Master> spMaster(new Master);
		sp<Registry> spRegistry=spMaster->registry();
		spRegistry->manage("feAutoLoadDL");

		spRegistry->manage("fexBeaconServerDL");
		sp<beacon::BeaconServerI> spBeaconServer(
			spRegistry->create("*.BeaconServer"));

		if(!spBeaconServer.isValid())
		{
			feX(argv[0], "couldn't create Beacon server component");
			return 1;
		}

		// Create Keyboard Handler
		sp<SignalerI> spSignalerI(spRegistry->create("SignalerI"));
		sp<Scope> spScope=spRegistry->create("Scope");
		WindowEvent pollEvent;
		pollEvent.bind(spScope);
		Record pollRecord=pollEvent.createRecord();
		sp<Layout> pollLayout=pollRecord.layout();
		pollEvent.setSIS(WindowEvent::e_sourceSystem,
				WindowEvent::e_itemPoll,WindowEvent::e_stateNull);

		sp<HandlerI> spKeyHandler=spRegistry->create("*.ConsoleKeys");
		if(spKeyHandler.isValid())
		{
			spSignalerI->insert(spKeyHandler,pollLayout);
		}

		sp<UserInterface> spMyUserInterface(new UserInterface());
		spSignalerI->insert(spMyUserInterface,pollLayout);

		// Initalize beacon activity
		spBeaconServer->start(beaconPort, gdPort);

		bool sameline = false;
		uint16_t timestamp_len = 0;
		String msg;

		// Operate until either an internal beacon termination condition
		// occurs, or a console quit is received.
		while(true)
		{
			if(!spBeaconServer->running())
			{
				msg = "Internal BeaconServer no longer running\n";
				break;
			}

			if(!spMyUserInterface->isRunning())
			{
				msg = "Console quit request received\n";
				break;
			}

			spSignalerI->signal(pollRecord);
			std::this_thread::sleep_for(std::chrono::milliseconds(100));

			// Display time
			int64_t now = std::chrono::duration_cast<std::chrono::milliseconds>
				(std::chrono::system_clock::now().time_since_epoch()).count();
			int64_t ms = now % 1000;
			now /= 1000;
			int64_t seconds = now % 60;
			now /= 60;
			int64_t minutes = now % 60;
			now /= 60;
			int64_t hours = now;

			if(sameline) {
				std::cout << "\r";
			}
			else
			{
				sameline = true;
			}

			msg.sPrintf("beacon time: %dhr %dmin %dsec %dms",
				hours, minutes, seconds, ms);
			std::cout << msg.c_str();

			// If this timestamp string isn't as long as the previous, we need
			// to blank out the risidual trailing chars from the prior output.
			uint16_t len = msg.length();

			if(len < timestamp_len)
			{
				std::cout << std::string(timestamp_len-len, ' ');
			}

			timestamp_len = len;
		}

		// If any timestamps got emitted, we need to clear that stdout line.
		if(sameline)
		{
			std::cout << "\n";
		}

		// msg will be the reason for stopping.
		feLog(msg.c_str());

		feLog("Beacon host shutting down\n");
		spBeaconServer->stop();

	}

	feLog("Beacon host terminated.\n");
	return 0;
}
