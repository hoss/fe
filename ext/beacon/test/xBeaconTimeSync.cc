/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <chrono>
#include "beacon/beacon.h"

int main(int argc,char** argv)
{

	// Setup default values
	char ipAddress[16];			// Beacon IP address (xxx.xxx.xxx.xxx\0)
	strcpy(ipAddress, "127.0.0.1");
	uint16_t requestPort = 5000;  // Beacon request port

    fe::sp<fe::Master> spMaster(new fe::Master);
    fe::sp<fe::Registry> spRegistry=spMaster->registry();

    spRegistry->manage("fexBeaconTimeSyncDL");
    fe::sp<beacon::BeaconTimeSyncI>spBeaconTimeSync(
			spRegistry->create("*.BeaconTimeSync"));
	feLog("spBeaconTimeSync valid %d\n",spBeaconTimeSync.isValid());

	if(!spBeaconTimeSync.isValid())
    {
        feX(argv[0], "couldn't create component");
		return 1;
    }

	// Let any of the parameters to be replaced
	if( argc >= 2)
	{
		strncpy(ipAddress, argv[1], sizeof(ipAddress)-1);

		if( argc >= 3)
		{
			requestPort = atoi(argv[2]);
		}
	}

	feLog("Beacon time sync at IP:%s requestPort:%u\n",
			ipAddress, requestPort);

	bool timeSynced = spBeaconTimeSync->timeSyncWithBeacon(ipAddress, requestPort);

    if(timeSynced)
	{
        // Save results;
        int64_t latency = spBeaconTimeSync->getLatency();
        int64_t deltaTime = spBeaconTimeSync->getTimeOffset();

		feLog("Time sync complete\n");
		feLog("Latency: %d\n", latency);
		feLog("Delta time: %d\n", deltaTime);
		return 0;
	}
	else
	{
		feLog("Time sync failed\n");
		return 1;
	}
}

