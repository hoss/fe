/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <chrono>
#include "beacon/beacon.h"

int main(int argc,char** argv)
{
		// Setup default configuration
	beacon::BeaconConfig config;
	strcpy(config.ipAddress, "127.0.0.1");
	config.requestPort     		= 5000; // Beacon request port
	config.gdRequestPort   		= 5001; // Global dictionary request port

    fe::sp<fe::Master> spMaster(new fe::Master);
    fe::sp<fe::Registry> spRegistry=spMaster->registry();

    spRegistry->manage("fexBeaconClientDL");
    fe::sp<beacon::BeaconClientI>spBeaconClient(
			spRegistry->create("*.BeaconClient"));
	feLog("spBeaconClient valid %d\n",spBeaconClient.isValid());

	if(!spBeaconClient.isValid())
    {
        feX(argv[0], "couldn't create component");
		return 1;
    }


	// Let any of the configuration parameters to be replaced
	if( argc >= 2)
	{
		strncpy(config.ipAddress, argv[1], sizeof(config.ipAddress)-1);

		if( argc >= 3)
		{
			config.requestPort = atoi(argv[2]);
		}
	}

	feLog("Connecting to Beacon at IP:%s requestPort:%u\n",
			config.ipAddress, config.requestPort);
	feLog("Connecting to Global Dictionary gdRequestPort: %u\n",
			config.gdRequestPort);

	if(spBeaconClient->registerWithBeacon(config, true))
	{
		feLog("Latency: %u\n", spBeaconClient->getLatency());
		feLog("Delta time: %d\n", spBeaconClient->getTimeOffset());
	}

	feLog("Testing keep alive...\n");
	// Wait for 15 second to make sure the keep alive messages are working
	for(int i = 0; i < 15; i++)
	{
		if(spBeaconClient->isBeaconAlive())
		{
			feLog("Beacon is alive\n");
		}
		else
		{
			feLog("Beacon is dead\n");
		}

		fe::milliSleep(1000);
	}
	feLog("Keep alive test finished\n");


	feLog("Testing global dictionary...\n");

	uint64_t counter;
	counter = spBeaconClient->dictionaryGetUpdateCounter();
	feLog("Current GD update counter %u\n", counter);

	std::vector<std::pair<fe::String,fe::String>> setList;

	setList.push_back(std::make_pair(
			fe::String("name1"),fe::String("value1")));
	setList.push_back(std::make_pair(
			fe::String("name2"),fe::String("value2")));
	setList.push_back(std::make_pair(
			fe::String("name3"),fe::String("value3")));
	setList.push_back(std::make_pair(
			fe::String("name3"),fe::String("value3a")));
	setList.push_back(std::make_pair(
			fe::String("name3a"),fe::String("value3b")));
	setList.push_back(std::make_pair(
			fe::String("name3b"),fe::String("value3c")));

	spBeaconClient->dictionarySet(setList);

	counter = spBeaconClient->dictionaryGetUpdateCounter();
	feLog("Current GD update counter %u\n", counter);

	std::vector<std::pair<fe::String,fe::String>> getList;
	getList.push_back(std::make_pair(fe::String("name1"),    fe::String("")));
	getList.push_back(std::make_pair(fe::String("fake_name"),fe::String("")));
	getList.push_back(std::make_pair(fe::String("name3"),    fe::String("")));

	spBeaconClient->dictionaryGet(getList);

	counter = spBeaconClient->dictionaryGetUpdateCounter();
	feLog("Current GD update counter %u\n", counter);

	spBeaconClient->dictionaryUnset(getList);

	counter = spBeaconClient->dictionaryGetUpdateCounter();
	feLog("Current GD update counter %u\n", counter);

	std::vector<std::pair<fe::String,fe::String>> getList2;
	getList2.push_back(std::make_pair(fe::String("name1"), fe::String("")));
	getList2.push_back(std::make_pair(fe::String("name2"), fe::String("")));

	spBeaconClient->dictionaryGet(getList2);

	counter = spBeaconClient->dictionaryGetUpdateCounter();
	feLog("Current GD update counter %u\n", counter);

	spBeaconClient->dictionaryClear();

	counter = spBeaconClient->dictionaryGetUpdateCounter();
	feLog("Current GD update counter %u\n", counter);

	spBeaconClient->dictionaryGet(getList2);

	counter = spBeaconClient->dictionaryGetUpdateCounter();
	feLog("Current GD update counter %u\n", counter);

	spBeaconClient->dictionarySet(setList);

	counter = spBeaconClient->dictionaryGetUpdateCounter();
	feLog("Current GD update counter %u\n", counter);

	std::vector<std::pair<fe::String,fe::String>> list3;

	spBeaconClient->dictionaryGetRegex(fe::String("name3"), list3);

	counter = spBeaconClient->dictionaryGetUpdateCounter();
	feLog("Current GD update counter %u\n", counter);

	spBeaconClient->shutdown();

	// Display time + delta time offset
	while(true)
	{
		int64_t now = spBeaconClient->getBeaconTime();

		int64_t ms = now % 1000;
		now /= 1000;
		int64_t seconds = now % 60;
		now /= 60;
		int64_t minutes = now % 60;
		now /= 60;
		int64_t hours = now;

		std::cout << "h:" << hours
		<< " m:" << minutes
		<< " s:" << seconds
		<< " ms:" << ms
		<< "           \r";
	}

	return 0;
}

