/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "beaconClient.pmh"

#define FE_BEACON_CLIENT_STARTUP_VERBOSE FALSE
#define MILLISECOND_MONITOR_DELAY 20
#define DEFAULT_STARTING_PORT 5000

using namespace fe;
using namespace ext;

namespace beacon
{

bool BeaconClientStartup::startup(const char *fileName)
{
#if FE_BEACON_CLIENT_STARTUP_VERBOSE
	feLog("BeaconClientStartup::Init file name = %s\n", fileName);
#endif

	sp<SingleMaster> spSingleMaster=SingleMaster::create();
	sp<Master> spMaster=spSingleMaster->master();
	sp<Registry> spRegistry=spMaster->registry();

	spRegistry->manage("feAutoLoadDL");

	sp<CatalogReaderI> spCatalogReader =
			spRegistry->create("CatalogReaderI.*.*.yaml");
    if(!spCatalogReader.isValid())
	{
		feLog("BeaconClientStartup::startup: spCatalogReader invalid\n");
        return false;
	}

	sp<Catalog> spCatalogIn = spMaster->createCatalog("input");
	if(!spCatalogIn.isValid())
	{
		feLog("BeaconClientStartup::startup: spCatalogIn invalid\n");
        return false;
	}

    // Load catalog
	if( spCatalogReader->load(fe::String(fileName),spCatalogIn) == 0 )
    {
        feLog("BeaconClientStartup::startup: Unable to load %s\n", fileName);
        return false;
    }

	// Setup default values
	strcpy(m_config.ipAddress, "127.0.0.1");
	m_config.requestPort    	= DEFAULT_STARTING_PORT;
	m_config.gdRequestPort  	= DEFAULT_STARTING_PORT+1;

	// Read values from configuration file
	strcpy(m_config.ipAddress,
			spCatalogIn->catalogOrDefault<fe::String>(
								"IPaddress",
								m_config.ipAddress).c_str());
    m_config.requestPort = spCatalogIn->catalogOrDefault<int>(
						"RequestPort",
						m_config.requestPort);
    m_config.gdRequestPort = spCatalogIn->catalogOrDefault<int>(
						"GDRequestPort",
						m_config.requestPort);

	if(!registerWithBeacon(m_config, true))
	{
		return false;
	}

	// Send any global dictionary values in configuration file to the Beacon
	Array<String> properties;
	spCatalogIn->catalogProperties("GlobalDictionary",properties);

	std::vector<std::pair<String,String>> setList;

	for(int i = 0; i < (int)properties.size(); i++)
	{
		String value = spCatalogIn->catalog<String>(
				"GlobalDictionary", properties[i]);
		feLog("Property %d - %s, %s\n",
				i, properties[i].c_str(), value.c_str());
		setList.push_back(std::make_pair(properties[i],value));
	}

	return dictionarySet(setList);
}

} // namespace beacon
