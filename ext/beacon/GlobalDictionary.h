/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __Global_Dictonary_h__
#define __Global_Dictonary_h__

namespace beacon
{

/***************************************************************************//**
	@brief Global Dictionary of name/value strings

	@ingroup beacon
*//****************************************************************************/
class FE_DL_EXPORT GlobalDictionary
{
	public:
		GlobalDictionary();
		virtual	~GlobalDictionary();

		/// @brief Initialize Global Dictionary with the given entries
		virtual void setInitialEntries(const std::vector<std::pair<fe::String,fe::String>> &entries);

		/// @brief Starts the Global Dictionary with the port for receiving requests
		virtual bool start(uint16_t receivePort);

		/// @brief Stop the Global Dictionary server
		virtual void stop(void);

		/// @brief Returns to true if the Global Dictionary server is running
		virtual bool running(void);

		/// @brief Delete entries by the given ID
		virtual void deleteEntriesByID(uint8_t id);

		/// @brief Clears the entire Global Dictionary
		virtual void clear();

		/// @brief Returns to true if the Global Dictionary server is running
		virtual uint64_t getUpdateCounter() { return m_updateCounter; };

	protected:
		bool m_initialized;
		bool m_done;
		bool m_running;
		std::vector<uint8_t> m_responseMsg;
		char m_fromIPaddress[16]; // Address from where the request came from

		std::thread* m_requestThread;
		fe::sp<fe::SingleMaster> m_spSingleMaster;
		fe::sp<fe::ext::MessageReliableUDPI> m_reliableMessageSystem;
		std::map<fe::String, std::pair<uint8_t,fe::String>> m_globalDictionary;
		std::atomic<uint64_t> m_updateCounter;

		virtual void requestThread();

		/// Global dictionary functions
		virtual void dictionaryGet(char *nameList, char *nameListEnd, uint16_t responsePort);
		virtual void dictionarySet(uint8_t id, char *nameValueList, char *nameValueListEnd, uint16_t responsePort);
		virtual void dictionaryUnset(char *nameList, char *nameListEnd, uint16_t responsePort);
		virtual void dictionaryGetRegex(char *nameList, uint16_t responsePort);
		virtual void dictionaryClear(uint16_t responsePort);
		virtual void displayDictionary();

		void setupSimpleResponse(uint8_t type, uint16_t result);
		void sendResponseMsg(uint16_t responsePort);
};

}  // namespace beacon

#endif // __Global_Dictonary_h__
