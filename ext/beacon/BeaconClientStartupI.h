/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __BeaconClientStartupI_h__
#define __BeaconClientStartupI_h__

namespace beacon
{

/**************************************************************************//**
	@brief Client Node interface to Beacon

	@ingroup beacon
*//***************************************************************************/
class FE_DL_EXPORT BeaconClientStartupI :
	virtual public BeaconClientI,
	public fe::CastableAs<BeaconClientStartupI>
{
public:
	/// @brief	Startup Beacon Client from a configuration file
	virtual bool startup(const char *fileName) = 0;
};

} // namespace beacon

#endif // __BeaconClientStartupI_h__
