/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "beaconServer.pmh"

#define FE_BEACON_SERVER_VERBOSE			FALSE
#define FE_BEACON_EXIT_ON_LAST_SIM_LEAVES	FALSE

using namespace fe;
using namespace ext;

namespace beacon
{

BeaconServer::BeaconServer()
{
	m_initialized = false;
	m_running = false;
	m_done	= true;

	m_id = 1;  // ID 0 reserved

	m_registeringThread = nullptr;

	// Fill in header for time msg to decrease response time
	CurrentTimeMsg *timeMsg = (CurrentTimeMsg *)m_timeMsg.data;
	m_timeMsg.dataLen = sizeof(CurrentTimeMsg);
	timeMsg->header.msgType = BeaconMessageType::currentTime;
	timeMsg->header.msgLen = sizeof(CurrentTimeMsg);

	// Fill in header of keep alive response message
	KeepAliveResponseMsg *respMsg = (KeepAliveResponseMsg *)m_respMsg.data;
	m_respMsg.dataLen = sizeof(KeepAliveResponseMsg);
	respMsg->header.msgType = BeaconMessageType::keepAliveResponse;
	respMsg->header.msgLen = sizeof(KeepAliveResponseMsg);

#if FE_BEACON_SERVER_VERBOSE
	feLog("[BeaconServer] constructor()\n");
#endif
}

BeaconServer::~BeaconServer()
{
	stop();
}

bool BeaconServer::start(uint16_t beaconPort, uint16_t gdPort)
{
	stop();

	m_registeredNodes.clear();

	if(!m_spSingleMaster.isValid())
	{
		m_spSingleMaster=SingleMaster::create();
	}
	sp<Master> spMaster=m_spSingleMaster->master();
	sp<Registry> spRegistry = spMaster->registry();

	Result result = spRegistry->manage("fexMessageDL");
    if(failure(result))
    {
		feLog("BeaconServer::start Error loading fexMessageDL\n");
		return false;
    }

	m_sendMsgSystem = spRegistry->create("MessageI.MessageUDP");

	if(!m_sendMsgSystem.isValid() || !m_sendMsgSystem->postInit())
	{
		feLog("BeaconServer::start Initialization of UDP send system failed\n");
		return false;
	}

	m_recMsgSystem = spRegistry->create("MessageI.MessageUDP");

	if(!m_recMsgSystem.isValid() || !m_recMsgSystem->postInit())
	{
		feLog("BeaconServer::start Initialization of receive UDP system "
			"failed\n");
		return false;
	}

	feLog("BeaconServer::start binding to UDP port %d\n", beaconPort);
	if(!m_recMsgSystem->bind(beaconPort))
	{
		feLog("BeaconServer::start Failed bind to UDP port %d\n", beaconPort);
		return false;
	}

	m_registeringThread =
			new std::thread(&BeaconServer::registrationThread,this);
	m_running = true;

	if(!m_globalDictionary.start(gdPort))
	{
		return false;
	}

    m_initialized = true;
	return true;
}

void BeaconServer::stop()
{
#if FE_BEACON_SERVER_VERBOSE
	feLog( "BeaconServer::stop\n");
#endif
	if(m_initialized)
	{
		if(m_recMsgSystem.isValid())
		{
			if(m_registeringThread )
			{
				if(m_done.load(std::memory_order_relaxed) == false)
				{
#if FE_BEACON_SERVER_VERBOSE
					feLog( "BeaconServer::stop"
							" telling registration thread to shutdown\n");
#endif
					m_running = false;
					while(m_done.load(std::memory_order_relaxed) == false)
					{
						std::this_thread::sleep_for(
								std::chrono::milliseconds(20));
					}
				}
			}

#if FE_BEACON_SERVER_VERBOSE
			feLog( "BeaconServer::stop shutting down rec msg system\n");
#endif
			m_recMsgSystem->shutdown();
			m_recMsgSystem = nullptr;
		}

		if(m_sendMsgSystem.isValid())
		{
#if FE_BEACON_SERVER_VERBOSE
			feLog( "BeaconServer::stop shutting down send msg system\n");
#endif
			m_sendMsgSystem->shutdown();
			m_sendMsgSystem = nullptr;
		}

		m_globalDictionary.stop();

		m_initialized = false;
	}
}

bool BeaconServer::running()
{
	return m_running.load(std::memory_order_relaxed);
}

void BeaconServer::registrationThread()
{
	feLog("BeaconServer::registrationThread started\n");

	m_done = false;
	m_running = true;
	Messagegram msg;

#if FE_BEACON_SERVER_VERBOSE
	feLog("BeaconServer:registrationThread waiting for msg\n");
#endif

	while(running())
	{
		if(m_recMsgSystem->recvFrom(&msg, 2, 0) &&
				validCRC(msg.data, msg.dataLen))
		{
			MPPmessageHeader *msgHeader = (MPPmessageHeader *)msg.data;

			switch((NodeMessageType)msgHeader->msgType)
			{
				case NodeMessageType::registrationRequest:
					{
#if FE_BEACON_SERVER_VERBOSE
						feLog("BeaconServer::registrationThread"
								" received registration request\n");
#endif
						uint32_t ipAddress;
						m_recMsgSystem->getRecvAddress(ipAddress);

						RegisterNodeMsg *regMsg = (RegisterNodeMsg *)msg.data;

						add(ipAddress, regMsg->responsePort);
#if FE_BEACON_SERVER_VERBOSE
						displayList();
#endif
						sendUpdatedList();
					}
					break;

				case NodeMessageType::shutdown:
					{
#if FE_BEACON_SERVER_VERBOSE
						feLog("BeaconServer::registrationThread"
								" received shutdown message\n");
#endif
						uint32_t ipAddress;
						m_recMsgSystem->getRecvAddress(ipAddress);

						remove(ipAddress);
#if FE_BEACON_SERVER_VERBOSE
						displayList();
#endif
						sendUpdatedList();

						ShutdownSimMsg *shutdownMsg =
								(ShutdownSimMsg *)msg.data;
						m_globalDictionary.deleteEntriesByID(shutdownMsg->id);

#if FE_BEACON_EXIT_ON_LAST_SIM_LEAVES
						if(m_registeredNodes.size() == 0)
						{
#if FE_BEACON_SERVER_VERBOSE
							feLog("BeaconServer::registrationThread"
									" shutting down on last sim leaving\n");
#endif
							m_running = false;
						}
#endif
					}
					break;

				case NodeMessageType::keepAlive:
					{
						uint32_t ipAddress;
						m_recMsgSystem->getRecvAddress(ipAddress);

						KeepAliveMsg *aliveMsg = (KeepAliveMsg *)msg.data;
#if FE_BEACON_SERVER_VERBOSE
						feLog("BeaconServer::registrationThread"
								" received keep alive message from ID:%u\n",
								aliveMsg->id);
#endif
						keepAlive(aliveMsg->id);
						sendKeepAliveResponse(aliveMsg->responsePort);
					}
					break;

				case NodeMessageType::timeSyncRequest:
					{
#if FE_BEACON_SERVER_VERBOSE
						feLog("BeaconServer::registrationThread"
								" received time sync request\n");
#endif
						TimeSyncRequestMsg *timeSyncMsg =
								(TimeSyncRequestMsg *)msg.data;
						sendTimeResponse(timeSyncMsg->clientTime,
								timeSyncMsg->estBeaconTime,
								timeSyncMsg->responsePort);
					}
					break;

				default:
					feLog("BeaconServer::registrationThread"
							" invalid message type %d\n",
							msgHeader->msgType);
					break;
			}
		}
		else
		{
#if FE_BEACON_SERVER_VERBOSE
			static int i = 0;
			const char tick[4] = {'-', '\\', '|', '/' };
			feLog("%c\r", tick[i]);
			i = (i+1) % 4;
#endif
			checkForDeadNodes();
		}
	}

	m_done = true;
}

void BeaconServer::sendTimeResponse(const int64_t time,
							  const int64_t estBeaconTime,
							  const uint16_t port)
{
	// Fill in current time and re-calculate the CRC
	CurrentTimeMsg *msg = (CurrentTimeMsg *)m_timeMsg.data;

	msg->clientTime = time;
	msg->estBeaconTime = estBeaconTime;
	msg->BeaconTime = getCurrentTime();

	msg->CRC = calculateCRC(m_timeMsg.data,
			sizeof(CurrentTimeMsg)-sizeof(uint32_t), 0);

	uint32_t ipAddress;
	m_recMsgSystem->getRecvAddress(ipAddress);

	char ipAddressStr[16];
	m_recMsgSystem->convertAddressToStr(ipAddress, ipAddressStr);

#if FE_BEACON_SERVER_VERBOSE
	feLog("BeaconServer::sendTimeResponse to \"%s\"\n",ipAddressStr);
#endif

	// Respond to the same port on to the IP address that
	// requested the time sync
	m_sendMsgSystem->sendTo(m_timeMsg, ipAddressStr, port);
}

void BeaconServer::sendKeepAliveResponse(const uint16_t port)
{
	KeepAliveResponseMsg *msg = (KeepAliveResponseMsg *)m_respMsg.data;
	msg->globalDictionaryUpdateCounter = m_globalDictionary.getUpdateCounter();
	msg->CRC = calculateCRC(m_respMsg.data,
			sizeof(KeepAliveResponseMsg)-sizeof(uint32_t), 0);

	uint32_t ipAddress;
	m_recMsgSystem->getRecvAddress(ipAddress);

	char ipAddressStr[16];
	m_recMsgSystem->convertAddressToStr(ipAddress, ipAddressStr);

	// Respond to the same port on to the IP address that
	// keep alive msg requested
	m_sendMsgSystem->sendTo(m_respMsg, ipAddressStr, port);
}

int64_t BeaconServer::getCurrentTime()
{
    int64_t time = std::chrono::duration_cast<std::chrono::milliseconds>
			(std::chrono::system_clock::now().time_since_epoch()).count();
    return time;
}

void BeaconServer::sendUpdatedList()
{
	NodeListMsg *msg = (NodeListMsg *)m_listMsg.data;

	// Clear message
	memset((void *)m_listMsg.data, 0, sizeof(NodeListMsg));
	m_listMsg.dataLen = sizeof(NodeListMsg);

	// Fill in header
	msg->header.msgType = BeaconMessageType::nodeList;
	msg->header.msgLen = sizeof(NodeListMsg);

	// Fill in current GD update counter
	msg->globalDictionaryUpdateCounter = m_globalDictionary.getUpdateCounter();

	// Fill in list of sims
	msg->numNodes = m_registeredNodes.size();

	int i = 0;
	for(auto it = m_registeredNodes.begin();
			it != m_registeredNodes.end(); ++it)
	{
		msg->nodeList[i].id = it->id;
		msg->nodeList[i].ipAddress = it->ipAddress;
		i++;
	}

	// Send the updated list to everyone on the list
	sendListMsgToAll(m_listMsg);
}

void BeaconServer::sendListMsgToAll(const fe::ext::Messagegram &listMsg)
{
	char ipAddressStr[16];
	NodeListMsg *msg = (NodeListMsg *)listMsg.data;

	for(auto it = m_registeredNodes.begin(); it != m_registeredNodes.end(); ++it)
	{
		m_sendMsgSystem->convertAddressToStr(it->ipAddress, ipAddressStr);

#if FE_BEACON_SERVER_VERBOSE
		feLog("Send msg to IP:%s Port:%d\n", ipAddressStr, it->replyPort);
#endif

		msg->yourID = it->id;
		msg->CRC = calculateCRC(m_listMsg.data,
				sizeof(NodeListMsg)-sizeof(msg->CRC), 0);

		m_sendMsgSystem->sendTo(listMsg, ipAddressStr, it->replyPort);
	}
}

void BeaconServer::add(const uint32_t ipAddress, const uint16_t replyPort)
{
	// Should not hit this limit
	if(m_registeredNodes.size() >= MAX_BEACON_NODES)
	{
		feLog("BeaconServer::add"
				" WARNING current max number of sims reached"
				" (Increase limit if needed).\n");
		return;
	}

	// Check to see if the sim is already in the list
	for(NodeEntry &peer : m_registeredNodes)
	{
		if(peer.ipAddress == ipAddress && peer.replyPort == replyPort)
		{
			return;
		}
	}

	const int64_t time  = getCurrentTime();

	m_nodeListMutex.lock();
	m_registeredNodes.emplace_front(NodeEntry(m_id, ipAddress,
			replyPort, time));
	m_nodeListMutex.unlock();
	m_id++;
}

void BeaconServer::remove(const uint32_t ipAddress)
{
	// remove match from list
	for(auto it = m_registeredNodes.begin();
			it != m_registeredNodes.end(); ++it)
	{
		if(it->ipAddress == ipAddress)
		{
			m_nodeListMutex.lock();
			m_registeredNodes.erase(it);
			m_nodeListMutex.unlock();
			return;
		}
	}
}

void BeaconServer::remove(uint8_t id)
{
	// remove match from list
	for(auto it = m_registeredNodes.begin();
			it != m_registeredNodes.end(); ++it)
	{
		if(it->id == id)
		{
			m_nodeListMutex.lock();
			m_registeredNodes.erase(it);
			m_nodeListMutex.unlock();
			return;
		}
	}
}

void BeaconServer::keepAlive(const uint32_t id)
{
	// Find node in the list
	for(auto it = m_registeredNodes.begin();
			it != m_registeredNodes.end(); ++it)
	{
		if(it->id == id)
		{
			// Update the time
			const int64_t time  = getCurrentTime();

			m_nodeListMutex.lock();
			it->lastHeardFromTime = time;
			m_nodeListMutex.unlock();
			return;
		}
	}

	feLog("BeaconServer::keepAlive"
			" WARNING received a keep alive message from an ID"
			" that wasn't registered.\n");
	return;
}

void BeaconServer::checkForDeadNodes()
{
	const int64_t time  = getCurrentTime();

	int deadCount = 0;

	// Remove nodes if we haven't heard from them
	for(auto it = m_registeredNodes.begin(); it != m_registeredNodes.end();)
	{
		if((time - it->lastHeardFromTime) >= DEAD_TIME)
		{
#if FE_BEACON_SERVER_VERBOSE
			feLog("BeaconServer::checkForDeadNodes"
					" node timed out with ID:%u\n", it->id);
#endif
			// Delete any entries created by the node
			m_globalDictionary.deleteEntriesByID(it->id);

			m_nodeListMutex.lock();
			it = m_registeredNodes.erase(it);
			m_nodeListMutex.unlock();

			deadCount++;
		}
		else
		{
			++it;
		}
	}

	if(deadCount > 0)
	{
#if FE_BEACON_SERVER_VERBOSE
		displayList();
#endif
		sendUpdatedList();
#if FE_BEACON_EXIT_ON_LAST_SIM_LEAVES
		if(m_registeredNodes.size() == 0)
		{
#if FE_BEACON_SERVER_VERBOSE
			feLog("BeaconServer::checkForDeadNodes"
					" Shutting down on last sim leaving\n");
#endif
			m_running = false;
		}
#endif
	}
}

void BeaconServer::getList(std::list<NodeEntry> &registeredNodes)
{
	registeredNodes.clear();

	m_nodeListMutex.lock();
	for(NodeEntry &peer : m_registeredNodes)
	{
		registeredNodes.emplace_back(peer);
	}
	m_nodeListMutex.unlock();
}

void BeaconServer::displayList()
{
	char ipAddressStr[16];

    feLog("Registered nodes:\n");
	for(NodeEntry &node : m_registeredNodes)
	{
		m_recMsgSystem->convertAddressToStr(node.ipAddress, ipAddressStr);

		feLog("ID:%u IP:%s reply port:%u\n",
				node.id, ipAddressStr, node.replyPort);
	}
}

void BeaconServer::clearList()
{
	m_registeredNodes.clear();
}

} // namespace beacon
