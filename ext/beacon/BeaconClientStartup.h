/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __BeaconClientStartup_h__
#define __BeaconClientStartup_h__

namespace beacon
{

/**************************************************************************//**
	@brief Client Node interface to Beacon

	@ingroup beacon
*//***************************************************************************/
class FE_DL_EXPORT BeaconClientStartup : virtual public BeaconClient
{
public:
	virtual bool startup(const char *fileName) override;
};

} // namespace beacon

#endif // __BeaconClientStartup_h__
