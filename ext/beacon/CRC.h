/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __CRC_h__
#define __CRC_h__

#include <stdint.h>
#include <stddef.h>

namespace beacon
{
	uint32_t calculateCRC(const void* dataPtr, const size_t dataSize,
					const uint32_t seed);
	bool validCRC(const unsigned char* dataPtr, const size_t dataSize);
}

#endif // __CRC_h__
