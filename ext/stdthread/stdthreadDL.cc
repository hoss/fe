/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <stdthread/stdthread.pmh>

#include "platform/dlCore.cc"

#define FE_SMX_DEBUG	FALSE
#define FE_STH_DEBUG	FALSE

namespace fe
{
namespace ext
{

class StdThreadGroup
{
	public:

		std::thread*	create_thread(Thread::Functor& a_rFunctor);

		void			join_all(void);

	private:

		Array<std::thread*>		m_threadArray;
};

std::thread* StdThreadGroup::create_thread(Thread::Functor& a_rFunctor)
{
	std::thread* pThread=new std::thread(a_rFunctor);

	if(pThread)
	{
		m_threadArray.push_back(pThread);
	}

	return pThread;
}

void StdThreadGroup::join_all(void)
{
	const I32 threadCount=m_threadArray.size();
	for(I32 threadIndex=0;threadIndex<threadCount;threadIndex++)
	{
		m_threadArray[threadIndex]->join();
	}
}

void* stdmutex_init(bool a_recursive)
{
	void* pMutex=NULL;
	if(a_recursive)
	{
		pMutex=new std::recursive_mutex();
	}
	else
	{
		pMutex=new std::shared_mutex();
	}

#if FE_SMX_DEBUG
	feLogError("stdmutex_init recursive %d -> %p\n",
			a_recursive,pMutex);
#endif

	return pMutex;
}

bool stdmutex_lock(bool a_recursive,void* a_pMutex,
	bool a_un,bool a_try,bool a_readOnly)
{
#if FE_SMX_DEBUG
	feLogError("stdmutex_lock recursive %d mutex %p"
			" un %d try %d read %d\n",
			a_recursive,a_pMutex,a_un,a_try,a_readOnly);
#endif

	FEASSERT(a_pMutex);

	if(a_recursive)
	{
		std::recursive_mutex& rMutex=*(std::recursive_mutex*)(a_pMutex);

		if(a_un)
		{
			rMutex.unlock();
			return true;
		}
		else if(a_try)
		{
			return rMutex.try_lock();
		}

		rMutex.lock();
		return true;
	}

	std::shared_mutex& rMutex=*(std::shared_mutex*)(a_pMutex);

	if(a_un)
	{
		if(a_readOnly)
		{
			rMutex.unlock_shared();
			return true;
		}
		rMutex.unlock();
		return true;
	}
	else if(a_readOnly)
	{
		if(a_try)
		{
			return rMutex.try_lock_shared();
		}
		rMutex.lock_shared();
		return true;
	}
	else if(a_try)
	{
		return rMutex.try_lock();
	}
	rMutex.lock();
	return true;
}

void stdmutex_finish(bool a_recursive,void* a_pMutex)
{
#if FE_SMX_DEBUG
	feLogError("stdmutex_finish recursive %d mutex %p\n",
			a_recursive,a_pMutex);
#endif

	FEASSERT(a_pMutex);

	if(a_recursive)
	{
		std::recursive_mutex* pMutex=(std::recursive_mutex*)(a_pMutex);
		delete pMutex;
		return;
	}

	std::shared_mutex* pMutex=(std::shared_mutex*)(a_pMutex);
	delete pMutex;
}

void* stdguard_init(bool a_recursive,void* a_pMutex,bool a_readOnly)
{
	FEASSERT(a_pMutex);

	void* pGuard=NULL;
	if(a_recursive)
	{
		std::recursive_mutex& rMutex=*(std::recursive_mutex*)(a_pMutex);

//		pGuard=(void*)new std::scoped_lock(rMutex);
		pGuard=new std::unique_lock<std::recursive_mutex>(rMutex);
	}
	else if(a_readOnly)
	{
		std::shared_mutex& rMutex=*(std::shared_mutex*)(a_pMutex);
		pGuard=new std::shared_lock<std::shared_mutex>(rMutex);
	}
	else
	{
		std::shared_mutex& rMutex=*(std::shared_mutex*)(a_pMutex);
		pGuard=new std::unique_lock<std::shared_mutex>(rMutex);
	}

#if FE_SMX_DEBUG
	feLogError("stdguard_init recursive %d mutex %p read %d -> %p\n",
			a_recursive,a_pMutex,a_readOnly,pGuard);
#endif

	return pGuard;
}

bool stdguard_lock(bool a_recursive,void* a_pGuard,
	bool a_un,bool a_try,bool a_readOnly)
{
#if FE_SMX_DEBUG
	feLogError("stdguard_lock recursive %d guard %p"
			" un %d try %d read %d\n",
			a_recursive,a_pGuard,a_un,a_try,a_readOnly);
#endif

	FEASSERT(a_pGuard);

	if(a_recursive)
	{
		std::unique_lock<std::recursive_mutex>& rGuard=
				*(std::unique_lock<std::recursive_mutex>*)(a_pGuard);

		if(a_un)
		{
			rGuard.unlock();
			return true;
		}
		else if(a_try)
		{
			return rGuard.try_lock();
		}
		rGuard.lock();
		return true;
	}

	if(a_readOnly)
	{
		std::shared_lock<std::shared_mutex>& rGuard=
				*(std::shared_lock<std::shared_mutex>*)(a_pGuard);

		if(a_un)
		{
			rGuard.unlock();
			return true;
		}
		else if(a_try)
		{
			return rGuard.try_lock();
		}

		rGuard.lock();
		return true;
	}

	std::unique_lock<std::shared_mutex>& rGuard=
			*(std::unique_lock<std::shared_mutex>*)(a_pGuard);

	if(a_un)
	{
		rGuard.unlock();
		return true;
	}
	else if(a_try)
	{
		return rGuard.try_lock();
	}
	rGuard.lock();
	return true;
}

void stdguard_finish(bool a_recursive,void* a_pGuard,bool a_readOnly)
{
#if FE_SMX_DEBUG
	feLogError("stdguard_finish recursive %d guard %p read %d\n",
			a_recursive,a_pGuard,a_readOnly);
#endif

	FEASSERT(a_pGuard);

	if(a_recursive)
	{
//		std::scoped_lock* pGuard=(std::scoped_lock*)(a_pGuard);
		std::unique_lock<std::recursive_mutex>* pGuard=
				(std::unique_lock<std::recursive_mutex>*)(a_pGuard);
		delete pGuard;
		return;
	}

	if(a_readOnly)
	{
		std::shared_lock<std::shared_mutex>* pGuard=
				(std::shared_lock<std::shared_mutex>*)(a_pGuard);
		delete pGuard;
		return;
	}

	std::unique_lock<std::shared_mutex>* pGuard=
			(std::unique_lock<std::shared_mutex>*)(a_pGuard);
	delete pGuard;
}

void* stdcondition_init(void)
{
	void* pCondition=new std::condition_variable_any();

#if FE_SMX_DEBUG
	feLogError("stdcondition_init -> %p\n",pCondition);
#endif

	return pCondition;
}

bool stdcondition_wait(void* a_pCondition,bool a_recursive,
	void* a_pGuard,bool a_readOnly)
{
#if FE_SMX_DEBUG
	feLogError("stdcondition_wait %p guard %p\n",a_pCondition,a_pGuard);
#endif

	FEASSERT(a_pGuard);

	std::condition_variable_any& rCondition=
			*(std::condition_variable_any*)(a_pCondition);

	if(a_recursive)
	{
		std::unique_lock<std::recursive_mutex>& rGuard=
				*(std::unique_lock<std::recursive_mutex>*)(a_pGuard);

		rCondition.wait(rGuard);
		return true;
	}

	if(a_readOnly)
	{
		std::shared_lock<std::shared_mutex>& rGuard=
				*(std::shared_lock<std::shared_mutex>*)(a_pGuard);

		rCondition.wait(rGuard);
		return true;
	}

	std::unique_lock<std::shared_mutex>& rGuard=
			*(std::unique_lock<std::shared_mutex>*)(a_pGuard);

	rCondition.wait(rGuard);
	return true;
}

bool stdcondition_notify(void* a_pCondition,bool a_all)
{
#if FE_SMX_DEBUG
	feLogError("stdcondition_notify %p all %d\n",a_pCondition,a_all);
#endif

	FEASSERT(a_pCondition);

	std::condition_variable_any& rCondition=
			*(std::condition_variable_any*)(a_pCondition);

	if(a_all)
	{
		rCondition.notify_all();
	}
	else
	{
		rCondition.notify_one();
	}

	return true;
}

void stdcondition_finish(void* a_pCondition)
{
#if FE_SMX_DEBUG
	feLogError("stdcondition_finish %p\n",a_pCondition);
#endif

	FEASSERT(a_pCondition);

	std::condition_variable_any* pCondition=
			(std::condition_variable_any*)(a_pCondition);
	delete pCondition;
}

extern "C"
{

FE_DL_EXPORT bool mutex_init(void)
{
#if FE_SMX_DEBUG
	feLogDirect("stdThreadDL mutex_init()\n");
#endif

//	feLogDirect("  using std::mutex\n");

	Mutex::replaceInitFunction(stdmutex_init);
	Mutex::replaceLockFunction(stdmutex_lock);
	Mutex::replaceFinishFunction(stdmutex_finish);

	Mutex::replaceGuardInitFunction(stdguard_init);
	Mutex::replaceGuardLockFunction(stdguard_lock);
	Mutex::replaceGuardFinishFunction(stdguard_finish);

	Mutex::replaceConditionInitFunction(stdcondition_init);
	Mutex::replaceConditionWaitFunction(stdcondition_wait);
	Mutex::replaceConditionNotifyFunction(stdcondition_notify);
	Mutex::replaceConditionFinishFunction(stdcondition_finish);

	return true;
}

}

///////////////////////////////////////////////////////////////////////////////

void* stdthread_default_init(void)
{
	void* pThread=new std::thread();

#if FE_STH_DEBUG
	feLogError("stdthread_default_init -> %p\n",pThread);
#endif

	return pThread;
}

void* stdthread_init(void* a_pFunctor)
{
	FEASSERT(a_pFunctor);

	Thread::Functor& rFunctor=*(Thread::Functor*)(a_pFunctor);
	void* pThread=new std::thread(rFunctor);

#if FE_STH_DEBUG
	feLogError("stdthread_init -> %p\n",pThread);
#endif

	return pThread;
}

bool stdthread_config(void* a_pThread,String a_property,String a_value)
{
	FEASSERT(a_pThread);

#if FE_STH_DEBUG
	feLogError("stdthread_config thread %p property \"%s\" value \"%s\"\n",
			a_pThread,a_property.c_str(),a_value.c_str());
#endif

#if FE_OS==FE_WIN32 || FE_OS==FE_WIN64
	std::thread& rThread=*(std::thread*)(a_pThread);

	if(a_property=="priority")
	{

		/*
			THREAD_PRIORITY_IDLE			-15
			THREAD_PRIORITY_LOWEST			-2
			THREAD_PRIORITY_BELOW_NORMAL	-1
			THREAD_PRIORITY_NORMAL			0
			THREAD_PRIORITY_ABOVE_NORMAL	1
			THREAD_PRIORITY_HIGHEST			2
			THREAD_PRIORITY_TIME_CRITICAL	15
		*/
		int threadPriority=THREAD_PRIORITY_NORMAL;
		if(a_value=="idle")
		{
			threadPriority=THREAD_PRIORITY_IDLE;
		}
		else if(a_value=="lowest")
		{
			threadPriority=THREAD_PRIORITY_LOWEST;
		}
		else if(a_value=="below" || a_value=="low")
		{
			threadPriority=THREAD_PRIORITY_BELOW_NORMAL;
		}
		else if(a_value=="above" || a_value=="high")
		{
			threadPriority=THREAD_PRIORITY_ABOVE_NORMAL;
		}
		else if(a_value=="highest")
		{
			threadPriority=THREAD_PRIORITY_HIGHEST;
		}
		else if(a_value=="critical")
		{
			threadPriority=THREAD_PRIORITY_TIME_CRITICAL;
		}

		return (bool)SetThreadPriority(rThread.native_handle(),
				threadPriority);
	}
#endif

	return false;
}

void stdthread_interrupt(void* a_pThread)
{
#if FE_STH_DEBUG
	feLogError("stdthread_interrupt thread %p\n",a_pThread);
#endif

	FEASSERT(a_pThread);

//* TODO
//	std::thread& rThread=*(std::thread*)(a_pThread);
//	rThread.interrupt();
}

void stdthread_join(void* a_pThread)
{
#if FE_STH_DEBUG
	feLogError("stdthread_join thread %p\n",a_pThread);
#endif

	FEASSERT(a_pThread);

	std::thread& rThread=*(std::thread*)(a_pThread);
	rThread.join();
}

bool stdthread_joinable(void* a_pThread)
{
#if FE_STH_DEBUG
	feLogError("stdthread_joinable thread %p\n",a_pThread);
#endif

	FEASSERT(a_pThread);

	std::thread& rThread=*(std::thread*)(a_pThread);
	return rThread.joinable();
}

void stdthread_finish(void* a_pThread)
{
#if FE_STH_DEBUG
	feLogError("stdthread_finish thread %p\n",a_pThread);
#endif

	FEASSERT(a_pThread);

	std::thread* pThread=(std::thread*)(a_pThread);
	delete pThread;
}

void stdthread_interruption(void)
{
#if FE_STH_DEBUG
	feLogError("stdthread_interruption\n");
#endif

	std::this_thread::yield();
}

int stdthread_concurrency(void)
{
#if FE_STH_DEBUG
	feLogError("stdthread_concurrency\n");
#endif

	return std::thread::hardware_concurrency();
}

void* stdgroup_init(void)
{
	StdThreadGroup* pThreadGroup=new StdThreadGroup();

#if FE_STH_DEBUG
	feLogError("stdgroup_init -> %p\n",pThreadGroup);
#endif

	return pThreadGroup;
}

void* stdgroup_create(void* a_pThreadGroup,void* a_pFunctor)
{
	FEASSERT(a_pThreadGroup);
	FEASSERT(a_pFunctor);

	StdThreadGroup& rThreadGroup=*(StdThreadGroup*)(a_pThreadGroup);
	Thread::Functor& rFunctor=*(Thread::Functor*)(a_pFunctor);
	std::thread* pThread=rThreadGroup.create_thread(rFunctor);

#if FE_STH_DEBUG
	feLogError("stdgroup_create group %p functor %p -> %p\n",
			a_pThreadGroup,a_pFunctor,pThread);
#endif

	return pThread;
}

void stdgroup_join_all(void* a_pThreadGroup)
{
#if FE_STH_DEBUG
	feLogError("stdgroup_join_all group %p\n",a_pThreadGroup);
#endif

	FEASSERT(a_pThreadGroup);

	StdThreadGroup& rThreadGroup=*(StdThreadGroup*)(a_pThreadGroup);
	rThreadGroup.join_all();
}

void stdgroup_finish(void* a_pThreadGroup)
{
#if FE_STH_DEBUG
	feLogError("stdgroup_finish %p\n",a_pThreadGroup);
#endif

	FEASSERT(a_pThreadGroup);

	StdThreadGroup* pThreadGroup=(StdThreadGroup*)(a_pThreadGroup);
	delete pThreadGroup;
}

extern "C"
{

FE_DL_EXPORT bool thread_init(void)
{
#if FE_STH_DEBUG
	feLogDirect("stdThreadDL thread_init()\n");
#endif

//	feLogDirect("  using std::thread\n");

	Thread::replaceDefaultInitFunction(stdthread_default_init);
	Thread::replaceInitFunction(stdthread_init);
	Thread::replaceConfigFunction(stdthread_config);
	Thread::replaceInterruptFunction(stdthread_interrupt);
	Thread::replaceJoinFunction(stdthread_join);
	Thread::replaceJoinableFunction(stdthread_joinable);
	Thread::replaceFinishFunction(stdthread_finish);
	Thread::replaceInterruptionFunction(stdthread_interruption);
	Thread::replaceConcurrencyFunction(stdthread_concurrency);

	Thread::replaceGroupInitFunction(stdgroup_init);
	Thread::replaceGroupCreateFunction(stdgroup_create);
	Thread::replaceGroupJoinAllFunction(stdgroup_join_all);
	Thread::replaceGroupFinishFunction(stdgroup_finish);

	return true;
}

}

#if FE_COMPILER==FE_GNU
void fe_stdthread_finish(void) __attribute__((destructor));
void fe_stdthread_finish(void)
{
	feLogError("[90mFE stdthread finish[0m\n");

	Thread::clearFunctionPointers();
	Mutex::clearFunctionPointers();
}
#endif

} /* namespace ext */
} /* namespace fe */
