/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <optest/optest.pmh>

namespace fe
{
namespace ext
{

void DrawTestOp::initialize(void)
{
	catalog<String>("icon")="FE_alpha";
}

void DrawTestOp::handle(Record& a_rSignal)
{
	sp<DrawI> spDrawI;
	if(!accessDraw(spDrawI,a_rSignal)) return;

	for(Real angle=0.0f;angle<2.0f*fe::pi;angle+=0.2f*fe::pi)
	{
		SpatialVector point(cosf(angle),sinf(angle));

		spDrawI->drawPoints(&point,NULL,1,false,NULL);

		SpatialVector line[2];
		line[0]=0.3*point;
		line[1]=0.7*point;

		spDrawI->drawLines(line,NULL,2,DrawI::e_discrete,false,NULL);
	}
}

} /* namespace ext */
} /* namespace fe */
