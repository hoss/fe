/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __optest_IdleSpinOp_h__
#define __optest_IdleSpinOp_h__
namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Operator to keep spinning the input even without provocation

	@ingroup optest
*//***************************************************************************/
class FE_DL_EXPORT IdleSpinOp:
	public OperatorSurfaceCommon,
	public Initialize<IdleSpinOp>
{
	public:

					IdleSpinOp(void):
						m_angle(0.0)										{}

virtual				~IdleSpinOp(void)										{}

		void		initialize(void);

					//* As HandlerI
virtual	void		handle(Record& a_rSignal);

	private:
		Real		m_angle;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __optest_IdleSpinOp_h__ */
