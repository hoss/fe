/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __optest_MimicOp_h__
#define __optest_MimicOp_h__
namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Operator that draws input into the brush

	@ingroup optest
*//***************************************************************************/
class FE_DL_EXPORT MimicOp:
	public OperatorSurfaceCommon,
	public Initialize<MimicOp>
{
	public:

					MimicOp(void)											{}
virtual				~MimicOp(void)											{}

		void		initialize(void);

					//* As HandlerI
virtual	void		handle(Record& a_rSignal);

	private:
		sp<SurfaceI>	m_spInput;

		String			m_currentStyle;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __optest_MimicOp_h__ */
