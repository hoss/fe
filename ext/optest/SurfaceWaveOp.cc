/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <optest/optest.pmh>

namespace fe
{
namespace ext
{

void SurfaceWaveOp::initialize(void)
{
	catalog<String>("icon")="FE_alpha";

	catalog<String>("Attribute")="P";

	catalog<Real>("Scalar")=0.9;
	catalog<Real>("Scalar","low")=0.1;
	catalog<Real>("Scalar","high")=10.0;
	catalog<Real>("Scalar","min")=0.01;
	catalog<Real>("Scalar","max")=100.0;

	catalog<SpatialVector>("Offset")=SpatialVector(1.2, 3.4, 5.6);

	catalog< sp<Component> >("Input Surface");
}

void SurfaceWaveOp::handle(Record& a_rSignal)
{
	const String& attribute=catalog<String>("Attribute");
	const Real scalar=catalog<Real>("Scalar");
	const SpatialVector& offset=catalog<SpatialVector>("Offset");

	const I32 frame=0;			// TODO
	const Real time=0.03*frame;

	sp<SurfaceAccessorI> spOutputPoint;
	if(!accessOutput(spOutputPoint,a_rSignal,e_point,attribute)) return;

	const U32 count=spOutputPoint->count();
	for(U32 index=0;index<count;index++)
	{
		SpatialVector point=spOutputPoint->spatialVector(index);

		point[1]+=scalar*sin(0.2*point[0] + 0.3*point[2] + time);
		point+=offset;

		spOutputPoint->set(index,point);
	}
}

} /* namespace ext */
} /* namespace fe */
