/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <optest/optest.pmh>

using namespace fe;
using namespace fe::ext;

void BrushTestOp::initialize(void)
{
	catalog<String>("icon")="FE_alpha";

	catalog<I32>("Refinement")=0;

	catalog<Real>("Normal Scale")=1.0;
	catalog<Real>("Normal Scale","high")=1.0;

	catalog<Real>("Outline Offset")=0.01;
	catalog<Real>("Outline Offset","high")=0.1;

	catalog< sp<Component> >("Brush");
	catalog<String>("Brush","implementation")="DrawI";

	catalog< sp<Component> >("Input Surface");
}

void BrushTestOp::handle(Record& a_rSignal)
{
	SpatialVector line[4];

	sp<DrawI> spDrawBrush;
	accessBrush(spDrawBrush,a_rSignal,e_quiet);

	if(spDrawBrush.isValid())
	{
		if(m_spInput.isValid())
		{
			const SpatialVector& rRayOrigin=rayOrigin(a_rSignal);
			const SpatialVector& rRayDirection=rayDirection(a_rSignal);

			const Real maxDistance=100.0;
			sp<SurfaceI::ImpactI> spImpact=m_spInput->rayImpact(
					rRayOrigin,rRayDirection,maxDistance);
			if(spImpact.isValid())
			{
				const Color red(0.6,0.0,0.0,1.0);
				const Color green(0.0,0.6,0.0,1.0);
				const Color blue(0.0,0.0,0.6,1.0);
				const Color cyan(0.0,0.6,0.6,1.0);
				const Color yellow(0.6,0.6,0.0,1.0);

				const Real normalScale=catalog<Real>("Normal Scale");
				const Real outlineOffset=catalog<Real>("Outline Offset");

				const SpatialVector intersection=spImpact->intersection();
				const SpatialVector normal=spImpact->normal();

				m_event.bind(windowEvent(a_rSignal));
//				feLog("BrushTestOp::handle %s\n",c_print(m_event));

				WindowEvent::MouseButtons buttons=m_event.mouseButtons();
				const BWORD pressed=(buttons&WindowEvent::e_mbLeft);

				line[0]=intersection;
				line[1]=intersection+normalScale*normal*(pressed? 3.0: 1.0);

				spDrawBrush->drawLines(line,NULL,2,
						DrawI::e_strip,false,&blue);

				const SpatialVector du=spImpact->du();
				const SpatialVector dv=spImpact->dv();

				line[1]=intersection+normalScale*du;
				spDrawBrush->drawLines(line,NULL,2,
						DrawI::e_strip,false,&red);

				line[1]=intersection+normalScale*dv;
				spDrawBrush->drawLines(line,NULL,2,
						DrawI::e_strip,false,&green);

#if FALSE
				const Vector2 uv=spImpact->uv();
				feLog("BrushTestOp::handle face %d pick %d uv %s du %s dv %s\n",
						spImpact->face(),spImpact->pickIndex(),
						c_print(uv),c_print(du),c_print(dv));

				const SpatialTransform xform=m_spInput->sample(uv);
				feLog("  at\n%s\n",c_print(xform));
#endif

				sp<SurfaceTriangles::Impact> spTriImpact=spImpact;
				if(spTriImpact.isValid())
				{
					line[0]=spTriImpact->vertex0()+
							outlineOffset*spTriImpact->normal0();
					line[1]=spTriImpact->vertex1()+
							outlineOffset*spTriImpact->normal1();
					line[2]=spTriImpact->vertex2()+
							outlineOffset*spTriImpact->normal2();
					line[3]=line[0];
					spDrawBrush->drawLines(line,NULL,4,DrawI::e_strip,
							false,&yellow);

					line[0]=spTriImpact->vertex0();
					line[1]=line[0]+normalScale*spTriImpact->normal0();
					spDrawBrush->drawLines(line,NULL,2,DrawI::e_strip,
							false,&cyan);

					line[0]=spTriImpact->vertex1();
					line[1]=line[0]+normalScale*spTriImpact->normal1();
					spDrawBrush->drawLines(line,NULL,2,DrawI::e_strip,
							false,&cyan);

					line[0]=spTriImpact->vertex2();
					line[1]=line[0]+normalScale*spTriImpact->normal2();
					spDrawBrush->drawLines(line,NULL,2,DrawI::e_strip,
							false,&cyan);
				}
			}
		}

		m_keepInput=TRUE;

		return;
	}

	if(!m_keepInput)
	{
		if(!access(m_spInput,"Input Surface")) return;

		m_spInput->setRefinement(catalog<I32>("Refinement"));
	}
	m_keepInput=FALSE;

	sp<DrawI> spDrawGuide;
	if(!accessGuide(spDrawGuide,a_rSignal)) return;

	sp<DrawableI> spDrawable=m_spInput;
	if(spDrawable.isValid())
	{
		spDrawable->draw(spDrawGuide,NULL);
	}
}
