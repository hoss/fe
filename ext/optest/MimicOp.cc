/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <optest/optest.pmh>

using namespace fe;
using namespace fe::ext;

void MimicOp::initialize(void)
{
	catalog<String>("icon")="FE_alpha";

	catalog<String>("style")="solid";
	catalog<String>("style","label")="Format";
	catalog<String>("style","choice:0")="pointCloud";
	catalog<String>("style","label:0")="Point Cloud";
	catalog<String>("style","choice:1")="wireframe";
	catalog<String>("style","label:1")="Wireframe";
	catalog<String>("style","choice:2")="outline";
	catalog<String>("style","label:2")="Outline";
	catalog<String>("style","choice:3")="solid";
	catalog<String>("style","label:3")="Solid";
	catalog<String>("style","choice:4")="edgedSolid";
	catalog<String>("style","label:4")="Edged Solid";
	catalog<String>("style","choice:5")="foreshadow";
	catalog<String>("style","label:5")="Foreshadow";
	catalog<String>("style","choice:6")="ghost";
	catalog<String>("style","label:6")="Ghost";

	catalog<I32>("refinement")=2;
	catalog<I32>("refinement","min")=0;
	catalog<I32>("refinement","max")=8;

	catalog< sp<Component> >("Brush");
	catalog<String>("Brush","implementation")="DrawI";

	catalog< sp<Component> >("Input Surface");
}

void MimicOp::handle(Record& a_rSignal)
{
	sp<DrawI> spDrawBrush;
	accessBrush(spDrawBrush,a_rSignal,e_quiet);

	if(spDrawBrush.isValid())
	{
		if(m_spInput.isValid())
		{
			sp<DrawableI> spDrawable=m_spInput;
			if(spDrawable.isValid())
			{
				const String style=catalog<String>("style");
				if(style=="pointCloud")
				{
					spDrawBrush->drawMode()->setDrawStyle(
							DrawMode::e_pointCloud);
				}
				else if(style=="wireframe")
				{
					spDrawBrush->drawMode()->setDrawStyle(
							DrawMode::e_wireframe);
				}
				else if(style=="outline")
				{
					spDrawBrush->drawMode()->setDrawStyle(
							DrawMode::e_outline);
				}
				else if(style=="solid")
				{
					spDrawBrush->drawMode()->setDrawStyle(
							DrawMode::e_solid);
				}
				else if(style=="edgedSolid")
				{
					spDrawBrush->drawMode()->setDrawStyle(
							DrawMode::e_edgedSolid);
				}
				else if(style=="foreshadow")
				{
					spDrawBrush->drawMode()->setDrawStyle(
							DrawMode::e_foreshadow);
				}
				else if(style=="ghost")
				{
					spDrawBrush->drawMode()->setDrawStyle(
							DrawMode::e_ghost);
				}

				spDrawBrush->drawMode()->setRefinement(
						catalog<I32>("refinement"));

				//* HACK DrawHydra will bypass without a buffer
				sp<DrawBufferI> spDrawBuffer=spDrawBrush->createBuffer();

				spDrawBrush->draw(spDrawable,NULL,spDrawBuffer);
			}
		}

		return;
	}

	const BWORD inputReplaced=
			catalogOrDefault<bool>("Input Surface","replaced",true);

	const String style=catalog<String>("style");
	if(m_currentStyle!=style)
	{
		m_spInput=NULL;
		m_currentStyle=style;
	}

	if(inputReplaced || m_spInput.isNull())
	{
		//* HACK need hull support (missing in houdini version)
//		if(!access(m_spInput,"Input Surface",e_warning)) return;

#if FALSE
		m_spInput=registry()->create("*.SurfaceTrianglesAccessible");

		sp<SurfaceTrianglesAccessible> spSurfaceTrianglesAccessible=
				m_spInput;

		if(spSurfaceTrianglesAccessible.isValid())
		{
			sp<SurfaceAccessibleI> spInputAccessible;
			if(!access(spInputAccessible,"Input Surface")) return;
			spSurfaceTrianglesAccessible->bind(spInputAccessible);
		}
#else
		m_spInput=registry()->create("*.SurfaceCurvesAccessible");

		sp<SurfaceCurvesAccessible> spSurfaceCurvesAccessible=
				m_spInput;

		if(spSurfaceCurvesAccessible.isValid())
		{
			sp<SurfaceAccessibleI> spInputAccessible;
			if(!access(spInputAccessible,"Input Surface")) return;
			spSurfaceCurvesAccessible->bind(spInputAccessible);
		}
#endif
	}
}
