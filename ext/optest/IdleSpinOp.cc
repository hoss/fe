/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <optest/optest.pmh>

using namespace fe;
using namespace fe::ext;

void IdleSpinOp::initialize(void)
{
	catalog<String>("icon")="FE_alpha";

	catalog< sp<Component> >("Brush");
	catalog<String>("Brush","implementation")="DrawI";
	catalog<String>("Brush","prompt")="just watch it spin...";
	catalog<String>("Brush","idle")="any";

	catalog< sp<Component> >("Input Surface");
}

void IdleSpinOp::handle(Record& a_rSignal)
{
	sp<DrawI> spDrawBrush;
	accessBrush(spDrawBrush,a_rSignal,e_quiet);

	if(spDrawBrush.isValid())
	{
		m_angle+=0.01;
		return;
	}

	sp<SurfaceAccessorI> spOutputPoints;
	if(!accessOutput(spOutputPoints,a_rSignal,e_point,e_position)) return;

	const Real cosa=cos(m_angle);
	const Real sina=sin(m_angle);

	const U32 count=spOutputPoints->count();
	for(U32 index=0;index<count;index++)
	{
		const SpatialVector point=spOutputPoints->spatialVector(index);

		const Real x=point[0]*cosa-point[2]*sina;
		const Real z=point[0]*sina+point[2]*cosa;

		spOutputPoints->set(index,SpatialVector(x,point[1],z));
	}
}
