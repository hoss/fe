/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __optest_SurfaceDrawOp_h__
#define __optest_SurfaceDrawOp_h__
namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Draw input surface to brush

	@ingroup optest
*//***************************************************************************/
class FE_DL_EXPORT SurfaceDrawOp:
	public OperatorSurfaceCommon,
	public Initialize<SurfaceDrawOp>
{
	public:

					SurfaceDrawOp(void):
						m_keepInput(FALSE)										{}
virtual				~SurfaceDrawOp(void)										{}

		void		initialize(void);

					//* As HandlerI
virtual	void		handle(Record& a_rSignal);

	private:
		WindowEvent		m_event;

		BWORD			m_keepInput;
		sp<SurfaceI>	m_spInput;
		sp<DrawBufferI>	m_spDrawBuffer;
		sp<DrawMode>	m_spDrawSolid;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __optest_SurfaceDrawOp_h__ */
