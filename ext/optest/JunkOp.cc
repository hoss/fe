/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <optest/optest.pmh>

using namespace fe;
using namespace fe::ext;

void JunkOp::initialize(void)
{
	catalog<String>("icon")="FE_alpha";

	catalog<String>("mode")="bad";
	catalog<String>("mode","choice:0")="good";
	catalog<String>("mode","choice:1")="bad";
}

void JunkOp::handle(Record& a_rSignal)
{
	const BWORD bad=(catalog<String>("mode")=="bad");

	sp<DrawI> spDrawI;
	if(!accessDraw(spDrawI,a_rSignal)) return;

	const I32 pointCount=8;
	SpatialVector line[pointCount];

	for(I32 m=0;m<pointCount;m++)
	{
		set(line[m],2.0+cos(m*45*fe::degToRad),Real(m),0.0);

		if(bad && (m%4)==1)
		{
			line[m][0]=nan("");
		}
	}

	spDrawI->drawLines(line,NULL,pointCount,DrawI::e_strip,false,NULL);
}
