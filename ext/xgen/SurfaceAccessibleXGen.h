/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __xgen_SurfaceAccessibleXGen_h__
#define __xgen_SurfaceAccessibleXGen_h__
namespace fe
{
namespace ext
{

/**************************************************************************//**
    @brief XGen Surface Binding

	@ingroup xgen
*//***************************************************************************/
class FE_DL_EXPORT SurfaceAccessibleXGen:
	public SurfaceAccessibleRecord,
	public ClassSafe<SurfaceAccessibleXGen>
{
	public:

	class Procedural: public XGenRenderAPI::ProceduralCallbacks
	{
	public:
					Procedural(void);
	virtual			~Procedural(void);

			BWORD	load(String a_filename,Real a_frame);

	virtual	void	flush(const char* a_geomName,
						XGenRenderAPI::PrimitiveCache* a_cache);
	virtual	void	log(const char* a_str);

	virtual	bool	get(EBoolAttribute a_attr) const;
	virtual
	const	char*	get(EStringAttribute a_attr) const;
	virtual	float	get(EFloatAttribute a_attr) const;
	virtual
	const	float*	get(EFloatArrayAttribute a_attr) const;
	virtual	unsigned int getSize(EFloatArrayAttribute a_attr) const;

	virtual
	const	char*	getOverride(const char* a_name) const;
	virtual	bool	getArchiveBoundingBox(
						const char* a_filename,
						XGenRenderAPI::bbox& out_bbox) const;
	virtual	void	getTransform(float a_time,
						XGenRenderAPI::mat44& out_mat) const;

			void	setSurfaceAccessible(
							sp<SurfaceAccessibleXGen> a_spSurfaceAccessibleXGen)
					{	m_spSurfaceAccessibleXGen=a_spSurfaceAccessibleXGen; }
			void	setSkinContext(sp<SurfaceAccessibleI> a_spSkinContext)
					{	m_spSkinContext=a_spSkinContext; }
			void	setSkinSurface(sp<SurfaceI> a_spSurfaceI)
					{	m_spSkinSurface=a_spSurfaceI; }

			void	setClearDescription(BWORD a_clearDescription)
					{	m_clearDescription=a_clearDescription; }
			void	setResolution(String a_resolution)
					{	m_resolution=a_resolution; }
			void	setDescription(String a_description)
					{	m_description=a_description; }

		private:

			sp<SurfaceAccessibleXGen>		m_spSurfaceAccessibleXGen;
			sp<SurfaceAccessibleI>			m_spSkinContext;
			sp<SurfaceI>					m_spSkinSurface;
			BWORD							m_clearDescription;
			String							m_resolution;
			String							m_description;

			float							m_zero;
			float							m_falloffArray[7];
	};

								SurfaceAccessibleXGen(void)					{}
virtual							~SurfaceAccessibleXGen(void)				{}

								//* as SurfaceAccessibleI

								using SurfaceAccessibleRecord::load;

virtual	BWORD					load(String a_filename,
										sp<Catalog> a_spSettings);

	protected:

virtual	void					reset(void);

	private:
		BWORD					probeOptions(String a_filename);
		BWORD					importBakedCurves(String a_filename);
		BWORD					importProcedural(String a_filename);

		std::map<String,String>				m_optionMap;
		std::map<String,String>				m_patchMap;
		Real								m_frame;

		sp<Scope>							m_spScope;
		sp<Layout>							m_spLayoutPoint;
		sp<Layout>							m_spLayoutPrimitive;
		sp<SurfaceAccessibleI>				m_spSkinContext;

		Accessor<SpatialVector>				m_aPosition;
		Array< Accessor<SpatialVector> >	m_aMotionArray;
		Accessor< Array<I32> >				m_aVerts;
		Accessor<Real>						m_aRadius;
		Accessor<Color>						m_aColor;
		Accessor<SpatialVector>				m_aNormal;
		Accessor<String>					m_aName;
		Accessor<String>					m_aSurfaceName;
		Accessor<String>					m_aSurfacePart;
		Accessor<SpatialVector>				m_aUVW;
		Accessor<SpatialVector>				m_aSurfacePosition;
		Accessor<SpatialVector>				m_aRefSurfacePosition;
		Accessor<SpatialVector>				m_aSurfaceNormal;
		Accessor<SpatialVector>				m_aRefSurfaceNormal;

		Accessor<SpatialVector>				m_aFaceUVW;
		Accessor<I32>						m_aProperties;
		Accessor<I32>						m_aFace;
		Accessor<I32>						m_aPrimitive;
		Accessor<I32>						m_aId;
		Accessor<Real>						m_aLength;
		Accessor<Real>						m_aWidth;
		Accessor<Real>						m_aTaper;
		Accessor<Real>						m_aTaperStart;

		sp<RecordGroup>						m_spPointRG;
		sp<RecordGroup>						m_spPrimRG;

		I32									m_pointCount;

		BWORD								m_shutter_populated;
		BWORD								m_normal_populated;
		BWORD								m_uv_populated;
		BWORD								m_surfaceP_populated;
		BWORD								m_refSurfaceP_populated;
		BWORD								m_surfaceN_populated;
		BWORD								m_refSurfaceN_populated;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __xgen_SurfaceAccessibleXGen_h__ */
