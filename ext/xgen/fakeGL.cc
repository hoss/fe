/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "fe/platform.h"

//* seems to work the same as regular or weak
#define FE_DL_WEAK FE_DL_EXPORT __attribute__((weak))

extern "C"
{
FE_DL_WEAK void glBegin(void) {}
FE_DL_WEAK void glBindTexture(void) {}
FE_DL_WEAK void glCallList(void) {}
FE_DL_WEAK void glCallLists(void) {}
FE_DL_WEAK void glClientActiveTexture(void) {}
FE_DL_WEAK void glColor3fv(void) {}
FE_DL_WEAK void glColor3ubv(void) {}
FE_DL_WEAK void glColor4fv(void) {}
FE_DL_WEAK void glColorMaterial(void) {}
FE_DL_WEAK void glColorPointer(void) {}
FE_DL_WEAK void glDeleteLists(void) {}
FE_DL_WEAK void glDisable(void) {}
FE_DL_WEAK void glDisableClientState(void) {}
FE_DL_WEAK void glDrawElements(void) {}
FE_DL_WEAK void glDrawRangeElements(void) {}
FE_DL_WEAK void glEnable(void) {}
FE_DL_WEAK void glEnableClientState(void) {}
FE_DL_WEAK void glEnd(void) {}
FE_DL_WEAK void glEndList(void) {}
FE_DL_WEAK void glFinish(void) {}
FE_DL_WEAK void glGenLists(void) {}
FE_DL_WEAK void glGenTextures(void) {}
FE_DL_WEAK void glGetFloatv(void) {}
FE_DL_WEAK void glGetIntegerv(void) {}
FE_DL_WEAK void glGetMaterialfv(void) {}
FE_DL_WEAK void glIsEnabled(void) {}
FE_DL_WEAK void glListBase(void) {}
FE_DL_WEAK void glMaterialfv(void) {}
FE_DL_WEAK void glMultMatrixf(void) {}
FE_DL_WEAK void glMultiTexCoord2f(void) {}
FE_DL_WEAK void glMultiTexCoord3f(void) {}
FE_DL_WEAK void glNewList(void) {}
FE_DL_WEAK void glNormal3f(void) {}
FE_DL_WEAK void glNormal3fv(void) {}
FE_DL_WEAK void glNormal3sv(void) {}
FE_DL_WEAK void glNormalPointer(void) {}
FE_DL_WEAK void glPointSize(void) {}
FE_DL_WEAK void glPolygonMode(void) {}
FE_DL_WEAK void glPopAttrib(void) {}
FE_DL_WEAK void glPopClientAttrib(void) {}
FE_DL_WEAK void glPopMatrix(void) {}
FE_DL_WEAK void glPushAttrib(void) {}
FE_DL_WEAK void glPushClientAttrib(void) {}
FE_DL_WEAK void glPushMatrix(void) {}
FE_DL_WEAK void glRasterPos3f(void) {}
FE_DL_WEAK void glRotatef(void) {}
FE_DL_WEAK void glScalef(void) {}
FE_DL_WEAK void glTexCoord2f(void) {}
FE_DL_WEAK void glTexCoordPointer(void) {}
FE_DL_WEAK void glTexEnvi(void) {}
FE_DL_WEAK void glTexImage2D(void) {}
FE_DL_WEAK void glTranslatef(void) {}
FE_DL_WEAK void glVertex3f(void) {}
FE_DL_WEAK void glVertex3fv(void) {}
FE_DL_WEAK void glVertexPointer(void) {}
FE_DL_WEAK void gluNewQuadric(void) {}
FE_DL_WEAK void gluQuadricDrawStyle(void) {}
FE_DL_WEAK void gluQuadricNormals(void) {}
FE_DL_WEAK void gluQuadricTexture(void) {}
FE_DL_WEAK void gluSphere(void) {}

FE_DL_WEAK void glMatrixMode(void) {}
FE_DL_WEAK void glMultMatrixd(void) {}
FE_DL_WEAK void glVertex3d(void) {}
FE_DL_WEAK void gluCylinder(void) {}
FE_DL_WEAK void gluDeleteQuadric(void) {}

FE_DL_WEAK void glGetString(void) {}
FE_DL_WEAK void glXGetCurrentDisplay(void) {}
FE_DL_WEAK void glXGetProcAddressARB(void) {}
FE_DL_WEAK void glXQueryExtensionsString(void) {}

FE_DL_WEAK void __clewBuildProgram(void) {}
FE_DL_WEAK void __clewCreateBuffer(void) {}
FE_DL_WEAK void __clewCreateFromGLBuffer(void) {}
FE_DL_WEAK void __clewCreateFromGLTexture3D(void) {}
FE_DL_WEAK void __clewCreateImage2D(void) {}
FE_DL_WEAK void __clewCreateImage3D(void) {}
FE_DL_WEAK void __clewCreateKernel(void) {}
FE_DL_WEAK void __clewCreateProgramWithSource(void) {}
FE_DL_WEAK void __clewEnqueueAcquireGLObjects(void) {}
FE_DL_WEAK void __clewEnqueueBarrier(void) {}
FE_DL_WEAK void __clewEnqueueMapBuffer(void) {}
FE_DL_WEAK void __clewEnqueueMapImage(void) {}
FE_DL_WEAK void __clewEnqueueNDRangeKernel(void) {}
FE_DL_WEAK void __clewEnqueueReadBuffer(void) {}
FE_DL_WEAK void __clewEnqueueCopyBuffer(void) {}
FE_DL_WEAK void __clewEnqueueReleaseGLObjects(void) {}
FE_DL_WEAK void __clewEnqueueUnmapMemObject(void) {}
FE_DL_WEAK void __clewGetDeviceInfo(void) {}
FE_DL_WEAK void __clewGetImageInfo(void) {}
FE_DL_WEAK void __clewGetKernelWorkGroupInfo(void) {}
FE_DL_WEAK void __clewGetMemObjectInfo(void) {}
FE_DL_WEAK void __clewGetProgramBuildInfo(void) {}
FE_DL_WEAK void __clewReleaseCommandQueue(void) {}
FE_DL_WEAK void __clewReleaseEvent(void) {}
FE_DL_WEAK void __clewReleaseKernel(void) {}
FE_DL_WEAK void __clewReleaseMemObject(void) {}
FE_DL_WEAK void __clewReleaseProgram(void) {}
FE_DL_WEAK void __clewSetKernelArg(void) {}
FE_DL_WEAK void __clewWaitForEvents(void) {}
}
