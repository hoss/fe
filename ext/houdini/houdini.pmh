#ifndef __houdini_houdini_pmh__
#define __houdini_houdini_pmh__

/**
	@defgroup houdini Houdini HDK
	@ingroup ext

	@section houdini_install Installation Notes

	Get started here:

	https://www.sidefx.com/docs/hdk/_h_d_k__intro__getting_started.html

	Update the 'houdini' variables in @c ${FE_ROOT}/base/local/py.
*/

// replaced with using 'hcustom -c'
#if 0 // __GNUC__
#define	LINUX
#define	AMD64
#define	SIZEOF_VOID_P		8
#define	ENABLE_THREADS
#define	USE_PTHREADS
#define	_FILE_OFFSET_BITS	64
#define	GCC4
#define	GCC3
#define	MAKING_DSO
#define OPENVDB_ENABLED
#define	SESI_LITTLE_ENDIAN
#endif

#include <GU/GU_Detail.h>
#include <GU/GU_PrimCircle.h>
#include <GU/GU_PrimMesh.h>
#include <GU/GU_PrimPoly.h>
#include <GU/GU_PrimSphere.h>
#include <GU/GU_PrimTube.h>
#include <GU/GU_PrimTriStrip.h>
#include <GEO/GEO_AttributeHandle.h>
#include <GEO/GEO_Point.h>
#include <GEO/GEO_TriMesh.h>

#if FE_HOUDINI_MAJOR < 12 || (FE_HOUDINI_MAJOR == 12 && FE_HOUDINI_MINOR < 5)
//* newer glibc macro conflics with old boost enum (pre 1_50)
#undef TIME_UTC
#endif

#include <MSS/MSS_SingleOpState.h>
#include <PRM/PRM_Include.h>
#include <PRM/PRM_Parm.h>
#include <PRM/PRM_SpareData.h>
#include <OP/OP_Director.h>
#include <OP/OP_Operator.h>
#include <OP/OP_OperatorTable.h>
#include <OP/OP_SaveFlags.h>
#include <SOP/SOP_Node.h>
#include <SYS/SYS_Math.h>
#include <UT/UT_Interrupt.h>
#include <UT/UT_CPIO.h>
#include <UT/UT_Exit.h>
#include <UT/UT_Ramp.h>
#include <UT/UT_Version.h>
#include <UT/UT_Undo.h>
#include <UT/UT_UndoManager.h>
#include <GR/GR_OptionTable.h>
#include <GR/GR_UserOption.h>

#if FE_HOUDINI_MAJOR >= 12
#include <UT/UT_IStream.h>
#include <UT/UT_OStream.h>
#include <GA/GA_PageIterator.h>
#define FE_HOUDINI_USE_GA
#else
#include <GB/GB_Element.h>
#include <GB/GB_ElementTree.h>
#include <GB/GB_Group.h>
#include <UT/UT_StreamFilter.h>
#endif

#define FE_HOUDINI_12_5_PLUS (UT_MAJOR_VERSION_INT>=13 || (UT_MAJOR_VERSION_INT>=12 && UT_MINOR_VERSION_INT>=5))
#define FE_HOUDINI_13_PLUS (UT_MAJOR_VERSION_INT>=13)
#define FE_HOUDINI_14_PLUS (UT_MAJOR_VERSION_INT>=14)
#define FE_HOUDINI_15_PLUS (UT_MAJOR_VERSION_INT>=15)
#define FE_HOUDINI_16_PLUS (UT_MAJOR_VERSION_INT>=16)
#define FE_HOUDINI_19_PLUS (UT_MAJOR_VERSION_INT>=19)
#define FE_HOUDINI_16_0_700_PLUS (FE_HOUDINI_16_5_PLUS || (FE_HOUDINI_16_PLUS && UT_BUILD_VERSION_INT>=700))
#define FE_HOUDINI_16_5_PLUS (UT_MAJOR_VERSION_INT>=17 || (UT_MAJOR_VERSION_INT>=16 && UT_MINOR_VERSION_INT>=5))
#define FE_HOUDINI_19_5_PLUS (UT_MAJOR_VERSION_INT>=20 || (UT_MAJOR_VERSION_INT>=19 && UT_MINOR_VERSION_INT>=5))
#define FE_HOUDINI_19_0_671_PLUS (FE_HOUDINI_19_5_PLUS || (FE_HOUDINI_19_PLUS && UT_BUILD_VERSION_INT>=671))
#define FE_HOUDINI_19_6_PLUS (UT_MAJOR_VERSION_INT>=20 || (UT_MAJOR_VERSION_INT>=19 && UT_MINOR_VERSION_INT>=6))
#define FE_HOUDINI_19_5_323_PLUS (FE_HOUDINI_19_6_PLUS || (FE_HOUDINI_19_PLUS && UT_BUILD_VERSION_INT>=323))

#define FE_HOUDINI_HARDENING FE_HOUDINI_12_5_PLUS

#define FE_HOUDINI_ALLOWS_MSVS_2022 (FE_HOUDINI_19_0_671_PLUS || FE_HOUDINI_19_5_323_PLUS) && !(UT_MAJOR_VERSION_INT==19 && UT_MINOR_VERSION_INT==5 && UT_BUILD_VERSION_INT<323)

#define FE_HOUDINI_HYDRA_HOOK FE_HOUDINI_16_0_700_PLUS
#define FE_HOUDINI_HYDRA_BRUSH FALSE//FE_HOUDINI_16_0_700_PLUS

//* TODO fix VDB on 16.5+
#if FE_HOUDINI_USE_VDB && FE_HOUDINI_12_5_PLUS && !FE_HOUDINI_16_5_PLUS
#define FE_HOUDINI_VDB
#include <GEO/GEO_PrimVDB.h>
#endif

#if FE_HOUDINI_12_5_PLUS
#include <GEO/GEO_PolyCounts.h>
#endif

#include "houdini/houdini.h"
#include "meta/meta.pmh"
#include "operate/operate.pmh"

#include "houdini/assertHoudini.h"

#include "houdini/HoudiniContext.h"
#include "houdini/HoudiniBrush.h"
#include "houdini/HoudiniRamp.h"
#include "houdini/HoudiniSOP.h"
#include "houdini/SurfaceCurvesHoudini.h"
#include "houdini/SurfacePointsHoudini.h"
#include "houdini/SurfaceTrianglesHoudini.h"
#include "houdini/SurfaceAccessorHoudini.h"
#include "houdini/SurfaceAccessibleHoudini.h"
#include "houdini/HoudiniDraw.h"
#include "houdini/CacheOp.h"
#include "houdini/PortalOp.h"
#include "houdini/FontHoudini.h"

#if FE_HOUDINI_15_PLUS
#include "houdini/UnveilOp.h"
#include "houdini/VeilOp.h"

#include <GEO/GEO_PrimNull.h>
#include <GUI/GUI_PrimitiveHook.h>
#include <GR/GR_Primitive.h>
#include <DM/DM_RenderTable.h>
#include <GA/GA_Primitive.h>
#include <GA/GA_IntrinsicDef.h>
#include <GA/GA_IntrinsicMacros.h>

#include "houdini/HoudiniPrimBase.h"
#include "houdini/HoudiniPrimComponent.h"
#include "houdini/HoudiniRenderComponent.h"
#endif

#endif /* __houdini_houdini_pmh__ */


//* `info from sample and running hcustom'

//* already defined
//#define	_GNU_SOURCE
//#define	_REENTRANT

//* unneeded?

//#define	VERSION				"11.0.775"
//#define	LINUX
//#define	AMD64
//#define	SIZEOF_VOID_P		8
//#define	ENABLE_THREADS
//#define	USE_PTHREADS
//#define	_FILE_OFFSET_BITS	64
//#define	GCC4
//#define	GCC3
//#define	MAKING_DSO

/*
gcc:
-DUT_DSO_TAGINFO='"3262197cbf18590326a4089a673794e82e41c7b0917c87ce10f990485e46e4887b912bda9f321e710746c7fd29da20094bffec0afdd0e35040162fe866fbabac5bbe45a0ff924994e17f970847f041abee0c90a2e5"'
-DVERSION=\"11.0.775\"
-D_GNU_SOURCE
-DLINUX
-DAMD64
-m64
-fPIC
-DSIZEOF_VOID_P=8
-DSESI_LITTLE_ENDIAN
-DENABLE_THREADS
-DUSE_PTHREADS
-D_REENTRANT
-D_FILE_OFFSET_BITS=64
-c
-DGCC4
-DGCC3
-Wno-deprecated
-I/opt/hfs11.0.775/toolkit/include
-I/opt/hfs11.0.775/toolkit/include/htools
-Wall -W -Wno-parentheses -Wno-sign-compare -Wno-reorder
-Wno-uninitialized -Wunused -Wno-unused-parameter
-O2
-DMAKING_DSO -o SOP_Surface.o SOP_Surface.cc

g++ -shared SOP_Surface.o
-L/usr/X11R6/lib64 -L/usr/X11R6/lib
-lGLU -lGL -lX11 -lXext -lXi -ldl
-o /home/jpweber/houdini11.0/dso/SOP_Surface.so


visual studio:
"C:\Program Files\Microsoft Visual Studio\2022\Community\VC\Tools\MSVC\14.32.31326\bin\Hostx64\x64\cl"
-TP none.cc
-DUT_DSO_TAGINFO='"3262197cbf18590326a4089a671eb4cf4b7afac082148ff618d9c97d1714bfd251bd2a8dcf7b25710746e2e67dda317104a0f57aba8ef94447162ced6dfb91b648ac45b2ef805bbea3268d0847ba11fcab0c8ceacfa29374e4b35f1d0560"'
-nologo
-TP
-Zc:forScope
-Zc:rvalueCast
-Zc:inline
-Zc:strictStrings
-std:c++17
-Zc:referenceBinding
-Zc:ternary
-Zc:throwingNew
-permissive-
-Zc:__cplusplus
-DAMD64
-DSIZEOF_VOID_P=8
-DI386
-DWIN32
-DSWAP_BITFIELDS
-D_WIN32_WINNT=0x0600
-DNOMINMAX
-DSTRICT
-DWIN32_LEAN_AND_MEAN
-D_USE_MATH_DEFINES
-D_CRT_SECURE_NO_DEPRECATE
-D_CRT_NONSTDC_NO_DEPRECATE
-D_SCL_SECURE_NO_WARNINGS
-DSESI_LITTLE_ENDIAN
-DHBOOST_ALL_NO_LIB
-DEIGEN_MALLOC_ALREADY_ALIGNED=0
-DFBX_ENABLED=1
-DOPENCL_ENABLED=1
-DOPENVDB_ENABLED=1
-D_SILENCE_ALL_CXX17_DEPRECATION_WARNINGS=1
-I .
-I "C:/PROGRA~1/SIDEEF~1/HOUDIN~1.657/toolkit/include"
-I "C:/Program Files/Microsoft Visual Studio/2022/Community/VC/Tools/MSVC/14.32.31326/include"
-I "C:/Program Files (x86)/Windows Kits/10/Include/10.0.19041.0/ucrt"
-I "C:/Program Files (x86)/Windows Kits/10/Include/10.0.19041.0/um"
-I "C:/Program Files (x86)/Windows Kits/10/Include/10.0.19041.0/shared"
-wd4384 -wd4355 -w14996
-O2
-DNDEBUG
-DUSE_PYTHON3=1
-MD
-EHsc
-GR
-bigobj
-DMAKING_DSO
-Fo"none.o"
-LD
-link
-LIBPATH:"C:/Program Files/Microsoft Visual Studio/2022/Community/VC/Tools/MSVC/14.32.31326/lib/x64"
-LIBPATH:"C:/Program Files (x86)/Windows Kits/10/Lib/10.0.19041.0/ucrt/x64"
-LIBPATH:"C:/Program Files (x86)/Windows Kits/10/Lib/10.0.19041.0/um/x64"
-LIBPATH:"C:/PROGRA~1/SIDEEF~1/HOUDIN~1.657/custom/houdini/dsolib"
"C:/PROGRA~1/SIDEEF~1/HOUDIN~1.657/custom/houdini/dsolib/*.a"
"C:/PROGRA~1/SIDEEF~1/HOUDIN~1.657/custom/houdini/dsolib/*.lib"
-out:"c:/Users/jpweber/houdini19.0/dso/none.dll"

in local.env:
HT="c:/Program Files/Side Effects Software//Houdini 19.0.657/toolkit"
MSVCDir="C:/Program Files/Microsoft Visual Studio/2022/Community/VC/Tools/MSVC/14.32.31326"

to run on command line:
set HOUDINI_DSO_ERROR=1
set HOUDINI_DSO_PATH=^&;c:/Users/jpweber/proj/gitlab/fe/lib/x86_win64_optimize/houdini19.0.657/dso
set path=%PATH%;c:\Program Files\Side Effects Software\Houdini 19.0.657\bin
set path=%PATH%;c:\Users\jpweber\proj\gitlab\fe\lib\x86_win64_optimize

to check HOUDINI_DSO_PATH:
hconfig -p
*/

//#include <UT/UT_DSOVersion.h>
//#include <SYS/SYS_Math.h>
//#include <GU/GU_Detail.h>
//#include <GEO/GEO_AttributeHandle.h>
//#include <PRM/PRM_Include.h>
//#include <OP/OP_Director.h>
//#include <OP/OP_Operator.h>
//#include <OP/OP_OperatorTable.h>
