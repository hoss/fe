/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __houdini_SurfaceAccessorHapi_h__
#define __houdini_SurfaceAccessorHapi_h__

#define	FE_SAHE_ASSERT_BOUNDS	TRUE

namespace fe
{
namespace ext
{

/**************************************************************************//**
    @brief Houdini Engine Surface Editing Implementation

	@ingroup houdini
*//***************************************************************************/
class FE_DL_EXPORT SurfaceAccessorHapi:
	public SurfaceAccessorBase,
	public CastableAs<SurfaceAccessorHapi>
{
	public:
						SurfaceAccessorHapi(void):
							m_pHapiSession(NULL)
						{
							setName("SurfaceAccessorHapi");
						}

virtual					~SurfaceAccessorHapi(void)
						{
						}

		BWORD			bind(SurfaceAccessibleI::Element a_element,
								SurfaceAccessibleI::Attribute a_attribute)
						{
							m_attribute=a_attribute;

							String name;
							switch(a_attribute)
							{
								case SurfaceAccessibleI::e_generic:
								case SurfaceAccessibleI::e_position:
									name="P";
									break;
								case SurfaceAccessibleI::e_normal:
									name="N";
									break;
								case SurfaceAccessibleI::e_uv:
									name="uv";
									break;
								case SurfaceAccessibleI::e_color:
									name="Cd";
									break;
								case SurfaceAccessibleI::e_vertices:
									m_attrName="vertices";
									FEASSERT(a_element==
											SurfaceAccessibleI::e_primitive);
									m_element=a_element;
									break;
								case SurfaceAccessibleI::e_properties:
									m_attrName="properties";
									FEASSERT(a_element==
											SurfaceAccessibleI::e_primitive);
							}
							return bindInternal(a_element,name);
						}
		BWORD			bind(SurfaceAccessibleI::Element a_element,
								const String& a_name)
						{
							m_attribute=SurfaceAccessibleI::e_generic;

							if(a_name=="P")
							{
								m_attribute=SurfaceAccessibleI::e_position;
							}
							else if(a_name=="N")
							{
								m_attribute=SurfaceAccessibleI::e_normal;
							}
							else if(a_name=="uv")
							{
								m_attribute=SurfaceAccessibleI::e_uv;
							}
							else if(a_name=="Cd")
							{
								m_attribute=SurfaceAccessibleI::e_color;
							}
							else if(a_name=="properties")
							{
								m_attribute=SurfaceAccessibleI::e_properties;
								FEASSERT(a_element==
										SurfaceAccessibleI::e_primitive);
							}

							return bindInternal(a_element,a_name);
						}

						//* as SurfaceAccessorI
virtual	String			type(void) const;

virtual	U32				count(void) const;
virtual	U32				subCount(U32 a_index) const;

						using SurfaceAccessorBase::set;
virtual	void			set(U32 a_index,U32 a_subIndex,String a_string)
						{
#if	FE_SAHE_ASSERT_BOUNDS
							FEASSERT(a_index<count());
							FEASSERT(a_subIndex<subCount(a_index));
#endif
							//* TODO
						}
virtual	String			string(U32 a_index,U32 a_subIndex=0)
						{
#if	FE_SAHE_ASSERT_BOUNDS
							FEASSERT(a_index<count());
							FEASSERT(a_subIndex<subCount(a_index));
#endif

							if(m_stringBuffer.size()>a_index)
							{
								return m_stringBuffer[a_index];
							}

							//* TODO
							return String();
						}

virtual	void			set(U32 a_index,U32 a_subIndex,I32 a_integer)
						{
#if	FE_SAHE_ASSERT_BOUNDS
							FEASSERT(a_index<count());
							FEASSERT(a_subIndex<subCount(a_index));
#endif
							//* TODO
						}

virtual	I32				integer(U32 a_index,U32 a_subIndex=0)
						{
							if(m_attribute==SurfaceAccessibleI::e_properties)
							{
								//* TODO per primitive
								if(a_subIndex==SurfaceAccessibleI::e_openCurve
										&& m_curveParts)
								{
									return 1;
								}

								return 0;
							}

#if	FE_SAHE_ASSERT_BOUNDS
							FEASSERT(a_index<count());
							FEASSERT(m_attribute==
									SurfaceAccessibleI::e_properties ||
									a_subIndex<subCount(a_index));
#endif
							if(m_element==SurfaceAccessibleI::e_primitive &&
									m_attribute==SurfaceAccessibleI::e_vertices)
							{
								const I32 offset=m_vertexOffset[a_index];

								if(a_subIndex<m_vertexCount[a_index])
								{
									if(!m_vertexBuffer.size())
									{
										return offset+a_subIndex;
									}

									return m_vertexBuffer[offset+a_subIndex];
								}
							}

							if(m_integerBuffer.size()>a_index)
							{
								return m_integerBuffer[a_index];
							}

							//* TODO
							return 0;
						}
virtual	I32				duplicate(U32 a_index,U32 a_subIndex=0)
						{
							//* TODO
							return -1;
						}

						using SurfaceAccessorBase::append;
virtual	I32				append(SurfaceAccessibleI::Form a_form)
						{
							//* TODO
							return -1;
						}
virtual	void			append(U32 a_index,I32 a_integer)
						{
							//* TODO
						}
virtual	void			append(Array< Array<I32> >& a_rPrimVerts)
						{
							//* TODO
						}

virtual	void			remove(U32 a_index,I32 a_integer)
						{
							//* TODO
						}

virtual	void			set(U32 a_index,U32 a_subIndex,Real a_real)
						{
#if	FE_SAHE_ASSERT_BOUNDS
							FEASSERT(a_index<count());
							FEASSERT(a_subIndex<subCount(a_index));
#endif
							//* TODO
						}

virtual	Real			real(U32 a_index,U32 a_subIndex=0)
						{
#if	FE_SAHE_ASSERT_BOUNDS
							FEASSERT(a_index<count());
							FEASSERT(a_subIndex<subCount(a_index));
#endif

							if(m_realBuffer.size()>a_index)
							{
								return m_realBuffer[a_index];
							}

							//* TODO
							return 0.0;
						}

virtual	void			set(U32 a_index,U32 a_subIndex,
								const SpatialVector& a_vector)
						{
							//* TODO
						}
virtual	SpatialVector	spatialVector(U32 a_index,U32 a_subIndex=0)
						{
#if	FE_SAHE_ASSERT_BOUNDS
							FEASSERT(a_index<count());
							FEASSERT(a_subIndex<subCount(a_index));
#endif
							if(m_attribute==SurfaceAccessibleI::e_properties)
							{
								return SpatialVector(0.0,0.0,0.0);
							}

							if(m_element==SurfaceAccessibleI::e_pointGroup)
							{
								//* TODO
								return SpatialVector(0.0,0.0,0.0);
							}

							if(m_element==
									SurfaceAccessibleI::e_primitiveGroup ||
									m_attribute==SurfaceAccessibleI::e_vertices)
							{
								FEASSERT(m_element==
										SurfaceAccessibleI::e_primitiveGroup
										|| m_element==
										SurfaceAccessibleI::e_primitive);

								//* TODO
								return SpatialVector(0.0,0.0,0.0);
							}

							if((m_element==SurfaceAccessibleI::e_primitive ||
									m_element==SurfaceAccessibleI::e_vertex ||
									m_element==
									SurfaceAccessibleI::e_primitiveGroup) &&
									m_attribute==SurfaceAccessibleI::e_uv)
							{
								//* TODO
								return SpatialVector(0.0,0.0,0.0);
							}

							if(m_vectorBuffer.size()>a_index)
							{
								return m_vectorBuffer[a_index];
							}

							//* TODO
							return SpatialVector(0.0,0.0,0.0);
						}
virtual	BWORD			spatialVector(SpatialVector* a_pVectorArray,
								const Vector2i* a_pIndexArray,I32 a_arrayCount)
						{
							//* TODO
							return FALSE;
						}

		sp<SurfaceAccessibleI>	surfaceAccessible(void)
						{	return m_spSurfaceAccessibleI; }

						//* Houdini specific
		void			setSession(HAPI_Session* a_pHapiSession)
						{
							m_pHapiSession=a_pHapiSession;
						}
		HAPI_Session*	session(void) const		{ return m_pHapiSession; }

		void			setNodeId(HAPI_NodeId a_hapiNodeId)
						{
							m_hapiNodeId=a_hapiNodeId;
						}
		HAPI_NodeId		nodeId(void) const		{ return m_hapiNodeId; }

	private:
virtual	BWORD			bindInternal(SurfaceAccessibleI::Element a_element,
								const String& a_name);

		BWORD			isBound(void) const
						{
							//* TODO

							return (HAPI_IsSessionValid(m_pHapiSession)==
									HAPI_RESULT_SUCCESS &&
									(m_stringBuffer.size() ||
									m_integerBuffer.size() ||
									m_realBuffer.size() ||
									m_vectorBuffer.size()));
						}

		HAPI_Session*			m_pHapiSession;
		HAPI_NodeId				m_hapiNodeId;

		I32						m_curveParts;
		I32						m_meshParts;

		Array<String>			m_stringBuffer;
		Array<I32>				m_integerBuffer;
		Array<Real>				m_realBuffer;
		Array<SpatialVector>	m_vectorBuffer;

		Array<I32>				m_vertexBuffer;	//* point index per vertex
		Array<I32>				m_vertexOffset;	//* first vertex per primitive
		Array<I32>				m_vertexCount;	//* vertices per primitive
};

} /* namespace ext */
} /* namespace fe */

#endif /* __houdini_SurfaceAccessorHapi_h__ */
