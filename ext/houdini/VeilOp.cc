/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <houdini/houdini.pmh>

using namespace fe;
using namespace fe::ext;

void VeilOp::initialize(void)
{
	catalog<String>("icon")="FE_alpha";

	catalog< sp<Component> >("Input Surface");
	catalog<bool>("Input Surface","optional")=true;

	catalog< sp<Component> >("Output Surface");
	catalog<String>("Output Surface","IO")="output";
}

void VeilOp::handle(Record& a_rSignal)
{
//	feLog("VeilOp::handle\n");

	sp<SurfaceAccessibleI> spInputAccessible;
	if(!access(spInputAccessible,"Input Surface")) return;

	sp<SurfaceAccessibleI> spOutputAccessible;
	if(!accessOutput(spOutputAccessible,a_rSignal)) return;

	sp<SurfaceAccessibleHoudini> spAccessibleHoudini=spOutputAccessible;
	if(spAccessibleHoudini.isNull())
	{
		catalog<String>("error")=
				"failed to access output as a Houdini surface;";
		return;
	}

	//* TODO other surface implementations
	sp<SurfaceAccessibleI> spPayload=
			registry()->create("*.SurfaceAccessibleOpenCL");

	if(spPayload.isNull())
	{
		catalog<String>("error")="failed to create OpenCL surface;";
		return;
	}

	GU_Detail* pGdp=spAccessibleHoudini->gdpChangeable();

	HoudiniPrimComponent* pPrimComponent=static_cast<HoudiniPrimComponent*>(
			pGdp->appendPrimitive(HoudiniPrimComponent::getTypeId()));
	if(!pPrimComponent)
	{
		catalog<String>("error")="failed to create FE surface primitive;";
		return;
	}

	spPayload->copy(spInputAccessible);
	pPrimComponent->bind(spPayload);
}
