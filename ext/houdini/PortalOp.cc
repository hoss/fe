/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <houdini/houdini.pmh>

using namespace fe;
using namespace fe::ext;

//* TODO
//* could be general, not Houdini-dependent

void PortalOp::initialize(void)
{
	catalog<String>("icon")="FE_alpha";

	catalog<String>("sendName");
	catalog<String>("sendName","label")="Send";

	catalog<String>("receiveName");
	catalog<String>("receiveName","label")="Receive";

	catalog< sp<Component> >("Input Surface");
	catalog<bool>("Input Surface","optional")=true;

	catalog< sp<Component> >("Output Surface");
	catalog<String>("Output Surface","IO")="output";
	catalog<String>("Output Surface","copy")="Input Surface";
	catalog<bool>("Output Surface","continuous")=true;
}

void PortalOp::handle(Record& a_rSignal)
{
	feLog("PortalOp::handle \"%s\" \"%s\"\n",
			catalog<String>("sendName").c_str(),
			catalog<String>("receiveName").c_str());

	sp<Catalog> spMasterCatalog=registry()->master()->catalog();

	const String sendName=catalog<String>("sendName");
	if(!sendName.empty())
	{
		const String portalName="portal:"+sendName;

		sp<SurfaceAccessibleI> spInputAccessible;
		if(!access(spInputAccessible,"Input Surface")) return;

		sp<SurfaceAccessibleHoudini> spAccessibleHoudini=spInputAccessible;
		if(spAccessibleHoudini.isValid())
		{
			const GU_Detail* pGdp=spAccessibleHoudini->gdp();

			sp<SurfaceAccessibleHoudini> spPortalHoudini=
					spMasterCatalog->catalog< sp<Component> >(portalName);
			if(!spPortalHoudini.isValid())
			{
				spPortalHoudini=
						registry()->create("*.SurfaceAccessibleHoudini");
				spMasterCatalog->catalog< sp<Component> >(portalName)=
						spPortalHoudini;
			}
			if(spPortalHoudini.isValid())
			{
				//* duplicate
				//* WARNING presumably leaky
				spPortalHoudini->setGdp(new GU_Detail(pGdp));
			}
		}
	}

	const String receiveName=catalog<String>("receiveName");
	if(!receiveName.empty())
	{
		const String portalName="portal:"+receiveName;

		sp<SurfaceAccessibleI> spOutputAccessible;
		if(!accessOutput(spOutputAccessible,a_rSignal)) return;

		sp<SurfaceAccessibleHoudini> spAccessibleHoudini=spOutputAccessible;
		feLog("PortalOp::handle spAccessibleHoudini %d\n",
				spAccessibleHoudini.isValid());
		if(spAccessibleHoudini.isValid())
		{
			GU_Detail* pGdp=spAccessibleHoudini->gdpChangeable();

			feLog("PortalOp::handle pGdp %p\n",pGdp);

			sp<SurfaceAccessibleHoudini> spPortalHoudini=
					spMasterCatalog->catalog< sp<Component> >(portalName);
			feLog("PortalOp::handle spPortalHoudini %d\n",
					spPortalHoudini.isValid());
			if(spPortalHoudini.isValid())
			{
				pGdp->duplicate(*spPortalHoudini->gdp());
			}
		}
	}
}
