/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

//*	derived from Houndini sample: PrimSurface_Surface

#ifndef __houdini_HoudiniRenderComponent_h__
#define __houdini_HoudiniRenderComponent_h__

namespace fe
{
namespace ext
{

class HoudiniRenderHookComponent: public GUI_PrimitiveHook
{
	public:

						HoudiniRenderHookComponent(void);
virtual					~HoudiniRenderHookComponent(void);

virtual	GR_Primitive*	createPrimitive(const GT_PrimitiveHandle& gt_prim,
							const GEO_Primitive* geo_prim,
							const GR_RenderInfo* info,
							const char* cache_name,
							GR_PrimAcceptResult& processed);

static	sp<DrawI>		ms_spDraw;
};

/**************************************************************************//**
	@brief Houdini render handler for general Component

	@ingroup houdini
*//***************************************************************************/
class HoudiniRenderComponent: public GR_Primitive
{
	public:

							HoudiniRenderComponent(const GR_RenderInfo *info,
								const char *cache_name,
								const GEO_Primitive *prim);
virtual						~HoudiniRenderComponent(void);

virtual
const	char*				className(void) const
							{	return "HoudiniRenderComponent"; }

virtual GR_PrimAcceptResult	acceptPrimitive(GT_PrimitiveType t,
								int geo_type,
								const GT_PrimitiveHandle& ph,
								const GEO_Primitive* prim);

virtual	void				update(RE_Render* a_pRender,
								const GT_PrimitiveHandle& primh,
								const GR_UpdateParms& p);

#if FE_HOUDINI_16_PLUS
virtual	void				render(RE_Render* a_pRender,
								GR_RenderMode render_mode,
								GR_RenderFlags flags,
								GR_DrawParms dp);
#else
virtual	void				render(RE_Render* a_pRender,
								GR_RenderMode render_mode,
								GR_RenderFlags flags,
								const GR_DisplayOption *opt,
								const RE_MaterialList* materials);

virtual	void				renderInstances(RE_Render *a_pRender,
								GR_RenderMode render_mode,
								GR_RenderFlags flags,
								const GR_DisplayOption *opt,
								const RE_MaterialList *materials,
								int render_instance)						{}
#endif

virtual	int					renderPick(RE_Render* a_pRender,
								const GR_DisplayOption* opt,
								unsigned int pick_type,
								GR_PickStyle pick_style,
								bool has_pick_map)
							{	return 0; }

	private:
		int					m_typeID;
		sp<Component>		m_spPayload;

	class RenderBrush: public MetaBrush
	{
		public:
							RenderBrush(void);
	virtual					~RenderBrush(void);

			void			render(RE_Render* a_pRender,
								sp<Component> a_spPayload);

	virtual	void			drawPost(void);

		private:

			RE_Render*		m_pRender;
			sp<Component>	m_spPayload;
			sp<DrawableI>	m_spDrawable;
			sp<DrawBufferI>	m_spDrawBuffer;
	};

		RenderBrush*		m_pRenderBrush;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __houdini_HoudiniRenderComponent_h__ */
