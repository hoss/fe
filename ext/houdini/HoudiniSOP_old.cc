/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <houdini/houdini.pmh>

#define	FE_HSOP_DISCOVER_DEBUG		FALSE
#define	FE_HSOP_CATALOG_DEBUG		FALSE
#define	FE_HSOP_COOK_DEBUG			FALSE
#define	FE_HSOP_BRUSH_DEBUG			FALSE
#define	FE_HSOP_UNDO_DEBUG			FALSE
#define	FE_HSOP_IO_DEBUG			FALSE
#define	FE_HSOP_PAYLOAD_DEBUG		TRUE
#define	FE_HSOP_SHOW_CLASS			FALSE
#define	FE_HSOP_WARNINGS			(FE_CODEGEN<=FE_DEBUG)

extern "C"
{

#if FE_HSOP_DISCOVER_DEBUG
__attribute__((constructor))
void houdiniDL_init(void)
{
	Dl_info dlInfo;
	dladdr((void*)houdiniDL_init,&dlInfo);
	feLog("houdiniDL_init \"%s\"\n",dlInfo.dli_fname);

	char* path=getenv("LD_LIBRARY_PATH");
	feLog("houdiniDL_init LD_LIBRARY_PATH \"%s\"\n",path);

	path=getenv("HOUDINI_PATH");
	feLog("houdiniDL_init HOUDINI_PATH \"%s\"\n",path);

	path=getenv("HOUDINI_DSO_PATH");
	feLog("houdiniDL_init HOUDINI_DSO_PATH \"%s\"\n",path);

	//* NOTE changes to LD_LIBRARY_PATH are ignored
//	const int overwrite=true;
//	int result=setenv("LD_LIBRARY_PATH",
//			"/home/symhost/sym/proj/fe/main/lib/x86_64_linux_debug",overwrite);
}

__attribute__((destructor))
void houdiniDL_fini(void)
{
	feLog("houdiniDL_fini\n");
}

static void houdiniExit(void *)
{
	feLog("houdiniExit\n");
}
#endif

//* discovery points for Houdini
FE_DL_EXPORT void newSopOperator(OP_OperatorTable *pTable)
{
#if FE_HSOP_DISCOVER_DEBUG
	feLog("newSopOperator\n");

	UT_Exit::addExitCallback(houdiniExit);
#endif

	fe::HoudiniSOP::discoverOperators(pTable);

#if FE_HSOP_DISCOVER_DEBUG
	feLog("newSopOperator done\n");
#endif
}

}

namespace fe
{
namespace ext
{

static PRM_Name	gs_sopNames[] = {
	PRM_Name("fe_component", "FE Component"),
	PRM_Name("reload", "Reload")
};

static PRM_Template gs_houdiniTemplateList[] =
{
	PRM_Template(PRM_STRING, 1, &gs_sopNames[0], 0),
	PRM_Template(PRM_CALLBACK, 1, &gs_sopNames[1], 0, 0, 0,
			&HoudiniSOP::reloadCallback),
	PRM_Template()
};

#if FE_HOUDINI_MAJOR >= 13
static PRM_SpareData gs_houdiniRampAscending =
		PRM_SpareData(PRM_SpareArgs()
		<< PRM_SpareToken("rampfloatdefault",
		"1pos ( 0 ) 1value ( 0 ) 1interp ( catmull-rom ) "
		"2pos ( 1 ) 2value ( 1 ) 2interp ( catmull-rom )"));
static PRM_SpareData gs_houdiniRampDescending =
		PRM_SpareData(PRM_SpareArgs()
		<< PRM_SpareToken("rampfloatdefault",
		"1pos ( 0 ) 1value ( 1 ) 1interp ( catmull-rom ) "
		"2pos ( 1 ) 2value ( 0 ) 2interp ( catmull-rom )"));
static PRM_SpareData gs_houdiniRampZero =
		PRM_SpareData(PRM_SpareArgs()
		<< PRM_SpareToken("rampfloatdefault",
		"1pos ( 0 ) 1value ( 0 ) 1interp ( catmull-rom ) "
		"2pos ( 1 ) 2value ( 0 ) 2interp ( catmull-rom )"));
static PRM_SpareData gs_houdiniRampOne =
		PRM_SpareData(PRM_SpareArgs()
		<< PRM_SpareToken("rampfloatdefault",
		"1pos ( 0 ) 1value ( 1 ) 1interp ( catmull-rom ) "
		"2pos ( 1 ) 2value ( 1 ) 2interp ( catmull-rom )"));
#endif

int HoudiniSOP::reloadCallback(void *pData,int index, float time,
	const PRM_Template* pTemplate)
{
	feLog("HoudiniSOP::reloadCallback\n");

	HoudiniSOP* pHoudiniSOP = (HoudiniSOP*)pData;
	pHoudiniSOP->m_lastName="";

	feLog("HoudiniSOP::reloadCallback done\n");

	return 1;
}

int HoudiniSOP::discoverOperators(OP_OperatorTable *pTable)
{
#if FE_HSOP_DISCOVER_DEBUG
	feLog("HoudiniSOP:discoverOperators\n");
#endif

#if FALSE
	//* show FE version as global display variable
	const String option_label="FE "+System::buildDate();
	GR_OptionTable *pGrOptionTable=GRgetOptionTable();
	FEASSERT(pGrOptionTable);
	GR_UserOption* pGrUserOption=
			pGrOptionTable->createOption("generic","fe_version");
	FEASSERT(pGrUserOption);
	pGrUserOption->setLabel(option_label.c_str());
#endif

	HoudiniContext houdiniContext;

#if FE_HSOP_DISCOVER_DEBUG
	feLog("HoudiniSOP:discoverOperators master=%p\n",
		houdiniContext.master().raw());
#endif

	sp<Registry> spRegistry=houdiniContext.master()->registry();
	const BWORD firstLoad=houdiniContext.loadLibraries();

	int count=0;

	const char* exposure=getenv("FE_EXPOSURE");
	const BWORD showNoHoudini=exposure && exposure==String("all");
	const BWORD showPrototype=showNoHoudini ||
			(exposure && exposure==String("prototype"));

	Array<String> available;
	spRegistry->listAvailable("OperatorSurfaceI",available);
	U32 candidates=available.size();
	for(U32 index=0;index<candidates;index++)
	{
#if FE_HSOP_DISCOVER_DEBUG
		feLog("checking %d/%d \"%s\"\n",
				index,candidates,available[index].c_str());
#endif

		BWORD hideOperator=FALSE;

		//* ignore operators tagged as prototype
		if(!showPrototype && available[index].dotMatch("*.*.*.prototype"))
		{
#if FE_CODEGEN<=FE_DEBUG
			feLog("HoudiniSOP:discoverOperators"
					" hiding prototype operator:\n"
					"  %s\n",available[index].c_str());
#endif
			hideOperator=TRUE;
		}

		//* ignore operators tagged as nohoudini
		if(!showNoHoudini && available[index].dotMatch("*.*.*.nohoudini"))
		{
#if FE_CODEGEN<=FE_DEBUG
			feLog("HoudiniSOP:discoverOperators"
					" hiding houdini-excluded operator:\n"
					"  %s\n",available[index].c_str());
#endif

			continue;
//			hideOperator=TRUE;
		}

		sp<OperatorSurfaceI> spOperatorSurfaceI=
				spRegistry->create(available[index]);
		if(!spOperatorSurfaceI.isValid())
		{
			feLog("HoudiniSOP:discoverOperators"
					" Component is not an OperatorSurfaceI \"%s\"\n",
					available[index].c_str());
			continue;
		}

		String feName=available[index];
		char* buffer1=strchr(const_cast<char*>(available[index].c_str()),'.');
		if(buffer1)
		{
			buffer1++;
			const char* buffer2=strchr(buffer1,'.');
			if(buffer2>buffer1)
			{
				feName.sPrintf("%.*s",int(buffer2-buffer1),buffer1);
			}
			else
			{
				feName=buffer1;
			}
		}
		String shortName=feName;
		if(!strncmp(feName.c_str(),"Operator",8))
		{
			shortName=feName.c_str()+8;
		}

#if FE_HSOP_DISCOVER_DEBUG
		feLog("HoudiniSOP::discoverOperators feName \"%s\" shortName \"%s\"\n",
				feName.c_str(), shortName.c_str());
#endif

		Array<String> keys;

		sp<Catalog> spCatalog=spOperatorSurfaceI;
		if(!spCatalog.isValid())
		{
#if FE_CODEGEN<=FE_DEBUG
			feLog("HoudiniSOP:discoverOperators"
					" Component is not a Catalog \"%s\"\n",
					available[index].c_str());
#endif
		}
		else
		{
			spCatalog->catalogKeys(keys);
		}

		const U32 number=keys.size();

#if FE_HSOP_DISCOVER_DEBUG
		feLog("HoudiniSOP::discoverOperators keys %d\n",number);
#endif

		Array<String>	pageNames;
		Array<U32>	pageCounts;
		U32 pages=0;
		U32 others=0;
		U32 hiders=0;
		for(U32 m=0;m<number;m++)
		{
			if(keys[m]=="label" || keys[m]=="icon" || keys[m]=="url" ||
					keys[m]=="precook" || keys[m]=="summary")
			{
				spCatalog->catalog<bool>(keys[m],"internal")=true;
			}
			if(spCatalog->catalogOrDefault<bool>(keys[m],"internal",false))
			{
				continue;
			}
			if(spCatalog->catalogOrDefault<bool>(keys[m],"hidden",false))
			{
				hiders++;
				continue;
			}
			Instance instance;
			if(!spCatalog->catalogLookup(keys[m],instance))
			{
				continue;
			}

			if(instance.is< sp<Component> >() &&
					spCatalog->catalogOrDefault<String>(keys[m],
					"implementation","")!="RampI")
			{
				continue;
			}

			if(instance.is< Array< sp<Component> > >())
			{
				continue;
			}

			const String pageName=
					spCatalog->catalogOrDefault<String>(keys[m],"page","");
			if(pageName.empty())
			{
				others++;
				continue;
			}
			U32 n=0;
			for(;n<pages;n++)
			{
				if(pageName==pageNames[n])
				{
					pageCounts[n]++;
					break;
				}
			}
			if(n==pages)
			{
				pageNames.push_back(pageName);
				pageCounts.push_back(1);
				pages++;
			}
		}
		if(pages&&others)
		{
			pageNames.push_back("");
			pageCounts.push_back(others);
			pages++;
		}
		if(pages)
		{
			pageNames.push_back("hidden");
			pageCounts.push_back(hiders);
			pages++;
		}

		const BWORD showClass=FE_HSOP_SHOW_CLASS;
		const U32 given=(pages!=0) + (showClass? 2: 1);

		PRM_Template* pTemplateList=new PRM_Template[given+number+1];
		pTemplateList[0]=gs_houdiniTemplateList[0];

		if(showClass)
		{
			pTemplateList[1]=gs_houdiniTemplateList[1];
		}
		else
		{
			pTemplateList[0].setInvisible(true);
		}

		String parsing=available[index];
		String interface=parsing.parse("",".");
		String implementation=parsing.parse("",".");
		String componentName=interface+"."+implementation;
#if FE_HSOP_DISCOVER_DEBUG
		String provider=parsing.parse("",".");
		feLog("componentName \"%s\" provider \"%s\"\n",
				componentName.c_str(),provider.c_str());
#endif
		pTemplateList[0].setStringDefault(0,componentName.c_str());

		if(pages)
		{
			PRM_Name* pPagerName=new PRM_Name("pager");
			PRM_Default* pagerList=new PRM_Default[pages-1];
			for(U32 n=0;n<pages-1;n++)
			{
				pagerList[n]=PRM_Default(pageCounts[n],
						strdup((others && n==pages-2)?
						"Other": pageNames[n].c_str()));
			}

			pTemplateList[given-1]=PRM_Template(PRM_SWITCHER,
						pages-1,
						pPagerName,
						pagerList);
		}

		PRM_Name* pNameList=new PRM_Name[number];

		char** inputlabels=NULL;
		unsigned minSources=0;
		unsigned maxSources=0;
		U32 templateIndex=given;

		const U32 passes=pages? pages: 1;

		for(U32 pass=0;pass<passes;pass++)
		{
#if FE_HSOP_DISCOVER_DEBUG
			if(pages)
			{
				feLog("HoudiniSOP::discoverOperators processing page \"%s\"\n",
						pageNames[pass].c_str());
			}
#endif
			for(U32 m=0;m<number;m++)
			{
				const BWORD internal=spCatalog->catalogOrDefault<bool>(
						keys[m],"internal",false);
				if(internal)
				{
					continue;
				}

				const String pageName=spCatalog->catalogOrDefault<String>(
						keys[m],"page","");

				const BWORD hidden=spCatalog->catalogOrDefault<bool>(
						keys[m],"hidden",false);

#if FE_HSOP_DISCOVER_DEBUG
				feLog("HoudiniSOP::discoverOperators"
						" key %d: \"%s\" page \"%s\"%s\n",
						m,keys[m].c_str(),pageName.c_str(),
						hidden? " (hidden)": "");
#endif

				Instance instance;
				bool success=spCatalog->catalogLookup(keys[m],instance);
				if(!success)
				{
					feLog("HoudiniSOP::discoverOperators"
							" invalid Instance %d: \"%s\"\n",
							m,keys[m].c_str());
					continue;
				}

				if(hidden)
				{
					if(pages && pass!=passes-1)
					{
#if FE_HSOP_DISCOVER_DEBUG
						feLog("HoudiniSOP::discoverOperators"
								" key \"%s\" hidden (defer to last page)\n",
								keys[m].c_str());
#endif
						continue;
					}
				}
				else if(pages && pageName!=pageNames[pass] &&
						(pass!=passes-2 ||
						!(instance.is< sp<Component> >() &&
						spCatalog->catalogOrDefault<String>(keys[m],
						"implementation","")!="RampI")))
				{
#if FE_HSOP_DISCOVER_DEBUG
				feLog("HoudiniSOP::discoverOperators"
						" key \"%s\" not on page \"%s\"\n",
						keys[m].c_str(),pageNames[pass].c_str());
#endif
					continue;
				}

				//* leaks?
				char* keyname=strdup(keys[m].c_str());
				const char* cleanname=
						strdup(keys[m].substitute(" ","_").c_str());
				const char* labelName=
						strdup(spCatalog->catalogOrDefault<String>(
						keyname,"label",keyname).c_str());

				const BWORD joined=spCatalog->catalogOrDefault<bool>(
						keyname,"joined",false);
				const BWORD slider=spCatalog->catalogOrDefault<bool>(
						keyname,"slider",true);
				const String hintText=
						spCatalog->catalogOrDefault<String>(keyname,"hint","");
				const String suggest=spCatalog->catalogOrDefault<String>(
						keyname,"suggest","");

				pNameList[m]=PRM_Name(cleanname,labelName);

				if(instance.is<String>())
				{
					char* value=strdup(instance.cast<String>().c_str());

					if(!spCatalog->catalogOrDefault<String>(
							keyname,"choice:0","").empty())
					{
						U32 choiceCount=0;
						while(TRUE)
						{
							String keyChoice;
							keyChoice.sPrintf("choice:%d",choiceCount);
							if(spCatalog->catalogOrDefault<String>(
									keyname,keyChoice,"").empty())
							{
								break;
							}
							choiceCount++;
						}
						PRM_Name* pNameChoices=new PRM_Name[choiceCount+1];
						for(U32 choice=0;choice<choiceCount;choice++)
						{
							String keyChoice;
							keyChoice.sPrintf("choice:%d",choice);
							const String nameChoice=
									spCatalog->catalogOrDefault<String>(
									keyname,keyChoice,"");
							keyChoice.sPrintf("label:%d",choice);
							const String labelString=
									spCatalog->catalogOrDefault<String>(
									keyname,keyChoice,nameChoice);
							const char* cleanChoice=strdup(
									nameChoice.substitute(" ","_").c_str());
							const char* labelChoice=strdup(
									labelString.c_str());
#if FE_HSOP_DISCOVER_DEBUG
							feLog("  choice %d/%d \"%s\" \"%s\"\n",
									choice,choiceCount,cleanChoice,labelChoice);
#endif
							pNameChoices[choice]=
									PRM_Name(cleanChoice,labelChoice);
						}
						pNameChoices[choiceCount]=PRM_Name(0);

						PRM_ChoiceList* pStringMenu=new PRM_ChoiceList(
								PRM_CHOICELIST_SINGLE, pNameChoices);

						pTemplateList[templateIndex]=
								PRM_Template(PRM_STRING_E,1,&pNameList[m],NULL,
								pStringMenu,NULL,0,NULL,1,hintText.c_str());
					}
					else if(suggest=="word")
					{
						//* [A-Za-z_0-9]
						pTemplateList[templateIndex]=
								PRM_Template(PRM_ALPHASTRING,1,
								&pNameList[m],NULL,
								NULL,NULL,0,NULL,1,hintText.c_str());
					}
					else if(suggest=="filename")
					{
						pTemplateList[templateIndex]=
								PRM_Template(PRM_FILE_E,1,&pNameList[m],NULL,
								NULL,NULL,0,NULL,1,hintText.c_str());
					}
#if FE_HOUDINI_12_5_PLUS
					else if(suggest=="multiline")
					{
						pTemplateList[templateIndex]=
								PRM_Template(PRM_STRING_E,1,&pNameList[m],NULL,
								NULL,NULL,0,&PRM_SpareData::stringEditor,
								1,hintText.c_str());
					}
#endif
					else
					{
						pTemplateList[templateIndex]=
								PRM_Template(PRM_STRING_E,1,&pNameList[m],NULL,
								NULL,NULL,0,NULL,1,hintText.c_str());
					}

					pTemplateList[templateIndex].setStringDefault(0,value);
					templateIndex++;
				}
				else if(instance.is<bool>())
				{
					bool value=instance.cast<bool>();

					pTemplateList[templateIndex]=
							PRM_Template(PRM_TOGGLE_J,1,&pNameList[m],
							NULL,NULL,NULL,0,NULL,1,hintText.c_str());
					pTemplateList[templateIndex].setOrdinalDefault(0,value);
					templateIndex++;
				}
				else if(instance.is<Real>())
				{
					Real value=instance.cast<Real>();

					//* Unfortunately, it appears that we can't set the
					//*	UI limits separate from the PRM limits.

					Real min=spCatalog->catalogOrDefault<Real>(
							keyname,"min",0.0);
					Real max=spCatalog->catalogOrDefault<Real>(
							keyname,"max",1.0);
					Real low=spCatalog->catalogOrDefault<Real>(
							keyname,"low",min);
					Real high=spCatalog->catalogOrDefault<Real>(
							keyname,"high",max);

					PRM_RangeFlag prmRangeFlagMin=
							(low==min)? PRM_RANGE_RESTRICTED: PRM_RANGE_UI;
					PRM_RangeFlag prmRangeFlagMax=
							(high==max)? PRM_RANGE_RESTRICTED: PRM_RANGE_UI;

					PRM_Range* pPrmRange=new PRM_Range(prmRangeFlagMin,low,
							prmRangeFlagMax,high);
					pTemplateList[templateIndex]=
							PRM_Template(slider? PRM_FLT_J: PRM_SPINNER,
							1,&pNameList[m],
							NULL,NULL,pPrmRange,0,NULL,1,hintText.c_str());
					pTemplateList[templateIndex].setFloatDefault(0,value);
					templateIndex++;
				}
				else if(instance.is<I32>())
				{
					I32 value=instance.cast<I32>();

					I32 min=spCatalog->catalogOrDefault<I32>(keyname,"min",0);
					I32 max=spCatalog->catalogOrDefault<I32>(keyname,"max",10);
					I32 low=spCatalog->catalogOrDefault<I32>(keyname,"low",min);
					I32 high=spCatalog->catalogOrDefault<I32>(
							keyname,"high",max);

					PRM_RangeFlag prmRangeFlagMin=
							(low==min)? PRM_RANGE_RESTRICTED: PRM_RANGE_UI;
					PRM_RangeFlag prmRangeFlagMax=
							(high==max)? PRM_RANGE_RESTRICTED: PRM_RANGE_UI;

					PRM_Range* pPrmRange=new PRM_Range(prmRangeFlagMin,low,
							prmRangeFlagMax,high);
					if(suggest=="button")
					{
						PRM_Callback* pCallback=
								new PRM_Callback(buttonCallback);

						pTemplateList[templateIndex]=
								PRM_Template(PRM_CALLBACK,1,&pNameList[m],
								NULL,NULL,pPrmRange,*pCallback,NULL,1,
								hintText.c_str());
					}
					else
					{
						pTemplateList[templateIndex]=
								PRM_Template(slider? PRM_INT_J: PRM_INT_SPINNER,
								1,&pNameList[m],
								NULL,NULL,pPrmRange,0,NULL,1,
								hintText.c_str());
					}
					pTemplateList[templateIndex].setOrdinalDefault(0,value);
					templateIndex++;
				}
				else if(instance.is<SpatialVector>())
				{
					SpatialVector& value=instance.cast<SpatialVector>();

					//* using '_J' vs '_E' means it can be animated
					//* omitting '_J' seems to the same
					//* SPINNER versions seem to remove slider
					//* see PRM_Type.h: XYZ RGB RGBA UVW

					pTemplateList[templateIndex]=
							PRM_Template(
							suggest=="color"? PRM_RGB_J: PRM_XYZ_J,
							3,&pNameList[m],
							NULL,NULL,NULL,0,NULL,1,hintText.c_str());
					pTemplateList[templateIndex].setFloatDefault(0,value[0]);
					pTemplateList[templateIndex].setFloatDefault(1,value[1]);
					pTemplateList[templateIndex].setFloatDefault(2,value[2]);
					templateIndex++;
				}
				else if(instance.is<Vector4>())
				{
					Vector4& value=instance.cast<Vector4>();

					pTemplateList[templateIndex]=
							PRM_Template(PRM_FLT_J,4,&pNameList[m],
							NULL,NULL,NULL,0,NULL,1,hintText.c_str());
					pTemplateList[templateIndex].setFloatDefault(0,value[0]);
					pTemplateList[templateIndex].setFloatDefault(1,value[1]);
					pTemplateList[templateIndex].setFloatDefault(2,value[2]);
					pTemplateList[templateIndex].setFloatDefault(3,value[3]);
					templateIndex++;
				}
				else if(instance.is< sp<Component> >())
				{
					const String io=spCatalog->catalogOrDefault<String>(
							keyname,"IO","input");
					if(io!="input")
					{
						continue;
					}

					const String implementation=
							spCatalog->catalogOrDefault<String>(keyname,
							"implementation",
							"SurfaceAccessibleI.SurfaceAccessibleHoudini");

					if(implementation=="SurfaceAccessibleI" ||
							implementation==
							"SurfaceAccessibleI.SurfaceAccessibleHoudini")
					{
						const bool optional=spCatalog->catalogOrDefault<bool>(
								keyname,"optional",false);

						if(!optional)
						{
							minSources++;
						}
						maxSources++;

						char** newlabels=new char*[maxSources+1];
						for(U32 m=0;m<maxSources-1;m++)
						{
							newlabels[m]=inputlabels[m];
						}
						newlabels[maxSources-1]=strdup(labelName);
						newlabels[maxSources]=NULL;

						delete[] inputlabels;
						inputlabels=newlabels;
					}
					else if(implementation=="DrawI")
					{
						addBrushName(feName);
					}
					else if(implementation=="RampI")
					{
#if FE_HOUDINI_MAJOR >= 13
						const String defaultRamp=
								spCatalog->catalogOrDefault<String>(
								keyname,"default","");

						pTemplateList[templateIndex]=
								PRM_Template(PRM_MULTITYPE_RAMP_FLT,NULL,1,
								&pNameList[m],PRMtwoDefaults,0,
								((defaultRamp=="ascending")?
								&gs_houdiniRampAscending:
								((defaultRamp=="descending")?
								&gs_houdiniRampDescending:
								((defaultRamp=="one")?
								&gs_houdiniRampOne:
								&gs_houdiniRampZero))));
#else
						pTemplateList[templateIndex]=
								PRM_Template(PRM_MULTITYPE_RAMP_FLT,NULL,1,
								&pNameList[m],PRMtwoDefaults);
#endif
						templateIndex++;
					}
				}
				if(hidden)
				{
					pTemplateList[templateIndex-1].setInvisible(true);
				}
				if(joined)
				{
					pTemplateList[templateIndex-1].setJoinNext(true);
				}

				if(!suggest.empty())
				{
					if(suggest=="pointGroups")
					{
						pTemplateList[templateIndex-1].setChoiceListPtr(
								&SOP_Node::pointNamedGroupMenu);
					}
					if(suggest=="primitiveGroups")
					{
						pTemplateList[templateIndex-1].setChoiceListPtr(
								&SOP_Node::primNamedGroupMenu);
					}
				}

				free(keyname);
			}
		}

		pTemplateList[templateIndex]=PRM_Template();

		String iconString=spCatalog->catalogOrDefault<String>("icon","FE");
		String prefix;

		//* HOUDINI_UI_ICON_PATH does not work consistantly, so go online
		if(iconString=="FE" || iconString.match("FE_.*"))
		{
			iconString=FE_HOUDINI_DEFAULT_ICON_ROOT+iconString+".svg";
			prefix="FE ";
		}

		const String labelName=
				spCatalog->catalogOrDefault<String>("label",prefix+shortName);

		const String menuName=
				labelName.substitute("SOP_","").substitute("_"," ");

		const char* name=strdup(feName.c_str());
		const char* english=strdup(menuName.c_str());
		const char* iconname=strdup(iconString.c_str());

		CH_LocalVariable* variables=NULL;
		const unsigned flags=0x0;

#if FE_HSOP_DISCOVER_DEBUG
		feLog("HoudiniSOP::discoverOperators %s sources %d-%d\n",
				name,minSources,maxSources);
#endif

		OP_Operator* pOp=new OpOperator(
				name,
				english,
				fe::HoudiniSOP::create,
				pTemplateList,
				minSources,
				maxSources,
				variables,
				flags,
				(const char**)inputlabels,
				spCatalog);

		const BWORD threadSafe=Mutex::supported();

		pOp->setIconName(iconname);
		pOp->setIsThreadSafe(threadSafe);
		pTable->addOperator(pOp);

		if(hideOperator)
		{
			pTable->addOpHidden(name);
		}

//		LEAK or released by Houdini?
//		delete[] pNameList;
//		delete[] pTemplateList;

		count++;
	}

	if(firstLoad)
	{
		const String verbose=System::getVerbose();
		if(verbose!="none")
		{
			feLog("added %d SOP operators\n", count);
		}
	}

#if FE_HSOP_DISCOVER_DEBUG
	feLog("HoudiniSOP:discoverOperators done count=%d\n",count);
#endif

	return count;
}

HoudiniSOP::OpOperator::OpOperator(const char *name,const char *english,
		OP_Constructor construct,PRM_Template *templates,
		unsigned min_sources,unsigned max_sources,CH_LocalVariable *variables,
		unsigned flags,const char **inputlabels,sp<Catalog> a_spCatalog):
	OP_Operator(name,english,construct,templates,
		min_sources,max_sources,variables,flags,inputlabels)
{
	m_helpUrl=a_spCatalog->catalogOrDefault<String>("url","");
}

//* NOTE getOpHelpURL checked before getHDKHelp
bool HoudiniSOP::OpOperator::getOpHelpURL(UT_String& a_rUtString)
{
//	feLog("HoudiniSOP::OpOperator::getOpHelpURL %s m_helpUrl \"%s\"\n",
//			(const char*)getName(),m_helpUrl.c_str());

	if(m_helpUrl.empty())
	{
		m_helpUrl=String(FE_HOUDINI_DEFAULT_URL_ROOT)+(const char*)getName();

//		feLog("HoudiniSOP::OpOperator::getOpHelpURL -> \"%s\"\n",
//				m_helpUrl.c_str());
	}

	a_rUtString.harden(m_helpUrl.c_str());
	return true;
}

HoudiniSOP::UndoCallback::UndoCallback(
	HoudiniSOP* a_pHoudiniSOP,String a_change,
	sp<Catalog> a_spNewCatalog,
	sp<Counted> a_spOldCounted,
	sp<Counted> a_spNewCounted):
	m_pHoudiniSOP(a_pHoudiniSOP),
	m_change(a_change),
	m_spNewCatalog(a_spNewCatalog),
	m_spOldCounted(a_spOldCounted),
	m_spNewCounted(a_spNewCounted)
{
#if	FE_HSOP_UNDO_DEBUG
	feLog("HoudiniSOP::UndoCallback::UndoCallback %p \"%s\"\n",
			m_pHoudiniSOP,m_change.c_str());
#endif
}

bool HoudiniSOP::UndoCallback::isValid(void)
{
#if	FE_HSOP_UNDO_DEBUG
//	feLog("HoudiniSOP::UndoCallback::isValid %d \"%s\"\n",
//			m_spOperatorSurfaceI.isValid(),m_change.c_str());
#endif

	return m_pHoudiniSOP!=NULL;
}

void HoudiniSOP::UndoCallback::undoOrRedo(BWORD a_redo)
{
#if	FE_HSOP_UNDO_DEBUG
	feLog("HoudiniSOP::UndoCallback::undoOrRedo %p %s \"%s\"\n",
			m_pHoudiniSOP,a_redo? "redo": "undo",m_change.c_str());
#endif

	if(!m_pHoudiniSOP)
	{
		return;
	}

	sp<OperatorSurfaceI> spOperatorSurfaceI=m_pHoudiniSOP->operatorSurface();
	if(spOperatorSurfaceI.isNull())
	{
		return;
	}

	sp<Catalog> spCatalog=spOperatorSurfaceI;

	if(a_redo)
	{
//		if(spCatalog.isValid())
//		{
//			spCatalog->catalogOverlay(m_spNewCatalog);
//		}

		spOperatorSurfaceI->redo(m_change,m_spNewCounted);
	}
	else
	{
		spOperatorSurfaceI->undo(m_change,m_spOldCounted);
	}

	//* TODO
	const Real time=0.0;

	m_pHoudiniSOP->updateCatalog(time,FALSE,TRUE);

	const BWORD aggressive=TRUE;
	m_pHoudiniSOP->dirty(aggressive);
}

//* static
OP_Node* HoudiniSOP::create(OP_Network *pNetwork,const char *name,
	OP_Operator *pOp)
{
	OP_Node* pOpNode=new HoudiniSOP(pNetwork,name,pOp);

	return pOpNode;
}

HoudiniSOP::HoudiniSOP(OP_Network *pNetwork,const char *name,OP_Operator *pOp):
	SOP_Node(pNetwork,name,pOp),
	m_pOpOperator(pOp),
	m_pOpContext(NULL),
	m_frame(-1.0),
	m_cooking(FALSE),
	m_inputsLocked(FALSE),
	m_guideEmpty(TRUE),
	m_temporaryTemplate(FALSE),
	m_pInterrupt(NULL)
{
	mySopFlags.setNeedGuide1(1);

#if FE_HSOP_COOK_DEBUG
	feLog("HoudiniSOP::HoudiniSOP master=%p\n",m_houdiniContext.master().raw());
#endif

	sp<Registry> spRegistry=m_houdiniContext.master()->registry();
	m_houdiniContext.loadLibraries();

	m_spDrawOutput=spRegistry->create("DrawI.HoudiniDraw");
	m_spDrawOutput->setName("DrawOutput");

	m_spDrawGuideChain=spRegistry->create("DrawI.HoudiniDraw");
	if(m_spDrawGuideChain.isValid())
	{
		m_spDrawGuideChain->setName("DrawGuideChain");
		sp<ViewI> spView=m_spDrawGuideChain->view();
		if(spView.isValid())
		{
			sp<CameraDisplay> spCameraDisplay=spView->cameraDisplay();
			if(spCameraDisplay.isValid())
			{
//~				spCameraDisplay->setProjection(CameraI::e_perspective);
			}
			else
			{
				feLog("HoudiniSOP::HoudiniSOP CameraDisplay is NULL\n");
			}
		}
		else
		{
			feLog("HoudiniSOP::HoudiniSOP"
					" \"%s\" guide drawing cache has no view\n",name);
		}
	}
	else
	{
		feLog("HoudiniSOP::HoudiniSOP"
				" \"%s\" failed to create guide drawing cache\n",name);
	}

	setupScope(m_houdiniContext.scope());

#if FE_COUNTED_TRACK
	Counted::registerRegion(this,sizeof(HoudiniSOP),"HoudiniSOP");
#endif

	UT_String operatorParam;
	evalString(operatorParam, 0, 0, 0.0);
	String operatorName((char*)operatorParam);

#if FE_HSOP_COOK_DEBUG
	feLog("HoudiniSOP::HoudiniSOP \"%s\"\n",operatorName.c_str());
#endif
}

HoudiniSOP::~HoudiniSOP(void)
{
//	feLog("~HoudiniSOP\n");

	m_pOpOperator=NULL;

	FEASSERT(!m_pInterrupt);
	if(m_pInterrupt)
	{
		delete m_pInterrupt;
	}

	clearReferences();

#if FE_COUNTED_TRACK
	Counted::deregisterRegion(this);
#endif
}

unsigned int HoudiniSOP::disableParms(void)
{
	const Real time=CHgetEvalTime();

	//* this can validly happen if node has never been cooked
	if(!m_spOperatorSurfaceI.isValid())
	{
		UT_String operatorParam;
		evalString(operatorParam, 0, 0, 0.0);
		String operatorName((char*)operatorParam);

//		feLog("HoudiniSOP::disableParms creating operator \"%s\"\n",
//				operatorName.c_str());

		if(operatorName.empty())
		{
			return 0;
		}

		createOperator(operatorName,time);
	}

	sp<Catalog> spCatalog=m_spOperatorSurfaceI;
	if(!spCatalog.isValid())
	{
		UT_String operatorParam;
		evalString(operatorParam, 0, 0, 0.0);
		String operatorName((char*)operatorParam);

		feLog("HoudiniSOP:disableParms \"%s\" operator is not a Catalog\n",
				operatorName.c_str());
		return 0;
	}

	unsigned int changed=0;

	PRM_ParmList* pPrmParmList=getParmList();	// as OP_Parameters
	U32 parms=pPrmParmList->getEntries();
	for(U32 parm=0;parm<parms;parm++)
	{
		PRM_Parm* pPrmParm=pPrmParmList->getParmPtr(parm);
//		const PRM_Type& rPrmType=pPrmParm->getType();

		const char* token=pPrmParm->getToken();

		//HACK
		String catalogName=String(token).substitute("_"," ");

		const String io=spCatalog->catalogOrDefault<String>(
				catalogName,"IO","input");
		if(io=="output")
		{
			changed+=enableParm(token,false);
			continue;
		}

		const String enabler=spCatalog->catalogOrDefault<String>(
				catalogName,"enabler","");
		if(enabler.empty())
		{
			continue;
		}

		//* TODO generalize and share with Maya

		//* TODO so many more operations
		bool enabled=true;
		String buffer=enabler;
		String condition;
		while(enabled && !(condition=buffer.parse("\"","&")).empty())
		{
			String chopped;
			while((chopped=condition.prechop(" "))!=condition)
			{
				condition=chopped;
			}
			while((chopped=condition.chop(" "))!=condition)
			{
				condition=chopped;
			}
			String key=condition.parse("\"","=");
			String desired=condition.parse("\"","=");

			char* cleanname=strdup(key.substitute(" ","_").c_str());

			if(spCatalog->catalogInstance(key).is<bool>() &&
					!evalInt(cleanname,0,time))
			{
				enabled=false;
			}

			if(spCatalog->catalogInstance(key).is<String>())
			{
				UT_String utString;
				evalString(utString,cleanname,0,time);
				const String evaluated=(char*)utString;

				if(!desired.empty())
				{
					if(evaluated!=desired)
					{
						enabled=false;
					}
				}
				else if(evaluated.empty())
				{
					enabled=false;
				}
			}

			free(cleanname);
		}

		changed+=enableParm(token,enabled);
	}

	return changed;
}

void HoudiniSOP::updateCatalog(Real time,BWORD a_doInput,BWORD a_force)
{
	if(!m_spOperatorSurfaceI.isValid())
	{
		feLog("HoudiniSOP::updateCatalog invalid m_spOperatorSurfaceI\n");
		return;
	}

	sp<Catalog> spCatalog=m_spOperatorSurfaceI;
	if(!spCatalog.isValid())
	{
		feLog("HoudiniSOP:updateCatalog operator is not a Catalog\n");
		return;
	}

	sp<Registry> spRegistry=m_houdiniContext.master()->registry();

	PRM_ParmList* pPrmParmList=getParmList();	// as OP_Parameters
	U32 parms=pPrmParmList->getEntries();
	for(U32 parm=0;parm<parms;parm++)
	{
		PRM_Parm* pPrmParm=pPrmParmList->getParmPtr(parm);
		const PRM_Type& rPrmType=pPrmParm->getType();

		const char* token=pPrmParm->getToken();
		const int size=pPrmParm->getVectorSize();

		//HACK
		String catalogName=String(token).substitute("_"," ");

#if FE_HSOP_CATALOG_DEBUG
		const char* label=pPrmParm->getLabel();

		feLog("HoudiniSOP::updateCatalog \"%s\" \"%s\" %d \"%s\"\n",
				token, label, size, catalogName.c_str());
#endif

#ifdef FE_HOUDINI_USE_GA
		const BWORD hidden=spCatalog->catalogOrDefault<bool>(
				catalogName,"hidden",false);

		pPrmParm->setVisibleState(!hidden);
#else
		//* TODO
#endif

		String io=spCatalog->catalogOrDefault<String>(catalogName,"IO","input");
		if(!a_force && io!="input output" && (io=="input")!=a_doInput)
		{
#if FE_HSOP_CATALOG_DEBUG
			feLog("HoudiniSOP::updateCatalog skip \"%s\"\n",io.c_str());
#endif
			continue;
		}

		if(pPrmParm->isRampType())
		{
			//* TODO missing first update since ramp not created yet

#if FE_HSOP_CATALOG_DEBUG
			feLog("HoudiniSOP::updateCatalog input ramp\n");
#endif

			sp<HoudiniRamp> spRamp=
					spCatalog->catalog< sp<Component> >(catalogName);
			if(spRamp.isNull())
			{
				spRamp=spRegistry->create("RampI.HoudiniRamp");
				spRamp->setName(catalogName);

				spCatalog->catalog< sp<Component> >(catalogName)=spRamp;
			}
			if(spRamp.isValid())
			{
				UT_Ramp& rRamp=spRamp->nativeRamp();
				updateRampFromMultiParm(time,*pPrmParm,rRamp);

				if(!a_doInput)
				{
					//* TODO clear and repopulate ramp
//					updateMultiParmFromRamp(time,rRamp,pPrmParm,false);
				}
			}
		}
		else if(rPrmType.isStringType())
		{
			UT_String utString;
			const int index=0;

			if(a_doInput)
			{
				evalString(utString,pPrmParm,index,time);


				const int thread=UT_Thread::getMyThreadId();

				UT_String utExpression;
				pPrmParm->getExpressionOnly(time,utExpression,0,thread);

				String expression;

				if(utExpression!="\"\"")
				{
					const char* buffer(utExpression);
					const U32 len=strlen(buffer);

					if(len>1 && buffer[0]=='"' && buffer[len-1]=='"')
					{
						UT_String utSubstr;
						utExpression.substr(utSubstr,1,len-2);
						expression=(char*)utSubstr;
					}
					else
					{
						expression=(char*)utExpression;
					}
				}

#if FE_HSOP_CATALOG_DEBUG
				feLog("HoudiniSOP::updateCatalog input string \"%s\""
						" from expression \"%s\" raw \"%s\"\n",
						(char*)utString,expression.c_str(),
						(char*)utExpression);
#endif

				//* NOTE don't really want hscript text
				spCatalog->catalog<String>(catalogName)=
						(char*)utString;

				spCatalog->catalog<String>(catalogName,"eval")=
						(char*)utString;
			}
			else
			{
				const String value=spCatalog->catalog<String>(catalogName);

#if FE_HSOP_CATALOG_DEBUG
				feLog("HoudiniSOP::updateCatalog output string \"%s\"\n",
						value.c_str());
#endif

				utString=value.c_str();

				setString(utString,CH_STRING_LITERAL,parm,index,time);
			}
		}
		else if(rPrmType.isFloatType())
		{
			const PRM_Type::PRM_FloatType& prmFloatType=
					rPrmType.getFloatType();
			if(a_doInput)
			{
				if(prmFloatType==PRM_Type::PRM_FLOAT_INTEGER)
				{
					const int index=0;
					const int value=evalInt(pPrmParm,index,time);

#if FE_HSOP_CATALOG_DEBUG
					feLog("HoudiniSOP::updateCatalog input int %d\n",value);
#endif

					spCatalog->catalog<I32>(catalogName)=value;
				}
				else
				{
					float value[size];
					evalFloats(pPrmParm,value,time);

					switch(size)
					{
						case 1:
#if FE_HSOP_CATALOG_DEBUG
							feLog("HoudiniSOP::updateCatalog"
									" input float %.6G\n",value[0]);
#endif

							spCatalog->catalog<Real>(catalogName)=value[0];
							break;
						case 3:
#if FE_HSOP_CATALOG_DEBUG
							feLog("HoudiniSOP::updateCatalog input float[3]"
									" %.6G %.6G %.6G\n",
									value[0],value[1],value[2]);
#endif

							spCatalog->catalog<SpatialVector>(catalogName)=
									SpatialVector(value);
							break;
						case 4:
#if FE_HSOP_CATALOG_DEBUG
							feLog("HoudiniSOP::updateCatalog input float[4]"
									" %.6G %.6G %.6G %.6G\n",
									value[0],value[1],value[2],value[3]);
#endif

							spCatalog->catalog<Vector4>(catalogName)=
									Vector4(value);
							break;
						default:
							break;
					}
				}
			}
			else
			{
				if(prmFloatType==PRM_Type::PRM_FLOAT_INTEGER)
				{
					const int index=0;
					const int value=spCatalog->catalog<I32>(catalogName);

#if FE_HSOP_CATALOG_DEBUG
					feLog("HoudiniSOP::updateCatalog output int %d\n",value);
#endif

					setInt(parm,index,time,value);
				}
				else
				{
					switch(size)
					{
						case 1:
						{
							const Real value=
									spCatalog->catalog<Real>(catalogName);

#if FE_HSOP_CATALOG_DEBUG
							feLog("HoudiniSOP::updateCatalog"
									" output float %.6G\n",value);
#endif

//							setFloat(parm,0,time,value);
							setFloat(token,0,time,value);

#if FE_HSOP_CATALOG_DEBUG
							float verify[4];
							verify[0]= -1.0;
							verify[1]= -1.0;
							verify[2]= -1.0;
							verify[3]= -1.0;

							evalFloats(pPrmParm,verify,time);
							feLog("HoudiniSOP::updateCatalog"
									" verify float %.6G %.6G %.6G %.6G\n",
									verify[0],verify[1],verify[2],verify[3]);
#endif
						}
							break;
						case 3:
						{
							const SpatialVector value=
									spCatalog->catalog<SpatialVector>(
									catalogName);
#if FE_HSOP_CATALOG_DEBUG
							feLog("HoudiniSOP::updateCatalog"
									" output float[3] %s\n",c_print(value));
#endif
							setFloat(parm,0,time,value[0]);
							setFloat(parm,1,time,value[1]);
							setFloat(parm,2,time,value[2]);

#if FE_HSOP_CATALOG_DEBUG
							float verify[4];
							verify[0]= -1.0;
							verify[1]= -1.0;
							verify[2]= -1.0;
							verify[3]= -1.0;

							evalFloats(pPrmParm,verify,time);
							feLog("HoudiniSOP::updateCatalog"
									" verify float[3] %.6G %.6G %.6G %.6G\n",
									verify[0],verify[1],verify[2],verify[3]);
#endif
						}
							break;
						case 4:
						{
							const Vector4 value=
									spCatalog->catalog<Vector4>(
									catalogName);
#if FE_HSOP_CATALOG_DEBUG
							feLog("HoudiniSOP::updateCatalog"
									" output float[4] %s\n",c_print(value));
#endif
							setFloat(parm,0,time,value[0]);
							setFloat(parm,1,time,value[1]);
							setFloat(parm,2,time,value[2]);
							setFloat(parm,3,time,value[3]);

#if FE_HSOP_CATALOG_DEBUG
							float verify[4];
							verify[0]= -1.0;
							verify[1]= -1.0;
							verify[2]= -1.0;
							verify[3]= -1.0;

							evalFloats(pPrmParm,verify,time);
							feLog("HoudiniSOP::updateCatalog"
									" verify float[4] %.6G %.6G %.6G %.6G\n",
									verify[0],verify[1],verify[2],verify[3]);
#endif
						}
							break;
						default:
							break;
					}
				}
			}
		}
		else if(rPrmType.isOrdinalType())
		{
			if(spCatalog->catalogOrDefault<String>(
						catalogName,"suggest","")=="button")
			{
				//* NOTE incremented in callback
			}
			else
			{
				if(a_doInput)
				{
					const int index=0;
					const int value=evalInt(pPrmParm,index,time);

#if FE_HSOP_CATALOG_DEBUG
					feLog("HoudiniSOP::updateCatalog input ordinal %d\n",value);
#endif

					spCatalog->catalog<bool>(catalogName)=value;
				}
				else
				{
					const int index=0;
					const int value=spCatalog->catalog<bool>(catalogName);
					setInt(parm,index,time,value);
				}
			}
		}
		else
		{
#if FE_HSOP_CATALOG_DEBUG
			feLog("HoudiniSOP::updateCatalog unknown type\n");
#endif
		}
	}

	Array<String> keys;
	spCatalog->catalogKeys(keys);

	const U32 number=keys.size();
	for(U32 m=0;m<number;m++)
	{
		Instance instance;
		bool success=spCatalog->catalogLookup(keys[m],instance);
		if(!success)
		{
#if FE_HSOP_WARNINGS
			feLog("HoudiniSOP::updateCatalog invalid Instance"
					" on key %d \"%s\"\n",m,keys[m].c_str());
#endif
			continue;
		}

		char* cleanname=strdup(keys[m].substitute(" ","_").c_str());
		char* keyname=strdup(keys[m].c_str());

		if(instance.is< sp<Component> >())
		{
			sp<Component>& rspComponent=instance.cast< sp<Component> >();

			if(!rspComponent.isValid())
			{
				String implementation=spCatalog->catalogOrDefault<String>(
						keyname,"implementation",
						"SurfaceAccessibleI.SurfaceAccessibleHoudini");
				if(!implementation.empty())
				{
					if(implementation=="SurfaceAccessibleI")
					{
						implementation=
								"SurfaceAccessibleI.SurfaceAccessibleHoudini";
					}
					if(implementation=="DrawI")
					{
						implementation=(keyname==String("Brush"))?
								"DrawI.DrawCached": "DrawI.HoudiniDraw";
					}
					if(implementation=="RampI")
					{
						//* NOTE should already be created earlier
						implementation="RampI.HoudiniRamp";
					}
					rspComponent=spRegistry->create(implementation);
					rspComponent->setName(keyname);
				}
				sp<DrawI> spDrawI=rspComponent;
				if(spDrawI.isValid())
				{
					if(keyname==String("Brush"))
					{
						m_spDrawBrush=spDrawI;
						m_brushName=keyname;

						m_spDrawBrushOverlay=spRegistry->create(implementation);
						m_spDrawBrushOverlay->setName("DrawBrushOverlay");

						setupBrush(m_houdiniContext.scope());
					}
				}
				sp<SurfaceAccessibleI> spSurfaceAccessibleI=rspComponent;
				if(spSurfaceAccessibleI.isValid())
				{
					String io=spCatalog->catalogOrDefault<String>(
							keyname,"IO","input");
					if(io=="input")
					{
						I32 channel=m_surfaceInputArray.size();
#if FE_HSOP_CATALOG_DEBUG
						feLog("HoudiniSOP::updateCatalog"
								" adding surface input %d \"%s\" \"%s\"\n",
								channel,cleanname,keyname);
#endif
						spCatalog->catalog<I32>(keyname,"channel")=channel;
						m_surfaceInputArray.push_back(spSurfaceAccessibleI);
					}
					else
					{
#if FE_HSOP_CATALOG_DEBUG
						feLog("HoudiniSOP::updateCatalog"
								" adding surface output \"%s\" \"%s\"\n",
								cleanname,keyname);
#endif
						m_nameOutput=keyname;
						m_spSurfaceOutput=spSurfaceAccessibleI;
					}
				}
			}
		}

		free(keyname);
		free(cleanname);
	}

#if FALSE
	//* HACK dynamic test
	if(spCatalog->cataloged("Dynamic") &&
			!spCatalog->catalogOrDefault<bool>("Dynamic","created",false))
	{
		String name;
		name.sPrintf("p%x",m_pOpOperator);

		PRM_Name* pPrmName=new PRM_Name("name",strdup(name.c_str()));
		PRM_Template* pPrmTemplate=new PRM_Template(PRM_STRING_E,1,
				pPrmName,NULL,NULL,NULL,NULL,NULL,1,"hint");

		const PRM_Template* pPrmTemplates=
				getInterfaceParmTemplates();

		const PRM_Template* pTemplate=pPrmTemplates;
		U32 templateIndex=0;
		while(pTemplate->getType()!=PRM_LIST_TERMINATOR)
		{
			templateIndex++;
			pTemplate=&pPrmTemplates[templateIndex];
		}
		const U32 templateCount=templateIndex;
		PRM_Template* pPrmCopies=new PRM_Template[templateCount+2];
		for(templateIndex=0;templateIndex<templateCount;templateIndex++)
		{
			pPrmCopies[templateIndex]=pPrmTemplates[templateIndex];
		}
		pPrmCopies[templateCount]=*pPrmTemplate;
		pPrmCopies[templateCount+1]=PRM_Template();

		m_pOpOperator->changeParmTemplate(pPrmCopies);

		spCatalog->catalog<bool>("Dynamic","created")=true;
	}
#endif
}

void HoudiniSOP::createOperator(String a_operatorName,Real a_time)
{
		sp<Registry> spRegistry=m_houdiniContext.master()->registry();
		m_spOperatorSurfaceI=spRegistry->create(a_operatorName);
//		feLog("HoudiniSOP::createOperator m_spOperatorSurfaceI \"%s\" valid=%d\n",
//				operatorName.c_str(),m_spOperatorSurfaceI.isValid());

		if(m_spOperatorSurfaceI.isValid())
		{
			m_spOperatorSurfaceI->setPlugin(this);
			updateCatalog(a_time,TRUE);

			m_lastName=a_operatorName;
			insertHandler(m_spOperatorSurfaceI);
		}

//		BWORD loaded=
				m_spOperatorSurfaceI->loadState(m_loadedState);
		m_loadedState="";
}

void HoudiniSOP::dirty(BWORD a_aggressive)
{
/*
	feLog("HoudiniSOP::dirty \"%s\" %d\n",(const char*)getName(),a_aggressive);

	feLog("  modeled %d always %d unload %d force %d cooking %d modified %d"
			" template %d display %d render %d expose %d\n",
			flags().getModelled(),
			flags().getAlwaysCook(),
			flags().getUnload(),
			flags().getForceCook(),
			flags().getCooking(),
			flags().getModified(),
			flags().getTemplate(),
			flags().getDisplay(),
			flags().getRender(),
			flags().getExpose());
*/

	if(a_aggressive && !flags().getTemplate())
	{
//		setTemplate(TRUE);
		flags().setTemplate(TRUE),

		m_temporaryTemplate=TRUE;
	}

	forceRecook();

	resignalBrush();
}

void HoudiniSOP::select(void)
{
	setCurrentAndPicked();
}

//* TODO store old operator catalog as well
BWORD HoudiniSOP::chronicle(String a_change,sp<Counted> a_spOldCounted,
	sp<Counted> a_spNewCounted)
{
    UT_AutoUndoBlock undoblock(a_change.c_str(),ANYLEVEL);

	UT_UndoManager* pUndoManager=UTgetUndoManager();
	const BWORD accepting=pUndoManager->willAcceptUndoAddition();

#if	FE_HSOP_UNDO_DEBUG
	feLog("HoudiniSOP::chronicle \"%s\" accepting %d\n",
			a_change.c_str(),accepting);
#endif

	if(accepting)
	{
		//* TODO consider removing Catalog storage
		Array<String> typeList;
		typeList.push_back(typeid(bool).name());
		typeList.push_back(typeid(I32).name());
		typeList.push_back(typeid(Real).name());
		typeList.push_back(typeid(String).name());
		typeList.push_back(typeid(SpatialVector).name());

		sp<Catalog> spCatalog=m_spOperatorSurfaceI;
		sp<Catalog> spDuplicate(new Catalog());
		spDuplicate->catalogOverlay(spCatalog,&typeList);

		pUndoManager->addToUndoBlock(new UndoCallback(
				this,a_change,spDuplicate,a_spOldCounted,a_spNewCounted));
	}

	return accepting;
}

BWORD HoudiniSOP::interrupted(void)
{
//	FEASSERT(m_pInterrupt);

	const BWORD result=
			m_pInterrupt? m_pInterrupt->wasInterrupted(): FALSE;

#if FE_CODEGEN<=FE_DEBUG
	if(result)
	{
		feLogError("HoudiniSOP::interrupted \"%s\"\n",
				(const char*)getName());
	}
#endif

	return result;
}

//* static
int HoudiniSOP::buttonCallback(void* a_pNode,int a_index,fpreal32 a_time,
	const PRM_Template* a_pPrmTemplate)
{
	OP_Node* pOpNode=(OP_Node*)a_pNode;

	const char* token=a_pPrmTemplate->getToken();
	const String catalogName=String(token).substitute("_"," ");

#if FE_HSOP_COOK_DEBUG
	feLog("HoudiniSOP::buttonCallback \"%s\" param \"%s\"\n",
			(const char*)pOpNode->getName(),catalogName.c_str());
#endif

	HoudiniSOP* pHoudiniSOP=dynamic_cast<HoudiniSOP*>(pOpNode);
	if(pHoudiniSOP)
	{
		sp<Catalog> spCatalog=pHoudiniSOP->m_spOperatorSurfaceI;
		if(spCatalog.isValid())
		{
			spCatalog->catalog<I32>(catalogName)++;

			const BWORD aggressive=TRUE;
			pHoudiniSOP->dirty(aggressive);
		}
	}

	//* "if you want the dialog to refresh (ie if you changed any values)"
	const int refresh=1;

	return refresh;
}

BWORD HoudiniSOP::cookInputs(void)
{
	if(!m_pOpContext || lockInputs(*m_pOpContext)>=UT_ERROR_ABORT)
	{
#if FE_HSOP_WARNINGS
		feLog("HoudiniSOP::cookInputs failed to lock inputs on \"%s\"\n",
				(const char*)getName());
#endif
		return FALSE;
	}

	U32 surfaceCount=m_surfaceInputArray.size();
	for(int surfaceIndex=0;surfaceIndex<surfaceCount;surfaceIndex++)
	{
		const GU_Detail* pGdp=inputGeo(surfaceIndex);
		sp<SurfaceAccessibleHoudini> spSurfaceInput=
				m_surfaceInputArray[surfaceIndex];
		spSurfaceInput->setGdp(pGdp);
	}

	m_inputsLocked=TRUE;
	return TRUE;
}

void HoudiniSOP::getDescriptiveName(UT_String &a_rString) const
{
	if(m_spOperatorSurfaceI.isValid())
	{
		sp<Catalog> spCatalog=m_spOperatorSurfaceI;
		if(spCatalog.isValid())
		{
			a_rString=
					spCatalog->catalogOrDefault<String>("summary","").c_str();
			return;
		}
	}

	a_rString="";
}

//* TODO SOP sims should replace SOP_Node::shouldResetGeoToEmpty to return false

OP_ERROR HoudiniSOP::bypassMe(OP_Context &context,int &copied_input)
{
//	feLog("HoudiniSOP::bypassMe\n");
	return SOP_Node::bypassMe(context,copied_input);
}

void HoudiniSOP::opChanged(OP_EventType reason, void *data)
{
//	feLog("HoudiniSOP::opChanged \"%s\"\n",OPeventToString(reason));
	SOP_Node::opChanged(reason, data);
}

OP_ERROR HoudiniSOP::cookMySop(OP_Context &a_rContext)
{
	const double time=a_rContext.getTime();
	const double newFrame=
			OPgetDirector()->getChannelManager()->getSample(time);
	const BWORD frameChanged=(newFrame!=m_frame);

	const Real startFrame=
			OPgetDirector()->getChannelManager()->getGlobalStartFrame();
	const Real endFrame=
			OPgetDirector()->getChannelManager()->getGlobalEndFrame();

	m_frame=newFrame;

	UT_String operatorParam;
	evalString(operatorParam, 0, 0, time);
	String operatorName((char*)operatorParam);

	if(operatorName.empty())
	{
		feLog("HoudiniSOP::cookMySop empty name\n");
		return UT_ERROR_ABORT;
	}

#if FE_HSOP_COOK_DEBUG
	feLog("--------------------------------\n");
	feLog("HoudiniSOP::cookMySop m_frame %f time %f node \"%s\" (\"%s\")\n",
			m_frame,time,(const char*)getName(),operatorName.c_str());
	feLog("  frame range %.6G %.6G\n",startFrame,endFrame);
#endif

	if(m_temporaryTemplate)
	{
#if FE_HSOP_COOK_DEBUG
		feLog("HoudiniSOP::cookMySop reverting temporary template\n");
#endif

//		setTemplate(FALSE);
		flags().setTemplate(FALSE),

		m_temporaryTemplate=FALSE;

		forceRecook();
	}

	m_cooking=TRUE;

	const BWORD resetState=FALSE;//(time==0.0f && m_lastTime>0.0);

	if(m_spOperatorSurfaceI.isValid())
	{
//		feLog("HoudiniSOP::cookMySop component name \"%s\" as \"%s\"\n",
//				m_spOperatorSurfaceI->name().c_str(),m_lastName.c_str());
		if(resetState || !m_lastName.dotMatch(operatorName))
		{
			removeHandler(m_spOperatorSurfaceI);
			m_spOperatorSurfaceI=NULL;
			m_lastName="";
			m_surfaceInputArray.clear();
		}
	}
	if(!m_spOperatorSurfaceI.isValid())
	{
		createOperator(operatorName,time);
	}
	else
	{
		updateCatalog(time,TRUE);
	}

	if(m_spOperatorSurfaceI.isValid())
	{
		m_spOperatorSurfaceI->setName((const char *)getName());

		sp<Catalog> spCatalog=m_spOperatorSurfaceI;

		// Before we do anything, we must lock our inputs.  Before returning,
		//	we have to make sure that the inputs get unlocked.
		const BWORD precook=spCatalog->catalogOrDefault<bool>("precook",true);
		if(precook)
		{
#if FE_HSOP_COOK_DEBUG
			feLog("HoudiniSOP::cookMySop locking \"%s\"\n",
					(const char*)getName());
#endif

			if(lockInputs(a_rContext)>=UT_ERROR_ABORT)
			{
#if FE_HSOP_WARNINGS
				feLog("HoudiniSOP::cookMySop"
						" failed to lock inputs on \"%s\" (%s)\n",
						(const char*)getName(),operatorName.c_str());
#endif
				m_cooking=FALSE;

				//* HACK
//				return UT_ERROR_NONE;

				return error();
			}
			m_inputsLocked=TRUE;

#if FE_HSOP_COOK_DEBUG
			feLog("HoudiniSOP::cookMySop locked \"%s\"\n",
					(const char*)getName());
#endif
		}

		m_pOpContext=&a_rContext;

		UT_String nodePath;
		getRelativePathTo(NULL,nodePath);
		spCatalog->catalog<String>("nodePath")=(char*)nodePath;
		spCatalog->catalog<I32>("nodeId")=getUniqueId();

		//* A Houdini-aware operator can use:
		//* OP_Node* pOpNode=OP_Node::lookupNode(nodeId);

		// Flag the SOP as being time dependent (i.e. cook on time changes)
		flags().timeDep=spCatalog->catalogOrDefault<bool>(m_nameOutput,
						"temporal",false);

		flags().alwaysCook=spCatalog->catalogOrDefault<bool>(m_nameOutput,
						"continuous",false);

		sp<HoudiniDraw> spHoudiniDraw=m_spDrawOutput;
		if(spHoudiniDraw.isValid())
		{
//			feLog("HoudiniSOP::cookMySop set draw gdp=%p\n",gdp);
			spHoudiniDraw->setGdp(gdp);
		}

		if(m_spDrawGuideCached.isValid())
		{
			m_spDrawGuideCached->clearInput();
		}

		bool recycle=false;
		I32 channel=0;
		if(!m_nameOutput.empty())
		{
			channel= -1;

			String copy=spCatalog->catalogOrDefault<String>(m_nameOutput,
					"copy","");
			if(!copy.empty())
			{
				channel=spCatalog->catalogOrDefault<I32>(copy,"channel",-1);
			}

			recycle=spCatalog->catalogOrDefault<bool>(m_nameOutput,
					"recycle",false);
		}

		U32 surfaceCount=m_surfaceInputArray.size();
		for(int surfaceIndex=0;surfaceIndex<surfaceCount;surfaceIndex++)
		{
			const GU_Detail* pGdp=inputGeo(surfaceIndex);
			sp<SurfaceAccessibleHoudini> spSurfaceInput=
					m_surfaceInputArray[surfaceIndex];
			int changed=(pGdp != spSurfaceInput->gdp());

			spSurfaceInput->setGdp(pGdp);

			const String inputName=spSurfaceInput->name();

			if(pGdp)
			{
#if FE_HSOP_COOK_DEBUG
				OP_ERROR opError=
#endif

						checkChangedSourceFlags(surfaceIndex,
						a_rContext,&changed);

#if FE_HSOP_COOK_DEBUG
				feLog("HoudiniSOP::cookMySop"
						" surface %d/%d error %d changed %d\n",
						surfaceIndex,surfaceCount,opError,changed);
#endif

#if FE_HOUDINI_15_PLUS
				GEO_PrimList primList=pGdp->primitives();
				const I32 primitiveCount=pGdp->getNumPrimitives();

				if(primitiveCount)
				{
#if FE_HSOP_PAYLOAD_DEBUG
					feLog("HoudiniSOP::cookMySop"
							" input %d/%d payload on \"%s\"\n",
							surfaceIndex,surfaceCount,
							(const char*)getName());
#endif

					Array< sp<Component> >& rPayload=
							spCatalog->catalog< Array< sp<Component> > >(
							inputName,"payload");

					rPayload.resize(primitiveCount);

					for(U32 primitiveIndex=0;primitiveIndex<primitiveCount;
							primitiveIndex++)
					{
						const GEO_Primitive* pPrimitive=
								pGdp->getGEOPrimitive(
								pGdp->primitiveOffset(primitiveIndex));

#if FE_HSOP_PAYLOAD_DEBUG
						feLog("  prim %d/%d pPrimitive %p\n",
								primitiveIndex,primitiveCount,pPrimitive);
#endif

						if(!pPrimitive)
						{
							rPayload[primitiveIndex]=NULL;
							continue;
						}

						const HoudiniPrimComponent* pPrimComponent=
								dynamic_cast<const HoudiniPrimComponent*>(
								pPrimitive);

#if FE_HSOP_PAYLOAD_DEBUG
						feLog("    pPrimComponent %p\n",pPrimComponent);
#endif

						if(!pPrimComponent)
						{
							rPayload[primitiveIndex]=NULL;
							continue;
						}

						rPayload[primitiveIndex]=
								const_cast<HoudiniPrimComponent*>(
								pPrimComponent)->payload();

						rPayload[primitiveIndex]=pPrimComponent->payload();

#if FE_HSOP_PAYLOAD_DEBUG
						feLog("    spPayload %p\n",
								rPayload[primitiveIndex].raw());
#endif
					}
				}
#endif
			}

			spCatalog->catalog<bool>(inputName,"replaced")=changed;

			if(!pGdp && channel==surfaceIndex)
			{
				channel= -1;
			}
		}

		sp<SurfaceAccessibleHoudini> spSurfaceOutput=m_spSurfaceOutput;
		if(!spSurfaceOutput.isValid())
		{
			sp<Registry> spRegistry=m_houdiniContext.master()->registry();
			spSurfaceOutput=spRegistry->create(
					"SurfaceAccessibleI.SurfaceAccessibleHoudini");
			spSurfaceOutput->setName((const char *)getName()+String(" output"));
		}
//		feLog("HoudiniSOP::cookMySop spSurfaceOutput valid=%d\n",
//				spSurfaceOutput.isValid());

		int changed=1;
		if(channel>=0 && channel<surfaceCount)
		{
#if FE_HSOP_COOK_DEBUG
				feLog("HoudiniSOP::cookMySop output copies input %d\n",channel);
#endif
			if(recycle && !resetState)
			{

				// reuse copy of input geometry as output
				duplicateChangedSource(channel,a_rContext,&changed);
#if FE_HSOP_COOK_DEBUG
				feLog("HoudiniSOP::cookMySop changed %d\n",changed);
#endif

#if !FE_HOUDINI_14_PLUS
				//* make sure NURBS get rebuilt
				gdp->notifyCache(GU_CACHE_ALL);
#endif
			}
			else
			{
				// repeatedly copy input geometry to output
				duplicateSource(channel,a_rContext);
			}
		}
		else
		{
			changed=(!recycle || resetState);
			if(changed)
			{
				// erase all prior geometry
				gdp->clearAndDestroy();
			}
		}

		if(!m_nameOutput.empty())
		{
			spCatalog->catalog<bool>(m_nameOutput,"replaced")=changed;
		}

#if FE_HOUDINI_15_PLUS
		GEO_PrimList primList=gdp->primitives();
		const I32 primitiveCount=gdp->getNumPrimitives();

		if(primitiveCount)
		{
#if FE_HSOP_PAYLOAD_DEBUG
			feLog("HoudiniSOP::cookMySop output payload on \"%s\"\n",
					(const char*)getName());
#endif

			Array< sp<Component> >& rPayload=
					spCatalog->catalog< Array< sp<Component> > >(
					m_nameOutput,"payload");

			rPayload.resize(primitiveCount);

			for(U32 primitiveIndex=0;primitiveIndex<primitiveCount;
					primitiveIndex++)
			{
				GEO_Primitive* pPrimitive=gdp->getGEOPrimitive(
						gdp->primitiveOffset(primitiveIndex));

#if FE_HSOP_PAYLOAD_DEBUG
				feLog("  prim %d/%d pPrimitive %p\n",
						primitiveIndex,primitiveCount,pPrimitive);
#endif

				if(!pPrimitive)
				{
					rPayload[primitiveIndex]=NULL;
					continue;
				}

				HoudiniPrimComponent* pPrimComponent=
						dynamic_cast<HoudiniPrimComponent*>(pPrimitive);

#if FE_HSOP_PAYLOAD_DEBUG
				feLog("    pPrimComponent %p\n",pPrimComponent);
#endif

				if(!pPrimComponent)
				{
					rPayload[primitiveIndex]=NULL;
					continue;
				}

				rPayload[primitiveIndex]=pPrimComponent->payload();

#if FE_HSOP_PAYLOAD_DEBUG
				feLog("    spPayload %p\n",rPayload[primitiveIndex].raw());
#endif
			}
		}
#endif

#if FE_HSOP_COOK_DEBUG
		feLog("HoudiniSOP::cookMySop set output gdp %p\n",gdp);
#endif
		spSurfaceOutput->setGdpChangeable(gdp);

		m_aFrame(m_cookSignal)=m_frame;
		m_aStartFrame(m_cookSignal)=startFrame;
		m_aEndFrame(m_cookSignal)=endFrame;
		m_aTime(m_cookSignal)=time;
		m_aSurfaceOutput(m_cookSignal)=spSurfaceOutput;

		const String operation=String("cooking ")+(const char *)getName();
		m_pInterrupt=new UT_AutoInterrupt(operation.c_str());

		try
		{
			m_spSignalerI->signal(m_cookSignal);

			if(spCatalog->catalogOrDefault<String>("error","").empty())
			{
				for(int surfaceIndex=0;surfaceIndex<surfaceCount;surfaceIndex++)
				{
					const GU_Detail* pGdp=inputGeo(surfaceIndex);
					if(pGdp)
					{
						const bool force=false;
						bool change=false;
#if FE_HSOP_COOK_DEBUG
						bool noError=
#endif
								useInputSource(surfaceIndex,change,force);
#if FE_HSOP_COOK_DEBUG
						feLog("HoudiniSOP::cookMySop surface %d/%d"
								" noError %d change %d\n",
								surfaceIndex,surfaceCount,noError,change);
#endif
					}

					//* NOTE this is the magic needed if you don't lock inputs
					const bool mark_used=true;
//					OP_Node* pInputNode=
							getInput(surfaceIndex,mark_used);

//					feLog("HoudiniSOP::cookMySop inputNode %d/%d %p\n",
//							surfaceIndex,surfaceCount,pInputNode);
				}
			}
		}
		catch(const Exception& e)
		{
			spCatalog->catalog<String>("error")+=
					"\nFE EXCEPTION: "+print(e);
		}
		catch(const std::exception& std_e)
		{
			spCatalog->catalog<String>("error")+=
					"\nstdlib EXCEPTION: "+String(std_e.what());
		}
#ifdef FE_BOOST_EXCEPTIONS
		catch(const boost::exception& boost_e)
		{
			spCatalog->catalog<String>("error")+=
					"\nboost EXCEPTION: "+
					String(boost::diagnostic_information(boost_e).c_str());
		}
#endif
		catch(...)
		{
			spCatalog->catalog<String>("error")+=
					"\nUNRECOGNIZED EXCEPTION";
		}

		delete m_pInterrupt;
		m_pInterrupt=NULL;

		updateCatalog(time,FALSE);

		const String comment="Free Electron "+System::buildDate();

#if TRUE
		addMessage(SOP_MESSAGE,comment.c_str());
#else
		//* WARNING floods the undo buffer
		setComment(comment.c_str());
#endif

		relayErrors();

		if(m_inputsLocked)
		{
			unlockInputs();
			m_inputsLocked=FALSE;
		}

		if(spCatalog->cataloged("nodeColor"))
		{
			const Color& nodeColor=
					spCatalog->catalog<Color>("nodeColor");
			const UT_Color utColor(UT_RGB,
					nodeColor[0],nodeColor[1],nodeColor[2]);
//			const bool colorSet=
					setColor(utColor);
		}

		m_pOpContext=NULL;

#if FE_HOUDINI_15_PLUS
		if(spCatalog->cataloged(m_nameOutput,"payload"))
		{
#if FE_HSOP_PAYLOAD_DEBUG
			feLog("HoudiniSOP::cookMySop result payload found on \"%s\"\n",
					(const char*)getName());
#endif

			//* TODO maybe keep some things
			gdp->clearAndDestroy();

			Array< sp<Component> >& rPayload=
					spCatalog->catalog< Array< sp<Component> > >(
					m_nameOutput,"payload");
			const I32 count=rPayload.size();
			for(I32 index=0;index<count;index++)
			{
				sp<Component> spComponent=rPayload[index];

#if FE_HSOP_PAYLOAD_DEBUG
				feLog("  prim %d/%d spPayload %p\n",
						index,count,spComponent.raw());
#endif

				if(spComponent.isValid())
				{
					HoudiniPrimComponent* pPrimComponent=
							static_cast<HoudiniPrimComponent*>(
							gdp->appendPrimitive(
							HoudiniPrimComponent::getTypeId()));

#if FE_HSOP_PAYLOAD_DEBUG
					feLog("    pPrimComponent %p\n",pPrimComponent);
#endif

					if(pPrimComponent)
					{
						pPrimComponent->bind(spComponent);
					}
				}
			}
		}
#endif
	}

	m_lastTime=time;

	m_cooking=FALSE;

	if(frameChanged)
	{
		resignalBrush();
	}

#if FE_HSOP_COOK_DEBUG
	feLog("HoudiniSOP::cookMySop done \"%s\"\n",(const char*)getName());
#endif

	return error();
}

void HoudiniSOP::clearErrors(void)
{
	sp<Catalog> spCatalog=m_spOperatorSurfaceI;
	if(!spCatalog.isValid())
	{
		feLog("HoudiniSOP::clearErrors no catalog\n");
		return;
	}

	String messageString=spCatalog->catalogOrDefault<String>("message","");
	if(!messageString.empty())
	{
		spCatalog->catalog<String>("message")="";
	}

	String warningString=spCatalog->catalogOrDefault<String>("warning","");
	if(!warningString.empty())
	{
		spCatalog->catalog<String>("warning")="";
	}

	String errorString=spCatalog->catalogOrDefault<String>("error","");
	if(!errorString.empty())
	{
		spCatalog->catalog<String>("error")="";
	}
}

void HoudiniSOP::relayErrors(void)
{
	sp<Catalog> spCatalog=m_spOperatorSurfaceI;
	if(!spCatalog.isValid())
	{
		feLog("HoudiniSOP::relayErrors no catalog\n");
		return;
	}

	String messageString=spCatalog->catalogOrDefault<String>("message","");
	if(!messageString.empty())
	{
		addMessage(SOP_MESSAGE, messageString.c_str());
		spCatalog->catalog<String>("message")="";

#if FE_HSOP_COOK_DEBUG
		feLog("HoudiniSOP::relayErrors message string \"%s\"\n",
				messageString.c_str());
#endif
	}

	String warningString=spCatalog->catalogOrDefault<String>("warning","");
	if(!warningString.empty())
	{
		addWarning(SOP_MESSAGE, warningString.c_str());
		spCatalog->catalog<String>("warning")="";

#if FE_HSOP_COOK_DEBUG
		feLog("HoudiniSOP::relayErrors warning string \"%s\"\n",
				warningString.c_str());
#endif
	}

	String errorString=spCatalog->catalogOrDefault<String>("error","");
	if(!errorString.empty())
	{
		addError(SOP_MESSAGE, errorString.c_str());
		spCatalog->catalog<String>("error")="";

#if FE_HSOP_COOK_DEBUG
		feLog("HoudiniSOP::relayErrors error string \"%s\"\n",
				errorString.c_str());
#endif
	}
}

const String HoudiniSOP::prompt(void)
{
	sp<Catalog> spCatalog=m_spOperatorSurfaceI;
	if(!spCatalog.isValid())
	{
		return String();
	}

	return spCatalog->catalogOrDefault<String>(m_brushName,"prompt","");
}

const String HoudiniSOP::idleEvents(void)
{
	sp<Catalog> spCatalog=m_spOperatorSurfaceI;
	if(!spCatalog.isValid())
	{
		return String();
	}

	return spCatalog->catalogOrDefault<String>(m_brushName,"idle","");
}

const String HoudiniSOP::brushVisible(void)
{
	sp<Catalog> spCatalog=m_spOperatorSurfaceI;
	if(!spCatalog.isValid())
	{
		return String();
	}

	return spCatalog->catalogOrDefault<String>(m_brushName,"visible","viewer");
}

const String HoudiniSOP::brushCook(void)
{
	sp<Catalog> spCatalog=m_spOperatorSurfaceI;
	if(!spCatalog.isValid())
	{
		return String();
	}

	return spCatalog->catalogOrDefault<String>(m_brushName,"cook","");
}

void HoudiniSOP::brush(GU_Detail* pGdp,WindowEvent& a_rEvent,
	SpatialVector& a_rRayOrigin,SpatialVector& a_rRayDirection)
{
#if FE_HSOP_BRUSH_DEBUG
	feLog("HoudiniSOP::brush %s\n",c_print(a_rEvent));
	feLog(" ray %s dir %s\n",c_print(a_rRayOrigin),c_print(a_rRayDirection));
#endif

	if(!m_spDrawBrush.isValid())
	{
#if FE_HSOP_BRUSH_DEBUG
		feLog("HoudiniSOP::brush no brush\n");
#endif
		return;
	}

	sp<HoudiniDraw> spHoudiniDraw=m_spDrawBrush;
	if(spHoudiniDraw.isValid())
	{
		spHoudiniDraw->setGdp(pGdp);
		if(pGdp)
		{
			pGdp->clearAndDestroy();
		}
	}

	if(a_rEvent.isNull())
	{
#if FE_HSOP_BRUSH_DEBUG
		feLog("HoudiniSOP::brush null event\n");
#endif
		return;
	}

	if(m_spDrawBrush->drawChain().isNull() ||
			m_spDrawBrushOverlay->drawChain().isNull())
	{
#if FE_HSOP_BRUSH_DEBUG
		feLog("HoudiniSOP::brush not ready\n");
#endif
		return;
	}

	clearErrors();

	m_spDrawBrush->clearInput();
	m_spDrawBrushOverlay->clearInput();

	m_aWindowEvent(m_brushSignal)=a_rEvent.record();
	m_aRayOrigin(m_brushSignal)=a_rRayOrigin;
	m_aRayDirection(m_brushSignal)=a_rRayDirection;

	sp<Catalog> spCatalog=m_spOperatorSurfaceI;

	try
	{
		m_spSignalerI->signal(m_brushSignal);
	}
	catch(const Exception& e)
	{
#if FE_CODEGEN<=FE_DEBUG
		feLog("HoudiniSOP::brush \"%s\" FE exception:\n  %s\n",
				m_spOperatorSurfaceI->name().c_str(),c_print(e));
#else
		feLog("operator \"%s\" tool mode failed\n",
				m_spOperatorSurfaceI->name().c_str());
#endif

		spCatalog->catalog<String>("error")+=
				"\nFE EXCEPTION: "+print(e);
	}
	catch(const std::exception& std_e)
	{
#if FE_CODEGEN<=FE_DEBUG
		feLog("HoudiniSOP::brush \"%s\" stdlib exception:\n  %s\n",
				m_spOperatorSurfaceI->name().c_str(),std_e.what());
#else
		feLog("operator \"%s\" tool mode failed\n",
				m_spOperatorSurfaceI->name().c_str());
#endif

		spCatalog->catalog<String>("error")+=
				"\nstdlib EXCEPTION: "+String(std_e.what());
	}
#ifdef FE_BOOST_EXCEPTIONS
	catch(const boost::exception& boost_e)
	{
#if FE_CODEGEN<=FE_DEBUG
		feLog("HoudiniSOP::brush \"%s\" boost exception:\n  %s\n",
				m_spOperatorSurfaceI->name().c_str(),
				boost::diagnostic_information(boost_e).c_str());
#else
		feLog("operator \"%s\" tool mode failed\n",
				m_spOperatorSurfaceI->name().c_str());
#endif

		spCatalog->catalog<String>("error")+=
				"\nboost EXCEPTION: "+
				String(boost::diagnostic_information(boost_e).c_str());
	}
#endif
	catch(...)
	{
#if FE_CODEGEN<=FE_DEBUG
		feLog("HoudiniSOP::brush \"%s\" unrecognized exception\n",
				m_spOperatorSurfaceI->name().c_str());
#else
		feLog("operator \"%s\" tool mode failed\n",
				m_spOperatorSurfaceI->name().c_str());
#endif

		spCatalog->catalog<String>("error")+=
				"\nUNRECOGNIZED EXCEPTION";
	}

	const String cook=brushCook();
	if(!cook.empty())
	{
#if FE_HSOP_BRUSH_DEBUG
		feLog("HoudiniSOP::brush forceRecook \"%s\"\n",cook.c_str());
#endif

		relayErrors();

		forceRecook();

		if(cook!="always")
		{
			spCatalog->catalog<String>(m_brushName,"cook")="";
		}
	}
}

void HoudiniSOP::resignalBrush(void)
{
	if(!m_brushSignal.isValid())
	{
		return;
	}

	Record record=m_aWindowEvent(m_brushSignal);
	if(!record.isValid())
	{
		return;
	}

	m_spDrawBrush->clearInput();
	m_spDrawBrushOverlay->clearInput();

	WindowEvent event;
	event.bind(record);
	event.reset();
	event.setSIS(WindowEvent::e_sourceSystem,
				WindowEvent::Item(WindowEvent::e_itemExpose),
				WindowEvent::e_stateNull);

	sp<Catalog> spCatalog=m_spOperatorSurfaceI;
	try
	{
		m_spSignalerI->signal(m_brushSignal);
	}
	catch(Exception &e)
	{
#if FE_CODEGEN<=FE_DEBUG
		feLog("HoudiniSOP::resignalBrush \"%s\" FE exception:\n  %s\n",
				m_spOperatorSurfaceI->name().c_str(),c_print(e));
#else
		feLog("operator \"%s\" tool mode failed\n",
				m_spOperatorSurfaceI->name().c_str());
#endif

		spCatalog->catalog<String>("warning")+=
				"\n[rebrush] FE EXCEPTION: "+print(e);
	}
	catch(const std::exception& std_e)
	{
#if FE_CODEGEN<=FE_DEBUG
		feLog("HoudiniSOP::resignalBrush \"%s\" stdlib exception:\n  %s\n",
				m_spOperatorSurfaceI->name().c_str(),std_e.what());
#else
		feLog("operator \"%s\" tool mode failed\n",
				m_spOperatorSurfaceI->name().c_str());
#endif

		spCatalog->catalog<String>("warning")+=
				"\n[rebrush] stdlib EXCEPTION: "+String(std_e.what());
	}
#ifdef FE_BOOST_EXCEPTIONS
	catch(const boost::exception& boost_e)
	{
#if FE_CODEGEN<=FE_DEBUG
		feLog("HoudiniSOP::resignalBrush \"%s\" boost exception:\n  %s\n",
				m_spOperatorSurfaceI->name().c_str(),
				boost::diagnostic_information(boost_e).c_str());
#else
		feLog("operator \"%s\" tool mode failed\n",
				m_spOperatorSurfaceI->name().c_str());
#endif

		spCatalog->catalog<String>("warning")+=
				"\n[rebrush] boost EXCEPTION: "+
				String(boost::diagnostic_information(boost_e).c_str());
	}
#endif
	catch(...)
	{
#if FE_CODEGEN<=FE_DEBUG
		feLog("HoudiniSOP::resignalBrush \"%s\" unrecognized exception\n",
				m_spOperatorSurfaceI->name().c_str());
#else
		feLog("operator \"%s\" tool mode failed\n",
				m_spOperatorSurfaceI->name().c_str());
#endif

		spCatalog->catalog<String>("warning")+=
				"\n[rebrush] UNRECOGNIZED EXCEPTION";
	}

	relayErrors();
}

OP_ERROR HoudiniSOP::cookMyGuide1(OP_Context &a_rContext)
{
#if FE_HSOP_COOK_DEBUG
	const double time=a_rContext.getTime();

	UT_String operatorParam;
	evalString(operatorParam,0,0,time);
	String operatorName((char*)operatorParam);

	feLog("--------------------------------\n");
	feLog("HoudiniSOP::cookMyGuide1 %f \"%s\"\n",time,operatorName.c_str());
#endif

	if(m_guideEmpty && m_spDrawGuideCached->empty())
	{
#if FE_HSOP_COOK_DEBUG
		feLog("HoudiniSOP::cookMyGuide1 no guides\n");
#endif
		return error();
	}

	if(lockInputs(a_rContext)>=UT_ERROR_ABORT)
	{
		return error();
	}

	myGuide1->clearAndDestroy();

	if(m_spDrawGuideCached->empty())
	{
		m_guideEmpty=TRUE;
	}
	else
	{
		m_guideEmpty=FALSE;

		sp<HoudiniDraw> spHoudiniDraw=m_spDrawGuideChain;
		if(spHoudiniDraw.isValid())
		{
//			feLog("HoudiniSOP::cookMyGuide1 set guide gdp=%p\n",gdp);
			spHoudiniDraw->setGdp(myGuide1);
		}

		sp<Catalog> spCatalog=m_spOperatorSurfaceI;
		try
		{
			//* don't clear cache since we may need to draw the same data again
			m_spDrawGuideCached->flushLive();
		}
		catch(Exception &e)
		{
			spCatalog->catalog<String>("warning")+=
					"\n[cookMyGuide1] FE EXCEPTION: "+print(e);
		}
		catch(...)
		{
			spCatalog->catalog<String>("warning")+=
					"\n[cookMyGuide1] UNRECOGNIZED EXCEPTION";
		}

		relayErrors();
	}

	unlockInputs();

	return error();
}

OP_ERROR HoudiniSOP::save(std::ostream& a_rStreamOut,
	const OP_SaveFlags& a_rFlags,const char* a_pathPrefix)
{
	const double time=0.0;

	UT_String operatorParam;
	evalString(operatorParam,0,0,time);
	String operatorName((char*)operatorParam);

#if FE_HSOP_IO_DEBUG
	feLog("HoudiniSOP::save %s \"%s\"\n",
			a_rFlags.getBinary()? "binary": "ascii",a_pathPrefix);
#endif

	if(SOP_Node::save(a_rStreamOut,a_rFlags,a_pathPrefix) >= UT_ERROR_ABORT)
	{
		return error();
	}

	if(!m_spOperatorSurfaceI.isValid())
	{
		createOperator(operatorName,time);
	}

	String buffer;
	if(m_spOperatorSurfaceI.isValid() &&
			m_spOperatorSurfaceI->saveState(buffer))
	{
		//* custom save

		const BWORD binary=a_rFlags.getBinary();

		const char* extension=binary? "fe_binary" : "fe_ascii";

		UT_WorkBuffer path;
		path.sprintf("%s%s.%s",a_pathPrefix,(const char *)getName(),extension);

		UT_CPIO packet;
		packet.open(a_rStreamOut,path.buffer());

#if FE_HOUDINI_MAJOR >= 12
		UT_OStream utOStream(a_rStreamOut,binary);

		const bool newline=true;
		utOStream.write(std::string(buffer.c_str()),newline);
#else
		UT_OStreamFilter filter(a_rStreamOut,binary);

		filter.write(buffer.c_str());
		filter.endLine();
#endif

		packet.close(a_rStreamOut);
	}

	return error();
}

bool HoudiniSOP::load(UT_IStream& a_rStreamIn,
	const char* a_extension,const char* a_path)
{
#if FE_HSOP_IO_DEBUG
	feLog("HoudiniSOP::load \"%s\" \"%s\"\n",a_extension,a_path);
#endif

	if(strcmp(a_extension, "fe_binary") == 0 ||
			strcmp(a_extension, "fe_ascii") == 0)
	{
		//* custom load

		std::string bytes;
		if(!a_rStreamIn.read(bytes))
		{
			return false;
		}

		m_loadedState=bytes.c_str();

		//* NOTE if not now, then try again when created
		if(m_spOperatorSurfaceI.isValid())
		{
			m_spOperatorSurfaceI->loadState(m_loadedState);
			m_loadedState="";
		}

		return true;
	}

	return SOP_Node::load(a_rStreamIn,a_extension,a_path);
}

} /* namespace ext */
} /* namespace fe */
