/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __houdini_PortalOp_h__
#define __houdini_PortalOp_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Operator send and receive surfaces

	@ingroup houdini
*//***************************************************************************/
class FE_DL_EXPORT PortalOp:
	public OperatorSurfaceCommon,
	public Initialize<PortalOp>
{
	public:

					PortalOp(void)											{}
virtual				~PortalOp(void)											{}

		void		initialize(void);

					//* As HandlerI
virtual	void		handle(Record& a_rSignal);

	private:
};

} /* namespace ext */
} /* namespace fe */

#endif /* __houdini_PortalOp_h__ */
