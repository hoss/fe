/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __surface_SurfaceTrianglesHoudini_h__
#define __surface_SurfaceTrianglesHoudini_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Houdini Trianglar Surface

	@ingroup houdini
*//***************************************************************************/
class FE_DL_EXPORT SurfaceTrianglesHoudini:
	public SurfaceTriangles,
	public CastableAs<SurfaceTrianglesHoudini>
{
	public:
							SurfaceTrianglesHoudini(void);
virtual						~SurfaceTrianglesHoudini(void);

							//* As Protectable
virtual	Protectable*		clone(Protectable* pInstance=NULL);

							//* Houdini specific
		void				setGdp(const GU_Detail* a_pGdp)
							{
								m_pGdp=a_pGdp;
							}
const	GU_Detail*			gdp(void) const					{ return m_pGdp; }

		void				setSubIndex(I32 a_subIndex)
							{	m_subIndex=a_subIndex; }
		I32					subIndex(void) const
							{	return m_subIndex; }

	protected:

virtual	void				cache(void);

	private:

const	GU_Detail*			m_pGdp;
		I32					m_subIndex;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __surface_SurfaceTriangles_h__ */

