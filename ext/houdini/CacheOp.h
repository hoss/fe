/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __houdini_CacheOp_h__
#define __houdini_CacheOp_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Store and reuse copies of the input

	@ingroup houdini
*//***************************************************************************/
class FE_DL_EXPORT CacheOp:
	public OperatorSurfaceCommon,
	public Initialize<CacheOp>
{
	public:

					CacheOp(void);
virtual				~CacheOp(void);

		void		initialize(void);

					//* As HandlerI
virtual	void		handle(Record& a_rSignal);

		void		clearCache(void);

	private:

	class Stamp
	{
		public:
					Stamp(void):
						m_parmSerial(0),
						m_feSerial(0),
						m_upstreamChanges(0),
						m_hit(FALSE)										{}

			U64		m_parmSerial;
			I32		m_feSerial;
			I32		m_upstreamChanges;
			BWORD	m_hit;
			String	m_expressions;
			String	m_values;
	};

		void		reset(void);
		I32			runScan(BWORD a_write);
		I32			scan(OP_Node* a_pOpNode,BWORD a_write,BWORD a_rootNode);
		String		nodeSignature(OP_Node* a_pOpNode,BWORD a_valuesOnly);
		void		changeHoard(String a_hoard);
		void		changeGlobal(BWORD a_include);
		void		findHoardCaches(String a_hoard,BWORD a_clear);

		OP_Node*	m_pThisOpNode;
		String		m_thisNodeParent;
		Real		m_lastFrame;
		Real		m_currentFrame;
		Real		m_poisonFrame;
		Real		m_time;
		I32			m_cacheCount;
		String		m_currentHoard;
		String		m_blame;

		std::map<I32, sp<SurfaceAccessibleI> >	m_frameCache;
		std::map<String,Stamp>					m_stampMap;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __houdini_CacheOp_h__ */
