/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <houdini/houdini.pmh>

namespace fe
{
namespace ext
{

class InfoGdp : public BaseType::Info
{
virtual	String	print(void *instance)
				{
					GU_Detail* pGdp = (GU_Detail*)instance;
					return pGdp? "<valid>": "<invalid>";
				}

virtual	IWORD	output(std::ostream &ostrm, void *instance, t_serialMode mode)
				{
					if(mode == e_ascii) { return c_noascii; }
					return 0;
				}
virtual	void	input(std::istream &istrm, void *instance, t_serialMode mode)
				{
				}
virtual	IWORD	iosize(void)				{ return c_implicit; }
virtual	bool	getConstruct(void)			{ return true; }
virtual	void	construct(void *instance)	{ new(instance)GU_Detail; }
virtual	void	destruct(void *instance)
				{	((GU_Detail*)instance)->~GU_Detail();}
};

void assertHoudini(sp<TypeMaster> spTypeMaster)
{
	sp<BaseType> spT;

	spT = spTypeMaster->assertType<GU_Detail*>("gdp");
	spT->setInfo(new InfoGdp());
}

} /* namespace ext */
} /* namespace fe */
