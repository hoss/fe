/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <houdini/houdini.pmh>

#include <RE/RE_Render.h>

namespace fe
{
namespace ext
{

RE_Render* FontHoudini::ms_pRender=NULL;
Array<FontHoudini::Phrase> FontHoudini::ms_phraseArray;

FontHoudini::FontHoudini(void):
	m_fontAscent(0),
	m_fontDescent(0),
	m_multiplication(0.0)
{
}

FontHoudini::~FontHoudini(void)
{
}

void FontHoudini::initialize(void)
{
//	feLog("FontHoudini::initialize\n");

	//* TODO query
	m_fontAscent=16;
	m_fontDescent=4;
}

void FontHoudini::drawAlignedText(sp<Component> a_spDrawComponent,
	const SpatialVector& a_location,const String a_text,const Color &a_color)
{
//	feLog("FontHoudini::drawAlignedText loc %s text \"%s\" color %s\n",
//			c_print(a_location),a_text.c_str(),c_print(a_color));

	SpatialVector location=a_location;

	sp<DrawI> spDrawI=a_spDrawComponent;
	if(spDrawI.isValid())
	{
		sp<ViewI> spViewI=spDrawI->view();
		if(spViewI->projection()==ViewI::e_ortho)
		{
			location=spViewI->unproject(location[0],location[1],0.01,
					ViewI::e_perspective);
		}
	}

	const I32 index=ms_phraseArray.size();
	ms_phraseArray.resize(index+1);
	Phrase& rPhrase=ms_phraseArray[index];

	rPhrase.m_location=location;
	rPhrase.m_text=a_text;
	rPhrase.m_color=a_color;
}

I32 FontHoudini::pixelWidth(String a_string)
{
	const U32 count=a_string.length();

	//* TODO query
	return 13*count;
}

//* static
void FontHoudini::flush(void)
{
	const I32 count=ms_phraseArray.size();
	for(I32 index=0;index<count;index++)
	{
		Phrase& rPhrase=ms_phraseArray[index];
		const SpatialVector& rLocation=rPhrase.m_location;
		const Color& rColor=rPhrase.m_color;

		fpreal32 loc[3];
		loc[0]=rLocation[0];
		loc[1]=rLocation[1];
		loc[2]=rLocation[2];

		const UT_Color utColor(UT_RGB,rColor[0],rColor[1],rColor[2]);

		const UT_Matrix4D* view = nullptr;
		const UT_Matrix4D* proj = nullptr;
		const UT_DimRect* viewport = nullptr;
		ms_pRender->drawViewportString(loc,rPhrase.m_text.c_str(),
				&utColor,view,proj,viewport);
	}

	ms_phraseArray.clear();
}

} /* namespace ext */
} /* namespace fe */
