/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <houdini/houdini.pmh>

//* TODO generalize beyond Houdini

#define FE_CHO_DEBUG		FALSE
#define FE_CHO_VERBOSE		FALSE
#define FE_CHO_SCAN			FALSE
#define FE_CHO_BLAME		FALSE

#define FE_CHO_GLOBAL		"<global>"

using namespace fe;
using namespace fe::ext;

CacheOp::CacheOp(void)
{
	reset();
}

void CacheOp::reset(void)
{
	m_lastFrame= -1.0;
	m_currentFrame= -1.0;
	m_poisonFrame= -1.0;
	m_cacheCount=0;
	m_frameCache.clear();
	m_stampMap.clear();
}

CacheOp::~CacheOp(void)
{
	changeHoard("");
	changeGlobal(FALSE);
}

void CacheOp::initialize(void)
{
	changeGlobal(TRUE);

	catalog<String>("icon")="FE_alpha";

	catalog<bool>("precook")=false;

	catalog<bool>("temporal")=false;
	catalog<String>("temporal","label")="Temporal";
	catalog<String>("temporal","page")="Data";
	catalog<String>("temporal","hint")=
			"When rebaking the cache for a frame,"
			" clear the cache for all following frames.";

	catalog<bool>("scan")=true;
	catalog<String>("scan","label")="Watch For Changes";
	catalog<String>("scan","page")="Data";
	catalog<String>("scan","hint")=
			"Probe all nodes feeding input for any parameter edits.";

	catalog<String>("hoard")="Hoard";
	catalog<String>("hoard","label")="Hoard Name";
	catalog<String>("hoard","suggest")="word";
	catalog<String>("hoard","page")="Data";
	catalog<String>("hoard","hint")=
			"Named collection of caches that can be cleared together.";

	catalog<I32>("clear")=0;
	catalog<String>("clear","label")="Clear Cache";
	catalog<String>("clear","suggest")="button";
	catalog<bool>("clear","joined")=true;
	catalog<String>("clear","page")="Data";
	catalog<String>("clear","hint")=
			"Reset the cache for this node.";

	catalog<I32>("clearHoard")=0;
	catalog<String>("clearHoard","label")="Clear Hoard";
	catalog<String>("clearHoard","suggest")="button";
	catalog<bool>("clearHoard","joined")=true;
	catalog<String>("clearHoard","page")="Data";
	catalog<String>("clearHoard","hint")=
			"Reset the cache for this node and any nodes"
			" found with the same hoard name.";

	catalog<I32>("clearGlobal")=0;
	catalog<String>("clearGlobal","label")="Clear Global";
	catalog<String>("clearGlobal","suggest")="button";
	catalog<String>("clearGlobal","page")="Data";
	catalog<String>("clearGlobal","hint")=
			"Reset the cache for all nodes found.";

	catalog<bool>("changeColor")=false;
	catalog<String>("changeColor","label")="Change Node Color";
	catalog<String>("changeColor","page")="Display";
	catalog<String>("changeColor","hint")=
			"Allow this node to change its network view color.";

	catalog<SpatialVector>("resetColor")=SpatialVector(1.0,0.8,0.8);
	catalog<String>("resetColor","label")="Color After Reset";
	catalog<String>("resetColor","suggest")="color";
	catalog<String>("resetColor","enabler")="changeColor";
	catalog<String>("resetColor","page")="Display";
	catalog<String>("resetColor","hint")=
			"Color to use just after the cache has been cleared.";

	catalog<SpatialVector>("bakeColor")=SpatialVector(1.0,1.0,0.6);
	catalog<String>("bakeColor","label")="Color After Rebake";
	catalog<String>("bakeColor","suggest")="color";
	catalog<String>("bakeColor","enabler")="changeColor";
	catalog<String>("bakeColor","page")="Display";
	catalog<String>("bakeColor","hint")=
			"Color to use when the input gets cooked,"
			" except just after the cache has been cleared.";

	catalog<SpatialVector>("cacheColor")=SpatialVector(0.8,1.0,0.8);
	catalog<String>("cacheColor","label")="Color Using Cache";
	catalog<String>("cacheColor","suggest")="color";
	catalog<String>("cacheColor","enabler")="changeColor";
	catalog<String>("cacheColor","page")="Display";
	catalog<String>("bakeColor","hint")=
			"Color to use when the cached frame is copied to the output.";

	catalog<String>("collaborators")="";
	catalog<String>("collaborators","label")="Collaborators";
	catalog<String>("collaborators","IO")="output";
	catalog<String>("collaborators","suggest")="multiline";
	catalog<String>("collaborators","page")="Hoard";
	catalog<String>("collaborators","hint")=
			"Other caches in the same hoard.";

	catalog< sp<Component> >("Input Surface");

	catalog< sp<Component> >("Watch Surface");
	catalog<bool>("Watch Surface","optional")=true;

	catalog< sp<Component> >("Output Surface");
	catalog<String>("Output Surface","IO")="output";
	catalog<bool>("Output Surface","continuous")=true;
	catalog<bool>("Output Surface","temporal")=true;
	catalog<I32>("Output Surface","serial")=0;
}

void CacheOp::handle(Record& a_rSignal)
{
#if FE_CHO_DEBUG
	feLog("CacheOp::handle node \"%s\"\n",name().c_str());
#endif

	String& rSummary=catalog<String>("summary");
	if(rSummary!="CLEARED")
	{
		rSummary="";
	}

	const BWORD changeColor=catalog<bool>("changeColor");

	const I32 nodeId=catalog<I32>("nodeId");
	m_pThisOpNode=OP_Node::lookupNode(nodeId);
	if(!m_pThisOpNode)
	{
		catalog<String>("error")="failed to lookup this node";
		return;
	}

	UT_String utNodePath;

	m_pThisOpNode->getRelativePathTo(NULL,utNodePath);
	const String thisNodePath=(char*)utNodePath;
	m_thisNodeParent=thisNodePath.replace("/[^/]*$","");

#if FALSE
	const String nodePath=catalog<String>("nodePath");
	feLog("CacheOp::handle nodePath \"%s\"\n",nodePath.c_str());
#endif

	m_time=currentTime(a_rSignal);
	const Real frame=currentFrame(a_rSignal);
	const I32 cacheIndex=frame*1000;	//* TODO precision param

	m_lastFrame=m_currentFrame;
	m_currentFrame=frame;

	if(m_poisonFrame!=m_currentFrame)
	{
		m_poisonFrame= -1.0;
	}

#if FE_CHO_DEBUG
	feLog("CacheOp::handle frame %.6G -> %.6G cacheIndex %d\n",
			m_lastFrame,frame,cacheIndex);
#endif
	changeHoard(catalog<String>("hoard"));

	m_blame="";

	String clearHoard;

	I32& rClearCount=catalog<I32>("clear");
	I32& rClearHoardCount=catalog<I32>("clearHoard");
	I32& rClearGlobalCount=catalog<I32>("clearGlobal");
	if(rClearCount || rClearHoardCount || rClearGlobalCount)
	{
#if FE_CHO_DEBUG
		feLog("CacheOp::handle >>>>>>>> CLEAR CACHE\n");
#endif
		if(rClearGlobalCount)
		{
			clearHoard=FE_CHO_GLOBAL;
		}
		else if(rClearHoardCount)
		{
			clearHoard=m_currentHoard;
		}

		clearCache();

		rClearCount=0;
		rClearHoardCount=0;
		rClearGlobalCount=0;
	}
	else if(m_poisonFrame==m_currentFrame)
	{
#if FE_CHO_DEBUG
		feLog("CacheOp::handle >>>>>>>> POISON CACHE\n");
#endif

		clearCache();

		rSummary="POISONED";
	}
	else if(m_frameCache[cacheIndex].isValid())
	{
		//* NOTE no scan needed if going to regenerate regardless

		if(catalog<bool>("scan"))
		{
			const I32 changeCount=runScan(FALSE);

			if(changeCount)
			{
#if FE_CHO_DEBUG
				feLog("CacheOp::handle >>>>>>>> RESET CACHE\n");
#endif

				if(!m_blame.empty())
				{
					catalog<String>("message")+=
							"changes detected on: "+m_blame+";";
				}

				clearCache();

				if(changeCount>1)
				{
					rSummary.sPrintf("CHANGES x%d",changeCount);
				}
				else
				{
					rSummary="CHANGES";
				}
			}
		}
	}

	sp<SurfaceAccessibleI> spCacheFrame=m_frameCache[cacheIndex];
	if(spCacheFrame.isNull())
	{
#if FE_CHO_DEBUG
		feLog("CacheOp::handle >>>>>>>> GENERATE CACHE\n");
#endif

		if(catalog<bool>("temporal"))
		{
			for(std::map<I32, sp<SurfaceAccessibleI> >::iterator it=
					m_frameCache.begin();it!=m_frameCache.end();it++)
			{
				const I32 otherIndex=it->first;
				sp<SurfaceAccessibleI>& rOtherFrame=it->second;
				if(otherIndex>cacheIndex && rOtherFrame.isValid())
				{
#if FE_CHO_VERBOSE
					feLog("CacheOp::handle temporal clear %d (vs %d)\n",
							otherIndex,cacheIndex);
#endif

					rOtherFrame=NULL;
					m_cacheCount--;
				}
			}
		}

		HoudiniSOP* pHoudiniSOP=dynamic_cast<HoudiniSOP*>(m_pThisOpNode);

		if(!pHoudiniSOP->cookInputs())
		{
			catalog<String>("error")="failed to cook input";
			return;
		}

		spCacheFrame=catalog< sp<Component> >("Input Surface");

#if FE_CHO_VERBOSE
			feLog("CacheOp::handle baked input has %d points\n",
					spCacheFrame->count(SurfaceAccessibleI::e_point));
#endif

		if(m_poisonFrame!=m_currentFrame)
		{
			sp<SurfaceAccessibleI> spSurfaceAccessibleI=
					registry()->create("*.SurfaceAccessibleHoudini");
			if(spSurfaceAccessibleI.isNull())
			{
#if FE_CHO_DEBUG
				feLog("CacheOp::handle fallback to catalog\n");
#endif
				spSurfaceAccessibleI=
						registry()->create("*.SurfaceAccessibleCatalog");
			}
			if(spSurfaceAccessibleI.isValid())
			{
				spSurfaceAccessibleI->copy(spCacheFrame);

#if FE_CHO_VERBOSE
				feLog("CacheOp::handle cached %d points\n",
						spSurfaceAccessibleI->count(
						SurfaceAccessibleI::e_point));
#endif

				m_frameCache[cacheIndex]=spSurfaceAccessibleI;
				m_cacheCount++;
			}
#if FE_CHO_DEBUG
			else
			{
				feLog("CacheOp::handle failed to create cache\n");
			}
#endif
		}
#if FE_CHO_DEBUG
		else
		{
			feLog("CacheOp::handle not caching poisoned frame\n");
		}
#endif

		if(changeColor && (m_cacheCount>1 || rSummary.empty()))
		{
			catalog<Color>("nodeColor")=catalog<SpatialVector>("bakeColor");
		}
	}
	else
	{
#if FE_CHO_VERBOSE
		feLog("CacheOp::handle cache has %d points\n",
				spCacheFrame->count(SurfaceAccessibleI::e_point));
#endif

		if(changeColor)
		{
			catalog<Color>("nodeColor")=catalog<SpatialVector>("cacheColor");
		}
	}

	sp<SurfaceAccessibleI> spOutputAccessible;
	if(!accessOutput(spOutputAccessible,a_rSignal)) return;

	spOutputAccessible->copy(spCacheFrame);

#if FE_CHO_VERBOSE
	feLog("CacheOp::handle output has %d points\n",
			spOutputAccessible->count(SurfaceAccessibleI::e_point));
#endif

	if(catalog<bool>("scan"))
	{
		runScan(TRUE);
	}

	if(rSummary.empty() || m_cacheCount>1)
	{
		rSummary.sPrintf("%s%d frame%s",
				m_currentHoard.empty()? "": (m_currentHoard+" ").c_str(),
				m_cacheCount,m_cacheCount>1? "s": "");
	}

	findHoardCaches(clearHoard,TRUE);
	findHoardCaches(m_currentHoard,FALSE);

#if FE_CHO_DEBUG
	feLog("CacheOp::handle node \"%s\" done\n",name().c_str());
#endif
}

I32 CacheOp::runScan(BWORD a_write)
{
	//* clear hit flags

	for(std::map<String,Stamp>::iterator it=
			m_stampMap.begin();it!=m_stampMap.end();it++)
	{
		Stamp& rStamp=it->second;
		rStamp.m_hit=FALSE;
		rStamp.m_upstreamChanges=0;
	}

	return scan(m_pThisOpNode,a_write,TRUE);
}

I32 CacheOp::scan(OP_Node* a_pOpNode,BWORD a_write,BWORD a_rootNode)
{
	const bool mark_used=true;

#if FE_HOUDINI_12_5_PLUS
	OP_Node* pPassThrough=a_pOpNode->getPassThroughNode(m_time);
#else
	OP_Node* pPassThrough=NULL;
#endif

	const I32 singleInput=a_pOpNode->cookedInputIndex();
	const I32 isNetwork=a_pOpNode->isNetwork();
	const U32 connectedCount=a_pOpNode->nConnectedInputs();

#if FE_CHO_SCAN
	if(!a_write)
	{
		feLog("CacheOp::scan \"%s\" input %d/%d pass \"%s\" net %d\n",
				(const char *)a_pOpNode->getName(),
				singleInput,connectedCount,
				pPassThrough? (const char *)pPassThrough->getName(): "<null>",
				isNetwork);
	}
#endif

	//* pass through of parent node is display node of where we started
	if(pPassThrough)
	{
		UT_String utNodePath;
		a_pOpNode->getRelativePathTo(NULL,utNodePath);
		const String opNodePath=(char*)utNodePath;

		if(opNodePath==m_thisNodeParent)
		{
#if FE_CHO_SCAN
			if(!a_write)
			{
				feLog("  ignore passthrough of cache's parent\n");
			}
#endif
			pPassThrough=NULL;
		}
	}

	OP_NodeList reflist;
	const bool recurse=false;
	a_pOpNode->getExistingOpReferences(reflist,recurse);
	const U32 refCount=reflist.entries();

#if FALSE
	for(U32 refIndex=0;refIndex<refCount;refIndex++)
	{
		OP_Node* pRefNode=reflist(refIndex);
		feLog("CacheOp::scan \"%s\" ref %d/%d \"%s\"\n",
				(const char *)a_pOpNode->getName(),refIndex,refCount,
				(const char *)pRefNode->getName());
	}
#endif

	I32 changeCount=0;

	//* inputs, then references
	for(U32 pass=0;pass<2;pass++)
	{
		U32 checkCount=
				pass? refCount: (a_rootNode? 1: connectedCount);
		for(U32 checkIndex=0;checkIndex<checkCount;checkIndex++)
		{
			OP_Node* pCheckNode=NULL;

			const I32 modIndex=
					(!pass && a_rootNode)? connectedCount-1: checkIndex;
			I32 inputIndex= -1;

			if(pass)
			{
				pCheckNode=reflist(checkIndex);
			}
			else
			{
				inputIndex=a_pOpNode->getNthConnectedInput(modIndex);
				pCheckNode=a_pOpNode->getInput(inputIndex);
			}

#if FE_CHO_SCAN
			if(!a_write)
			{
				feLog("  \"%s\" pass %d check %d/%d \"%s\"\n",
						(const char *)a_pOpNode->getName(),
						pass,checkIndex,checkCount,
						pCheckNode?
						(const char *)pCheckNode->getName(): "<NULL>");
			}
#endif

			if(!pCheckNode)
			{
				feLog("CacheOp::scan \"%s\" indicated input %d missing\n",
						(const char *)a_pOpNode->getName(),modIndex);
				continue;
			}

			//* prevent loop to original node
			if(pCheckNode==m_pThisOpNode)
			{
#if FE_CHO_SCAN
				if(!a_write)
				{
					feLog("  skip original CacheOp\n");
				}
#endif
				continue;
			}

			//* TODO tag and skip repeats

			//* NOTE filter out unused switch inputs
			if(!pass)
			{
				if(!checkIndex && pPassThrough)
				{
					if(!isNetwork && pCheckNode!=pPassThrough)
					{
#if FE_CHO_SCAN
						if(!a_write)
						{
							feLog("  skip not network, with passthrough\n");
						}
#endif
						continue;
					}
					pCheckNode=pPassThrough;
					checkCount=1;
#if FE_CHO_SCAN
					if(!a_write)
					{
						feLog("  passthrough\n");
					}
#endif
				}
				else if(singleInput>=0 && checkIndex!=singleInput)
				{
#if FE_CHO_SCAN
					if(!a_write)
					{
						feLog("  skip wrong single input\n");
					}
#endif
					continue;
				}
			}

			//* NOTE make sure CacheOp gets cooked if anything changes
			if(inputIndex>=0)
			{
				a_pOpNode->getInput(inputIndex,mark_used);
			}

			UT_String utNodePath;
			pCheckNode->getRelativePathTo(NULL,utNodePath);

			const String nodePath=(char*)utNodePath;
			Stamp& rStamp=m_stampMap[nodePath];

			if(rStamp.m_hit)
			{
#if FE_CHO_SCAN
				if(!a_write)
				{
					feLog("  skip already hit\n");
				}
#endif
				//* NOTE to prevent blame string
				//* TODO don't count multiple times
				changeCount+=rStamp.m_upstreamChanges;
				continue;
			}

			const BWORD bypassed=pCheckNode->getBypass();

			HoudiniSOP* pHoudiniSOP=dynamic_cast<HoudiniSOP*>(pCheckNode);
			sp<OperatorSurfaceI> spOperatorSurfaceI=pHoudiniSOP?
					pHoudiniSOP->operatorSurface():
					sp<OperatorSurfaceI>(NULL);

			String toBlame;

			const U64 parmSerial=pCheckNode->getVersionParms();

			sp<Catalog> spCatalog=spOperatorSurfaceI;
			const I32 feSerial=spCatalog.isValid()?
					spCatalog->catalogOrDefault<I32>("Output Surface",
					"serial",-1): -2;

#if FE_CHO_SCAN
			if(!a_write)
			{
				const U32 inputCount=a_pOpNode->nInputs();
				const I32 cookCount=pCheckNode->getCookCount();

				feLog("  \"%s\" cookCount %d parm %d->%d fe %d->%d\n",
						nodePath.c_str(),cookCount,
						rStamp.m_parmSerial,parmSerial,
						rStamp.m_feSerial,feSerial);
			}
#endif

			if(a_write)
			{
				if(rStamp.m_parmSerial!=parmSerial ||
						rStamp.m_feSerial!=feSerial)
				{
					rStamp.m_parmSerial=parmSerial;
					rStamp.m_feSerial=feSerial;
					rStamp.m_expressions=nodeSignature(pCheckNode,FALSE);
					rStamp.m_values=nodeSignature(pCheckNode,TRUE);
				}
			}
			else
			{
				if(rStamp.m_feSerial!=feSerial)
				{
#if FE_CHO_BLAME
					feLog("CacheOp:scan \"%s\" BLAMES \"%s\" fe %d -> %d\n",
							name().c_str(),nodePath.c_str(),
							rStamp.m_feSerial,feSerial);
#endif

					toBlame=nodePath;
					changeCount++;
				}
				else if(rStamp.m_parmSerial!=parmSerial)
				{
					const String expressions=nodeSignature(pCheckNode,FALSE);
					String values;

					if(rStamp.m_expressions!=expressions)
					{
#if FE_CHO_BLAME
						feLog("CacheOp:scan \"%s\""
								" BLAMES \"%s\" parm %d -> %d\n",
								name().c_str(),nodePath.c_str(),
								rStamp.m_parmSerial,parmSerial);
						feLog("  expr sig \"%s\"\n",
								rStamp.m_expressions.c_str());
						feLog("  to       \"%s\"\n",
								expressions.c_str());
#endif

						toBlame=nodePath;
						changeCount++;
					}
					else if(m_currentFrame==m_lastFrame &&
							(values=nodeSignature(pCheckNode,TRUE))!=
							rStamp.m_values)
					{
						//* TODO do not cache frame for value-only changes
#if FE_CHO_BLAME
						feLog("CacheOp:scan \"%s\""
								" BLAMES \"%s\" %d -> %d\n",
								name().c_str(),nodePath.c_str(),
								rStamp.m_parmSerial,parmSerial);
						feLog("  value sig \"%s\"\n",
								rStamp.m_values.c_str());
						feLog("  to        \"%s\"\n",
								values.c_str());
#endif

						m_poisonFrame=m_currentFrame;
						toBlame=nodePath;
						changeCount++;
					}
#if FE_CHO_BLAME
					else
					{
						feLog("CacheOp:scan \"%s\" FAKE \"%s\" %d -> %d\n",
								name().c_str(),nodePath.c_str(),
								rStamp.m_parmSerial,parmSerial);
					}
#endif
				}

			}

			rStamp.m_hit=TRUE;

			I32 upstreamChanges=0;

			//* stop at other CacheOp nodes (serial param should provoke update)
			sp<CacheOp> spCacheOp=spOperatorSurfaceI;

			if(!bypassed && spCacheOp.isValid())
			{
				if(!a_write)
				{
#if FE_CHO_SCAN
					feLog("  \"%s\" deferring scan read to active CacheOp\n",
							(const char *)a_pOpNode->getName());
#endif
					if(spCacheOp->catalog<bool>("scan"))
					{
						upstreamChanges=spCacheOp->runScan(a_write);
					}
				}
			}
			else
			{
				upstreamChanges=scan(pCheckNode,a_write,FALSE);
			}

			changeCount+=upstreamChanges;

			if(!upstreamChanges && !toBlame.empty())
			{
				m_blame+=(m_blame.empty()? "": " ")+toBlame;
			}

			if(!a_write)
			{
				rStamp.m_upstreamChanges=upstreamChanges;
			}
		}
	}

	return changeCount;
}

String CacheOp::nodeSignature(OP_Node* a_pOpNode,BWORD a_valuesOnly)
{
	if(a_pOpNode->getBypass())
	{
		return "BYPASS";
	}

	//* NOTE cross-check with values_only_ on to see between-key value changes
	//* otherwise, adding key doesn't trigger a refresh

#ifdef FE_HOUDINI_USE_GA
	OP_SaveCommandOptions options(
			a_valuesOnly,	//* values_only_
			false,			//* defaultstoo_
			false,			//* docreate_
			true,			//* doparms_
			false,			//* doflags_
			false,			//* dowires_
			false,			//* dowiresout_
			true,			//* frames_
			false,			//* dogeneral_
			false,			//* dochblock_
			false,			//* dospareparms_
			true);			//* omit_version_info_

	std::ostringstream streamOut;
	a_pOpNode->saveCommand(streamOut,(const char *)a_pOpNode->getName(),
			0.0,0.0,"N",options);
	std::stringbuf* pStringBuf=streamOut.rdbuf();

	const String raw=pStringBuf->str().c_str();
	const String signature=raw
			.replace("\n#.*","")
			.replace("\nopcomment.*","")
			.replace("\nopcolor.*","")
			.replace("\nnbop.*","")
			.replace("\nopexprlanguage.*","")
			.replace(" \\( ","(")
			.replace(" \\) *",")")
			.replace("\nchkey -f (\\S+) -v (\\S+) .*-F (\\S+) \\S+",
			"\n:\\1=\\2@\\3")
			.replace("\n(\\S+)@'bezier\\(\\)'","\n\\1")
			.replace("\n.*@('.*)","\n\\1")
			.replace("\nchadd -f \\S+ \\S+ \\S+ (\\S+)","\\1")
			.replace("\nopparm \\S+\\s+","")
			.replace("\n","");

//	feLog(a_valuesOnly? "    VALUES\n": "    EXPRESSIONS\n");
//	feLog("    RAW\n\"%s\"\n",raw.c_str());
//	feLog("    SIG %s: \"%s\"\n",
//			(const char *)a_pOpNode->getName(),signature.c_str());

	return signature;
#else
	return "";
#endif
}

void CacheOp::clearCache(void)
{
	if(m_cacheCount)
	{
		catalog<I32>("Output Surface","serial")++;
	}

	reset();

	catalog<String>("summary")="CLEARED";

	if(catalog<bool>("changeColor"))
	{
		catalog<Color>("nodeColor")=catalog<SpatialVector>("resetColor");
	}
}

void CacheOp::changeHoard(String a_hoard)
{
	if(m_currentHoard==a_hoard)
	{
		return;
	}

	sp<Catalog> spMasterCatalog=
			registry()->master()->catalog();

	if(!m_currentHoard.empty())
	{
		const String keyFrom="CacheOp:"+m_currentHoard;

		Array< hp<Component> >& rFromArray=
				spMasterCatalog->catalog< Array< hp<Component> > >(keyFrom);

		const U32 hoardCount=rFromArray.size();
		for(U32 hoardIndex=0;hoardIndex<hoardCount;hoardIndex++)
		{
			sp<Component> spComponent=rFromArray[hoardIndex];
			if(spComponent.raw()==this)
			{
				rFromArray[hoardIndex]=rFromArray[hoardCount-1];
				rFromArray.resize(hoardCount-1);
				break;
			}
		}
	}

	if(!a_hoard.empty())
	{
		const String keyTo="CacheOp:"+a_hoard;

		Array< hp<Component> >& rToArray=
				spMasterCatalog->catalog< Array< hp<Component> > >(keyTo);
		rToArray.push_back(sp<Component>(this));
	}

	m_currentHoard=a_hoard;
}

void CacheOp::changeGlobal(BWORD a_include)
{
	sp<Catalog> spMasterCatalog=
			registry()->master()->catalog();

	const String keyGlobal="CacheOp:"+String(FE_CHO_GLOBAL);

	Array< hp<Component> >& rGlobalArray=
			spMasterCatalog->catalog< Array< hp<Component> > >(keyGlobal);

	if(a_include)
	{
		rGlobalArray.push_back(sp<Component>(this));
		return;
	}

	const U32 hoardCount=rGlobalArray.size();
	for(U32 hoardIndex=0;hoardIndex<hoardCount;hoardIndex++)
	{
		sp<Component> spComponent=rGlobalArray[hoardIndex];
		if(spComponent.raw()==this)
		{
			rGlobalArray[hoardIndex]=rGlobalArray[hoardCount-1];
			rGlobalArray.resize(hoardCount-1);
			return;
		}
	}
}

//* NOTE could be static aside from registry access
void CacheOp::findHoardCaches(String a_hoard,BWORD a_clear)
{
	String& rCollaborators=catalog<String>("collaborators");
	if(!a_clear)
	{
		rCollaborators="";
	}

	if(a_hoard.empty())
	{
		return;
	}

	sp<Catalog> spMasterCatalog=
			registry()->master()->catalog();

	const String keyname="CacheOp:"+a_hoard;

	Array< hp<Component> >& rComponentArray=
			spMasterCatalog->catalog< Array< hp<Component> > >(
			keyname);

	const U32 hoardCount=rComponentArray.size();
	for(U32 hoardIndex=0;hoardIndex<hoardCount;hoardIndex++)
	{
		sp<Component> spComponent=rComponentArray[hoardIndex];
		if(spComponent.isValid())
		{
#if FE_CHO_SCAN
			feLog("CacheOp::findHoardCaches %d/%d \"%s\"\n",
					hoardIndex,hoardCount,spComponent->name().c_str());
#endif

			sp<CacheOp> spCacheOp=spComponent;
			if(spCacheOp.isValid())
			{
				if(!a_clear)
				{
					if(spCacheOp.raw()!=this)
					{
						rCollaborators+=spCacheOp->name()+"\n";
					}
					continue;
				}

				spCacheOp->clearCache();

				if(spCacheOp.raw()!=this)
				{
#if FE_CHO_SCAN
					feLog("  dirty \"%s\"\n",spCacheOp->name().c_str());
#endif

					const BWORD aggresive=TRUE;
					spCacheOp->dirty(aggresive);
				}
			}
		}
	}
}
