/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <houdini/houdiniEngine.pmh>

namespace fe
{
namespace ext
{

String SurfaceAccessorHapi::type(void) const
{
	if(!m_attrType.empty())
	{
		return m_attrType;
	}

	String attrType ="integer";

	//* TODO

//~	if(m_handle.isSharedStringAttribute())
//~	{
//~		attrType="string";
//~	}
//~
//~	if(m_handle.isFloatOrVectorAttribute())
//~	{
//~		const int floatCount=m_handle.getFloatCount(false);
//~
//~		switch(floatCount)
//~		{
//~			case 1:
//~				attrType="real";
//~				break;
//~			case 2:
//~				attrType="vector2";
//~				break;
//~			case 3:
//~				attrType="vector3";
//~				break;
//~			case 4:
//~				attrType="vector4";
//~				break;
//~			default:
//~				;
//~		}
//~	}
//~
//~	const_cast<SurfaceAccessorHapi*>(this)->m_attrType=attrType;

	return m_attrType;
}

U32 SurfaceAccessorHapi::count(void) const
{
	FEASSERT(m_pHapiSession);

	if(m_attribute==SurfaceAccessibleI::e_position &&
			!isBound())
	{
		return 0;
	}

	if(m_element==SurfaceAccessibleI::e_detail)
	{
		return 1;
	}

	if(m_attribute==SurfaceAccessibleI::e_vertices ||
			m_attribute==SurfaceAccessibleI::e_properties)
	{
		return m_vertexCount.size();
	}

	if(m_stringBuffer.size())
	{
		return m_stringBuffer.size();
	}

	if(m_integerBuffer.size())
	{
		return m_integerBuffer.size();
	}

	if(m_realBuffer.size())
	{
		return m_realBuffer.size();
	}

	if(m_vectorBuffer.size())
	{
		return m_vectorBuffer.size();
	}

	return 0;
}

U32 SurfaceAccessorHapi::subCount(U32 a_index) const
{
	if(m_attribute!=SurfaceAccessibleI::e_generic &&
			m_attribute!=
			SurfaceAccessibleI::e_vertices &&
			m_attribute!=
			SurfaceAccessibleI::e_uv &&
			m_attribute!=
			SurfaceAccessibleI::e_color &&
			m_attribute!=
			SurfaceAccessibleI::e_properties &&
			!isBound())
	{
		return 0;
	}

	const BWORD isPrimitiveUV=
			((m_element==SurfaceAccessibleI::e_primitive
			||m_element==SurfaceAccessibleI::e_vertex
			|| m_element==
			SurfaceAccessibleI::e_primitiveGroup) &&
			m_attribute==SurfaceAccessibleI::e_uv);

	if(m_element==
			SurfaceAccessibleI::e_primitiveGroup ||
			m_element==
			SurfaceAccessibleI::e_vertex ||
			m_attribute==SurfaceAccessibleI::e_vertices
			|| isPrimitiveUV)
	{
		FEASSERT(m_element==
				SurfaceAccessibleI::e_primitiveGroup
				|| m_element==
				SurfaceAccessibleI::e_vertex
				|| m_element==
				SurfaceAccessibleI::e_primitive);

		return m_vertexCount[a_index];
	}

	return 1;
}

BWORD SurfaceAccessorHapi::bindInternal(
	SurfaceAccessibleI::Element a_element,const String& a_name)
{
	FEASSERT(m_pHapiSession);

	if(HAPI_IsSessionValid(m_pHapiSession)!=HAPI_RESULT_SUCCESS)
	{
		feLog("SurfaceAccessorHapi::bindInternal invalid session\n");
		return FALSE;
	}

	//* TEMP
//~	if(m_attribute!=SurfaceAccessibleI::e_position &&
//~			m_attribute!=SurfaceAccessibleI::e_vertices)
//~	{
//~		return FALSE;
//~	}

	m_curveParts=0;
	m_meshParts=0;

	m_element=SurfaceAccessibleI::e_point;
	if(a_element>=0 && a_element<=5)
	{
		m_element=a_element;
	}
	m_attrName=a_name;

	if(m_element==SurfaceAccessibleI::e_primitiveGroup && m_attrName.empty())
	{
		m_element=SurfaceAccessibleI::e_primitive;
		m_attribute=SurfaceAccessibleI::e_vertices;
		return TRUE;
	}

	HAPI_Result result;

	HAPI_GeoInfo geo_info;
	result=HAPI_GetGeoInfo(m_pHapiSession,m_hapiNodeId,&geo_info);
	if(result!=HAPI_RESULT_SUCCESS)
	{
		feLog("SurfaceAccessorHapi::bindInternal"
				" get geo info failed on %d\n  \"%s\"\n",m_hapiNodeId,
				SurfaceAccessibleHapi::lastError(m_pHapiSession).c_str());
	}

	const I32 partCount=geo_info.partCount;
	if(partCount<1)
	{
		feLog("SurfaceAccessorHapi::bindInternal no parts\n");
		return FALSE;
	}

	HAPI_AttributeOwner owner=HAPI_ATTROWNER_INVALID;
	switch(m_element)
	{
		case SurfaceAccessibleI::e_point:
			owner=HAPI_ATTROWNER_POINT;
			break;

		case SurfaceAccessibleI::e_vertex:
			owner=HAPI_ATTROWNER_VERTEX;
			break;

		case SurfaceAccessibleI::e_primitive:
			owner=HAPI_ATTROWNER_PRIM;
			break;

		case SurfaceAccessibleI::e_pointGroup:
		case SurfaceAccessibleI::e_primitiveGroup:
			break;

		case SurfaceAccessibleI::e_detail:
			owner=HAPI_ATTROWNER_DETAIL;
			break;

		default:
			;
	}

	if(m_attribute==SurfaceAccessibleI::e_vertices ||
			m_attribute==SurfaceAccessibleI::e_properties)
	{
		owner=HAPI_ATTROWNER_POINT;
	}

	m_vertexBuffer.resize(0);

	for(I32 partIndex=0;partIndex<partCount;partIndex++)
	{
		const HAPI_PartId part_id=partIndex;
		HAPI_PartInfo part_info;

		result=HAPI_GetPartInfo(m_pHapiSession,m_hapiNodeId,part_id,&part_info);
		if(result!=HAPI_RESULT_SUCCESS)
		{
			feLog("SurfaceAccessorHapi::bindInternal"
					" get part info failed on %d part %d/%d\n  \"%s\"\n",
					m_hapiNodeId,partIndex,partCount,
					SurfaceAccessibleHapi::lastError(m_pHapiSession).c_str());
			return FALSE;
		}

//		feLog("  part %d/%d points %d verts %d prims %d\n",
//				partIndex,partCount,part_info.pointCount,
//				part_info.vertexCount,part_info.faceCount);

		const I32 pointCount=part_info.pointCount;
		const I32 vertexCount=part_info.vertexCount;
		const I32 faceCount=part_info.faceCount;

		I32 elementCount=0;

		switch(m_element)
		{
			case SurfaceAccessibleI::e_point:
				elementCount=pointCount;
				break;

			case SurfaceAccessibleI::e_vertex:
				elementCount=vertexCount;
				break;

			case SurfaceAccessibleI::e_primitive:
				elementCount=faceCount;
				break;

			case SurfaceAccessibleI::e_pointGroup:
			case SurfaceAccessibleI::e_primitiveGroup:
				break;

			case SurfaceAccessibleI::e_detail:
				elementCount=1;
				break;

			default:
				;
		}

		if(m_attribute==SurfaceAccessibleI::e_vertices ||
				m_attribute==SurfaceAccessibleI::e_properties)
		{
			elementCount=pointCount;
		}

		HAPI_AttributeInfo attr_info;

		const String hapiAttrName=
				(m_attribute==SurfaceAccessibleI::e_vertices ||
				m_attribute==SurfaceAccessibleI::e_properties)?
				"P": a_name;

		result=HAPI_GetAttributeInfo(m_pHapiSession,m_hapiNodeId,part_id,
				hapiAttrName.c_str(),owner,&attr_info);
		if(result!=HAPI_RESULT_SUCCESS)
		{
			feLog("SurfaceAccessorHapi::bindInternal"
					" get attr info failed on %d part %d/%d\n  \"%s\"\n",
					m_hapiNodeId,partIndex,partCount,
					SurfaceAccessibleHapi::lastError(m_pHapiSession).c_str());
			return FALSE;
		}

		if(!attr_info.exists)
		{
			continue;
		}

//		feLog("  attr count %d tupleSize %d\n",
//				attr_info.count,attr_info.tupleSize);

		const I32 tupleSize=attr_info.tupleSize;

		const I32 stride= -1;
		int subCount_array[faceCount];
		int vertex_list_array[vertexCount];
		BWORD isMeshes=FALSE;

		result=HAPI_GetFaceCounts(m_pHapiSession,m_hapiNodeId,part_id,
				subCount_array,0,faceCount);
		if(result==HAPI_RESULT_SUCCESS)
		{
			m_meshParts++;
			isMeshes=TRUE;

			result=HAPI_GetVertexList(m_pHapiSession,m_hapiNodeId,part_id,
					vertex_list_array,0,vertexCount);
			if(result!=HAPI_RESULT_SUCCESS)
			{
				feLog("SurfaceAccessorHapi::bindInternal"
						" get vert list failed on %d part %d/%d\n  \"%s\"\n",
						m_hapiNodeId,partIndex,partCount,
						SurfaceAccessibleHapi::lastError(m_pHapiSession).c_str());
				return FALSE;
			}
		}
		else
		{
			result=HAPI_GetCurveCounts(m_pHapiSession,m_hapiNodeId,part_id,
					subCount_array,0,faceCount);
			if(result!=HAPI_RESULT_SUCCESS)
			{
				feLog("SurfaceAccessorHapi::bindInternal"
						" get counts failed on %d part %d/%d\n  \"%s\"\n",
						m_hapiNodeId,partIndex,partCount,
						SurfaceAccessibleHapi::lastError(m_pHapiSession).c_str());
				continue;
			}
			m_curveParts++;
		}


		//* TODO accumulate from multiple parts

		const HAPI_StorageType storage=attr_info.storage;

		//* TODO only one
		HAPI_StringHandle string_array[elementCount*tupleSize];
		int int_array[elementCount*tupleSize];
		float float_array[elementCount*tupleSize];

		if(storage==HAPI_STORAGETYPE_STRING)
		{
			result=HAPI_GetAttributeStringData(m_pHapiSession,m_hapiNodeId,
					part_id,hapiAttrName.c_str(),&attr_info,
					string_array,0,elementCount);
			if(result!=HAPI_RESULT_SUCCESS)
			{
				feLog("SurfaceAccessorHapi::bindInternal"
						" get string attr data failed on %d part %d/%d\n  \"%s\"\n",
						m_hapiNodeId,partIndex,partCount,
						SurfaceAccessibleHapi::lastError(
						m_pHapiSession).c_str());
				return FALSE;
			}
		}
		else if(storage==HAPI_STORAGETYPE_INT)
		{
			result=HAPI_GetAttributeIntData(m_pHapiSession,m_hapiNodeId,
					part_id,hapiAttrName.c_str(),&attr_info,
					stride,int_array,0,elementCount);
			if(result!=HAPI_RESULT_SUCCESS)
			{
				feLog("SurfaceAccessorHapi::bindInternal"
						" get int attr data failed on %d part %d/%d\n  \"%s\"\n",
						m_hapiNodeId,partIndex,partCount,
						SurfaceAccessibleHapi::lastError(
						m_pHapiSession).c_str());
				return FALSE;
			}
		}
		else if(storage==HAPI_STORAGETYPE_FLOAT)
		{
			result=HAPI_GetAttributeFloatData(m_pHapiSession,m_hapiNodeId,
					part_id,hapiAttrName.c_str(),&attr_info,
					stride,float_array,0,elementCount);
			if(result!=HAPI_RESULT_SUCCESS)
			{
				feLog("SurfaceAccessorHapi::bindInternal"
						" get float attr data failed on %d part %d/%d\n  \"%s\"\n",
						m_hapiNodeId,partIndex,partCount,
						SurfaceAccessibleHapi::lastError(
						m_pHapiSession).c_str());
				return FALSE;
			}
		}

		if(storage==HAPI_STORAGETYPE_STRING && tupleSize==1)
		{
			m_attrType="string";
			m_stringBuffer.resize(elementCount);

			int length=1024;
			char string_value[length];
			string_value[0]=0;

			for(I32 elementIndex=0;elementIndex<elementCount;elementIndex++)
			{
				result=HAPI_GetString(m_pHapiSession,string_array[elementIndex],
						string_value,length);
				if(result!=HAPI_RESULT_SUCCESS)
				{
					feLog("SurfaceAccessorHapi::bindInternal"
							" get string failed on %d part %d/%d\n  \"%s\"\n",
							m_hapiNodeId,partIndex,partCount,
							SurfaceAccessibleHapi::lastError(
							m_pHapiSession).c_str());
				}

				m_stringBuffer[elementIndex]=string_value;
			}
		}
		else if(storage==HAPI_STORAGETYPE_INT && tupleSize==1)
		{
			m_attrType="integer";
			m_integerBuffer.resize(elementCount);

			for(I32 elementIndex=0;elementIndex<elementCount;elementIndex++)
			{
				m_integerBuffer[elementIndex]=int_array[elementIndex];
			}
		}
		else if(storage==HAPI_STORAGETYPE_FLOAT && tupleSize==1)
		{
			m_attrType="real";
			m_realBuffer.resize(elementCount);

			for(I32 elementIndex=0;elementIndex<elementCount;elementIndex++)
			{
				m_realBuffer[elementIndex]=float_array[elementIndex];
			}
		}
		else if(storage==HAPI_STORAGETYPE_FLOAT && tupleSize==3)
		{
			m_attrType="vector3";
			m_vectorBuffer.resize(elementCount);

			for(I32 elementIndex=0;elementIndex<elementCount;elementIndex++)
			{
				const I32 elementIndex3=elementIndex*3;
				fe::set(m_vectorBuffer[elementIndex],
						float_array[elementIndex3],
						float_array[elementIndex3+1],
						float_array[elementIndex3+2]);
			}
		}

		m_vertexOffset.resize(faceCount);
		m_vertexCount.resize(faceCount);

		if(isMeshes)
		{
			m_vertexBuffer.resize(vertexCount);
			for(I32 vertexIndex=0;vertexIndex<vertexCount;vertexIndex++)
			{
				m_vertexBuffer[vertexIndex]=vertex_list_array[vertexIndex];
			}
		}

		I32 offset=0;
		for(I32 faceIndex=0;faceIndex<faceCount;faceIndex++)
		{
			const I32 subCount=subCount_array[faceIndex];

			m_vertexOffset[faceIndex]=offset;
			m_vertexCount[faceIndex]=subCount;
			offset+=subCount;
		}
	}

	return isBound();
}

} /* namespace ext */
} /* namespace fe */
