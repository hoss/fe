/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <houdini/houdini.pmh>

#define FE_HPC_DEBUG	FALSE

extern "C"
{

FE_DL_EXPORT void newGeometryPrim(GA_PrimitiveFactory* pFactory)
{
#if FE_HPC_DEBUG
	feLog("newGeometryPrim\n");
#endif

	fe::ext::HoudiniPrimComponent::registerMyself(pFactory);

#if FE_HPC_DEBUG
	feLog("newGeometryPrim done\n");
#endif
}

}

namespace fe
{
namespace ext
{

GA_PrimitiveDefinition* HoudiniPrimComponent::ms_pDefinition=NULL;

HoudiniPrimComponent::HoudiniPrimComponent(GA_Detail &d, GA_Offset offset):
    HoudiniPrimBase(&d, offset)
{
#if FE_HPC_DEBUG
	feLog("HoudiniPrimComponent::HoudiniPrimComponent\n");
#endif
}

HoudiniPrimComponent::HoudiniPrimComponent(const GA_MergeMap &map,
	GA_Detail &detail,
	GA_Offset offset,
	const HoudiniPrimComponent &src):
    HoudiniPrimBase(&detail, offset)
{
#if FE_HPC_DEBUG
	feLog("HoudiniPrimComponent::HoudiniPrimComponent (merge)\n");
#endif

	m_spPayload=src.m_spPayload;
}

HoudiniPrimComponent::~HoudiniPrimComponent()
{
#if FE_HPC_DEBUG
	feLog("HoudiniPrimComponent::~HoudiniPrimComponent\n");
#endif
}

void HoudiniPrimComponent::copyPrimitive(const GEO_Primitive* src)
{
	const HoudiniPrimComponent* pHoudiniPrimComponent=
			static_cast<const HoudiniPrimComponent*>(src);

#if FE_HPC_DEBUG
	feLog("HoudiniPrimComponent::copyPrimitive %p\n",
			pHoudiniPrimComponent);
#endif

	if(pHoudiniPrimComponent)
	{
		m_spPayload=pHoudiniPrimComponent->m_spPayload;
	}
}

void HoudiniPrimComponent::copyOffsetPrimitive(const GEO_Primitive* src,
									GA_Index basept)
{
	const HoudiniPrimComponent* pHoudiniPrimComponent=
			static_cast<const HoudiniPrimComponent*>(src);

#if FE_HPC_DEBUG
	feLog("HoudiniPrimComponent::copyOffsetPrimitive %p\n",
			pHoudiniPrimComponent);
#endif

	if(pHoudiniPrimComponent)
	{
		m_spPayload=pHoudiniPrimComponent->m_spPayload;
	}
}

void HoudiniPrimComponent::copyUnwiredForMerge(const GA_Primitive *src,
									const GA_MergeMap& map)
{
	const HoudiniPrimComponent* pHoudiniPrimComponent=
			static_cast<const HoudiniPrimComponent*>(src);

#if FE_HPC_DEBUG
	feLog("HoudiniPrimComponent::copyUnwiredForMerge %p\n",
			pHoudiniPrimComponent);
#endif

	if(pHoudiniPrimComponent)
	{
		m_spPayload=pHoudiniPrimComponent->m_spPayload;
	}
}

static UT_String s_evalPayload(const HoudiniPrimComponent* pPrimitive)
{
	if(!pPrimitive)
	{
		return "NULL";
	}

	sp<Component> spPayload=pPrimitive->payload();
	if(spPayload.isNull())
	{
		return "INVALID";
	}

	return spPayload->name().c_str();
}

GA_START_INTRINSIC_DEF(HoudiniPrimComponent,1)
    GA_INTRINSIC_S(HoudiniPrimComponent,0,"payload",s_evalPayload);
GA_END_INTRINSIC_DEF(HoudiniPrimComponent, HoudiniPrimBase)

//*	https://www.sidefx.com/docs/hdk/_g_e_o___prim_tetra_8_c_source.html

#if FE_HOUDINI_16_5_PLUS
static void s_newPrimComponent(
	GA_Primitive **new_prims,
	GA_Size nprimitives,
	GA_Detail &gdp,
	GA_Offset start_offset,
	const GA_PrimitiveDefinition &def,
	bool allowed_to_parallelize)
{
#if FE_HPC_DEBUG
	feLog("HoudiniPrimComponent s_newPrimComponent\n");
#endif
	if (allowed_to_parallelize && nprimitives>=4*GA_PAGE_SIZE)
	{
		// Allocate them in parallel if we're allocating many.
		// This is using the C++11 lambda syntax to make a functor.
		UTparallelForLightItems(
				UT_BlockedRange<GA_Offset>(start_offset,
				start_offset+nprimitives),

				[new_prims,&gdp,start_offset](
				const UT_BlockedRange<GA_Offset> &r)
				{
					GA_Offset primoff(r.begin());
					GA_Primitive **pprims = new_prims+(primoff-start_offset);
					GA_Offset endprimoff(r.end());
					for(;primoff!=endprimoff;++primoff,++pprims)
					{
						*pprims=new fe::ext::HoudiniPrimComponent(gdp, primoff);
					}
				}
		);
	}
	else
	{
		// Allocate them serially if we're only allocating a few.
		GA_Offset endprimoff(start_offset + nprimitives);
		for (GA_Offset primoff(start_offset);primoff!=endprimoff;
				++primoff,++new_prims)
		{
			*new_prims=new fe::ext::HoudiniPrimComponent(gdp, primoff);
		}
	}
}
#elif FE_HOUDINI_16_PLUS
static void s_newPrimComponent(
	GA_Primitive **new_prims,
	GA_Size nprimitives,
	GA_Detail &gdp,
	GA_Offset start_offset,
	const GA_PrimitiveDefinition &def)
{
#if FE_HPC_DEBUG
	feLog("HoudiniPrimComponent s_newPrimComponent\n");
#endif
	GA_Offset endprimoff(start_offset + nprimitives);
	for (GA_Offset primoff(start_offset);primoff!=endprimoff;
			++primoff, ++new_prims)
	{
		*new_prims = new fe::ext::HoudiniPrimComponent(gdp, primoff);
	}
}
#else
static GA_Primitive* s_newPrimComponent(GA_Detail& detail,GA_Offset offset,
	const GA_PrimitiveDefinition&)
{
#if FE_HPC_DEBUG
	feLog("HoudiniPrimComponent s_newPrimComponent\n");
#endif
    return new fe::ext::HoudiniPrimComponent(detail, offset);
}

static GA_Primitive* s_mergePrimComponent(const GA_MergeMap &map,
    GA_Detail &detail,GA_Offset offset,const GA_Primitive &src)
{
    return new fe::ext::HoudiniPrimComponent(map,detail,offset,
			static_cast<const fe::ext::HoudiniPrimComponent&>(src));
}
#endif

//* static
void HoudiniPrimComponent::registerMyself(GA_PrimitiveFactory* pFactory)
{
#if FE_HPC_DEBUG
	feLog("HoudiniPrimComponent::registerMyself %p\n",ms_pDefinition);
#endif

    if(ms_pDefinition)
	{
		return;
	}

    ms_pDefinition=pFactory->registerDefinition(
			"fe::Component",
			s_newPrimComponent,
			GA_FAMILY_NONE);

    ms_pDefinition->setLabel("FE Component");

#if !FE_HOUDINI_16_PLUS
    ms_pDefinition->setMergeConstructor(s_mergePrimComponent);
#endif

    registerIntrinsics(*ms_pDefinition);

    GUI_PrimitiveHookFlags collect_type=GUI_HOOK_FLAG_NONE;
    const int hook_priority=0;

    DM_RenderTable::getTable()->registerGEOHook(
			new HoudiniRenderHookComponent,
			ms_pDefinition->getId(),hook_priority,collect_type);
}

} /* namespace ext */
} /* namespace fe */
