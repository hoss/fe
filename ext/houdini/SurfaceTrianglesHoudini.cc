/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <houdini/houdini.pmh>

#define FE_STH_DEBUG		FALSE
#define	FE_STH_WARNINGS		(FE_CODEGEN<=FE_DEBUG)

namespace fe
{
namespace ext
{

SurfaceTrianglesHoudini::SurfaceTrianglesHoudini(void):
	m_pGdp(NULL),
	m_subIndex(-1)
{
#if FE_STH_DEBUG
	feLog("SurfaceTrianglesHoudini()\n");
#endif
}

SurfaceTrianglesHoudini::~SurfaceTrianglesHoudini(void)
{
}

Protectable* SurfaceTrianglesHoudini::clone(Protectable* pInstance)
{
#if FE_STH_DEBUG
	feLog("SurfaceTrianglesHoudini::clone\n");
#endif

	SurfaceTrianglesHoudini* pSurfaceTrianglesHoudini= pInstance?
			fe_cast<SurfaceTrianglesHoudini>(pInstance):
			new SurfaceTrianglesHoudini();

	SurfaceTriangles::clone(pSurfaceTrianglesHoudini);

	// TODO copy attributes

	return pSurfaceTrianglesHoudini;
}

//* TODO check with a mix of polygons, meshes, and strips

void SurfaceTrianglesHoudini::cache(void)
{
#if FE_STH_DEBUG
	feLog("SurfaceTrianglesHoudini::cache\n");
#endif

	//* NOTE not doing any Record serialization

	//* convert Houdini data to vertex and normal arrays

	if(!m_pGdp)
	{
		feLog("SurfaceTrianglesHoudini::cache NULL gdp\n");
		return;
	}

	setOptionalArrays(Arrays(e_arrayColor|e_arrayUV));

#ifdef FE_HOUDINI_USE_GA
	const GA_PrimitiveGroup* pPrimitiveGroup=NULL;
#else
	GB_PrimitiveGroup* pPrimitiveGroup=NULL;
#endif
	if(!group().empty())
	{
		pPrimitiveGroup=m_pGdp->findPrimitiveGroup(group().c_str());
	}

#ifdef FE_HOUDINI_USE_GA
	GEO_PrimList primList=m_pGdp->primitives();
	const I32 entries=pPrimitiveGroup?
			pPrimitiveGroup->entries(): m_pGdp->getNumPrimitives();
#else
	const I32 entries=pPrimitiveGroup?
			pPrimitiveGroup->entries(): m_pGdp->primitives().entries();
#endif

	if(m_subIndex>=entries)
	{
		feLog("SurfaceTrianglesHoudini::cache excessive subIndex %d/%d\n",
				m_subIndex,entries);
		return;
	}
	const U32 primitiveCount=(m_subIndex>=0)? 1: entries;

	m_elements=primitiveCount;
	m_vertices=0;
	U32 elementAllocated=0;
	U32 vertexAllocated=0;
	BWORD keepUV=FALSE;

#ifdef FE_HOUDINI_USE_GA
	GEO_AttributeHandle handlePointNormal=
			m_pGdp->getAttribute(GA_ATTRIB_POINT,GEO_STD_ATTRIB_NORMAL);
	GEO_AttributeHandle handleVertexNormal=
			m_pGdp->getAttribute(GA_ATTRIB_VERTEX,GEO_STD_ATTRIB_NORMAL);

	GEO_AttributeHandle handlePointColor=
			m_pGdp->getAttribute(GA_ATTRIB_POINT,"Cd");
	GEO_AttributeHandle handleVertexColor=
			m_pGdp->getAttribute(GA_ATTRIB_VERTEX,"Cd");

	GEO_AttributeHandle handlePointUV=
			m_pGdp->getAttribute(GA_ATTRIB_POINT,"uv");
	GEO_AttributeHandle handleVertexUV=
			m_pGdp->getAttribute(GA_ATTRIB_VERTEX,"uv");
#else
	GEO_AttributeHandle handlePointNormal=
			m_pGdp->getAttribute(GEO_POINT_DICT,GEO_STD_ATTRIB_NORMAL);
	GEO_AttributeHandle handleVertexNormal=
			m_pGdp->getAttribute(GEO_VERTEX_DICT,GEO_STD_ATTRIB_NORMAL);

	GEO_AttributeHandle handlePointColor=
			m_pGdp->getAttribute(GEO_POINT_DICT,"Cd");
	GEO_AttributeHandle handleVertexColor=
			m_pGdp->getAttribute(GEO_VERTEX_DICT,"Cd");

	GEO_AttributeHandle handlePointUV=
			m_pGdp->getAttribute(GEO_POINT_DICT,"uv");
	GEO_AttributeHandle handleVertexUV=
			m_pGdp->getAttribute(GEO_VERTEX_DICT,"uv");
#endif

	GEO_AttributeHandle handlePartition=
			m_pGdp->getPrimAttribute(m_partitionAttr.c_str());

	const GEO_Primitive* pGroupPrimitive=NULL;
	U32 triangleIndex=0;
	U32 total=0;
	for(U32 m=0;m<primitiveCount;m++)
	{
		if(pPrimitiveGroup)
		{
			if(!m)
			{
#ifdef FE_HOUDINI_USE_GA
				pGroupPrimitive=primList.head(*pPrimitiveGroup);
#else
				pGroupPrimitive=m_pGdp->primitives().head(*pPrimitiveGroup);
#endif
				if(m_subIndex>=0)
				{
					for(U32 n=0;n<m_subIndex;n++)
					{
						FEASSERT(pGroupPrimitive);
#ifdef FE_HOUDINI_USE_GA
						pGroupPrimitive=primList.next(
								pGroupPrimitive,*pPrimitiveGroup);
#else
						pGroupPrimitive=m_pGdp->primitives().next(
								pGroupPrimitive,*pPrimitiveGroup);
#endif
					}
				}
			}
			else
			{
				FEASSERT(pGroupPrimitive);
#ifdef FE_HOUDINI_USE_GA
				pGroupPrimitive=primList.next(
						pGroupPrimitive,*pPrimitiveGroup);
#else
				pGroupPrimitive=m_pGdp->primitives().next(
						pGroupPrimitive,*pPrimitiveGroup);
#endif
			}
		}

		const U32 index=(m_subIndex>=0)? m_subIndex: m;

#ifdef FE_HOUDINI_USE_GA
		const GEO_Primitive* pPrimitive=pPrimitiveGroup?
				pGroupPrimitive: m_pGdp->getGEOPrimitive(
				m_pGdp->primitiveOffset(index));

		const I32 primitiveIndex=pPrimitiveGroup?
				m_pGdp->primitiveIndex(pPrimitive->getMapOffset()): m;
#else
		const GEO_Primitive* pPrimitive=pPrimitiveGroup?
				pGroupPrimitive: m_pGdp->primitives()(index);
		const I32 primitiveIndex=pPrimitive->getNum();
#endif

		if(!pPrimitive)
		{
			break;
		}

		I32 partitionIndex= -1;
		handlePartition.setElement(pPrimitive);
		if(handlePartition.isAttributeValid())
		{
			UT_String utString;
			handlePartition.getString(utString,0);
			const String partition=utString.buffer();
			partitionIndex=lookupPartition(partition);
		}

#ifdef FE_HOUDINI_USE_GA
		if(pPrimitive->getTypeId().get()==GEO_PRIMMESH)
#else
		if(pPrimitive->getPrimitiveId()==GEOPRIMMESH)
#endif
		{
			//* NOTE castTo doesn't work, but RTTI does
//			const GEO_PrimMesh* pPrimMesh=
//					(const GEO_PrimMesh*)pPrimitive->castTo();
			const GEO_PrimMesh* pPrimMesh=
					dynamic_cast<const GEO_PrimMesh*>(pPrimitive);

			const U32 columns=pPrimMesh->getNumCols();
			const U32 rows=pPrimMesh->getNumRows();
			const BWORD wrapU=pPrimMesh->isWrappedU();
			const BWORD wrapV=pPrimMesh->isWrappedV();
#if FE_STH_DEBUG
			feLog("rows %d columns %d wrapped %d %d\n",
					rows,columns,wrapU,wrapV);
#endif

			if(!columns || !rows)
			{
				feLog("SurfaceTrianglesHoudini::cache as GEO_PrimMesh"
						" primitive %d/%d columns=%d rows=%d (degenerate)\n",
						index,primitiveCount,columns,rows);
				continue;
			}

			U32 countU=columns+wrapU-1;
			U32 countV=rows+wrapV-1;

			m_vertices+=6*countU*countV;

			resizeFor(m_vertices/2,m_vertices,
					elementAllocated,vertexAllocated);

			SpatialVector* pVert0= new SpatialVector[columns];	// first
			SpatialVector* pVert1= new SpatialVector[columns];	// previous
			SpatialVector* pVert2= new SpatialVector[columns];	// current
			SpatialVector* pNorm0= new SpatialVector[columns];
			SpatialVector* pNorm1= new SpatialVector[columns];
			SpatialVector* pNorm2= new SpatialVector[columns];
			Color* pColor0=new Color[columns];
			Color* pColor1=new Color[columns];
			Color* pColor2=new Color[columns];
			Vector2* pUv0=new Vector2[columns];
			Vector2* pUv1=new Vector2[columns];
			Vector2* pUv2=new Vector2[columns];
			I32* pNumber0=new I32[columns];			// point index
			I32* pNumber1=new I32[columns];			// point index
			I32* pNumber2=new I32[columns];			// point index

			for(U32 v=0;v<rows+wrapV;v++)
			{
				if(v)
				{
					for(U32 u=0;u<columns;u++)
					{
						pVert1[u]=pVert2[u];
						pNorm1[u]=pNorm2[u];
						pColor1[u]=pColor2[u];
						pUv1[u]=pUv2[u];
						pNumber1[u]=pNumber2[u];
					}
				}
				if(v<rows)
				{
					for(U32 u=0;u<columns;u++)
					{
#ifdef FE_HOUDINI_USE_GA
						const GEO_Vertex geoVertex=
								pPrimMesh->getVertexElement(v,u);
#else
						const GEO_Vertex& geoVertex=pPrimMesh->getVertex(v,u);
#endif

						//* location
						UT_Vector4 utVector4=geoVertex.getPos();
						pVert2[u]=Vector3f(utVector4.data());

						//* normal

#ifdef FE_HOUDINI_USE_GA
						const GA_Index pointIndex=geoVertex.getPointIndex();

//						const GEO_Point* pPoint=m_pGdp->getGEOPoint(
//								m_pGdp->pointOffset(pointIndex));

						const GA_Offset pointOffset=geoVertex.getPointOffset();
						handlePointNormal.setPoint(pointOffset);
#else
						const U32 pointIndex=geoVertex.getBasePt()->getNum();
						const GEO_Point* pPoint=m_pGdp->points()(pointIndex);
						handlePointNormal.setElement(pPoint);
#endif

						if(handlePointNormal.isAttributeValid())
						{
							utVector4=handlePointNormal.getV4(0);
							pNorm2[u]=Vector3f(utVector4.data());
						}
						else
						{
							handleVertexNormal.setElement(&geoVertex);
							if(handleVertexNormal.isAttributeValid())
							{
								utVector4=handleVertexNormal.getV4(0);
								pNorm2[u]=Vector3f(utVector4.data());
							}
							else
							{
								set(m_pNormalArray[total],0.0,1.0,0.0);
							}
						}

						handleVertexColor.setElement(&geoVertex);
						if(handleVertexColor.isAttributeValid())
						{
							UT_Vector3 utVector3=handleVertexColor.getV3(0);
							pColor2[u]=Vector3f(utVector3.data());
						}
						else
						{
#ifdef FE_HOUDINI_USE_GA
							handlePointColor.setPoint(pointOffset);
#else
							handlePointColor.setElement(pPoint);
#endif
							if(handlePointColor.isAttributeValid())
							{
								UT_Vector3 utVector3=handlePointColor.getV3(0);
								pColor2[u]=Vector3f(utVector3.data());
							}
							else
							{
								set(pColor2[u],u/(columns-1.0),v/(rows-1.0));
							}
						}

						handleVertexUV.setElement(&geoVertex);
						if(handleVertexUV.isAttributeValid())
						{
							UT_Vector3 utVector3=handleVertexUV.getV3(0);
							pUv2[u]=Vector2f(utVector3.data());
							keepUV=TRUE;
						}
						else
						{
#ifdef FE_HOUDINI_USE_GA
							handlePointUV.setPoint(pointOffset);
#else
							handlePointUV.setElement(pPoint);
#endif
							if(handlePointUV.isAttributeValid())
							{
								UT_Vector3 utVector3=handlePointUV.getV3(0);
								pUv2[u]=Vector2f(utVector3.data());
								keepUV=TRUE;
							}
							else
							{
								set(pUv2[u],u/(columns-1.0),v/(rows-1.0));
							}
						}

						pNumber2[u]=pointIndex;

#if FE_STH_DEBUG
						feLog(" %d %d %s n %s\n",u,v,
								c_print(pVert2[u]),c_print(pNorm2[u]));
#endif
					}
				}
				else
				{
					for(U32 u=0;u<columns;u++)
					{
						pVert2[u]=pVert0[u];
						pNorm2[u]=pNorm0[u];
						pColor2[u]=pColor0[u];
						pUv2[u]=pUv0[u];
						pNumber2[u]=pNumber0[u];
					}
				}
				if(v)
				{
					U32 u;
					for(u=0;u<columns-1;u++)
					{
						FEASSERT(triangleIndex<elementAllocated);
						m_pElementArray[triangleIndex]=
								Vector3i(total,3,FALSE);

						//* lower left
						m_pVertexArray[total]=pVert1[u];
						m_pNormalArray[total]=pNorm1[u];
						m_pColorArray[total]=pColor1[u];
						m_pUVArray[total]=pUv1[u];
						m_pTriangleIndexArray[total]=triangleIndex;
						m_pPointIndexArray[total]=pNumber1[u];
						m_pPrimitiveIndexArray[total]=primitiveIndex;
						m_pPartitionIndexArray[total]=partitionIndex;
						total++;
						//* lower right
						m_pVertexArray[total]=pVert1[u+1];
						m_pNormalArray[total]=pNorm1[u+1];
						m_pColorArray[total]=pColor1[u+1];
						m_pUVArray[total]=pUv1[u+1];
						m_pTriangleIndexArray[total]=triangleIndex;
						m_pPointIndexArray[total]=pNumber1[u+1];
						m_pPrimitiveIndexArray[total]=primitiveIndex;
						m_pPartitionIndexArray[total]=partitionIndex;
						total++;
						//* upper right
						m_pVertexArray[total]=pVert2[u+1];
						m_pNormalArray[total]=pNorm2[u+1];
						m_pColorArray[total]=pColor2[u+1];
						m_pUVArray[total]=pUv2[u+1];
						m_pTriangleIndexArray[total]=triangleIndex;
						m_pPointIndexArray[total]=pNumber2[u+1];
						m_pPrimitiveIndexArray[total]=primitiveIndex;
						m_pPartitionIndexArray[total]=partitionIndex;
						total++;
						triangleIndex++;

						FEASSERT(triangleIndex<elementAllocated);
						m_pElementArray[triangleIndex]=
								Vector3i(total,3,FALSE);

						//* upper right
						m_pVertexArray[total]=pVert2[u+1];
						m_pNormalArray[total]=pNorm2[u+1];
						m_pColorArray[total]=pColor2[u+1];
						m_pUVArray[total]=pUv2[u+1];
						m_pTriangleIndexArray[total]=triangleIndex;
						m_pPointIndexArray[total]=pNumber2[u+1];
						m_pPrimitiveIndexArray[total]=primitiveIndex;
						m_pPartitionIndexArray[total]=partitionIndex;
						total++;
						//* upper left
						m_pVertexArray[total]=pVert2[u];
						m_pNormalArray[total]=pNorm2[u];
						m_pColorArray[total]=pColor2[u];
						m_pUVArray[total]=pUv2[u];
						m_pTriangleIndexArray[total]=triangleIndex;
						m_pPointIndexArray[total]=pNumber2[u];
						m_pPrimitiveIndexArray[total]=primitiveIndex;
						m_pPartitionIndexArray[total]=partitionIndex;
						total++;
						//* lower left
						m_pVertexArray[total]=pVert1[u];
						m_pNormalArray[total]=pNorm1[u];
						m_pColorArray[total]=pColor1[u];
						m_pUVArray[total]=pUv1[u];
						m_pTriangleIndexArray[total]=triangleIndex;
						m_pPointIndexArray[total]=pNumber1[u];
						m_pPrimitiveIndexArray[total]=primitiveIndex;
						m_pPartitionIndexArray[total]=partitionIndex;
						total++;
						triangleIndex++;
					}
					//* NOTE u=columns-1
					if(wrapU)
					{
						FEASSERT(triangleIndex<elementAllocated);
						m_pElementArray[triangleIndex]=
								Vector3i(total,3,FALSE);

						//* lower left
						m_pVertexArray[total]=pVert1[u];
						m_pNormalArray[total]=pNorm1[u];
						m_pColorArray[total]=pColor1[u];
						m_pUVArray[total]=pUv1[u];
						m_pTriangleIndexArray[total]=triangleIndex;
						m_pPointIndexArray[total]=pNumber1[u];
						m_pPrimitiveIndexArray[total]=primitiveIndex;
						m_pPartitionIndexArray[total]=partitionIndex;
						total++;
						//* lower right
						m_pVertexArray[total]=pVert1[0];
						m_pNormalArray[total]=pNorm1[0];
						m_pColorArray[total]=pColor1[0];
						m_pUVArray[total]=pUv1[0];
						m_pTriangleIndexArray[total]=triangleIndex;
						m_pPointIndexArray[total]=pNumber1[0];
						m_pPrimitiveIndexArray[total]=primitiveIndex;
						m_pPartitionIndexArray[total]=partitionIndex;
						total++;
						//* upper right
						m_pVertexArray[total]=pVert2[0];
						m_pNormalArray[total]=pNorm2[0];
						m_pColorArray[total]=pColor2[0];
						m_pUVArray[total]=pUv2[0];
						m_pTriangleIndexArray[total]=triangleIndex;
						m_pPointIndexArray[total]=pNumber2[0];
						m_pPrimitiveIndexArray[total]=primitiveIndex;
						m_pPartitionIndexArray[total]=partitionIndex;
						total++;
						triangleIndex++;

						FEASSERT(triangleIndex<elementAllocated);
						m_pElementArray[triangleIndex]=
								Vector3i(total,3,FALSE);

						//* upper right
						m_pVertexArray[total]=pVert2[0];
						m_pNormalArray[total]=pNorm2[0];
						m_pColorArray[total]=pColor2[0];
						m_pUVArray[total]=pUv2[0];
						m_pTriangleIndexArray[total]=triangleIndex;
						m_pPointIndexArray[total]=pNumber2[0];
						m_pPrimitiveIndexArray[total]=primitiveIndex;
						m_pPartitionIndexArray[total]=partitionIndex;
						total++;
						//* upper left
						m_pVertexArray[total]=pVert2[u];
						m_pNormalArray[total]=pNorm2[u];
						m_pColorArray[total]=pColor2[u];
						m_pUVArray[total]=pUv2[u];
						m_pTriangleIndexArray[total]=triangleIndex;
						m_pPointIndexArray[total]=pNumber2[u];
						m_pPrimitiveIndexArray[total]=primitiveIndex;
						m_pPartitionIndexArray[total]=partitionIndex;
						total++;
						//* lower left
						m_pVertexArray[total]=pVert1[u];
						m_pNormalArray[total]=pNorm1[u];
						m_pColorArray[total]=pColor1[u];
						m_pUVArray[total]=pUv1[u];
						m_pTriangleIndexArray[total]=triangleIndex;
						m_pPointIndexArray[total]=pNumber1[u];
						m_pPrimitiveIndexArray[total]=primitiveIndex;
						m_pPartitionIndexArray[total]=partitionIndex;
						total++;
						triangleIndex++;
					}
				}
				else
				{
					for(U32 u=0;u<columns;u++)
					{
						pVert0[u]=pVert2[u];
						pNorm0[u]=pNorm2[u];
						pColor0[u]=pColor2[u];
						pUv0[u]=pUv2[u];
						pNumber0[u]=pNumber2[u];
					}
				}
			}


			delete[] pNumber2;
			delete[] pNumber1;
			delete[] pNumber0;
			delete[] pUv2;
			delete[] pUv1;
			delete[] pUv0;
			delete[] pColor2;
			delete[] pColor1;
			delete[] pColor0;
			delete[] pNorm2;
			delete[] pNorm1;
			delete[] pNorm0;
			delete[] pVert2;
			delete[] pVert1;
			delete[] pVert0;
		}

#ifdef FE_HOUDINI_USE_GA
		if(pPrimitive->getTypeId().get()==GEO_PRIMTRISTRIP)
#else
		if(pPrimitive->getPrimitiveId()==GEOPRIMTRISTRIP)
#endif
		{
			const U32 subCount=pPrimitive->getVertexCount();
			if(subCount<3)
			{
#if FE_STH_WARNINGS
				feLog("SurfaceTrianglesHoudini::cache as GEO_PrimTriStrip"
						" primitive %d/%d subCount=%d (not triangles?)\n",
						index,primitiveCount,subCount);
#endif
				continue;
			}

			const U32 newCount=(subCount-2)*3;

			m_vertices+=newCount;

			resizeFor(m_vertices/3,m_vertices,
					elementAllocated,vertexAllocated);

			const I32 order[2][3]={ {0,1,2}, {0,2,1} };

			//* unpack
			for(U32 newIndex=0;newIndex<newCount;newIndex++)
			{
				const I32 subIndex=newIndex/3+order[(newIndex/3)%2][newIndex%3];

#ifdef FE_HOUDINI_USE_GA
				const GEO_Vertex geoVertex=
						pPrimitive->getVertexElement(subIndex);
#else
				const GEO_Vertex& geoVertex=pPrimitive->getVertex(subIndex);
#endif

				//* location
				UT_Vector4 utVector4=geoVertex.getPos();
				m_pVertexArray[total]=Vector3f(utVector4.data());

#if FE_STH_DEBUG
				feLog("tristrip prim %d/%d new %d/%d sub %d/%d vert %d/%d %s\n",
						index,primitiveCount,
						newIndex,newCount,
						subIndex,subCount,
						total,m_vertices,
						c_print(m_pVertexArray[total]));
#endif

				//* normal

#ifdef FE_HOUDINI_USE_GA
				const GA_Index pointIndex=geoVertex.getPointIndex();

//				const GEO_Point* pPoint=m_pGdp->getGEOPoint(
//						m_pGdp->pointOffset(pointIndex));

				const GA_Offset pointOffset=geoVertex.getPointOffset();
				handlePointNormal.setPoint(pointOffset);
#else
				const U32 pointIndex=geoVertex.getBasePt()->getNum();
				const GEO_Point* pPoint=m_pGdp->points()(pointIndex);
				handlePointNormal.setElement(pPoint);
#endif

				if(!(newIndex%3))
				{
					FEASSERT(triangleIndex<elementAllocated);
					m_pElementArray[triangleIndex]=
							Vector3i(total,3,FALSE);
					triangleIndex++;
				}

				m_pTriangleIndexArray[total]= -1;
				m_pPointIndexArray[total]=pointIndex;
				m_pPrimitiveIndexArray[total]=primitiveIndex;
				m_pPartitionIndexArray[total]=partitionIndex;


				if(handlePointNormal.isAttributeValid())
				{
					utVector4=handlePointNormal.getV4(0);
					m_pNormalArray[total]=Vector3f(utVector4.data());
				}
				else
				{
					handleVertexNormal.setElement(&geoVertex);
					if(handleVertexNormal.isAttributeValid())
					{
						utVector4=handlePointNormal.getV4(0);
						m_pNormalArray[total]=Vector3f(utVector4.data());
					}
					else
					{
						set(m_pNormalArray[total],0.0,1.0,0.0);
					}
				}

				handleVertexColor.setElement(&geoVertex);
				if(handleVertexColor.isAttributeValid())
				{
					UT_Vector3 utVector3=handleVertexColor.getV3(0);
					m_pColorArray[total]=Vector3f(utVector3.data());
				}
				else
				{
#ifdef FE_HOUDINI_USE_GA
					handlePointColor.setPoint(pointOffset);
#else
					handlePointColor.setElement(pPoint);
#endif

					if(handlePointColor.isAttributeValid())
					{
						UT_Vector3 utVector3=handlePointColor.getV3(0);
						m_pColorArray[total]=Vector3f(utVector3.data());
					}
					else
					{
						set(m_pColorArray[total]);
					}
				}

				handleVertexUV.setElement(&geoVertex);
				if(handleVertexUV.isAttributeValid())
				{
					UT_Vector3 utVector3=handleVertexUV.getV3(0);
					m_pUVArray[total]=Vector2f(utVector3.data());
					keepUV=TRUE;
				}
				else
				{
#ifdef FE_HOUDINI_USE_GA
					handlePointUV.setPoint(pointOffset);
#else
					handlePointUV.setElement(pPoint);
#endif

					if(handlePointUV.isAttributeValid())
					{
						UT_Vector3 utVector3=handlePointUV.getV3(0);
						m_pUVArray[total]=Vector2f(utVector3.data());
						keepUV=TRUE;
					}
					else
					{
						set(m_pUVArray[total]);
					}
				}

				total++;
			}
		}

#ifdef FE_HOUDINI_USE_GA
		if(pPrimitive->getTypeId().get()==GEO_PRIMPOLY)
#else
		if(pPrimitive->getPrimitiveId()==GEOPRIMPOLY)
#endif
		{
			const U32 subCount=pPrimitive->getVertexCount();
			if(subCount!=3 && subCount!=4)
			{
#if FE_STH_WARNINGS
				feLog("SurfaceTrianglesHoudini::cache as GEO_PrimPoly"
						" primitive %d/%d subCount=%d"
						" (not triangles or quads?)\n",
						index,primitiveCount,subCount);
#endif
				continue;
			}

			if(m_triangulation==SurfaceI::e_existingPoints)
			{
				m_vertices+=(subCount==4)? 6: 3;
			}
			else
			{
				m_vertices+=(subCount==4)? 12: 3;
			}

			resizeFor(m_vertices/3,m_vertices,
					elementAllocated,vertexAllocated);

			FEASSERT(triangleIndex<elementAllocated);
			m_pElementArray[triangleIndex]=Vector3i(total,3,FALSE);

			for(U32 subIndex=0;subIndex<subCount;subIndex++)
			{
#if FE_HOUDINI_12_5_PLUS
//				const GEO_Vertex geoVertex=
//						pPrimitive->getVertexElement(subIndex);
				const GA_Offset pointOffset=
						pPrimitive->getPointOffset(subIndex);
				const GA_Offset vertexOffset=
						pPrimitive->getVertexOffset(subIndex);

				//* location
				const UT_Vector3 position=m_pGdp->getPos3(pointOffset);
#else
				const GEO_Vertex& geoVertex=pPrimitive->getVertex(subIndex);

				//* location
				const UT_Vector4 position=geoVertex.getPos();
#endif

				//* normal

#if FE_HOUDINI_12_5_PLUS
//				const GA_Index pointIndex=geoVertex.getPointIndex();
				const GA_Index pointIndex=
						pPrimitive->getPointIndex(subIndex);

				handlePointNormal.setPoint(pointOffset);
#else
				const U32 pointIndex=geoVertex.getBasePt()->getNum();
				const GEO_Point* pPoint=m_pGdp->points()(pointIndex);
				handlePointNormal.setElement(pPoint);
#endif

				m_pVertexArray[total]=Vector3f(position.data());
				m_pTriangleIndexArray[total]=triangleIndex+(subIndex==3);
				m_pPointIndexArray[total]=pointIndex;
				m_pPrimitiveIndexArray[total]=primitiveIndex;
				m_pPartitionIndexArray[total]=partitionIndex;

				//* HACK add garbage
//				if(!(primitiveIndex%13) && subIndex==1)
//				{
//					m_pVertexArray[total][1]=
//							std::numeric_limits<Real>::quiet_NaN();
//				}

				if(handlePointNormal.isAttributeValid())
				{
					const UT_Vector4 utVector4=handlePointNormal.getV4(0);
					m_pNormalArray[total]=Vector3f(utVector4.data());
				}
				else
				{
#if FE_HOUDINI_12_5_PLUS
					handleVertexNormal.setVertex(vertexOffset);
#else
					handleVertexNormal.setElement(&geoVertex);
#endif
					if(handleVertexNormal.isAttributeValid())
					{
						const UT_Vector4 utVector4=handleVertexNormal.getV4(0);
						m_pNormalArray[total]=Vector3f(utVector4.data());
					}
					else
					{
						set(m_pNormalArray[total],0.0,1.0,0.0);
					}
				}

#if FE_HOUDINI_12_5_PLUS
				handleVertexColor.setVertex(vertexOffset);
#else
				handleVertexColor.setElement(&geoVertex);
#endif
				if(handleVertexColor.isAttributeValid())
				{
					UT_Vector3 utVector3=handleVertexColor.getV3(0);
					m_pColorArray[total]=Vector3f(utVector3.data());
				}
				else
				{
#if FE_HOUDINI_12_5_PLUS
					handlePointColor.setPoint(pointOffset);
#else
					handlePointColor.setElement(pPoint);
#endif
					if(handlePointColor.isAttributeValid())
					{
						UT_Vector3 utVector3=handlePointColor.getV3(0);
						m_pColorArray[total]=Vector3f(utVector3.data());
					}
					else
					{
						set(m_pColorArray[total]);
					}
				}

#if FE_HOUDINI_12_5_PLUS
				handleVertexUV.setVertex(vertexOffset);
#else
				handleVertexUV.setElement(&geoVertex);
#endif
				if(handleVertexUV.isAttributeValid())
				{
					UT_Vector3 utVector3=handleVertexUV.getV3(0);
					m_pUVArray[total]=Vector2f(utVector3.data());
					keepUV=TRUE;
				}
				else
				{
#if FE_HOUDINI_12_5_PLUS
					handlePointUV.setPoint(pointOffset);
#else
					handlePointUV.setElement(pPoint);
#endif
					if(handlePointUV.isAttributeValid())
					{
						UT_Vector3 utVector3=handlePointUV.getV3(0);
						m_pUVArray[total]=Vector2f(utVector3.data());
						keepUV=TRUE;
					}
					else
					{
						set(m_pUVArray[total]);
					}
				}

#if FE_STH_DEBUG
				feLog("poly index %d/%d sub %d/%d vert %d"
						" pick %d point %d prim %d part %d"
						" v %s n %s c %s uv %s\n",
						index,primitiveCount,subIndex,subCount,total,
						m_pTriangleIndexArray[total],
						m_pPointIndexArray[total],
						m_pPrimitiveIndexArray[total],
						m_pPartitionIndexArray[total],
						c_print(m_pVertexArray[total]),
						c_print(m_pNormalArray[total]),
						c_print(m_pColorArray[total]),
						c_print(m_pUVArray[total]));
#endif

				total++;

				if(subIndex==3)
				{
					if(m_triangulation==SurfaceI::e_existingPoints)
					{
						triangleIndex++;

						FEASSERT(triangleIndex<elementAllocated);
						m_pElementArray[triangleIndex]=
								Vector3i(m_vertices-3,3,FALSE);

						m_pVertexArray[total]=m_pVertexArray[total-4];
						m_pNormalArray[total]=m_pNormalArray[total-4];
						m_pColorArray[total]=m_pColorArray[total-4];
						m_pUVArray[total]=m_pUVArray[total-4];
						m_pTriangleIndexArray[total]=triangleIndex;
						m_pPointIndexArray[total]=m_pPointIndexArray[total-4];
						m_pPrimitiveIndexArray[total]=primitiveIndex;
						m_pPartitionIndexArray[total]=partitionIndex;
						total++;

						m_pVertexArray[total]=m_pVertexArray[total-3];
						m_pNormalArray[total]=m_pNormalArray[total-3];
						m_pColorArray[total]=m_pColorArray[total-3];
						m_pUVArray[total]=m_pUVArray[total-3];
						m_pTriangleIndexArray[total]=triangleIndex;
						m_pPointIndexArray[total]=m_pPointIndexArray[total-3];
						m_pPrimitiveIndexArray[total]=primitiveIndex;
						m_pPartitionIndexArray[total]=partitionIndex;
						total++;
					}
					else
					{
						//* quartering

						total-=4;

						//* 0:	0  1  2
						//* 1:	3  4  5
						//* 2:	6  7  8
						//* 3:	9 10 11

						//* prevent backwards UV interpolation
						for(U32 pass=0;pass<2;pass++)
						{
							if(m_pUVArray[total][pass]>0.7 ||
									m_pUVArray[total+1][pass]>0.7 ||
									m_pUVArray[total+2][pass]>0.7 ||
									m_pUVArray[total+3][pass]>0.7)
							{
								for(U32 n=0;n<4;n++)
								{
									Real& rValue=m_pUVArray[total+n][pass];
									if(rValue<0.3)
									{
										rValue+=1.0;
									}
								}
							}
						}

						FEASSERT(triangleIndex<elementAllocated-2);
						m_pElementArray[triangleIndex+1]=
								Vector3i(m_vertices-9,3,FALSE);
						m_pElementArray[triangleIndex+2]=
								Vector3i(m_vertices-6,3,FALSE);
						m_pElementArray[triangleIndex+3]=
								Vector3i(m_vertices-3,3,FALSE);

						//* TODO use TrianglePN for midpoint
#if FALSE
						TrianglePN<Real> trianglePN;
						SpatialVector sumVertex;
						SpatialVector sumNormal;
						set(sumVertex);
						set(sumNormal);
						for(U32 pass=0;pass<4;pass++)
						{
							trianglePN.configure(
								m_pVertexArray[total+pass],
								m_pVertexArray[total+(pass+1)%4],
								m_pVertexArray[total+(pass+2)%4],
								m_pNormalArray[total+pass],
								m_pNormalArray[total+(pass+1)%4],
								m_pNormalArray[total+(pass+2)%4]);
							sumVertex+=trianglePN.midpoint(
									TrianglePN<Real>::e_v3v1);
							sumNormal+=trianglePN.midnormal(
									TrianglePN<Real>::e_v3v1);
						}
						const SpatialVector meanVertex=0.25*sumVertex;
						const SpatialVector meanNormal=unitSafe(sumNormal);
#else
						const SpatialVector meanVertex=0.25*
								(m_pVertexArray[total]+
								m_pVertexArray[total+1]+
								m_pVertexArray[total+2]+
								m_pVertexArray[total+3]);

						const SpatialVector meanNormal=unitSafe(
								m_pNormalArray[total]+
								m_pNormalArray[total+1]+
								m_pNormalArray[total+2]+
								m_pNormalArray[total+3]);
#endif

						const Color meanColor=0.25*
								(m_pColorArray[total]+
								m_pColorArray[total+1]+
								m_pColorArray[total+2]+
								m_pColorArray[total+3]);

						const SpatialVector meanUV=0.25*
								(m_pUVArray[total]+
								m_pUVArray[total+1]+
								m_pUVArray[total+2]+
								m_pUVArray[total+3]);

						//* triangle 2
						m_pVertexArray[total+6]=m_pVertexArray[total+2];
						m_pNormalArray[total+6]=m_pNormalArray[total+2];
						m_pColorArray[total+6]=m_pColorArray[total+2];
						m_pUVArray[total+6]=m_pUVArray[total+2];
						m_pTriangleIndexArray[total+6]=triangleIndex+2;
						m_pPointIndexArray[total+6]=m_pPointIndexArray[total+2];
						m_pPrimitiveIndexArray[total+6]=primitiveIndex;
						m_pPartitionIndexArray[total+6]=partitionIndex;

						m_pVertexArray[total+7]=m_pVertexArray[total+3];
						m_pNormalArray[total+7]=m_pNormalArray[total+3];
						m_pColorArray[total+7]=m_pColorArray[total+3];
						m_pUVArray[total+7]=m_pUVArray[total+3];
						m_pTriangleIndexArray[total+7]=triangleIndex+2;
						m_pPointIndexArray[total+7]=m_pPointIndexArray[total+3];
						m_pPrimitiveIndexArray[total+7]=primitiveIndex;
						m_pPartitionIndexArray[total+7]=partitionIndex;

						//* triangle 3
						m_pVertexArray[total+9]=m_pVertexArray[total+3];
						m_pNormalArray[total+9]=m_pNormalArray[total+3];
						m_pColorArray[total+9]=m_pColorArray[total+3];
						m_pUVArray[total+9]=m_pUVArray[total+3];
						m_pTriangleIndexArray[total+9]=triangleIndex+3;
						m_pPointIndexArray[total+9]=m_pPointIndexArray[total+3];
						m_pPrimitiveIndexArray[total+9]=primitiveIndex;
						m_pPartitionIndexArray[total+9]=partitionIndex;

						m_pVertexArray[total+10]=m_pVertexArray[total];
						m_pNormalArray[total+10]=m_pNormalArray[total];
						m_pColorArray[total+10]=m_pColorArray[total];
						m_pUVArray[total+10]=m_pUVArray[total];
						m_pTriangleIndexArray[total+10]=triangleIndex+3;
						m_pPointIndexArray[total+10]=m_pPointIndexArray[total];
						m_pPrimitiveIndexArray[total+10]=primitiveIndex;
						m_pPartitionIndexArray[total+10]=partitionIndex;

						//* triangle 1
						m_pVertexArray[total+3]=m_pVertexArray[total+1];
						m_pNormalArray[total+3]=m_pNormalArray[total+1];
						m_pColorArray[total+3]=m_pColorArray[total+1];
						m_pUVArray[total+3]=m_pUVArray[total+1];
						m_pTriangleIndexArray[total+3]=triangleIndex+1;
						m_pPointIndexArray[total+3]=m_pPointIndexArray[total+1];
						m_pPrimitiveIndexArray[total+3]=primitiveIndex;
						m_pPartitionIndexArray[total+3]=partitionIndex;

						m_pVertexArray[total+4]=m_pVertexArray[total+2];
						m_pNormalArray[total+4]=m_pNormalArray[total+2];
						m_pColorArray[total+4]=m_pColorArray[total+2];
						m_pUVArray[total+4]=m_pUVArray[total+2];
						m_pTriangleIndexArray[total+4]=triangleIndex+1;
						m_pPointIndexArray[total+4]=m_pPointIndexArray[total+2];
						m_pPrimitiveIndexArray[total+4]=primitiveIndex;
						m_pPartitionIndexArray[total+4]=partitionIndex;

						//* mean
						m_pVertexArray[total+2]=meanVertex;
						m_pNormalArray[total+2]=meanNormal;
						m_pColorArray[total+2]=meanColor;
						m_pUVArray[total+2]=meanUV;
						m_pTriangleIndexArray[total+2]=triangleIndex;
						m_pPointIndexArray[total+2]= -1;
						m_pPrimitiveIndexArray[total+2]=primitiveIndex;
						m_pPartitionIndexArray[total+2]=partitionIndex;

						m_pVertexArray[total+5]=meanVertex;
						m_pNormalArray[total+5]=meanNormal;
						m_pColorArray[total+5]=meanColor;
						m_pUVArray[total+5]=meanUV;
						m_pTriangleIndexArray[total+5]=triangleIndex+1;
						m_pPointIndexArray[total+5]= -1;
						m_pPrimitiveIndexArray[total+5]=primitiveIndex;
						m_pPartitionIndexArray[total+5]=partitionIndex;

						m_pVertexArray[total+8]=meanVertex;
						m_pNormalArray[total+8]=meanNormal;
						m_pColorArray[total+8]=meanColor;
						m_pUVArray[total+8]=meanUV;
						m_pTriangleIndexArray[total+8]=triangleIndex+2;
						m_pPointIndexArray[total+8]= -1;
						m_pPrimitiveIndexArray[total+8]=primitiveIndex;
						m_pPartitionIndexArray[total+8]=partitionIndex;

						m_pVertexArray[total+11]=meanVertex;
						m_pNormalArray[total+11]=meanNormal;
						m_pColorArray[total+11]=meanColor;
						m_pUVArray[total+11]=meanUV;
						m_pTriangleIndexArray[total+11]=triangleIndex+3;
						m_pPointIndexArray[total+11]= -1;
						m_pPrimitiveIndexArray[total+11]=primitiveIndex;
						m_pPartitionIndexArray[total+11]=partitionIndex;

						triangleIndex+=3;
						total+=12;
					}
				}
			}

			triangleIndex++;
		}

#ifdef FE_HOUDINI_USE_GA
		if(pPrimitive->getTypeId().get()==GEO_PRIMNURBSURF ||
				pPrimitive->getTypeId().get()==GEO_PRIMNURBCURVE)
#else
		if(pPrimitive->getPrimitiveId()==GEOPRIMNURBSURF ||
				pPrimitive->getPrimitiveId()==GEOPRIMNURBCURVE)
#endif
		{
			feX(e_unsupported,"SurfaceTrianglesHoudini::cache",
					"searchable NURBS surfaces not yet supported");

			//* TODO
		}

#ifdef FE_HOUDINI_USE_GA
		if(pPrimitive->getTypeId().get()==GEO_PRIMBEZSURF ||
				pPrimitive->getTypeId().get()==GEO_PRIMBEZCURVE)
#else
		if(pPrimitive->getPrimitiveId()==GEOPRIMBEZSURF ||
				pPrimitive->getPrimitiveId()==GEOPRIMBEZCURVE)
#endif
		{
			feX(e_unsupported,"SurfaceTrianglesHoudini::cache",
					"searchable Bezier surfaces not yet supported");

			//* TODO
		}
	}

	if(elementAllocated!=triangleIndex || vertexAllocated!=m_vertices)
	{
		resizeArrays(triangleIndex,m_vertices);
	}

	if(total!=m_vertices)
	{
		feLog("SurfaceTrianglesHoudini::cache converted %d/%d vertices\n",
				total,m_vertices);
		for(U32 m=total;m<m_vertices;m++)
		{
			set(m_pVertexArray[m]);
			set(m_pNormalArray[m],0.0,1.0,0.0);
			set(m_pColorArray[m]);
			set(m_pUVArray[m]);
			m_pTriangleIndexArray[m]= -1;
			m_pPointIndexArray[total]= -1;
			m_pPrimitiveIndexArray[total]= -1;
			m_pPartitionIndexArray[total]= -1;
		}
	}

	m_elements=triangleIndex;

	if(!keepUV)
	{
		deallocate(m_pUVArray);
		m_pUVArray=NULL;
	}

#if FE_STH_DEBUG
	feLog("SurfaceTrianglesHoudini::cache %d triangles %d vertices\n",
			m_elements,m_vertices);
#endif

	calcBoundingSphere();
}

} /* namespace ext */
} /* namespace fe */
