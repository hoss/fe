/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

//*	derived from Houndini sample: MSS_CustomBrushState

#ifndef __houdini_HoudiniBrush_h__
#define __houdini_HoudiniBrush_h__

#include <MSS/MSS_SingleOpState.h>

class JEDI_View;

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Houdini Model State node

	@ingroup houdini
*//***************************************************************************/
class HoudiniBrush:
	public MSS_SingleOpState,
	public MetaBrush
{
	public:
					HoudiniBrush(JEDI_View &a_rView,
							PI_StateTemplate &a_rTemplate,
							BM_SceneManager *a_pScene,
							const char *a_pCursor=BM_DEFAULT_CURSOR);

virtual				~HoudiniBrush(void);

static	int			discoverBrushes(BM_ResourceManager *a_pManager);

					/// used by DM to create the state
static	BM_State*	construct(BM_View &a_rView,PI_StateTemplate &a_rTemplate,
							BM_SceneManager *a_pScene);

					/// name and type of this class
virtual const char*	className(void) const;

	protected:
					/// called when the user enters the state
virtual int			enter(BM_SimpleState::BM_EntryType a_how) override;

					/// called when the user leaves the state
virtual void		exit(void) override;

					/// called when the user temporarily leaves the state
					/// (mouse leaves the viewport)
virtual void		interrupt(BM_SimpleState* a_pState=0) override;

					/// called when the user returns to the state after
					/// leaving temporarily (mouse re-enters the viewport)
virtual void		resume(BM_SimpleState* a_pState=0) override;

					/// respond to mouse events
virtual int			handleMouseEvent(UI_Event* a_pEvent) override;

					/// respond to mouse wheel events
virtual int			handleMouseWheelEvent(UI_Event* a_pEvent) override;

					/// respond to double mouse clicks (additional)
virtual	bool		handleDoubleClickEvent(UI_Event* a_pEvent) override;

					//* not since 16.5?
//virtual	int		handleArrowEvent(UI_Event* a_pEvent) override;

virtual	void		handleGeoChangedEvent(UI_Event* a_pEvent) override;

					/// respond to keyboard events
virtual	int			handleKeyTypeEvent(int key,UI_Event* a_pEvent,
							BM_Viewport& a_rViewport) override;

virtual bool		handleTransitoryKey(const UI_Event& a_rEvent,
							int a_hotkey_id) override;

virtual void		handleEvent(UI_Event* a_pEvent);

					/// render the brush "cursor" geometry
virtual void		doRender(RE_Render* a_pRender,int a_x,int a_y,
							int a_ghost) override;

					//* before 12.5
//virtual void		doRender(RE_Render* a_pRender,short a_x,short a_y,
//							int a_ghost)
//					{	doRender(a_pRender,(int)a_x,(int)a_y,a_ghost); }

					/// set the prompt's text
virtual void		updatePrompt(void);

					/// reposition the brush's guide geometry
		void		updateBrush(int a_x,int a_y);

	private:
static	String		scanForBrushNames(void);

		void		eventStart(void);
		void		eventStop(void);
		void		drawStart(void);
		void		drawStop(void);

		void		generateNoEvent(void);
		void		printEvent(const UI_Event* a_pEvent);
		int			handleAnyEvent(const UI_Event* a_pEvent);

		void		brush(void);

		BWORD				m_started;
		BWORD				m_doubleClick;
		BWORD				m_idleQueued;
		BWORD				m_exposed;
		BWORD				m_ortho;
		Matrix<4,4,F64>		m_projection;

		bool			m_isBrushVisible;
		GU_Detail		m_gdp;
		Real			m_nearPlane;
		Real			m_farPlane;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __houdini_HoudiniBrush_h__ */
