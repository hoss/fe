/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <houdini/houdiniEngine.pmh>

#define FE_STHE_DEBUG		FALSE
#define	FE_STHE_WARNINGS	(FE_CODEGEN<=FE_DEBUG)

namespace fe
{
namespace ext
{

SurfaceTrianglesHapi::SurfaceTrianglesHapi(void):
	m_pHapiSession(NULL),
	m_subIndex(-1)
{
#if FE_STHE_DEBUG
	feLog("SurfaceTrianglesHapi()\n");
#endif
}

SurfaceTrianglesHapi::~SurfaceTrianglesHapi(void)
{
}

Protectable* SurfaceTrianglesHapi::clone(Protectable* pInstance)
{
#if FE_STHE_DEBUG
	feLog("SurfaceTrianglesHapi::clone\n");
#endif

	SurfaceTrianglesHapi* pSurfaceTrianglesHapi= pInstance?
			fe_cast<SurfaceTrianglesHapi>(pInstance):
			new SurfaceTrianglesHapi();

	SurfaceTriangles::clone(pSurfaceTrianglesHapi);

	// TODO copy attributes

	return pSurfaceTrianglesHapi;
}

//* TODO check with a mix of polygons, meshes, and strips

void SurfaceTrianglesHapi::cache(void)
{
#if FE_STHE_DEBUG
	feLog("SurfaceTrianglesHapi::cache\n");
#endif

	if(!m_pHapiSession)
	{
		feLog("SurfaceTrianglesHapi::cache NULL session\n");
		return;
	}

	if(HAPI_IsSessionValid(m_pHapiSession)!=HAPI_RESULT_SUCCESS)
	{
		feLog("SurfaceTrianglesHapi::cache invalid session\n");
		return;
	}

	setOptionalArrays(Arrays(e_arrayColor|e_arrayUV));

	//* TODO m_subIndex

	m_elements=0;
	m_vertices=0;
	U32 elementAllocated=0;
	U32 vertexAllocated=0;

	I32 partitionIndex= -1;
	U32 triangleIndex=0;
	U32 total=0;

	HAPI_Result result;

	HAPI_GeoInfo geo_info;
	result=HAPI_GetGeoInfo(m_pHapiSession,m_hapiNodeId,&geo_info);
	if(result!=HAPI_RESULT_SUCCESS)
	{
		feLog("SurfaceTrianglesHapi::cache"
				" get geo info failed on %d\n  \"%s\"\n",m_hapiNodeId,
				SurfaceAccessibleHapi::lastError(m_pHapiSession).c_str());
	}

	const I32 partCount=geo_info.partCount;

	for(I32 partIndex=0;partIndex<partCount;partIndex++)
	{
		const HAPI_PartId part_id=partIndex;
		HAPI_PartInfo part_info;

		result=HAPI_GetPartInfo(m_pHapiSession,m_hapiNodeId,part_id,&part_info);
		if(result!=HAPI_RESULT_SUCCESS)
		{
			feLog("SurfaceTrianglesHapi::cache"
					" get part info failed on %d part %d/%d\n  \"%s\"\n",
					m_hapiNodeId,partIndex,partCount,
					SurfaceAccessibleHapi::lastError(m_pHapiSession).c_str());
		}

//		feLog("     %d/%d points %d verts %d prims %d\n",
//				partIndex,partCount,part_info.pointCount,
//				part_info.vertexCount,part_info.faceCount);

		const I32 pointCount=part_info.pointCount;
		const I32 vertexCount=part_info.vertexCount;
		const I32 faceCount=part_info.faceCount;

		resizeFor(triangleIndex+faceCount*2,total+pointCount*6,
				elementAllocated,vertexAllocated);

		HAPI_AttributeOwner owner=HAPI_ATTROWNER_POINT;
		HAPI_AttributeInfo attr_info;
		const I32 stride= -1;

		float* position_array=NULL;
		float* normal_array=NULL;
		float* color_array=NULL;
		float* uv_array=NULL;

		for(I32 attrIndex=0;attrIndex<4;attrIndex++)
		{
			const String attrName=attrIndex==3? "uv":
					(attrIndex==2? "Cd": (attrIndex? "N": "P"));

			result=HAPI_GetAttributeInfo(m_pHapiSession,m_hapiNodeId,part_id,
					attrName.c_str(),owner,&attr_info);
			if(result!=HAPI_RESULT_SUCCESS)
			{
				feLog("SurfaceTrianglesHapi::cache"
						" get attr info failed on %d part %d/%d\n  \"%s\"\n",
						m_hapiNodeId,partIndex,partCount,
						SurfaceAccessibleHapi::lastError(
						m_pHapiSession).c_str());
			}

			if(!attr_info.exists)
			{
				continue;
			}

			float* data_array=NULL;

			switch(attrIndex)
			{
				case 0:
					position_array=new float[pointCount*3];
					data_array=position_array;
					break;
				case 1:
					normal_array=new float[pointCount*3];
					data_array=normal_array;
					break;
				case 2:
					color_array=new float[pointCount*3];
					data_array=color_array;
					break;
				case 3:
					uv_array=new float[pointCount*3];
					data_array=uv_array;
					break;
				default:
					break;
			}

			result=HAPI_GetAttributeFloatData(m_pHapiSession,
					m_hapiNodeId,part_id,
					attrName.c_str(),&attr_info,
					stride,data_array,0,pointCount);
			if(result!=HAPI_RESULT_SUCCESS)
			{
				feLog("SurfaceTrianglesHapi::cache"
						" get attr data failed on %d part %d/%d\n  \"%s\"\n",
						m_hapiNodeId,partIndex,partCount,
						SurfaceAccessibleHapi::lastError(
						m_pHapiSession).c_str());
			}
		}

		for(I32 pointIndex=0;pointIndex<pointCount && pointIndex<4;pointIndex++)
		{
			const I32 index3=pointIndex*3;
			const SpatialVector point(position_array[index3],
					position_array[index3+1],position_array[index3+2]);

//~			feLog("      %d/%d %s\n",pointIndex,pointCount,c_print(point));
		}

		int face_counts_array[faceCount];
		result=HAPI_GetFaceCounts(m_pHapiSession,m_hapiNodeId,part_id,
				face_counts_array,0,faceCount);
		if(result!=HAPI_RESULT_SUCCESS)
		{
			feLog("SurfaceTrianglesHapi::cache"
					" get face counts failed on %d part %d/%d\n  \"%s\"\n",
					m_hapiNodeId,partIndex,partCount,
					SurfaceAccessibleHapi::lastError(m_pHapiSession).c_str());
		}

		int vertex_list_array[vertexCount];
		result=HAPI_GetVertexList(m_pHapiSession,m_hapiNodeId,part_id,
				vertex_list_array,0,vertexCount);
		if(result!=HAPI_RESULT_SUCCESS)
		{
			feLog("SurfaceTrianglesHapi::cache"
					" get vert list failed on %d part %d/%d\n  \"%s\"\n",
					m_hapiNodeId,partIndex,partCount,
					SurfaceAccessibleHapi::lastError(m_pHapiSession).c_str());
		}

		const SpatialVector defaultNormal(0.0,1.0,0.0);
		const Color defaultColor(1.0,1.0,1.0);
		const SpatialVector defaultUV(0.0,0.0,0.0);

		I32 vertexIndex=0;
		for(I32 faceIndex=0;faceIndex<faceCount;faceIndex++)
		{
			const I32 subCount=face_counts_array[faceIndex];

			if(subCount>2)
			{
				m_pElementArray[triangleIndex]=Vector3i(total,3,FALSE);

				for(I32 subIndex=0;subIndex<3;subIndex++)
				{
					const I32 pointIndex=
							vertex_list_array[vertexIndex+subIndex];
					const I32 pointIndex3=pointIndex*3;

					m_pVertexArray[total]=SpatialVector(
							position_array[pointIndex3],
							position_array[pointIndex3+1],
							position_array[pointIndex3+2]);
					m_pNormalArray[total]=normal_array? SpatialVector(
							normal_array[pointIndex3],
							normal_array[pointIndex3+1],
							normal_array[pointIndex3+2]):
							defaultNormal;
					m_pColorArray[total]=color_array? Color(
							color_array[pointIndex3],
							color_array[pointIndex3+1],
							color_array[pointIndex3+2]):
							defaultColor;
					m_pUVArray[total]=uv_array? SpatialVector(
							uv_array[pointIndex3],
							uv_array[pointIndex3+1],
							uv_array[pointIndex3+2]):
							defaultUV;
					m_pTriangleIndexArray[total]=triangleIndex;
					m_pPointIndexArray[total]=pointIndex;
					m_pPrimitiveIndexArray[total]=faceIndex;
					m_pPartitionIndexArray[total]=partitionIndex;
					total++;
				}

				triangleIndex++;
			}
			if(subCount>3)
			{
				m_pElementArray[triangleIndex]=Vector3i(total,3,FALSE);

				//* vertex 0 of previous
				m_pVertexArray[total]=m_pVertexArray[total-3];
				m_pNormalArray[total]=m_pNormalArray[total-3];
				m_pColorArray[total]=m_pColorArray[total-3];
				m_pUVArray[total]=m_pUVArray[total-3];
				m_pTriangleIndexArray[total]=m_pTriangleIndexArray[total-3];
				m_pPointIndexArray[total]=m_pPointIndexArray[total-3];
				m_pPrimitiveIndexArray[total]=m_pPrimitiveIndexArray[total-3];
				m_pPartitionIndexArray[total]=m_pPartitionIndexArray[total-3];
				total++;

				//* vertex 2 of previous
				m_pVertexArray[total]=m_pVertexArray[total-2];
				m_pNormalArray[total]=m_pNormalArray[total-2];
				m_pColorArray[total]=m_pColorArray[total-2];
				m_pUVArray[total]=m_pUVArray[total-2];
				m_pTriangleIndexArray[total]=m_pTriangleIndexArray[total-2];
				m_pPointIndexArray[total]=m_pPointIndexArray[total-2];
				m_pPrimitiveIndexArray[total]=m_pPrimitiveIndexArray[total-2];
				m_pPartitionIndexArray[total]=m_pPartitionIndexArray[total-2];
				total++;

				const I32 pointIndex=
						vertex_list_array[vertexIndex+3];
				const I32 pointIndex3=pointIndex*3;

				m_pVertexArray[total]=SpatialVector(
						position_array[pointIndex3],
						position_array[pointIndex3+1],
						position_array[pointIndex3+2]);
				m_pNormalArray[total]=normal_array? SpatialVector(
						normal_array[pointIndex3],
						normal_array[pointIndex3+1],
						normal_array[pointIndex3+2]):
						defaultNormal;
				m_pColorArray[total]=color_array? Color(
						color_array[pointIndex3],
						color_array[pointIndex3+1],
						color_array[pointIndex3+2]):
						defaultColor;
				m_pUVArray[total]=uv_array? SpatialVector(
						uv_array[pointIndex3],
						uv_array[pointIndex3+1],
						uv_array[pointIndex3+2]):
						defaultUV;
				m_pTriangleIndexArray[total]=triangleIndex;
				m_pPointIndexArray[total]=pointIndex;
				m_pPrimitiveIndexArray[total]=faceIndex;
				m_pPartitionIndexArray[total]=partitionIndex;
				total++;

				triangleIndex++;
			}

			vertexIndex+=subCount;
		}

		if(position_array)
		{
			delete[] position_array;
		}
		if(normal_array)
		{
			delete[] normal_array;
		}
		if(color_array)
		{
			delete[] color_array;
		}
		if(uv_array)
		{
			delete[] uv_array;
		}
	}

	m_elements=triangleIndex;
	m_vertices=total;

	if(elementAllocated!=triangleIndex || vertexAllocated!=m_vertices)
	{
		resizeArrays(triangleIndex,m_vertices);
	}

#if FE_STHE_DEBUG
	feLog("SurfaceTrianglesHapi::cache %d triangles %d vertices\n",
			m_elements,m_vertices);
#endif

	calcBoundingSphere();
}

} /* namespace ext */
} /* namespace fe */
