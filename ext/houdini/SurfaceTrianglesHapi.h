/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __surface_SurfaceTrianglesHapi_h__
#define __surface_SurfaceTrianglesHapi_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Houdini Engine Trianglar Surface

	@ingroup houdini
*//***************************************************************************/
class FE_DL_EXPORT SurfaceTrianglesHapi:
	public SurfaceTriangles,
	public CastableAs<SurfaceTrianglesHapi>
{
	public:
						SurfaceTrianglesHapi(void);
virtual					~SurfaceTrianglesHapi(void);

						//* As Protectable
virtual	Protectable*	clone(Protectable* pInstance=NULL);

						//* Houdini specific
		void			setSession(HAPI_Session* a_pHapiSession)
						{
							m_pHapiSession=a_pHapiSession;
						}
		HAPI_Session*	session(void) const		{ return m_pHapiSession; }

		void			setNodeId(HAPI_NodeId a_hapiNodeId)
						{
							m_hapiNodeId=a_hapiNodeId;
						}
		HAPI_NodeId		nodeId(void) const		{ return m_hapiNodeId; }

		void			setSubIndex(I32 a_subIndex)
						{	m_subIndex=a_subIndex; }
		I32				subIndex(void) const
						{	return m_subIndex; }

	protected:

virtual	void			cache(void);

	private:

		HAPI_Session*	m_pHapiSession;
		HAPI_NodeId		m_hapiNodeId;

		I32				m_subIndex;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __surface_SurfaceTriangles_h__ */

