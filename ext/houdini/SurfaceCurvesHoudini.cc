/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <houdini/houdini.pmh>

#define FE_SCH_DEBUG	FALSE

namespace fe
{
namespace ext
{

SurfaceCurvesHoudini::SurfaceCurvesHoudini(void):
	m_pGdp(NULL),
	m_subIndex(-1)
{
#if FE_SCH_DEBUG
	feLog("SurfaceCurvesHoudini()\n");
#endif
}

SurfaceCurvesHoudini::~SurfaceCurvesHoudini(void)
{
#if FE_SCH_DEBUG
	feLog("~SurfaceCurvesHoudini()\n");
#endif
}

Protectable* SurfaceCurvesHoudini::clone(Protectable* pInstance)
{
#if FE_SCH_DEBUG
	feLog("SurfaceCurvesHoudini::clone\n");
#endif

	SurfaceCurvesHoudini* pSurfaceCurvesHoudini= pInstance?
			fe_cast<SurfaceCurvesHoudini>(pInstance):
			new SurfaceCurvesHoudini();

	SurfaceCurves::clone(pSurfaceCurvesHoudini);

	// TODO copy attributes

	return pSurfaceCurvesHoudini;
}

void SurfaceCurvesHoudini::cache(void)
{
#if FE_SCH_DEBUG
	feLog("SurfaceCurvesHoudini::cache\n");
#endif

	//* NOTE not doing any Record serialization

	//* convert Houdini data to vertex and normal arrays

	if(!m_pGdp)
	{
		feLog("SurfaceCurvesHoudini::cache NULL gdp\n");
		return;
	}

	//* TODO e_arrayColor
	setOptionalArrays(e_arrayUV);

#ifdef FE_HOUDINI_USE_GA
	const GA_PrimitiveGroup* pPrimitiveGroup=NULL;
#else
	GB_PrimitiveGroup* pPrimitiveGroup=NULL;
#endif
	if(!m_group.empty())
	{
		pPrimitiveGroup=m_pGdp->findPrimitiveGroup(m_group.c_str());
	}

#ifdef FE_HOUDINI_USE_GA
	const I32 entries=pPrimitiveGroup?
			pPrimitiveGroup->entries(): m_pGdp->getNumPrimitives();
#else
	const I32 entries=pPrimitiveGroup?
			pPrimitiveGroup->entries(): m_pGdp->primitives().entries();
#endif

	if(m_subIndex>=entries)
	{
		feLog("SurfaceCurvesHoudini::cache excessive subIndex %d/%d\n",
				m_subIndex,entries);
		return;
	}
	const U32 primitiveCount=(m_subIndex>=0)? 1: entries;

#if FE_SCH_DEBUG
	feLog("SurfaceCurvesHoudini::cache primitiveCount %d\n",primitiveCount);
#endif

	m_elements=primitiveCount;
	m_vertices=0;
	U32 elementAllocated=0;
	U32 vertexAllocated=0;

#ifdef FE_HOUDINI_USE_GA
	GEO_AttributeHandle handleNormal=
			m_pGdp->getAttribute(GA_ATTRIB_POINT,GEO_STD_ATTRIB_NORMAL);
#else
	GEO_AttributeHandle handleNormal=
			m_pGdp->getAttribute(GEO_POINT_DICT,GEO_STD_ATTRIB_NORMAL);
#endif

	GEO_AttributeHandle handlePartition=
			m_pGdp->getPrimAttribute(m_partitionAttr.c_str());

	//* preallocate
	resizeFor(primitiveCount,primitiveCount*8,elementAllocated,vertexAllocated);

	const GEO_Primitive* pGroupPrimitive=NULL;
	U32 total=0;
	for(U32 m=0;m<primitiveCount;m++)
	{
		if(pPrimitiveGroup)
		{
			if(!m)
			{
				pGroupPrimitive=m_pGdp->primitives().head(*pPrimitiveGroup);
				if(m_subIndex>=0)
				{
					for(U32 n=0;n<m_subIndex;n++)
					{
						FEASSERT(pGroupPrimitive);
						pGroupPrimitive=m_pGdp->primitives().next(
								pGroupPrimitive,*pPrimitiveGroup);
					}
				}
			}
			else
			{
				FEASSERT(pGroupPrimitive);
				pGroupPrimitive=m_pGdp->primitives().next(
						pGroupPrimitive,*pPrimitiveGroup);
			}
		}

		const U32 primitiveIndex=(m_subIndex>=0)? m_subIndex: m;

#ifdef FE_HOUDINI_USE_GA
		const GEO_Primitive* pPrimitive=pPrimitiveGroup?
				pGroupPrimitive: m_pGdp->getGEOPrimitive(
				m_pGdp->primitiveOffset(primitiveIndex));
#else
		const GEO_Primitive* pPrimitive=pPrimitiveGroup?
				pGroupPrimitive: m_pGdp->primitives()(primitiveIndex);
#endif

		if(!pPrimitive)
		{
			break;
		}

		I32 partitionIndex= -1;
		handlePartition.setElement(pPrimitive);
		if(handlePartition.isAttributeValid())
		{
			UT_String utString;
			handlePartition.getString(utString,0);
			const String partition=utString.buffer();
			partitionIndex=lookupPartition(partition);
		}

#ifdef FE_HOUDINI_USE_GA
		if(pPrimitive->getTypeId().get()==GEO_PRIMPOLY)
#else
		if(pPrimitive->getPrimitiveId()==GEOPRIMPOLY)
#endif
		{
#if FE_SCH_DEBUG
			feLog("SurfaceCurvesHoudini::cache prim poly\n");
#endif

			const U32 subCount=pPrimitive->getVertexCount();
			if(subCount<2)
			{
				feLog("SurfaceCurvesHoudini::cache as GEO_PrimPoly"
						" primitive %d/%d subCount=%d (not triangles?)\n",
						primitiveIndex,primitiveCount,subCount);
				continue;
			}

			const U32 startIndex=m_vertices;
			m_vertices+=subCount;

#if FALSE
			U32 desired=vertexAllocated;
			if(!vertexAllocated)
			{
				desired=m_vertices*primitiveCount;
			}
			else if(vertexAllocated<m_vertices)
			{
				desired=m_vertices*1.2;	//* tweak
			}
			if(vertexAllocated!=desired)
			{
				resizeArrays(primitiveCount,desired);
				vertexAllocated=desired;
			}
#else
			if(primitiveCount>elementAllocated || m_vertices>vertexAllocated)
			{
				resizeFor(primitiveCount,m_vertices,
						elementAllocated,vertexAllocated);
			}
#endif

			m_pElementArray[primitiveIndex]=
					Vector3i(startIndex,subCount,TRUE);

			for(U32 subIndex=0;subIndex<subCount;subIndex++)
			{
				FEASSERT(total<vertexAllocated);

#ifdef FE_HOUDINI_USE_GA
				UT_Vector4 utVector4=
						pPrimitive->getVertexElement(subIndex).getPos();

#if FE_HOUDINI_12_5_PLUS
				const GA_Index pointIndex=pPrimitive->getPointIndex(subIndex);
#else
				const U32 pointIndex=
						pPrimitive->getVertexElement(subIndex).getBasePt()->getNum();
#endif

#else
				UT_Vector4 utVector4=
						pPrimitive->getVertex(subIndex).getPt()->getPos();

				const U32 pointIndex=
						pPrimitive->getVertex(subIndex).getBasePt()->getNum();
#endif

				//* location
				m_pVertexArray[total]=Vector3f(utVector4.data());

				//* normal
#ifdef FE_HOUDINI_USE_GA
//				const GEO_Point* pPoint=
//						m_pGdp->getGEOPoint(m_pGdp->pointOffset(pointIndex));
				const GA_Offset pointOffset=m_pGdp->pointOffset(pointIndex);
				handleNormal.setPoint(pointOffset);
#else
				const GEO_Point* pPoint=m_pGdp->points()(pointIndex);
				handleNormal.setElement(pPoint);
#endif

				utVector4=handleNormal.getV4(0);
				m_pNormalArray[total]=Vector3f(utVector4.data());
				m_pTriangleIndexArray[total]=primitiveIndex;
				m_pPointIndexArray[total]=pointIndex;
				m_pPrimitiveIndexArray[total]=primitiveIndex;
				m_pPartitionIndexArray[total]=partitionIndex;
				set(m_pUVArray[total],subIndex/(subCount-1.0),0.0);

#if FE_SCH_DEBUG
				feLog("poly prim %d/%d sub %d/%d part %d uv %s point %d\n",
						primitiveIndex,primitiveCount,
						subIndex,subCount,partitionIndex,
						c_print(m_pUVArray[total]),pointIndex);
				feLog("  vert %s norm %s\n",
						c_print(m_pVertexArray[total]),
						c_print(m_pNormalArray[total]));
#endif


				total++;
			}
		}

#ifdef FE_HOUDINI_USE_GA
		if(pPrimitive->getTypeId().get()==GEO_PRIMNURBSURF ||
				pPrimitive->getTypeId().get()==GEO_PRIMNURBCURVE)
#else
		if(pPrimitive->getPrimitiveId()==GEOPRIMNURBSURF ||
				pPrimitive->getPrimitiveId()==GEOPRIMNURBCURVE)
#endif
		{
			feX(e_unsupported,"SurfaceCurvesHoudini::cache",
					"searchable NURBS curves not yet supported");

			//* TODO
		}

#ifdef FE_HOUDINI_USE_GA
		if(pPrimitive->getTypeId().get()==GEO_PRIMBEZSURF ||
				pPrimitive->getTypeId().get()==GEO_PRIMBEZCURVE)
#else
		if(pPrimitive->getPrimitiveId()==GEOPRIMBEZSURF ||
				pPrimitive->getPrimitiveId()==GEOPRIMBEZCURVE)
#endif
		{
			feX(e_unsupported,"SurfaceCurvesHoudini::cache",
					"searchable Bezier curves not yet supported");

			//* TODO
		}
	}

	resizeFor(primitiveCount,m_vertices,
			elementAllocated,vertexAllocated);

	if(total!=m_vertices)
	{
		feLog("SurfaceCurvesHoudini::cache converted %d/%d vertices\n",
				total,m_vertices);
		for(U32 m=total;m<m_vertices;m++)
		{
			set(m_pVertexArray[m]);
			set(m_pNormalArray[m],0.0,1.0,0.0);
			set(m_pUVArray[m]);
			m_pTriangleIndexArray[m]= -1;
			m_pPointIndexArray[total]= -1;
			m_pPrimitiveIndexArray[total]= -1;
			m_pPartitionIndexArray[total]= -1;
		}
	}

#if FE_SCH_DEBUG
	feLog("SurfaceCurvesHoudini::cache %d vertices\n",m_vertices);
#endif

	calcBoundingSphere();
}

} /* namespace ext */
} /* namespace fe */
