/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <houdini/houdini.pmh>

#define	FE_SAH_GROUP_DEBUG	FALSE
#define	FE_SAH_SCAN_DEBUG	FALSE

namespace fe
{
namespace ext
{

void SurfaceAccessorHoudini::append(
	Array< Array<I32> >& a_rPrimVerts)
{
	const U32 primitiveCount=a_rPrimVerts.size();
	if(!primitiveCount)
	{
		return;
	}

#if FE_HOUDINI_12_5_PLUS
	int vertexCount=0;
	int minOffset= -1;
	int maxOffset= -1;

	GEO_PolyCounts polyCounts;

	for(I32 primitiveIndex=0;primitiveIndex<primitiveCount;primitiveIndex++)
	{
		Array<I32>& rVerts=a_rPrimVerts[primitiveIndex];
		const U32 subCount=rVerts.size();
		for(I32 subIndex=0;subIndex<subCount;subIndex++)
		{
			const I32 pointIndex=rVerts[subIndex];
			const I32 pointOffset=m_pGdpChangeable->pointOffset(pointIndex);
			if(minOffset<0 || minOffset>pointOffset)
			{
				minOffset=pointOffset;
			}
			if(maxOffset<pointOffset)
			{
				maxOffset=pointOffset;
			}
		}

		polyCounts.append(subCount);
		vertexCount+=subCount;
	}

	//* NOTE primitives can have no vertices
//~	if(!vertexCount)
//~	{
//~		return;
//~	}
//~	FEASSERT(minOffset>=0);

	minOffset=fe::maximum(0,minOffset);

	const GA_Offset startOffset=minOffset;
	const GA_Size numOffsets=maxOffset-minOffset+1;
	int* pPolyPoints=new int[vertexCount];	//* offset relative to startOffset;
	I32 vertexIndex=0;

	for(I32 primitiveIndex=0;primitiveIndex<primitiveCount;primitiveIndex++)
	{
		Array<I32>& rVerts=a_rPrimVerts[primitiveIndex];
		const U32 subCount=rVerts.size();
		for(I32 subIndex=0;subIndex<subCount;subIndex++)
		{
			const I32 pointIndex=rVerts[subIndex];
			const I32 pointOffset=m_pGdpChangeable->pointOffset(pointIndex);
			pPolyPoints[vertexIndex++]=pointOffset-startOffset;
			FEASSERT(pointOffset-startOffset<numOffsets);
		}
	}

	const bool closed=true;
	GU_PrimPoly::buildBlock(m_pGdpChangeable,startOffset,numOffsets,
			polyCounts,pPolyPoints,closed);
#else
	SurfaceAccessorBase::append(a_rPrimVerts);
#endif

	delete[] pPolyPoints;
}

String SurfaceAccessorHoudini::type(void) const
{
	if(!m_attrType.empty())
	{
		return m_attrType;
	}

	String attrType ="integer";

#ifdef FE_HOUDINI_USE_GA
	if(m_handle.isSharedStringAttribute())
	{
		attrType="string";
	}
#else
	if(m_handle.isIndexAttribute())
	{
		attrType="string";
	}
#endif

	if(m_handle.isFloatOrVectorAttribute())
	{
		const int floatCount=m_handle.getFloatCount(false);

		switch(floatCount)
		{
			case 1:
				attrType="real";
				break;
			case 2:
				attrType="vector2";
				break;
			case 3:
				attrType="vector3";
				break;
			case 4:
				attrType="vector4";
				break;
			default:
				;
		}
	}

	const_cast<SurfaceAccessorHoudini*>(this)->m_attrType=attrType;

	return m_attrType;
}

void SurfaceAccessorHoudini::addString(void)
{
//	feLog("SurfaceAccessorHoudini::addString \"%s\" %p\n",
//			m_attrName.c_str(),m_pGdpChangeable);

	m_tupleSize=fe::maximum(1,m_tupleSize);

	if(m_pGdpChangeable)
	{
		if(m_element==SurfaceAccessibleI::e_detail)
		{
#ifdef FE_HOUDINI_USE_GA
			m_pGdpChangeable->addStringTuple(GA_ATTRIB_DETAIL,
					m_attrName.c_str(), m_tupleSize);
#else
			m_pGdpChangeable->addAttrib(m_attrName.c_str(),
					sizeof(UT_String),GB_ATTRIB_INDEX,NULL);
#endif
		}
		else if(m_element==SurfaceAccessibleI::e_primitive)
		{
#ifdef FE_HOUDINI_USE_GA
			m_pGdpChangeable->addStringTuple(GA_ATTRIB_PRIMITIVE,
					m_attrName.c_str(), m_tupleSize);
#else
			m_pGdpChangeable->addPrimAttrib(m_attrName.c_str(),
					sizeof(UT_String),GB_ATTRIB_INDEX,NULL);
#endif
		}
		else if(m_element==SurfaceAccessibleI::e_vertex)
		{
#ifdef FE_HOUDINI_USE_GA
			m_pGdpChangeable->addStringTuple(GA_ATTRIB_VERTEX,
					m_attrName.c_str(), m_tupleSize);
#else
			m_pGdpChangeable->addVertexAttrib(m_attrName.c_str(),
					sizeof(UT_String),GB_ATTRIB_INDEX,NULL);
#endif
		}
		else if(m_element==SurfaceAccessibleI::e_point)
		{
#ifdef FE_HOUDINI_USE_GA
			m_pGdpChangeable->addStringTuple(GA_ATTRIB_POINT,
					m_attrName.c_str(), m_tupleSize);
#else
			m_pGdpChangeable->addPointAttrib(m_attrName.c_str(),
					sizeof(UT_String),GB_ATTRIB_INDEX,NULL);
#endif
		}
		bindInternal(m_element,m_attrName);
	}
	if(!m_handle.isAttributeValid())
	{
		feLog("SurfaceAccessorHoudini::addString"
			" could not add %s attribute \"%s\"\n",
			OperatorSurfaceCommon::elementText(m_element).c_str(),
			m_attrName.c_str());
		return;
	}
}
void SurfaceAccessorHoudini::addInteger(void)
{
//	feLog("SurfaceAccessorHoudini::addInteger \"%s\" %p\n",
//			m_attrName.c_str(),m_pGdpChangeable);

	m_tupleSize=fe::maximum(1,m_tupleSize);

	if(m_pGdpChangeable)
	{
		if(m_element==SurfaceAccessibleI::e_detail)
		{
#ifdef FE_HOUDINI_USE_GA
			m_pGdpChangeable->addIntTuple(GA_ATTRIB_DETAIL,
					m_attrName.c_str(), m_tupleSize);
#else
			m_pGdpChangeable->addAttrib(m_attrName.c_str(),
					sizeof(I32),GB_ATTRIB_INT,NULL);
#endif
		}
		else if(m_element==SurfaceAccessibleI::e_primitive)
		{
#ifdef FE_HOUDINI_USE_GA
			m_pGdpChangeable->addIntTuple(GA_ATTRIB_PRIMITIVE,
					m_attrName.c_str(), m_tupleSize);
#else
			m_pGdpChangeable->addPrimAttrib(m_attrName.c_str(),
					sizeof(I32),GB_ATTRIB_INT,NULL);
#endif
		}
		else if(m_element==SurfaceAccessibleI::e_vertex)
		{
#ifdef FE_HOUDINI_USE_GA
			m_pGdpChangeable->addIntTuple(GA_ATTRIB_VERTEX,
					m_attrName.c_str(), m_tupleSize);
#else
			m_pGdpChangeable->addVertexAttrib(m_attrName.c_str(),
					sizeof(I32),GB_ATTRIB_INT,NULL);
#endif
		}
		else if(m_element==SurfaceAccessibleI::e_point)
		{
#ifdef FE_HOUDINI_USE_GA
			m_pGdpChangeable->addIntTuple(GA_ATTRIB_POINT,
					m_attrName.c_str(), m_tupleSize);
#else
			m_pGdpChangeable->addPointAttrib(m_attrName.c_str(),
					sizeof(I32),GB_ATTRIB_INT,NULL);
#endif
		}
		bindInternal(m_element,m_attrName);
	}
	if(!m_handle.isAttributeValid())
	{
		feLog("SurfaceAccessorHoudini::addInteger"
			" could not add %s attribute \"%s\"\n",
			OperatorSurfaceCommon::elementText(m_element).c_str(),
			m_attrName.c_str());
		return;
	}
}

void SurfaceAccessorHoudini::addReal(void)
{
//	feLog("SurfaceAccessorHoudini::addReal \"%s\" %p\n",
//			m_attrName.c_str(),m_pGdpChangeable);

	m_tupleSize=fe::maximum(1,m_tupleSize);

	if(m_pGdpChangeable)
	{
		if(m_element==SurfaceAccessibleI::e_detail)
		{
#ifdef FE_HOUDINI_USE_GA
			m_pGdpChangeable->addFloatTuple(GA_ATTRIB_DETAIL,
					m_attrName.c_str(), m_tupleSize);
#else
			const Real zero=0.0;
			m_pGdpChangeable->addAttrib(m_attrName.c_str(),
					sizeof(Real),GB_ATTRIB_FLOAT,&zero);
#endif
		}
		else if(m_element==SurfaceAccessibleI::e_primitive)
		{
#ifdef FE_HOUDINI_USE_GA
			m_pGdpChangeable->addFloatTuple(GA_ATTRIB_PRIMITIVE,
					m_attrName.c_str(), m_tupleSize);
#else
			const Real zero=0.0;
			m_pGdpChangeable->addPrimAttrib(m_attrName.c_str(),
					sizeof(Real),GB_ATTRIB_FLOAT,&zero);
#endif
		}
		else if(m_element==SurfaceAccessibleI::e_vertex)
		{
#ifdef FE_HOUDINI_USE_GA
			m_pGdpChangeable->addFloatTuple(GA_ATTRIB_VERTEX,
					m_attrName.c_str(), m_tupleSize);
#else
			const Real zero=0.0;
			m_pGdpChangeable->addVertexAttrib(m_attrName.c_str(),
					sizeof(Real),GB_ATTRIB_FLOAT,&zero);
#endif
		}
		else if(m_element==SurfaceAccessibleI::e_point)
		{
#ifdef FE_HOUDINI_USE_GA
			m_pGdpChangeable->addFloatTuple(GA_ATTRIB_POINT,
					m_attrName.c_str(), m_tupleSize);
#else
			const Real zero=0.0;
			m_pGdpChangeable->addPointAttrib(m_attrName.c_str(),
					sizeof(Real),GB_ATTRIB_FLOAT,&zero);
#endif
		}
		bindInternal(m_element,m_attrName);
	}
	if(!m_handle.isAttributeValid())
	{
		feLog("SurfaceAccessorHoudini::addReal"
			" could not add %s attribute \"%s\"\n",
			OperatorSurfaceCommon::elementText(m_element).c_str(),
			m_attrName.c_str());
		return;
	}
}

void SurfaceAccessorHoudini::addSpatialVector(void)
{
//	feLog("SurfaceAccessorHoudini::addSpatialVector \"%s\" %p\n",
//			m_attrName.c_str(),m_pGdpChangeable);

	SurfaceAccessibleI::Element element=m_element;
	if(m_element==SurfaceAccessibleI::e_primitive &&
			m_attribute==SurfaceAccessibleI::e_uv)
	{
		element=SurfaceAccessibleI::e_vertex;
	}

	if(m_pGdpChangeable)
	{
#ifdef FE_HOUDINI_USE_GA
		GA_RWAttributeRef attrRef;
#endif
		if(element==SurfaceAccessibleI::e_detail)
		{
#ifdef FE_HOUDINI_USE_GA
			attrRef=m_pGdpChangeable->addFloatTuple(GA_ATTRIB_DETAIL,
					m_attrName.c_str(), 3);
#else
			m_pGdpChangeable->addAttrib(m_attrName.c_str(),
					sizeof(UT_Vector3),GB_ATTRIB_VECTOR,NULL);
#endif
		}
		else if(element==SurfaceAccessibleI::e_primitive)
		{
#ifdef FE_HOUDINI_USE_GA
			attrRef=m_pGdpChangeable->addFloatTuple(GA_ATTRIB_PRIMITIVE,
					m_attrName.c_str(), 3);
#else
			m_pGdpChangeable->addPrimAttrib(m_attrName.c_str(),
					sizeof(UT_Vector3),GB_ATTRIB_VECTOR,NULL);
#endif
		}
		else if(element==SurfaceAccessibleI::e_vertex)
		{
#ifdef FE_HOUDINI_USE_GA
			attrRef=m_pGdpChangeable->addFloatTuple(GA_ATTRIB_VERTEX,
					m_attrName.c_str(), 3);
#else
			m_pGdpChangeable->addVertexAttrib(m_attrName.c_str(),
					sizeof(UT_Vector3),GB_ATTRIB_VECTOR,NULL);
#endif
		}
		else if(element==SurfaceAccessibleI::e_point)
		{
#ifdef FE_HOUDINI_USE_GA
			attrRef=m_pGdpChangeable->addFloatTuple(GA_ATTRIB_POINT,
					m_attrName.c_str(), 3);
#else
			m_pGdpChangeable->addPointAttrib(m_attrName.c_str(),
					sizeof(UT_Vector3),GB_ATTRIB_VECTOR,NULL);
#endif
		}
#ifdef FE_HOUDINI_USE_GA
		if(attrRef.isValid())
		{
			if(m_attrName=="N")
			{
				attrRef.getAttribute()->setTypeInfo(GA_TYPE_NORMAL);
			}
			else if(m_attrName=="Cd")
			{
				attrRef.getAttribute()->setTypeInfo(GA_TYPE_COLOR);
			}
			else if(m_attrName=="uv")
			{
				//* TODO anything else known to be not directional

				attrRef.getAttribute()->setTypeInfo(GA_TYPE_POINT);
			}
			else
			{
				attrRef.getAttribute()->setTypeInfo(GA_TYPE_VECTOR);
			}
		}
#endif
		bindInternal(m_element,m_attrName);
	}
	if(!m_handle.isAttributeValid())
	{
		feLog("SurfaceAccessorHoudini::addSpatialVector"
			" could not add %s attribute \"%s\"\n",
			OperatorSurfaceCommon::elementText(m_element).c_str(),
			m_attrName.c_str());
		return;
	}
}

BWORD SurfaceAccessorHoudini::bindInternal(
	SurfaceAccessibleI::Element a_element,const String& a_name)
{
	FEASSERT(m_pGdp);
	m_element=SurfaceAccessibleI::e_point;
	if(a_element>=0 && a_element<=5)
	{
		m_element=a_element;
	}
	m_attrName=a_name;
	m_handle.invalidate();
	m_pPrimitive=NULL;
//~	m_pGroup=NULL;
//~	m_groupIndex=0;

#ifdef FE_HOUDINI_USE_GA
//~	m_pIterator=NULL;
#else
	m_pPointChangeable=NULL;
	m_pPoint=NULL;
#endif

	if(m_element==SurfaceAccessibleI::e_primitiveGroup && m_attrName.empty())
	{
		m_element=SurfaceAccessibleI::e_primitive;
		m_attribute=SurfaceAccessibleI::e_vertices;
		return TRUE;
	}

	switch(m_element)
	{
		case SurfaceAccessibleI::e_point:
			if(m_pGdpChangeable)
			{
				m_handle=m_pGdpChangeable->getPointAttribute(a_name.c_str());
			}
			else
			{
				m_handle=m_pGdp->getPointAttribute(a_name.c_str());
			}
			break;

		case SurfaceAccessibleI::e_vertex:
			if(m_pGdpChangeable)
			{
				m_handle=m_pGdpChangeable->getVertexAttribute(
						a_name.c_str());
			}
			else
			{
				m_handle=m_pGdp->getVertexAttribute(a_name.c_str());
			}
			break;

		case SurfaceAccessibleI::e_primitive:
			if(m_attribute==SurfaceAccessibleI::e_uv)
			{
				if(m_pGdpChangeable)
				{
					m_handle=m_pGdpChangeable->getVertexAttribute(
							a_name.c_str());
				}
				else
				{
					m_handle=m_pGdp->getVertexAttribute(a_name.c_str());
				}
			}
			else
			{
				if(m_pGdpChangeable)
				{
					m_handle=m_pGdpChangeable->getPrimAttribute(
							a_name.c_str());
				}
				else
				{
					m_handle=m_pGdp->getPrimAttribute(a_name.c_str());
				}
			}
			break;

		case SurfaceAccessibleI::e_pointGroup:
		{
//~			if(m_pGdpChangeable)
//~			{
//~				m_pGroup=m_pGdpChangeable->findPointGroup(m_attrName.c_str());
//~			}
//~			else
//~			{
//~#ifdef FE_HOUDINI_USE_GA
//~				m_pGroup=const_cast<GA_PointGroup*>(
//~						m_pGdp->findPointGroup(m_attrName.c_str()));
//~#else
//~				m_pGroup=const_cast<GB_PointGroup*>(
//~						m_pGdp->findPointGroup(m_attrName.c_str()));
//~#endif
//~			}
//~			break;

		case SurfaceAccessibleI::e_primitiveGroup:
//~			if(m_pGdpChangeable)
//~			{
//~				m_pGroup=m_pGdpChangeable->findPrimitiveGroup(
//~						m_attrName.c_str());
//~			}
//~			else
//~			{
//~#ifdef FE_HOUDINI_USE_GA
//~				m_pGroup=const_cast<GA_PrimitiveGroup*>(
//~						m_pGdp->findPrimitiveGroup(m_attrName.c_str()));
//~
//~				feLog("entries %d \"%s\"\n",
//~						m_pGroup->entries(),m_attrName.c_str());
//~#else
//~				m_pGroup=const_cast<GB_PrimitiveGroup*>(
//~						m_pGdp->findPrimitiveGroup(m_attrName.c_str()));
//~#endif
//~			}

#if !FE_SAH_MULTIGROUP
			m_spCompoundGroup=new SurfaceAccessorHoudini::CompoundGroup();
			if(m_spCompoundGroup.isValid())
			{
				m_spCompoundGroup->bind(sp<SurfaceAccessorHoudini>(this));
				m_spCompoundGroup->scan(m_attrName);
			}
#endif

			sp<SurfaceAccessibleBase> spAccessible=m_spSurfaceAccessibleI;
			if(spAccessible.isValid())
			{
				m_spMultiGroup=spAccessible->group(m_element,m_attrName);
			}
		}
			break;

		case SurfaceAccessibleI::e_detail:
			if(m_pGdpChangeable)
			{
				m_handle=m_pGdpChangeable->getDetailAttribute(a_name.c_str());
				m_handle.setElement(m_pGdpChangeable);
			}
			else
			{
				m_handle=m_pGdp->getDetailAttribute(a_name.c_str());
				m_handle.setElement(m_pGdp);
			}
			break;

		default:
			;
	}

#ifdef FE_HOUDINI_USE_GA
	m_handleIntRO.bind(m_handle.getAttribute());
	m_handleRealRO.bind(m_handle.getAttribute());
	m_handleV3RO.bind(m_handle.getAttribute());

	if(m_pGdpChangeable)
	{
		GA_AttributeOwner owner=m_handle.getDictionary();
		GA_RWAttributeRef attrRef=m_pGdpChangeable->findAttribute(
				owner,m_attrName.c_str());
		GA_Attribute* pAttribute=attrRef.getAttribute();

		//* NOTE for new attributes, pAttribute may be NULL at first

		m_handleIntRW.bind(pAttribute);
		m_handleRealRW.bind(pAttribute);
		m_handleV3RW.bind(pAttribute);
	}
#endif

	return isBound();
}

void SurfaceAccessorHoudini::harden(void)
{
#if FE_HOUDINI_HARDENING
	if(m_pGdpChangeable)
	{
		sp<SurfaceAccessibleHoudini> spAccessible=m_spSurfaceAccessibleI;
		if(spAccessible.isNull())
		{
			return;
		}

		String attrRate;
		switch(m_element)
		{
			case SurfaceAccessibleI::e_point:
				attrRate="t";
				break;
			case SurfaceAccessibleI::e_pointGroup:
				attrRate="T";
				break;
			case SurfaceAccessibleI::e_vertex:
				attrRate="v";
				break;
			case SurfaceAccessibleI::e_primitive:
				attrRate="r";
				break;
			case SurfaceAccessibleI::e_primitiveGroup:
				attrRate="R";
				break;
			case SurfaceAccessibleI::e_detail:
				attrRate="d";
				break;
		}

		const String attrKey=attrRate+":"+m_attrName;

		m_spHardening=spAccessible->hardening(attrKey);

		if(!m_spHardening->hardened())
		{
			GA_RWAttributeRef attrRef;

			if(m_attribute==SurfaceAccessibleI::e_vertices)
			{
				attrRef=m_pGdpChangeable->findPointAttribute("P");
			}
			else
			{
				GA_AttributeOwner owner=m_handle.getDictionary();
				attrRef=m_pGdpChangeable->findAttribute(
						owner,m_attrName.c_str());
			}

			GA_Attribute* pAttribute=attrRef.getAttribute();

			if(pAttribute)
			{
				m_spHardening->harden(
						new GA_AutoHardenForThreading(*pAttribute));
			}
		}
	}
#endif
}

void SurfaceAccessorHoudini::select(U32 a_index,U32 a_subIndex)
{
	FEASSERT(m_pGdp);
	switch(m_element)
	{
		case SurfaceAccessibleI::e_point:
			if(m_pGdpChangeable)
			{
#ifdef FE_HOUDINI_USE_GA
//				GEO_Point* pPointChangeable=m_pGdpChangeable->getGEOPoint(
//						m_pGdpChangeable->pointOffset(a_index));
//				m_handle.setElement(pPointChangeable);
				m_handle.setPoint(m_pGdpChangeable->pointOffset(a_index));
#else
				m_pPointChangeable=m_pGdpChangeable->points()(a_index);
				m_handle.setElement(m_pPointChangeable);
				m_pPoint=m_pPointChangeable;
#endif
			}
			else
			{
#ifdef FE_HOUDINI_USE_GA
//				const GEO_Point* pPoint=m_pGdp->getGEOPoint(
//						m_pGdp->pointOffset(a_index));
//				m_handle.setElement(pPoint);
				m_handle.setPoint(m_pGdp->pointOffset(a_index));
#else
				m_pPoint=m_pGdp->points()(a_index);
				m_handle.setElement(m_pPoint);
				m_pPointChangeable=NULL;
#endif
			}
			break;

		case SurfaceAccessibleI::e_vertex:
		case SurfaceAccessibleI::e_primitive:
			if(m_pGdpChangeable)
			{
#ifdef FE_HOUDINI_USE_GA
				GEO_Primitive* pPrimitive=m_pGdpChangeable->getGEOPrimitive(
						m_pGdpChangeable->primitiveOffset(a_index));

//				m_handle.setPrimitive(
//						m_pGdpChangeable->primitiveOffset(a_index));
#else
				GEO_Primitive* pPrimitive=
						m_pGdpChangeable->primitives()(a_index);
#endif
				m_pPrimitive=pPrimitive; //* to const

				if(m_element!=SurfaceAccessibleI::e_vertex &&
						m_attribute!=SurfaceAccessibleI::e_uv)
				{
					m_handle.setElement(pPrimitive);
				}
			}
			else
			{
#ifdef FE_HOUDINI_USE_GA
				m_pPrimitive=m_pGdp->getGEOPrimitive(
						m_pGdp->primitiveOffset(a_index));

//				m_handle.setPrimitive(m_pGdp->primitiveOffset(a_index));
#else
				m_pPrimitive=m_pGdp->primitives()(a_index);
#endif

				if(m_element!=SurfaceAccessibleI::e_vertex &&
						m_attribute!=SurfaceAccessibleI::e_uv)
				{
					m_handle.setElement(m_pPrimitive);
				}
			}

			if(m_element==SurfaceAccessibleI::e_vertex ||
					m_attribute==SurfaceAccessibleI::e_uv)
			{
				FEASSERT(m_pPrimitive);
#ifdef FE_HOUDINI_USE_GA
				const GA_Offset offset=
						m_pPrimitive->getVertexOffset(a_subIndex);

				m_handle.setVertex(offset);
#else
				//* TODO
#endif
			}
			break;

		case SurfaceAccessibleI::e_detail:
			//* index not relevant
			break;

		case SurfaceAccessibleI::e_pointGroup:
//~	{
//~#ifdef FE_HOUDINI_USE_GA
//~			if(m_pGdpChangeable)
//~			{
//~				GA_PointGroup* pPointGroup=(GA_PointGroup*)m_pGroup;
//~				if(!m_pIterator)
//~				{
//~					m_groupIndex=0;
//~					GA_Range range=
//~							m_pGdpChangeable->getPointRange(pPointGroup);
//~					m_pIterator=new GA_Iterator(range);
//~				}
//~				U32 m=m_groupIndex;
//~				if(m>a_index)
//~				{
//~					m_pIterator->rewind();
//~					m=0;
//~				}
//~				for(;m<a_index && m_pIterator->isValid();m++)
//~				{
//~					m_pIterator->advance();
//~				}
//~				m_groupIndex=m;
//~			}
//~			else
//~			{
//~				const GA_PointGroup* pPointGroup=(const GA_PointGroup*)m_pGroup;
//~				if(!m_pIterator)
//~				{
//~					m_groupIndex=0;
//~					GA_Range range=m_pGdp->getPointRange(pPointGroup);
//~					m_pIterator=new GA_Iterator(range);
//~				}
//~				U32 m=m_groupIndex;
//~				if(m>a_index)
//~				{
//~					m_pIterator->rewind();
//~					m=0;
//~				}
//~				for(;m<a_index && m_pIterator->isValid();m++)
//~				{
//~					m_pIterator->advance();
//~				}
//~				m_groupIndex=m;
//~			}
//~#else
//~			GB_PointGroup& rPointGroup=*(GB_PointGroup*)m_pGroup;
//~			if(m_pGdpChangeable)
//~			{
//~				if(!m_pPointChangeable)
//~				{
//~					m_groupIndex=0;
//~					m_pPointChangeable=
//~							m_pGdpChangeable->points().head(rPointGroup);
//~				}
//~				U32 m=m_groupIndex;
//~				for(;m>a_index && m_pPointChangeable;m--)
//~				{
//~					m_pPointChangeable=
//~							m_pGdpChangeable->points().prev(
//~							m_pPointChangeable,rPointGroup);
//~				}
//~				for(;m<a_index && m_pPointChangeable;m++)
//~				{
//~					m_pPointChangeable=
//~							m_pGdpChangeable->points().next(
//~							m_pPointChangeable,rPointGroup);
//~				}
//~				m_groupIndex=m;
//~				m_pPoint=m_pPointChangeable;
//~			}
//~			else
//~			{
//~				m_pPointChangeable=NULL;
//~
//~				if(!m_pPoint)
//~				{
//~					m_groupIndex=0;
//~					m_pPoint=m_pGdp->points().head(rPointGroup);
//~				}
//~				U32 m=m_groupIndex;
//~				for(;m>a_index && m_pPoint;m--)
//~				{
//~					m_pPoint=m_pGdp->points().prev(m_pPoint,rPointGroup);
//~				}
//~				for(;m<a_index && m_pPoint;m++)
//~				{
//~					m_pPoint=m_pGdp->points().next(m_pPoint,rPointGroup);
//~				}
//~				m_groupIndex=m;
//~			}
//~#endif
//~		}
#if !FE_SAH_MULTIGROUP
			if(m_spCompoundGroup.isValid())
			{
				m_spCompoundGroup->select(a_index);
#ifndef FE_HOUDINI_USE_GA
				m_pPoint=m_spCompoundGroup->getPoint();
				m_pPointChangeable=m_pGdpChangeable?
						const_cast<GEO_Point*>(m_pPoint): NULL;
#endif
			}
#ifndef FE_HOUDINI_USE_GA
			else
			{
				m_pPoint=NULL;
				m_pPointChangeable=NULL;
			}
#endif
#endif
			break;

		case SurfaceAccessibleI::e_primitiveGroup:
//~		{
//~#ifdef FE_HOUDINI_USE_GA
//~			GA_PrimitiveGroup* pPrimitiveGroup=(GA_PrimitiveGroup*)m_pGroup;
//~			if(!m_pIterator)
//~			{
//~				m_groupIndex=0;
//~				GA_Range range=m_pGdp->getPrimitiveRange(pPrimitiveGroup);
//~				m_pIterator=new GA_Iterator(range);
//~			}
//~			U32 m=m_groupIndex;
//~			if(m>a_index)
//~			{
//~				m_pIterator->rewind();
//~				m=0;
//~			}
//~			for(;m<a_index && m_pIterator->isValid();m++)
//~			{
//~				m_pIterator->advance();
//~			}
//~			m_pPrimitive=m_pGdp->getGEOPrimitive(m_pIterator->getOffset());
//~#else
//~			GB_PrimitiveGroup& rPrimitiveGroup=*(GB_PrimitiveGroup*)m_pGroup;
//~			if(!m_pPrimitive)
//~			{
//~				m_groupIndex=0;
//~				m_pPrimitive=m_pGdp->primitives().head(rPrimitiveGroup);
//~			}
//~			U32 m=m_groupIndex;
//~			for(;m>a_index && m_pPrimitive;m--)
//~			{
//~				m_pPrimitive=
//~						m_pGdp->primitives().prev(m_pPrimitive,rPrimitiveGroup);
//~			}
//~			for(;m<a_index && m_pPrimitive;m++)
//~			{
//~				m_pPrimitive=
//~						m_pGdp->primitives().next(m_pPrimitive,rPrimitiveGroup);
//~			}
//~#endif
//~			m_groupIndex=m;
//~		}
#if FE_SAH_MULTIGROUP
			m_pPrimitive=NULL;
#else
			if(m_spCompoundGroup.isValid())
			{
				m_spCompoundGroup->select(a_index);
				m_pPrimitive=m_spCompoundGroup->getPrimitive();
			}
			else
			{
				m_pPrimitive=NULL;
			}
#endif
			break;

		default:
			;
	}
}

//* TODO use SpannedRange::MultiSpan
void SurfaceAccessorHoudini::CompoundGroup::scan(String a_patterns)
{
#if FE_SAH_SCAN_DEBUG
	feLog("SurfaceAccessorHoudini::CompoundGroup::scan \"%s\"\n",
			a_patterns.c_str());
#endif

	SurfaceAccessibleI::Element element=m_spSurfaceAccessorHoudini->element();
	const GU_Detail* pGdp=m_spSurfaceAccessorHoudini->gdp();
	GU_Detail* pGdpChangeable=m_spSurfaceAccessorHoudini->gdpChangeable();

	Array<String> patternArray;
	String buffer=a_patterns;
	while(!buffer.empty())
	{
		patternArray.push_back(buffer.parse());
	}
	const U32 patternCount=patternArray.size();
	for(U32 patternIndex=0;patternIndex<patternCount;patternIndex++)
	{
		const String pattern=patternArray[patternIndex];

#if FE_SAH_SCAN_DEBUG
		feLog("  pattern %d/%d \"%s\"\n",
				patternIndex,patternCount,pattern.c_str());
#endif

		FEASSERT(!pattern.empty());
		const char first=pattern.c_str()[0];
		if(first>='0' && first<='9')
		{
			buffer=pattern;
			const I32 start=atoi(buffer.parse("\"","-").c_str());
			const I32 end=atoi(buffer.parse("\"","-").c_str());

#if FE_SAH_SCAN_DEBUG
			feLog("  parsed %d to %d\n",start,end);
#endif
		}
	}

	m_groupArray.clear();
	I32 sumCount=0;

	Array<String> nameArray;
	m_spSurfaceAccessorHoudini->surfaceAccessible()->groupNames(
			nameArray,element);

	const U32 nameCount=nameArray.size();
	for(U32 nameIndex=0;nameIndex<nameCount;nameIndex++)
	{
		const String groupName=nameArray[nameIndex];

#if FE_SAH_SCAN_DEBUG
		feLog("  groupName %d/%d \"%s\"\n",
				nameIndex,nameCount,groupName.c_str());
#endif

		for(U32 patternIndex=0;patternIndex<patternCount;patternIndex++)
		{
			const String pattern=patternArray[patternIndex];

#if FE_SAH_SCAN_DEBUG
			feLog("  pattern %d/%d \"%s\"\n",
					patternIndex,patternCount,pattern.c_str());
#endif

			if(!groupName.match(pattern))
			{
				continue;
			}

#if FE_SAH_SCAN_DEBUG
			feLog("  MATCH\n");
#endif

			const U32 groupIndex=m_groupArray.size();
			m_groupArray.resize(groupIndex+1);

#ifdef FE_HOUDINI_USE_GA
			GA_ElementGroup*& rpGroup=m_groupArray[groupIndex];
#else
			GB_Group*& rpGroup=m_groupArray[groupIndex];
#endif

			switch(element)
			{
				case SurfaceAccessibleI::e_pointGroup:
					if(pGdpChangeable)
					{
						rpGroup=pGdpChangeable->findPointGroup(
								groupName.c_str());
					}
					else
					{
#ifdef FE_HOUDINI_USE_GA
						rpGroup=const_cast<GA_PointGroup*>(
								pGdp->findPointGroup(groupName.c_str()));
#else
						rpGroup=const_cast<GB_PointGroup*>(
								pGdp->findPointGroup(groupName.c_str()));
#endif
					}
					break;

				case SurfaceAccessibleI::e_primitiveGroup:
					if(pGdpChangeable)
					{
						rpGroup=pGdpChangeable->findPrimitiveGroup(
								groupName.c_str());
					}
					else
					{
#ifdef FE_HOUDINI_USE_GA
						rpGroup=const_cast<GA_PrimitiveGroup*>(
								pGdp->findPrimitiveGroup(groupName.c_str()));
#else
						rpGroup=const_cast<GB_PrimitiveGroup*>(
								pGdp->findPrimitiveGroup(groupName.c_str()));
#endif
					}
					break;

				default:
					;
			}

			if(!rpGroup)
			{
				m_groupArray.resize(groupIndex);
				continue;
			}

			const U32 subCount=rpGroup->entries();
			m_startEnd.push_back(Vector2i(sumCount,sumCount+subCount-1));

#if FE_SAH_SCAN_DEBUG
			feLog("  start %d count %d\n",sumCount,subCount);
#endif

			sumCount+=subCount;
		}
	}
}

void SurfaceAccessorHoudini::CompoundGroup::create(String a_groupName)
{
#if FE_SAH_GROUP_DEBUG
	feLog("SurfaceAccessorHoudini::CompoundGroup::create \"%s\"\n",
			a_groupName.c_str());
#endif

	SurfaceAccessibleI::Element element=m_spSurfaceAccessorHoudini->element();
	GU_Detail* pGdpChangeable=m_spSurfaceAccessorHoudini->gdpChangeable();

	//* NOTE single explicit group
	m_groupArray.resize(1);
#ifdef FE_HOUDINI_USE_GA
	GA_ElementGroup*& rpGroup=m_groupArray[0];
#else
	GB_Group*& rpGroup=m_groupArray[0];
#endif

	if(element==SurfaceAccessibleI::e_pointGroup)
	{
		rpGroup=pGdpChangeable->newPointGroup(a_groupName.c_str());
	}
	else
	{
		rpGroup=pGdpChangeable->newPrimitiveGroup(a_groupName.c_str());
	}
}

void SurfaceAccessorHoudini::CompoundGroup::append(I32 a_index)
{
#if FE_SAH_GROUP_DEBUG
	feLog("SurfaceAccessorHoudini::CompoundGroup::append %d\n",
			a_index);
#endif

	if(m_groupArray.size()>1)
	{
		feLog("SurfaceAccessorHoudini::CompoundGroup::append"
				" can not change plural compound group\n");
		return;
	}

	//* NOTE single explicit group
	m_groupArray.resize(1);
#ifdef FE_HOUDINI_USE_GA
	GA_ElementGroup*& rpGroup=m_groupArray[0];
#else
	GB_Group*& rpGroup=m_groupArray[0];
#endif

	if(!rpGroup)
	{
		create(m_spSurfaceAccessorHoudini->attributeName());
	}

#ifdef FE_HOUDINI_USE_GA
	rpGroup->addIndex(a_index);
#else
	rpGroup->add(a_index);
#endif
}

void SurfaceAccessorHoudini::CompoundGroup::remove(I32 a_index)
{
#if FE_SAH_GROUP_DEBUG
	feLog("SurfaceAccessorHoudini::CompoundGroup::remove %d\n",
			a_index);
#endif

	if(m_groupArray.size()>1)
	{
		feLog("SurfaceAccessorHoudini::CompoundGroup::append"
				" can not change plural compound group\n");
		return;
	}

	//* NOTE single explicit group
	m_groupArray.resize(1);
#ifdef FE_HOUDINI_USE_GA
	GA_ElementGroup*& rpGroup=m_groupArray[0];
#else
	GB_Group*& rpGroup=m_groupArray[0];
#endif

#ifdef FE_HOUDINI_USE_GA
	rpGroup->removeIndex(a_index);
#else
	rpGroup->remove(a_index);
#endif
}

I32 SurfaceAccessorHoudini::CompoundGroup::count(void)
{
	const U32 groupCount=m_groupArray.size();
	if(!groupCount)
	{
		return 0;
	}

	const I32 totalCount=m_startEnd[groupCount-1][1]+1;

#if FE_SAH_GROUP_DEBUG
	feLog("SurfaceAccessorHoudini::CompoundGroup::count %d\n",
			totalCount);
#endif

	return totalCount;
}

void SurfaceAccessorHoudini::CompoundGroup::select(I32 a_index)
{
	SurfaceAccessibleI::Element element=m_spSurfaceAccessorHoudini->element();
	const GU_Detail* pGdp=m_spSurfaceAccessorHoudini->gdp();
	GU_Detail* pGdpChangeable=m_spSurfaceAccessorHoudini->gdpChangeable();

	const U32 groupCount=m_groupArray.size();
	FEASSERT(m_startEnd.size()==groupCount);

	if(!groupCount)
	{
		return;
	}

	U32 searchIndex=0;
	while(searchIndex<groupCount-1 && m_startEnd[searchIndex][1]<a_index)
	{
		searchIndex++;
	}

#if FE_SAH_GROUP_DEBUG
	feLog("SurfaceAccessorHoudini::CompoundGroup::select %d group %d -> %d\n",
			a_index,m_groupIndex,searchIndex);
#endif

	const BWORD resetGroup=(m_groupIndex!=searchIndex);
	m_groupIndex=searchIndex;

#ifdef FE_HOUDINI_USE_GA
	GA_ElementGroup*& rpGroup=m_groupArray[m_groupIndex];
#else
	GB_Group*& rpGroup=m_groupArray[m_groupIndex];
#endif

	const I32 targetIndex=a_index-m_startEnd[m_groupIndex][0];

	switch(element)
	{
		case SurfaceAccessibleI::e_pointGroup:
		{
#ifdef FE_HOUDINI_USE_GA
			if(resetGroup)
			{
				m_pIterator=NULL;
			}

			if(pGdpChangeable)
			{
				GA_PointGroup* pPointGroup=(GA_PointGroup*)rpGroup;
				if(!m_pIterator)
				{
					m_subIndex=0;
					GA_Range range=
							pGdpChangeable->getPointRange(pPointGroup);
					m_pIterator=new GA_Iterator(range);
				}
				U32 m=m_subIndex;
				if(m>targetIndex)
				{
					m_pIterator->rewind();
					m=0;
				}
				for(;m<targetIndex && m_pIterator->isValid();m++)
				{
					m_pIterator->advance();
				}
				m_subIndex=m;
			}
			else
			{
				const GA_PointGroup* pPointGroup=(const GA_PointGroup*)rpGroup;
				if(!m_pIterator)
				{
					m_subIndex=0;
					GA_Range range=pGdp->getPointRange(pPointGroup);
					m_pIterator=new GA_Iterator(range);
				}
				U32 m=m_subIndex;
				if(m>targetIndex)
				{
					m_pIterator->rewind();
					m=0;
				}
				for(;m<targetIndex && m_pIterator->isValid();m++)
				{
					m_pIterator->advance();
				}
				m_subIndex=m;
			}
#else
			if(resetGroup)
			{
				m_pPoint=NULL;
				m_pPointChangeable=NULL;
			}

			GB_PointGroup& rPointGroup=*(GB_PointGroup*)rpGroup;
			if(pGdpChangeable)
			{
				if(!m_pPointChangeable)
				{
					m_subIndex=0;
					m_pPointChangeable=
							pGdpChangeable->points().head(rPointGroup);
				}
				U32 m=m_subIndex;
				for(;m>targetIndex && m_pPointChangeable;m--)
				{
					m_pPointChangeable=
							pGdpChangeable->points().prev(
							m_pPointChangeable,rPointGroup);
				}
				for(;m<targetIndex && m_pPointChangeable;m++)
				{
					m_pPointChangeable=
							pGdpChangeable->points().next(
							m_pPointChangeable,rPointGroup);
				}
				m_subIndex=m;
				m_pPoint=m_pPointChangeable;
			}
			else
			{
				m_pPointChangeable=NULL;

				if(!m_pPoint)
				{
					m_subIndex=0;
					m_pPoint=pGdp->points().head(rPointGroup);
				}
				U32 m=m_subIndex;
				for(;m>targetIndex && m_pPoint;m--)
				{
					m_pPoint=pGdp->points().prev(m_pPoint,rPointGroup);
				}
				for(;m<targetIndex && m_pPoint;m++)
				{
					m_pPoint=pGdp->points().next(m_pPoint,rPointGroup);
				}
				m_subIndex=m;
			}
#endif
		}
			break;

		case SurfaceAccessibleI::e_primitiveGroup:
		{
#ifdef FE_HOUDINI_USE_GA
			if(resetGroup)
			{
				m_pIterator=NULL;
			}

			GA_PrimitiveGroup* pPrimitiveGroup=(GA_PrimitiveGroup*)rpGroup;
			if(!m_pIterator)
			{
				m_subIndex=0;
				GA_Range range=pGdp->getPrimitiveRange(pPrimitiveGroup);
				m_pIterator=new GA_Iterator(range);
			}
			U32 m=m_subIndex;
			if(m>targetIndex)
			{
				m_pIterator->rewind();
				m=0;
			}
			for(;m<targetIndex && m_pIterator->isValid();m++)
			{
				m_pIterator->advance();
			}
			m_pPrimitive=pGdp->getGEOPrimitive(m_pIterator->getOffset());
#else
			if(resetGroup)
			{
				m_pPrimitive=NULL;
			}

			GB_PrimitiveGroup& rPrimitiveGroup=*(GB_PrimitiveGroup*)rpGroup;
			if(!m_pPrimitive)
			{
				m_subIndex=0;
				m_pPrimitive=pGdp->primitives().head(rPrimitiveGroup);
			}
			U32 m=m_subIndex;
			for(;m>targetIndex && m_pPrimitive;m--)
			{
				m_pPrimitive=
						pGdp->primitives().prev(m_pPrimitive,rPrimitiveGroup);
			}
			for(;m<targetIndex && m_pPrimitive;m++)
			{
				m_pPrimitive=
						pGdp->primitives().next(m_pPrimitive,rPrimitiveGroup);
			}
#endif
			m_subIndex=m;
		}
			break;

		default:
			;
	}
}

I32 SurfaceAccessorHoudini::CompoundGroup::getIndex(void)
{
	SurfaceAccessibleI::Element element=m_spSurfaceAccessorHoudini->element();

#ifdef FE_HOUDINI_USE_GA
	const GU_Detail* pGdp=m_spSurfaceAccessorHoudini->gdp();
#endif

	I32 elementIndex= -1;

	switch(element)
	{

		case SurfaceAccessibleI::e_pointGroup:
		{
#ifdef FE_HOUDINI_USE_GA
			if(m_pIterator && m_pIterator->isValid())
			{
				elementIndex=pGdp->pointIndex(m_pIterator->getOffset());
			}
#else
			FEASSERT(m_pPoint);
			elementIndex=m_pPoint->getNum();
#endif
		}
			break;

		case SurfaceAccessibleI::e_primitiveGroup:
		{
#ifdef FE_HOUDINI_USE_GA
			if(m_pIterator && m_pIterator->isValid())
			{
				const GA_Offset offset=m_pIterator->getOffset();
				elementIndex=pGdp->primitiveIndex(offset);
			}
#else
			FEASSERT(m_pPrimitive);
			elementIndex=m_pPrimitive->getNum();
#endif
		}
			break;

		default:
			;
	}

#if FE_SAH_GROUP_DEBUG
	feLog("SurfaceAccessorHoudini::CompoundGroup::getIndex %d\n",
			elementIndex);
#endif

	return elementIndex;
}

const GEO_Point* SurfaceAccessorHoudini::CompoundGroup::getPoint(void)
{
#if FE_SAH_GROUP_DEBUG
	feLog("SurfaceAccessorHoudini::CompoundGroup::getPoint\n");
#endif

#ifdef FE_HOUDINI_USE_GA
	return NULL;
#else
	return m_pPoint;
#endif
}

const GEO_Primitive* SurfaceAccessorHoudini::CompoundGroup::getPrimitive(void)
{
#if FE_SAH_GROUP_DEBUG
	feLog("SurfaceAccessorHoudini::CompoundGroup::getPrimitive %p\n",
			m_pPrimitive);
#endif

	return m_pPrimitive;
}

} /* namespace ext */
} /* namespace fe */
