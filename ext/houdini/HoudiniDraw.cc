/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <houdini/houdini.pmh>

#define	FE_HD_AUTOROTATE	FALSE	// convert for z-up to y-up

namespace fe
{
namespace ext
{

void HoudiniDraw::drawPoints(const SpatialVector *vertex,
	const SpatialVector *normal,U32 vertices,
	BWORD multicolor,const Color *color,
	sp<DrawBufferI> spDrawBuffer)
{
	if(!gdp())
	{
		feLog("HoudiniDraw::drawPoints gdp is NULL\n");
		return;
	}

#if FE_HOUDINI_12_5_PLUS
	const GA_Offset offset=gdp()->appendPointBlock(vertices);

	for(U32 index=0;index<vertices;index++)
	{
		const SpatialVector& vert=vertex[index];

#if FE_HD_AUTOROTATE
		UT_Vector3 utPoint3(vert[1],vert[2],vert[0]);
#else
		UT_Vector3 utPoint3(vert[0],vert[1],vert[2]);
#endif

		gdp()->setPos3(offset+index,utPoint3);
	}
#else
	for(U32 index=0;index<vertices;index++)
	{
		const SpatialVector& vert=vertex[index];

#if FE_HD_AUTOROTATE
		UT_Vector3 utPoint3(vert[1],vert[2],vert[0]);
#else
		UT_Vector3 utPoint3(vert[0],vert[1],vert[2]);
#endif

		GEO_Point* pGeoPoint=gdp()->appendPoint();

		pGeoPoint->setPos(utPoint3);
	}
#endif
}

void HoudiniDraw::drawLines(const SpatialVector *vertex,
	const SpatialVector *normal,U32 vertices,
	StripMode strip,BWORD multicolor,const Color *color,
	BWORD multiradius,const Real *radius,
	const Vector3i *element,U32 elementCount,
	sp<DrawBufferI> spDrawBuffer)
{
	if(element==NULL)
	{
		drawPolygons(vertex,normal,vertices,strip,multicolor,color,TRUE);
		return;
	}

	for(U32 elementIndex=0;elementIndex<elementCount;elementIndex++)
	{
		const U32 start=element[elementIndex][0];
		const U32 subCount=element[elementIndex][1];
		const BWORD openCurve=element[elementIndex][2];
		if(!subCount || !openCurve)
		{
			continue;
		}

		drawPolygons(&vertex[start],&normal[start],subCount,
				strip,multicolor,color,TRUE);
	}
}

void HoudiniDraw::drawTriangles(const SpatialVector *vertex,
	const SpatialVector *normal,const Vector2 *texture,U32 vertices,
	StripMode strip,BWORD multicolor,const Color* color,
	const Array<I32>* vertexMap,
	const Array<I32>* hullPointMap,const Array<Vector4i>* hullFacePoint,
	sp<DrawBufferI> spDrawBuffer)
{
	//* TODO other strip modes

	if(strip==DrawI::e_strip)
	{
		drawPolygons(vertex,normal,vertices,strip,multicolor,color,FALSE);
	}
	else if(strip==DrawI::e_discrete)
	{
		for(U32 m=0;m<vertices;m+=3)
		{
			if(color && multicolor)
			{
				drawPolygons(&vertex[m],&normal[m],3,DrawI::e_strip,
						multicolor,&color[m],FALSE);
			}
			else
			{
				drawPolygons(&vertex[m],&normal[m],3,DrawI::e_strip,
						multicolor,color,FALSE);
			}
		}
	}
}

void HoudiniDraw::drawPolygons(const SpatialVector *vertex,
	const SpatialVector *normal,U32 vertices,
	StripMode strip,BWORD multicolor,const Color *color,BWORD openPoly)
{
	if(!gdp())
	{
		feLog("HoudiniDraw::drawPolygons gdp is NULL\n");
		return;
	}

	sp<DrawMode> spDrawMode=drawMode();
	String group;
	if(spDrawMode.isValid())
	{
		group=spDrawMode->group();
	}

	const U32 subCount=(strip==DrawI::e_strip)? vertices: 2;
	for(U32 index=0;index<vertices;index+=subCount)
	{
#ifdef FE_HOUDINI_USE_GA
		GA_PointGroup* pPointGroup=NULL;
#else
		GB_PointGroup* pPointGroup=NULL;
#endif
		if(!group.empty())
		{
			pPointGroup=m_pGdp->findPointGroup(group.c_str());
			if(!pPointGroup)
			{
				pPointGroup=m_pGdp->newPointGroup(group.c_str());
			}
			FEASSERT(pPointGroup);
		}

		const int npts=subCount;
		const int appendPts=1;

		GEO_TriMesh* pGeoTriMesh=NULL;
		if(strip==DrawI::e_strip && !openPoly)
		{
			pGeoTriMesh=(GEO_TriMesh*)
					GU_PrimTriStrip::build(gdp(),npts,appendPts);
		}
		else
		{
			const int open=openPoly? GU_POLY_OPEN: GU_POLY_CLOSED;

			pGeoTriMesh=(GEO_TriMesh*)
					GU_PrimPoly::build(gdp(),npts,open,appendPts);
		}

		GEO_AttributeHandle handle;
		if(color)
		{
#ifdef FE_HOUDINI_USE_GA
			handle=m_pGdp->getAttribute(GA_ATTRIB_POINT,GEO_STD_ATTRIB_DIFFUSE);
#else
			handle=m_pGdp->getAttribute(GEO_POINT_DICT,GEO_STD_ATTRIB_DIFFUSE);
#endif

			if(!handle.isAttributeValid())
			{
#ifdef FE_HOUDINI_USE_GA
				GA_RWAttributeRef attrCd=
						m_pGdp->addFloatTuple(GA_ATTRIB_POINT, "Cd", 3);
				if(attrCd.isValid())
				{
					attrCd.getAttribute()->setTypeInfo(GA_TYPE_COLOR);
				}
				handle=m_pGdp->getAttribute(GA_ATTRIB_POINT,
						GEO_STD_ATTRIB_DIFFUSE);
#else
				const float defaultColor[3]={0.0, 0.0, 0.0};
				m_pGdp->addPointAttrib(GEO_STD_ATTRIB_DIFFUSE,
						3*sizeof(float),GB_ATTRIB_FLOAT,&defaultColor);
				handle=m_pGdp->getAttribute(GEO_POINT_DICT,
						GEO_STD_ATTRIB_DIFFUSE);
#endif
			}
		}

		for(U32 subIndex=0;subIndex<subCount;subIndex++)
		{
			const SpatialVector& vert=vertex[index+subIndex];

#if FE_HD_AUTOROTATE
			UT_Vector3 utPoint3(vert[1],vert[2],vert[0]);
#else
			UT_Vector3 utPoint3(vert[0],vert[1],vert[2]);
#endif

#ifdef FE_HOUDINI_USE_GA
//			GEO_Point* pGeoPoint=
//					pGeoTriMesh->getVertexElement(subIndex).getPt();

			const GA_Offset pointOffset=pGeoTriMesh->getPointOffset(subIndex);
			m_pGdp->setPos3(pointOffset,utPoint3);

			if(pPointGroup)
			{
				pPointGroup->addOffset(pointOffset);
			}
#else
			GEO_Point* pGeoPoint=pGeoTriMesh->getVertex(subIndex).getPt();
			pGeoPoint->setPos(utPoint3);

			if(pPointGroup)
			{
				pPointGroup->add(pGeoPoint);
			}
#endif

			if(color)
			{
				const SpatialVector& color0=
						color[multicolor? index+subIndex: 0];

#ifdef FE_HOUDINI_USE_GA
				handle.setPoint(pointOffset);
#else
				handle.setElement(pGeoPoint);
#endif

				UT_Vector4 utVector4(color0.temp());
				handle.setV4(utVector4);
			}

#ifdef FE_HOUDINI_USE_GA
//			const I32 pointIndex=pGeoPoint->getNum();
//			const GA_Offset offset=
//					m_pGdp->pointOffset(pointIndex);

//			const GA_Offset offset=pGeoPoint->getMapOffset();

			pGeoTriMesh->setVertexPoint(subIndex,pointOffset);
#else
			pGeoTriMesh->setVertex(subIndex, pGeoPoint);
#endif
		}
	}
}

} /* namespace ext */
} /* namespace fe */