/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <houdini/houdini.pmh>

#define FE_SPH_DEBUG	FALSE

namespace fe
{
namespace ext
{

SurfacePointsHoudini::SurfacePointsHoudini(void):
	m_pGdp(NULL),
	m_subIndex(-1)
{
#if FE_SPH_DEBUG
	feLog("SurfacePointsHoudini()\n");
#endif
}

SurfacePointsHoudini::~SurfacePointsHoudini(void)
{
#if FE_SPH_DEBUG
	feLog("~SurfacePointsHoudini()\n");
#endif
}

Protectable* SurfacePointsHoudini::clone(Protectable* pInstance)
{
#if FE_SPH_DEBUG
	feLog("SurfacePointsHoudini::clone\n");
#endif

	SurfacePointsHoudini* pSurfacePointsHoudini= pInstance?
			fe_cast<SurfacePointsHoudini>(pInstance):
			new SurfacePointsHoudini();

	SurfaceCurves::clone(pSurfacePointsHoudini);

	// TODO copy attributes

	return pSurfacePointsHoudini;
}

void SurfacePointsHoudini::cache(void)
{
#if FE_SPH_DEBUG
	feLog("SurfacePointsHoudini::cache\n");
#endif

	//* convert Houdini data to vertex and normal arrays

	//* NOTE m_subIndex ignored

	if(!m_pGdp)
	{
		feLog("SurfacePointsHoudini::cache NULL gdp\n");
		return;
	}

	//* TODO e_arrayColor
	setOptionalArrays(e_arrayUV);

#ifdef FE_HOUDINI_USE_GA
	const GA_PointGroup* pPointGroup=NULL;
#else
	GB_PointGroup* pPointGroup=NULL;
#endif
	if(!m_group.empty())
	{
		pPointGroup=m_pGdp->findPointGroup(m_group.c_str());
	}

#ifdef FE_HOUDINI_USE_GA
	const I32 entries=pPointGroup?
			pPointGroup->entries(): m_pGdp->getNumPoints();
#else
	const I32 entries=pPointGroup?
			pPointGroup->entries(): m_pGdp->points().entries();
#endif

	const U32 pointCount=entries;

#if FE_SPH_DEBUG
	feLog("SurfacePointsHoudini::cache pointCount %d\n",pointCount);
#endif

	m_elements=pointCount;
	m_vertices=pointCount;
	U32 elementAllocated=0;
	U32 vertexAllocated=0;
	BWORD keepUV=FALSE;
	BWORD keepTangent=FALSE;

#ifdef FE_HOUDINI_USE_GA
	GA_Iterator* pIterator=NULL;
	GEO_AttributeHandle handlePosition=
			m_pGdp->getAttribute(GA_ATTRIB_POINT,GEO_STD_ATTRIB_POSITION);
	GEO_AttributeHandle handleNormal=
			m_pGdp->getAttribute(GA_ATTRIB_POINT,GEO_STD_ATTRIB_NORMAL);
	GEO_AttributeHandle handlePointUV=
			m_pGdp->getAttribute(GA_ATTRIB_POINT,"uv");
	GEO_AttributeHandle handlePointTangent=
			m_pGdp->getAttribute(GA_ATTRIB_POINT,"tangent");
#else
	GEO_AttributeHandle handleNormal=
			m_pGdp->getAttribute(GEO_POINT_DICT,GEO_STD_ATTRIB_NORMAL);
	GEO_AttributeHandle handlePointUV=
			m_pGdp->getAttribute(GEO_POINT_DICT,"uv");
	GEO_AttributeHandle handlePointTangent=
			m_pGdp->getAttribute(GEO_POINT_DICT,"tangent");
#endif

	GEO_AttributeHandle handlePartition=
			m_pGdp->getPointAttribute(m_partitionAttr.c_str());

	//* preallocate
	resizeFor(pointCount,pointCount,elementAllocated,vertexAllocated);

	//* NOTE m_pTangentArray not allocated with resizeFor()
	m_pTangentArray=(SpatialVector*)reallocate(m_pTangentArray,
			pointCount*sizeof(SpatialVector));

	const GEO_Point* pGroupPoint=NULL;

	for(U32 pointIndex=0;pointIndex<pointCount;pointIndex++)
	{
#ifdef FE_HOUDINI_USE_GA
		GA_Offset pointOffset=0;

		if(pPointGroup)
		{
			if(!pointIndex)
			{
				GA_Range range=m_pGdp->getPointRange(pPointGroup);
				pIterator=new GA_Iterator(range);
			}
			else
			{
				FEASSERT(pGroupPoint);
				FEASSERT(pIterator);
				pIterator->advance();
			}

			pointOffset=pIterator->getOffset();
		}

		pointOffset=m_pGdp->pointOffset(pointIndex);

		handlePosition.setPoint(pointOffset);
		handlePartition.setPoint(pointOffset);

		UT_Vector4 utVector4=handlePosition.getV4(0);
#else
		if(pPointGroup)
		{
			if(!pointIndex)
			{
				pGroupPoint=m_pGdp->points().head(*pPointGroup);
			}
			else
			{
				FEASSERT(pGroupPoint);
				pGroupPoint=m_pGdp->points().next(pGroupPoint,*pPointGroup);
			}
		}

		const GEO_Point* pPoint=pPointGroup?
				pGroupPoint: m_pGdp->points()(pointIndex);
		if(!pPoint)
		{
			break;
		}

		handlePartition.setElement(pPoint);

		UT_Vector4 utVector4=pPoint->getPos();
#endif

		I32 partitionIndex= -1;
		if(handlePartition.isAttributeValid())
		{
			UT_String utString;
			handlePartition.getString(utString,0);
			const String partition=utString.buffer();
			partitionIndex=lookupPartition(partition);
		}

		m_pElementArray[pointIndex]=Vector3i(pointIndex,1,TRUE);

		//* location
		m_pVertexArray[pointIndex]=Vector3f(utVector4.data());

		//* normal
#ifdef FE_HOUDINI_USE_GA
		handleNormal.setPoint(pointOffset);
#else
		handleNormal.setElement(pPoint);
#endif

		utVector4=handleNormal.getV4(0);

		m_pNormalArray[pointIndex]=Vector3f(utVector4.data());


#ifdef FE_HOUDINI_USE_GA
		handlePointUV.setPoint(pointOffset);
#else
		handlePointUV.setElement(pPoint);
#endif

		Vector2 uv;
		if(handlePointUV.isAttributeValid())
		{
			UT_Vector3 utVector3=handlePointUV.getV3(0);
			uv=Vector2f(utVector3.data());
			keepUV=TRUE;
		}
		else
		{
			set(uv);
		}

		m_pUVArray[pointIndex]=uv;

#ifdef FE_HOUDINI_USE_GA
		handlePointTangent.setPoint(pointOffset);
#else
		handlePointTangent.setElement(pPoint);
#endif

		SpatialVector tangent;
		if(handlePointTangent.isAttributeValid())
		{
			UT_Vector3 utVector3=handlePointTangent.getV3(0);
			tangent=Vector3f(utVector3.data());
			keepTangent=TRUE;
		}
		else
		{
			set(tangent);
		}

		m_pTangentArray[pointIndex]=tangent;

		m_pTriangleIndexArray[pointIndex]=pointIndex;
		m_pPointIndexArray[pointIndex]=pointIndex;
		m_pPrimitiveIndexArray[pointIndex]=pointIndex;
		m_pPartitionIndexArray[pointIndex]=partitionIndex;

#if FE_SPH_DEBUG
		feLog("point %d/%d part %d uv %s\n",
				pointIndex,pointCount,partitionIndex,
				c_print(m_pUVArray[pointIndex]));
		feLog("  vert %s norm %s tangent %s\n",
				c_print(m_pVertexArray[pointIndex]),
				c_print(m_pNormalArray[pointIndex]),
				c_print(m_pTangentArray[pointIndex]));
#endif
	}

#ifdef FE_HOUDINI_USE_GA
	if(pIterator)
	{
		delete pIterator;
	}
#endif

	if(!keepUV)
	{
		deallocate(m_pUVArray);
		m_pUVArray=NULL;
	}

	if(!keepTangent)
	{
		deallocate(m_pTangentArray);
		m_pTangentArray=NULL;
	}

#if FE_SPH_DEBUG
	feLog("SurfacePointsHoudini::cache %d vertices\n",m_vertices);
#endif

	calcBoundingSphere();
}

} /* namespace ext */
} /* namespace fe */
