/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __houdini_HoudiniContext_h__
#define __houdini_HoudiniContext_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
    @brief shared FE context for Houdini plugins

	@ingroup houdini
*//***************************************************************************/
class HoudiniContext:
	public OperatorContext,
	public CastableAs<HoudiniContext>
{
	public:
				HoudiniContext(void)
				{
					m_libEnvVar="FE_HOUDINI_LIBS";
#ifdef FE_HOUDINI_LIBS
					m_libDefault=FE_STRING(FE_HOUDINI_LIBS);
#else
					m_libDefault="fexHoudiniDL";
#endif

				}
virtual			~HoudiniContext(void)										{}
};

} /* namespace ext */
} /* namespace fe */

#endif /* __houdini_HoudiniContext_h__ */
