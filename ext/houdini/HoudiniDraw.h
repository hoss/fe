/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __houdini_HoudiniDraw_h__
#define __houdini_HoudiniDraw_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
    @brief Draw into Houdini GU_Detail (gdp)

	@ingroup houdini
*//***************************************************************************/
class FE_DL_EXPORT HoudiniDraw:
	public DrawCommon,
	public CastableAs<HoudiniDraw>
{
	public:
					HoudiniDraw(void): m_pGdp(NULL)							{}
virtual				~HoudiniDraw(void)										{}

					//* as DrawI

					using DrawCommon::drawPoints;

virtual void		drawPoints(const SpatialVector *vertex,
							const SpatialVector *normal,U32 vertices,
							BWORD multicolor,const Color *color,
							sp<DrawBufferI> spDrawBuffer);

					using DrawCommon::drawLines;

virtual	void		drawLines(const SpatialVector *vertex,
							const SpatialVector *normal,U32 vertices,
							StripMode strip,
							BWORD multicolor,const Color *color,
							BWORD multiradius,const Real *radius,
							const Vector3i *element,U32 elementCount,
							sp<DrawBufferI> spDrawBuffer);

					using DrawCommon::drawTriangles;

virtual void		drawTriangles(const SpatialVector *vertex,
							const SpatialVector *normal,
							const Vector2 *texture,U32 vertices,
							StripMode strip,BWORD multicolor,
							const Color* color,
							const Array<I32>* vertexMap,
							const Array<I32>* hullPointMap,
							const Array<Vector4i>* hullFacePoint,
							sp<DrawBufferI> spDrawBuffer);

					//* Houdini specific

		void		setGdp(GU_Detail* a_pGdp)	{ m_pGdp=a_pGdp; }
		GU_Detail*	gdp(void)					{ return m_pGdp; }

	private:
		void		drawPolygons(const SpatialVector *vertex,
							const SpatialVector *normal,U32 vertices,
							StripMode strip,BWORD multicolor,
							const Color *color,BWORD openPoly);

		GU_Detail*	m_pGdp;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __houdini_HoudiniDraw_h__ */
