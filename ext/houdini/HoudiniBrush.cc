/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <houdini/houdini.pmh>

#define FE_HBR_DISCOVER_DEBUG	FALSE
#define FE_HBR_EVENT_DEBUG		FALSE
#define FE_HBR_RENDER_DEBUG		FALSE
#define FE_HBR_CAMERA_DEBUG		FALSE

#include <DM/DM_Defines.h>
#include <DM/DM_ViewportType.h>
#include <GR/GR_DisplayOption.h>
#include <GU/GU_PrimCircle.h>
#include <MSS/MSS_SingleOpState.h>
#include <RE/RE_Render.h>
#include <RE/RE_Material.h>

//#include <GR/GR_Detail.h>

#define MSS_CLICK_BUTTONS (DM_PRIMARY_BUTTON|DM_SECONDARY_BUTTON)

extern "C"
{

//* brush discovery point for Houdini
FE_DL_EXPORT void newModelState(BM_ResourceManager *a_pManager)
{
#if FE_HBR_DISCOVER_DEBUG
	feLog("::newModelState\n");
#endif

	//* NOTE 20200709 std::regex crashes, so use boost::regex
	fe::Regex::confirm("fexBoostRegex");

	fe::ext::HoudiniBrush::discoverBrushes(a_pManager);
}

}

namespace fe
{
namespace ext
{

//* NOTE stripped down from HoudiniSOP::discoverOperators
//* static
String HoudiniBrush::scanForBrushNames(void)
{
	String brushNames;

	HoudiniContext houdiniContext;

#if FE_HBR_DISCOVER_DEBUG
	feLog("HoudiniSOP:scanForBrushNames master=%p\n",
		houdiniContext.master().raw());
#endif

	sp<Registry> spRegistry=houdiniContext.master()->registry();
	const BWORD firstLoad=houdiniContext.loadLibraries();

	const char* exposure=getenv("FE_EXPOSURE");
	const BWORD showNoHoudini=exposure && exposure==String("all");

	Array<String> available;
	spRegistry->listAvailable("OperatorSurfaceI",available);
	U32 candidates=available.size();
	for(U32 index=0;index<candidates;index++)
	{
#if FE_HBR_DISCOVER_DEBUG
		feLog("checking %d/%d \"%s\"\n",
				index,candidates,available[index].c_str());
#endif

		//* ignore operators tagged as nohoudini
		if(!showNoHoudini && available[index].dotMatch("*.*.*.nohoudini"))
		{
#if FE_CODEGEN<=FE_DEBUG
			feLog("HoudiniBrush:scanForBrushNames"
					" ignoring houdini-excluded operator:\n"
					"  %s\n",available[index].c_str());
#endif

			continue;
		}

		sp<OperatorSurfaceI> spOperatorSurfaceI=
				spRegistry->create(available[index]);
		if(!spOperatorSurfaceI.isValid())
		{
			feLog("HoudiniBrush:scanForBrushNames"
					" Component is not an OperatorSurfaceI \"%s\"\n",
					available[index].c_str());
			continue;
		}

		const String rawName=available[index];
		const String feName=HoudiniSOP::classOfImplementation(rawName);

		Array<String> keys;

		sp<Catalog> spCatalog=spOperatorSurfaceI;
		if(spCatalog.isValid())
		{
			spCatalog->catalogKeys(keys);
		}

		const U32 keyCount=keys.size();
		for(U32 keyIndex=0;keyIndex<keyCount;keyIndex++)
		{
			Instance instance;
			bool success=spCatalog->catalogLookup(keys[keyIndex],instance);
			if(!success)
			{
				continue;
			}
			if(instance.is< sp<Component> >())
			{
				const String io=spCatalog->catalogOrDefault<String>(
						keys[keyIndex],"IO","input");
				if(io!="input")
				{
					continue;
				}

				const String implementation=
						spCatalog->catalogOrDefault<String>(keys[keyIndex],
						"implementation",
						"SurfaceAccessibleI.SurfaceAccessibleHoudini");

				if(implementation=="DrawI")
				{
#if FE_HBR_DISCOVER_DEBUG
					feLog("HoudiniBrush::scanForBrushNames"
							" found \"%s\"\n",feName.c_str());
#endif

					if(!brushNames.empty())
					{
						brushNames+=" ";
					}
					brushNames+=feName;
				}
			}
		}
	}

	return brushNames;
}

int HoudiniBrush::discoverBrushes(BM_ResourceManager *a_pManager)
{
	String brushNames=scanForBrushNames();

#if FE_HBR_DISCOVER_DEBUG
	feLog("HoudiniBrush::discoverBrushes \"%s\"\n",brushNames.c_str());
#endif

	U32 brushCount=0;
	while(!brushNames.empty())
	{
		const String brushName=brushNames.parse();

#if FE_HBR_DISCOVER_DEBUG
		feLog("HoudiniBrush::discoverBrushes %d \"%s\"\n",
				brushCount,brushName.c_str());
#endif

		a_pManager->registerState(new PI_StateTemplate(
				brushName.c_str(),					// state name
				("FE "+brushName).c_str(),			// English name
				"FE",								// icon name
				(void *)fe::ext::HoudiniBrush::construct,
				NULL,
				PI_VIEWER_SCENE,
				PI_NETMASK_SOP,						// marks as a SOP state
				0));

		brushCount++;
	}

	return brushCount;
}

BM_State* HoudiniBrush::construct(BM_View &a_rView,
	PI_StateTemplate &a_rTemplate,BM_SceneManager *a_pScene)
{
	return new HoudiniBrush((JEDI_View &)a_rView,a_rTemplate,a_pScene);
}

/*
#define DM_VIEWPORT_ORTHO       0x0ff
#define DM_VIEWPORT_TOP         0x001
#define DM_VIEWPORT_FRONT       0x002
#define DM_VIEWPORT_RIGHT       0x004
#define DM_VIEWPORT_BOTTOM      0x008
#define DM_VIEWPORT_BACK        0x010
#define DM_VIEWPORT_LEFT        0x020
#define DM_VIEWPORT_PERSPECTIVE 0xf00
#define DM_VIEWPORT_ALL_3D      (DM_VIEWPORT_PERSPECTIVE|DM_VIEWPORT_ORTHO)
#define DM_VIEWPORT_UV          0x1000
#define DM_VIEWPORT_ALL         (DM_VIEWPORT_ALL_3D | DM_VIEWPORT_UV)
*/

HoudiniBrush::HoudiniBrush(JEDI_View &a_rView,PI_StateTemplate &a_rTemplate,
	BM_SceneManager *a_pScene,const char *a_pCursor):
	MSS_SingleOpState(a_rView,a_rTemplate,a_pScene,a_pCursor),
	m_started(FALSE),
	m_doubleClick(FALSE),
	m_idleQueued(FALSE),
	m_exposed(FALSE),
	m_ortho(FALSE),
	m_isBrushVisible(false),
	m_nearPlane(1.0),
	m_farPlane(1e3)
{
#if FE_HBR_EVENT_DEBUG
	feLog("HoudiniBrush::HoudiniBrush %p\n",this);
#endif

	//* only use this state in specific viewports
	//* DM_VIEWPORT_ALL_3D = DM_VIEWPORT_PERSPECTIVE | DM_VIEWPORT_ORTHO
	//* TODO DM_VIEWPORT_UV
	setViewportMask(DM_VIEWPORT_ALL_3D);

	set(m_viewport);
	setIdentity(m_projection);

	m_pOperatorContext=new HoudiniContext();
	sp<Scope> spScope=m_pOperatorContext->scope();
	initEvent(spScope);
}

HoudiniBrush::~HoudiniBrush(void)
{
#if FE_HBR_EVENT_DEBUG
	feLog("HoudiniBrush::~HoudiniBrush %p\n",this);
#endif
}

const char* HoudiniBrush::className(void) const
{
	return "HoudiniBrush";
}

void HoudiniBrush::brush(void)
{
	HoudiniSOP* pHoudiniSOP=dynamic_cast<HoudiniSOP*>(getNode());

#if FE_HBR_EVENT_DEBUG
	feLog("HoudiniBrush::brush %p SOP %p\n",this,pHoudiniSOP);
#endif

	if(!pHoudiniSOP)
	{
		return;
	}

	//* accessing inputs apparently not safe on bypassed nodes
	if(pHoudiniSOP->getBypass())
	{
#if FE_HBR_EVENT_DEBUG
	feLog("HoudiniBrush::brush bypassed\n");
#endif
		return;
	}

	//* TODO should only need to do once
	initDrawInterfaces(pHoudiniSOP);

	const I32 centerX=width(m_viewport)/2;
	const I32 centerY=height(m_viewport)/2;
	if(centerX<1 || centerY<1)
	{
#if FE_HBR_EVENT_DEBUG
		feLog("HoudiniBrush::brush no viewport (%d %d)\n",
				width(m_viewport),height(m_viewport));
#endif
		return;
	}

	UT_Matrix4 brushMatrix;
	getViewportTransform(brushMatrix);
	const float* floats=brushMatrix.data();

	SpatialTransform cameraMatrix;
	set(cameraMatrix.column(0),floats[0],floats[1],floats[2]);
	set(cameraMatrix.column(1),floats[4],floats[5],floats[6]);
	set(cameraMatrix.column(2),floats[8],floats[9],floats[10]);
	set(cameraMatrix.column(3),floats[12],floats[13],floats[14]);

#if FE_HBR_CAMERA_DEBUG
	feLog("camera\n%s\n",c_print(cameraMatrix));
#endif

	I32 mx;
	I32 my;
	m_event.getMousePosition(&mx,&my);

	UT_Vector3 rayorig;
	UT_Vector3 dir;
	mapToWorld(mx,my,dir,rayorig);
	SpatialVector rayOrigin(rayorig[0],rayorig[1],rayorig[2]);
	SpatialVector rayDirection(dir[0],dir[1],dir[2]);

	mapToWorld(centerX,centerY,dir,rayorig);
	SpatialVector centerOrigin(rayorig[0],rayorig[1],rayorig[2]);
	SpatialVector centerDirection(dir[0],dir[1],dir[2]);

/*
	mapToWorld(0,centerY,dir,rayorig);
	SpatialVector leftOrigin(rayorig[0],rayorig[1],rayorig[2]);
	SpatialVector leftDirection(dir[0],dir[1],dir[2]);
	const Real fovx=2.0*acos(dot(centerDirection,leftDirection))/degToRad;
*/

	mapToWorld(centerX,0,dir,rayorig);
	SpatialVector bottomOrigin(rayorig[0],rayorig[1],rayorig[2]);
	SpatialVector bottomDirection(dir[0],dir[1],dir[2]);
	const Real fovy=2.0*acos(dot(centerDirection,bottomDirection))/degToRad;

	//* NOTE fovy is near zero when ortho and exactly zero when uv
//	FEASSERT(fovy>1.0);
	FEASSERT(fovy<180.0);

#if FE_HOUDINI_HARDENING	//* 12.5+
	fpreal focal_length;
	fpreal horizontal_aperture;
	bool is_ortho;
	fpreal ortho_width;
	fpreal aspect_ratio;
	fpreal near_plane;
	fpreal far_plane;
//	bool result=
			getViewportProjection(focal_length,horizontal_aperture,
			is_ortho,ortho_width,aspect_ratio,near_plane,far_plane);

#if FE_HBR_CAMERA_DEBUG
	feLog("projection focal %.6G aperture %.6G ortho %d width %.6G"
			" aspect %.6G near %.6G far %.6G\n",
			focal_length,horizontal_aperture,is_ortho,ortho_width,
			aspect_ratio,near_plane,far_plane);
#endif

	m_ortho=is_ortho;
	m_nearPlane=near_plane;
	m_farPlane=far_plane;

#else

	m_ortho=(fovy<1.0);	//* NOTE not a smart test

/*
	a=(n+f)/(n-f);
	b=2*n*f/(n-f);

	an-af=n+f
	bn-bf=2nf

	n(a-1)=f(a+1)
	f(2n+b)=bn

	n(a-1)=(a+1)(bn)/(2n+b)

	(na-n)*(2n+b)=abn+bn

	2ann-2nn+abn-bn=abn+bn

	an-n=b

	n=b/(a-1)
*/

	const F64 a=m_projection[10];
	const F64 b=m_projection[14];

	FEASSERT(fabs(a-1.0)>1e-3);
	F64 n=b/(a-1.0);

	FEASSERT(fabs(2.0*n+b)>1e-15);
	F64 f=b*n/(2.0*n+b);

	m_nearPlane=n;
	m_farPlane=f;

#if FALSE
	feLog("\nnear %.6G far %.6G\n",m_nearPlane,m_farPlane);
	for(U32 m=0;m<16;m++)
	{
		feLog("%12.8f%s",utMatrix4.data()[m],(m%4)==3? "\n": " ");
	}
#endif

#endif

	sp<CameraI> spCameraPerspective=m_spDrawPerspective->view()->camera();
	sp<CameraI> spCameraOrtho=m_spDrawOrtho->view()->camera();

	m_spDrawPerspective->view()->setViewport(m_viewport);
	m_spDrawOrtho->view()->setViewport(m_viewport);
	spCameraPerspective->setCameraMatrix(cameraMatrix);
	spCameraOrtho->setCameraMatrix(cameraMatrix);

	if(m_ortho)
	{
		/*	ortho projection
			m0 = 2/(r-l)
			m12 = -(r+l)/(r-l)

			r = 2/m0+l

			2/m0 = -(r+l)/m12
			r+l = -2*m12/m0

			2/m0 + 2*l = -2*m12/m0
			l = -1/m0 -m12/m0 = -(1+m12)/m0

			same for t as r, b as l, m5 as m0, m13 as m12 */

		const Real m0=m_projection[0];
		const Real m12=m_projection[12];
		const Real left= -(1.0+m12)/m0;
		const Real right= 2.0/m0+left;

		const Real m5=m_projection[5];
		const Real m13=m_projection[13];
		const Real bottom= -(1.0+m13)/m5;
		const Real top= 2.0/m5+bottom;

		//* TODO
		const Real zoom=1.0;
		const Vector2 center(0.5*(left+right),0.5*(bottom+top));
		spCameraPerspective->setOrtho(zoom,center);
		spCameraOrtho->setOrtho(zoom,center);

#if FE_HBR_CAMERA_DEBUG
		feLog("ortho L %.6G R %.6G B %.6G T %.6G\n",
				left,right,bottom,top);
		const Matrix<4,4,Real> ortho_proj=
				ortho(left,right,bottom,top,m_nearPlane,m_farPlane);
		feLog("%s\n",c_print(ortho_proj));
#endif
	}
	else
	{
		spCameraPerspective->setFov(Vector2(0.0,fovy));
		spCameraPerspective->setPlanes(m_nearPlane,m_farPlane);

		spCameraOrtho->setFov(Vector2(0.0,fovy));
		spCameraOrtho->setPlanes(m_nearPlane,m_farPlane);

#if FE_HBR_CAMERA_DEBUG
		const Real aspect=width(m_viewport)/Real(height(m_viewport));
		feLog("fovy %.6G aspect %.6G\n",fovy,aspect);
		const Matrix<4,4,Real> persp_proj=
				perspective(fovy,aspect,m_nearPlane,m_farPlane);
		feLog("perspective\n%s\n",c_print(persp_proj));
#endif
	}

#if FE_HBR_CAMERA_DEBUG
	feLog("viewport %s\n",c_print(m_viewport));
	feLog("projection\n%s\n",c_print(m_projection));
#endif

	pHoudiniSOP->brush(&m_gdp,m_event,rayOrigin,rayDirection);

	//* if not fully recooking, then redraw
	if(pHoudiniSOP->brushCook().empty())
	{
#if FE_HBR_EVENT_DEBUG
		feLog("HoudiniBrush::brush redrawScene\n");
#endif
		redrawScene();
	}

	//* nullify to prevent repeats, but keep last known mouse position
	m_event.setSIS(WindowEvent::e_sourceNull,
			WindowEvent::e_itemNull,WindowEvent::e_stateNull);
}

void HoudiniBrush::eventStart(void)
{
	//* ask for handleMouseEvent to be called when the mouse moves or a
	//* mouse button is clicked
	wantsLocates(1);
	addClickInterest(MSS_CLICK_BUTTONS);
}

void HoudiniBrush::eventStop(void)
{
	wantsLocates(0);
	removeClickInterest(MSS_CLICK_BUTTONS);
}

void HoudiniBrush::drawStart(void)
{
#if FE_HBR_EVENT_DEBUG
	feLog("HoudiniBrush::drawStart\n");
#endif

	//* NOTE maybe m_started and m_isBrushVisible are always the same

	m_started=TRUE;
	m_isBrushVisible=true;
	redrawScene();
	brush();
	m_exposed=TRUE;
}

void HoudiniBrush::drawStop(void)
{
#if FE_HBR_EVENT_DEBUG
	feLog("HoudiniBrush::drawStop\n");
#endif

	m_started=FALSE;
	m_isBrushVisible=false;
	redrawScene();
}

int HoudiniBrush::enter(BM_SimpleState::BM_EntryType a_how)
{
#if FE_HBR_EVENT_DEBUG
	feLog("\nHoudiniBrush::enter\n");
#endif

	int result=MSS_SingleOpState::enter(a_how);

	eventStart();
	updatePrompt();

	//* turn off the highlight so we can see the color we are painting
//	OP_Node *op=getNode();
//	if(op)
//	{
//		op->setHighlight(0);
//	}

	HoudiniSOP* pHoudiniSOP=dynamic_cast<HoudiniSOP*>(getNode());

	if(pHoudiniSOP && pHoudiniSOP->brushVisible()=="any")
	{
		drawStart();
	}

	return result;
}

void HoudiniBrush::exit(void)
{
	//* can happen during shutdown
#if FE_HBR_EVENT_DEBUG
	feLog("\nHoudiniBrush::exit\n");
#endif

	eventStop();
	drawStop();

	MSS_SingleOpState::exit();
}

void HoudiniBrush::interrupt(BM_SimpleState *a_pState)
{
	HoudiniSOP* pHoudiniSOP=dynamic_cast<HoudiniSOP*>(getNode());

#if FE_HBR_EVENT_DEBUG
	feLog("\nHoudiniBrush::interrupt %p %p\n",this,a_pState);
	if(pHoudiniSOP)
	{
		feLog("  visible \"%s\"\n",pHoudiniSOP->brushVisible().c_str());
	}
#endif

	if(!pHoudiniSOP || pHoudiniSOP->brushVisible()!="any")
	{
		eventStop();
		drawStop();

		MSS_SingleOpState::interrupt(a_pState);
	}

	return;
}

void HoudiniBrush::resume(BM_SimpleState *a_pState)
{
	HoudiniSOP* pHoudiniSOP=dynamic_cast<HoudiniSOP*>(getNode());

#if FE_HBR_EVENT_DEBUG
	feLog("\nHoudiniBrush::resume %p %p\n",this,a_pState);
#endif

	if(!m_started || !pHoudiniSOP || pHoudiniSOP->brushVisible()!="any")
	{
		MSS_SingleOpState::resume(a_pState);

		eventStart();
		drawStart();
	}

	updatePrompt();

	//* turn off the highlight so we can see the color we are painting
//	OP_Node *op=getNode();
//	if(op)
//	{
//		op->setHighlight(0);
//	}

	return;
}

void HoudiniBrush::printEvent(const UI_Event* a_pEvent)
{
	String stateString;

#if FE_HOUDINI_14_PLUS
	const UI_EventType stateType=a_pEvent->state.type;
#else
	const int stateType=a_pEvent->state.myType;
#endif
	switch(stateType)
	{
		case UI_EVENT_NO_EVENT:
			stateString="no event";
			break;
		case UI_EVENT_KEYBOARD:
			stateString="key";
			break;
		case UI_EVENT_FUNCTION_KEY:
			stateString="function-key";
			break;
		case UI_EVENT_ARROW_KEY:
			stateString="arrow-key";
			break;
		case UI_EVENT_MOUSEWHEEL:
			stateString="wheel";
			break;
		case UI_EVENT_DBLCLK:
			stateString="double-click";
			break;
		case UI_EVENT_BUTTON:
			stateString="button";
			break;
		case UI_EVENT_MOTION:
			stateString="motion";
			break;
		default:
			stateString.sPrintf("other %d",stateType);
			break;
	}

#if FE_HOUDINI_14_PLUS
	feLog("HoudiniBrush::printEvent"
			" data %d flags 0x%x id %d value %d %d %d %d type 0x%x %s\n",
			a_pEvent->state.data,a_pEvent->state.altFlags,
			a_pEvent->state.id,
			a_pEvent->state.values[0],a_pEvent->state.values[1],
			a_pEvent->state.values[2],a_pEvent->state.values[3],
			a_pEvent->state.type,
			stateString.c_str());
#endif
}

int HoudiniBrush::handleMouseEvent(UI_Event* a_pEvent)
{
#if FE_HBR_EVENT_DEBUG
	feLog("\nHoudiniBrush::handleMouseEvent\n");
#endif

	return handleAnyEvent(a_pEvent);
}

int HoudiniBrush::handleMouseWheelEvent(UI_Event* a_pEvent)
{
#if FE_HBR_EVENT_DEBUG
	feLog("\nHoudiniBrush::handleMouseWheelEvent\n");
#endif

	return handleAnyEvent(a_pEvent);
}

bool HoudiniBrush::handleDoubleClickEvent(UI_Event* a_pEvent)
{
#if FE_HBR_EVENT_DEBUG
	feLog("\nHoudiniBrush::handleDoubleClickEvent\n");
#endif

	return handleAnyEvent(a_pEvent);
}

//int HoudiniBrush::handleArrowEvent(UI_Event* a_pEvent)
//{
//#if FE_HBR_EVENT_DEBUG
//	feLog("\nHoudiniBrush::handleArrowEvent\n");
//#endif
//
//	return handleAnyEvent(a_pEvent);
//}

void HoudiniBrush::handleGeoChangedEvent(UI_Event* a_pEvent)
{
#if FE_HBR_EVENT_DEBUG
	feLog("\nHoudiniBrush::handleGeoChangedEvent\n");
#endif

	handleAnyEvent(a_pEvent);
}

int HoudiniBrush::handleKeyTypeEvent(int a_key,
		UI_Event* a_pEvent,BM_Viewport& a_rViewport)
{
#if FE_HBR_EVENT_DEBUG
	feLog("\nHoudiniBrush::handleKeyTypeEvent\n");
#endif

	if(a_pEvent->state.altFlags&UI_CTRL_KEY)
	{
		const I32 key=a_pEvent->state.data;

		//* undo/redo
		if(key=='z' || key=='Z' || key=='y')
		{
			return MSS_SingleOpState::handleKeyTypeEvent(
					a_key,a_pEvent,a_rViewport);
		}
	}

	return handleAnyEvent(a_pEvent);
}

bool HoudiniBrush::handleTransitoryKey(const UI_Event& a_rEvent,int a_hotkey_id)
{
#if FE_HBR_EVENT_DEBUG
	feLog("\nHoudiniBrush::handleTransitoryKey\n");
#endif
	return MSS_SingleOpState::handleTransitoryKey(a_rEvent,a_hotkey_id);

	return handleAnyEvent(&a_rEvent);
}

void HoudiniBrush::handleEvent(UI_Event* a_pEvent)
{
#if FE_HBR_EVENT_DEBUG
	feLog("\nHoudiniBrush::handleEvent\n");
#endif

	handleAnyEvent(a_pEvent);
}

int HoudiniBrush::handleAnyEvent(const UI_Event* a_pEvent)
{
	SOP_Node *sop=(SOP_Node *)getNode();
	if(!sop)
	{
#if FE_HBR_EVENT_DEBUG
		feLog("HoudiniBrush::handleAnyEvent not a SOP_Node\n");
#endif
		//* consumed but useless
		return 1;
	}

//	const float t=getTime();
	const int x=a_pEvent->state.values[X];
	const int y=a_pEvent->state.values[Y];

#if FE_HBR_EVENT_DEBUG
	printEvent(a_pEvent);
#endif

	WindowEvent::Item modifiers=WindowEvent::e_itemNull;
	if(a_pEvent->state.altFlags&UI_SHIFT_KEY)
	{
#if FE_HBR_EVENT_DEBUG
		feLog(" state SHIFT\n");
#endif
		modifiers=WindowEvent::Item(modifiers|WindowEvent::e_keyShift);
	}
	if(a_pEvent->state.altFlags&UI_CTRL_KEY)
	{
#if FE_HBR_EVENT_DEBUG
		feLog(" state CTRL\n");
#endif
		modifiers=WindowEvent::Item(modifiers|WindowEvent::e_keyControl);
	}
#if FE_HBR_EVENT_DEBUG
	if(a_pEvent->state.altFlags&UI_ALT_KEY)
	{
		feLog(" state ALT\n");
	}
	if(a_pEvent->state.altFlags&UI_COMMAND_KEY)
	{
		feLog(" state COMMAND\n");
	}
#endif

	BWORD send=FALSE;
	BWORD validPosition=TRUE;

#if FE_HOUDINI_14_PLUS
	const UI_EventType stateType=a_pEvent->state.type;
#else
	const int stateType=a_pEvent->state.myType;
#endif

	if(stateType==UI_EVENT_NO_EVENT)
	{
		//* not supposed to have key modifiers on system events
//		modifiers=WindowEvent::Item(m_event.item()&WindowEvent::e_keyModifiers);

		if(m_exposed)
		{
			m_event.setSIS(WindowEvent::e_sourceSystem,
					WindowEvent::Item(WindowEvent::e_itemExpose|modifiers),
					WindowEvent::e_stateNull);
			m_event.setState2(WindowEvent::e_stateNull);
		}
		else
		{
			m_event.setSIS(WindowEvent::e_sourceSystem,
					WindowEvent::Item(WindowEvent::e_itemIdle|modifiers),
					WindowEvent::e_stateNull);
		}

		send=TRUE;
		m_idleQueued=FALSE;
		m_exposed=FALSE;
		validPosition=FALSE;
	}

	if(stateType==UI_EVENT_KEYBOARD)
	{
		m_event.setSIS(WindowEvent::e_sourceKeyboard,
				WindowEvent::Item(a_pEvent->state.data|modifiers),
				WindowEvent::e_statePress);
		send=TRUE;
	}

	if(stateType==UI_EVENT_FUNCTION_KEY)
	{
		m_event.setSIS(WindowEvent::e_sourceKeyboard,
				WindowEvent::Item(
				(WindowEvent::e_keyFunction+a_pEvent->state.data-68)|modifiers),
				WindowEvent::e_statePress);
		send=TRUE;
	}

	if(stateType==UI_EVENT_ARROW_KEY)
	{
		m_event.setSIS(WindowEvent::e_sourceKeyboard,
				WindowEvent::Item(
				(WindowEvent::e_keyCursor+a_pEvent->state.data)|modifiers),
				WindowEvent::e_statePress);
		send=TRUE;
	}

	if(stateType==UI_EVENT_MOUSEWHEEL)
	{
#if FE_HOUDINI_13_PLUS
		const U32 wheelValueIndex=6;
#else
		const U32 wheelValueIndex=3;
#endif
		m_event.setSIS(WindowEvent::e_sourceMouseButton,
				WindowEvent::Item(WindowEvent::e_itemWheel|modifiers),
				(WindowEvent::State)a_pEvent->state.values[wheelValueIndex]);
		send=TRUE;
	}

	if(stateType==UI_EVENT_BUTTON)
	{
		const WindowEvent::State state2=m_doubleClick?
				WindowEvent::e_state2ClickDouble:
				WindowEvent::e_state2ClickSingle;
		send=TRUE;

		switch(a_pEvent->state.values[W])
		{
			case 0:
				if(m_event.mouseButtons()==WindowEvent::e_mbLeft)
				{
					m_event.setSIS(WindowEvent::e_sourceMouseButton,
							WindowEvent::Item(
							WindowEvent::e_itemLeft|modifiers),
							WindowEvent::e_stateRelease);
				}
				else if(m_event.mouseButtons()==WindowEvent::e_mbMiddle)
				{
					m_event.setSIS(WindowEvent::e_sourceMouseButton,
							WindowEvent::Item(
							WindowEvent::e_itemMiddle|modifiers),
							WindowEvent::e_stateRelease);
				}
				else
				{
					send=FALSE;
				}
				m_event.setMouseButtons(WindowEvent::e_mbNone);
				m_doubleClick=FALSE;
				break;
			case DM_PRIMARY_BUTTON:
				m_event.setSIS(WindowEvent::e_sourceMouseButton,
						WindowEvent::Item(WindowEvent::e_itemLeft|modifiers),
						WindowEvent::e_statePress);
				m_event.setMouseButtons(WindowEvent::e_mbLeft);
				break;
			case DM_SECONDARY_BUTTON:
				m_event.setSIS(WindowEvent::e_sourceMouseButton,
						WindowEvent::Item(WindowEvent::e_itemMiddle|modifiers),
						WindowEvent::e_statePress);
				m_event.setMouseButtons(WindowEvent::e_mbMiddle);
				break;
		}
		m_event.setState2(state2);
	}
	else
	{
		m_doubleClick=FALSE;
		m_event.setState2(WindowEvent::e_stateNull);
	}

	if(stateType==UI_EVENT_DBLCLK)
	{
		m_doubleClick=TRUE;
	}

	if(!m_idleQueued)
	{
		HoudiniSOP* pHoudiniSOP=dynamic_cast<HoudiniSOP*>(getNode());
		if(pHoudiniSOP)
		{
			String idle=pHoudiniSOP->idleEvents();
			if(idle=="any" || (m_isBrushVisible && idle=="viewer"))
			{
				generateNoEvent();
			}
		}
		m_idleQueued=TRUE;
	}

	if(validPosition)
	{
		m_event.setMousePosition(x,y);
	}

	if(send)
	{
		brush();
		return 1;
	}

#if FE_HBR_EVENT_DEBUG
	switch(a_pEvent->type)
	{
		case UI_EVENT_VALUE_CHANGE:
			feLog(" type value_change\n");
			break;
		default:
			feLog(" type other %d\n",a_pEvent->type);
			break;
	}
	switch(a_pEvent->reason)
	{
		case UI_VALUE_START:
			feLog(" reason start\n");
			break;
		case UI_VALUE_CHANGED:
			feLog(" reason changed\n");
			break;
		case UI_VALUE_LOCATED:
			feLog(" reason located\n");
			break;
		case UI_VALUE_PICKED:
			feLog(" reason picked\n");
			break;
		case UI_VALUE_ACTIVE:
			feLog(" reason active\n");
			break;
		default:
			feLog(" reason other %d\n",a_pEvent->reason);
			break;
	}
#endif

#if FALSE
	I32 oldX;
	I32 oldY;
	m_event.getMousePosition(&oldX,&oldY);
	if(x==oldX && y==oldY)
	{
		return 1;
	}
#endif

	if(a_pEvent->reason==UI_VALUE_LOCATED)
	{
		m_event.setSIS(WindowEvent::e_sourceMousePosition,
				WindowEvent::Item(WindowEvent::e_itemMove|modifiers),
				WindowEvent::e_stateRepeat);

		//* NOTE we may not get a mouse release
		m_event.setMouseButtons(WindowEvent::e_mbNone);
	}
	else
	{
		//* trigger a cook of the CustomBrush SOP so it can cook with
		//* the current stroke values
//		sop->getCookedGeo(context);

		m_event.setSIS(WindowEvent::e_sourceMousePosition,
				WindowEvent::Item(WindowEvent::e_itemDrag|modifiers),
				WindowEvent::e_stateRepeat);

	}

	updateBrush(x,y);
	brush();

	return 1;
}

void HoudiniBrush::generateNoEvent(void)
{
	const UI_EventType eventType=UI_EVENT_NO_EVENT;
	UI_Object* target=this;

	generateEvent(eventType,target);
}

//* NOTE called twice per refresh: ghost 1, ghost 0 (then geometry render)
void HoudiniBrush::doRender(RE_Render *a_pRender,int a_x,int a_y,
	int a_ghost)
{
#if FE_HBR_RENDER_DEBUG
	feLog("HoudiniBrush::doRender xy %d %d ghost %d preempt %d visible %d\n",
			a_x,a_y,a_ghost,isPreempted(),m_isBrushVisible);
#endif

	UT_Matrix4 utMatrix4;
#if FE_HOUDINI_14_PLUS
	a_pRender->getMatrix(utMatrix4,RE_MATRIX_PROJECTION);
#else
	a_pRender->getAMatrix(RE_MATRIX_PROJECTION,utMatrix4);
#endif

	FontHoudini::setRender(a_pRender);

	set(m_projection,utMatrix4.data());

	UT_DimRect rect=a_pRender->getViewport2DI();
	set(m_viewport,rect.x(),rect.y(),rect.w(),rect.h());

#if FE_HBR_RENDER_DEBUG
	feLog("  set viewport %s\n",c_print(m_viewport));
#endif

	if(a_ghost)
	{
		return;
	}

	if(isPreempted() || !m_isBrushVisible)
	{
		HoudiniSOP* pHoudiniSOP=dynamic_cast<HoudiniSOP*>(getNode());
		if(!pHoudiniSOP)
		{
#if FE_HBR_RENDER_DEBUG
			feLog("  !pHoudiniSOP\n");
#endif
			return;
		}

		const String brushVisible=pHoudiniSOP->brushVisible();
		if(brushVisible!="any")
		{
#if FE_HBR_RENDER_DEBUG
			feLog("  brushVisible!=\"any\"\n");
#endif
			return;
		}

#if FE_HBR_RENDER_DEBUG
		feLog("  brush()\n");
#endif
		brush();
		return;
	}

	if(m_exposed)
	{
#if FE_HBR_RENDER_DEBUG
			feLog("  exposed\n");
#endif
		generateNoEvent();
		return;
	}

//	GR_Detail rgdp;
//	rgdp.wireDraw(&m_gdp,*a_pRender,getViewportLOD(),1);

//	a_pRender->lockContextForRender();

#if FE_HOUDINI_HYDRA_BRUSH
	if(m_spDrawOpenGL.isNull() && m_spMaster.isValid())
	{
		m_spDrawOpenGL=m_spMaster->registry()->create("DrawI.DrawHydra");

		if(m_spDrawOpenGL.isValid())
		{
			m_spDrawOpenGL->setName("HoudiniBrush DrawHydra");
		}

		//* only try to create once
		m_spMaster=NULL;
	}
#endif

#if FE_HBR_RENDER_DEBUG
	feLog("  draw()\n");
#endif

	draw();

	FontHoudini::flush();

//	a_pRender->unlockContextAfterRender();

//#if FE_HOUDINI_16_5_PLUS
//	a_pRender->resync();
//#elif FE_HOUDINI_16_PLUS
//	a_pRender->getBoundUniformBlocks().zero();
//#endif
#if FE_HOUDINI_HYDRA_BRUSH
//	feLog("RESYNC\n");
//	a_pRender->getBoundUniformBlocks().zero();
	a_pRender->resync();
#endif
}

void HoudiniBrush::updatePrompt(void)
{
#if FE_HBR_RENDER_DEBUG
	feLog("HoudiniBrush::updatePrompt\n");
#endif

	HoudiniSOP* pHoudiniSOP=dynamic_cast<HoudiniSOP*>(getNode());

#if FE_HBR_RENDER_DEBUG
	feLog("HoudiniBrush::updatePrompt pHoudiniSOP %p\n",pHoudiniSOP);
#endif

	if(pHoudiniSOP)
	{

		const String prompt=pHoudiniSOP->prompt();

#if FE_HBR_RENDER_DEBUG
		feLog("HoudiniBrush::updatePrompt \"%s\"\n",prompt.c_str());
#endif

		showPrompt(prompt.c_str());
	}
}

void HoudiniBrush::updateBrush(int a_x,int a_y)
{
//	feLog("HoudiniBrush::updateBrush\n");

	//* ensure the brush is visible
	m_isBrushVisible=true;
//	redrawScene();
}

} /* namespace ext */
} /* namespace fe */
