/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __houdini_VeilOp_h__
#define __houdini_VeilOp_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Encode native Houdini data into an FE surface primitive

	@ingroup houdini
*//***************************************************************************/
class FE_DL_EXPORT VeilOp:
	public OperatorSurfaceCommon,
	public Initialize<VeilOp>
{
	public:

					VeilOp(void)											{}
virtual				~VeilOp(void)											{}

		void		initialize(void);

					//* As HandlerI
virtual	void		handle(Record& a_rSignal);

	private:
};

} /* namespace ext */
} /* namespace fe */

#endif /* __houdini_VeilOp_h__ */
