/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __houdini_SurfaceAccessorHoudini_h__
#define __houdini_SurfaceAccessorHoudini_h__

#define	FE_SAH_ASSERT_BOUNDS	TRUE
#define	FE_SAH_MULTIGROUP		TRUE

namespace fe
{
namespace ext
{

//* TODO remove Houdini-only CompoundGroup in favor of general MultiGroup

//* TODO rework select() methodology to be reentrant

//* TODO remove old style of vertex uvs using e_primitive

//* look at VM/VM_Math for simd

//* thread-safe only on isolated pages
//* different threads can't write to the same page

//* GA_AttributeRefMap could replace GEO_AttributeHandleList

/**************************************************************************//**
    @brief Houdini Surface Editing Implementation

	Note that attempts to access non-existant attributes isn't a failure.
	New attributes may be added when first written to by a set() method.

	@ingroup houdini
*//***************************************************************************/
class FE_DL_EXPORT SurfaceAccessorHoudini:
	public SurfaceAccessorBase,
	public CastableAs<SurfaceAccessorHoudini>
{
	public:
						SurfaceAccessorHoudini(void):
							m_pGdp(NULL),
							m_pGdpChangeable(NULL),
							m_pPrimitive(NULL),
							m_tupleSize(0)
//~							,m_pGroup(NULL)
						{
#ifdef FE_HOUDINI_USE_GA
//~							m_pIterator=NULL;
#else
							m_pPoint=NULL;
							m_pPointChangeable=NULL;
#endif
							setName("SurfaceAccessorHoudini");
						}

virtual					~SurfaceAccessorHoudini(void)
						{
//~#ifdef FE_HOUDINI_USE_GA
//~							delete m_pIterator;
//~#endif
#if FE_HOUDINI_HARDENING
							if(m_spHardening.isValid())
							{
								FEASSERT(m_spSurfaceAccessibleI.isValid());
								m_spSurfaceAccessibleI->lock(I64(this));
								m_spHardening=NULL;
								m_spSurfaceAccessibleI->unlock(I64(this));
							}
#endif
						}

						using SurfaceAccessorBase::set;
						using SurfaceAccessorBase::append;
						using SurfaceAccessorBase::spatialVector;

		BWORD			bind(SurfaceAccessibleI::Element a_element,
								SurfaceAccessibleI::Attribute a_attribute)
						{
							m_attribute=a_attribute;

							String name;
							switch(a_attribute)
							{
								case SurfaceAccessibleI::e_generic:
								case SurfaceAccessibleI::e_position:
									name="P";
									break;
								case SurfaceAccessibleI::e_normal:
									name="N";
									break;
								case SurfaceAccessibleI::e_uv:
									name="uv";
									break;
								case SurfaceAccessibleI::e_color:
									name="Cd";
									break;
								case SurfaceAccessibleI::e_vertices:
									m_attrName="vertices";
									FEASSERT(a_element==
											SurfaceAccessibleI::e_primitive);
									m_element=a_element;
									return TRUE;
								case SurfaceAccessibleI::e_properties:
									m_attrName="properties";
									FEASSERT(a_element==
											SurfaceAccessibleI::e_primitive);
									m_element=a_element;
									return TRUE;
							}
							return bindInternal(a_element,name);
						}
		BWORD			bind(SurfaceAccessibleI::Element a_element,
								const String& a_name)
						{
							m_attribute=SurfaceAccessibleI::e_generic;

							if(a_name=="P")
							{
								m_attribute=SurfaceAccessibleI::e_position;
							}
							else if(a_name=="N")
							{
								m_attribute=SurfaceAccessibleI::e_normal;
							}
							else if(a_name=="uv")
							{
								m_attribute=SurfaceAccessibleI::e_uv;
							}
							else if(a_name=="Cd")
							{
								m_attribute=SurfaceAccessibleI::e_color;
							}

							return bindInternal(a_element,a_name);
						}

						//* as SurfaceAccessorI
virtual	String			type(void) const;

virtual	U32				count(void) const
						{
							FEASSERT(m_pGdp);

#if FALSE
							if(m_attribute!=SurfaceAccessibleI::e_generic &&
									m_attribute!=
									SurfaceAccessibleI::e_vertices &&
									m_attribute!=
									SurfaceAccessibleI::e_properties &&
									!isBound())
							{
								return 0;
							}
#else
							if(m_attribute==SurfaceAccessibleI::e_position &&
									!isBound())
							{
								return 0;
							}
#endif

							switch(m_element)
							{
							case SurfaceAccessibleI::e_point:
#ifdef FE_HOUDINI_USE_GA
								return m_pGdp->getNumPoints();
#else
								return m_pGdp->points().entries();
#endif

							case SurfaceAccessibleI::e_vertex:
							case SurfaceAccessibleI::e_primitive:
#ifdef FE_HOUDINI_USE_GA
								return m_pGdp->getNumPrimitives();
#else
								return m_pGdp->primitives().entries();
#endif

							case SurfaceAccessibleI::e_detail:
								return 1;

							case SurfaceAccessibleI::e_pointGroup:
							case SurfaceAccessibleI::e_primitiveGroup:
//~								return m_pGroup? m_pGroup->entries(): 0;
#if FE_SAH_MULTIGROUP
								return m_spMultiGroup->count();
#else
								return m_spCompoundGroup.isValid()?
										m_spCompoundGroup->count(): 0;
#endif

							default:
								;
							}
							return 0;
						}
virtual	U32				subCount(U32 a_index) const
						{
							if(m_attribute!=SurfaceAccessibleI::e_generic &&
									m_attribute!=
									SurfaceAccessibleI::e_vertices &&
									m_attribute!=
									SurfaceAccessibleI::e_uv &&
									m_attribute!=
									SurfaceAccessibleI::e_color &&
									m_attribute!=
									SurfaceAccessibleI::e_properties &&
									!isBound())
							{
								return 0;
							}

							//* TODO do we really want direct access
							//* as e_primitiveGroup?

							const BWORD isPrimitiveUV=
									((m_element==SurfaceAccessibleI::e_primitive
									||m_element==SurfaceAccessibleI::e_vertex
									|| m_element==
									SurfaceAccessibleI::e_primitiveGroup) &&
									m_attribute==SurfaceAccessibleI::e_uv);

							if(m_element==
									SurfaceAccessibleI::e_primitiveGroup ||
									m_element==
									SurfaceAccessibleI::e_vertex ||
									m_attribute==SurfaceAccessibleI::e_vertices
									|| isPrimitiveUV)
							{
								FEASSERT(m_element==
										SurfaceAccessibleI::e_primitiveGroup
										|| m_element==
										SurfaceAccessibleI::e_vertex
										|| m_element==
										SurfaceAccessibleI::e_primitive);

								const_cast<SurfaceAccessorHoudini*>(this)
										->select(a_index,0);

								if(m_element==
										SurfaceAccessibleI::e_primitiveGroup &&
										!m_pPrimitive)
								{
									return 1;
								}

								FEASSERT(m_pPrimitive);
								return m_pPrimitive->getVertexCount();
							}

							return 1;

							// TODO check what really needs sub-indexes
//							return m_handle.entries();
						}


virtual	void			set(U32 a_index,U32 a_subIndex,String a_string)
						{
#if	FE_SAH_ASSERT_BOUNDS
							FEASSERT(a_index<count());
							FEASSERT(a_subIndex<subCount(a_index));
#endif
							if(m_attribute!=SurfaceAccessibleI::e_vertices &&
									m_attribute!=
									SurfaceAccessibleI::e_properties &&
									!isBound())
							{
								addString();
							}
							select(a_index,a_subIndex);

							UT_String utString(a_string.c_str());
							m_handle.setString(utString);
						}
virtual	String			string(U32 a_index,U32 a_subIndex=0)
						{
#if	FE_SAH_ASSERT_BOUNDS
							FEASSERT(a_index<count());
							FEASSERT(a_subIndex<subCount(a_index));
#endif
							select(a_index,a_subIndex);

							if(m_attribute==SurfaceAccessibleI::e_vertices ||
									m_attribute==
									SurfaceAccessibleI::e_properties)
							{
								return String();
							}

							UT_String utString;
							m_handle.getString(utString,a_subIndex);
							return String(utString.buffer());
						}

virtual	void			set(U32 a_index,U32 a_subIndex,I32 a_integer)
						{
#if	FE_SAH_ASSERT_BOUNDS
							FEASSERT(a_index<count());
							FEASSERT(a_subIndex<subCount(a_index));
#endif

#ifdef FE_HOUDINI_USE_GA
							if(m_element==SurfaceAccessibleI::e_point &&
									m_handleIntRW.isValid())
							{
#if FE_HOUDINI_12_5_PLUS
								m_handleIntRW.set(m_pGdp->pointOffset(a_index),
										a_subIndex,a_integer);
								return;
								return;
#else
								if(!a_subIndex)
								{
									m_handleIntRW.set(
											m_pGdp->pointOffset(a_index),
											a_integer);
									return;
								}
#endif
							}
#endif

							if(m_attribute!=SurfaceAccessibleI::e_vertices &&
									m_attribute!=
									SurfaceAccessibleI::e_properties &&
									!isBound())
							{
								addInteger();
							}

							select(a_index,a_subIndex);

							if(m_element==
									SurfaceAccessibleI::e_primitiveGroup ||
									m_attribute==SurfaceAccessibleI::e_vertices)
							{
								FEASSERT(m_element==
										SurfaceAccessibleI::e_primitiveGroup
										|| m_element==
										SurfaceAccessibleI::e_primitive);
								FEASSERT(m_pPrimitive);
#ifdef FE_HOUDINI_USE_GA
								const GA_Offset offset=
										m_pGdp->pointOffset(a_integer);
								const_cast<GEO_Primitive*>(m_pPrimitive)
										->setPointOffset(a_subIndex,offset);
#else
								const GB_Element* pElement=
										m_pGdp->points()(a_integer);
								const_cast<GEO_Primitive*>(m_pPrimitive)
										->getVertex(a_subIndex).setPt(
										const_cast<GB_Element*>(pElement));
#endif
							}

							if(m_attribute==SurfaceAccessibleI::e_properties)
							{
								FEASSERT(m_element==
										SurfaceAccessibleI::e_primitiveGroup
										|| m_element==
										SurfaceAccessibleI::e_primitive);
								FEASSERT(m_pPrimitive);
								if(a_subIndex==SurfaceAccessibleI::e_openCurve)
								{
									GEO_Face* pGeoFace=
											dynamic_cast<GEO_Face*>(
											const_cast<GEO_Primitive*>(
											m_pPrimitive));
									if(!pGeoFace)
									{
										return;
									}

									if(pGeoFace->isClosed())
									{
										if(a_integer)
										{
											pGeoFace->open();
										}
									}
									else if(!a_integer)
									{
										pGeoFace->close();
									}

									return;
								}
								if(a_subIndex==SurfaceAccessibleI::e_countU ||
										a_subIndex==
										SurfaceAccessibleI::e_countV ||
										a_subIndex==
										SurfaceAccessibleI::e_wrappedU ||
										a_subIndex==
										SurfaceAccessibleI::e_wrappedV)
								{
#ifdef FE_HOUDINI_USE_GA
									if(m_pPrimitive->getTypeId().get()!=
											GEO_PRIMMESH)
									{
										return;
									}
#else
									if(m_pPrimitive->getPrimitiveId()!=
											GEOPRIMMESH)
									{
										return;
									}
#endif
									const GEO_PrimMesh* pPrimMesh=
											dynamic_cast<const GEO_PrimMesh*>(
											m_pPrimitive);
									if(pPrimMesh)
									{
										switch(a_subIndex)
										{
											case SurfaceAccessibleI::e_countU:
//												return pPrimMesh->getNumCols();
											case SurfaceAccessibleI::e_countV:
//												return pPrimMesh->getNumRows();
											case SurfaceAccessibleI::e_wrappedU:
//												return pPrimMesh->isWrappedU();
											case SurfaceAccessibleI::e_wrappedV:
//												return pPrimMesh->isWrappedV();
											default:
												;
										}
									}
								}
								return;
							}

							m_handle.setI(a_integer);
						}
virtual	I32				integer(U32 a_index,U32 a_subIndex=0)
						{
#if	FE_SAH_ASSERT_BOUNDS
							FEASSERT(a_index<count());
							FEASSERT(m_attribute==
									SurfaceAccessibleI::e_properties ||
									a_subIndex<subCount(a_index));
#endif

#ifdef FE_HOUDINI_USE_GA
							if(m_element==SurfaceAccessibleI::e_point &&
									m_handleIntRO.isValid())
							{
#if FE_HOUDINI_12_5_PLUS
								return m_handleIntRO.get(
										m_pGdp->pointOffset(a_index),
										a_subIndex);
#else
								if(!a_subIndex)
								{
									return m_handleIntRO.get(
											m_pGdp->pointOffset(a_index));
								}
#endif
							}
#endif

							select(a_index,a_subIndex);

							if(m_element==SurfaceAccessibleI::e_pointGroup ||
									m_element==
									SurfaceAccessibleI::e_primitiveGroup)
							{
#if FE_SAH_MULTIGROUP
								if(m_spMultiGroup.isNull())
								{
									return -1;
								}
								return m_spMultiGroup->at(a_index);
#else
								if(m_spCompoundGroup.isNull())
								{
									return -1;
								}
								return m_spCompoundGroup->getIndex();
#endif
							}

//~							if(m_element==SurfaceAccessibleI::e_pointGroup)
//~							{
//~#ifdef FE_HOUDINI_USE_GA
//~								if(!m_pIterator || !m_pIterator->isValid())
//~								{
//~									return -1;
//~								}
//~								return m_pGdp->pointIndex(
//~										m_pIterator->getOffset());
//~#else
//~								FEASSERT(m_pPoint);
//~								return m_pPoint->getNum();
//~#endif
//~							}
//~
//~							if(m_element==
//~									SurfaceAccessibleI::e_primitiveGroup)
//~							{
//~								FEASSERT(m_pPrimitive);
//~#ifdef FE_HOUDINI_USE_GA
//~								const GA_Offset offset=
//~										m_pIterator->getOffset();
//~								return m_pGdp->primitiveIndex(offset);
//~#else
//~								return m_pPrimitive->getNum();
//~#endif
//~							}

							if(m_attribute==SurfaceAccessibleI::e_vertices)
							{
								FEASSERT(m_element==
										SurfaceAccessibleI::e_primitive);
								FEASSERT(m_pPrimitive);
#ifdef FE_HOUDINI_USE_GA
								const GA_Offset offset=m_pPrimitive
										->getPointOffset(a_subIndex);
								return m_pGdp->pointIndex(offset);
#else
								return m_pPrimitive->getVertex(a_subIndex)
										.getBasePt()->getNum();
#endif
								return 1;
							}

							if(m_attribute==SurfaceAccessibleI::e_properties)
							{
								FEASSERT(m_element==
										SurfaceAccessibleI::e_primitiveGroup
										|| m_element==
										SurfaceAccessibleI::e_primitive);
								FEASSERT(m_pPrimitive);
								if(a_subIndex==SurfaceAccessibleI::e_openCurve)
								{
									const GEO_Face* pGeoFace=
											dynamic_cast<const GEO_Face*>(
											m_pPrimitive);

									return pGeoFace && !pGeoFace->isClosed();
								}
								if(a_subIndex==SurfaceAccessibleI::e_countU ||
										a_subIndex==
										SurfaceAccessibleI::e_countV ||
										a_subIndex==
										SurfaceAccessibleI::e_wrappedU ||
										a_subIndex==
										SurfaceAccessibleI::e_wrappedV)
								{
#ifdef FE_HOUDINI_USE_GA
									if(m_pPrimitive->getTypeId().get()!=
											GEO_PRIMMESH)
									{
										return 0;
									}
#else
									if(m_pPrimitive->getPrimitiveId()!=
											GEOPRIMMESH)
									{
										return 0;
									}
#endif
									const GEO_PrimMesh* pPrimMesh=
											dynamic_cast<const GEO_PrimMesh*>(
											m_pPrimitive);
									if(pPrimMesh)
									{
										switch(a_subIndex)
										{
											case SurfaceAccessibleI::e_countU:
												return pPrimMesh->getNumCols();
											case SurfaceAccessibleI::e_countV:
												return pPrimMesh->getNumRows();
											case SurfaceAccessibleI::e_wrappedU:
												return pPrimMesh->isWrappedU();
											case SurfaceAccessibleI::e_wrappedV:
												return pPrimMesh->isWrappedV();
											default:
												;
										}
									}
								}
								return 0;
							}

							return m_handle.getI(a_subIndex);

						}
virtual	I32				duplicate(U32 a_index,U32 a_subIndex=0)
						{
							//* TODO maybe other elements
							if(m_element!=SurfaceAccessibleI::e_point &&
									m_element!=SurfaceAccessibleI::e_primitive)
							{
								return -1;
							}

							FEASSERT(m_pGdpChangeable);
							if(!m_pGdpChangeable)
							{
								return -1;
							}

							I32 dupIndex= -1;
							GEO_Primitive* pDupPrimitive=NULL;

#ifdef FE_HOUDINI_USE_GA
							if(m_element==SurfaceAccessibleI::e_point)
							{
								const GA_Offset offset=
										m_pGdpChangeable->pointOffset(a_index);
								GA_Offset dupOffset=
										m_pGdpChangeable->appendPointCopy(
										offset);
								return m_pGdpChangeable->pointIndex(dupOffset);
							}
							else
							{
								select(a_index,a_subIndex);

								FEASSERT(m_pPrimitive);
								const GA_PrimitiveTypeId primType=
										m_pPrimitive->getTypeId();
#if FE_HOUDINI_14_PLUS
								//* TODO verify this does the same thing
								pDupPrimitive=
										m_pGdpChangeable->appendPrimitive(
										primType);
								m_pGdpChangeable->copyPrimitiveAttributes(
										*pDupPrimitive,*m_pPrimitive);
#else
								pDupPrimitive=
										m_pGdpChangeable->appendPrimitive(
										primType,m_pPrimitive,NULL);
#endif
								dupIndex=pDupPrimitive->getNum();
							}
#else
							if(m_element==SurfaceAccessibleI::e_point)
							{
								select(a_index,a_subIndex);

								FEASSERT(m_pPoint);
								const GEO_Point* pDupPoint=
										m_pGdpChangeable->appendPoint(
										*m_pPoint);
								return pDupPoint->getNum();
							}
							else
							{
								select(a_index,a_subIndex);

								FEASSERT(m_pPrimitive);
								const unsigned primType=
										m_pPrimitive->getPrimitiveId();
								pDupPrimitive=
										m_pGdpChangeable->appendPrimitive(
										primType,m_pPrimitive);
								dupIndex=pDupPrimitive->getNum();
							}
#endif

							const GU_PrimPoly* pPolyOrig=
									dynamic_cast<const GU_PrimPoly*>(
									m_pPrimitive);
							GU_PrimPoly* pPolyDup=
									dynamic_cast<GU_PrimPoly*>(pDupPrimitive);

							const U32 vertCount=subCount(a_index);
							for(U32 vertIndex=0;vertIndex<vertCount;
									vertIndex++)
							{
								const I32 pointIndex=integer(a_index,vertIndex);
								append(dupIndex,pointIndex);
							}

							FEASSERT(subCount(dupIndex)==vertCount);

							if(pPolyOrig && pPolyDup && pPolyOrig->isClosed())
							{
								pPolyDup->close();
							}

							return dupIndex;
						}
virtual	I32				append(SurfaceAccessibleI::Form a_form)
						{
							if(m_element==SurfaceAccessibleI::e_point)
							{
//								GEO_Point* pGeoPoint=
										m_pGdpChangeable->appendPoint();

								return count()-1;
							}

							if(m_element==SurfaceAccessibleI::e_primitive)
							{
								//* add new poly

								const int open=
										(a_form==SurfaceAccessibleI::e_open)?
										GU_POLY_OPEN: GU_POLY_CLOSED;

								int npts=0;
								int appendPts=0;
//								GU_PrimPoly* pGuPrimPoly=
										GU_PrimPoly::build(m_pGdpChangeable,
										npts,open,appendPts);

								return count()-1;
							}

							return -1;
						}
virtual	void			append(U32 a_index,I32 a_integer)
						{
							if(m_element==SurfaceAccessibleI::e_detail)
							{
								m_tupleSize+=a_integer;
								return;
							}

							if(m_element!=SurfaceAccessibleI::e_pointGroup &&
									m_element!=
									SurfaceAccessibleI::e_primitiveGroup &&
									!(m_element==
									SurfaceAccessibleI::e_primitive &&
									m_attribute==
									SurfaceAccessibleI::e_vertices))
							{
								return;
							}

							FEASSERT(m_pGdpChangeable);
							if(!m_pGdpChangeable)
							{
								return;
							}

							if(m_element==SurfaceAccessibleI::e_primitive)
							{
								select(a_index,0);

								GEO_TriMesh* pGeoTriMesh=
										dynamic_cast<GEO_TriMesh*>(
										const_cast<GEO_Primitive*>(
										m_pPrimitive));
								FEASSERT(pGeoTriMesh);
#ifdef FE_HOUDINI_USE_GA
								const GA_Offset offset=
										m_pGdpChangeable->pointOffset(
										a_integer);
//								const int vertIndex=
										pGeoTriMesh->appendVertex(offset);
#else
								GEO_Point* pPoint=
										m_pGdpChangeable->points()(a_integer);
//								const int vertIndex=
										pGeoTriMesh->appendVertex(pPoint);
#endif
								return;
							}

							// a_index ignored

//~							if(!m_pGroup)
//~							{
//~								if(m_element==SurfaceAccessibleI::e_pointGroup)
//~								{
//~									m_pGroup=m_pGdpChangeable->newPointGroup(
//~											m_attrName.c_str());
//~								}
//~								else
//~								{
//~									m_pGroup=
//~											m_pGdpChangeable->newPrimitiveGroup(
//~											m_attrName.c_str());
//~								}
//~							}
//~
//~							FEASSERT(m_pGroup);
//~
//~#ifdef FE_HOUDINI_USE_GA
//~							m_pGroup->addIndex(a_integer);
//~#else
//~							m_pGroup->add(a_integer);
//~#endif

#if FE_SAH_MULTIGROUP
							if(m_spMultiGroup.isValid())
							{
								m_spMultiGroup->add(a_integer);
							}
#else
							if(m_spCompoundGroup.isValid())
							{
								m_spCompoundGroup->append(a_integer);
							}
#endif
						}
virtual	void			append(Array< Array<I32> >& a_rPrimVerts);

virtual	void			remove(U32 a_index,I32 a_integer)
						{
							FEASSERT(m_pGdpChangeable);
							if(!m_pGdpChangeable)
							{
								return;
							}

#ifdef FE_HOUDINI_USE_GA
							if(m_element==SurfaceAccessibleI::e_point)
							{
								m_pGdpChangeable->destroyPointIndex(a_index);
								return;
							}

							if(m_element==SurfaceAccessibleI::e_primitive)
							{
								m_pGdpChangeable->destroyPrimitiveIndex(
										a_index);
								return;
							}
#else
							//* TODO
#endif

							if(m_element!=SurfaceAccessibleI::e_pointGroup &&
									m_element!=
									SurfaceAccessibleI::e_primitiveGroup)
							{
								return;
							}

//~							if(!m_pGroup)
//~							{
//~								return;
//~							}
//~
//~							FEASSERT(m_pGroup);
//~#ifdef FE_HOUDINI_USE_GA
//~							m_pGroup->removeIndex(a_index);
//~#else
//~							m_pGroup->remove(a_index);
//~#endif

#if FE_SAH_MULTIGROUP
							if(m_spMultiGroup.isValid())
							{
								m_spMultiGroup->remove(a_integer);
							}
#else
							if(m_spCompoundGroup.isValid())
							{
								m_spCompoundGroup->remove(a_index);
							}
#endif
						}

virtual	void			set(U32 a_index,U32 a_subIndex,Real a_real)
						{
#ifdef FE_HOUDINI_USE_GA
							if(m_element==SurfaceAccessibleI::e_point &&
									m_handleRealRW.isValid())
							{
#if	FE_SAH_ASSERT_BOUNDS
								FEASSERT(a_index<count());
								FEASSERT(a_subIndex<subCount(a_index));
#endif

#if FE_HOUDINI_12_5_PLUS
								m_handleRealRW.set(m_pGdp->pointOffset(a_index),
										a_subIndex,a_real);
								return;
								return;
#else
								if(!a_subIndex)
								{
									m_handleRealRW.set(
											m_pGdp->pointOffset(a_index),
											a_real);
									return;
								}
#endif
							}
#endif

							if(m_attribute!=SurfaceAccessibleI::e_vertices &&
									m_attribute!=
									SurfaceAccessibleI::e_properties &&
									!isBound())
							{
								addReal();
							}
#if	FE_SAH_ASSERT_BOUNDS
							FEASSERT(a_index<count());
#endif
							select(a_index,a_subIndex);

#if	FE_SAH_ASSERT_BOUNDS
//							FEASSERT(a_subIndex<subCount(a_index));
#endif

							m_handle.setF(a_real,a_subIndex);
						}
virtual	Real			real(U32 a_index,U32 a_subIndex=0)
						{
#if	FE_SAH_ASSERT_BOUNDS
							FEASSERT(a_index<count());
							FEASSERT(a_subIndex<subCount(a_index));
#endif

#ifdef FE_HOUDINI_USE_GA
							if(m_element==SurfaceAccessibleI::e_point &&
									m_handleRealRO.isValid())
							{
#if FE_HOUDINI_12_5_PLUS
								return m_handleRealRO.get(
										m_pGdp->pointOffset(a_index),
										a_subIndex);
#else
								if(!a_subIndex)
								{
									return m_handleRealRO.get(
											m_pGdp->pointOffset(a_index));
								}
#endif
							}
#endif

							select(a_index,a_subIndex);

							if(m_attribute==SurfaceAccessibleI::e_vertices ||
									m_attribute==
									SurfaceAccessibleI::e_properties)
							{
								return 0.0;
							}

							return m_handle.getF(a_subIndex);
						}

virtual	void			set(U32 a_index,U32 a_subIndex,
								const SpatialVector& a_vector)
						{
							if(m_attribute==SurfaceAccessibleI::e_properties)
							{
								return;
							}

							UT_Vector3 utVector3(a_vector[0],
									a_vector[1],a_vector[2]);

#ifdef FE_HOUDINI_USE_GA
							if(m_element==SurfaceAccessibleI::e_point &&
									m_handleV3RW.isValid())
							{
#if FE_HOUDINI_12_5_PLUS
								m_handleV3RW.set(m_pGdp->pointOffset(a_index),
										a_subIndex,utVector3);
								return;
								return;
#else
								if(!a_subIndex)
								{
									m_handleV3RW.set(
											m_pGdp->pointOffset(a_index),
											utVector3);
									return;
								}
#endif
							}
#endif

							const BWORD isPrimitiveUV=
									((m_element==SurfaceAccessibleI::e_primitive
									|| m_element==SurfaceAccessibleI::e_vertex
									|| m_element==
									SurfaceAccessibleI::e_primitiveGroup) &&
									m_attribute==SurfaceAccessibleI::e_uv);

							if(m_element!=SurfaceAccessibleI::e_pointGroup &&
									m_element!=
									SurfaceAccessibleI::e_primitiveGroup &&
									m_attribute!=
									SurfaceAccessibleI::e_vertices &&
									!isBound())
							{
								addSpatialVector();
							}

#if	FE_SAH_ASSERT_BOUNDS
							FEASSERT(a_index<count());
							FEASSERT(a_subIndex<subCount(a_index));
#endif

							select(a_index,a_subIndex);

							if(m_element==SurfaceAccessibleI::e_pointGroup)
							{
#ifdef FE_HOUDINI_USE_GA
//~								if(!m_pIterator || !m_pIterator->isValid())
//~								{
//~									return;
//~								}
//~								m_pGdpChangeable->setPos3(
//~										m_pIterator->getOffset(),utVector3);
#if FE_SAH_MULTIGROUP
								if(m_spMultiGroup.isNull())
								{
									return;
								}
								const I32 elementIndex=
										m_spMultiGroup->at(a_index);
#else
								if(m_spCompoundGroup.isNull())
								{
									return;
								}
								const I32 elementIndex=
										m_spCompoundGroup->getIndex();
#endif
								const GA_Offset offset=
										m_pGdpChangeable->pointOffset(
										elementIndex);
								m_pGdpChangeable->setPos3(offset,utVector3);
#else
								m_pPointChangeable->setPos(utVector3);
#endif
								return;
							}

							if(m_element==
									SurfaceAccessibleI::e_primitiveGroup ||
									m_attribute==SurfaceAccessibleI::e_vertices)
							{
								FEASSERT(m_element==
										SurfaceAccessibleI::e_primitiveGroup
										|| m_element==
										SurfaceAccessibleI::e_primitive);
								FEASSERT(m_pPrimitive);
#ifdef FE_HOUDINI_USE_GA
								const GA_Offset offset=
										m_pPrimitive->getPointOffset(a_subIndex);
								m_pGdpChangeable->setPos3(offset,utVector3);
#else
								m_pPrimitive->getVertex(a_subIndex).getPt()
										->setPos(utVector3);
#endif
								return;
							}

							m_handle.setV3(utVector3);
						}
virtual	SpatialVector	spatialVector(U32 a_index,U32 a_subIndex=0)
						{
#if	FE_SAH_ASSERT_BOUNDS
							FEASSERT(a_index<count());
							FEASSERT(a_subIndex<subCount(a_index));
#endif
							if(m_attribute==SurfaceAccessibleI::e_properties)
							{
								return SpatialVector(0.0,0.0,0.0);
							}

#ifdef FE_HOUDINI_USE_GA
							if(m_element==SurfaceAccessibleI::e_point &&
									m_handleV3RO.isValid())
							{
#if FE_HOUDINI_12_5_PLUS
								return Vector3f(m_handleV3RO.get(
										m_pGdp->pointOffset(a_index),
										a_subIndex).data());
#else
								if(!a_subIndex)
								{
									return Vector3f(m_handleV3RO.get(
											m_pGdp->pointOffset(a_index))
											.data());
								}
#endif
							}
#endif

							select(a_index,a_subIndex);

							if(m_element==SurfaceAccessibleI::e_pointGroup)
							{
#ifdef FE_HOUDINI_USE_GA
//~								if(!m_pIterator || !m_pIterator->isValid())
//~								{
//~									return SpatialVector(0.0,0.0,0.0);
//~								}
//~								const UT_Vector3 utVector3=m_pGdp->getPos3(
//~										m_pIterator->getOffset());
//~								return SpatialVector(utVector3.data());
#if FE_SAH_MULTIGROUP
								if(m_spMultiGroup.isNull())
								{
									return SpatialVector(0.0,0.0,0.0);
								}
								const I32 elementIndex=
										m_spMultiGroup->at(a_index);
#else
								if(m_spCompoundGroup.isNull())
								{
									return SpatialVector(0.0,0.0,0.0);
								}
								const I32 elementIndex=
										m_spCompoundGroup->getIndex();
#endif
								const GA_Offset offset=
										m_pGdp->pointOffset(elementIndex);
								const UT_Vector3 utVector3=
										m_pGdp->getPos3(offset);
								return Vector3f(utVector3.data());
#else
								FEASSERT(m_pPoint);
								UT_Vector3 utVector3=m_pPoint->getPos();
								return Vector3f(utVector3.data());
#endif
							}

							if(m_element==
									SurfaceAccessibleI::e_primitiveGroup ||
									m_attribute==SurfaceAccessibleI::e_vertices)
							{
								FEASSERT(m_element==
										SurfaceAccessibleI::e_primitiveGroup
										|| m_element==
										SurfaceAccessibleI::e_primitive);
								FEASSERT(m_pPrimitive);
#ifdef FE_HOUDINI_USE_GA
								const GA_Offset offset=
										m_pPrimitive->getPointOffset(
										a_subIndex);
								const UT_Vector3 utVector3=
										m_pGdp->getPos3(offset);
								return Vector3f(utVector3.data());
#else
								UT_Vector3 utVector3=m_pPrimitive
										->getVertex(a_subIndex).getPos();
								Vector3f vector(utVector3.data());
								return vector;
#endif
							}

							UT_Vector3 utVector3=m_handle.getV3();
							Vector3f vector(utVector3.data());
							return vector;
						}
virtual	BWORD			spatialVector(SpatialVector* a_pVectorArray,
								const Vector2i* a_pIndexArray,I32 a_arrayCount)
						{
#ifdef FE_HOUDINI_USE_GA
							if(m_element==SurfaceAccessibleI::e_point &&
									m_handleV3RO.isValid())
							{
								for(I32 arrayIndex=0;arrayIndex<a_arrayCount;
										arrayIndex++)
								{
									a_pVectorArray[arrayIndex]=
											Vector3f(m_handleV3RO.get(
											m_pGdp->pointOffset(
											a_pIndexArray[arrayIndex][0]))
											.data());
								}

								return TRUE;
							}
#endif

							return SurfaceAccessorBase::spatialVector(
									a_pVectorArray,a_pIndexArray,a_arrayCount);
						}

		sp<SurfaceAccessibleI>	surfaceAccessible(void)
						{	return m_spSurfaceAccessibleI; }

						//* Houdini specific
		void			setGdp(const GU_Detail* a_pGdp)
						{
							m_pGdp=a_pGdp;
							m_pGdpChangeable=NULL;
						}
		void			setGdpChangeable(GU_Detail* a_pGdp)
						{
							m_pGdp=a_pGdp;
							m_pGdpChangeable=a_pGdp;
						}
const	GU_Detail*		gdp(void)				{ return m_pGdp; }
		GU_Detail*		gdpChangeable(void)		{ return m_pGdpChangeable; }

		void			harden(void);

	private:
virtual	BWORD			bindInternal(SurfaceAccessibleI::Element a_element,
								const String& a_name);

		void			addString(void);
		void			addInteger(void);
		void			addReal(void);
		void			addSpatialVector(void);

		BWORD			isBound(void) const
						{	return (m_handle.isAttributeValid() ||
#if FE_SAH_MULTIGROUP
									m_spMultiGroup.isValid()
#else
									m_spCompoundGroup.isValid()
#endif
									);
						}

		void			select(U32 a_index,U32 a_subIndex);

const	GU_Detail*						m_pGdp;
		GU_Detail*						m_pGdpChangeable;

		GEO_AttributeHandle				m_handle;
const	GEO_Primitive*					m_pPrimitive;

//~		U32								m_groupIndex;

		I32								m_tupleSize;

#ifdef FE_HOUDINI_USE_GA
//~		GA_ElementGroup*				m_pGroup;

//~		GA_Iterator*					m_pIterator;

		GA_ROHandleI					m_handleIntRO;
		GA_RWHandleI					m_handleIntRW;
		GA_ROHandleF					m_handleRealRO;
		GA_RWHandleF					m_handleRealRW;
		GA_ROHandleV3					m_handleV3RO;
		GA_RWHandleV3					m_handleV3RW;
#else
const	GEO_Point*						m_pPoint;
		GEO_Point*						m_pPointChangeable;
//~		GB_Group*						m_pGroup;
#endif

	class CompoundGroup: public Counted
	{
		public:
							CompoundGroup(void):
								m_groupIndex(-1)
							{
#ifdef FE_HOUDINI_USE_GA
								m_pIterator=NULL;
#else
#endif
							}

	virtual					~CompoundGroup(void)							{}

			void			bind(sp<SurfaceAccessorHoudini>
									a_spSurfaceAccessorHoudini)
							{	m_spSurfaceAccessorHoudini=
										a_spSurfaceAccessorHoudini; }

			void			scan(String a_patterns);
			void			create(String a_groupName);

			void			append(I32 a_index);
			void			remove(I32 a_index);

			I32				count(void);
			void			select(I32 a_index);

			I32				getIndex(void);
	const	GEO_Point*		getPoint(void);
	const	GEO_Primitive*	getPrimitive(void);

			sp<SurfaceAccessorHoudini>	m_spSurfaceAccessorHoudini;

			Array<Vector2i>				m_startEnd;
			I32							m_groupIndex;
			U32							m_subIndex;

	const	GEO_Primitive*				m_pPrimitive;

#ifdef FE_HOUDINI_USE_GA
			//* NOTE pointers may have been cast from const
			//* TODO changeable distinction

			Array<GA_ElementGroup*>		m_groupArray;
			GA_Iterator*				m_pIterator;

#else
			Array<GB_Group*>			m_groupArray;
	const	GEO_Point*					m_pPoint;
			GEO_Point*					m_pPointChangeable;
#endif
	};

#if FE_SAH_MULTIGROUP
		sp<SurfaceAccessibleBase::MultiGroup>	m_spMultiGroup;
#else
		sp<CompoundGroup>						m_spCompoundGroup;
#endif

#if FE_HOUDINI_HARDENING
	public:

	class Hardening: public Handled<Hardening>
	{
		public:
					Hardening(void):
						m_pHarden(NULL)										{}
	virtual			~Hardening(void)
					{	delete m_pHarden; }

			void	harden(GA_AutoHardenForThreading* a_pHarden)
					{	m_pHarden=a_pHarden; }
			BWORD	hardened(void) const
					{	return m_pHarden!=NULL; }

		private:
			GA_AutoHardenForThreading*	m_pHarden;
	};
		sp<Hardening>					m_spHardening;
#endif
};

} /* namespace ext */
} /* namespace fe */

#endif /* __houdini_SurfaceAccessorHoudini_h__ */
