/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __houdini_HoudiniRamp_h__
#define __houdini_HoudiniRamp_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
    @brief Ramp in Houdini

	@ingroup houdini
*//***************************************************************************/
class FE_DL_EXPORT HoudiniRamp:
	public MetaRamp,
	public CastableAs<HoudiniRamp>
{
	public:
					HoudiniRamp(void)										{}
virtual				~HoudiniRamp(void)										{}

					//* as RampI
virtual	U32			entryCount(void) const
					{	return m_ramp.getNodeCount(); }

virtual	Vector2		entry(U32 a_index) const
					{
						const UT_ColorNode* pColorNode=m_ramp.getNode(a_index);
						return Vector2(pColorNode->t,pColorNode->rgba.r);
					}

virtual	Real		eval(Real a_u) const
					{
						float values[4];
						m_ramp.rampLookup(a_u,values);
						return values[0];
					}

		UT_Ramp&	nativeRamp(void)	{ return m_ramp; }

	private:

		UT_Ramp		m_ramp;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __houdini_HoudiniRamp_h__ */
