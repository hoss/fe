/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __tire_LocatorFromEulerSystem_h__
#define __tire_LocatorFromEulerSystem_h__

#include "solve/solve.h"

namespace fe
{
namespace ext
{

class LocatorFromEulerSystem :
	virtual public SystemI,
	public Initialize<LocatorFromEulerSystem>
{
	public:
		LocatorFromEulerSystem(void) {}
		void initialize(void) {}
		void compile(const t_note_id &a_note_id)
		{
			AsLocatorFromEuler	asLocatorFromEuler;
			asLocatorFromEuler.bind(m_rg_dataset->scope());
			std::vector<Record>	locators;
			asLocatorFromEuler.filter(locators, m_rg_dataset);

			for(unsigned int i_loc = 0; i_loc < locators.size(); i_loc++)
			{
				Record &r_locator = locators[i_loc];
				setIdentity(asLocatorFromEuler.transform(r_locator));
				translate(asLocatorFromEuler.transform(r_locator),
					asLocatorFromEuler.location(r_locator));
				rotate(asLocatorFromEuler.transform(r_locator),
					asLocatorFromEuler.zyx(r_locator)[0], e_zAxis);
				rotate(asLocatorFromEuler.transform(r_locator),
					asLocatorFromEuler.zyx(r_locator)[1], e_yAxis);
				rotate(asLocatorFromEuler.transform(r_locator),
					asLocatorFromEuler.zyx(r_locator)[2], e_xAxis);
			}
		}
virtual	void connectOrchestrator(sp<OrchestratorI> a_spOrchestrator)
		{
			a_spOrchestrator->connect(this,
				&LocatorFromEulerSystem::compile,FE_NOTE_COMPILE);
			m_rg_dataset = a_spOrchestrator->dataset();
		}

	protected:
		sp<RecordGroup>			m_rg_dataset;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __tire_LocatorFromEulerSystem_h__ */

