/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __tire_SoftSystem_h__
#define __tire_SoftSystem_h__

#include "solve/solve.h"
#include "evaluate/evaluate.h"
#include "evaluate/Function.h"

namespace fe
{
namespace ext
{

// Force for internal use
class ExternalForce : public SemiImplicit::Force,
	public CastableAs<ExternalForce>
{
	public:
		ExternalForce(sp<SemiImplicit> a_integrator) {}
virtual	~ExternalForce(void) {}

		void bind(SemiImplicit::Particle *a_particle)
		{
			m_particle = a_particle;
		}

virtual	void accumulate(void)
		{
			m_particle->m_force[0] += (t_solve_real)(m_force[0]);
			m_particle->m_force[1] += (t_solve_real)(m_force[1]);
			m_particle->m_force[2] += (t_solve_real)(m_force[2]);
		}

		void	set(const t_moa_v3 &a_force)
		{
			m_force = a_force;
		}
		void	add(const t_moa_v3 &a_force)
		{
			m_force += a_force;
		}
		t_solve_v3	location(void)
		{
			return m_particle->m_location;
		}
		t_solve_v3	velocity(void)
		{
			return m_particle->m_velocity;
		}

	private:
		SemiImplicit::Particle		*m_particle;
		t_moa_v3					m_force;
};


/// @brief Soft System
class SoftSystem : //MOA basic soft body system
	virtual public Stepper,
	public Initialize<SoftSystem>
{
	public:
		SoftSystem(void)
		{
		}
		void	initialize(void) {}
virtual void	compile(const t_note_id &a_note_id);
		void	step(t_moa_real a_dt);

		struct Soft
		{
			WeakRecord					m_r_parent;
			sp<SemiImplicit>			m_spSemiImplicit;

			std::vector< sp<ExternalForce> >	m_spExternalForces;
			std::vector<Record>					m_forceParticles;
		};

		sp<Scope>							m_spScope;

		std::vector<Soft>					m_softs;
		AsSoft								m_asSoft;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __tire_SoftSystem_h__ */
