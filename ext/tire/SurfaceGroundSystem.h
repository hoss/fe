/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __tire_SurfaceGroundSystem_h__
#define __tire_SurfaceGroundSystem_h__

namespace fe
{
namespace ext
{

/// @brief AsTireLiveCollision to SurfaceI collision system
class SurfaceGroundTireSystem :
	virtual public Stepper,
	virtual public GroundI,
	public Initialize<SurfaceGroundTireSystem>
{
	public:
		class Tire
		{
			public:
				WeakRecord			m_r_tire;
		};
		SurfaceGroundTireSystem(void);
		void	initialize(void) {}
virtual void	compile(const t_note_id &a_note_id);
virtual	void	step(t_moa_real a_dt);
virtual	void	setSurface(sp<SurfaceI> a_spSurface);
virtual	void	setTransform(const SpatialTransform &a_transform);
	private:
		sp<SurfaceI>		m_spSurface;
		SpatialTransform		m_transform;
		std::vector<Tire>		m_tires;
};


class SurfaceClosestPointSystem :
	virtual public Stepper,
	public Initialize<SurfaceClosestPointSystem>
{
	public:
		SurfaceClosestPointSystem(void);
		void	initialize(void) {}
virtual void	compile(const t_note_id &a_note_id);
virtual	void	step(t_moa_real a_dt);
	private:
		sp<RecordGroup> rg_all;
		AsContactLive			m_asContact;
		AsLocator				m_asLocator;
		AsSurfaceLive			m_asSurface;
		AsSurfacePlane			m_asSurfacePlane;
		AsPoint		m_asPoint;
		std::vector<Record> m_xform_contacts;
		std::vector<Record> m_surfaces;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __tire_SurfaceGroundSystem_h__ */

