/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "tire/tire.pmh"

namespace fe
{
namespace ext
{

LinearTire::LinearTire(void)
{
	initialize();
}

LinearTire::~LinearTire(void)
{
}

void LinearTire::initialize(void)
{
	m_contact_radius = 1.0e12;
	m_inclination = 0.0;
}

void LinearTire::step(t_moa_real a_dt)
{
	m_force = t_moa_v3(0.0,0.0,0.0);
	m_moment = t_moa_v3(0.0,0.0,0.0);

	t_moa_real deflection = m_radius - m_contact_radius;

#if FE_GS_EVAL_GLOBAL_VAR
	gs_eval_global_var["LT Vx"] = m_velocity[0];
	gs_eval_global_var["LT Vy"] = m_velocity[1];
	gs_eval_global_var["LT Vz"] = m_velocity[2];
#endif

	if(deflection <= 0.0) { return; } // Not touching the surface

	// vertical spring damper model
	m_force[2] += deflection*m_z_stiffness;
	m_force[2] -= m_velocity[2]*m_z_damping;

	// m_radius, not m_contact_radius, for slip, because that much tread
	// has to be flowing through the patch, no matter how deformed on average
	t_moa_v2 slip = -m_velocity;
	slip[0] += m_angular_velocity[1] * m_radius;

	t_moa_v2 nF = m_xy_stiffness * slip;
	t_moa_real MnF = magnitude(nF);
	if(MnF > 1.0)
	{
		nF *= 1.0 / MnF;
		MnF = 1.0;
	}

	t_moa_real F = m_Cf * m_force[2];

	if(F > m_Fmax)
	{
		F = m_Fmax;
	}

	m_force[0] += F * nF[0];
	m_force[1] += F * nF[1];

	m_moment[2] -= m_force[1] * m_pneumatic_trail * (1.0-MnF);

#if FE_GS_EVAL_GLOBAL_VAR
	gs_eval_global_var["LT Fx"] = m_force[0];
	gs_eval_global_var["LT Fy"] = m_force[1];
	gs_eval_global_var["LT Fz"] = m_force[2];
	gs_eval_global_var["LT Mz"] = m_moment[2];
#endif

//TODO: fixing the moment from patch to tire should not longer be here
	if(m_do_rollover)
	{
		t_moa_real z = -m_contact_radius;
		m_moment[1] += +z * m_force[0];
		m_moment[0] += -z * m_force[1];
	}
}

void LinearTire::setVelocity(const t_moa_v3 &a_velocity)
{
	m_velocity = a_velocity;
}

void LinearTire::setAngularVelocity(const t_moa_v3 &a_ang_velocity)
{
	m_angular_velocity = a_ang_velocity;
}

void LinearTire::setContact(const t_moa_real a_radius,
	const t_moa_real a_inclination)
{
	m_contact_radius = a_radius;
	m_inclination = a_inclination;
}

const t_moa_v3 &LinearTire::getForce(void)
{
	return m_force;
}

const t_moa_v3 &LinearTire::getMoment(void)
{
	return m_moment;
}

const t_moa_v3 &LinearTire::getVelocity(void)
{
	return m_velocity;
}

const t_moa_v3	&LinearTire::getAngularVelocity(void)
{
	return m_angular_velocity;
}

bool LinearTire::compile(Record r_tire, Record r_config,fe::sp<fe::RecordGroup> a_rg_dataset)
{
	AsLinearTireModel as(r_config);
	if(!as.check(r_config))
	{
		fe_fprintf(stderr, "LinearTire::compile: failed to read config");
		return false;
	}

	m_xy_stiffness = as.xy_stiffness(r_config);
	m_z_stiffness = as.stiffness(r_config);
	m_z_damping = as.damping(r_config);
	m_Cf = as.friction(r_config);
	m_Fmax = as.Fmax(r_config);
	m_pneumatic_trail = as.pneumatic_trail(r_config);
	m_do_rollover = as.do_rollover(r_config);

	AsTireLive asTireLive(r_tire);

	m_radius = asTireLive.radius(r_tire);
	m_width = asTireLive.width(r_tire);

	return true;
}

} /* namespace ext */
} /* namespace fe */

