/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __tire_MountSystem_h__
#define __tire_MountSystem_h__

#include "solve/solve.h"

namespace fe
{
namespace ext
{

/// @brief Tire to Rig Connection
class MountSystem :
	virtual public Stepper,
	public Initialize<MountSystem>
{
	public:
		struct Mount
		{
			WeakRecord					m_r_mount;
			WeakRecord					m_r_rig;
			WeakRecord					m_r_tire;
		};
		MountSystem(void)
		{
		}
		void	initialize(void) {}
virtual	void	compile(const t_note_id &a_note_id)
		{
			sp<Scope> spScope = m_rg_dataset->scope();

			if(!spScope.isValid()) { return; }

			enforce<AsTire,AsTireLive>(m_rg_dataset);

			m_asRig.bind(spScope);
			m_asTireLive.bind(spScope);
			m_asMount.bind(spScope);

			AsTire asTire(spScope);

			RecordDictionary<AsRig> rd_rigs(m_rg_dataset);
			RecordDictionary<AsTire> rd_tires(m_rg_dataset);

			std::vector<Record> mounts;
			m_asMount.filter(mounts, m_rg_dataset);

			for(unsigned int i_mount = 0; i_mount < mounts.size(); i_mount++)
			{
				Record &r_mount = mounts[i_mount];
				Mount mount;
				mount.m_r_mount = r_mount;
				mount.m_r_rig = rd_rigs[m_asMount.rig(r_mount)];
				mount.m_r_tire = rd_tires[m_asMount.tire(r_mount)];
				if(	mount.m_r_rig.isValid() &&
					mount.m_r_tire.isValid() &&
					m_asRig.check(mount.m_r_rig) &&
					m_asTireLive.check(mount.m_r_tire))
				{
					m_mounts.push_back(mount);
				}
			}

		}
		void	step(t_moa_real a_dt)
		{
			for(unsigned int i_mount = 0; i_mount < m_mounts.size(); i_mount++)
			{
				WeakRecord &r_rig = m_mounts[i_mount].m_r_rig;
				WeakRecord &r_tire = m_mounts[i_mount].m_r_tire;
				WeakRecord &r_mount = m_mounts[i_mount].m_r_mount;

				t_moa_real alpha = m_asRig.alpha(r_rig)*pi/180.0;
				t_moa_v3 vel;
				vel[0] = m_asRig.velocity(r_rig) * cos(alpha);
				vel[1] = m_asRig.velocity(r_rig) * sin(alpha)
					* cos(m_asRig.inclination(r_rig));
				vel[2] = m_asRig.pnt_velocity(r_rig)[2];

				setIdentity(m_asTireLive.transform(r_tire));
				translate(m_asTireLive.transform(r_tire),
					m_asRig.location(r_rig));
				rotate(m_asTireLive.transform(r_tire),
					m_asRig.inclination(r_rig), e_xAxis);
				invert(m_asMount.transform_inv(r_mount),
					m_asTireLive.transform(r_tire));

				vel = rotateVector<3, t_moa_real>(
						m_asMount.transform_inv(r_mount),vel);
				m_asTireLive.velocity(r_tire) = vel;
				t_moa_v3 angular_velocity = t_moa_v3(0,m_asRig.kappa(r_rig),0);
				if(m_asRig.free_wheel(r_rig)) { angular_velocity[1] += vel[0] /
					m_asTireLive.radius(r_tire); }
				m_asTireLive.angular_velocity(r_tire) = angular_velocity;
			}
			accumulate();
		}
		void	accumulate(void)
		{
			for(unsigned int i_mount = 0; i_mount < m_mounts.size(); i_mount++)
			{
				WeakRecord &r_rig = m_mounts[i_mount].m_r_rig;
				WeakRecord &r_tire = m_mounts[i_mount].m_r_tire;
				WeakRecord &r_mount = m_mounts[i_mount].m_r_mount;

				t_moa_v3 rig_space_F = rotateVector<3, t_moa_real>
					(m_asTireLive.transform(r_tire),
					m_asTireLive.force(r_tire));
				t_moa_v3 rig_space_M = rotateVector<3, t_moa_real>
					(m_asTireLive.transform(r_tire),
					m_asTireLive.moment(r_tire));

				m_asRig.force_accum(r_rig).add(rig_space_F);
				m_asRig.moment_accum(r_rig).add(rig_space_M);
			}
		}

		std::vector<Mount>	m_mounts;
		AsRigLive			m_asRig;
		AsTireLive			m_asTireLive;
		AsMount				m_asMount;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __tire_MountSystem_h__ */


