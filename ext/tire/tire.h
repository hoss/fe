/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __tire_tire_h__
#define __tire_tire_h__

#include "fe/plugin.h"
#include "fe/data.h"
#include "math/math.h"
#include "mechanics/mechanics.h"
#include "surface/surface.h"
#include "moa/moa.h"

#define FE_RIG_ATT_PARTICLE		"rig_attach_particle"
#define FE_L_RIG_CONSTRAINT		"layout_rig_constraint"
#define FE_L_RIG_SPRING			"layout_rig_spring"
#define FE_L_RIG_PARTICLE			"layout_rig_particle"

namespace fe
{
namespace ext
{

t_moa_real sigmoid(t_moa_real a_x, t_moa_real a_max, t_moa_real a_min,
	t_moa_real a_cross_value, t_moa_real a_smoothness, t_moa_real a_zero_clip);

void applyFMToPoints(t_moa_v3 &F, t_moa_v3 &M, t_moa_v3 &P,
	AsForcePoint &a_asPoint, WeakRecord &r_p0,
	WeakRecord &r_p1, WeakRecord &r_p2);

} /* namespace ext */
} /* namespace fe */

#include "tire/TireI.h"
#include "tire/tireAS.h"
//#include "tire/BrushAS.h"
//#include "tire/LinearAS.h"

#endif /* __tire_tire_h__ */
