/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "tire/tire.pmh"

namespace fe
{
namespace ext
{

BlendTire::BlendTire(void)
{
	initialize();
}

BlendTire::~BlendTire(void)
{
}

void BlendTire::initialize(void)
{
	m_contact_radius = 1.0e12;
	m_inclination = 0.0;
}

void BlendTire::step(t_moa_real a_dt)
{
	if(m_spTireIupper.isValid() && m_spTireIlower.isValid())
	{
		t_moa_real surface_speed = magnitude(m_velocity);
		t_moa_real surface_blend = 1.0 - sigmoid(	surface_speed,
													1.0,
													0.0,
													m_slow_blend[0],
													m_slow_blend[1],
													0.0);

		t_moa_v3 slip_velocity = m_velocity;
		slip_velocity[0] -= m_angular_velocity[1] * m_radius;
		t_moa_real slip_speed = magnitude(slip_velocity);
		t_moa_real slip_blend = 1.0 - sigmoid(	slip_speed,
												1.0,
												0.0,
												m_slow_blend[2],
												m_slow_blend[3],
												0.0);

		t_moa_real blend = slip_blend*surface_blend;

		m_spTireIupper->step(a_dt);
		m_spTireIlower->step(a_dt);

		m_force = blend*m_spTireIlower->getForce()
			+ (1.0-blend)*m_spTireIupper->getForce();
		m_moment = blend*m_spTireIlower->getMoment()
			+ (1.0-blend)*m_spTireIupper->getMoment();
		m_radius = blend*m_spTireIlower->getRadius()
			+ (1.0-blend)*m_spTireIupper->getRadius();
	}
	else if(m_spTireIupper.isValid())
	{
		m_spTireIupper->step(a_dt);
		m_force = m_spTireIupper->getForce();
		m_moment = m_spTireIupper->getMoment();
	}
	else if(m_spTireIlower.isValid())
	{
		m_spTireIlower->step(a_dt);
		m_force = m_spTireIlower->getForce();
		m_moment = m_spTireIlower->getMoment();
	}
}

void BlendTire::setVelocity(const t_moa_v3 &a_velocity)
{
	m_velocity = a_velocity;
	if(m_spTireIupper.isValid()) { m_spTireIupper->setVelocity(a_velocity); }
	if(m_spTireIlower.isValid()) { m_spTireIlower->setVelocity(a_velocity); }
}

void BlendTire::setAngularVelocity(const t_moa_v3 &a_ang_velocity)
{
	m_angular_velocity = a_ang_velocity;
	if(m_spTireIupper.isValid())
	{
		m_spTireIupper->setAngularVelocity(a_ang_velocity);
	}
	if(m_spTireIlower.isValid())
	{
		m_spTireIlower->setAngularVelocity(a_ang_velocity);
	}
}

void BlendTire::setContact(const t_moa_real a_radius,
	const t_moa_real a_inclination)
{
	m_contact_radius = a_radius;
	m_inclination = a_inclination;
	if(m_spTireIupper.isValid())
	{
		m_spTireIupper->setContact(a_radius, a_inclination);
	}
	if(m_spTireIlower.isValid())
	{
		m_spTireIlower->setContact(a_radius, a_inclination);
	}
}

const t_moa_v3 &BlendTire::getForce(void)
{
	return m_force;
}

const t_moa_v3 &BlendTire::getMoment(void)
{
	return m_moment;
}

const t_moa_v3 &BlendTire::getVelocity(void)
{
	return m_velocity;
}

const t_moa_v3	&BlendTire::getAngularVelocity(void)
{
	return m_angular_velocity;
}

bool BlendTire::compile(Record r_tire, Record r_config,
	fe::sp<fe::RecordGroup> a_rg_dataset)
{
	AsBlendTireModel as(r_config);
	if(!as.check(r_config))
	{
		fe_fprintf(stderr, "BlendTire::compile: failed to read config");
		return false;
	}

	m_slow_blend = as.slow_blend(r_config);

	AsTireLive asTireLive(r_tire);

	m_radius = asTireLive.radius(r_tire);
	m_width = asTireLive.width(r_tire);

	// Dictionary for all tire models available
	sp<Scope> spScope = a_rg_dataset->scope();
	RecordDictionary<AsTireModel> rd_models(a_rg_dataset);
	AsTireModel asTireModel;
	asTireModel.bind(spScope);

	std::vector<Record>	tires;
	asTireLive.filter(tires, a_rg_dataset);

	Record r_model;
	r_model = rd_models[as.model_upper(r_config)];
	if(r_model.isValid())
	{
		sp<TireI> spTire(registry()->create(
			asTireModel.component(r_model)));
		if(spTire.isValid())
		{
			spTire->compile(r_tire, r_model, a_rg_dataset);
			m_spTireIupper = spTire;
		}
	}
	r_model = rd_models[as.model_lower(r_config)];
	if(r_model.isValid())
	{
		sp<TireI> spTire(registry()->create(
			asTireModel.component(r_model)));
		if(spTire.isValid())
		{
			spTire->compile(r_tire, r_model, a_rg_dataset);
			m_spTireIlower = spTire;
		}
	}

	return true;
}

} /* namespace ext */
} /* namespace fe */
