/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __tire_RigParticleMountSystem_h__
#define __tire_RigParticleMountSystem_h__

#include "solve/solve.h"

namespace fe
{
namespace ext
{

/// @brief Tire to Rig Connection
class RigParticleMountSystem :
	virtual public Stepper,
	public Initialize<RigParticleMountSystem>
{
	public:
		struct Mount
		{
			WeakRecord					m_r_parent;
			WeakRecord					m_r_p0;
			WeakRecord					m_r_p1;
			WeakRecord					m_r_p2;
			WeakRecord					m_r_tire;
			t_moa_xform						m_inv_R;

			WeakRecord					m_r_rig;
			WeakRecord					m_r_floor;
		};
		RigParticleMountSystem(void)
		{
		}
		void	initialize(void)
		{
		}
virtual	void	compile(const t_note_id &a_note_id)
		{
			sp<Scope> spScope = m_rg_dataset->scope();

			if(!spScope.isValid()) { return; }

			enforce<AsTire,AsTireLive>(m_rg_dataset);

			m_asRig.bind(spScope);
			m_asPoint.bind(spScope);
			m_asTireLive.bind(spScope);
			m_asMount.bind(spScope);
			m_asSurface.bind(spScope);

			AsTire asTire(spScope);

			RecordDictionary<AsRig> rd_rigs(m_rg_dataset);
			RecordDictionary<AsTire> rd_tires(m_rg_dataset);

			std::vector<Record> mounts;
			m_asMount.filter(mounts, m_rg_dataset);

			for(unsigned int i_mount = 0; i_mount < mounts.size(); i_mount++)
			{
				Record &r_mount = mounts[i_mount];
				Mount mount;
				mount.m_r_parent = r_mount;
				mount.m_r_p0 = m_asMount.p0(r_mount);
				mount.m_r_p1 = m_asMount.p1(r_mount);
				mount.m_r_p2 = m_asMount.p2(r_mount);
				mount.m_r_tire = rd_tires[m_asMount.tire(r_mount)];
				mount.m_r_rig = rd_rigs[m_asMount.rig(r_mount)];
				if(	mount.m_r_p0.isValid() &&
					mount.m_r_p1.isValid() &&
					mount.m_r_p2.isValid() &&
					mount.m_r_tire.isValid() &&
					mount.m_r_rig.isValid() &&
					m_asPoint.check(mount.m_r_p0) &&
					m_asPoint.check(mount.m_r_p1) &&
					m_asPoint.check(mount.m_r_p2) &&
					m_asRig.check(mount.m_r_rig) &&
					m_asTireLive.check(mount.m_r_tire))
				{

					if(m_asRig.floor(mount.m_r_rig).isValid())
					{
						if(m_asSurface.check(m_asRig.floor(mount.m_r_rig)))
						{
							mount.m_r_floor = m_asRig.floor(mount.m_r_rig);
						}
					}

					t_moa_xform frame;
					t_moa_xform relative;
					setIdentity(relative);

					bool frame_made = makeFrame(frame,
						m_asPoint.location(mount.m_r_p0),
						m_asPoint.location(mount.m_r_p1),
						m_asPoint.location(mount.m_r_p2),
						relative);

					invert(mount.m_inv_R, frame);

					translate(relative, m_asMount.location(r_mount));
					rotate(relative, m_asMount.inclination(r_mount),
						e_xAxis);

					// useful to test for pitch relates bugs/bias
					rotate(relative, m_asRig.pitch(mount.m_r_rig), e_yAxis);

					mount.m_inv_R = relative * mount.m_inv_R;

					m_mounts.push_back(mount);
				}
			}

		}
		void	step(t_moa_real a_dt)
		{
			for(unsigned int i_mount = 0; i_mount < m_mounts.size(); i_mount++)
			{
				WeakRecord &r_p0 = m_mounts[i_mount].m_r_p0;
				WeakRecord &r_p1 = m_mounts[i_mount].m_r_p1;
				WeakRecord &r_p2 = m_mounts[i_mount].m_r_p2;
				WeakRecord &r_tire = m_mounts[i_mount].m_r_tire;
				WeakRecord &r_mount = m_mounts[i_mount].m_r_parent;
				WeakRecord &r_rig = m_mounts[i_mount].m_r_rig;

				t_moa_xform frame;
				t_moa_xform relative;
				setIdentity(relative);
				bool frame_made = makeFrame(frame,
					m_asPoint.location(r_p0),
					m_asPoint.location(r_p1),
					m_asPoint.location(r_p2),
					m_mounts[i_mount].m_inv_R);

				if(m_mounts[i_mount].m_r_floor.isValid())
				{
					t_moa_real alpha = m_asRig.alpha(r_rig)*pi/180.0;

					m_asSurface.velocity(m_mounts[i_mount].m_r_floor)[0] =
						m_asRig.velocity(r_rig);
					m_asSurface.velocity(m_mounts[i_mount].m_r_floor)[1] = 0.0;

					m_asTireLive.transform(r_tire) = frame;

					if(determinant(m_asTireLive.transform(r_tire)) == 0.0)
						{ continue; }

					t_moa_xform incl_xform;
					setIdentity(incl_xform);

					if(m_asRig.incline_floor(r_rig))
					{
						rotate(incl_xform, m_asRig.inclination(r_rig),
							e_xAxis);
					}
					else
					{
						rotate(m_asTireLive.transform(r_tire),
							m_asRig.inclination(r_rig), e_xAxis);
					}

					rotate(incl_xform, alpha, e_zAxis);
					//translate(incl_xform, t_moa_v3(0.0,0.0,-0.22));
					m_asSurface.transform(m_mounts[i_mount].m_r_floor)
						= incl_xform;

					invert(m_asMount.transform_inv(r_mount),
						m_asTireLive.transform(r_tire));

					m_asTireLive.velocity(r_tire) =
						rotateVector<3, t_moa_real> (
						m_asMount.transform_inv(r_mount),
						m_asPoint.velocity(r_p0));

					t_moa_v3 angular_velocity = t_moa_v3(0,
						m_asRig.kappa(r_rig),0);
					if(m_asRig.free_wheel(r_rig))
					{
						angular_velocity[1]
							+= (m_asRig.velocity(r_rig)*cos(alpha))
							/ m_asTireLive.radius(r_tire);
					}
					m_asTireLive.angular_velocity(r_tire) = angular_velocity;
				}
				else
				{

					// rig is essentially being used as a rolling road here
					t_moa_real alpha = m_asRig.alpha(r_rig)*pi/180.0;
					t_moa_v3 rig_vel(0,0,0);
					rig_vel[0] = m_asRig.velocity(r_rig) * cos(alpha);
					rig_vel[1] = m_asRig.velocity(r_rig) * sin(alpha);
					rig_vel[2] = 0.0;

					m_asTireLive.transform(r_tire) = frame;

					if(determinant(m_asTireLive.transform(r_tire)) == 0.0)
					{
						continue;
					}

					rotate(m_asTireLive.transform(r_tire),
						m_asRig.inclination(r_rig), e_xAxis);
					invert(m_asMount.transform_inv(r_mount),
						m_asTireLive.transform(r_tire));

					rig_vel = rotateVector<3, t_moa_real>(
						m_asMount.transform_inv(r_mount),rig_vel);

					t_moa_v3 tire_space_p0_v =
						rotateVector<3, t_moa_real> (
							m_asMount.transform_inv(r_mount),
							m_asPoint.velocity(r_p0));

					t_moa_v3 vel = tire_space_p0_v + rig_vel;

					m_asTireLive.velocity(r_tire) = vel;

					t_moa_v3 angular_velocity = t_moa_v3(0,
						m_asRig.kappa(r_rig),0);
					if(m_asRig.free_wheel(r_rig))
					{
						angular_velocity[1]
							+= (m_asRig.velocity(r_rig)*cos(alpha))
							/ m_asTireLive.radius(r_tire);
					}
					m_asTireLive.angular_velocity(r_tire) = angular_velocity;
				}
			}
			accumulate();
		}
		void	accumulate(void)
		{
			for(unsigned int i_mount = 0; i_mount < m_mounts.size(); i_mount++)
			{
				WeakRecord &r_p0 = m_mounts[i_mount].m_r_p0;
				WeakRecord &r_p1 = m_mounts[i_mount].m_r_p1;
				WeakRecord &r_p2 = m_mounts[i_mount].m_r_p2;
				WeakRecord &r_tire = m_mounts[i_mount].m_r_tire;

				t_moa_xform tire_xform = m_asTireLive.transform(r_tire);

				t_moa_v3 rig_space_F = rotateVector<3, t_moa_real>
					(tire_xform, m_asTireLive.force(r_tire));
				t_moa_v3 rig_space_M = rotateVector<3, t_moa_real>
					(tire_xform, m_asTireLive.moment(r_tire));

				t_moa_v3 rig_space_P = tire_xform.translation();

				applyFMToPoints(rig_space_F, rig_space_M, rig_space_P,
					m_asPoint, r_p0, r_p1, r_p2);
			}
		}

		std::vector<Mount>		m_mounts;
		AsForcePoint	m_asPoint;
		AsTireLive				m_asTireLive;
		AsRigParticlesMount		m_asMount;
		AsRigLive				m_asRig;
		AsSurface				m_asSurface;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __tire_RigParticleMountSystem_h__ */

