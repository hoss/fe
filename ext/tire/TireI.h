/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __tire_TireI_h__
#define __tire_TireI_h__

namespace fe
{
namespace ext
{

/// The most simple least common denominator, single contact, patch oriented
/// Many (most?) "tire models" are this, or use this.
class FE_DL_EXPORT TireI
	: virtual public Component,
	public CastableAs<TireI>
{
	public:
				/** Timestep the tire */
virtual	void	step(t_moa_real a_dt)										= 0;

				/** "Make" a tire based on src/data configuration.
					r_tire is the actual record the tire can access "live".
					r_config is the record, possibly loaded from file, to
					parameterize the model.
					*/
virtual	bool	compile(Record r_tire, Record r_config,
							sp<RecordGroup> a_rg_dataset)					= 0;

				/** Translational velocity in tire space, wrt surface.
					Tire space:
					X is longitudinal
					Y is lateral
					Z is vertical (yaw axis)
					*/
virtual	void	setVelocity(		const t_moa_v3		&a_velocity)		= 0;
				/** Angular velocity around Y axis
					- X axis (camber-like roll)
					- Y axis (normal sense of tire rotation)
					- Z axis (steering velocity)
					*/
virtual	void	setAngularVelocity(	const t_moa_v3		&a_ang_velocity)	= 0;
virtual	void	setContact(			const t_moa_real	a_radius,
									const t_moa_real	a_inclination)		= 0;

				/** Return the force, in tire space */
virtual	const t_moa_v3		&getForce(void)									= 0;
				/** Return the moment, in tire space */
virtual	const t_moa_v3		&getMoment(void)								= 0;
virtual	const t_moa_v3		&getVelocity(void)								= 0;
virtual	const t_moa_v3		&getAngularVelocity(void)						= 0;
virtual	const t_moa_real	&getRadius(void)								= 0;

virtual	t_moa_real	&contactRadius(void)									= 0;
virtual	t_moa_real	&inclination(void)										= 0;

};

} /* namespace ext */
} /* namespace fe */

#endif /* __tire_TireI_h__ */


