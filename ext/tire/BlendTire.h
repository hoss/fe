/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __tire_BlendTire_h__
#define __tire_BlendTire_h__

namespace fe
{
namespace ext
{

/// @brief Simple Blend Tire Model
class AsBlendTireModel
	: public AsTireModel, public Initialize<AsBlendTireModel>
{
	public:
		AsConstruct(AsBlendTireModel);
		void initialize(void)
		{
			add(model_upper,			FE_USE("tire:model:upper"));
			add(model_lower,			FE_USE("tire:model:lower"));
			add(slow_blend,				FE_USE("tire:slow_blend"));
		}
		Accessor<String>			model_upper;
		Accessor<String>			model_lower;
		/**
			slow_blend parameters:
			0: surface vel crossover (m/s)
			1: surface vel crossover smoothness
			2: slip vel crossover (m/s)
			3: slip vel crossover smoothness
			*/
		Accessor<t_moa_v4>				slow_blend;


};


/// @brief Blend Tire Model
class BlendTire
	: virtual public TireI, virtual public StreamableI,
	public CastableAs<BlendTire>
{
	public:
					BlendTire(void);
virtual				~BlendTire(void);

					/** Integrate a time step. */
		void		step(t_moa_real a_dt);

					/** Compile internal structure.   This should be done
						before any time stepping. */
		bool		compile(Record r_tire, Record r_config,
						sp<RecordGroup> a_rg_dataset);

		/// @name	Dynamic State Accessors
		/// @{
		void		setVelocity(		const t_moa_v3		&a_velocity);
		void		setAngularVelocity(	const t_moa_v3		&a_ang_velocity);
		void		setContact(			const t_moa_real	a_radius,
										const t_moa_real	a_inclination);
		const t_moa_v3		&getForce(void);
		const t_moa_v3		&getMoment(void);
		const t_moa_v3		&getVelocity(void);
		const t_moa_v3		&getAngularVelocity(void);
		const t_moa_real	&getRadius(void) { return m_radius; }

		t_moa_real &contactRadius(void) { return m_contact_radius; }
		t_moa_real &inclination(void) { return m_inclination; }

virtual	void	output(std::ostream &a_ostrm)
		{
			a_ostrm << m_force[0] << " " << m_force[1] << " " << m_force[2];
		}
virtual	void	input(std::istream &a_istrm)
		{
			a_istrm >> m_force[0] >> m_force[1] >> m_force[2];
		}

	private:
		void			initialize(void);
		// dynamic state
		t_moa_v3			m_force;
		t_moa_v3			m_moment;
		t_moa_v3			m_velocity;
		t_moa_v3			m_angular_velocity;
		t_moa_real			m_contact_radius;
		t_moa_real			m_inclination;

		// geometry
		t_moa_real			m_radius;
		t_moa_real			m_width;

		//
		sp<TireI>			m_spTireIupper;
		sp<TireI>			m_spTireIlower;
		t_moa_v4			m_slow_blend;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __tire_BlendTire_h__ */

