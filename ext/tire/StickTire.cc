/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "tire/tire.pmh"

namespace fe
{
namespace ext
{

StickTire::StickTire(void)
{
	initialize();
}

StickTire::~StickTire(void)
{
}

void StickTire::initialize(void)
{
}

void StickTire::step(t_moa_real a_dt)
{

	m_force = t_moa_v3(0.0,0.0,0.0);
	m_moment = t_moa_v3(0.0,0.0,0.0);

	t_moa_real deflection = m_radius - m_contact_radius;

#if FE_GS_EVAL_GLOBAL_VAR
	gs_eval_global_var["LT Vx"] = m_velocity[0];
	gs_eval_global_var["LT Vy"] = m_velocity[1];
	gs_eval_global_var["LT Vz"] = m_velocity[2];
#endif

	if(deflection <= 0.0)
	{
		m_contact_stick = false;
		return;
	}

	t_moa_v3 F(0,0,0);

	m_force[2] += deflection*m_z_stiffness;
	m_force[2] -= m_velocity[2]*m_z_damping;

	t_moa_real stick_Fmax = m_Cf * m_force[2] * m_stick_bias;
	t_moa_real slip_Fmax = m_Cf * m_force[2] * (1.0 - m_stick_bias);
	if(stick_Fmax < 0.0) { stick_Fmax = 0.0; }
	if(slip_Fmax < 0.0) { slip_Fmax = 0.0; }

	if(m_contact_stick)
	{
		t_moa_v3 ctc_stick = m_ctc_stick.translation();

#if FE_GS_EVAL_GLOBAL_VAR
		// world space step contact for contact vel
		//ctc_stick -= m_asContactLive.velocity(m_r_tire) * a_dt;
		gs_eval_global_var["LT V0"] = m_asContactLive.velocity(m_r_tire)[0] / 100.0;
		gs_eval_global_var["LT V1"] = m_asContactLive.velocity(m_r_tire)[1] / 100.0;
#endif

		ctc_stick = transformVector<3,t_moa_real>(
			m_asTireLive.inv_transform(m_r_tire), ctc_stick);
		ctc_stick = rotateVector<3,t_moa_real>(
			m_asTireLive.inv_patch(m_r_tire), ctc_stick);

		ctc_stick -= m_velocity * a_dt;

		t_moa_v3 velocity = -m_velocity;
		velocity[0] += m_angular_velocity[1] * m_radius;

		ctc_stick[0] += m_angular_velocity[1] * m_contact_radius * a_dt;

		// current contact
		t_moa_v3 now_ctc = transformVector<3,t_moa_real>(
			m_asTireLive.inv_transform(m_r_tire),
			m_asContactLive.contact(m_r_tire));
		now_ctc = transformVector<3,t_moa_real>(
			m_asTireLive.inv_patch(m_r_tire),
			now_ctc);

		t_moa_v3 stretch = now_ctc - ctc_stick;

		F = stretch * m_stick_stiffness;

		t_moa_real Fm = magnitude(F);
		if(Fm > stick_Fmax)
		{
			t_moa_real scale = stick_Fmax / Fm;
			F *= scale;
			ctc_stick = now_ctc - stretch*scale;
		}

		t_moa_v3 Ff = -velocity * m_slip_stiffness;
		t_moa_real Ffm = magnitude(Ff);
		if(Ffm > slip_Fmax)
		{
			t_moa_real scale = slip_Fmax / Ffm;
			Ff *= scale;
		}

		F += Ff;

		m_force[0] -= F[0];
		m_force[1] -= F[1];

		F = rotateVector<3,t_moa_real>(m_asTireLive.patch(m_r_tire), F);

		ctc_stick = transformVector<3,t_moa_real>(
			m_asTireLive.patch(m_r_tire), ctc_stick);
		ctc_stick = transformVector<3,t_moa_real>(
			m_asTireLive.transform(m_r_tire), ctc_stick);
		m_ctc_stick.translation() = ctc_stick;
	}
	else
	{
		m_ctc_stick = m_asContactLive.transform(m_r_tire);
		m_contact_stick = true;
	}
}

void StickTire::setVelocity(const t_moa_v3 &a_velocity)
{
	m_velocity = a_velocity;
}

void StickTire::setAngularVelocity(const t_moa_v3 &a_ang_velocity)
{
	m_angular_velocity = a_ang_velocity;
}

void StickTire::setContact(const t_moa_real a_radius,
	const t_moa_real a_inclination)
{
	m_contact_radius = a_radius;
	m_inclination = a_inclination;
}

const t_moa_v3 &StickTire::getForce(void)
{
	return m_force;
}

const t_moa_v3 &StickTire::getMoment(void)
{
	return m_moment;
}

const t_moa_v3 &StickTire::getVelocity(void)
{
	return m_velocity;
}

const t_moa_v3	&StickTire::getAngularVelocity(void)
{
	return m_angular_velocity;
}

bool StickTire::compile(Record r_tire,
	Record r_config,fe::sp<fe::RecordGroup> a_rg_dataset)
{
	AsStickTireModel as(r_config);
	if(!as.check(r_config))
	{
		fe_fprintf(stderr, "StickTire::compile: failed to read config");
		return false;
	}

	m_r_tire = r_tire;

	m_asTireLive.bind(r_tire);
	m_asContactLive.bind(r_tire);

	m_radius = m_asTireLive.radius(r_tire);
	m_width = m_asTireLive.width(r_tire);
	m_contact_stick = false;
	setIdentity(m_ctc_stick);

	m_z_stiffness = as.stiffness(r_config);
	m_z_damping = as.damping(r_config);

	m_Cf = as.friction(r_config);
	m_stick_bias = as.bias(r_config);
	m_stick_stiffness = as.stick_stiffness(r_config);
	m_slip_stiffness = as.slip_stiffness(r_config);

	return true;
}

} /* namespace ext */
} /* namespace fe */
