/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "tire/tire.pmh"

//#define FE_BRUSH_TBB
#ifdef FE_BRUSH_TBB
#include "tbb/parallel_for.h"
#include "tbb/blocked_range.h"
#include "tbb/task_scheduler_init.h"
#endif

//#define FE_BRUSH_STDTHREAD
#ifdef FE_BRUSH_STDTHREAD
#include <thread>
#endif

namespace fe
{
namespace ext
{

t_moa_real GetRelativeStiffness(t_moa_real fLoad, t_moa_real fInitialScale,
	t_moa_real fDesignLoad)
{
	t_moa_real fDesignIPF = 0.4;
	t_moa_real fDesignRelation = 2.0 * (fDesignIPF - 0.50 *
		(fDesignIPF*fDesignIPF));
	t_moa_real fSpan = (1.0 - fInitialScale) / fDesignRelation;
	t_moa_real fX = clamp<t_moa_real>(fDesignRelation * fLoad /
		fDesignLoad, 0.0, 1.0);
	t_moa_real fRelativeStiffness = fInitialScale + fSpan *
		(fX - 0.5 * (fX*fX));
	return fRelativeStiffness;
}

BrushTireSystem::BrushTireSystem(void)
{
}

BrushTireSystem::~BrushTireSystem(void)
{
}

void BrushTireSystem::initialize(void)
{
}

#ifdef FE_BRUSH_TBB
class ApplyBrushStep
{
	public:
		ApplyBrushStep(sp<BrushTireSystem> spTireSystem, t_moa_real a_dt)
		{
			m_spTireSystem = spTireSystem;
			m_dt = a_dt;
		}
		void operator()( const tbb::blocked_range<size_t>& r ) const
		{
			for(size_t i = r.begin(); i!=r.end(); ++i )
			{
				m_spTireSystem->step(m_dt, i);
			}
		}

		sp<BrushTireSystem> m_spTireSystem;
		t_moa_real m_dt;
};
#endif

#ifdef FE_BRUSH_STDTHREAD
class BrushApplier
{
	public:
		BrushApplier(sp<BrushTireSystem> spTireSystem, t_moa_real a_dt,
			unsigned int a_tire_id)
		{
			m_spTireSystem = spTireSystem;
			m_dt = a_dt;
			m_tire_id = a_tire_id;
		}
		void operator()( void ) const
		{
			m_spTireSystem->step(m_dt, m_tire_id);
		}

		sp<BrushTireSystem> m_spTireSystem;
		//sp<BrushTire> m_tire;
		unsigned int m_tire_id;
		t_moa_real m_dt;
};
#endif


void BrushTireSystem::step(t_moa_real a_dt)
{
	m_asTireLive.bind(m_spScope);


#ifdef FE_BRUSH_TBB
	tbb::task_scheduler_init init();
	tbb::parallel_for(tbb::blocked_range<size_t>(0,m_r_tires.size(),1),
		ApplyBrushStep(sp<BrushTireSystem>(this),a_dt),
			tbb::simple_partitioner());
#else
#ifdef FE_BRUSH_STDTHREAD
	// I'm not sold on for_each as being clearer for this sort of thing
	std::vector<std::thread> threads;
	for(unsigned int i_tire = 0; i_tire < m_r_tires.size(); i_tire++)
	{
		//threads.push_back( std::thread( BrushApplier(sp<BrushTireSystem>(this),
		//	a_dt,m_asTireLive.spTireI(m_r_tires[i_tire])) ) );
		threads.push_back( std::thread( BrushApplier(sp<BrushTireSystem>(this),
			a_dt,i_tire) ) );
	}

	for(unsigned int i_thread = 0; i_thread < threads.size(); i_thread++)
	{
		threads[i_thread].join();
	}
#else
	for(unsigned int i_tire = 0; i_tire < m_r_tires.size(); i_tire++)
	{
		step(a_dt, i_tire);
	}
#endif
#endif

#if 0
#pragma omp parallel num_threads(16)
{
#pragma omp for
	for(unsigned int i_tire = 0; i_tire < m_tires.size(); i_tire++)
	{
		sp<BrushTire> &spTire = m_tires[i_tire];
		WeakRecord &r_parent = spTire->parent();

		// bridging data to solver:
		t_moa_real radius = asTireLive.contact_radius(r_parent);
		spTire->setVelocity(asTireLive.velocity(r_parent));
		spTire->setAngularVelocity(asTireLive.angular_velocity(r_parent));
		spTire->setContact(radius, asTireLive.inclination(r_parent));

		spTire->step(a_dt);

		// reflecting solve to data:
		asTireLive.force(r_parent) = spTire->getForce();
		asTireLive.radius(r_parent) = spTire->getRadius();

		//ext::AsNamed asNamed;
		//asNamed.bind(r_parent);

		t_moa_v3 F = asTireLive.force(r_parent);
		fe_fprintf(stderr, "BT [%s] %d %6.2g | %6.2f %6.3f %6.2f | %d\n",
			asNamed.name(r_parent).c_str(), i_tire, 0.0, F[0], F[1], F[2],
			omp_get_thread_num());
		fe_fprintf(stderr, "BT %d %6.2g | %6.2f %6.3f %6.2f | %d\n", i_tire,
			0.0, F[0], F[1], F[2], omp_get_thread_num());
	}
}
#endif
}

#if 0
void BrushTireSystem::step(t_moa_real a_dt, unsigned int i_tire)
{
	sp<BrushTire> spTire = m_asTireLive.spTireI(m_r_tires[i_tire]);
	step(a_dt, spTire);
}
#endif

void BrushTireSystem::step(t_moa_real a_dt, unsigned int a_tire_id)
{
	AsTireLive &asTireLive = m_asTireLive;
	//Record &r_parent = spTire->parent();

	Record &r_tire = m_r_tires[a_tire_id];
	sp<TireI> spTire = m_asTireLive.spTireI(r_tire);

	// bridging data to solver:
	t_moa_real radius = asTireLive.contact_radius(r_tire);
	spTire->setVelocity(asTireLive.velocity(r_tire));
	spTire->setAngularVelocity(asTireLive.angular_velocity(r_tire));
	spTire->setContact(radius, asTireLive.inclination(r_tire));

	spTire->step(a_dt);

	// reflecting solve to data:
	asTireLive.force(r_tire) = spTire->getForce();
	asTireLive.moment(r_tire) = spTire->getMoment();
	asTireLive.radius(r_tire) = spTire->getRadius();

	//t_moa_v3 F = asTireLive.force(r_parent);
	//fe_fprintf(stderr, "BT %6.2g | %6.2f %6.3f %6.2f\n", 0.0, F[0], F[1], F[2]);
}

void BrushTireSystem::compile(const t_note_id &a_note_id)
{
	rg_dataset = m_rg_dataset;
	m_spScope = rg_dataset->scope();

	if(!m_spScope.isValid()) { return; }

	enforce<AsTire,AsTireLive>(rg_dataset);

	// Dictionary for all tire models available
	RecordDictionary<AsBrushTireModel> rd_models(rg_dataset);

	// Give tires what they need to run live
	//AsTireLive asTireLive(rg_dataset->scope());
	AsBrushTire asBrushTire(rg_dataset->scope());

	m_asTireLive.bind(m_spScope);

	// Find all matching tires
	std::vector<Record>	tires;
	asBrushTire.filter(tires, rg_dataset);

	for(unsigned int i_tire = 0; i_tire < tires.size(); i_tire++)
	{
		Record r_tire = tires[i_tire];
		Record r_brush = rd_models[asBrushTire.model(r_tire)];

		if(r_brush.isValid() && m_asTireLive.check(r_tire))
		{
			sp<BrushTire> spBT(new BrushTire());

			if(spBT.isValid())
			{
				spBT->compile(r_tire, r_brush,rg_dataset);
				m_asTireLive.spTireI(r_tire) = spBT;
				m_r_tires.push_back(r_tire);
			}

		}
	}
}



BrushTire::BrushTire(void)
{
	initialize();
}

BrushTire::~BrushTire(void)
{
}

void BrushTire::initialize(void)
{
	m_radius = 0.55;
	m_width = 0.2;

	m_internal.fCCamberRelative = 0.1f;

	m_internal.fMyx = 1.33f;
	m_internal.fMyy = 1.32f;
	m_internal.fMySlide = 1.27f;

	m_internal.fCx = 30000.f;
	m_internal.fCy = 30000.f;
	m_internal.fCz = 5000.f;

	m_internal.fLateralViscoGrip = 0.f;
	m_internal.fLateralViscoSlope = 0.f;

	m_internal.fMaxSlideSpeedFade = 0.0f;
	m_internal.fMaxSlideSpeed = 100.f;

	m_Contact.fSinCamber = 0.f;
	m_Contact.Velocities.fSinSlipAngle = 0.f;
	m_Contact.Velocities.fCosSlipAngle = 1.f;
	m_Contact.Velocities.fHubVelocity = 0.f;
	m_Contact.Velocities.vHubVelocity_cf = t_moa_v2(0.0,0.0);
	m_Contact.Velocities.vTreadSlippage_cf = t_moa_v2(0.0,0.0);

	m_brakingStiffness = 235000.0;
	m_corneringStiffness = 195000.0;
	m_selfAligningStiffness = 15000.0;

	m_externalFactor = 1.0;

	m_internal.fCCamberRelative = 0.050000;
	m_deflectionStiffness = 312000.0;
	m_deflectionDamping = 500.0;
	m_latInitialLoadStiffnessScale = 1.0;
	m_longInitialLoadStiffnessScale = 1.0;
	m_designLoad = 0.0;
	m_slipSpeedFade[0] = 0.0;
	m_slipSpeedFade[1] = 100.0;
	m_sensSlope = -0.000003;
	m_finalSens = 0.650000;
	m_finalLoad = 5000.0;
	m_latGrip = 2.0;
	m_longGrip = 2.0;
	m_slideGrip = 2.02;
	m_tireHalfGripSpeed = 1000.5;

	m_loadSensitivity.Init(m_sensSlope, m_finalSens, m_finalLoad);

	m_mode = 1;

	m_force_filtered = t_moa_v3(0.0,0.0,0.0);
	m_force_v = t_moa_v3(0.0,0.0,0.0);

	// 10 m/s
	m_stiffness_filter = 20000.0;
	m_damping_filter = 200.0;

	// increased pressure
	m_damping_filter = 300.0;

}


void BrushTire::step(t_moa_real a_dt)
{
	m_force = t_moa_v3(0.0,0.0,0.0);
	m_moment = t_moa_v3(0.0,0.0,0.0);

	t_moa_real deflection = m_radius - m_contact_radius;

#if FE_GS_EVAL_GLOBAL_VAR
	gs_eval_global_var["LT Vx"] = m_velocity[0];
	gs_eval_global_var["LT Vy"] = m_velocity[1];
	gs_eval_global_var["LT Vz"] = m_velocity[2];
#endif

	if(deflection <= 0.0)
	{
		return;
	}

	// Tire as a simple spring
	m_force[2] += deflection*m_deflectionStiffness
		- m_velocity[2]*m_deflectionDamping;

	// grip adjustments
	t_moa_real grip = 1.0;

	// external
	grip *= m_externalFactor;

#if 0
	// load sensitivity
	t_moa_real loadSensitivity = m_loadSensitivity.Sensitivity( m_force[2] );
	grip *= loadSensitivity;
#endif

#if 0
	// speed sensitivity
	if (m_tireHalfGripSpeed > 0.0)
		grip *= (m_tireHalfGripSpeed / (m_tireHalfGripSpeed
			+ fabs(m_velocity[0])));
#endif

	// todo: optimal pressure

	// todo: optimial temperature

	m_internal.fMyx = (m_longGrip * grip);
	m_internal.fMyy = (m_latGrip * grip);
	m_internal.fMySlide = (m_slideGrip * grip);

	t_moa_real fLongLoadStiffnessScale = GetRelativeStiffness(m_force[2],
		m_longInitialLoadStiffnessScale, m_designLoad);
	t_moa_real fLatLoadStiffnessScale = GetRelativeStiffness(m_force[2],
		m_latInitialLoadStiffnessScale, m_designLoad);

	m_internal.fCx = m_brakingStiffness * fLongLoadStiffnessScale;
	m_internal.fCy = m_corneringStiffness * fLatLoadStiffnessScale;
	m_internal.fCz = 0.23f * m_selfAligningStiffness* fLatLoadStiffnessScale;

	m_internal.fMaxSlideSpeedFade =
		clamp<t_moa_real>(m_slipSpeedFade[0], 0.0, 1.0);
	m_internal.fMaxSlideSpeed = clampmin<t_moa_real>(m_slipSpeedFade[1],1.0);

	t_moa_real tireLongVel =
		m_velocity[0] - (m_angular_velocity[1] * (m_radius - 0.0*deflection));
	t_moa_real tireLatVel = m_velocity[1];

	// Contact

	t_moa_real groundSpeed = sqrt((m_velocity[0] * m_velocity[0])
		+ (m_velocity[1] * m_velocity[1]));

	m_Contact.Velocities.vTreadSlippage_cf = t_moa_v2((t_moa_real)tireLongVel,
		(t_moa_real)tireLatVel);
	m_Contact.Velocities.vHubVelocity_cf = t_moa_v2((t_moa_real)m_velocity[0],
		(t_moa_real)m_velocity[1]);

	m_Contact.Velocities.fHubVelocity = (t_moa_real)groundSpeed;
	if ( groundSpeed > 0.001)
	{
		m_Contact.Velocities.fSinSlipAngle =
			m_Contact.Velocities.vHubVelocity_cf[1] / (t_moa_real)groundSpeed;
		m_Contact.Velocities.fCosSlipAngle =
			m_Contact.Velocities.vHubVelocity_cf[0] / (t_moa_real)groundSpeed;
	}
	else
	{
		m_Contact.Velocities.fSinSlipAngle = 0.0;
		m_Contact.Velocities.fCosSlipAngle = 1.0;
	}

	m_Contact.fSinCamber = sin(m_inclination);

	t_moa_real pneumaticTrail, adhesiveWeight;
	t_moa_v3 force;

	GetForce(m_Contact, m_force[2], groundSpeed, &force, &pneumaticTrail,
		&adhesiveWeight);
	m_force[1] = force[1];
	m_force[0] = force[0];


#if 0
	if (m_do_stick && m_grip_point_valid && a_dt >= 0.0)
	{
		m_grip_point = inverseTransformVector(m_frame, m_grip_point_world);
		t_moa_real availforce = m_longGrip * grip * m_force[2];
		//m_grip_point[0] -= tireLongVel*a_dt;
//TODO:X switch back to the first version of tireLongVel
//-- just using this one for comparison -- maybe address other models
	//m_grip_point[0] += m_angular_velocity[1] * (m_radius - 0.5*deflection)* a_dt;
	m_grip_point[0] += m_angular_velocity[1] * (m_radius - 0.0*deflection) * a_dt;
	//m_grip_point[1] -= tireLatVel*a_dt;

		//t_moa_real diffPointLen = sqrt(m_grip_point[0]*m_grip_point[0]
		// + m_grip_point[1]*m_grip_point[1]);
		t_moa_real diffPointLen = magnitude(m_grip_point);
		t_moa_real slipSpeed
			=sqrt(tireLongVel*tireLongVel+tireLatVel*tireLatVel);

		// Compare current location to grip point
		if ((diffPointLen >= (2.0 * GRIP_HALF_DIFF))
			|| (slipSpeed >= GRIP_MAX_SPEED))
		//if ((slipSpeed >= GRIP_MAX_SPEED))
		//if(false)
		{
			// Too far or too fast to grip
			m_grip_point_valid = false;
		}
		else
		{
			// Get fraction of spring force we should apply
			t_moa_real spring = GRIP_SPRING *
				(1.0 - (slipSpeed / GRIP_MAX_SPEED)) *
				(1.0 - (groundSpeed / MIN_GROUNDSPEED));
			if (diffPointLen > GRIP_HALF_DIFF)
				spring *= (2.0 - (diffPointLen / GRIP_HALF_DIFF));

			t_moa_real damp = spring * 0.00;

			// Compute force in each direction
			t_moa_real latStatic = 1.0 * m_force[1]
				+ (m_grip_point[1] * spring) - tireLatVel*damp;
			t_moa_real longStatic;
			if(m_mode == 0)
			{
				longStatic = 0.2 * m_force[0]
					+ (m_grip_point[0] * spring) - tireLongVel*damp;
			}
			else
			{
				damp = spring * 0.1;
				longStatic = 1.0 * m_force[0] + (m_grip_point[0] * spring)
					- tireLongVel*damp;
			}
			t_moa_real sqrdStatic = (latStatic * latStatic)
				+ (longStatic * longStatic);
			if (sqrdStatic > (availforce * availforce))
			//if (false)
			{
				// Force is not possible, reset grip point
				m_grip_point_valid = false;
			}
			else
			{
				// Force is possible with current grip, so use it
				m_force[1] = latStatic;
				m_force[0] = longStatic;
			}
		}
	}
#endif


#if 0
if(false)
{
FILE *fp = fopen("brush_step.txt", "a");
fe_fprintf(fp, "m_grip_point %f %f dt %f F %f %f\n",
	m_grip_point[0], m_grip_point[1], a_dt, m_force[0], m_force[1]);
fclose(fp);
}
#endif

#if 0
	t_moa_v3 delta_filter_v = (m_force - m_force_filtered);
	t_moa_real delta_filter_m = magnitude(delta_filter_v);
	t_moa_real filter_f = delta_filter_m * m_stiffness_filter;

	for(unsigned k = 0; k < 100; k++)
	{
		t_moa_real filter_dv = filter_f * a_dt / 100.0;

		if(delta_filter_m > 0.00001)
		{
			m_force_v += filter_dv * delta_filter_v / delta_filter_m;

			m_force_v -= m_damping_filter * a_dt * m_force_v / 100.0;

			m_force_filtered += m_force_v * a_dt / 100.0;
		}
	}

#endif

	//t_moa_real max_rate = 0.01/a_dt; // 2 m/s
	//t_moa_real max_rate = 0.075/a_dt; // 5 m/s
	//t_moa_real max_rate = 0.15/a_dt; // 10 m/s
	//t_moa_real max_rate = 4.0 * 0.15/a_dt; // 40 m/s
	//

	//max_rate = groundSpeed*0.015*1.0e6 * a_dt / 7000.0;

#if 0
	t_moa_real delta_filter_v0 = (m_force[0] - m_force_filtered[0])/m_force[2];
	if(fabs(delta_filter_v0) > max_rate)
	{
		t_moa_real scale = max_rate / fabs(delta_filter_v0);
		delta_filter_v0 *= scale;
	}

	t_moa_real delta_filter_v1 = (m_force[1] - m_force_filtered[1])/m_force[2];
	if(fabs(delta_filter_v1) > max_rate)
	{
		t_moa_real scale = max_rate / fabs(delta_filter_v1);
		delta_filter_v1 *= scale;
	}

	m_force_filtered[0] += delta_filter_v0 * m_force[2];
	m_force_filtered[1] += delta_filter_v1 * m_force[2];
	m_force_filtered[2] = m_force[2];
#endif

	//m_moment[2] = -pneumaticTrail*m_force_filtered[1];
	if((m_velocity[0] >= 0.0))
	{
		m_moment[2] = -pneumaticTrail*m_force[1];
	}
	else
	{
		m_moment[2] = pneumaticTrail*m_force[1];
	}


	t_moa_real z = -m_contact_radius;
	m_moment[1] += +z * m_force[0];
	m_moment[0] += -z * m_force[1];

#if FE_GS_EVAL_GLOBAL_VAR
	gs_eval_global_var["BT Fx"] = m_force[0];
	gs_eval_global_var["BT Fy"] = m_force[1];
	gs_eval_global_var["BT Fz"] = m_force[2];
	gs_eval_global_var["BT Mz"] = m_moment[2];
#endif

#ifdef FE_ENABLE_CHANNEL_LOGGING
	t_moa_v2 tv2(m_et, m_force[1]);
	m_log->log(tv2);
	m_et += a_dt;
#endif
}

t_moa_real	GetNormalizedSlip(t_moa_real fLambda, t_moa_real fLambdaSlide,
	t_moa_real fTanAlfa, t_moa_real fTanAlfaSlide)
{
	t_moa_real f1mLamba = 1.0 - fLambda;
	if (f1mLamba < 0.000010)
	{
		return 1.0;
	}

	t_moa_real fA = fLambda * (f1mLamba) /(f1mLamba*fLambdaSlide);
	t_moa_real fB = fTanAlfa / (f1mLamba*fTanAlfaSlide);

	t_moa_real fSq = fA*fA + fB*fB;

	if (fSq < 1.0)
	{
		return sqrt(fSq);
	}
	else
	{
		return 1.0;
	}
}

t_moa_real BrushTire::GetTreadSlideGripFactor(const t_contact& rContact,
	t_moa_real fLambda) const
{
	t_moa_real fGripFactor = 1.0;

	if (m_internal.fMaxSlideSpeedFade > 0.0 && fLambda < 0.0)
	{
		t_moa_real fGripFade = m_internal.fMaxSlideSpeedFade;
		t_moa_real fMaxVel = m_internal.fMaxSlideSpeed;
		const t_contact::t_velocities& rVelocities = rContact.Velocities;

		t_moa_real fIPF =
			clampmax<t_moa_real>( fabs(rVelocities.vTreadSlippage_cf[0])
				/ fMaxVel, 1.0 );

		fGripFactor = (1.0 - fGripFade) + fGripFade * sqrt(fIPF - 1.0);
	}

	return fGripFactor;

}

void  BrushTire::GetForce(const t_contact& rContact,
	const t_moa_real fNormalForce, const t_moa_real fGroundSpeed,
	t_moa_v3* pvForce_cf,
	t_moa_real* pfPneumaticTrail, t_moa_real* pfAdhesiveWeight)
{
	t_moa_real fHighSpeedThrehold = 1.0;
	t_moa_real fLowSpeedThreshold = 0.5;
#if 0
	if(m_mode == 1)
	{
		GetHighSpeedForce(rContact,fNormalForce,
			pvForce_cf,pfPneumaticTrail,pfAdhesiveWeight);
//if(fGroundSpeed <= fHighSpeedThrehold)
{
//fe_fprintf(stderr, "B fGrdSpd %f pvF_cf %f %f %f  pfAdhW %f  pfPnecTrail %f\n",
	fGroundSpeed, (*pvForce_cf)[0], (*pvForce_cf)[1], (*pvForce_cf)[2],
	*pfAdhesiveWeight, *pfPneumaticTrail);
}
		return;
	}
#endif
//GetHighSpeedForce(rContact,fNormalForce,
	//pvForce_cf,pfPneumaticTrail,pfAdhesiveWeight);
//GetLowSpeedForce(rContact,fNormalForce, pvForce_cf,pfAdhesiveWeight);
//return;
	if ( fGroundSpeed > fHighSpeedThrehold )
	{
		GetHighSpeedForce(rContact,fNormalForce, pvForce_cf,pfPneumaticTrail,
			pfAdhesiveWeight);
	}
	else if (fGroundSpeed < fLowSpeedThreshold)
	{
		GetLowSpeedForce(rContact,fNormalForce, pvForce_cf,pfAdhesiveWeight);
		*pfPneumaticTrail = 0.f;
	}
	else
	{
		t_moa_v3 vHighSpeedForce_cf;
		t_moa_v3 vLowSpeedForce_cf;
		t_moa_real fHighSpeedAdhesiveWeight;
		t_moa_real fLowSpeedAdhesiveWeight;
		t_moa_real fHighSpeedPneumaticTrail;

		t_moa_real fHighWeight = (fGroundSpeed - fLowSpeedThreshold)
			/ (fHighSpeedThrehold - fLowSpeedThreshold);
		t_moa_real fLowWeight = 1.f - fHighWeight;

		GetHighSpeedForce(rContact,fNormalForce, &vHighSpeedForce_cf,
			&fHighSpeedPneumaticTrail,&fHighSpeedAdhesiveWeight);
		GetLowSpeedForce(rContact,fNormalForce, &vLowSpeedForce_cf,
			&fLowSpeedAdhesiveWeight);

		*pvForce_cf = fLowWeight * vLowSpeedForce_cf
			+ fHighWeight * vHighSpeedForce_cf;
		*pfAdhesiveWeight = fLowWeight * fLowSpeedAdhesiveWeight
			+ fHighWeight * fHighSpeedAdhesiveWeight;
		*pfPneumaticTrail =
			fHighWeight * fHighSpeedPneumaticTrail; // + fLowWeight * 0.f
	}
}

void BrushTire::GetHighSpeedForce(const t_contact& rContact,
	const t_moa_real fNormalForce, t_moa_v3* pvForce_cf,
	t_moa_real* pfPneumaticTrail, t_moa_real* pfAdhesiveWeight)
{
	*pvForce_cf = t_moa_v3(0.0,0.0,0.0);

	const t_contact::t_velocities& rVelocities = rContact.Velocities;
	t_moa_real fMyx = m_internal.fMyx;
	t_moa_real fMyy = m_internal.fMyy;
	t_moa_real fMySlide = m_internal.fMySlide;

	//Carcass stiffness
	t_moa_real fCx = m_internal.fCx;	//Braking stiffness
	t_moa_real fCy = m_internal.fCy;	//Cornering stiffness;
	t_moa_real fCz = m_internal.fCz;	//Self Alignment stiffness

	//Adhesive region
	t_moa_real fFax = 0.0;		//adhesive lateral force
	t_moa_real fFay = 0.0;		//adhesive long force
	t_moa_real fTrailAdhesion = 0.0;

	//Slippage relation
	t_moa_real fNormalizedSlip;
	t_moa_real fAdhesionWeight;

	t_moa_real fLambda = -1.0;
	t_moa_real fCamberEffect = 0.0;
	if (fabs(rVelocities.vHubVelocity_cf[0]) > 0.0001)
	{
		fLambda = (rVelocities.vTreadSlippage_cf[0]
			/ rVelocities.vHubVelocity_cf[0]);
		fCamberEffect = clamp(fLambda + 1.0,0.0, 1.0);
	}

	//fe_fprintf(stderr, "BRUSH fLambda %f\n", fLambda);
	fMySlide *= GetTreadSlideGripFactor(rContact, fLambda);

	//Adjusting slip angle due to camber
	t_moa_real fCCamberRel = fCamberEffect * m_internal.fCCamberRelative *
		(1.0 - rContact.fSinCamber*rContact.fSinCamber);
	t_moa_real fSinSlipAngle = rVelocities.fSinSlipAngle + fCCamberRel *
		rContact.fSinCamber
			* (1.0 - rVelocities.fSinSlipAngle*rVelocities.fSinSlipAngle);
	fSinSlipAngle = clamp<t_moa_real>(fSinSlipAngle,-1.0, 1.0);
	t_moa_real fCosSlipAngle = sqrt(1.0 - fSinSlipAngle*fSinSlipAngle) *
		sign(rVelocities.fCosSlipAngle);
	t_moa_real fTreadSlippageYAdjusted = rVelocities.fHubVelocity
		* fSinSlipAngle;

	t_moa_real fTanAlfa = 0.0;
	if ( fabs(fCosSlipAngle) > 0.0001)
	{
		fTanAlfa = fSinSlipAngle / fCosSlipAngle;

		//fe_fprintf(stderr, "BRUSH fTanAlfa %f\n", fTanAlfa);
		t_moa_real fFxMax = fMyx * fNormalForce;
		t_moa_real fFyMax = fMyy * fNormalForce;

		t_moa_real fLambdaSlide = (3.0*fFxMax)/(fCx + 3.0*fFxMax);
		t_moa_real fTanAlfaSlide = 3.0*fFyMax/fCy;
//fe_fprintf(stderr, "B fLambide %f fTanAlide %f\n", fLambdaSlide, fTanAlfaSlide);

		fNormalizedSlip = GetNormalizedSlip(fLambda, fLambdaSlide, fTanAlfa,
			fTanAlfaSlide);
		t_moa_real tmp = 1.0-fNormalizedSlip;
		fAdhesionWeight = tmp*tmp;

		if (fAdhesionWeight > 0.00001)
		{
			fFax = -fCx * fLambda  * fAdhesionWeight /
				(1.0 - fLambda) * sign(rVelocities.vHubVelocity_cf[0]);
			fFay = -fCy * fTanAlfa * fAdhesionWeight /
				(1.0 - fLambda) * sign(rVelocities.vHubVelocity_cf[0]);
		}
		//fe_fprintf(stderr, "BRUSH fFax %f fFay %f\n", fFax, fFay);

		fTrailAdhesion = (fCz/fCy) * (4.0 * fNormalizedSlip - 1.0);
	}
	else
	{
		fFax = 0.0;
		fFay = 0.0;
		fNormalizedSlip = 1.0;	//Pure sliding

		fAdhesionWeight = 0.0;
	}

	//scalar(e_slide) = fNormalizedSlip;

	//Sliding Region

	//Normal force for slipping region
	t_moa_real fFsz = fNormalForce * fNormalizedSlip*fNormalizedSlip
		* (3.0-2.0*fNormalizedSlip);

	t_moa_real fPatchSlipVelocity =
		sqrt( (rVelocities.vTreadSlippage_cf[0]
			*rVelocities.vTreadSlippage_cf[0]) +
		(fTreadSlippageYAdjusted*fTreadSlippageYAdjusted));

	t_moa_real fFsx, fFsy, fTrailSliding;
	t_moa_real thresholdPatchSlipVelocity;
	if(m_mode == 1)
	{
		thresholdPatchSlipVelocity = 0.0001;
	}
	else
	{
		thresholdPatchSlipVelocity = 1.0;
	}
	if(fPatchSlipVelocity < thresholdPatchSlipVelocity)
	{
		fFsx = 0.0;
		fFsy = 0.0;
		fTrailSliding = 0.0;
	}
	else
	{

		t_moa_real fCosB = (rVelocities.vTreadSlippage_cf[0])
			/ fPatchSlipVelocity;
		t_moa_real fSinB = (fTreadSlippageYAdjusted) / fPatchSlipVelocity;

		fFsx = -fCosB * fFsz * fMySlide;
		fFsy = -fSinB * fFsz * fMySlide;

		fTrailSliding = -9.0*(fCz/fCy) * fAdhesionWeight
			/ (3.0-2.0*fNormalizedSlip);
		//fe_fprintf(stderr, "BRUSH fPatchSlipVelocity %f\n", fPatchSlipVelocity);
		//fe_fprintf(stderr, "BRUSH fCosB %f\n", fCosB);
		//fe_fprintf(stderr, "BRUSH fSinB %f\n", fSinB);
	}

	//Combining adhesive and sliding forces
	(*pvForce_cf)[0] = fFax + fFsx;
	(*pvForce_cf)[1] = fFay + fFsy;

	*pfPneumaticTrail = 0.0;
	if (fabs((*pvForce_cf)[1]) > 0.001)
	{
		*pfPneumaticTrail = -(fFay * fTrailAdhesion + fFsy * fTrailSliding)
			/ (*pvForce_cf)[1];
	}

	*pfAdhesiveWeight = fAdhesionWeight;
}

void  BrushTire::GetLowSpeedForce(const t_contact& rContact,
		const t_moa_real fNormalForce, t_moa_v3* pvForce_cf,
		t_moa_real* pfAdhesiveWeight) const
{
	t_moa_real fStiffness = 0.5;

	t_moa_real fSlipVelocity= magnitude(rContact.Velocities.vTreadSlippage_cf);

	t_moa_v3 vNormalizedSlip(
		rContact.Velocities.vTreadSlippage_cf[0],
		rContact.Velocities.vTreadSlippage_cf[1],
		0.0);

	t_moa_real x = fSlipVelocity * fStiffness;
	t_moa_real fSlipFactor;
	if (x<1.0)
	{
		fSlipFactor = x*(2.f-x);
		vNormalizedSlip *= (fStiffness) * (2.f-x);
	}
	else
	{
		fSlipFactor = 1.0;
		vNormalizedSlip *= (1.0/fSlipVelocity);
	}

	*pvForce_cf = -(fNormalForce * m_internal.fMySlide) * vNormalizedSlip;
	*pfAdhesiveWeight = 1.0 - fSlipFactor;
}

#if 0
void	CHDSlipModel::GetViscousForce(
const t_contact&	rContact,
const t_moa_real					fNormalForce,
nsMaths::BVec3f*	pvForce_cf,
t_moa_real*			  pfEffect) const
{
t_moa_real fMyy = m_internal.fLateralViscoGrip;
pvForce_cf->Clear();
t_moa_real fVelocityIPF
	= Base::Clamp(
		nsMaths::FAbs32(rContact.Velocities.vTreadSlippage_cf.y)
			/ 7.f, 0.f, 1.f);
fVelocityIPF = 2.f * fVelocityIPF - nsMaths::Sq32(fVelocityIPF);

t_moa_real fAngleIPF = Base::Clamp( -m_internal.fLateralViscoSlope
	* rContact.Velocities.fSinSlipAngle , -1.f, 1.f);

t_moa_real fIPF = fAngleIPF * fVelocityIPF;
pvForce_cf->y = fIPF * fMyy * fNormalForce;
*pfEffect = nsMaths::FAbs32(fIPF);
}
#endif

#if 0
void  CHDSlipModel::GetForceGradient( const t_contact&	  rContact,
const f32					fNormalForce,
const f32		  fGroundSpeed,
nsMaths::BVec3f*	pvForceGradient_cf,
f32*							pfPneumaticTrailGradient) const
{
f32 fHighSpeedThrehold = 1.f;
if ( fGroundSpeed > fHighSpeedThrehold )
{
nsMaths::BVec3f vCurForce_cf;
f32 fCurPneumaticTrail = 0.f;
f32 fCurAdhesiveWeight;
GetHighSpeedForce(rContact,fNormalForce, &vCurForce_cf, &fCurPneumaticTrail,
	&fCurAdhesiveWeight);

f32 fOffsetAngle = nsMaths::DegToRad32(2.f);
if (rContact.fSinCamber > 0.f) { fOffsetAngle = -fOffsetAngle; }
t_contact OffsetContact = rContact;
GetOffsetVelocities(rContact.Velocities, nsMaths::Sin32(fOffsetAngle),
	OffsetContact.Velocities);

nsMaths::BVec3f vOffsetForce_cf;
f32 fOffsetPneumaticTrail = 0.f;
f32 fOffsetAdhesiveWeight;
GetHighSpeedForce(OffsetContact,fNormalForce, &vOffsetForce_cf,
	&fOffsetPneumaticTrail, &fOffsetAdhesiveWeight);
f32 fInvOffsetAngle = 1.f / fOffsetAngle;
*pvForceGradient_cf = fInvOffsetAngle * (vOffsetForce_cf - vCurForce_cf);
*pfPneumaticTrailGradient = fInvOffsetAngle * (fOffsetPneumaticTrail
	- fCurPneumaticTrail);
}
else
{
pvForceGradient_cf->Clear();
*pfPneumaticTrailGradient = 0.f;
}
}
#endif



void BrushTire::setGeometry(t_moa_real a_radius, t_moa_real a_width)
{
	m_radius = a_radius;
	m_width = a_width;
}

void BrushTire::setVelocity(const t_moa_v3 &a_velocity)
{
	m_velocity = a_velocity;
}

void BrushTire::setAngularVelocity(const t_moa_v3 &a_ang_velocity)
{
	m_angular_velocity = a_ang_velocity;
}

void BrushTire::setContact(const t_moa_real a_radius,
	const t_moa_real a_inclination)
{
	m_contact_radius = a_radius;
	m_inclination = a_inclination;
}

const t_moa_v3 &BrushTire::getForce(void)
{
	return m_force;
}

const t_moa_v3 &BrushTire::getMoment(void)
{
	return m_moment;
}

const t_moa_v3 &BrushTire::getVelocity(void)
{
	return m_velocity;
}

const t_moa_v3	&BrushTire::getAngularVelocity(void)
{
	return m_angular_velocity;
}


void BrushTire::printf(FILE *fp)
{
}

bool BrushTire::compile(Record r_tire, Record r_config,fe::sp<fe::RecordGroup> a_rg_dataset)
{
	m_r_parent = r_tire;

	AsBrushTireModel as(r_config);
	if(!as.check(r_config))
	{
		fe_fprintf(stderr, "BrushTire::read: failed to read config");
		return false;
	}

	AsTireLive asTireLive(r_tire);
	setGeometry(asTireLive.radius(r_tire), asTireLive.width(r_tire));

	m_internal.fCCamberRelative = as.camberStiffness(r_config);
	m_deflectionStiffness = as.stiffness(r_config);
	m_deflectionDamping = as.damping(r_config);
	m_brakingStiffness = as.brakingStiffness(r_config);
	m_corneringStiffness = as.corneringStiffness(r_config);
	m_selfAligningStiffness = as.selfAligningStiffness(r_config);
	m_latInitialLoadStiffnessScale = as.latInitialLoadStiffnessScale(r_config);
	m_longInitialLoadStiffnessScale= as.longInitialLoadStiffnessScale(r_config);
	m_designLoad = as.designLoad(r_config);
	m_slipSpeedFade[0] = as.slipSpeedFade(r_config)[0];
	m_slipSpeedFade[1] = as.slipSpeedFade(r_config)[1];
	m_sensSlope = as.sensSlope(r_config);
	m_finalSens = as.finalSens(r_config);
	m_finalLoad = as.finalLoad(r_config);
	m_latGrip = as.latGrip(r_config);
	m_longGrip = as.longGrip(r_config);
	m_slideGrip = as.slideGrip(r_config);
	m_tireHalfGripSpeed = as.tireHalfGripSpeed(r_config);

	m_loadSensitivity.Init(m_sensSlope, m_finalSens, m_finalLoad);

	return true;
}

template <class T> void TemporaryConstrainValue(T &value, T minimum, T maximum)
{
	if (value < minimum)
		value = minimum;
	else if (value > maximum)
		value = maximum;
}

inline void GaussianSwap( t_moa_real *r1,
	t_moa_real *r2, unsigned int i, unsigned int terms )
{
	t_moa_real swap;

	for (/* i passed in */; i <= terms; i++)
	{
		swap = r1[i];
		r1[i] = r2[i];
		r2[i] = swap;
	}
}


inline void GaussianScale( t_moa_real *r1, t_moa_real scale,
	unsigned int i, unsigned int terms )
{
	for (/* i passed in */; i <= terms; i++)
		r1[i] *= scale;
}


inline void GaussianAddMultiple( t_moa_real* const r1,
	const t_moa_real* const r2, const t_moa_real scale,
	const unsigned int i, const unsigned int terms )
{
	unsigned int j;
	for ( j = i /* i passed in */; j <= terms; j++)
	{
		if (r2[j] != 0.0)
		r1[j] += (r2[j] * scale);
	}
}


unsigned int GaussianElimination( t_moa_real **m, unsigned int terms )
{
	unsigned int i, j;

	// Indexing into matrix is m[row][column]
	for (i = 0; i < terms; i++)
	{
		// First guarantee a non-zero number in the i-th column
		if (m[i][i] == 0.0)
		{
			for (j = (i + 1); j < terms; j++)
			{
				if (m[j][i] != 0.0)
				break;
			}

			// Unsolvable if the rest of column is zeroes
			if (j == terms)
				return (-1);

			// Swap in the non-zero number
			GaussianSwap (m[i], m[j], i, terms);
		}

		// Now scale so number is 1.0
		if (m[i][i] != 1.0)
		{
			GaussianScale (m[i], 1.0 / m[i][i], i + 1, terms);
		}

		// Now make every other entry in column equal to 0.0
		for (j = 0; j < terms; j++)
		{
			if ((i != j) && (m[j][i] != 0.0))
			{
				GaussianAddMultiple (m[j], m[i], -m[j][i], i + 1, terms);
			}
		}
	}

	return (0);
}

void TireLoadSens::Init( t_moa_real zeroSlope, t_moa_real finalSens,
	t_moa_real finalLoad )
{
	// Error-checking
	if (zeroSlope > 0.0) zeroSlope = -zeroSlope;
	TemporaryConstrainValue<t_moa_real>( finalSens, 0.0, 1.0 );

	// Copy final load as max
	mMaxLoad = finalLoad;

	// Setup for Gaussian solver below
	t_moa_real eqn[4][5];
	t_moa_real *ptrEqn[4];
	for (unsigned int j = 0; j < 4; j++)
	ptrEqn[j] = eqn[j];

	// Start at value 1.0 at zero load
	eqn[0][0] = 0.0;
	eqn[0][1] = 0.0;
	eqn[0][2] = 0.0;
	eqn[0][3] = 1.0;
	eqn[0][4] = 1.0;

	// Start with a slope of zeroSlope
	eqn[1][0] = 0.0;
	eqn[1][1] = 0.0;
	eqn[1][2] = 1.0;
	eqn[1][3] = 0.0;
	eqn[1][4] = (t_moa_real)zeroSlope;

	// End at value finalSens and finalLoad load
	eqn[2][0] = (t_moa_real)(finalLoad * finalLoad * finalLoad);
	eqn[2][1] = (t_moa_real)(finalLoad * finalLoad);
	eqn[2][2] = (t_moa_real)finalLoad;
	eqn[2][3] = 1.0;
	eqn[2][4] = (t_moa_real)finalSens;

	// End with a slope of zero (meaning flat, not zeroSlope) at finalLoad load
	eqn[3][0] = (t_moa_real)(3.0 * finalLoad * finalLoad);
	eqn[3][1] = (t_moa_real)(2.0 * finalLoad);
	eqn[3][2] = 1.0;
	eqn[3][3] = 0.0;
	eqn[3][4] = 0.0;

	// Solve
	if (GaussianElimination( ptrEqn, 4 ) == 0)
	{
		// Fill in cubic parameters
		mA = eqn[0][4];
		mB = eqn[1][4];
		mC = eqn[2][4];
		mD = eqn[3][4];
	}
	else
	{
		// Um, just return 1.0 all the time
		mA = 0.0;
		mB = 0.0;
		mC = 0.0;
		mD = 1.0;
	}
}

void BrushTire::setExternalFactor(const t_moa_real	a_factor)
{
	m_externalFactor = a_factor;
}

void BrushTire::setFrame(const t_moa_v3 &a_pos, const t_moa_v3 &a_x,
	const t_moa_v3 &a_y, const t_moa_v3 &a_z)
{
	m_frame.direction() = a_x;
	m_frame.left() = a_y;
	m_frame.up() = a_z;
	m_frame.translation() = a_pos;
}

} /* namespace ext */
} /* namespace fe */

