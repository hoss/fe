/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __tire_ParticleMountSystem_h__
#define __tire_ParticleMountSystem_h__

#include "solve/solve.h"

namespace fe
{
namespace ext
{

/// @brief Tire to Particles Connection
class ParticleMountSystem :
	virtual public Stepper,
	public Initialize<ParticleMountSystem>
{
	public:
		struct Mount
		{
			WeakRecord					m_r_parent;
			WeakRecord					m_r_p0;
			WeakRecord					m_r_p1;
			WeakRecord					m_r_p2;
			WeakRecord					m_r_tire;
			t_moa_xform						m_inv_R;
		};
		ParticleMountSystem(void)
		{
		}
		void	initialize(void) {}
virtual	void	compile(const t_note_id &a_note_id)
		{
			sp<Scope> spScope = m_rg_dataset->scope();

			if(!spScope.isValid()) { return; }

			enforce<AsTire,AsTireLive>(m_rg_dataset);

			m_asPoint.bind(spScope);
			m_asTireLive.bind(spScope);
			m_asMount.bind(spScope);

			AsTire asTire(spScope);

			RecordDictionary<AsTire> rd_tires(m_rg_dataset);

			std::vector<Record> mounts;
			m_asMount.filter(mounts, m_rg_dataset);

			for(unsigned int i_mount = 0; i_mount < mounts.size(); i_mount++)
			{
				Record &r_mount = mounts[i_mount];
				Mount mount;
				mount.m_r_parent = r_mount;
				mount.m_r_p0 = m_asMount.p0(r_mount);
				mount.m_r_p1 = m_asMount.p1(r_mount);
				mount.m_r_p2 = m_asMount.p2(r_mount);
				mount.m_r_tire = rd_tires[m_asMount.tire(r_mount)];
				if(	mount.m_r_p0.isValid() &&
					mount.m_r_p1.isValid() &&
					mount.m_r_p2.isValid() &&
					mount.m_r_tire.isValid() &&
					m_asPoint.check(mount.m_r_p0) &&
					m_asPoint.check(mount.m_r_p1) &&
					m_asPoint.check(mount.m_r_p2) &&
					m_asTireLive.check(mount.m_r_tire))
				{

					t_moa_xform frame;
					t_moa_xform relative;
					setIdentity(relative);

					bool frame_made = makeFrame(frame,
						m_asPoint.location(mount.m_r_p0),
						m_asPoint.location(mount.m_r_p1),
						m_asPoint.location(mount.m_r_p2),
						relative);

					invert(mount.m_inv_R, frame);

					translate(relative, m_asMount.location(r_mount));
					rotate(relative, m_asMount.inclination(r_mount),
						e_xAxis);

//rotate(relative, 0.44, e_yAxis);
//rotate(relative, -0.44, e_zAxis);

					mount.m_inv_R = relative * mount.m_inv_R;

					m_mounts.push_back(mount);
				}
			}

		}
		void	step(t_moa_real a_dt)
		{
			for(unsigned int i_mount = 0; i_mount < m_mounts.size(); i_mount++)
			{
				WeakRecord &r_p0 = m_mounts[i_mount].m_r_p0;
				WeakRecord &r_p1 = m_mounts[i_mount].m_r_p1;
				WeakRecord &r_p2 = m_mounts[i_mount].m_r_p2;
				WeakRecord &r_tire = m_mounts[i_mount].m_r_tire;
				WeakRecord &r_mount = m_mounts[i_mount].m_r_parent;

				t_moa_xform frame;
				t_moa_xform relative;
				setIdentity(relative);
				bool frame_made = makeFrame(frame,
					m_asPoint.location(r_p0),
					m_asPoint.location(r_p1),
					m_asPoint.location(r_p2),
					m_mounts[i_mount].m_inv_R);

				m_asTireLive.transform(r_tire) = frame;
				invert(m_asMount.transform_inv(r_mount),
					m_asTireLive.transform(r_tire));

				t_moa_v3 tire_space_p0_v =  rotateVector<3, t_moa_real>
					(m_asMount.transform_inv(r_mount),
					m_asPoint.velocity(r_p0));


				t_moa_v3 vel = tire_space_p0_v;

				m_asTireLive.velocity(r_tire) = vel;

//TODO: ang vel -- frame ang is purely reflection -- useful ang vel from driveline
//TODO: grip point TM
			}
			accumulate();
		}
		void	accumulate(void)
		{
			for(unsigned int i_mount = 0; i_mount < m_mounts.size(); i_mount++)
			{
				WeakRecord &r_p0 = m_mounts[i_mount].m_r_p0;
				WeakRecord &r_p1 = m_mounts[i_mount].m_r_p1;
				WeakRecord &r_p2 = m_mounts[i_mount].m_r_p2;
				WeakRecord &r_tire = m_mounts[i_mount].m_r_tire;

				t_moa_xform tire_xform = m_asTireLive.transform(r_tire);

				t_moa_v3 rig_space_F = rotateVector<3, t_moa_real>
					(tire_xform, m_asTireLive.force(r_tire));
				t_moa_v3 rig_space_M = rotateVector<3, t_moa_real>
					(tire_xform, m_asTireLive.moment(r_tire));

				t_moa_v3 rig_space_P = tire_xform.translation();

				applyFMToPoints(rig_space_F, rig_space_M, rig_space_P,
					m_asPoint, r_p0, r_p1, r_p2);
			}
		}

		std::vector<Mount>		m_mounts;
		AsForcePoint			m_asPoint;
		AsTireLive				m_asTireLive;
		AsParticlesMount		m_asMount;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __tire_ParticleMountSystem_h__ */

