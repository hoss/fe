/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "tire/tire.pmh"

namespace fe
{
namespace ext
{

TireISystem::TireISystem(void)
{
}

TireISystem::~TireISystem(void)
{
}

void TireISystem::initialize(void)
{
}

void TireISystem::connectOrchestrator(fe::sp<OrchestratorI> a_spOrchestrator)
{
	Stepper::connectOrchestrator(a_spOrchestrator);
}

#define moa_rotate rotateVector<3,t_moa_real>
#define moa_transform transformVector<3,t_moa_real>
void TireISystem::step(t_moa_real a_dt)
{
	for(unsigned int i_tire = 0; i_tire < m_r_tires.size(); i_tire++)
	{
		Record &r_tire = m_r_tires[i_tire];

		// bridging data to solver:
		t_moa_real radius = m_asTireLive.contact_radius(r_tire);

		// Summary:
		//	* go from tire space to patch space
		//	* step TireI (in patch space)
		//  * go from patch space to tire space

		// tire space add velocities
		invert(m_asTireLive.inv_transform(r_tire), m_asTireLive.transform(r_tire));
		const t_moa_xform &inv = m_asTireLive.inv_transform(r_tire);
		t_moa_v3 contact_V = fe::rotateVector<3, t_moa_real>(inv,
			m_asContactLive.velocity(r_tire));
		t_moa_v3 velocity = m_asTireLive.velocity(r_tire) + contact_V;

		setIdentity(m_asTireLive.inv_patch(r_tire));
		if(determinant(m_asTireLive.patch(r_tire)) != 0.0)
		{
			invert(m_asTireLive.inv_patch(r_tire), m_asTireLive.patch(r_tire));
			velocity = rotateVector<3,t_moa_real>(m_asTireLive.inv_patch(r_tire), velocity);
		}
		const t_moa_xform &p_inv = m_asTireLive.inv_patch(r_tire);

		// velocity here should now be patch/contact space

		sp<TireI> spTire = m_asTireLive.spTireI(r_tire);
		spTire->setVelocity(velocity);
		spTire->setAngularVelocity(m_asTireLive.angular_velocity(r_tire));
		spTire->setContact(radius,m_asTireLive.inclination(r_tire));

		// Patch Oriented TireI core
		spTire->step(a_dt);

		m_asTireLive.force(r_tire) = moa_rotate(m_asTireLive.patch(r_tire), spTire->getForce());
		m_asTireLive.moment(r_tire) = moa_rotate(m_asTireLive.patch(r_tire), spTire->getMoment());
		m_asTireLive.radius(r_tire) = spTire->getRadius();
	}
}


void TireISystem::compile(const t_note_id &a_note_id)
{
	m_spScope = m_rg_dataset->scope();

	if(!m_spScope.isValid()) { return; }

	enforce<AsTire,AsTireLive>(m_rg_dataset);
	enforce<AsTire,AsContactLive>(m_rg_dataset);

	// Dictionary for all tire models available
	RecordDictionary<AsTireModel> rd_models(m_rg_dataset);
	AsTireModel asTireModel;
	asTireModel.bind(m_spScope);

	// Give tires what they need to run live
	AsTireLive asTireLive(m_rg_dataset->scope());

	m_asTireLive.bind(m_spScope);
	m_asContactLive.bind(m_spScope);

	// Find all matching tires
	std::vector<Record>	tires;
	asTireLive.filter(tires, m_rg_dataset);

	for(unsigned int i_tire = 0; i_tire < tires.size(); i_tire++)
	{
		Record r_tire = tires[i_tire];

		Record r_model = rd_models[asTireLive.model(r_tire)];

		if(r_model.isValid())
		{
			sp<TireI> spTire(registry()->create(
				asTireModel.component(r_model)));

			if(spTire.isValid())
			{
				spTire->compile(r_tire, r_model, m_rg_dataset);
				m_r_tires.push_back(r_tire);

				asTireLive.spTireI(r_tire) = spTire;

				// this screws up tires and locators being same record
				// setIdentity(asTireLiveDirector.transform(r_tire));

				// should default to no effective contact
				asTireLive.contact_radius(r_tire) = 1.0e12;
				asTireLive.inclination(r_tire) = 0.0;
			}
		}
	}
}

t_moa_real sigmoid(t_moa_real a_x, t_moa_real a_max, t_moa_real a_min, t_moa_real a_cross_value, t_moa_real a_smoothness, t_moa_real a_zero_clip)
{
	// shifted sigmoid
	t_moa_real value = 0.5+0.5*(a_x-a_cross_value) / (1.0e-8 + sqrt((a_x-a_cross_value)*(a_x-a_cross_value)+a_smoothness*a_smoothness));

	value = (a_max-a_min)*value + a_min;

	// tidy it down to zero at zero
	if(a_zero_clip > 1.0e-8)
	{
		value *= fabs(a_x) / (fabs(a_x) + a_zero_clip);
	}
	return value;
}

} /* namespace ext */
} /* namespace fe */
