/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __tire_PointViewerSystem_h__
#define __tire_PointViewerSystem_h__

#include "viewer_system/viewer_system.h"

namespace fe
{
namespace ext
{

/**
 */
class PointViewerSystem : virtual public ViewerSystem,
	public Initialize<PointViewerSystem>
{
	public:
				PointViewerSystem(void);
virtual			~PointViewerSystem(void);
		void	initialize(void);
virtual	void	updatePoints(const t_note_id &note_id);
virtual	void	draw(Record viewportRecord);
virtual	void	connectOrchestrator(sp<OrchestratorI> orchestrator);

	private:
		AsPoint				m_asPoint;
		std::vector<Record>	m_points;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __tire_PointViewerSystem_h__ */


