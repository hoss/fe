/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "fe/data.h"
#include "tire/tire.h"
#include "evaluate/evaluate.h"
#include "surface/surface.h"

#include "viewer_system/viewer_system.h"

#define TEST_TIREISYSTEM
//TODO: fix SurfaceI version
#define TEST_MANUAL_SURFACEI

using namespace fe;
using namespace ext;

int main(int argc, char *argv[])
{
	UnitTest unitTest;
	try
	{
		feLogger()->clear(".*");
		feLogger()->clear("error");
		feLogger()->clear("info");
		feLogger()->clear("warning");

		sp<Master> spMaster(new Master);
		assertMath(spMaster->typeMaster());
		assertInterface<DrawI>(spMaster, "DrawI");
		sp<Registry> spRegistry = spMaster->registry();
		spRegistry->manage("fexMechanicsDL");
		spRegistry->manage("fexMoaDL");
		spRegistry->manage("fexTireDL");
		spRegistry->manage("fexEvaluateDL");
		spRegistry->manage("fexSurfaceDL");
		spRegistry->manage("feDataDL");
		spRegistry->manage("fexViewerDL");
		spRegistry->manage("fexViewerSystemDL");

		sp<Scope> spScope = spRegistry->create("Scope");

		Overlayer overlayer(spScope);

		overlayer.load("rig.rg");
		overlayer.load("tire.rg");
		overlayer.load("brush.rg");
		overlayer.load("linear.rg");
		overlayer.load("splat.rg");

		for(sp<RecordGroup> rg_clone : overlayer.bakedDatasets())
		{
fprintf(stderr, "DATASET\n");
			// >>>>>>>>> CREATE ORCHESTRATOR
			sp<OrchestratorI> spOrchestrator(
				spRegistry->create("OrchestratorI.Orchestrator.fe"));
			spOrchestrator->datasetInitialize(rg_clone);

			spOrchestrator->append(spRegistry->create("SystemI.RigSystem.fe"));
			spOrchestrator->append(spRegistry->create("SystemI.SimulatorLoop.fe"));
			//spOrchestrator->append(spRegistry->create("SystemI.MultiSweep.fe"));
			//spOrchestrator->append(spRegistry->create("SystemI.FrequencyResponse.fe"));
			//spOrchestrator->append(spRegistry->create("SystemI.MountSystem.fe"));
			spOrchestrator->append(spRegistry->create("SystemI.ParticleMountSystem.fe"));
			//spOrchestrator->append(spRegistry->create("SystemI.RigParticleMountSystem.fe"));
			//spOrchestrator->append(spRegistry->create("SystemI.LocatorRigBinding.fe"));
#ifdef TEST_TIREISYSTEM
			spOrchestrator->append(spRegistry->create("SystemI.TireISystem.fe"));
#else
			spOrchestrator->append(spRegistry->create("SystemI.BrushTireSystem.fe"));
#endif

#ifdef TEST_MANUAL_SURFACEI

			spOrchestrator->append(spRegistry->create("SystemI.SurfaceContact.fe"));

#if 0
			//spOrchestrator->append(spRegistry->create("SystemI.ClosestGroundContact.fe"));
			/* manually setup ground, as a demostrations of a
				not-hermetically sealed SystemI */
			AsGround asGround(spScope);
			asGround.bind(spScope);
			std::vector<Record> grounds;
			asGround.filter(grounds, rg_clone);
			for(unsigned int i_ground = 0; i_ground < grounds.size();
				i_ground++)
			{
				sp<SurfaceI> spSurface(
					spRegistry->create("SurfaceI.SurfacePlane"));
				sp<GroundI> spGround(
					spRegistry->create("SystemI.SurfaceGround.fe"));
					//spRegistry->create("GroundI.SurfaceGroundSystem.fe"));
				spGround->setSurface(spSurface);

				SpatialTransform xform;
				setIdentity(xform);
				translate(xform, SpatialVector(0.0,0.0,
					asGround.height(grounds[i_ground])));
				spGround->setTransform(xform);
				spOrchestrator->append(spGround);
			}
#endif
			spOrchestrator->append(spRegistry->create("SystemI.TireContact.fe"));
#else
			spOrchestrator->append(spRegistry->create("SystemI.ClosestGroundContact.fe"));
			spOrchestrator->append(spRegistry->create("SystemI.TireContact.fe"));
			//spOrchestrator->append(spRegistry->create("SystemI.GroundCollisionSystem.fe"));
#endif

			//spOrchestrator->append(spRegistry->create("SystemI.NyquistLineSystem.fe"));
			//spOrchestrator->append(spRegistry->create("SystemI.SplatLineSystem.fe"));

			sp<ViewerSystem> spViewerSystem = spRegistry->create("SystemI.PointViewer.fe");
			spOrchestrator->append(spViewerSystem);

			if(!spOrchestrator->compile())
			{
				fprintf(stderr, "compile failed\n");
				exit(101);
			}

			sp<ThreadedViewerI> spViewer = spRegistry->create("ThreadedViewerI.MOAViewer");
			spViewer->open(spOrchestrator);

			// >>>>>>>>> START NOTE
			t_note_id note_start = spOrchestrator->note_id(FE_NOTE_START);
			spOrchestrator->perform(note_start);


			snapshot(rg_clone, "dataset.rg");
		}
	}
	catch(Exception &e)
	{
		e.log();
	}
	return unitTest.failures();
}

