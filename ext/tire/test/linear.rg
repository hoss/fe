INFO 5

###################################################################
# Linear Parameters
ATTRIBUTE	name									string
ATTRIBUTE	tire:component							string		"=TireI.Linear.fe"
ATTRIBUTE	tire:z:stiffness						real		=200000
ATTRIBUTE	tire:z:damping							real		=1000
ATTRIBUTE	tire:xy:stiffness						real		=0.1
ATTRIBUTE	tire:friction							real		=1.0
ATTRIBUTE	tire:Fmax								real		=1e5
ATTRIBUTE	tire:pneumatic_trail					real		=0.05
ATTRIBUTE	tire:do_rollover						boolean		=true

LAYOUT linear_tire_model
	name
	tire:component
	tire:z:stiffness
	tire:z:damping
	tire:xy:stiffness
	tire:friction
	tire:Fmax
	tire:pneumatic_trail
	tire:do_rollover

DEFAULTGROUP 1

RECORD * linear_tire_model
	name "Linear Default"

END

