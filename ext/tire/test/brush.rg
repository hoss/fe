INFO 5

ATTRIBUTE	name string
ATTRIBUTE	tire:component						string		"=TireI.Brush.fe"
ATTRIBUTE	tire:z:stiffness real =312000.0
ATTRIBUTE	tire:z:damping	real =500.0
ATTRIBUTE	brush:camber_stiffness real =0.05
ATTRIBUTE	brush:braking_stiffness real =235000.0
ATTRIBUTE	brush:cornering_stiffness real =195000.0
ATTRIBUTE	brush:self_aligning_stiffness real =15000.0
ATTRIBUTE	brush:lat_init_load_stiff_scale real =1.0
ATTRIBUTE	brush:long_init_load_stiff_scale real =1.0
ATTRIBUTE	brush:design_load real =0.0
ATTRIBUTE	brush:slip_speed_fade vector2 "=0.0 100.0"
ATTRIBUTE	brush:load_sensitivy_slope real =-0.000003
ATTRIBUTE	brush:load_sensitivy_final real =0.65
ATTRIBUTE	brush:load_sensitivy_final_load real =5000.0
ATTRIBUTE	brush:half_grip_speed real =1000.5
ATTRIBUTE	brush:lat_grip real =0.5
ATTRIBUTE	brush:long_grip real =0.5
ATTRIBUTE	brush:slide_grip real =0.52

LAYOUT brush_model
	name
	tire:component
	tire:z:stiffness
	tire:z:damping
	brush:camber_stiffness
	brush:braking_stiffness
	brush:cornering_stiffness
	brush:self_aligning_stiffness
	brush:lat_init_load_stiff_scale
	brush:long_init_load_stiff_scale
	brush:design_load
	brush:slip_speed_fade
	brush:load_sensitivy_slope
	brush:load_sensitivy_final
	brush:load_sensitivy_final_load
	brush:half_grip_speed
	brush:lat_grip
	brush:long_grip
	brush:slide_grip

DEFAULTGROUP 1

RECORD Brush brush_model
	name "Brush Default"
	tire:z:stiffness 200000
	tire:z:damping 1000
	brush:cornering_stiffness 10000
	brush:self_aligning_stiffness 10000
	brush:braking_stiffness 10000

END

