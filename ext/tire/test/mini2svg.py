#!/usr/bin/python2

from PIL import Image
import os, sys
import re

outdir = 'report'
if (os.path.isdir(outdir) == 0):
    os.mkdir(outdir, 0755)

re_kv = re.compile(r'#\s*(.*)=(.*)')
re_annotation = re.compile(r'#\s*([-+]?[0-9]*\.?[0-9]+)\s*([-+]?[0-9]*\.?[0-9]+)\s*(.*)')
re_size = re.compile(r'^\s*([-+]?[0-9]*\.?[0-9]+)\s+([-+]?[0-9]*\.?[0-9]+)$')

class Context:
    def __init__(self, txt):
        self.txt = txt
        tokens = txt.split(',')
        self.name = ""
        self.xrange = (0.0, 1.0)
        self.yrange = (0.0, 1.0)
        self.color = (0, 0, 0)
        
        if len(tokens) >= 8:
            self.name = tokens[0]
            self.xrange = ( float(tokens[1]), float(tokens[2]) )
            self.yrange = ( float(tokens[3]), float(tokens[4]) )
            self.color = (int(float(tokens[5])*255.0), int(float(tokens[6])*255.0), int(float(tokens[7])*255.0))

        self.xspan = self.xrange[1] - self.xrange[0]
        self.yspan = self.yrange[1] - self.yrange[0]
        self.xkeycolor = self.color
        self.ykeycolor = self.color

        self.annos = []

    def realx(self, unitx):
        return self.xspan * unitx + self.xrange[0]
    def realy(self, unity):
        return self.yspan * unity + self.yrange[0]

def mut(color):
    d = 80
    if color > 127:
        return color - d
    return color + d


def convert_ppm(root, basename):
    contexts = []
    current_context = Context("")


    im = Image.open(os.path.join(root, basename + ".ppm"))
    rgb_im = im.convert('RGB')
    rgb_im.save(os.path.join(outdir, basename + ".png"))

    infos = {}
    annos = []

    tokens = basename.split('_')
    datacode = tokens[0]
    runcode = tokens[1]
    infos["DATACODE"] = [ datacode ]
    infos["RUNCODE"] = [ runcode ]

    fp = open(os.path.join(root, basename + ".ppm"))
    lines = fp.readlines()
    width = 0
    height = 0
    for line in lines:
        match_kv = re_kv.match(line)
        match_anno = re_annotation.match(line)
        match_size = re_size.match(line)
        if match_kv:
            k = match_kv.group(1).strip()
            v = match_kv.group(2).strip()
            if not k in infos:
                infos[k] = []
            infos[k].append(v)
            if k == "CONTEXT":
                current_context = Context(v)
                contexts.append(current_context)
                
        elif match_anno:
            anno = (float(match_anno.group(1).strip()), float(match_anno.group(2).strip()), match_anno.group(3).strip(), current_context)
            annos.append(anno)
            current_context.annos.append(anno)
        elif match_size:
            width = int(match_size.group(1).strip())
            height = int(match_size.group(2).strip())

    SCALE = 1.0
    width = int(SCALE*float(width))
    height = int(SCALE*float(height))
    MARGIN_T = 30
    MARGIN_B = 70
    MARGIN_L = 20
    MARGIN_R = 300
    HASH_CNT = 7
    OUTER_HASH = 10
    CR = 18
    TXT_H = CR-4
    FULL_W = width + MARGIN_L + MARGIN_R
    FULL_H = height + MARGIN_T + MARGIN_B
    dhash = 1.0 / float(HASH_CNT-1)
    CIRCLE_R = TXT_H / 2 - 1

    fp.close()

    text = """<svg width="%d" height="%d" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">\n""" % (FULL_W, FULL_H)
    text += """<rect x="0" y="0" width="%d" height="%d" fill="#444444" style="stroke-width:3;stroke:rgb(0,0,255)" />\n""" % (FULL_W, FULL_H)
    text += """<image x="%d" y="%d" width="%d" height="%d" xlink:href="%s.png"/>\n""" % (MARGIN_L, MARGIN_T, width, height, basename)

    x = 10
    y = 20

    #for title in infos["TITLE"]:
    #    text += """<text font-weight="bold" font-family="Helvetica" font-size="18" x="%d" y="%d" fill="red">%s</text>""" % (x, y, title)
    #    y += 20
    for title in infos["METHOD"]:
        text += """<text font-weight="bold" font-family="Helvetica" font-size="18" x="%d" y="%d" fill="#00ffff">%s</text>""" % (x, y, title)
        y += 20

    x = MARGIN_L+width
    y = 20
    if "ASPECT" in infos:
        for title in infos["ASPECT"]:
            text += """<text text-anchor="end" font-weight="normal" font-family="Helvetica" font-size="16" x="%d" y="%d" fill="#f0ffff">%s</text>""" % (x, y, title)
            y += 20

    #for title in infos["VIEW"]:
    #    text += """<text font-weight="normal" font-family="Helvetica" font-size="16" x="%d" y="%d" fill="#00ff0f">%s</text>""" % (x, y, title)
    #    y += 20

    i = 0
    for context in contexts:
        context.order = i
        i += 1

    do_annotation = 1
    if do_annotation:


        x_axis_text_placement = MARGIN_T + height + OUTER_HASH + CR
        x_ranges = {}
        #y_placement_offset = 0.0
        for context in contexts:
            if context.xrange in x_ranges:
                context.xkeycolor = x_ranges[context.xrange].xkeycolor
                continue
            x_ranges[context.xrange] = context
            rr = 0.0
            while rr <= 1.0:
                x_in = context.realx(rr)
                x_placement = rr * width
                # X AXIS TEXT
                text += """<text text-anchor="middle" font-weight="normal" font-family="Consolas" font-size="%d" fill="rgb(%d, %d, %d)" transform="translate(%d %d)" >%.1f</text>""" % (TXT_H, context.color[0], context.color[1], context.color[2], MARGIN_L+x_placement, x_axis_text_placement, x_in)
                rr += dhash
            x_axis_text_placement += CR

        y_ranges = {}
        for context in contexts:
            if context.yrange in y_ranges:
                context.ykeycolor = y_ranges[context.yrange].ykeycolor
                continue
            y_ranges[context.yrange] = context
        uniq_ctxs = len(y_ranges)
        y_axis_text_placement = MARGIN_L + width + OUTER_HASH + OUTER_HASH
        y_placement_offset = -int(((float(uniq_ctxs) - 1.0) / 2.0)*float(CR)) + int(float(TXT_H)/2.0)
        y_ranges = {}
        for context in contexts:
            if context.yrange in y_ranges:
                continue
            y_ranges[context.yrange] = 1
            rr = 0.0
            while rr <= 1.0:
                y_in = context.realy(1.0-rr)
                y_placement = y_placement_offset + rr * height -0.2*TXT_H
                # Y AXIS TEXT
                text += """<text text-anchor="start" font-weight="normal" font-family="Consolas" font-size="%d" fill="rgb(%d, %d, %d)" transform="translate(%d %d)" >%.1f</text>""" % (TXT_H, context.color[0], context.color[1], context.color[2], y_axis_text_placement, MARGIN_T+y_placement, y_in)
                rr += dhash
            y_placement_offset += CR




        anno_count = 0
        for context in contexts:
            text += """<text text-anchor="start" font-weight="normal" font-family="Consolas" font-size="%d" x="%d" y="%d" fill="rgb(%d, %d, %d)">%s</text>\n""" % \
                (TXT_H, MARGIN_L+width+5*CR, MARGIN_T+anno_count*CR, context.color[0], context.color[1], context.color[2], context.name)
            text += """<rect x="%f" y="%f" width="%f" height="%f" stroke="none" fill="rgb(%d, %d, %d)" stroke-width="0"/>\n""" % \
                (MARGIN_L+width+MARGIN_R-2*CR-TXT_H+3, MARGIN_T+anno_count*CR-0.0*TXT_H, TXT_H-2, 4, (context.xkeycolor[0]), (context.xkeycolor[1]), (context.xkeycolor[2]))
            text += """<rect x="%f" y="%f" width="%f" height="%f" stroke="none" fill="rgb(%d, %d, %d)" stroke-width="0"/>\n""" % \
                (MARGIN_L+width+MARGIN_R-2*CR, MARGIN_T+anno_count*CR-1.0*TXT_H+2, 4, TXT_H-2, (context.ykeycolor[0]), (context.ykeycolor[1]), (context.ykeycolor[2]))
            for anno in context.annos:
                anno_count = anno_count + 1
                #context = anno[3]
                inferred_x = context.realx(anno[0])
                inferred_y = context.realy(anno[1])
                hh = float(height)
                #text += """<text text-anchor="middle" font-weight="normal" font-family="Helvetica" font-size="14" x="%d" y="%d" fill="white">%s (%.1f,%.1f)</text>\n""" % (MARGIN_L+anno[0]*width, MARGIN_T+height-anno[1]*height-10, anno[2], inferred_x, inferred_y)

                # GRAPH ANNOTATION TEXT
                text += """<text text-anchor="middle" font-weight="normal" font-family="Helvetica" font-size="%d" x="%d" y="%d" fill="white">%s</text>\n""" % \
                    (TXT_H, MARGIN_L+anno[0]*width, MARGIN_T+hh-anno[1]*hh-TXT_H, anno[2])

                # KEY X
                text += """<text text-anchor="end" font-weight="normal" font-family="Consolas" font-size="%d" x="%d" y="%d" fill="rgb(%d, %d, %d)">%.1f</text>\n""" % \
                    (TXT_H, MARGIN_L+width+10*CR, MARGIN_T+anno_count*CR, (context.xkeycolor[0]), (context.xkeycolor[1]), (context.xkeycolor[2]), inferred_x)

                # KEY Y
                text += """<text text-anchor="end" font-weight="normal" font-family="Consolas" font-size="%d" x="%d" y="%d" fill="rgb(%d, %d, %d)">%.1f</text>\n""" % \
                    (TXT_H, MARGIN_L+width+15*CR, MARGIN_T+anno_count*CR, (context.ykeycolor[0]), (context.ykeycolor[1]), (context.ykeycolor[2]), inferred_y)

                # GRAPH CIRCLE
                text += """<circle cx="%f" cy="%f" r="%f" fill="none" stroke="rgb(%d, %d, %d)" stroke-width="2"/>\n""" % \
                    (MARGIN_L+anno[0]*float(width), MARGIN_T+hh-anno[1]*hh, CIRCLE_R, (context.color[0]), (context.color[1]), (context.color[2]))

                # KEY CIRCLE
                text += """<circle cx="%f" cy="%f" r="%f" fill="none" stroke="rgb(%d, %d, %d)" stroke-width="2"/>\n""" % \
                    (MARGIN_L+width+5*CR, MARGIN_T+anno_count*CR-0.3*TXT_H, CIRCLE_R, (context.color[0]), (context.color[1]), (context.color[2]))
            anno_count = anno_count + 2

        # HASHES
        rr = 0.0
        while rr <= 1.0:
            x_in = context.realx(rr)
            x_placement = rr * width
            text += """<line x1="%g" y1="%g" x2="%g" y2="%g" stroke="#333333" stroke-width="3" />""" % (MARGIN_L+x_placement, MARGIN_T+height, MARGIN_L+x_placement, MARGIN_T+height+OUTER_HASH)
            text += """<line x1="%g" y1="%g" x2="%g" y2="%g" stroke="#555555" stroke-width="2" opacity="50%%" stroke-dasharray="1 5" />""" % (MARGIN_L+x_placement, MARGIN_T+height, MARGIN_L+x_placement, MARGIN_T)
            y_in = context.realy(rr)
            y_placement = rr * height
            text += """<line x1="%g" y1="%g" x2="%g" y2="%g" stroke="#333333" stroke-width="3" />""" % (MARGIN_L+width, MARGIN_T+y_placement, MARGIN_L+width+OUTER_HASH, MARGIN_T+y_placement)
            text += """<line x1="%g" y1="%g" x2="%g" y2="%g" stroke="#555555" stroke-width="2" opacity="50%%" stroke-dasharray="1 5" />""" % (MARGIN_L+width, MARGIN_T+y_placement, MARGIN_L, MARGIN_T+y_placement)
            rr +=dhash 



        
    text += """</svg>"""

    #fp = open(os.path.join(outdir, basename + ".svg"), "w")
    #fp.write(text)
    #fp.close()

    return (text, infos, contexts)

header_html = """
<html><body BGCOLOR="#110000" TEXT="#FFFFFF" LINK="#ffdddd" ALINK="#ddffdd" VLINK="#ddddff">
<A HREF="index.html"><font face = "Helvetica" size = "+1" color = "#ffff00">Splat Based Testing</font></A>
<HR>
"""

by_topic = {}
by_test = {}

from os.path import join, getsize
for root, dirs, files in os.walk('data'):
    for fname in files:
        (basename, ext) = os.path.splitext(fname)
        if ext == ".ppm":
            (svg,infos,contexts) = convert_ppm(root, basename)

            print root
            print fname
            print "  %s (%s %s)" % (fname, basename, ext)
            print os.path.join(root, fname)
            tokens = basename.split('_')
            print tokens[0]
            print tokens[1]

            topic = tokens[0]

            runcode = infos["RUNCODE"][0]

            if "ASPECT" in infos:
                for topic in infos["ASPECT"]:
                    if not by_topic.has_key(topic):
                        by_topic[topic] = []
                    by_topic[topic].append(svg)

            if not by_test.has_key(runcode):
                by_test[runcode] = []
            by_test[runcode].append((svg,contexts))

            #header_html += ''' | %s''' % (svg)

for (k,v) in by_topic.items():
    header_html += ''' | <A HREF="%s.html"><font face = "Helvetica" size = "-1" color = "#ffffff">%s</font></A>''' % (k, k)
header_html += "<HR>\n"

for (k,v) in by_test.items():
    header_html += ''' | <A HREF="%s.html"><font face = "Helvetica" size = "-1" color = "#ffffff">%s</font></A>''' % (k, k)
header_html += "<HR>\n"

for (k,v) in by_topic.items():
    hfp = open( os.path.join(outdir, '%s.html' % k), 'w' )
    hfp.write(header_html)
    v.sort()
    for s in v:
        hfp.write(s)
    hfp.write("</body></html>")
    hfp.close()

for (k,v) in by_test.items():
    hfp = open( os.path.join(outdir, '%s.html' % k), 'w' )
    hfp.write(header_html)
    for s in v:
        hfp.write(s[0])
    hfp.write("</body></html>")
    hfp.close()

hfp = open( os.path.join(outdir, 'index.html'), 'w' )
hfp.write(header_html)
hfp.write("</body></html>")
hfp.close()



