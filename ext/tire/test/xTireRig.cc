/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "fe/data.h"
#include "tire/tire.h"
#include "evaluate/evaluate.h"
#include "surface/surface.h"

#define TEST_TIREISYSTEM
#define TEST_SURFACEI

using namespace fe;
using namespace ext;

int main(int argc, char *argv[])
{
	UnitTest unitTest;
	try
	{
		//feLogger()->clear(".*");
		//feLogger()->clear("error");
		//feLogger()->clear("info");
		//feLogger()->clear("warning");

		sp<Master> spMaster(new Master);
		assertMath(spMaster->typeMaster());
		sp<Registry> spRegistry = spMaster->registry();
		spRegistry->manage("fexMechanicsDL");
		spRegistry->manage("fexMoaDL");
		spRegistry->manage("fexTireDL");
		spRegistry->manage("fexEvaluateDL");
		spRegistry->manage("fexSurfaceDL");
		spRegistry->manage("feDataDL");
		spRegistry->manage("fexViewerDL");

		sp<Scope> spScope = spRegistry->create("Scope");

		Overlayer overlayer(spScope);

		overlayer.load("rig.rg");
		overlayer.load("tire.rg");
		overlayer.load("brush.rg");
		overlayer.load("linear.rg");
		overlayer.load("splat.rg");

		for(sp<RecordGroup> rg_clone : overlayer.bakedDatasets())
		{
			// >>>>>>>>> CREATE ORCHESTRATOR
			sp<OrchestratorI> spOrchestrator(
				spRegistry->create("OrchestratorI.Orchestrator.fe"));
			spOrchestrator->datasetInitialize(rg_clone);

			spOrchestrator->append(spRegistry->create("SystemI.RigSystem.fe"));
			spOrchestrator->append(spRegistry->create("SystemI.MultiSweep.fe"));
			//spOrchestrator->append(spRegistry->create("SystemI.FrequencyResponse.fe"));

			spOrchestrator->append(spRegistry->create("SystemI.MountSystem.fe"));

			//spOrchestrator->append(spRegistry->create("SystemI.RigParticleMountSystem.fe"));

			//spOrchestrator->append(spRegistry->create("SystemI.ParticleMountSystem.fe"));

			// rig -> locator (location | alpha, incl, rate, etc)
			//spOrchestrator->append(spRegistry->create("SystemI.LocatorRigBinding.fe"));


			// locator ->

			// particles -> locator (infer frame)

			// locator -> particles (apply forces)

			// tire -> locator (apply forces, in place, locator == tire)
			// locator -> tire (just is, by construction locator == tire)

#ifdef TEST_TIREISYSTEM
			spOrchestrator->append(spRegistry->create("SystemI.TireISystem.fe"));
#else
			spOrchestrator->append(spRegistry->create("SystemI.BrushTireSystem.fe"));
#endif

#ifdef TEST_SURFACEI
			spOrchestrator->append(spRegistry->create("SystemI.SurfaceContact.fe"));
#else
			spOrchestrator->append(spRegistry->create("SystemI.ClosestGroundContact.fe"));
			//spOrchestrator->append(spRegistry->create("SystemI.GroundCollisionSystem.fe"));
#endif
			spOrchestrator->append(spRegistry->create("SystemI.TireContact.fe"));

			spOrchestrator->append(spRegistry->create("SystemI.NyquistLineSystem.fe"));
			spOrchestrator->append(spRegistry->create("SystemI.SplatLineSystem.fe"));

			if(!spOrchestrator->compile())
			{
				fprintf(stderr, "compile failed\n");
				exit(101);
			}

			// >>>>>>>>> START NOTE
			//t_note_id note_start = spOrchestrator->note_id(FE_NOTE_START);
			spOrchestrator->perform(FE_NOTE_START);
		}
	}
	catch(Exception &e)
	{
		e.log();
	}
	return unitTest.failures();
}

