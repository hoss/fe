INFO 5
ATTRIBUTE splat:expression string
ATTRIBUTE splat:x_range vector2
ATTRIBUTE splat:y_range vector2
ATTRIBUTE composer:step string
ATTRIBUTE composer:home string
ATTRIBUTE composer:outer stringarray "=0 0 0"
ATTRIBUTE composer:inner stringarray
ATTRIBUTE composer:linger real =1.0
ATTRIBUTE composer:frequency real =600.0
ATTRIBUTE composer:runup real =4.0
ATTRIBUTE name string
ATTRIBUTE label string
ATTRIBUTE dataset:group group
ATTRIBUTE mount:tire string
ATTRIBUTE mount:rig string
ATTRIBUTE mount:stripped_force boolean =false
ATTRIBUTE spc:transform_inv transform "=0 0 0 0 0 0 0 0 0 0 0 0"
ATTRIBUTE splat:splat string
ATTRIBUTE splat:color vector3 "=1 1 1"
#ATTRIBUTE splat:x_label string
#ATTRIBUTE splat:y_label string
ATTRIBUTE composer:samples int =1024
ATTRIBUTE splatline:is void
ATTRIBUTE nyquist:scale real =2.0
ATTRIBUTE tire:model string
ATTRIBUTE tire:radius real "=0.22"
ATTRIBUTE tire:width real "=0.3"
ATTRIBUTE other record
ATTRIBUTE mixer:inputs stringarray
ATTRIBUTE mixer:left stringarray
ATTRIBUTE mixer:right stringarray
ATTRIBUTE aspect:label string "=unset"
ATTRIBUTE dataset:active boolean =false
ATTRIBUTE loop:duration real "=100.2"
LAYOUT splat
	name
	splat:x_range
	splat:y_range
	splat:expression
	splat:splat
	splat:color
	splatline:is
LAYOUT nyquist_splat
	name
	splat:x_range
	splat:y_range
	splat:expression
	splat:splat
	splat:color
	nyquist:scale
LAYOUT loop
	name
	composer:linger
	composer:frequency
	loop:duration
LAYOUT multisweep
	name
	composer:step
	composer:outer
	composer:inner
	composer:linger
	composer:frequency
	composer:runup
LAYOUT freqresp
	name
	composer:step
	composer:home
	composer:outer
	composer:inner
	composer:samples
	composer:frequency
LAYOUT overlay
	name
	label
	dataset:group
LAYOUT dataset
	name
	dataset:group
	dataset:active
#LAYOUT mount
#	mount:tire
#	mount:rig
#	tire:xform_inv
LAYOUT mount
	#name
	mount:tire
	mount:rig
	mount:stripped_force
	spc:transform_inv
LAYOUT dataset_mixer
	name
	mixer:inputs
	dataset:active
LAYOUT dataset_many_to_many
	mixer:left
	mixer:right
	dataset:active
	
#LAYOUT tire
#	name
#	tire:radius
#	tire:width
#	tire:model
LAYOUT aspect
	aspect:label

DEFAULTGROUP 1

TEMPLATE alpha_splat splat
	splat:x_range   "-30.0      30.0"

DEFAULTGROUP 1

#RECORD * tire
#	name "PT tire"
#	tire:radius 0.22
#	tire:model "Pacejka Alternate"

RECORD * overlay
	name		"BTg"
	label		"Brush Generic"
	dataset:group		RG_TIRE_BT

RECORD * overlay
	name		"PTg"
	label		"Pacejka Generic"
	dataset:group		RG_TIRE_PT

RECORD * overlay
	name		"LTg"
	label		"Linear Generic"
	dataset:group		RG_TIRE_LT


RECORD * dataset
	name		"Freewheel Run"
	dataset:group		RG_FREEWHEEL_SWEEP

RECORD * dataset
	name		"Omega Run"
	dataset:group		RG_KAPPA_ALPHA_SWEEP

RECORD * dataset
	name		"Freq Run"
	dataset:group		RG_FRQ_RESP

RECORD * dataset_mixer
	name				"PTg Omega Rn"
	mixer:inputs		"PTg 'Omega Run'"
	dataset:active		false

RECORD * dataset_mixer
	name				"BTg Omega Rn"
	mixer:inputs		"BTg 'Omega Run'"
	dataset:active		false

RECORD * dataset_mixer
	name				"LTg Omega Rn"
	mixer:inputs		"LTg 'Omega Run'"
	dataset:active		false

RECORD * dataset_mixer
	name				"PTg Freq"
	mixer:inputs		"PTg 'Freq Run'"
	dataset:active		false

RECORD * dataset_mixer
	name				"Pacejka Free Wheel"
	mixer:inputs		"PTg 'Freewheel Run'"
	dataset:active		false

RECORD * dataset_mixer
	name				"BTg FW"
	mixer:inputs		"BTg 'Freewheel Run'"
	dataset:active		false

RECORD * dataset_many_to_many
	#mixer:left			"'PTg' 'BTg' 'LTg'"
	mixer:left			"'LTg'"
	#mixer:right			"'Omega Run' 'Freq Run' 'Freewheel Run'"
	#mixer:right			"'Omega Run' 'Freq Run'"
	mixer:right			"'Freewheel Run'"
	#mixer:right			"'Freewheel Run'"
	dataset:active		true



DEFAULTGROUP RG_FRQ_RESP
TEMPLATE T_FREQ_RESP freqresp
RECORD * T_FREQ_RESP
	name				"f.vα"
	composer:outer		"2.0 64.0 *4.0"
	composer:inner		"-0.1 -120.0 *1.4"
	composer:samples	1024
	composer:frequency			600.0
	composer:step   "	(set    'base_rig.rig:velocity' INNER)
						(set    'base_rig.rig:alpha' (* MOD 10.0))"
	composer:home	"	(set    'base_rig.rig:load' 600)"
RECORD * nyquist_splat
	name			"αFy.n"
	splat:x_range	"-30.0		30.0"
	splat:y_range	"-7000.0	7000.0"
	splat:expression "	(X (get		'base_rig.rig:alpha')		)
						(Y (get		'base_rig.mnt.tire:force[1]')	)"
	splat:color		"1 0 0.6"

DEFAULTGROUP RG_FREEWHEEL_SWEEP
RECORD * loop
	name				"loop"
	composer:frequency	600.0
#RECORD * multisweep
#	name			"fwα"
#	composer:inner	"-20.0 20.0 1.0"
#	composer:step	"	(set	'base_rig.rig:alpha' INNER)		"
#RECORD * multisweep
#	name			"κα"
#	composer:outer	"0.0 20.0 10.0"
#	composer:inner	"-20.0 20.0 1.0"
#	composer:step	"	(set	'base_rig.rig:alpha' INNER)
#						(set	'base_rig.rig:kappa' OUTER)		"
# ----------------------------------------------------------------------------
# ----------------------------------------------------------------------------
# ----------------------------------------------------------------------------
# ----------------------------------------------------------------------------
# ----------------------------------------------------------------------------
RECORD * multisweep
	name			"inclα"
	composer:outer	"-0.4 0.4 0.4"
	composer:inner	"-15.0 15.0 1.0"
	composer:step	"	(set	'base_rig.rig:alpha' INNER)
						(set	'base_rig.rig:inclination' OUTER)		"
#RECORD * multisweep
#	name			"speed"
#	composer:inner	"-15.0 15.0 1.0"
#	composer:step	"	(set	'base_rig.rig:velocity' INNER)"

#RECORD * multisweep
#	name			"inclα"
#	composer:inner	"-15.0 15.0 1.0"
#	composer:step	"	(set	'base_rig.rig:alpha' INNER) "
RECORD * alpha_splat
	name			"xαFy"
	splat:splat		"αFyFzMz__"
	splat:x_range	"-20.0		20.0"
	splat:y_range	"-2000.0	2000.0"
	splat:expression "	(X (get		'base_rig.rig:alpha')		)
						(Y (get		'base_rig.mnt.sim:force[0]')	) "
						#(R        (/  (get 'base_rig.rig:kappa')    20.0) )
						#(G (- 1.0 (/  (get 'base_rig.rig:kappa')    20.0)))
						#(B (0.0) ) "
	splat:color		"1 0 0"
RECORD * alpha_splat
	name			"xαFy"
	splat:splat		"αFyFzMz__"
	splat:x_range	"-20.0		20.0"
	splat:y_range	"-2000.0	2000.0"
	splat:expression "	(X (get		'base_rig.rig:alpha')		)
						(Y (get		'base_rig.mnt.sim:force[1]')	) "
						#(R        (/  (get 'base_rig.rig:kappa')    20.0) )
						#(G (- 1.0 (/  (get 'base_rig.rig:kappa')    20.0)))
						#(B (0.0) ) "
	splat:color		"0 1 0"
RECORD * alpha_splat
	name			"xαFz"
	splat:splat		"αFyFzMz__"
	splat:x_range	"-20.0		20.0"
	splat:y_range	"-2000.0	2000.0"
	splat:expression "	(X (get		'base_rig.rig:alpha')		)
						(Y (get		'base_rig.mnt.sim:force[2]')	) "
						#(R        (/  (get 'base_rig.rig:kappa')    20.0) )
						#(G (- 1.0 (/  (get 'base_rig.rig:kappa')    20.0)))
						#(B (0.0) ) "
	splat:color		"0 0 1"
RECORD * alpha_splat
	name			"LT Fz"
	splat:splat		"αFyFzMz____"
	splat:x_range	"-20.0		20.0"
	splat:y_range	"-2000.0	2000.0"
	splat:expression "	(X (get		'base_rig.rig:alpha')		)
						(Y (global	'LT Fz')	) "
	splat:color		"1 0.5 1"
RECORD * alpha_splat
	name			"LT V0"
	splat:splat		"αFyFzMz____"
	splat:x_range	"-20.0		20.0"
	splat:y_range	"-1.0	1.0"
	splat:expression "	(X (get		'base_rig.rig:alpha')		)
						(Y (global	'LT V0')	) "
	splat:color		"1 0.2 0.2"
RECORD * alpha_splat
	name			"LT V1"
	splat:splat		"αFyFzMz____"
	splat:x_range	"-20.0		20.0"
	splat:y_range	"-1.0	1.0"
	splat:expression "	(X (get		'base_rig.rig:alpha')		)
						(Y (global	'LT V1')	) "
	splat:color		"1 0.5 0.0"
RECORD * alpha_splat
	name			"LT Fx"
	splat:splat		"αFyFzMz____"
	splat:x_range	"-20.0		20.0"
	splat:y_range	"-2000.0	2000.0"
	splat:expression "	(X (get		'base_rig.rig:alpha')		)
						(Y (global	'LT Fx')	) "
	splat:color		"1.0 0.0 0.6"
RECORD * alpha_splat
	name			"LT Fy"
	splat:splat		"αFyFzMz____"
	splat:x_range	"-20.0		20.0"
	splat:y_range	"-2000.0	2000.0"
	splat:expression "	(X (get		'base_rig.rig:alpha')		)
						(Y (global	'LT Fy')	) "
	splat:color		"0.5 0.5 0.5"
RECORD * alpha_splat
	name			"LT Mz"
	splat:splat		"αFyFzMz____"
	splat:x_range	"-20.0		20.0"
	splat:y_range	"-42.0	42.0"
	splat:expression "	(X (get		'base_rig.rig:alpha')		)
						(Y (global	'LT Mz')	) "
	splat:color		"0.5 1.0 0.5"

RECORD * alpha_splat
	name			"LT Vx"
	splat:splat		"αFyFzMz___"
	splat:x_range	"-20.0		20.0"
	splat:y_range	"-100.0	100.0"
	splat:expression "	(X (get		'base_rig.rig:alpha')		)
						(Y (global	'LT Vx')	) "
	splat:color		"1.0 0.0 0.0"
RECORD * alpha_splat
	name			"LT Vy"
	splat:splat		"αFyFzMz___"
	splat:x_range	"-20.0		20.0"
	splat:y_range	"-100.0	100.0"
	splat:expression "	(X (get		'base_rig.rig:alpha')		)
						(Y (global	'LT Vy')	) "
	splat:color		"0.0 1.0 0.0"
RECORD * alpha_splat
	name			"LT Vz"
	splat:splat		"αFyFzMz___"
	splat:x_range	"-20.0		20.0"
	splat:y_range	"-100.0	100.0"
	splat:expression "	(X (get		'base_rig.rig:alpha')		)
						(Y (global	'LT Vz')	) "
	splat:color		"0.0 0.0 1.0"

RECORD * alpha_splat
	name			"xαMz"
	splat:splat		"αFyFzMz__"
	splat:x_range	"-20.0		20.0"
	splat:y_range	"-40.0	40.0"
	splat:expression "	(X (get		'base_rig.rig:alpha')		)
						(Y (get		'base_rig.mnt.tire:moment[2]')	) "
	splat:color		"1 1 0"
# Mx on a wishbone rig basically just tracks Fy
#RECORD * alpha_splat
#	name			"αMx"
#	splat:splat		"αFyFzMz"
#	splat:x_range	"-20.0		20.0"
#	splat:y_range	"-2000.0	2000.0"
#	splat:expression "	(X (get		'base_rig.rig:alpha')		)
#						(Y (get		'base_rig.sim:moment[0]')	) "
#	splat:color		"1 0 0"
RECORD * alpha_splat
	name			"αFx"
	splat:splat		"αFyFzMz"
	splat:x_range	"-20.0		20.0"
	splat:y_range	"-2000.0	2000.0"
	splat:expression "	(X (get		'base_rig.rig:alpha')		)
						(Y (get		'base_rig.sim:force[0]')	) "
	splat:color		"1 0.2 0.1"
RECORD * splat
	name			"αMy"
	splat:splat		"αFyFzMz"
	splat:x_range	"-20.0		20.0"
	splat:y_range	"-40.0	40.0"
	#splat:expression "	(X (get		'base_rig.rig:alpha')		)
	#					(Y (get		'base_rig.mnt.tire:moment[2]')	)	 "
	splat:expression "	(X (get		'base_rig.rig:alpha')		)
						(Y (get		'base_rig.sim:moment[1]')	)	 "
	splat:color		"0 1 0"
RECORD * splat
	name			"αFy"
	splat:splat		"αFyFzMz"
	splat:x_range	"-20.0		20.0"
	splat:y_range	"-2000.0	2000.0"
	#splat:expression "	(X (get		'base_rig.rig:alpha')		)
	#					(Y (get		'base_rig.mnt.tire:moment[2]')	)	 "
	splat:expression "	(X (get		'base_rig.rig:alpha')		)
						(Y (get		'base_rig.sim:force[1]')	)	 "
	splat:color		"0.5 1.0 0.0"
RECORD * splat
	name			"αMz"
	splat:splat		"αFyFzMz"
	splat:x_range	"-20.0		20.0"
	splat:y_range	"-40.0	40.0"
	#splat:expression "	(X (get		'base_rig.rig:alpha')		)
	#					(Y (* -1.0 (get		'base_rig.mnt.tire:force[2]'))	)	 "
	splat:expression "	(X (get		'base_rig.rig:alpha')		)
						(Y (get		'base_rig.sim:moment[2]')	)	 "
	splat:color		"0 0 1"
RECORD * splat
	name			"αFz"
	splat:splat		"αFyFzMz"
	splat:x_range	"-20.0		20.0"
	splat:y_range	"-2000.0	2000.0"
	splat:expression "	(X (get		'base_rig.rig:alpha')		)
						(Y (* -1.0 (get		'base_rig.mnt.tire:force[2]'))	)	 "
	#splat:expression "	(X (get		'base_rig.rig:alpha')		)
	#					(Y (get		'base_rig.sim:force[2]')	)	 "
	splat:color		"0.5 0.5 1"

DEFAULTGROUP RG_KAPPA_ALPHA_SWEEP
RECORD * multisweep
	name			"ακ"
	composer:inner	"-20.0 20.0 5.0"
	composer:outer	"-20.0 20.0 5.0"
	composer:step	"	(set	'base_rig.rig:kappa' INNER)
						(set	'base_rig.rig:alpha' OUTER)		"
RECORD * splat
	name			"κFx"
	splat:x_range	"-30.0		30.0"
	splat:y_range	"-5000.0	5000.0"
	splat:expression "	(X (get		'base_rig.rig:kappa')		)
						(Y (get		'base_rig.mnt.tire:force[0]')	)
						(R 1.0) (G 0.1) (B 0.0))  "


DEFAULTGROUP RG_TIRE_BT
#RECORD * mount
#	mount:tire "BT tire"
#	mount:rig "base_rig"
RECORD * tire
	name "BT tire"
	tire:radius 0.22
	tire:model "Brush Default"
RECORD * aspect
	aspect:label "Brush Tire"

DEFAULTGROUP RG_TIRE_PT
#RECORD * mount
#	mount:tire "PT tire"
#	mount:rig "base_rig"
RECORD PT_tire tire
	name "PT tire"
	tire:radius 0.22
	tire:model "Pacejka Default"
RECORD * aspect
	aspect:label "Pacejka Tire"

DEFAULTGROUP RG_TIRE_LT
#RECORD * mount
#	mount:tire "LT tire"
#	mount:rig "base_rig"
RECORD LT_tire tire
	name "LT tire"
	tire:radius 0.22
	tire:model "Linear Default"
RECORD * aspect
	aspect:label "Linear Tire"


RECORDGROUP 1
END
