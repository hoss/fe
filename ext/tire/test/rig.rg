INFO 5
ATTRIBUTE name string "=unset"
ATTRIBUTE ground:height real
ATTRIBUTE rig:inclination real =0.0
ATTRIBUTE rig:location vector3 ="0 0 0"
ATTRIBUTE rig:load real
ATTRIBUTE rig:pitch real =0.0
ATTRIBUTE rig:alpha real
ATTRIBUTE rig:velocity real
ATTRIBUTE rig:kappa real
ATTRIBUTE rig:free_wheel bool boolean
ATTRIBUTE rig:incline_floor bool boolean =false
ATTRIBUTE rig:solve_extract bool boolean
ATTRIBUTE rig:structure group
ATTRIBUTE rig:root record
ATTRIBUTE rig:floor WeakRecord
ATTRIBUTE mount:tire string
ATTRIBUTE mount:rig string
ATTRIBUTE mount:p0 WeakRecord
ATTRIBUTE mount:p1 WeakRecord
ATTRIBUTE mount:p2 WeakRecord
ATTRIBUTE mount:at vector3 spatial_vector
ATTRIBUTE tire:inclination real "=0.0"
ATTRIBUTE spc:transform_inv transform "=0 0 0 0 0 0 0 0 0 0 0 0"
ATTRIBUTE spc:at vector3 spatial_vector
ATTRIBUTE spc:velocity vector3 spatial_vector "=0 0 0"
ATTRIBUTE sim:force vector3 spatial_vector "=0 0 0"
ATTRIBUTE sim:mass real =1
ATTRIBUTE bnd:radius real
ATTRIBUTE sim:color color
ATTRIBUTE pair:left WeakRecord
ATTRIBUTE pair:right WeakRecord
ATTRIBUTE bdy:length real =-1
ATTRIBUTE mat:stiffness real =1e+6
ATTRIBUTE mat:damping real =1e+3
ATTRIBUTE mat:flags integer int I32 =0
ATTRIBUTE moa:contact spatial_vector
ATTRIBUTE spc:normal spatial_vector
ATTRIBUTE is:plane void
LAYOUT ground
	ground:height
LAYOUT rig
	name
	rig:inclination
	rig:location
	rig:load
	rig:pitch
	rig:alpha
	rig:velocity
	rig:kappa
	rig:free_wheel
	rig:incline_floor
	rig:solve_extract
	rig:structure
	rig:root
	rig:floor
LAYOUT pmount
	mount:tire
	mount:p0
	mount:p1
	mount:p2
	spc:transform_inv
	mount:rig
	mount:at
	tire:inclination
LAYOUT mount
	mount:tire
	mount:rig
	spc:transform_inv
LAYOUT l_rig_particle
	name
	spc:at
	spc:velocity
	sim:force
	sim:mass
	bnd:radius
	sim:color
LAYOUT l_rig_constraint
	spc:at
	spc:velocity
	sim:force
	bnd:radius
	sim:color
LAYOUT l_rig_spring
	pair:left
	pair:right
	bdy:length
	mat:stiffness
	mat:damping
	mat:flags

ATTRIBUTE moa:rig		WeakRecord
ATTRIBUTE moa:locator	WeakRecord
LAYOUT l_rig_to_locator
	moa:rig
	moa:locator
ATTRIBUTE spc:transform transform "=0 0 0 0 0 0 0 0 0 0 0 0"
LAYOUT l_locator
	spc:transform
LAYOUT l_contact
	spc:at
	moa:contact
	spc:normal
LAYOUT l_surface
	spc:transform
	spc:velocity
	is:plane

DEFAULTGROUP 2

#RECORD * mount
#	mount:tire "BT tire"
#	mount:rig "base_rig"
#RECORD * mount
#	mount:tire "PT tire"
#	mount:rig "base_rig"

DEFAULTGROUP 1

#RECORD * l_contact
#	spc:at	"0.2 0.2 0.4"

RECORD * ground
	ground:height -0.22

RECORD rig_control_surface l_surface
	spc:transform  "1 0 0 0 0 1 0 0 0 0 1 -0.22"

RECORD R_SELF rig
	name "base_rig"
	rig:inclination 0.0
	rig:load 600
	rig:alpha 0
	rig:velocity 60
	rig:kappa 0
	rig:free_wheel 1
	rig:solve_extract 1
	rig:structure RG_RIG
	rig:root attach_particle
	rig:floor rig_control_surface

# Single Wishbone is enough for Rig locked inclination
RECORD attach_particle l_rig_particle
	name "rig_attach_particle"
	spc:at "0 0 0"
	sim:mass 10.0
RECORD wishbone_root_A l_rig_constraint
	spc:at "10.2 10.5 0"
RECORD wishbone_root_B l_rig_constraint
	spc:at "-10.2 10.5 0"
RECORD spring_root l_rig_constraint
	spc:at "0 0.5 0.5"
RECORD wishbone_arm_A l_rig_spring
	pair:left attach_particle
	pair:right wishbone_root_A
RECORD wishbone_arm_B l_rig_spring
	pair:left attach_particle
	pair:right wishbone_root_B
RECORD main_spring_damper l_rig_spring
	pair:left attach_particle
	pair:right spring_root
	mat:stiffness 0
	mat:damping 0

DEFAULTGROUP 1
# Double Wishbone

RECORD upright_rear l_rig_particle
	spc:at "0 0 0"
RECORD upright_top l_rig_particle
	spc:at "0.1 0 0.1"
RECORD upright_bottom l_rig_particle
	spc:at "0.1 0 -0.1"

RECORD upper_wishbone_root_front l_rig_constraint
	spc:at "0.2 10.5 0.1"
RECORD upper_wishbone_root_rear l_rig_constraint
	spc:at "-0.2 10.5 0.1"
RECORD lower_wishbone_root_front l_rig_constraint
	spc:at "0.2 10.5 -0.1"
RECORD lower_wishbone_root_rear l_rig_constraint
	spc:at "-0.2 10.5 -0.1"
RECORD tie_rod_root l_rig_constraint
	spc:at "-0.15 10.5 0.05"

RECORD upper_wishbone_arm_front l_rig_spring
	pair:left upright_top
	pair:right upper_wishbone_root_front
RECORD upper_wishbone_arm_rear l_rig_spring
	pair:left upright_top
	pair:right upper_wishbone_root_rear

RECORD lower_wishbone_arm_front l_rig_spring
	pair:left upright_bottom
	pair:right lower_wishbone_root_front
RECORD lower_wishbone_arm_rear l_rig_spring
	pair:left upright_bottom
	pair:right lower_wishbone_root_rear

RECORD upright_top_edge l_rig_spring
	pair:left upright_top
	pair:right upright_rear
RECORD upright_bottom_edge l_rig_spring
	pair:left upright_bottom
	pair:right upright_rear
RECORD upright_front_edge l_rig_spring
	pair:left upright_top
	pair:right upright_bottom

RECORD dbl_spring_damper l_rig_spring
	pair:left upright_bottom
	pair:right spring_root
	mat:stiffness 0
	mat:damping 0

RECORD tie_rod l_rig_spring
	pair:left tie_rod_root
	pair:right upright_rear


DEFAULTGROUP 1

RECORD tire_frame pmount
	mount:tire "LT tire"
	mount:rig "base_rig"
	mount:p0	attach_particle
	mount:p1	wishbone_root_A
	mount:p2	wishbone_root_B
#	mount:p0	upright_rear
#	mount:p1	upright_top
#	mount:p2	upright_bottom

#RECORD LOC0 l_locator

#RECORD * l_rig_to_locator
#	moa:rig	R_SELF
#	moa:locator LOC0

DEFAULTGROUP 0

RECORD * mount
	mount:tire "LT tire"
	mount:rig "base_rig"

DEFAULTGROUP 1

RECORDGROUP RG_RIG 
	attach_particle
	wishbone_root_A
	wishbone_root_B
	spring_root
	wishbone_arm_A
	wishbone_arm_B
	main_spring_damper

	tire_frame

	upright_rear
	upright_top
	upright_bottom
	upper_wishbone_root_front
	upper_wishbone_root_rear
	lower_wishbone_root_front
	lower_wishbone_root_rear

	upper_wishbone_arm_front
	upper_wishbone_arm_rear
	lower_wishbone_arm_front
	lower_wishbone_arm_rear

	upright_top_edge
	upright_bottom_edge
	upright_front_edge

	dbl_spring_damper

	tie_rod_root
	tie_rod


RECORDGROUP 1
END
