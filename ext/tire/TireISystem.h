/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __tire_TireISystem_h__
#define __tire_TireISystem_h__

namespace fe
{
namespace ext
{

/// @brief TireI Model System
class FE_DL_EXPORT TireISystem :
	virtual public Stepper,
	public Initialize<TireISystem>
{
	public:
				TireISystem(void);
virtual			~TireISystem(void);
		void	initialize(void);

virtual void		compile(const t_note_id &a_note_id);
virtual	void		step(t_moa_real a_dt);
virtual	void		connectOrchestrator(sp<OrchestratorI> a_spOrchestrator);

	private:
		sp<Scope>						m_spScope;
		std::vector< Record >			m_r_tires;
		AsTireLive						m_asTireLive;
		AsContactLive					m_asContactLive;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __tire_TireISystem_h__ */

