/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __ironworks_WayRestraintOp_h__
#define __ironworks_WayRestraintOp_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Operator to restrict distance between locators

	@ingroup ironworks
*//***************************************************************************/
class FE_DL_EXPORT WayRestraintOp:
	public OperatorSurfaceCommon,
	public Initialize<WayRestraintOp>
{
	public:

					WayRestraintOp(void)									{}

virtual				~WayRestraintOp(void)									{}

		void		initialize(void);

					//* As HandlerI
virtual	void		handle(Record& a_rSignal);

	private:
};

} /* namespace ext */
} /* namespace fe */

#endif /* __ironworks_WayRestraintOp_h__ */

