/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <ironworks/ironworks.pmh>

#define FE_WRO_DEBUG		FALSE

using namespace fe;
using namespace fe::ext;

void WayRestraintOp::initialize(void)
{
	catalog<String>("label")="Iron WayRestraintOp";

	catalog<bool>("enable")=true;
	catalog<String>("enable","label")="Enable";

	catalog<Real>("envelope")=1.0;
	catalog<String>("envelope","label")="Envelope";

	catalog<String>("wayNameA")="anchor";
	catalog<String>("wayNameA","label")="Anchor Name";

	catalog<String>("wayNameB")="satelite";
	catalog<String>("wayNameB","label")="Satelite Name";

	catalog<Real>("minDist")=0.0;
	catalog<Real>("minDist","high")=100.0;
	catalog<Real>("minDist","max")=1e6;
	catalog<String>("minDist","label")="Minimum Distance";

	catalog<Real>("maxDist")=100.0;
	catalog<Real>("maxDist","high")=100.0;
	catalog<Real>("maxDist","max")=1e6;
	catalog<String>("maxDist","label")="Maximum Distance";

	catalog<SpatialVector>("translation")=SpatialVector(0.0,0.0,0.0);
	catalog<String>("translation","label")="Translation";
	catalog<String>("translation","IO")="output";

	catalog< sp<Component> >("Input Surface");
	catalog<bool>("Input Surface","optional")=true;
}

void WayRestraintOp::handle(Record& a_rSignal)
{
#if FE_WRO_DEBUG
	feLog("WayRestraintOp::handle\n");
#endif

	const String wayNameA=catalog<String>("wayNameA");
	const String wayNameB=catalog<String>("wayNameB");

	catalog<String>("summary")=wayNameA + " " + wayNameB;

	if(!catalog<bool>("enable"))
	{
		catalog<SpatialVector>("translation")=SpatialVector(0.0,0.0,0.0);
		catalog<String>("summary")+=" DISABLED";
		return;
	}

	const Real envelope=catalog<Real>("envelope");
	const Real maxDist=catalog<Real>("maxDist");
	const Real minDist=fe::minimum(maxDist,catalog<Real>("minDist"));

	if(wayNameA==wayNameB)
	{
		catalog<String>("error")+="both waypoints have the same name;";
		return;
	}

	sp<DrawI> spDrawGuide;
	if(!accessGuide(spDrawGuide,a_rSignal)) return;

	sp<SurfaceAccessibleI> spOutputAccessible;
	if(!accessOutput(spOutputAccessible,a_rSignal)) return;

	sp<SurfaceAccessibleI> spInputAccessible;
	if(!access(spInputAccessible,"Input Surface")) return;

	sp<SurfaceAccessorI> spOutputVertices;
	if(!access(spOutputVertices,spOutputAccessible,
			e_primitive,e_vertices)) return;

	sp<SurfaceAccessorI> spInputName;
	if(!access(spInputName,spInputAccessible,e_primitive,"name")) return;

	const I32 primitiveCount=spInputName->count();

	I32 primitiveIndexA= -1;
	I32 primitiveIndexB= -1;
	for(I32 primitiveIndex=0;primitiveIndex<primitiveCount;primitiveIndex++)
	{
		const String wayName=spInputName->string(primitiveIndex);

		if(wayNameA==wayName)
		{
			primitiveIndexA=primitiveIndex;
		}
		if(wayNameB==wayName)
		{
			primitiveIndexB=primitiveIndex;
		}
	}

	if(primitiveIndexA<0)
	{
		String message;
		message.sPrintf("waypoint %s not found;",wayNameA.c_str());
		catalog<String>("error")+=message;
		return;
	}

	if(primitiveIndexB<0)
	{
		String message;
		message.sPrintf("waypoint %s not found;",wayNameB.c_str());
		catalog<String>("error")+=message;
		return;
	}

	const SpatialVector pointA=
			spOutputVertices->spatialVector(primitiveIndexA,0);
	const SpatialVector pointB=
			spOutputVertices->spatialVector(primitiveIndexB,0);

	const SpatialVector delta=pointB-pointA;
	const Real distance=magnitude(delta);
	const SpatialVector towards=delta*(distance>0.0? 1.0/distance: 1.0);

	Real newDistance= -1.0;
	if(distance<minDist)
	{
		newDistance=minDist;
	}
	if(distance>maxDist)
	{
		newDistance=maxDist;
	}

	const Color red(1.0,0.0,0.0);
	const Color green(0.0,1.0,0.0);
	const Color blue(0.0,0.0,1.0);

	SpatialVector line[2];

	SpatialVector translation(0.0,0.0,0.0);
	if(newDistance>=0.0)
	{

		if(distance<1e-6)
		{
			catalog<String>("warning")+="waypoints are nearly conincident;";

			set(translation,0.0,newDistance,0.0);
		}
		else
		{
			translation=towards*(newDistance-distance);
		}

		translation*=envelope;

		const I32 vertexCount=spOutputVertices->subCount(primitiveIndexB);
		for(I32 vertexIndex=0;vertexIndex<vertexCount;vertexIndex++)
		{
			const SpatialVector point=spOutputVertices->spatialVector(
					primitiveIndexB,vertexIndex);

			spOutputVertices->set(primitiveIndexB,vertexIndex,
					point+translation);
		}

		if(newDistance==maxDist)
		{
			line[0]=pointA;
			line[1]=pointA+minDist*towards;

			spDrawGuide->drawLines(line,NULL,2,
					DrawI::e_strip,false,&green);

			line[0]=pointA+maxDist*towards;

			spDrawGuide->drawLines(line,NULL,2,
					DrawI::e_strip,false,&blue);

			line[1]=pointB;

			spDrawGuide->drawLines(line,NULL,2,
					DrawI::e_strip,false,&red);
		}
		else
		{
			line[0]=pointA;
			line[1]=pointB;

			spDrawGuide->drawLines(line,NULL,2,
					DrawI::e_strip,false,&green);

			line[0]=pointA+minDist*towards;

			spDrawGuide->drawLines(line,NULL,2,
					DrawI::e_strip,false,&red);

			line[1]=pointA+maxDist*towards;

			spDrawGuide->drawLines(line,NULL,2,
					DrawI::e_strip,false,&blue);
		}
	}
	else
	{
		line[0]=pointA;
		line[1]=pointA+minDist*towards;

		spDrawGuide->drawLines(line,NULL,2,
				DrawI::e_strip,false,&green);

		line[0]=pointA+maxDist*towards;

		spDrawGuide->drawLines(line,NULL,2,
				DrawI::e_strip,false,&blue);
	}

	catalog<SpatialVector>("translation")=translation;

#if FE_WRO_DEBUG
	feLog("WayRestraintOp::handle done\n");
#endif
}
