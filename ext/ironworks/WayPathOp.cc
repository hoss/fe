/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <ironworks/ironworks.pmh>

#define FE_WPT_DEBUG		FALSE
#define FE_WPT_VERBOSE		FALSE

using namespace fe;
using namespace fe::ext;

void WayPathOp::initialize(void)
{
	catalog<String>("label")="Iron WayPathOp";

	catalog<I32>("passes")=1;
	catalog<I32>("passes","high")=10;
	catalog<I32>("passes","min")=1;
	catalog<I32>("passes","max")=1000;

	catalog< sp<Component> >("Brush");
	catalog<String>("Brush","implementation")="DrawI";
	catalog<String>("Brush","visible")="any";
	catalog<String>("Brush","prompt")="Prompt here.";

	catalog< sp<Component> >("Input Surface");

	//* no auto-copy
	catalog< sp<Component> >("Output Curves");
	catalog<String>("Output Curves","IO")="output";

	m_spSolid=new DrawMode();
	m_spSolid->setDrawStyle(DrawMode::e_foreshadow);
	m_spSolid->setAntialias(TRUE);
	m_spSolid->setLit(FALSE);

	m_spOverlay=new DrawMode();
	m_spOverlay->setLineWidth(3.0);
	m_spOverlay->setLit(FALSE);
}

void WayPathOp::handle(Record& a_rSignal)
{
#if FE_WPT_DEBUG
	feLog("WayPathOp::handle\n");
#endif

	sp<DrawI> spDrawBrush;
	accessBrush(spDrawBrush,a_rSignal,e_quiet);

	sp<DrawI> spDrawOverlay;
	accessBrushOverlay(spDrawOverlay,a_rSignal,e_quiet);

	if(spDrawBrush.isValid())
	{
		m_brushed=TRUE;

		spDrawBrush->pushDrawMode(m_spSolid);
		spDrawOverlay->pushDrawMode(m_spOverlay);

		sp<ViewI> spView=spDrawOverlay->view();

		m_event.bind(windowEvent(a_rSignal));

#if FE_WPT_DEBUG
		feLog("%s\n",c_print(m_event));
#endif

		const Color black(0.0,0.0,0.0);
		const Color white(1.0,1.0,1.0);

		String text;
		text.sPrintf("curve length %.6G",m_totalLength);

		const SpatialVector location(8,64);
		drawLabel(spDrawOverlay,location,text,FALSE,2,white,NULL,&black);

		spDrawOverlay->popDrawMode();
		spDrawBrush->popDrawMode();

#if FE_WPT_DEBUG
	feLog("WayPathOp::handle brush done\n");
#endif
		return;
	}

	catalog<String>("summary")="";

	const I32 passes=catalog<I32>("passes");

	sp<DrawI> spOutputDraw;
	if(!accessDraw(spOutputDraw,a_rSignal)) return;

	sp<DrawI> spDrawGuide;
	accessGuide(spDrawGuide,a_rSignal);

	sp<SurfaceAccessibleI> spOutputAccessible;
	if(!accessOutput(spOutputAccessible,a_rSignal)) return;

	sp<SurfaceAccessibleI> spInputAccessible;
	if(!access(spInputAccessible,"Input Surface")) return;

	sp<SurfaceAccessorI> spInputVertices;
	if(!access(spInputVertices,spInputAccessible,
			e_primitive,e_vertices)) return;

	sp<SurfaceAccessorI> spInputName;
	if(!access(spInputName,spInputAccessible,
			e_primitive,"name")) return;

	sp<SurfaceAccessorI> spInputEnvelope;
	if(!access(spInputEnvelope,spInputAccessible,
			e_primitive,"envelope")) return;

	sp<SurfaceAccessorI> spInputSamples;
	if(!access(spInputSamples,spInputAccessible,
			e_primitive,"samples")) return;

	sp<SurfaceAccessorI> spInputRadius;
	if(!access(spInputRadius,spInputAccessible,
			e_primitive,"radius")) return;

	sp<SurfaceAccessorI> spInputAlignment;
	if(!access(spInputAlignment,spInputAccessible,
			e_primitive,"alignment")) return;

	sp<SurfaceAccessorI> spInputSpread;
	if(!access(spInputSpread,spInputAccessible,
			e_primitive,"spread")) return;

	sp<SurfaceAccessorI> spInputAdvance;
	if(!access(spInputAdvance,spInputAccessible,
			e_primitive,"advance")) return;

	sp<SurfaceAccessorI> spInputStick;
	if(!access(spInputStick,spInputAccessible,
			e_primitive,"stick")) return;

	sp<SurfaceAccessorI> spInputLinearityIn;
	if(!access(spInputLinearityIn,spInputAccessible,
			e_primitive,"linearityIn")) return;

	sp<SurfaceAccessorI> spInputLinearityOut;
	if(!access(spInputLinearityOut,spInputAccessible,
			e_primitive,"linearityOut")) return;

	sp<SurfaceAccessorI> spInputMinDistIn;
	if(!access(spInputMinDistIn,spInputAccessible,
			e_primitive,"minDistIn")) return;

	sp<SurfaceAccessorI> spInputMaxDistIn;
	if(!access(spInputMaxDistIn,spInputAccessible,
			e_primitive,"maxDistIn")) return;

//	sp<SurfaceAccessorI> spInputContactP;
//	if(!access(spInputContactP,spInputAccessible,
//			e_primitive,"contactP")) return;

	sp<SurfaceAccessorI> spInputContactN;
	if(!access(spInputContactN,spInputAccessible,
			e_primitive,"contactN")) return;

	const I32 primitiveCount=spInputVertices->count();

	SpatialVector* inputArray=new SpatialVector[primitiveCount];
	SpatialVector* stepArray0=new SpatialVector[primitiveCount];
	SpatialVector* stepArray1=new SpatialVector[primitiveCount];
	SpatialVector* stepArray2=new SpatialVector[primitiveCount];
	Real* radiusArray=new Real[primitiveCount];
	Real* envelopeArray=new Real[primitiveCount];

	I32 sampleTotal=0;

	for(I32 primitiveIndex=0;primitiveIndex<primitiveCount;primitiveIndex++)
	{
		if(interrupted())
		{
			return;
		}

		const U32 subCount=spInputVertices->subCount(primitiveIndex);
		if(!subCount)
		{
			continue;
		}

		const SpatialVector point=
				spInputVertices->spatialVector(primitiveIndex,0);

		inputArray[primitiveIndex]=point;
		stepArray0[primitiveIndex]=point;
		stepArray1[primitiveIndex]=point;
		stepArray2[primitiveIndex]=point;

		radiusArray[primitiveIndex]=spInputRadius->real(primitiveIndex);
		envelopeArray[primitiveIndex]=spInputEnvelope->real(primitiveIndex);

		sampleTotal+=spInputSamples->integer(primitiveIndex);
	}

	const Color red(1.0,0.0,0.0);
	const Color green(0.0,1.0,0.0);
	const Color blue(0.0,0.0,1.0);
	const Color cyan(0.0,1.0,1.0);

	//* NOTE there may be excess
	SpatialVector* pointArray=new SpatialVector[sampleTotal];
	SpatialVector* normalArray=new SpatialVector[sampleTotal];
	Real* linearityInArray=new Real[sampleTotal];
	Real* linearityOutArray=new Real[sampleTotal];
	Real* minDistInArray=new Real[sampleTotal];
	Real* maxDistInArray=new Real[sampleTotal];
	I32* lookupArray=new I32[sampleTotal];

	U32 outputCount=0;

	for(I32 pass=0;pass<passes;pass++)
	{
		outputCount=0;

		for(I32 primitiveIndex=0;primitiveIndex<primitiveCount;primitiveIndex++)
		{
			if(interrupted())
			{
				return;
			}

			const U32 subCount=spInputVertices->subCount(primitiveIndex);
			if(!subCount)
			{
				continue;
			}

			const SpatialVector center=
					spInputVertices->spatialVector(primitiveIndex,0);
			const SpatialVector animX=
					spInputVertices->spatialVector(primitiveIndex,1)-center;

			const Real linearityIn=
					spInputLinearityIn->real(primitiveIndex);
			const Real linearityOut=
					spInputLinearityOut->real(primitiveIndex);

			const Real minDistIn=
					spInputMinDistIn->real(primitiveIndex);
			const Real maxDistIn=
					spInputMaxDistIn->real(primitiveIndex);

			const I32 samples=spInputSamples->integer(primitiveIndex);
			const Real radius=spInputRadius->real(primitiveIndex);
			const BWORD simple=(samples==1 && radius<=0.0);

			const Real envelope=envelopeArray[primitiveIndex];

			if(simple && envelope==1.0)
			{
				lookupArray[outputCount]=primitiveIndex;
				normalArray[outputCount]=animX;
				linearityInArray[outputCount]=linearityIn;
				linearityOutArray[outputCount]=linearityOut;
				minDistInArray[outputCount]=minDistIn;
				maxDistInArray[outputCount]=maxDistIn;
				pointArray[outputCount++]=center;
				stepArray0[primitiveIndex]=center;
				stepArray1[primitiveIndex]=center;
				stepArray2[primitiveIndex]=center;
				continue;
			}

			I32 previousIndex=primitiveIndex-1;
			I32 nextIndex=primitiveIndex+1;
			SpatialVector next;
			SpatialVector previous;
			SpatialVector recenter=stepArray1[primitiveIndex];
			if(pass)
			{
				previous=(previousIndex>=0)?
						stepArray2[previousIndex]:
						recenter;
				next=(nextIndex<primitiveCount)?
						stepArray0[nextIndex]:
						recenter;
			}
			else
			{
#if FE_WPT_VERBOSE
				feLog("previous:\n");
#endif
				previous=envelopePoint(
						(previousIndex>=0)? previousIndex: primitiveIndex,
						primitiveCount,inputArray,envelopeArray);
#if FE_WPT_VERBOSE
				feLog("current:\n");
#endif
				recenter=envelopePoint(primitiveIndex,
						primitiveCount,inputArray,envelopeArray);
#if FE_WPT_VERBOSE
				feLog("next:\n");
#endif
				next=envelopePoint(
						(nextIndex<primitiveCount)? nextIndex: primitiveIndex,
						primitiveCount,inputArray,envelopeArray);
			}

			const SpatialVector nearest=nearPoint(previous,next,recenter);

			if(simple)
			{
				const SpatialVector point=
						envelope*center+(Real(1)-envelope)*nearest;

#if FE_WPT_VERBOSE
				feLog("pass %d/%d prim %d/%d"
						" previous %s next %s recenter %s"
						" nearest %s point %s\n",
						pass,passes,primitiveIndex,primitiveCount,
						c_print(previous),c_print(next),
						c_print(recenter),c_print(nearest),c_print(point));
#endif

				lookupArray[outputCount]=primitiveIndex;
				normalArray[outputCount]=animX;
				linearityInArray[outputCount]=linearityIn;
				linearityOutArray[outputCount]=linearityOut;
				minDistInArray[outputCount]=minDistIn;
				maxDistInArray[outputCount]=maxDistIn;
				pointArray[outputCount++]=point;
				stepArray0[primitiveIndex]=point;
				stepArray1[primitiveIndex]=point;
				stepArray2[primitiveIndex]=point;

				if(spDrawGuide.isValid())
				{
					SpatialVector line[2];
					Color color[2];

					line[0]=recenter;
					line[1]=nearest;

					color[0]=red;
					color[1]=green;

					spDrawGuide->drawLines(line,NULL,2,
							DrawI::e_strip,true,color);

					line[0]=previous;
					line[1]=next;

					color[0]=blue;
					color[1]=cyan;

					spDrawGuide->drawLines(line,NULL,2,
							DrawI::e_strip,true,color);

				}

				continue;
			}

			SpatialVector previousMod=previous;
			SpatialVector nextMod=next;
			matchLengths(recenter,previousMod,nextMod);

			const SpatialVector nearestMod=
					nearPoint(previousMod,nextMod,recenter);

			I32 earlierIndex=primitiveIndex-1;
			while(earlierIndex>0 && radiusArray[earlierIndex]>0.0)
			{
				earlierIndex--;
			}
			const SpatialVector earlier=(earlierIndex>=0)?
					stepArray1[earlierIndex]:
					recenter;

			I32 laterIndex=primitiveIndex+1;
			while(laterIndex<primitiveCount-1 &&
					radiusArray[laterIndex]>0.0)
			{
				laterIndex++;
			}
			const SpatialVector later=(laterIndex<primitiveCount)?
					(pass? stepArray1[laterIndex]:
					spInputVertices->spatialVector(laterIndex,0)):
					recenter;

//			const SpatialVector contactP=
//					spInputContactP->spatialVector(primitiveIndex);
			const SpatialVector contactN=
					spInputContactN->spatialVector(primitiveIndex);
			const Real alignment=spInputAlignment->real(primitiveIndex);
			const Real spread=spInputSpread->real(primitiveIndex);
			const Real advance=spInputAdvance->real(primitiveIndex);
			const BWORD stick=spInputStick->integer(primitiveIndex);

			const SpatialVector animZ=
					spInputVertices->spatialVector(primitiveIndex,2)-center;

			const SpatialVector norm=cross(animZ,animX);

			SpatialVector earlierMod=earlier;
			SpatialVector laterMod=later;
			matchLengths(recenter,earlierMod,laterMod);

			const SpatialVector longDir=unitSafe(laterMod-earlierMod);
			const SpatialVector side=unitSafe(cross(longDir,norm));
			const SpatialVector aligned=unitSafe(contactN);
			const SpatialVector rimDir=
					unitSafe(side*(1.0-alignment)+aligned*alignment);
			const SpatialVector rimPoint=center+radius*rimDir;

			const SpatialVector candidate=rimPoint-nearestMod;
			const Real distance=magnitude(candidate);
			const SpatialVector unitCandidate=
					(distance>0.0)? candidate/distance: candidate;
			const Real permitDot=dot(unitCandidate,side);

			Real permitted=0.0;
			SpatialVector permittedCandidate(0.0,0.0,0.0);

			if(permitDot>0.0)
			{
				permitted=fe::maximum(permitDot,Real(0));
				permittedCandidate=permitted*candidate;
			}
			else if(stick)
			{
				permitted=1.0;

				const SpatialVector rimPoint2=center-radius*rimDir;
				permittedCandidate=rimPoint2-nearestMod;
			}

			const SpatialVector nearestBlend=
					nearest+envelope*permitted*(nearestMod-nearest);

			const SpatialVector point=
					nearestBlend+envelope*permittedCandidate;

			if(spDrawGuide.isValid())
			{
				SpatialVector line[2];

				line[0]=center;
				line[1]=rimPoint;

				spDrawGuide->drawLines(line,NULL,2,
						DrawI::e_strip,false,&red);

				line[0]=earlierMod;
				line[1]=laterMod;

				spDrawGuide->drawLines(line,NULL,2,
						DrawI::e_strip,false,&green);

				line[0]=nearestBlend;
				line[1]=point;

				spDrawGuide->drawLines(line,NULL,2,
						DrawI::e_strip,false,&blue);

				line[0]=nearest;
				line[1]=nearestMod;

				spDrawGuide->drawLines(line,NULL,2,
						DrawI::e_strip,false,&cyan);
			}

			if(samples>1)
			{
				const SpatialVector target=nearestBlend+permittedCandidate;
				const SpatialVector arm=target-center;
				const Real permittedAngle=envelope*permitted*spread*degToRad;

				if(spDrawGuide.isValid())
				{
					SpatialVector line[2];
					Color color[2];

					line[0]=center;
					line[1]=target;

					color[0]=red;
					color[1]=green;

					spDrawGuide->drawLines(line,NULL,2,
							DrawI::e_strip,true,color);
				}

				//* TODO make it so that repeating points are ok downstream
				const Real tiny=1e-3;

				for(I32 sample=0;sample<samples;sample++)
				{
					const Real ratio=sample/Real(samples-1);

					SpatialQuaternion rotation(
							(ratio-0.5)*permittedAngle,norm);

					SpatialVector rotArm;
					rotateVector(rotation,arm,rotArm);

					const SpatialVector shortDir=unitSafe(next-previous);
					const SpatialVector subPoint=nearestBlend+
							envelope*(center+rotArm-nearestBlend)+
							ratio*(tiny*shortDir+permitted*advance*norm);

					const Real linearity=linearityIn+
							(linearityOut-linearityIn)*ratio;
					linearityInArray[outputCount]=linearity;
					linearityOutArray[outputCount]=linearity;
					minDistInArray[outputCount]=minDistIn;
					maxDistInArray[outputCount]=maxDistIn;

					lookupArray[outputCount]=primitiveIndex;
					normalArray[outputCount]=unitSafe(rotArm);
					pointArray[outputCount++]=subPoint;

					if(!sample)
					{
						stepArray0[primitiveIndex]=subPoint;
					}
					else if(sample==samples-1)
					{
						stepArray2[primitiveIndex]=subPoint;
					}
				}
			}
			else
			{
				lookupArray[outputCount]=primitiveIndex;
				normalArray[outputCount]=animX;
				linearityInArray[outputCount]=linearityIn;
				linearityOutArray[outputCount]=linearityOut;
				minDistInArray[outputCount]=minDistIn;
				maxDistInArray[outputCount]=maxDistIn;
				pointArray[outputCount++]=point;

				stepArray0[primitiveIndex]=point;
				stepArray2[primitiveIndex]=point;
			}

			stepArray1[primitiveIndex]=point;
		}
	}

	delete[] inputArray;
	delete[] stepArray0;
	delete[] stepArray1;
	delete[] stepArray2;
	delete[] radiusArray;
	delete[] envelopeArray;

	m_totalLength=0.0;
	if(outputCount>1)
	{
		SpatialVector lastPoint=pointArray[0];
		for(U32 outputIndex=1;outputIndex<outputCount;outputIndex++)
		{
			const SpatialVector point=pointArray[outputIndex];

			m_totalLength+=magnitude(point-lastPoint);

			lastPoint=point;
		}
	}

	spOutputDraw->drawLines(pointArray,NULL,outputCount,
			DrawI::e_strip,false,NULL);

	sp<SurfaceAccessorI> spOutputNormal;
	access(spOutputNormal,spOutputAccessible,e_point,e_normal);
	if(spOutputNormal.isValid())
	{
		for(U32 outputIndex=0;outputIndex<outputCount;outputIndex++)
		{
			spOutputNormal->set(outputIndex,normalArray[outputIndex]);
		}
	}

	sp<SurfaceAccessorI> spOutputLinearityIn;
	access(spOutputLinearityIn,spOutputAccessible,e_point,"linearityIn");
	if(spOutputLinearityIn.isValid())
	{
		for(U32 outputIndex=0;outputIndex<outputCount;outputIndex++)
		{
			spOutputLinearityIn->set(outputIndex,
					linearityInArray[outputIndex]);
		}
	}

	sp<SurfaceAccessorI> spOutputLinearityOut;
	access(spOutputLinearityOut,spOutputAccessible,e_point,"linearityOut");
	if(spOutputLinearityOut.isValid())
	{
		for(U32 outputIndex=0;outputIndex<outputCount;outputIndex++)
		{
			spOutputLinearityOut->set(outputIndex,
					linearityOutArray[outputIndex]);
		}
	}

	sp<SurfaceAccessorI> spOutputMinDistIn;
	access(spOutputMinDistIn,spOutputAccessible,e_point,"minDistIn");
	if(spOutputMinDistIn.isValid())
	{
		for(U32 outputIndex=0;outputIndex<outputCount;outputIndex++)
		{
			spOutputMinDistIn->set(outputIndex,
					minDistInArray[outputIndex]);
		}
	}

	sp<SurfaceAccessorI> spOutputMaxDistIn;
	access(spOutputMaxDistIn,spOutputAccessible,e_point,"maxDistIn");
	if(spOutputMaxDistIn.isValid())
	{
		for(U32 outputIndex=0;outputIndex<outputCount;outputIndex++)
		{
			spOutputMaxDistIn->set(outputIndex,
					maxDistInArray[outputIndex]);
		}
	}

	sp<SurfaceAccessorI> spOutputName;
	access(spOutputName,spOutputAccessible,e_point,"waypoint");
	if(spOutputName.isValid())
	{
		for(U32 outputIndex=0;outputIndex<outputCount;outputIndex++)
		{
			const I32 lookupIndex=lookupArray[outputIndex];

			const String wayName=spInputName->string(lookupIndex);

			spOutputName->set(outputIndex,wayName);
		}
	}

	delete[] pointArray;
	delete[] normalArray;
	delete[] linearityInArray;
	delete[] linearityOutArray;
	delete[] minDistInArray;
	delete[] maxDistInArray;
	delete[] lookupArray;

	String summary;
	summary.sPrintf("length %.1f",m_totalLength);
	catalog<String>("summary")=summary;

#if FE_WPT_DEBUG
	feLog("WayPathOp::handle done\n");
#endif
}

void WayPathOp::matchLengths(const SpatialVector a_center,
	SpatialVector& a_rArm1,SpatialVector& a_rArm2)
{
	const SpatialVector delta1=a_rArm1-a_center;
	const SpatialVector delta2=a_rArm2-a_center;

	const Real dist1=magnitude(delta1);
	const Real dist2=magnitude(delta2);
	const Real minDist=fe::minimum(dist1,dist2);

	if(minDist>0.0)
	{
		a_rArm1=a_center+delta1*(minDist/dist1);
		a_rArm2=a_center+delta2*(minDist/dist2);
	}
}

SpatialVector WayPathOp::envelopePoint(
	const I32 a_primitiveIndex,const I32 a_primitiveCount,
	const SpatialVector a_inputArray[],const Real a_envelopeArray[])
{
	const Real tiny=1e-3;

	if(a_envelopeArray[a_primitiveIndex]>(Real(1)-tiny))
	{
		return a_inputArray[a_primitiveIndex];
	}

	Real* weightArray=new Real[a_primitiveCount];
	Real* addArray=new Real[a_primitiveCount];

	for(I32 index=0;index<a_primitiveCount;index++)
	{
		weightArray[index]=0.0;
		addArray[index]=0.0;
	}
	addArray[a_primitiveIndex]=1.0;

	BWORD changing=TRUE;
	while(changing)
	{
		changing=FALSE;
		for(I32 index=0;index<a_primitiveCount;index++)
		{
			const Real envelope=a_envelopeArray[index];
			Real &rAdd=addArray[index];
			if(rAdd>tiny)
			{
				changing=TRUE;

				const Real envAdd=envelope*rAdd;
				weightArray[index]+=envAdd;
				const Real split=0.5*(rAdd-envAdd);
				if(index>0)
				{
					addArray[index-1]+=split;
				}
				else
				{
					weightArray[index]+=split;
				}
				if(index<a_primitiveCount-1)
				{
					addArray[index+1]+=split;
				}
				else
				{
					weightArray[index]+=split;
				}
			}

			rAdd=0.0;
		}
	}

	Real weightTotal=0.0;
	for(I32 index=0;index<a_primitiveCount;index++)
	{
		weightTotal+=weightArray[index];
	}

	SpatialVector result(0.0,0.0,0.0);
	for(I32 index=0;index<a_primitiveCount;index++)
	{
		result+=a_inputArray[index]*(weightArray[index]/weightTotal);

#if FE_WPT_VERBOSE
		feLog("prim %d/%d weight %.6G/%.6G input %s sum %s\n",
				index,a_primitiveCount,weightArray[index],weightTotal,
				c_print(a_inputArray[index]),c_print(result));
#endif
	}

	delete[] weightArray;
	delete[] addArray;

	return result;
}

SpatialVector WayPathOp::nearPoint(const SpatialVector& a_rPrevious,
	const SpatialVector& a_rNext,const SpatialVector& a_rFrom)
{
	const SpatialVector segment=a_rNext-a_rPrevious;
	const Real dist=magnitude(segment);
	const SpatialVector dir=segment*(dist>Real(0)? Real(1)/dist: Real(1));
	const Real tiny=1e-2;
	const Real along=fe::minimum(dist-tiny,
			fe::maximum(tiny,dot(dir,a_rFrom-a_rPrevious)));

	return a_rPrevious+dir*along;
}
