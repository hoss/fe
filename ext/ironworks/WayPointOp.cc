/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <ironworks/ironworks.pmh>

#define FE_WPT_DEBUG		FALSE

using namespace fe;
using namespace fe::ext;

WayPointOp::WayPointOp(void):
	m_shareFrame(-1.0),
	m_lastFrame(0.0),
	m_picking(1),
	m_dragging(FALSE),
	m_brushed(FALSE),
	m_pickFace(-1)
{
	setIdentity(m_shareTransform);
}

WayPointOp::~WayPointOp(void)
{
	changeSharing(FALSE);
}

void WayPointOp::initialize(void)
{
	setSharingName("WayPointOp:sharing");
	changeSharing(TRUE);

	catalog<String>("label")="Iron WayPointOp";

	catalog<bool>("enable")=true;
	catalog<String>("enable","label")="Enable";

	catalog<Real>("envelope")=1.0;
	catalog<String>("envelope","label")="Envelope";

	catalog<Real>("blend")=0.0;
	catalog<String>("blend","label")="Blend In";
	catalog<String>("blend","hint")=
			"If named waypoint already exists,"
			" blend in the new settings by this much."
			"For new waypoints, the blend value is ignored.";

	catalog<String>("name")="waypoint";
	catalog<String>("name","label")="Name";
	catalog<String>("name","IO")="input output";
	catalog<bool>("name","joined")=true;

	catalog<bool>("autoname")=true;
	catalog<String>("autoname","label")="Auto Name";

	catalog<String>("driverMethod")="world";
	catalog<String>("driverMethod","label")="Drive with";
	catalog<String>("driverMethod","choice:0")="world";
	catalog<String>("driverMethod","label:0")="World";
	catalog<String>("driverMethod","choice:1")="surface";
	catalog<String>("driverMethod","label:1")="Surface";
	catalog<String>("driverMethod","choice:2")="previous";
	catalog<String>("driverMethod","label:2")="Previous Waypoint";
//	catalog<String>("driverMethod","choice:3")="joint";
//	catalog<String>("driverMethod","label:3")="Joint";
	catalog<bool>("driverMethod","joined")=true;
	catalog<String>("driverMethod","hint")="Parent space of motion.";

	catalog<bool>("driverRotation")=true;
	catalog<String>("driverRotation","label")="Use Rotation";

	catalog<I32>("face")=0;
	catalog<I32>("face","high")=100;
	catalog<I32>("face","max")=INT_MAX;
	catalog<String>("face","label")="Face";
	catalog<String>("face","IO")="input output";

	catalog<SpatialVector>("bary")=SpatialVector(1.0,0.0,0.0);
	catalog<String>("bary","label")="Barycenter";
	catalog<String>("bary","IO")="input output";

	catalog<SpatialVector>("pivot")=SpatialVector(0.0,0.0,0.0);
	catalog<String>("pivot","label")="Offset Pivot";
	catalog<String>("pivot","IO")="input output";

	catalog<SpatialVector>("rotate")=SpatialVector(0.0,0.0,0.0);
	catalog<String>("rotate","label")="Offset Rotate";
	catalog<String>("rotate","IO")="input output";

	catalog<SpatialVector>("translate")=SpatialVector(0.0,0.0,0.0);
	catalog<String>("translate","label")="Offset Translate";
	catalog<String>("translate","IO")="input output";

	catalog<I32>("samples")=1;
	catalog<I32>("samples","min")=1;
	catalog<I32>("samples","high")=16;
	catalog<I32>("samples","max")=100;
	catalog<String>("samples","label")="Samples";
	catalog<String>("samples","IO")="input output";

	catalog<Real>("radius")=0.0;
	catalog<Real>("radius","high")=10.0;
	catalog<Real>("radius","max")=1e3;
	catalog<String>("radius","label")="Radius";
	catalog<String>("radius","IO")="input output";

	catalog<Real>("alignment")=0.0;
	catalog<Real>("alignment","high")=1.0;
	catalog<String>("alignment","label")="Contact Alignment";
	catalog<String>("alignment","IO")="input output";

	catalog<Real>("spread")=0.0;
	catalog<Real>("spread","high")=360.0;
	catalog<Real>("spread","max")=1e4;
	catalog<String>("spread","label")="Spread Angle";
	catalog<String>("spread","IO")="input output";

	catalog<Real>("advance")=0.0;
	catalog<Real>("advance","min")= -1e6;
	catalog<Real>("advance","low")=0.0;
	catalog<Real>("advance","high")=10.0;
	catalog<Real>("advance","max")=1e6;
	catalog<String>("advance","label")="Advance";
	catalog<String>("advance","IO")="input output";

	catalog<bool>("stick")=false;
	catalog<String>("stick","label")="Stick";
	catalog<String>("stick","IO")="input output";

	catalog<Real>("linearityIn")=0.0;
	catalog<String>("linearityIn","label")="Linearity In";
	catalog<String>("linearityIn","IO")="input output";
	catalog<bool>("linearityIn","joined")=true;

	catalog<Real>("linearityOut")=0.0;
	catalog<String>("linearityOut","label")="Out";
	catalog<String>("linearityOut","IO")="input output";

	catalog<Real>("minDistIn")=0.0;
	catalog<Real>("minDistIn","high")=10.0;
	catalog<Real>("minDistIn","max")=1e6;
	catalog<String>("minDistIn","label")="Min Dist In";
	catalog<String>("minDistIn","IO")="input output";
	catalog<bool>("minDistIn","joined")=true;

	catalog<Real>("maxDistIn")=0.0;
	catalog<Real>("maxDistIn","high")=10.0;
	catalog<Real>("maxDistIn","max")=1e6;
	catalog<String>("maxDistIn","label")="Max";
	catalog<String>("maxDistIn","IO")="input output";

	catalog<bool>("setValue")=false;
	catalog<String>("setValue","label")="Set Value";
	catalog<bool>("setValue","joined")=true;

	catalog<String>("valueAttr")="value";
	catalog<String>("valueAttr","label")="Named";
	catalog<bool>("valueAttr","joined")=true;

	catalog<SpatialVector>("value")=SpatialVector(0.0,0.0,0.0);
	catalog<String>("value","label")="To";

	catalog< sp<Component> >("Brush");
	catalog<String>("Brush","implementation")="DrawI";
	catalog<String>("Brush","visible")="any";
	catalog<String>("Brush","prompt")=
			"ESC toggles surface picking or manipulation."
			"  Enter toggles pivot/deform (for manipulator)."
			"  or peer/anchor (for picking)."
			"  LMB picks peer/anchor or manipulates."
			"  MMB remanipulates last control."
			"  Shift slows manipulation.";

	catalog< sp<Component> >("Input Surface");
	catalog<bool>("Input Surface","optional")=true;

	catalog< sp<Component> >("Driver Surface");
	catalog<bool>("Driver Surface","optional")=true;

	catalog< sp<Component> >("Output Surface");
	catalog<String>("Output Surface","IO")="output";
	catalog<String>("Output Surface","copy")="Input Surface";
	catalog<bool>("Output Surface","temporal")=true;
	catalog<I32>("Output Surface","serial")=0;

	m_spSolid=new DrawMode();
	m_spSolid->setDrawStyle(DrawMode::e_foreshadow);
	m_spSolid->setLineWidth(2.0);
	m_spSolid->setAntialias(TRUE);
	m_spSolid->setLit(FALSE);

	m_spOverlay=new DrawMode();
	m_spOverlay->setPointSize(4.0);
	m_spOverlay->setLineWidth(2.0);
	m_spOverlay->setLit(FALSE);

	m_spManipulator=registry()->create("*.TransformManipulator");

	setIdentity(m_locator);
	setIdentity(m_pivot);
	setIdentity(m_transform);
}

void WayPointOp::handle(Record& a_rSignal)
{
#if FE_WPT_DEBUG
	feLog("WayPointOp::handle\n");
#endif

	String& rWayName=catalog<String>("name");

	if(catalog<bool>("autoname"))
	{
		String subname=name().replace(".*?_(.*)","\\1");
		if(subname.empty() || subname==name())
		{
			subname=name().replace("WayPointOp","");

			if(subname.empty() || subname==name())
			{
				subname=name().replace("WayPoint","");
			}
		}

		if(!subname.empty())
		{
			rWayName=subname;
		}
	}

	const String driverMethod=catalog<String>("driverMethod");
	const BWORD driverRotation=catalog<bool>("driverRotation");

	sp<DrawI> spDrawBrush;
	accessBrush(spDrawBrush,a_rSignal,e_quiet);

	sp<DrawI> spDrawOverlay;
	accessBrushOverlay(spDrawOverlay,a_rSignal,e_quiet);

	const Real foreshadow=0.4;

	if(m_spManipulator.isValid())
	{
		sp<Catalog> spCatalog=m_spManipulator;
		FEASSERT(spCatalog.isValid());

		spCatalog->catalog<bool>("deformScale")=false;

		spCatalog->catalog<bool>("pivotRotate")=false;
		spCatalog->catalog<bool>("pivotScale")=false;

		spCatalog->catalog<bool>("gripLabels")=false;
		spCatalog->catalog<Real>("dimming")=0.6;
		spCatalog->catalog<Real>("foreshadow")=foreshadow;
		spCatalog->catalog<Real>("aura")=0.5;
	}

	if(spDrawBrush.isValid())
	{
		const Real alpha=foreshadow;

		const Color lightred(1.0,0.3,0.3);
		const Color lightgreen(0.3,1.0,0.3);
		const Color lightblue(0.3,0.3,1.0);
		const Color blue(0.0,0.0,1.0);
		const Color yellow(1.0,1.0,0.0);

		const Color black(0.0,0.0,0.0,alpha);
		const Color white(1.0,1.0,1.0,alpha);
		const Color green(0.0,1.0,0.0,alpha);

		spDrawBrush->pushDrawMode(m_spSolid);
		spDrawOverlay->pushDrawMode(m_spOverlay);

		sp<ViewI> spView=spDrawOverlay->view();
		const Box2i viewport=spView->viewport();
		const I32 width=viewport.size()[0];
		const I32 height=viewport.size()[1];

		sp<CameraI> spCamera=spView->camera();
		SpatialTransform cameraMatrix=spCamera->cameraMatrix();
		SpatialTransform cameraTransform;
		invert(cameraTransform,cameraMatrix);
		const SpatialVector cameraPos=cameraTransform.column(3);
		const SpatialVector cameraDir= -cameraTransform.column(2);

		m_event.bind(windowEvent(a_rSignal));

#if FE_WPT_DEBUG
		feLog("%s\n",c_print(m_event));
#endif

//		if(m_event.isExpose())
//		{
//			return;
//		}

		const WindowEvent::MouseButtons buttons=m_event.mouseButtons();

		m_brushed=(buttons&(WindowEvent::e_mbLeft|WindowEvent::e_mbMiddle));

		//* m_picking 0=not picking 1=peer picking 2=anchor picking

		if(m_event.isKeyPress(WindowEvent::e_keyEscape))
		{
			m_picking=!m_picking;
		}

		if(m_picking && m_event.isKeyPress(WindowEvent::e_keyCarriageReturn))
		{
			m_picking=3-m_picking;
		}

		if(m_event.isKeyPress(WindowEvent::e_itemAny))
		{
			setAnchor();
		}
		else if(!m_event.isMouseMove() && !m_event.isExpose())
		{
			catalog<String>("Brush","cook")="once";
		}

		//* start dragging
		if(!m_dragging && m_event.isMouseDrag())
		{
			m_changing=m_picking? "WayPointOp pick": "WayPointOp manipulation";
			anticipate(m_changing);
		}
		if(m_dragging && m_event.isMouseRelease())
		{
			resolve(m_changing);
		}

		m_dragging=m_event.isMouseDrag();

		const Real axisScale=0.3;			//* TODO

		const Real anchorScale=1.0;			//* TODO
		const U32 anchorResolution=16;		//* TODO
		const Color anchorColor(0.5,0.5,1.0,0.5);

		if(!m_picking && m_spManipulator.isValid())
		{
			const Real axisPixels=axisScale*height;
			const Real gripScale=
					spView->worldSize(m_locator.translation(),axisPixels);

			sp<Catalog> spCatalog=m_spManipulator;
			FEASSERT(spCatalog.isValid());

			spCatalog->catalog<Real>("anchorScale")=anchorScale;
			spCatalog->catalog<Real>("gripScale")=gripScale;

			m_spManipulator->bindOverlay(spDrawOverlay);
			m_spManipulator->handle(a_rSignal);
			m_spManipulator->draw(spDrawBrush,NULL);

			getAnchor();
		}

		m_highlightNode="";
		if(m_picking==1)
		{
			m_highlightNode=scanPeers(spDrawOverlay,cameraPos,cameraDir,
					FALSE,FALSE,m_event.mouseX(),m_event.mouseY());

			if(m_event.isMousePress(WindowEvent::e_itemLeft))
			{
				selectNode(m_highlightNode);
			}
		}

		if(m_spDriver.isValid())
		{
			const Real bigRadius=6.0;
			const Real smallRadius=2.0;
			const Real normalPixels=64.0;

			const SpatialVector& rRayOrigin=rayOrigin(a_rSignal);
			const SpatialVector& rRayDirection=rayDirection(a_rSignal);

#if FE_WPT_DEBUG
			feLog("origin %s dir %s\n",
					c_print(rRayOrigin),c_print(rRayDirection));
#endif

			I32 triangleIndex=catalog<I32>("face");
			SpatialBary bary=catalog<SpatialVector>("bary");
			if(m_pickFace>=0)
			{
				triangleIndex=m_pickFace;
				bary=m_pickBary.operator SpatialVector();
			}

			if(m_picking==2)
			{
				const Real maxDistance= -1.0;
				sp<SurfaceI::ImpactI> spImpact=m_spDriver->rayImpact(
						rRayOrigin,rRayDirection,maxDistance);
				if(spImpact.isValid())
				{
					const SpatialVector intersection=spImpact->intersection();
					const SpatialVector normal=spImpact->normal();

#if FE_WPT_DEBUG
					feLog("intersection %s\n",
							c_print(intersection));
#endif

					const Real normalScale=spView->worldSize(
							m_spDriver->center(),normalPixels);

					SpatialVector line[2];
					line[0]=intersection;
					line[1]=intersection+normalScale*normal;

					spDrawBrush->drawLines(line,NULL,2,
							DrawI::e_strip,false,&lightgreen);

					const SpatialVector du=spImpact->du();
					const SpatialVector dv=spImpact->dv();

					line[1]=intersection+normalScale*du;
					spDrawBrush->drawLines(line,NULL,2,
							DrawI::e_strip,false,&lightred);

					line[1]=intersection+normalScale*dv;
					spDrawBrush->drawLines(line,NULL,2,
							DrawI::e_strip,false,&lightblue);

					drawDot(spView,spDrawOverlay,intersection,bigRadius,blue);

					sp<SurfaceSearchable::Impact> spSearchableImpact=spImpact;
					if(spSearchableImpact.isValid())
					{
#if FE_WPT_DEBUG
						feLog("prim %d tri %d bary %s\nuv %s du %s dv %s\n",
								spSearchableImpact->primitiveIndex(),
								spSearchableImpact->triangleIndex(),
								c_print(spSearchableImpact->barycenter()),
								c_print(spSearchableImpact->uv()),
								c_print(spSearchableImpact->du()),
								c_print(spSearchableImpact->dv()));
#endif

						if(buttons&WindowEvent::e_mbLeft)
						{
							triangleIndex=spSearchableImpact->triangleIndex();
							bary=spSearchableImpact->barycenter();

							m_pickFace=triangleIndex;
							m_pickBary=bary;

#if FE_WPT_DEBUG
							feLog("pick face %d bary %s\n",
									m_pickFace,c_print(m_pickBary));
#endif
							//* make sure any CacheOp notices a change
							catalog<I32>("Output Surface","serial")++;
						}
					}
				}
			}

			m_locator=m_spDriver->sample(triangleIndex,bary);
			if(!driverRotation)
			{
				const SpatialVector point=m_locator.translation();
				setIdentity(m_locator);
				m_locator.translation()=point;
			}

#if FE_WPT_DEBUG
			feLog("draw face %d bary %s\n%s\n",
					triangleIndex,c_print(bary),c_print(m_locator));
#endif

			drawDot(spView,spDrawOverlay,
					m_locator.translation(),smallRadius,yellow);

			if(m_picking==2)
			{
				drawAnchor(spDrawBrush,m_locator,anchorScale,
						anchorResolution,anchorColor);
			}
		}

		const I32 fontHeight=spDrawOverlay->font()->fontHeight(NULL,NULL);

		String label=rWayName;
		if(label.empty())
		{
			label="UNNAMED";
		}

		SpatialVector textPoint(0.5*width,
				height-2*fontHeight,1.0);
		drawLabel(spDrawOverlay,textPoint,label,
				TRUE,3,white,NULL,&black);

		if(m_picking)
		{
			scanPeers(spDrawOverlay,cameraPos,cameraDir,TRUE,
					(m_picking==1),0,0);

			textPoint[1]-=2*fontHeight;
			label=m_picking==2? "Move Anchor": "Pick Another";

			drawLabel(spDrawOverlay,textPoint,label,
					TRUE,3,green,NULL,&black);
		}

		spDrawOverlay->popDrawMode();
		spDrawBrush->popDrawMode();

#if FE_WPT_DEBUG
		feLog("WayPointOp::handle brush done\n");
#endif

		return;
	}

	catalog<String>("summary")="";

	Real frame=currentFrame(a_rSignal);
	const BWORD frameChanged=(frame!= m_lastFrame);
	if(frameChanged)
	{
		setAnchor();
	}
	const BWORD paramChanged=(!m_brushed && !frameChanged);

#if FE_WPT_DEBUG
	feLog("WayPointOp::handle frame %.6G->%.6G paramChanged %d m_brushed %d"
			" frameChanged %d\n",
			m_lastFrame,frame,paramChanged,m_brushed,frameChanged);
#endif

	m_brushed=FALSE;
	m_lastFrame=frame;

	if(m_pickFace>=0)
	{
#if FE_WPT_DEBUG
		feLog("relay face %d bary %s\n",m_pickFace,c_print(m_pickBary));
#endif

		catalog<I32>("face")=m_pickFace;
		catalog<SpatialVector>("bary")=m_pickBary.operator SpatialVector();

		m_pickFace= -1;
	}

	sp<DrawI> spOutputDraw;
	if(!accessDraw(spOutputDraw,a_rSignal)) return;

	const SpatialVector offsetP=catalog<SpatialVector>("pivot");
	const SpatialVector offsetR=catalog<SpatialVector>("rotate");
	const SpatialVector offsetT=catalog<SpatialVector>("translate");

	if(driverMethod!="surface")
	{
		m_spDriver=NULL;
	}
	else if(m_spDriver.isNull() ||
			catalogOrDefault<bool>("Driver Surface","replaced",true))
	{
		if(!access(m_spDriver,"Driver Surface")) return;
	}

	const Color red(1.0,0.0,0.0);
	const Color green(0.0,1.0,0.0);
	const Color blue(0.0,0.0,1.0);

	Color colors[3];
	colors[0]=green;
	colors[1]=red;
	colors[2]=blue;

	SpatialTransform offset;
	setIdentity(offset);
	translate(offset,offsetP);
	rotate(offset,offsetR[2]*fe::degToRad,e_zAxis);
	rotate(offset,offsetR[1]*fe::degToRad,e_yAxis);
	rotate(offset,offsetR[0]*fe::degToRad,e_xAxis);
	translate(offset,offsetT);

	SpatialTransform xform;
	SpatialVector contactP;
	SpatialVector contactN;
	set(contactP);
	set(contactN,0.0,1.0,0.0);

	sp<SurfaceAccessibleI> spOutputAccessible;
	if(!accessOutput(spOutputAccessible,a_rSignal)) return;

	if(driverMethod=="world")
	{
		xform=offset;
	}
	else if(driverMethod=="surface")
	{
		const I32 triangleIndex=catalog<I32>("face");
		const SpatialBary bary=catalog<SpatialVector>("bary");

		SpatialTransform sample=m_spDriver->sample(triangleIndex,bary);
		if(!driverRotation)
		{
			const SpatialVector point=sample.translation();
			setIdentity(sample);
			sample.translation()=point;
		}
		xform=offset*sample;
		contactP=sample.translation();
		contactN=sample.column(1);
	}
	else if(driverMethod=="previous")
	{
		sp<SurfaceAccessorI> spOutputPoint;
		if(!access(spOutputPoint,spOutputAccessible,
				e_point,e_position)) return;

		const I32 pointCount=spOutputPoint->count();
		if(pointCount<3)
		{
			xform=offset;
		}
		else
		{
			const SpatialVector frameT=
					spOutputPoint->spatialVector(pointCount-3);
			const SpatialVector frameX=
					spOutputPoint->spatialVector(pointCount-2)-frameT;
			const SpatialVector frameZ=
					spOutputPoint->spatialVector(pointCount-1)-frameT;

			const SpatialVector frameY=cross(frameZ,frameX);

			SpatialTransform parent;

			if(driverRotation)
			{
				parent.column(0)=frameX;
				parent.column(1)=frameY;
				parent.column(2)=frameZ;
			}
			else
			{
				setIdentity(parent);
			}

			parent.column(3)=frameT;

			xform=offset*parent;
		}
	}

	//* TODO else joints

	const BWORD enable=catalog<bool>("enable");
	const I32 samples=catalog<I32>("samples");
	const BWORD stick=catalog<bool>("stick");

	Real envelope=catalog<Real>("envelope");
	Real radius=catalog<Real>("radius");
	Real alignment=catalog<Real>("alignment");
	Real spread=catalog<Real>("spread");
	Real advance=catalog<Real>("advance");
	Real linearityIn=catalog<Real>("linearityIn");
	Real linearityOut=catalog<Real>("linearityOut");
	Real minDistIn=catalog<Real>("minDistIn");
	Real maxDistIn=catalog<Real>("maxDistIn");

	//* TODO blend also
	const BWORD setValue=catalog<BWORD>("setValue");
	const String valueAttr=catalog<String>("valueAttr");
	const SpatialVector value=catalog<SpatialVector>("value");

	m_shareBlend= -1.0;

	if(enable)
	{
		I32 replaceIndex= -1;

		sp<SurfaceAccessibleI> spInputAccessible;
		access(spInputAccessible,"Input Surface",e_quiet);

		if(spInputAccessible.isValid())
		{
			sp<SurfaceAccessorI> spInputName;
			access(spInputName,spInputAccessible,e_primitive,"name",e_warning);
			if(spInputName.isValid())
			{
				const I32 primitiveCount=spInputName->count();

				for(I32 primitiveIndex=0;
						primitiveIndex<primitiveCount;primitiveIndex++)
				{
					if(rWayName==spInputName->string(primitiveIndex))
					{
						replaceIndex=primitiveIndex;
						m_shareBlend=1.0;
					}
				}
			}
		}

		SpatialVector vert[3];
		vert[0]=xform.translation();
		vert[1]=vert[0]+xform.column(0);
		vert[2]=vert[0]+xform.column(2);

		const Real blend=catalog<Real>("blend");

		if(blend>0.0 && replaceIndex<0)
		{
			catalog<String>("warning")+=
					"non-zero blending requires matching name from input\n";
		}

		if(replaceIndex>=0 && blend<1.0)
		{
			m_shareBlend=blend;

			sp<SurfaceAccessorI> spInputRadius;
			if(!access(spInputRadius,spInputAccessible,
					e_primitive,"radius")) return;

			sp<SurfaceAccessorI> spInputEnvelope;
			if(!access(spInputEnvelope,spInputAccessible,
					e_primitive,"envelope")) return;

			sp<SurfaceAccessorI> spInputAlignment;
			if(!access(spInputAlignment,spInputAccessible,
					e_primitive,"alignment")) return;

			sp<SurfaceAccessorI> spInputSpread;
			if(!access(spInputSpread,spInputAccessible,
					e_primitive,"spread")) return;

			sp<SurfaceAccessorI> spInputAdvance;
			if(!access(spInputAdvance,spInputAccessible,
					e_primitive,"advance")) return;

			sp<SurfaceAccessorI> spInputLinearityIn;
			if(!access(spInputLinearityIn,spInputAccessible,
					e_primitive,"linearityIn")) return;

			sp<SurfaceAccessorI> spInputLinearityOut;
			if(!access(spInputLinearityOut,spInputAccessible,
					e_primitive,"linearityOut")) return;

			sp<SurfaceAccessorI> spInputMinDistIn;
			if(!access(spInputMinDistIn,spInputAccessible,
					e_primitive,"minDistIn")) return;

			sp<SurfaceAccessorI> spInputMaxDistIn;
			if(!access(spInputMaxDistIn,spInputAccessible,
					e_primitive,"maxDistIn")) return;

			sp<SurfaceAccessorI> spInputContactP;
			if(!access(spInputContactP,spInputAccessible,
					e_primitive,"contactP")) return;

			sp<SurfaceAccessorI> spInputContactN;
			if(!access(spInputContactN,spInputAccessible,
					e_primitive,"contactN")) return;

			sp<SurfaceAccessorI> spInputPoint;
			if(!access(spInputPoint,spInputAccessible,
					e_point,e_position)) return;

			const Real blend1=Real(1)-blend;

			radius=blend*radius+
					blend1*spInputRadius->real(replaceIndex);
			envelope=blend*envelope+
					blend1*spInputEnvelope->real(replaceIndex);
			alignment=blend*alignment+
					blend1*spInputAlignment->real(replaceIndex);
			spread=blend*spread+
					blend1*spInputSpread->real(replaceIndex);
			advance=blend*advance+
					blend1*spInputAdvance->real(replaceIndex);
			linearityIn=blend*linearityIn+
					blend1*spInputLinearityIn->real(replaceIndex);
			linearityOut=blend*linearityOut+
					blend1*spInputLinearityOut->real(replaceIndex);
			minDistIn=blend*minDistIn+
					blend1*spInputMinDistIn->real(replaceIndex);
			maxDistIn=blend*maxDistIn+
					blend1*spInputMaxDistIn->real(replaceIndex);
			contactP=blend*contactP+
					blend1*spInputContactP->spatialVector(replaceIndex);
			contactN=unitSafe(blend*contactN+
					blend1*spInputContactN->spatialVector(replaceIndex));

			const I32 pointIndex=replaceIndex*3;
			const SpatialVector oldPoint=
					spInputPoint->spatialVector(pointIndex);
			const SpatialVector oldX=
					spInputPoint->spatialVector(pointIndex+1)-oldPoint;
			const SpatialVector oldZ=
					spInputPoint->spatialVector(pointIndex+2)-oldPoint;

			vert[0]=blend*vert[0]+blend1*oldPoint;
			vert[1]=vert[0]+unitSafe(blend*xform.column(0)+blend1*oldX);
			vert[2]=vert[0]+unitSafe(blend*xform.column(2)+blend1*oldZ);

		}

		sp<SurfaceAccessorI> spOutputSamples;
		if(!access(spOutputSamples,spOutputAccessible,
				e_primitive,"samples")) return;

		sp<SurfaceAccessorI> spOutputRadius;
		if(!access(spOutputRadius,spOutputAccessible,
				e_primitive,"radius")) return;

		sp<SurfaceAccessorI> spOutputEnvelope;
		if(!access(spOutputEnvelope,spOutputAccessible,
				e_primitive,"envelope")) return;

		sp<SurfaceAccessorI> spOutputAlignment;
		if(!access(spOutputAlignment,spOutputAccessible,
				e_primitive,"alignment")) return;

		sp<SurfaceAccessorI> spOutputSpread;
		if(!access(spOutputSpread,spOutputAccessible,
				e_primitive,"spread")) return;

		sp<SurfaceAccessorI> spOutputAdvance;
		if(!access(spOutputAdvance,spOutputAccessible,
				e_primitive,"advance")) return;

		sp<SurfaceAccessorI> spOutputStick;
		if(!access(spOutputStick,spOutputAccessible,
				e_primitive,"stick")) return;

		sp<SurfaceAccessorI> spOutputLinearityIn;
		if(!access(spOutputLinearityIn,spOutputAccessible,
				e_primitive,"linearityIn")) return;

		sp<SurfaceAccessorI> spOutputLinearityOut;
		if(!access(spOutputLinearityOut,spOutputAccessible,
				e_primitive,"linearityOut")) return;

		sp<SurfaceAccessorI> spOutputMinDistIn;
		if(!access(spOutputMinDistIn,spOutputAccessible,
				e_primitive,"minDistIn")) return;

		sp<SurfaceAccessorI> spOutputMaxDistIn;
		if(!access(spOutputMaxDistIn,spOutputAccessible,
				e_primitive,"maxDistIn")) return;

		sp<SurfaceAccessorI> spOutputContactP;
		if(!access(spOutputContactP,spOutputAccessible,
				e_primitive,"contactP")) return;

		sp<SurfaceAccessorI> spOutputContactN;
		if(!access(spOutputContactN,spOutputAccessible,
				e_primitive,"contactN")) return;

		sp<SurfaceAccessorI> spOutputName;
		if(!access(spOutputName,spOutputAccessible,
				e_primitive,"name")) return;

		sp<SurfaceAccessorI> spOutputValue;
		if(setValue && !access(spOutputValue,spOutputAccessible,
				e_primitive,valueAttr)) return;

		if(replaceIndex<0)
		{
			//* NOTE append points

			replaceIndex=spOutputName->count();

			spOutputDraw->drawTriangles(vert,NULL,NULL,3,
					DrawI::e_discrete,true,colors);
		}
		else
		{
			//* NOTE replace points

			sp<SurfaceAccessorI> spOutputPoint;
			if(!access(spOutputPoint,spOutputAccessible,
					e_point,e_position)) return;

			const I32 pointIndex=replaceIndex*3;
			spOutputPoint->set(pointIndex,vert[0]);
			spOutputPoint->set(pointIndex+1,vert[1]);
			spOutputPoint->set(pointIndex+2,vert[2]);
		}

		spOutputName->set(replaceIndex,rWayName);
		spOutputEnvelope->set(replaceIndex,envelope);
		spOutputSamples->set(replaceIndex,samples);
		spOutputRadius->set(replaceIndex,radius);
		spOutputAlignment->set(replaceIndex,alignment);
		spOutputSpread->set(replaceIndex,spread);
		spOutputAdvance->set(replaceIndex,advance);
		spOutputStick->set(replaceIndex,I32(stick));
		spOutputLinearityIn->set(replaceIndex,linearityIn);
		spOutputLinearityOut->set(replaceIndex,linearityOut);
		spOutputMinDistIn->set(replaceIndex,minDistIn);
		spOutputMaxDistIn->set(replaceIndex,maxDistIn);
		spOutputContactP->set(replaceIndex,contactP);
		spOutputContactN->set(replaceIndex,contactN);

		if(spOutputValue.isValid())
		{
			spOutputValue->set(replaceIndex,value);
		}
	}

	m_shareTransform=xform;
	m_shareFrame=frame;

	const Color circleColor(0.0,envelope,envelope);

	sp<DrawI> spDrawGuide;
	if(accessGuide(spDrawGuide,a_rSignal))
	{
		//* rotate XY circle to XZ
		SpatialTransform circleTransform;
		circleTransform.column(0)=xform.column(0);
		circleTransform.column(1)=xform.column(2);
		circleTransform.column(2)=xform.column(1);
		circleTransform.translation()=xform.translation();

		const SpatialVector circleScale(radius,radius,radius);

		spDrawGuide->drawCircle(circleTransform,&circleScale,circleColor);

		if(radius>0.0 && spread>0.0)
		{
			SpatialVector line[3];

			line[1]=xform.translation();

			const SpatialVector arm=xform.column(0)*radius*(0.5+0.5*alignment);
			const SpatialVector axis=xform.column(1);

			SpatialQuaternion rotation(
					-0.5*spread*degToRad,axis);
			SpatialVector rotArm;

			rotateVector(rotation,arm,rotArm);
			line[0]=line[1]+rotArm;

			invert(rotation);

			rotateVector(rotation,arm,rotArm);
			line[2]=line[1]+rotArm;

			spDrawGuide->drawLines(line,NULL,3,
					DrawI::e_strip,false,&circleColor);
		}
	}

	if(paramChanged)
	{
		setAnchor();
	}
	else
	{
		getAnchor();
	}

	SpatialVector scaling(
			magnitude(m_transform.column(0)),
			magnitude(m_transform.column(1)),
			magnitude(m_transform.column(2)));

	SpatialTransform unscaled=m_transform;
	unscaled.column(0)*=1.0/scaling[0];
	unscaled.column(1)*=1.0/scaling[1];
	unscaled.column(2)*=1.0/scaling[2];

	SpatialEuler euler(unscaled);

	SpatialVector& rPivot=catalog<SpatialVector>("pivot");
	SpatialVector& rRotate=catalog<SpatialVector>("rotate");
	SpatialVector& rTranslate=catalog<SpatialVector>("translate");

	const SpatialVector newPivot=m_pivot.translation();
	const SpatialVector newRotate=euler/fe::degToRad;
	const SpatialVector newTranslate=
			inverseRotateVector(unscaled,m_transform.translation());

#if FE_WPT_DEBUG
	feLog("WayPointOp::handle update\n");
	feLog("  unscaled\n%s\n\n",c_print(unscaled));
	feLog("  pivot %s -> %s\n",c_print(rPivot),c_print(newPivot));
	feLog("  rotate %s -> %s\n",c_print(rRotate),c_print(newRotate));
	feLog("  translate %s -> %s\n",c_print(rTranslate),c_print(newTranslate));
#endif

	const Real minStep=1e-3;	//* TODO param

	for(U32 m=0;m<3;m++)
	{
		if(fabs(rPivot[m]-newPivot[m])>minStep)
		{
			rPivot[m]=newPivot[m];
		}

		if(fabs(rRotate[m]-newRotate[m])>minStep)
		{
			rRotate[m]=newRotate[m];
		}

		if(fabs(rTranslate[m]-newTranslate[m])>minStep)
		{
			rTranslate[m]=newTranslate[m];
		}
	}

	String& rSummary=catalog<String>("summary");
	rSummary=rWayName;
	if(!enable)
	{
		rSummary+=String(rSummary.empty()? "": " ")+"DISABLED";
	}

#if FE_WPT_DEBUG
	feLog("WayPointOp::handle done\n");
#endif
}

void WayPointOp::setAnchor(void)
{
	sp<Catalog> spCatalog=m_spManipulator;
	FEASSERT(spCatalog.isValid());

	setIdentity(m_pivot);
	setTranslation(m_pivot,catalog<SpatialVector>("pivot"));

	const SpatialEuler euler=catalog<SpatialVector>("rotate")*fe::degToRad;
	m_transform=euler;
	translate(m_transform,catalog<SpatialVector>("translate"));

	spCatalog->catalog<SpatialTransform>("anchor")=m_locator;
	spCatalog->catalog<SpatialTransform>("pivot")=m_pivot;
	spCatalog->catalog<SpatialTransform>("transform")=m_transform;

//	feLog("WayPointOp::setAnchor pivot\n%s\ndeform\n%s\n",
//			c_print(m_pivot),c_print(m_transform));
}

void WayPointOp::getAnchor(void)
{
	sp<Catalog> spCatalog=m_spManipulator;
	FEASSERT(spCatalog.isValid());

	m_pivot=spCatalog->catalog<SpatialTransform>("pivot");
	m_transform=spCatalog->catalog<SpatialTransform>("transform");

//	feLog("WayPointOp::getAnchor pivot\n%s\ndeform\n%s\n",
//			c_print(m_pivot),c_print(m_transform));
}

String WayPointOp::scanPeers(sp<DrawI>& a_rspDrawOverlay,
	const SpatialVector& a_cameraPos,const SpatialVector& a_cameraDir,
	BWORD a_draw,BWORD a_labels,I32 a_mouseX,I32 a_mouseY)
{
	const Color black(0.0,0.0,0.0);
	const Color grey(0.5,0.5,0.5);
	const Color white(1.0,1.0,1.0);
	const Color cyan(0.0,1.0,1.0);
	const Color magenta(1.0,0.0,1.0);
	const Color yellow(1.0,1.0,0.0);
	const Color red(1.0,0.0,0.0);
	const Color green(0.0,1.0,0.0);
	const Color blue(0.0,0.0,1.0);
	const Real dimAlpha=0.4;

//	feLog("\nWayPointOp::scanPeers for \"%s\"\n",name().c_str());

	sp<Catalog> spMasterCatalog=registry()->master()->catalog();

	Array< hp<Component> >& rGlobalArray=
			spMasterCatalog->catalog< Array< hp<Component> > >(
			sharingName());

	const Vector2i mousePoint(a_mouseX,a_mouseY);

	String nearestNode;
	Real nearestDistance= -1.0;

	const U32 componentCount=rGlobalArray.size();
	for(U32 componentIndex=0;componentIndex<componentCount;componentIndex++)
	{
		sp<Component> spComponent=rGlobalArray[componentIndex];
		if(spComponent.isValid())
		{
//			feLog("WayPointOp::scanPeers %d/%d \"%s\"\n",
//					componentIndex,componentCount,spComponent->name().c_str());

			sp<WayPointOp> spWayPointOp=spComponent;
			if(spWayPointOp.isValid())
			{
				if(spWayPointOp.raw()!=this)
				{
					Real& rShareFrame=
							spWayPointOp->m_shareFrame;
					if(rShareFrame!=m_lastFrame)
					{
						rShareFrame= -1.0;
						continue;
					}

					const SpatialTransform& rShareTranform=
							spWayPointOp->m_shareTransform;
					const SpatialVector toShare=
							unitSafe(rShareTranform.translation()-a_cameraPos);
					const Real cameraDot=dot(toShare,a_cameraDir);
					if(cameraDot<0.0)
					{
						//* behind camera
						continue;
					}

					const String nodeName=spWayPointOp->name();
					const String method=
							spWayPointOp->catalog<String>("driverMethod");
					const BWORD rotating=
							spWayPointOp->catalog<bool>("driverRotation");
					const Real radius=spWayPointOp->catalog<Real>("radius");
					const BWORD highlighted=(nodeName==m_highlightNode);

					sp<ViewI> spView=a_rspDrawOverlay->view();
					const Vector2i projected=spView->project(
							rShareTranform.translation(),ViewI::e_perspective);

					if(a_draw)
					{
						const Real envelope=
								spWayPointOp->catalog<Real>("envelope");
						const BWORD disabled=envelope<=0.0 ||
								!spWayPointOp->catalog<bool>("enable");

						const BWORD backfacing=(method!="world" && rotating &&
								dot(rShareTranform.column(1),a_cameraDir)>0.0);

						const BWORD faded=(backfacing && !highlighted);

						Color rimColor=disabled? red:
							((envelope<1.0)? yellow: green);
						Color centerColor=highlighted? white:
							((method=="world")? black:
							((method=="surface")? blue: grey));
						Color shadowColor=magenta;
						Color sparkleColor=cyan;

						if(faded)
						{
							rimColor[3]=dimAlpha;
							centerColor[3]=dimAlpha;
							shadowColor[3]=dimAlpha;
							sparkleColor[3]=dimAlpha;
						}

						const Real multiplication=
								a_rspDrawOverlay->multiplication();
						const Color* pShadowColor=rotating? NULL: &shadowColor;
						const Color* pSparkleColor=
								(radius>0.0)? &sparkleColor: NULL;
						const I32 rimSize=4*multiplication;
						const BWORD bevel=TRUE;

						drawSquare(a_rspDrawOverlay,
								rShareTranform.translation(),
								rimSize,bevel,centerColor,rimColor,
								pShadowColor,pSparkleColor);

						if(faded || !a_labels)
						{
							continue;
						}

						String label=
								spWayPointOp->catalog<String>("name");

						const Real shareBlend=spWayPointOp->m_shareBlend;
						if(shareBlend>0.99)
						{
							label+="*";
						}
						else if(shareBlend>0.01)
						{
							label+="+";
						}
						else if(shareBlend>=0.0)
						{
							label+="-";
						}

						const BWORD centered=FALSE;
						const U32 border=1;
						drawLabel(a_rspDrawOverlay,
								projected+Vector2i(6,0)*multiplication,
								label,centered,border,white,NULL,
								highlighted? &blue: &black);

						continue;
					}

					const Real pixelDistance=
							magnitude(projected-mousePoint);
					if(nearestDistance<0.0 || pixelDistance<nearestDistance)
					{
						nearestDistance=pixelDistance;
						nearestNode=nodeName;
					}
				}
			}
		}
	}

	return nearestNode;
}
