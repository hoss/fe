/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __opensubdiv_SurfaceAccessibleOsd_h__
#define __opensubdiv_SurfaceAccessibleOsd_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
    @brief OpenSubdiv Surface Binding

	@ingroup opensubdiv
*//***************************************************************************/
class FE_DL_EXPORT SurfaceAccessibleOsd:
	public SurfaceAccessibleCatalog
{
	public:
								SurfaceAccessibleOsd(void);
virtual							~SurfaceAccessibleOsd(void);

								using SurfaceAccessibleCatalog::accessor;

virtual sp<SurfaceAccessorI>	accessor(String a_node,
										SurfaceAccessibleI::Element a_element,
										String a_name,
										SurfaceAccessibleI::Creation a_create,
										SurfaceAccessibleI::Writable
										a_writable);
virtual sp<SurfaceAccessorI>	accessor(String a_node,
										SurfaceAccessibleI::Element a_element,
										SurfaceAccessibleI::Attribute
										a_attribute,
										SurfaceAccessibleI::Creation a_create,
										SurfaceAccessibleI::Writable
										a_writable);

		I32&					subdivDepth(void)
								{	return m_subdivDepth; }
		Array<SpatialVector>&	subdivPosition(void)
								{	return m_subdivPosition; }
		Array< Vector<4,I32> >&	subdivVertexIndex(void)
								{	return m_subdivVertexIndex; }

	protected:
virtual	void					reset(void);

	private:
		sp<SurfaceAccessorI>	createAccessor(void);

		I32						m_subdivDepth;
		Array<SpatialVector>	m_subdivPosition;
		Array< Vector<4,I32> >	m_subdivVertexIndex;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __opensubdiv_SurfaceAccessibleOsd_h__ */
