/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __opensubdiv_SurfaceAccessorOsd_h__
#define __opensubdiv_SurfaceAccessorOsd_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
    @brief Accessor for OpenSubdiv

	@ingroup opensubdiv
*//***************************************************************************/
class FE_DL_EXPORT SurfaceAccessorOsd:
	virtual public SurfaceAccessorCatalog
{
	public:
						SurfaceAccessorOsd(void);
virtual					~SurfaceAccessorOsd(void);

						using SurfaceAccessorCatalog::set;
						using SurfaceAccessorCatalog::spatialVector;

						//* as SurfaceAccessorI
virtual	U32				count(void) const;
virtual	U32				subCount(U32 a_index) const;

virtual	I32				integer(U32 a_index,U32 a_subIndex=0);

virtual	SpatialVector	spatialVector(U32 a_index,U32 a_subIndex=0);

virtual	void			set(U32 a_index,U32 a_subIndex,I32 a_integer);

	private:

	class Vertex: public SpatialVector
	{
		public:
					Vertex(void)											{}

					Vertex(Vertex const& a_src):
						SpatialVector()
					{	*this=a_src; }

			void	Clear(void* a_void=NULL)
					{	fe::set(*this); }

			void	AddWithWeight(Vertex const& a_src,float a_weight)
					{	(*this)[0]+=a_src[0]*a_weight;
						(*this)[1]+=a_src[1]*a_weight;
						(*this)[2]+=a_src[2]*a_weight; }

			void	SetPosition(float a_x, float a_y, float a_z)
					{	fe::set(*this,a_x,a_y,a_z); }

	const	Real*	GetPosition(void) const
					{	return raw(); }
	};
};

} /* namespace ext */
} /* namespace fe */

#endif /* __opensubdiv_SurfaceAccessorOsd_h__ */
