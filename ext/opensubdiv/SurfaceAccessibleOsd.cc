/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <opensubdiv/opensubdiv.pmh>

namespace fe
{
namespace ext
{

SurfaceAccessibleOsd::SurfaceAccessibleOsd(void):
	m_subdivDepth(0)
{
}

SurfaceAccessibleOsd::~SurfaceAccessibleOsd(void)
{
}

void SurfaceAccessibleOsd::reset(void)
{
	SurfaceAccessibleCatalog::reset();

	m_subdivDepth=0;
	m_subdivPosition.clear();
	m_subdivVertexIndex.clear();
}

//* copy from SurfaceAccessibleCatalog
sp<SurfaceAccessorI> SurfaceAccessibleOsd::accessor(
	String a_node,SurfaceAccessibleI::Element a_element,
	String a_name,SurfaceAccessibleI::Creation a_create,
	SurfaceAccessibleI::Writable a_writable)
{
	sp<SurfaceAccessorOsd> spAccessor=createAccessor();
	if(spAccessor.isValid())
	{
		spAccessor->setWritable(a_writable);
		spAccessor->setSurfaceAccessible(sp<SurfaceAccessibleI>(this));

		if(spAccessor->bind(a_element,a_name) ||
				a_create==SurfaceAccessibleI::e_createMissing)
		{
			return spAccessor;
		}
	}
	return sp<SurfaceAccessorI>(NULL);
}

//* copy from SurfaceAccessibleCatalog
sp<SurfaceAccessorI> SurfaceAccessibleOsd::accessor(
	String a_node,SurfaceAccessibleI::Element a_element,
	SurfaceAccessibleI::Attribute a_attribute,
	SurfaceAccessibleI::Creation a_create,
	SurfaceAccessibleI::Writable a_writable)
{
	sp<SurfaceAccessorOsd> spAccessor=createAccessor();
	if(spAccessor.isValid())
	{
		spAccessor->setWritable(a_writable);
		spAccessor->setSurfaceAccessible(sp<SurfaceAccessibleI>(this));

		if(spAccessor->bind(a_element,a_attribute) ||
				a_create==SurfaceAccessibleI::e_createMissing)
		{
			return spAccessor;
		}
	}
	return sp<SurfaceAccessorI>(NULL);
}

//* variation of SurfaceAccessibleCatalog::createAccessor(void)
sp<SurfaceAccessorI> SurfaceAccessibleOsd::createAccessor(void)
{
	sp<Catalog> spCatalog=catalog();

	if(!spCatalog.isValid())
	{
		return sp<SurfaceAccessorI>(NULL);
	}
	sp<SurfaceAccessorOsd> spAccessorOsd(new SurfaceAccessorOsd());
	if(spAccessorOsd.isValid())
	{
		spAccessorOsd->setCatalog(spCatalog);

		//* NOTE intentionally duplicating String buffer to be thread safe
		spAccessorOsd->setKey(key().c_str());
	}

	return spAccessorOsd;
}

} /* namespace ext */
} /* namespace fe */
