/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <opensubdiv/opensubdiv.pmh>

namespace fe
{
namespace ext
{

SurfaceAccessorOsd::SurfaceAccessorOsd(void)
{
}

SurfaceAccessorOsd::~SurfaceAccessorOsd(void)
{
}

U32 SurfaceAccessorOsd::count(void) const
{
	sp<SurfaceAccessibleOsd> spSurfaceAccessibleOsd=
			m_spSurfaceAccessibleI;

	if(spSurfaceAccessibleOsd.isValid() &&
			spSurfaceAccessibleOsd->subdivDepth()>0)
	{
		if(m_element==SurfaceAccessibleI::e_point)
		{
			return spSurfaceAccessibleOsd->subdivPosition().size();
		}

		if(m_element==SurfaceAccessibleI::e_primitive)
		{
			return spSurfaceAccessibleOsd->subdivVertexIndex().size();
		}
	}

	return SurfaceAccessorCatalog::count();
}

U32 SurfaceAccessorOsd::subCount(U32 a_index) const
{
	sp<SurfaceAccessibleOsd> spSurfaceAccessibleOsd=
			m_spSurfaceAccessibleI;

	if(spSurfaceAccessibleOsd.isValid() &&
			spSurfaceAccessibleOsd->subdivDepth()>0)
	{
		if(m_element==SurfaceAccessibleI::e_primitive)
		{
			return 4;
		}
	}

	return SurfaceAccessorCatalog::subCount(a_index);
}

I32 SurfaceAccessorOsd::integer(U32 a_index,U32 a_subIndex)
{
	sp<SurfaceAccessibleOsd> spSurfaceAccessibleOsd=
			m_spSurfaceAccessibleI;

	if(spSurfaceAccessibleOsd.isValid() &&
			spSurfaceAccessibleOsd->subdivDepth()>0)
	{
		if(m_element==SurfaceAccessibleI::e_primitive &&
				m_attribute==SurfaceAccessibleI::e_vertices)
		{
			return spSurfaceAccessibleOsd->subdivVertexIndex()
					[a_index][a_subIndex];
		}
	}

	return SurfaceAccessorCatalog::integer(a_index,a_subIndex);
}

SpatialVector SurfaceAccessorOsd::spatialVector(U32 a_index,U32 a_subIndex)
{
	sp<SurfaceAccessibleOsd> spSurfaceAccessibleOsd=
			m_spSurfaceAccessibleI;

	if(spSurfaceAccessibleOsd.isValid() &&
			spSurfaceAccessibleOsd->subdivDepth()>0)
	{
		if(m_element==SurfaceAccessibleI::e_point &&
				m_attribute==SurfaceAccessibleI::e_position)
		{
			return spSurfaceAccessibleOsd->subdivPosition()[a_index];
		}

		if(m_element==SurfaceAccessibleI::e_primitive &&
				m_attribute==SurfaceAccessibleI::e_vertices)
		{
			FEASSERT(a_subIndex<4);
			const I32 pointIndex=spSurfaceAccessibleOsd->subdivVertexIndex()
					[a_index][a_subIndex];
			return spSurfaceAccessibleOsd->subdivPosition()[pointIndex];
		}
	}

	return SurfaceAccessorCatalog::spatialVector(a_index,a_subIndex);
}

void SurfaceAccessorOsd::set(U32 a_index,U32 a_subIndex,I32 a_integer)
{
//	feLog("SurfaceAccessorOsd::set %d %d %d\n",a_index,a_subIndex,a_integer);

	//* check for depth change
	if(m_element==SurfaceAccessibleI::e_detail &&
			m_attribute==SurfaceAccessibleI::e_properties &&
			a_index==SurfaceAccessibleI::e_depth)
	{
		FEASSERT(m_spSurfaceAccessibleI.isValid());
		sp<SurfaceAccessibleOsd> spSurfaceAccessibleOsd=
				m_spSurfaceAccessibleI;
		FEASSERT(spSurfaceAccessibleOsd.isValid());

		sp<SurfaceAccessorI> spCoarsePosition=
				spSurfaceAccessibleOsd->SurfaceAccessibleCatalog::accessor(
				SurfaceAccessibleI::e_point,
				SurfaceAccessibleI::e_position,
				SurfaceAccessibleI::e_refuseMissing);
		sp<SurfaceAccessorI> spCoarseVertices=
				spSurfaceAccessibleOsd->SurfaceAccessibleCatalog::accessor(
				SurfaceAccessibleI::e_primitive,
				SurfaceAccessibleI::e_vertices,
				SurfaceAccessibleI::e_refuseMissing);

		const I32 coarsePointCount=spCoarsePosition->count();

		SpatialVector* position=new SpatialVector[coarsePointCount];
		for(I32 pointIndex=0;pointIndex<coarsePointCount;pointIndex++)
		{
			position[pointIndex]=spCoarsePosition->spatialVector(pointIndex);
		}

		const I32 coarsePrimitiveCount=spCoarseVertices->count();

		I32 coarseVertexCount=0;

		I32* numVertsPerFace=new I32[coarsePrimitiveCount];
		for(I32 primitiveIndex=0;primitiveIndex<coarsePrimitiveCount;
				primitiveIndex++)
		{
			const I32 coarseSubCount=
					spCoarseVertices->subCount(primitiveIndex);
			numVertsPerFace[primitiveIndex]=coarseSubCount;
			coarseVertexCount+=coarseSubCount;
		}

		I32* vertIndicesPerFace=new I32[coarseVertexCount];
		I32 coarseVertexIndex=0;
		for(I32 primitiveIndex=0;primitiveIndex<coarsePrimitiveCount;
				primitiveIndex++)
		{
			const I32 coarseSubCount=numVertsPerFace[primitiveIndex];

			for(I32 subIndex=0;subIndex<coarseSubCount;subIndex++)
			{
				vertIndicesPerFace[coarseVertexIndex++]=
						spCoarseVertices->integer(primitiveIndex,subIndex);
			}
		}
		FEASSERT(coarseVertexIndex==coarseVertexCount);

		OpenSubdiv::Sdc::SchemeType schemeType=OpenSubdiv::Sdc::SCHEME_CATMARK;

		OpenSubdiv::Sdc::Options options;
		options.SetVtxBoundaryInterpolation(
				OpenSubdiv::Sdc::Options::VTX_BOUNDARY_EDGE_ONLY);

		OpenSubdiv::Far::TopologyDescriptor descriptor;
		descriptor.numVertices=coarsePointCount;
		descriptor.numFaces=coarsePrimitiveCount;
		descriptor.numVertsPerFace=numVertsPerFace;
		descriptor.vertIndicesPerFace=vertIndicesPerFace;

		OpenSubdiv::Far::TopologyRefiner *pRefiner=
				OpenSubdiv::Far::TopologyRefinerFactory
				<OpenSubdiv::Far::TopologyDescriptor>::Create(descriptor,
				OpenSubdiv::Far::TopologyRefinerFactory
				<OpenSubdiv::Far::TopologyDescriptor>::Options(
				schemeType,options));

		const I32 maxLevel=a_integer;

		// Uniformly refine the topology up to 'maxLevel'
		pRefiner->RefineUniform(
				OpenSubdiv::Far::TopologyRefiner::UniformOptions(maxLevel));

		// Allocate a buffer for vertex primvar data.
		// The buffer length is set to be the sum of all children vertices
		// up to the highest level of refinement.
		std::vector<Vertex> buffer(pRefiner->GetNumVerticesTotal());
		Vertex* pVerts=&buffer[0];

		// Initialize coarse mesh positions
		const I32 coarseVertCount=descriptor.numVertices;
		for(I32 i=0;i<coarseVertCount;i++)
		{
			pVerts[i].SetPosition(
					position[i][0],position[i][1],position[i][2]);
		}

		delete[] vertIndicesPerFace;
		delete[] numVertsPerFace;
		delete[] position;

		// Interpolate vertex primvar data
		OpenSubdiv::Far::PrimvarRefiner primvarRefiner(*pRefiner);

		Vertex* pSrcVert=pVerts;
		for(I32 level=1;level<=maxLevel;level++)
		{
			Vertex* pDestVert=
					pSrcVert+pRefiner->GetLevel(level-1).GetNumVertices();
			primvarRefiner.Interpolate(level,pSrcVert,pDestVert);
			pSrcVert=pDestVert;
		}

		// Output OBJ of the highest level refined -----------

		OpenSubdiv::Far::TopologyLevel const& rLastLevel=
				pRefiner->GetLevel(maxLevel);

		const I32 pointCount=rLastLevel.GetNumVertices();
		const I32 faceCount=rLastLevel.GetNumFaces();

		I32 firstOfLastVerts=pRefiner->GetNumVerticesTotal()-pointCount;

		I32& rSubdivDepth=
				spSurfaceAccessibleOsd->subdivDepth();
		Array<SpatialVector>& rSubdivPosition=
				spSurfaceAccessibleOsd->subdivPosition();
		Array< Vector<4,I32> >& rSubdivVertexIndex=
				spSurfaceAccessibleOsd->subdivVertexIndex();

		rSubdivDepth=maxLevel;
		rSubdivPosition.resize(pointCount);
		rSubdivVertexIndex.resize(faceCount);

		for(I32 pointIndex=0;pointIndex<pointCount;pointIndex++)
		{
			Real const* pPos=pVerts[firstOfLastVerts+pointIndex].GetPosition();
//			feLog("v %d/%d %f %f %f\n",
//					pointIndex,pointCount,pPos[0],pPos[1],pPos[2]);

			fe::set(rSubdivPosition[pointIndex],pPos[0],pPos[1],pPos[2]);
		}

		for(I32 face=0;face<faceCount;face++)
		{
			OpenSubdiv::Far::ConstIndexArray faceVerts=
					rLastLevel.GetFaceVertices(face);

			const I32 faceVertCount=faceVerts.size();

			// all refined Catmark faces should be quads
			FEASSERT(faceVertCount==4);

			fe::set(rSubdivVertexIndex[face],
					faceVerts[0],faceVerts[1],faceVerts[2],faceVerts[3]);

//			feLog("f %d/%d ",face,faceCount);
//			for(I32 subIndex=0;subIndex<faceVertCount;subIndex++)
//			{
//				// OBJ uses 1-based arrays...
//				feLog("%d ",faceVerts[subIndex]);
//			}
//			feLog("\n");
		}

		return;
	}

	SurfaceAccessorCatalog::set(a_index,a_subIndex,a_integer);
}

} /* namespace ext */
} /* namespace fe */
