/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __audio_h__
#define __audio_h__

/// @defgroup audio Audio Interface
/// @ingroup ext

#include "fe/plugin.h"
#include "math/math.h"

#include "audio/AudioI.h"
#include "audio/ListenerI.h"
#include "audio/VoiceI.h"

#endif /* __audio_h__ */
