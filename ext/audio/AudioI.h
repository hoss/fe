/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __audio_AudioI_h__
#define __audio_AudioI_h__
namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Sound system

	@ingroup audio
*//***************************************************************************/
class FE_DL_EXPORT AudioI:
	virtual public Component,
	public CastableAs<AudioI>
{
	public:
virtual	Result	load(String name,String filename)							=0;
virtual	Result	unload(String name)											=0;
virtual	Result	flush(void)													=0;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __audio_AudioI_h__ */
