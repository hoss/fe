/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __audio_ListenerI_h__
#define __audio_ListenerI_h__
namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Sound receiver

	@ingroup audio
*//***************************************************************************/
class FE_DL_EXPORT ListenerI:
	virtual public Component,
	public CastableAs<ListenerI>
{
	public:

virtual	Result	setLocation(const SpatialVector& location)					=0;
virtual	Result	setOrientation(const SpatialVector& at,
						const SpatialVector& up)							=0;
virtual	Result	setVelocity(const SpatialVector& velocity)					=0;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __audio_ListenerI_h__ */
