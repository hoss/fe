/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __audio_VoiceI_h__
#define __audio_VoiceI_h__
namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Sound producer

	@ingroup audio
*//***************************************************************************/
class FE_DL_EXPORT VoiceI:
	virtual public Component,
	public CastableAs<VoiceI>
{
	public:

virtual	Result	setReferenceDistance(Real distance)							=0;
virtual	Result	setLocation(const SpatialVector& location)					=0;
virtual	Result	setDirection(const SpatialVector& direction)				=0;
virtual	Result	setVelocity(const SpatialVector& velocity)					=0;
virtual	Result	setPitch(F32 pitch)											=0;
virtual	Result	setVolume(F32 volume)										=0;

virtual	Result	play(void)													=0;
virtual	Result	pause(void)													=0;
virtual	Result	stop(void)													=0;
virtual	Result	rewind(void)												=0;

				/// @brief Add another sound to be played in order
virtual	Result	queue(String name)											=0;

				/// @brief Play current sound repeatedly
virtual	Result	loop(BWORD on)												=0;

				/// @brief Continue playing without external references
virtual	Result	persist(BWORD on)											=0;

				/// @brief Return number of sounds playing or pending
virtual	I32		remaining(void)												=0;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __audio_VoiceI_h__ */

