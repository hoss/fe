/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __native_assertNative_h__
#define __native_assertNative_h__

namespace fe
{
namespace ext
{

class TypeNative: public Component, public Initialize<TypeNative>
{
	public:
				TypeNative(void)											{}
virtual			~TypeNative(void);
		void	initialize(void);

	private:
		sp<BaseType> m_spT;
};

class FE_DL_EXPORT InfoWindowHandlePtr:
		public TypeInfoInitialized<FE_WINDOW_HANDLE*>
{
};

} /* namespace ext */
} /* namespace fe */

#endif /* __native_assertNative_h__ */
