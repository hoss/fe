/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <native/native.pmh>

#define	FE_NW_DEBUG		FALSE

namespace fe
{
namespace ext
{

#if FE_2DGL==FE_2D_X_GFX
bool operator==(const FE_WINDOW_HANDLE& left,const FE_WINDOW_HANDLE& right)
{
return left.m_window==right.m_window;
}
#endif


NativeWindow::NativeWindow(void):
	m_lockThreads(TRUE),
	m_background(0.0f,0.0f,0.0f)
{
	set(m_box,20,30,500,400);
	m_started=FALSE;
	m_created=FALSE;
	m_mapped=FALSE;

	fe::String value;
	if(!fe::System::getEnvironmentVariable("FE_GL_LOCKING", value) ||
			!atoi(value.c_str()))
	{
		m_lockThreads=FALSE;
	}
}

NativeWindow::~NativeWindow(void)
{
#ifdef FE_CHAINSIGNALER_PROFILING
	/*	if used as a hub for its own handlers (it is a useful practice to
		create Handlers and concede ownership to the signaler), this peek
		breaks since the Handlers will already be gone but the shared count
		will not yet be zero (this still exists) */
#if 0
	Peeker peeker;
	peek(peeker);

	feLog("~NativeWindow\n%s\n",peeker.str().c_str());
#endif
#endif

	if(m_mapped)
		close();

	nativeShutdown();

	if(m_spEventContextI.isValid())
	{
		m_spEventContextI->deregisterWindow(this);
	}
}

void NativeWindow::initialize(void)
{
	m_spEventContextI=registry()->create("EventContextI.NativeEventContext");
	if(!m_spEventContextI.isValid())
	{
		feX("fe::NativeWindow::initialize",
				"needed to create a NativeEventContext");
	}

	m_spEventContextI->registerWindow(this);

	nativeInitialize();
}

void NativeWindow::startup(void)
{
	if(m_started)
	{
		return;
	}

	Result result=nativeStartup();
	if(failure(result))
		feX("fe::NativeWindow::initialize","nativeStartup failed");
	m_started=TRUE;
}

void NativeWindow::handleBind(sp<SignalerI> spSignalerI,sp<Layout> spLayout)
{
	sp<Scope> spScope=spLayout->scope();
	m_aWindowHandle.initialize(spScope,FE_USE("evt:winHandle"));
	m_eventLayout = spScope->lookupLayout(m_event.name());
}

void NativeWindow::handle(Record &record)
{
	if(record.layout() == m_eventLayout)
	{
		handleEvent(record);
		return;
	}
	// as a general signaler, this window is made current
	//makeCurrent();

	signal(record);
}

void NativeWindow::handleEvent(Record &record)
{
	m_event.bind(record);

#if	FE_NW_DEBUG
	if(!m_event.isIdleMouse())
		feLog("%p NativeWindow::handle() %s\n",
				(U32)this,m_event.out(TRUE).c_str());
#endif

	FE_WINDOW_HANDLE **ppWindowHandle=reinterpret_cast<FE_WINDOW_HANDLE**>
			(m_aWindowHandle.queryAttribute(record));

	if(m_event.isUser())
	{
		if(ppWindowHandle)
		{
			*ppWindowHandle=&m_windowHandle;
		}
		makeCurrent();
		if(m_event.isClear())
		{
			clear();
		}
		if(m_event.isSwap())
		{
			swapBuffers();
		}
		signal(record);
		return;
	}

	if(ppWindowHandle && (!*ppWindowHandle || **ppWindowHandle==m_windowHandle))
	{
		if(m_event.isResize())
			setSize(m_event.state(),m_event.state2());

#if	FE_NW_DEBUG
	if(!m_event.isIdleMouse())
		feLog("\trepeater WH=%p\n",*ppWindowHandle);
#endif

		signal(record);

#if	FE_NW_DEBUG
	if(!m_event.isIdleMouse())
		feLog("\trepeater END\n",*ppWindowHandle);
#endif
	}
}

void NativeWindow::setBackground(const Color& color)
{
	m_background = color;
}

const Color& NativeWindow::background(void) const
{
	return m_background;
}

void NativeWindow::clear(void)
{
	if(m_lockThreads)
	{
		m_spEventContextI->threadLock();
	}
	glClearColor(m_background[0],m_background[1],m_background[2],m_background[3]);
	glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
	if(m_lockThreads)
	{
		m_spEventContextI->threadUnlock();
	}
}

} /* namespace ext */
} /* namespace fe */
