/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <native/native.pmh>

namespace fe
{
namespace ext
{

NativeEventContext::NativeEventContext(void):
	m_multiplication(1.0),
	m_pointerMotion(FALSE)
{
//	feLog("NativeEventContext::NativeEventContext %p\n",this);
	nativeStartup();
}

NativeEventContext::~NativeEventContext(void)
{
#ifdef FE_CHAINSIGNALER_PROFILING
	Peeker peeker;
	sp<SignalerI> spSignalerI(m_hpSignalerI);
	if(spSignalerI.isValid())
	{
		spSignalerI->peek(peeker);
	}
	feLog("NativeEventContext::~NativeEventContext\n%s\n",
			peeker.str().c_str());
#endif

	nativeShutdown();
}

void NativeEventContext::initialize(void)
{
//	feLog("NativeEventContext::initialize\n");

	sp<Component> spTypeNative=registry()->create("*.TypeNative");
	FEASSERT(spTypeNative.isValid());
}

void NativeEventContext::registerWindow(WindowI* pWindow)
{
	m_windows.append(pWindow);

	sp<SignalerI> spSignalerI(m_hpSignalerI);
	if(spSignalerI.isValid())
	{
		spSignalerI->insert(sp<WindowI>(pWindow),m_spLayout);
	}
}

void NativeEventContext::deregisterWindow(WindowI* pWindow)
{
	m_windows.remove(pWindow);
}

void NativeEventContext::handle(Record &record)
{
//	feLog("%p NativeEventContext::handle()\n",(U32)this);

	if(!m_record.isValid())
		createEvent();

	//* Poll input devices to produce events
	WindowEvent::MouseButtons mb=m_event.mouseButtons();
	m_event.reset();
	m_event.setMouseButtons(mb);
	m_event.setSIS(WindowEvent::e_sourceSystem,
				WindowEvent::e_itemPoll,WindowEvent::e_stateNull);
//	feLog("POLL %s\n",m_event.out(TRUE).c_str());
	broadcastEvent(NULL);

	//* resolve all outstanding window-system native events
	loop(TRUE);
}

void NativeEventContext::handleBind(sp<SignalerI> spSignalerI,
	sp<Layout> spLayout)
{
//	feLog("%p NativeEventContext::bind()\n",(U32)this);

	FEASSERT(spSignalerI.isValid());
	m_hpSignalerI=spSignalerI;

	//* TODO
//	addAttributes();
	bindScope(spLayout->scope());

	ListCore::Context context;
	WindowI *pWindow;
	m_windows.toHead(context);
	while((pWindow=m_windows.postIncrement(context)) != NULL)
	{
		spSignalerI->insert(sp<WindowI>(pWindow),m_spLayout);
	}
}

Result NativeEventContext::bindScope(sp<Scope> spScope)
{
	spScope->support(FE_USE("evt:winHandle"),"WindowHandlePtr");

	m_event.bind(spScope);
	m_spLayout=m_event.layout();
	m_spLayout->populate(FE_USE("evt:winHandle"));

	m_aWindowHandle.initialize(spScope,FE_USE("evt:winHandle"));

#if FALSE
	Peeker peek;
	peek(*spScope);
	feLog("%s\n",peek.output().c_str());
#endif

	return e_ok;
}

void NativeEventContext::createEvent(void)
{
	if(m_spLayout.isValid())
	{
		m_record=m_spLayout->scope()->createRecord(m_spLayout);
		m_event.bind(m_record);
		m_event.reset();
	}
}

Result NativeEventContext::broadcastEvent(FE_WINDOW_HANDLE* pWindow)
{
#if 0//FE_WINDOW_EVENT_DEBUG
	if(!m_event.isIdleMouse())
	{
		feLog("NativeEventContext::broadcastEvent() %s\n",
				print(m_event).c_str());
	}
#endif

	sp<SignalerI> spSignalerI(m_hpSignalerI);
	if(!spSignalerI.isValid())
	{
		return e_notInitialized;
	}

	FE_WINDOW_HANDLE **ppWindowHandle= reinterpret_cast<FE_WINDOW_HANDLE**>
			(m_aWindowHandle.queryAttribute(m_record));
	if(ppWindowHandle)
		*ppWindowHandle=pWindow;

	spSignalerI->signal(m_record);
	return e_ok;
}

} /* namespace ext */
} /* namespace fe */
