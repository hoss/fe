/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __window_WindowHandle_h__
#define __window_WindowHandle_h__

FE_ATTRIBUTE("evt:winHandle","Platform-specific WindowHandle added to events");

#if FE_2DGL==FE_2D_X_GFX

//* X Windows
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xatom.h>
#include <X11/keysym.h>
#include <X11/cursorfont.h>
#include <X11/extensions/Xrender.h>

namespace fe
{
namespace ext
{

typedef struct _FE_WINDOW_HANDLE
{
	Display*		m_pDisplay;
	XVisualInfo*	m_pVisualInfo;
	Window			m_window;
} FE_WINDOW_HANDLE;

bool operator==(const FE_WINDOW_HANDLE& left,const FE_WINDOW_HANDLE& right);

} /* namespace ext */
} /* namespace fe */

#define FE_DISPLAY_HANDLE	Display*

#elif FE_2DGL==FE_2D_GDI

//* Win32 GDI
#define FE_WINDOW_HANDLE	HWND
#define FE_DISPLAY_HANDLE	void*

#elif FE_2DGL==FE_2D_OSX

#define FE_WINDOW_HANDLE	WindowRef
#define FE_DISPLAY_HANDLE	void*

#endif

#endif /* __window_WindowHandle_h__ */
