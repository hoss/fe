import sys
forge = sys.modules["forge"]

def setup(module):

	deplibs =	forge.corelibs + [
				"fexSignalLib",
				"fexDataToolDLLib",
				"fexWindowLib" ]

	tests = [ 'xFont', 'xWindow' ]

	if forge.fe_os == 'FE_WIN32' or forge.fe_os == 'FE_WIN64':
		deplibs += [	"fexDrawDLLib" ]
	else:
		tests += [ 'xRawKey' ]

	for t in tests:
		module.Exe(t)
		forge.deps([t + "Exe"], deplibs)

	forge.tests += [
		("xFont.exe",		"",								None,		None),
		("xWindow.exe",		"10",							None,		None) ]
