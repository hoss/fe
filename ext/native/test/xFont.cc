/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "window/window.h"
#include "draw/draw.h"

using namespace fe;
using namespace fe::ext;


int main(int argc,char** argv)
{
	UNIT_START();

	BWORD complete=FALSE;

	try
	{
		sp<Master> spMaster(new Master);
		sp<Registry> spRegistry=spMaster->registry();

		Result result=spRegistry->manage("fexNativeWindowDL");
		UNIT_TEST(successful(result));

		result=spRegistry->manage("fexOpenGLDL");
		UNIT_TEST(successful(result));

		{
			sp<WindowI> spWindowI(spRegistry->create("WindowI"));
			spWindowI->open(argv[0]);

			sp<DrawI> spDrawI(spRegistry->create("DrawI"));

			if(!spWindowI.isValid() || !spDrawI.isValid())
			{
				feX(argv[0], "can't continue");
			}

			sp<EventContextI> spEventContextI=spWindowI->getEventContextI();
			if(!spEventContextI.isValid())
			{
				feX(argv[0], "can't continue");
			}
		}

		complete=TRUE;
	}
	catch(Exception &e)
	{
		e.log();
	}
	catch(...)
	{
		feLog("uncaught exception\n");
	}

	UNIT_TEST(complete);
	UNIT_TEST(!Counted::trackerCount());
	UNIT_TRACK(4);
	UNIT_RETURN();
}

