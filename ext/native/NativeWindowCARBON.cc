/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <native/native.pmh>

namespace fe
{
namespace ext
{

#if FE_2DGL==FE_2D_OSX

Result NativeWindow::nativeInitialize(void)
{
	return e_ok;
}

Result NativeWindow::nativeStartup(void)
{
	return e_ok;
}

Result NativeWindow::nativeShutdown(void)
{
	return e_ok;
}

void NativeWindow::setPointerMotion(BWORD active)
{
}

void NativeWindow::setTitle(const String &title)
{
}

Result NativeWindow::open(const String title)
{
	return e_ok;
}

Result NativeWindow::close(void)
{
	return e_ok;
}

void NativeWindow::makeCurrent(void)
{
}

void NativeWindow::releaseCurrent(void)
{
}

void NativeWindow::swapBuffers(void)
{
}

#endif

} /* namespace ext */
} /* namespace fe */
