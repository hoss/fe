/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __native_NativeKeyboard_h__
#define __native_NativeKeyboard_h__

#if FE_2DGL==FE_2D_X_GFX

#include "window/WindowEvent.h"

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief WindowEvent handler that watches for a poll event and then
			feeds back any accumulated keyboard events

	@ingroup native
*//***************************************************************************/
class FE_DL_EXPORT NativeKeyboard:
	virtual public KeyI,
	public Initialize<NativeKeyboard>
{
	public:
				NativeKeyboard(void);
virtual			~NativeKeyboard(void);

		void	initialize(void);

				//* As HandlerI
virtual	void	handleBind(sp<SignalerI> spSignalerI, sp<Layout> spLayout);
virtual	void	handle(Record &record);

virtual	BWORD	generatesReleaseEvents(void)	{ return TRUE; }

	private:

		void	poll(void);

		Display*		m_pDisplay;
		Window			m_rootWindow;
		I32				m_xi_opcode;

		XIEventMask		m_eventMask[2];

		hp<SignalerI>	m_hpSignalerI;

		WindowEvent		m_event;
		WindowEvent		m_keyEvent;
		Record			m_keyRecord;

		I32				m_maxButtons;
		Array<I32>		m_pButtonState;

		BWORD			m_shift;
		BWORD			m_control;
		BWORD			m_alt;
		BWORD			m_capsLock;
};

} /* namespace ext */
} /* namespace fe */

#endif

#endif /* __native_NativeKeyboard_h__ */
