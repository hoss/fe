/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __native_NativeEventContext_h__
#define __native_NativeEventContext_h__

namespace fe
{
namespace ext
{

#define FE_WINDOW_EVENT_DEBUG	FALSE
#define FE_COMMANDLINE_LENGTH	1024

//* send mouse motion event even without buttons pressed
#define	FE_POINTERMOTION		FALSE


/**************************************************************************//**
	@brief Implementation of an event loop using the raw native system

	@ingroup native
*//***************************************************************************/
class FE_DL_EXPORT NativeEventContext:
		virtual public EventContextI,
		virtual public HandlerI,
		public Initialize<NativeEventContext>
{
	public:
						NativeEventContext(void);
virtual					~NativeEventContext(void);
virtual	void			initialize(void);

						//* As EventContextI
virtual	void			loop(BWORD dropout);
virtual	void			threadLock(void);
virtual	void			threadUnlock(void);

virtual	void			setPointerMotion(BWORD active);
virtual	BWORD			pointerMotion(void)			{ return m_pointerMotion; }

virtual	Real			multiplication(void)		{ return m_multiplication; }

virtual	void			registerWindow(WindowI *window);
virtual	void			deregisterWindow(WindowI *window);

						//* As HandlerI
virtual	void			handleBind(sp<SignalerI> spSignalerI,
							sp<Layout> spLayout);
virtual	void			handle(Record &record);

						//* As NativeEventContext
		Result			broadcastEvent(FE_WINDOW_HANDLE* pWindow);

static	U32				systemTimer(void);
		void			setTimer(U32 set)			{ m_timer=set; }
		U32				getTimer(void)				{ return m_timer; }

		void			accumulateTime(U32 exclusive,U32 inclusive);
		void			fps(F32 *exclusive,F32 *inclusive)
						{	*exclusive=m_fps_excl;
							*inclusive=m_fps_incl; }

		void			setCommandLine(char *line)
						{	strncpy(m_commandline,line,FE_COMMANDLINE_LENGTH);
							m_commandline[FE_COMMANDLINE_LENGTH-1]=0; }
const	char*			commandLine(void) const		{ return m_commandline; }

		void			endLoop(void)				{ m_endloop=true; }

		FE_DISPLAY_HANDLE	displayHandle(void)		{ return m_pDisplayHandle; }

		WindowEvent&	windowEvent(void)			{ return m_event; }

		void			setLastWindowHandle(FE_WINDOW_HANDLE handle)
						{	m_lastWindowHandle=handle; }

		void			notifyCreate(FE_WINDOW_HANDLE &windowHandle);

#if FE_2DGL==FE_2D_GDI
static LRESULT CALLBACK wndProc(HWND hWND,UINT message,
								WPARAM wParam,LPARAM lParam);
#endif

	private:
		void			nativeStartup(void);
		void			nativeShutdown(void);
		void			createEvent(void);

		Result			bindScope(sp<Scope> spScope);

//X		U32				loadFont(FE_WINDOW_HANDLE &windowHandle);
//X		void			useLoadedFont(FE_WINDOW_HANDLE &windowHandle);
//X		void			calcFontHeight(FE_WINDOW_HANDLE &windowHandle);

		hp<SignalerI>				m_hpSignalerI;
		sp<Layout>					m_spLayout;
		WindowEvent					m_poll;
		WindowEvent					m_event;
		Record						m_record;
		List<WindowI*>				m_windows;

		Accessor<FE_WINDOW_HANDLE*>	m_aWindowHandle;

		Real						m_multiplication;
		BWORD						m_endloop;
		BWORD						m_pointerMotion;
		char						m_commandline[FE_COMMANDLINE_LENGTH];

		U32							m_timer;
		U32							m_time_incl;
		U32							m_time_excl;
		U32							m_frames;
		F32							m_fps_incl;
		F32							m_fps_excl;

		FE_DISPLAY_HANDLE			m_pDisplayHandle;
		FE_WINDOW_HANDLE			m_lastWindowHandle;

#if FE_2DGL==FE_2D_X_GFX
		XFontStruct*				m_pFontStruct;
		Atom						m_atom[2];
		I32							m_threadLocks;	// just tracking
		I32							m_threadID;		// just tracking

	public:
		XFontStruct*				loadXFont(Display* a_pDisplayHandle,
											Real a_multiplication);
#elif FE_2DGL==FE_2D_GDI
		WindowEvent::MouseButtons	m_mousebuttons;

	public:
		void						setMouseButtons(
											WindowEvent::MouseButtons set)
									{	m_mousebuttons=set; }
		WindowEvent::MouseButtons	mouseButtons(void) const
									{	return m_mousebuttons; }
#endif
};

} /* namespace ext */
} /* namespace fe */

#endif /* __native_NativeEventContext_h__ */
