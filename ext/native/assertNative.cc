/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <native/native.pmh>

namespace fe
{
namespace ext
{

void TypeNative::initialize(void)
{
	sp<TypeMaster> spTypeMaster=registry()->master()->typeMaster();

	m_spT=spTypeMaster->assertType<FE_WINDOW_HANDLE*>("WindowHandlePtr");

	m_spT->setInfo(new InfoWindowHandlePtr());
}

TypeNative::~TypeNative(void)
{
	m_spT->setInfo(NULL);
}

} /* namespace ext */
} /* namespace fe */

