/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <native/native.pmh>

namespace fe
{
namespace ext
{

#if FE_2DGL==FE_2D_OSX

void NativeEventContext::nativeStartup(void)
{
}

void NativeEventContext::nativeShutdown(void)
{
}

void NativeEventContext::threadLock(void)
{
}

void NativeEventContext::threadUnlock(void)
{
}

void NativeEventContext::loop(BWORD dropout)
{
}

void NativeEventContext::setPointerMotion(BWORD active)
{
}

#endif

} /* namespace ext */
} /* namespace fe */


