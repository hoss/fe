/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __yaml_CatalogWriterYaml_h__
#define __yaml_CatalogWriterYaml_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
    @brief save catalog to yaml file

	@ingroup yaml
*//***************************************************************************/
class FE_DL_EXPORT CatalogWriterYaml:
	virtual public CatalogWriterI
{
	public:

virtual	BWORD	save(String a_filename,sp<Catalog> a_spCatalog);

	private:

		void	emitFloat(YAML::Emitter& a_rEmitter,float a_float);
		void	emitDouble(YAML::Emitter& a_rEmitter,double a_double);

		void	emit(YAML::Emitter& a_rEmitter,sp<Catalog> a_spCatalog,
						String a_key,String a_property);

		sp<YamlNode>	m_spYamlRoot;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __yaml_CatalogWriterYaml_h__ */
