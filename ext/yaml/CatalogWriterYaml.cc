/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <yaml/yaml.pmh>

#define FE_CWY_DEBUG	FALSE
#define FE_CWY_NODAL	FALSE
#define FE_CWY_TAG_ALL	FALSE

namespace fe
{
namespace ext
{

void CatalogWriterYaml::emitFloat(YAML::Emitter& a_rEmitter,float a_float)
{
	const I32 intValue(a_float);
	if(a_float==intValue)
	{
		String text;
		text.sPrintf("%d.0",intValue);
		a_rEmitter<<YAML::Value<<text.c_str();
	}
	else
	{
		a_rEmitter<<YAML::Value<<a_float;
	}
}

void CatalogWriterYaml::emitDouble(YAML::Emitter& a_rEmitter,double a_double)
{
	const I32 intValue(a_double);
	if(a_double==intValue)
	{
		String text;
		text.sPrintf("%d.0",intValue);
		a_rEmitter<<YAML::Value<<text.c_str();
	}
	else
	{
		a_rEmitter<<YAML::Value<<a_double;
	}
}

/***
	https://yaml.org/type/

	YAML::VerbatimTag("bool")		-> !<bool>
	YAML::LocalTag("bool")			-> !bool
	YAML::LocalTag("prefix","bool")	-> !prefix!bool
	YAML::SecondaryTag("bool")		-> !!bool
*/

void CatalogWriterYaml::emit(YAML::Emitter& a_rEmitter,
	sp<Catalog> a_spCatalog,String a_key,String a_property)
{
	const String entryType=a_spCatalog->catalogTypeName(a_key,a_property);
	const String entryValue=a_spCatalog->catalogValue(a_key,a_property);

	if(entryType=="string")
	{
#if FE_CWY_TAG_ALL
		//* quotes are supposed to be good enough
//		a_rEmitter<<YAML::SecondaryTag("str");
#endif

		a_rEmitter<<YAML::DoubleQuoted;
		a_rEmitter<<YAML::Value<<entryValue.c_str();
	}
	else if(a_spCatalog->catalogInstance(a_key,a_property).is<bool>())
	{
#if FE_CWY_TAG_ALL
		a_rEmitter<<YAML::SecondaryTag("bool");
#endif
		a_rEmitter<<YAML::Value<<a_spCatalog->catalog<bool>(a_key,a_property);
	}
	else if(a_spCatalog->catalogInstance(a_key,a_property).is<I32>())
	{
#if FE_CWY_TAG_ALL
		a_rEmitter<<YAML::SecondaryTag("int");
#endif
		a_rEmitter<<YAML::Value<<a_spCatalog->catalog<I32>(a_key,a_property);
	}
	else if(a_spCatalog->catalogInstance(a_key,a_property).is<float>())
	{
#if FE_CWY_TAG_ALL
		a_rEmitter<<YAML::SecondaryTag("float");
#endif
		emitFloat(a_rEmitter,a_spCatalog->catalog<float>(a_key,a_property));
	}
	else if(a_spCatalog->catalogInstance(a_key,a_property).is<double>())
	{
		a_rEmitter<<YAML::LocalTag("double");
		emitDouble(a_rEmitter,a_spCatalog->catalog<double>(a_key,a_property));
	}
	else if(a_spCatalog->catalogInstance(a_key,a_property).is<Vector3>())
	{
#if FE_CWY_TAG_ALL
		a_rEmitter<<YAML::SecondaryTag("vector3f");
#endif

		const Vector3 value3=
				a_spCatalog->catalog<Vector3>(a_key,a_property);

		a_rEmitter<<YAML::Flow;
		a_rEmitter<<YAML::BeginSeq;
		emitFloat(a_rEmitter,value3[0]);
		emitFloat(a_rEmitter,value3[1]);
		emitFloat(a_rEmitter,value3[2]);
		a_rEmitter<<YAML::EndSeq;
	}
	else if(a_spCatalog->catalogInstance(a_key,a_property).is<Vector3d>())
	{

		a_rEmitter<<YAML::LocalTag("vector3d");

		const Vector3d value3=
				a_spCatalog->catalog<Vector3d>(a_key,a_property);

		a_rEmitter<<YAML::Flow;
		a_rEmitter<<YAML::BeginSeq;
		emitDouble(a_rEmitter,value3[0]);
		emitDouble(a_rEmitter,value3[1]);
		emitDouble(a_rEmitter,value3[2]);
		a_rEmitter<<YAML::EndSeq;
	}
	else
	{
		a_rEmitter<<YAML::LocalTag(entryType.c_str());

		a_rEmitter<<YAML::DoubleQuoted;
		a_rEmitter<<YAML::Value<<entryValue.c_str();
	}
}

BWORD CatalogWriterYaml::save(String a_filename,sp<Catalog> a_spCatalog)
{
#if FE_CWY_DEBUG
	feLog("CatalogWriterYaml::save \"%s\"\n",a_filename.c_str());
#endif

	m_spYamlRoot=sp<YamlNode>(new YamlNode());

	std::ofstream ostrm(a_filename.c_str());
	if(!ostrm.is_open())
	{
		feLog("CatalogWriterYaml::save failed to open \"%s\"\n",
				a_filename.c_str());
		return FALSE;
	}

	YAML::Emitter emitter;

#if !FE_CWY_NODAL
	emitter<<YAML::BeginMap;
#endif

	Array<String> keys;
	a_spCatalog->catalogKeys(keys);
	const U32 keyCount=keys.size();
	for(U32 keyIndex=0;keyIndex<keyCount;keyIndex++)
	{
		const String key=keys[keyIndex];
#if FE_CWY_DEBUG
		feLog("key \"%s\"\n",key.c_str());
#endif

		const String keyType=a_spCatalog->catalogTypeName(key);
		const String keyValue=a_spCatalog->catalogValue(key);

		Array<String> properties;
		a_spCatalog->catalogProperties(key,properties);
		const U32 propertyCount=properties.size();

#if FE_CWY_NODAL
		YAML::Node keyNode=m_spYamlRoot->node()[key.c_str()];
		keyNode["type"]=keyType.c_str();
		keyNode["value"]=keyValue.c_str();
#else
		if(propertyCount>1)
		{
			emitter<<YAML::Key<<key.c_str();
			emitter<<YAML::BeginMap;
			emitter<<YAML::Key<<"value";
			emit(emitter,a_spCatalog,key,"value");
		}
		else
		{
			emitter<<YAML::Key<<key.c_str();
			emit(emitter,a_spCatalog,key,"value");

			continue;
		}
#endif

		for(U32 propertyIndex=0;propertyIndex<propertyCount;propertyIndex++)
		{
			const String propertyName=properties[propertyIndex];
			if(propertyName=="value")
			{
				continue;
			}

#if FE_CWY_DEBUG
			feLog("  propertyName \"%s\"\n",propertyName.c_str());
#endif

			const String propertyType=
					a_spCatalog->catalogTypeName(key,propertyName);
			const String propertyValue=
					a_spCatalog->catalogValue(key,propertyName);

#if FE_CWY_NODAL
			YAML::Node propertyNode=keyNode[propertyName.c_str()];
			propertyNode["type"]=propertyType.c_str();
			propertyNode["value"]=propertyValue.c_str();
#else
			emitter<<YAML::Key<<propertyName.c_str();
			emit(emitter,a_spCatalog,key,propertyName);
#endif
		}

#if !FE_CWY_NODAL
		emitter<<YAML::EndMap;
#endif
	}

#if FE_CWY_NODAL
	emitter<<m_spYamlRoot->node();
#else
	emitter<<YAML::EndMap;
#endif

	ostrm<<emitter.c_str();

	return TRUE;
}

} /* namespace ext */
} /* namespace fe */
