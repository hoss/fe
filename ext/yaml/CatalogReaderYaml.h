/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __yaml_CatalogReaderYaml_h__
#define __yaml_CatalogReaderYaml_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
    @brief load catalog from yaml file

	@ingroup yaml

@code
# Examples

# simple import (one liner)
- import: model/biplane.yaml

# simple import list
- import:
    - model/biplane.yaml
    - model/triplane.yaml

# conditional import of files, forcing variable values in imported files
- import:
    files:
      - model/high_wing.yaml
      - model/low_wing.yaml
    only:
      variables:
        - $ADD_MONOPLANES != 0
    variables:
      ENGINE_COUNT: 2

# import variable values (child values will replace local values)
- import:
    adopt_variables: true
    files:
      - override.yaml

# conditional replacement of local variables
- variables:
    only:
      variables:
        - $PLANE_INDEX == 2
    PLANE_INDEX: 7

# catalog entries
- state:
  planes.${PLANE_INDEX}.width: 10.0
  planes.${PLANE_INDEX}.engine_count: ${ENGINE_COUNT}
@endcode
*//***************************************************************************/
class FE_DL_EXPORT CatalogReaderYaml:
	public CatalogReaderBase
{
	public:

virtual	BWORD	load(String a_filename,sp<Catalog> a_spCatalog);

	private:

		BWORD	loadWithVariables(String a_pathname,String a_filename,
						sp<Catalog> a_spCatalog,
						std::map<String,String>& a_variableMapIn,
						std::map<String,String>& a_variableMapOut);

		void	addEntry(sp<Catalog> a_spCatalog,String a_keyName,
						String a_propertyName,const YAML::Node a_keyNode);

		void	addState(sp<Catalog> a_spCatalog,const YAML::Node a_yamlNode);

		void	addImport(sp<Catalog> a_spCatalog,
						const YAML::Node a_yamlNode,String a_pathname,
						BWORD a_adoptVariables,
						std::map<String,String>& a_variableMapIn,
						std::map<String,String>& a_variableMapOut);

		BWORD	checkOnly(const YAML::Node a_yamlNode,
						std::map<String,String>& a_variableMap);

		String	m_rootPath;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __yaml_CatalogReaderYaml_h__ */
