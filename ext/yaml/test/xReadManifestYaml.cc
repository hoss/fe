/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "fe/plugin.h"
#include "math/math.h"

using namespace fe;

int main(int argc,char** argv)
{
	UNIT_START();

	BWORD complete=FALSE;

	if(argc<2)
	{
		feLog("Usage: %s [yaml file]\n",argv[0]);
		return -1;
	}
	String filename=argv[1];

	try
	{
		sp<Master> spMaster(new Master);
		sp<Registry> spRegistry=spMaster->registry();

		Result result=spRegistry->manage("fexYamlDL");
		UNIT_TEST(successful(result));

		sp<ManifestReaderI> spManifestReader=
				spRegistry->create("ManifestReaderI.*.*.yaml");
		UNIT_TEST(spManifestReader.isValid());
		if(!spManifestReader.isValid())
		{
			feX(argv[0],"spManifestReader invalid");
		}

		feLog("loading \"%s\"\n",filename.c_str());
		const BWORD loaded=spManifestReader->load(filename);
		UNIT_TEST(loaded);

		complete=TRUE;
	}
	catch(Exception &e) { e.log(); }
	catch(...)			{ feLog("uncaught exception\n"); }

	UNIT_TEST(complete);
	UNIT_TEST(!Counted::trackerCount());
	UNIT_TRACK(5);
	UNIT_RETURN();
}
