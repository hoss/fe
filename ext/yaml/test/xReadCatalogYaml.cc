/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "fe/plugin.h"
#include "math/math.h"

using namespace fe;

int main(int argc,char** argv)
{
	UNIT_START();

	BWORD complete=FALSE;

	if(argc<2)
	{
		feLog("Usage: %s [yaml file]\n",argv[0]);
		return -1;
	}
	String filename=argv[1];

	try
	{
		sp<Master> spMaster(new Master);
		sp<Registry> spRegistry=spMaster->registry();

		Result result=spRegistry->manage("fexYamlDL");
		UNIT_TEST(successful(result));

		sp<Catalog> spCatalogIn=spMaster->createCatalog("input");
		UNIT_TEST(spCatalogIn.isValid());
		if(!spCatalogIn.isValid())
		{
			feX(argv[0],"spCatalogIn invalid");
		}

		sp<CatalogReaderI> spCatalogReader=
				spRegistry->create("CatalogReaderI.*.*.yaml");
		UNIT_TEST(spCatalogReader.isValid());
		if(!spCatalogReader.isValid())
		{
			feX(argv[0],"spCatalogReader invalid");
		}

		feLog("loading \"%s\"\n",filename.c_str());
		const BWORD loaded=spCatalogReader->load(filename,spCatalogIn);
		UNIT_TEST(loaded);

		feLog("\nCatalog in:\n");
		spCatalogIn->catalogDump();

		complete=TRUE;
	}
	catch(Exception &e) { e.log(); }
	catch(...)			{ feLog("uncaught exception\n"); }

	UNIT_TEST(complete);
	UNIT_TEST(!Counted::trackerCount());
	UNIT_TRACK(6);
	UNIT_RETURN();
}
