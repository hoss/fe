/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "fe/plugin.h"
#include "math/math.h"

using namespace fe;

int main(int argc,char** argv)
{
	UNIT_START();

	BWORD complete=FALSE;

	if(argc<2)
	{
		feLog("Usage: %s [yaml file]\n",argv[0]);
		return -1;
	}
	String filename=argv[1];

	try
	{
		sp<Master> spMaster(new Master);
		sp<Registry> spRegistry=spMaster->registry();

		Result result=spRegistry->manage("fexYamlDL");
		UNIT_TEST(successful(result));

		sp<Catalog> spCatalogOut=spMaster->createCatalog("output");
		UNIT_TEST(spCatalogOut.isValid());
		if(!spCatalogOut.isValid())
		{
			feX(argv[0],"spCatalogOut invalid");
		}

		spCatalogOut->catalog<String>("A")="alpha";
		spCatalogOut->catalog<bool>("B")=true;
		spCatalogOut->catalog<Real>("C")=3.14;
		spCatalogOut->catalog<String>("C","hint")="it's pi";
		spCatalogOut->catalog<String>("E")="2.71";
		spCatalogOut->catalog<SpatialVector>("F")=SpatialVector(1.2,3,4.5);
		spCatalogOut->catalog<Vector3d>("G")=SpatialVector(M_PI,M_E,0);
		spCatalogOut->catalog<String>("H")=" spaced ";
		spCatalogOut->catalog<I32>("I")=7;
		spCatalogOut->catalog<Real>("J")=7.0;
		spCatalogOut->catalog<double>("K")=M_SQRT2;
		spCatalogOut->catalog<Quaterniond>("Q")=Quaterniond(0,0,0,1);

		feLog("\nCatalog out:\n");
		spCatalogOut->catalogDump();

		sp<CatalogWriterI> spCatalogWriter=
				spRegistry->create("CatalogWriterI.*.*.yaml");
		UNIT_TEST(spCatalogWriter.isValid());
		if(!spCatalogWriter.isValid())
		{
			feX(argv[0],"spCatalogWriter invalid");
		}

		System::createParentDirectories(filename);

		feLog("saving \"%s\"\n",filename.c_str());
		spCatalogWriter->save(filename,spCatalogOut);

		sp<CatalogReaderI> spCatalogReader=
				spRegistry->create("CatalogReaderI.*.*.yaml");
		UNIT_TEST(spCatalogReader.isValid());
		if(!spCatalogReader.isValid())
		{
			feX(argv[0],"spCatalogReader invalid");
		}

		sp<Catalog> spCatalogIn=spMaster->createCatalog("input");
		UNIT_TEST(spCatalogIn.isValid());
		if(!spCatalogIn.isValid())
		{
			feX(argv[0],"spCatalogIn invalid");
		}

		feLog("reloading \"%s\"\n",filename.c_str());
		spCatalogReader->load(filename,spCatalogIn);

		feLog("\nCatalog in:\n");
		spCatalogIn->catalogDump();

		complete=TRUE;
	}
	catch(Exception &e) { e.log(); }
	catch(...)			{ feLog("uncaught exception\n"); }

	UNIT_TEST(complete);
	UNIT_TEST(!Counted::trackerCount());
	UNIT_TRACK(7);
	UNIT_RETURN();
}
