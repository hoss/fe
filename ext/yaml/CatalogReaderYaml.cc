/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <yaml/yaml.pmh>

#define FE_CRY_VERBOSE	FALSE
#define FE_CRY_DEBUG	FALSE

namespace fe
{
namespace ext
{

void CatalogReaderYaml::addEntry(sp<Catalog> a_spCatalog,String a_keyName,
	String a_propertyName,const YAML::Node a_keyNode)
{
#if FE_CRY_VERBOSE
	feLog("CatalogReaderYaml::addEntry key \"%s\" prop \"%s\"\n",
			a_keyName.c_str(),a_propertyName.c_str());
#endif

	YAML::Node keyValueNode;
	if(a_keyNode.IsMap())
	{
		if(!a_keyNode["value"])
		{
			return;
		}
		keyValueNode=a_keyNode["value"];
	}
	else
	{
		keyValueNode=a_keyNode;
	}

	const String tag(keyValueNode.Tag().c_str());
	String yamlTag=tag.prechop("tag:yaml.org,2002:");
	String localTag;
	if(yamlTag==tag)
	{
		yamlTag="";
		localTag=tag.prechop("!");
		if(localTag==tag)
		{
			localTag="";
		}
	}

#if FE_CRY_VERBOSE
	feLog("CatalogReaderYaml::addEntry"
			"   tag \"%s\" yamlTag \"%s\" localTag \"%s\"\n",
			tag.c_str(),yamlTag.c_str(),localTag.c_str());
#endif

	String keyType;
	String keyValue;

	if(keyValueNode.IsSequence())
	{
		//* TODO other vectors

		const I32 sequenceCount=keyValueNode.size();

		if(localTag=="" && sequenceCount)
		{
			if(sequenceCount==3)
			{
				localTag="vector3f";
				for(I32 m=0;m<3;m++)
				{
					const String entry=
							keyValueNode[m].as<std::string>().c_str();
					if(entry!="0" && entry!="0.0" && entry.real()==Real(0))
					{
						localTag="";
						break;
					}
				}
			}

			//* TODO other array types

			//* default presumes just strings
			if(localTag.empty())
			{
				localTag="stringarray";
			}
		}

		if(localTag=="vector3d")
		{
			keyType=localTag;
			keyValue.sPrintf("%.15G %.15G %.15G",
					keyValueNode[0].as<double>(),
					keyValueNode[1].as<double>(),
					keyValueNode[2].as<double>());
		}
		else if(localTag=="vector3")
		{
			keyType="vector3";
			keyValue.sPrintf("%.6G %.6G %.6G",
					keyValueNode[0].as<Real>(),
					keyValueNode[1].as<Real>(),
					keyValueNode[2].as<Real>());
		}
		else if(localTag=="vector3f")
		{
			keyType="vector3f";
			keyValue.sPrintf("%.6G %.6G %.6G",
					keyValueNode[0].as<float>(),
					keyValueNode[1].as<float>(),
					keyValueNode[2].as<float>());
		}
		else if(localTag=="stringarray")
		{
			keyType="stringarray";

			Array<String> stringArray(sequenceCount);
			for(I32 m=0;m<sequenceCount;m++)
			{
				stringArray[m]=keyValueNode[m].as<std::string>().c_str();
			}

			registry()->master()->typeMaster()->output(stringArray,keyValue);
		}
	}
	else
	{
		keyValue=keyValueNode.as<std::string>().c_str();

#if FE_CRY_VERBOSE
		feLog("CatalogReaderYaml::addEntry   as real %.6G\n",keyValue.real());
#endif

		String lower=keyValue;
		lower.forceLowercase();

		if(tag=="!" || yamlTag=="str")
		{
			keyType="string";
		}
		else if(yamlTag=="bool")
		{
			keyType="bool";
		}
		else if(yamlTag=="int")
		{
			keyType="integer";
		}
		else if(yamlTag=="float")
		{
			keyType="float";
		}
		else if(localTag!="" && localTag!="?")
		{
			keyType=localTag;
		}
		else if(lower == "true" || lower == "false")
		{
			keyType="bool";
		}
		else if(keyValue!="0" && keyValue!="0.0" &&
				keyValue.real()==Real(0))
		{
			keyType="string";
		}
		else if(keyValue.contains("."))
		{
			keyType="float";
		}
		else
		{
			keyType="integer";
		}
	}

#if FE_CRY_VERBOSE
	feLog("CatalogReaderYaml::addEntry"
			"   --> type \"%s\" value \"%s\"\n",
			keyType.c_str(),keyValue.c_str());
#endif

	if(!keyType.empty())
	{
		addInstance(a_spCatalog,a_keyName,a_propertyName,keyType,keyValue);
	}
}

void CatalogReaderYaml::addState(sp<Catalog> a_spCatalog,
	const YAML::Node a_yamlNode)
{
	for(YAML::const_iterator it=a_yamlNode.begin();it!=a_yamlNode.end();it++)
	{
		const YAML::Node valueNode=it->second;
		if(!valueNode.IsDefined() || valueNode.IsNull())
		{
			continue;
		}

		const String keyName(it->first.as<std::string>().c_str());

		if(keyName=="only")
		{
			continue;
		}

		addEntry(a_spCatalog,keyName,"value",valueNode);

		if(!valueNode.IsMap())
		{
			continue;
		}

		for(YAML::const_iterator it2=valueNode.begin();
				it2!=valueNode.end();it2++)
		{
			const String propertyName(it2->first.as<std::string>().c_str());

			if(propertyName=="type" || propertyName=="value")
			{
				continue;
			}

			const YAML::Node propertyNode=it2->second;

			addEntry(a_spCatalog,keyName,propertyName,propertyNode);
		}
	}
}

void CatalogReaderYaml::addImport(sp<Catalog> a_spCatalog,
	const YAML::Node a_yamlNode,String a_pathname,
	BWORD a_adoptVariables,
	std::map<String,String>& a_variableMapIn,
	std::map<String,String>& a_variableMapOut)
{
#if FE_CRY_VERBOSE
	feLog("CatalogReaderYaml::addImport pathname \"%s\"\n",
			a_pathname.c_str());
	for(std::map<String,String>::iterator it=
			a_variableMapOut.begin();
			it!=a_variableMapOut.end(); it++)
	{
		const String key(it->first);
		const String value(it->second);

		feLog("  var  \"%s\" \"%s\"\n",
				key.c_str(),value.c_str());
	}
#endif

	std::map<String,String>	variableMapIn;
	std::map<String,String>	variableMapOut;

	//* NOTE simple import (one liner)
	//* import: model/biplane.yaml
	if(a_yamlNode.IsScalar())
	{
		const String filename(a_yamlNode.as<std::string>().c_str());

		loadWithVariables(a_pathname,filename,a_spCatalog,
				variableMapIn,variableMapOut);
		return;
	}

	//* NOTE simple import list
	//*	- import:
	//*		- model/biplane.yaml
	//*		- model/triplane.yaml
	if(a_yamlNode.IsSequence())
	{
		const I32 nodeCount=a_yamlNode.size();
		for(I32 nodeIndex=0;nodeIndex<nodeCount;nodeIndex++)
		{
			const String filename(
					a_yamlNode[nodeIndex].as<std::string>().c_str());

			loadWithVariables(a_pathname,filename,a_spCatalog,
					variableMapIn,variableMapOut);
		}
		return;
	}

	//* NOTE otherwise, expect long form with subsections, like 'files:'

	if(!a_yamlNode.IsMap())
	{
		feLog("CatalogReaderYaml::addImport NOT MAP\n");
		return;
	}

	if(!a_yamlNode["files"])
	{
		return;
	}

	if(a_yamlNode["variables"])
	{
		const YAML::Node variableNode=a_yamlNode["variables"];
		if(variableNode.IsMap())
		{
			for(YAML::const_iterator it=variableNode.begin();
					it!=variableNode.end();it++)
			{
				const YAML::Node keyNode=it->first;
				const YAML::Node valueNode=it->second;

				const String variable(keyNode.as<std::string>().c_str());
				const String value(valueNode.as<std::string>().c_str());

#if FE_CRY_DEBUG
				feLog("      variable %s=\"%s\"\n",
						variable.c_str(),value.c_str());
#endif

				variableMapIn[variable]=value;
			}
		}
	}

	//* NOTE for loop (currently presumes very limited syntax)
	/***
		- import:
			for:
			  init: VAR = $START
			  cond: VAR < $LIMIT	# < >
			  step: VAR++			# ++ -- += -=
	*/
	String indexVariable;
	I32 initValue(0);
	String condition("<");
	I32 condValue(1);
	I32 stepValue(1);

	if(a_yamlNode["for"])
	{
		const YAML::Node forNode=a_yamlNode["for"];
		if(forNode.IsMap())
		{
			if(forNode["init"])
			{
				const YAML::Node initNode=forNode["init"];
				String text(initNode.as<std::string>().c_str());
				indexVariable = text.parse();
				text.parse();
				initValue=text.parse().integer();
			}
			if(forNode["cond"])
			{
				const YAML::Node condNode=forNode["cond"];
				String text(condNode.as<std::string>().c_str());
				text.parse();
				condition=text.parse();
				condValue=text.parse().integer();
			}
			if(forNode["step"])
			{
				const YAML::Node stepNode=forNode["step"];
				String text(stepNode.as<std::string>().c_str());
				text.parse("\""," \t+-");
				const String op=text.parse();
				if(op=="++")
				{
					stepValue=1;
				}
				else if(op=="--")
				{
					stepValue= -1;
				}
				else if(op=="+=")
				{
					stepValue=text.parse().integer();
				}
				else if(op=="-=")
				{
					stepValue= -text.parse().integer();
				}
			}
		}
	}

//	feLog("index \"%s\" init %d cond \"%s\" %d step %d\n",
//			indexVariable.c_str(),initValue,
//			condition.c_str(),condValue,stepValue);

	I32 index(initValue);

	while(TRUE)
	{
		if(condition=="<" && index>=condValue)
		{
			break;
		}
		if(condition==">" && index<=condValue)
		{
			break;
		}

		if(!indexVariable.empty())
		{
			variableMapIn[indexVariable].sPrintf("%d",index);
		}

		const YAML::Node filesNode=a_yamlNode["files"];
		if(filesNode.IsScalar())
		{
			const String filename(filesNode.as<std::string>().c_str());

			loadWithVariables(a_pathname,filename,a_spCatalog,
					variableMapIn,variableMapOut);
		}
		if(filesNode.IsSequence())
		{
			const I32 nodeCount=filesNode.size();
			for(I32 nodeIndex=0;nodeIndex<nodeCount;nodeIndex++)
			{
				const String filename(
						filesNode[nodeIndex].as<std::string>().c_str());

				loadWithVariables(a_pathname,filename,a_spCatalog,
						variableMapIn,variableMapOut);
			}
		}

		if(a_adoptVariables)
		{
			for(std::map<String,String>::iterator it=
					variableMapOut.begin();
					it!=variableMapOut.end(); it++)
			{
				const String key(it->first);
				const String value(it->second);

//				feLog("adopt \"%s\" \"%s\"\n",
//						key.c_str(),value.c_str());

				//* NOTE don't clobber any incoming variables
				if(a_variableMapIn.find(key)!=a_variableMapIn.end())
				{
					continue;
				}

				//* NOTE don't replace _FILE_BASE, etc
				if(key.match("_.*"))
				{
					continue;
				}

				a_variableMapOut[key]=value;
			}
		}

		index+=stepValue;
	}
}

//* NOTE use "filename.yaml?_ROOT_PATH=/x" for alternate root of absolute names
BWORD CatalogReaderYaml::load(String a_filename,sp<Catalog> a_spCatalog)
{
#if FE_CRY_VERBOSE
	feLog("CatalogReaderYaml::load \"%s\"\n",a_filename.c_str());
#endif

	m_rootPath="";

	std::map<String,String>	variableMapIn;

	String url=a_filename;
	String filename=url.parse("","?");
	url=url.prechop("?");
	while(!url.empty())
	{
		String value=url.parse("","&");
		String key=value.parse("","=");
		value=value.prechop("=");
#if FE_CRY_VERBOSE
		feLog("CatalogReaderYaml::load '%s'=\"%s\"\n",
				key.c_str(),value.c_str());
#endif

		variableMapIn[key]=value;

		if(key=="_ROOT_PATH")
		{
			m_rootPath=value;
		}
	}

	const String cwd=System::getCurrentWorkingDirectory();

	std::map<String,String>	variableMapOut;
	return loadWithVariables(cwd,filename,a_spCatalog,
			variableMapIn,variableMapOut);
}

BWORD CatalogReaderYaml::loadWithVariables(String a_pathname,String a_filename,
	sp<Catalog> a_spCatalog,
	std::map<String,String>& a_variableMapIn,
	std::map<String,String>& a_variableMapOut)
{
#if FE_CRY_VERBOSE
	feLog("CatalogReaderYaml::loadWithVariables"
			" pathname \"%s\" filename \"%s\" map size %d\n",
			a_pathname.c_str(),a_filename.c_str(),a_variableMapIn.size());
#endif

	a_variableMapOut=a_variableMapIn;

	const BWORD absolute=(a_filename.c_str()[0]=='/');

	const String fullname=absolute? m_rootPath+a_filename:
			a_pathname.empty()? a_filename: a_pathname+"/"+a_filename;

	std::ifstream testFile;
	testFile.open(fullname.c_str());
	if(testFile)
	{
		testFile.close();
	}
	else
	{
		feLog("CatalogReaderYaml::loadWithVariables"
				" could not open \"%s\"\n",
				fullname.c_str());
		return FALSE;
	}

	String homePath;
	System::getHomePath(homePath);

	a_variableMapOut["_HOME_PATH"]="\""+homePath+"\"";
	a_variableMapOut["_FILE_PATH"]="\""+fullname.pathname()+"\"";
	a_variableMapOut["_FILE_BASE"]="\""+fullname.basename()+"\"";

#if FE_CRY_VERBOSE
	feLog("CatalogReaderYaml::loadWithVariables"
			" _HOME_PATH \"%s\" _FILE_PATH \"%s\" _FILE_BASE \"%s\"\n",
			a_variableMapOut["_HOME_PATH"].c_str(),
			a_variableMapOut["_FILE_PATH"].c_str(),
			a_variableMapOut["_FILE_BASE"].c_str());
#endif

	std::ifstream infile(fullname.c_str());
	std::stringstream fileStream;
	fileStream << infile.rdbuf();

	YAML::Node yamlRoot;

	//* NOTE if root is not a sequence, it may be a raw state block
	try
	{
		//* don't try to substitute if it is a sequence
		yamlRoot=YAML::Load(fileStream.str().c_str());

		if(!yamlRoot.IsSequence())
		{
			const String text=String(fileStream.str().c_str()).substituteEnv(
					a_variableMapOut);

			yamlRoot=YAML::Load(text.c_str());
		}
	}
	catch(const std::exception& e)
	{
		feLog("CatalogReaderYaml::loadWithVariables"
				" caught exception loading \"%s\" \"%s\"\n",
				fullname.c_str(),e.what());
		return FALSE;
	}
	catch(...)
	{
		feLog("CatalogReaderYaml::loadWithVariables"
				" uncaught exception loading \"%s\"\n",
				fullname.c_str());
		return FALSE;
	}

	if(!yamlRoot.IsSequence())
	{
		if(yamlRoot.IsMap())
		{
			addState(a_spCatalog,yamlRoot);
		}

		return TRUE;
	}

	const I32 maxLine(1024);
	char lineBuffer[maxLine+1];

	//* skip any prelude of comments and whitespace
	while(!fileStream.eof() && fileStream.peek()!='-')
	{
		fileStream.getline(lineBuffer,maxLine);
	}

	while(TRUE)
	{
		if(fileStream.eof())
		{
//			feLog("EOF\n----\n");
			break;
		}

		std::stringstream blockStream;
		fileStream.getline(lineBuffer,maxLine);
		blockStream.write(lineBuffer,strlen(lineBuffer));
		blockStream.put('\n');

		while(!fileStream.eof() && fileStream.peek()!='-')
		{
			fileStream.getline(lineBuffer,maxLine);
			blockStream.write(lineBuffer,strlen(lineBuffer));
			blockStream.put('\n');
		}

//		feLog("BLOCK:\n%s",blockStream.str().c_str());

		try
		{
			yamlRoot=YAML::Load(blockStream.str().c_str());
		}
		catch(const std::exception& e)
		{
			feLog("CatalogReaderYaml::loadWithVariables"
					" caught exception loading \"%s\" \"%s\"\n",
					fullname.c_str(),e.what());
			return FALSE;
		}
		catch(...)
		{
			feLog("CatalogReaderYaml::loadWithVariables"
					" uncaught exception loading \"%s\" %d\n",
					fullname.c_str());
			return FALSE;
		}

#if FE_CRY_DEBUG
		feLog("CatalogReaderYaml::loadWithVariables root size %d\n",
				yamlRoot.size());
#endif
		if(!yamlRoot.IsSequence())
		{
			feLog("CatalogReaderYaml::loadWithVariables"
					" block is not sequence\n");
			continue;
		}

		const I32 nodeCount=yamlRoot.size();
		for(I32 nodeIndex=0;nodeIndex<nodeCount;nodeIndex++)
		{
#if FE_CRY_DEBUG
			feLog("Node %d/%d\n",nodeIndex,nodeCount);
#endif

			const YAML::Node yamlNode=yamlRoot[nodeIndex];

			if(!yamlNode.IsMap())
			{
#if FE_CRY_DEBUG
				feLog("NOT MAP\n");
#endif
				continue;
			}

			YAML::const_iterator it=yamlNode.begin();
			if(it==yamlNode.end())
			{
#if FE_CRY_DEBUG
				feLog("NO ZERO\n");
#endif
				continue;
			}

			const YAML::Node keyNode=it->first;
			if(!keyNode.IsDefined() || keyNode.IsNull())
			{
#if FE_CRY_DEBUG
				feLog("NOT VALID\n");
#endif
				continue;
			}

			const String directive(keyNode.as<std::string>().c_str());

#if FE_CRY_DEBUG
			feLog("CatalogReaderYaml::loadWithVariables"
					" directive \"%s\"\n",directive.c_str());
#endif

			const YAML::Node valueNode=it->second;

			if(directive=="variables")
			{
				const YAML::Node valueNode=it->second;
				if(!checkOnly(valueNode,a_variableMapOut))
				{
#if FE_CRY_DEBUG
					feLog("FAILED ONLY CHECK\n");
#endif
					continue;
				}

				for(YAML::const_iterator it=valueNode.begin();
						it!=valueNode.end();it++)
				{
					const YAML::Node subKeyNode=it->first;
					const YAML::Node subValueNode=it->second;


					if(!subKeyNode.IsDefined() || subKeyNode.IsNull())
					{
#if FE_CRY_DEBUG
						feLog("CatalogReaderYaml::loadWithVariables"
								" key def %d null %d \n",
								subKeyNode.IsDefined(),subKeyNode.IsNull());
#endif
						continue;
					}

					const String variable(
							subKeyNode.as<std::string>().c_str());

					if(!subValueNode.IsDefined() || !subValueNode.IsScalar())
					{
#if FE_CRY_DEBUG
						feLog("CatalogReaderYaml::loadWithVariables"
								" variable \"%s\" def %d scalar %d \n",
								variable.c_str(),
								subValueNode.IsDefined(),
								subValueNode.IsScalar());
#endif
						continue;
					}

					const String tag(subValueNode.Tag().c_str());

					String value(subValueNode.as<std::string>().c_str());
					if(tag=="!")
					{
						value="\""+value+"\"";
					}
					value=value.substituteEnv(a_variableMapOut);

#if FE_CRY_VERBOSE
					feLog("CatalogReaderYaml::loadWithVariables"
							" variable %s=\"%s\" tag=\"%s\"\n",
							variable.c_str(),value.c_str(),
							tag.c_str());
#endif

					//* NOTE don't clobber any incoming variables
					if(a_variableMapIn.find(variable)==a_variableMapIn.end())
					{
						a_variableMapOut[variable]=value;
					}
				}

				continue;
			}

			//* NOTE resolve all variables first

			YAML::Node yamlRoot2;
			YAML::Node valueNode2;

			try
			{
				const String text=
						String(blockStream.str().c_str())
						.stripComments("#","\n",FALSE,TRUE)
						.substituteEnv(a_variableMapOut,FALSE,TRUE);

//				feLog("SUBSTITUTED:\n%s",text.c_str());

				yamlRoot2=YAML::Load(text.c_str());

				const YAML::Node yamlNode2=yamlRoot2[0];
				YAML::const_iterator it2=yamlNode2.begin();
				valueNode2=it2->second;
			}
			catch(const std::exception& e)
			{
				feLog("CatalogReaderYaml::loadWithVariables"
						" caught exception loading \"%s\" \"%s\"\n",
						fullname.c_str(),e.what());
				return FALSE;
			}
			catch(...)
			{
				feLog("CatalogReaderYaml::loadWithVariables"
						" uncaught exception loading \"%s\" %d\n",
						fullname.c_str());
				return FALSE;
			}

			if(directive=="import")
			{
				if(checkOnly(valueNode2,a_variableMapOut))
				{
					const BWORD adoptVariables=
							(valueNode2.IsMap() &&
							valueNode2["adopt_variables"] &&
							valueNode2["adopt_variables"].as<bool>());

					String importPathname=a_filename.pathname();
					if(absolute)
					{
						if(importPathname.empty())
						{
							importPathname="/";
						}
					}
					else
					{
						if(importPathname.empty())
						{
							importPathname=a_pathname;
						}
						else if(!a_pathname.empty())
						{
							importPathname=a_pathname+"/"+importPathname;
						}
					}

					addImport(a_spCatalog,valueNode2,
							importPathname,adoptVariables,
							a_variableMapIn,a_variableMapOut);
				}
			}

			if(directive=="state")
			{
				if(checkOnly(valueNode2,a_variableMapOut))
				{
					addState(a_spCatalog,valueNode2);
				}
			}
		}
	}

	return TRUE;
}

BWORD CatalogReaderYaml::checkOnly(const YAML::Node a_yamlNode,
	std::map<String,String>& a_variableMap)
{
#if FE_CRY_DEBUG
	feLog("CatalogReaderYaml::checkOnly\n");
#endif

	if(!a_yamlNode.IsMap())
	{
#if FE_CRY_DEBUG
		feLog("NOT MAP\n");
#endif
		return TRUE;
	}

	if(!a_yamlNode["only"])
	{
#if FE_CRY_DEBUG
		feLog("NO ONLY\n");
#endif
		return TRUE;
	}

	const YAML::Node onlyNode=a_yamlNode["only"];

	if(!onlyNode["variables"])
	{
#if FE_CRY_DEBUG
		feLog("NO VARIABLES\n");
#endif
		return FALSE;
	}

	const YAML::Node variableNode=onlyNode["variables"];
	if(!variableNode.IsSequence())
	{
#if FE_CRY_DEBUG
		feLog("NOT SEQUENCE\n");
#endif
		return FALSE;
	}

	const I32 variableCount=variableNode.size();
	for(I32 variableIndex=0;variableIndex<variableCount;variableIndex++)
	{
		String text=
				variableNode[variableIndex].as<std::string>().c_str();

#if FE_CRY_DEBUG
		feLog("check (%s)\n",text.c_str());
#endif

		text=text.substituteEnv(a_variableMap);

#if FE_CRY_DEBUG
		feLog("as (%s)\n",text.c_str());
#endif

		//* NOTE this could be way more expansive and sophisticated

		const String lhs=text.parse(""," ");
		const String op=text.parse(""," ");
		const String rhs=text.parse(""," ");

		const Real lhv=(lhs=="true")? Real(1): lhs.real();
		const Real rhv=(rhs=="true")? Real(1): rhs.real();

		const String lhu=lhs.maybeUnQuote();
		const String rhu=rhs.maybeUnQuote();
		const BWORD compareStrings=
				(lhs!=lhu || rhs!=rhu || (lhv==0.0 && rhv==0.0));

#if FE_CRY_DEBUG
		feLog("(%s) %.6G (%s) (%s) %.6G compareStrings=%d\n",
				lhs.c_str(),lhv,op.c_str(),rhs.c_str(),rhv,compareStrings);
#endif

		if(op=="==")
		{
			if(compareStrings)
			{
				if(lhu.compare(rhu))
				{
					return FALSE;
				}
			}
			else if(lhv!=rhv)
			{
				return FALSE;
			}
		}
		else if(op=="!=")
		{
			if(compareStrings)
			{
				if(!lhu.compare(rhu))
				{
					return FALSE;
				}
			}
			else if(lhv==rhv)
			{
				return FALSE;
			}
		}
		else if(op=="<")
		{
			if(compareStrings)
			{
				if(lhu.compare(rhu)>=0)
				{
					return FALSE;
				}
			}
			else if(lhv>=rhv)
			{
				return FALSE;
			}
		}
		else if(op=="<=")
		{
			if(compareStrings)
			{
				if(lhu.compare(rhu)>0)
				{
					return FALSE;
				}
			}
			else if(lhv>rhv)
			{
				return FALSE;
			}
		}
		else if(op==">=")
		{
			if(compareStrings)
			{
				if(lhu.compare(rhu)<0)
				{
					return FALSE;
				}
			}
			else if(lhv<rhv)
			{
				return FALSE;
			}
		}
		else if(op==">")
		{
			if(compareStrings)
			{
				if(lhu.compare(rhu)<=0)
				{
					return FALSE;
				}
			}
			else if(lhv<=rhv)
			{
				return FALSE;
			}
		}
	}

#if FE_CRY_DEBUG
	feLog("CatalogReaderYaml::checkOnly success\n");
#endif

	return TRUE;
}

} /* namespace ext */
} /* namespace fe */
