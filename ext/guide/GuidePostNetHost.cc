/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <guide/guide.pmh>

#define FE_GPA_DEBUG			TRUE

namespace fe
{
namespace ext
{

GuidePostNetHost::GuidePostNetHost(void)
{
#if FE_GPA_DEBUG
	feLog("GuidePostNetHost::GuidePostNetHost\n");
#endif
}

GuidePostNetHost::~GuidePostNetHost(void)
{
#if FE_GPA_DEBUG
	feLog("GuidePostNetHost::~GuidePostNetHost\n");
#endif
}

BWORD GuidePostNetHost::connect(String a_configuration)
{
	beacon::BeaconConfig config;

	//* TODO more, like port
	const String ipAddress=a_configuration;

	if(a_configuration.empty())
	{
		feLog("Using Beacon address default (127.0.0.1) \n");
		strcpy(config.ipAddress,"127.0.0.1");
	}
	else
	{
		feLog("Using Beacon address \"%s\"\n", ipAddress.c_str());
		strcpy(config.ipAddress, ipAddress.c_str());
	}

	config.requestPort=5000;	// Beacon request port
	config.gdRequestPort=5001;	// Global dictionary request port

	const bool timeSync(true);
	return registerWithBeacon(config,timeSync);
}

void GuidePostNetHost::listSpaces(Array<String>& a_rSpaceList)
{
	std::list<beacon::Node> nodelist;
	getList(nodelist);

	std::vector< std::pair<String,String> > getList;

	for(beacon::Node& rNode: nodelist)
	{
		const I32 otherID=rNode.id;

		String spacesKey;
		spacesKey.sPrintf("node[%x].spaces",otherID);

		getList.push_back(std::make_pair(spacesKey,""));
	}

	const bool success=dictionaryGet(getList);

	//* TODO handle error properly
	FEASSERT(success);

	const I32 getCount=getList.size();
	for(I32 getIndex=0;getIndex<getCount;getIndex++)
	{
		String spaces=getList[getIndex].second;

		feLog("GuidePostNetHost::listSpaces node %02p spaces \"%s\"\n",
				getIndex,spaces.c_str());

		while(!spaces.empty())
		{
			const String space=spaces.parse();
			a_rSpaceList.push_back(space);
		}
	}
}

sp<StateCatalog> GuidePostNetHost::connectToSpace(String a_space)
{
#if FE_GPA_DEBUG
	feLog("GuidePostNetHost::connectToSpace \"%s\"\n",a_space.c_str());
#endif

	sp<NetHost> spNetHost(NetHost::create());
	FEASSERT(spNetHost.isValid());

	if(spNetHost->hasSpace(a_space))
	{
		return spNetHost->accessSpace(a_space);
	}

	const U8 beaconID=getID();

#if FE_GPA_DEBUG
	feLog("GuidePostNetHost::connectToSpace"
			" client %02p latency %u delta time %d\n",
			beaconID,getLatency(),getTimeOffset());
#endif

	BWORD found(FALSE);

	Array<String> spaceList;
	listSpaces(spaceList);

	const I32 spaceCount=spaceList.size();
	for(I32 spaceIndex=0;spaceIndex<spaceCount;spaceIndex++)
	{
		const String space=spaceList[spaceIndex];

		if(space==a_space)
		{
			found=TRUE;
			break;
		}
	}

	if(!found)
	{
		feLog("GuidePostNetHost::connectToSpace space \"%s\" not found\n",
				a_space.c_str());
		return sp<StateCatalog>(NULL);
	}

	std::vector< std::pair<String,String> > getList;

	const String spacePrefix("space["+a_space+"].");

#if FALSE
	const String implementationKey(spacePrefix+"implementation");
	const String addressKey(spacePrefix+"address");
	const String transportKey(spacePrefix+"transport");
	const String portKey(spacePrefix+"port");

	getList.push_back(std::make_pair(implementationKey,String("")));
	getList.push_back(std::make_pair(addressKey,String("")));
	getList.push_back(std::make_pair(transportKey,String("")));
	getList.push_back(std::make_pair(portKey,String("")));

	const bool success=dictionaryGet(getList);

	//* TODO handle error properly
	FEASSERT(success);

	const String implementation=getList[0].second;
#else
	const bool success=
			dictionaryGetRegex("space\\["+a_space+"\\]\\..*",getList);

	//* TODO handle error properly
	FEASSERT(success);

	String implementation("ConnectedCatalog");

	const I32 matchCount=getList.size();
	for(I32 matchIndex=0;matchIndex<matchCount;matchIndex++)
	{
		std::pair<String,String>& pair=getList[matchIndex];

		if(pair.first==spacePrefix+"implementation")
		{
			implementation=pair.second;
			break;
		}
	}
#endif

	sp<StateCatalog> spStateCatalog=
			spNetHost->accessSpace(a_space,implementation);

	if(spStateCatalog.isNull())
	{
		feX("GuidePostNetHost::connectToSpace", "Failed to obtain catalog "
				"for space \"%s\", implementation \"%s\"\n",
				a_space.c_str(), implementation.c_str());
	}

	const I32 pairCount=getList.size();

#if FE_GPA_DEBUG
		feLog("GuidePostNetHost::connectToSpace pairCount %d\n",pairCount);
#endif

	for(I32 pairIndex=0;pairIndex<pairCount;pairIndex++)
	{
		std::pair<String,String>& pair=getList[pairIndex];

#if FE_GPA_DEBUG
		feLog("  pair \"%s\" \"%s\"\n",
				pair.first.c_str(),pair.second.c_str());
#endif

		String value=pair.second;
		if(value.empty())
		{
			continue;
		}

		const String& rKey=pair.first;

		String buffer=rKey;
		buffer.parse("",".");
		const String subKey=buffer.parse("",".");

#if FE_GPA_DEBUG
		feLog("    subKey \"%s\"\n",subKey.c_str());
#endif

		String type("string");

		buffer=value.prechop("(");
		if(buffer!=value)
		{
			String token=buffer.parse("",")");
			if(token!=buffer)
			{
				type=token;
				value=buffer.parse("",")");
			}
		}

#if FE_GPA_DEBUG
		feLog("    type \"%s\" value \"%s\"\n",type.c_str(),value.c_str());
#endif

		if(type=="string")
		{
			spStateCatalog->catalog<String>("net:"+subKey)=value;
		}
		else if(type=="int")
		{
			spStateCatalog->catalog<I32>("net:"+subKey)=value.integer();
		}
	}

	spStateCatalog->start();
	spStateCatalog->waitForConnection();

	while(!spStateCatalog->catalogOrDefault<bool>("space:initialized",false))
	{
		feLog("GuidePostNetHost::connectToSpace waiting for initialization\n");
		std::this_thread::sleep_for(std::chrono::milliseconds(100));
	}

	return spStateCatalog;
}

sp<StateCatalog> GuidePostNetHost::createSpace(String a_space,
	sp<Catalog> a_spCatalog)
{
#if FE_GPA_DEBUG
	feLog("GuidePostNetHost::createSpace \"%s\"\n",a_space.c_str());
#endif

	sp<NetHost> spNetHost(NetHost::create());
	FEASSERT(spNetHost.isValid());

	String implementation="ConnectedCatalog";

	if(a_spCatalog.isValid())
	{
		implementation=a_spCatalog->catalogOrDefault<String>(
				"net:implementation",implementation);
	}

	sp<StateCatalog> spStateCatalog=
			spNetHost->accessSpace(a_space,implementation);

	String address="127.0.0.1";
	String transport="tcp";
	I32 port=12346;

	if(a_spCatalog.isValid())
	{
		spStateCatalog->overlayState(a_spCatalog);

		address=a_spCatalog->catalogOrDefault<String>(
				"net:address","127.0.0.1");
		transport=a_spCatalog->catalogOrDefault<String>(
				"net:transport","tcp");
		port=a_spCatalog->catalogOrDefault<I32>(
				"net:port",12345);
	}

	//* make sure catalog is set, if defaulted
	spStateCatalog->catalog<String>("net:implementation")=implementation;
	spStateCatalog->catalog<String>("net:address")=address;
	spStateCatalog->catalog<String>("net:transport")=transport;
	spStateCatalog->catalog<I32>("net:port")=port;

	String portString;
	portString.sPrintf("%d",port);

	Result result=spStateCatalog->start();
	if(failure(result))
	{
		feLog("GuidePostNetHost::createSpace"
				" failed to start ConnectedCatalog\n");
		return sp<StateCatalog>(NULL);
	}

	std::vector< std::pair<String,String> > setList;

	String spacePrefix("space["+a_space+"].");
	setList.push_back(std::make_pair(
			spacePrefix+"implementation",
			implementation));
	setList.push_back(std::make_pair(
			spacePrefix+"address",
			address));
	setList.push_back(std::make_pair(
			spacePrefix+"transport",
			transport));
	setList.push_back(std::make_pair(
			spacePrefix+"port",
			"(int)"+portString));

	const U8 beaconID=getID();

	String spacesKey;
	spacesKey.sPrintf("node[%x].spaces",beaconID);
	String spaces=a_space;
	setList.push_back(std::make_pair(spacesKey,spaces));

	dictionarySet(setList);

	spStateCatalog->setState<bool>("space:initialized",true);
	spStateCatalog->flush();

	return spStateCatalog;
}

} /* namespace ext */
} /* namespace fe */
