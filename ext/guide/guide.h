/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __guide_guide_h__
#define __guide_guide_h__

#include "networkhost/networkhost.h"
#include "beacon/beacon.h"

#include "guide/GuidePostI.h"

#endif /* __guide_guide_h__ */
