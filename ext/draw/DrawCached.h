/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __draw_DrawCached_h__
#define __draw_DrawCached_h__

#define DCH_MAX_VERTS	512

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Delayed rendering to another DrawI, presumably an immediate one

	@ingroup draw

	Opportunites exist to use the cache for sorting, optimization,
	and off-threading.
*//***************************************************************************/
class FE_DL_EXPORT DrawCached:
	public DrawCommon,
	public Initialize<DrawCached>
{
	private:

	class Buffer
	{
		public:
			SpatialVector	m_pVertex[DCH_MAX_VERTS];
			SpatialVector	m_pNormal[DCH_MAX_VERTS];
			Color			m_pColor[DCH_MAX_VERTS];
			Real			m_pRadius[DCH_MAX_VERTS];
			Vector2			m_pTexture[DCH_MAX_VERTS];
			Vector3i		m_pElement[DCH_MAX_VERTS];
#if FE_DOUBLE_REAL
			U32				m_pFiller[DCH_MAX_VERTS];
#endif
	};

	//* byte-wise same size as m_pTexture (for blind cast)
	class CylinderOverlay
	{
		public:
			Real			m_pBaseScale[DCH_MAX_VERTS];
			U32				m_pSlices[DCH_MAX_VERTS];
	};

	class FE_DL_EXPORT Command
	{
		public:

			enum	Instruction
					{
						e_null,
						e_points,
						e_lines,
						e_triangles,
						e_rectangles,
						e_sphere,
						e_cylinder,
						e_cylinders,
						e_raster,
						e_drawable,
						e_text,
						e_max
					};

							Command(void):
								m_scissoring(FALSE),
								m_projection(ViewI::e_current),
								m_instruction(e_null),
								m_stripMode(DrawI::e_discrete),
								m_multicolor(FALSE),
								m_multiradius(FALSE),
								m_vertices(0),
								m_elementCount(0),
								m_pVertex(NULL),
								m_pNormal(NULL),
								m_pColor(NULL),
								m_pRadius(NULL),
								m_pTexture(NULL),
								m_pElement(NULL)
							{
								m_pVertex=m_localBuffer.m_pVertex;
								m_pNormal=m_localBuffer.m_pNormal;
								m_pColor=m_localBuffer.m_pColor;
								m_pTexture=m_localBuffer.m_pTexture;
								m_pElement=m_localBuffer.m_pElement;
							}

							Command(const Command& rCommand)
							{	operator=(rCommand); }

			Command&		operator=(const Command& rCommand);

			I32				layer(void) const
							{	return m_spDrawMode.isValid()?
										m_spDrawMode->layer(): -1; }

			BWORD					m_scissoring;
			Box2i					m_scissor;
			Box2i					m_viewport;
			sp<CameraEditable>		m_spCameraEditable;
			sp<DrawMode>			m_spDrawMode;
			sp<DrawBufferI>			m_spDrawBuffer;
			sp<ImageI>				m_spImageI;
			cp<DrawableI>			m_cpDrawableI;
			ViewI::Projection		m_projection;
			Instruction				m_instruction;
			StripMode				m_stripMode;
			BWORD					m_multicolor;
			BWORD					m_multiradius;
			U32						m_vertices;
			U32						m_elementCount;

			SpatialVector*			m_pVertex;
			SpatialVector*			m_pNormal;
			Color*					m_pColor;
			Real*					m_pRadius;
			Vector2*				m_pTexture;
			Vector3i*				m_pElement;
	const	Array<I32>*				m_pVertexMap;
	const	Array<I32>*				m_pHullPointMap;
	const	Array<Vector4i>*		m_pHullFacePoint;

									//* TEMP should use variable size pool
			Buffer					m_localBuffer;
	};

	class CommandQueue: public Array<Command>
	{
		public:
						CommandQueue(void):
							m_commands(0)
						{
							reserve(256);
						}

			Command&	access(U32 index)
						{
							if(index>=size())
							{
								resize(index+1);
							}
							return Array<Command>::operator[](index);
						}

			U32			m_commands;
	};

	public:
					DrawCached();
virtual				~DrawCached();

		void		initialize(void);

virtual	void		flush(void);
virtual	void		flushLive(void);
virtual	void		clearInput(void);
virtual	BWORD		empty(void) const;

virtual	BWORD		isDirect(void) const
					{	return m_spDrawChain.isValid() &&
								m_spDrawChain->isDirect(); }

virtual	sp<FontI>	font(void)
					{	if(!prepareChain()) sp<FontI>(NULL);
						return m_spDrawChain->font(); }

//~virtual	void		setFontBase(U32 index)
//~					{	if(!prepareChain()) return;
//~						m_spDrawChain->setFontBase(index); }
//~virtual	I32			fontHeight(I32* pAscent,I32* pDescent)
//~					{	if(!prepareChain()) return 1;
//~						return m_spDrawChain->fontHeight(pAscent,pDescent); }
//~virtual	I32			pixelWidth(String string)
//~					{	if(!prepareChain()) return 1;
//~						return m_spDrawChain->pixelWidth(string); }

virtual	Real		multiplication(void)
					{	if(!prepareChain()) return Real(1);
						return m_spDrawChain->multiplication(); }

					using DrawCommon::drawPoints;

virtual void		drawPoints(const SpatialVector *vertex,
							const SpatialVector *normal,U32 vertices,
							BWORD multicolor,const Color *color,
							sp<DrawBufferI> spDrawBuffer);

					using DrawCommon::drawLines;

virtual void		drawLines(const SpatialVector *vertex,
							const SpatialVector *normal,U32 vertices,
							StripMode strip,
							BWORD multicolor,const Color *color,
							BWORD multiradius,const Real *radius,
							const Vector3i *element,U32 elementCount,
							sp<DrawBufferI> spDrawBuffer);

					using DrawCommon::drawTriangles;

virtual	void		drawTriangles(const SpatialVector *vertex,
							const SpatialVector *normal,
							const Vector2 *texture,U32 vertices,
							StripMode strip,BWORD multicolor,
							const Color *color,
							const Array<I32>* vertexMap,
							const Array<I32>* hullPointMap,
							const Array<Vector4i>* hullFacePoint,
							sp<DrawBufferI> spDrawBuffer);

virtual void		drawRectangles(const SpatialVector *vertex,U32 vertices,
						BWORD multicolor,const Color *color);

virtual void		drawSphere(const SpatialTransform &transform,
							const SpatialVector *scale,const Color &color);

					using DrawCommon::drawCylinder;

virtual	void		drawCylinder(const SpatialTransform &transform,
							const SpatialVector *scale,
							Real baseScale,const Color &color,U32 slices);
virtual void		drawCylinders(const SpatialTransform* transform,
							const SpatialVector* scale,const Real *baseScale,
							const U32* slices,U32 cylinders,
							BWORD multicolor,const Color* color);

					using DrawCommon::draw;

virtual	void		draw(cp<DrawableI> cpDrawableI,const Color* color,
							sp<DrawBufferI> spDrawBuffer);

					using DrawCommon::drawTransformed;

virtual	void		drawTransformed(const SpatialTransform &transform,
							cp<DrawableI> cpDrawableI,const Color* color,
							sp<DrawBufferI> spDrawBuffer);

virtual	void		drawRaster(sp<ImageI> spImageI,
							const SpatialVector& location);

virtual	sp<DrawBufferI>	createBuffer(void)
					{	return m_spDrawChain.isValid()?
								m_spDrawChain->createBuffer():
								sp<DrawBufferI>(NULL); }

virtual	void		drawAlignedText(const SpatialVector &location,
							const String text,const Color &color);

virtual	void		setDrawChain(sp<DrawI> a_spDrawI)
					{	m_spDrawChain=a_spDrawI; }
virtual	sp<DrawI>	drawChain(void)
					{	return m_spDrawChain; }

	protected:

		BWORD			prepareChain(void);
		void			drawOutput(U32 queue,BWORD a_clearQueue);
		void			clearCameraMap(void);

		sp<DrawI>		m_spDrawChain;
		U32				m_inputQueue;
		U32				m_outputQueue;
		U32				m_queues;

	private:

		void			drawGeometry(Command::Instruction instruction,
							const SpatialVector *vertex,
							const SpatialVector *normal,
							const Vector2 *texture,
							const Real* auxReal,const U32* auxU32,
							U32 vertices,StripMode strip,
							BWORD multicolor,const Color *color,
							BWORD multiradius,const Real *radius,
							const Array<I32>* vertexMap,
							const Array<I32>* hullPointMap,
							const Array<Vector4i>* hullFacePoint,
							const Vector3i *element,U32 elementCount,
							sp<DrawBufferI> spDrawBuffer);

		void			adoptView(DrawCached::Command& rCommand,
								sp<ViewI> spViewI);

		U32				m_grain[Command::e_max];
		U32				m_oversize[Command::e_max];

		CommandQueue*	m_pCommandQueue;

		std::map< sp<CameraEditable>, sp<CameraEditable> >	m_cameraMap;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __draw_DrawCached_h__ */
