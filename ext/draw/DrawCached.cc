/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <draw/draw.pmh>

#define	FE_DCH_CACHE_DEBUG	FALSE

namespace fe
{
namespace ext
{

DrawCached::DrawCached(void):
	m_inputQueue(0),
	m_outputQueue(0),
	m_queues(2)
{
	m_pCommandQueue=new CommandQueue[m_queues];

	m_grain[Command::e_null]=1;
	m_grain[Command::e_points]=1;
	m_grain[Command::e_lines]=2;
	m_grain[Command::e_triangles]=3;
	m_grain[Command::e_rectangles]=4;
	m_grain[Command::e_sphere]=1;
	m_grain[Command::e_cylinder]=1;
	m_grain[Command::e_cylinders]=1;
	m_grain[Command::e_raster]=1;
	m_grain[Command::e_text]=1;

	m_oversize[Command::e_null]=1;
	m_oversize[Command::e_points]=1;
	m_oversize[Command::e_lines]=1;
	m_oversize[Command::e_triangles]=1;
	m_oversize[Command::e_rectangles]=1;
	m_oversize[Command::e_sphere]=1;
	m_oversize[Command::e_cylinder]=1;
#if FE_DOUBLE_REAL
	m_oversize[Command::e_cylinders]=4;
#else
	m_oversize[Command::e_cylinders]=
			sizeof(SpatialTransform)/sizeof(SpatialVector);
#endif
	m_oversize[Command::e_raster]=1;
	m_oversize[Command::e_text]=1;
}

DrawCached::~DrawCached(void)
{
	delete[] m_pCommandQueue;
}

void DrawCached::initialize(void)
{
}

void DrawCached::flush(void)
{
	clearCameraMap();

	m_outputQueue=m_inputQueue;
	m_inputQueue=(m_inputQueue+1)%m_queues;

	drawOutput(m_outputQueue,TRUE);
}

void DrawCached::clearInput(void)
{
	CommandQueue& rCommandQueue=m_pCommandQueue[m_inputQueue];
	U32& rCommands=rCommandQueue.m_commands;
	rCommands=0;

	clearCameraMap();
}

BWORD DrawCached::empty(void) const
{
	CommandQueue& rCommandQueue=m_pCommandQueue[m_inputQueue];
	return !rCommandQueue.m_commands;
}

void DrawCached::flushLive(void)
{
	drawOutput(m_inputQueue,FALSE);
}

BWORD DrawCached::prepareChain(void)
{
	if(!m_spDrawChain.isValid())
	{
		const String hintChain=
				registry()->master()->catalog()->catalogOrDefault<String>(
				"hint:DrawChain","");
		if(!hintChain.empty())
		{
			m_spDrawChain=registry()->create(hintChain);
		}

		if(!m_spDrawChain.isValid())
		{
			registry()->manifest()->catalog<I32>(
					"DrawI.DrawOpenGL.fe","priority")=1;
			m_spDrawChain=registry()->create("DrawI");
		}
		if(!m_spDrawChain.isValid())
		{
			feLog("DrawCached::prepareChain"
					" failed to create immediate DrawI\n");
			return FALSE;
		}
	}

	m_spDrawChain->view()->setWindow(view()->window());
	return TRUE;
}

void DrawCached::drawOutput(U32 queue,BWORD a_clearQueue)
{
	FEASSERT(queue<m_queues);

	if(!prepareChain())
	{
		return;
	}

	//* NOTE clear and swap
	m_spDrawChain->drawMode()->copy(drawMode());
	m_spDrawChain->flush();

	if(m_spDrawChain->brightness() != brightness())
	{
		m_spDrawChain->setBrightness(brightness());

		//* write back in case it was corrected
		setBrightness(m_spDrawChain->brightness());
	}

	if(m_spDrawChain->contrast() != contrast())
	{
		m_spDrawChain->setContrast(contrast());

		//* write back in case it was corrected
		setContrast(m_spDrawChain->contrast());
	}

	CommandQueue& rCommandQueue=m_pCommandQueue[queue];
	U32& rCommands=rCommandQueue.m_commands;

#if FE_DCH_CACHE_DEBUG
	feLog("DrawCached::drawOutput \"%s\" queue %d commands %d\n",
			name().c_str(),queue,rCommands);
#endif

	if(!rCommands)
	{
		return;
	}

	I32 currentLayer=INT_MAX;
	for(U32 command=0;command<rCommands;command++)
	{
		const Command& rCommand=rCommandQueue[command];
		const I32 layer=rCommand.layer();
		if(currentLayer>layer)
		{
			currentLayer=layer;
		}
	}

	I32 nextLayer=currentLayer;
	U32 completed=0;
	while(completed<rCommands)
	{
		for(U32 command=0;command<rCommands;command++)
		{
			const Command& rCommand=rCommandQueue[command];
			const I32 layer=rCommand.layer();

			if(layer>currentLayer &&
					(nextLayer==currentLayer || nextLayer>layer))
			{
				nextLayer=layer;
			}

			if(layer!=currentLayer)
			{
				continue;
			}

#if FE_DCH_CACHE_DEBUG
			feLog("[36mDrawCached::drawOutput queue %d command %d layer %d\n"
					" vertices %d strip %d multicolor %d multiradius %d\n"
					" vertex %p normal %p color %p%s radius %p\n"
					" texture %p image %p drawable %p[0m\n",
					queue,command,
					rCommand.layer(),
					rCommand.m_vertices,
					rCommand.m_stripMode,
					rCommand.m_multicolor,
					rCommand.m_multiradius,
					rCommand.m_pVertex,
					rCommand.m_pNormal,
					rCommand.m_pColor,
					(rCommand.m_pColor && !rCommand.m_multicolor)?
					(" "+print(*rCommand.m_pColor)).c_str(): "",
					rCommand.m_pRadius,
					rCommand.m_pTexture,
					rCommand.m_spImageI.raw(),
					rCommand.m_cpDrawableI.raw());

/*
			for(U32 m=0;m<rCommand.m_vertices;m++)
			{
				feLog("vert %d/%d %s norm %s\n",m,rCommand.m_vertices,
						rCommand.m_pVertex?
						c_print(rCommand.m_pVertex[m]): "NULL",
						rCommand.m_pNormal?
						c_print(rCommand.m_pNormal[m]): "NULL");
			}
*/
#endif
			sp<ViewI> spView=m_spDrawChain->view();

			if(rCommand.m_scissoring)
			{
				spView->setScissor(&rCommand.m_scissor);
			}

			spView->setViewport(rCommand.m_viewport);

			const sp<CameraI> spViewCamera=spView->camera();
			const sp<CameraEditable>& rspCameraEditable=
					rCommand.m_spCameraEditable;

			if(rspCameraEditable.isNull())
			{
				feLog("DrawCached::drawOutput CameraEditable NULL\n");
			}

			if(!completed || spViewCamera!=rspCameraEditable)
			{
#if FE_DCH_CACHE_DEBUG
				feLog("DrawCached::drawOutput changing camera\n");
#endif

				spView->setCamera(rspCameraEditable);
				spView->use(rCommand.m_projection);
			}
			else if(spView->projection()!=rCommand.m_projection)
			{
#if FE_DCH_CACHE_DEBUG
				feLog("DrawCached::drawOutput just changing projection\n");
#endif

				spView->use(rCommand.m_projection);
			}

			//* TODO secondary sort on DrawMode (minimize context switching)

			const sp<DrawMode>& rspDrawMode=rCommand.m_spDrawMode;
			const BWORD changeDrawMode=
					(m_spDrawChain->drawMode()!=rspDrawMode);
			if(changeDrawMode)
			{
#if FE_DCH_CACHE_DEBUG
				feLog("DrawCached::drawOutput changing DrawMode\n");
#endif

				m_spDrawChain->pushDrawMode(rspDrawMode);
			}

#if FE_DCH_CACHE_DEBUG
			feLog("DrawCached::drawOutput Camera:\n  %s\n",
					print(rspCameraEditable).c_str());
			feLog(" %.6G %.6G %.6G %.6G\n",
					spView->viewport()[0],
					spView->viewport()[1],
					spView->viewport().size()[0],
					spView->viewport().size()[1]);
			feLog("DrawCached::drawOutput DrawMode:\n  %s\n",
					print(rspDrawMode).c_str());
#endif

			switch(rCommand.m_instruction)
			{
				case Command::e_points:
				{
#if FE_DCH_CACHE_DEBUG
					feLog("  points\n");
#endif
					m_spDrawChain->drawPoints(rCommand.m_pVertex,
							rCommand.m_pNormal,
							rCommand.m_vertices,
							rCommand.m_multicolor,
							rCommand.m_pColor,

							//* TODO transient buffers
//							rCommand.m_spDrawBuffer);
							sp<DrawBufferI>(NULL));
				}
				break;

				case Command::e_lines:
				{
#if FE_DCH_CACHE_DEBUG
					feLog("  lines\n");
#endif
					m_spDrawChain->drawLines(rCommand.m_pVertex,
							rCommand.m_pNormal,
							rCommand.m_vertices,
							rCommand.m_stripMode,
							rCommand.m_multicolor,
							rCommand.m_pColor,
							rCommand.m_multiradius,
							rCommand.m_pRadius,
							rCommand.m_pElement,
							rCommand.m_elementCount,

							//* TODO transient buffers
//							rCommand.m_spDrawBuffer);
							sp<DrawBufferI>(NULL));
				}
				break;

				case Command::e_triangles:
				{
#if FE_DCH_CACHE_DEBUG
					feLog("  triangles\n");
#endif
					m_spDrawChain->drawTriangles(rCommand.m_pVertex,
							rCommand.m_pNormal,
							rCommand.m_pTexture,
							rCommand.m_vertices,
							rCommand.m_stripMode,
							rCommand.m_multicolor,
							rCommand.m_pColor,
							rCommand.m_pVertexMap,
							rCommand.m_pHullPointMap,
							rCommand.m_pHullFacePoint,

							//* TODO transient buffers
//							rCommand.m_spDrawBuffer);
							sp<DrawBufferI>(NULL));
				}
				break;

				case Command::e_rectangles:
				{
#if FE_DCH_CACHE_DEBUG
					feLog("  rectangles\n");
#endif
					m_spDrawChain->drawRectangles(rCommand.m_pVertex,
							rCommand.m_vertices,
							rCommand.m_multicolor,
							rCommand.m_pColor);
				}
				break;

				case Command::e_sphere:
				{
#if FE_DCH_CACHE_DEBUG
					feLog("  sphere\n");
#endif
					SpatialTransform transform;
					set(transform,&rCommand.m_pVertex[0][0]);

					m_spDrawChain->drawSphere(transform,
							&rCommand.m_pVertex[6],
							*rCommand.m_pColor);
				}
				break;

				case Command::e_cylinder:
				{
#if FE_DCH_CACHE_DEBUG
					feLog("  cylinder\n");
#endif
					SpatialTransform transform;
					set(transform,&rCommand.m_pVertex[0][0]);

					m_spDrawChain->drawCylinder(transform,
							&rCommand.m_pVertex[6],rCommand.m_pVertex[7][0],
							*rCommand.m_pColor,U32(rCommand.m_pVertex[7][1]));
				}
				break;

				case Command::e_cylinders:
				{
#if FE_DCH_CACHE_DEBUG
					feLog("  cylinders\n");
#endif
					CylinderOverlay* pCylinderOverlay=
							(CylinderOverlay*)rCommand.m_pTexture;

					m_spDrawChain->drawCylinders(
							(SpatialTransform*)rCommand.m_pVertex,
							rCommand.m_pNormal,
							pCylinderOverlay->m_pBaseScale,
							pCylinderOverlay->m_pSlices,
							rCommand.m_vertices,
							rCommand.m_multicolor,
							rCommand.m_pColor);
				}
				break;

				case Command::e_raster:
				{
#if FE_DCH_CACHE_DEBUG
					feLog("  raster\n");
#endif
					m_spDrawChain->drawRaster(rCommand.m_spImageI,
							rCommand.m_pVertex[0]);
				}
				break;

				case Command::e_drawable:
				{
#if FE_DCH_CACHE_DEBUG
					feLog("  drawable\n");
#endif
					SpatialTransform transform;
					set(transform,&rCommand.m_pVertex[0][0]);
					m_spDrawChain->drawTransformed(
							transform,
							rCommand.m_cpDrawableI,
							rCommand.m_pColor,
							rCommand.m_spDrawBuffer);
				}
				break;

				case Command::e_text:
				{
					char* buffer=reinterpret_cast<char*>(rCommand.m_pTexture);

#if FE_DCH_CACHE_DEBUG
					feLog("  text \"%s\"\n",buffer);
#endif
					m_spDrawChain->drawAlignedText(rCommand.m_pVertex[0],
							buffer,rCommand.m_pColor[0]);
				}
				break;

				default:
				break;
			}

			if(changeDrawMode)
			{
				m_spDrawChain->popDrawMode();
			}

			if(rCommand.m_scissoring)
			{
				m_spDrawChain->view()->setScissor(NULL);
			}

			completed++;
		}

		if(completed<rCommands && nextLayer<=currentLayer)
		{
			feX("DrawCached::drawOutput",
					"layer processing failed, completed %d/%d layer %d->%d",
					completed,rCommands,currentLayer,nextLayer);
		}
		currentLayer=nextLayer;
	}

	if(a_clearQueue)
	{
		for(U32 command=0;command<rCommands;command++)
		{
			Command& rCommand=rCommandQueue[command];
			rCommand.m_spCameraEditable=NULL;
			rCommand.m_spDrawMode=NULL;
			rCommand.m_spDrawBuffer=NULL;
			rCommand.m_spImageI=NULL;
			rCommand.m_cpDrawableI=NULL;
		}

		rCommands=0;
	}

#if FE_DCH_CACHE_DEBUG
		feLog("DrawCached::drawOutput queue %d done\n",queue);
#endif
}

void DrawCached::drawPoints(const SpatialVector *vertex,
		const SpatialVector *normal,U32 vertices,
		BWORD multicolor,const Color *color,
		sp<DrawBufferI> spDrawBuffer)
{
#if FE_DCH_CACHE_DEBUG
	feLog("DrawCached::drawPoints\n");
#endif
	drawGeometry(Command::e_points,
			vertex,normal,NULL,NULL,NULL,vertices,e_discrete,multicolor,color,
			FALSE,NULL,NULL,NULL,NULL,NULL,0,spDrawBuffer);
}

void DrawCached::drawLines(const SpatialVector *vertex,
		const SpatialVector *normal,U32 vertices,
		StripMode strip,BWORD multicolor,const Color *color,
		BWORD multiradius,const Real *radius,
		const Vector3i *element,U32 elementCount,
		sp<DrawBufferI> spDrawBuffer)
{
#if FE_DCH_CACHE_DEBUG
	feLog("DrawCached::drawLines\n");
#endif
	//* TODO pass along element and elementCount, if set
	drawGeometry(Command::e_lines,
			vertex,normal,NULL,NULL,NULL,vertices,strip,multicolor,color,
			multiradius,radius,NULL,NULL,NULL,
			element,elementCount,spDrawBuffer);
}

void DrawCached::drawTriangles(const SpatialVector *vertex,
	const SpatialVector *normal,const Vector2 *texture,U32 vertices,
	StripMode strip,BWORD multicolor,const Color *color,
	const Array<I32>* vertexMap,
	const Array<I32>* hullPointMap,const Array<Vector4i>* hullFacePoint,
	sp<DrawBufferI> spDrawBuffer)
{
#if FE_DCH_CACHE_DEBUG
	feLog("DrawCached::drawTriangles\n");
#endif
	drawGeometry(Command::e_triangles,
			vertex,normal,texture,NULL,NULL,vertices,strip,multicolor,color,
			FALSE,NULL,vertexMap,hullPointMap,hullFacePoint,
			NULL,0,spDrawBuffer);
}

void DrawCached::drawRectangles(const SpatialVector *vertex,U32 vertices,
		BWORD multicolor,const Color *color)
{
#if FE_DCH_CACHE_DEBUG
	feLog("DrawCached::drawRectangles\n");
#endif
	drawGeometry(Command::e_rectangles,
			vertex,NULL,NULL,NULL,NULL,vertices,e_discrete,multicolor,color,
			FALSE,NULL,NULL,NULL,NULL,NULL,0,sp<Component>(NULL));
}

void DrawCached::drawSphere(const SpatialTransform &transform,
		const SpatialVector *scale,const Color &color)
{
#if FE_DCH_CACHE_DEBUG
	feLog("DrawCached::drawSphere\n");
#endif

	FEASSERT(m_inputQueue<m_queues);

	CommandQueue& rCommandQueue=m_pCommandQueue[m_inputQueue];
	U32& rCommands=rCommandQueue.m_commands;
	Command& rCommand=rCommandQueue.access(rCommands++);

	rCommand.m_spDrawMode=drawMode();

#if FE_DCH_CACHE_DEBUG
	feLog("[35mDrawCached::drawSphere queue %d command %d layer %d[0m\n",
			m_inputQueue,rCommands-1,rCommand.layer());
#endif

	adoptView(rCommand,view());
	rCommand.m_projection=view()->projection();
	rCommand.m_instruction=Command::e_sphere;
	rCommand.m_stripMode=e_discrete;
	rCommand.m_multicolor=FALSE;
	rCommand.m_multiradius=FALSE;
	rCommand.m_vertices=1;
	rCommand.m_spImageI=NULL;

	rCommand.m_pVertex=rCommand.m_localBuffer.m_pVertex;
	transform.copy16(&rCommand.m_pVertex[0][0]);

	if(scale)
	{
		rCommand.m_pVertex[6]= *scale;
	}
	else
	{
		set(rCommand.m_pVertex[6],1.0f,1.0f,1.0f);
	}

	rCommand.m_pNormal=NULL;

	rCommand.m_pColor=rCommand.m_localBuffer.m_pColor;
	rCommand.m_pColor[0]=color;

	rCommand.m_pRadius=NULL;

	rCommand.m_pTexture=NULL;
}

void DrawCached::drawCylinder(const SpatialTransform &transform,
		const SpatialVector *scale,Real baseScale,const Color &color,
		U32 slices)
{
#if FE_DCH_CACHE_DEBUG
	feLog("DrawCached::drawCylinder\n");
#endif

	FEASSERT(m_inputQueue<m_queues);

	CommandQueue& rCommandQueue=m_pCommandQueue[m_inputQueue];
	U32& rCommands=rCommandQueue.m_commands;
	Command& rCommand=rCommandQueue.access(rCommands++);

	rCommand.m_spDrawMode=drawMode();

#if FE_DCH_CACHE_DEBUG
	feLog("[35mDrawCached::drawCylinder queue %d command %d layer %d[0m\n",
			m_inputQueue,rCommands-1,rCommand.layer());
#endif

	adoptView(rCommand,view());
	rCommand.m_projection=view()->projection();
	rCommand.m_instruction=Command::e_cylinder;
	rCommand.m_stripMode=e_discrete;
	rCommand.m_multicolor=FALSE;
	rCommand.m_multiradius=FALSE;
	rCommand.m_vertices=1;
	rCommand.m_spImageI=NULL;

	rCommand.m_pVertex=rCommand.m_localBuffer.m_pVertex;
	transform.copy16(&rCommand.m_pVertex[0][0]);

	if(scale)
	{
		rCommand.m_pVertex[6]= *scale;
	}
	else
	{
		set(rCommand.m_pVertex[6],1.0f,1.0f,1.0f);
	}
	rCommand.m_pVertex[7][0]=baseScale;
	rCommand.m_pVertex[7][1]=slices;

	rCommand.m_pNormal=NULL;

	rCommand.m_pColor=rCommand.m_localBuffer.m_pColor;
	rCommand.m_pColor[0]=color;

	rCommand.m_pRadius=NULL;

	rCommand.m_pTexture=NULL;
}

void DrawCached::drawCylinders(const SpatialTransform* transform,
		const SpatialVector* scale,const Real *baseScale,const U32* slices,
		U32 cylinders,BWORD multicolor,const Color* color)
{
#if FE_DCH_CACHE_DEBUG
	feLog("DrawCached::drawCylinders\n");
#endif

	// store scale in normal
	drawGeometry(Command::e_cylinders,(SpatialVector*)transform,scale,NULL,
			baseScale,slices,cylinders,e_discrete,multicolor,color,
			FALSE,NULL,NULL,NULL,NULL,NULL,0,sp<Component>(NULL));
}

void DrawCached::drawRaster(sp<ImageI> spImageI,const SpatialVector& location)
{
#if FE_DCH_CACHE_DEBUG
	feLog("DrawCached::drawRaster\n");
#endif

	FEASSERT(m_inputQueue<m_queues);

	CommandQueue& rCommandQueue=m_pCommandQueue[m_inputQueue];
	U32& rCommands=rCommandQueue.m_commands;
	Command& rCommand=rCommandQueue.access(rCommands++);

	rCommand.m_spDrawMode=drawMode();

#if FE_DCH_CACHE_DEBUG
	feLog("[35mDrawCached::drawRaster queue %d command %d layer %d[0m\n",
			m_inputQueue,rCommands-1,rCommand.layer());
#endif

	adoptView(rCommand,view());
	rCommand.m_projection=view()->projection();
	rCommand.m_instruction=Command::e_raster;
	rCommand.m_stripMode=e_discrete;
	rCommand.m_multicolor=FALSE;
	rCommand.m_multiradius=FALSE;
	rCommand.m_vertices=1;
	rCommand.m_spImageI=spImageI;

	rCommand.m_pVertex=rCommand.m_localBuffer.m_pVertex;
	rCommand.m_pVertex[0]=location;

	rCommand.m_pNormal=NULL;

	rCommand.m_pColor=NULL;

	rCommand.m_pRadius=NULL;

	rCommand.m_pTexture=NULL;
}

void DrawCached::draw(cp<DrawableI> cpDrawableI,const Color* color,
	sp<DrawBufferI> spDrawBuffer)
{
	drawTransformed(SpatialTransform::identity(),cpDrawableI,color,
			spDrawBuffer);
}

void DrawCached::drawTransformed(const SpatialTransform& transform,
	cp<DrawableI> cpDrawableI,const Color* color,sp<DrawBufferI> spDrawBuffer)
{
	if(cpDrawableI.isNull())
	{
		return;
	}

#if FE_DCH_CACHE_DEBUG
	feLog("DrawCached::drawTransformed\n");
#endif

	FEASSERT(m_inputQueue<m_queues);

	CommandQueue& rCommandQueue=m_pCommandQueue[m_inputQueue];
	U32& rCommands=rCommandQueue.m_commands;
	Command& rCommand=rCommandQueue.access(rCommands++);

#if FE_DCH_CACHE_DEBUG
	feLog("[35mDrawCached::draw queue %d command %d layer %d[0m\n",
			m_inputQueue,rCommands-1,rCommand.layer());
	feLog("transform\n%s\n",c_print(transform));
#endif

	adoptView(rCommand,view());
	rCommand.m_spDrawMode=drawMode();
	rCommand.m_spDrawBuffer=spDrawBuffer;
	rCommand.m_projection=view()->projection();
	rCommand.m_instruction=Command::e_drawable;
	rCommand.m_stripMode=e_discrete;
	rCommand.m_multicolor=FALSE;
	rCommand.m_multiradius=FALSE;
	rCommand.m_vertices=1;
	rCommand.m_cpDrawableI=cpDrawableI;
	rCommand.m_cpDrawableI.protect();

	rCommand.m_pVertex=rCommand.m_localBuffer.m_pVertex;
	transform.copy16(&rCommand.m_pVertex[0][0]);

	rCommand.m_pNormal=NULL;

	if(color)
	{
		rCommand.m_pColor=rCommand.m_localBuffer.m_pColor;
		rCommand.m_pColor[0]= *color;
	}
	else
	{
		rCommand.m_pColor=NULL;
	}

	rCommand.m_pRadius=NULL;

	rCommand.m_pTexture=NULL;
}

void DrawCached::drawAlignedText(const SpatialVector &location,
		const String text,const Color &color)
{
	FEASSERT(m_inputQueue<m_queues);

	CommandQueue& rCommandQueue=m_pCommandQueue[m_inputQueue];
	U32& rCommands=rCommandQueue.m_commands;
	Command& rCommand=rCommandQueue.access(rCommands++);

	rCommand.m_spDrawMode=drawMode();

#if FE_DCH_CACHE_DEBUG
	feLog("DrawCached::drawAlignedText queue %d command %d layer %d"
			" string \"%s\"\n",
			m_inputQueue,rCommands-1,rCommand.layer(),text.c_str());
#endif

	adoptView(rCommand,view());
	rCommand.m_projection=view()->projection();
	rCommand.m_instruction=Command::e_text;
	rCommand.m_stripMode=e_discrete;
	rCommand.m_multicolor=FALSE;
	rCommand.m_multiradius=FALSE;
	rCommand.m_vertices=1;
	rCommand.m_spImageI=NULL;

	rCommand.m_pVertex=rCommand.m_localBuffer.m_pVertex;
	rCommand.m_pVertex[0]=location;

	rCommand.m_pNormal=NULL;

	rCommand.m_pColor=rCommand.m_localBuffer.m_pColor;
	rCommand.m_pColor[0]=color;

	rCommand.m_pRadius=NULL;

	rCommand.m_pTexture=rCommand.m_localBuffer.m_pTexture;
	char* buffer=reinterpret_cast<char*>(rCommand.m_pTexture);
	const FE_UWORD length=fe::minimum(text.length()+1,
			U32(DCH_MAX_VERTS*sizeof(Vector2)));
	strncpy(buffer,text.c_str(),length);
	buffer[length-1]=0;
}

void DrawCached::drawGeometry(Command::Instruction instruction,
		const SpatialVector *vertex,const SpatialVector *normal,
		const Vector2 *texture,const Real* auxReal,const U32* auxU32,
		U32 vertices,StripMode strip,
		BWORD multicolor,const Color *color,
		BWORD multiradius,const Real *radius,
		const Array<I32>* vertexMap,
		const Array<I32>* hullPointMap,const Array<Vector4i>* hullFacePoint,
		const Vector3i *element,U32 elementCount,
		sp<DrawBufferI> spDrawBuffer)
{
	FEASSERT(m_inputQueue<m_queues);

	CommandQueue& rCommandQueue=m_pCommandQueue[m_inputQueue];
	U32& rCommands=rCommandQueue.m_commands;

	const U32 grain=m_grain[instruction];
	const U32 oversize=m_oversize[instruction];
	const U32 limit=(DCH_MAX_VERTS/(grain*oversize))*grain;
	const U32 groups=(vertices>=limit && strip==DrawI::e_discrete)?
			(vertices-1)/limit+1: 1;

	//* TODO split elements
	if(groups>1 && element)
	{
		feLog("DrawCached::drawGeometry splitting elements not implemented\n");
		element=NULL;
		elementCount=0;
	}

	// TODO what if not discrete and too many verts

#if FE_DCH_CACHE_DEBUG
	feLog("[35mDrawCached::drawGeometry queue %d command %d layer %d"
			" vertices %d strip %d multicolor %d multiradius %d\n"
			" grain %d limit %d groups %d\n"
			" vertex %p normal %p color %p radius %p texture %p[0m\n",
			m_inputQueue,rCommands,drawMode()->layer(),
			vertices,strip,multicolor,multiradius,
			grain, limit, groups,
			vertex,normal,color,radius,texture);
#endif

	for(U32 group=0;group<groups;group++)
	{
		Command& rCommand=rCommandQueue.access(rCommands++);

		U32 offset=limit*group;
		U32 subvertices=(group==groups-1)? vertices-offset: limit;

#if FE_DCH_CACHE_DEBUG
		feLog("  command %d group %d offset %d subvertices %d ptr=%p %p\n",
				rCommands-1,group,offset,subvertices,
				vertex+offset,color+offset);
#endif

		//* TODO continue strip in another group
		if(subvertices>limit)
		{
			feLog("DrawCached::drawGeometry clipping long strip %d to %d\n",
					subvertices,limit);

			subvertices=limit;
		}

		adoptView(rCommand,view());
		rCommand.m_spDrawMode=drawMode();
		rCommand.m_spDrawBuffer=spDrawBuffer;
		rCommand.m_pVertexMap=vertexMap;
		rCommand.m_pHullPointMap=hullPointMap;
		rCommand.m_pHullFacePoint=hullFacePoint;
		rCommand.m_elementCount=elementCount;
		rCommand.m_projection=view()->projection();
		rCommand.m_instruction=instruction;
		rCommand.m_stripMode=strip;
		rCommand.m_multicolor=multicolor;
		rCommand.m_multiradius=multiradius;
		rCommand.m_vertices=subvertices;
		rCommand.m_spImageI=NULL;

		rCommand.m_pVertex=rCommand.m_localBuffer.m_pVertex;
		memcpy((void*)rCommand.m_pVertex,vertex+oversize*offset,
				subvertices*oversize*sizeof(SpatialVector));

		if(!normal)
		{
			rCommand.m_pNormal=NULL;
		}
		else
		{
			rCommand.m_pNormal=rCommand.m_localBuffer.m_pNormal;
			memcpy((void*)rCommand.m_pNormal,normal+offset,
					subvertices*sizeof(SpatialVector));
		}

		if(!color)
		{
			rCommand.m_pColor=NULL;
		}
		else if (multicolor)
		{
			rCommand.m_pColor=rCommand.m_localBuffer.m_pColor;
			memcpy((void*)rCommand.m_pColor,color+offset,
					subvertices*sizeof(Color));
		}
		else
		{
			rCommand.m_pColor=rCommand.m_localBuffer.m_pColor;
			rCommand.m_pColor[0]=color[0];
		}

		if(!radius)
		{
			rCommand.m_pRadius=NULL;
		}
		else if (multiradius)
		{
			rCommand.m_pRadius=rCommand.m_localBuffer.m_pRadius;
			memcpy((void*)rCommand.m_pRadius,radius+offset,
					subvertices*sizeof(Color));
		}
		else
		{
			rCommand.m_pRadius=rCommand.m_localBuffer.m_pRadius;
			rCommand.m_pRadius[0]=radius[0];
		}

		if(!texture)
		{
			rCommand.m_pTexture=NULL;
		}
		else
		{
			rCommand.m_pTexture=rCommand.m_localBuffer.m_pTexture;
			memcpy((void*)rCommand.m_pTexture,texture+offset,
					subvertices*sizeof(Vector2));
		}

		if(!element)
		{
			rCommand.m_pElement=NULL;
		}
		else
		{
			rCommand.m_pElement=rCommand.m_localBuffer.m_pElement;
			memcpy((void*)rCommand.m_pElement,element,
					elementCount*sizeof(Vector3i));
		}

		if(rCommand.m_instruction==Command::e_cylinders)
		{
			rCommand.m_pTexture=rCommand.m_localBuffer.m_pTexture;

			CylinderOverlay* pCylinderOverlay=
					(CylinderOverlay*)rCommand.m_pTexture;
			Real* pBaseScale=pCylinderOverlay->m_pBaseScale;
			U32* pSlices=pCylinderOverlay->m_pSlices;

			memcpy((void*)pBaseScale,auxReal+offset,subvertices*sizeof(Real));
			memcpy((void*)pSlices,auxU32+offset,subvertices*sizeof(U32));
		}
	}
}

void DrawCached::clearCameraMap(void)
{
	m_cameraMap.clear();
}

void DrawCached::adoptView(DrawCached::Command& rCommand,sp<ViewI> spViewI)
{
	const Box2i* pBox=spViewI->scissor();
	rCommand.m_scissoring=(pBox!=NULL);
	if(rCommand.m_scissoring)
	{
		rCommand.m_scissor=*pBox;
	}

	rCommand.m_viewport=spViewI->viewport();

	sp<CameraI> spCameraEditable=spViewI->camera();

	sp<CameraEditable> spMappedMode=m_cameraMap[spCameraEditable];
	if(!spMappedMode.isValid())
	{
		spMappedMode=new CameraEditable();
		m_cameraMap[spCameraEditable]=spMappedMode;
	}

	rCommand.m_spCameraEditable=spMappedMode;

	if(spCameraEditable.isValid())
	{
		spMappedMode->copy(spCameraEditable);
	}
}

DrawCached::Command& DrawCached::Command::operator=(
		const DrawCached::Command& rCommand)
{
	m_spCameraEditable=rCommand.m_spCameraEditable;
	m_scissoring=rCommand.m_scissoring;
	m_scissor=rCommand.m_scissor;
	m_viewport=rCommand.m_viewport;
	m_spDrawMode=rCommand.m_spDrawMode;
	m_spDrawBuffer=rCommand.m_spDrawBuffer;
	m_spImageI=rCommand.m_spImageI;
	m_cpDrawableI=rCommand.m_cpDrawableI;
	m_projection=rCommand.m_projection;
	m_instruction=rCommand.m_instruction;
	m_stripMode=rCommand.m_stripMode;
	m_multicolor=rCommand.m_multicolor;
	m_multiradius=rCommand.m_multiradius;
	m_vertices=rCommand.m_vertices;
	m_elementCount=rCommand.m_elementCount;

	if(rCommand.m_pVertex)
	{
		m_pVertex=m_localBuffer.m_pVertex;
		memcpy((void*)m_pVertex,rCommand.m_pVertex,
				DCH_MAX_VERTS*sizeof(SpatialVector));
	}
	else
	{
		m_pVertex=NULL;
	}

	if(rCommand.m_pNormal)
	{
		m_pNormal=m_localBuffer.m_pNormal;
		memcpy((void*)m_pNormal,rCommand.m_pNormal,
				DCH_MAX_VERTS*sizeof(SpatialVector));
	}
	else
	{
		m_pNormal=NULL;
	}

	if(rCommand.m_pColor)
	{
		m_pColor=m_localBuffer.m_pColor;
		memcpy((void*)m_pColor,rCommand.m_pColor,
				DCH_MAX_VERTS*sizeof(Color));
	}
	else
	{
		m_pColor=NULL;
	}

	if(rCommand.m_pRadius)
	{
		m_pRadius=m_localBuffer.m_pRadius;
		memcpy((void*)m_pRadius,rCommand.m_pRadius,
				DCH_MAX_VERTS*sizeof(Real));
	}
	else
	{
		m_pRadius=NULL;
	}

	if(rCommand.m_pTexture)
	{
		m_pTexture=m_localBuffer.m_pTexture;
		memcpy((void*)m_pTexture,rCommand.m_pTexture,
				DCH_MAX_VERTS*sizeof(Vector2));
	}
	else
	{
		m_pTexture=NULL;
	}

	if(rCommand.m_pElement)
	{
		m_pElement=m_localBuffer.m_pElement;
		memcpy((void*)m_pElement,rCommand.m_pElement,
				DCH_MAX_VERTS*sizeof(Vector2));
	}
	else
	{
		m_pElement=NULL;
	}

	return *this;
}

} /* namespace ext */
} /* namespace fe */
