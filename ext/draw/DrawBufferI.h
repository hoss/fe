/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __draw_DrawBufferI_h__
#define __draw_DrawBufferI_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief GPU cache

	@ingroup draw
*//***************************************************************************/
class FE_DL_EXPORT DrawBufferI:
	virtual public Component,
	public CastableAs<DrawBufferI>
{
};

} /* namespace ext */
} /* namespace fe */

#endif /* __draw_DrawBufferI_h__ */
