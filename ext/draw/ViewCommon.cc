/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <draw/draw.pmh>

namespace fe
{
namespace ext
{

ViewCommon::ViewCommon():
	m_projection(ViewI::e_none)
{
	set(m_viewport,0,0,1,1);

	m_spCameraInternal=new CameraEditable();
	m_spCameraInternal->setName("ViewCommon.CameraEditable");

	setCamera(m_spCameraInternal);
}

void ViewCommon::setCamera(sp<CameraI> a_spCameraI)
{
	m_spCameraI=a_spCameraI;

	if(m_spCameraI.isNull())
	{
		m_spCameraI=m_spCameraInternal;
	}
}

void ViewCommon::screenAlignment(SpatialTransform &a_rotation)
{
	SpatialVector zero(0.0,0.0,0.0);
	SpatialVector v[3];
	v[0] = unproject(1000.0, 0.0, 0.0, ViewI::e_current);
	v[1] = unproject(0.0, 1000.0, 0.0, ViewI::e_current);
	v[2] = unproject(0.0, 0.0, 0.0, ViewI::e_current);
	v[0] -= v[2];
	v[1] -= v[2];
	cross3(v[2], v[1], v[0]);
	cross3(v[0], v[1], v[2]);
	if(v[0] == zero || v[1] == zero || v[2] == zero)
	{
		v[0] = SpatialVector(1.0,0.0,0.0);
		v[1] = SpatialVector(0.0,1.0,0.0);
		v[2] = SpatialVector(0.0,0.0,1.0);
	}
	else
	{
		normalize(v[0]);
		normalize(v[1]);
		normalize(v[2]);
	}
	// v[0]: horizontal   v[1]: vertical v[2]: screen normal
	setColumn(0, a_rotation, v[0]);
	setColumn(1, a_rotation, v[1]);
	setColumn(2, a_rotation, v[2]);
}

void ViewCommon::screenInfo(Real &a_screenScale,
		SpatialVector &a_screenLocation,
		const SpatialTransform &a_rotation,
		const SpatialVector &a_location)
{
	a_screenLocation = project(a_location, ViewI::e_current);
	a_screenLocation[2] = 0.0;

	SpatialVector horiz;
	getColumn(0,a_rotation,horiz);

	SpatialVector strutl = project(a_location + horiz, ViewI::e_current);
	strutl[2] = 0.0;

	a_screenScale = 1.0/magnitude(strutl - a_screenLocation);

}

void ViewCommon::use(ViewI::Projection a_projection)
{
	if(a_projection!=ViewI::e_current)
	{
		setProjection(a_projection);
	}
}

SpatialVector ViewCommon::project(const SpatialVector& a_point,
	ViewI::Projection a_projection) const
{
//	feLog("ViewCommon::project %s\n",c_print(a_point));

	if(a_projection!=ViewI::e_perspective && a_projection!=ViewI::e_ortho)
	{
		feLog("ViewCommon::project not in ortho or perspective\n");
		return SpatialVector(0.0,0.0,0.0);
	}

	sp<CameraI> spCamera=camera();
	const SpatialTransform& cameraMatrix=spCamera->cameraMatrix();

	const Real fovy=spCamera->fov()[1];

	Real nearplane;
	Real farplane;
	spCamera->getPlanes(nearplane,farplane);

	SpatialVector transformed;
	transformVector(cameraMatrix,a_point,transformed);
	Vector<4,Real> transformed4(transformed);
	transformed4[3]=Real(1);

	const Real aspect=Real(width(m_viewport))/Real(height(m_viewport));

	Vector<4,Real> projected4(0.0,0.0,0.0,0.0);

	if(a_projection==ViewI::e_perspective)
	{
		const Matrix<4,4,Real> proj4x4=perspective(fovy,aspect,
				nearplane,farplane);

		fe::project(projected4,proj4x4,transformed4);
	}
	else if(a_projection==ViewI::e_ortho)
	{
		Real zoom(1);
		Vector2 center(0,0);
		if(!spCamera->rasterSpace())
		{
			spCamera->getOrtho(zoom,center);
		}

		const Vector2 size(width(viewport()),height(viewport()));
		const Matrix<4,4,Real> proj4x4=computeOrtho(zoom,center,size);

		projected4=transformVector(proj4x4,transformed4);
	}

	const SpatialVector win(
			0.5*(projected4[0]+1.0)*width(m_viewport),
			0.5*(projected4[1]+1.0)*height(m_viewport),
			0.5*(projected4[2]+1.0));

	return win;
}

SpatialVector ViewCommon::unproject(Real a_winx,Real a_winy,Real a_winz,
	ViewI::Projection a_projection) const
{
//	feLog("ViewCommon::unproject %.6G %.6G %.6G\n",
//			a_winx,a_winy,a_winz);

	if(a_projection!=ViewI::e_perspective && a_projection!=ViewI::e_ortho)
	{
		feLog("ViewCommon::project not in ortho or perspective\n");
		return SpatialVector(0.0,0.0,0.0);
	}

	sp<CameraI> spCamera=camera();
	const SpatialTransform& cameraMatrix=spCamera->cameraMatrix();

	const Real fovy=spCamera->fov()[1];

	Real nearplane;
	Real farplane;
	spCamera->getPlanes(nearplane,farplane);

	const Real aspect=Real(width(m_viewport))/Real(height(m_viewport));

	const Vector<4,Real> projected4(
			a_winx/width(m_viewport)*2.0-1.0,
			a_winy/height(m_viewport)*2.0-1.0,
			2.0*a_winz-1.0,
			1.0);

	Vector<4,Real> transformed4(0.0,0.0,0.0,0.0);

	if(a_projection==ViewI::e_perspective)
	{
		const Matrix<4,4,Real> proj4x4=perspective(fovy,aspect,
				nearplane,farplane);

		fe::unproject(transformed4,proj4x4,projected4);
	}
	else if(a_projection==ViewI::e_ortho)
	{
		Real zoom(1);
		Vector2 center(0,0);
		if(!spCamera->rasterSpace())
		{
			spCamera->getOrtho(zoom,center);
		}

		const Vector2 size(width(viewport()),height(viewport()));
		const Matrix<4,4,Real> proj4x4=computeOrtho(zoom,center,size);

		transformed4=inverseTransformVector(proj4x4,projected4);
	}

	SpatialVector transformed=transformed4;
	const SpatialVector point=
			inverseTransformVector(cameraMatrix,transformed);

	return point;
}

Real ViewCommon::pixelSize(const SpatialVector& a_center,Real a_radius) const
{
//	feLog("ViewCommon::pixelSize %s radius %.6G\n",c_print(a_center),a_radius);

	sp<CameraI> spCamera=camera();
	const SpatialTransform& cameraMatrix=spCamera->cameraMatrix();

	const Real fovy=spCamera->fov()[1];

	SpatialVector moved;
	transformVector(cameraMatrix,a_center,moved);

	const Real pixels=
			-height(viewport())*a_radius/(tanf(0.5f*fovy*degToRad)*moved[2]);

	return pixels;
}

Real ViewCommon::worldSize(const SpatialVector& a_center,Real a_pixels) const
{
//	feLog("ViewCommon::worldSize %s pixels %.6G\n",c_print(a_center),a_pixels);

	sp<CameraI> spCamera=camera();
	const SpatialTransform& cameraMatrix=spCamera->cameraMatrix();

	const Real fovy=spCamera->fov()[1];

	SpatialVector moved;
	transformVector(cameraMatrix,a_center,moved);

	const Real units=fe::maximum(Real(0),
			-a_pixels*(tanf(0.5f*fovy*degToRad)*moved[2])/height(viewport()));

	return units;
}

//* static
Matrix<4,4,Real> ViewCommon::computeOrtho(Real a_zoom,
		const Vector<2,Real> &a_center,
		const Vector<2,Real> &a_size)
{
	FEASSERT(a_zoom>0.0f);
	FEASSERT(a_size[0]>0.0f);
	FEASSERT(a_size[1]>0.0f);

	//* NOTE get it pixel perfect

#if FALSE
	Real left = (-a_center[0]-0.375)/a_zoom;
	Real right = (-a_center[0]+a_size[0]-0.375)/a_zoom;
	Real bottom = (-a_center[1]-0.375)/a_zoom;
	Real top = (-a_center[1]+a_size[1]-0.375)/a_zoom;
#else
	Real left = (-a_center[0]*a_size[0]-0.375)/a_zoom;
	Real right = ((1.0-a_center[0])*a_size[0]-0.375)/a_zoom;
	Real bottom = (-a_center[1]*a_size[1]-0.375)/a_zoom;
	Real top = ((1.0-a_center[1])*a_size[1]-0.375)/a_zoom;
#endif

	//* TODO
	Real near_val = -1e3;
	Real far_val = 1e3;

	return ortho(left,right,bottom,top,near_val,far_val);
}

} /* namespace ext */
} /* namespace fe */
