/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __draw_DrawableI_h__
#define __draw_DrawableI_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Visualizable object using a DrawI

	@ingroup draw
*//***************************************************************************/
class FE_DL_EXPORT DrawableI:
	virtual public Component,
	public CastableAs<DrawableI>
{
	public:
				/** @brief Draw with current settings

					const versions do not permit caching. */
virtual	void	draw(sp<DrawI> a_spDrawI,const Color *a_pColor)				=0;
virtual	void	draw(sp<DrawI> a_spDrawI,const Color *a_pColor) const		=0;
virtual void	draw(const SpatialTransform &a_transform,
						sp<DrawI> a_spDrawI,const Color* a_pColor)			=0;
virtual void	draw(const SpatialTransform &a_transform,
						sp<DrawI> a_spDrawI,const Color* a_pColor) const	=0;
virtual void	draw(const SpatialTransform &a_transform,
						sp<DrawI> a_spDrawI,const Color* a_pColor,
						sp<DrawBufferI> a_spDrawBuffer,
						sp<PartitionI> a_spPartition)						=0;
virtual void	draw(const SpatialTransform &a_transform,
						sp<DrawI> a_spDrawI,const Color* a_pColor,
						sp<DrawBufferI> a_spDrawBuffer,
						sp<PartitionI> a_spPartition) const					=0;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __draw_DrawableI_h__ */

