/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __draw_ViewI_h__
#define __draw_ViewI_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief A rendering region

	@ingroup draw
*//***************************************************************************/
class FE_DL_EXPORT ViewI:
	virtual public Component,
	public CastableAs<ViewI>
{
	public:

		enum Projection
		{
			e_none,
			e_perspective,
			e_ortho,
			e_current
		};

						/// @brief Specify window where to draw
virtual	void			setWindow(sp<WindowI> a_spWindowI)					=0;
virtual	sp<WindowI>		window(void) const									=0;

						/// @brief Define region where to draw
virtual	void			setViewport(const Box2i& a_rect)					=0;
virtual Box2i			viewport(void) const								=0;

						/** @brief Get current camera

							This is the most recent camera set with setCamera().
							*/
virtual	sp<CameraI>		camera(void) const									=0;
virtual	void			setCamera(sp<CameraI> a_spCameraI)					=0;

virtual	void			use(Projection a_projection)						=0;

						/// @brief Set whether ortho or perspective
virtual	void			setProjection(Projection a_projection)				=0;
						/// @brief Get current projection
virtual	Projection		projection(void) const								=0;

						/** @brief Determine 3D location of screen coordinate

							By default, the current Projection
							determines the projection method.

							Window coordinates orginate from bottom left. */
virtual	SpatialVector	unproject(Real a_winx,Real a_winy,Real a_winz,
								Projection a_projection=e_current) const	=0;

						/** @brief Project 3D location into screen coordinates

							By default, the current Projection
							determines the projection method.

							Window coordinates orginate from bottom left. */
virtual	SpatialVector	project(const SpatialVector& a_point,
								Projection a_projection= e_current) const	=0;

						/** @brief Estimate object's screen size

							The input is a sphere center and radius. */
virtual	Real			pixelSize(const SpatialVector& a_center,
								Real a_radius) const						=0;

						/** @brief Estimate object's world size

							The input is a sphere center and projected
							pixel radius. */
virtual	Real			worldSize(const SpatialVector& a_center,
								Real a_pixels) const						=0;

						/**	Return a screen aligned rotation. */
virtual	void			screenAlignment(SpatialTransform &a_rotation)		=0;

						/** Given a screen aligned rotation and world location,
							return the screen projected location and scale.
							Useful for normalizing object scale to
							the screen. */
virtual	void			screenInfo(Real &a_screenScale,
							SpatialVector &a_screenLocation,
							const SpatialTransform &a_rotation,
							const SpatialVector &a_location)				=0;

						/** @brief Clip all drawing operations to a 2D box

							Use NULL pointer to stop scissoring. */
virtual	void			setScissor(const Box2i* a_pBox)						=0;

						/** @brief Get current scissor region

							NULL indicates no scissoring. */
virtual	const Box2i*	scissor(void) const									=0;

};

} /* namespace ext */
} /* namespace fe */

#endif /* __draw_ViewI_h__ */
