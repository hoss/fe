/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __draw_DrawNull_h__
#define __draw_DrawNull_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Draw nothing for DrawI, elegantly

	@ingroup draw
*//***************************************************************************/
class FE_DL_EXPORT DrawNull: virtual public DrawI
{
	public:
virtual	BWORD			isDirect(void) const	{ return TRUE; }

virtual	void			setDrawMode(sp<DrawMode> spDrawMode)				{}
virtual	sp<DrawMode>	drawMode(void) const	{ return sp<DrawMode>(NULL); }
virtual void			pushDrawMode(sp<DrawMode> mode)						{}
virtual sp<DrawMode>	popDrawMode(void)		{ return sp<DrawMode>(NULL); }
virtual void			pushMatrixState(void)								{}
virtual void			popMatrixState(void)								{}
virtual	void			pushMatrix(MatrixMode a_mode,Real a_values[16])		{}
virtual	void			popMatrix(MatrixMode a_mode)						{}
virtual	void			unbindVertexArray(void)								{}
virtual void			setBrightness(Real a_brightness)					{}
virtual Real			brightness(void)		{ return 0.0; }
virtual void			setContrast(Real a_contrast)						{}
virtual Real			contrast(void)			{ return 1.0; }
virtual void			setView(sp<ViewI> spViewI)							{}
virtual sp<ViewI>		view(void) const		{ return sp<ViewI>(NULL); }

virtual	void			setDrawChain(sp<DrawI> a_spDrawI)					{}
virtual	sp<DrawI>		drawChain(void)
						{	return sp<DrawI>(NULL); }
virtual	void			setTransform(const SpatialTransform& a_rTransform)	{}
virtual	void			flush(void)											{}
virtual	void			flushLive(void)										{}
virtual	void			clearInput(void)									{}
virtual	BWORD			empty(void) const		{ return TRUE; }

virtual	sp<FontI>		font(void)					{ return sp<FontI>(NULL); }
//~virtual	void		setFontBase(U32 index)								{}
//~virtual	I32			fontHeight(I32* pAscent,I32* pDescent)	{ return 0; }
//~virtual	I32			pixelWidth(String string)				{ return 0; }

virtual	Real			multiplication(void)			{ return Real(1); }
virtual void			drawPoints(const SpatialVector *vertex,
								const SpatialVector *normal,U32 vertices,
								BWORD multicolor,const Color *color)		{}
virtual void			drawPoints(const SpatialVector *vertex,
								const SpatialVector *normal,U32 vertices,
								BWORD multicolor,const Color *color,
								sp<DrawBufferI> spDrawBuffer)				{}
virtual	void			drawLines(const SpatialVector *vertex,
								const SpatialVector *normal,U32 vertices,
								StripMode strip,BWORD multicolor,
								const Color *color)							{}
virtual	void			drawLines(const SpatialVector *vertex,
								const SpatialVector *normal,U32 vertices,
								StripMode strip,
								BWORD multicolor,const Color *color,
								BWORD multiradius,const Real *radius,
								const Vector3i *element,U32 elementCount,
								sp<DrawBufferI> spDrawBuffer)				{}
virtual	void			drawCurve(const SpatialVector *vertex,
								const SpatialVector *normal,
								const Real* radius,U32 vertices,
								BWORD multicolor,const Color *color)		{}
virtual void			drawTriangles(const SpatialVector *vertex,
								const SpatialVector *normal,
								const Vector2 *texture,U32 vertices,
								StripMode strip,BWORD multicolor,
								const Color *color)							{}
virtual void			drawTriangles(const SpatialVector *vertex,
								const SpatialVector *normal,
								const Vector2 *texture,U32 vertices,
								StripMode strip,BWORD multicolor,
								const Color* color,
								const Array<I32>* vertexMap,
								const Array<I32>* hullPointMap,
								const Array<Vector4i>* hullFacePoint,
								sp<DrawBufferI> spDrawBuffer)				{}
virtual void			drawRectangles(const SpatialVector *vertex,
								U32 vertices,
								BWORD multicolor,const Color *color)		{}
virtual void			draw(cp<DrawableI> cpDrawableI,const Color* color)	{}
virtual void			draw(cp<DrawableI> cpDrawableI,const Color* color,
								sp<DrawBufferI> spDrawBuffer)			{}
virtual void			drawTransformed(const SpatialTransform& transform,
								cp<DrawableI> cpDrawableI,
								const Color* color)							{}
virtual void			drawTransformed(const SpatialTransform& transform,
								cp<DrawableI> cpDrawableI,const Color* color,
								sp<DrawBufferI> spDrawBuffer)			{}
virtual void			drawBox(const Box2& box,const Color& color)			{}
virtual void			drawBox(const Box2i& box,const Color& color)		{}
virtual void			drawBox(const Box3& box,const Color& color)			{}
virtual	void			drawAlignedText(const SpatialVector& location,
								const String text,const Color& color)		{}
virtual void			drawCircle(const SpatialTransform& transform,
								const SpatialVector *scale,
								const Color& color)							{}
virtual void			drawSphere(const SpatialTransform& transform,
								const SpatialVector *scale,
								const Color& color)							{}
virtual void			drawCylinder(const SpatialTransform& transform,
								const SpatialVector *scale,Real baseScale,
								const Color& color,U32 slices)				{}
virtual void			drawCylinder(const SpatialVector& location1,
								const SpatialVector& location2,
								Real radius1,Real radius2,
								const Color& color,U32 slices)				{}
virtual void			drawCylinders(const SpatialTransform* transform,
								const SpatialVector* scale,
								const Real *baseScale,
								const U32* slices,U32 cylinders,
								BWORD multicolor,const Color* color)		{}
virtual void			drawTransformedCylinders(
								const SpatialTransform& pretransform,
								const SpatialTransform* transform,
								const SpatialVector* scale,
								const Real *baseScale,
								const U32* slices,U32 cylinders,
								BWORD multicolor,const Color* color)		{}
virtual	void			drawArc(const SpatialTransform& transform,
								const SpatialVector *scale,
								Real startangle,Real endangle,
								const Color& color)							{}
virtual void			drawTransformedPoints(
								const SpatialTransform& transform,
								const SpatialVector *scale,
								const SpatialVector *vertex,
								const SpatialVector *normal,U32 vertices,
								BWORD multicolor,const Color *color)		{}
virtual void			drawTransformedPoints(
								const SpatialTransform& transform,
								const SpatialVector *scale,
								const SpatialVector *vertex,
								const SpatialVector *normal,U32 vertices,
								BWORD multicolor,const Color *color,
								sp<DrawBufferI> spDrawBuffer)				{}
virtual void			drawTransformedLines(
								const SpatialTransform& transform,
								const SpatialVector *scale,
								const SpatialVector *vertex,
								const SpatialVector *normal,
								U32 vertices,StripMode strip,
								BWORD multicolor,const Color *color)		{}
virtual void			drawTransformedLines(
								const SpatialTransform& transform,
								const SpatialVector *scale,
								const SpatialVector *vertex,
								const SpatialVector *normal,
								U32 vertices,StripMode strip,
								BWORD multicolor,const Color *color,
								BWORD multiradius,const Real *radius,
								const Vector3i *element,U32 elementCount,
								sp<DrawBufferI> spDrawBuffer)				{}
virtual void			drawTransformedCurve(
								const SpatialTransform& transform,
								const SpatialVector *scale,
								const SpatialVector *vertex,
								const SpatialVector *normal,
								const Real* radius,U32 vertices,
								BWORD multicolor,const Color *color)		{}
virtual void			drawTransformedTriangles(
								const SpatialTransform& transform,
								const SpatialVector *scale,
								const SpatialVector *vertex,
								const SpatialVector *normal,
								const Vector2 *texture,
								U32 vertices,StripMode strip,BWORD multicolor,
								const Color *color)							{}
virtual void			drawTransformedTriangles(
								const SpatialTransform& transform,
								const SpatialVector *scale,
								const SpatialVector *vertex,
								const SpatialVector *normal,
								const Vector2 *texture,
								U32 vertices,StripMode strip,BWORD multicolor,
								const Color* color,
								const Array<I32>* vertexMap,
								const Array<I32>* hullPointMap,
								const Array<Vector4i>* hullFacePoint,
								sp<DrawBufferI> spDrawBuffer)				{}
virtual void			drawTransformedBox(
								const SpatialTransform& transform,
								const Box3& box,
								const Color& color) 						{}
virtual void 			drawTransformedBoxes(
								const SpatialTransform* transform,
								const SpatialVector* scale,
								U32 boxes,
								BWORD multicolor,
								const Color* color)									{}
virtual void			drawAxes(Real scale)								{}
virtual void			drawTransformedAxes(const SpatialTransform& transform,
								Real scale)									{}
virtual void			drawMarker(Real radius,const Color& color)			{}
virtual void			drawTransformedMarker(
								const SpatialTransform& transform,
								Real radius,const Color& color)				{}
virtual	void			drawRaster(sp<ImageI> spImageI,
								const SpatialVector& location)				{}

virtual	sp<DrawBufferI>	createBuffer(void)
						{	return sp<DrawBufferI>(NULL); }
};

} /* namespace ext */
} /* namespace fe */

#endif /* __draw_DrawNull_h__ */
