/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __draw_CameraEditable_h__
#define __draw_CameraEditable_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Adjustable Camera

	@ingroup draw
*//***************************************************************************/
class FE_DL_EXPORT CameraEditable:
	virtual public CameraI,
	public CastableAs<CameraEditable>
{
	public:

							CameraEditable(void):
								m_fov(0.0f,45.0f),
								m_nearplane(0.1f),
								m_farplane(1e4f),
								m_zoom(1.0f),
								m_rasterSpace(FALSE)
							{
								setIdentity(m_cameraMatrix);
								set(m_center);
								setName("CameraEditable");
							}

		void				copy(sp<CameraI> a_spCameraI);

							//* as CameraI
virtual	void				setCameraMatrix(const SpatialTransform& matrix)
							{	m_cameraMatrix=matrix; }
virtual
const	SpatialTransform&	cameraMatrix(void) const
							{	return m_cameraMatrix; }

virtual	Vector2				fov(void) const
							{	return m_fov; }
virtual	void				setFov(Vector2 a_fov)
							{	m_fov=a_fov; }

virtual	void				setPlanes(Real nearplane,Real farplane)
							{	m_nearplane=nearplane;
								m_farplane=farplane; }
virtual	void				getPlanes(Real& nearplane,Real& farplane)
							{	nearplane=m_nearplane;
								farplane=m_farplane; }

virtual	void				setOrtho(Real a_zoom, const Vector2 &a_center)
							{	m_zoom = a_zoom;
								m_center = a_center; }
virtual	void				getOrtho(Real &a_zoom, Vector2 &a_center) const
							{	a_zoom = m_zoom;
								a_center = m_center; }

virtual	void				setRasterSpace(BWORD a_rasterSpace)
							{	m_rasterSpace = a_rasterSpace; }
virtual	BWORD				rasterSpace(void) const
							{	return m_rasterSpace; }

	protected:

							//* ortho or perspective
		SpatialTransform	m_cameraMatrix;

							//* perspective
		Vector2				m_fov;
		Real				m_nearplane;
		Real				m_farplane;

							//* ortho
		Vector2				m_center;
		Real				m_zoom;

		BWORD				m_rasterSpace;
};

inline void CameraEditable::copy(const sp<CameraI> a_spCameraI)
{
	setCameraMatrix(a_spCameraI->cameraMatrix());

	setFov(a_spCameraI->fov());

	Real nearplane;
	Real farplane;
	a_spCameraI->getPlanes(nearplane,farplane);
	setPlanes(nearplane,farplane);

	Real zoom;
	Vector2 center;
	a_spCameraI->getOrtho(zoom,center);
	setOrtho(zoom,center);

	setRasterSpace(a_spCameraI->rasterSpace());
}

inline String print(const sp<CameraEditable> a_spCameraEditable)
{
	const Real fovy=a_spCameraEditable->fov()[1];

	Real nearplane;
	Real farplane;
	a_spCameraEditable->getPlanes(nearplane,farplane);

	Real zoom;
	Vector2 center;
	a_spCameraEditable->getOrtho(zoom,center);

	const SpatialTransform xform=a_spCameraEditable->cameraMatrix();

	String s;
	s.sPrintf("fovy %g near %g far %g"
			" zoom %g center %s camera:\n%s",
			fovy,nearplane,farplane,
			zoom,c_print(center),c_print(xform));
	return s;
}

} /* namespace ext */
} /* namespace fe */

#endif /* __draw_CameraEditable_h__ */
