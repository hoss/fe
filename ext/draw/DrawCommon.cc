/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <draw/draw.pmh>

namespace fe
{
namespace ext
{

DrawCommon::DrawCommon(void):
	m_brightness(0.1),
	m_contrast(0.9)
{
	createCircle();
	createIcosahedron();
	createCylinders();

	m_spDefaultMode=new DrawMode();
	pushDrawMode(m_spDefaultMode);
}

void DrawCommon::initialize(void)
{
}

void DrawCommon::createDefaultView(void)
{
	m_spDefaultView=registry()->create("ViewI");
	m_spDefaultView->setName("DrawCommon default "+m_spDefaultView->name());
#if FALSE
	if(!m_spDefaultView.isValid())
	{
		feX(e_cannotCreate,"DrawCommon::initialize",
				"failed to create a ViewI");
	}
#endif
}

DrawCommon::~DrawCommon(void)
{
	destroyCylinders();
}

void DrawCommon::setDrawMode(sp<DrawMode> spMode)
{
	SAFEGUARD;
	m_spDrawMode=spMode.isValid()? spMode: m_spDefaultMode;
}

void DrawCommon::pushDrawMode(sp<DrawMode> spMode)
{
	SAFEGUARD;
	m_drawStack.push_back(spMode);
	setDrawMode(spMode);
}

sp<DrawMode> DrawCommon::popDrawMode(void)
{
	SAFEGUARD;
	sp<DrawMode> spMode;
	if(m_drawStack.size() > 0)
	{
		spMode = m_drawStack.back();
		m_drawStack.pop_back();
		if(m_drawStack.size() > 0)
		{
			setDrawMode(m_drawStack.back());
		}
	}
	return spMode;
}

void DrawCommon::drawCurve(const SpatialVector *vertex,
	const SpatialVector *normal,const Real *radius,U32 vertices,
	BWORD multicolor,const Color *color)
{
	drawLines(vertex,normal,vertices,DrawI::e_strip,multicolor,color);
}

void DrawCommon::draw(cp<DrawableI> cpDrawableI,const Color* color)
{
	draw(cpDrawableI,color,sp<DrawBufferI>(NULL));
}

void DrawCommon::draw(cp<DrawableI> cpDrawableI,const Color* color,
	sp<DrawBufferI> spDrawBuffer)
{
	if(cpDrawableI.isValid())
	{
		//* immediate rendering doesn't seem like it needs to be protected,
		//* but this needs to behave similar to complex implementations
		cpDrawableI.protect();

		if(spDrawBuffer.isValid())
		{
			SpatialTransform transform;
			setIdentity(transform);

			cpDrawableI->draw(transform,sp<DrawI>(this),color,spDrawBuffer,
					sp<PartitionI>(NULL));
		}
		else
		{
			cpDrawableI->draw(sp<DrawI>(this),color);
		}
	}

	//* cpDrawableI destruction will release its share of protection
}

void DrawCommon::drawTransformed(const SpatialTransform &transform,
	cp<DrawableI> cpDrawableI,const Color* color)
{
	drawTransformed(transform,cpDrawableI,color,sp<DrawBufferI>(NULL));
}

void DrawCommon::drawTransformed(const SpatialTransform &transform,
	cp<DrawableI> cpDrawableI,const Color* color,sp<DrawBufferI> spDrawBuffer)
{
	if(cpDrawableI.isValid())
	{
		//* immediate rendering doesn't seem like it needs to be protected,
		//* but this needs to behave similar to complex implementations
		cpDrawableI.protect();

		cpDrawableI->draw(transform,sp<DrawI>(this),color,
				spDrawBuffer,sp<PartitionI>(NULL));
	}

	//* cpDrawableI destruction will release its share of protection
}

void DrawCommon::drawBox(const Box2& box,const Color& color)
{
	const Vector2& size=box.size();

	const Real x1=box[0];
	const Real x2=box[0]+size[0];
	const Real y1=box[1];
	const Real y2=box[1]+size[1];

	SpatialVector vertex[5];
	set(vertex[0],x1,y1);
	set(vertex[1],x2,y1);
	set(vertex[2],x2,y2);
	set(vertex[3],x1,y2);
	set(vertex[4],x1,y1);
	drawLines(vertex,NULL,5,e_strip,false,&color);
}

void DrawCommon::drawBox(const Box2i& box,const Color& color)
{
	I32 margin=0;
	sp<DrawMode> spDrawMode=drawMode();
	if(spDrawMode.isValid())
	{
		margin=0.5*spDrawMode->lineWidth();
	}

	const Vector2i& size=box.size();

	const Real x1=box[0]+1.0;
	const Real x2=box[0]+size[0]-1.0;
	const Real y1=box[1]+1.0;
	const Real y2=box[1]+size[1]-1.0;

	SpatialVector vertex[8];
	set(vertex[0],x1-margin,	y1);	//* bottom
	set(vertex[1],x2+margin+0.5,y1);
	set(vertex[2],x1-margin,	y2);	//* top
	set(vertex[3],x2+margin+0.5,y2);
	set(vertex[4],x2,y1+1.0);			//* right
	set(vertex[5],x2,y2);
	set(vertex[6],x1,y1+1.0);			//* left
	set(vertex[7],x1,y2);
	drawLines(vertex,NULL,8,e_discrete,false,&color);
}

/*
 * Draws axis-aligned box with "origin" corner at (box[0],box[1],box[2])
 * and dimensions contained in box.size()
 */
void DrawCommon::drawBox(const Box3& box,const Color& color)
{
	const SpatialVector& size=box.size();

	const Real x1=box[0];
	const Real x2=box[0]+size[0];
	const Real y1=box[1];
	const Real y2=box[1]+size[1];
	const Real z1=box[2];
	const Real z2=box[2]+size[2];

	SpatialVector vertex[6];
	set(vertex[0],x1,y1,z1);
	set(vertex[1],x2,y1,z1);
	set(vertex[2],x2,y2,z1);
	set(vertex[3],x1,y2,z1);
	set(vertex[4],x1,y1,z1);
	set(vertex[5],x1,y1,z2);
	drawLines(vertex,NULL,6,e_strip,false,&color);

	set(vertex[0],x2,y1,z2);
	set(vertex[1],x2,y2,z2);
	set(vertex[2],x1,y2,z2);
	set(vertex[3],x1,y1,z2);
	set(vertex[4],x2,y1,z2);
	set(vertex[5],x2,y1,z1);
	drawLines(vertex,NULL,6,e_strip,false,&color);

	set(vertex[0],x1,y2,z1);
	set(vertex[1],x1,y2,z2);
	set(vertex[2],x2,y2,z1);
	set(vertex[3],x2,y2,z2);
	drawLines(vertex,NULL,4,e_discrete,false,&color);
}

void DrawCommon::drawPoints(const SpatialVector *vertex,
	const SpatialVector *normal,U32 vertices,
	BWORD multicolor,const Color *color)
{
	drawPoints(vertex,normal,vertices,
			multicolor,color,sp<DrawBufferI>(NULL));
}

void DrawCommon::drawPoints(const SpatialVector *vertex,
	const SpatialVector *normal,U32 vertices,
	BWORD multicolor,const Color *color,
	sp<DrawBufferI> spDrawBuffer)
{
	feLog("DrawCommon::drawPoints stub\n");
}

void DrawCommon::drawLines(const SpatialVector *vertex,
	const SpatialVector *normal,U32 vertices,
	StripMode strip,BWORD multicolor,
	const Color *color)
{
	drawLines(vertex,normal,vertices,strip,
			multicolor,color,FALSE,NULL,NULL,0,sp<DrawBufferI>(NULL));
}

void DrawCommon::drawLines(const SpatialVector *vertex,
	const SpatialVector *normal,U32 vertices,
	StripMode strip,BWORD multicolor,const Color *color,
	BWORD multiradius,const Real *radius,
	const Vector3i *element,U32 elementCount,
	sp<DrawBufferI> spDrawBuffer)
{
	feLog("DrawCommon::drawLines stub\n");
}

void DrawCommon::drawTriangles(const SpatialVector *vertex,
	const SpatialVector *normal,const Vector2 *texture,U32 vertices,
	StripMode strip,BWORD multicolor,const Color *color)
{
	drawTriangles(vertex,normal,texture,vertices,strip,
			multicolor,color,NULL,NULL,NULL,sp<DrawBufferI>(NULL));
}

void DrawCommon::drawTriangles(const SpatialVector *vertex,
	const SpatialVector *normal,const Vector2 *texture,U32 vertices,
	StripMode strip,BWORD multicolor,const Color *color,
	const Array<I32>* vertexPointMap,
	const Array<I32>* hullPointMap,const Array<Vector4i>* hullFacePoint,
	sp<DrawBufferI> spDrawBuffer)
{
	feLog("DrawCommon::drawTriangles stub\n");
}

void DrawCommon::drawTransformedPoints(const SpatialTransform &transform,
	const SpatialVector *scale,const SpatialVector *vertex,
	const SpatialVector *normal,U32 vertices,
	BWORD multicolor,const Color *color)
{
	drawTransformedPoints(transform,scale,vertex,normal,vertices,
			multicolor,color,sp<DrawBufferI>(NULL));
}

void DrawCommon::drawTransformedPoints(const SpatialTransform &transform,
	const SpatialVector *scale,const SpatialVector *vertex,
	const SpatialVector *normal,U32 vertices,
	BWORD multicolor,const Color *color,
	sp<DrawBufferI> spDrawBuffer)
{
	if(!scale && transform==SpatialTransform::identity())
	{
		drawPoints(vertex,normal,vertices,multicolor,color);
		return;
	}

	SpatialVector *vertex2=(vertices>FE_DC_BUFFER_VERTICES)?
			new SpatialVector[vertices]: m_transformBuffer;
	SpatialVector *normal2=(vertices>FE_DC_BUFFER_VERTICES)?
			new SpatialVector[vertices]: m_normalBuffer;

	U32 m;
	if(scale)
	{
		SpatialVector temp;
		for(m=0;m<vertices;m++)
		{
			temp=vertex[m];
			transformVector(transform,(*scale)*vertex[m],vertex2[m]);
			if(normal)
			{
				rotateVector(transform,(*scale)*normal[m],normal2[m]);
			}
		}
	}
	else
	{
		for(m=0;m<vertices;m++)
		{
			transformVector(transform,vertex[m],vertex2[m]);
			if(normal)
			{
				rotateVector(transform,normal[m],normal2[m]);
			}
		}
	}

	drawPoints(vertex2,normal? normal2: NULL,vertices,multicolor,color);

	if(vertex2!=m_transformBuffer)
	{
		delete[] vertex2;
		delete[] normal2;
	}
}

void DrawCommon::drawTransformedLines(const SpatialTransform &transform,
	const SpatialVector *scale,const SpatialVector *vertex,
	const SpatialVector *normal,U32 vertices,StripMode strip,BWORD multicolor,
	const Color *color)
{
	drawTransformedLines(transform,scale,vertex,normal,vertices,strip,
			multicolor,color,FALSE,NULL,NULL,0,sp<DrawBufferI>(NULL));
}

void DrawCommon::drawTransformedLines(const SpatialTransform &transform,
	const SpatialVector *scale,const SpatialVector *vertex,
	const SpatialVector *normal,U32 vertices,StripMode strip,
	BWORD multicolor,const Color *color,
	BWORD multiradius,const Real *radius,
	const Vector3i *element,U32 elementCount,
	sp<DrawBufferI> spDrawBuffer)
{
	if(!scale && transform==SpatialTransform::identity())
	{
		drawLines(vertex,normal,vertices,strip,
				multicolor,color,multiradius,radius,
				element,elementCount,spDrawBuffer);
		return;
	}

	SpatialVector *vertex2=(vertices>FE_DC_BUFFER_VERTICES)?
			new SpatialVector[vertices]: m_transformBuffer;
	SpatialVector *normal2=(vertices>FE_DC_BUFFER_VERTICES)?
			new SpatialVector[vertices]: m_normalBuffer;

	U32 m;
	if(scale)
	{
		for(m=0;m<vertices;m++)
		{
			transformVector(transform,(*scale)*vertex[m],vertex2[m]);
		}
		if(normal)
		{
			for(m=0;m<vertices;m++)
			{
				rotateVector(transform,(*scale)*normal[m],normal2[m]);
			}
		}
	}
	else
	{
		for(m=0;m<vertices;m++)
		{
			transformVector(transform,vertex[m],vertex2[m]);
		}
		if(normal)
		{
			for(m=0;m<vertices;m++)
			{
				rotateVector(transform,normal[m],normal2[m]);
			}
		}
	}

	drawLines(vertex2,normal? normal2: NULL,vertices,strip,
			multicolor,color,multiradius,radius,
			element,elementCount,spDrawBuffer);

	if(vertex2!=m_transformBuffer)
	{
		delete[] vertex2;
		delete[] normal2;
	}
}

void DrawCommon::drawTransformedCurve(const SpatialTransform &transform,
	const SpatialVector *scale,const SpatialVector *vertex,
	const SpatialVector *normal,const Real* radius,U32 vertices,
	BWORD multicolor,const Color *color)
{
	if(!scale && transform==SpatialTransform::identity())
	{
		drawCurve(vertex,normal,radius,vertices,multicolor,color);
		return;
	}

	SpatialVector *vertex2=(vertices>FE_DC_BUFFER_VERTICES)?
			new SpatialVector[vertices]: m_transformBuffer;
	SpatialVector *normal2=(vertices>FE_DC_BUFFER_VERTICES)?
			new SpatialVector[vertices]: m_normalBuffer;

	U32 m;
	if(scale)
	{
		//* TODO scale radius

		for(m=0;m<vertices;m++)
		{
			transformVector(transform,(*scale)*vertex[m],vertex2[m]);
		}
		if(normal)
		{
			for(m=0;m<vertices;m++)
			{
				rotateVector(transform,(*scale)*normal[m],normal2[m]);
			}
		}
	}
	else
	{
		for(m=0;m<vertices;m++)
		{
			transformVector(transform,vertex[m],vertex2[m]);
		}
		if(normal)
		{
			for(m=0;m<vertices;m++)
			{
				rotateVector(transform,normal[m],normal2[m]);
			}
		}
	}

	drawCurve(vertex2,normal? normal2: NULL,radius,vertices,multicolor,color);

	if(vertex2!=m_transformBuffer)
	{
		delete[] vertex2;
		delete[] normal2;
	}
}

void DrawCommon::drawTransformedTriangles(const SpatialTransform &transform,
	const SpatialVector *scale,const SpatialVector *vertex,
	const SpatialVector *normal,const Vector2 *texture,U32 vertices,
	StripMode strip,BWORD multicolor,const Color *color)
{
	drawTransformedTriangles(transform,scale,vertex,
			normal,texture,vertices,strip,
			multicolor,color,NULL,NULL,NULL,sp<DrawBufferI>(NULL));
}

void DrawCommon::drawTransformedTriangles(const SpatialTransform &transform,
	const SpatialVector *scale,const SpatialVector *vertex,
	const SpatialVector *normal,const Vector2 *texture,U32 vertices,
	StripMode strip,BWORD multicolor,const Color *color,
	const Array<I32>* vertexMap,
	const Array<I32>* hullPointMap,const Array<Vector4i>* hullFacePoint,
	sp<DrawBufferI> spDrawBuffer)
{
	if(!scale && transform==SpatialTransform::identity())
	{
		drawTriangles(vertex,normal,texture,vertices,strip,multicolor,
				color,vertexMap,hullPointMap,hullFacePoint,spDrawBuffer);
		return;
	}

	SpatialVector *vertex2=(vertices>FE_DC_BUFFER_VERTICES)?
			new SpatialVector[vertices]: m_transformBuffer;
	SpatialVector *normal2=(vertices>FE_DC_BUFFER_VERTICES)?
			new SpatialVector[vertices]: m_normalBuffer;

	U32 m;
	if(scale)
	{
		for(m=0;m<vertices;m++)
		{
			transformVector(transform,(*scale)*vertex[m],vertex2[m]);
			rotateVector(transform,(*scale)*normal[m],normal2[m]);
		}
	}
	else
	{
		for(m=0;m<vertices;m++)
		{
			transformVector(transform,vertex[m],vertex2[m]);
			rotateVector(transform,normal[m],normal2[m]);
		}
	}

	drawTriangles(vertex2,normal2,texture,vertices,strip,multicolor,
			color,vertexMap,hullPointMap,hullFacePoint,spDrawBuffer);

	if(vertex2!=m_transformBuffer)
	{
		delete[] vertex2;
		delete[] normal2;
	}
}

/*
 * Draws a box that is rotated and translated according to the provided transform
 */
void DrawCommon::drawTransformedBox(const SpatialTransform& transform, const Box3& box, const Color& color)
{
	SpatialVector boxPosition(box[0], box[1], box[2]);
	SpatialTransform translatedTransform = transform;
	translate(translatedTransform, boxPosition);
	drawTransformedBoxes(&translatedTransform,&box.size(),1,false,&color);
}

/*
 * Draws one or more boxes that are rotated and translated according to the provided transform
 * Box is specified by scale (span along x, y, and z)
 */
void DrawCommon::drawTransformedBoxes(
	const SpatialTransform* transform, const SpatialVector* scale,
	U32 boxes, BWORD multicolor,const Color* color)
{
	int arraySize = boxes * 24;
	SpatialVector* vertex = new SpatialVector[arraySize];

	const Real x1 = 0;
	const Real y1 = 0;
	const Real z1 = 0;

	for(U32 i = 0; i < boxes; i++)
	{
		const SpatialVector& size=scale[i];

		const Real x2 = size[0];
		const Real y2 = size[1];
		const Real z2 = size[2];

		const I32 i24=i*24;

		vertex[i24+0] = transformVector(transform[i],SpatialVector(x1,y1,z1));
		vertex[i24+1] = transformVector(transform[i],SpatialVector(x2,y1,z1));
		vertex[i24+2] = transformVector(transform[i],SpatialVector(x2,y1,z2));
		vertex[i24+3] = transformVector(transform[i],SpatialVector(x2,y2,z2));
		vertex[i24+4] = transformVector(transform[i],SpatialVector(x2,y2,z1));
		vertex[i24+5] = transformVector(transform[i],SpatialVector(x1,y2,z1));
		vertex[i24+6] = transformVector(transform[i],SpatialVector(x1,y2,z2));
		vertex[i24+7] = transformVector(transform[i],SpatialVector(x1,y1,z2));
		vertex[i24+8] =  vertex[i24+0];
		vertex[i24+9] =  vertex[i24+5];
		vertex[i24+10] = vertex[i24+1];
		vertex[i24+11] = vertex[i24+2];
		vertex[i24+12] = vertex[i24+3];
		vertex[i24+13] = vertex[i24+4];
		vertex[i24+14] = vertex[i24+5];
		vertex[i24+15] = vertex[i24+6];
		vertex[i24+16] = vertex[i24+7];
		vertex[i24+17] = vertex[i24+0];
		vertex[i24+18] = vertex[i24+1];
		vertex[i24+19] = vertex[i24+4];
		vertex[i24+20] = vertex[i24+3];
		vertex[i24+21] = vertex[i24+6];
		vertex[i24+22] = vertex[i24+2];
		vertex[i24+23] = vertex[i24+7];
	}

	if(multicolor)
	{
		// If multicolor, the 'color' argument should be of length 'boxes'
		// This color argument must be boxes * 12, since there are 12 lines per box
		Color* convertedColor = new Color[24*boxes];
		for(U32 i = 0; i < boxes; i++)
		{
			for(U32 j = 0; j < 24; j++)
			{
				convertedColor[24*i+j] = color[i];
			}
		}
		drawLines(vertex,NULL,arraySize,e_discrete,true,convertedColor);
		delete[] convertedColor;
	}
	else
	{
		drawLines(vertex,NULL,arraySize,e_discrete,false,&color[0]);
	}
	delete[] vertex;
}

void DrawCommon::createCircle(void)
{
	U32 m;
	for(m=0;m<FE_DC_CIRCLE_SIDES;m++)
	{
		Real angle=fe::pi*2.0f*m/(Real)FE_DC_CIRCLE_SIDES;

		set(m_circleVertex[m+1],cosf(angle),sinf(angle));
	}
	for(m=0;m<FE_DC_CIRCLE_SIDES+2;m++)
	{
		set(m_circleNormal[m],0.0f,0.0f,1.0f);
	}

	set(m_circleVertex[0]);
	m_circleVertex[FE_DC_CIRCLE_SIDES+1]=m_circleVertex[1];
}

void DrawCommon::drawCircle(const SpatialTransform &transform,
	const SpatialVector *scale,const Color &color)
{
	DrawMode::DrawStyle style=drawStyle();

	if(style==DrawMode::e_solid || style==DrawMode::e_edgedSolid ||
			style==DrawMode::e_foreshadow || style==DrawMode::e_ghost)
	{
		drawTransformedTriangles(transform,scale,m_circleVertex,
				m_circleNormal,NULL,FE_DC_CIRCLE_SIDES+2,e_fan,false,&color);
	}

	if(style==DrawMode::e_pointCloud || style==DrawMode::e_wireframe ||
			style==DrawMode::e_outline || style==DrawMode::e_edgedSolid)
	{
		drawTransformedLines(transform,scale,&m_circleVertex[1],NULL,
				FE_DC_CIRCLE_SIDES+1,e_strip,false,&color);
	}
}

void DrawCommon::drawSphere(const SpatialTransform &transform,
	const SpatialVector *scale,const Color &color)
{
#if FALSE
	drawOrthogonalCircles(transform,scale,color);
#else
	drawIcosahedron(transform,scale,color);
#endif
}

void DrawCommon::drawOrthogonalCircles(const SpatialTransform &transform,
	const SpatialVector *scale,const Color &color)
{
	drawCircle(transform,scale,color);

	SpatialTransform transform2=transform;
	rotate(transform2,90.0f*degToRad,e_yAxis);
	drawCircle(transform2,scale,color);

	rotate(transform2,90.0f*degToRad,e_xAxis);
	drawCircle(transform2,scale,color);
}

void DrawCommon::createIcosahedron(void)
{
	const Real g=0.5f*(1.0f+sqrtf(5.0f)); //* golden mean

	//* unique vertices
	SpatialVector v[12];
	set(v[0],	-1.0f,	-g,		0.0f);
	set(v[1],	 1.0f,	-g,		0.0f);
	set(v[2],	 1.0f,	 g,		0.0f);
	set(v[3],	-1.0f,	 g,		0.0f);

	set(v[4],	-g,		0.0f,	-1.0f);
	set(v[5],	-g,		0.0f,	 1.0f);
	set(v[6],	 g,		0.0f,	 1.0f);
	set(v[7],	 g,		0.0f,	-1.0f);

	set(v[8],	0.0f,	-1.0f,	-g);
	set(v[9],	0.0f,	 1.0f,	-g);
	set(v[10],	0.0f,	 1.0f,	g);
	set(v[11],	0.0f,	-1.0f,	g);

	m_icosahedronVertex[0]=v[4];
	m_icosahedronVertex[1]=v[0];
	m_icosahedronVertex[2]=v[5];
	m_icosahedronVertex[3]=v[11];
	m_icosahedronVertex[4]=v[10];
	m_icosahedronVertex[5]=v[6];
	m_icosahedronVertex[6]=v[2];
	m_icosahedronVertex[7]=v[7];
	m_icosahedronVertex[8]=v[9];
	m_icosahedronVertex[9]=v[8];
	m_icosahedronVertex[10]=v[4];
	m_icosahedronVertex[11]=v[0];

	m_icosahedronVertex[12]=v[3];
	m_icosahedronVertex[13]=v[5];
	m_icosahedronVertex[14]=v[10];
	m_icosahedronVertex[15]=v[2];
	m_icosahedronVertex[16]=v[9];
	m_icosahedronVertex[17]=v[4];
	m_icosahedronVertex[18]=v[5];

	m_icosahedronVertex[19]=v[1];
	m_icosahedronVertex[20]=v[0];
	m_icosahedronVertex[21]=v[8];
	m_icosahedronVertex[22]=v[7];
	m_icosahedronVertex[23]=v[6];
	m_icosahedronVertex[24]=v[11];
	m_icosahedronVertex[25]=v[0];

	for(U32 m=0;m<FE_DC_ICOSAHEDRON_VERTICES;m++)
	{
		m_icosahedronNormal[m]=normalize(m_icosahedronVertex[m]);

		Real phi=acosf(m_icosahedronNormal[m][1]);
		Real theta=atan2f(m_icosahedronNormal[m][2],m_icosahedronNormal[m][0]);

		set(m_icosahedronTexture[m], 0.5f+0.5f*theta/fe::pi, phi/fe::pi);
	}
}

void DrawCommon::drawIcosahedron(const SpatialTransform &transform,
	const SpatialVector *scale,const Color &color)
{
	const BWORD multicolor=false;

	//* equator (askew)
	drawTransformedTriangles(transform,scale,m_icosahedronVertex,
			m_icosahedronNormal,m_icosahedronTexture,
			12,e_strip,multicolor,&color);

	//* top
	drawTransformedTriangles(transform,scale,&m_icosahedronVertex[12],
			&m_icosahedronNormal[12],&m_icosahedronTexture[12],
			7,e_fan,multicolor,&color);

	//* bottom
	drawTransformedTriangles(transform,scale,&m_icosahedronVertex[19],
			&m_icosahedronNormal[19],&m_icosahedronTexture[19],
			7,e_fan,multicolor,&color);
}

void DrawCommon::createCylinders(void)
{
	for(U32 m=0;m<FE_DC_CYLINDER_MAXSLICES-2;m++)
	{
		SpatialVector*& rCylinderVertex=m_pCylinderVertex[m];
		SpatialVector*& rCylinderNormal=m_pCylinderNormal[m];
		Vector2*& rCylinderTexture=m_pCylinderTexture[m];

		const U32 slices=m+3;
		const U32 count=(slices+1)*2;
		rCylinderVertex=new SpatialVector[count];
		rCylinderNormal=new SpatialVector[count];
		rCylinderTexture=new Vector2[count];

		for(U32 n=0;n<=slices;n++)
		{
			const Real around=n/Real(slices);
			const Real angle=2.0f*pi*around;

			set(rCylinderVertex[n*2]);
			rCylinderVertex[n*2+1]=
					SpatialVector(-sinf(angle),cosf(angle),1.0f);

			rCylinderNormal[n*2+1]=rCylinderVertex[n*2+1];
			rCylinderNormal[n*2+1][2]=0.0f;
			rCylinderNormal[n*2]=rCylinderNormal[n*2+1];

			rCylinderTexture[n*2]=Vector2(around,0.0f);
			rCylinderTexture[n*2+1]=Vector2(around,1.0f);
		}
	}
}

void DrawCommon::destroyCylinders(void)
{
	for(I32 m=FE_DC_CYLINDER_MAXSLICES-3;m>=0;m--)
	{
		delete[] m_pCylinderTexture[m];
		delete[] m_pCylinderNormal[m];
		delete[] m_pCylinderVertex[m];
	}
}

void DrawCommon::drawCylinder(const SpatialTransform& transform,
		const SpatialVector *scale,Real baseScale,
		const Color& color,U32 slices)
{
	const BWORD multicolor=false;

	if(slices<1)
	{
		slices=6;	// tweak
	}
	else if(slices<3)
	{
		slices=3;
	}
	else if(slices>FE_DC_CYLINDER_MAXSLICES)
	{
		slices=FE_DC_CYLINDER_MAXSLICES;
	}
	const U32 cylinder=slices-3;

	SpatialVector* cylinderVertex=m_pCylinderVertex[cylinder];
	for(U32 n=0;n<=slices;n++)
	{
		cylinderVertex[n*2]=cylinderVertex[n*2+1]*baseScale;
		cylinderVertex[n*2][2]=0.0f;

		// TODO adapt normals to baseScale
	}

	drawTransformedTriangles(transform,scale,cylinderVertex,
			m_pCylinderNormal[cylinder],m_pCylinderTexture[cylinder],
			(slices+1)*2,e_strip,multicolor,&color);
}

void DrawCommon::drawCylinder(const SpatialVector& location1,
	const SpatialVector& location2,Real radius1,Real radius2,
	const Color& color,U32 slices)
{
	SpatialVector direction=location2-location1;

	Real length=magnitude(direction);
	if(length>1e-6f)
	{
		normalize(direction);
	}
	else
	{
		feX(e_unsolvable,"DrawCommon::drawCylinder","negligible length");

		set(direction,0.0f,1.0f,0.0f);
	}

	SpatialTransform transform;
	set(transform,SpatialVector(0.0f,0.0f,1.0f),direction);

	setTranslation(transform,location1);
	SpatialVector scale(radius2,radius2,length);
	((DrawI*)this)->drawCylinder(transform,&scale,radius1/radius2,
			color,slices);
}

void DrawCommon::drawCylinders(const SpatialTransform* transform,
		const SpatialVector* scale,const Real *baseScale,const U32* slices,
		U32 cylinders,BWORD multicolor,const Color* color)
{
	if(multicolor)
	{
		for(U32 cylinder=0;cylinder<cylinders;cylinder++)
		{
			((DrawI*)this)->drawCylinder(transform[cylinder],&scale[cylinder],
					baseScale[cylinder],color[cylinder],slices[cylinder]);
		}
	}
	else
	{
		for(U32 cylinder=0;cylinder<cylinders;cylinder++)
		{
			((DrawI*)this)->drawCylinder(transform[cylinder],&scale[cylinder],
					baseScale[cylinder],color[0],slices[cylinder]);
		}
	}
}

void DrawCommon::drawTransformedCylinders(
	const SpatialTransform& pretransform,
	const SpatialTransform* transform,
	const SpatialVector* scale,
	const Real *baseScale,
	const U32* slices,U32 cylinders,
	BWORD multicolor,const Color* color)
{
	if(multicolor)
	{
		for(U32 cylinder=0;cylinder<cylinders;cylinder++)
		{
			((DrawI*)this)->drawCylinder(transform[cylinder]*pretransform,
					&scale[cylinder],
					baseScale[cylinder],color[cylinder],slices[cylinder]);
		}
	}
	else
	{
		for(U32 cylinder=0;cylinder<cylinders;cylinder++)
		{
			((DrawI*)this)->drawCylinder(transform[cylinder]*pretransform,
					&scale[cylinder],
					baseScale[cylinder],color[0],slices[cylinder]);
		}
	}
}

void DrawCommon::drawArc(const SpatialTransform &transform,
	const SpatialVector *scale,Real startangle,Real endangle,
	const Color &color)
{
	Real rotations=(endangle-startangle)/(fe::pi*2.0f);
	U32 segments=(U32)(FE_DC_CIRCLE_SIDES*rotations+1.0f);

	Real remainder=FE_DC_CIRCLE_SIDES*rotations+1.0f-segments;

	if(remainder>0.5f)
	{
		// round up
		segments++;
		remainder-=1.0f;
	}
	startangle+=remainder*0.5f * (fe::pi*2.0f)/(Real)FE_DC_CIRCLE_SIDES;

#if FALSE
	if(segments<0)
	{
		segments= -segments;
		startangle=endangle;
	}
#endif

	if(segments>FE_DC_CIRCLE_SIDES+1)
	{
		segments=FE_DC_CIRCLE_SIDES+1;
	}

	SpatialTransform rot=transform;
	rotate(rot,startangle,e_xAxis);

	drawTransformedLines(rot,scale,&m_circleVertex[1],NULL,segments,
			e_strip,false,&color);

#if FALSE
	const Color white(1.0f,1.0f,1.0f);
	SetPointSize(2.0f);
	drawTransformedPoints(rot,scale,&m_circleVertex[1],NULL,segments,
			false,&white);
	SetPointSize(1.0f);
#endif
}

void DrawCommon::drawAxes(Real scale)
{
	drawTransformedAxes(SpatialTransform::identity(),scale);
}

void DrawCommon::drawTransformedAxes(const SpatialTransform& transform,
		Real scale)
{
	const Color red(1.0,0.0,0.0,1.0);
	const Color green(0.0,1.0,0.0,1.0);
	const Color blue(0.0,0.0,1.0,1.0);
	const Color color[6]={ red,red, green,green, blue,blue };

	SpatialVector vertex[6];
	set(vertex[0]);
	set(vertex[1],(Real)scale);
	set(vertex[2]);
	set(vertex[3],0.0f,scale);
	set(vertex[4]);
	set(vertex[5],0.0f,0.0f,scale);

	drawTransformedLines(transform,NULL,vertex,NULL,6,e_discrete,true,color);
}

void DrawCommon::drawMarker(Real radius,const Color& color)
{
	drawTransformedMarker(SpatialTransform::identity(),radius,color);
}

void DrawCommon::drawTransformedMarker(const SpatialTransform& transform,
		Real radius,const Color& color)
{
#if FALSE
	SpatialVector vertex[8];
	set(vertex[0],radius,0.0f,0.0f);
	set(vertex[1],-0.707f*radius,0.707f*radius,0.0f);
	set(vertex[2],-0.707f*radius,-0.707f*radius,0.0f);
	vertex[3]=vertex[0];
	set(vertex[4],0.0f,0.0f,0.5f*radius);
	vertex[5]=vertex[1];
	vertex[6]=vertex[2];
	vertex[7]=vertex[4];

	drawTransformedLines(transform,NULL,vertex,NULL,8,e_strip,false,&color);
#else
	const Color red(1.0,0.0,0.0,1.0);
	const Color green(0.0,1.0,0.0,1.0);
	const Color blue(0.0,0.0,1.0,1.0);
	const Color colors[6]={ color,red, color,green, color,blue };

	SpatialVector vertex[6];
	set(vertex[0]);
	set(vertex[1],(Real)radius);
	set(vertex[2]);
	set(vertex[3],0.0f,radius);
	set(vertex[4]);
	set(vertex[5],0.0f,0.0f,radius);

	drawTransformedLines(transform,NULL,vertex,NULL,6,e_discrete,true,colors);
#endif
}

} /* namespace ext */
} /* namespace fe */
