/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __draw_PartitionI_h__
#define __draw_PartitionI_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief matchable list of strings

	@ingroup draw
*//***************************************************************************/
class FE_DL_EXPORT PartitionI:
	virtual public Counted,
	public CastableAs<PartitionI>
{
	public:
		enum	FilterMethod
				{
					e_matchRegex,
					e_notMatchRegex
				};

virtual	sp<PartitionI>		clone(void) const							=0;

virtual	U32					partitionCount(void) const					=0;
virtual	String				partitionName(U32 a_index) const			=0;

virtual	I32					lookup(String a_partitionString)			=0;
virtual	I32					select(String a_filterString)				=0;
virtual	I32					select(String a_filterString,
									FilterMethod a_filterMethod)		=0;
virtual	BWORD				add(String a_string)						=0;
virtual	BWORD				remove(String a_string)						=0;
virtual	BWORD				match(I32 a_partitionIndex)					=0;
virtual	BWORD				matchable(void) const						=0;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __draw_PartitionI_h__ */

