/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __draw_DrawThreaded_h__
#define __draw_DrawThreaded_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Off-threaded rendering

	@ingroup draw
*//***************************************************************************/
class FE_DL_EXPORT DrawThreaded: public DrawCached,
		public Initialize<DrawThreaded>
{
	public:
					DrawThreaded(void);
virtual				~DrawThreaded(void);

		void		initialize(void);

virtual	void		flush(void);

	private:

	class RenderWorker: public Thread::Functor
	{
		public:
							RenderWorker(sp< JobQueue<I32> > a_spJobQueue,
								U32 a_id,String a_stage):
								m_id(a_id),
								m_hpJobQueue(a_spJobQueue)					{}

	virtual					~RenderWorker(void)								{}

	virtual	void			operate(void);

			void			setObject(sp<Component> spObject)
							{	m_hpDrawThreaded=spObject; }

		private:
			U32					m_id;
			hp< JobQueue<I32> > m_hpJobQueue;
			hp<DrawThreaded>	m_hpDrawThreaded;
	};

		U32								m_jobs;
		I32								m_rendered;
		BWORD							m_released;
		sp< JobQueue<I32> >				m_spJobQueue;
		sp< Gang<RenderWorker,I32> >	m_spGang;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __draw_DrawThreaded_h__ */

