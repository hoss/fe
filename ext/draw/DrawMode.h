/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __draw_DrawMode_h__
#define __draw_DrawMode_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Configuration for rendering such as line width and backface culling

	@ingroup draw

	An instance of a DrawMode is shared between all draw operations that
	occur while it is the current DrawMode to a DrawI.
	Changing a DrawMode at any time can affect all elements that are
	associated with that DrawMode.

	It is intended for these modes to be configured and left alone prior
	to being used.  They are not an indirect interface extension to DrawI.

	The DrawMode is an optimization for cached rendering.  Without them,
	either every draw operation would have to cache an entire drawing
	state or there would be an extraordinary amount of cross checking
	to match up coincidentally identical states.
*//***************************************************************************/
class FE_DL_EXPORT DrawMode: public Counted
{
	public:

					/** @brief Rendering schemes that an an
							implementation @em may recognize

						Not all methods may be available in a
						particular situation.  */
		enum		DrawStyle
					{
						e_defaultStyle,
						e_pointCloud,
						e_wireframe,
						e_outline,
						e_solid,
						e_edgedSolid,
						e_foreshadow,
						e_ghost,
						e_drawStyleCount
					};

		enum		Coloring
					{
						e_color,
						e_normal,
						e_tangent,
						e_uv,
						e_partition,
						e_coloringCount
					};

					DrawMode(void):
						m_textureImageID(-1),
						m_drawStyle(e_defaultStyle),
						m_coloring(e_color),
						m_layer(0),
						m_pointSize(1.0f),
						m_lineWidth(1.0f),
						m_refinement(0),
						m_antialias(FALSE),
						m_frontfaceCulling(FALSE),
						m_backfaceCulling(TRUE),
						m_twoSidedLighting(FALSE),
						m_zBuffering(TRUE),
						m_lit(TRUE),
						m_shadows(FALSE),
						m_uvSpace(FALSE)
					{	setName("DrawMode"); }

		void		copy(sp<DrawMode> a_spDrawMode)
					{
						m_spTextureImage=a_spDrawMode->m_spTextureImage;
						m_textureImageID=a_spDrawMode->m_textureImageID;
						m_drawStyle=a_spDrawMode->m_drawStyle;
						m_coloring=a_spDrawMode->m_coloring;
						m_group=a_spDrawMode->m_group;
						m_layer=a_spDrawMode->m_layer;
						m_pointSize=a_spDrawMode->m_pointSize;
						m_lineWidth=a_spDrawMode->m_lineWidth;
						m_refinement=a_spDrawMode->m_refinement;
						m_antialias=a_spDrawMode->m_antialias;
						m_frontfaceCulling=a_spDrawMode->m_frontfaceCulling;
						m_backfaceCulling=a_spDrawMode->m_backfaceCulling;
						m_twoSidedLighting=a_spDrawMode->m_twoSidedLighting;
						m_zBuffering=a_spDrawMode->m_zBuffering;
						m_lit=a_spDrawMode->m_lit;
						m_shadows=a_spDrawMode->m_shadows;
						m_uvSpace=a_spDrawMode->m_uvSpace;
					}

					/// @brief Set raster for texture mapping
		void		setTextureImage(sp<ImageI> spImageI)
					{	m_spTextureImage=spImageI; }

					/// @brief Get raster for texture mapping
		sp<ImageI>	textureImage(void) const
					{	return m_spTextureImage; }

					/** @brief Set raster for texture mapping

						An id less than zero indicates to just use the
						currently selected image id for the ImageI.
					*/
		void		setTextureImageID(I32 a_imageId)
					{	m_textureImageID=a_imageId; }

					/// @brief Get raster for texture mapping
		I32			textureImageID(void) const
					{	return m_textureImageID; }

					/// @brief Set rendering scheme (see DrawStyle)
		void		setDrawStyle(DrawStyle style)	{ m_drawStyle=style; }

					/// @brief Get rendering scheme (see DrawStyle)
		DrawStyle	drawStyle(void) const			{ return m_drawStyle; }

					/// @brief Set color source (see Coloring)
		void		setColoring(Coloring coloring)	{ m_coloring=coloring; }

					/// @brief Get color source (see Coloring)
		Coloring	coloring(void) const			{ return m_coloring; }

					/// @brief Set named group
		void		setGroup(String a_group)		{ m_group=a_group; }

					/// @brief Get named group
		String		group(void) const				{ return m_group; }

					/// @brief Set rendering ordering (highest last)
		void		setLayer(I32 set)				{ m_layer=set; }

					/// @brief Get rendering ordering (highest last)
		I32			layer(void) const				{ return m_layer; }

					/// @brief Set diameter of all following drawn points
		void		setPointSize(Real pixels)
					{	m_pointSize=pixels; }

					/// @brief Get diameter of all following drawn points
		Real		pointSize(void) const
					{	return m_pointSize; }

					/// @brief Set width of all following drawn lines
		void		setLineWidth(Real pixels)
					{	m_lineWidth=pixels; }

					/// @brief Get width of all following drawn lines
		Real		lineWidth(void) const
					{	return m_lineWidth; }

					/// @brief Set subvision level of all following drawn polys
		void		setRefinement(I32 level)
					{	m_refinement=level; }

					/// @brief Get subvision level of all following drawn polys
		I32			refinement(void) const
					{	return m_refinement; }

					/// @brief Set rasterization anti-aliasing
		void		setAntialias(BWORD antialias)
					{	m_antialias=antialias; }

					/// @brief Get whether anti-aliasing is used
		BWORD		antialias(void) const
					{	return m_antialias; }

					/// @brief Set whether frontfaces are removed
		void		setFrontfaceCulling(BWORD frontfaceCulling)
					{	m_frontfaceCulling=frontfaceCulling; }

					/// @brief Get whether frontfaces are removed
		BWORD		frontfaceCulling(void) const
					{	return m_frontfaceCulling; }

					/// @brief Set whether backfaces are removed
		void		setBackfaceCulling(BWORD backfaceCulling)
					{	m_backfaceCulling=backfaceCulling; }

					/// @brief Get whether backfaces are removed
		BWORD		backfaceCulling(void) const
					{	return m_backfaceCulling; }

					/** @brief Set whether backfaces have an independent
						material */
		void		setTwoSidedLighting(BWORD twoSidedLighting)
					{	m_twoSidedLighting=twoSidedLighting; }

					/** @brief Get whether backfaces have an independent
						material */
		BWORD		twoSidedLighting(void) const
					{	return m_twoSidedLighting; }

					/// @brief Set whether z-buffering is used
		void		setZBuffering(BWORD zBuffering)
					{	m_zBuffering=zBuffering; }

					/// @brief Get whether z-buffering is used
		BWORD		zBuffering(void) const
					{	return m_zBuffering; }

					/** @brief Set lit */
		void		setLit(BWORD lit)
					{	m_lit=lit; }

					/// @brief Get whether lit
		BWORD		lit(void) const
					{	return m_lit; }

					/** @brief Set shadows */
		void		setShadows(BWORD shadows)
					{	m_shadows=shadows; }

					/// @brief Get whether shadows
		BWORD		shadows(void) const
					{	return m_shadows; }

					/** @brief Set whether in UV space */
		void		setUvSpace(BWORD uvSpace)
					{	m_uvSpace=uvSpace; }

					/// @brief Get whether in UV space
		BWORD		uvSpace(void) const
					{	return m_uvSpace; }

static	DrawStyle	stringToStyle(const String &a_styleName);

static	Coloring	stringToColoring(const String &a_coloringName);

	private:

		sp<ImageI>		m_spTextureImage;
		I32				m_textureImageID;
		DrawStyle		m_drawStyle;
		Coloring		m_coloring;
		String			m_group;
		I32				m_layer;
		Real			m_pointSize;
		Real			m_lineWidth;
		I32				m_refinement;
		BWORD			m_antialias;
		BWORD			m_frontfaceCulling;
		BWORD			m_backfaceCulling;
		BWORD			m_twoSidedLighting;
		BWORD			m_zBuffering;
		BWORD			m_lit;
		BWORD			m_shadows;
		BWORD			m_uvSpace;
};

static char DrawStyleText[DrawMode::e_drawStyleCount][32] =
{
	"default",
	"pointCloud",
	"wireframe",
	"outline",
	"solid",
	"edgedSolid",
	"foreshadow",
	"ghost"
};

static char ColoringText[DrawMode::e_coloringCount][32] =
{
	"color",
	"normal",
	"tangent",
	"uv",
	"partition"
};

inline DrawMode::DrawStyle DrawMode::stringToStyle(const String &a_styleName)
{
	if(a_styleName == "pointCloud")	{ return e_pointCloud; }
	if(a_styleName == "wireframe")	{ return e_wireframe; }
	if(a_styleName == "outline")	{ return e_outline; }
	if(a_styleName == "edgedSolid")	{ return e_edgedSolid; }
	if(a_styleName == "solid")		{ return e_solid; }
	if(a_styleName == "foreshadow")	{ return e_foreshadow; }
	if(a_styleName == "ghost")		{ return e_ghost; }

	return e_defaultStyle;
}

inline DrawMode::Coloring DrawMode::stringToColoring(
	const String &a_coloringName)
{
	if(a_coloringName == "normal")		{ return e_normal; }
	if(a_coloringName == "tangent")		{ return e_tangent; }
	if(a_coloringName == "uv")			{ return e_uv; }
	if(a_coloringName == "partition" ||
			a_coloringName == "part")	{ return e_partition; }

	return e_color;
}

} /* namespace ext */
} /* namespace fe */

namespace fe
{

inline String print(ext::DrawMode::DrawStyle a_drawStyle)
{
	return ext::DrawStyleText[a_drawStyle%ext::DrawMode::e_drawStyleCount];
}

inline String print(ext::DrawMode::Coloring a_coloring)
{
	return ext::ColoringText[a_coloring%ext::DrawMode::e_coloringCount];
}

inline String print(const sp<ext::DrawMode> spDrawMode)
{
	String s;
	s.sPrintf("style \"%s\" coloring \"%s\" layer %d pointSize %g lineWidth %g"
			" refinement %d\n  aa %d front %d back %d twoSided %d z %d lit %d"
			" shadows %d uv %d texture %p group \"%s\"",

			ext::DrawStyleText[spDrawMode->drawStyle()%
			ext::DrawMode::e_drawStyleCount],

			ext::ColoringText[spDrawMode->coloring()%
			ext::DrawMode::e_coloringCount],

			spDrawMode->layer(),
			spDrawMode->pointSize(),
			spDrawMode->lineWidth(),
			spDrawMode->refinement(),
			spDrawMode->antialias(),
			spDrawMode->frontfaceCulling(),
			spDrawMode->backfaceCulling(),
			spDrawMode->twoSidedLighting(),
			spDrawMode->zBuffering(),
			spDrawMode->lit(),
			spDrawMode->shadows(),
			spDrawMode->uvSpace(),
			spDrawMode->textureImage().raw(),
			spDrawMode->group().c_str());
	return s;
}

} /* namespace fe */

#endif /* __draw_DrawMode_h__ */
