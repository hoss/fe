/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __draw_ViewCommon_h__
#define __draw_ViewCommon_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief general-implementation methods for ViewI

	@ingroup draw
*//***************************************************************************/
class FE_DL_EXPORT ViewCommon: virtual public ViewI
{
	public:
							ViewCommon(void);

							//* as ViewI
virtual	void				setWindow(sp<WindowI> a_spWindowI)
							{	m_spWindowI=a_spWindowI; }
virtual	sp<WindowI>			window(void) const
							{	return m_spWindowI; }

virtual	void				setViewport(const Box2i& a_rect)
							{	m_viewport=a_rect; }
virtual Box2i				viewport(void) const
							{	return m_viewport; }

virtual	void				setCamera(sp<CameraI> a_spCameraI);
virtual	sp<CameraI>			camera(void) const
							{	return m_spCameraI; }

virtual	ViewI::Projection	projection(void) const
							{	return m_projection; }
virtual	void				setProjection(Projection a_projection)
							{	m_projection=a_projection; }

virtual	void				screenAlignment(SpatialTransform& a_rotation);

virtual	void				screenInfo(Real &a_screenScale,
								SpatialVector &a_screenLocation,
								const SpatialTransform& a_rotation,
								const SpatialVector &a_location);

virtual	void				setScissor(const Box2i* a_pBox)					{}
virtual	const Box2i*		scissor(void) const				{ return NULL; }

virtual	void				use(ViewI::Projection a_projection);

virtual	SpatialVector		unproject(Real a_winx,Real a_winy,Real a_winz,
									ViewI::Projection a_projection=
									ViewI::e_current) const;

virtual	SpatialVector		project(const SpatialVector& a_point,
									ViewI::Projection a_projection=
									ViewI::e_current) const;

virtual	Real				pixelSize(const SpatialVector& a_center,
									Real a_radius) const;

virtual	Real				worldSize(const SpatialVector& a_center,
									Real a_pixels) const;

static	Matrix<4,4,Real>	computeOrtho(Real a_zoom,
								const Vector<2,Real> &a_center,
								const Vector<2,Real> &a_size);

	private:
		sp<CameraI>			m_spCameraI;
		sp<CameraEditable>	m_spCameraInternal;
		ViewI::Projection	m_projection;

		sp<WindowI>			m_spWindowI;
		Box2i				m_viewport;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __draw_ViewCommon_h__ */
