/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __draw_DrawCommon_h__
#define __draw_DrawCommon_h__

namespace fe
{
namespace ext
{

#define FE_DC_CIRCLE_SIDES			64	//* tweak
#define FE_DC_CYLINDER_MAXSLICES	16	//* tweak

#define FE_DC_BUFFER_VERTICES		100	//* reusable space vs new()
#define FE_DC_ICOSAHEDRON_VERTICES	26	//* don't tweak, includes repeats

// TODO SAFEGUARD all drawing operations

/**************************************************************************//**
	@brief Drawing functions not specific to the graphics language

	@ingroup draw

	Using setDrawMode(NULL) activates the default mode,
	which you can get with drawMode() and change.
	The default mode is also the initial mode.
*//***************************************************************************/
class FE_DL_EXPORT DrawCommon: public Initialize<DrawCommon>,
		public ObjectSafe<DrawCommon>, virtual public DrawI
	{
	public:
						DrawCommon(void);
		void			initialize(void);
virtual					~DrawCommon(void);

virtual void			setDrawMode(sp<DrawMode> spMode);
virtual sp<DrawMode>	drawMode(void) const
						{	SAFEGUARD;
							return m_spDrawMode; }
virtual void			pushDrawMode(sp<DrawMode> mode);
virtual sp<DrawMode>	popDrawMode(void);
virtual void			pushMatrixState(void)								{}
virtual void			popMatrixState(void)								{}

virtual	void			pushMatrix(MatrixMode a_mode,Real a_values[16])		{}
virtual	void			popMatrix(MatrixMode a_mode)						{}

virtual	void			unbindVertexArray(void)								{}

virtual void			setBrightness(Real a_brightness)
						{	m_brightness=a_brightness; }
virtual Real			brightness(void)	{ return m_brightness; }

virtual void			setContrast(Real a_contrast)
						{	m_contrast=a_contrast; }
virtual Real			contrast(void)		{ return m_contrast; }

virtual void			setView(sp<ViewI> spViewI)
						{	SAFEGUARD;
							m_spViewI=spViewI.isValid()?
									spViewI: m_spDefaultView; }
virtual sp<ViewI>		view(void) const
						{
							SAFEGUARD;
							if(!m_spViewI.isValid())
							{
								if(!m_spDefaultView.isValid())
								{
									const_cast<DrawCommon*>(this)
											->createDefaultView();
								}
								const_cast<DrawCommon*>(this)
										->m_spViewI=m_spDefaultView;
							}
							return m_spViewI;
						}

virtual	void			drawCurve(const SpatialVector *vertex,
								const SpatialVector *normal,
								const Real *radius,U32 vertices,
								BWORD multicolor,const Color *color);
virtual void			draw(cp<DrawableI> cpDrawableI,const Color* color);
virtual void			draw(cp<DrawableI> cpDrawableI,const Color* color,
								sp<DrawBufferI> spDrawBuffer);

virtual void			drawTransformed(const SpatialTransform& transform,
								cp<DrawableI> cpDrawableI,const Color* color);
virtual void			drawTransformed(const SpatialTransform& transform,
								cp<DrawableI> cpDrawableI,const Color* color,
								sp<DrawBufferI> spDrawBuffer);

virtual void			drawBox(const Box2& box,const Color& color);
virtual void			drawBox(const Box2i& box,const Color& color);
virtual void			drawBox(const Box3& box,const Color& color);

virtual void			drawCircle(const SpatialTransform &transform,
								const SpatialVector *scale,const Color &color);

virtual void			drawSphere(const SpatialTransform &transform,
								const SpatialVector *scale,const Color &color);

virtual void			drawCylinder(const SpatialTransform& transform,
								const SpatialVector *scale,Real baseScale,
								const Color& color,U32 slices);

virtual void			drawCylinder(const SpatialVector& location1,
								const SpatialVector& location2,
								Real radius1,Real radius2,
								const Color& color,U32 slices);

virtual void			drawCylinders(const SpatialTransform* transform,
								const SpatialVector* scale,
								const Real *baseScale,
								const U32* slices,U32 cylinders,
								BWORD multicolor,const Color* color);

virtual void			drawTransformedCylinders(
								const SpatialTransform& pretransform,
								const SpatialTransform* transform,
								const SpatialVector* scale,
								const Real *baseScale,
								const U32* slices,U32 cylinders,
								BWORD multicolor,const Color* color);

virtual	void			drawArc(const SpatialTransform &transform,
								const SpatialVector *scale,
								Real startangle,Real endangle,
								const Color &color);

virtual void			drawTransformedPoints(
								const SpatialTransform &transform,
								const SpatialVector *scale,
								const SpatialVector *vertex,
								const SpatialVector *normal,U32 vertices,
								BWORD multicolor,const Color *color);
virtual void			drawTransformedPoints(
								const SpatialTransform &transform,
								const SpatialVector *scale,
								const SpatialVector *vertex,
								const SpatialVector *normal,U32 vertices,
								BWORD multicolor,const Color *color,
								sp<DrawBufferI> spDrawBuffer);

virtual void			drawTransformedLines(
								const SpatialTransform &transform,
								const SpatialVector *scale,
								const SpatialVector *vertex,
								const SpatialVector *normal,
								U32 vertices,StripMode strip,BWORD multicolor,
								const Color *color);
virtual void			drawTransformedLines(
								const SpatialTransform &transform,
								const SpatialVector *scale,
								const SpatialVector *vertex,
								const SpatialVector *normal,
								U32 vertices,StripMode strip,
								BWORD multicolor,const Color *color,
								BWORD multiradius,const Real *radius,
								const Vector3i *element,U32 elementCount,
								sp<DrawBufferI> spDrawBuffer);

virtual void			drawTransformedCurve(
								const SpatialTransform &transform,
								const SpatialVector *scale,
								const SpatialVector *vertex,
								const SpatialVector *normal,
								const Real *radius,
								U32 vertices,BWORD multicolor,
								const Color *color);

virtual void			drawTransformedTriangles(
								const SpatialTransform &transform,
								const SpatialVector *scale,
								const SpatialVector *vertex,
								const SpatialVector *normal,
								const Vector2 *texture,U32 vertices,
								StripMode strip,BWORD multicolor,
								const Color *color);
virtual void			drawTransformedTriangles(
								const SpatialTransform &transform,
								const SpatialVector *scale,
								const SpatialVector *vertex,
								const SpatialVector *normal,
								const Vector2 *texture,U32 vertices,
								StripMode strip,BWORD multicolor,
								const Color *color,
								const Array<I32>* vertexMap,
								const Array<I32>* hullPointMap,
								const Array<Vector4i>* hullFacePoint,
								sp<DrawBufferI> spDrawBuffer);

virtual void 			drawTransformedBox(const SpatialTransform& transform,
								const Box3& box, const Color& color);

virtual void 			drawTransformedBoxes(
								const SpatialTransform* transform,
								const SpatialVector* scale,
								U32 boxes,
								BWORD multicolor,
								const Color* color);

virtual	void			drawAxes(Real scale);
virtual	void			drawTransformedAxes(const SpatialTransform &transform,
								Real scale);

virtual	void			drawMarker(Real radius,const Color& color);
virtual	void			drawTransformedMarker(
								const SpatialTransform &transform,
								Real radius,const Color& color);

virtual	BWORD			isDirect(void) const	{ return TRUE; }
virtual	void			setDrawChain(sp<DrawI> a_spDrawI)					{}
virtual	sp<DrawI>		drawChain(void)
						{	return sp<DrawI>(NULL); }
virtual	void			setTransform(const SpatialTransform& a_rTransform)	{}
virtual	void			flush(void)											{}
virtual	void			flushLive(void)										{}
virtual	void			clearInput(void)									{}
virtual	BWORD			empty(void) const		{ return TRUE; }

virtual	sp<FontI>		font(void)					{ return sp<FontI>(NULL); }
//~virtual	void			setFontBase(U32 index)								{}
//~virtual	I32				fontHeight(I32* pAscent,I32* pDescent)	{ return 0; }
//~virtual	I32				pixelWidth(String string)				{ return 0; }

virtual	Real			multiplication(void)			{ return Real(1); }

virtual void			drawPoints(const SpatialVector *vertex,
								const SpatialVector *normal,U32 vertices,
								BWORD multicolor,const Color *color);
virtual void			drawPoints(const SpatialVector *vertex,
								const SpatialVector *normal,U32 vertices,
								BWORD multicolor,const Color *color,
								sp<DrawBufferI> spDrawBuffer);
virtual	void			drawLines(const SpatialVector *vertex,
								const SpatialVector *normal,U32 vertices,
								StripMode strip,BWORD multicolor,
								const Color *color);
virtual	void			drawLines(const SpatialVector *vertex,
								const SpatialVector *normal,U32 vertices,
								StripMode strip,
								BWORD multicolor,const Color *color,
								BWORD multiradius,const Real *radius,
								const Vector3i *element,U32 elementCount,
								sp<DrawBufferI> spDrawBuffer);
virtual void			drawTriangles(const SpatialVector *vertex,
								const SpatialVector *normal,
								const Vector2 *texture,U32 vertices,
								StripMode strip,BWORD multicolor,
								const Color *color);
virtual void			drawTriangles(const SpatialVector *vertex,
								const SpatialVector *normal,
								const Vector2 *texture,U32 vertices,
								StripMode strip,BWORD multicolor,
								const Color *color,
								const Array<I32>* vertexMap,
								const Array<I32>* hullPointMap,
								const Array<Vector4i>* hullFacePoint,
								sp<DrawBufferI> spDrawBuffer);
virtual void			drawRectangles(const SpatialVector *vertex,
								U32 vertices,
								BWORD multicolor,const Color *color)		{}
virtual	void			drawAlignedText(const SpatialVector& location,
								const String text,const Color& color)		{}
virtual	void			drawRaster(sp<ImageI> spImageI,
								const SpatialVector& location)				{}

virtual	sp<DrawBufferI>	createBuffer(void)
						{	return sp<DrawBufferI>(NULL); }

	protected:

		DrawMode::DrawStyle drawStyle(void) const
						{	DrawMode::DrawStyle style=m_spDrawMode->drawStyle();
							return style==DrawMode::e_defaultStyle?
									(isDirect()? DrawMode::e_edgedSolid:
									DrawMode::e_outline): style; }

		sp<ImageI>		textureImage(void) const
						{	return m_spDrawMode->textureImage(); }
		I32				textureImageID(void) const
						{	return m_spDrawMode->textureImageID(); }
		Real			pointSize(void) const
						{	return m_spDrawMode->pointSize(); }
		Real			lineWidth(void) const
						{	return m_spDrawMode->lineWidth(); }
		BWORD			antialias(void) const
						{	return m_spDrawMode->antialias(); }
		BWORD			frontfaceCulling(void) const
						{	return m_spDrawMode->frontfaceCulling(); }
		BWORD			backfaceCulling(void) const
						{	return m_spDrawMode->backfaceCulling(); }
		BWORD			twoSidedLighting(void) const
						{	return m_spDrawMode->twoSidedLighting(); }
		BWORD			zBuffering(void) const
						{	return m_spDrawMode->zBuffering(); }
		BWORD			isLit(void) const
						{	return m_spDrawMode.isValid() &&
									m_spDrawMode->lit() &&
									(drawStyle()==DrawMode::e_solid ||
									drawStyle()==DrawMode::e_edgedSolid ||
									drawStyle()==DrawMode::e_ghost); }
		BWORD			isUvSpace(void) const
						{	return m_spDrawMode.isValid() &&
									m_spDrawMode->uvSpace(); }

virtual	void			createDefaultView(void);

		sp<ViewI>		m_spDefaultView;

		Real			m_brightness;
		Real			m_contrast;

	private:

		void			drawOrthogonalCircles(
								const SpatialTransform &transform,
								const SpatialVector *scale,const Color &color);
		void			drawIcosahedron(const SpatialTransform &transform,
								const SpatialVector *scale,const Color &color);

		void			createCircle(void);
		void			createIcosahedron(void);
		void			createCylinders(void);
		void			destroyCylinders(void);

		SpatialVector	m_circleVertex[FE_DC_CIRCLE_SIDES+2];
		SpatialVector	m_circleNormal[FE_DC_CIRCLE_SIDES+2];

		SpatialVector	m_transformBuffer[FE_DC_BUFFER_VERTICES];
		SpatialVector	m_normalBuffer[FE_DC_BUFFER_VERTICES];

		SpatialVector	m_icosahedronVertex[FE_DC_ICOSAHEDRON_VERTICES];
		SpatialVector	m_icosahedronNormal[FE_DC_ICOSAHEDRON_VERTICES];
		Vector2			m_icosahedronTexture[FE_DC_ICOSAHEDRON_VERTICES];

		SpatialVector*	m_pCylinderVertex[FE_DC_CYLINDER_MAXSLICES-2];
		SpatialVector*	m_pCylinderNormal[FE_DC_CYLINDER_MAXSLICES-2];
		Vector2*		m_pCylinderTexture[FE_DC_CYLINDER_MAXSLICES-2];

		sp<DrawMode>	m_spDefaultMode;
		sp<DrawMode>	m_spDrawMode;
		sp<ViewI>		m_spViewI;

		std::vector< sp<DrawMode> >	m_drawStack;
	};

} /* namespace ext */
} /* namespace fe */

#endif /* __draw_DrawCommon_h__ */
