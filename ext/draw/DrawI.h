/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __draw_DrawI_h__
#define __draw_DrawI_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Generalized drawing of points, lines, text, and basic triangles

	@ingroup draw
*//***************************************************************************/
class FE_DL_EXPORT DrawI:
	virtual public Component,
	public CastableAs<DrawI>
{
	public:
				/// @brief Format of vertices for tri-stripping
		enum	StripMode
				{
					e_discrete,
					e_strip,
					e_fan,
					e_triple
				};
		enum	MatrixMode
				{
					e_modelview,
					e_projection,
					e_texture,
					e_color
				};

					/// @brief Set rendering mode
virtual void		setDrawMode(sp<DrawMode> mode)							=0;

					/// @brief Get rendering mode
virtual sp<DrawMode>	drawMode(void) const								=0;

					/// @brief Push rendering mode onto stack
virtual void		pushDrawMode(sp<DrawMode> mode)							=0;

					/// @brief Pop rendering mode from stack
virtual sp<DrawMode>	popDrawMode(void)									=0;

					/// @brief Push transform matrices
virtual void		pushMatrixState(void)									=0;

					/// @brief Pop transform matrices
virtual void		popMatrixState(void)									=0;

					/// @brief push a new 4x4 matrix on a particular stack
virtual	void		pushMatrix(MatrixMode a_mode,Real a_values[16])			=0;
					/// @brief pop the top 4x4 matrix from a particular stack
virtual	void		popMatrix(MatrixMode a_mode)							=0;

					/// @brief currently, sets vertex array to zero in OpenGL
virtual	void		unbindVertexArray(void)									=0;

					/// @brief set additive lightening
virtual void		setBrightness(Real a_brightness)						=0;
					/// @brief get lightening
virtual Real		brightness(void)										=0;

					/// @brief set multiplicitive light scaling
virtual void		setContrast(Real a_contrast)							=0;
					/// @brief get light scaling
virtual Real		contrast(void)											=0;

					/** @brief Returns TRUE is display is local and
							rendered directly */
virtual	BWORD		isDirect(void) const									=0;

					/** @brief Set next DrawI in a chain

						If this DrawI is designed to pass drawing commands
						to another DrawI, such as for conversion or caching,
						this commands specifies that next component.

						Any implementation may ignore this request. */
virtual	void		setDrawChain(sp<DrawI> a_spDrawI)						=0;
virtual	sp<DrawI>	drawChain(void)											=0;

					/** @brief Impose a spatial translation

						If supported, this adjusts the output in an
						implementation-specific manner.

						Any implementation may ignore this request
						or just use part of the transform,
						such as translation only. */
virtual	void		setTransform(const SpatialTransform& a_rTransform)		=0;

					/** @brief Called once after each frame

						This shifts data access to freeze the current input
						buffer and prepare a new empty input buffer.

						The resulting output buffer is sent on to the
						next DrawI interface. */
virtual	void		flush(void)												=0;

					/** @brief Pass on current input data as is

						This sends the current input to the
						next DrawI interface without shifting buffers.
						The same input can still be written to afterwards. */
virtual	void		flushLive(void)											=0;

					/// @brief Reset the current input queue
virtual	void		clearInput(void)										=0;

					/// @brief Returns TRUE if the current input queue is empty
virtual	BWORD		empty(void) const										=0;

					/// @brief Set current view
virtual void		setView(sp<ViewI> spViewI)								=0;

					/// @brief Get current view
virtual sp<ViewI>	view(void) const										=0;

					/// @brief Get current font
virtual	sp<FontI>	font(void)												=0;

					/** @brief Select the GL-specific index/handle

						An index of 0 indicates no font. */
//~virtual	void	setFontBase(U32 index)									=0;
//~virtual	I32		fontHeight(I32* pAscent,I32* pDescent)					=0;
//~virtual	I32		pixelWidth(String string)								=0;

					/// @brief upscaling preference, such as for high DPI
virtual	Real		multiplication(void)									=0;

					/** @brief Draw points vertex[vertices].

						The array @em vertex is assumed to contain @em vertices
							elements.
						If @em multicolor, points are independently colored
							with color[vertices], else only color[0] is used.
						If @em color is NULL, no colors are specified,
							so the current color state is left in effect. */
virtual void		drawPoints(const SpatialVector *vertex,
							const SpatialVector *normal,U32 vertices,
							BWORD multicolor,const Color *color)			=0;
virtual void		drawPoints(const SpatialVector *vertex,
							const SpatialVector *normal,U32 vertices,
							BWORD multicolor,const Color *color,
							sp<DrawBufferI> spDrawBuffer)					=0;

					/** @brief Draw lines

						Uses vertices and colors as explained in drawPoints.

						If @em strip is DrawI::e_strip,
						vertices are contiguous with vertices-1 segments,
						else vertices are independent pairs
						with vertices/2 segments. */
virtual	void		drawLines(const SpatialVector *vertex,
							const SpatialVector *normal,U32 vertices,
							StripMode strip,BWORD multicolor,
							const Color *color)								=0;
virtual	void		drawLines(const SpatialVector *vertex,
							const SpatialVector *normal,U32 vertices,
							StripMode strip,
							BWORD multicolor,const Color *color,
							BWORD multiradius,const Real *radius,
							const Vector3i *element,U32 elementCount,
							sp<DrawBufferI> spDrawBuffer)					=0;

					/** @brief Draw one curve

						Uses vertices and colors as explained in drawPoints.

						This is similar to drawLines in strip mode and
						with a radius property. */
virtual	void		drawCurve(const SpatialVector *vertex,
							const SpatialVector *normal,
							const Real *radius,U32 vertices,
							BWORD multicolor,const Color *color)			=0;

					/** @brief Draw triangles

						Uses vertices and colors as explained in drawPoints.

						Normals are treated the same as vertices.
						If @em strip is DrawI::e_strip,
						vertices are tri-striped with
						vertices-2 triangles, else vertices are independent
						triplets with vertices/3 triangles. */
virtual void		drawTriangles(const SpatialVector *vertex,
							const SpatialVector *normal,
							const Vector2 *texture,U32 vertices,
							StripMode strip,BWORD multicolor,
							const Color* color)								=0;
virtual void		drawTriangles(const SpatialVector *vertex,
							const SpatialVector *normal,
							const Vector2 *texture,U32 vertices,
							StripMode strip,BWORD multicolor,
							const Color* color,
							const Array<I32>* vertexMap,
							const Array<I32>* hullPointMap,
							const Array<Vector4i>* hullFacePoint,
							sp<DrawBufferI> spDrawBuffer)					=0;

					/** @brief Draw rectangles

						Uses vertices and colors as explained in drawPoints.

						Rectangles are never stripped. */
virtual void		drawRectangles(const SpatialVector *vertex,U32 vertices,
							BWORD multicolor,const Color *color)			=0;

					/** @brief Draw DrawableI

						If color is NULL, the surface is expected to use its
						own color data. */
virtual void		draw(cp<DrawableI> cpDrawableI,const Color* color)		=0;
virtual void		draw(cp<DrawableI> cpDrawableI,const Color* color,
							sp<DrawBufferI> spDrawBuffer)					=0;

virtual void		drawTransformed(const SpatialTransform& transform,
							cp<DrawableI> cpDrawableI,const Color* color)	=0;
virtual void		drawTransformed(const SpatialTransform& transform,
							cp<DrawableI> cpDrawableI,const Color* color,
							sp<DrawBufferI> spDrawBuffer)					=0;

					/// @brief Draw 2D box
virtual void		drawBox(const Box2& box,const Color& color)				=0;

					/// @brief Draw 2D box with pixel adjustments
virtual void		drawBox(const Box2i& box,const Color& color)			=0;

					/// @brief Draw 3D box
virtual void		drawBox(const Box3& box,const Color& color)				=0;

					/** @brief Draw screen-aligned single color text

						Text is drawn at the given location, transformed and
						projected by the current state of the underlying
						graphics system. */
virtual	void		drawAlignedText(const SpatialVector& location,
							const String text,const Color& color)			=0;

virtual void		drawCircle(const SpatialTransform& transform,
							const SpatialVector *scale,
							const Color& color)								=0;

virtual void		drawSphere(const SpatialTransform& transform,
							const SpatialVector *scale,
							const Color& color)								=0;

					/** @brief Draw a transformed cylinder or cone

						The baseScale is a additional scale at the base of the
						cylinder.  For a cone, specify a baseScale of 0. */
virtual void		drawCylinder(const SpatialTransform& transform,
							const SpatialVector *scale,Real baseScale,
							const Color& color,U32 slices)					=0;

virtual	void		drawCylinder(const SpatialVector& location1,
							const SpatialVector& location2,
							Real radius1,Real radius2,
							const Color& color,U32 slices)					=0;

virtual void		drawCylinders(const SpatialTransform* transform,
							const SpatialVector* scale,const Real *baseScale,
							const U32* slices,U32 cylinders,
							BWORD multicolor,const Color* color)			=0;

virtual void		drawTransformedCylinders(
							const SpatialTransform& pretransform,
							const SpatialTransform* transform,
							const SpatialVector* scale,const Real *baseScale,
							const U32* slices,U32 cylinders,
							BWORD multicolor,const Color* color)			=0;

virtual	void		drawArc(const SpatialTransform& transform,
							const SpatialVector *scale,
							Real startangle,Real endangle,
							const Color& color)								=0;

virtual void		drawTransformedPoints(
							const SpatialTransform& transform,
							const SpatialVector *scale,
							const SpatialVector *vertex,
							const SpatialVector *normal,U32 vertices,
							BWORD multicolor,const Color *color)			=0;
virtual void		drawTransformedPoints(
							const SpatialTransform& transform,
							const SpatialVector *scale,
							const SpatialVector *vertex,
							const SpatialVector *normal,U32 vertices,
							BWORD multicolor,const Color *color,
							sp<DrawBufferI> spDrawBuffer)					=0;

virtual void		drawTransformedLines(
							const SpatialTransform& transform,
							const SpatialVector *scale,
							const SpatialVector *vertex,
							const SpatialVector *normal,
							U32 vertices,StripMode strip,BWORD multicolor,
							const Color *color)								=0;
virtual void		drawTransformedLines(
							const SpatialTransform& transform,
							const SpatialVector *scale,
							const SpatialVector *vertex,
							const SpatialVector *normal,
							U32 vertices,StripMode strip,
							BWORD multicolor,const Color *color,
							BWORD multiradius,const Real *radius,
							const Vector3i *element,U32 elementCount,
							sp<DrawBufferI> spDrawBuffer)					=0;

virtual void		drawTransformedCurve(
							const SpatialTransform& transform,
							const SpatialVector *scale,
							const SpatialVector *vertex,
							const SpatialVector *normal,
							const Real *radius,
							U32 vertices,BWORD multicolor,
							const Color *color)								=0;

virtual void		drawTransformedTriangles(
							const SpatialTransform& transform,
							const SpatialVector *scale,
							const SpatialVector *vertex,
							const SpatialVector *normal,
							const Vector2 *texture,U32 vertices,
							StripMode strip,BWORD multicolor,
							const Color *color)								=0;
virtual void		drawTransformedTriangles(
							const SpatialTransform& transform,
							const SpatialVector *scale,
							const SpatialVector *vertex,
							const SpatialVector *normal,
							const Vector2 *texture,U32 vertices,
							StripMode strip,BWORD multicolor,
							const Color* color,
							const Array<I32>* vertexMap,
							const Array<I32>* hullPointMap,
							const Array<Vector4i>* hullFacePoint,
							sp<DrawBufferI> spDrawBuffer)					=0;

virtual void		drawTransformedBox(
							const SpatialTransform& transform,
							const Box3& box,
							const Color& color) 							=0;

virtual void 		drawTransformedBoxes(
							const SpatialTransform* transform,
							const SpatialVector* scale,
							U32 boxes,
							BWORD multicolor,
							const Color* color)								=0;

virtual void		drawAxes(Real scale)									=0;
virtual void		drawTransformedAxes(const SpatialTransform& transform,
							Real scale)										=0;

virtual void		drawMarker(Real radius,const Color& color)				=0;
virtual void		drawTransformedMarker(const SpatialTransform& transform,
							Real radius,const Color& color)					=0;

virtual	void		drawRaster(sp<ImageI> spImageI,
							const SpatialVector& location)					=0;

virtual	sp<DrawBufferI>	createBuffer(void)									=0;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __draw_DrawI_h__ */
