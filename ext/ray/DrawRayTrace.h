/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __ray_DrawRayTrace_h__
#define __ray_DrawRayTrace_h__
namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Ray-tracing implementation for DrawI

	@ingroup ray
*//***************************************************************************/
class FE_DL_EXPORT DrawRayTrace: public Initialize<DrawRayTrace>,
	public DrawNull, virtual public HandlerI
{
	public:
	class Sphere
	{
		public:
							Sphere(void):
								m_radius(0.0f)
							{
								set(m_center);
								set(m_color);
							}
			SpatialVector	m_center;
			Color			m_color;
			Real			m_radius;
			Box2i			m_box;
	};
	class Cylinder
	{
		public:
							Cylinder(void):
								m_radius(0.0f)
							{
								set(m_base);
								set(m_axis);
								set(m_color);
								set(m_box);
							}
			SpatialVector	m_base;
			SpatialVector	m_axis;
			Color			m_color;
			Real			m_radius;
			Box2i			m_box;
	};
	class Disk
	{
		public:
							Disk(void):
								m_radius(0.0f)
							{
								set(m_center);
								set(m_facing);
								set(m_color);
								set(m_box);
							}
			SpatialVector	m_center;
			SpatialVector	m_facing;
			Color			m_color;
			Real			m_radius;
			Box2i			m_box;
	};
	class Drawable
	{
		public:
							Drawable(void):
								m_transformed(FALSE)
							{
								set(m_color);
								set(m_box);
							}
			cp<DrawableI>		m_cpDrawableI;
			SpatialTransform	m_transform;
			BWORD				m_transformed;
			Color				m_color;
			Box2i				m_box;
	};
	class Impact:
		public SurfaceBase::Impact,
		public CastableAs<Impact>
	{
		public:
	virtual	SpatialVector	origin(void) const		{ return m_origin; }

	virtual	SpatialVector	direction(void) const	{ return m_direction; }

	virtual	Real			distance(void) const	{ return m_distance; }

	virtual	SpatialVector	intersection(void)		{ return m_intersection; }

	virtual	SpatialVector	normal(void)			{ return m_normal; }

	virtual	Color			diffuse(void) const
							{	return m_diffuse; }
	virtual	void			setDiffuse(Color a_diffuse)
							{	m_diffuse=a_diffuse; }

			SpatialVector			m_origin;
			SpatialVector			m_direction;
			Color					m_diffuse;
			SpatialVector			m_intersection;
			SpatialVector			m_normal;
			Real					m_distance;
	};

					DrawRayTrace();
virtual				~DrawRayTrace();
		void		initialize(void);

					//* As HandlerI
virtual void		handle(Record& record);

					//* As DrawI
virtual sp<ViewI>	view(void) const
					{
						//* HACK ignore const
						const_cast<DrawRayTrace*>(this)->ensureDrawChain();
						return m_spDrawChain->view();
					}

virtual	void		setTransform(const SpatialTransform& a_rTransform)
					{	m_offset=a_rTransform.translation(); }

virtual	void		setDrawChain(sp<DrawI> a_spDrawI)
					{	m_spDrawChain=a_spDrawI; }

virtual	void		flush(void);
virtual	void		flushLive(void);
virtual	void		clearInput(void);

virtual	void		drawSphere(const SpatialTransform &transform,
						const SpatialVector *scale,const Color &color);

virtual void		drawCircle(const SpatialTransform& transform,
							const SpatialVector *scale,
							const Color& color)
					{	m_spDrawChain->drawCircle(transform,scale,color); }
virtual void		drawCylinder(const SpatialTransform& transform,
							const SpatialVector *scale,Real baseScale,
							const Color& color,U32 slices)
					{	m_spDrawChain->drawCylinder(transform,scale,baseScale,
								color,slices); }
virtual void		drawCylinder(const SpatialVector& location1,
							const SpatialVector& location2,
							Real radius1,Real radius2,
							const Color& color,U32 slices);
virtual	void		drawDisk(const SpatialVector& center,
							const SpatialVector& facing,Real radius,
							const Color& color,U32 slices);

					using DrawNull::draw;

virtual void		draw(cp<DrawableI> cpDrawableI,const Color* color);

					using DrawNull::drawTransformed;

virtual void		drawTransformed(const SpatialTransform& transform,
							cp<DrawableI> cpDrawableI,const Color* color);

virtual void		drawAxes(Real scale)
					{	m_spDrawChain->drawAxes(scale); }
virtual void		drawTransformedMarker(const SpatialTransform& transform,
							Real radius,const Color& color)
					{	m_spDrawChain->drawTransformedMarker(transform,radius,
							color); }
virtual	void		drawAlignedText(const SpatialVector& location,
							const String text,const Color& color)
					{	m_spDrawChain->drawAlignedText(location,text,color); }

					//* single ray testing
		void		clearObjects(void);
		BWORD		rayPick(const SpatialVector& origin,
							const SpatialVector& direction,
							Real maxDistance,
							sp<SurfaceI::ImpactI>& rspImpactI,
							cp<SurfaceI>& rcpSurfaceI);

	private:
		void		ensureDrawChain(void);
		void		drawInternal(cp<DrawableI> cpDrawableI,const Color* color,
							const SpatialTransform* pTransform=NULL);
		void		drawCropBox(const Box2i& box,const Color& color);

		void		runTracers(void);
		void		traceImage(U32 section,U32 sections);
		BWORD		traceRay(U32 depth,const Vector2i& coordinate,
							U8* element,Real contribution,BWORD from_shadow,
							const SpatialVector& origin,
							const SpatialVector& direction,
							Real maxDistance,BWORD probe,
							BWORD getImpact,sp<SurfaceI::ImpactI>& rspImpactI,
							cp<SurfaceI>& rcpSurfaceI);
		void		logElement(U8* element);

		U8*			pixel(U32 a_x,U32 a_y)
					{
						const U32 x=(a_x>=m_lenx)? m_lenx-1: a_x;
						const U32 y=(a_y>=m_leny)? m_leny-1: a_y;
						return &m_pPixelData[(y*m_lenx+x)*3];
					}

		BWORD		probeCheck(U32 x,U32 y)
					{	return m_mousePress && fabs(x-m_mouse[0])<0.01f &&
								fabs(y-m_mouse[1])<0.01f; }
		sp<ViewI>	view(void)
					{	return m_spDrawChain.isValid()? m_spDrawChain->view():
								sp<ViewI>(NULL); }

		void		stopThreads(void);

		WindowEvent					m_event;
		sp<DrawMode>				m_spWireframe;
		Array<Sphere>				m_spheres;
		Array<Cylinder>				m_cylinders;
		Array<Disk>					m_disks;
		Array<Drawable>				m_drawables;
		Array<SpatialVector>		m_showHitArray;
		Array<SpatialVector>		m_showNormalArray;
		Array<SpatialVector>		m_showReflectionArray;
		Array<SpatialVector>		m_showShadowArray;
		U8*							m_pPixelData;
		U32							m_sphereCount;
		U32							m_cylinderCount;
		U32							m_diskCount;
		U32							m_drawableCount;
		sp<DrawI>					m_spDrawChain;
		BWORD						m_autoDrawChain;
		sp<ImageI>					m_spImageI[2];
		U32							m_current;
		U32							m_lenx;
		U32							m_leny;
		SpatialVector				m_light;
		SpatialVector				m_mouse;
		BWORD						m_mousePress;
		SpatialVector				m_offset;

	class TracerWorker: public Thread::Functor
	{
		public:
							TracerWorker(sp< JobQueue<I32> > a_spJobQueue,
								U32 a_id,String a_stage):
								m_id(a_id),
								m_hpJobQueue(a_spJobQueue)					{}
	virtual					~TracerWorker(void)								{}

	virtual	void			operate(void);

			void			setObject(sp<Component> a_spDrawRayTrace)
							{	m_hpDrawRayTrace=a_spDrawRayTrace; }

		private:
			U32					m_id;
			hp< JobQueue<I32> > m_hpJobQueue;
			hp<DrawRayTrace>	m_hpDrawRayTrace;
	};

		sp< JobQueue<I32> >				m_spJobQueue;
		sp< Gang<TracerWorker,I32> >	m_spGang;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __ray_DrawRayTrace_h__ */
