/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __ray_h__
#define __ray_h__

#include "draw/draw.h"
#include "surface/surface.h"

#ifdef MODULE_ray
#define FE_RAY_PORT FE_DL_EXPORT
#else
#define FE_RAY_PORT FE_DL_IMPORT
#endif

namespace fe
{
namespace ext
{

Library* CreateRayLibrary(sp<Master>);

} /* namespace ext */
} /* namespace fe */

#endif /* __ray_h__ */
