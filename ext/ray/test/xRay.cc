/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "viewer/viewer.h"

using namespace fe;
using namespace fe::ext;

class MyDraw: virtual public HandlerI
{
	public:
	class Sphere
	{
		public:
							Sphere(void):
								m_radius(0.0f)
							{
								set(m_center);
								set(m_color);
							}
			SpatialVector	m_center;
			F32				m_radius;
			Color			m_color;
	};
	class Cylinder
	{
		public:
							Cylinder(void):
								m_radius(0.0f)
							{
								set(m_base);
								set(m_end);
								set(m_color);
							}
			SpatialVector	m_base;
			SpatialVector	m_end;
			F32				m_radius;
			Color			m_color;
	};

				MyDraw(sp<DrawI> spDrawI,sp<Registry> spRegistry):
					m_delta(0.1f),
					m_limit(3.0f),
					m_orbit(3.0f),
					m_time(0.0f),
					m_timestep(0.07f)
				{
					setIdentity(m_transform);

					m_spheres.resize(3);
					set(m_spheres[0].m_center,2.0f,0.0f,0.0f);
					set(m_spheres[0].m_color,1.0f,0.0f,0.0f,1.0f);
					m_spheres[0].m_radius=1.0f;
					set(m_spheres[1].m_center,0.0f,0.0f,-2.0f);
					set(m_spheres[1].m_color,0.0f,1.0f,0.0f,1.0f);
					m_spheres[1].m_radius=1.0f;
					set(m_spheres[2].m_center,0.0f,0.0f,2.0f);
					set(m_spheres[2].m_color,0.0f,0.0f,1.0f,1.0f);
					m_spheres[2].m_radius=1.0f;

					m_cylinders.resize(2);
					set(m_cylinders[0].m_color,1.0f,1.0f,0.0f,1.0f);
					m_cylinders[0].m_radius=0.5f;
					set(m_cylinders[1].m_color,1.0f,0.0f,1.0f,1.0f);
					m_cylinders[1].m_radius=0.5f;

					setName("MyDraw");

					m_spDrawI=spDrawI;
				}

virtual void	handle(Record& render)
				{
					if(!m_asViewer.scope().isValid())
					{
						m_asViewer.bind(render.layout()->scope());
					}

					const I32& rLayer=m_asViewer.viewer_layer(render);
#if FALSE
					if(rLayer==1)
					{
						m_spDrawI->drawAxes(1.0f);
						return;
					}
#endif
					if(rLayer!=2)
					{
						return;
					}

					m_spheres[0].m_center[1]+=m_delta;
					if(m_spheres[0].m_center[1]>m_limit)
					{
						m_delta= -0.1f;
					}
					else if(m_spheres[0].m_center[1]< -m_limit)
					{
						m_delta= 0.1f;
					}

					m_time+=m_timestep;
					m_spheres[1].m_center[0]=m_orbit*sinf(m_time);
					m_spheres[1].m_center[1]=m_orbit*cosf(m_time);

					for(U32 m=0;m<3;m++)
					{
						setTranslation(m_transform,m_spheres[m].m_center);
						const F32 radius=m_spheres[m].m_radius;
						SpatialVector scale(radius,radius,radius);
						m_spDrawI->drawSphere(m_transform,&scale,
								m_spheres[m].m_color);
					}

					m_cylinders[0].m_base=m_spheres[0].m_center;
					m_cylinders[0].m_end=m_spheres[1].m_center;
					m_cylinders[1].m_base=m_spheres[0].m_center;
					m_cylinders[1].m_end=m_spheres[2].m_center;

					for(U32 m=0;m<2;m++)
					{
						const F32 radius=m_cylinders[m].m_radius;
						m_spDrawI->drawCylinder(m_cylinders[m].m_base,
								m_cylinders[m].m_end,
								radius,radius,
								m_cylinders[m].m_color,6);
					}
				}

	private:

		AsViewer				m_asViewer;
		sp<DrawI>				m_spDrawI;
		SpatialTransform		m_transform;

		Array<Sphere>			m_spheres;
		Array<Cylinder>			m_cylinders;
		F32						m_delta;
		F32						m_limit;
		F32						m_orbit;
		F32						m_time;
		F32						m_timestep;
};

int main(int argc,char** argv)
{
	if(argc>3)
	{
		feLog("%s: too many arguments\nTry %s [image DL] [frames]\n",
				argv[0],argv[0]);
		return 1;
	}

	UNIT_START();

	BWORD complete=FALSE;
	U32 frames=0;
	const char* libname="fexImageDL";
	if(argc>1)
	{
		frames=atoi(argv[argc-1])+1;
		if(argc>2 || frames<2)
		{
			libname=argv[1];
			if(argc==2)
			{
				frames=0;
			}
		}
	}

	try
	{
		sp<Master> spMaster(new Master);
		sp<Registry> spRegistry=spMaster->registry();

		Result result=spRegistry->manage("fexViewerDL");
		UNIT_TEST(successful(result));
		result=spRegistry->manage("fexRayDL");
		UNIT_TEST(successful(result));
		result=spRegistry->manage(libname);
		UNIT_TEST(successful(result));

		{
			sp<QuickViewerI> spQuickViewerI(
					spRegistry->create("QuickViewerI"));
			if(!spQuickViewerI.isValid())
			{
				feX(argv[0], "couldn't create components");
			}

			spQuickViewerI->getWindowI()->setSize(900,800);

			spQuickViewerI->open();

			sp<DrawI> spDrawI=spRegistry->create("*.DrawRayTrace");
			UNIT_TEST(spDrawI.isValid());
			spQuickViewerI->bind(spDrawI);

			sp<MyDraw> spMyDraw(new MyDraw(spDrawI,spRegistry));

			spQuickViewerI->insertDrawHandler(spMyDraw);
			spQuickViewerI->run(frames);
		}

		complete=TRUE;
	}
	catch(Exception &e) { e.log(); }
	catch(...)			{ feLog("uncaught exception\n"); }

	UNIT_TEST(complete);
	UNIT_TEST(!Counted::trackerCount());
	UNIT_TRACK(6);
	UNIT_RETURN();
}
