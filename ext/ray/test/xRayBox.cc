/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <ray/ray.pmh>

#include "viewer/viewer.h"

using namespace fe;
using namespace fe::ext;

class MyDraw: virtual public HandlerI
{
	public:
	class Node
	{
		public:
							Node(void):
								m_radius(0.0f)
							{
								set(m_center);
								set(m_color);
							}
			SpatialVector	m_center;
			SpatialVector	m_velocity;
			F32				m_radius;
			Color			m_color;
	};

				MyDraw(sp<Registry> spRegistry,sp<DrawI> spDrawI):
					m_delta(0.01f),
					m_limit(5.0f),
					m_spin(0.2f),
					m_orbit(0.1f),
					m_time(0.0f),
					m_timestep(0.07f)
				{
					setName("MyDraw");
					setIdentity(m_transform);

					m_spDrawI=spDrawI;
					m_spDrawRayTrace=
							spRegistry->create("*.DrawRayTrace");

					m_nodes.resize(8);
					for(U32 m=0;m<m_nodes.size();m++)
					{
						set(m_nodes[m].m_color,
								0.5f+0.5f*(m%2),
								0.4f+0.3f*(m%3),
								0.1f+0.3f*(m%4),
								1.0f);
						m_nodes[m].m_center=m_limit*m_nodes[m].m_color;
						set(m_nodes[m].m_velocity,
							m_nodes[m].m_center[1],
							m_nodes[m].m_center[2],
							m_nodes[m].m_center[0]);
						m_nodes[m].m_radius=1.0f;
					}
				}

virtual void	handle(Record& render)
				{
					if(!m_asViewer.scope().isValid())
					{
						m_asViewer.bind(render.layout()->scope());
					}
					if(!m_asViewer.viewer_spatial.check(render) ||
							!m_asViewer.viewer_layer.check(render) ||
							!m_asViewer.viewer_layer(render))
					{
						return;
					}

					if(!m_asViewer.viewer_spatial(render))
					{
						if(m_target[2]>-1e3 && m_spViewI.isValid())
						{
							const Color yellow(1.0f,1.0f,0.0f,1.0f);
							const SpatialVector scale(10.0f,10.0f,10.0f);
							SpatialTransform transform;
							setIdentity(transform);
							SpatialVector center=m_spViewI->project(
									m_target,ViewI::e_perspective);
							center[2]=1.0f;
							translate(transform,center);

							m_spDrawI->drawCircle(transform,&scale,yellow);
						}
						return;
					}

					m_spViewI=m_spDrawI->view();

					for(U32 m=0;m<m_nodes.size();m++)
					{
						m_nodes[m].m_center+=m_delta*m_nodes[m].m_velocity;
						for(U32 n=0;n<3;n++)
						{
							if(m_nodes[m].m_center[n]>m_limit)
							{
								m_nodes[m].m_velocity[n]*= -1.0;
								m_nodes[m].m_center[n]=2.0*m_limit-
									m_nodes[m].m_center[n];
							}
							if(m_nodes[m].m_center[n]< -m_limit)
							{
								m_nodes[m].m_velocity[n]*= -1.0;
								m_nodes[m].m_center[n]= -2.0*m_limit-
									m_nodes[m].m_center[n];
							}
						}
					}

					m_time+=m_timestep;

					const SpatialVector origin(-10.0,0.0,3.0);
					const SpatialVector direction(1.0,
							m_orbit*cosf(m_spin*m_time),
							m_orbit*sinf(m_spin*m_time));
					const Real maxDistance=20.0;

					set(m_target,0.0,0.0,-1e6);
					SpatialVector target=origin+maxDistance*direction;
					sp<SurfaceI::ImpactI> spImpactI;
					cp<SurfaceI> cpSurfaceI;

					for(U32 pass=0;pass<2;pass++)
					{
						for(U32 m=0;m<m_nodes.size();m++)
						{
							sp<DrawI> spPassDrawI;
							if(pass)
							{
								spPassDrawI=m_spDrawI;
							}
							else
							{
								spPassDrawI=m_spDrawRayTrace;
							}

							setTranslation(m_transform,m_nodes[m].m_center);
							const F32 radius=m_nodes[m].m_radius;
							SpatialVector scale(radius,radius,radius);
//							feLog("%d\n%s\n%s\n",m,c_print(m_transform),
//									c_print(scale),c_print(m_nodes[m].m_color));
							spPassDrawI->drawSphere(m_transform,&scale,
									m_nodes[m].m_color);
						}

						if(!pass)
						{

							BWORD hit=m_spDrawRayTrace->rayPick(
									origin,direction,maxDistance,
									spImpactI,cpSurfaceI);
							m_spDrawRayTrace->clearObjects();

							if(hit)
							{
								target=spImpactI->intersection();
								m_target=target;
//								feLog("hit %s\n",c_print(target));
							}
						}
					}

					const Color red(1.0,0.0,0.0,1.0);
					const U32 slices=4;
					const F32 distance=magnitude(target-origin);
					const F32 radius=2e-3*distance;
					m_spDrawI->drawCylinder(origin,target,
							0.0,radius,red,slices);

					m_spDrawI->drawAxes(5.0f);
				}

	private:

		AsViewer				m_asViewer;
		sp<ViewI>				m_spViewI;
		sp<DrawI>				m_spDrawI;
		sp<DrawRayTrace>		m_spDrawRayTrace;
		SpatialTransform		m_transform;
		SpatialVector			m_target;

		Array<Node>				m_nodes;
		F32						m_delta;
		F32						m_limit;
		F32						m_spin;
		F32						m_orbit;
		F32						m_time;
		F32						m_timestep;
};

int main(int argc,char** argv)
{
	if(argc>2)
	{
		feLog("%s: too many arguments\nTry %s [frames]\n",
				argv[0],argv[0]);
		return 1;
	}

	UNIT_START();

	BWORD complete=FALSE;
	U32 frames=0;
	if(argc>1)
	{
		frames=atoi(argv[argc-1])+1;
	}

	try
	{
		sp<Master> spMaster(new Master);
		sp<Registry> spRegistry=spMaster->registry();

		Result result=spRegistry->manage("fexViewerDL");
		UNIT_TEST(successful(result));
		result=spRegistry->manage("fexRayDL");
		UNIT_TEST(successful(result));

		{
			sp<QuickViewerI> spQuickViewerI(
					spRegistry->create("QuickViewerI"));
			if(!spQuickViewerI.isValid())
			{
				feX(argv[0], "couldn't create components");
			}

			spQuickViewerI->getWindowI()->setSize(900,800);

			spQuickViewerI->open();

			sp<DrawI> spDrawI=spQuickViewerI->getDrawI();
			sp<MyDraw> spMyDraw(new MyDraw(spRegistry,spDrawI));

			spQuickViewerI->insertDrawHandler(spMyDraw);
			spQuickViewerI->run(frames);
		}

		complete=TRUE;
	}
	catch(Exception &e) { e.log(); }
	catch(...)			{ feLog("uncaught exception\n"); }

	UNIT_TEST(complete);
	UNIT_TEST(!Counted::trackerCount());
	UNIT_TRACK(4);
	UNIT_RETURN();
}

