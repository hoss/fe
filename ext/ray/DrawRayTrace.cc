/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <ray/ray.pmh>

#define	FE_TRACER_INPUT_DEBUG		FALSE
#define	FE_TRACER_OUTPUT_DEBUG		FALSE

#define	FE_TRACER_NORMAL_SCALE		3.0f

#define	FE_TRACER_MOUSE_RAY			FALSE
#define	FE_TRACER_SHOW_BOXES		FALSE
#define	FE_TRACER_SHOW_AXES			FALSE
#define	FE_TRACER_SHOW_LIGHT		TRUE

#define	FE_TRACER_REFLECTION		TRUE	// including specular
#define	FE_TRACER_REFLECTION_MAX	4		// (-1)
#define	FE_TRACER_SHADOW			TRUE

#define	FE_TRACER_MT_DEBUG			FALSE
#define FE_TRACER_MT_SECTIONS		16
#define FE_TRACER_MT_THREADS		(FE_TRACER_MT_SECTIONS-1) // added to main

#define	FE_TRACER_MT_ACTION_QUIT	0x00010000
#define	FE_TRACER_MT_ACTION_SOLVE	0x00020000
#define	FE_TRACER_MT_ACTION_MASK	0x7fff0000
#define	FE_TRACER_MT_INDEX_MASK		0x0000ffff

namespace fe
{
namespace ext
{

DrawRayTrace::DrawRayTrace(void):
	m_sphereCount(0),
	m_cylinderCount(0),
	m_diskCount(0),
	m_drawableCount(0),
	m_spDrawChain(NULL),
	m_autoDrawChain(FALSE),
	m_current(0),
	m_lenx(1),
	m_leny(1),
	m_mousePress(FALSE)
{
	set(m_light,-5.0f,5.0f,5.0f);
	set(m_mouse,250.0f,200.0f,-1.0f);
	set(m_offset);
}

DrawRayTrace::~DrawRayTrace(void)
{
	stopThreads();
}

void DrawRayTrace::stopThreads(void)
{
	if(FE_TRACER_MT_SECTIONS>1 && m_spGang.isValid())
	{
		//* complete threads
		for(U32 m=0;m<FE_TRACER_MT_THREADS;m++)
		{
			m_spGang->post(FE_TRACER_MT_ACTION_QUIT);
		}
	}

	if(m_spGang.isValid())
	{
		m_spGang->finish();
		m_spGang=NULL;
	}
}

void DrawRayTrace::initialize(void)
{
//	Mutex::confirm("fexBoostThread");
	Mutex::confirm();

	m_spWireframe=new DrawMode();
	m_spWireframe->setDrawStyle(DrawMode::e_wireframe);
	m_spWireframe->setLineWidth(1.5f);

	m_spImageI[0]=registry()->create("ImageI.ImageRaw");
	m_spImageI[1]=registry()->create("ImageI.ImageRaw");
	if(!m_spImageI[0].isValid() || !m_spImageI[1].isValid())
	{
		feX("MyDraw::MyDraw", "couldn't create components");
	}

	for(U32 m=0;m<2;m++)
	{
		m_spImageI[m]->createSelect();
		m_spImageI[m]->setFormat(ImageI::e_rgb);
		m_spImageI[m]->resize(m_lenx,m_leny,1);
	}
}

void DrawRayTrace::ensureDrawChain(void)
{
	if(m_spDrawChain.isNull())
	{
		try
		{
			String &draw_hint=registry()->master()->catalog()->catalog<String>(
					"hint:DrawI", "");
			if(draw_hint!="" && !draw_hint.dotMatch("*.DrawRayTrace"))
			{
				feLog("DrawRayTrace::initialize using \"%s\"\n",
						draw_hint.c_str());
				m_spDrawChain=registry()->create(draw_hint);
			}
			if(!m_spDrawChain.isValid())
			{
				feLog("DrawRayTrace::initialize using threaded draw\n");
				m_spDrawChain=registry()->create("*.DrawOpenGL");
			}
		}
		catch(fe::Exception &e)
		{
			if(e.getResult()!=e_cannotCreate)
			{
				throw;
			}
		}
		if(m_spDrawChain.isValid())
		{
			m_autoDrawChain=TRUE;
		}
	}

//	if(m_spDrawChain.isValid())
//	{
//		m_spDrawChain->setDrawMode(m_spWireframe);
//	}
}

void DrawRayTrace::handle(Record &record)
{
	m_event.bind(record);
	if(!m_event.eventSource.check())
	{
		return;
	}

	if(m_event.isUnmodified(WindowEvent::e_sourceMouseButton,
			WindowEvent::e_itemLeft,WindowEvent::e_statePress))
	{
		I32 mx,my;
		m_event.getMousePosition(&mx,&my);

		set(m_mouse,mx,my,-1.0f);
		m_mousePress=TRUE;
	}
}


void DrawRayTrace::drawCropBox(const Box2i& box,const Color& color)
{
	// overlay screen boxes
	ViewI::Projection projection=view()->projection();
	view()->use(ViewI::e_ortho);
	const Vector<2,I32>& size=box.size();
	const I32 x1=box[0];
	const I32 x2=box[0]+size[0];
	const I32 y1=box[1];
	const I32 y2=box[1]+size[1];

	SpatialVector vertex[5];
	set(vertex[0],x1,y1);
	set(vertex[1],x2,y1);
	set(vertex[2],x2,y2);
	set(vertex[3],x1,y2);
	set(vertex[4],x1,y1);

	m_spDrawChain->pushDrawMode(m_spWireframe);
	m_spDrawChain->drawLines(vertex,NULL,5,e_strip,false,&color);
	m_spDrawChain->popDrawMode();

	view()->use(projection);
}

void DrawRayTrace::drawSphere(const SpatialTransform &transform,
		const SpatialVector *scale,const Color &color)
{
	const U32 index=m_sphereCount++;
	if(index>=m_spheres.size())
	{
		m_spheres.resize(m_sphereCount);
	}
	Sphere& sphere=m_spheres[index];
	sphere.m_center=transform.translation();
	sphere.m_color=color;
	sphere.m_radius=scale? (*scale)[0]: 1.0f;

	sp<ViewI> spViewI=view();
	if(spViewI.isValid())
	{
		SpatialVector screenCenter=spViewI->project(sphere.m_center,
				ViewI::e_perspective);
		Real screenRadius=1.0+1.0*spViewI->pixelSize(sphere.m_center,
				sphere.m_radius);
		set(sphere.m_box,
				Vector<2,I32>(screenCenter[0]-screenRadius,
				screenCenter[1]-screenRadius),
				Vector<2,I32>(screenRadius*2.0f,screenRadius*2.0f));

#if FE_TRACER_SHOW_BOXES
		drawCropBox(sphere.m_box,sphere.m_color);
#endif

#if FE_TRACER_INPUT_DEBUG
		feLog("  sphere %d l %s s %.6G c %s\n",index,c_print(sphere.m_center),
				sphere.m_radius,c_print(sphere.m_color));
		feLog("    screen %s radius %.6G\n",
				c_print(screenCenter),screenRadius);
		feLog("    box %d %d %d %d\n",sphere.m_box[0],sphere.m_box[1],
				width(sphere.m_box),height(sphere.m_box));
#endif
	}
}

void DrawRayTrace::drawCylinder(const SpatialVector& location1,
		const SpatialVector& location2,
		Real radius1,Real radius2,
		const Color& color,U32 slices)
{
#if FE_TRACER_INPUT_DEBUG
	feLog("  cylinder l %s  %s s %.6G %.6G c %s sl %d\n",
			c_print(location1),c_print(location2),
			radius1,radius2,c_print(color),slices);
#endif

	const U32 index=m_cylinderCount++;
	if(index>=m_cylinders.size())
	{
		m_cylinders.resize(m_cylinderCount);
	}
	Cylinder& cylinder=m_cylinders[index];
	cylinder.m_base=location1;
	cylinder.m_axis=location2-location1;
	cylinder.m_color=color;
	cylinder.m_radius=radius1;

	SpatialVector center=cylinder.m_base+0.5f*cylinder.m_axis;
	Real halfLength=0.5f*magnitude(cylinder.m_axis);
	Real extent=sqrtf(radius1*radius1+halfLength*halfLength);

	sp<ViewI> spViewI=view();
	if(spViewI.isValid())
	{
		SpatialVector screenCenter=spViewI->project(center,
				ViewI::e_perspective);
		Real screenRadius=1.0+1.0f*spViewI->pixelSize(center,extent);
		set(cylinder.m_box,
				Vector<2,I32>(screenCenter[0]-screenRadius,
				screenCenter[1]-screenRadius),
				Vector<2,I32>(screenRadius*2.0f,screenRadius*2.0f));

#if FE_TRACER_SHOW_BOXES
		drawCropBox(cylinder.m_box,cylinder.m_color);
#endif
	}
}

void DrawRayTrace::drawDisk(const SpatialVector& center,
		const SpatialVector& facing,Real radius,
		const Color& color,U32 slices)
{
#if FE_TRACER_INPUT_DEBUG
	feLog("  disk l %s f %s r %.6G c %s sl %d\n",
			c_print(center),c_print(facing),
			radius,c_print(color),slices);
#endif

	const U32 index=m_diskCount++;
	if(index>=m_disks.size())
	{
		m_disks.resize(m_diskCount);
	}
	Disk& disk=m_disks[index];
	disk.m_center=center;
	disk.m_facing=facing;
	disk.m_color=color;
	disk.m_radius=radius;

	sp<ViewI> spViewI=view();
	if(spViewI.isValid())
	{
		SpatialVector screenCenter=spViewI->project(disk.m_center,
				ViewI::e_perspective);
		Real screenRadius=1.0+1.0f*spViewI->pixelSize(disk.m_center,
				disk.m_radius);
		set(disk.m_box,
				Vector<2,I32>(screenCenter[0]-screenRadius,
				screenCenter[1]-screenRadius),
				Vector<2,I32>(screenRadius*2.0f,screenRadius*2.0f));

#if FE_TRACER_SHOW_BOXES
		drawCropBox(disk.m_box,disk.m_color);
#endif
	}
}

void DrawRayTrace::draw(cp<DrawableI> cpDrawableI,const Color* color)
{
	drawInternal(cpDrawableI,color);
}

void DrawRayTrace::drawTransformed(const SpatialTransform& transform,
	cp<DrawableI> cpDrawableI,const Color* color)
{
	drawInternal(cpDrawableI,color,&transform);
}

void DrawRayTrace::drawInternal(cp<DrawableI> cpDrawableI,const Color* color,
	const SpatialTransform* pTransform)
{
#if FE_TRACER_INPUT_DEBUG
	feLog("  drawable color %p xform %p\n",color,pTransform);
#endif

	if(cpDrawableI.isNull())
	{
		feX("DrawRayTrace::drawInternal","Drawable is NULL");
	}

	const Color white(1.0f,1.0f,1.0f,1.0f);
	const U32 index=m_drawableCount++;
	if(index>=m_drawables.size())
	{
		m_drawables.resize(m_drawableCount);
	}
	Drawable& drawable=m_drawables[index];

	drawable.m_cpDrawableI=cpDrawableI;
	drawable.m_cpDrawableI.protect();
	drawable.m_color=white;
	drawable.m_transformed=(pTransform!=NULL);
	if(drawable.m_transformed)
	{
		drawable.m_transform= *pTransform;
	}

	SpatialVector center(0.0f,0.0f,0.0f);
	Real radius=1.0f;
	cp<SurfaceI> cpSurfaceI=drawable.m_cpDrawableI;
	if(cpSurfaceI.isValid())
	{
		center=cpSurfaceI->center();
		radius=cpSurfaceI->radius();
		drawable.m_color=cpSurfaceI->diffuse();
	}
	if(color)
	{
		drawable.m_color= *color;
	}

	sp<ViewI> spViewI=view();
	if(spViewI.isValid())
	{
		const Box2i& viewport=spViewI->viewport();

		const SpatialVector screenCenter=spViewI->project(center,
				ViewI::e_perspective);
		const Real screenRadius=1.0+1.0f*spViewI->pixelSize(center,radius);
		set(drawable.m_box,
				Vector<2,I32>(
				screenCenter[0]-screenRadius-viewport[0],
				screenCenter[1]-screenRadius-viewport[1]),
				Vector<2,I32>(
				screenRadius*2.0f,
				screenRadius*2.0f));

#if FE_TRACER_SHOW_BOXES
		drawCropBox(drawable.m_box,drawable.m_color);
#endif
	}
}

BWORD DrawRayTrace::rayPick(const SpatialVector& origin,
	const SpatialVector& direction,Real maxDistance,
	sp<SurfaceI::ImpactI>& rspImpactI,
	cp<SurfaceI>& rcpSurfaceI)
{
	Vector2i coordinate(0,0);
	U8 element[3];

	SpatialVector unitDir=unit(direction);

	BWORD hit=traceRay(1,coordinate,element,1.0f,FALSE,
			origin,unitDir,maxDistance,FALSE,
			TRUE,rspImpactI,rcpSurfaceI);

	return hit;
}

void DrawRayTrace::clearObjects(void)
{
	m_sphereCount=0;
	m_cylinderCount=0;
	m_diskCount=0;
	m_drawableCount=0;
}

void DrawRayTrace::clearInput(void)
{
	clearObjects();
	m_current=!m_current;
}

void DrawRayTrace::flush(void)
{
	ensureDrawChain();

	flushLive();
	clearObjects();

	//* NOTE someone needs to flush m_spDrawChain
	if(m_autoDrawChain)
	{
		m_spDrawChain->flush();
	}
}

void DrawRayTrace::flushLive(void)
{
	const Color white(1.0f,1.0f,1.0f,1.0f);
	const Color red(1.0f,0.0f,0.0f,1.0f);
	const Color green(0.0f,1.0f,0.0f,1.0f);
	const Color blue(0.0f,0.0f,1.0f,1.0f);
	const Color cyan(0.0f,1.0f,1.0f,1.0f);
	const Color magenta(1.0f,0.0f,1.0f,1.0f);
	const Color yellow(1.0f,1.0f,0.0f,1.0f);

	const SpatialVector disk_location(0.0f,0.0f,-4.0f);
	const SpatialVector disk_facing(0.0f,0.0f,1.0f);
	drawDisk(disk_location,disk_facing,6.0f,white,0);

#if	FE_TRACER_SHOW_AXES
	const SpatialVector origin(0.0f,0.0f,0.0f);
	const SpatialVector unitX(1.0f,0.0f,0.0f);
	const SpatialVector unitY(0.0f,1.0f,0.0f);
	const SpatialVector unitZ(0.0f,0.0f,1.0f);
	const Real radius=0.2f;
	drawCylinder(origin,unitX,radius,radius,red,0);
	drawCylinder(origin,unitY,radius,radius,green,0);
	drawCylinder(origin,unitZ,radius,radius,blue,0);
#endif

	sp<ViewI> spViewI=view();
	if(spViewI.isValid())
	{
		const Box2i& viewport=spViewI->viewport();
		const I32 vx=width(viewport);
		const I32 vy=height(viewport);
		if(I32(m_lenx)!=vx || I32(m_leny)!=vy)
		{
			m_lenx=vx;
			m_leny=vy;

			// TODO shouldn't be changing rendering image
			m_spImageI[0]->resize(m_lenx,m_leny,1);
			m_spImageI[1]->resize(m_lenx,m_leny,1);
		}
	}

	if(spViewI.isValid() && m_autoDrawChain)
	{
		sp<CameraI> spCamera=spViewI->camera();
		spCamera->setRasterSpace(TRUE);
		spViewI->use(ViewI::e_ortho);
	}

	m_pPixelData=new U8[m_lenx*m_leny*3];

	memset(m_pPixelData,0,m_lenx*m_leny*3);

	if(m_mousePress)
	{
		m_showHitArray.resize(0);
		m_showNormalArray.resize(0);
		m_showReflectionArray.resize(0);
		m_showShadowArray.resize(0);
	}

	runTracers();

	m_mousePress=FALSE;

	U8* element=pixel(m_mouse[0],m_mouse[1]);
	element[0]=255.0f;
	element[1]=80.0f;
	element[2]=80.0f;

	m_spImageI[m_current]->replaceRegion(
			0,0,0,m_lenx,m_leny,1,m_pPixelData);
	delete[] m_pPixelData;

	const U32 imageHeight=m_spImageI[m_current]->height();
	const SpatialVector location=SpatialVector(0.0,imageHeight-1)+m_offset;

#if	FE_TRACER_OUTPUT_DEBUG
	feLog("DrawRayTrace::flushLive drawRaster %d %d offset %s at %s \n",
			m_spImageI[m_current]->width(),
			m_spImageI[m_current]->height(),
			c_print(m_offset),c_print(location));
#endif

	m_spDrawChain->drawRaster(m_spImageI[m_current],location);

#if FE_TRACER_MOUSE_RAY
	const SpatialVector hit_scale(5.0f,5.0f,5.0f);
	m_spDrawChain->pushDrawMode(m_spWireframe);
	U32 size=m_showHitArray.size();
	for(U32 m=0;m<size;m++)
	{
		SpatialTransform transform;
		setIdentity(transform);
		SpatialVector center=view()->project(m_showHitArray[m],
				ViewI::e_perspective);
		center[2]=1.0f;
		translate(transform,center);

		m_spDrawChain->drawCircle(transform,&hit_scale,cyan);
	}
	size=m_showNormalArray.size();
	for(U32 m=0;m<size;m+=2)
	{
		SpatialVector vertex[2];
		vertex[0]=view()->project(m_showNormalArray[m],
				ViewI::e_perspective);
		vertex[1]=view()->project(m_showNormalArray[m+1],
				ViewI::e_perspective);

		m_spDrawChain->drawLines(vertex,NULL,2,DrawI::e_discrete,FALSE,&red);
	}
	size=m_showReflectionArray.size();
	for(U32 m=0;m<size;m+=2)
	{
		if(!m)
		{
			SpatialVector back=
					m_showReflectionArray[m]-m_showReflectionArray[m+1];
			Real length=magnitude(back);
			if(length>FE_TRACER_NORMAL_SCALE)
			{
				back*=FE_TRACER_NORMAL_SCALE/length;
				m_showReflectionArray[m]=m_showReflectionArray[m+1]+back;
			}
		}

		SpatialVector vertex[2];
		vertex[0]=view()->project(m_showReflectionArray[m],
				ViewI::e_perspective);
		vertex[1]=view()->project(m_showReflectionArray[m+1],
				ViewI::e_perspective);

		m_spDrawChain->drawLines(vertex,NULL,2,DrawI::e_discrete,FALSE,
				m? &blue: &magenta);
	}
	size=m_showShadowArray.size();
	for(U32 m=0;m<size;m+=2)
	{
		SpatialVector vertex[2];
		vertex[0]=view()->project(m_showShadowArray[m],
				ViewI::e_perspective);
		vertex[1]=view()->project(m_showShadowArray[m+1],
				ViewI::e_perspective);

		m_spDrawChain->drawLines(vertex,NULL,2,DrawI::e_discrete,FALSE,&green);
	}
	m_spDrawChain->popDrawMode();
#endif

#if FE_TRACER_SHOW_LIGHT
	const SpatialVector scale(5.0f,5.0f,5.0f);
	SpatialTransform transform;
	setIdentity(transform);
	SpatialVector center=view()->project(m_light,ViewI::e_perspective);
	center[2]=1.0f;
	translate(transform,center+m_offset);

	m_spDrawChain->pushDrawMode(m_spWireframe);
	m_spDrawChain->drawCircle(transform,&scale,yellow);
	m_spDrawChain->popDrawMode();
#endif

	if(spViewI.isValid() && m_autoDrawChain)
	{
		sp<CameraI> spCamera=spViewI->camera();
		spCamera->setRasterSpace(FALSE);
		spViewI->use(ViewI::e_perspective);
	}

#if FALSE
	// overlay wire spheres
	for(U32 m=0;m<m_sphereCount;m++)
	{
		Sphere& sphere=m_spheres[m];
		setTranslation(m_transform,sphere.m_center);
		const Real radius=sphere.m_radius;
		SpatialVector scale(radius,radius,radius);
		m_spDrawChain->drawSphere(m_transform,&scale,sphere.m_color);

//		feLog("sphere %d l %s s %.6G c %s\n",m,c_print(sphere.m_center),
//				sphere.m_radius,c_print(sphere.m_color));
	}
#endif
}

void DrawRayTrace::traceImage(U32 section,U32 sections)
{
	// tweak
	const U32 step=4;
	const U32 antialias=1;
	const Real margin=500.0f;
	const BWORD debug=FALSE;
	const BWORD log=FALSE;
	const U32 miss=debug? 255: 0;

	sp<ViewI> spViewI=view();
	if(!spViewI.isValid())
	{
		return;
	}

	sp<CameraI> spCamera=spViewI->camera();
	const SpatialTransform& cameraMatrix=spCamera->cameraMatrix();

	SpatialTransform cameraTransform;
	invert(cameraTransform,cameraMatrix);

	const Real fovy=spCamera->fov()[1];

	const Real antiscale=1.0f/(antialias? Real(antialias): 1.0f);
	const Real antioffset=0.5f*antiscale;
	const Real antiscale2=1.0f/Real(antialias*antialias+1);
	const SpatialVector origin= cameraTransform.translation();
	const SpatialVector camera_right= cameraTransform.direction();
	const SpatialVector camera_up= -cameraTransform.left();
	const SpatialVector camera_dir= -cameraTransform.up();
	const Real scale=1.07f*sinf(fovy*fe::degToRad)/m_leny; // align to OpenGL
	const SpatialVector scalex=scale*camera_right;
	const SpatialVector scaley=scale*camera_up;
	const SpatialVector target=origin+camera_dir-
			scalex*(m_lenx*0.5f)-scaley*(m_leny*0.5f);
	SpatialVector end;
	sp<SurfaceI::ImpactI> spImpactI;
	cp<SurfaceI> cpSurfaceI;

#if FALSE
	feLog("\nview fovy %.6G\n\n%s\n\n%s\n",
			fovy,c_print(cameraMatrix),c_print(cameraTransform));
#endif

	if(log)
	{
		feLog("[H");
	}

	Real fraction[step];
	for(U32 m=0;m<step;m++)
	{
		fraction[m]=m/Real(step);
	}

	const U32 sectionSize=(m_leny/sections)/step*step;
	const U32 starty=section*sectionSize;
	U32 endy=starty+sectionSize;
	if(endy>m_leny)
	{
		endy=m_leny;
	}

//	feLog("%d/%d %dx%d %d %d  %d\n",
//			section,sections,m_lenx,m_leny,starty,endy,sectionSize);

	for(U32 y=starty+step-1;y<endy;y+=step)
	{
		for(U32 x=step-1;x<m_lenx;x+=step)
		{
			Vector2i coordinate(x,m_leny-1-y);
			end=target+Real(x)*scalex+Real(y)*scaley;

			U8* element=pixel(x,y);

			SpatialVector direction=end-origin;

//			if(x==m_lenx/2 && y==m_leny/2)
//			{
//				feLog("%s\n%s\n%s\n",c_print(origin),c_print(end),
//						c_print(direction));
//			}

			normalize(direction);

			BWORD hit=traceRay(0,coordinate,element,1.0f,FALSE,
					origin,direction,-1.0f,probeCheck(x,y),
					FALSE,spImpactI,cpSurfaceI);
			if(!hit)
			{
				element[0]=miss;
				element[1]=miss;
				element[2]=miss;
			}
			if(step<2)
			{
				if(log)
				{
					logElement(element);
				}
				continue;
			}

			U8* element1=pixel(x,y-step);
			U8* element2=pixel(x-step,y);
			U8* element3=pixel(x-step,y-step);

			const SpatialVector color0(*(Vector<3,U8>*)element);

			if(x<step || (y-starty)<step ||
					magnitudeSquared(SpatialVector(*(Vector<3,U8>*)element1)-
							color0)>margin ||
					magnitudeSquared(SpatialVector(*(Vector<3,U8>*)element2)-
							color0)>margin ||
					magnitudeSquared(SpatialVector(*(Vector<3,U8>*)element3)-
							color0)>margin
#if FE_TRACER_MOUSE_RAY
					|| (m_mouse[0]<=x && (x-m_mouse[0])<step &&
					m_mouse[1]<=y && (y-m_mouse[1])<step)
#endif
					)
			{
				for(U32 yy=0;yy<step;yy++)
				{
					U32 y2=y-yy;
					for(U32 xx=!yy;xx<step;xx++)
					{
						U32 x2=x-xx;
						Vector2i pixel2(x2,m_leny-1-y2);

						end=target+Real(x2)*scalex+Real(y2)*scaley;

						U8* mid=pixel(x2,y2);

						SpatialVector direction2=end-origin;
						normalize(direction2);
						BWORD hit2=traceRay(0,pixel2,mid,1.0f,FALSE,
								origin,direction2,-1.0f,probeCheck(x2,y2),
								FALSE,spImpactI,cpSurfaceI);
						if(!hit2)
						{
							mid[0]=miss;
							mid[1]=miss;
							mid[2]=miss;
						}
						if(antialias>1)
						{
							U8 sample[3];
							Vector3i value(mid[0],mid[1],mid[2]);
							for(U32 yyy=0;yyy<antialias;yyy++)
							{
								Real y3=y-yy-yyy*antiscale+antioffset;
								for(U32 xxx=0;xxx<antialias;xxx++)
								{
									Real x3=x-xx-xxx*antiscale+antioffset;
									end=target+x3*scalex+y3*scaley;

									sample[0]=0;
									sample[1]=0;
									sample[2]=0;

									SpatialVector direction3=end-origin;
									normalize(direction3);
									BWORD hit3=traceRay(0,pixel2,sample,1.0f,
											FALSE,origin,direction3,
											-1.0f,FALSE,
											FALSE,spImpactI,cpSurfaceI);
									if(hit3)
									{
										value[0]+=sample[0];
										value[1]+=sample[1];
										value[2]+=sample[2];
									}
								}
							}
							value*=antiscale2;
							for(U32 m=0;m<3;m++)
							{
								if(value[m]>255)
								{
									value[m]=255;
								}
								mid[m]=value[m];
							}
						}
					}
				}
			}
			else if(!debug && hit)
			{
				for(U32 yy=0;yy<step;yy++)
				{
					const Real fy=fraction[yy];
					const U32 y2=y-yy;
					const U8* left=pixel(x-step,y2);
					U8* right=pixel(x,y2);

					if(yy)
					{
						right[0]=U8(element[0]+
								fy*(element1[0]-element[0]));
						right[1]=U8(element[1]+
								fy*(element1[1]-element[1]));
						right[2]=U8(element[2]+
								fy*(element1[2]-element[2]));
					}
					for(U32 xx=1;xx<step;xx++)
					{
						const Real fx=fraction[xx];

						U8* mid=pixel(x-xx,y2);
						mid[0]=U8(right[0]+
								fx*(left[0]-right[0]));
						mid[1]=U8(right[1]+
								fx*(left[1]-right[1]));
						mid[2]=U8(right[2]+
								fx*(left[2]-right[2]));
					}
				}
			}
			if(log)
			{
				logElement(element);
			}
		}
		if(log)
		{
			feLog("\n");
		}
	}
}

BWORD DrawRayTrace::traceRay(U32 depth,const Vector2i& coordinate,
		U8* element,Real contribution,BWORD from_shadow,
		const SpatialVector& origin,const SpatialVector& direction,
		Real maxDistance,BWORD probe,
		BWORD getImpact,sp<SurfaceI::ImpactI>& rspImpactI,
		cp<SurfaceI>& rcpSurfaceI)
{
	Real closest_range=1e6f;
	Real closest_along=0.0f;
	SpatialVector closest_intersection;
	Sphere* pClosestSphere=NULL;
	Cylinder* pClosestCylinder=NULL;
	Disk* pClosestDisk=NULL;
	Drawable* pClosestDrawable=NULL;
	for(U32 m=0;m<m_sphereCount;m++)
	{
		Sphere& sphere=m_spheres[m];
		if(!depth && !intersecting(sphere.m_box,coordinate))
		{
			continue;
		}
		const Real range=RaySphereIntersect<Real>::solve(
				sphere.m_center,sphere.m_radius,
				origin,direction);
		if(range>0.0f && closest_range>range)
		{
			closest_range=range;
			pClosestSphere=&sphere;
		}
	}
	for(U32 m=0;m<m_cylinderCount;m++)
	{
		Cylinder& cylinder=m_cylinders[m];
		if(!depth && !intersecting(cylinder.m_box,coordinate))
		{
			continue;
		}
		SpatialVector intersection;
		Real along;
		const Real range=RayCylinderIntersect<Real>::solve(
				cylinder.m_base,cylinder.m_axis,cylinder.m_radius,
				origin,direction,along,intersection);
		if(range>0.0f && closest_range>range)
		{
			closest_range=range;
			closest_along=along;
			closest_intersection=intersection;
			pClosestCylinder=&cylinder;
			pClosestSphere=NULL;
		}
	}
	for(U32 m=0;m<m_diskCount;m++)
	{
		Disk& disk=m_disks[m];
		if(!depth && !intersecting(disk.m_box,coordinate))
		{
			continue;
		}
		SpatialVector intersection;
		const Real range=RayDiskIntersect<Real>::solve(
				disk.m_center,disk.m_facing,disk.m_radius,
				origin,direction,intersection);
		if(range>0.0f && closest_range>range)
		{
			closest_range=range;
			closest_intersection=intersection;
			pClosestDisk=&disk;
			pClosestSphere=NULL;
			pClosestCylinder=NULL;
		}
	}
	sp<SurfaceI::ImpactI> spClosestImpactI;
	cp<SurfaceI> cpClosestSurfaceI;
	for(U32 m=0;m<m_drawableCount;m++)
	{
		Drawable& drawable=m_drawables[m];
		if(!depth && !intersecting(drawable.m_box,coordinate))
		{
			continue;
		}

		cp<SurfaceI> cpSurfaceI=drawable.m_cpDrawableI;

		const BWORD anyHit=FALSE;

		sp<SurfaceI::ImpactI> spImpactI=drawable.m_transformed?
				cpSurfaceI->rayImpact(drawable.m_transform,
				origin,direction,-1.0,anyHit):
				cpSurfaceI->rayImpact(origin,direction,-1.0,anyHit);
		if(!spImpactI.isValid())
		{
			continue;
		}

		if(spImpactI.isValid())
		{
			spImpactI->setDiffuse(drawable.m_color);
		}

		const Real range=spImpactI->distance();
		if(range>0.0f && closest_range>range)
		{
			spClosestImpactI=spImpactI;
			cpClosestSurfaceI=cpSurfaceI;
			closest_range=range;
			closest_intersection=spImpactI->intersection();
			pClosestDrawable=&drawable;
			pClosestDisk=NULL;
			pClosestSphere=NULL;
			pClosestCylinder=NULL;
		}
	}
	if(!pClosestSphere && !pClosestCylinder && !pClosestDisk &&
			!pClosestDrawable)
	{
		if(!from_shadow)
		{
			const Real specular=8.0f;
			const SpatialVector to_light=unit(m_light-origin);
			const Real shine=dot(direction,to_light);
			if(shine>0.0f)
			{
				const Real shade=powf(shine,specular);
				U32 add=U32(contribution*(200.0f*shade));
				for(U32 m=0;m<3;m++)
				{
					U32 value=element[m]+add;
					if(value>255)
					{
						value=255;
					}
					element[m]=value;
				}
			}
		}

#if FE_TRACER_MOUSE_RAY
		if(probe)
		{
			Real scale=(maxDistance>0.0f &&
					maxDistance<FE_TRACER_NORMAL_SCALE)?
					maxDistance: FE_TRACER_NORMAL_SCALE;
			const BWORD shadowCheck=(contribution<=0.0f);
			Array<SpatialVector>& rArray=
					shadowCheck? m_showShadowArray: m_showReflectionArray;
			U32 size=rArray.size();
			rArray.resize(size+2);
			rArray[size]=origin;
			rArray[size+1]=
					origin+direction*scale;
		}
#endif

		return FALSE;
	}
	if(maxDistance>0.0f && closest_range>maxDistance)
	{
		return FALSE;
	}
#if !FE_TRACER_MOUSE_RAY
	if(contribution<=0.0f)
	{
		return TRUE;
	}
#endif

	rspImpactI=NULL;
	rcpSurfaceI=NULL;

	SpatialVector intersection=closest_intersection;
	SpatialVector normal;
	Color color;
	if(pClosestSphere)
	{
		RaySphereIntersect<Real>::resolveContact(
				pClosestSphere->m_center,pClosestSphere->m_radius,
				origin,direction,closest_range,intersection,normal);
		color=pClosestSphere->m_color;
	}
	else if(pClosestCylinder)
	{
		RayCylinderIntersect<Real>::resolveContact(
				pClosestCylinder->m_base,
				pClosestCylinder->m_axis,pClosestCylinder->m_radius,
				origin,direction,closest_range,closest_along,
				intersection,normal);
		color=pClosestCylinder->m_color;
	}
	else if(pClosestDisk)
	{
		RayDiskIntersect<Real>::resolveContact(
				pClosestDisk->m_center,
				pClosestDisk->m_facing,pClosestDisk->m_radius,
				origin,direction,closest_range,
				intersection,normal);
		color=pClosestDisk->m_color;
	}
	else
	{
		intersection=spClosestImpactI->intersection();
		normal=spClosestImpactI->normal();
		color=pClosestDrawable->m_color;
		rspImpactI=spClosestImpactI;
		rcpSurfaceI=cpClosestSurfaceI;
	}
	intersection+=0.01f*normal;
	if(getImpact)
	{
		if(!rspImpactI.isValid())
		{
			Impact* pImpact=new Impact();
			pImpact->m_origin=origin;
			pImpact->m_direction=direction;
			pImpact->m_diffuse=color;
			pImpact->m_intersection=intersection;
			pImpact->m_normal=normal;
			pImpact->m_distance=closest_range;

			rspImpactI=pImpact;
		}
		return TRUE;
	}

#if FE_TRACER_MOUSE_RAY
	if(probe)
	{
		U32 size=m_showHitArray.size();
		m_showHitArray.resize(size+1);
		m_showHitArray[size]=intersection;

		const BWORD shadowCheck=(contribution<=0.0f);
		if(!shadowCheck)
		{
			size=m_showNormalArray.size();
			m_showNormalArray.resize(size+2);
			m_showNormalArray[size]=intersection;
			m_showNormalArray[size+1]=
					intersection+normal*FE_TRACER_NORMAL_SCALE;
		}

		Array<SpatialVector>& rArray=
				shadowCheck? m_showShadowArray: m_showReflectionArray;
		size=rArray.size();
		rArray.resize(size+2);
		rArray[size]=origin;
		rArray[size+1]=intersection;
	}
	if(contribution<=0.0f)
	{
		return TRUE;
	}
#endif

	SpatialVector to_light=m_light-intersection;
	Real light_distance=magnitude(to_light);
	to_light*=1.0f/(light_distance);
	Real shade=dot(normal,to_light);
	if(shade<0.0f)
	{
		shade=0.0f;
	}

	const Real reflectivity=0.7f;	// tweak
	const Real fraction=contribution*(1.0f-reflectivity);
	const Real next_contribution=contribution*reflectivity;
	sp<SurfaceI::ImpactI> spImpactI;
	cp<SurfaceI> cpSurfaceI;

#if FE_TRACER_SHADOW
	BWORD hit=traceRay(depth+1,coordinate,element,0.0f,FALSE,
			intersection,to_light,light_distance,probe,
			FALSE,spImpactI,cpSurfaceI);
#else
	BWORD hit=FALSE;
#endif

	for(U32 m=0;m<3;m++)
	{
		U32 value=element[m]+
				U32(color[m]*fraction*(100.0f+(!hit)*400.0f*shade));
		if(value>255)
		{
			value=255;
		}
		element[m]=value;
	}

#if FE_TRACER_REFLECTION_MAX >= 0
	if(depth>=FE_TRACER_REFLECTION_MAX)
	{
		return TRUE;
	}
#endif

	const SpatialVector vertical=normal*(-dot(direction,normal));
	const SpatialVector reflection=2.0f*vertical+direction;

	//* reflection/specular
#if	FE_TRACER_REFLECTION
	traceRay(depth+1,coordinate,element,next_contribution,
			hit,intersection,reflection,-1.0f,probe,
			FALSE,spImpactI,cpSurfaceI);
#endif

	return TRUE;
}

void DrawRayTrace::logElement(U8* element)
{
	U32 code=0;
	if(element[0]>50)
	{
		code+=1;
	}
	if(element[1]>50)
	{
		code+=2;
	}
	if(element[2]>50)
	{
		code+=4;
	}
	if(code)
	{
		feLog("[3%dm",code);
	}
	if(element[0]>element[1] && element[0]>element[2])
	{
		feLog(element[0]>127? "R": "r");
	}
	else if(element[1]>element[2])
	{
		feLog(element[1]>127? "G": "c");
	}
	else if(element[2]>0)
	{
		feLog(element[2]>127? "B": "o");
	}
	else
	{
		feLog(" ");
		return;
	}
	feLog("[0m");
}

void DrawRayTrace::runTracers(void)
{
	if(FE_TRACER_MT_SECTIONS<2)
	{
		traceImage(0,FE_TRACER_MT_SECTIONS);
		return;
	}

	if(m_spGang.isNull())
	{
		//* start helper threads
		m_spGang=new Gang<TracerWorker,I32>();
	}

#if FE_TRACER_MT_DEBUG
	feLog("[33mDrawRayTrace::runTracers start[0m\n");
#endif

	if(!m_spGang->workers())
	{
		if(!m_spGang->start(sp<Counted>(this),FE_TRACER_MT_THREADS))
		{
			m_spGang=NULL;
			feX(e_cannotCreate,"DrawRayTrace::runTracers",
					"Gang failed to create thread");
		}
	}

	if(m_spGang.isNull())
	{
		return;
	}

	for(U32 index=1;index<FE_TRACER_MT_SECTIONS;index++)
	{
		m_spGang->post(FE_TRACER_MT_ACTION_SOLVE|index);
	}

#if FE_TRACER_MT_DEBUG
	feLog("[33mDrawRayTrace::runTracers posted[0m\n");
#endif

	traceImage(0,FE_TRACER_MT_SECTIONS);

#if FE_TRACER_MT_DEBUG
	feLog("[33mDrawRayTrace::runTracers master finished[0m\n");
#endif

	U32 jobs=FE_TRACER_MT_SECTIONS-1;
	while(jobs)
	{
		I32 job=0;
		m_spGang->waitForDelivery(job);
		jobs--;

#if FE_TRACER_MT_DEBUG
		feLog("[33mDrawRayTrace::runTracers accepted %10p[0m\n",job);
#endif
	}

#if FALSE
	//* force clean-up
	stopThreads();
#endif

#if FE_TRACER_MT_DEBUG
	feLog("[33mDrawRayTrace::runTracers done[0m\n");
#endif
}


void DrawRayTrace::TracerWorker::operate(void)
{
#if FE_TRACER_MT_DEBUG
	const U32 thread=m_id+1;

	feLog("[32mDrawRayTrace::TracerWorker::operate"
			" %p Thread %d starting[0m\n",this,thread);

	BWORD wait_message=TRUE;
#endif

	I32 job=0;
	I32 index=0;
	while(TRUE)
	{
		m_hpJobQueue->waitForJob(job);
		if(job==FE_TRACER_MT_ACTION_QUIT)
		{
#if FE_TRACER_MT_DEBUG
			feLog("[32mDrawRayTrace::TracerWorker::operate"
					" %p Thread %d Job %10p break[0m\n",this,thread,job);
#endif
			break;
		}

#if FE_TRACER_MT_DEBUG
		feLog("[32mDrawRayTrace::TracerWorker::operate"
				" %p Thread %d Job %10p[0m\n",
				this,thread,job);
#endif

		index=job&FE_TRACER_MT_INDEX_MASK;

		if(job&FE_TRACER_MT_ACTION_SOLVE)
		{
			m_hpDrawRayTrace->traceImage(index,FE_TRACER_MT_SECTIONS);
		}

#if FE_TRACER_MT_DEBUG
		feLog("[32mDrawRayTrace::TracerWorker::operate"
				" %p Thread %d Job %10p finished[0m\n",
				this,thread,job);
		wait_message=TRUE;
#endif

		m_hpJobQueue->deliver(job);
	}
#if FE_TRACER_MT_DEBUG
	feLog("[32mDrawRayTrace::TracerWorker::operate"
			" %p Thread %d leaving[0m\n",this,thread);
#endif
}

} /* namespace ext */
} /* namespace fe */
