import sys
import os
import re
forge = sys.modules["forge"]

def setup(module):
	# TODO find
	incPath = "/usr/include"

	version = ""
	il_h = incPath + "/IL/il.h"
	if os.path.exists(il_h):
		for line in open(il_h).readlines():
			if re.match("#define IL_VERSION .*", line):
				version = line.split()[2].rstrip()
				break

	if version != "":
		sys.stdout.write(" " + version)

	srcList = [	"openil.pmh",
				"ImageIL",
				"openilDL" ]

	dll = module.DLL( "fexOpenILDL", srcList )

	deplibs = forge.basiclibs + [
				"fePluginLib",
				"fexImageDLLib" ]

	dll.linkmap = { "gfxlibs": forge.gfxlibs }

	if forge.fe_os == "FE_LINUX":
		dll.linkmap["imagelibs"] = "-lIL -lILU -lILUT"
	elif forge.fe_os == "FE_WIN32" or forge.fe_os == "FE_WIN64":
		dll.linkmap["imagelibs"] = "DevIL.lib ILU.lib ILUT.lib"

	forge.deps( ["fexOpenILDLLib"], deplibs )

	forge.tests += [
		("inspect.exe",	"fexOpenILDL",						None,	None) ]

	if not 'ray' in forge.modules_found:
		forge.color_on(0, forge.BLUE)
		sys.stdout.write(" -ray")
		forge.color_off()

	if 'viewer' in forge.modules_confirmed:
		forge.tests += [
			("xImage.exe",	"fexOpenILDL 10",					None,	None) ]

		if forge.media:
			forge.tests += [ ("xImage.exe",
				"fexOpenILDL " + forge.media + "/image/OpenIL.png 10",
																None,	None) ]
		if 'ray' in forge.modules_found:
			forge.tests += [
				("xRay.exe",	"fexOpenILDL 30",				None,	None) ]
	else:
		forge.color_on(0, forge.BLUE)
		sys.stdout.write(" -viewer")
		forge.color_off()

#	module.Module('test')

def auto(module):
	test_file = """
#include <IL/ilut.h>

int main(void)
{
	return(0);
}
    \n"""

	ilmap = { "gfxlibs": forge.gfxlibs }
	if forge.fe_os == "FE_LINUX":
		ilmap["imagelibs"] = "-lIL -lILU -lILUT"
	elif forge.fe_os == "FE_WIN32" or forge.fe_os == "FE_WIN64":
		ilmap["imagelibs"] = "DevIL.lib ILU.lib ILUT.lib"

	return forge.cctest(test_file, ilmap)
