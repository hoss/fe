/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __openal_VoiceAL_h__
#define __openal_VoiceAL_h__
namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Voice using OpenAL

	@ingroup openal
*//***************************************************************************/
class VoiceAL:
	public Initialize<VoiceAL>,
	virtual public VoiceI
{
	public:
				VoiceAL(void);
virtual			~VoiceAL(void);

		void	initialize(void);

				//* As VoiceI
virtual	Result	setReferenceDistance(Real distance);
virtual	Result	setLocation(const SpatialVector& location);
virtual	Result	setDirection(const SpatialVector& direction);
virtual	Result	setVelocity(const SpatialVector& velocity);
virtual	Result	setPitch(F32 pitch);
virtual	Result	setVolume(F32 volume);

virtual	Result	play(void);
virtual	Result	pause(void);
virtual	Result	stop(void);
virtual	Result	rewind(void);

virtual	Result	queue(String name);
virtual	Result	loop(BWORD on);
virtual	Result	persist(BWORD on);
virtual	I32		remaining(void);

	private:
		Result	checkError(String text);
		I32		queued(void);
		I32		processed(void);
		Result	prune(void);
		Result	clear(void);

		ALuint		m_source;
		hp<AudioAL>	m_hpAudioAL;
		sp<AudioAL>	m_spAudioAL;	// intermittent persistence lock
		BWORD		m_persistent;
};

inline Result VoiceAL::checkError(String text)
{
	return m_hpAudioAL.isValid()?
			m_hpAudioAL->checkError("VoiceAL::~VoiceAL"): e_ok;
}

} /* namespace ext */
} /* namespace fe */

#endif /* __openal_VoiceAL_h__ */

