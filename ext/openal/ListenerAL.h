/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __openal_ListenerAL_h__
#define __openal_ListenerAL_h__
namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Listener using OpenAL

	@ingroup openal
*//***************************************************************************/
class ListenerAL:
	public Initialize<ListenerAL>,
	virtual public ListenerI
{
	public:
				ListenerAL(void);
virtual			~ListenerAL(void);

		void	initialize(void);

				//* As ListenerI
virtual	Result	setLocation(const SpatialVector& location);
virtual	Result	setOrientation(const SpatialVector& at,const SpatialVector& up);
virtual	Result	setVelocity(const SpatialVector& velocity);
};

} /* namespace ext */
} /* namespace fe */

#endif /* __openal_ListenerAL_h__ */

