/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <openal/openal.pmh>

namespace fe
{
namespace ext
{

VoiceAL::VoiceAL(void):
	m_persistent(FALSE)
{
}

void VoiceAL::initialize(void)
{
	alGenSources(1, &m_source);
	if (alGetError() != AL_NO_ERROR)
	{
		feX("VoiceAL::initialize","alGenSources failed");
	}

	//* we will use AudioAL-specific extensions to AudioI
	m_spAudioAL=registry()->create("*.AudioAL");
	FEASSERT(m_spAudioAL.isValid());

	m_hpAudioAL=m_spAudioAL;

	SpatialVector zero(0.0f,0.0f,0.0f);
	SpatialVector posZ(0.0f,0.0f,1.0f);
	setLocation(zero);
	setDirection(posZ);
	setVelocity(zero);

	//* audible sound pressure follows 1/d (not sound intensity of 1/d^2)
	//* which would be AL_INVERSE_DISTANCE with default AL_ROLLOFF_FACTOR=1

	// AL_INVERSE_DISTANCE AL_INVERSE_DISTANCE_CLAMPED
	// AL_LINEAR_DISTANCE AL_LINEAR_DISTANCE_CLAMPED
	// AL_EXPONENT_DISTANCE AL_EXPONENT_DISTANCE_CLAMPED AL_NONE
	// default is AL_INVERSE_DISTANCE_CLAMPED
	alDistanceModel(AL_INVERSE_DISTANCE);

//	alSourcef(m_source,AL_ROLLOFF_FACTOR,0.1);
//	alSourcef(m_source,AL_MAX_DISTANCE,100.0);
}

VoiceAL::~VoiceAL(void)
{
//	feLog("~VoiceAL\n");
	clear();
	alDeleteSources(1, &m_source);
	checkError("VoiceAL::~VoiceAL");
}

Result VoiceAL::setReferenceDistance(Real distance)
{
	alSourcef(m_source,AL_REFERENCE_DISTANCE,distance);
	return checkError("VoiceAL::setReferenceDistance");
}

Result VoiceAL::setLocation(const SpatialVector& location)
{
	alSource3f(m_source,AL_POSITION,location[0],location[1],location[2]);
	return checkError("VoiceAL::setLocation");
}

Result VoiceAL::setDirection(const SpatialVector& direction)
{
	alSource3f(m_source,AL_DIRECTION,direction[0],direction[1],direction[2]);
	return checkError("VoiceAL::setDirection");
}

Result VoiceAL::setVelocity(const SpatialVector& velocity)
{
	alSource3f(m_source,AL_VELOCITY,velocity[0],velocity[1],velocity[2]);
	return checkError("VoiceAL::setVelocity");
}

Result VoiceAL::setPitch(F32 pitch)
{
	alSourcef(m_source,AL_PITCH,pitch);
	return checkError("VoiceAL::setPitch");
}

Result VoiceAL::setVolume(F32 volume)
{
	alSourcef(m_source,AL_GAIN,volume);
	return checkError("VoiceAL::setVolume");
}

Result VoiceAL::play(void)
{
	alSourcePlay(m_source);
	return checkError("VoiceAL::play");
}

Result VoiceAL::pause(void)
{
	alSourcePause(m_source);
	return checkError("VoiceAL::pause");
}

Result VoiceAL::stop(void)
{
	alSourceStop(m_source);
	return checkError("VoiceAL::stop");
}

Result VoiceAL::rewind(void)
{
	alSourceRewind(m_source);
	return checkError("VoiceAL::rewind");
}

Result VoiceAL::queue(String name)
{
	prune();

	ALuint buffer=m_hpAudioAL->lookup(name);
	if(buffer==ALuint(-1))
	{
		return e_cannotFind;
	}

//	alSourcei(m_source, AL_BUFFER, buffer);

	alSourceQueueBuffers(m_source,1,&buffer);
	return checkError("VoiceAL::queue");
}

Result VoiceAL::loop(BWORD newstate)
{
	alSourcei(m_source,AL_LOOPING,newstate);
	return checkError("VoiceAL::loop");
}

Result VoiceAL::persist(BWORD newstate)
{
	sp<VoiceI> spVoiceI(this);
	if(newstate && !m_persistent)
	{
		m_persistent=TRUE;
		m_hpAudioAL->remember(spVoiceI);
		m_spAudioAL=NULL;
	}
	else if(!newstate && m_persistent)
	{
		m_persistent=FALSE;
		m_spAudioAL=m_hpAudioAL;
		m_hpAudioAL->forget(spVoiceI);
	}

	return e_ok;
}

I32 VoiceAL::remaining(void)
{
	ALint count=queued()-processed();

	return count;
}

I32 VoiceAL::queued(void)
{
	ALint queued=0;

	alGetSourcei(m_source,AL_BUFFERS_QUEUED,&queued);
	checkError("VoiceAL::queued");

	return queued;
}

I32 VoiceAL::processed(void)
{
	ALint count=0;

	alGetSourcei(m_source,AL_BUFFERS_PROCESSED,&count);
	checkError("VoiceAL::processed");

	return count;
}

Result VoiceAL::prune(void)
{
	ALint count=processed();

	ALuint* pBufferNames=new ALuint[count];
	alSourceUnqueueBuffers(m_source,count,pBufferNames);
	delete[] pBufferNames;

	return checkError("VoiceAL::prune alSourceUnqueueBuffers");
}

Result VoiceAL::clear(void)
{
	stop();

	ALint count=queued();

	ALuint* pBufferNames=new ALuint[count];
	alSourceUnqueueBuffers(m_source,count,pBufferNames);
	delete[] pBufferNames;

	return checkError("VoiceAL::clear alSourceUnqueueBuffers");
}

} /* namespace ext */
} /* namespace fe */
