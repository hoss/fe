#define HAVE_STDINT_H		1
#define HAVE_STAT			1
#define ALUT_BUILD_LIBRARY	1

#if defined(_WIN32) && !defined(_XBOX)
#define HAVE_SLEEP			1
#define HAVE_WINDOWS_H		1
#else
#define HAVE_NANOSLEEP		1
#define HAVE_TIME_H			1
#define HAVE_UNISTD_H		1
#endif

#include "openal/freealut/src/alutBufferData.c"
#include "openal/freealut/src/alutCodec.c"
#include "openal/freealut/src/alutError.c"
#include "openal/freealut/src/alutInit.c"
#include "openal/freealut/src/alutInputStream.c"
#include "openal/freealut/src/alutLoader.c"
#include "openal/freealut/src/alutOutputStream.c"
#include "openal/freealut/src/alutUtil.c"
#include "openal/freealut/src/alutVersion.c"
#include "openal/freealut/src/alutWaveform.c"
