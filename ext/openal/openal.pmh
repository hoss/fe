#ifndef __openal_openal_pmh__
#define __openal_openal_pmh__

//* look into Alure: https://github.com/kcat/alure

/**
	@defgroup openal OpenAL Audio Integration
	@ingroup ext

	http://www.openal.org

	https://kcat.strangesoft.net/openal.html

	Note that original OpenAL has been forked to "OpenAL Soft".
	A tested version of OpenAL Soft is included in the FE distribution
	and built automatically.
*/

/***
	@section openal_install Installation Notes

	http://www.openal.org

	Note that original OpenAL has been forked to "OpenAL Soft".

	@subsection openal_win32 Win32
	- install the original OpenAL for the dll: https://openal.org/downloads/oalinst.zip
	- download OpenAL Soft: https://kcat.strangesoft.net/openal-binaries/openal-soft-1.19.1-bin.zip
	- extract as c:\\local\\openal-soft-1.19.1-bin
	- copy c:\\local\\openal-soft-1.19.1-bin\\bin\\soft_oal.dll C:\\Windows\\SysWOW64\\OpenAL32.dll

	@subsection openal_debian Debian/Ubuntu
	- apt-get install libopenal-dev
*/

/***
	even older instructions:

	- install OpenAL to default C:\\Program Files\\OpenAL 1.1 SDK
	- copy C:\\Program Files\\OpenAL 1.1 SDK\\include\\* to
	C:\\OpenAL1.1\\include (create folder)
	- copy C:\\Program Files\\OpenAL 1.1 SDK\\libs\\Win32\\* to
	C:\\OpenAL1.1\\libs (create folder)
	- download freealut
	- copy ALUT header files to C:\\OpenAL1.1\\include\\ALUT (create folder)
	- copy ALUT lib files to C:\\OpenAL1.1\\libs

	OR

	- use main/dep/OpenAL1.1 included with FE

	@subsection openal_debian Debian/Ubuntu
	- apt-get install libopenal-dev libalut-dev

	@subsection openal_gentoo Gentoo
	- emerge openal
*/

#include "audio/audio.h"

#include "openal/openal_core.h"

#include "openal/AudioAL.h"
#include "openal/ListenerAL.h"
#include "openal/VoiceAL.h"

#endif /* __openal_openal_pmh__ */
