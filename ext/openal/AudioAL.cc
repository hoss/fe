/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <openal/openal.pmh>

#define FE_AL_DEBUG		FALSE

namespace fe
{
namespace ext
{

AudioAL::AudioAL(void)
{
	m_chorus.setAutoDestruct(TRUE);

	alutInitWithoutContext(NULL,NULL);

#if FE_OS==FE_WIN32 || FE_OS==FE_WIN64
	const ALchar* deviceName=NULL;	//"DirectSound3D";
#else
	const ALCchar* deviceName=NULL;//"'( ( devices '( native alsa null ) ) )";
#endif

	m_pDevice=alcOpenDevice(deviceName);
	if(!m_pDevice)
	{
		feX(e_cannotCreate,"AudioAL::AudioAL","alcOpenDevice failed");
	}

	m_pContext=alcCreateContext(m_pDevice,NULL);
	if(!m_pContext)
	{
		alcCloseDevice(m_pDevice);
		feX(e_cannotCreate,"AudioAL::AudioAL","alcCreateContext failed");
	}

	alcMakeContextCurrent(m_pContext);

	ALCint major= 0;
	ALCint minor= 0;
	ALCint mono_sources= 0;
	ALCint stereo_sources= 0;
	alcGetIntegerv(m_pDevice, ALC_MAJOR_VERSION, 1, &major);
	alcGetIntegerv(m_pDevice, ALC_MINOR_VERSION, 1, &minor);
//	alcGetIntegerv(m_pDevice, ALC_MONO_SOURCES, 1, &mono_sources);
//	alcGetIntegerv(m_pDevice, ALC_STEREO_SOURCES, 1, &stereo_sources);
	feLog("AudioAL::AudioAL OpenAL version %d.%d  voices: stereo %d mono %d\n",
			major,minor,stereo_sources,mono_sources);

	alGetError();

	alDopplerFactor(1.0);
	alDopplerVelocity(343);
}

AudioAL::~AudioAL(void)
{
#if FE_AL_DEBUG
	feLog("AudioAL::~AudioAL\n");
#endif

	forgetAll();

	//* dispose of remaining buffers
	for(BufferMap::iterator it=m_bufferMap.begin();it!=m_bufferMap.end();it++)
	{
		ALuint buffer=it->second;
		alDeleteBuffers(1, &buffer);
	}

	ALCboolean success=alcMakeContextCurrent(NULL);
	if(!success)
	{
		feLog("AudioAL::~AudioAL releasing OpenAL context failed\n",success);
	}
	alcDestroyContext(m_pContext);
	alcCloseDevice(m_pDevice);

	alutExit();

#if FE_AL_DEBUG
	feLog("AudioAL::~AudioAL done\n");
#endif
}

Result AudioAL::load(String name,String filename)
{
	ALuint buffer=lookup(name);
	if(buffer!=ALuint(-1))
	{
		return e_alreadyAvailable;
	}

#if TRUE
	buffer=alutCreateBufferFromFile(filename.c_str());
	if(buffer==AL_NONE)
	{
		return e_invalidHandle;
	}
#else
	alGenBuffers(1, &buffer);
	FE_RETURN_AL_ERROR("AudioAL::load alGenBuffers");

	ALenum format;
	ALsizei size;
	ALvoid* data;
	ALsizei freq;

#if FE_OS==FE_OSX
	alutLoadWAVFile((ALbyte*)filename.c_str(),
			&format, &data, &size, &freq);
#else
	ALboolean loop;
	alutLoadWAVFile((ALbyte*)filename.c_str(),
			&format, &data, &size, &freq, &loop);
#endif
	FE_RETURN_AL_ERROR("AudioAL::load alutLoadWAVFile");

#if FE_AL_DEBUG
	String formatString="undefined";
	switch(format)
	{
		case AL_FORMAT_MONO8:
			formatString="AL_FORMAT_MONO8";
			break;
		case AL_FORMAT_MONO16:
			formatString="AL_FORMAT_MONO16";
			break;
		case AL_FORMAT_STEREO8:
			formatString="AL_FORMAT_STEREO8";
			break;
		case AL_FORMAT_STEREO16:
			formatString="AL_FORMAT_STEREO16";
			break;
	}

	feLog("AudioAL::load \"%s\" format=%p (%s) size=%d freq=%d\n",
			filename.c_str(),format,formatString.c_str(),size,freq);
#endif

	if(format!=AL_FORMAT_MONO8 && format!=AL_FORMAT_MONO16 &&
			format!=AL_FORMAT_STEREO8 && format!=AL_FORMAT_STEREO16)
	{
		return e_invalidHandle;
	}

	alBufferData(buffer, format, data, size, freq);
	FE_RETURN_AL_ERROR("AudioAL::load alBufferData");

	alutUnloadWAV(format, data, size, freq);
	FE_RETURN_AL_ERROR("AudioAL::load alutUnloadWAV");
#endif

	m_bufferMap[name]=buffer;
	return e_ok;
}

/*
Result AudioAL::load(String name,String filename)
{
	ALuint buffer=alutCreateBufferFromFile((ALbyte*)filename.c_str());
	FE_RETURN_AL_ERROR("AudioAL::load alutCreateBufferFromFile");

	m_bufferMap[name]=buffer;
	return e_ok;
}
*/

Result AudioAL::unload(String name)
{
	ALuint buffer=lookup(name);
	if(buffer==ALuint(-1))
	{
		return e_cannotFind;
	}

	m_bufferMap.erase(name);

	alDeleteBuffers(1, &buffer);
	FE_RETURN_AL_ERROR("AudioAL::unload");

	return e_ok;
}

void AudioAL::remember(sp<VoiceI> spVoiceI)
{
	spVoiceI->adjoin(sp<Component>(this));
	m_chorus.append(spVoiceI);
}

void AudioAL::forget(sp<VoiceI> spVoiceI)
{
//	if(spVoiceI->count()==2)
	{
#if FE_AL_DEBUG
		feLog("AudioAL::forget %s\n",spVoiceI->name().c_str());
#endif
		spVoiceI->disjoin();
	}

	hp<VoiceI> hpVoiceI(spVoiceI);
	m_chorus.remove(hpVoiceI);
}

Result AudioAL::flush(void)
{
	//* forget finished voices in the chorus
	List< hp<VoiceI> >::Iterator iterator(m_chorus);
	iterator.toHead();

	hp<VoiceI> hpVoiceI;
	while((hpVoiceI= *iterator).isValid())
	{
		if(hpVoiceI->remaining()<1)
		{
#if FE_AL_DEBUG
			feLog("AudioAL::flush removing voice\n");
#endif
			forget(hpVoiceI);
		}
		else
		{
			iterator++;
		}
	}

	return e_ok;
}

void AudioAL::forgetAll(void)
{
	//* forget all voices in the chorus
	List< hp<VoiceI> >::Iterator iterator(m_chorus);
	iterator.toHead();

	hp<VoiceI> hpVoiceI;
	while((hpVoiceI= *iterator).isValid())
	{
#if FE_AL_DEBUG
			feLog("AudioAL::forgetAll removing voice\n");
#endif
			forget(hpVoiceI);
	}
}

} /* namespace ext */
} /* namespace fe */
