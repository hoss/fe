/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <maya/maya.pmh>

#define FE_SCM_DEBUG	FALSE

namespace fe
{
namespace ext
{

SurfaceCurvesMaya::SurfaceCurvesMaya(void):
	m_subIndex(-1)
{
#if FE_SCM_DEBUG
	feLog("SurfaceCurvesMaya()\n");
#endif
}

SurfaceCurvesMaya::~SurfaceCurvesMaya(void)
{
#if FE_SCM_DEBUG
	feLog("~SurfaceCurvesMaya()\n");
#endif
}

Protectable* SurfaceCurvesMaya::clone(Protectable* pInstance)
{
#if FE_SCM_DEBUG
	feLog("SurfaceCurvesMaya::clone\n");
#endif

	SurfaceCurvesMaya* pSurfaceCurvesMaya= pInstance?
			fe_cast<SurfaceCurvesMaya>(pInstance):
			new SurfaceCurvesMaya();

	SurfaceCurves::clone(pSurfaceCurvesMaya);

	// TODO copy attributes

	return pSurfaceCurvesMaya;
}

void SurfaceCurvesMaya::cache(void)
{
#if FE_SCM_DEBUG
	feLog("SurfaceCurvesMaya::cache\n");
#endif

	//* TODO e_arrayColor
	setOptionalArrays(e_arrayUV);

	sp<SurfaceAccessorMaya> spAccessor;
	if(!group().empty())
	{
		spAccessor=new SurfaceAccessorMaya;
		spAccessor->setMaster(registry()->master());
		spAccessor->setMeshData(m_meshData);
		spAccessor->bind(SurfaceAccessibleI::e_primitive,group());

	}

	MFnMesh* pFnMesh=new MFnMesh(m_meshData.asMesh());

#if FALSE
	sp<SurfaceAccessorMaya> spPrimitiveVertices;
	sp<SurfaceAccessorMaya> spPointNormals(new SurfaceAccessorMaya);
	spPointNormals->setMaster(registry()->master());
	spPointNormals->setMeshData(m_meshData);
	spPointNormals->setMesh(pFnMesh);

	if(m_useTransform)
	{
		spPointNormals->setTransform(m_transform);
	}
	else
	{
		spPointNormals->clearTransform();
	}

	if(!spPointNormals->bind(SurfaceAccessibleI::e_point,"N"))
	{
		spPointNormals=NULL;
	}
	else
	{
		spPrimitiveVertices=new SurfaceAccessorMaya;
		spPrimitiveVertices->setMaster(registry()->master());
		spPrimitiveVertices->setMeshData(m_meshData);
		spPrimitiveVertices->setMesh(pFnMesh);

		if(m_useTransform)
		{
			spPrimitiveVertices->setTransform(m_transform);
		}
		else
		{
			spPrimitiveVertices->clearTransform();
		}

		if(!spPrimitiveVertices->bind(SurfaceAccessibleI::e_primitive,
				SurfaceAccessibleI::e_vertices))
		{
			spPrimitiveVertices=NULL;
			spPointNormals=NULL;
		}
	}
#else
	sp<SurfaceAccessorMaya> spPrimitiveVertices;
	sp<SurfaceAccessorMaya> spPointNormals;
#endif

	//* NOTE not doing any Record serialization

	//* convert Maya to vertex and normal arrays


	const U32 primitiveCount=pFnMesh->numPolygons();

	m_elements=primitiveCount;
	m_vertices=0;
	U32 elementAllocated=0;
	U32 vertexAllocated=0;

	//* preallocate
	resizeFor(primitiveCount,primitiveCount*8,elementAllocated,vertexAllocated);

	U32 triangleIndex=0;
	U32 total=0;
	for(U32 primitiveIndex=0;primitiveIndex<primitiveCount;primitiveIndex++)
	{
		//* if using group and not in that group
		if(spAccessor.isValid() && !spAccessor->integer(primitiveIndex))
		{
			continue;
		}

		//* cut in half to skip twein vertices
		const U32 subCount=pFnMesh->polygonVertexCount(primitiveIndex)/2;
		if(subCount<2)
		{
#if FE_SCM_DEBUG
			feLog("SurfaceCurvesMaya::cache"
					" primitive %d/%d subCount %d\n",
					primitiveIndex,primitiveCount,subCount);
#endif
			continue;
		}

		m_vertices+=subCount;

		if(primitiveCount>elementAllocated || m_vertices>vertexAllocated)
		{
			resizeFor(primitiveCount,m_vertices,
					elementAllocated,vertexAllocated);
		}

		FEASSERT(triangleIndex<elementAllocated);
		m_pElementArray[triangleIndex]=Vector3i(total,subCount,TRUE);

		MIntArray vertexArray;
		MStatus status=pFnMesh->getPolygonVertices(primitiveIndex,vertexArray);

		I32 normalCount=0;

		MFloatVectorArray normalArray;
		if(spPointNormals.isValid())
		{
			normalCount=spPointNormals->count();
		}
		else
		{
			status=pFnMesh->getFaceVertexNormals(primitiveIndex,normalArray);
			normalCount=normalArray.length();
		}

#if FE_SCM_DEBUG
			feLog("SurfaceCurvesMaya::cache"
					" primitive %d/%d subCount %d normalCount %d\n",
					primitiveIndex,primitiveCount,subCount,normalCount);
#endif

		for(U32 subIndex=0;subIndex<subCount;subIndex++)
		{
			const I32 rawPointIndex=vertexArray[subIndex];

			MPoint point;
			status=pFnMesh->getPoint(rawPointIndex,point);

			float u=0.0;
			float v=0.0;
			pFnMesh->getUV(vertexArray[subIndex],u,v);

			m_pVertexArray[total]=SpatialVector(point[0],point[1],point[2]);
			m_pTriangleIndexArray[total]=triangleIndex;
			m_pPointIndexArray[total]=rawPointIndex;
			m_pPrimitiveIndexArray[total]=primitiveIndex;
			m_pPartitionIndexArray[total]= -1;	//* TODO
			set(m_pUVArray[total],u,v);

			if(m_useTransform)
			{
				transformVector(m_transform,
						m_pVertexArray[total],m_pVertexArray[total]);
			}

			if(subIndex<normalCount)
			{
				if(spPointNormals.isValid())
				{
					const I32 pointIndex=spPrimitiveVertices->integer(
							primitiveIndex,subIndex);
					m_pNormalArray[total]=
							spPointNormals->spatialVector(pointIndex);
				}
				else
				{
					MFloatVector& rNormal=normalArray[subIndex];
					m_pNormalArray[total]=
							SpatialVector(rNormal[0],rNormal[1],rNormal[2]);

					if(m_useTransform)
					{
						rotateVector(m_transform,
								m_pNormalArray[total],m_pNormalArray[total]);
					}
				}

#if FE_SCM_DEBUG
				feLog("  %d/%d point %s normal %s\n",subIndex,subCount,
						c_print(m_pVertexArray[total]),
						c_print(m_pNormalArray[total]));
#endif
			}
			else
			{
				m_pNormalArray[total]=SpatialVector(0.0,1.0,0.0);
			}

			total++;
		}

		triangleIndex++;
	}

	delete pFnMesh;

	if(elementAllocated!=triangleIndex || vertexAllocated!=m_vertices)
	{
		resizeArrays(triangleIndex,m_vertices);
	}

	if(total!=m_vertices)
	{
#if FE_SCM_DEBUG
		feLog("SurfaceCurvesMaya::cache"
				" converted %d/%d vertices\n",total,m_vertices);
#endif

		//* TODO variable subCounts
//		m_elements=triangleIndex;

		m_vertices=total;
		resizeArrays(primitiveCount,m_vertices);
	}

	m_elements=triangleIndex;

	calcBoundingSphere();
}

} /* namespace ext */
} /* namespace fe */
