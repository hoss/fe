/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __maya_SurfaceAccessorMaya_h__
#define __maya_SurfaceAccessorMaya_h__

#define FE_SAM_BLIND_DEBUG	FALSE

namespace fe
{
namespace ext
{

// NOTE Maya normals are JIT, not threadsafe to read unless all preread
// NOTE setVertexNormal appears to never be threadsafe

// NOTE MSpace::kWorld requires a MFnMesh created from a DagPath, not data

/**************************************************************************//**
    @brief Maya Surface Editing Implementation

	@ingroup maya

	Currently, this only supports poly meshes.
	We need to add NURBS support here or in a different class.
*//***************************************************************************/
class FE_DL_EXPORT SurfaceAccessorMaya:
	public SurfaceAccessorBase,
	public CastableAs<SurfaceAccessorMaya>
{
	public:

		enum	PolyFormat
				{
					e_unknown,
					e_closedPoly,
					e_openPoly,
					e_freePoint
				};
						SurfaceAccessorMaya(void):
							m_markPoly(0),
							m_markVertex(0),
							m_markRawVertex(0),
							m_pFnMesh(NULL),
							m_blindDataID(-1),
							m_polyFormatID(-1),
							m_pGroupIt(NULL),
							m_pStringCache(NULL),
							m_pIntegerCache(NULL),
							m_pRealCache(NULL),
							m_pSpatialVectorCache(NULL),
							m_pBooleanCache(NULL)
						{	setName("SurfaceAccessorMaya"); }
virtual					~SurfaceAccessorMaya(void)
						{
							if(m_pSpatialVectorCache)
							{
								if(m_attribute==SurfaceAccessibleI::e_normal)
								{
									MVectorArray normalArray;
									MIntArray faceList;
									MIntArray vertexList;

									const U32 normalCount=
											m_faceVertTable.size();
									for(U32 normalIndex=0;
											normalIndex<normalCount;
											normalIndex++)
									{
										if(!m_pBooleanCache)
										{
											continue;
										}
										SpatialVector normal;
										if(m_useTransform)
										{
											normal=m_pSpatialVectorCache
													[normalIndex];
										}
										else
										{
											rotateVector(m_inverse,
													m_pSpatialVectorCache
													[normalIndex],normal);
										}
										const MVector vector(normal[0],
												normal[1],normal[2]);

										Array<FaceVert>& rFaceVertArray=
												m_faceVertTable[normalIndex];
										const U32 faceVertCount=
												rFaceVertArray.size();
										for(U32 faceVertIndex=0;
												faceVertIndex<faceVertCount;
												faceVertIndex++)
										{
											FaceVert& faceVert=rFaceVertArray
													[faceVertIndex];
											normalArray.append(vector);
											faceList.append(faceVert.m_face);
											vertexList.append(normalIndex);
										}
									}
//									MStatus status=
											m_pFnMesh->setFaceVertexNormals(
											normalArray,faceList,vertexList);
								}

								delete[] m_pSpatialVectorCache;
							}
							if(m_pBooleanCache)
							{
								delete[] m_pBooleanCache;
							}
							if(m_pRealCache)
							{
								delete[] m_pRealCache;
							}
							if(m_pIntegerCache)
							{
								delete[] m_pIntegerCache;
							}
							if(m_pStringCache)
							{
								delete[] m_pStringCache;
							}
							if(m_pFnMesh)
							{
								delete m_pFnMesh;
							}
						}

						using SurfaceAccessorBase::set;
						using SurfaceAccessorBase::append;
						using SurfaceAccessorBase::spatialVector;

		BWORD			bind(SurfaceAccessibleI::Element a_element,
								SurfaceAccessibleI::Attribute a_attribute)
						{
							FEASSERT(m_pFnMesh);

							m_attribute=a_attribute;
							if(a_attribute==SurfaceAccessibleI::e_vertices ||
									a_attribute==
									SurfaceAccessibleI::e_properties)
							{
								FEASSERT(a_element==
										SurfaceAccessibleI::e_primitive);
								m_element=a_element;
								return TRUE;
							}

							return bindInternal(a_element,String());
						}
		BWORD			bind(SurfaceAccessibleI::Element a_element,
								const String& a_name)
						{
							FEASSERT(m_pFnMesh);

							String rename=a_name;
							String textureName;

							m_attribute=SurfaceAccessibleI::e_generic;

							if(a_name=="P")
							{
								m_attribute=SurfaceAccessibleI::e_position;
							}
							else if(a_name=="N")
							{
								m_attribute=SurfaceAccessibleI::e_normal;
							}
							else if(a_name=="uv")
							{
								m_attribute=SurfaceAccessibleI::e_uv;
							}
							else if(a_name=="Cd")
							{
								m_attribute=SurfaceAccessibleI::e_color;
							}
							else if(a_element==SurfaceAccessibleI::e_point &&
									a_name.match("[A-z0-9_]+\\.[A-z0-9_]+"))
							{
								textureName=a_name;

								m_attribute=SurfaceAccessibleI::e_uv;
								rename="uv";
							}

							if(!bindInternal(a_element,rename))
							{
								return FALSE;
							}

							if(!textureName.empty())
							{
								cacheTexture(textureName);
							}

							return TRUE;
						}

						//* as SurfaceAccessorI

virtual	String			type(void) const;
virtual	U32				count(void) const
						{
							FEASSERT(m_pFnMesh);

							switch(m_element)
							{
								case SurfaceAccessibleI::e_point:
									return vertexCount();

//								case SurfaceAccessibleI::e_vertex:
//									return 0;

								case SurfaceAccessibleI::e_primitive:
									return m_pFnMesh->numPolygons();

								case SurfaceAccessibleI::e_detail:
									return 1;

								case SurfaceAccessibleI::e_pointGroup:
									return (m_attrName=="")?
											vertexCount():
											m_groupList.size();

								case SurfaceAccessibleI::e_primitiveGroup:
									return (m_attrName=="")?
											m_pFnMesh->numPolygons():
											m_groupList.size();

								default:
									;
							}
							return 0;
						}

virtual	U32				subCount(U32 a_index) const
						{
							FEASSERT(m_pFnMesh);

							if(m_element==SurfaceAccessibleI::e_primitiveGroup
									&& m_attrName!="")
							{
								if(a_index>=m_groupList.size())
								{
									return 0;
								}
								return polyVertCount(m_groupList[a_index]);
							}
							else if(m_element==
									SurfaceAccessibleI::e_primitiveGroup ||
									m_attribute==SurfaceAccessibleI::e_vertices)
							{
								FEASSERT(m_element==
										SurfaceAccessibleI::e_primitive ||
										m_element==
										SurfaceAccessibleI::e_primitiveGroup);

								return polyVertCount(a_index);
							}

							return 1;
						}

virtual	void			set(U32 a_index,U32 a_subIndex,String a_string)
						{
							FEASSERT(m_pFnMesh);

							if(m_element==SurfaceAccessibleI::e_detail)
							{
								setDetail(m_attrName,a_string);
								return;
							}

							if(m_spSurfaceAccessibleI.isValid())
							{
								m_spSurfaceAccessibleI->lock(I64(this));
							}

							assureBlindDataID("string");

							const I32 compID=rawVertex(a_index);
							const MFn::Type component=componentType();
							MStatus status=m_pFnMesh->setStringBlindData(
									compID,
									component,
									m_blindDataID,
									m_attrName.c_str(),
									a_string.c_str());

							if(m_spSurfaceAccessibleI.isValid())
							{
								m_spSurfaceAccessibleI->unlock(I64(this));
							}

							if(!status)
							{
								feLog("SurfaceAccessorMaya::set(... String)"
										" index %d raw %d"
										" setStringBlindData failed\n",
										a_index, compID);
							}

							if(m_pStringCache)
							{
								m_pStringCache[a_index]=a_string;
							}
						}
virtual	String			string(U32 a_index,U32 a_subIndex=0)
						{
							FEASSERT(m_pFnMesh);

							if(m_element==SurfaceAccessibleI::e_detail)
							{
								return getDetail(m_attrName);
							}

							if(m_pStringCache)
							{
								return m_pStringCache[a_index];
							}

							if(m_attrName==".shader")
							{
								cacheShaders();
								return m_pStringCache[a_index];
							}

							checkBlindDataID("string");

							if(!m_pFnMesh->isBlindDataTypeUsed(m_blindDataID))
							{
#if FE_SAM_BLIND_DEBUG
								feLog("SurfaceAccessorMaya::string"
										" \"%s\" blind data %d not used\n",
										m_attrName.c_str(),m_blindDataID);
#endif
								return "";
							}
							const MFn::Type component=componentType();
							if(!m_pFnMesh->hasBlindData(component))
							{
#if FE_SAM_BLIND_DEBUG
								feLog("SurfaceAccessorMaya::string"
										" no component blind data\n");
#endif
								return "";
							}
							const I32 compID=rawVertex(a_index);
							if(!m_pFnMesh->hasBlindDataComponentId(compID,
									component))
							{
#if FE_SAM_BLIND_DEBUG
								feLog("SurfaceAccessorMaya::string"
										" no blind data on"
										" index %d compID %d\n",a_index,compID);
#endif
								return "";
							}

							const I32 size=count();

							MIntArray compIDs;
							MStringArray data;
							MStatus status=m_pFnMesh->getStringBlindData(
									component,
									m_blindDataID,
									m_attrName.c_str(),
									compIDs,
									data);
							if(!status)
							{
								feLog("SurfaceAccessorMaya::integer"
										" getStringBlindData failed\n");
								return "";
							}

							//* TODO reverse lookup for curves

							m_pStringCache=new String[size];
							for(I32 m=0;m<size;m++)
							{
								m_pStringCache[m]="";
							}

							const I32 found=compIDs.length();
							for(I32 m=0;m<found;m++)
							{
								FEASSERT(compIDs[m]<size);
								m_pStringCache[compIDs[m]]=data[m].asChar();
							}

							return m_pStringCache[a_index];
						}

virtual	void			set(U32 a_index,U32 a_subIndex,I32 a_integer)
						{
							FEASSERT(m_pFnMesh);

							if(m_element==SurfaceAccessibleI::e_detail)
							{
								String text;
								text.sPrintf("%d",a_integer);
								setDetail(m_attrName,text);
								return;
							}

							if(m_spSurfaceAccessibleI.isValid())
							{
								m_spSurfaceAccessibleI->lock(I64(this));
							}

#if FALSE
							const BWORD useBool=
									(m_element==
									SurfaceAccessibleI::e_pointGroup ||
									m_element==
									SurfaceAccessibleI::e_primitiveGroup);
#else
							const BWORD useBool=FALSE;
#endif

							assureBlindDataID(useBool? "boolean": "integer");

							const I32 compID=rawVertex(a_index);
							const MFn::Type component=componentType();

							if(useBool)
							{
								const bool data=a_integer;
								MStatus status=m_pFnMesh->setBoolBlindData(
										compID,
										component,
										m_blindDataID,
										m_attrName.c_str(),
										data);
								if(!status)
								{
									feLog("SurfaceAccessorMaya::set(... I32)"
											" index %d raw %d"
											" setBoolBlindData failed\n",
											a_index, compID);
								}
							}
							else
							{
								const I32 data=a_integer;
								MStatus status=m_pFnMesh->setIntBlindData(
										compID,
										component,
										m_blindDataID,
										m_attrName.c_str(),
										data);
								if(!status)
								{
									feLog("SurfaceAccessorMaya::set(... I32)"
											" index %d raw %d"
											" setIntBlindData failed\n",
											a_index, compID);
								}
							}

							if(m_spSurfaceAccessibleI.isValid())
							{
								m_spSurfaceAccessibleI->unlock(I64(this));
							}

							if(m_attrName=="POLY_FORMAT")
							{
								m_cachePolyFormat=PolyFormat(a_integer);
							}

							if(m_pIntegerCache)
							{
								m_pIntegerCache[a_index]=a_integer;
							}
						}
virtual	I32				integer(U32 a_index,U32 a_subIndex=0)
						{
							FEASSERT(m_pFnMesh);

							if(m_element==SurfaceAccessibleI::e_detail)
							{
								return getDetail(m_attrName).integer();
							}

							if(m_pIntegerCache)
							{
								return m_pIntegerCache[a_index];
							}

							const BWORD isGroup=
									(m_element==
									SurfaceAccessibleI::e_pointGroup ||
									m_element==
									SurfaceAccessibleI::e_primitiveGroup);
#if FALSE
							const BWORD useBool=isGroup;
#else
							const BWORD useBool=FALSE;
#endif

							checkBlindDataID(useBool? "boolean": "integer");

							if(isGroup && m_groupList.size())
							{
								FEASSERT(a_index<m_groupList.size());
								return (a_index>=m_groupList.size())?
										0: m_groupList[a_index];
							}

							if(m_element==
									SurfaceAccessibleI::e_primitiveGroup ||
									m_attribute==SurfaceAccessibleI::e_vertices)
							{
								FEASSERT(m_element==
										SurfaceAccessibleI::e_primitive ||
										m_element==
										SurfaceAccessibleI::e_primitiveGroup);

								I32 primitiveIndex=a_index;
								if(m_element==
										SurfaceAccessibleI::e_primitiveGroup &&
										m_attrName!="")
								{
									FEASSERT(a_index<m_groupList.size());
									if(a_index>=m_groupList.size())
									{
										return 0;
									}
									primitiveIndex=m_groupList[a_index];
								}

								U32 subs=m_pFnMesh->polygonVertexCount(
										primitiveIndex);
								FEASSERT(a_subIndex<subs);
								if(a_subIndex>=subs)
								{
									return 0;
								}

								MIntArray vertexList;
//								MStatus status=
								m_pFnMesh->getPolygonVertices(
										primitiveIndex,vertexList);

								const U32 raw=vertexList[a_subIndex];

								return logicalVertex(raw);
							}

							if(m_attribute==SurfaceAccessibleI::e_properties)
							{
								FEASSERT(m_element==
										SurfaceAccessibleI::e_primitive ||
										m_element==
										SurfaceAccessibleI::e_primitiveGroup);

								const PolyFormat format=polyFormat(a_index);
								return format==e_openPoly;
							}

							if(!m_pFnMesh->isBlindDataTypeUsed(m_blindDataID))
							{
#if FE_SAM_BLIND_DEBUG
								feLog("SurfaceAccessorMaya::integer"
										" \"%s\" blind data %d not used\n",
										m_attrName.c_str(),m_blindDataID);
#endif
								return 0;
							}
							const MFn::Type component=componentType();
							if(!m_pFnMesh->hasBlindData(component))
							{
#if FE_SAM_BLIND_DEBUG
								feLog("SurfaceAccessorMaya::integer"
										" no component blind data\n");
#endif
								return 0;
							}
							const I32 compID=rawVertex(a_index);
							if(!m_pFnMesh->hasBlindDataComponentId(compID,
									component))
							{
#if FE_SAM_BLIND_DEBUG
								feLog("SurfaceAccessorMaya::integer"
										" no blind data on"
										" index %d compID %d\n",a_index,compID);
#endif
								return 0;
							}
#if FALSE
							//* NOTE very very slow
							int data;
							MStatus status=m_pFnMesh->getIntBlindData(
									rawVertex(a_index),
									component,
									m_blindDataID,
									m_attrName.c_str(),
									data);
							if(!status)
							{
								feLog("SurfaceAccessorMaya::integer"
										" getIntBlindData failed\n");
								return 0;
							}
							return data;
#else
							const I32 size=count();

							MIntArray compIDs;
							MIntArray data;
							if(useBool)
							{
								MStatus status=m_pFnMesh->getBoolBlindData(
										component,
										m_blindDataID,
										m_attrName.c_str(),
										compIDs,
										data);
								if(!status)
								{
									feLog("SurfaceAccessorMaya::integer"
											" getBoolBlindData failed\n");
									return 0;
								}
							}
							else
							{
								MStatus status=m_pFnMesh->getIntBlindData(
										component,
										m_blindDataID,
										m_attrName.c_str(),
										compIDs,
										data);
								if(!status)
								{
									feLog("SurfaceAccessorMaya::integer"
											" getIntBlindData failed\n");
									return 0;
								}
							}

							m_pIntegerCache=new I32[size];
							for(I32 m=0;m<size;m++)
							{
								m_pIntegerCache[m]=0;
							}

							const I32 found=compIDs.length();
							for(I32 m=0;m<found;m++)
							{
								const I32 rawIndex=compIDs[m];
								FEASSERT(rawIndex>=0);

								const I32 index=
										logicalVertex(rawIndex);

								FEASSERT(index<size);
								m_pIntegerCache[index]=data[m];
							}

							return m_pIntegerCache[a_index];
#endif
						}
virtual	I32				duplicate(U32 index,U32 subIndex=0)
						{
							//* TODO
							return -1;
						}
virtual	I32				append(SurfaceAccessibleI::Form a_form)
						{
							if(m_element==SurfaceAccessibleI::e_point)
							{
								//* HACK doesn't work
								return -1;

								//* NOTE can be very inefficient
								//* TODO resize all at once?

								const MPoint origin(0.0,0.0,0.0);

								MPointArray vertexArray;
								m_pFnMesh->getPoints(vertexArray);

								feLog("SurfaceAccessorMaya::append was %d\n",
										vertexArray.length());

#if TRUE
								vertexArray.append(origin);

								m_pFnMesh->setPoints(vertexArray);
#else
								const int vertexId=count();
								m_pFnMesh->setPoint(vertexId,origin);
#endif

								feLog("SurfaceAccessorMaya::append now %d\n",
										count());

								return count()-1;
							}

							if(m_element==SurfaceAccessibleI::e_primitive)
							{
								const bool mergeVertices=false;

								MPointArray vertexArray;
								m_pFnMesh->addPolygon(
										(const MPointArray&)vertexArray,
										mergeVertices);

								return count()-1;
							}

							return -1;
						}
virtual	I32				append(I32 a_integer,SurfaceAccessibleI::Form a_form)
						{
							if(m_element!=SurfaceAccessibleI::e_primitive ||
									m_attribute!=SurfaceAccessibleI::e_vertices)
							{
								append(U32(0),a_integer);
								return 0;
							}

							/*** append a primitive with the specified
								number of vertices */
							MPointArray vertexArray;
							vertexArray.setLength(a_integer);

							//* default of non-coincident non-linear points
							for(I32 vertIndex=0;vertIndex<a_integer;vertIndex++)
							{
								vertexArray.set(vertIndex,0.01*vertIndex,0.0,
										0.001*vertIndex*vertIndex);
							}

							const bool mergeVertices=false;
							m_pFnMesh->addPolygon(
									(const MPointArray&)vertexArray,
									mergeVertices);

							return m_pFnMesh->numPolygons()-1;
						}
virtual	void			append(U32 a_index,I32 a_integer)
						{
							if(m_element==SurfaceAccessibleI::e_pointGroup ||
									m_element==
									SurfaceAccessibleI::e_primitiveGroup)
							{
								// a_index ignored

								set(a_integer,U32(0),I32(true));

								for(U32 m=0;m<m_groupList.size();m++)
								{
									if(m_groupList[m]==a_integer)
									{
										return;
									}
								}
								m_groupList.push_back(a_integer);
							}
						}

virtual	void			append(Array< Array<I32> >& a_rPrimVerts);

virtual	void			remove(U32 a_index,I32 a_integer)
						{
							if(m_element==SurfaceAccessibleI::e_pointGroup ||
									m_element==
									SurfaceAccessibleI::e_primitiveGroup)
							{
								set(a_index,U32(0),I32(false));
								for(U32 m=0;m<m_groupList.size();m++)
								{
									if(m_groupList[m]==I32(a_index))
									{
										m_groupList.erase(
												m_groupList.begin()+m);
									}
								}
							}
						}

virtual	void			set(U32 a_index,U32 a_subIndex,Real a_real)
						{
							FEASSERT(m_pFnMesh);

							if(m_element==SurfaceAccessibleI::e_detail)
							{
								String text;
								text.sPrintf("%.8G",a_real);
								setDetail(m_attrName,text);
								return;
							}

							if(m_element==SurfaceAccessibleI::e_detail)
							{
								//* try getting existing attribute
								MStatus status;
								MPlug plug=m_pFnMesh->findPlug(
										m_attrName.c_str(),status);

								if(status && !plug.isNull())
								{
									plug.setDouble(a_real);
									return;
								}

								//* create missing attribute
								MFnNumericAttribute numAttr;
								MObject attrObject=numAttr.create(
										m_attrName.c_str(),m_attrName.c_str(),
										MFnNumericData::kDouble,a_real,&status);
								if(!status)
								{
									feLog("SurfaceAccessorMaya::set(... Real)"
											" index %d failed to create"
											" detail attribute \"%s\""
											" because \"%s\"\n",
											a_index,m_attrName.c_str(),
											status.errorString().asChar());
								}

								status=m_pFnMesh->addAttribute(attrObject,
										MFnDependencyNode::kLocalDynamicAttr);
								if(!status)
								{
									feLog("SurfaceAccessorMaya::set(... Real)"
											" index %d failed to add"
											" detail attribute \"%s\""
											" because \"%s\"\n",
											a_index,m_attrName.c_str(),
											status.errorString().asChar());
								}

								return;
							}

							if(m_spSurfaceAccessibleI.isValid())
							{
								m_spSurfaceAccessibleI->lock(I64(this));
							}

							assureBlindDataID("real");

							const I32 compID=rawVertex(a_index);

							const double data=a_real;
							const MFn::Type component=componentType();
							MStatus status=m_pFnMesh->setDoubleBlindData(
									compID,
									component,
									m_blindDataID,
									m_attrName.c_str(),
									data);

							if(m_spSurfaceAccessibleI.isValid())
							{
								m_spSurfaceAccessibleI->unlock(I64(this));
							}

							if(!status)
							{
								feLog("SurfaceAccessorMaya::set(... Real)"
										" index %d compID %d"
										" setDoubleBlindData failed:"
										" \"%s\"\n",
										a_index, compID,
										status.errorString().asChar());
							}

							if(m_pRealCache)
							{
								m_pRealCache[a_index]=a_real;
							}
						}
virtual	Real			real(U32 a_index,U32 a_subIndex=0)
						{
							FEASSERT(m_pFnMesh);

							if(m_element==SurfaceAccessibleI::e_detail)
							{
								return getDetail(m_attrName).real();
							}

							if(m_pRealCache)
							{
								return m_pRealCache[a_index];
							}

							checkBlindDataID("real");

							if(!m_pFnMesh->isBlindDataTypeUsed(m_blindDataID))
							{
#if FE_SAM_BLIND_DEBUG
								feLog("SurfaceAccessorMaya::real"
										" \"%s\" blind data %d not used\n",
										m_attrName.c_str(),m_blindDataID);
#endif
								return 0.0;
							}

							const MFn::Type component=componentType();
							if(!m_pFnMesh->hasBlindData(component))
							{
#if FE_SAM_BLIND_DEBUG
								feLog("SurfaceAccessorMaya::real"
										" no component blind data\n");
#endif
								return 0.0;
							}

							const I32 compID=rawVertex(a_index);
							if(!m_pFnMesh->hasBlindDataComponentId(compID,
									component))
							{
#if FE_SAM_BLIND_DEBUG
								feLog("SurfaceAccessorMaya::real"
										" no blind data on"
										" index %d compID %d\n",a_index,compID);
#endif
								return 0.0;
							}

							const I32 size=count();

							MIntArray compIDs;
							MDoubleArray data;
							MStatus status=m_pFnMesh->getDoubleBlindData(
									component,
									m_blindDataID,
									m_attrName.c_str(),
									compIDs,
									data);
							if(!status)
							{
								feLog("SurfaceAccessorMaya::real"
										" getDoubleBlindData failed\n");
								return 0.0;
							}

							m_pRealCache=new Real[size];
							for(I32 m=0;m<size;m++)
							{
								m_pRealCache[m]=Real(0);
							}

							const I32 found=compIDs.length();
							for(I32 m=0;m<found;m++)
							{
								const I32 rawIndex=compIDs[m];
								FEASSERT(rawIndex>=0);

								const I32 index=
										logicalVertex(rawIndex);

								FEASSERT(index<size);
								m_pRealCache[index]=data[m];
							}

							return m_pRealCache[a_index];
						}

virtual	void			set(U32 a_index,U32 a_subIndex,
								const SpatialVector& a_vector)
						{
							FEASSERT(m_pFnMesh);

							if(m_element==SurfaceAccessibleI::e_detail)
							{
								String text;
								text.sPrintf("%.8G %.8G %.8G",
										a_vector[0],a_vector[1],a_vector[2]);
								setDetail(m_attrName,text);
								return;
							}

#if FALSE
							if(m_element==SurfaceAccessibleI::e_detail)
							{
								//* try getting existing attribute
								MStatus status;
								MPlug plug=m_pFnMesh->findPlug(
										m_attrName.c_str(),status);

								if(status && !plug.isNull())
								{
									const U32 size=plug.numElements();
									FEASSERT(size==3);
									for(U32 m=0;m<size && m<3;m++)
									{
										plug[m].setDouble(a_vector[m]);
									}
									return;
								}

								//* create missing attribute
								MFnNumericAttribute numAttr;
								MObject attrObject=numAttr.create(
										m_attrName.c_str(),m_attrName.c_str(),
										MFnNumericData::k3Double,0,&status);
								if(!status)
								{
									feLog("SurfaceAccessorMaya::"
											"set(... SpatialVector)"
											" index %d failed to create"
											" detail attribute \"%s\""
											" because \"%s\"\n",
											a_index,m_attrName.c_str(),
											status.errorString().asChar());
								}

								numAttr.setDefault((double)a_vector[0],
										(double)a_vector[1],
										(double)a_vector[2]);

								status=m_pFnMesh->addAttribute(attrObject,
										MFnDependencyNode::kLocalDynamicAttr);
								if(!status)
								{
									feLog("SurfaceAccessorMaya::"
											"set(... SpatialVector)"
											" index %d failed to add"
											" detail attribute \"%s\""
											" because \"%s\"\n",
											a_index,m_attrName.c_str(),
											status.errorString().asChar());
								}

								return;
							}
#endif

							if(m_element==SurfaceAccessibleI::e_pointGroup)
							{
								if(a_index>=m_groupList.size())
								{
									return;
								}
								a_index=m_groupList[a_index];
							}
							else if(m_element==
									SurfaceAccessibleI::e_primitiveGroup ||
									m_attribute==SurfaceAccessibleI::e_vertices)
							{
								if(m_element==
										SurfaceAccessibleI::e_primitiveGroup &&
									m_attrName!="")
								{
									if(a_index>=m_groupList.size())
									{
										return;
									}
									a_index=m_groupList[a_index];
								}

								FEASSERT(m_element==
										SurfaceAccessibleI::e_primitive ||
										m_element==
										SurfaceAccessibleI::e_primitiveGroup);

								const U32 subs=
										m_pFnMesh->polygonVertexCount(a_index);
								FEASSERT(a_subIndex<subs);
								if(a_subIndex>=subs)
								{
									return;
								}

								MIntArray vertexList;
								MStatus status=m_pFnMesh->getPolygonVertices(
										a_index,vertexList);

								SpatialVector local;
								if(m_useTransform)
								{
									transformVector(m_inverse,a_vector,local);
								}
								else
								{
									local=a_vector;
								}

								MPoint point(local[0],local[1],local[2]);

								const U32 raw=vertexList[a_subIndex];
								status=m_pFnMesh->setPoint(raw,point);

								const PolyFormat format=polyFormat(a_index);
								if(format==e_openPoly)
								{
									const U32 raw2=
											vertexList[subs-1-a_subIndex];
									status=m_pFnMesh->setPoint(raw2,point);
								}
								return;
							}

							// TODO set color

							if(m_attribute==SurfaceAccessibleI::e_position ||
									m_element==SurfaceAccessibleI::e_pointGroup)
							{
								SpatialVector local;
								if(m_useTransform)
								{
									transformVector(m_inverse,a_vector,local);
								}
								else
								{
									local=a_vector;
								}

								MPoint point(local[0],local[1],local[2]);

								const U32 raw=rawVertex(a_index);
								MStatus status=m_pFnMesh->setPoint(raw,point);
								const PolyFormat format=polyFormat(0);	// HACK
								if(format==e_openPoly)
								{
									const U32 twin=twinVertex(a_index);
									status=m_pFnMesh->setPoint(twin,point);
								}
								return;
							}

							//* TODO verify UV editing

							if(m_element==SurfaceAccessibleI::e_point &&
									m_attribute==SurfaceAccessibleI::e_uv)
							{
								MVector vector;
								const U32 raw=rawVertex(a_index);
								if(!m_faceVertTable.size())
								{
									initializeFaceVertArray();
								}
								Array<FaceVert>& rFaceVertArray=
										m_faceVertTable[raw];
								const U32 faceVertCount=rFaceVertArray.size();

								for(U32 faceVertIndex=0;
										faceVertIndex<faceVertCount;
										faceVertIndex++)
								{
									FaceVert& faceVert=
											rFaceVertArray[faceVertIndex];

									I32 uvId=0;
									m_pFnMesh->getPolygonUVid(faceVert.m_face,
											faceVert.m_vert,uvId);

									m_pFnMesh->setUV(uvId,
											a_vector[0],a_vector[1]);
								}
								return;
							}

							if(m_element==SurfaceAccessibleI::e_primitive &&
									m_attribute==SurfaceAccessibleI::e_uv)
							{
								MVector vector;
								const U32 raw=rawVertex(a_index);

								I32 uvId=0;
								m_pFnMesh->getPolygonUVid(raw,a_subIndex,uvId);

								m_pFnMesh->setUV(uvId,a_vector[0],a_vector[1]);
								return;
							}

							if(m_element==SurfaceAccessibleI::e_point &&
									m_attribute==SurfaceAccessibleI::e_normal)
							{
								MVector vector(a_vector[0],a_vector[1],
										a_vector[2]);

								const U32 raw=rawVertex(a_index);

								MStatus status;

								if(!m_faceVertTable.size())
								{
									MFloatVectorArray normals;
									status=m_pFnMesh->getNormals(normals);
									if(!normals.length())
									{
										feLog("SurfaceAccessorMaya::set"
												" (vector3)"
												" no normals (%d points)\n",
												count());
										return;
									}

									initializeFaceVertArray();

//									MPointArray vertexArray;
//									m_pFnMesh->getPoints(vertexArray);

									const U32 normalCount=
											m_faceVertTable.size();

//									feLog("SurfaceAccessorMaya::set (vector3)"
//											" normalCount %d %d\n",
//											normalCount,normals.length());

									m_pSpatialVectorCache=
											new SpatialVector[normalCount];
									m_pBooleanCache=
											new BWORD[normalCount];
									if(m_useTransform)
									{
										for(U32 normalIndex=0;
												normalIndex<normalCount;
												normalIndex++)
										{
											MFloatVector& normal=
													normals[normalIndex];
											rotateVector(m_transform,
													SpatialVector(
													normal[0],normal[1],
													normal[2]),
													m_pSpatialVectorCache
													[normalIndex]);

											m_pBooleanCache[normalIndex]=FALSE;
										}
									}
									else
									{
										for(U32 normalIndex=0;
												normalIndex<normalCount;
												normalIndex++)
										{
											MFloatVector& normal=
													normals[normalIndex];
											m_pSpatialVectorCache[normalIndex]=
													SpatialVector(normal[0],
													normal[1],normal[2]);

											m_pBooleanCache[normalIndex]=FALSE;
										}
									}
								}

								FEASSERT(raw<m_faceVertTable.size());
								m_pSpatialVectorCache[raw]=a_vector;
								m_pBooleanCache[raw]=TRUE;

								return;
							}

							if(m_spSurfaceAccessibleI.isValid())
							{
								m_spSurfaceAccessibleI->lock(I64(this));
							}

							assureBlindDataID("vector3");

							MPoint point(a_vector[0],a_vector[1],a_vector[2]);

							I32 compID=rawVertex(a_index);
							for(U32 n=0;n<3;n++)
							{
								const double data=point[n];
								String name;
								name.sPrintf("%s_%c",m_attrName.c_str(),'x'+n);

								const MFn::Type component=componentType();
								MStatus status=m_pFnMesh->setDoubleBlindData(
										compID,
										component,
										m_blindDataID,
										name.c_str(),
										data);
								if(!status)
								{
									feLog("SurfaceAccessorMaya::set"
											"(... SpatialVector)"
											" index %d %d compID %d"
											" setDoubleBlindData failed:"
											" \"%s\"\n",
											a_index,n,compID,
											status.errorString().asChar());
								}
							}

							if(m_spSurfaceAccessibleI.isValid())
							{
								m_spSurfaceAccessibleI->unlock(I64(this));
							}

							if(m_pSpatialVectorCache)
							{
								m_pSpatialVectorCache[a_index]=a_vector;
							}
						}
virtual	SpatialVector	spatialVector(U32 a_index,U32 a_subIndex=0)
						{
							FEASSERT(m_pFnMesh);

							if(m_element==SurfaceAccessibleI::e_detail)
							{
								const String text=getDetail(m_attrName);

								float x,y,z;
								sscanf(text.c_str(),"%f %f %f",&x,&y,&z);

								return SpatialVector(x,y,z);
							}

							if(m_pSpatialVectorCache)
							{
								return m_pSpatialVectorCache[a_index];
							}

							MPoint point;

							if(m_element==SurfaceAccessibleI::e_pointGroup)
							{
								if(a_index>=m_groupList.size())
								{
									return SpatialVector(0.0,0.0,0.0);
								}
								const U32 raw=rawVertex(m_groupList[a_index]);

//								MStatus status=
										m_pFnMesh->getPoint(raw,point);
							}
							else if(m_element==
									SurfaceAccessibleI::e_primitiveGroup ||
									m_attribute==SurfaceAccessibleI::e_vertices)
							{
								if(m_element==
										SurfaceAccessibleI::e_primitiveGroup &&
									m_attrName!="")
								{
									if(a_index>=m_groupList.size())
									{
										return SpatialVector(0.0,0.0,0.0);
									}
									a_index=m_groupList[a_index];
								}

								FEASSERT(m_element==
										SurfaceAccessibleI::e_primitive ||
										m_element==
										SurfaceAccessibleI::e_primitiveGroup);

								const U32 subs=
										m_pFnMesh->polygonVertexCount(a_index);
								FEASSERT(a_subIndex<subs);
								if(a_subIndex>=subs)
								{
									return SpatialVector(0.0,0.0,0.0);
								}

								MIntArray vertexList;
								MStatus status=m_pFnMesh->getPolygonVertices(
										a_index,vertexList);

								const U32 raw=vertexList[a_subIndex];
								status=m_pFnMesh->getPoint(raw,point);
							}
							else if(m_attribute==SurfaceAccessibleI::e_position)
							{
								const U32 raw=rawVertex(a_index);
//								MStatus status=
								m_pFnMesh->getPoint(raw,point);
							}
							else if(m_element==SurfaceAccessibleI::e_point &&
									(m_attribute==SurfaceAccessibleI::e_normal
									|| m_attribute==SurfaceAccessibleI::e_uv))
							{
								MVector vector;
								const U32 raw=rawVertex(a_index);
								if(!m_faceVertTable.size())
								{
									initializeFaceVertArray();
								}
								Array<FaceVert>& rFaceVertArray=
										m_faceVertTable[raw];
								const U32 faceVertCount=rFaceVertArray.size();
								if(!faceVertCount)
								{
									return SpatialVector(0.0,0.0,0.0);
								}

								//* NOTE only looking at the first
								FaceVert& faceVert=rFaceVertArray[0];

								if(m_attribute==SurfaceAccessibleI::e_uv)
								{
									I32 uvId=0;
									m_pFnMesh->getPolygonUVid(faceVert.m_face,
											faceVert.m_vert,uvId);

									float u=0.0;
									float v=0.0;
									m_pFnMesh->getUV(uvId,u,v);

									return SpatialVector(u,v,0.0);
								}

//								MStatus status=
								m_pFnMesh->getFaceVertexNormal(
										faceVert.m_face,raw,vector);

								if(!m_useTransform)
								{
									return SpatialVector(vector[0],
											vector[1],vector[2]);
								}

								SpatialVector world;
								rotateVector(m_transform,
										SpatialVector(vector[0],
										vector[1],vector[2]),world);
								return world;
							}
//							else if(m_element==SurfaceAccessibleI::e_point &&
//									m_attribute==SurfaceAccessibleI::e_uv)
//							{
//								const U32 raw=rawVertex(a_index);
//								MStatus status=m_pFnMesh->getPoint(raw,point);
//
//								//* NOTE don't know which polygon (arbitrary)
//								float2 uvPoint;
//								status=m_pFnMesh->getUVAtPoint(point,uvPoint);
//
//								return SpatialVector(uvPoint[0],uvPoint[1],0.0);
//							}
							else if(m_element==SurfaceAccessibleI::e_primitive
									&& m_attribute==SurfaceAccessibleI::e_uv)
							{
								MVector vector;
								const U32 raw=rawVertex(a_index);

								I32 uvId=0;
								m_pFnMesh->getPolygonUVid(raw,a_subIndex,uvId);

								float u=0.0;
								float v=0.0;
								m_pFnMesh->getUV(uvId,u,v);

								return SpatialVector(u,v,0.0);
							}
							else
							{
								// TODO get color

								checkBlindDataID("vector3");

								if(!m_pFnMesh->isBlindDataTypeUsed(
										m_blindDataID))
								{
#if FE_SAM_BLIND_DEBUG
									feLog("SurfaceAccessorMaya::spatialVector"
											" \"%s\" blind data %d not used\n",
											m_attrName.c_str(),m_blindDataID);
#endif
									return SpatialVector(0.0,0.0,0.0);
								}

								const MFn::Type component=componentType();
								if(!m_pFnMesh->hasBlindData(component))
								{
#if FE_SAM_BLIND_DEBUG
									feLog("SurfaceAccessorMaya::spatialVector"
											" no component blind data\n");
#endif
									return SpatialVector(0.0,0.0,0.0);
								}

								if(!m_pFnMesh->hasBlindDataComponentId(
										rawVertex(a_index),component))
								{
#if FE_SAM_BLIND_DEBUG
									feLog("SurfaceAccessorMaya::spatialVector"
											" no blind data on"
											" index %d (%d)\n",
											a_index,rawVertex(a_index));
#endif
									return SpatialVector(0.0,0.0,0.0);
								}

								const I32 size=count();
								m_pSpatialVectorCache=new SpatialVector[size];
								for(I32 m=0;m<size;m++)
								{
									fe::set(m_pSpatialVectorCache[m]);
								}

								for(U32 n=0;n<3;n++)
								{
									String name;
									name.sPrintf("%s_%c",
											m_attrName.c_str(),'x'+n);

									MIntArray compIDs;
									MDoubleArray data;
									MStatus status=
											m_pFnMesh->getDoubleBlindData(
											component,
											m_blindDataID,
											name.c_str(),
											compIDs,
											data);
									if(!status)
									{
										feLog("SurfaceAccessorMaya::"
												"spatialVector"
												" getDoubleBlindData failed\n");

										delete m_pSpatialVectorCache;
										m_pSpatialVectorCache=NULL;

										return SpatialVector(0.0,0.0,0.0);
									}

									const I32 found=compIDs.length();
									for(I32 m=0;m<found;m++)
									{
										const I32 rawIndex=compIDs[m];
										FEASSERT(rawIndex>=0);

										const I32 index=
												logicalVertex(rawIndex);

										FEASSERT(index<size);
										m_pSpatialVectorCache[index][n]=
												data[m];
									}
								}

								return m_pSpatialVectorCache[a_index];
							}

							SpatialVector world(point[0],point[1],point[2]);
							if(m_useTransform)
							{
								transformVector(m_transform,world,world);
							}

							return world;
						}

						//* Maya specific
		void			setMeshNode(const MObject a_meshNode)
						{
							m_meshNode=a_meshNode;
						}
		void			setMeshData(const MDataHandle a_meshData)
						{
							m_meshData=a_meshData;
							resetMark();

							if(m_pFnMesh)
							{
								delete m_pFnMesh;
							}

							//* not thread safe
							m_pFnMesh=new MFnMesh(m_meshData.asMesh());

							m_cachePolyFormat=PolyFormat(e_unknown);
							polyFormat(0);	// HACK set cache
						}
const	MDataHandle		meshData(void)
						{	return m_meshData; }
		void			setGroupIt(MItGeometry* a_pGroupIt)
						{	m_pGroupIt=a_pGroupIt; }

		void			clearTransform(void)
						{
							m_useTransform=FALSE;
						}
		void			setTransform(SpatialTransform a_transform)
						{
							m_useTransform=TRUE;
							m_transform=a_transform;
							invert(m_inverse,m_transform);
						}

						//* TODO move to a shared base class
		void			setMaster(sp<Master> a_spMaster)
						{	m_spMaster=a_spMaster; }

static	String			typeOfFormat(String a_format);

static	MFn::Type		componentTypeOfElement(
								SurfaceAccessibleI::Element a_element)
						{
							/*
								kMeshEdgeComponent
								kMeshPolygonComponent

								kMeshVertComponent
								kMeshFaceVertComponent
							*/
							switch (a_element)
							{
								case SurfaceAccessibleI::e_point:
								case SurfaceAccessibleI::e_pointGroup:
									return MFn::kMeshVertComponent;

								case SurfaceAccessibleI::e_vertex:
									//* TODO verify
									return MFn::kMeshFaceVertComponent;

								case SurfaceAccessibleI::e_primitive:
								case SurfaceAccessibleI::e_primitiveGroup:
									return MFn::kMeshPolygonComponent;

								case SurfaceAccessibleI::e_detail:
									//* HACK store on first point
//									return MFn::kMeshVertComponent;

								default:
									;
							}
							return MFn::kInvalid;
						}

static	String			readDetailAt(MDataHandle a_meshData,I32 a_id);
static	void			writeDetailAt(MDataHandle a_meshData,I32 a_id,
								const String& a_rString);
static	void			removeDetailAt(MDataHandle a_meshData,I32 a_id);
static	BWORD			removeDetailFor(MDataHandle a_meshData,
								String a_name);
static	I32				findDetailKey(MDataHandle a_meshData,
								const String& a_rKey,BWORD a_createMissing);
static	void			dumpDetail(MDataHandle a_meshData);

	private:
virtual	BWORD			bindInternal(SurfaceAccessibleI::Element a_element,
								const String& a_name)
						{
							m_element=SurfaceAccessibleI::e_point;
							if(a_element>=0 && a_element<=5)
							{
								m_element=a_element;
							}
							m_blindDataID= -1;
							m_attrName=a_name;
							updateGroupList();

							return TRUE;	//* TODO only TRUE if exists
						}

		void			cacheShaders(void);
		void			cacheTexture(String a_textureName);
		void			updateGroupList(void);

		void			checkBlindDataID(const String& a_attrType)
						{
#if FE_SAM_BLIND_DEBUG
							feLog("SurfaceAccessorMaya::checkBlindDataID"
									" attr %d type \"%s\"\n",
									m_attribute,a_attrType.c_str());
#endif

							if(m_attribute==SurfaceAccessibleI::e_generic &&
									m_blindDataID<0)
							{
								m_attrType=a_attrType;
								if(!m_attrName.empty())
								{
									const String attrKey=
											m_attrName+":"+m_attrType;
									m_blindDataID=
											m_spMaster->catalog()
											->catalogOrDefault<I32>(
											"MayaBlindID",attrKey,-1);
#if FE_SAM_BLIND_DEBUG
									feLog("SurfaceAccessorMaya::"
											"checkBlindDataID"
											" found id %d for \"%s\"\n",
											m_blindDataID,attrKey.c_str());
#endif
								}
							}
						}
		void			assureBlindDataID(const String& a_attrType)
						{
#if FE_SAM_BLIND_DEBUG
							feLog("SurfaceAccessorMaya::assureBlindDataID"
									" attr %d type \"%s\"\n",
									m_attribute,a_attrType.c_str());
#endif

							checkBlindDataID(a_attrType);
							if(m_attribute==SurfaceAccessibleI::e_generic &&
									m_blindDataID<0)
							{
								m_blindDataID=createBlindDataID(
										m_attrName,m_attrType);
#if FE_SAM_BLIND_DEBUG
								feLog("SurfaceAccessorMaya::assureBlindDataID"
										" created new id %d for \"%s\"\n",
										m_blindDataID,m_attrName.c_str());
#endif
							}
						}
		String			getBlindFormat(String a_attrName);
		I32				getBlindDataID(String a_attrName,String a_attrType,
								BWORD& a_rFound);
		I32				createBlindDataID(String a_attrName,String a_attrType);

		void			checkPolyFormatID(void)
						{
							if(m_polyFormatID<0)
							{
								const String attrKey="POLY_FORMAT:I32";
								m_polyFormatID=
										m_spMaster->catalog()->catalog<I32>(
										"MayaBlindID",attrKey,-1);
							}
						}
		void			assurePolyFormatID(void)
						{
							checkPolyFormatID();
							if(m_polyFormatID<0)
							{
								m_polyFormatID=createBlindDataID(
										"POLY_FORMAT","integer");
							}

							//* This is apparently needed before hasBlindData()
							//* to wake it up or something.
							//* It doesn't have to be the same one we check.
							FEASSERT(m_pFnMesh);
							I32 probeInt= -1;
							m_pFnMesh->getIntBlindData(0,
									MFn::kMeshPolygonComponent,
									m_polyFormatID, "POLY_FORMAT",
									probeInt);
						}
		PolyFormat		polyFormat(U32 a_index) const
						{
							//* TODO cache all polys, like with m_pIntegerCache

							if(m_cachePolyFormat!=e_unknown)
							{
								return m_cachePolyFormat;
							}

							const_cast<SurfaceAccessorMaya*>(this)
									->assurePolyFormatID();

							PolyFormat format=e_unknown;

							I32 data=0;
							MStatus status=m_pFnMesh->getIntBlindData(
									a_index,
									MFn::kMeshPolygonComponent,
									m_polyFormatID,
									"POLY_FORMAT",
									data);
							if(status)
							{
								format=PolyFormat(data);
							}
							else
							{
								feLog("SurfaceAccessorMaya::polyFormat"
										" getIntegerBlindData failed\n");
							}

							if(format==e_unknown)
							{
								format=guessPolyFormat();
							}
							if(format==e_unknown)
							{
								format=e_closedPoly;
							}

							const_cast<SurfaceAccessorMaya*>(this)->
									m_cachePolyFormat=format;
							return m_cachePolyFormat;
						}

		PolyFormat		guessPolyFormat(void) const;

		U32				polyVertCount(U32 a_index) const
						{
							FEASSERT(m_pFnMesh);

							const U32 rawCount=
									m_pFnMesh->polygonVertexCount(a_index);
							const PolyFormat format=polyFormat(a_index);
							switch(format)
							{
								case e_unknown:
								case e_closedPoly:
									return rawCount;
								case e_openPoly:
									return rawCount>>1;
								case e_freePoint:
									return rawCount? 1: 0;
								default:
									;
							}

							return 0;
						}
		U32				vertexCount(void) const
						{
							//* wake up blind data
							polyFormat(0);

							if(!m_pFnMesh->hasBlindData(
									MFn::kMeshPolygonComponent,m_polyFormatID))
							{
#if FALSE
								if(m_pFnMesh->numVertices()!=countVertices())
								{
									feLog("SurfaceAccessorMaya::vertexCount"
											" \"%s\""
											" bogus numVertices %d"
											" vs counted %d\n",
											m_attrName.c_str(),
											m_pFnMesh->numVertices(),
										countVertices());

									const U32 primitiveCount=
											m_pFnMesh->numPolygons();
									for(U32 primitiveIndex=0;
											primitiveIndex<primitiveCount;
											primitiveIndex++)
									{
										feLog("  prim %d/%d\n",
												primitiveIndex,primitiveCount);

										MIntArray vertexList;
										m_pFnMesh->getPolygonVertices(
												primitiveIndex,vertexList);

										const U32 subCount=
												m_pFnMesh->polygonVertexCount(
												primitiveIndex);
										for(U32 subIndex=0;
												subIndex<subCount;subIndex++)
										{
											const U32 raw=
													vertexList[subIndex];

											feLog("    sub %d/%d index %d\n",
													subIndex,subCount,raw);
										}
									}

									return countVertices();
								}
#endif

								return m_pFnMesh->numVertices();
							}
							//* TODO account for potentially shared points
							return countVertices();
						}
		U32				countVertices(void) const
						{
							// TODO cache
							U32 sum=0;
							U32 primitiveCount=m_pFnMesh->numPolygons();
							for(U32 index=0;index<primitiveCount;index++)
							{
								sum+=polyVertCount(index);
							}
							return sum;
						}
		void			resetMark(void)
						{
							m_markPoly=0;
							m_markVertex=0;
							m_markRawVertex=0;
						}
		U32				logicalVertex(U32 a_rawVertex)
						{
							//* wake up blind data
							polyFormat(0);
#if FALSE
							if(!m_pFnMesh->hasBlindData(
									MFn::kMeshPolygonComponent,m_polyFormatID))
							{
								return a_rawVertex;
							}
#else
							if(m_cachePolyFormat!=e_openPoly)
							{
								return a_rawVertex;
							}
#endif
							while(TRUE)
							{
								if(a_rawVertex<m_markRawVertex)
								{
									if(m_markPoly<1)
									{
										feX("SurfaceAccessorMaya::rawVertex",
												"mark data corrupt");
									}
//									feLog("logicalVertex %d poly %d dec\n",
//											a_rawVertex,m_markPoly);
									m_markPoly--;
									m_markVertex-=polyVertCount(m_markPoly);
									m_markRawVertex-=
											m_pFnMesh->polygonVertexCount(
											m_markPoly);
									continue;
								}
								const U32 subIndex=a_rawVertex-m_markRawVertex;
								const U32 subs=polyVertCount(m_markPoly);
								if(subIndex<subs)
								{
									return m_markVertex+subIndex;
								}
								else if(subIndex<subs*2)
								{
									return m_markVertex+(subs*2-1-subIndex);
								}
								const U32 primitiveCount=
										m_pFnMesh->numPolygons();
//								feLog("logicalVertex %d poly %d/%d inc\n",
//										a_rawVertex,
//											m_markPoly,primitiveCount);
								if(m_markPoly>=primitiveCount-1)
								{
									feX("SurfaceAccessorMaya::logicalVertex",
											"excessive point index");
								}
								m_markVertex+=polyVertCount(m_markPoly);
								m_markRawVertex+=
										m_pFnMesh->polygonVertexCount(
										m_markPoly);
								m_markPoly++;
							}
						}
		U32				rawVertex(U32 a_logicalVertex)
						{
							const PolyFormat format=polyFormat(a_logicalVertex);

							if(format!=e_openPoly)
							{
								return a_logicalVertex;
							}

							while(TRUE)
							{
								if(a_logicalVertex<m_markVertex)
								{
									if(m_markPoly<1)
									{
										feX("SurfaceAccessorMaya::rawVertex",
												"mark data corrupt");
									}
//									feLog("rawVertex %d poly %d dec\n",
//											a_logicalVertex,m_markPoly);
									m_markPoly--;
									m_markVertex-=polyVertCount(m_markPoly);
									m_markRawVertex-=
											m_pFnMesh->polygonVertexCount(
											m_markPoly);
									continue;
								}
								const U32 subIndex=a_logicalVertex-m_markVertex;
								const U32 subs=polyVertCount(m_markPoly);
								if(subIndex<subs)
								{
									return m_markRawVertex+subIndex;
								}
								const U32 primitiveCount=
										m_pFnMesh->numPolygons();
//								feLog("rawVertex %d poly %d/%d inc\n",
//										a_logicalVertex,
//										m_markPoly,primitiveCount);
								if(m_markPoly>=primitiveCount-1)
								{
									feX("SurfaceAccessorMaya::rawVertex",
											"excessive point index");
								}
								m_markVertex+=polyVertCount(m_markPoly);
								m_markRawVertex+=
										m_pFnMesh->polygonVertexCount(
										m_markPoly);
								m_markPoly++;
							}
						}
		U32				twinVertex(U32 a_logicalVertex)
						{
							rawVertex(a_logicalVertex);
							const U32 subIndex=a_logicalVertex-m_markVertex;
							const U32 subs=m_pFnMesh->polygonVertexCount(
											m_markPoly);

							MIntArray vertexList;
//							MStatus status=
									m_pFnMesh->getPolygonVertices(
									m_markPoly,vertexList);

							return vertexList[subs-1-subIndex];
						}

		MFn::Type		componentType(void)
						{
							return componentTypeOfElement(m_element);
						}

		void			setDetail(const String& a_rKey,const String& a_rValue);
		String			getDetail(const String& a_rKey);

		void			initializeFaceVertArray(void);

	class FaceVert
	{
		public:
					FaceVert(void):
						m_face(-1),
						m_vert(-1)											{}

					FaceVert(I32 a_face,I32 a_vert):
						m_face(a_face),
						m_vert(a_vert)										{}

			I32		m_face;
			I32		m_vert;
	};

		Array< Array<FaceVert> >	m_faceVertTable;

									//* cache last raw vertex lookup
		U32							m_markPoly;
		U32							m_markVertex;
		U32							m_markRawVertex;

		PolyFormat					m_cachePolyFormat;	// TODO mixed

		sp<Master>					m_spMaster;
		MObject						m_meshNode;
		MDataHandle					m_meshData;
		MFnMesh*					m_pFnMesh;
		I32							m_blindDataID;
		I32							m_polyFormatID;

		MItGeometry*				m_pGroupIt;
		Array<I32>					m_groupList;

		BWORD						m_useTransform;
		SpatialTransform			m_transform;
		SpatialTransform			m_inverse;

		String*						m_pStringCache;
		I32*						m_pIntegerCache;
		Real*						m_pRealCache;
		SpatialVector*				m_pSpatialVectorCache;
		BWORD*						m_pBooleanCache;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __maya_SurfaceAccessorMaya_h__ */
