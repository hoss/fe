/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <maya/maya.pmh>

using namespace fe;
using namespace fe::ext;

int main(int argc,char** argv)
{
	UNIT_START();

	BWORD complete=FALSE;

	try
	{
		MayaContext mayaContext;
		sp<Master> spMaster=mayaContext.master();
		feLog("spMaster 0x%p\n",spMaster.raw());

		UNIT_TEST(spMaster.isValid());

		MayaContext mayaContext2;
		sp<Master> spMaster2=mayaContext2.master();
		feLog("spMaster2 0x%p\n",spMaster.raw());

		UNIT_TEST(spMaster2.isValid());
		UNIT_TEST(spMaster==spMaster2);

		complete=TRUE;
	}
	catch(Exception &e) { e.log(); }
	catch(...)			{ feLog("uncaught exception\n"); }

	UNIT_TEST(complete);
	UNIT_TEST(!Counted::trackerCount());
	UNIT_TRACK(5);
	UNIT_RETURN();
}
