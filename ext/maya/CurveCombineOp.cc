/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <maya/maya.pmh>

#define FE_CCO_DEBUG		FALSE

using namespace fe;
using namespace fe::ext;

void CurveCombineOp::initialize(void)
{
	catalog<String>("icon")="FE_alpha";

	catalog< sp<Component> >("Input Surface");
	catalog<String>("Input Surface","implementation")="curve";
	catalog<bool>("Input Surface","array")=true;

	catalog< sp<Component> >("Output Surface");
	catalog<String>("Output Surface","IO")="output";
}

void CurveCombineOp::handle(Record& a_rSignal)
{
#if FE_CCO_DEBUG
	feLog("CurveCombineOp::handle\n");
#endif

	sp<DrawI> spDrawI;
	if(!accessDraw(spDrawI,a_rSignal)) return;

	sp<SurfaceAccessibleI> spSurfaceOutput;
	if(!accessOutput(spSurfaceOutput,a_rSignal)) return;

	sp<SurfaceAccessibleI> spSurfaceInput=
			catalog< sp<Component> >("Input Surface");
	if(spSurfaceInput.isNull())
	{
		catalog<String>("error")+="Inaccessible input;";
		return;
	}

	sp<SurfaceAccessibleMaya> spSurfaceAccessibleMaya=spSurfaceInput;
	if(spSurfaceAccessibleMaya.isNull())
	{
		catalog<String>("error")+="Incompatible input;";
		return;
	}

	const MDataHandle meshData=spSurfaceAccessibleMaya->meshData();
	MArrayDataHandle arrayData(meshData);
	const U32 curveCount=arrayData.elementCount();

#if FE_CCO_DEBUG
	feLog("CurveCombineOp::handle curveCount %d\n",curveCount);
#endif

	for(U32 curveIndex=0;curveIndex<curveCount;curveIndex++)
	{
		MDataHandle nurbsData=arrayData.outputValue();
		MObject nurbsObject=nurbsData.asNurbsCurve();
		MFnNurbsCurve nurbsCurve(nurbsObject);

		const I32 vertexCount=nurbsCurve.numCVs();
#if FE_CCO_DEBUG
		feLog("  curve %d/%d as %d degree %d  form %d vertexCount %d\n",
				curveIndex,curveCount,arrayData.elementIndex(),
				nurbsCurve.degree(),nurbsCurve.form(),vertexCount);

		const I32 knotCount=nurbsCurve.numKnots();
		for(I32 knotIndex=0;knotIndex<knotCount;knotIndex++)
		{
			feLog("    knot %d/%d %.6G\n",knotIndex,knotCount,
					nurbsCurve.knot(knotIndex));
		}
#endif

		SpatialVector* pointArray=new SpatialVector[vertexCount];

		for(I32 vertexIndex=0;vertexIndex<vertexCount;vertexIndex++)
		{
			MPoint mPoint;
			nurbsCurve.getCV(vertexIndex,mPoint);

			pointArray[vertexIndex]=
					SpatialVector(mPoint[0],mPoint[1],mPoint[2]);

#if FE_CCO_DEBUG
			feLog("    cv %d/%d point %s\n",vertexIndex,vertexCount,
					c_print(pointArray[vertexIndex]));
#endif
		}

		spDrawI->drawLines(pointArray,NULL,vertexCount,
				DrawI::e_strip,false,NULL);

		delete[] pointArray;

		arrayData.next();
	}

#if FE_CCO_DEBUG
	feLog("CurveCombineOp::handle done\n");
#endif
}
