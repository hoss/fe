/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <maya/maya.pmh>

#define FE_MSO_DEBUG		FALSE

using namespace fe;
using namespace fe::ext;

MeshSeparateOp::StringNumber& MeshSeparateOp::StringNumber::operator=(
	String a_string)
{
	m_prefix=a_string.replace("(.*?)[0-9]+$","\\1");

	const U32 prefixLength=m_prefix.length();

	m_hasNumber=(prefixLength<a_string.length());

	if(!m_hasNumber)
	{
		m_number=0;
		return *this;
	}

	const String digits=a_string.c_str()+prefixLength;

	m_number=digits.integer();

	return *this;
}

fe::String MeshSeparateOp::StringNumber::print(void)
{
	if(!m_hasNumber)
	{
		return m_prefix;
	}

	String result;
	result.sPrintf("%s%d",m_prefix.c_str(),m_number);
	return result;
}

bool MeshSeparateOp::StringNumber::Sort::operator()(
	const StringNumber& rStringNumber1,
	const StringNumber& rStringNumber2)
{
	const I32 comparison=
			rStringNumber1.m_prefix.compare(rStringNumber2.m_prefix);
	if(comparison!=0)
	{
		return (comparison<0);
	}

	if(!rStringNumber1.m_hasNumber || !rStringNumber2.m_hasNumber)
	{
		return rStringNumber2.m_hasNumber;
	}

	return (rStringNumber1.m_number<rStringNumber2.m_number);
}

void MeshSeparateOp::initialize(void)
{
	catalog<String>("icon")="FE_alpha";

	catalog<String>("FragmentAttr")="part";
	catalog<String>("FragmentAttr","label")="Fragment Attr";
	catalog<String>("FragmentAttr","hint")=
			"Name of string attribute describing membership"
			" in a collection of primitives.";

	catalog< sp<Component> >("Input Surface");

	catalog< sp<Component> >("Output Surface");
	catalog<String>("Output Surface","IO")="output";
	catalog<bool>("Output Surface","array")=true;
}

void MeshSeparateOp::handle(Record& a_rSignal)
{
#if FE_MSO_DEBUG
	feLog("MeshSeparateOp::handle\n");
#endif

	const String fragmentAttr=catalog<String>("FragmentAttr");

	sp<SurfaceAccessibleI> spInputAccessible;
	if(!access(spInputAccessible,"Input Surface")) return;

	sp<SurfaceAccessorI> spInputPoints;
	if(!access(spInputPoints,spInputAccessible,
			e_point,e_position)) return;

	sp<SurfaceAccessorI> spInputVertices;
	if(!access(spInputVertices,spInputAccessible,
			e_primitive,e_vertices)) return;

	sp<SurfaceAccessibleI> spOutputAccessible;
	if(!accessOutput(spOutputAccessible,a_rSignal))
	{
		catalog<String>("error")+="Inaccessible output;";
		return;
	}

	sp<SurfaceAccessibleMaya> spSurfaceAccessibleMaya=spOutputAccessible;
	if(spSurfaceAccessibleMaya.isNull())
	{
		catalog<String>("error")+="Incompatible output;";
		return;
	}

	MObject rootObject=spSurfaceAccessibleMaya->meshNode();

	MFnDagNode dagNode(rootObject);
	const String dagName=dagNode.fullPathName().asChar();

#if FE_MSO_DEBUG
	feLog("MeshSeparateOp::handle bound %d root \"%s\" null %d\n",
			spOutputAccessible->isBound(),dagName.c_str(),
			rootObject==MObject::kNullObj);
#endif

	MStatus status;

	const String group="";
	spInputVertices->fragmentWith(SurfaceAccessibleI::e_primitive,
			fragmentAttr,group);
	const I32 fragmentCount=spInputVertices->fragmentCount();

#if FE_MSO_DEBUG
	feLog("MeshSeparateOp::handle fragmentCount %d\n",fragmentCount);
#endif

	MArrayDataHandle& rArrayData=spSurfaceAccessibleMaya->meshArrayData();
	rArrayData.jumpToArrayElement(0);

	const U32 arrayCount=rArrayData.elementCount();

	MArrayDataBuilder builder=rArrayData.builder();

#if FE_MSO_DEBUG
	feLog("MeshSeparateOp::handle builderCount %d\n",builder.elementCount());
#endif

	//* already starts with first one?
	for(U32 fragmentIndex=arrayCount;fragmentIndex<fragmentCount;
			fragmentIndex++)
	{
		builder.addElement(fragmentIndex);
	}

	Array<String> fragmentName(fragmentCount);

#if FALSE
	for(U32 fragmentIndex=0;fragmentIndex<fragmentCount;fragmentIndex++)
	{
		fragmentName[fragmentIndex]=spInputVertices->fragment(fragmentIndex);
	}

	//* TODO move this general Sort to String
	std::sort(fragmentName.begin(),fragmentName.end(),StringSort());
#else
	Array<StringNumber> fragmentSorter(fragmentCount);

	for(U32 fragmentIndex=0;fragmentIndex<fragmentCount;fragmentIndex++)
	{
		fragmentSorter[fragmentIndex]=spInputVertices->fragment(fragmentIndex);
	}

	std::sort(fragmentSorter.begin(),fragmentSorter.end(),StringNumber::Sort());

	for(U32 fragmentIndex=0;fragmentIndex<fragmentCount;fragmentIndex++)
	{
		fragmentName[fragmentIndex]=fragmentSorter[fragmentIndex].print();
	}

#endif

	const U32 childCount=dagNode.childCount(&status);
	for(U32 childIndex=0;childIndex<childCount;childIndex++)
	{
		//* exact naming
		MObject child=dagNode.child(childIndex,&status);
		MFnDagNode dagChild(child);
		const String childName(dagChild.name().asChar());

//		feLog("child %d/%d \"%s\"\n",
//				childIndex,childCount,childName.c_str());

		if(childIndex<fragmentCount)
		{
//			feLog("  fragment \"%s\"\n",
//					fragmentName[childIndex].c_str());
			dagChild.setName(fragmentName[childIndex].c_str());
		}
	}

	//* push changes back to array
	rArrayData.set(builder);
	rArrayData.setAllClean();
	rArrayData.jumpToArrayElement(0);

#if FE_MSO_DEBUG
	feLog("MeshSeparateOp::handle builderCount %d arrayCount %d\n",
			builder.elementCount(),rArrayData.elementCount());
#endif

	for(U32 fragmentIndex=0;fragmentIndex<fragmentCount;fragmentIndex++)
	{
		const String fragment=fragmentName[fragmentIndex];

#if FE_MSO_DEBUG
		feLog("MeshSeparateOp::handle %d/%d element %d \"%s\"\n",
				fragmentIndex,fragmentCount,rArrayData.elementIndex(),
				fragment.c_str());
#endif

		sp<SurfaceAccessibleI::FilterI> spFilter;
		if(!spInputVertices->filterWith(fragment,spFilter) ||
				spFilter.isNull())
		{
			continue;
		}

		const I32 filterCount=spFilter->filterCount();
		if(!filterCount)
		{
			continue;
		}

		I32 numConnects=0;
		I32 numPolygons=filterCount;
		MIntArray polygonCounts(numPolygons);

		for(I32 filterIndex=0;filterIndex<filterCount;filterIndex++)
		{
			const I32 primitiveIndex=spFilter->filter(filterIndex);

			const U32 subCount=spInputVertices->subCount(primitiveIndex);

			numConnects+=subCount;
			polygonCounts[filterIndex]=subCount;
		}

		MIntArray polygonConnects(numConnects);

		std::set<I32> pointSet;

		I32 connectIndex=0;

		for(I32 filterIndex=0;filterIndex<filterCount;filterIndex++)
		{
			const I32 primitiveIndex=spFilter->filter(filterIndex);

			const I32 subCount=spInputVertices->subCount(primitiveIndex);

			for(I32 subIndex=0;subIndex<subCount;subIndex++)
			{
				const I32 pointIndex=
						spInputVertices->integer(primitiveIndex,subIndex);

				polygonConnects[connectIndex++]=pointIndex;
				pointSet.insert(pointIndex);

			}
		}

		FEASSERT(connectIndex==numConnects);

		std::map<I32,I32> fragPointMap;

		const I32 fragPointCount=pointSet.size();
		MFloatPointArray vertexArray(fragPointCount);

		I32 fragPointIndex=0;
		for(std::set<I32>::iterator it=pointSet.begin();
				it!=pointSet.end();it++)
		{
			const I32 pointIndex=*it;
			const SpatialVector point=spInputPoints->spatialVector(pointIndex);

			vertexArray.set(fragPointIndex,point[0],point[1],point[2]);
			fragPointMap[pointIndex]=fragPointIndex;

			fragPointIndex++;
		}

		FEASSERT(fragPointIndex==fragPointCount);

		for(connectIndex=0;connectIndex<numConnects;connectIndex++)
		{
			polygonConnects[connectIndex]=
					fragPointMap[polygonConnects[connectIndex]];
		}

		MFnMeshData meshDataFn;
		MObject newMeshData=meshDataFn.create();

		I32 numVertices=fragPointCount;

#if FALSE
		for(I32 vertexIndex=0;vertexIndex<numVertices;vertexIndex++)
		{
			feLog("vertex %d/%d %.6G %.6G %.6G\n",vertexIndex,numVertices,
					vertexArray[vertexIndex][0],
					vertexArray[vertexIndex][1],
					vertexArray[vertexIndex][2]);
		}

		connectIndex=0;
		for(I32 polyIndex=0;polyIndex<numPolygons;polyIndex++)
		{
			const I32 subCount=polygonCounts[polyIndex];

			feLog("polygon %d/%d count %d\n",polyIndex,numPolygons,subCount);

			for(I32 subIndex=0;subIndex<subCount;subIndex++)
			{
				feLog(" %d",polygonConnects[connectIndex]);
				connectIndex++;
			}
			feLog("\n");
		}
#endif

		MFnMesh newMeshFn;
		MObject meshObject=newMeshFn.create(numVertices,numPolygons,
				vertexArray,polygonCounts,polygonConnects,
				newMeshData,&status);
		if(!status)
		{
			feLog("MeshSeparateOp::handle create failed: %s\n",
					status.errorString().asChar());
		}

		MDataHandle meshData=rArrayData.outputValue();
		meshData.set(newMeshData);

		rArrayData.next();
	}

#if FE_MSO_DEBUG
	feLog("MeshSeparateOp::handle done\n");
#endif
}
