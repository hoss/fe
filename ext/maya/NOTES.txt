*Installing Maya 2022 or 2023 on Debian*

Use Ubuntu Guide:
https://knowledge.autodesk.com/support/maya/learn-explore/caas/simplecontent/content/installing-maya-2022-ubuntu.html

Find and install libxp6_1.0.2-2_amd64.deb:
https://apt.ligo-wa.caltech.edu:8443/debian/pool/bullseye-unstable/libxp6/

Fix permissions:
chmod -R adsklic/adsklic /var/opt/Autodesk/AdskLicensingService

Prevent startup crash:
https://aur.archlinux.org/packages/maya
basically: touch /usr/autodesk/maya2022/lib/libmd.so

