/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __maya_MayaBrush_h__
#define __maya_MayaBrush_h__

namespace fe
{
namespace ext
{

class MayaNode;

class MayaBrushManip: public MPxManipContainer
{
public:
				MayaBrushManip(void);
virtual			~MayaBrushManip(void);

static	void*	creator(void);
static	MStatus	initialize(void);

virtual	MStatus	connectToDependNode(const MObject &node) override;
virtual	MStatus	createChildren(void) override;

virtual	void	preDrawUI(const M3dView &mayaView);

virtual	void	draw(M3dView &mayaView,const MDagPath &path,
					M3dView::DisplayStyle style,
					M3dView::DisplayStatus status) override;

virtual	void	drawUI(MHWRender::MUIDrawManager& drawManager,
					const MHWRender::MFrameContext& frameContext) const
					override;

static	MTypeId	m_id;
};

/**************************************************************************//**
    @brief Maya Tool

	Maya's parallel to the Houdini brush is a MPxContext tool.
	The MayaContext singleton class provided by FE is not related.

	@ingroup maya
*//***************************************************************************/
class MayaBrush:
	public MPxContext,
	public MetaBrush
{
	public:

	class Command: public MPxContextCommand
	{
		public:
						Command(void)										{}
	virtual	MPxContext*	makeObj(void)	{ return new MayaBrush(); }
	static	void*		creator(void)	{ return new MayaBrush::Command(); }
	};

							MayaBrush(void);
virtual						~MayaBrush(void);

							using MPxContext::doPress;
							using MPxContext::doRelease;
							using MPxContext::doDrag;

virtual	void				toolOnSetup(MEvent &mayaEvent) override;
virtual	void				toolOffCleanup(void) override;
virtual	MStatus				doEnterRegion(MEvent &mayaEvent) override;
virtual	MStatus				doPress(MEvent &mayaEvent,
								MHWRender::MUIDrawManager& drawManager,
								const MHWRender::MFrameContext& frameContext)
								override;
virtual	MStatus				doRelease(MEvent &mayaEvent,
								MHWRender::MUIDrawManager& drawManager,
								const MHWRender::MFrameContext& frameContext)
								override;
virtual	MStatus				doDrag(MEvent &mayaEvent,
								MHWRender::MUIDrawManager& drawManager,
								const MHWRender::MFrameContext& frameContext)
								override;
virtual	MStatus				doHold(MEvent &mayaEvent,
								MHWRender::MUIDrawManager& drawManager,
								const MHWRender::MFrameContext& frameContext)
								override;
virtual	MStatus				doPtrMoved(MEvent &mayaEvent,
								MHWRender::MUIDrawManager& drawManager,
								const MHWRender::MFrameContext& frameContext)
								override;
virtual	MStatus				drawFeedback(
								MHWRender::MUIDrawManager& drawManager,
								const MHWRender::MFrameContext& frameContext)
								override;
virtual	void				deleteAction(void) override;
virtual	void				completeAction(void) override;
virtual	void				abortAction(void) override;
virtual	bool				processNumericalInput(const MDoubleArray& rValues,
									const MIntArray& rFlags,
									bool isAbsolute) override;

		void				doMove(I32 a_mouseX,I32 a_mouseY);

static	void				staticSetup(void);
static	void				idleAdjust(void);

static	MHWRender::MUIDrawManager*	ms_pMUIDrawManager;
static	MayaBrush*			ms_pMayaBrush;
static	MayaNode*			ms_pCurrentNode;
static	BWORD				ms_mayaBrushCurrent;
static	MCallbackId			ms_commandCallback;
static	MCallbackId			ms_idleCallback;
		MCallbackId			m_updateCallback;

static	void				updateManipulators(void* a_pData);
static	void				provokeRedraw(void);

		void				brush(void);
		void				redraw(void);

	private:
		void				interpretMouse(MEvent& mayaEvent,
									WindowEvent::State state);

		Matrix<4,4,Real>	m_projection;

		I32					m_mouseX;
		I32					m_mouseY;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __maya_MayaBrush_h__ */

