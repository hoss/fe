/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __surface_SurfaceCurvesMaya_h__
#define __surface_SurfaceCurvesMaya_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Maya Curve Primitives

	@ingroup maya
*//***************************************************************************/
class FE_DL_EXPORT SurfaceCurvesMaya:
	public SurfaceCurves,
	public CastableAs<SurfaceCurvesMaya>
{
	public:
						SurfaceCurvesMaya(void);
virtual					~SurfaceCurvesMaya(void);

						//* As Protectable
virtual	Protectable*	clone(Protectable* pInstance=NULL);

						//* Maya specific
		void			setMeshData(const MDataHandle a_meshData)
						{
							m_meshData=a_meshData;
						}
const	MDataHandle		meshData(void)
						{	return m_meshData; }

		void			setSubIndex(I32 a_subIndex)
						{	m_subIndex=a_subIndex; }
		I32				subIndex(void) const
						{	return m_subIndex; }

		void			clearTransform(void)
						{
							m_useTransform=FALSE;
						}
		void			setTransform(SpatialTransform a_transform)
						{
							m_useTransform=TRUE;
							m_transform=a_transform;
						}

		void			setGroup(String a_group)
						{	m_group=a_group; }
		String			group(void) const
						{	return m_group; }

	protected:

virtual	void			cache(void);

	private:

		MDataHandle			m_meshData;

		BWORD				m_useTransform;
		SpatialTransform	m_transform;

		String				m_group;
		I32					m_subIndex;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __surface_SurfaceCurves_h__ */
