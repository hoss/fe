/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <maya/maya.pmh>

namespace fe
{
namespace ext
{

FontMaya::FontMaya(void):
	m_fontAscent(0),
	m_fontDescent(0)
{
	//* NOTE MHWRender::MUIDrawManager::kDefaultFontSize is 12

	//* TODO find the real values
	m_charWidth=13;
	m_fontAscent=23;

	feLog("FontMaya::FontMaya %dx%d\n",m_charWidth,m_fontAscent);
}

FontMaya::~FontMaya(void)
{
}

void FontMaya::drawAlignedText(sp<Component> a_spDrawComponent,
	const SpatialVector& a_location,
	const String a_text,const Color &a_color)
{
	feLog("FontMaya::drawAlignedText not implemented\n");
}

} /* namespace ext */
} /* namespace fe */
