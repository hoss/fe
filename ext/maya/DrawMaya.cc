/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <maya/maya.pmh>

#define	FE_DMY_DEBUG	FALSE

namespace fe
{
namespace ext
{

void DrawMaya::initialize(void)
{
	m_spFont=registry()->create("FontI.FontMaya");
}

//* copied from DrawOpenGL::multiplication
Real DrawMaya::multiplication(void)
{
	if(m_multiplication<=Real(0))
	{
		const Real dpi=fe::compute_dpi();
		m_multiplication=fe::determine_multiplication(dpi);
	}

	return m_multiplication;
}

void DrawMaya::drawAlignedText(const SpatialVector& location,
	const String text,const Color& color)
{
#if FE_DMY_DEBUG
	feLog("DrawMaya::drawAlignedText %s \"%s\" %s\n",
			c_print(location),text.c_str(),c_print(color));
#endif

	if(!drawManager())
	{
		feLog("DrawMaya::drawAlignedText drawManager is NULL\n");
		return;
	}

	drawManager()->setColor(MColor(color[0],color[1],color[2],color[3]));

	if(view()->projection()==ViewI::e_ortho)
	{
		drawManager()->text2d(
				MPoint(location[0],location[1],location[2]),
				text.c_str(),MHWRender::MUIDrawManager::kLeft);
	}
	else
	{
		drawManager()->text(
				MPoint(location[0],location[1],location[2]),
				text.c_str(),MHWRender::MUIDrawManager::kLeft);
	}
}

void DrawMaya::drawPoints(const SpatialVector *vertex,
	const SpatialVector *normal,U32 vertices,
	BWORD multicolor,const Color *color,
	sp<DrawBufferI> spDrawBuffer)
{
	if(!drawManager())
	{
		feLog("DrawMaya::drawPoints drawManager is NULL\n");
		return;
	}

#if FE_DMY_DEBUG
	feLog("DrawMaya::drawPoints pointSize %.3f\n",drawMode()->pointSize());
	feLog("%s\n",c_print(drawMode()));
#endif

	drawManager()->setPointSize(drawMode()->pointSize());

	const bool draw2D=(view()->projection()==ViewI::e_ortho);

	if(!color)
	{
		//* Maya default draw color
		drawManager()->setColor(MColor(0.7,0.7,0.7,1.0));
	}
	else if(!multicolor)
	{
		drawManager()->setColor(
				MColor(color[0][0],color[0][1],color[0][2],color[0][3]));
	}

	if(color && multicolor)
	{
#if FE_DMY_DEBUG
		feLog("DrawMaya::drawPoints %d multicolor\n",vertices);
#endif

		for(I32 vertexIndex=0;vertexIndex<vertices;vertexIndex++)
		{
			const SpatialVector& rPoint=vertex[vertexIndex];
			const Color& rColor=color[vertexIndex];

			drawManager()->setColor(
					MColor(rColor[0],rColor[1],rColor[2],rColor[3]));

			if(draw2D)
			{
				drawManager()->point2d(
						MPoint(rPoint[0],rPoint[1],rPoint[2]));
			}
			else
			{
				drawManager()->point(
						MPoint(rPoint[0],rPoint[1],rPoint[2]));
			}
		}
	}
	else
	{
#if FE_DMY_DEBUG
		feLog("DrawMaya::drawPoints %d monocolor\n",vertices);
#endif

		MPointArray points(vertices);
		for(I32 vertexIndex=0;vertexIndex<vertices;vertexIndex++)
		{
			const SpatialVector& rPoint=vertex[vertexIndex];
			points.set(vertexIndex,rPoint[0],rPoint[1],rPoint[2]);
		}

		drawManager()->points(points,draw2D);
	}
}

void DrawMaya::drawLines(const SpatialVector *vertex,
	const SpatialVector *normal,U32 vertices,
	StripMode strip,BWORD multicolor,const Color *color,
	BWORD multiradius,const Real *radius,
	const Vector3i *element,U32 elementCount,
	sp<DrawBufferI> spDrawBuffer)
{
	if(!drawManager())
	{
		feLog("DrawMaya::drawLines drawManager is NULL\n");
		return;
	}

#if FE_DMY_DEBUG
	feLog("DrawMaya::drawLines lineWidth %.3f\n",drawMode()->lineWidth());
	feLog("%s\n",c_print(drawMode()));
#endif

	drawManager()->setLineWidth(drawMode()->lineWidth());

	const bool draw2D=(view()->projection()==ViewI::e_ortho);

	if(!color)
	{
		//* Maya default draw color
		drawManager()->setColor(MColor(0.7,0.7,0.7,1.0));
	}
	else if(!multicolor)
	{
		if(drawMode()->drawStyle()==DrawMode::e_foreshadow)
		{
			//* HACK without depth control, just draw with full alpha
			drawManager()->setColor(
					MColor(color[0][0],color[0][1],color[0][2]));
		}
		else
		{
			drawManager()->setColor(
					MColor(color[0][0],color[0][1],color[0][2],color[0][3]));
		}
	}

	if(strip==DrawI::e_strip)
	{
#if FE_DMY_DEBUG
		feLog("DrawMaya::drawLines %d STRIP\n",vertices);
#endif

		MPointArray points(vertices);
		for(I32 vertexIndex=0;vertexIndex<vertices;vertexIndex++)
		{
			const SpatialVector& rPoint=vertex[vertexIndex];
			points.set(vertexIndex,rPoint[0],rPoint[1],rPoint[2]);
		}

		drawManager()->lineStrip(points,draw2D);
	}
	else if(strip==DrawI::e_discrete)
	{
		if(color && multicolor)
		{
#if FE_DMY_DEBUG
			feLog("DrawMaya::drawLines %d DISCRETE multicolor\n",vertices);
#endif

			for(I32 vertexIndex=0;vertexIndex<vertices;vertexIndex+=2)
			{
				const SpatialVector& rPoint0=vertex[vertexIndex];
				const SpatialVector& rPoint1=vertex[vertexIndex+1];

				//* NOTE you only get one color per segment
				const Color& rColor=color[vertexIndex];

				drawManager()->setColor(
						MColor(rColor[0],rColor[1],rColor[2],rColor[3]));

				if(draw2D)
				{
					drawManager()->line2d(
							MPoint(rPoint0[0],rPoint0[1],rPoint0[2]),
							MPoint(rPoint1[0],rPoint1[1],rPoint1[2]));
				}
				else
				{
					drawManager()->line(
							MPoint(rPoint0[0],rPoint0[1],rPoint0[2]),
							MPoint(rPoint1[0],rPoint1[1],rPoint1[2]));
				}
			}
		}
		else
		{
#if FE_DMY_DEBUG
			feLog("DrawMaya::drawLines %d DISCRETE monocolor\n",vertices);
#endif

			MPointArray points(vertices);
			for(I32 vertexIndex=0;vertexIndex<vertices;vertexIndex++)
			{
				const SpatialVector& rPoint=vertex[vertexIndex];
				points.set(vertexIndex,rPoint[0],rPoint[1],rPoint[2]);
			}

			drawManager()->lineList(points,draw2D);
		}
	}
}

void DrawMaya::drawTriangles(const SpatialVector *vertex,
	const SpatialVector *normal,const Vector2 *texture,U32 vertices,
	StripMode strip,BWORD multicolor,const Color* color,
	const Array<I32>* vertexMap,
	const Array<I32>* hullPointMap,const Array<Vector4i>* hullFacePoint,
	sp<DrawBufferI> spDrawBuffer)
{
#if FE_DMY_DEBUG
	feLog("DrawMaya::drawTriangles %d\n",vertices);
#endif

	if(!drawManager())
	{
		feLog("DrawMaya::drawTriangles drawManager is NULL\n");
		return;
	}

	const bool draw2D=(view()->projection()==ViewI::e_ortho);

	//* TODO DrawMode::e_foreshadow

	if(!color)
	{
		//* Maya default draw color
		drawManager()->setColor(MColor(0.7,0.7,0.7,1.0));
	}
	else if(!multicolor)
	{
		drawManager()->setColor(
				MColor(color[0][0],color[0][1],color[0][2],color[0][3]));
	}

	MPointArray points(vertices);
	for(I32 vertexIndex=0;vertexIndex<vertices;vertexIndex++)
	{
		const SpatialVector& rPoint=vertex[vertexIndex];
		points.set(vertexIndex,rPoint[0],rPoint[1],rPoint[2]);
	}

	MHWRender::MUIDrawManager::Primitive renderPrimitive=
			strip==DrawI::e_strip? MHWRender::MUIDrawManager::kTriStrip:
			MHWRender::MUIDrawManager::kTriangles;

	MVectorArray* pNormals(NULL);
	if(normal)
	{
		pNormals=new MVectorArray(vertices);
		for(I32 vertexIndex=0;vertexIndex<vertices;vertexIndex++)
		{
			const SpatialVector& rNormal=normal[vertexIndex];
			pNormals->set(rNormal.raw(),vertexIndex);
		}

	}

	MColorArray* pColors(NULL);
	if(color && multicolor)
	{
		pColors=new MColorArray(vertices);
		for(I32 vertexIndex=0;vertexIndex<vertices;vertexIndex++)
		{
			const Color& rColor=color[vertexIndex];
			pColors->set(vertexIndex,rColor[0],rColor[1],rColor[2],rColor[3]);
		}
	}

/*
	mesh(Primitive mode,
			const MPointArray &position,
			const MVectorArray *normal=NULL,
			const MColorArray *color=NULL,
			const MUintArray *index=NULL,
			const MPointArray *texcoord=NULL)
*/

	if(draw2D)
	{
		drawManager()->mesh2d(renderPrimitive,points,pColors);
	}
	else
	{
		drawManager()->mesh(renderPrimitive,points,pNormals,pColors);
	}

	if(pColors)
	{
		delete pColors;
	}

	if(pNormals)
	{
		delete pNormals;
	}
}

void DrawMaya::drawRectangles(const SpatialVector *vertex,U32 vertices,
							BWORD multicolor,const Color *color)
{
#if FE_DMY_DEBUG
	feLog("DrawMaya::drawRectangles %d\n",vertices);
#endif

/*
	rect(const MPoint &center,const MVector &up, const MVector &normal,
		double scaleX, double scaleY, bool filled=false)
	rect2d(const MPoint &center, const MVector &up,
		double scaleX, double scaleY, bool filled=false)
*/

	if(!drawManager())
	{
		feLog("DrawMaya::drawRectangles drawManager is NULL\n");
		return;
	}

	const bool draw2D=(view()->projection()==ViewI::e_ortho);

	if(!color)
	{
		//* Maya default draw color
		drawManager()->setColor(MColor(0.7,0.7,0.7,1.0));
	}
	else if(!multicolor)
	{
		drawManager()->setColor(
				MColor(color[0][0],color[0][1],color[0][2],color[0][3]));
	}

	const MVector up(0,1,0);
	const MVector normal(0,0,1);
	const bool filled=(drawMode()->drawStyle()!=DrawMode::e_wireframe);

	for(I32 vertexIndex=0;vertexIndex<vertices;vertexIndex+=2)
	{
		const SpatialVector& rPoint0=vertex[vertexIndex];
		const SpatialVector& rPoint1=vertex[vertexIndex+1];

		const MPoint center(
				0.5*(rPoint0[0]+rPoint1[0]),
				0.5*(rPoint0[1]+rPoint1[1]),
				draw2D? 0.0: 0.5*(rPoint0[2]+rPoint1[2]));
		const MPoint scale(
				0.5*(rPoint1[0]-rPoint0[0]),
				0.5*(rPoint1[1]-rPoint0[1]));

		if(color && multicolor)
		{
			const Color rColor=color[vertexIndex*2];
			drawManager()->setColor(
					MColor(rColor[0],rColor[1],rColor[2],rColor[3]));
		}

		if(draw2D)
		{
//			feLog("DrawMaya::drawRectangles"
//					" %d/%d center %.6G %.6G %.6G scale %.6G %.6G\n",
//					vertexIndex,vertices,center[0],center[1],center[2],
//					scale[0],scale[1]);

			drawManager()->rect2d(center,up,scale[0],scale[1],filled);

#if FALSE
			drawManager()->setColor(MColor(0,0,0,1));
			drawManager()->line2d(
					MPoint(center[0]-scale[0],center[1],0),
					MPoint(center[0]+scale[0],center[1],0));
			drawManager()->line2d(
					MPoint(center[0],center[1]-scale[1],0),
					MPoint(center[0],center[1]+scale[1],0));
#endif
		}
		else
		{
			drawManager()->rect(center,up,normal,scale[0],scale[1],filled);
		}
	}
}

} /* namespace ext */
} /* namespace fe */
