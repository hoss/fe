/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <maya/maya.pmh>

#define FE_STM_DEBUG		FALSE
#define FE_STM_VERBOSE		FALSE

namespace fe
{
namespace ext
{

SurfaceTrianglesMaya::SurfaceTrianglesMaya(void)
{
#if FE_STM_DEBUG
	feLog("SurfaceTrianglesMaya()\n");
#endif
}

SurfaceTrianglesMaya::~SurfaceTrianglesMaya(void)
{
}

Protectable* SurfaceTrianglesMaya::clone(Protectable* pInstance)
{
#if FE_STM_DEBUG
	feLog("SurfaceTrianglesMaya::clone\n");
#endif

	SurfaceTrianglesMaya* pSurfaceTrianglesMaya= pInstance?
			fe_cast<SurfaceTrianglesMaya>(pInstance):
			new SurfaceTrianglesMaya();

	SurfaceTriangles::clone(pSurfaceTrianglesMaya);

	// TODO copy attributes

	return pSurfaceTrianglesMaya;
}

void SurfaceTrianglesMaya::cache(void)
{
#if FE_STM_DEBUG
	feLog("SurfaceTrianglesMaya::cache\n");
#endif

	const MObject meshObject=m_meshData.asMesh();
	MFnMesh fnMesh(meshObject);

	setOptionalArrays(Arrays(e_arrayColor|e_arrayUV));

	sp<SurfaceAccessorMaya> spPrimitiveGroup;
	if(!group().empty())
	{
		spPrimitiveGroup=new SurfaceAccessorMaya;
		spPrimitiveGroup->setMaster(registry()->master());
		spPrimitiveGroup->setMeshData(m_meshData);
		spPrimitiveGroup->bind(SurfaceAccessibleI::e_primitive,group());
	}

	sp<SurfaceAccessorMaya> spPrimitivePartition;
	if(!m_partitionAttr.empty())
	{
		spPrimitivePartition=new SurfaceAccessorMaya;
		spPrimitivePartition->setMaster(registry()->master());
		spPrimitivePartition->setMeshData(m_meshData);

		//* NOTE currently, bind doesn't properly fail if missing
		if(!spPrimitivePartition->bind(SurfaceAccessibleI::e_primitive,
				m_partitionAttr.c_str()))
		{
			spPrimitivePartition=NULL;
		}
	}

	//* TODO color
	const Color white(1.0,1.0,1.0);

#if FE_STM_DEBUG
	feLog("SurfaceTrianglesMaya::cache partitionAttr \"%s\" valid %d\n",
			m_partitionAttr.c_str(),spPrimitivePartition.isValid());
#endif

	//* NOTE not doing any Record serialization

	//* convert Maya to vertex and normal arrays


	const U32 primitiveCount=fnMesh.numPolygons();

	m_elements=primitiveCount;
	m_vertices=0;
	U32 elementAllocated=0;
	U32 vertexAllocated=0;

	//* preallocate
	resizeFor(primitiveCount*4,primitiveCount*12,
			elementAllocated,vertexAllocated);

	U32 triangleIndex=0;
	U32 total=0;
	for(U32 primitiveIndex=0;primitiveIndex<primitiveCount;primitiveIndex++)
	{
		//* if using group and not in that group
		if(spPrimitiveGroup.isValid() &&
				!spPrimitiveGroup->integer(primitiveIndex))
		{
			continue;
		}

		const U32 subCount=fnMesh.polygonVertexCount(primitiveIndex);
		if(subCount!=3 && subCount!=4)
		{
#if FE_STM_DEBUG
			feLog("SurfaceTrianglesMaya::cache"
					" primitive %d/%d subCount=%d\n",
					primitiveIndex,primitiveCount,subCount);
#endif
			continue;
		}

		I32 partitionIndex= -1;
		if(spPrimitivePartition.isValid())
		{
			const String partition=
					spPrimitivePartition->string(primitiveIndex);
			partitionIndex=lookupPartition(partition);

#if FE_STM_VERBOSE
			feLog("partition %d \"%s\"\n",partitionIndex,partition.c_str());
#endif
		}

		if(m_triangulation==SurfaceI::e_existingPoints)
		{
			m_vertices+=(subCount==4)? 6: 3;
		}
		else
		{
			m_vertices+=(subCount==4)? 12: 3;
		}

		if(m_vertices>vertexAllocated)
		{
			resizeFor(primitiveCount*4,m_vertices,
					elementAllocated,vertexAllocated);
		}

		FEASSERT(triangleIndex<elementAllocated);
		m_pElementArray[triangleIndex]=Vector3i(total,3,FALSE);

		MIntArray vertexArray;
		MStatus status=fnMesh.getPolygonVertices(primitiveIndex,vertexArray);

		MFloatVectorArray normalArray;
		status=fnMesh.getFaceVertexNormals(primitiveIndex,normalArray);
		I32 normalCount=normalArray.length();

		for(U32 subIndex=0;subIndex<subCount;subIndex++)
		{
			const I32 pointIndex=vertexArray[subIndex];

			I32 uvId=0;
			fnMesh.getPolygonUVid(primitiveIndex,subIndex,uvId);

			MPoint point;
			status=fnMesh.getPoint(pointIndex,point);

			float u=0.0;
			float v=0.0;
			fnMesh.getUV(uvId,u,v);

			m_pVertexArray[total]=SpatialVector(point[0],point[1],point[2]);
			m_pColorArray[total]=white;
			m_pTriangleIndexArray[total]=triangleIndex+(subIndex==3);
			m_pPointIndexArray[total]=pointIndex;
			m_pPrimitiveIndexArray[total]=primitiveIndex;
			m_pPartitionIndexArray[total]=partitionIndex;
			set(m_pUVArray[total],u,v);

			if(m_useTransform)
			{
				transformVector(m_transform,
						m_pVertexArray[total],m_pVertexArray[total]);
			}

			if(subIndex<normalCount)
			{
				MFloatVector& rNormal=normalArray[subIndex];
				m_pNormalArray[total]=
						SpatialVector(rNormal[0],rNormal[1],rNormal[2]);

				if(m_useTransform)
				{
					rotateVector(m_transform,
							m_pNormalArray[total],m_pNormalArray[total]);
				}
			}
			else
			{
				m_pNormalArray[total]=SpatialVector(0.0,1.0,0.0);
			}

			total++;

			if(subIndex==3)
			{
				if(m_triangulation==SurfaceI::e_existingPoints)
				{
					triangleIndex++;

					FEASSERT(triangleIndex<elementAllocated);
					m_pElementArray[triangleIndex]=
							Vector3i(m_vertices-3,3,FALSE);

					m_pVertexArray[total]=m_pVertexArray[total-4];
					m_pNormalArray[total]=m_pNormalArray[total-4];
					m_pColorArray[total]=white;
					m_pUVArray[total]=m_pUVArray[total-4];
					m_pTriangleIndexArray[total]=triangleIndex;
					m_pPointIndexArray[total]=m_pPointIndexArray[total-4];
					m_pPrimitiveIndexArray[total]=primitiveIndex;
					m_pPartitionIndexArray[total]=partitionIndex;
					total++;

					m_pVertexArray[total]=m_pVertexArray[total-3];
					m_pNormalArray[total]=m_pNormalArray[total-3];
					m_pColorArray[total]=white;
					m_pUVArray[total]=m_pUVArray[total-3];
					m_pTriangleIndexArray[total]=triangleIndex;
					m_pPointIndexArray[total]=m_pPointIndexArray[total-3];
					m_pPrimitiveIndexArray[total]=primitiveIndex;
					m_pPartitionIndexArray[total]=partitionIndex;
					total++;
				}
				else
				{
					//* quartering

					total-=4;

					//* 0:	0  1  2
					//* 1:	3  4  5
					//* 2:	6  7  8
					//* 3:	9 10 11

					FEASSERT(triangleIndex<elementAllocated-2);
					m_pElementArray[triangleIndex+1]=
							Vector3i(m_vertices-9,3,FALSE);
					m_pElementArray[triangleIndex+2]=
							Vector3i(m_vertices-6,3,FALSE);
					m_pElementArray[triangleIndex+3]=
							Vector3i(m_vertices-3,3,FALSE);

					const SpatialVector meanVertex=0.25*
							(m_pVertexArray[total]+
							m_pVertexArray[total+1]+
							m_pVertexArray[total+2]+
							m_pVertexArray[total+3]);
					const SpatialVector meanNormal=unitSafe(
							m_pNormalArray[total]+
							m_pNormalArray[total+1]+
							m_pNormalArray[total+2]+
							m_pNormalArray[total+3]);
					const SpatialVector meanUV=0.25*
							(m_pUVArray[total]+
							m_pUVArray[total+1]+
							m_pUVArray[total+2]+
							m_pUVArray[total+3]);

					//* triangle 2
					m_pVertexArray[total+6]=m_pVertexArray[total+2];
					m_pNormalArray[total+6]=m_pNormalArray[total+2];
					m_pColorArray[total+6]=white;
					m_pUVArray[total+6]=m_pUVArray[total+2];
					m_pTriangleIndexArray[total+6]=triangleIndex+2;
					m_pPointIndexArray[total+6]=m_pPointIndexArray[total+2];
					m_pPrimitiveIndexArray[total+6]=primitiveIndex;
					m_pPartitionIndexArray[total+6]=partitionIndex;

					m_pVertexArray[total+7]=m_pVertexArray[total+3];
					m_pNormalArray[total+7]=m_pNormalArray[total+3];
					m_pColorArray[total+7]=white;
					m_pUVArray[total+7]=m_pUVArray[total+3];
					m_pTriangleIndexArray[total+7]=triangleIndex+2;
					m_pPointIndexArray[total+7]=m_pPointIndexArray[total+3];
					m_pPrimitiveIndexArray[total+7]=primitiveIndex;
					m_pPartitionIndexArray[total+7]=partitionIndex;

					//* triangle 3
					m_pVertexArray[total+9]=m_pVertexArray[total+3];
					m_pNormalArray[total+9]=m_pNormalArray[total+3];
					m_pColorArray[total+9]=white;
					m_pUVArray[total+9]=m_pUVArray[total+3];
					m_pTriangleIndexArray[total+9]=triangleIndex+3;
					m_pPointIndexArray[total+9]=m_pPointIndexArray[total+3];
					m_pPrimitiveIndexArray[total+9]=primitiveIndex;
					m_pPartitionIndexArray[total+9]=partitionIndex;

					m_pVertexArray[total+10]=m_pVertexArray[total];
					m_pNormalArray[total+10]=m_pNormalArray[total];
					m_pColorArray[total+10]=white;
					m_pUVArray[total+10]=m_pUVArray[total];
					m_pTriangleIndexArray[total+10]=triangleIndex+3;
					m_pPointIndexArray[total+10]=m_pPointIndexArray[total];
					m_pPrimitiveIndexArray[total+10]=primitiveIndex;
					m_pPartitionIndexArray[total+10]=partitionIndex;

					//* triangle 1
					m_pVertexArray[total+3]=m_pVertexArray[total+1];
					m_pNormalArray[total+3]=m_pNormalArray[total+1];
					m_pColorArray[total+3]=white;
					m_pUVArray[total+3]=m_pUVArray[total+1];
					m_pTriangleIndexArray[total+3]=triangleIndex+1;
					m_pPointIndexArray[total+3]=m_pPointIndexArray[total+1];
					m_pPrimitiveIndexArray[total+3]=primitiveIndex;
					m_pPartitionIndexArray[total+3]=partitionIndex;

					m_pVertexArray[total+4]=m_pVertexArray[total+2];
					m_pNormalArray[total+4]=m_pNormalArray[total+2];
					m_pColorArray[total+4]=white;
					m_pUVArray[total+4]=m_pUVArray[total+2];
					m_pTriangleIndexArray[total+4]=triangleIndex+1;
					m_pPointIndexArray[total+4]=m_pPointIndexArray[total+2];
					m_pPrimitiveIndexArray[total+4]=primitiveIndex;
					m_pPartitionIndexArray[total+4]=partitionIndex;

					//* mean
					m_pVertexArray[total+2]=meanVertex;
					m_pNormalArray[total+2]=meanNormal;
					m_pColorArray[total+2]=white;
					m_pUVArray[total+2]=meanUV;
					m_pTriangleIndexArray[total+2]=triangleIndex;
					m_pPointIndexArray[total+2]= -1;
					m_pPrimitiveIndexArray[total+2]=primitiveIndex;
					m_pPartitionIndexArray[total+2]=partitionIndex;

					m_pVertexArray[total+5]=meanVertex;
					m_pNormalArray[total+5]=meanNormal;
					m_pColorArray[total+5]=white;
					m_pUVArray[total+5]=meanUV;
					m_pTriangleIndexArray[total+5]=triangleIndex+1;
					m_pPointIndexArray[total+5]= -1;
					m_pPrimitiveIndexArray[total+5]=primitiveIndex;
					m_pPartitionIndexArray[total+5]=partitionIndex;

					m_pVertexArray[total+8]=meanVertex;
					m_pNormalArray[total+8]=meanNormal;
					m_pColorArray[total+8]=white;
					m_pUVArray[total+8]=meanUV;
					m_pTriangleIndexArray[total+8]=triangleIndex+2;
					m_pPointIndexArray[total+8]= -1;
					m_pPrimitiveIndexArray[total+8]=primitiveIndex;
					m_pPartitionIndexArray[total+8]=partitionIndex;

					m_pVertexArray[total+11]=meanVertex;
					m_pNormalArray[total+11]=meanNormal;
					m_pColorArray[total+11]=white;
					m_pUVArray[total+11]=meanUV;
					m_pTriangleIndexArray[total+11]=triangleIndex+3;
					m_pPointIndexArray[total+11]= -1;
					m_pPrimitiveIndexArray[total+11]=primitiveIndex;
					m_pPartitionIndexArray[total+11]=partitionIndex;

					triangleIndex+=3;
					total+=12;
				}
			}
		}

		triangleIndex++;
	}

	if(elementAllocated!=triangleIndex || vertexAllocated!=m_vertices)
	{
		resizeArrays(triangleIndex,m_vertices);
	}

	if(total!=m_vertices)
	{
#if FE_STM_DEBUG
#endif
		feLog("SurfaceTrianglesMaya::cache"
				" converted %d/%d vertices\n",total,m_vertices);

		//* TODO variable subCounts
//		m_elements=triangleIndex;

		m_vertices=total;
		resizeArrays(primitiveCount,m_vertices);
	}

	m_elements=triangleIndex;

	calcBoundingSphere();
}

} /* namespace ext */
} /* namespace fe */
