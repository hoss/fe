/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __maya_CurveCombineOp_h__
#define __maya_CurveCombineOp_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Combine multiple native curves into a multi-curve surface

	@ingroup maya
*//***************************************************************************/
class FE_DL_EXPORT CurveCombineOp:
	public OperatorSurfaceCommon,
	public Initialize<CurveCombineOp>
{
	public:

					CurveCombineOp(void)									{}
virtual				~CurveCombineOp(void)									{}

		void		initialize(void);

					//* As HandlerI
virtual	void		handle(Record& a_rSignal);

	private:
};

} /* namespace ext */
} /* namespace fe */

#endif /* __maya_CurveCombineOp_h__ */
