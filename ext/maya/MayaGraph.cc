/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <maya/maya.pmh>

#define FE_MGH_DAG_DEBUG	FALSE
#define FE_MGH_DG_DEBUG		FALSE

namespace fe
{
namespace ext
{

//* NOTE this gets *ALL* mel
static void commandCB(const MString& string,void *data)
{
	MayaGraph* pMayaGraph=reinterpret_cast<MayaGraph*>(data);
	if(!pMayaGraph || !pMayaGraph->isBuilt())
	{
		return;
	}

	//* make the check simple and fast
	const char* buffer=string.asChar();
	if(!strncmp(buffer,"connectAttr ",12) ||
			!strncmp(buffer,"disconnectAttr ",15) ||
			!strncmp(buffer,"scriptJob ",10))
	{
//		feLog("MayaGraph commandCB TRIGGER %p: %s\n",data,buffer);
		pMayaGraph->discard();
	}

//	const U32 length=strlen(buffer);
//	feLog("MEL %d %s",length,buffer);
}

MayaGraph::MayaGraph(void)
{
	MStatus status=MStatus::kSuccess;

	m_cid=MCommandMessage::addCommandCallback(commandCB,this,&status);
	FEASSERT(status);
}

MayaGraph::~MayaGraph(void)
{
	MStatus status=MMessage::removeCallback(m_cid);
	FEASSERT(status);
}

sp<ImageI> MayaGraph::image(void)
{
	if(m_spImage.isNull())
	{
		m_spImage=rebuild();
	}

	return m_spImage;
}

BWORD MayaGraph::connect(String a_outputName,String a_outputConnector,
	String a_inputName,String a_inputConnector)
{
	// connectAttr -f NexusOp.Output_Surface polyNexusShape.inMesh;
	String command="connectAttr -f ";
	command+=a_outputName+"."+a_outputConnector;
	command+=" "+a_inputName+"."+a_inputConnector;
	MString response;
	MGlobal::executeCommand(command.c_str(),response);

	return MetaGraph::addConnection(a_outputName,a_outputConnector,
			a_inputName,a_inputConnector);
}

BWORD MayaGraph::disconnect(String a_inputName,String a_inputConnector)
{
	String outputName=inputNode(a_inputName,a_inputConnector);
	String outputConnector=
			inputNodeOutputConnector(a_inputName,a_inputConnector);

	// disconnectAttr NexusOp.Output_Surface polyNexusShape.inMesh;
	String command="disconnectAttr ";
	command+=outputName+"."+outputConnector;
	command+=" "+a_inputName+"."+a_inputConnector;
	MString response;
	MGlobal::executeCommand(command.c_str(),response);

	return MetaGraph::removeConnection(a_inputName,a_inputConnector);
}

sp<ImageI> MayaGraph::rebuild(void)
{
	feLog("MayaGraph::rebuild\n");

	clear();

	MStatus status;

#if FE_MGH_DAG_DEBUG
	feLog("\nDAG\n");

	MItDag::TraversalType traversalType=MItDag::kDepthFirst;
	MFn::Type filter=MFn::kInvalid;
	MItDag dagIterator(traversalType,filter,&status);
	FEASSERT(status);

	for(; !dagIterator.isDone(); dagIterator.next())
	{
		MDagPath dagPath;
		status=dagIterator.getPath(dagPath);
		FEASSERT(status);

		MFnDagNode dagNode(dagPath,&status);
		FEASSERT(status);

		feLog("name \"%s\" type \"%s\" path \"%s\"\n",
				dagNode.name().asChar(),
				dagNode.typeName().asChar(),
				dagNode.fullPathName().asChar());

		MPxNode *pMPxNode=dagNode.userNode(&status);
		if(status)
		{
			feLog("  MPxNode %p\n",pMPxNode);
		}

		const U32 parentCount=dagNode.parentCount(&status);
		FEASSERT(status);
		for(U32 parentIndex=0;parentIndex<parentCount;parentIndex++)
		{
			MObject parent=dagNode.parent(parentIndex,&status);
			FEASSERT(status);

			MFnDagNode parentNode(parent,&status);
			FEASSERT(status);

			feLog("  parent %d/%d \"%s\"\n",parentIndex,parentCount,
					parentNode.name().asChar());
		}

		const U32 childCount=dagNode.childCount(&status);
		FEASSERT(status);
		for(U32 childIndex=0;childIndex<childCount;childIndex++)
		{
			MObject child=dagNode.child(childIndex,&status);
			FEASSERT(status);

			MFnDagNode childNode(child,&status);
			FEASSERT(status);

			feLog("  child %d/%d \"%s\"\n",childIndex,childCount,
					childNode.name().asChar());
		}

		MPlugArray plugArray;
		status=dagNode.getConnections(plugArray);
		const U32 plugCount=plugArray.length();
		for(U32 plugIndex=0;plugIndex<plugCount;plugIndex++)
		{
			const MPlug& rPlug=plugArray[plugIndex];
			feLog("  plug %d/%d \"%s\"\n",plugIndex,plugCount,
					rPlug.name().asChar());

			for(U32 pass=0;pass<2;pass++)
			{
				MPlugArray connectionArray;
				rPlug.connectedTo(connectionArray,!pass,pass,&status);
				FEASSERT(status);

				const U32 connectionCount=connectionArray.length();
				for(U32 connectionIndex=0;connectionIndex<connectionCount;
						connectionIndex++)
				{
					MPlug connectionPlug=connectionArray[connectionIndex];
					feLog("    %s %d/%d \"%s\"\n",pass? "to": "from",
							connectionIndex,connectionCount,
							connectionPlug.name().asChar());

					MObject connectionNode=connectionPlug.node();
					MFnDependencyNode dependencyNode(connectionNode,&status);
					if(status)
					{
						feLog("    node \"%s\"\n",
								dependencyNode.name().asChar());

						MPxNode *pNode=dependencyNode.userNode(&status);
						if(status)
						{
							feLog("    MPxNode %p\n",pNode);

							MayaNode* pMayaNode=fe_cast<MayaNode>(pNode);
							if(pMayaNode)
							{
								feLog("    MayaNode %p\n",pNode);

								sp<OperatorSurfaceI> spOperator=
										pMayaNode->operatorSurface();
								if(spOperator.isValid())
								{
									feLog("    operator \"%s\"\n",
											spOperator->name().c_str());

									const U32 inputCount=
											pMayaNode->inputCount();
									for(U32 index=0;index<inputCount;index++)
									{
										sp<Component> spInput=
												pMayaNode->input(index);
										if(spInput.isValid())
										{
											feLog("      input %d/%d \"%s\"\n",
													index,inputCount,
													spInput->name().c_str());
										}
									}

									sp<Component> spOutput=pMayaNode->output();
									if(spOutput.isValid())
									{
										feLog("      output \"%s\"\n",
												spOutput->name().c_str());
									}
								}
							}
						}
					}
				}
			}
		}
	}

	feLog("\nDG\n");

#endif

	const U32 filterCount=3;
	const MFn::Type filterList[filterCount]=
	{
		//* see MFn.h
//		MFn::kTransform,
		MFn::kTransformGeometry,
		MFn::kMesh,
		MFn::kPluginDependNode
	};

	for(U32 filterIndex=0;filterIndex<filterCount;filterIndex++)
	{
		MItDependencyNodes dgIterator(filterList[filterIndex],&status);
		FEASSERT(status);

		for(; !dgIterator.isDone(); dgIterator.next())
		{
			MObject dgObject=dgIterator.thisNode(&status);
			FEASSERT(status);

			void* pMayaObject=((MayaObject*)&dgObject)->ptr;

			MFnDependencyNode dgNode(dgObject,&status);
			String dgName=dgNode.name().asChar();
			String typeName=dgNode.typeName().asChar();

#if FE_MGH_DG_DEBUG
			feLog("name \"%s\" type \"%s\"\n",
					dgName.c_str(),typeName.c_str());
#endif

			if(typeName.match(".*Manip.*"))
			{
				continue;
			}

			BWORD added=FALSE;
			if(typeName!="transform")
			{
				add(pMayaObject,dgName);
				added=TRUE;
			}

			MayaNode* pMayaNode=NULL;
			sp<OperatorSurfaceI> spOperator;
			MPxNode *pMPxNode=dgNode.userNode(&status);
			if(status)
			{
#if FE_MGH_DG_DEBUG
				feLog("  MPxNode %p\n",pMPxNode);
#endif
				pMayaNode=dynamic_cast<MayaNode*>(pMPxNode);
				if(pMayaNode)
				{
#if FE_MGH_DG_DEBUG
					feLog("    MayaNode %p\n",pMayaNode);
#endif
					spOperator=pMayaNode->operatorSurface();
					if(spOperator.isNull())
					{
						feLog("MayaGraph::rebuild MayaNode has no OperatorI\n");
					}
				}
			}
			if(spOperator.isValid())
			{
#if FE_MGH_DG_DEBUG
				feLog("    operator \"%s\"\n",
						spOperator->name().c_str());
#endif

				const U32 inputCount=
						pMayaNode->inputCount();
				for(U32 index=0;index<inputCount;
						index++)
				{
					sp<Component> spInput=
							pMayaNode->MetaPlugin::input(index);
					if(spInput.isValid())
					{
#if FE_MGH_DG_DEBUG
						feLog("      input %d/%d"
								" \"%s\"\n",
								index,inputCount,
								spInput->name()
								.c_str());
#endif
						addInput(pMayaObject,
								spInput->name().substitute(" ","_"));
					}
				}

				sp<Component> spOutput=
						pMayaNode->output();
				if(spOutput.isValid())
				{
#if FE_MGH_DG_DEBUG
					feLog("      output \"%s\"\n",
							spOutput->name().c_str());
#endif
					addOutput(pMayaObject,
							spOutput->name().substitute(" ","_"));
				}
			}
			else
			{
				const U32 attrCount=dgNode.attributeCount(&status);
				FEASSERT(status);

				for(U32 attrIndex=0;attrIndex<attrCount;attrIndex++)
				{
					MObject attrObject=dgNode.attribute(attrIndex,&status);
					FEASSERT(status);

					MFnAttribute attribute(attrObject);
					if(attribute.isHidden() ||
							attribute.internal() ||
							!attribute.isReadable() ||
							!attribute.isConnectable() ||
							attribute.accepts(MFnData::kNumeric,&status) ||
							!attribute.accepts(MFnData::kMesh,&status))
					{
						continue;
					}

					const bool writable=attribute.isWritable();
					const bool array=attribute.isArray();
					const String attrShortName=attribute.shortName().asChar();
					const String attrName=attribute.name().asChar();

#if FE_MGH_DG_DEBUG
					feLog("  attr %d/%d write %d array %d"
							" \"%s\" \"%s\"\n",
							attrIndex,attrCount,writable,array,
							attrShortName.c_str(),attrName.c_str());
#endif

					if(writable)
					{
						addInput(pMayaObject,attrName);
					}
					else
					{
						addOutput(pMayaObject,attrName+(array? "[0]": ""));
					}

#if FALSE
					const bool storable=attribute.isStorable();
					const bool cached=attribute.isCached();
					const bool indexMatters=attribute.indexMatters();
					const bool channel=attribute.isChannelBoxFlagSet();
					const bool asColor=attribute.isUsedAsColor();
					const bool indeterminant=attribute.isIndeterminant();
					const bool render=attribute.isRenderSource();
					const bool dynamic=attribute.isDynamic();
					const bool extension=attribute.isExtension();
					const bool world=attribute.isWorldSpace();
					const bool affectsWorld=attribute.isAffectsWorldSpace();
					const bool asFile=attribute.isUsedAsFilename();
					const bool appear=attribute.affectsAppearance();
					const bool builder=attribute.usesArrayDataBuilder();

					feLog("  store %d cache %d index %d chan %d"
							" color %d indet %d ren %d dy %d"
							" ext %d world %d affects %d file %d"
							" app %d build %d\n",
							storable,cached,indexMatters,channel,
							asColor,indeterminant,render,dynamic,
							extension,world,affectsWorld,asFile,
							appear,builder);
					for(I32 as=1;as<18;as++)
					{
						feLog(" %d",attribute.accepts(MFnData::Type(as)));
					}
					feLog("\n");
#endif
				}
			}

			MPlugArray plugArray;
			status=dgNode.getConnections(plugArray);
			const U32 plugCount=plugArray.length();
			for(U32 plugIndex=0;plugIndex<plugCount;plugIndex++)
			{
				const MPlug& rPlug=plugArray[plugIndex];
#if FE_MGH_DG_DEBUG
				feLog("  plug %d/%d \"%s\"\n",plugIndex,plugCount,
						rPlug.name().asChar());
#endif
				String plugName=rPlug.name().asChar();
				plugName.parse("\"",".");
				plugName=plugName.prechop(".");

				for(U32 pass=0;pass<2;pass++)
				{
					MPlugArray connectionArray;
					rPlug.connectedTo(connectionArray,!pass,pass,&status);
					FEASSERT(status);

					//* only track plugs for attributes we accepted
					const BWORD found=pass?
							hasOutputConnector(pMayaObject,plugName):
							hasInputConnector(pMayaObject,plugName);
					if(!found)
					{
						continue;
					}

					const U32 connectionCount=connectionArray.length();
					for(U32 connectionIndex=0;connectionIndex<connectionCount;
							connectionIndex++)
					{
						MPlug connectionPlug=connectionArray[connectionIndex];
						String connectionName=connectionPlug.name().asChar();

#if FE_MGH_DG_DEBUG
						feLog("    %s %d/%d \"%s\"\n",pass? "to": "from",
								connectionIndex,connectionCount,
								connectionName.c_str());
#endif

						connectionName.parse("\"",".");
						connectionName=connectionName.prechop(".");

						if(pass)
						{
							addOutput(pMayaObject,plugName);
						}
						else
						{
							addInput(pMayaObject,plugName);
						}

						MObject connectionNode=connectionPlug.node();
						MFnDependencyNode dependencyNode(connectionNode,
								&status);
						if(status)
						{
#if FE_MGH_DG_DEBUG
							feLog("    node \"%s\"\n",
									dependencyNode.name().asChar());
#endif

							if(pass)
							{
								if(!added)
								{
									add(pMayaObject,dgName);
									added=TRUE;
								}
#if FE_MGH_DG_DEBUG
								feLog("    connect \"%s\" -> \"%s\"\n",
										plugName.c_str(),
										connectionName.c_str());
#endif
								addConnection(pMayaObject,plugName,
										((MayaObject*)&connectionNode)->ptr,
										connectionName);
							}

#if FE_MGH_DG_DEBUG
							MPxNode *pNode=dependencyNode.userNode(&status);
							if(status)
							{
								feLog("    MPxNode %p\n",pNode);

								MayaNode* pMayaNode=fe_cast<MayaNode>(pNode);
								if(pMayaNode)
								{
									feLog("    MayaNode %p\n",pNode);

									sp<OperatorSurfaceI> spOperator=
											pMayaNode->operatorSurface();
									if(spOperator.isValid())
									{
										feLog("    operator \"%s\"\n",
												spOperator->name().c_str());

										const U32 inputCount=
												pMayaNode->inputCount();
										for(U32 index=0;index<inputCount;
												index++)
										{
											sp<Component> spInput=
													pMayaNode->input(index);
											if(spInput.isValid())
											{
												feLog("      connect in %d/%d"
														" \"%s\"\n",
														index,inputCount,
														spInput->name()
														.c_str());
											}
										}

										sp<Component> spOutput=
												pMayaNode->output();
										if(spOutput.isValid())
										{
											feLog("      connect out \"%s\"\n",
													spOutput->name().c_str());
										}
									}
								}
							}
							//* not MPxNode
#endif
						}
					}
				}
			}
		}
	}

#if FE_MGH_DG_DEBUG
	dump();
#endif

	const String dotSource=generateDot();

#if FE_MGH_DG_DEBUG
	feLog("\nDOT:\n%s\n",dotSource.c_str());
#endif

	return render();
}

} /* namespace ext */
} /* namespace fe */
