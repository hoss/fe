/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __surface_SurfaceTrianglesMaya_h__
#define __surface_SurfaceTrianglesMaya_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Maya Trianglar Surface

	@ingroup maya
*//***************************************************************************/
class FE_DL_EXPORT SurfaceTrianglesMaya:
	public SurfaceTriangles,
	public CastableAs<SurfaceTrianglesMaya>
{
	public:
						SurfaceTrianglesMaya(void);
virtual					~SurfaceTrianglesMaya(void);

						//* As Protectable
virtual	Protectable*	clone(Protectable* pInstance=NULL);

						//* Maya specific
		void			setMeshData(const MDataHandle a_meshData)
						{
							m_meshData=a_meshData;
						}
const	MDataHandle		meshData(void)
						{	return m_meshData; }

		void			clearTransform(void)
						{
							m_useTransform=FALSE;
						}
		void			setTransform(SpatialTransform a_transform)
						{
							m_useTransform=TRUE;
							m_transform=a_transform;
						}

	protected:

virtual	void			cache(void);

	private:

		MDataHandle			m_meshData;

		BWORD				m_useTransform;
		SpatialTransform	m_transform;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __surface_SurfaceTriangles_h__ */

