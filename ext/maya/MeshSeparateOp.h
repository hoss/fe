/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __maya_MeshSeparateOp_h__
#define __maya_MeshSeparateOp_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Separate a multi-part mesh into multiple separate meshes

	@ingroup maya
*//***************************************************************************/
class FE_DL_EXPORT MeshSeparateOp:
	public OperatorSurfaceCommon,
	public Initialize<MeshSeparateOp>
{
	public:

					MeshSeparateOp(void)									{}
virtual				~MeshSeparateOp(void)									{}

		void		initialize(void);

					//* As HandlerI
virtual	void		handle(Record& a_rSignal);

	private:

	struct StringNumber
	{
						StringNumber(void)									{}

		StringNumber&	operator=(String a_string);

		struct Sort
		{
			bool operator()(const StringNumber& rStringNumber1,
					const StringNumber& rStringNumber2);
		};

		String	print(void);

		String	m_prefix;
		BWORD	m_hasNumber;
		I32		m_number;
	};

	struct StringSort
	{
		bool operator()(const String& rString1,const String& rString2)
		{
			return (rString1.compare(rString2)<0);
		}
	};
};

} /* namespace ext */
} /* namespace fe */

#endif /* __maya_MeshSeparateOp_h__ */
