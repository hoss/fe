/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __maya_JointCombineOp_h__
#define __maya_JointCombineOp_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Combine multiple native joints a single multi-joint surface

	@ingroup maya
*//***************************************************************************/
class FE_DL_EXPORT JointCombineOp:
	public OperatorSurfaceCommon,
	public Initialize<JointCombineOp>
{
	public:

					JointCombineOp(void):
						m_dirtied(FALSE)									{}
virtual				~JointCombineOp(void);

		void		initialize(void);

					//* As HandlerI
virtual	void		handle(Record& a_rSignal);

	private:
		void		cancelCallbacks(void);

		void		scanDag(MObject a_parent,String a_grandparent);

static	void		changeCB(MNodeMessage::AttributeMessage a_message,
							MPlug & a_plug,MPlug & a_otherPlug,
							void* a_pClientData);
static	void		renameCB(MObject &a_node, const MString &a_string,
							void* a_pClientData);

		sp<Scope>							m_spScope;
		sp<SurfaceAccessibleJoint>			m_spJointAccessible;

		std::map<String,SpatialTransform>	m_animWorldMap;
		std::map<String,SpatialTransform>	m_refWorldMap;
		std::map<String,U32>				m_primitiveIndexMap;

		sp<SurfaceAccessorI>				m_spInputRefX;
		sp<SurfaceAccessorI>				m_spInputRefY;
		sp<SurfaceAccessorI>				m_spInputRefZ;
		sp<SurfaceAccessorI>				m_spInputRefT;

		Array<MCallbackId>					m_callbackIDs;
		BWORD								m_dirtied;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __maya_JointCombineOp_h__ */
