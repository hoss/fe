/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <maya/maya.pmh>

#define FE_CSO_DEBUG		FALSE

using namespace fe;
using namespace fe::ext;

void CurveSeparateOp::initialize(void)
{
	catalog<String>("icon")="FE_alpha";

	catalog< sp<Component> >("Input Surface");

	catalog< sp<Component> >("Output Surface");
	catalog<String>("Output Surface","IO")="output";
	catalog<String>("Output Surface","implementation")="curve";
	catalog<bool>("Output Surface","array")=true;
}

void CurveSeparateOp::handle(Record& a_rSignal)
{
#if FE_CSO_DEBUG
	feLog("CurveSeparateOp::handle\n");
#endif

	sp<SurfaceAccessorI> spInputVertices;
	if(!access(spInputVertices,"Input Surface",e_primitive,e_vertices)) return;

	sp<SurfaceAccessibleI> spSurfaceOutput=
			catalog< sp<Component> >("Output Surface");
	if(spSurfaceOutput.isNull())
	{
		catalog<String>("error")+="Inaccessible output;";
		return;
	}

	sp<SurfaceAccessibleMaya> spSurfaceAccessibleMaya=spSurfaceOutput;
	if(spSurfaceAccessibleMaya.isNull())
	{
		catalog<String>("error")+="Incompatible output;";
		return;
	}

	const U32 primitiveCount=spInputVertices->count();

#if FE_CSO_DEBUG
	feLog("CurveSeparateOp::handle primitiveCount %d\n",primitiveCount);
#endif

	MArrayDataHandle& rArrayData=spSurfaceAccessibleMaya->meshArrayData();
	rArrayData.jumpToArrayElement(0);

	const U32 arrayCount=rArrayData.elementCount();
#if FE_CSO_DEBUG
	feLog("CurveSeparateOp::handle arrayCount %d\n",arrayCount);

	for(U32 arrayIndex=0;arrayIndex<arrayCount;arrayIndex++)
	{
		feLog("  %d/%d element %d\n",
				arrayIndex,arrayCount,rArrayData.elementIndex());
		rArrayData.next();
	}
#endif

	MArrayDataBuilder builder=rArrayData.builder();

#if FE_CSO_DEBUG
	feLog("CurveSeparateOp::handle builderCount %d\n",builder.elementCount());
#endif

	//* already starts with first one?
	for(U32 primitiveIndex=arrayCount;primitiveIndex<primitiveCount;
			primitiveIndex++)
	{
		builder.addElement(primitiveIndex);
	}

	//* push changes back to array
	rArrayData.set(builder);
	rArrayData.setAllClean();
	rArrayData.jumpToArrayElement(0);

#if FE_CSO_DEBUG
	feLog("CurveSeparateOp::handle builderCount %d arrayCount %d\n",
			builder.elementCount(),rArrayData.elementCount());
#endif

	const unsigned int degree=3;
	const bool create2D=false;
	const bool createRational=false;

	//* subCount = cvs = spans + degree
	//* knots = spans + 2*degree - 1 = cvs + degree - 1

	for(U32 primitiveIndex=0;primitiveIndex<primitiveCount;primitiveIndex++)
	{
#if FE_CSO_DEBUG
		feLog("CurveSeparateOp::handle %d/%d element %d\n",
				primitiveIndex,primitiveCount,rArrayData.elementIndex());
#endif

		const U32 subCount=spInputVertices->subCount(primitiveIndex);
		MPointArray controlVertices(subCount);
		for(U32 subIndex=0;subIndex<subCount;subIndex++)
		{
			const SpatialVector point=
					spInputVertices->spatialVector(primitiveIndex,subIndex);
			const MPoint mPoint(point[0],point[1],point[2]);
			controlVertices.set(mPoint,subIndex);

#if FE_CSO_DEBUG
			feLog("  %d/%d point %s\n",subIndex,subCount,c_print(point));
#endif
		}

		const U32 knotCount=subCount+degree-1;
		MDoubleArray knotSequences(knotCount);
		U32 knotIndex=0;
		for(;knotIndex<degree;knotIndex++)
		{
			knotSequences[knotIndex]=0;
		}
		for(;knotIndex<knotCount-degree;knotIndex++)
		{
			knotSequences[knotIndex]=knotIndex-degree+1;
		}
		for(;knotIndex<knotCount;knotIndex++)
		{
			knotSequences[knotIndex]=knotCount-2*degree+1;
		}

		MFnNurbsCurveData nurbsDataFn;
		MObject newNurbsData=nurbsDataFn.create();

		MStatus status;

		MFnNurbsCurve newNurbsFn;
		MObject nurbsObject=newNurbsFn.create(controlVertices,knotSequences,
				degree,MFnNurbsCurve::kOpen,create2D,createRational,
				newNurbsData,&status);
		if(!status)
		{
			feLog("CurveSeparateOp::handle create failed: %s\n",
					status.errorString().asChar());
		}

		MDataHandle nurbsData=rArrayData.outputValue();
		nurbsData.set(newNurbsData);

		rArrayData.next();
	}

#if FE_CSO_DEBUG
	feLog("CurveSeparateOp::handle done\n");
#endif
}
