/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __maya_DrawMaya_h__
#define __maya_DrawMaya_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
    @brief Draw via Maya MUIDrawManager

	@ingroup maya
*//***************************************************************************/
class FE_DL_EXPORT DrawMaya:
	public DrawCommon,
	public Initialize<DrawMaya>
{
	public:
					DrawMaya(void): m_pMUIDrawManager(NULL)					{}
virtual				~DrawMaya(void)											{}

		void		initialize(void);

					//* as DrawI

virtual	Real		multiplication(void);

virtual	sp<FontI>	font(void)	{ return m_spFont; }

virtual	void		drawAlignedText(const SpatialVector& location,
							const String text,const Color& color);

					using DrawCommon::drawPoints;

virtual void		drawPoints(const SpatialVector *vertex,
							const SpatialVector *normal,U32 vertices,
							BWORD multicolor,const Color *color,
							sp<DrawBufferI> spDrawBuffer);

					using DrawCommon::drawLines;

virtual	void		drawLines(const SpatialVector *vertex,
							const SpatialVector *normal,U32 vertices,
							StripMode strip,
							BWORD multicolor,const Color *color,
							BWORD multiradius,const Real *radius,
							const Vector3i *element,U32 elementCount,
							sp<DrawBufferI> spDrawBuffer);

					using DrawCommon::drawTriangles;

virtual void		drawTriangles(const SpatialVector *vertex,
							const SpatialVector *normal,
							const Vector2 *texture,U32 vertices,
							StripMode strip,BWORD multicolor,
							const Color* color,
							const Array<I32>* vertexMap,
							const Array<I32>* hullPointMap,
							const Array<Vector4i>* hullFacePoint,
							sp<DrawBufferI> spDrawBuffer);

virtual void		drawRectangles(const SpatialVector *vertex,U32 vertices,
							BWORD multicolor,const Color *color);

					//* Maya specific

		void		setDrawManager(
							MHWRender::MUIDrawManager* a_pMUIDrawManager)
					{	m_pMUIDrawManager=a_pMUIDrawManager; }
		MHWRender::MUIDrawManager*	drawManager(void)
					{	return m_pMUIDrawManager; }

	private:
		MHWRender::MUIDrawManager*	m_pMUIDrawManager;

		Real						m_multiplication;
		sp<FontI>					m_spFont;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __maya_DrawMaya_h__ */
