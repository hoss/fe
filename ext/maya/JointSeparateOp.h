/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __maya_JointSeparateOp_h__
#define __maya_JointSeparateOp_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Separate a multi-joint surface into multiple native joint

	@ingroup maya
*//***************************************************************************/
class FE_DL_EXPORT JointSeparateOp:
	public OperatorSurfaceCommon,
	public Initialize<JointSeparateOp>
{
	public:

					JointSeparateOp(void):
						m_dirtied(FALSE),
						m_deactivate(FALSE)									{}
virtual				~JointSeparateOp(void);

		void		initialize(void);

					//* As HandlerI
virtual	void		handle(Record& a_rSignal);

	private:
		void		cancelCallbacks(void);

		BWORD		scanDag(MObject a_parent);

		String		fullMayaName(String a_rootPath,String a_leafName);

static	void		changeCB(MNodeMessage::AttributeMessage a_message,
							MPlug & a_plug,MPlug & a_otherPlug,
							void* a_pClientData);
static	void		renameCB(MObject &a_node, const MString &a_string,
							void* a_pClientData);

		Array<MObject>				m_keepList;
		Array<String>				m_salvageList;

		Array<MCallbackId>			m_callbackIDs;
		BWORD						m_dirtied;
		BWORD						m_deactivate;
		String						m_selectedJoint;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __maya_JointSeparateOp_h__ */
