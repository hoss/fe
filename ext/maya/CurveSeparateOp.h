/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __maya_CurveSeparateOp_h__
#define __maya_CurveSeparateOp_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Separate a multi-curve surface into multiple native curves

	@ingroup maya
*//***************************************************************************/
class FE_DL_EXPORT CurveSeparateOp:
	public OperatorSurfaceCommon,
	public Initialize<CurveSeparateOp>
{
	public:

					CurveSeparateOp(void)									{}
virtual				~CurveSeparateOp(void)									{}

		void		initialize(void);

					//* As HandlerI
virtual	void		handle(Record& a_rSignal);

	private:
};

} /* namespace ext */
} /* namespace fe */

#endif /* __maya_CurveSeparateOp_h__ */
