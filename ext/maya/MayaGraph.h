/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __maya_MayaGraph_h__
#define __maya_MayaGraph_h__

//* NOTE needed to key std::map<> with MObject
inline bool operator<(const MObject& rLeft,const MObject& rRight)
{
	return *(int*)(void*)(&rLeft) < *(int*)(void*)(&rRight);
}

namespace fe
{
namespace ext
{

//* public accessible version of MObject (cast by pointer)
class MayaObject
{
	public:
		MPtrBase*		ptr;
#if defined (MBits64_)
		MFn::Type		tp;
#else
		unsigned short	tp;
#endif // defined (MBits64_)
		unsigned short	flags;
};

/**************************************************************************//**
    @brief  Maya DAG Navigation

	@ingroup maya
*//***************************************************************************/
class FE_DL_EXPORT MayaGraph:
	public MetaGraph,
	public CastableAs<MayaGraph>
{
	public:
						MayaGraph(void);
virtual					~MayaGraph(void);

virtual		sp<ImageI>	image(void);

virtual	BWORD			connect(String a_outputName,String a_outputConnector,
								String a_inputName,String a_inputConnector);
virtual	BWORD			disconnect(String a_inputName,String a_inputConnector);

			BWORD		isBuilt(void)	{ return m_spImage.isValid(); }
			void		discard(void)	{ m_spImage=NULL; }

	private:

			sp<ImageI>	rebuild(void);

			MCallbackId	m_cid;

			sp<ImageI>	m_spImage;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __maya_MayaGraph_h__ */
