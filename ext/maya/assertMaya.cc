/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <maya/maya.pmh>

namespace fe
{
namespace ext
{

class InfoMayaMesh : public BaseType::Info
{
virtual	String	print(void *instance)
				{
					MDataHandle meshData= *(MDataHandle *)instance;
					return meshData.asMesh().isNull()? "<invalid>": "<valid>";
				}

virtual	IWORD	output(std::ostream &ostrm, void *instance, t_serialMode mode)
				{
					if(mode == e_ascii) { return c_noascii; }
					return 0;
				}
virtual	void	input(std::istream &istrm, void *instance, t_serialMode mode)
				{
				}
virtual	IWORD	iosize(void)				{ return c_implicit; }
virtual	bool	getConstruct(void)			{ return true; }
virtual	void	construct(void *instance)	{ new(instance)MDataHandle; }
virtual	void	destruct(void *instance)
				{
					MDataHandle meshData= *(MDataHandle *)instance;
					meshData.~MDataHandle();
				}
};

void assertMaya(sp<TypeMaster> spTypeMaster)
{
	sp<BaseType> spT;

	spT = spTypeMaster->assertType<MDataHandle>("MayaMesh");
	spT->setInfo(new InfoMayaMesh());
}

} /* namespace ext */
} /* namespace fe */
