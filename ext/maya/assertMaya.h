/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __maya_assertMaya_h__
#define __maya_assertMaya_h__

namespace fe
{

template<>
inline bool Type<MDataHandle>::equiv(void *a_a, void *a_b)
{
	MDataHandle meshDataA= *(MDataHandle *)a_a;
	MDataHandle meshDataB= *(MDataHandle *)a_b;

	return meshDataA.data()==meshDataB.data();
}

namespace ext
{

FE_DL_EXPORT void assertMaya(sp<TypeMaster> spTypeMaster);

} /* namespace ext */
} /* namespace fe */

#endif /* __maya_assertMaya_h__ */
