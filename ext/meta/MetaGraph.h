/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __operator_MetaGraph_h__
#define __operator_MetaGraph_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Common graph navigation for Houdini, Maya, etc

	@ingroup meta
*//***************************************************************************/
class FE_DL_EXPORT MetaGraph: public OperatorGraphI
{
	protected:

	class MetaNode: public DAGNode
	{
		public:
			void	set(void* a_nativeNode,String a_nodeName)
					{	m_nativeNode=a_nativeNode;
						m_nodeName=a_nodeName; }

			void*	m_nativeNode;		//* MObject.ptr for Maya
			String	m_nodeName;
	};

	public:
					MetaGraph(void)											{}
virtual				~MetaGraph(void)										{}

virtual	U32			inputCount(String a_nodeName) const;
virtual	String		inputConnector(String a_nodeName,U32 a_index) const;
virtual	BWORD		hasInputConnector(String a_nodeName,
							String a_inputConnector) const;
virtual	String		inputNode(String a_nodeName,
							String a_inputConnector) const;
virtual	String		inputNodeOutputConnector(String a_nodeName,
							String a_inputConnector) const;

virtual	U32			outputCount(String a_nodeName) const;
virtual	String		outputConnector(String a_nodeName,U32 a_index) const;
virtual	BWORD		hasOutputConnector(String a_nodeName,
							String a_outputConnector) const;
virtual	String		outputNode(String a_nodeName,
							String a_outputConnector) const;

		U32			inputCount(const void* a_rNativeNode) const;
		String		inputConnector(const void* a_rNativeNode,
							U32 a_index) const;
		BWORD		hasInputConnector(const void* a_rNativeNode,
							String a_inputConnector) const;
		void*		inputNode(const void* a_rNativeNode,
							String a_inputConnector) const;

		U32			outputCount(const void* a_rNativeNode) const;
		String		outputConnector(const void* a_rNativeNode,
							U32 a_index) const;
		BWORD		hasOutputConnector(const void* a_rNativeNode,
							String a_outputConnector) const;
		void*		outputNode(const void* a_rNativeNode,
							String a_outputConnector) const;

	protected:

		BWORD		addConnection(String a_outputName,String a_outputConnector,
							String a_inputName,String a_inputConnector);
		BWORD		removeConnection(String a_inputName,
							String a_inputConnector);
		void		clear(void);
		void		add(void* a_nativeNode,String a_nodeName);
		void		addInput(const void* a_nativeNode,
							String a_inputConnector);
		void		addOutput(const void* a_nativeNode,
							String a_outputConnector);
		BWORD		addConnection(void* a_outputNode,String a_outputConnector,
							void* a_inputNode,String a_inputConnector);
		BWORD		removeConnection(void* a_inputNode,String a_inputConnector);

		void		dump(void) const;
		String		generateDot(void) const;

		sp<ImageI>	render(void);

	private:

		std::map< const void*, sp<MetaNode> >	m_nodeMap;
		std::map<String,const void*>			m_voidMap;
		std::map<const void*,String>			m_nameMap;

		String		m_source;
		sp<ImageI>	m_spRenderer;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __operator_MetaGraph_h__ */
