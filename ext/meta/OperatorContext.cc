/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <meta/meta.pmh>

#define	FE_OPC_DEBUG	FALSE

namespace fe
{
namespace ext
{

FE_DL_PUBLIC BWORD OperatorContext::ms_logged=FALSE;
FE_DL_PUBLIC BWORD OperatorContext::ms_loaded=FALSE;
FE_DL_PUBLIC BWORD OperatorContext::ms_registered=FALSE;
FE_DL_PUBLIC sp<Master> OperatorContext::ms_spMaster;

OperatorContext::OperatorContext(void):
	m_libDefault("fexOperatorDL")
{
	//* see SingleMaster
	if(PrefixLog::prefix().empty())
	{
		PrefixLog* pPrefixLog=new PrefixLog();

#if FE_OS==FE_WIN32 || FE_OS==FE_WIN64
		pPrefixLog->setPrefix("fe  ");
#else
		pPrefixLog->setPrefix("[34mfe[0m  ");
#endif

		feLogger()->clearAll();
		feLogger()->setLog("stdout",pPrefixLog);
		feLogger()->bind(".*", "stdout");
	}

#if FE_OPC_DEBUG
	feLog("OperatorContext\n");
#endif

	m_spMasterHold=ms_spMaster;

#if FE_OPC_DEBUG
	feLog("OperatorContext done\n");
#endif
}

OperatorContext::~OperatorContext(void)
{
#if FE_OPC_DEBUG
	feLog("~OperatorContext\n");
#endif
	m_spMasterHold=NULL;
	m_spScope=NULL;

#if FE_OPC_DEBUG
	if(ms_spMaster.isValid())
	{
		feLog("~OperatorContext Master count is %d\n",ms_spMaster->count());
	}
#endif

	if(ms_spMaster.isValid() && ms_spMaster->count()==1)
	{
#if FE_OPC_DEBUG
		feLog("~OperatorContext releasing Master\n");
#endif
		ms_spMaster=NULL;
	}
}

sp<Master> OperatorContext::master(void)
{
#if FE_OPC_DEBUG
	feLog("OperatorContext::master\n");
#endif

	if(!ms_spMaster.isValid())
	{
		ms_spMaster=new Master();
		ms_loaded=FALSE;
	}

	if(!ms_registered)
	{
		feRegisterSegvHandler();
		ms_registered=TRUE;
	}


#if FE_OPC_DEBUG
	feLog("OperatorContext::master %p\n",ms_spMaster.raw())
#endif

	m_spMasterHold=ms_spMaster;
	return ms_spMaster;
}

sp<Scope> OperatorContext::scope(void)
{
	if(!m_spScope.isValid() && master().isValid())
	{
		m_spScope=master()->catalog()->catalogComponent(
				"Scope","OperatorScope");

#if FE_OPC_DEBUG
	feLog("OperatorContext::scope 0x%p\n",m_spScope.raw())
#endif
	}
	return m_spScope;
}

BWORD OperatorContext::loadLibraries(void)
{
#if FE_OPC_DEBUG
	feLog("OperatorContext::loadLibraries\n");
#endif

	if(ms_loaded)
	{
		return FALSE;
	}

	sp<Registry> spRegistry=master()->registry();

	String libs;
	System::getEnvironmentVariable(m_libEnvVar, libs);
	if(libs.empty())
	{
		System::getEnvironmentVariable("FE_OPERATOR_LIBS", libs);
	}
	if(libs.empty())
	{
		libs=m_libDefault;
	}

	BWORD logUsing=FALSE;
	BWORD logMissing=TRUE;
	const String verbose=System::getVerbose();
	if(verbose=="none")
	{
		logMissing=FALSE;
	}
	else if(verbose=="all")
	{
		logUsing=TRUE;
	}

#if FE_OPC_DEBUG
	feLog("OperatorContext::loadLibraries managing \"%s\"\n",libs.c_str());
#endif

	while(!libs.empty())
	{
		String lib=libs.parse("'",":");
		if(lib.empty())
		{
			break;
		}
		try
		{
			Result result=spRegistry->manage(lib);

			if(!ms_logged)
			{
				if(failure(result))
				{
					if(logMissing)
					{
						feLog("missing %s\n",lib.c_str());
					}
				}
				else
				{
					if(logUsing)
					{
						feLog("adding %s\n",lib.c_str());
					}

					libraryLoaded(lib);
				}
			}
#if FE_OPC_DEBUG
			feLog("OperatorContext::loadLibraries manage \"%s\" result=%p\n",
					lib.c_str(),result);
#endif
		}
		catch(...)
		{
			if(logMissing)
			{
				feLog("faulted %s\n",lib.c_str());
			}
		}
	}

	ms_logged=TRUE;
	ms_loaded=TRUE;

	return TRUE;
}

} /* namespace ext */
} /* namespace fe */
