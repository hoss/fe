/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __meta_OperatorContext_h__
#define __meta_OperatorContext_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
    @brief shared FE context for Operator plugins

	@ingroup meta

	TODO shouldn't this be Counted?

*//***************************************************************************/
class FE_DL_EXPORT OperatorContext
{
	public:
					OperatorContext(void);
virtual				~OperatorContext(void);

		sp<Master>	master(void);
		sp<Scope>	scope(void);

		BWORD		loadLibraries(void);

virtual	void		libraryLoaded(String a_libraryName)						{}

	protected:
						String			m_libEnvVar;
						String			m_libDefault;

	private:

static	FE_DL_PUBLIC	BWORD			ms_logged;
static	FE_DL_PUBLIC	BWORD			ms_loaded;
static	FE_DL_PUBLIC	BWORD			ms_registered;
static	FE_DL_PUBLIC	sp<Master>		ms_spMaster;
						sp<Master>		m_spMasterHold;
						sp<Scope>		m_spScope;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __meta_OperatorContext_h__ */
