/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __operator_MetaBrush_h__
#define __operator_MetaBrush_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Common functionality of meta brushes for Houdini, Maya, etc

	@ingroup meta
*//***************************************************************************/
class FE_DL_EXPORT MetaBrush
{
	public:
								MetaBrush(void);
virtual							~MetaBrush(void);

		sp<Master>				master(void) const	{ return m_spMaster; }

	protected:
		void					initEvent(sp<Scope>& a_rspScope);
		void					initDrawInterfaces(MetaPlugin* a_pMetaPlugin);
		void					updateDrawInterfaces(void);
		void					setupDrawOpenGL(void);
		void					draw(void);
		void					drawBrush(void);

virtual	void					drawPost(void)								{}

		WindowEvent				m_event;
		Box2i					m_viewport;

		OperatorContext*		m_pOperatorContext;
		sp<Master>				m_spMaster;
		sp<DrawI>				m_spDrawOpenGL;
		sp<DrawI>				m_spDrawPerspective;
		sp<DrawI>				m_spDrawOrtho;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __operator_MetaBrush_h__ */
