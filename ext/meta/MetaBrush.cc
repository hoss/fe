/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <meta/meta.pmh>

#define FE_MBR_LOG_EXCEPTIONS	TRUE

using namespace fe;
using namespace fe::ext;

MetaBrush::MetaBrush(void):
	m_pOperatorContext(NULL)
{
}

MetaBrush::~MetaBrush(void)
{
}

void MetaBrush::initEvent(sp<Scope>& a_rspScope)
{
	m_event.bind(a_rspScope);
	sp<Layout> spLayout=m_event.layout();
	Record record=a_rspScope->createRecord(spLayout);
	m_event.bind(record);
	m_event.reset();

	//* NOTE Houdini may try destruct the brush way too late,
	//* so get rid of references as soon as possible
	FEASSERT(m_pOperatorContext);
	m_spMaster=m_pOperatorContext->master();
	delete(m_pOperatorContext);
	m_pOperatorContext=NULL;
}

void MetaBrush::initDrawInterfaces(MetaPlugin* a_pMetaPlugin)
{
	m_spDrawPerspective=a_pMetaPlugin->drawBrush();
	m_spDrawOrtho=a_pMetaPlugin->drawBrushOverlay();
	updateDrawInterfaces();
}

void MetaBrush::updateDrawInterfaces(void)
{
	sp<DrawCached> spDrawCached=m_spDrawPerspective;
	if(spDrawCached.isValid())
	{
		spDrawCached->setDrawChain(m_spDrawOpenGL);
	}
	spDrawCached=m_spDrawOrtho;
	if(spDrawCached.isValid())
	{
		spDrawCached->setDrawChain(m_spDrawOpenGL);
	}
}

void MetaBrush::setupDrawOpenGL(void)
{
	updateDrawInterfaces();

	sp<DrawMode> spDrawMode(new DrawMode);
	spDrawMode->setDrawStyle(DrawMode::e_solid);
	spDrawMode->setLineWidth(1.0);
	spDrawMode->setPointSize(2.0);
//	spDrawMode->setAntialias(TRUE);

	m_spDrawOpenGL->pushDrawMode(spDrawMode);
}

void MetaBrush::draw(void)
{
	try
	{
		drawBrush();
	}
	catch(const Exception &e)
	{
#if FE_MBR_LOG_EXCEPTIONS
		feLog("MetaBrush::draw"
				" caught fe::Exception %s\n",c_print(e));
#endif
	}
	catch(const std::exception& std_e)
	{
#if FE_MBR_LOG_EXCEPTIONS
		feLog("MetaBrush::draw"
				" caught std::exception %s\n",std_e.what());
#endif
	}
#ifdef FE_BOOST_EXCEPTIONS
	catch(const boost::exception& boost_e)
	{
#if FE_MBR_LOG_EXCEPTIONS
		feLog("MetaBrush::draw"
				" caught boost::exception %s\n",
				boost::diagnostic_information(boost_e).c_str());
#endif
	}
#endif
	catch(...)
	{
#if FE_MBR_LOG_EXCEPTIONS
		feLog("MetaBrush::draw"
				" caught unknown exception\n");
#endif
	}
}

void MetaBrush::drawBrush(void)
{
	const BWORD createDraw=m_spDrawOpenGL.isNull();
	if(createDraw)
	{
		feLog("MetaBrush::drawBrush creating DrawOpenGL\n");

		if(m_spMaster.isNull())
		{
			feLog("MetaBrush::drawBrush m_spMaster invalid\n");
			return;
		}

		m_spMaster->registry()->manifest()->catalog<I32>(
				"DrawI.DrawOpenGL.fe","priority")=1;
		m_spDrawOpenGL=m_spMaster->registry()->create("DrawI");
		if(m_spDrawOpenGL.isValid())
		{
			m_spDrawOpenGL->setName("MetaBrush DrawI");
		}

		//* only try to create once
		m_spMaster=NULL;
	}

	if(m_spDrawOpenGL.isNull())
	{
		feLog("MetaBrush::drawBrush m_spDrawOpenGL invalid\n");
		return;
	}

	//* sandbox any OpenGL state changes
	m_spDrawOpenGL->pushMatrixState();

	if(createDraw)
	{
		setupDrawOpenGL();
	}

	//* TODO where is the pop?
	sp<DrawMode> spDrawMode=m_spDrawOpenGL->popDrawMode();
	m_spDrawOpenGL->pushDrawMode(spDrawMode);

	sp<DrawCached> spDrawCached=m_spDrawPerspective;
	if(spDrawCached.isNull())
	{
		feLog("MetaBrush::drawBrush spDrawCached invalid\n");
		return;
	}

	sp<ViewI> spViewI=spDrawCached->view();
	if(spViewI.isNull())
	{
		feLog("MetaBrush::drawBrush spViewI invalid\n");
		return;
	}

	spViewI->setViewport(m_viewport);

	//* perspective
//	spViewI->use(CameraI::e_perspective);

	//* NOTE if application leaves a VBO bound, glVertexPointer will fail
	m_spDrawOpenGL->unbindVertexArray();

	if(m_spDrawPerspective.isValid())
	{
		m_spDrawOpenGL->pushDrawMode(spDrawMode);
		m_spDrawPerspective->flushLive();
		m_spDrawOpenGL->popDrawMode();
	}

	//* ortho
//	spViewI->use(CameraI::e_ortho);

	if(m_spDrawOrtho.isValid())
	{
		m_spDrawOpenGL->pushDrawMode(spDrawMode);
		m_spDrawOrtho->flushLive();
		m_spDrawOpenGL->popDrawMode();
	}

	drawPost();

	//* restore original state
	m_spDrawOpenGL->popMatrixState();
}
