/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __operator_MetaRamp_h__
#define __operator_MetaRamp_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Common ramp interface for Houdini, Maya, etc

	@ingroup meta
*//***************************************************************************/
class FE_DL_EXPORT MetaRamp:
	public RampI,
	public CastableAs<MetaRamp>
{
	public:
				MetaRamp(void)												{}
virtual			~MetaRamp(void)												{}

				//* do we need this?

virtual	U32		entryCount(void) const		{ return 0; }
virtual	Vector2	entry(U32 a_index) const	{ return Vector2(0.0,0.0); }

virtual	Real	eval(Real a_u) const		{ return 0.0; }
};

} /* namespace ext */
} /* namespace fe */

#endif /* __operator_MetaRamp_h__ */
