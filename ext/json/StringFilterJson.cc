/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <json/json.pmh>

#define FE_SFJ_DEBUG	FALSE

namespace fe
{
namespace ext
{

BWORD StringFilterJson::load(String a_filename)
{
#if FE_SFJ_DEBUG
	feLog("StringFilterJson::load \"%s\"\n",a_filename.c_str());
#endif

	if(m_spJsonRoot.isNull())
	{
		m_spJsonRoot=sp<JsonValue>(new JsonValue());
	}
	m_spJsonRoot->value().clear();

	std::ifstream in(a_filename.c_str());

	if(!in.good())
	{
		feLog("StringFilterJson::load failed to open\n  \"%s\"\n",
				a_filename.c_str());

		return FALSE;
	}

	Json::Reader reader;
	const BWORD success=reader.parse(in,m_spJsonRoot->value());

	if(!success)
	{
		feLog("StringFilterJson::load failed to parse\n  \"%s\"\n"
				"  because \"%s\"\n",a_filename.c_str(),
				reader.getFormattedErrorMessages().c_str());
	}

	return success;
}

String StringFilterJson::filter(String a_string)
{
	BWORD allowed=FALSE;

	const I32 directiveCount=m_spJsonRoot->value().size();
	for(I32 directiveIndex=0;directiveIndex<directiveCount;directiveIndex++)
	{
		Json::Value directive=m_spJsonRoot->value()[directiveIndex];

		const Json::Value ifValue=directive["if"];
		const String ifString=ifValue.isString()? ifValue.asCString(): "";

#if FE_SFJ_DEBUG
		feLog("%d/%d if \"%s\"\n",directiveIndex,directiveCount,
				ifString.c_str());
#endif

		if(!ifString.empty() && !m_config.match(ifString))
		{
			continue;
		}

		const Json::Value doValue=directive["do"];
		const String doString=doValue.isString()? doValue.asCString(): "";

		const BWORD allow=(doString=="allow");

#if FE_SFJ_DEBUG
		feLog("  do \"%s\"\n",doString.c_str());
#endif

		const Json::Value reList=directive["re"];

		const I32 reCount=reList.size();
		for(I32 reIndex=0;reIndex<reCount;reIndex++)
		{
			const Json::Value reValue=reList[reIndex];
			if(!reValue.isString())
			{
				continue;
			}

			const String expression=reValue.asCString();

#if FE_SFJ_DEBUG
			feLog("    %d/%d \"%s\"\n",reIndex,reCount,expression.c_str());
#endif

			if(a_string.match(expression))
			{
				allowed=allow;
			}
		}
	}

	return allowed? a_string: "";
}

} /* namespace ext */
} /* namespace fe */
