/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <json/json.pmh>

#define FE_CWJ_DEBUG	FALSE

namespace fe
{
namespace ext
{

BWORD CatalogWriterJson::save(String a_filename, sp<Catalog> spCatalog)
{
#if FE_CWJ_DEBUG
	feLog("CatalogWriterJson::save \"%s\"\n",a_filename.c_str());
#endif

	m_spJsonRoot=sp<JsonValue>(new JsonValue());
	m_spJsonRoot->value()=Json::objectValue;

	std::ofstream ostrm(a_filename.c_str());
	if(!ostrm.is_open())
	{
		feLog("CatalogWriterJson::save failed to open \"%s\"\n",
				a_filename.c_str());
		return FALSE;
	}

	Array<String> keys;
	spCatalog->catalogKeys(keys);
	const U32 keyCount=keys.size();
	for(U32 keyIndex=0;keyIndex<keyCount;keyIndex++)
	{
		const String key=keys[keyIndex];
#if FE_CWJ_DEBUG
		feLog("key \"%s\"\n",key.c_str());
#endif

//		const Instance& rInstance=spCatalog->catalogInstance(key);
		String typeName=spCatalog->catalogTypeName(key);
		String text=spCatalog->catalogValue(key);

		Json::Value jsonEntry;
		jsonEntry["type"]=typeName.c_str();
		jsonEntry["value"]=text.c_str();
		m_spJsonRoot->value()[key.c_str()]=jsonEntry;

		Array<String> properties;
		spCatalog->catalogProperties(key,properties);
		const U32 propertyCount=properties.size();
		for(U32 propertyIndex=0;propertyIndex<propertyCount;propertyIndex++)
		{
			const String property=properties[propertyIndex];
			if(property=="type" || property=="value")
			{
				continue;
			}

#if FE_CWJ_DEBUG
			feLog("  property \"%s\"\n",property.c_str());
#endif

//			const Instance& rInstance2=
//					spCatalog->catalogInstance(key,property);
			typeName=spCatalog->catalogTypeName(key,property);
			text=spCatalog->catalogValue(key,property);

			jsonEntry["type"]=typeName.c_str();
			jsonEntry["value"]=text.c_str();
			m_spJsonRoot->value()[key.c_str()][property.c_str()]=jsonEntry;
		}
	}

	Json::StyledStreamWriter writer;
	writer.write(ostrm,m_spJsonRoot->value());
	ostrm.close();

	return TRUE;
}

} /* namespace ext */
} /* namespace fe */
