/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __json_JsonWriter_h__
#define __json_JsonWriter_h__

namespace fe
{
namespace ext
{
namespace data
{

/**************************************************************************//**
    @brief Stream generator for writing JSON files

	@ingroup json
*//***************************************************************************/
class FE_DL_EXPORT JsonWriter : public fe::data::AsciiWriter
{
	public:
				JsonWriter(sp<Scope> spScope);
virtual			~JsonWriter(void);
virtual	void	output(std::ostream &a_ostrm, sp<RecordGroup> spRG);

	private:

virtual	void	write(std::ostream &a_ostrm, Record r_out, int a_sb_id);
virtual	void	write(std::ostream &a_ostrm, sp<RecordGroup> spRG, int a_id);
virtual	void	write(std::ostream &a_ostrm, sp<RecordArray> spRA, int a_id);
virtual	void	write(std::ostream &a_ostrm, sp<Layout> spLayout, int a_id);
virtual	void	write(std::ostream &a_ostrm, sp<Attribute> spAttribute);

		sp<JsonValue>		m_spJsonRoot;

		Json::Value			m_jsonAttributes;
		Json::Value			m_jsonLayouts;
		Json::Value			m_jsonRecords;
		Json::Value			m_jsonRecordGroups;
		Json::Value			m_jsonRecordArrays;
};

} /* namespace ext */
} /* namespace fe */

} /* namespace */

#endif /* __json_JsonWriter_h__ */

