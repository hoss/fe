/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __json_JsonStream_h__
#define __json_JsonStream_h__
namespace fe
{
namespace ext
{
namespace data
{

class FE_DL_EXPORT JsonStream : virtual public fe::data::StreamI
{
	public:
							JsonStream(void);
virtual						~JsonStream(void);
virtual	void				bind(sp<Scope> spScope);
virtual	void				output(std::ostream &ostrm, sp<RecordGroup> spRG);
virtual	sp<RecordGroup>		input(std::istream &istrm);
const	String&				name(void) const	{ return m_name; }
	private:
		sp<Scope>				m_spScope;
		sp<fe::data::Writer>	m_spWriter;
		sp<fe::data::Reader>	m_spReader;
		String					m_name;
};


} /* namespace ext */
} /* namespace fe */
} /* namespace */

#endif /* __json_JsonStream_h__ */

