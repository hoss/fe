/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __json_StringFilterJson_h__
#define __json_StringFilterJson_h__

namespace fe
{
namespace ext
{

//* TODO StringFilterI interface

/**************************************************************************//**
    @brief JSON String Check

	@ingroup json
*//***************************************************************************/
class FE_DL_EXPORT StringFilterJson:
	virtual public StringFilterI
{
	public:
								StringFilterJson(void)					{}
virtual							~StringFilterJson(void)					{}

virtual	BWORD					load(String a_filename);

virtual	void					configure(String a_config)
								{	m_config=a_config; }

virtual	String					configuration(void)
								{	return m_config; }

virtual	String					filter(String a_string);

								//* JSON specific
		void					setJsonRoot(sp<JsonValue> a_spJsonRoot)
								{	m_spJsonRoot=a_spJsonRoot; }

		sp<JsonValue>			jsonRoot(void)		{ return m_spJsonRoot; }

	private:

		sp<JsonValue>			m_spJsonRoot;

		String					m_config;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __json_StringFilterJson_h__ */
