/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "fe/plugin.h"

using namespace fe;

int main(int argc,char** argv)
{
	UNIT_START();

	BWORD complete=FALSE;

	if(argc<2)
	{
		feLog("Usage: %s [json file]\n",argv[0]);
		return -1;
	}
	String filename=argv[1];

	try
	{
		sp<Master> spMaster(new Master);
		sp<Registry> spRegistry=spMaster->registry();

		Result result=spRegistry->manage("fexJsonDL");
		UNIT_TEST(successful(result));

		sp<Catalog> spCatalogOut=spMaster->createCatalog("output");
		UNIT_TEST(spCatalogOut.isValid());
		if(!spCatalogOut.isValid())
		{
			feX(argv[0],"spCatalogOut invalid");
		}

		spCatalogOut->catalog<String>("A")="alpha";
		spCatalogOut->catalog<Real>("C")=3.14;
		spCatalogOut->catalog<String>("C","hint")="it's pi";
		spCatalogOut->catalog<bool>("D")=true;

		feLog("\nCatalog out:\n");
		spCatalogOut->catalogDump();

		sp<CatalogWriterI> spCatalogWriter=
				spRegistry->create("CatalogWriterI.*.*.json");
		UNIT_TEST(spCatalogWriter.isValid());
		if(!spCatalogWriter.isValid())
		{
			feX(argv[0],"spCatalogWriter invalid");
		}

		System::createParentDirectories(filename);

		feLog("saving \"%s\"\n",filename.c_str());
		spCatalogWriter->save(filename,spCatalogOut);

		sp<CatalogReaderI> spCatalogReader=
				spRegistry->create("CatalogReaderI.*.*.json");
		UNIT_TEST(spCatalogReader.isValid());
		if(!spCatalogReader.isValid())
		{
			feX(argv[0],"spCatalogReader invalid");
		}

		sp<Catalog> spCatalogIn=spMaster->createCatalog("input");
		UNIT_TEST(spCatalogIn.isValid());
		if(!spCatalogIn.isValid())
		{
			feX(argv[0],"spCatalogIn invalid");
		}

		feLog("reloading \"%s\"\n",filename.c_str());
		spCatalogReader->load(filename,spCatalogIn);

		feLog("\nCatalog in:\n");
		spCatalogIn->catalogDump();

		complete=TRUE;
	}
	catch(Exception &e) { e.log(); }
	catch(...)			{ feLog("uncaught exception\n"); }

	UNIT_TEST(complete);
	UNIT_TEST(!Counted::trackerCount());
	UNIT_TRACK(7);
	UNIT_RETURN();
}
