/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "surface/surface.h"

using namespace fe;
using namespace fe::ext;

int main(int argc,char** argv)
{
	UNIT_START();

	BWORD complete=FALSE;

	if(argc<2)
	{
		feLog("Usage: %s [geo file]\n",argv[0]);
		return -1;
	}
	String filename=argv[1];

	try
	{
		sp<Master> spMaster(new Master);
		sp<Registry> spRegistry=spMaster->registry();

		Result result=spRegistry->manage("fexJsonDL");
		UNIT_TEST(successful(result));

		sp<SurfaceAccessibleI> spSurfaceAccessibleI=
				spRegistry->create("SurfaceAccessibleI.*.*.geo");

		const String longname=
				spRegistry->master()->catalog()->catalog<String>(
				"path:media")+"/model/"+filename;

		const BWORD loaded=spSurfaceAccessibleI->load(longname);
		UNIT_TEST(loaded);

		sp<SurfaceAccessorI> spPointPosition=spSurfaceAccessibleI->accessor(
				SurfaceAccessibleI::e_point,SurfaceAccessibleI::e_position);

		const U32 pointCount=spPointPosition->count();
		for(U32 pointIndex=0;pointIndex<pointCount;pointIndex++)
		{
			const SpatialVector point=
					spPointPosition->spatialVector(pointIndex);
			feLog("point %d/%d %s\n",pointIndex,pointCount,c_print(point));
		}

		complete=TRUE;
	}
	catch(Exception &e) { e.log(); }
	catch(...)			{ feLog("uncaught exception\n"); }

	UNIT_TEST(complete);
	UNIT_TEST(!Counted::trackerCount());
	UNIT_TRACK(4);
	UNIT_RETURN();
}
