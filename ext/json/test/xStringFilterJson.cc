/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "surface/surface.h"

using namespace fe;
using namespace fe::ext;

int main(int argc,char** argv)
{
	UNIT_START();

	BWORD complete=FALSE;

	if(argc<2)
	{
		feLog("Usage: %s [json file]\n",argv[0]);
		return -1;
	}
	String filename=argv[1];

	try
	{
		sp<Master> spMaster(new Master);
		sp<Registry> spRegistry=spMaster->registry();

		Result result=spRegistry->manage("fexJsonDL");
		UNIT_TEST(successful(result));

		sp<StringFilterI> spStringFilter=
				spRegistry->create("StringFilterI.*.*.json");

		spStringFilter->load(filename);

		const String good=spStringFilter->filter("a_good_string");
		feLog("good \"%s\"\n",good.c_str());
		UNIT_TEST(!good.empty());

		const String bad=spStringFilter->filter("the_bad_one");
		feLog("bad \"%s\"\n",bad.c_str());
		UNIT_TEST(bad.empty());

		complete=TRUE;
	}
	catch(Exception &e) { e.log(); }
	catch(...)			{ feLog("uncaught exception\n"); }

	UNIT_TEST(complete);
	UNIT_TEST(!Counted::trackerCount());
	UNIT_TRACK(5);
	UNIT_RETURN();
}
