/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "fe/data.h"

using namespace fe;

int main(int argc,char** argv)
{
	UNIT_START();

	BWORD complete=FALSE;

	if(argc<2)
	{
		feLog("Usage: %s [rg file]\n",argv[0]);
		return -1;
	}
	String filename=argv[1];

	try
	{
		sp<Master> spMaster(new Master);
		sp<Registry> spRegistry=spMaster->registry();

		Result result=spRegistry->manage("fexJsonDL");
		UNIT_TEST(successful(result));

		sp<Scope> spScope=spRegistry->create("Scope");
		if(spScope.isNull())
		{
			feX(argv[0],"could not create Scope");
		}

		const String inputName=
				spRegistry->master()->catalog()->catalog<String>(
				"path:media")+"/"+filename;

		feLog("> opening \"%s\"\n",inputName.c_str());

		std::ifstream infile(inputName.c_str());
		if(!infile)
		{
			feX(argv[0],"could not open test input file");
		}

		sp<data::StreamI> spInStream=spRegistry->create("StreamI.AsciiStream");
		if(spInStream.isNull())
		{
			feX(argv[0],"could not create input stream");
		}

		feLog("> reading \"%s\"\n",inputName.c_str());
		spInStream->bind(spScope);
		sp<RecordGroup> spRG=spInStream->input(infile);
		UNIT_TEST(spRG.isValid());

		const String outputName="output/test/json/xJsonRecord.json";
		System::createParentDirectories(outputName);

		std::ofstream outfile(outputName.c_str());

		sp<data::StreamI> spOutStream=spRegistry->create("StreamI.JsonStream");
		if(spOutStream.isNull())
		{
			feX(argv[0],"could not create output stream");
		}

		feLog("> writing \"%s\"\n",outputName.c_str());
		spOutStream->bind(spScope);
		spOutStream->output(outfile, spRG);

		outfile.close();

		sp<Scope> spScope2=spRegistry->create("Scope");
		if(spScope2.isNull())
		{
			feX(argv[0],"could not create second Scope");
		}

		std::ifstream reloadfile(outputName.c_str());
		if(!reloadfile)
		{
			feX(argv[0],"could not open test reload file");
		}

		sp<data::StreamI> spReloadStream=
				spRegistry->create("StreamI.JsonStream");
		if(spReloadStream.isNull())
		{
			feX(argv[0],"could not create reload stream");
		}

		feLog("> rereading \"%s\"\n",outputName.c_str());
		spReloadStream->bind(spScope2);
		sp<RecordGroup> spReloadRG=spReloadStream->input(reloadfile);

		reloadfile.close();

		const String rewriteName="output/test/json/xJsonRecord.rg";
		std::ofstream rewritefile(rewriteName.c_str());
		if(!rewritefile)
		{
			feX(argv[0],"could not open test rewrite file");
		}

		sp<data::StreamI> spRewriteStream=
				spRegistry->create("StreamI.AsciiStream");
		if(spRewriteStream.isNull())
		{
			feX(argv[0],"could not create rewrite stream");
		}

		feLog("> writing \"%s\"\n",rewriteName.c_str());
		spRewriteStream->bind(spScope2);
		spRewriteStream->output(rewritefile, spReloadRG);

		rewritefile.close();

		sp<Scope> spScope3=spRegistry->create("Scope");
		if(spScope3.isNull())
		{
			feX(argv[0],"could not create second Scope");
		}

		const String templateName="ext/json/test/template.json";
		std::ifstream templatefile(templateName.c_str());
		if(!templatefile)
		{
			feX(argv[0],"could not open test template file");
		}

		sp<data::StreamI> spTemplateStream=
				spRegistry->create("StreamI.JsonStream");
		if(spTemplateStream.isNull())
		{
			feX(argv[0],"could not create template stream");
		}

		feLog("> reading template \"%s\"\n",templateName.c_str());
		spTemplateStream->bind(spScope3);
		sp<RecordGroup> spTemplateRG=spTemplateStream->input(templatefile);

		templatefile.close();

		sp<Scope> spScope4=spRegistry->create("Scope");
		if(spScope4.isNull())
		{
			feX(argv[0],"could not create second Scope");
		}

		const String groupInputName=
				spRegistry->master()->catalog()->catalog<String>(
				"path:media")+"/surface/triangle.json";
		std::ifstream groupInputfile(groupInputName.c_str());
		if(!groupInputfile)
		{
			feX(argv[0],"could not open group test file");
		}

		sp<data::StreamI> spGroupInputStream=
				spRegistry->create("StreamI.JsonStream");
		if(spGroupInputStream.isNull())
		{
			feX(argv[0],"could not create groupInput stream");
		}

		feLog("> reading group test \"%s\"\n",groupInputName.c_str());
		spGroupInputStream->bind(spScope4);
		sp<RecordGroup> spGroupInputRG=
				spGroupInputStream->input(groupInputfile);

		groupInputfile.close();

		const String groupOutputName=
				"output/test/json/xJsonRecord_triangle.json";
		std::ofstream groupOutfile(groupOutputName.c_str());

		sp<data::StreamI> spGroupOutStream=
				spRegistry->create("StreamI.JsonStream");
		if(spGroupOutStream.isNull())
		{
			feX(argv[0],"could not create groupOutput Json stream");
		}

		feLog("> writing \"%s\"\n",groupOutputName.c_str());
		spGroupOutStream->bind(spScope4);
		spGroupOutStream->output(groupOutfile, spGroupInputRG);

		groupOutfile.close();

		const String groupOutputName2=
				"output/test/json/xJsonRecord_triangle.rg";
		std::ofstream groupOutfile2(groupOutputName2.c_str());

		sp<data::StreamI> spGroupOutStream2=
				spRegistry->create("StreamI.AsciiStream");
		if(spGroupOutStream2.isNull())
		{
			feX(argv[0],"could not create groupOutput Ascii stream");
		}

		feLog("> writing \"%s\"\n",groupOutputName2.c_str());
		spGroupOutStream2->bind(spScope4);
		spGroupOutStream2->output(groupOutfile2, spGroupInputRG);

		groupOutfile2.close();

		feLog("> test complete\n");
		complete=TRUE;
	}
	catch(Exception &e) { e.log(); }
	catch(...)			{ feLog("uncaught exception\n"); }

	UNIT_TEST(complete);
	UNIT_TEST(!Counted::trackerCount());
	UNIT_TRACK(4);
	UNIT_RETURN();
}
