/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __json_CatalogReaderJson_h__
#define __json_CatalogReaderJson_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
    @brief load catalog from json file

	@ingroup json
*//***************************************************************************/
class FE_DL_EXPORT CatalogReaderJson:
	public CatalogReaderBase
{
	public:

virtual	BWORD	load(String a_filename,sp<Catalog> a_spCatalog);

	private:

		sp<JsonValue>		m_spJsonRoot;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __json_CatalogReaderJson_h__ */
