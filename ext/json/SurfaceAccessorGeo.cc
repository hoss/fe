/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <json/json.pmh>

#define FE_SAG_DEBUG	FALSE

namespace fe
{
namespace ext
{

/* string detail:

			[
				[
					"scope","public",
					"type","string",
					"name","bindDriverGroup",
					"options",{
					}
				],
				[
					"size",1,
					"storage","int32",
					"strings",[""
					],
					"indices",[
						"size",1,
						"storage","int32",
						"arrays",[[0]
						]
					]
				]
			],

	int detail:

			[
				[
					"scope","public",
					"type","numeric",
					"name","bindCount",
					"options",{
						"type":{
							"type":"string",
							"value":"nonarithmetic_integer"
						}
					}
				],
				[
					"size",1,
					"storage","int32",
					"defaults",[
						"size",1,
						"storage","fpreal64",
						"values",[0]
					],
					"values",[
						"size",1,
						"storage","int32",
						"arrays",[[8]
						]
					]
				]
			],
*/

//* static
Json::Value* SurfaceAccessorGeo::findKey(
	Json::Value* a_pParent,String a_key)
{
	if(!a_pParent)
	{
		return NULL;
	}

	const I32 count=a_pParent->size();
	for(I32 index=0;index<count;index+=2)
	{
		if((*a_pParent)[index]==a_key.c_str())
		{
			return &(*a_pParent)[index+1];
		}
	}
	return NULL;
}

//* static
Json::Value* SurfaceAccessorGeo::accessPointAttributes(
	sp<JsonValue> a_spJsonRoot,BWORD a_create)
{
	Json::Value* pAttributes=SurfaceAccessorGeo::findKey(
			&a_spJsonRoot->value(),"attributes");

	return SurfaceAccessorGeo::findKey(pAttributes,"pointattributes");
}

//* static
Json::Value* SurfaceAccessorGeo::accessVertexAttributes(
	sp<JsonValue> a_spJsonRoot,BWORD a_create)
{
	Json::Value* pAttributes=SurfaceAccessorGeo::findKey(
			&a_spJsonRoot->value(),"attributes");

	return SurfaceAccessorGeo::findKey(pAttributes,"vertexattributes");
}

//* static
Json::Value* SurfaceAccessorGeo::accessPrimitiveAttributes(
	sp<JsonValue> a_spJsonRoot,BWORD a_create)
{
	Json::Value* pAttributes=SurfaceAccessorGeo::findKey(
			&a_spJsonRoot->value(),"attributes");

	return SurfaceAccessorGeo::findKey(pAttributes,"primitiveattributes");
}

//* static
Json::Value* SurfaceAccessorGeo::accessDetailAttributes(
	sp<JsonValue> a_spJsonRoot,BWORD a_create)
{
	Json::Value* pAttributes=SurfaceAccessorGeo::findKey(
			&a_spJsonRoot->value(),"attributes");

	Json::Value* pGlobalAttributes=
			SurfaceAccessorGeo::findKey(pAttributes,"globalattributes");

	if(pGlobalAttributes)
	{
		return pGlobalAttributes;
	}

	if(!a_create)
	{
		return NULL;
	}

	const I32 count=(*pAttributes).size();

	a_spJsonRoot->value()[count]="globalattributes";
	a_spJsonRoot->value()[count+1]=a_spJsonRoot->value()[count-1]; //* HACK
	return &a_spJsonRoot->value()[count+1][5];
}

//* static
Json::Value* SurfaceAccessorGeo::accessAttributes(
	sp<JsonValue> a_spJsonRoot,SurfaceAccessibleI::Element a_element,
	BWORD a_create)
{
	switch(a_element)
	{
		case SurfaceAccessibleI::e_point:
			return accessPointAttributes(a_spJsonRoot,a_create);
		case SurfaceAccessibleI::e_vertex:
			return accessVertexAttributes(a_spJsonRoot,a_create);
		case SurfaceAccessibleI::e_primitive:
			return accessPrimitiveAttributes(a_spJsonRoot,a_create);
		case SurfaceAccessibleI::e_detail:
			return accessDetailAttributes(a_spJsonRoot,a_create);
		default:
			;
	}

	return NULL;
}

//* TODO cache instead of finding every time
I32 SurfaceAccessorGeo::elementCount(
	SurfaceAccessibleI::Element a_element) const
{
	switch(a_element)
	{
		case SurfaceAccessibleI::e_point:
		{
			Json::Value* pPointCount=SurfaceAccessorGeo::findKey(
					&m_spJsonRoot->value(),"pointcount");
			return (*pPointCount).asInt();
		}
		case SurfaceAccessibleI::e_vertex:
		{
			Json::Value* pVertexCount=SurfaceAccessorGeo::findKey(
					&m_spJsonRoot->value(),"vertexcount");
			return (*pVertexCount).asInt();
		}
		case SurfaceAccessibleI::e_primitive:
		{
			Json::Value* pPrimitiveCount=SurfaceAccessorGeo::findKey(
					&m_spJsonRoot->value(),"primitivecount");
			return (*pPrimitiveCount).asInt();
		}
		case SurfaceAccessibleI::e_detail:
			return 1;
		default:
			;
	}

	return 0;
}

void SurfaceAccessorGeo::addAttribute(String a_type,String a_typeValue,
	String a_storage,U32 a_components)
{
#if FE_SAG_DEBUG
	feLog("SurfaceAccessorGeo::addAttribute \"%s\"\n",
			m_attrName.c_str());
#endif

	Json::Value* pAttributes=accessAttributes(m_spJsonRoot,m_element,TRUE);
	if(!pAttributes)
	{
		feLog("SurfaceAccessorGeo::addInteger failed access %d\n",m_element);
		return;
	}

	const U32 attrIndex=(*pAttributes).size();

	Json::Value& rAttribute=(*pAttributes)[(int)attrIndex];

	//* copy P as template
	Json::Value* pPointAttributes=
			accessAttributes(m_spJsonRoot,SurfaceAccessibleI::e_point,FALSE);
	rAttribute=(*pPointAttributes)[0];

	const U32 count=elementCount(m_element);

	rAttribute[0][3]=a_type.c_str();
	rAttribute[0][5]=m_attrName.c_str();
	if(!a_typeValue.empty())
	{
		rAttribute[0][7]["type"]["value"]=a_typeValue.c_str();
	}

	rAttribute[1][1]=(int)a_components;		//* size
	rAttribute[1][3]=a_storage.c_str();		//* storage

	rAttribute[1][7][1]=(int)a_components;	//* value size
	rAttribute[1][7][3]=a_storage.c_str();	//* value storage
	rAttribute[1][7][5].resize(0);
	rAttribute[1][7][5].resize(count);		//* values

	if(a_type=="string")
	{
		rAttribute[1][4]="strings";
		rAttribute[1][5].resize(0);
		rAttribute[1][5].resize(count);
		rAttribute[1][6]="indices";
		rAttribute[1][7][4]="arrays";

		for(U32 m=0;m<count;m++)
		{
			rAttribute[1][7][5][(int)m][0]=(int)m;
		}
	}
	else
	{
		rAttribute[1][5][1]=(int)a_components;	//* default size
		rAttribute[1][5][5].resize(0);			//* default array
		rAttribute[1][5][5][0]=0;				//* default
	}

	m_pElementValues=&rAttribute[1][7][5];
	m_pStringValues=&rAttribute[1][5];
}

void SurfaceAccessorGeo::addString(void)
{
#if FE_SAG_DEBUG
	feLog("SurfaceAccessorGeo::addString \"%s\"\n",
			m_attrName.c_str());
#endif

	addAttribute("string","","int32",1);
}

void SurfaceAccessorGeo::addInteger(void)
{
#if FE_SAG_DEBUG
	feLog("SurfaceAccessorGeo::addInteger \"%s\"\n",
			m_attrName.c_str());
#endif

	addAttribute("numeric","nonarithmetic_integer","int32",1);
}

void SurfaceAccessorGeo::addReal(void)
{
#if FE_SAG_DEBUG
	feLog("SurfaceAccessorGeo::addReal \"%s\"\n",
			m_attrName.c_str());
#endif

	addAttribute("numeric","nonarithmetic_integer","fpreal32",1);
}

void SurfaceAccessorGeo::addSpatialVector(void)
{
#if FE_SAG_DEBUG
	feLog("SurfaceAccessorGeo::addSpatialVector \"%s\"\n",
			m_attrName.c_str());
#endif

	//* TODO typevalue: hpoint, normal, color, ...
	addAttribute("numeric","hpoint","fpreal32",4);
}

BWORD SurfaceAccessorGeo::bindInternal(SurfaceAccessibleI::Element a_element,
		const String& a_name)
{
#if FE_SAG_DEBUG
	feLog("SurfaceAccessorGeo::bindInternal element %d attribute \"%s\"\n",
			a_element,a_name.c_str());
#endif

	m_element=SurfaceAccessibleI::e_point;
	if(a_element<0 || a_element>=SurfaceAccessibleI::e_elementEnums)
	{
		feLog("SurfaceAccessorGeo::bindInternal invalid element %d\n",
				a_element);
		return FALSE;
	}

	m_element=a_element;
	m_attrName=a_name;
	m_pElementValues=NULL;

	Json::Value& rValue=m_spJsonRoot->value();
	if(rValue.isNull())
	{
		feLog("SurfaceAccessorGeo::bindInternal"
				" element %d attribute \"%s\" NULL root\n",
				a_element,a_name.c_str());
		return FALSE;
	}

	String checkname=a_name;

	if(m_attribute==SurfaceAccessibleI::e_vertices)
	{
		checkname="P";
		a_element=SurfaceAccessibleI::e_point;

		Json::Value* pTopology=SurfaceAccessorGeo::findKey(
				&m_spJsonRoot->value(),"topology");
		m_pVertexMap=&(*pTopology)[1][1];

		Json::Value* pPrimitives=SurfaceAccessorGeo::findKey(
				&m_spJsonRoot->value(),"primitives");
		m_pVertexValues=&(*pPrimitives)[0][1];

		Json::Value* pSpans=SurfaceAccessorGeo::findKey(
				m_pVertexValues,"nvertices_rle");

		if(pSpans)
		{
			//* TODO could use the RLE directly (conditionally)
			m_pVertexValues=new Json::Value();

			const I32 spanCount=(*pSpans).size();

			I32 primitiveCount(0);
			for(I32 spanIndex=0;spanIndex<spanCount;spanIndex+=2)
			{
				primitiveCount+=(*pSpans)[spanIndex+1].asInt();
			}

			(*m_pVertexValues).resize(primitiveCount);

			I32 primitiveIndex(0);
			I32 vertexIndex(0);
			for(I32 spanIndex=0;spanIndex<spanCount;spanIndex+=2)
			{
				const I32 spanVertices=(*pSpans)[spanIndex].asInt();
				const I32 spanPrims=(*pSpans)[spanIndex+1].asInt();

				for(I32 primIndex=0;primIndex<spanPrims;primIndex++)
				{
					Json::Value* pPrimitive=&(*m_pVertexValues)[primitiveIndex];
					(*pPrimitive).resize(1);

					Json::Value* pPrimitive0= &(*pPrimitive)[0];
					(*pPrimitive0).resize(spanVertices);

					for(I32 subIndex=0;subIndex<spanVertices;subIndex++)
					{
						(*pPrimitive0)[subIndex]=vertexIndex++;
					}
					primitiveIndex++;
				}
			}
		}
	}

	Json::Value* pAttributes=accessAttributes(m_spJsonRoot,a_element,FALSE);
	if(!pAttributes)
	{
#if FE_SAG_DEBUG
		feLog("SurfaceAccessorGeo::bindInternal"
				" no attributes for element %d\n",a_element);
#endif
		return FALSE;
	}

	const U32 attrCount=(*pAttributes).size();
	for(U32 attrIndex=0;attrIndex<attrCount;attrIndex++)
	{
		Json::Value& rAttribute=(*pAttributes)[(int)attrIndex];
		const String attrName=rAttribute[0][5].asString().c_str();

		if(attrName!=checkname)
		{
			continue;
		}

		m_pElementValues=&rAttribute[1][7][5];
		m_pStringValues=&rAttribute[1][5];

		return TRUE;
	}

#if FE_SAG_DEBUG
	feLog("SurfaceAccessorGeo::bindInternal \"%s\" not found\n",
			a_name.c_str());
#endif

	return FALSE;
}

} /* namespace ext */
} /* namespace fe */
