/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __json_SurfaceAccessorGeo_h__
#define __json_SurfaceAccessorGeo_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
    @brief Common Functionality for Accessor Surface

	@ingroup json
*//***************************************************************************/
class FE_DL_EXPORT SurfaceAccessorGeo:
	public SurfaceAccessorBase,
	public fe::CastableAs<SurfaceAccessorGeo>
{
	public:
						SurfaceAccessorGeo(void):
							m_pElementValues(NULL),
							m_pVertexValues(NULL)
						{	setName("SurfaceAccessorGeo"); }
virtual					~SurfaceAccessorGeo(void)							{}

						using SurfaceAccessorBase::set;
						using SurfaceAccessorBase::append;
						using SurfaceAccessorBase::spatialVector;

						//* as SurfaceAccessorI
		BWORD			bind(SurfaceAccessibleI::Element a_element,
								SurfaceAccessibleI::Attribute a_attribute)
						{
							m_attribute=a_attribute;

							String name;
							switch(a_attribute)
							{
								case SurfaceAccessibleI::e_generic:
								case SurfaceAccessibleI::e_position:
									name="P";
									break;
								case SurfaceAccessibleI::e_normal:
									name="N";
									break;
								case SurfaceAccessibleI::e_uv:
									name="uv";
									break;
								case SurfaceAccessibleI::e_color:
									name="Cd";
									break;
								case SurfaceAccessibleI::e_vertices:
									m_attrName="vertices";
									FEASSERT(a_element==
											SurfaceAccessibleI::e_primitive);
									break;
								case SurfaceAccessibleI::e_properties:
									m_attrName="properties";
									FEASSERT(a_element==
											SurfaceAccessibleI::e_primitive);
									break;
							}
							return bindInternal(a_element,name);
						}
		BWORD			bind(SurfaceAccessibleI::Element a_element,
								const String& a_name)
						{
							m_attribute=SurfaceAccessibleI::e_generic;
							return bindInternal(a_element,a_name);
						}
virtual	U32				count(void) const
						{
							if(m_attribute!=SurfaceAccessibleI::e_generic &&
									m_attribute!=
									SurfaceAccessibleI::e_vertices &&
									m_attribute!=
									SurfaceAccessibleI::e_properties &&
									!isBound())
							{
								return 0;
							}

							return elementCount(m_element);
						}
virtual	U32				subCount(U32 a_index) const
						{
							if(m_attribute!=SurfaceAccessibleI::e_generic &&
									m_attribute!=
									SurfaceAccessibleI::e_vertices &&
									m_attribute!=
									SurfaceAccessibleI::e_properties &&
									!isBound())
							{
								return 0;
							}

							if(m_attribute==SurfaceAccessibleI::e_vertices)
							{
								FEASSERT(m_element==
										SurfaceAccessibleI::e_primitive);

								Json::Value* pVertexArray=
										findVertices(a_index);

								FEASSERT(pVertexArray);
								return pVertexArray->size();
							}

							return 1;
						}

virtual	void			set(U32 a_index,U32 a_subIndex,String a_string)
						{
							if(m_element!=SurfaceAccessibleI::e_pointGroup &&
									m_element!=
									SurfaceAccessibleI::e_primitiveGroup &&
									m_attribute!=
									SurfaceAccessibleI::e_vertices &&
									!isBound())
							{
								addString();
							}

							if(!isBound())
							{
								return;
							}

							Json::Value& rElementValue=
									(*m_pStringValues)[(int)a_index];

							rElementValue=a_string.c_str();
						}

virtual	String			string(U32 a_index,U32 a_subIndex=0)
						{
							if(!isBound())
							{
								return 0;
							}

							if(m_element==
									SurfaceAccessibleI::e_primitiveGroup ||
									m_attribute==SurfaceAccessibleI::e_vertices)
							{
								FEASSERT(m_element==
										SurfaceAccessibleI::e_primitiveGroup
										|| m_element==
										SurfaceAccessibleI::e_primitive);

								return pointByVertex(a_index,a_subIndex);
							}

							const Json::Value& rElementValue=
									(*m_pStringValues)[(int)a_index];

							return rElementValue.asString().c_str();
						}

virtual	void			set(U32 a_index,U32 a_subIndex,I32 a_integer)
						{
							if(m_element!=SurfaceAccessibleI::e_pointGroup &&
									m_element!=
									SurfaceAccessibleI::e_primitiveGroup &&
									m_attribute!=
									SurfaceAccessibleI::e_vertices &&
									!isBound())
							{
								addInteger();
							}

							if(!isBound())
							{
								return;
							}

							Json::Value& rElementValue=
									(*m_pElementValues)[(int)a_index];

							rElementValue[0]=a_integer;
						}

virtual	I32				integer(U32 a_index,U32 a_subIndex=0)
						{
							if(!isBound())
							{
								return 0;
							}

							if(m_element==
									SurfaceAccessibleI::e_primitiveGroup ||
									m_attribute==SurfaceAccessibleI::e_vertices)
							{
								FEASSERT(m_element==
										SurfaceAccessibleI::e_primitiveGroup
										|| m_element==
										SurfaceAccessibleI::e_primitive);

								return pointByVertex(a_index,a_subIndex);
							}

							const Json::Value& rElementValue=
									(*m_pElementValues)[(int)a_index];

							return rElementValue[0].asInt();
						}

virtual	I32				append(SurfaceAccessibleI::Form a_form)
						{
							if(m_element==SurfaceAccessibleI::e_point)
							{
								const I32 index=
										m_spJsonRoot->value()[5].asInt();

								m_spJsonRoot->value()[5]=index+1;

								return index;
							}

							if(m_element==SurfaceAccessibleI::e_primitive)
							{
								const I32 index=
										m_spJsonRoot->value()[9].asInt();

								m_spJsonRoot->value()[9]=index+1;

								return index;
							}

							//* TODO otherwise

							return -1;
						}

virtual	void			append(U32 a_index,I32 a_integer)
						{
							if(m_element==
									SurfaceAccessibleI::e_primitiveGroup ||
									m_attribute==SurfaceAccessibleI::e_vertices)
							{
								FEASSERT(m_element==
										SurfaceAccessibleI::e_primitiveGroup
										|| m_element==
										SurfaceAccessibleI::e_primitive);

								Json::Value* pVertexMap=
										&m_spJsonRoot->value()[13][1][1];
								FEASSERT(pVertexMap);

								Json::Value* pVertexArray=
										findVertices(a_index);
								FEASSERT(pVertexArray);

								const int newPointIndex=
										pVertexArray->size();
								const int newVertexIndex=
										pVertexMap->size();

								(*pVertexArray)[newPointIndex]=
										newVertexIndex;
								(*pVertexMap)[newVertexIndex]=a_integer;
							}

							//* TODO otherwise
						}

virtual	void			set(U32 a_index,U32 a_subIndex,Real a_real)
						{
							if(m_element!=SurfaceAccessibleI::e_pointGroup &&
									m_element!=
									SurfaceAccessibleI::e_primitiveGroup &&
									m_attribute!=
									SurfaceAccessibleI::e_vertices &&
									!isBound())
							{
								addReal();
							}

							if(!isBound())
							{
								return;
							}

							Json::Value& rElementValue=
									(*m_pElementValues)[(int)a_index];

							rElementValue[0]=a_real;
						}

virtual	Real			real(U32 a_index,U32 a_subIndex=0)
						{
							if(!isBound())
							{
								return 0;
							}

							if(m_element==
									SurfaceAccessibleI::e_primitiveGroup ||
									m_attribute==SurfaceAccessibleI::e_vertices)
							{
								FEASSERT(m_element==
										SurfaceAccessibleI::e_primitiveGroup
										|| m_element==
										SurfaceAccessibleI::e_primitive);

								return pointByVertex(a_index,a_subIndex);
							}

							const Json::Value& rElementValue=
									(*m_pElementValues)[(int)a_index];

							return rElementValue[0].asFloat();
						}

virtual	void			set(U32 a_index,U32 a_subIndex,
								const SpatialVector& a_vector)
						{
							if(m_element!=SurfaceAccessibleI::e_pointGroup &&
									m_element!=
									SurfaceAccessibleI::e_primitiveGroup &&
									m_attribute!=
									SurfaceAccessibleI::e_vertices &&
									!isBound())
							{
								addSpatialVector();
							}

							if(!isBound())
							{
								return;
							}

							Json::Value& rElementValue=
									(*m_pElementValues)[(int)a_index];

							rElementValue[0]=a_vector[0];
							rElementValue[1]=a_vector[1];
							rElementValue[2]=a_vector[2];
							rElementValue[3]=0.0;
						}
virtual	SpatialVector	spatialVector(U32 a_index,U32 a_subIndex=0)
						{
							if(!isBound())
							{
								return SpatialVector(0.0,0.0,0.0);
							}

							U32 pointIndex=a_index;

							if(m_element==
									SurfaceAccessibleI::e_primitiveGroup ||
									m_attribute==SurfaceAccessibleI::e_vertices)
							{
								FEASSERT(m_element==
										SurfaceAccessibleI::e_primitiveGroup
										|| m_element==
										SurfaceAccessibleI::e_primitive);

								pointIndex=pointByVertex(a_index,a_subIndex);
							}

							const Json::Value& rElementValue=
									(*m_pElementValues)[(int)pointIndex];

							return SpatialVector(
									rElementValue[0].asFloat(),
									rElementValue[1].asFloat(),
									rElementValue[2].asFloat());
						}

						//* JSON specific
		void			setJsonRoot(sp<JsonValue> a_spJsonRoot)
						{	m_spJsonRoot=a_spJsonRoot; }

		sp<JsonValue>	jsonRoot(void)		{ return m_spJsonRoot; }

static	Json::Value*	accessAttributes(sp<JsonValue> a_spJsonRoot,
								SurfaceAccessibleI::Element a_element,
								BWORD a_create);

	private:

virtual	BWORD			bindInternal(SurfaceAccessibleI::Element a_element,
								const String& a_name);

		void			addAttribute(String a_type,String a_typeValue,
							String a_storage,U32 a_components);
		void			addString(void);
		void			addInteger(void);
		void			addReal(void);
		void			addSpatialVector(void);

static	Json::Value*	findKey(Json::Value* a_pParent,String a_key);
static	Json::Value*	accessPointAttributes(sp<JsonValue> a_spJsonRoot,
								BWORD a_create);
static	Json::Value*	accessVertexAttributes(sp<JsonValue> a_spJsonRoot,
								BWORD a_create);
static	Json::Value*	accessPrimitiveAttributes(sp<JsonValue> a_spJsonRoot,
								BWORD a_create);
static	Json::Value*	accessDetailAttributes(sp<JsonValue> a_spJsonRoot,
								BWORD a_create);

		I32				elementCount(
								SurfaceAccessibleI::Element a_element) const;

		BWORD			isBound(void) const
						{	return (m_pElementValues!=NULL); }

		Json::Value*	findVertices(U32 a_index) const
						{	return &(*m_pVertexValues)[(int)a_index][0]; }

		U32				pointByVertex(U32 a_index,U32 a_subIndex)
						{
							Json::Value* pVertexArray=findVertices(a_index);

							FEASSERT(pVertexArray);
							const Json::Value& rVertexValue=
									(*pVertexArray)[(int)a_subIndex];

							const U32 vertexIndex=rVertexValue.asInt();

							const Json::Value& rPointValue=
									(*m_pVertexMap)[(int)vertexIndex];

							return rPointValue.asInt();
						}

		sp<JsonValue>	m_spJsonRoot;
		Json::Value*	m_pElementValues;
		Json::Value*	m_pStringValues;
		Json::Value*	m_pVertexMap;
		Json::Value*	m_pVertexValues;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __json_SurfaceAccessorGeo_h__ */
