/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <json/json.pmh>

#include <sstream>

namespace fe
{
namespace ext
{

namespace data
{

JsonWriter::JsonWriter(sp<Scope> spScope):
	AsciiWriter(spScope)
{
}

JsonWriter::~JsonWriter(void)
{
}

void JsonWriter::output(std::ostream &a_ostrm, sp<RecordGroup> spRG)
{
	m_spJsonRoot=sp<JsonValue>(new JsonValue());
	m_spJsonRoot->value()=Json::objectValue;

	m_jsonAttributes=Json::objectValue;
	m_jsonLayouts=Json::objectValue;
	m_jsonRecords=Json::objectValue;
	m_jsonRecordGroups=Json::objectValue;
	m_jsonRecordArrays=Json::objectValue;

	String s;
	scan(spRG);
	setupSBIDs(m_spScannedRecords);

	// info
	Json::Value jsonBlock(Json::objectValue);
	jsonBlock["info"]=FE_SERIAL_VERSION;
	m_spJsonRoot->value()["Header"]=jsonBlock;

	// attributes
	for(unsigned int i = m_sentAttrID; i < m_attrArray.size(); i++)
	{
		write(a_ostrm, m_attrArray[i]);
	}
	m_sentAttrID = m_attrArray.size();

	// layouts
	for(fe::data::t_layout_loinfo::iterator it = m_layouts.begin();
		it != m_layouts.end(); it++)
	{
		if(!it->second.m_sent)
		{
			write(a_ostrm, it->first, it->second.m_id);
			it->second.m_sent = true;
		}
	}

	// state blocks (records)
	int sb_id = 1;
	for(RecordGroup::iterator it = m_spScannedRecords->begin();
		it != m_spScannedRecords->end(); it++)
	{
		sp<RecordArray> spRA = *it;
		for(int i = 0; i < spRA->length(); i++)
		{
			write(a_ostrm, spRA->getRecord(i), sb_id++);
		}
	}
	m_spScannedRecords->clear();

	// record groups
	for(fe::data::t_rg_id::iterator it = m_rgs.begin(); it != m_rgs.end(); it++)
	{
		write(a_ostrm, it->first, it->second);
	}

	// record arrays as groups
	for(fe::data::t_ra_id::iterator it = m_ras.begin(); it != m_ras.end(); it++)
	{
		write(a_ostrm, it->first, it->second);
	}

	m_nextRGID = 1;
	m_rgs.clear();
	m_ras.clear();
	m_sbs.clear();

	m_spJsonRoot->value()["Attribute"]=m_jsonAttributes;
	m_spJsonRoot->value()["Layout"]=m_jsonLayouts;
	m_spJsonRoot->value()["Record"]=m_jsonRecords;
	m_spJsonRoot->value()["RecordGroup"]=m_jsonRecordGroups;
	m_spJsonRoot->value()["RecordArray"]=m_jsonRecordArrays;

	Json::StyledStreamWriter writer;
	writer.write(a_ostrm,m_spJsonRoot->value());

	m_jsonAttributes=Json::nullValue;
	m_jsonLayouts=Json::nullValue;
	m_jsonRecords=Json::nullValue;
	m_jsonRecordGroups=Json::nullValue;
	m_jsonRecordArrays=Json::nullValue;

	m_spJsonRoot=NULL;
}

void JsonWriter::write(std::ostream &a_ostrm, Record r_out, int a_sb_id)
{
	Json::Value jsonBlock(Json::objectValue);

	Json::Value jsonAttributes(Json::objectValue);

	const U32 defaultRGID=m_firstRGID[a_sb_id];
	FEASSERT(defaultRGID>0);

	String groupName;
	if(defaultRGID>1)
	{
		groupName.sPrintf("RG%d",defaultRGID);
	}
	else
	{
		groupName.sPrintf("%d",defaultRGID);
	}

	sp<Layout> spLayout=r_out.layout();
	const String layoutKey=spLayout->name().maybeQuote();
	Json::Value& jsonRecordLayout=m_jsonRecords[layoutKey.c_str()];
	if(jsonRecordLayout==Json::nullValue)
	{
		jsonRecordLayout=Json::objectValue;
	}

//	feLog("JsonWriter::write Record layout \"%s\" group \"%s\"\n",
//			layoutKey.c_str(),groupName.c_str());

	sp<Scope> spScope = spLayout->scope();
	FE_UWORD cnt = spScope->getAttributeCount();
	for(FE_UWORD i = 0; i < cnt; i++)
	{
		if(spLayout->checkAttribute(i))
		{
			if(spScope->attribute(i)->isSerialize())
			{
				sp<BaseType> spBT = spScope->attribute(i)->type();
				if(spBT == m_spVoidType)
				{
					continue;
				}

				const String key=spScope->attribute(i)->name();

				if(spBT == m_spRecordGroupType)
				{
					sp<RecordGroup> *pspRG;
					void *instance = r_out.rawAttribute(i);
					pspRG = reinterpret_cast<sp<RecordGroup> *>(instance);

					// OUTPUT: r_out group id
					if(pspRG->isValid())
					{
						U32 id=(U32)getID(*pspRG);
						if(id==1)
						{
							jsonAttributes[key.c_str()]=1;
						}
						else
						{
							String idString;
							idString.sPrintf("RG%d",id);
							jsonAttributes[key.c_str()]=idString.c_str();
						}
					}
					else
					{
						jsonAttributes[key.c_str()]=0;
					}
				}
				else if(spBT == m_spRecordArrayType)
				{
					sp<RecordArray> *pspRA;
					void *instance = r_out.rawAttribute(i);
					pspRA = reinterpret_cast<sp<RecordArray> *>(instance);

					// OUTPUT: record array (group) id
					if(pspRA->isValid())
					{
						U32 id=(U32)getID(*pspRA);
						if(id==1)
						{
							jsonAttributes[key.c_str()]=1;
						}
						else
						{
							String idString;
							idString.sPrintf("RG%d",id);
							jsonAttributes[key.c_str()]=idString.c_str();
						}
					}
					else
					{
						jsonAttributes[key.c_str()]=0;
					}
				}
				else if(spBT == m_spRecordType)
				{
					Record *pR;
					void *instance = r_out.rawAttribute(i);
					pR = reinterpret_cast<Record *>(instance);

					// OUTPUT: record id
					if(pR->isValid())
					{
						if(m_sbs.find(pR->idr()) == m_sbs.end())
						{
							jsonAttributes[key.c_str()]=0;
						}
						jsonAttributes[key.c_str()]=
								(pR->layout()->name() +
								m_sbs[pR->idr()]).maybeQuote().c_str();
					}
					else
					{
						jsonAttributes[key.c_str()]=0;
					}
				}
				else if(spBT == m_spWeakRecordType)
				{
					WeakRecord *pR;
					void *instance = r_out.rawAttribute(i);
					pR = reinterpret_cast<WeakRecord *>(instance);

					// OUTPUT: record id
					if(pR->isValid())
					{
						if(m_sbs.find(pR->idr()) == m_sbs.end())
						{
							jsonAttributes[key.c_str()]=0;
						}
						jsonAttributes[key.c_str()]=
								(pR->layout()->name() +
								m_sbs[pR->idr()]).maybeQuote().c_str();
					}
					else
					{
						jsonAttributes[key.c_str()]=0;
					}
				}
				else if(spBT->getInfo().isValid())
				{
					void *instance = r_out.rawAttribute(i);

					std::ostringstream ostrstrm;

					// OUTPUT: instance data via Info
					if(-1 == spBT->getInfo()->output(ostrstrm, instance,
						BaseType::Info::e_ascii))
					{
						std::ostringstream str_ostrm;
						int n = spBT->getInfo()->output(str_ostrm, instance,
							BaseType::Info::e_binary);

						std::string string=str_ostrm.str();

						writeBinaryBlock(ostrstrm,
							(const void *)(str_ostrm.str().data()),n);
						ostrstrm.flush();
					}

					//* remove redundant quotes
					String text=ostrstrm.str().c_str();
					const String chopText=text.prechop("\"").chop("\"");
					if(chopText.length()==text.length()-2)
					{
						text=chopText;
					}

					jsonAttributes[key.c_str()]=text.c_str();
				}
			}
		}
	}

	const String recordName=(spLayout->name() + a_sb_id).maybeQuote();

	jsonBlock["attributes"]=jsonAttributes;
	jsonRecordLayout[groupName.c_str()][recordName.c_str()]=jsonBlock;
}

void JsonWriter::write(std::ostream &a_ostrm, sp<Attribute> spAttribute)
{
	sp<BaseType> spBT = spAttribute->type();

	Json::Value jsonBlock(Json::objectValue);
	Json::Value jsonNames(Json::arrayValue);

	sp<Scope> spScope = m_attrs[spAttribute].m_scope;

	if(spScope != m_spScope)
	{
		jsonBlock["scope"]=spScope->name().c_str();
	}

	std::list<String> typenames;
	spScope->typeMaster()->reverseLookup(spBT, typenames);

	std::list<String>::iterator it;
	for(it = typenames.begin();it != typenames.end(); it++)
	{
		jsonNames.append(it->c_str());
	}

	jsonBlock["typenames"]=jsonNames;
	m_jsonAttributes[spAttribute->name().c_str()]=jsonBlock;
}

void JsonWriter::write(std::ostream &a_ostrm, sp<RecordGroup> spRG, int a_id)
{
	Json::Value jsonBlock(Json::objectValue);
	if(spRG->isWeak())
	{
		jsonBlock["weak"]=1;
	}

	Json::Value jsonIds(Json::arrayValue);

	for(RecordGroup::iterator it = spRG->begin(); it != spRG->end(); it++)
	{
		sp<RecordArray> spRA = *it;
		for(int i = 0; i < spRA->length(); i++)
		{
			const U32 record_id=spRA->idr(i);

			// OUTPUT: record id
			if(m_sbs.find(record_id) == m_sbs.end())
			{
				feX(e_cannotFind,
					"JsonWriter::write RG",
					"attempt to write unknown record");
			}

			const I32 sb_id=m_sbs[record_id];

			if(m_firstRGID[sb_id]==a_id)
			{
				//* already added using default group
				continue;
			}

			jsonIds.append(
					(spRA->layout()->name() + sb_id).maybeQuote().c_str());
		}
	}

	jsonBlock["ids"]=jsonIds;
	String blockName;
	blockName.sPrintf("%s%d",a_id==1? "": "RG",a_id);
	m_jsonRecordGroups[blockName.c_str()]=jsonBlock;

}

void JsonWriter::write(std::ostream &a_ostrm, sp<RecordArray> spRA, int a_id)
{
	Json::Value jsonBlock(Json::objectValue);
	Json::Value jsonIds(Json::arrayValue);

	for(int i = 0; i < spRA->length(); i++)
	{
		// OUTPUT: record id
		if(m_sbs.find(spRA->idr(i)) == m_sbs.end())
		{
			feX(e_cannotFind,
				"JsonWriter::write RA",
				"attempt to write unknown record");
		}
		jsonIds.append((spRA->layout()->name() +
				m_sbs[spRA->idr(i)]).maybeQuote().c_str());
	}

	jsonBlock["ids"]=jsonIds;
	String blockName;
	blockName.sPrintf("%s%d",a_id==1? "": "RG",a_id);
	m_jsonRecordArrays[blockName.c_str()]=jsonBlock;
}

void JsonWriter::write(std::ostream &a_ostrm, sp<Layout> spLayout, int a_id)
{
	spLayout->initialize();

	Json::Value jsonBlock(Json::objectValue);

	Json::Value jsonAttributes(Json::arrayValue);

	sp<Scope> spScope = m_spScope;
	if(spLayout->scope() != m_spScope)
	{
		spScope = spLayout->scope();
		jsonBlock["scope"]=spScope->name().c_str();
	}

	FE_UWORD cnt = spScope->getAttributeCount();
	for(FE_UWORD i = 0; i < cnt; i++)
	{
		if(spLayout->checkAttribute(i))
		{
			sp<BaseType> spBT = spScope->attribute(i)->type();
			if(spBT->getInfo().isValid())
			{
				if(spScope->attribute(i)->isSerialize())
				{
					jsonAttributes.append(
							spScope->attribute(i)->name().c_str());
				}
			}
		}
	}

	jsonBlock["attributes"]=jsonAttributes;
	m_jsonLayouts[spLayout->name().maybeQuote().c_str()]=jsonBlock;
}

} /* namespace ext */
} /* namespace fe */

} /* namespace */

