/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <json/json.pmh>

#define FE_SACG_DEBUG	FALSE

namespace fe
{
namespace ext
{

void SurfaceAccessibleGeo::reset(void)
{
	SurfaceAccessibleBase::reset();

	if(m_spJsonRoot.isNull())
	{
		m_spJsonRoot=sp<JsonValue>(new JsonValue());
	}
	if(m_spJsonRoot.isValid())
	{
		m_spJsonRoot->value().clear();
		m_spJsonRoot->value()[0]="fileversion";
		m_spJsonRoot->value()[1]="";
		m_spJsonRoot->value()[2]="hasindex";
		m_spJsonRoot->value()[3]=false;
		m_spJsonRoot->value()[4]="pointcount";
		m_spJsonRoot->value()[5]=0;
		m_spJsonRoot->value()[6]="vertexcount";
		m_spJsonRoot->value()[7]=0;
		m_spJsonRoot->value()[8]="primitivecount";
		m_spJsonRoot->value()[9]=0;
		m_spJsonRoot->value()[10]="info";
		m_spJsonRoot->value()[12]="topology";
		m_spJsonRoot->value()[14]="attributes";
		m_spJsonRoot->value()[15][0]="vertexattributes";
		m_spJsonRoot->value()[15][2]="pointattributes";
		m_spJsonRoot->value()[15][4]="primitiveattributes";
		m_spJsonRoot->value()[15][6]="globalattributes";
		m_spJsonRoot->value()[16]="primitives";
	}
}

sp<SurfaceAccessorI> SurfaceAccessibleGeo::accessor(
	String a_node,Element a_element,String a_name,Creation a_create,
	Writable a_writable)
{
	sp<SurfaceAccessorGeo> spAccessor(new SurfaceAccessorGeo);

	if(spAccessor.isValid())
	{
		spAccessor->setWritable(a_writable);
		spAccessor->setJsonRoot(m_spJsonRoot);

		if(spAccessor->bind(a_element,a_name) ||
				a_create==SurfaceAccessibleI::e_createMissing)
		{
			spAccessor->setSurfaceAccessible(sp<SurfaceAccessibleI>(this));
			return spAccessor;
		}
	}

	return sp<SurfaceAccessorI>(NULL);
}

sp<SurfaceAccessorI> SurfaceAccessibleGeo::accessor(
	String a_node,Element a_element,Attribute a_attribute,Creation a_create,
	Writable a_writable)
{
	sp<SurfaceAccessorGeo> spAccessor(new SurfaceAccessorGeo);

	if(spAccessor.isValid())
	{
		spAccessor->setWritable(a_writable);
		spAccessor->setJsonRoot(m_spJsonRoot);

		if(spAccessor->bind(a_element,a_attribute) ||
				a_create==SurfaceAccessibleI::e_createMissing)
		{
			spAccessor->setSurfaceAccessible(sp<SurfaceAccessibleI>(this));
			return spAccessor;
		}
	}

	return sp<SurfaceAccessorI>(NULL);
}

BWORD SurfaceAccessibleGeo::load(String a_filename,sp<Catalog> a_spSettings)
{
#if FE_SACG_DEBUG
	feLog("SurfaceAccessibleGeo::load \"%s\"\n",a_filename.c_str());
#endif

	if(m_spJsonRoot.isNull())
	{
		m_spJsonRoot=sp<JsonValue>(new JsonValue());
	}
	m_spJsonRoot->value().clear();

	std::ifstream in(a_filename.c_str());

	Json::Reader reader;
	const BWORD success=reader.parse(in,m_spJsonRoot->value());

	if(!success)
	{
		feLog("SurfaceAccessibleGeo::load parsing failed: %s\n",
				reader.getFormattedErrorMessages().c_str());
	}

	return success;
}

BWORD SurfaceAccessibleGeo::save(String a_filename,sp<Catalog> a_spSettings)
{
#if FE_SACG_DEBUG
	feLog("SurfaceAccessibleGeo::save \"%s\"\n",a_filename.c_str());
#endif

	if(m_spJsonRoot.isNull())
	{
		return FALSE;
	}

	std::ofstream out(a_filename.c_str());

	Json::StyledWriter writer;
	out << writer.write(m_spJsonRoot->value());

	return TRUE;
}

Protectable* SurfaceAccessibleGeo::clone(Protectable* pInstance)
{
#if FE_SACG_DEBUG
	feLog("SurfaceAccessibleGeo::clone\n");
#endif

	SurfaceAccessibleGeo* pSurfaceAccessibleGeo= pInstance?
			fe_cast<SurfaceAccessibleGeo>(pInstance):
			new SurfaceAccessibleGeo();

	pSurfaceAccessibleGeo->setLibrary(library());
	pSurfaceAccessibleGeo->setFactoryIndex(factoryIndex());

	sp<JsonValue> spNewRoot=sp<JsonValue>(new JsonValue());
	if(m_spJsonRoot.isValid())
	{
		spNewRoot->value()=m_spJsonRoot->value();
	}
	pSurfaceAccessibleGeo->setJsonRoot(spNewRoot);

	return pSurfaceAccessibleGeo;
}

void SurfaceAccessibleGeo::attributeSpecs(
	Array<SurfaceAccessibleI::Spec>& a_rSpecs,
	String a_node,
	SurfaceAccessibleI::Element a_element) const
{
#if FE_SACG_DEBUG
	feLog("SurfaceAccessibleGeo::attributeSpecs\n");
#endif

	a_rSpecs.clear();

	//* NOTE a_node ignored

	Json::Value* pAttributes=SurfaceAccessorGeo::accessAttributes(
			m_spJsonRoot,a_element,FALSE);
	if(!pAttributes)
	{
		return;
	}

	const U32 attrCount=(*pAttributes).size();
	for(U32 attrIndex=0;attrIndex<attrCount;attrIndex++)
	{
		Json::Value& rAttribute=(*pAttributes)[(int)attrIndex];
		const String attrName=rAttribute[0][5].asString().c_str();

		String typeName=rAttribute[0][3].asString().c_str();
		if(typeName=="numeric")
		{
			typeName=rAttribute[0][7]["type"]["value"].asString().c_str();
		}

		if(typeName=="hpoint")
		{
			typeName="vector3";
		}
		else if(typeName=="fpreal32")
		{
			typeName="real";
		}
		else if(typeName=="int32" || typeName=="nonarithmetic_integer")
		{
			typeName="integer";
		}

//		feLog("SurfaceAccessibleGeo::attributeSpecs %d/%d \"%s\" \"%s\"\n",
//				attrIndex,attrCount,typeName.c_str(),attrName.c_str());

		SurfaceAccessibleI::Spec spec;
		spec.set(attrName,typeName);

		a_rSpecs.push_back(spec);
	}
}

} /* namespace ext */
} /* namespace fe */
