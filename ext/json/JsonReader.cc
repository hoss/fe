/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <json/json.pmh>

#define FE_JR_HANDLE_DEBUG	FALSE

#include <sstream>

namespace fe
{
namespace ext
{
namespace data
{

JsonReader::JsonReader(sp<Scope> spScope):
	AsciiReader(spScope)
{
}

JsonReader::~JsonReader(void)
{
}

void JsonReader::handleInfo(const Json::Value& a_jsonInfo)
{
	U32 version = a_jsonInfo.asInt();

	if(version != FE_SERIAL_VERSION)
	{
		feX(e_invalidFile,
			"JsonReader::readInfo",
			"version mismatch");
	}
}

void JsonReader::handleGroup(String a_key,const Json::Value& a_jsonBlock)
{
#if FE_JR_HANDLE_DEBUG
	feLog("JsonReader::handleGroup \"%s\"\n",a_key.c_str());
#endif

	String &rgid = a_key;
	if(rgid=="")
	{
		m_spDefaultRG=NULL;
		return;
	}

	sp<RecordGroup> spRG = m_rgs[rgid];
	if(!spRG.isValid())
	{
		spRG = new RecordGroup();
		m_rgs[rgid] = spRG;
	}

	if(a_jsonBlock.isMember("weak"))
	{
		spRG->setWeak(TRUE);
	}

	const Json::Value& jsonIds=a_jsonBlock["ids"];
	const I32 count=jsonIds.size();
	for(I32 index=0;index<count;index++)
	{
		const String &rid = jsonIds[index].asCString();

#if FE_JR_HANDLE_DEBUG
		feLog("JsonReader::handleGroup record \"%s\"\n",rid.c_str());
#endif

		t_str_r::iterator i_r = m_rs.find(rid);
		if(i_r == m_rs.end())
		{
			fail("unspecified record '"+rid+"'");
		}
		spRG->add(i_r->second);
	}
}

void JsonReader::handleAttribute(String a_key,const Json::Value& a_jsonBlock)
{
#if FE_JR_HANDLE_DEBUG
	feLog("JsonReader::handleAttribute \"%s\"\n",a_key.c_str());
#endif

	AttributeInfo info;
	info.m_name = a_key;

	const Json::Value& jsonTypenames=a_jsonBlock["typenames"];
	const I32 count=jsonTypenames.size();
	for(I32 index=0;index<count;index++)
	{
		const String typeName=jsonTypenames[index].asCString();
		info.m_typenames.push_back(typeName);

#if FE_JR_HANDLE_DEBUG
		feLog("JsonReader::handleAttribute type \"%s\"\n",typeName.c_str());
#endif
	}

	sp<BaseType> spBT;
	String tstr;
	String matched_typename;
	for(std::list<String>::iterator it = info.m_typenames.begin();
		it != info.m_typenames.end(); it++)
	{
		// lookup type for attribute
		spBT = m_spScope->typeMaster()->lookupName(*it);
		tstr.cat(*it);
		tstr.cat(" ");
		if(spBT.isValid())
		{
			matched_typename = *it;
			break;
		}
	}
	sp<Scope> spScope = m_spScope;
	if(spBT.isValid())
	{
		// check for scope override
		if(spBT->getInfo().isValid())
		{
			for(std::list<String>::iterator it = info.m_typenames.begin();
				it != info.m_typenames.end(); it++)
			{
				std::string s = (*it).c_str();
				if(s.size() > 1 && s.substr(0,1) == "@")
				{
					s = s.substr(1);
					spScope = findScope(s.c_str());
				}
			}
		}

		sp<Attribute> spAttribute;
		// look for attribute
		spAttribute = spScope->findAttribute(info.m_name);
		if(spAttribute.isValid())
		{
			// type check existing attribute
			if(spAttribute->type() != spBT)
			{
				feX(e_typeMismatch,
					"JsonReader::handleAttribute",
					"type mismatch for \'%s\'",
					info.m_name.c_str());
			}
		}
		else
		{
			// support new attribute
			spAttribute = spScope->support(info.m_name, matched_typename);
		}

		if(spBT->getInfo().isValid())
		{
			for(std::list<String>::iterator it = info.m_typenames.begin();
				it != info.m_typenames.end(); it++)
			{
				std::string s = (*it).c_str();
				// lookup defaults
				if(s.size() > 1 && s.substr(0,1) == "=")
				{
					s = s.substr(1);
					std::istringstream istrm(s);
					spAttribute->defaultInstance().create(spBT);
					spBT->getInfo()->input(istrm,
						spAttribute->defaultInstance().data(),
						BaseType::Info::e_ascii);
				}
			}
		}

		// check for local help
		if(spAttribute->type()->getInfo().isValid())
		{
			info.m_spAttribute = spAttribute;
		}
		else
		{
			feX(e_typeMismatch,
				"JsonReader::handleAttribute",
				"type \'%s\' has no serialization helper",
				info.m_name.c_str());
		}
	}
	else
	{
		feX(e_cannotFind,
			"JsonReader::readAttribute",
			"cannot find type %s", tstr.c_str());
	}

	m_attrInfos[info.m_name] = info;
}

void JsonReader::handleLayout(String a_key,const Json::Value& a_jsonBlock)
{
#if FE_JR_HANDLE_DEBUG
	feLog("JsonReader::handleLayout \"%s\"\n",a_key.c_str());
#endif

	String layout_name = a_key;
	LayoutInfo &layout_info = m_layoutInfos[layout_name];

	sp<Scope> spScope = m_spScope;

	const Json::Value& jsonAttributes=a_jsonBlock["attributes"];
	const I32 count=jsonAttributes.size();
	for(I32 index=0;index<count;index++)
	{
		const String attrName=jsonAttributes[index].asCString();
#if FE_JR_HANDLE_DEBUG
		feLog("JsonReader::handleLayout attr \"%s\"\n",attrName.c_str());
#endif

		std::string s = attrName.c_str();
		if(s.size() > 1 && s.substr(0,1) == "@")
		{
			s = s.substr(1);
			spScope = findScope(s.c_str());
		}
		else
		{
			t_attrinfo::iterator i_info
				= m_attrInfos.find(attrName);
			if(i_info == m_attrInfos.end())
			{
				feX(e_invalidFile,
					"JsonReader::handleLayout",
					"unknown attribute %s", attrName.c_str());
			}
			layout_info.m_attributeInfos[attrName] = i_info->second;
		}
	}

	sp<Layout> spL = spScope->lookupLayout(layout_name);

	if(!spL.isValid())
	{
		spL = spScope->declare(layout_name);
	}

	// if layout is not locked, populate
	//if(!spL->locked())
	{
		for(t_attrinfo::iterator i_info = layout_info.m_attributeInfos.begin();
			i_info != layout_info.m_attributeInfos.end(); i_info++)
		{
			// populate
			spL->populate(i_info->second.m_spAttribute);
		}
	}

	// set layout info for actual layout
	spL->initialize();
	//if(!spL->locked()){ spL->lock(); }

	for(t_attrinfo::iterator i_info = layout_info.m_attributeInfos.begin();
			i_info != layout_info.m_attributeInfos.end(); i_info++)
	{
		AttributeInfo &tinfo = i_info->second;
		sp<Attribute> spAttribute = tinfo.m_spAttribute;
		// look for attribute
		spAttribute = spScope->findAttribute(tinfo.m_name, tinfo.m_index);
		if(spAttribute.isValid())
		{
			// check if layout does not have has attribute
			if(!spL->checkAttribute(tinfo.m_index))
			{
				tinfo.m_spAttribute = NULL;
			}
		}
		else
		{
			feX(e_typeMismatch,
				"JsonReader::handleLayout",
				"ascii format does not handle reading unknown attributes");
		}
	}
	layout_info.m_spLayout = spL;
}

void JsonReader::handleTemplate(String a_key,
	const Json::Value& a_jsonBlock)
{
	String &name = a_key;	//* layout name

#if FE_JR_HANDLE_DEBUG
	feLog("JsonReader::handleTemplate \"%s\"\n",name.c_str());
#endif

	const Json::Value& jsonParents=a_jsonBlock["inherits"];
	const I32 parentCount=jsonParents.size();
	for(I32 parentIndex=0;parentIndex<parentCount;parentIndex++)
	{
		const String parent=jsonParents[parentIndex].asCString();
		m_spScope->cookbook()->derive(name,parent);

#if FE_JR_HANDLE_DEBUG
		feLog("  inherit \"%s\"\n",parent.c_str());
#endif
	}

	const Json::Value& jsonAttributes=a_jsonBlock["attributes"];
	for(Json::Value::const_iterator it=jsonAttributes.begin();
			it!=jsonAttributes.end();it++)
	{
		const String attrName=it.name().c_str();
		const String value=(*it).asCString();

		m_spScope->cookbook()->add(name,attrName,value);

#if FE_JR_HANDLE_DEBUG
		feLog("  attribute \"%s\" \"%s\"\n",attrName.c_str(),value.c_str());
#endif
	}
}

void JsonReader::handleRecord(String a_layout,String a_key,
	const Json::Value& a_jsonBlock)
{
	String &record_id = a_key;
	String &layout_name = a_layout;

	// RecordViews apparently don't need Layouts
	sp<Scope> spScope = m_spScope;
	t_id_layoutinfo::iterator i_linfo = m_layoutInfos.find(layout_name);
	if(i_linfo != m_layoutInfos.end())
	{
		spScope = m_layoutInfos[layout_name].m_spLayout->scope();
	}

#if FE_JR_HANDLE_DEBUG
	feLog("JsonReader::handleRecord record \"%s\" layout \"%s\"\n",
			record_id.c_str(),layout_name.c_str());
#endif

	Record record=spScope->produceRecord(layout_name);
	FEASSERT(record.isValid());

	if(m_spDefaultRG.isValid())
	{
		m_spDefaultRG->add(record);

#if FE_JR_HANDLE_DEBUG
	feLog("JsonReader::handleRecord record \"%s\" added to default group\n",
			record_id.c_str());
#endif
	}

	m_rs[record_id] = record;

	const Json::Value& jsonAttributes=a_jsonBlock["attributes"];
	for(Json::Value::const_iterator it=jsonAttributes.begin();
			it!=jsonAttributes.end();it++)
	{
		const String attrName=it.name().c_str();
		const String token=(*it).asCString();

#if FE_JR_HANDLE_DEBUG
		feLog("JsonReader::handleRecord attr \"%s\"\n",attrName.c_str());
#endif

		FE_UWORD index=0;
		sp<Attribute>& rspAttribute=spScope->findAttribute(attrName,index);
		if(!rspAttribute.isValid())
		{
			feX("JsonReader::handleRecord",
					"attribute \"%s\" not found in scope for \"%s\" in record \"%s\"\n",
					attrName.c_str(),layout_name.c_str(),record_id.c_str());
		}


		void *instance = record.rawAttribute(index);
		sp<BaseType> spBT = rspAttribute->type();

		if(!record.layout()->checkAttribute(index))
		{
			feX("JsonReader::handleRecord",
					"offsetTable[%d] for \"%s\" in \"%s\" invalid\n",
					index,attrName.c_str(),layout_name.c_str());
		}

		if(spBT == m_spRecordGroupType)
		{
			const String &rgid = token;
			if(rgid != "0")
			{
				sp<RecordGroup> *pspRG;
				pspRG = reinterpret_cast<sp<RecordGroup> *>(instance);
				sp<RecordGroup> spRG = m_rgs[rgid];
				if(!spRG.isValid())
				{
					spRG = new RecordGroup();
#if FE_COUNTED_TRACK
					spRG->setName(attrName);
#endif
					m_rgs[rgid] = spRG;
				}
				*pspRG = spRG;
			}
		}
		else if(spBT == m_spRecordArrayType)
		{
			const String &raid = token;

			if(raid != "0")
			{
				sp<RecordArray> *pspRA;
				pspRA = reinterpret_cast<sp<RecordArray> *>(instance);

				sp<RecordGroup> spRG = m_rgs[raid];
				sp<RecordArray> spRA = m_ras[raid];

				if(!spRG.isValid())
				{
					spRG = new RecordGroup();
#if FE_COUNTED_TRACK
					spRG->setName(attrName);
#endif
					m_rgs[raid] = spRG;
				}

				if(!spRA.isValid())
				{
					spRA = new RecordArray();
					m_ras[raid] = spRA;
				}

				*pspRA = spRA;
			}
		}
		else if(spBT == m_spRecordType)
		{
			const String &rid = token;

			if(rid != "0")
			{
				RecordWiringInfo wiring;
				wiring.m_record = record;
				wiring.m_aRecord.setup(m_spScope, attrName);
				wiring.m_id = rid;
				m_wiringList.push_back(wiring);
			}
		}
		else if(spBT == m_spWeakRecordType)
		{
			const String &rid = token;

			if(rid != "0")
			{
				WeakRecordWiringInfo wiring;
				wiring.m_record = record;
				wiring.m_aRecord.setup(m_spScope, attrName);
				wiring.m_id = rid;
				m_wkWiringList.push_back(wiring);
			}
		}
		else if(spBT->getInfo().isValid())
		{
			//* TODO test binary blocks

			if(!token.empty() && token.c_str()[0]==FE_KW_STARTBINARY)
			{
				FESTRING_U8 buffer[1];
				buffer[0]=FE_KW_STARTBINARY;
				const String delimiter(buffer);
				const String trimmed=token.chop(delimiter).prechop(delimiter);

				const void *value = trimmed.c_str();
				std::string s((const char *)value,trimmed.length());
				std::istringstream istrm(s);
				spBT->getInfo()->input(istrm, instance,
					BaseType::Info::e_binary);
			}
			else
			{
				const String &value = token;
				std::string s(value.c_str());
				std::istringstream istrm(s);
				spBT->getInfo()->input(istrm, instance,
					BaseType::Info::e_ascii);
			}
		}
		else
		{
			fail("impossibility");
		}
	}
#if FE_JR_HANDLE_DEBUG
		feLog("JsonReader::handleRecord finalize\n");
#endif
	spScope->finalize(record);
#if FE_JR_HANDLE_DEBUG
		feLog("JsonReader::handleRecord done\n");
#endif
}

sp<RecordGroup> JsonReader::input(std::istream &a_istrm)
{
#if FE_JR_HANDLE_DEBUG
		feLog("JsonReader::input\n");
#endif

	m_spJsonRoot=sp<JsonValue>(new JsonValue());

	Json::Reader reader;
	reader.parse(a_istrm,m_spJsonRoot->value());

#if FE_JR_HANDLE_DEBUG
		feLog("JsonReader::input Header\n");
#endif
	const Json::Value jsonInfo=m_spJsonRoot->value()["Header"]["info"];
	if(jsonInfo!=Json::nullValue)
	{
		handleInfo(jsonInfo);
	}

#if FE_JR_HANDLE_DEBUG
		feLog("JsonReader::input Attributes\n");
#endif
	const Json::Value jsonAttributes=m_spJsonRoot->value()["Attribute"];
	for(Json::Value::const_iterator it=jsonAttributes.begin();
			it!=jsonAttributes.end();it++)
	{
		handleAttribute(it.name().c_str(),*it);
	}

#if FE_JR_HANDLE_DEBUG
		feLog("JsonReader::input Layouts\n");
#endif
	const Json::Value jsonLayouts=m_spJsonRoot->value()["Layout"];
	for(Json::Value::const_iterator it=jsonLayouts.begin();
			it!=jsonLayouts.end();it++)
	{
		handleLayout(it.name().c_str(),*it);
	}

#if FE_JR_HANDLE_DEBUG
		feLog("JsonReader::input Templates\n");
#endif
	const Json::Value jsonLayoutTemplates=m_spJsonRoot->value()["Template"];
	for(Json::Value::const_iterator it=jsonLayoutTemplates.begin();
			it!=jsonLayoutTemplates.end();it++)
	{
		handleTemplate(it.name().c_str(),*it);
	}

#if FE_JR_HANDLE_DEBUG
		feLog("JsonReader::input Records\n");
#endif
	const Json::Value jsonLayoutRecords=m_spJsonRoot->value()["Record"];
	for(Json::Value::const_iterator it=jsonLayoutRecords.begin();
			it!=jsonLayoutRecords.end();it++)
	{
		const Json::Value jsonRecords= *it;

		const String layoutName=it.name().c_str();

		for(Json::Value::const_iterator it2=jsonRecords.begin();
				it2!=jsonRecords.end();it2++)
		{
			const Json::Value& jsonBlock= *it2;
			if(jsonBlock.isMember("attributes"))
			{
				const String key=it2.name().c_str();

				m_spDefaultRG=NULL;
				handleRecord(layoutName,key,jsonBlock);
			}
			else
			{
				//* presume wrapped in a RecordGroup name to put records in

				const String defaultGroup=it2.name().c_str();
				m_spDefaultRG = m_rgs[defaultGroup];
				if(!m_spDefaultRG.isValid())
				{
					m_spDefaultRG = new RecordGroup();
					m_rgs[defaultGroup] = m_spDefaultRG;
				}

				const Json::Value subRecords= *it2;
				for(Json::Value::const_iterator it3=subRecords.begin();
						it3!=subRecords.end();it3++)
				{
					const String key=it3.name().c_str();
					handleRecord(layoutName,key,*it3);
				}
			}
		}
	}

#if FE_JR_HANDLE_DEBUG
		feLog("JsonReader::input RecordGroups\n");
#endif
	const Json::Value jsonRecordGroups=m_spJsonRoot->value()["RecordGroup"];
	for(Json::Value::const_iterator it=jsonRecordGroups.begin();
			it!=jsonRecordGroups.end();it++)
	{
		handleGroup(it.name().c_str(),*it);
	}

	wireRecords();
	recordGroupsToArrays();

	sp<RecordGroup> spRG;
	if(m_rgs.size() >= 1)
	{
		spRG = m_rgs["1"];
	}
	reset();
	return spRG;
}

void JsonReader::fail(const String &a_message)
{
	feX("JsonReader::fail", a_message.c_str());
}

} /* namespace ext */
} /* namespace fe */

} /* namespace */

