/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __json_SurfaceAccessibleGeo_h__
#define __json_SurfaceAccessibleGeo_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
    @brief JSON Geo Surface Binding

	@ingroup json
*//***************************************************************************/
class FE_DL_EXPORT SurfaceAccessibleGeo:
	public SurfaceAccessibleBase,
	public CastableAs<SurfaceAccessibleGeo>
{
	public:
								SurfaceAccessibleGeo(void)					{}
virtual							~SurfaceAccessibleGeo(void)					{}

								//* As Protectable
virtual	Protectable*			clone(Protectable* pInstance=NULL);

								//* as SurfaceAccessibleI

								using SurfaceAccessibleBase::bind;

virtual	void					bind(Instance a_instance)
								{
									m_spJsonRoot=
											a_instance.cast< sp<Component> >();
								}
virtual	BWORD					isBound(void)
								{	return (m_spJsonRoot.isValid()); }

								using SurfaceAccessibleBase::accessor;

virtual sp<SurfaceAccessorI>	accessor(String a_node,Element a_element,
										String a_name,Creation a_create,
										Writable a_writable);

virtual sp<SurfaceAccessorI>	accessor(String a_node,Element a_element,
										Attribute a_attribute,
										Creation a_create,
										Writable a_writable);

								using SurfaceAccessibleBase::load;

virtual	BWORD					load(String a_filename,
										sp<Catalog> a_spSettings);

								using SurfaceAccessibleBase::save;

virtual	BWORD					save(String a_filename,
										sp<Catalog> a_spSettings);

								using SurfaceAccessibleBase::attributeSpecs;

virtual	void					attributeSpecs(
										Array<SurfaceAccessibleI::Spec>&
										a_rSpecs,
										String a_node,
										SurfaceAccessibleI::Element
										a_element) const;

								//* JSON specific
		void					setJsonRoot(sp<JsonValue> a_spJsonRoot)
								{	m_spJsonRoot=a_spJsonRoot; }

		sp<JsonValue>			jsonRoot(void)		{ return m_spJsonRoot; }

	private:

virtual	void					reset(void);

		sp<JsonValue>			m_spJsonRoot;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __json_SurfaceAccessibleGeo_h__ */
