/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "zeromq/zeromq.pmh"

#define FE_ZRC_DEBUG			FALSE
#define FE_ZRC_VERBOSE			FALSE
#define FE_ZRC_IDENTITY_BYTES	5		//* ZeroMQ specific (always 5)
#define FE_ZRC_LINGER_MS		1000	//* wait for messages before disconnect

namespace fe
{
namespace ext
{

ZeroCatalog::ZeroCatalog(void):
	m_pReceiverThread(NULL)
{
#if FE_ZRC_DEBUG
	feLog("ZeroCatalog::ZeroCatalog\n");
#endif

	if(FE_CNC_IDENTITY_BYTES<FE_ZRC_IDENTITY_BYTES)
	{
		feLog("ZeroCatalog::ZeroCatalog"
				" error FE_CNC_IDENTITY_BYTES<FE_ZRC_IDENTITY_BYTES,"
				" buffer overun almost certain\n");
	}
}

ZeroCatalog::~ZeroCatalog(void)
{
#if FE_ZRC_DEBUG
	feLog("ZeroCatalog::~ZeroCatalog\n");
#endif

	disconnect();
}

void ZeroCatalog::initialize(void)
{
	m_receiverTask.m_pZeroCatalog=this;
}

Result ZeroCatalog::connectAsServer(String a_address,U16 a_port)
{
	const String transport=catalogOrDefault<String>("net:transport","tcp");

#if FE_ZRC_DEBUG
#endif
	feLog("ZeroCatalog::connectAsServer"
			" transport '%s' address \"%s\" port %d\n",
			transport.c_str(),a_address.c_str(),a_port);

	Result result=m_receiverTask.connectAsServer(transport,a_address,a_port);
	if(failure(result))
	{
		return result;
	}

	m_pReceiverThread=new Thread(&m_receiverTask);

	return ConnectedCatalog::connectAsServer(a_address,a_port);
}

Result ZeroCatalog::connectAsClient(String a_address,U16 a_port)
{
	const String transport=catalogOrDefault<String>("net:transport","tcp");

#if FE_ZRC_DEBUG
#endif
	feLog("ZeroCatalog::connectAsClient"
			" transport '%s' address \"%s\" port %d\n",
			transport.c_str(),a_address.c_str(),a_port);

	Result result=m_receiverTask.connectAsClient(transport,a_address,a_port);
	if(failure(result))
	{
		return result;
	}

	m_pReceiverThread=new Thread(&m_receiverTask);

	return ConnectedCatalog::connectAsClient(a_address,a_port);
}

static void freeFunction(void* a_pData,void* pHint)
{
	free(a_pData);
}

void ZeroCatalog::sendString(String a_text)
{
#if FE_ZRC_VERBOSE
	feLog("ZeroCatalog::sendString \"%s\"\n",a_text.c_str());
#endif

	sendBytes((U8*)a_text.c_str(),a_text.length()+1);
}

void ZeroCatalog::sendBytes(const U8* a_pByteBlock,I32 a_byteCount)
{
#if FE_ZRC_VERBOSE
	feLog("ZeroCatalog::sendBytes %d %p\n",a_byteCount,a_pByteBlock);
#endif

	void *data=malloc(a_byteCount);
	FEASSERT(data);
	memcpy(data,a_pByteBlock,a_byteCount);

	const I32 nextMessage=m_messages.m_messageArray.size();
	m_messages.m_messageArray.resize(nextMessage+1);

	const I32 subCountIndex=m_messages.m_subCountArray.size()-1;
	FEASSERT(subCountIndex>=0);
	m_messages.m_subCountArray[subCountIndex]++;

	zmq_msg_t& msg=m_messages.m_messageArray[nextMessage];
	const I32 rc=zmq_msg_init_data(&msg,data,a_byteCount,freeFunction,NULL);
	if(rc<0)
	{
		feLog("ZeroCatalog::sendBytes failed to init %d bytes of data\n",
				a_byteCount);
		return;
	}
}

void ZeroCatalog::broadcastSelect(String a_name,String a_property,
	String a_message,
	I32 a_includeCount,const String* a_pIncludes,
	I32 a_excludeCount,const String* a_pExcludes,
	const U8* a_pRawBytes,I32 a_byteCount)
{
	const BWORD removing=(a_message.empty() &&
			!cataloged(a_name,a_property));
	const BWORD isSignal=catalogOrDefault<bool>(a_name,"signal",false);
	const BWORD isTick=(a_name=="net:tick");

#if FE_ZRC_VERBOSE
	feLog("ZeroCatalog::broadcastSelect"
			" \"%s\" \"%s\" removing %d signal %d tick %d\n",
			a_name.c_str(),a_property.c_str(),removing,isSignal,isTick);
#endif

	m_messages.safeLock();

	const BWORD isServer=(role() == "server");
	const I32 recipientCount=isServer? m_identityCount: 1;
	for(I32 recipientIndex=0;recipientIndex<recipientCount;recipientIndex++)
	{
		Identity& rIdentity=m_identityArray[recipientIndex];

		if(isServer)
		{
//			feLog("  vs %d/%d \"%s\"\n",
//					recipientIndex,recipientCount,
//					rIdentity.m_text.c_str());

			if(a_includeCount && a_pIncludes)
			{
				I32 includeIndex=0;
				for(;includeIndex<a_includeCount;includeIndex++)
				{
//					feLog("    check inclusion %d/%d \"%s\"\n",
//							includeIndex,a_includeCount,
//							a_pIncludes[includeIndex].c_str());
					if(rIdentity==a_pIncludes[includeIndex])
					{
//						feLog("    inclusion found\n");
						break;
					}
				}
				if(includeIndex>=a_includeCount)
				{
//					feLog("    not included\n");
					continue;
				}
			}
			if(a_excludeCount && a_pExcludes)
			{
				I32 excludeIndex=0;
				for(;excludeIndex<a_excludeCount;excludeIndex++)
				{
//					feLog("    check exclusion %d/%d \"%s\"\n",
//							excludeIndex,a_excludeCount,
//							a_pExcludes[excludeIndex].c_str());
					if(rIdentity==a_pExcludes[excludeIndex])
					{
//						feLog("    exclusion found\n");
						break;
					}
				}
				if(excludeIndex<a_excludeCount)
				{
//					feLog("    excluded\n");
					continue;
				}
			}

		}

		const I32 nextSubCount=m_messages.m_subCountArray.size();
		m_messages.m_subCountArray.resize(nextSubCount+1);
		m_messages.m_subCountArray[nextSubCount]=0;

		if(isServer)
		{
			sendBytes(rIdentity.m_data,FE_ZRC_IDENTITY_BYTES);
		}

		if(isTick)
		{
			sendString("tick");
		}
		else if(removing)
		{
			sendString("remove");
			sendString(a_name);
			sendString(a_property);
		}
		else
		{
			sendString(isSignal? "signal": "update");
			sendString(a_name);
			sendString(a_property);

			if(!a_message.empty())
			{
				sendString("string");
				sendString(a_message);

//				feLog("  send update \"%s\" \"%s\" \"%s\"\n",
//						a_name.c_str(),a_property.c_str(),a_message.c_str());
			}
			else
			{
				const String typeString=catalogTypeName(a_name,a_property);
				sendString(typeString);

				if(typeString=="bytearray")
				{
					Array<U8>& rByteArray=
							catalog< Array<U8> >(a_name,a_property);
					sendBytes(rByteArray.data(),rByteArray.size());
				}
				else if(useBinaryForType(typeString))
				{
					if(a_pRawBytes)
					{
						sendBytes(a_pRawBytes,a_byteCount);
					}
					else
					{
						Array<U8> byteArray;
						catalogBytes(a_name,a_property,byteArray);
						sendBytes(byteArray.data(),byteArray.size());
					}
				}
				else
				{
					sendString(catalogValue(a_name,a_property));
				}
			}
		}

		const I32 nextIdentity=m_messages.m_recipientArray.size();
		m_messages.m_recipientArray.resize(nextIdentity+1);
		m_messages.m_recipientArray[nextIdentity]=rIdentity;

		m_messages.m_available=true;
	}

	m_messages.safeUnlock();

	if(isServer && !removing && a_name!="net:flushed")
	{
		//* TODO also properties other than "value"
		catalog<bool>(a_name,"broadcasted")=true;
	}

	heartbeat();
}

Result ZeroCatalog::disconnect(void)
{
#if FE_ZRC_DEBUG
	feLog("ZeroCatalog::disconnect\n");
#endif

	Result result=ConnectedCatalog::disconnect();
	if(fe::failure(result))
	{
		return result;
	}

	m_receiverTask.m_terminate=true;

	if(m_pReceiverThread)
	{
#if FE_ZRC_DEBUG
		feLog("ZeroCatalog::disconnect join\n");
#endif

		if(m_pReceiverThread->joinable())
		{
			m_pReceiverThread->join();
		}

#if FE_ZRC_DEBUG
		feLog("ZeroCatalog::disconnect joined\n");
#endif

		delete m_pReceiverThread;
		m_pReceiverThread=NULL;
	}

	m_receiverTask.disconnect();
	m_receiverTask.m_terminate=false;

	return result;
}

ZeroCatalog::ReceiverTask::ReceiverTask(void):
	m_pContext(NULL),
	m_pSocket(NULL),
	m_terminate(false)
{
#if FE_ZRC_DEBUG
	feLog("ZeroCatalog::ReceiverTask::ReceiverTask\n");
#endif
}

ZeroCatalog::ReceiverTask::~ReceiverTask(void)
{
#if FE_ZRC_DEBUG
	feLog("ZeroCatalog::ReceiverTask::~ReceiverTask\n");
#endif

	disconnect();
}

Result ZeroCatalog::ReceiverTask::connectAsServer(
	String a_transport,String a_address,U16 a_port)
{
	if(a_transport=="ipc")
	{
		m_endpoint.sPrintf("%s://%s",a_transport.c_str(),a_address.c_str());
	}
	else
	{
		m_endpoint.sPrintf("%s://*:%d",a_transport.c_str(),a_port);
	}

#if FE_ZRC_DEBUG
	feLog("ZeroCatalog::ReceiverTask::connectAsServer \"%s\"\n",
			m_endpoint.c_str());
#endif

	m_pContext=zmq_ctx_new();
	if(!m_pContext)
	{
		feLog("ZeroCatalog::ReceiverTask::connectAsServer"
				" failed to create ZeroMQ context\n");
		return e_refused;
	}

	feLog("ZeroCatalog::ReceiverTask::connectAsServer context %p\n",m_pContext);

	m_pSocket=zmq_socket(m_pContext, ZMQ_ROUTER);

#if FALSE
	//* change message limit (default 1000)
	const int highWaterMark(1);
	if(zmq_setsockopt(m_pSocket, ZMQ_SNDHWM,
			&highWaterMark,sizeof(highWaterMark))<0)
	{
		feLog("ZeroCatalog::ReceiverTask::connectAsServer"
				" set option failed (send high water mark)\n");
		return e_refused;
	}
	if(zmq_setsockopt(m_pSocket, ZMQ_RCVHWM,
			&highWaterMark,sizeof(highWaterMark))<0)
	{
		feLog("ZeroCatalog::ReceiverTask::connectAsServer"
				" set option failed (receive high water mark)\n");
		return e_refused;
	}
#endif

#if TRUE
	//* give errors for dropped data
	const int mandatory(1);
	if(zmq_setsockopt(m_pSocket, ZMQ_ROUTER_MANDATORY,
			&mandatory,sizeof(mandatory))<0)
	{
		feLog("ZeroCatalog::ReceiverTask::connectAsServer"
				" set option failed  (mandatory routing)\n");
		return e_refused;
	}
#endif

	//* don't block on disconnect
	const int lingerMS(FE_ZRC_LINGER_MS);
	if(zmq_setsockopt(m_pSocket, ZMQ_LINGER,&lingerMS,sizeof(lingerMS))<0)
	{
		feLog("ZeroCatalog::ReceiverTask::connectAsServer"
				" set option failed (linger time)\n");
		return e_refused;
	}

	const I32 rc=zmq_bind(m_pSocket,m_endpoint.c_str());
	if(rc<0)
	{
		feLog("ZeroCatalog::ReceiverTask::connectAsServer"
				" port %d failed binding\n",a_port);
		return e_refused;
	}

#if FE_ZRC_DEBUG
	feLog("ZeroCatalog::ReceiverTask::connectAsServer bound to %d\n",rc);
#endif

	return e_ok;
}

Result ZeroCatalog::ReceiverTask::connectAsClient(
	String a_transport,String a_address,U16 a_port)
{
	if(a_transport=="ipc")
	{
		m_endpoint.sPrintf("%s://%s",a_transport.c_str(),a_address.c_str());
	}
	else
	{
		m_endpoint.sPrintf("%s://%s:%d",
				a_transport.c_str(),a_address.c_str(),a_port);
	}

#if FE_ZRC_DEBUG
	feLog("ZeroCatalog::ReceiverTask::connectAsClient \"%s\"\n",
			m_endpoint.c_str());
#endif

	m_pContext=zmq_ctx_new();
	if(!m_pContext)
	{
		feLog("ZeroCatalog::ReceiverTask::connectAsClient"
				" failed to create ZeroMQ context\n");
		return e_refused;
	}

	m_pSocket=zmq_socket(m_pContext,ZMQ_DEALER);

#if FALSE
	//* change message limit (default 1000)
	const int highWaterMark(1);
	if(zmq_setsockopt(m_pSocket, ZMQ_SNDHWM,
			&highWaterMark,sizeof(highWaterMark))<0)
	{
		feLog("ZeroCatalog::ReceiverTask::connectAsClient"
				" set option failed (send high water mark)\n");
		return e_refused;
	}
	if(zmq_setsockopt(m_pSocket, ZMQ_RCVHWM,
			&highWaterMark,sizeof(highWaterMark))<0)
	{
		feLog("ZeroCatalog::ReceiverTask::connectAsClient"
				" set option failed (receive high water mark)\n");
		return e_refused;
	}
#endif

	//* don't block on disconnect
	const int lingerMS(FE_ZRC_LINGER_MS);
	if(zmq_setsockopt(m_pSocket, ZMQ_LINGER,&lingerMS,sizeof(lingerMS))<0)
	{
		feLog("ZeroCatalog::ReceiverTask::connectAsClient"
				" set option failed (linger time)\n");
		return e_refused;
	}

	const I32 rc=zmq_connect(m_pSocket,m_endpoint.c_str());
	if(rc<0)
	{
		feLog("ZeroCatalog::ReceiverTask::connectAsClient"
				" \"%s\" port %d failed connection\n",
				a_address.c_str(),a_port);
		return e_refused;
	}

#if FE_ZRC_DEBUG
	feLog("ZeroCatalog::ReceiverTask::connectAsClient bound to %d\n",rc);
#endif

	return e_ok;
}

Result ZeroCatalog::ReceiverTask::disconnect(void)
{
	if(m_pSocket)
	{
		const size_t bufferSize=128;
		char buffer[bufferSize];
		int rc=zmq_getsockopt(m_pSocket,ZMQ_LAST_ENDPOINT,
				buffer,(size_t *)&bufferSize);
		if(rc<0)
		{
			feLog("ZeroCatalog::ReceiverTask::~ReceiverTask"
					" failed to read back endpoint\n");
		}
		else
		{
			m_endpoint=buffer;
		}

		if(m_pZeroCatalog && m_pZeroCatalog->role() == "server")
		{
			rc=zmq_unbind(m_pSocket,m_endpoint.c_str());
			if(rc<0)
			{
				feLog("ZeroCatalog::ReceiverTask::~ReceiverTask"
						" could not unbind \"%s\"\n",
						m_endpoint.c_str());
			}
		}
		else
		{
			const I32 rc=zmq_disconnect(m_pSocket, m_endpoint.c_str());
			if(rc<0)
			{
				feLog("ZeroCatalog::ReceiverTask::~ReceiverTask"
						" could not disconnect \"%s\"\n",
						m_endpoint.c_str());
			}
		}

		rc=zmq_close(m_pSocket);
		if(rc<0)
		{
			feLog("ZeroCatalog::ReceiverTask::~ReceiverTask"
					" could not close \"%s\"\n",
					m_endpoint.c_str());
		}
		m_pSocket=NULL;
	}

	if(m_pContext)
	{
		int rc=zmq_ctx_term(m_pContext);
		if(rc<0)
		{
			feLog("ZeroCatalog::ReceiverTask::~ReceiverTask"
					" could not terminate context\n");
		}
		m_pContext=NULL;
	}

	return e_ok;
}

I32 ZeroCatalog::ReceiverTask::readBuffer(void** a_ppBuffer,zmq_msg_t* a_pMsg)
{
	I32 rc=zmq_msg_init(a_pMsg);
	if(rc<0)
	{
		feLog("ZeroCatalog::ReceiverTask::readBuffer"
				" could not init message \n");
		return rc;
	}

	I32 bytesRead=zmq_msg_recv(a_pMsg,m_pSocket,ZMQ_DONTWAIT);
#if FE_ZRC_VERBOSE
	feLog("ZeroCatalog::ReceiverTask::readBuffer bytesRead %d\n",
			bytesRead);
#endif

	if(bytesRead<0)
	{
		return bytesRead;
	}

	*a_ppBuffer=zmq_msg_data(a_pMsg);

#if FALSE
	U8* pByteBlock=(U8*)(*a_ppBuffer);
	for(I32 index=0;index<bytesRead && index<8;index++)
	{
		fe_printf(" %x",pByteBlock[index]);
	}
	fe_printf("\n");
#endif

	return bytesRead;
}

I32 ZeroCatalog::ReceiverTask::readString(String& a_rString)
{
	zmq_msg_t msg;
	void* pBuffer;
	const I32 bytesRead=readBuffer(&pBuffer,&msg);

	if(bytesRead>0)
	{
#if FE_ZRC_VERBOSE
		feLog("ZeroCatalog::ReceiverTask::readString \"%s\"\n",pBuffer);
#endif

		a_rString=(char*)pBuffer;
	}
	else
	{
		a_rString="";
	}

	zmq_msg_close(&msg);

	return bytesRead;
}

I32 ZeroCatalog::ReceiverTask::readBytes(Array<U8>& a_rByteArray)
{
	zmq_msg_t msg;
	void* pBuffer;
	const I32 bytesRead=readBuffer(&pBuffer,&msg);

	if(bytesRead>0)
	{
		a_rByteArray.resize(bytesRead);
		memcpy(a_rByteArray.data(),(U8*)pBuffer,bytesRead);
	}
	else
	{
		a_rByteArray.resize(0);
	}

	zmq_msg_close(&msg);

	return bytesRead;
}

I32 ZeroCatalog::ReceiverTask::readIdentity(ZeroCatalog::Identity& a_identity)
{
	zmq_msg_t msg;
	void* pBuffer;
	const I32 bytesRead=readBuffer(&pBuffer,&msg);

	if(bytesRead==FE_ZRC_IDENTITY_BYTES)
	{
		memcpy(a_identity.m_data,(U8*)pBuffer,bytesRead);
	}
	else
	{
		memset(a_identity.m_data,0,FE_CNC_IDENTITY_BYTES);
	}

	zmq_msg_close(&msg);

	a_identity.m_text.sPrintf("0x%02x%02x%02x%02x%02x",
			a_identity.m_data[0],
			a_identity.m_data[1],
			a_identity.m_data[2],
			a_identity.m_data[3],
			a_identity.m_data[4]);

	return bytesRead;
}

void ZeroCatalog::ReceiverTask::operate(void)
{
#if FE_ZRC_DEBUG
	BWORD wasConnected=FALSE;
#endif

	while(TRUE)
	{
		if(!m_pZeroCatalog)
		{
			feLog("ZeroCatalog::ReceiverTask::operate"
					" ZeroCatalog disappeared\n");
			return;
		}

#if FE_ZRC_VERBOSE
		feLog("ZeroCatalog::ReceiverTask::operate"
				" checking for message timer=%.6G\n",
				m_pZeroCatalog->timerSeconds());
#endif

		if(!m_pZeroCatalog->started())
		{
			m_pZeroCatalog->yield();
			continue;
		}

		if(!m_pZeroCatalog->connected())
		{
			m_pZeroCatalog->timerRestart();
		}

		const BWORD isServer=(m_pZeroCatalog->role() == "server");

		if(!isServer && m_pZeroCatalog->connected() &&
				m_pZeroCatalog->timerReached())
		{
			m_pZeroCatalog->dropConnection();
			return;
		}

#if FE_ZRC_DEBUG
		const BWORD nowConnected=m_pZeroCatalog->connected();
		if(wasConnected && !nowConnected)
		{
			feLog("ZeroCatalog::ReceiverTask::operate disconnect detected\n");
		}
		wasConnected=nowConnected;
#endif

		if(m_pZeroCatalog->m_messages.m_available.load(
				std::memory_order_relaxed))
		{
			m_pZeroCatalog->m_messages.safeLock();

			Array<zmq_msg_t>& rMessageArray=
					m_pZeroCatalog->m_messages.m_messageArray;
			Array<I32>& rSubCountArray=
					m_pZeroCatalog->m_messages.m_subCountArray;
			Array<Identity>& rRecipientArray=
					m_pZeroCatalog->m_messages.m_recipientArray;
			std::set<Identity> removeSet;
			const I32 messageCount=rMessageArray.size();
			I32 recipientCount(0);
			I32 subIndex(0);
			for(I32 messageIndex=0;messageIndex<messageCount;messageIndex++)
			{
				zmq_msg_t& rMsg=rMessageArray[messageIndex];
				const I32 byteCount=zmq_msg_size(&rMsg);

				subIndex++;

				std::chrono::steady_clock::time_point tp0=
						std::chrono::steady_clock::now();

				const I32 bytesSent=zmq_msg_send(&rMsg,m_pSocket,ZMQ_SNDMORE);

				std::chrono::steady_clock::time_point tp1=
						std::chrono::steady_clock::now();
				const Real microseconds=1e6*
						std::chrono::duration<float>(tp1-tp0).count();
				if(microseconds>50.0)
				{
					feLog("ZeroCatalog::ReceiverTask::operate"
							" zmq_msg_send took %4.1f us\n",microseconds);
				}

				if(bytesSent<0)
				{
					const Identity& rIdentity=rRecipientArray[recipientCount];

					feLog("ZeroCatalog::ReceiverTask::operate"
							" recipient \"%s\" send message failed \"%s\"\n",
							rIdentity.m_text.c_str(),
							strerror(errno));

					if(isServer)
					{
						removeSet.insert(rIdentity);
					}
				}
				else if(bytesSent!=byteCount)
				{
					feLog("ZeroCatalog::ReceiverTask::operate"
							" only sent %d/%d bytes\n",
							bytesSent,byteCount);
				}

				const I32 subCount=rSubCountArray[recipientCount];
				if(subIndex==subCount)
				{
					std::chrono::steady_clock::time_point tp0=
							std::chrono::steady_clock::now();

					const I32 bytesSent=zmq_send(m_pSocket,
							"EOM",4,ZMQ_DONTWAIT);

					std::chrono::steady_clock::time_point tp1=
							std::chrono::steady_clock::now();
					const Real microseconds=1e6*
							std::chrono::duration<float>(tp1-tp0).count();
					if(microseconds>50.0)
					{
						feLog("ZeroCatalog::ReceiverTask::operate"
								" zmq_send took %4.1f us\n",microseconds);
					}

					if(bytesSent<0)
					{
						feLog("ZeroCatalog::ReceiverTask::operate"
								" send EOM failed \"%s\"\n",
								strerror(errno));
					}
					else if(bytesSent!=4)
					{
						feLog("ZeroCatalog::ReceiverTask::operate"
								" error completing message\n");
					}

					recipientCount++;
					subIndex=0;
					continue;
				}
			}

//			feLog("ZeroCatalog::ReceiverTask::operate"
//					" messageCount %d recipientCount %d\n",
//					messageCount,recipientCount);

			for(std::set<Identity>::reverse_iterator it=removeSet.rbegin();
					it!=removeSet.rend();it++)
			{
				const Identity& rIdentity= *it;

				feLog("ZeroCatalog::ReceiverTask::operate"
						" removing client \"%s\"\n",
						rIdentity.m_text.c_str());

				m_pZeroCatalog->removeIdentity(rIdentity);
			}

			rMessageArray.clear();
			rSubCountArray.clear();
			rRecipientArray.clear();

			m_pZeroCatalog->m_messages.m_available=false;

			m_pZeroCatalog->m_messages.safeUnlock();
		}

		if(m_terminate.load(std::memory_order_relaxed))
		{
#if FE_ZRC_DEBUG
			feLog("ZeroCatalog::ReceiverTask::operate terminate detected\n");
#endif
			return;
		}

		String source;

		if(isServer)
		{
			Identity identity;
			I32 bytesRead=readIdentity(identity);
			if(bytesRead<0)
			{
				if(errno==EAGAIN)
				{
#if FE_ZRC_VERBOSE
					feLog("ZeroCatalog::ReceiverTask::operate no identity\n");
#endif
					m_pZeroCatalog->yield();
				}
				else
				{
					feLog("ZeroCatalog::ReceiverTask::operate"
							" error reading identity\n");
				}
				continue;
			}
			if(bytesRead!=FE_ZRC_IDENTITY_BYTES)
			{
#if FE_ZRC_DEBUG
				feLog("ZeroCatalog::ReceiverTask::operate bad identity\n");
#endif
				continue;
			}

			I32& rIdentityCount=m_pZeroCatalog->m_identityCount;
			Identity *const pIdentityArray=m_pZeroCatalog->m_identityArray;
			I32 identityIndex=0;
			for(;identityIndex<rIdentityCount;identityIndex++)
			{
				if(identity==pIdentityArray[identityIndex])
				{
					break;
				}
			}

			if(identityIndex>=rIdentityCount)
			{
				m_pZeroCatalog->m_messages.safeLock();

				Identity& rNewIdentity=pIdentityArray[rIdentityCount];
				rNewIdentity=identity;

#if FE_ZRC_DEBUG
				feLog("ZeroCatalog::ReceiverTask::operate"
						" new identity %d %s\n",
						rIdentityCount,rNewIdentity.m_text.c_str());
#endif

				rIdentityCount++;	//* atomic-ish

				m_pZeroCatalog->m_messages.safeUnlock();
			}

			source=identity.m_text;
		}

		String command;
		I32 bytesRead=readString(command);
		if(bytesRead<0)
		{
			if(errno==EAGAIN)
			{
#if FE_ZRC_VERBOSE
				feLog("ZeroCatalog::ReceiverTask::operate no message\n");
#endif
				m_pZeroCatalog->yield();
			}
			else
			{
				feLog("ZeroCatalog::ReceiverTask::operate"
						" error reading 'command'\n");
			}
			continue;
		}

		m_pZeroCatalog->timerRestart();

		const BWORD isSignal=(command=="signal");
		if(isSignal || command=="update")
		{
			String key;
			bytesRead=readString(key);

			String property;
			bytesRead=readString(property);

			String typeString;
			bytesRead=readString(typeString);

//			feLog("ZeroCatalog::ReceiverTask::operate"
//					" got '%s' \"%s\" \"%s\" \"%s\"\n",
//					isSignal? "signal": "update",
//					key.c_str(),property.c_str(),typeString.c_str());

			if(m_pZeroCatalog->useBinaryForType(typeString))
			{
				Array<U8> byteArray;
				bytesRead=readBytes(byteArray);

//				feLog("bytearray %d %p\n",byteCount,byteArray.data());

				m_pZeroCatalog->update(isSignal? e_signal: e_update,source,
						key,property,typeString,"",
						byteArray.data(),byteArray.size());
			}
			else
			{
				String valueString;
				bytesRead=readString(valueString);

//				feLog("  got value \"%s\"\n",valueString.c_str());

				m_pZeroCatalog->update(isSignal? e_signal: e_update,source,
						key,property,
						typeString,valueString);
			}

			String eom;
			bytesRead=readString(eom);
			if(eom!="EOM")
			{
				feLog("ZeroCatalog::ReceiverTask::operate no EOM\n");
			}
		}
		else if(command=="remove")
		{
			String key;
			bytesRead=readString(key);

			String property;
			bytesRead=readString(property);

			m_pZeroCatalog->update(e_remove,source,key,property,"","");

			String eom;
			bytesRead=readString(eom);
			if(eom!="EOM")
			{
				feLog("ZeroCatalog::ReceiverTask::operate no EOM\n");
			}
		}
		else if(command=="tick")
		{
//			feLog("TICK\n");

			String eom;
			bytesRead=readString(eom);
			if(eom!="EOM")
			{
				feLog("ZeroCatalog::ReceiverTask::operate no EOM\n");
			}
		}
		else
		{
			feLog("ZeroCatalog::ReceiverTask::operate"
					" unknown command \"%s\"\n",command.c_str());
		}
	}
}

} /* namespace ext */
} /* namespace fe */

