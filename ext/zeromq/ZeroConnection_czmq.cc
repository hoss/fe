/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "zeromq/zeromq.pmh"

#define FE_ZCN_VERBOSE		FALSE

namespace fe
{
namespace ext
{

ZeroConnection::ZeroConnection(void):
	m_isServer(FALSE),
	m_connected(FALSE),
	m_pSocket(NULL),
	m_pMsgID(NULL)
{
#if FE_ZCN_VERBOSE
	feLog("ZeroConnection::ZeroConnection\n");
#endif
}

ZeroConnection::~ZeroConnection(void)
{
#if FE_ZCN_VERBOSE
	feLog("ZeroConnection::~ZeroConnection\n");
#endif

	disconnect();

	if(m_pSocket)
	{
		zsock_destroy(&m_pSocket);
		m_pSocket=NULL;
	}
}

void ZeroConnection::initialize(void)
{
}

Result ZeroConnection::connect(String a_address,short a_port)
{
#if FE_ZCN_VERBOSE
	feLog("ZeroConnection::connect \"%s\" port %d\n",
			a_address.c_str(),a_port);
#endif

	m_isServer=a_address.empty();

	if(m_isServer)
	{
		m_endpoint.sPrintf("tcp://*:%d",a_port);

#if FE_ZCN_VERBOSE
		feLog("ZeroConnection::connect server \"%s\"\n",m_endpoint.c_str());
#endif

		m_pSocket=zsock_new(ZMQ_ROUTER);
		const int rc=zsock_bind(m_pSocket,m_endpoint.c_str());
		if(rc<0)
		{
			feLog("ZeroConnection::connect port %d failed binding\n",a_port);
			return e_refused;
		}

		String message;
		Result result=receive(message);
		if(failure(result))
		{
			return result;
		}

#if FE_ZCN_VERBOSE
		feLog("ZeroConnection::connect server token message\"%s\"\n",
				message.c_str());
#endif
	}
	else
	{
		m_endpoint.sPrintf("tcp://%s:%d",a_address.c_str(),a_port);

#if FE_ZCN_VERBOSE
		feLog("ZeroConnection::connect client \"%s\"\n",m_endpoint.c_str());
#endif

		m_pSocket=zsock_new(ZMQ_DEALER);
		const int rc=zsock_connect(m_pSocket,m_endpoint.c_str());
		if(rc<0)
		{
			feLog("ZeroConnection::connect \"%s\" port %d failed connection\n",
					a_address.c_str(),a_port);
			return e_refused;
		}

		Result result=send("token");
		if(failure(result))
		{
			return result;
		}
	}

	m_connected=TRUE;
	return e_ok;
}

Result ZeroConnection::disconnect(void)
{
#if FE_ZRC_VERBOSE
	feLog("ZeroConnection::disconnect\n");
#endif

	if(!m_connected)
	{
		return e_notInitialized;
	}

	m_connected=FALSE;

    zsock_disconnect(m_pSocket, m_endpoint.c_str());

	return e_ok;
}

Result ZeroConnection::send(String a_message)
{
#if FE_ZCN_VERBOSE
	feLog("ZeroConnection::send \"%s\"\n",a_message.c_str());
#endif

	zmsg_t* pMsg=zmsg_new();

	if(m_isServer)
	{
		if(!m_pMsgID)
		{
			feLog("ZeroConnection::broadcast NULL routing ID\n");
			return e_writeFailed;
		}

		//* NOTE frame is auto-destroyed when sent

		//* routing ID
		zframe_t* pDup=zframe_dup(m_pMsgID);
		zmsg_prepend(pMsg,&pDup);
	}

	int rc=zmsg_addstr(pMsg,a_message.c_str());
	if(rc<0)
	{
		feLog("ZeroConnection::broadcast failed to add message \"%s\"\n",
				a_message.c_str());
		return e_writeFailed;
	}
	rc=zmsg_send(&pMsg,m_pSocket);
	if(rc<0)
	{
		feLog("ZeroConnection::broadcast failed to send message \"%s\"\n",
				a_message.c_str());
		return e_writeFailed;
	}

	return e_ok;
}

Result ZeroConnection::receive(String& a_rMessage)
{
#if FE_ZCN_VERBOSE
	feLog("ZeroConnection::receive\n");
#endif

	zmsg_t *pMsg=zmsg_recv(m_pSocket);
	if(!pMsg)
	{
		feLog("ZeroConnection::ReceiverTask::operate NULL message\n");
		return e_readFailed;
	}

	if(m_isServer)
	{
		//* routing ID
		zframe_t* pMsgID=zmsg_pop(pMsg);
		if(!m_pMsgID)
		{
			m_pMsgID=pMsgID;

#if FE_ZCN_VERBOSE
			feLog("ZeroConnection::receive pMsgId %p\n",m_pMsgID);
#endif
		}
	}

#if FE_ZCN_VERBOSE
	zmsg_print(pMsg);
#endif

	char* buffer=zmsg_popstr(pMsg);
	a_rMessage=buffer;
	zstr_free(&buffer);

	zmsg_destroy(&pMsg);

#if FE_ZCN_VERBOSE
	feLog("ZeroConnection::receive read \"%s\"\n",a_rMessage.c_str());
#endif

	return e_ok;
}

} /* namespace ext */
} /* namespace fe */

