
/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __zeromq_ConnectionI_h__
#define __zeromq_ConnectionI_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
    @brief Simple Network Connection

	@ingroup zeromq
*//***************************************************************************/
class FE_DL_EXPORT ConnectionI:
	virtual public Component,
	public CastableAs<ConnectionI>
{
	public:

virtual	Result	connect(String a_address,short a_port)						=0;
virtual	Result	send(String a_message)										=0;
virtual	Result	receive(String& a_rMessage)									=0;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __zeromq_ConnectionI_h__ */
