/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __zeromq_ZeroCatalog_h__
#define __zeromq_ZeroCatalog_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
    @brief Catalog over ZeroMQ

	@ingroup zeromq
*//***************************************************************************/
class FE_DL_EXPORT ZeroCatalog:
	public ConnectedCatalog,
	public Initialize<ZeroCatalog>
{
	public:

				ZeroCatalog(void);
virtual			~ZeroCatalog(void);

		void	initialize(void);

virtual	Result	connectAsServer(short a_port);
virtual	Result	connectAsClient(String a_address,short a_port);

virtual	void	broadcast(String a_name,String a_property);

virtual	Result	disconnect(void);

	private:

	class FE_DL_EXPORT ReceiverTask: public Thread::Functor
	{
		public:
					ReceiverTask(void):
						m_terminate(FALSE)									{}
virtual	void		operate(void);

			hp<ZeroCatalog>	m_hpZeroCatalog;
			zsock_t*		m_pSocket;
			BWORD			m_terminate;
	};

		Thread*				m_pReceiverThread;
		ReceiverTask		m_receiverTask;

		String				m_endpoint;
		zsock_t*			m_pSocket;
		zframe_t*			m_pMsgID;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __zeromq_ZeroCatalog_h__ */
