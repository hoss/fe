/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __zeromq_ZeroConnection_h__
#define __zeromq_ZeroConnection_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
    @brief Simple ZeroMQ Connection

	@ingroup zeromq
*//***************************************************************************/
class FE_DL_EXPORT ZeroConnection:
	public ConnectionI,
	public Initialize<ZeroConnection>
{
	public:

				ZeroConnection(void);
virtual			~ZeroConnection(void);

		void	initialize(void);

virtual	Result	connect(String a_address,short a_port);

virtual	Result	send(String a_message);
virtual	Result	receive(String& a_rMessage);

	private:
		Result	disconnect(void);

		BWORD		m_isServer;
		BWORD		m_connected;
		String		m_endpoint;
		zsock_t*	m_pSocket;
		zframe_t*	m_pMsgID;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __zeromq_ZeroConnection_h__ */
