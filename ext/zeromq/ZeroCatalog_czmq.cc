/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "zeromq/zeromq.pmh"

#define FE_ZRC_VERBOSE		FALSE

namespace fe
{
namespace ext
{

ZeroCatalog::ZeroCatalog(void):
	m_pReceiverThread(NULL),
	m_pSocket(NULL),
	m_pMsgID(NULL)
{
#if FE_ZRC_VERBOSE
	feLog("ZeroCatalog::ZeroCatalog\n");
#endif
}

ZeroCatalog::~ZeroCatalog(void)
{
#if FE_ZRC_VERBOSE
	feLog("ZeroCatalog::~ZeroCatalog\n");
#endif

	if(m_pSocket)
	{
		const int rc=zsock_unbind(m_pSocket,m_endpoint.c_str());
		if(rc<0)
		{
			feLog("ZeroCatalog::~ZeroCatalog could not unbind \"%s\"\n",
					m_endpoint.c_str());
		}

		zsock_destroy(&m_pSocket);
		m_pSocket=NULL;
	}
}

void ZeroCatalog::initialize(void)
{
	m_receiverTask.m_hpZeroCatalog=sp<ZeroCatalog>(this);
}

Result ZeroCatalog::connectAsServer(short a_port)
{
#if FE_ZRC_VERBOSE
	feLog("ZeroCatalog::connectAsServer port %d\n",a_port);
#endif

	m_endpoint.sPrintf("tcp://*:%d",a_port);

    m_pSocket=zsock_new(ZMQ_ROUTER);
    const int rc=zsock_bind(m_pSocket,m_endpoint.c_str());
	if(rc<0)
	{
		feLog("ZeroCatalog::connectAsServer port %d failed binding\n",a_port);
		return e_refused;
	}

#if FE_ZRC_VERBOSE
	feLog("ZeroCatalog::connectAsServer creating thread\n");
#endif

	m_receiverTask.m_pSocket=m_pSocket;
	m_pReceiverThread=new Thread(&m_receiverTask);

	return ConnectedCatalog::connectAsServer(a_port);
}

Result ZeroCatalog::connectAsClient(String a_address,short a_port)
{
#if FE_ZRC_VERBOSE
	feLog("ZeroCatalog::connectAsClient \"%s\" port %d\n",
			a_address.c_str(),a_port);
#endif

	m_endpoint.sPrintf("tcp://%s:%d",a_address.c_str(),a_port);

    m_pSocket=zsock_new(ZMQ_DEALER);
    const int rc=zsock_connect(m_pSocket,m_endpoint.c_str());
	if(rc<0)
	{
		feLog("ZeroCatalog::connectAsClient \"%s\" port %d failed connection\n",
				a_address.c_str(),a_port);
		return e_refused;
	}

	m_receiverTask.m_pSocket=m_pSocket;
	m_pReceiverThread=new Thread(&m_receiverTask);

	return ConnectedCatalog::connectAsClient(a_address,a_port);
}

void ZeroCatalog::broadcast(String a_name,String a_property)
{
#if FE_ZRC_VERBOSE
	feLog("ZeroCatalog::broadcast \"%s\" \"%s\"\n",
			a_name.c_str(),a_property.c_str());
#endif

	zmsg_t* pMsg=zmsg_new();

	if(role() == "server")
	{
		if(!m_pMsgID)
		{
			feLog("ZeroCatalog::broadcast \"%s\" \"%s\" NULL routing ID\n",
					a_name.c_str(),a_property.c_str());
		}

		//* routing ID
		zframe_t* pDup=zframe_dup(m_pMsgID);
		zmsg_prepend(pMsg,&pDup);
	}

	int rc=zmsg_addstr(pMsg,"update");
	if(rc<0)
	{
		feLog("failed to add update\n");
	}

	rc=zmsg_addstr(pMsg,a_name.c_str());
	if(rc<0)
	{
		feLog("failed to add name\n");
	}

	rc=zmsg_addstr(pMsg,a_property.c_str());
	if(rc<0)
	{
		feLog("failed to add property\n");
	}

	const String typeString=catalogTypeName(a_name,a_property);
	rc=zmsg_addstr(pMsg,typeString.c_str());
	if(rc<0)
	{
		feLog("failed to add type\n");
	}

	if(typeString=="bytearray")
	{
		Array<U8>& rByteArray=catalog< Array<U8> >(a_name,a_property);
		rc=zmsg_addmem(pMsg,rByteArray.data(),rByteArray.size());
		if(rc<0)
		{
			feLog("failed to add byte block\n");
		}
	}
	else
	{
		rc=zmsg_addstr(pMsg,catalogValue(a_name,a_property).c_str());
		if(rc<0)
		{
			feLog("failed to add value\n");
		}
	}

	rc=zmsg_send(&pMsg,m_pSocket);
	if(rc<0)
	{
		feLog("ZeroCatalog::broadcast \"%s\" \"%s\" failed to send\n",
			a_name.c_str(),a_property.c_str());
	}

	heartbeat();
}

Result ZeroCatalog::disconnect(void)
{
#if FE_ZRC_VERBOSE
	feLog("ZeroCatalog::disconnect\n");
#endif

	if(!connected())
	{
		return e_notInitialized;
	}

	Result result=ConnectedCatalog::disconnect();

    zsock_disconnect(m_pSocket, m_endpoint.c_str());

	m_receiverTask.m_terminate=TRUE;
	m_pReceiverThread->join();
	delete m_pReceiverThread;
	m_pReceiverThread=NULL;

	return result;
}

void ZeroCatalog::ReceiverTask::operate(void)
{
	BWORD wasConnected=FALSE;

	while(TRUE)
	{
#if FE_ZRC_VERBOSE
		feLog("ZeroCatalog::ReceiverTask::operate checking for message\n");
#endif

		const BWORD nowConnected=m_hpZeroCatalog->connected();
#if FE_ZRC_VERBOSE
		if(wasConnected && !nowConnected)
		{
			feLog("ZeroCatalog::ReceiverTask::operate disconnect detected\n");
		}
#endif
		wasConnected=nowConnected;

		if(m_terminate)
		{
#if FE_ZRC_VERBOSE
			feLog("ZeroCatalog::ReceiverTask::operate terminate detected\n");
#endif
			return;
		}

		zmsg_t *pMsg=zmsg_recv_nowait(m_pSocket);
		if(!pMsg)
		{
#if FE_ZRC_VERBOSE
			feLog("ZeroCatalog::ReceiverTask::operate no message\n");
#endif
			continue;
		}

#if FE_ZRC_VERBOSE
		zmsg_print(pMsg);
#endif

		if(m_hpZeroCatalog->role() == "server")
		{
			//* routing ID
			zframe_t* pMsgID=zmsg_pop(pMsg);
			if(!m_hpZeroCatalog->m_pMsgID)
			{
				m_hpZeroCatalog->m_pMsgID=pMsgID;
#if FE_ZRC_VERBOSE
				feLog("ZeroConnection::ReceiverTask::operate pMsgId %p\n",
						m_hpZeroCatalog->m_pMsgID);
#endif
			}
		}

		char* buffer=zmsg_popstr(pMsg);
		const String command(buffer);
		zstr_free(&buffer);

		if(String(command)=="update")
		{
			buffer=zmsg_popstr(pMsg);
			const String key(buffer);
			zstr_free(&buffer);

			buffer=zmsg_popstr(pMsg);
			const String property(buffer);
			zstr_free(&buffer);

			buffer=zmsg_popstr(pMsg);
			const String typeString(buffer);
			zstr_free(&buffer);

			if(typeString=="bytearray")
			{
				zframe_t* pFrame=zmsg_pop(pMsg);
				const I32 byteCount=zframe_size(pFrame);
				const U8* bRawBytes=zframe_data(pFrame);

//				feLog("bytearray %d %p\n",byteCount,bRawBytes);

				m_hpZeroCatalog->update(key,property,typeString,"",
						bRawBytes,byteCount);

				zframe_destroy(&pFrame);
			}
			else
			{
				buffer=zmsg_popstr(pMsg);
				const String valueString(buffer);
				zstr_free(&buffer);

				m_hpZeroCatalog->update(key,property,typeString,valueString);
			}
		}
		else
		{
			feLog("ZeroCatalog::ReceiverTask::operate"
					" unknown command \"%s\"\n",command.c_str());
		}

		zmsg_destroy(&pMsg);
	}
}

} /* namespace ext */
} /* namespace fe */

