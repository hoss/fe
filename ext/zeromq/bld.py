import os
import sys
import string
forge = sys.modules["forge"]

# zmq on Windows:
# install vcpkg in your FE_WINDOWS_LOCAL
# see https://github.com/Microsoft/vcpkg/
# vcpkg.exe install zmq:x86-windows-static-md
# vcpkg.exe install zmq:x64-windows-static-md

def prerequisites():
	return [ "network" ]

def setup(module):
	zmq_include = os.environ["FE_ZMQ_INCLUDE"]
	zmq_lib = os.environ["FE_ZMQ_LIB"]

	srclist = [	"zeromq.pmh",
				"ZeroCatalog",
				"ZeroConnection",
				"zeromqDL",
				]

	deplibs = forge.basiclibs + [
				"fePluginLib",
				"fexNetworkDLLib" ]

	dll = module.DLL( "fexZeroMQDL", srclist)

	module.includemap = {}
	dll.linkmap = {}

	if forge.fe_os == "FE_LINUX":
		if zmq_include != "" and zmq_lib != "":
			module.includemap['zmq'] = zmq_include

			dll.linkmap['zmq'] = "-Wl,-rpath='" + zmq_lib + "'"
			dll.linkmap['zmq'] += ' -L' + zmq_lib

		dll.linkmap["zmq_libs"] = "-lzmq"
	elif forge.fe_os == "FE_WIN32" or forge.fe_os == "FE_WIN64":
		deplibs += [ "feDataLib" ]

		if forge.codegen == 'debug':
			dll.linkmap["zmq_libs"] = "libzmq-mt-sgd-4_3_4.lib Iphlpapi.lib Rpcrt4.lib"
		else:
			dll.linkmap["zmq_libs"] = "libzmq-mt-s-4_3_4.lib Iphlpapi.lib Rpcrt4.lib"

	forge.deps( ["fexZeroMQDLLib"], deplibs )

	forge.tests += [
		("inspect.exe",		"fexZeroMQDL",					None,		None) ]

	module.Module('test')

def auto(module):
	zmq_include = os.environ["FE_ZMQ_INCLUDE"]
	zmq_lib = os.environ["FE_ZMQ_LIB"]

	if forge.fe_os == "FE_LINUX":
		if zmq_include != "" and zmq_lib != "":
			forge.includemap['zmq'] = zmq_include

			forge.linkmap['zmq'] = "-Wl,-rpath='" + zmq_lib + "'"
			forge.linkmap['zmq'] += ' -L' + zmq_lib

		forge.linkmap["zmq_libs"] = "-lzmq"

	elif forge.fe_os == "FE_WIN32" or forge.fe_os == "FE_WIN64":
		if forge.codegen == 'debug':
			forge.linkmap["zmq_libs"] = "libzmq-mt-sgd-4_3_4.lib Iphlpapi.lib Rpcrt4.lib"
		else:
			forge.linkmap["zmq_libs"] = "libzmq-mt-s-4_3_4.lib Iphlpapi.lib Rpcrt4.lib"

	test_file = """
#include <zmq.h>

int main(void)
{
	return(0);
}
    \n"""

	result = forge.cctest(test_file)

	forge.includemap.pop('zmq', None)
	forge.linkmap.pop('zmq', None)
	forge.linkmap.pop('zmq_libs', None)

	return result
