/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "zeromq/zeromq.pmh"

using namespace fe;
using namespace fe::ext;

int main(int argc,char** argv)
{
	UNIT_START();

	BWORD complete=FALSE;

	try
	{
		sp<Master> spMaster(new Master);
		sp<Registry> spRegistry=spMaster->registry();

		Result result=spRegistry->manage("fexZeroMQDL");
		UNIT_TEST(successful(result));

		{
			sp<ConnectionI> spConnectionI(spRegistry->create("ConnectionI"));
			if(spConnectionI.isNull())
			{
				feX(argv[0], "couldn't create components");
			}

			spConnectionI->connect("127.0.0.1",7890);

			String message;

			I32 count=0;
			while(count<1000)
			{
				spConnectionI->receive(message);

				if(!(++count%100))
				{
					feLog("count %d\n",count);
				}
			}

			spConnectionI->send("done");
		}

		complete=TRUE;
	}
	catch(Exception &e) { e.log(); }
	catch(...)			{ feLog("uncaught exception\n"); }

	UNIT_TEST(complete);
	UNIT_TEST(!Counted::trackerCount());
	UNIT_TRACK(3);
	UNIT_RETURN();
}
