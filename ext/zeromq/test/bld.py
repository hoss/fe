import sys
forge = sys.modules["forge"]

import os.path

def setup(module):
	deplibs = forge.corelibs[:]

	if forge.fe_os == 'FE_WIN32' or forge.fe_os == 'FE_WIN64':
		deplibs += [	"fexNetworkDLLib",
						"fexZeroMQDLLib" ]

	tests = [	'xZeroRawClient',
				'xZeroRawServer' ]

	for t in tests:
		exe = module.Exe(t)

		exe.linkmap = {}
		if forge.fe_os == "FE_LINUX":
			exe.linkmap["zmq_libs"] = "-lzmq"
		elif forge.fe_os == "FE_WIN32" or forge.fe_os == "FE_WIN64":
			if forge.codegen == 'debug':
				exe.linkmap["zmq_libs"] = "libzmq-mt-sgd-4_3_4.lib Iphlpapi.lib Rpcrt4.lib"
			else:
				exe.linkmap["zmq_libs"] = "libzmq-mt-s-4_3_4.lib Iphlpapi.lib Rpcrt4.lib"

		forge.deps([t + "Exe"], deplibs)
