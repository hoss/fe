/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __surface_SurfaceAccessibleLua_h__
#define __surface_SurfaceAccessibleLua_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
    @brief Lua Surface Binding

	The intent is to manipulate the input context surface or
	to generate a new procedural surface.

	@ingroup lua
*//***************************************************************************/
class FE_DL_EXPORT SurfaceAccessibleLua:
	public SurfaceAccessibleRecord
{
	public:
						SurfaceAccessibleLua(void);
virtual					~SurfaceAccessibleLua(void);

						//* as SurfaceAccessibleI

						using SurfaceAccessibleRecord::load;
virtual	BWORD			load(String a_filename,sp<Catalog> a_spSettings);

	protected:

virtual	void			reset(void);

	private:
		sp<LuaI>				m_spLuaI;
		sp<Scope>				m_spScope;
		SystemTicker			m_systemTicker;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __surface_SurfaceAccessibleLua_h__ */
