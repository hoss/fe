/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __lua_LuaComponent_h__
#define __lua_LuaComponent_h__

namespace fe
{
namespace ext
{

typedef LuaObject<sp<Component>, LuaContext::e_component> t_lua_component;

/** Component to Lua binding object */
class LuaComponentObject : public t_lua_component
{
	public:
					LuaComponentObject(
						LuaContext *pContext, const sp<Component> &data)
						: t_lua_component(pContext, data) {}
virtual				~LuaComponentObject(void){}
static		int		index(lua_State *pLuaState);
static		int		newindex(lua_State *pLuaState);
static		int		call(lua_State *pLuaState);
static		int		name(lua_State *pLuaState);
static		int		setName(lua_State *pLuaState);
static		int		adjoin(lua_State *pLuaState);

		void			setMethod(const String &name, lua_CFunction f)
		{
			m_methods[name] = f;
		}
		bool			pushMethod(String &name)
		{
			std::map<String, lua_CFunction>::iterator i_methods =
				m_methods.find(name);
			if(i_methods == m_methods.end()) { return false; }
			lua_State *pLuaState = context()->state();
			lua_pushstring(pLuaState, name.c_str());
			lua_pushcclosure(context()->state(), i_methods->second, 1);
			return true;
		}
		void			populateMethods(void);
		template<class T>
		bool			is(void)
		{
			return data().is<T>();
		}
static	void			pushComponent(lua_State *pLuaState, sp<Component> spC)
		{
			lua_getglobal(pLuaState, "fe_context");
			LuaContext *pContext = (LuaContext *)(lua_touserdata(pLuaState,-1));
			lua_pop(pLuaState, 1);

			void *pBlock = lua_newuserdata(pLuaState,
				sizeof(LuaComponentObject));

			LuaComponentObject *pC=
				new(pBlock)LuaComponentObject(pContext,spC);

			pC->populateMethods();
		}
	private:
		std::map<String, lua_CFunction>	m_methods;
};

typedef LuaType<sp<Component>, LuaComponentObject> t_lua_component_t;
class LuaComponentType : public t_lua_component_t
{
	public:
					LuaComponentType(LuaContext *pContext,const String &mtn);
virtual				~LuaComponentType(void){}
virtual		void	pushNewMetaTable(void);
};

template<class T>
T *to(lua_State *pLuaState, int index, int tid)
{
	void *pV = lua_touserdata(pLuaState, index);
	if(!pV)
	{
		String s;
		s.sPrintf("type mismatch on arg %d : did you use '.' instead of ':' for method?", index);
		LuaContext::error(pLuaState, s.c_str());
		assert(0);
	}

	LuaBaseObject *pB = (LuaBaseObject *)pV;
	if(pB->type() != pB->context()->lookup(tid))
	{
		String s;
		s.sPrintf("type mismatch on argument %d", index);
		assert(0);
		LuaContext::error(pLuaState, s.c_str());
	}

	return static_cast<T *>(pB);
}

template<class Obj, class T>
sp<T> counted(lua_State *pLuaState, int index, int tid)
{
	return to<Obj>(pLuaState,index,tid)->data();
}

template<class T>
sp<T> component(lua_State *pLuaState, int index)
{
	LuaComponentObject *pC =
		to<LuaComponentObject>(pLuaState, index, LuaContext::e_component);

	sp<T> spT(pC->data());

	if(!spT.isValid())
	{
		String s;
		s.sPrintf("component type mismatch on argument %d", index);
		LuaContext::error(pLuaState, s.c_str());
	}

	return spT;
}

template<class T>
LuaComponentObject *validate(lua_State *pLuaState, int index)
{
	sp<T> spT = component<T>(pLuaState, index);
	LuaComponentObject *pC = to<LuaComponentObject>(pLuaState, index,
			LuaContext::e_component);

	return pC;
}

Record record(lua_State *pLuaState, int index);

sp<RecordGroup> recordgroup(lua_State *pLuaState, int index);

} /* namespace ext */
} /* namespace fe */

#endif /* __lua_LuaComponent_h__ */
