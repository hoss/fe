/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <lua/lua.pmh>

#define FE_SAL_DEBUG			FALSE
#define FE_SAL_TICKER			FALSE
#define FE_SAL_TICK				FALSE

namespace fe
{
namespace ext
{

SurfaceAccessibleLua::SurfaceAccessibleLua(void):
	m_systemTicker("SurfaceAccessibleLua")
{
//	feLog("SurfaceAccessibleLua::SurfaceAccessibleLua\n");
}

SurfaceAccessibleLua::~SurfaceAccessibleLua(void)
{
//	feLog("SurfaceAccessibleLua::~SurfaceAccessibleLua\n");
}

void SurfaceAccessibleLua::reset(void)
{
	SurfaceAccessibleRecord::reset();
}

BWORD SurfaceAccessibleLua::load(String a_filename,sp<Catalog> a_spSettings)
{
#if FE_SAL_TICKER
	m_systemTicker.log("external");
#endif

#if FE_SAL_DEBUG
	feLog("SurfaceAccessibleLua::load \"%s\"\n",a_filename.c_str());
#endif

	sp<SurfaceAccessibleI> spDirectInput[4];
	const char* inputName[4]={ "input0", "input1", "input2", "input3" };

	Real frame=0.0;
	String options;
	String configuration;
	String inputString;
	if(a_spSettings.isValid())
	{
		if(a_spSettings->cataloged("frame"))
		{
			frame=a_spSettings->catalog<Real>("frame");
		}
		options=a_spSettings->catalog<String>("options");
		configuration=a_spSettings->catalog<String>("configuration");
		inputString=a_spSettings->catalogOrDefault<String>("inputString","");

		for(I32 m=0;m<4;m++)
		{
			spDirectInput[m]=a_spSettings->catalogOrDefault< sp<Component> >(
					inputName[m],sp<Component>(NULL));
		}
	}

	if(m_spLuaI.isNull())
	{
		const String setupString=
				"function createPoint()\n"
				"point = pt:createRecord()\n"
				"output[\"points\"]:add(point)\n"
				"return point\n"
				"end\n"
				"function createPrimitive()\n"
				"primitive = prim:createRecord()\n"
				"output[\"primitives\"]:add(primitive)\n"
				"return primitive\n"
				"end\n"
				"function createVertex(primitive)\n"
				"point = createPoint()\n"
				"pointCount = output[\"points\"]:length()\n"
				"primitive[\"vertexMap\"]:add(pointCount-1)\n"
				"return point\n"
				"end\n\n"
				"function createOperator(implementation)\n"
				"terminalDispatch = create(\"*.TerminalDispatch\")\n"
				"terminalDispatch[\"_scope_\"] = scope\n"
				"terminalDispatch[\"_implementation_\"] = implementation\n"
				"return terminalDispatch\n"
				"end\n"
				"function replaceOutputWith(mesh)\n"
				"output[\"points\"] = mesh[\"points\"]\n"
				"output[\"vertices\"] = mesh[\"vertices\"]\n"
				"output[\"primitives\"] = mesh[\"primitives\"]\n"
				"output[\"detail\"] = mesh[\"detail\"]\n"
				"end\n\n";

		m_spLuaI=registry()->create("LuaI");

		m_spLuaI->preloadString(setupString);

		if(!a_filename.empty())
		{
			Result result=m_spLuaI->loadFile(a_filename);
			if(result==e_invalidFile)
			{
				feLog("SurfaceAccessibleLua::load"
						" failed to open lua file \"%s\"\n",
						a_filename.c_str());
				reset();
				return FALSE;
			}
			if(result==e_readFailed)
			{
				feLog("SurfaceAccessibleLua::load"
						" failed to read lua file \"%s\"\n",
						a_filename.c_str());
				reset();
				return FALSE;
			}
			if(result==e_unsolvable)
			{
				feLog("SurfaceAccessibleLua::load"
						" failed to compile lua file \"%s\"\n",
						a_filename.c_str());
				reset();
				return FALSE;
			}
			if(failure(result))
			{
				feLog("SurfaceAccessibleLua::load"
						" unknown error using lua file \"%s\"\n",
						a_filename.c_str());
				reset();
				return FALSE;
			}
		}
		else if(inputString.empty() ||
				failure(m_spLuaI->loadString(inputString)))
		{
			feLog("SurfaceAccessibleLua::load"
					" failed to compile lua string\n");
			reset();
			return FALSE;
		}

		//* TEMP should just pass along a unique scope to TerminalDispatch
//X		m_spScope=registry()->master()->catalog()->catalogComponent(
//X				"Scope","TerminalScope");

		m_spScope=registry()->create("Scope");

		m_spScope->setLocking(FALSE);

		bind(m_spScope);

		BWORD validInput=FALSE;

		sp<SurfaceAccessibleRecord> spRecordInput[4];
		for(I32 m=0;m<4;m++)
		{
			if(spDirectInput[m].isValid())
			{
				//* only copy if input isn't already SurfaceAccessibleRecord
				sp<SurfaceAccessibleRecord> spSurfaceAccessibleRecord=
						spDirectInput[m];
				if(spSurfaceAccessibleRecord.isValid())
				{
					spRecordInput[m]=spSurfaceAccessibleRecord;
				}
				else
				{
					spRecordInput[m]=
							registry()->create("*.SurfaceAccessibleRecord");
#if FALSE
					//* unified Scope
					spRecordInput[m]->bind(m_spScope);
#else
					//* independent Scope
					sp<Scope> spScope=registry()->create("Scope");
					spRecordInput[m]->bind(spScope);
#endif

					spRecordInput[m]->copy(spDirectInput[m]);
				}

				m_spLuaI->set(inputName[m],spRecordInput[m]->record());

				//* copy first valid input to output
				if(!validInput)
				{
					copy(spRecordInput[m]);
					validInput=TRUE;
				}
			}
		}

		if(!validInput)
		{
			reset();
		}

		m_spLuaI->set("scope",m_spScope);
		m_spLuaI->set("output",record());

		sp<Layout> spLayoutPoint=m_spScope->declare("pt");
		spLayoutPoint->populate("spc:at","vector3");
		m_spLuaI->set("pt",spLayoutPoint);

		sp<Layout> spLayoutPrimitive=m_spScope->declare("prim");
		spLayoutPrimitive->populate("surf:points","intarray");
		m_spLuaI->set("prim",spLayoutPrimitive);

		SurfaceModel surfaceModelRV;
		surfaceModelRV.bind(record());

		sp<RecordGroup> spRG=surfaceModelRV.surfacePointRG();
		RecordGroup::iterator it;
		if((it=spRG->begin()) != spRG->end())
		{
			m_spLuaI->set("pointArray",*it);
		}

		spRG=surfaceModelRV.surfaceVertexRG();
		if((it=spRG->begin()) != spRG->end())
		{
			m_spLuaI->set("vertexArray",*it);
		}

		spRG=surfaceModelRV.surfacePrimitiveRG();
		if((it=spRG->begin()) != spRG->end())
		{
			m_spLuaI->set("primitiveArray",*it);
		}

		spRG=surfaceModelRV.surfaceDetailRG();
		if((it=spRG->begin()) != spRG->end())
		{
			m_spLuaI->set("detailArray",*it);
		}

		m_spLuaI->alias("P","spc:at");
		m_spLuaI->alias("N","spc:up");
		m_spLuaI->alias("uv","surf:uvw");
		m_spLuaI->alias("vertexMap","surf:points");
		m_spLuaI->alias("points","surf:pointgroup");
		m_spLuaI->alias("vertices","surf:vertgroup");
		m_spLuaI->alias("primitives","surf:primgroup");
		m_spLuaI->alias("detail","surf:detgroup");
	}

	Array<Vector2*> vector2PtrArray;
	Array<SpatialVector*> vector3PtrArray;
	Array<Vector4*> vector4PtrArray;
	Array<Vector4f*> vector4fPtrArray;

	if(a_spSettings.isValid())
	{
		Array<String> keys;
		a_spSettings->catalogKeys(keys);

		const U32 keyCount=keys.size();
		for(U32 keyIndex=0;keyIndex<keyCount;keyIndex++)
		{
			const String key=keys[keyIndex];
			const String io=a_spSettings->catalogOrDefault<String>(key,"IO","");
			if(io=="input" || io=="input output")
			{
				Instance instance;
				const bool success=a_spSettings->catalogLookup(key,instance);
				if(!success)
				{
					continue;
				}

				if(instance.is<String>())
				{
					m_spLuaI->set(key,a_spSettings->catalog<String>(key));
				}

				if(instance.is<bool>())
				{
					m_spLuaI->set(key,a_spSettings->catalog<bool>(key));
				}

				if(instance.is<I32>())
				{
					m_spLuaI->set(key,a_spSettings->catalog<I32>(key));
				}

				if(instance.is<Real>())
				{
					m_spLuaI->set(key,a_spSettings->catalog<Real>(key));
				}

				if(instance.is<Vector2>())
				{
					Vector2* pVector2=new Vector2();

					const I32 arrayIndex=vector2PtrArray.size();
					vector2PtrArray.resize(arrayIndex+1);
					vector2PtrArray[arrayIndex]=pVector2;

					*pVector2=a_spSettings->catalog<Vector2>(key);

					//* NOTE stored by pointer
					m_spLuaI->set(key,*pVector2);
				}

				if(instance.is<Vector3>())
				{
					SpatialVector* pVector3=new SpatialVector();

					const I32 arrayIndex=vector3PtrArray.size();
					vector3PtrArray.resize(arrayIndex+1);
					vector3PtrArray[arrayIndex]=pVector3;

					*pVector3=a_spSettings->catalog<Vector3>(key);

					//* NOTE stored by pointer
					m_spLuaI->set(key,*pVector3);
				}

				if(instance.is<Vector4>())
				{
					Vector4* pVector4=new Vector4();

					const I32 arrayIndex=vector4PtrArray.size();
					vector4PtrArray.resize(arrayIndex+1);
					vector4PtrArray[arrayIndex]=pVector4;

					*pVector4=a_spSettings->catalog<Vector4>(key);

					//* NOTE stored by pointer
					m_spLuaI->set(key,*pVector4);
				}

				if(instance.is<Vector4f>())
				{
					Vector4f* pVector4f=new Vector4f();

					const I32 arrayIndex=vector4fPtrArray.size();
					vector4fPtrArray.resize(arrayIndex+1);
					vector4fPtrArray[arrayIndex]=pVector4f;

					*pVector4f=a_spSettings->catalog<Vector4f>(key);

					//* NOTE stored by pointer
					m_spLuaI->set(key,*pVector4f);
				}

				if(instance.is< sp<Component> >())
				{
					m_spLuaI->set(key,
							a_spSettings->catalog< sp<Component> >(key));
				}
			}
		}
	}
	String option;
	while(!(option=options.parse()).empty())
	{
		const String property=option.parse("\"","=");
		const String value=option.parse("\"","=");

//		feLog("property \"%s\" value \"%s\"\n",property.c_str(),value.c_str());

		if(value.match("^[-0-9.E]+,[-0-9.E]+,[-0-9.E]+$"))
		{
			SpatialVector* pVector3=new SpatialVector();

			const I32 arrayIndex=vector3PtrArray.size();
			vector3PtrArray.resize(arrayIndex+1);
			vector3PtrArray[arrayIndex]=pVector3;

			set(*pVector3);
			sscanf(value.c_str(),"%G,%G,%G",
					&(*pVector3)[0],&(*pVector3)[1],&(*pVector3)[2]);

//			feLog("\"%s\" vector %s\n",property.c_str(),c_print(*pVector3));
			//* NOTE stored by pointer
			m_spLuaI->set(property,*pVector3);
			continue;
		}
		if(value.match("^[-0-9.E]+$"))
		{
			Real number=0.0;
			sscanf(value.c_str(),"%G",&number);

//			feLog("\"%s\" number %.6G\n",property.c_str(),number);
			m_spLuaI->set(property,number);
			continue;
		}

//		feLog("string \"%s\" \"%s\"\n",property.c_str(),value.c_str());
		m_spLuaI->set(property,value);
	}

	m_spLuaI->set("frame",frame);
	m_spLuaI->set("configuration",configuration);

#if FE_SAL_TICKER
	m_systemTicker.log("prep");
#endif

#if FE_SAL_DEBUG
	feLog("SurfaceAccessibleLua::load execute\n");
#endif

#if FE_SAL_TICK
	const U32 tickStart=systemTick();
#endif

	if(!m_spLuaI->execute())
	{
		feLog("SurfaceAccessibleLua::load Lua execution failed\n");
		return FALSE;
	}

#if FE_SAL_TICKER
	m_systemTicker.log("execute");
#endif

#if FE_SAL_DEBUG
	feLog("SurfaceAccessibleLua::load execute done\n");
#endif

#if FE_SAL_TICK
	const U32 tickEnd=systemTick();
	const U32 tickDiff=SystemTicker::tickDifference(tickStart,tickEnd);
	const Real ms=1e-3*tickDiff*SystemTicker::microsecondsPerTick();
	feLog("SurfaceAccessibleLua::load executed %.6G ms\n",ms);
#endif

	const I32 vector2Count=vector2PtrArray.size();
	for(I32 vectorIndex=0;vectorIndex<vector2Count;vectorIndex++)
	{
		delete vector2PtrArray[vectorIndex];
	}
	const I32 vector3Count=vector3PtrArray.size();
	for(I32 vectorIndex=0;vectorIndex<vector3Count;vectorIndex++)
	{
		delete vector3PtrArray[vectorIndex];
	}
	const I32 vector4Count=vector4PtrArray.size();
	for(I32 vectorIndex=0;vectorIndex<vector4Count;vectorIndex++)
	{
		delete vector4PtrArray[vectorIndex];
	}
	const I32 vector4fCount=vector4fPtrArray.size();
	for(I32 vectorIndex=0;vectorIndex<vector4fCount;vectorIndex++)
	{
		delete vector4fPtrArray[vectorIndex];
	}

	Record result=m_spLuaI->getRecord("output");

	m_spScope=result.layout()->scope();

	bind(result);

//	feLog("SurfaceAccessibleLua::load points %d primitives %d\n",
//			count(SurfaceAccessibleI::e_point),
//			count(SurfaceAccessibleI::e_primitive));

	if(a_spSettings.isValid())
	{
		Array<String> keys;
		a_spSettings->catalogKeys(keys);

		const U32 keyCount=keys.size();
		for(U32 keyIndex=0;keyIndex<keyCount;keyIndex++)
		{
			const String key=keys[keyIndex];
			const String io=a_spSettings->catalogOrDefault<String>(key,"IO","");
			if(io=="output" || io=="input output")
			{
				Instance instance;
				const bool success=a_spSettings->catalogLookup(key,instance);
				if(!success)
				{
					continue;
				}

				if(instance.is<String>())
				{
					a_spSettings->catalog<String>(key)=
							m_spLuaI->getString(key);
				}

				if(instance.is<bool>())
				{
					a_spSettings->catalog<bool>(key)=
							m_spLuaI->getReal(key);
				}

				if(instance.is<I32>())
				{
					a_spSettings->catalog<I32>(key)=
							m_spLuaI->getReal(key);
				}

				if(instance.is<Real>())
				{
					a_spSettings->catalog<Real>(key)=
							m_spLuaI->getReal(key);
				}

				if(instance.is<Vector2>())
				{
					a_spSettings->catalog<Vector2>(key)=
							m_spLuaI->getVector2(key);
				}

				if(instance.is<Vector3>())
				{
					a_spSettings->catalog<Vector3>(key)=
							m_spLuaI->getVector3(key);
				}

				if(instance.is<Vector4>())
				{
					a_spSettings->catalog<Vector4>(key)=
							m_spLuaI->getVector4(key);
				}

				if(instance.is<Vector4f>())
				{
					a_spSettings->catalog<Vector4f>(key)=
							m_spLuaI->getVector4f(key);
				}
			}
		}
	}

#if FE_SAL_DEBUG
	feLog("SurfaceAccessibleLua::load done\n");
#endif

#if FE_SAL_TICKER
	m_systemTicker.log("complete");
#endif

	return TRUE;
}

} /* namespace ext */
} /* namespace fe */
