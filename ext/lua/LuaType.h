/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __lua_LuaType_h__
#define __lua_LuaType_h__

namespace fe
{
namespace ext
{

class LuaBaseType : public Counted
{
	public:
					LuaBaseType(void) {}
virtual				~LuaBaseType(void) {}

virtual	void		putMetaTable(void)										= 0;
virtual	void		pushMetaTable(void)										= 0;

		lua_State		*state(void) { return m_pContext->state(); }
		LuaContext		*context(void) { return m_pContext; }

		void			setMethod(const String &name, lua_CFunction f)
		{
			m_methods[name] = f;
		}
		bool			pushMethod(String &name)
		{
			std::map<String, lua_CFunction>::iterator i_methods =
				m_methods.find(name);
			if(i_methods == m_methods.end()) { return false; }
			lua_pushcfunction(state(), i_methods->second);
			return true;
		}

		//sp<BaseType>	baseType(void) { return m_spBT; }

const	String&		name(void) const			{ return m_mtname; }
const	String		verboseName(void) const;

	protected:
		//sp<BaseType>					m_spBT;
		LuaContext						*m_pContext;
		String							m_mtname;
		std::map<String, lua_CFunction>	m_methods;
};

inline const String LuaBaseType::verboseName(void) const
{
	return "LuaBaseType " + name();
}

template<class T, class Obj>
class LuaType : public LuaBaseType
{
	public:
					LuaType(LuaContext *pContext, const String &mtname)
					{
						m_mtname = mtname;
						m_pContext = pContext;
						//m_spBT = pContext->typeMaster()->
						//	lookupType(TypeInfo(typeid(T)));
					}
virtual				~LuaType(void) {}
virtual		void	pushMetaTable(void)
					{
						lua_getglobal(m_pContext->state(), m_mtname.c_str());
					}
virtual		void	putMetaTable(void)
					{
						lua_rawgeti(m_pContext->state(),
								LUA_REGISTRYINDEX, LUA_RIDX_GLOBALS);
						lua_pushstring(m_pContext->state(), m_mtname.c_str());
						pushNewMetaTable();
						lua_settable(m_pContext->state(), -3);
						lua_pop(m_pContext->state(), 1);
					}
virtual		void	pushNewMetaTable(void)
					{
						lua_State *pLuaState = m_pContext->state();

//						lua_newtable(pLuaState);
						luaL_newmetatable(pLuaState,m_mtname.c_str());

						lua_pushstring(pLuaState, "__gc");
						lua_pushcfunction(pLuaState, Obj::destroy);
						lua_settable(pLuaState, -3);


						lua_pushstring(pLuaState, "__index");
						lua_pushcfunction(pLuaState, Obj::index);
						lua_settable(pLuaState, -3);

					}
	private:
};

} /* namespace ext */
} /* namespace fe */

#endif /* __lua_LuaType_h__ */

