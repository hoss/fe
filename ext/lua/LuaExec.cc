/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <lua/lua.pmh>

namespace fe
{
namespace ext
{

LuaExec::LuaExec(void)
{
	m_pLuaState = NULL;
	m_name = "unnamed";
}

LuaExec::~LuaExec(void)
{
	flush();
#if FE_COUNTED_TRACK
	U32 size=m_trackArray.size();
	for(U32 m=0;m<size;m++)
	{
		Counted::deregisterRegion(m_trackArray[m]);
	}
#endif
	if(m_pLuaState) { lua_close(m_pLuaState); }
}

void LuaExec::initialize(void)
{
	m_pLuaState = lua_open();
	luaL_Reg lualibs[] = {
		{ "base",       luaopen_base },
		{ "table",      luaopen_table },
		{ "io",         luaopen_io },
		{ "string",     luaopen_string },
		{ "math",       luaopen_math },
		{ "debug",      luaopen_debug },
		{ "loadlib",    luaopen_loadlib },
		{ NULL,         NULL }
		};
		luaL_Reg *lib;

		for (lib = lualibs; lib->func != NULL; lib++)
		{
			lib->func(m_pLuaState);
			lua_settop(m_pLuaState, 0);
		}

	m_spContext = new LuaContext(registry()->master(), m_pLuaState);

	sp<TypeMaster> spTM=registry()->master()->typeMaster();

	dispatch<String>("loadString");

	dispatch<String>("setRecord");
	dispatch<Record>("setRecord");
}

Result LuaExec::loadFile(const String &filename)
{
	m_name = filename;
	std::ifstream file;
	file.open(filename.c_str(),std::ifstream::in|std::ifstream::binary);
	if(!file)
	{
		return e_invalidFile;
	}

	file.seekg (0, std::ios::end);
	int length = file.tellg();
	file.seekg (0, std::ios::beg);

	char *buffer = new char [length+1];
	memset((void *)buffer, 0, length+1); // make valgrind happy

	file.read (buffer,length);
	if(!file)
	{
		return e_readFailed;
	}

	m_chunk = buffer;

	delete [] buffer;
	file.close();

	return compile();
}

Result LuaExec::loadString(const String &text)
{
	m_name = "loaded string";
	m_chunk = text;
	return compile();
}

Result LuaExec::compile(void)
{

	int result;
	result = luaL_loadbuffer(m_pLuaState, m_chunk.c_str(), m_chunk.length(),
			m_name.c_str());
	if(result != 0)
	{
		feLog("LUA syntax error: %s \n", lua_tostring(m_pLuaState, -1));
		return e_unsolvable;
	}
	lua_setglobal(m_pLuaState, m_name.c_str());
	return e_ok;
}

void LuaExec::set(const String &key, Record r_value)
{
	t_lua_record::push(m_spContext.raw(), r_value);
	lua_setglobal(m_pLuaState, key.c_str());

#if FE_COUNTED_TRACK
	Record& record=r_value;
	if(record.isValid())
	{
		void* data=record.data();
		U32 size=record.layout()->size();

		Counted::registerRegion(data,size,
				"LuaExec:Record "+record.layout()->name());
		Counted::trackReference(data,fe_cast<Counted>(this));

		m_trackArray.push_back(data);
	}
#endif
}

void LuaExec::set(const String &key, const String &value)
{
	lua_pushstring(m_pLuaState, value.c_str());
	lua_setglobal(m_pLuaState, key.c_str());
}

void LuaExec::set(const String &key, sp<Layout> l_value)
{
	t_lua_layout::push(m_spContext.raw(), l_value);
	lua_setglobal(m_pLuaState, key.c_str());
}

void LuaExec::set(const String &key, sp<Component> spValue)
{
	lua_rawgeti(m_pLuaState, LUA_REGISTRYINDEX, LUA_RIDX_GLOBALS);
	lua_pushstring(m_pLuaState, key.c_str());
	LuaComponentObject::pushComponent(m_pLuaState, spValue);
	lua_settable(m_pLuaState, -3);
	lua_pop(m_pLuaState, 1);

	//lua_setglobal(m_pLuaState, key.c_str());
}

void LuaExec::set(const String &key, sp<Scope> spValue)
{
	t_lua_scope::push(m_spContext.raw(), spValue);
	lua_setglobal(m_pLuaState, key.c_str());
}

bool LuaExec::execute(void)
{
	lua_pushcfunction(m_pLuaState, LuaContext::debug);

	lua_getglobal(m_pLuaState, m_name.c_str());

	int result = lua_pcall(m_pLuaState, 0, 0, -2);
	if(result)
	{
		String err = lua_tostring(m_pLuaState, -1);
		lua_pop(m_pLuaState, 1);

		//* NOTE avoid % formatting
		feLog("%s",err.c_str());

		return false;
	}
	lua_pop(m_pLuaState, 1);

#if 0
	// force garbage collection
	lua_setgcthreshold(m_pLuaState,0);
#endif

	return true;
}

void LuaExec::flush(void)
{
	lua_setgcthreshold(m_pLuaState,0);
}

void LuaExec::handle(Record& r_sig)
{
	set("r_sig", r_sig);
	execute();
}

bool LuaExec::call(const String &a_name, Array<Instance> a_argv)
{
	if(a_name == FE_DISPATCH("loadString", "[string]"))
	{
		String text = a_argv[0].cast<String>();
		if(!loadString(text))
		{
			feX("LuaExec::call",
				"lua script error");
		}
	}

	if(a_name == FE_DISPATCH("setRecord", "[name] [record]"))
	{
		String name = a_argv[0].cast<String>();
		Record record = a_argv[1].cast<Record>();
		set(name, record);
	}

	return true;
}

} /* namespace ext */
} /* namespace fe */
