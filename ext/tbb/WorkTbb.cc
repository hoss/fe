/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <tbb/tbb.pmh>

#define FE_WKT_DEBUG	FALSE

using namespace fe;
using namespace fe::ext;

void WorkTbb::initialize(void)
{
}

void WorkTbb::run(sp<WorkI> a_spWorkI,U32 a_threads,
	sp<SpannedRange> a_spSpannedRange,String a_stage)
{
	m_spWorkI=a_spWorkI;

#if FE_WKT_DEBUG
	feLog("WorkTbb::run %d threads\n",a_threads);
#endif

	//* NOTE SpannedRange::split discards postAtomic (but it might not split)

	sp<SpannedRange> spNotPostAtomic(new SpannedRange());

	spNotPostAtomic->nonAtomic().add(a_spSpannedRange->nonAtomic());

	const U32 atoms=a_spSpannedRange->atomicCount();
	for(U32 atomicIndex=0;atomicIndex<atoms;atomicIndex++)
	{
		spNotPostAtomic->addAtomic().add(a_spSpannedRange->atomic(atomicIndex));
	}

	sp<SpannedRange> spPostAtomic(new SpannedRange());
	spPostAtomic->nonAtomic().add(a_spSpannedRange->postAtomic());

#if FE_WKT_DEBUG
	feLog("WorkTbb::run range %s\n  nonPost %s\n  post %s\n",
			c_print(a_spSpannedRange),c_print(spNotPostAtomic),
			c_print(spPostAtomic));
#endif

//	tbb::auto_partitioner
//	tbb::affinity_partitioner
//	tbb::simple_partitioner
//	tbb::static_partitioner

	const U32 count=atoms+spNotPostAtomic->nonAtomic().valueCount();
	const I32 threadCount=Thread::hardwareConcurrency();

//	const U32 each=(count>threadCount)? (count+threadCount-1)/threadCount: 1;
	const U32 each=fe::maximum(U32(1),U32(0.51*count/threadCount));

	Range range(spNotPostAtomic,each);
	Body body(a_spWorkI,a_stage);
	tbb::parallel_for(range,body,tbb::auto_partitioner());

	const WorkI::Threading wasThreading=m_spWorkI->threading();
	m_spWorkI->setThreading(WorkI::e_singleThread);

	m_spWorkI->run(-1,spPostAtomic,a_stage);

	m_spWorkI->setThreading(wasThreading);
	m_spWorkI=NULL;

//	m_spGang->resolveException();

#if FE_WKT_DEBUG
	feLog("WorkTbb::run done\n");
#endif
}

WorkTbb::Range::Range(Range& a_rRange,tbb::split)
{
	//* NOTE (this) is new range; argument is the original range to split

	sp<SpannedRange> spOriginal=a_rRange.spannedRange();
	sp<SpannedRange> spNew0;
	sp<SpannedRange> spNew1;

	spOriginal->split(spNew0,spNew1);

#if FE_WKT_DEBUG
	feLog("WorkTbb::Range::Range split %s\n  %s\n  %s\n",
			c_print(spOriginal),c_print(spNew0),c_print(spNew1));
#endif

	m_spSpannedRange=spNew0;
	a_rRange.m_spSpannedRange=spNew1;

	m_grain=a_rRange.m_grain;
}
