/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __tbb_WorkTbb_h__
#define __tbb_WorkTbb_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Execute a SpannedRange with TBB

	@ingroup tbb
*//***************************************************************************/
class FE_DL_EXPORT WorkTbb: public WorkForceI,
	public Initialize<WorkTbb>
{
	public:
					WorkTbb(void)											{}
virtual				~WorkTbb(void)											{}

		void		initialize(void);

virtual	sp<WorkI>	assignment(void)			{ return m_spWorkI; }
virtual	void		run(sp<WorkI> a_spWorkI,U32 a_threads,
							sp<SpannedRange> a_spSpannedRange,String a_stage);
virtual	void		stop(void)												{}

virtual	BWORD		threadsAlive(void) const	{ return 0; }

virtual	BWORD		willWaitForJobs(void) const	{ return FALSE; }
virtual	void		waitForJobs(BWORD a_wait)								{}

	private:


	class Range
	{
		public:
								Range(sp<SpannedRange> a_spSpannedRange,
									I32 a_grain):
									m_spSpannedRange(a_spSpannedRange),
									m_grain(a_grain)						{}

								Range(const Range& a_rRange):
									m_spSpannedRange(
											a_rRange.m_spSpannedRange),
									m_grain(a_rRange.m_grain)				{}

								Range(Range& a_rRange,tbb::split);

								~Range()									{}

			bool				is_divisible(void) const
								{
									const I32 pieces=
											m_spSpannedRange->atomicCount()+
											m_spSpannedRange->nonAtomic()
											.valueCount();
									return (pieces>m_grain);
								}

			bool				empty(void) const
								{	return m_spSpannedRange->empty(); }

			sp<SpannedRange>	spannedRange(void) const
								{	return m_spSpannedRange; }

		private:

			sp<SpannedRange>	m_spSpannedRange;
			I32					m_grain;
	};

	class Body
	{
		public:
						Body(sp<WorkI> a_spWorkI,String a_stage):
							m_spWorkI(a_spWorkI),
							m_stage(a_stage)								{}

						Body(const Body& a_rBody):
							m_spWorkI(a_rBody.m_spWorkI),
							m_stage(a_rBody.m_stage)						{}

						~Body(void)											{}

			void		operator()(Range& a_rRange) const
						{
							const U32 id= -1;	//* TODO

							m_spWorkI->run(id,a_rRange.spannedRange(),m_stage);
						}

		private:
			sp<WorkI>	m_spWorkI;
			String		m_stage;
	};

		sp<WorkI>			m_spWorkI;
};

} /* namespace ext */
} /* namespace fe */

#endif // __tbb_WorkTbb_h__
