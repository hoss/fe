/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "spatial/spatial.h"

namespace fe
{
namespace ext
{

void gradient(Continuum<Real> &a_continuum, SpatialVector &a_gradient, const SpatialVector &a_location, Real a_h)
{
	for(unsigned int i = 0; i < 3; i++)
	{
		SpatialVector lo(a_location);
		SpatialVector hi(a_location);
		lo[i] -= a_h;
		hi[i] += a_h;
		a_continuum.safe(lo);
		a_continuum.safe(hi);
		Real lo_smpl = directSample(a_continuum, lo);
		Real hi_smpl = directSample(a_continuum, hi);
		a_gradient[i] = (hi_smpl - lo_smpl)/(2.0*a_h);
	}
}

void rad_func(UnitContinuum<Real,3> &a_unit, UnitContinuum<Real,3>::t_index &a_index, t_n_radial_set *a_data)
{
	Real value = 0.0;
	SpatialVector index_loc;
	Continuum<Real> &a_continuum(*(a_data->m_continuum));
	a_continuum.location(index_loc, a_index);
	for(unsigned int i = 0; i < 3; i++)
	{
		Real dist = fabs(index_loc[i] - (*(a_data->m_location))[i]);
		dist /= (*(a_data->m_radius))[i];
		dist *= dist;
		if(dist > 1.0) return;
		value += dist;
	}
	value = 1.0-sqrt(value);
	if(value < 0.0) return;
	value = a_data->m_value*value;
	a_continuum.accessUnit().set(a_index, value);
}

/** Radial set function.
	@relates Continuum */
void radialSet(Continuum<Real> &a_continuum, const SpatialVector &a_location,
		const SpatialVector &a_radius, Real a_value)
{
	t_n_radial_set radial_data;
	radial_data.m_location = const_cast<SpatialVector *>(&a_location);
	radial_data.m_radius = const_cast<SpatialVector *>(&a_radius);
	radial_data.m_value = a_value;
	radial_data.m_continuum = &a_continuum;
	a_continuum.accessUnit().iterate(rad_func, &radial_data);
}


} /* namespace ext */
} /* namespace fe */

