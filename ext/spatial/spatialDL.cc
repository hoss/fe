/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "fe/plugin.h"
#include "signal/signal.h"
#include "spatial/spatial.h"
#include "platform/dlCore.cc"
#include "Proximity.h"
#include "ProxBrute.h"
#include "ProxMultiGrid.h"
#include "ProxSweep.h"
#include "ProxHash.h"
#include "Flatten.h"

using namespace fe;
using namespace fe::ext;

extern "C"
{

FE_DL_EXPORT void ListDependencies(List<String*>& list)
{
}

FE_DL_EXPORT Library* CreateLibrary(sp<Master> spMaster)
{
	return CreateSpatialLibrary(spMaster);
}

FE_DL_EXPORT void InitializeLibrary(sp<Library> spLibrary)
{
}

}
