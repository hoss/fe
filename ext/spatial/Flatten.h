/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __spatial_Flatten_h__
#define __spatial_Flatten_h__

#include "signal/signal.h"
#include "datatool/datatool.h"
#include "math/math.h"
#include "shape/shape.h"

namespace fe
{
namespace ext
{

/**	Constrain particles in cartesian space by adjusting positions.

	@copydoc Flatten_info
	*/
class FE_DL_EXPORT Flatten : public Initialize<Flatten>,
	virtual public HandlerI,
	virtual public Config
{
	public:
		typedef enum
		{
			e_eq,
			e_lt,
			e_gt
		} t_mode;
				Flatten(void);
virtual			~Flatten(void);
		void	initialize(void);

		void	addLimit(int a_dimension, Real a_value, t_mode a_mode);

				// AS HandlerI
virtual	void	handle(Record &r_sig);

	private:
		struct t_limit
		{
			int		m_dimension;
			Real	m_value;
			t_mode	m_mode;
		};
		std::vector<t_limit>		m_limits;
		AsParticle					m_asParticle;
		AsSignal					m_asSignal;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __spatial_Flatten_h__ */

