/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __spatial_GridVectorField_h__
#define __spatial_GridVectorField_h__

#include "fe/plugin.h"
#include "spatial/VectorFieldI.h"

namespace fe
{
namespace ext
{

class FE_DL_EXPORT GridVectorField :
	virtual public VectorFieldI,
	public Initialize<GridVectorField>
{
	public:
typedef Continuum<SpatialVector>	t_continuum;

						GridVectorField(void);
virtual					~GridVectorField(void);

		void			initialize(void);

						// AS VectorFieldI
virtual	bool			sample(	SpatialVector &a_result,
								const SpatialVector &a_location);

		void			create(	const sp<SpaceI>	&a_space,
								const Vector3i		&a_count);

		t_continuum		&continuum(void);



	private:
		t_continuum						m_continuum;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __spatial_GridVectorField_h__ */

