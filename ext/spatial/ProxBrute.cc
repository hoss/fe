/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "ProxBrute.h"

namespace fe
{
namespace ext
{

ProxBrute::ProxBrute(void)
{
}

ProxBrute::~ProxBrute(void)
{
}

unsigned int ProxBrute::detect(	sp<Layout>			l_pair,
								sp<RecordGroup>		rg_input,
								sp<RecordGroup>		rg_output)
{
	m_asParticle.bind(l_pair->scope());
	m_asBounded.bind(l_pair->scope());
	m_filters.clear();
	m_filters.push_back(m_asBounded.radius);
	m_filters.push_back(m_asParticle.location);

	m_asProximity.bind(l_pair->scope());

	unsigned long col = 0;
	Record r_pair;

	for(RecordGroup::iterator i_rg = rg_input->begin(m_filters);
		i_rg != rg_input->end(); i_rg++)
	{
		sp<RecordArray> spRAi = *i_rg;
		for(int i = 0; i < spRAi->length(); i++)
		{
			for(RecordGroup::iterator j_rg = i_rg;
				j_rg != rg_input->end(); j_rg++)
			{
				sp<RecordArray> spRAj = *j_rg;
				int j;
				if(spRAi == spRAj) { j = i + 1; }
				else { j = 0; }
				for(; j < spRAj->length(); j++)
				{
					Real distSqr =
						magnitudeSquared(m_asParticle.location(spRAi,i) -
							m_asParticle.location(spRAj,j));
					Real bndSqr = m_asBounded.radius(spRAi,i) +
						m_asBounded.radius(spRAj,j);
					bndSqr *= bndSqr;
					if(distSqr < bndSqr)
					{
						col++;
						r_pair = l_pair->scope()->createRecord(l_pair);
						spRAi->set(m_asProximity.left(r_pair), i);
						spRAj->set(m_asProximity.right(r_pair), j);
						if(rg_output.isValid())
						{
							rg_output->add(r_pair);
						}
					}
				}
			}
		}
	}
	return col;
}

} /* namespace ext */
} /* namespace fe */

