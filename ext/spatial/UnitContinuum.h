/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __spatial_UnitContinuum_h__
#define __spatial_UnitContinuum_h__

namespace fe
{
namespace ext
{


template<typename T, unsigned int D>
class UnitContinuum
{
	public:
							UnitContinuum(void) { }
virtual						~UnitContinuum(void) { }

		UnitContinuum<T,D> &operator=(const UnitContinuum<T,D> &other)
		{
			m_count = other.m_count;
			for(unsigned int i = 0; i < D; i++)
			{
				m_periodic[i] = other.m_periodic[i];
				m_size[i] = other.m_size[i];
				m_halfsize[i] = other.m_halfsize[i];
			}
			m_cells = other.m_cells;
			m_totalCount = other.m_totalCount;
			return *this;
		}

		bool				operator==(const UnitContinuum<T,D> &other)
		{
			if(m_count != other.m_count) { return false; }
			if(m_periodic != other.m_periodic) { return false; }
			if(m_size != other.m_size) { return false; }
			if(m_totalCount != other.m_totalCount) { return false; }
			if(m_cells == other.m_cells) { return true; }
			return false;
		}

		bool				operator!=(const UnitContinuum<T,D> &other)
		{
			return !operator==(other);
		}

		typedef	Vector<D,bool>			t_periodic;
		typedef	Vector<D,int>			t_index;
		typedef Vector<D,Real>			t_span;
		typedef std::vector<T>			t_cells;

		void				create(	const t_index &a_count,
									const t_periodic &a_periodic,
									const T &a_init);

		void				create(	const t_index &a_count,
									const T &a_init);

		// safe index oriented access
		const T				&operator[](const t_index &a_index) const;
		void				set(const t_index &a_index, const T &value);
		void				set(const T &value);

		// unsafe index oriented access
		T					&access(const t_index &a_index);
		const T				&get(const t_index &a_index) const;

		// unsafe location to index methods (indices may be out of range)
		void				index(		t_index &a_index,
										const t_span &a_location) const;
		void				lower(		t_index &a_index,
										const t_span &a_location) const;

		// index to location methods
		void				location(	t_span &a_location,
										const t_index &a_index) const;

		// data structure informational
		const t_span		&cellsize(void) const { return m_size; }
		const t_index		&count(void) const { return m_count; }
		const t_periodic	&periodic(void) const { return m_periodic; }
		unsigned int		totalCount(void) const { return m_totalCount; }

		// data struction navigational
		template<typename A>
		void				iterate(	void (*a_function)(UnitContinuum<T,D> &,
										t_index &,
										A),
										A a_userarg);
		void				addNeighbors(
										std::list<t_index> &a_neighbors,
										const t_index &a_index) const;

		// out-of-range safety
		t_index				&safe(		t_index &a_index) const;
		t_span				&safe(		t_span &a_span) const;

	private:
		bool			safe(		unsigned int a_d,
									t_index &a_index) const;
		unsigned int	calc_array_index(	const t_index &a_index) const;
		template<typename A>
		void			rIterate(	unsigned int a_d,
									t_index &a_index,
									void (*a_function)(UnitContinuum<T,D> &,
									t_index &,
									A),
									A a_userarg);

	private:
		t_index			m_count;
		unsigned int	m_totalCount;
		t_cells			m_cells;
		t_periodic		m_periodic;
		t_span			m_size;
		t_span			m_halfsize;		// precomputed as optimization
};

template<typename T, unsigned int D>
inline bool UnitContinuum<T,D>::safe(unsigned int a_d, t_index &a_index) const
{
	// return true if the requested index is a valid index
	bool rv = true;
	if(m_count[a_d] == 1)
	{
		if(a_index[a_d] != 0)
		{
			if(!m_periodic[a_d])
			{
				rv = false;
			}
			a_index[a_d] = 0;
		}
	}
	else if(m_count[a_d] == 2)
	{
		if(a_index[a_d] < 0)
		{
			if(!m_periodic[a_d])
			{
				rv = false;
			}
			a_index[a_d] = 0;
		}
		else if(a_index[a_d] > 1)
		{
			if(!m_periodic[a_d])
			{
				rv = false;
			}
			a_index[a_d] = 1;
		}
	}
	else
	{
		if(a_index[a_d] < 0)
		{
			if(m_periodic[a_d])
			{
				a_index[a_d] = m_count[a_d] - (-a_index[a_d])%m_count[a_d];
			}
			else
			{
				if(!m_periodic[a_d])
				{
					rv = false;
				}
				a_index[a_d] = 0;
			}
		}
		else if(a_index[a_d] >= m_count[a_d])
		{
			if(m_periodic[a_d])
			{
				a_index[a_d] = a_index[a_d]%m_count[a_d];
			}
			else
			{
				if(!m_periodic[a_d])
				{
					rv = false;
				}
				a_index[a_d] = m_count[a_d]-1;
			}
		}
	}

	return rv;
}

template<typename T, unsigned int D>
inline typename UnitContinuum<T,D>::t_index &UnitContinuum<T,D>::safe(t_index &a_index) const
{
	for(unsigned int i = 0; i < D; i++)
	{
		safe(i, a_index);
	}
	return a_index;
}

template<typename T, unsigned int D>
inline typename UnitContinuum<T,D>::t_span &UnitContinuum<T,D>::safe(
		t_span &a_span) const
{
	for(unsigned int i = 0; i < D; i++)
	{
		if(m_periodic[i])
		{
			float integer;
			a_span[i] = modff(a_span[i], &integer);
			if(integer < 0.0)
			{
				a_span[i] = 1.0 + a_span[i];
			}
		}
		else
		{
			if(a_span[i] < 0.0)
			{
				a_span[i] = 0.0;
			}
			else if(a_span[i] > 1.0)
			{
				a_span[i] = 1.0;
			}
		}
	}

	return a_span;
}

template<typename T, unsigned int D>
template<typename A>
inline void UnitContinuum<T,D>::iterate(void (*a_function)(UnitContinuum<T,D> &,
t_index &, A), A a_userarg)
{
	t_index index;
	rIterate(D-1, index, a_function, a_userarg);
}

template<typename T, unsigned int D>
template<typename A>
inline void UnitContinuum<T,D>::rIterate(unsigned int a_d, t_index &a_index,
		void (*a_function)(UnitContinuum<T,D> &, t_index &, A), A a_userarg)
{
	for(int i = 0; i < count()[a_d]; i++)
	{
		a_index[a_d] = i;
		if(a_d == 0)
		{
			a_function(*this, a_index, a_userarg);
		}
		else
		{
			rIterate(a_d-1, a_index, a_function, a_userarg);
		}
	}
}

template<typename T, unsigned int D>
inline void UnitContinuum<T,D>::index(t_index &a_index, const t_span &a_location) const
{
	for(unsigned int i = 0; i < D; i++)
	{
		a_index[i] = (int)(a_location[i] / m_size[i]);
	}
}

template<typename T, unsigned int D>
inline void UnitContinuum<T,D>::lower(t_index &a_index, const t_span &a_location) const
{
	for(unsigned int i = 0; i < D; i++)
	{
		a_index[i] = (int)((a_location[i] - m_halfsize[i]) / m_size[i]);
	}
}

template<typename T, unsigned int D>
inline unsigned int UnitContinuum<T,D>::calc_array_index(
		const t_index &a_index) const
{
	unsigned int index = 0;
	unsigned int step = 1;
	for(int d = D-1; d >= 0; d--)
	{
		index += a_index[d] * step;
		step *= m_count[d];
	}
	return index;
}

template<typename T, unsigned int D>
inline void UnitContinuum<T,D>::location(t_span &a_location, const t_index &a_index) const
{
	for(unsigned int i = 0; i < D; i++)
	{
		a_location[i] = (Real)a_index[i] * cellsize()[i] + m_halfsize[i];
	}
}

template<typename T, unsigned int D>
inline void UnitContinuum<T,D>::create(const t_index &a_count,
		const T &a_init)
{
	m_totalCount = 1;
	for(unsigned int i = 0; i < D; i++)
	{
		m_periodic[i] = false;
		m_count[i] = a_count[i];
		m_size[i] = 1.0/m_count[i];
		m_totalCount *= m_count[i];
		m_halfsize[i] = m_size[i]/2.0;
	}

	m_cells.resize(m_totalCount, a_init);
}

template<typename T, unsigned int D>
inline void UnitContinuum<T,D>::create(const t_index &a_count,
		const t_periodic &a_periodic, const T &a_init)
{
	m_totalCount = 1;
	for(unsigned int i = 0; i < D; i++)
	{
		m_periodic[i] = a_periodic[i];
		m_count[i] = a_count[i];
		m_size[i] = 1.0/m_count[i];
		m_totalCount *= m_count[i];
		m_halfsize[i] = m_size[i]/2.0;
	}

	m_cells.resize(m_totalCount, a_init);
}

template<typename T, unsigned int D>
inline const T &UnitContinuum<T,D>::operator[](const t_index &a_index) const
{
	t_index safe_index(a_index);
	safe(safe_index);
	return m_cells[calc_array_index(safe_index)];
}

template<typename T, unsigned int D>
inline T &UnitContinuum<T,D>::access(const t_index &a_index)
{
	return m_cells[calc_array_index(a_index)];
}

template<typename T, unsigned int D>
inline const T &UnitContinuum<T,D>::get(const t_index &a_index) const
{
	return m_cells[calc_array_index(a_index)];
}

template<typename T, unsigned int D>
inline void UnitContinuum<T,D>::set(const t_index &a_index, const T &a_value)
{
	t_index safe_index(a_index);
	safe(safe_index);
	m_cells[calc_array_index(safe_index)] = a_value;
}

template<typename T, unsigned int D>
inline void UnitContinuum<T,D>::set(const T &a_value)
{
	for(unsigned int i = 0; i < m_cells.size(); i++)
	{
		m_cells[i] = a_value;
	}
}

template<typename T, unsigned int D>
inline void UnitContinuum<T,D>::addNeighbors(std::list<t_index> &a_neighbors,
		const t_index &a_index) const
{
	t_index candidate = a_index;
	safe(candidate);
	if(!(candidate == a_index))
	{
		feX("UnitContinuum<T,D>::addNeighbors",
			"invalid reference index");
	}
	for(unsigned int i = 0; i < D; i++)
	{
		candidate[i] = a_index[i] + 1;
		safe(i, candidate);
		if(candidate[i] != a_index[i])
		{
			a_neighbors.push_back(candidate);
		}

		candidate[i] = a_index[i] - 1;
		safe(i, candidate);
		if(candidate[i] != a_index[i])
		{
			a_neighbors.push_back(candidate);
		}

		candidate[i] = a_index[i];
	}
}

// ============================================================================
// ============================================================================



/**	sample a UnitContinuum by returning the direct cell value 
	@relates Continuum */
template<typename T, unsigned int D>
const T &directSample(const UnitContinuum<T,D> &a_continuum, const typename UnitContinuum<T,D>::t_span &a_location)
{
	typename UnitContinuum<T,D>::t_index idx;
	a_continuum.index(idx, a_location);
	return a_continuum[idx];
}

struct t_bnd
{
	Real	m_lo;
	Real	m_hi;
};

template<typename T, unsigned int D>
const T &rLinearSample(unsigned int a_d, const UnitContinuum<T,3> &a_continuum, typename UnitContinuum<T,D>::t_index &a_index, typename UnitContinuum<T,D>::t_index &a_lower, float a_mult, t_bnd *a_bnd)
{
	Real accum = 0.0;
	Real m;
	a_index[a_d] = a_lower[a_d];
	a_continuum.safe(a_index);
	m = a_bnd[a_d].m_hi * a_mult;
	if(m > 0.0)
	{
		if(a_d == 0)
		{
			accum += m * a_continuum.access(a_index);
		}
		else
		{
			accum += rLinearSample<T,D>(a_d-1, a_continuum, a_index, a_lower,
				m, a_bnd);
		}
	}

	a_index[a_d] = a_lower[a_d] + 1;
	a_continuum.safe(a_index);
	m = a_bnd[a_d].m_lo * a_mult;
	if(m > 0.0)
	{
		if(a_d == 0)
		{
			accum += m * a_continuum.access(a_index);
		}
		else
		{
			accum += rLinearSample<T,D>(a_d-1, a_continuum, a_index, a_lower,
				m, a_bnd);
		}
	}

	return accum;
}

/**	sample a Continuum by returning the a linear interpolated value.
	@relates Continuum */
template<typename T, unsigned int D>
const T &linearSample(const UnitContinuum<T,D> &a_continuum, const typename UnitContinuum<T,D>::t_span &a_location)
{
	t_bnd bnd[D];

	typename UnitContinuum<T,D>::t_index lower;
	a_continuum.lower(lower, a_location);


	for(unsigned int i = 0; i < D; i++)
	{
		bnd[i].m_lo = a_location[i]/a_continuum.cellsize()[i]
			- (Real)lower[i] - 0.5;
		bnd[i].m_hi = 1.0 - bnd[i].m_lo;
	}

	Real r = 1.0;

	typename UnitContinuum<T,D>::t_index index;
	return rLinearSample<T,D>(D-1, a_continuum, index, lower, r, bnd);
}


/**	Calculate the gradient of a UnitContinuum at a point using midpoint
	differencing and linear interpolation.
	@relates Continuum */
template<unsigned int D>
void gradient(UnitContinuum<Real,D> &a_continuum, typename UnitContinuum<Real,D>::t_span &a_gradient, const typename UnitContinuum<Real,D>::t_span &a_location, Real a_h = 0.1)
{
	for(unsigned int i = 0; i < D; i++)
	{
		typename UnitContinuum<Real,D>::t_span lo(a_location);
		typename UnitContinuum<Real,D>::t_span hi(a_location);
		lo[i] -= a_h;
		hi[i] += a_h;
		a_continuum.safe(lo);
		a_continuum.safe(hi);
		Real lo_smpl = linearSample(a_continuum, lo);
		Real hi_smpl = linearSample(a_continuum, hi);
		a_gradient[i] = (hi_smpl - lo_smpl)/(2.0*a_h);
	}
}

template<typename T, unsigned int D>
struct t_unit_derive
{
	UnitContinuum<T,D>	*m_vectors;
};

template<typename T, unsigned int D>
void derive_fun(UnitContinuum<Real,D> &a_continuum, typename UnitContinuum<Real,D>::t_index &a_index, t_unit_derive<T,D> *a_data)
{
	for(unsigned int d = 0; d < D; d++)
	{
		typename UnitContinuum<Real,D>::t_index dn(a_index);
		dn[d] -= 1;
		typename UnitContinuum<Real,D>::t_index up(a_index);
		up[d] += 1;
		Real value = (a_continuum[up] - a_continuum[dn]) /
			(2.0*a_continuum.cellsize()[d]);
		setAt(a_data->m_vectors->access(a_index), d, value);
	}
}

/** Calculate a gradient vector field for a scalar field.
	@relates Continuum */
template<typename T, unsigned int D>
void derive(UnitContinuum<T,D> &a_vector, UnitContinuum<Real,D> &a_scalar)
{
	T dummy;
	a_vector.create(a_scalar.count(), a_scalar.periodic(), dummy);
	t_unit_derive<T,D> derive_data;
	derive_data.m_vectors = &a_vector;
	a_scalar.iterate(derive_fun<T,D>, &derive_data);
}


} /* namespace ext */
} /* namespace fe */

#endif /* __spatial_UnitContinuum_h__ */
