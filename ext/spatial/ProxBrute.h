/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __spatial_ProxBrute_h__
#define __spatial_ProxBrute_h__

#include "signal/signal.h"
#include "Proximity.h"

namespace fe
{
namespace ext
{

/** currently this brute force method is incomplete since it does not
	honor wrap around boundaries.

	@b particle attributes
	@li AsParticle::location
	@li AsBounded::radius

	@b pair attributes
	@li AsProximity::left
	@li AsProximity::right
	*/
class ProxBrute : virtual public ProxI
{
	public:
		ProxBrute(void);
virtual	~ProxBrute(void);

		// AS ProxI
virtual	unsigned int	detect(	sp<Layout>			l_pair,
								sp<RecordGroup>		rg_in,
								sp<RecordGroup>		rg_out);

	private:
		Array<BaseAccessor>		m_filters;
		AsBounded				m_asBounded;
		AsParticle				m_asParticle;
		AsProximity				m_asProximity;
};

} /* namespace ext */
} /* namespace fe */


#endif /* __spatial_ProxBrute_h__ */

