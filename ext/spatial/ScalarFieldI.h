/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __spatial_ScalarFieldI_h__
#define __spatial_ScalarFieldI_h__

#include "math/math.h"

namespace fe
{
namespace ext
{

/**	Scalar Field */
class FE_DL_EXPORT ScalarFieldI : virtual public Component
{
	public:
virtual	Real			sample(		const SpatialVector &a_location)		= 0;
virtual	void			gradient(	SpatialVector &a_gradient,
									const SpatialVector &a_location)		= 0;
virtual	Real			volume(		void)									= 0;
virtual	void			set(		Real a_value)							= 0;
virtual	void			set(		const SpatialVector &a_location,
									Real a_value)							= 0;
virtual	void			add(		const SpatialVector &a_location,
									Real a_value)							= 0;
virtual	void			radialSet(	const SpatialVector &a_location,
									const SpatialVector &a_radius,
									Real a_value)							= 0;
virtual	sp<SpaceI>		space(		void)									= 0;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __spatial_ScalarFieldI_h__ */

