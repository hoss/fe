/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __spatial_ProxHash_h__
#define __spatial_ProxHash_h__

#include "signal/signal.h"
#include "Proximity.h"

#if FE_MT
#define FE_PROXHASH_THREADED 1
#else
#define FE_PROXHASH_THREADED 0
#endif

#if FE_PROXHASH_THREADED
//#include "tbb/tbb.h"
#include "tbb/concurrent_vector.h"
#include "tbb/blocked_range.h"
#include "tbb/spin_mutex.h"
#include "tbb/parallel_for.h"
//#include "omp.h"
#endif


namespace fe
{
namespace ext
{

// TODO: AJW: clean up the debris here
class ProxHash :
	public Config,
	virtual public ProxI,
	public Initialize<ProxHash>
{
	public:
						ProxHash(void);
virtual					~ProxHash(void);
		void			initialize(void);

		// AS ProxI
virtual	unsigned int	detect(	sp<Layout>			l_pair,
								sp<RecordGroup>		rg_in,
								sp<RecordGroup>		rg_out);

	private:
		class LocalRecord
		{
			public:
				LocalRecord(void) : m_mark(false)
				{
				}
				unsigned int		m_index;
				RecordArray			*m_pRA;
				SpatialVector		m_location;
				Real				m_radius;
				bool				m_mark;
				//std::set<void *> m_candidates;
		};
		typedef Array<LocalRecord>	t_lrecords;

		void			setup(sp<Scope> spScope);
		//bool			check(LocalRecord &r_a, LocalRecord &r_b);
		unsigned int	hash(int a_x, int a_y, int a_z);
public:
		void			checkSingle(unsigned int i, Array<unsigned int> &to_check, std::vector<bool> &mark, Array< std::pair<unsigned int, unsigned int> > &a_hits);
		void accumulateHits(Array< std::pair<unsigned int, unsigned int> > &a_hits);
		unsigned int	sz() { return m_store.size(); }
		void transferHitsToPool(Array< std::pair<unsigned int, unsigned int> > &a_hits, FE_UWORD i_pool);
		FE_UWORD getNextPoolIndex(void);
private:
		bool check(unsigned int a_a, unsigned int a_b, Array< std::pair<unsigned int, unsigned int> > &a_hits);
		void transferHitsToPool();

		sp<Scope>								m_spScope;
		AsBounded								m_asBounded;
		AsParticle								m_asParticle;
		AsProximity									m_asProximity;

		Array<BaseAccessor>				m_filters;

		unsigned int							m_poolCount;
		//sp<RecordArray>			m_spPool;
		//FE_UWORD					m_poolIndex;
		Array< sp<RecordArray> >			m_spPool;
		Array< FE_UWORD >					m_poolIndex;
		Array< Array< std::pair<unsigned int, unsigned int> > > m_hits;

#if FE_PROXHASH_THREADED
		tbb::concurrent_vector< std::pair<unsigned int, unsigned int> > m_chits;
#else
		Array< std::pair<unsigned int, unsigned int> > m_chits;
#endif

		Array< Array<unsigned int> >	m_cells;
		Array<LocalRecord>				m_store;

		hp<Layout>								m_hpPairLayout;

		Real									m_radius;
		unsigned int							m_tableSize;

		Array< std::pair<unsigned int, unsigned int> > m_checklist;

#if FE_PROXHASH_THREADED
		tbb::spin_mutex							m_mutex;
#endif

};

} /* namespace ext */
} /* namespace fe */


#endif /* __spatial_ProxHash_h__ */

