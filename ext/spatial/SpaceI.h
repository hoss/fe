/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __spatial_SpaceI_h__
#define __spatial_SpaceI_h__

#include "fe/plugin.h"
#include "math/math.h"

namespace fe
{
namespace ext
{

class FE_DL_EXPORT SpaceI : virtual public Component
{
	public:
virtual	void				to(		SpatialVector &a_out,
									const SpatialVector &a_in)				= 0;
virtual	void				from(	SpatialVector &a_out,
									const SpatialVector &a_in)				= 0;
};


} /* namespace ext */
} /* namespace fe */


#endif /* __spatial_SpaceI_h__ */

