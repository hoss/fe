/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __spatial_Proximity_h__
#define __spatial_Proximity_h__

#include "signal/signal.h"
#include "datatool/datatool.h"
#include "math/math.h"
#include "shape/shape.h"

namespace fe
{
namespace ext
{

/**	proximity detection interface */
class FE_DL_EXPORT ProxI : virtual public Component
{
	public:
virtual	unsigned int	detect(	sp<Layout>			l_pair,
								sp<RecordGroup>		rg_in,
								sp<RecordGroup>		rg_out)					= 0;
};

/**	Handler to detect collisions between bounded particles.
	Particles in the input group are checked for collisions, and a pair
	record (registered with setPair) is created for each collision and
	added to the output group.

	@copydoc Proximity_info
	*/
class Proximity :
	virtual public HandlerI,
	virtual public Config,
	public Initialize<Proximity>
{
	public:
				Proximity(void);
virtual			~Proximity(void);

		void	initialize(void);

				// AS HandlerI
virtual	void	handleBind(sp<SignalerI> spSignaler,
					sp<Layout> l_sig);
virtual	void	handle(Record &r_sig);

	private:
		sp<Scope>						m_spScope;

		sp<ProxI>						m_spProx;

};


} /* namespace ext */
} /* namespace fe */

#endif /* __spatial_Proximity_h__ */

