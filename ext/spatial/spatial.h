/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __ext_spatial_h__
#define __ext_spatial_h__

#include "signal/signal.h"
#include "datatool/datatool.h"
#include "math/math.h"

#include "spatial/Continuum.h"
#include "spatial/UnitContinuum.h"
#include "spatial/VectorFieldI.h"
#include "spatial/GridScalarField.h"
#include "spatial/GridVectorField.h"
#include "spatial/Proximity.h"
#include "spatial/BroadPhaseI.h"

namespace fe
{
namespace ext
{

Library* CreateSpatialLibrary(sp<Master>);

} /* namespace ext */
} /* namespace fe */

#endif /* __ext_spatial_h__ */

