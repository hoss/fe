/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __spatial_ProxMultiGrid_h__
#define __spatial_ProxMultiGrid_h__

#include "signal/signal.h"
#include "Proximity.h"
#include "SpaceI.h"

namespace fe
{
namespace ext
{

/** Proximity detection.

	The method used partitions space into grids of cells.  A separate
	grid is used for each specified cell size.  Cells are cubic; they are
	the same length in each dimension.

	WARNING: this class is by design not reentrant due to the pair pooling.

	Note: this class is also a good guide on getting the most speed possible
	out of the data system.

	@b particle attributes
	@li AsParticle::location
	@li AsBounded::radius

	@b pair attributes
	@li AsProximity::left
	@li AsProximity::right
	*/
class FE_DL_EXPORT ProxMultiGrid :
	virtual public ProxI,
	public Initialize<ProxMultiGrid>
{
	typedef Vector<3, int>	t_v3i;
	public:
						ProxMultiGrid(void);
virtual					~ProxMultiGrid(void);
		void			initialize(void);

		// AS ProxI
virtual	unsigned int	detect(	sp<Layout>			l_pair,
								sp<RecordGroup>		rg_in,
								sp<RecordGroup>		rg_out);


virtual	void			addGrid(Real cellSize);

	private:
		class LocalRecord
		{
			public:
				unsigned int		m_index;
				RecordArray			*m_pRA;
				SpatialVector		m_location;
				Real				m_radius;
		};
// apparently MS decided to stop supporting allocators properly
#if FE_COMPILER==FE_MICROSOFT
		typedef std::vector<LocalRecord> t_lrecords;
#else
		typedef std::vector<LocalRecord, StdAllocator<LocalRecord> > t_lrecords;
#endif
		class Grid
		{
			public:
							Grid(void);
				unsigned int	flatten(const t_v3i &a_idx)
				{
					return	a_idx[0] * m_count[1] * m_count[2] +
							a_idx[1] * m_count[2] +
							a_idx[2];
				}
				unsigned int	detect(void);
				std::vector<t_lrecords>		m_grid;
				Real						m_sz;
				t_v3i						m_count;
				ProxMultiGrid				*m_pMGrid;
		};

		void			add(sp<RecordArray> &ra_in, unsigned int index);
		unsigned int	detect(t_lrecords &a_a);
		unsigned int	detect(t_lrecords &a_a, t_lrecords &a_b);
		unsigned int	detect(Grid &a_ga, Grid &a_gb);
		bool			check(LocalRecord &r_a, LocalRecord &r_b);
		void			setup(sp<Scope> spScope);
		void			setup(Grid &a_grid, SpatialVector &a_extent);

		void			log(void);

	private:
		bool			rangeCheck(LocalRecord &r_a);
		sp<Scope>								m_spScope;
		AsBounded								m_asBounded;
		AsParticle								m_asParticle;
		AsProximity								m_asProximity;

		Array<Grid>								m_grids;
		SpatialVector							m_origin;
		sp<SpaceI>								m_spSpace;
		SpatialVector							m_boundary;

		Array<BaseAccessor>						m_filters;

		sp<RecordArray>							m_spPool;
		FE_UWORD								m_poolIndex;

		hp<Layout>								m_hpPairLayout;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __spatial_ProxMultiGrid_h__ */

