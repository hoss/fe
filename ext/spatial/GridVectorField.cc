/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "spatial/spatial.h"

namespace fe
{
namespace ext
{

GridVectorField::GridVectorField(void)
{
}

GridVectorField::~GridVectorField(void)
{
}

void GridVectorField::initialize(void)
{
}

#if 1
GridVectorField::t_continuum &GridVectorField::continuum(void)
{
	return m_continuum;
}
#endif

bool GridVectorField::sample(SpatialVector &a_result, const SpatialVector &a_location)
{
	a_result = directSample<SpatialVector>(m_continuum, a_location);
	return m_continuum.isSafe(a_location);
}

void GridVectorField::create(const sp<SpaceI> &a_space, const Vector3i &a_count)
{
	SpatialVector init(0.0f, 0.0f, 0.0f);
	m_continuum.create(a_count, init, a_space);
}

} /* namespace ext */
} /* namespace fe */

