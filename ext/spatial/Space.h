/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __spatial_Space_h__
#define __spatial_Space_h__

#include "SpaceI.h"
#include "signal/signal.h"

namespace fe
{
namespace ext
{

class FE_DL_EXPORT AffineSpace :
	virtual public SpaceI,
	public Initialize<AffineSpace>
{
	public:
				AffineSpace(void);
virtual			~AffineSpace(void);

		void	initialize(void);

		// AS SpaceI
virtual	void	to(		SpatialVector &a_unit,
						const SpatialVector &a_world);
virtual	void	from(	SpatialVector &a_world,
						const SpatialVector &a_unit);

virtual	void	setTransform(
						const SpatialTransform &a_transform);

virtual	void	setAxisAligned(
						const SpatialVector &a_origin,
						const SpatialVector &a_extent);


	private:
		SpatialTransform			m_forward;
		SpatialTransform			m_inverse;
};

} /* namespace ext */
} /* namespace fe */


#endif /* __spatial_Space_h__ */

