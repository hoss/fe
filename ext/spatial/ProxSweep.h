/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __spatial_ProxSweep_h__
#define __spatial_ProxSweep_h__

#include "signal/signal.h"
#include "Proximity.h"

namespace fe
{
namespace ext
{

/**	Proximity detection.

	Sweep sort in one dimension.

	Currently does not support wrap around borders.

	@b particle attributes
	@li AsParticle::location
	@li AsBounded::radius

	@b pair attributes
	@li AsProximity::left
	@li AsProximity::right
	*/
class ProxSweep :
	virtual public ProxI,
	public Initialize<ProxSweep>
{
	public:
						ProxSweep(void);
virtual					~ProxSweep(void);
		void			initialize(void);

		// AS ProxI
virtual	unsigned int	detect(	sp<Layout>			l_pair,
								sp<RecordGroup>		rg_in,
								sp<RecordGroup>		rg_out);


		void			setAxis(unsigned int a_d);

	private:
		class LocalRecord
		{
			public:
				unsigned int		m_index;
				RecordArray			*m_pRA;
				SpatialVector		m_location;
				Real				m_radius;
				Real				m_hi;
		};
		typedef std::vector<LocalRecord>	t_lrecords;
		class MinVal
		{
			public:
				float		m_key;
				int			m_index;
				bool		operator<(const MinVal &a_other) const
				{
					return (m_key < a_other.m_key);
				}
		};
		typedef std::vector<MinVal>	t_minvals;

		void			setup(sp<Scope> spScope);
		bool			check(LocalRecord &r_a, LocalRecord &r_b);

		sp<Scope>								m_spScope;
		AsBounded								m_asBounded;
		AsParticle								m_asParticle;
		AsProximity								m_asProximity;

		Array<BaseAccessor>						m_filters;

		sp<RecordArray>							m_spPool;
		FE_UWORD								m_poolIndex;

		hp<Layout>								m_hpPairLayout;

		unsigned int							m_sweepAxis;
};


} /* namespace ext */
} /* namespace fe */

#endif /* __spatial_ProxSweep_h__ */

