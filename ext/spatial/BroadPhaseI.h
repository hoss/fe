/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __spatial_BroadPhaseI_h__
#define __spatial_BroadPhaseI_h__

namespace fe
{
namespace ext
{

class FE_DL_EXPORT BroadPhaseI : virtual public Component
{
	public:
virtual	void	detect(sp<RecordGroup> rg_output)							= 0;

				/** Must attach so that broadphase can leverage temporal coherence,
					(handle add() and remove() calls as a WatcherI */
virtual	void	attach(sp<RecordGroup> rg_input)							= 0;

};

} /* namespace ext */
} /* namespace fe */


#endif /* __spatial_BroadPhaseI_h__ */

