/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __spatial_Continuum_h__
#define __spatial_Continuum_h__

#include "SpaceI.h"
#include "UnitContinuum.h"

namespace fe
{
namespace ext
{

template<typename T>
class Continuum
{
	public:
		Continuum(void)
		{
		}
virtual	~Continuum(void)
		{
		}

		Continuum<T> &operator=(const Continuum<T> &a_other)
		{
			m_unitContinuum = a_other.m_unitContinuum;
			m_space = a_other.m_space;
			return *this;
		}

		bool		operator==(const Continuum<T> &a_other)
		{
			if(m_unitContinuum != a_other.m_unitContinuum)
			{
				return false;
			}
			if(m_space != a_other.m_space)
			{
				return false;
			}
			return true;
		}

		typedef typename UnitContinuum<T,3>::t_index		t_index;
		typedef typename UnitContinuum<T,3>::t_cells		t_cells;
		typedef SpaceI										t_space;

		void				create(	const t_index &a_count,
									const T &a_init,
									const sp<t_space> &a_space);


		// unsafe location to index methods (indices may be out of range)
		void				index(		t_index &a_index,
										const SpatialVector &a_location) const;
		void				lower(		t_index &a_index,
										const SpatialVector &a_location) const;

		// index to location methods
		void				location(	SpatialVector &a_location,
										const t_index &a_index) const;

		// out-of-range safety
		SpatialVector		&safe(		SpatialVector &a_location) const;
		bool				isSafe(		const SpatialVector &a_location) const;

		// unit continuum access
		const UnitContinuum<T,3>	&unit(void) const;
		UnitContinuum<T,3>			&accessUnit(void);

		sp<t_space>					&space(void) { return m_space; }

		void				cellsize(SpatialVector &a_cellsize);


	private:
		UnitContinuum<T,3>			m_unitContinuum;
		sp<t_space>					m_space;
};

template<typename T>
inline void Continuum<T>::create(const t_index &a_count, const T &a_init,
		const sp<t_space> &a_space)
{
	m_unitContinuum.create(a_count, a_init);
	m_space = a_space;
}


template<typename T>
inline void Continuum<T>::index(t_index &a_index,
		const SpatialVector &a_location) const
{
	SpatialVector unit_location;
	m_space->from(unit_location, a_location);
	typename UnitContinuum<T,3>::t_span location;
	location = unit_location;
	m_unitContinuum.index(a_index, location);
}

template<typename T>
inline void Continuum<T>::lower(t_index &a_index,
		const SpatialVector &a_location) const
{
	SpatialVector unit_location;
	m_space->from(unit_location, a_location);
	m_unitContinuum.lower(a_index, unit_location);
}

template<typename T>
inline void Continuum<T>::location(SpatialVector &a_location,
		const t_index &a_index) const
{
	typename UnitContinuum<T,3>::t_span unit_location;
	m_unitContinuum.location(unit_location, a_index);
	SpatialVector location = unit_location;
	m_space->to(a_location, location);
}

template<typename T>
inline SpatialVector &Continuum<T>::safe(SpatialVector &a_span) const
{
	SpatialVector unit_span;
	m_space->from(unit_span, a_span);
	typename UnitContinuum<T,3>::t_span uspan;
	uspan = unit_span;
	m_unitContinuum.safe(uspan);
	unit_span = uspan;
	m_space->to(a_span, unit_span);
	return a_span;
}

template<typename T>
inline bool Continuum<T>::isSafe(const SpatialVector &a_span) const
{
	SpatialVector unit_span;
	m_space->from(unit_span, a_span);
	typename UnitContinuum<T,3>::t_index index, safe_index;
	typename UnitContinuum<T,3>::t_span uspan;
	uspan = unit_span;
	m_unitContinuum.index(index, uspan);
	safe_index = index;
	m_unitContinuum.safe(safe_index);
	return (index == safe_index);
}

template<typename T>
inline const UnitContinuum<T,3> &Continuum<T>::unit(void) const
{
	return m_unitContinuum;
}

template<typename T>
inline UnitContinuum<T,3> &Continuum<T>::accessUnit(void)
{
	return m_unitContinuum;
}

template<typename T>
inline void Continuum<T>::cellsize(SpatialVector &a_cellsize)
{
	typename UnitContinuum<T,3>::t_span unitsz = unit().cellsize();
	SpatialVector origin(0.0,0.0,0.0);
	SpatialVector sz;
	sz = unitsz;
	m_space->to(origin, origin);
	m_space->to(a_cellsize, sz);
	a_cellsize -= origin;
}

// ============================================================================
// ============================================================================


/**	sample a Continuum by returning the direct cell value 
	@relates Continuum */
template<typename T>
const T &directSample(const Continuum<T> &a_continuum,
		const SpatialVector &a_location)
{
	typename Continuum<T>::t_index idx;
	a_continuum.index(idx, a_location);
	return a_continuum.unit()[idx];
}


/**	Calculate the gradient of a UnitContinuum at a point using midpoint
	differencing and linear interpolation.
	@relates Continuum */
void gradient(Continuum<Real> &a_continuum, SpatialVector &a_gradient,
		const SpatialVector &a_location, Real a_h = 0.1);

struct t_n_radial_set
{
	SpatialVector						*m_location;
	SpatialVector						*m_radius;
	Continuum<Real>						*m_continuum;
	Real								m_value;
};

void rad_func(UnitContinuum<Real,3> &a_unit,
		UnitContinuum<Real,3>::t_index &a_index, t_n_radial_set *a_data);

/** Radial set function.
	@relates Continuum */
FE_DL_EXPORT void radialSet(Continuum<Real> &a_continuum,
		const SpatialVector &a_location,
		const SpatialVector &a_radius, Real a_value);

/** Calculate a gradient vector field for a scalar field.
	@relates Continuum */
template<typename T>
void derive(Continuum<T> &a_vector, Continuum<Real> &a_scalar)
{
	derive<T,3>(a_vector.accessUnit(), a_scalar.accessUnit());
	a_vector.space() = a_scalar.space();
}


// ============================================================================
// old implementation.  left here for a while for easy reference
// ============================================================================
#if 0

/**	N-Dimensional Spatial Continuum using a evenly spaced grid descretization
	*/
template<typename T, unsigned int D>
class Continuum
{
	public:
		Continuum(void)
		{
		}
virtual	~Continuum(void)
		{
		}

		Continuum<T,D> &operator=(const Continuum<T,D> &other)
		{
			m_origin = other.m_origin;
			m_count = other.m_count;
			for(unsigned int i = 0; i < D; i++)
			{
				m_boundary[i] = other.m_boundary[i];
				m_size[i] = other.m_size[i];
			}
			m_cells = other.m_cells;
			m_totalCount = other.m_totalCount;
			return *this;
		}

		bool		operator==(const Continuum<T,D> &other)
		{
			if(m_origin != other.m_origin) { return false; }
			if(m_count != other.m_count) { return false; }
			if(m_boundary != other.m_boundary) { return false; }
			if(m_size != other.m_size) { return false; }
			if(m_totalCount != other.m_totalCount) { return false; }
			if(m_cells == other.m_cells) { return true; }
			return false;
		}

		template <typename P>
		void			copyTopology(const Continuum<P,D> &a_other,
								const T &a_init)
		{
			create(a_other.m_count, a_other.m_boundary, a_init);
		}

		typedef	Vector<D,int>			t_index;
		typedef Vector<D,Real>			t_span;
		typedef std::vector<T>			t_cells;

		void			create(	const t_index &a_count,
								const t_span &a_boundary,
								const T &a_init);

		void			create(	const t_span &a_origin,
								const t_index &a_count,
								const t_span &a_boundary,
								const T &a_init);

		const T			&operator[](const t_index &a_index) const;

		void			set(const t_index &a_index, const T &value);
		void			set(const T &value);
		T				&access(const t_index &a_index);

		void			direct(		t_index &a_direct,
									const t_span &a_location) const;
		void			closest(	t_index &a_closest,
									const t_span &a_location) const;
		void			space(		t_span &a_space,
									const t_index &a_index) const;
		const t_span	&cellsize(void) const { return m_size; }
		const t_index	&count(void) const { return m_count; }
		const t_span	&origin(void) const { return m_origin; }
		const t_span	&boundary(void) const { return m_boundary; }
		unsigned int	totalCount(void) const { return m_totalCount; }
		//t_span			locate(const t_index &a_index) const;

		template<typename A>
		void			iterate(	void (*a_function)(Continuum<T,D> &,
										t_index &,
										A),
									A a_userarg);

		void			addNeighbors(
									std::list<t_index> &a_neighbors,
									const t_index &a_index) const;
		void			addNeighborsDiagonal(
									std::list<t_index> &a_neighbors,
									const t_index &a_index) const;
		t_index			&safe(		t_index &a_index) const;
		t_span			&safe(		t_span &a_span) const;
	private:
		bool			safe(		unsigned int a_i,
									t_index &a_index) const;
		void			addNeighborsD(unsigned int a_d,
									std::list<t_index> &a_neighbors,
									const t_index &a_index,
									const t_index &a_base) const;
		unsigned int	calc_array_index(	const t_index &a_index) const;
		template<typename A>
		void			rIterate(	unsigned int a_d,
									t_index &a_index,
									void (*a_function)(Continuum<T,D> &,
										t_index &,
										A),
									A a_userarg);

	private:
		t_span			m_origin;
		t_index			m_count;
		t_span			m_boundary;
		t_span			m_size;
		t_cells			m_cells;
		unsigned int	m_totalCount;
};

template<typename T, unsigned int D>
inline bool Continuum<T,D>::safe(unsigned int a_i, t_index &a_index) const
{
	bool rv = true;
	if(m_count[a_i] == 1)
	{
		if(a_index[a_i] != 0)
		{
			if(m_boundary[a_i] >= 0.0)
			{
				rv = false;
			}
			a_index[a_i] = 0;
		}
	}
	else if(m_count[a_i] == 2)
	{
		if(a_index[a_i] < 0)
		{
			if(m_boundary[a_i] >= 0.0)
			{
				rv = false;
			}
			a_index[a_i] = 0;
		}
		else if(a_index[a_i] > 1)
		{
			if(m_boundary[a_i] >= 0.0)
			{
				rv = false;
			}
			a_index[a_i] = 1;
		}
	}
	else
	{
		while(a_index[a_i] < 0)
		{
			if(m_boundary[a_i] < 0.0)
			{
				a_index[a_i] += m_count[a_i] - 1;
			}
			else
			{
				rv = false;
				a_index[a_i] = 0;
			}
		}
		while(a_index[a_i] >= m_count[a_i])
		{
			if(m_boundary[a_i] < 0.0)
			{
				a_index[a_i] -= m_count[a_i] - 1;
			}
			else
			{
				rv = false;
				a_index[a_i] = m_count[a_i]-1;
			}
		}
	}

	return rv;
}

template<typename T, unsigned int D>
inline typename Continuum<T,D>::t_index &Continuum<T,D>::safe(t_index &a_index) const
{
	for(unsigned int i = 0; i < D; i++)
	{
		if(m_count[i] == 1)
		{
			a_index[i] = 0;
		}
		else if(m_count[i] == 2)
		{
			if(a_index[i] < 0)
			{
				a_index[i] = 0;
			}
			else if(a_index[i] > 1)
			{
				a_index[i] = 1;
			}
		}
		else
		{
			while(a_index[i] < 0)
			{
				if(m_boundary[i] < 0.0)
				{
					a_index[i] += m_count[i] - 1;
				}
				else
				{
					a_index[i] = 0;
				}
			}
			while(a_index[i] >= m_count[i])
			{
				if(m_boundary[i] < 0.0)
				{
					a_index[i] -= m_count[i] - 1;
				}
				else
				{
					a_index[i] = m_count[i]-1;
				}
			}
		}
	}

	return a_index;
}

template<typename T, unsigned int D>
inline typename Continuum<T,D>::t_span &Continuum<T,D>::safe(
		t_span &a_span) const
{
	a_span -= m_origin;
	for(unsigned int i = 0; i < D; i++)
	{
		if(m_count[i] == 1)
		{
			a_span[i] = 0.0;
		}
		else
		{
			Real bnd = fabs(m_boundary[i]);
			while(a_span[i] < 0.0)
			{
				if(m_boundary[i] < 0.0)
				{
					a_span[i] += bnd;
				}
				else
				{
					a_span[i] = 0.0;
				}
			}
			while(a_span[i] > bnd)
			{
				if(m_boundary[i] < 0.0)
				{
					a_span[i] -= bnd;
				}
				else
				{
					a_span[i] = bnd;
				}
			}
		}
	}
	a_span += m_origin;

	return a_span;
}

template<typename T, unsigned int D>
template<typename A>
inline void Continuum<T,D>::iterate(void (*a_function)(Continuum<T,D> &,
		t_index &, A), A a_userarg)
{
	t_index index;
	rIterate(D-1, index, a_function, a_userarg);
}

template<typename T, unsigned int D>
template<typename A>
inline void Continuum<T,D>::rIterate(unsigned int a_d, t_index &a_index,
		void (*a_function)(Continuum<T,D> &, t_index &, A), A a_userarg)
{
	for(int i = 0; i < count()[a_d]; i++)
	{
		a_index[a_d] = i;
		if(a_d == 0)
		{
			a_function(*this, a_index, a_userarg);
		}
		else
		{
			rIterate(a_d-1, a_index, a_function, a_userarg);
		}
	}
}


template<typename T, unsigned int D>
inline void Continuum<T,D>::direct(t_index &a_index,
		const t_span &a_location) const
{
	t_span location = a_location - m_origin;
	for(int i = D-1; i >= 0; i--)
	{
		if(m_size[i] > 0.0)
		{
			a_index[i] = (int)(location[i] / m_size[i]);
		}
		else
		{
			a_index[i] = 0;
			continue;
		}
		if(a_index[i] >= m_count[i] - 1)
		{
			a_index[i] = m_count[i] - 2;
		}
		if(a_index[i] < 0)
		{
			a_index[i] = 0;
		}
	}
}

template<typename T, unsigned int D>
inline void Continuum<T,D>::closest(t_index &a_index,
		const t_span &a_location) const
{
	t_span location = a_location - m_origin;
	for(int i = D-1; i >= 0; i--)
	{
		if(m_size[i] > 0.0)
		{
			a_index[i] = (int)((location[i]+(0.5*m_size[i])) / m_size[i]);
		}
		else
		{
			a_index[i] = 0;
			continue;
		}
		if(a_index[i] >= m_count[i])
		{
			a_index[i] = m_count[i] - 1;
		}
		if(a_index[i] < 0)
		{
			a_index[i] = 0;
		}
	}
}

template<typename T, unsigned int D>
inline unsigned int Continuum<T,D>::calc_array_index(
		const t_index &a_index) const
{
	unsigned int index = 0;
	unsigned int step = 1;
	for(int d = D-1; d >= 0; d--)
	{
		index += a_index[d] * step;
		step *= m_count[d];
	}
	return index;
}


template<typename T, unsigned int D>
inline void Continuum<T,D>::space(t_span &a_space, const t_index &a_index) const
{
	for(unsigned int i = 0; i < D; i++)
	{
		a_space[i] = (Real)a_index[i] * cellsize()[i] + m_origin[i];
	}
}

template<typename T, unsigned int D>
inline void Continuum<T,D>::create(const t_index &a_count,
		const t_span &a_boundary, const T &a_init)
{
	m_totalCount = 1;
	for(unsigned int i = 0; i < D; i++)
	{
		m_boundary[i] = a_boundary[i];
		m_count[i] = a_count[i];
		Real bnd = fabs(m_boundary[i]);
		if(m_count[i] == 1)
		{
			m_size[i] = 0.0;
		}
		else
		{
			m_size[i] = bnd / (m_count[i] - 1);
		}
		m_totalCount *= m_count[i];
	}

	m_cells.resize(m_totalCount, a_init);
}

template<typename T, unsigned int D>
inline void Continuum<T,D>::create(const t_span &a_origin,
		const t_index &a_count,
		const t_span &a_boundary, const T &a_init)
{
	m_origin = a_origin;
	create(a_count, a_boundary, a_init);
}


template<typename T, unsigned int D>
inline const T &Continuum<T,D>::operator[](const t_index &a_index) const
{
	return m_cells[calc_array_index(a_index)];
}

template<typename T, unsigned int D>
inline T &Continuum<T,D>::access(const t_index &a_index)
{
	return m_cells[calc_array_index(a_index)];
}

template<typename T, unsigned int D>
inline void Continuum<T,D>::set(const t_index &a_index, const T &a_value)
{
	m_cells[calc_array_index(a_index)] = a_value;
}

template<typename T, unsigned int D>
inline void Continuum<T,D>::set(const T &a_value)
{
	for(unsigned int i = 0; i < m_cells.size(); i++)
	{
		m_cells[i] = a_value;
	}
}

template<typename T, unsigned int D>
inline void Continuum<T,D>::addNeighborsD(unsigned int a_d,
		std::list<t_index> &a_neighbors, const t_index &a_index,
		const t_index &a_base) const
{
	t_index candidate = a_index;
	if(a_d == 0)
	{
		candidate[a_d] = a_index[a_d] + 1;
		if(safe(a_d, candidate))
		{
			if(!(candidate == a_base))
			{
				a_neighbors.push_back(candidate);
			}
		}

		candidate[a_d] = a_index[a_d] - 1;
		if(safe(a_d, candidate))
		{
			if(!(candidate == a_base))
			{
				a_neighbors.push_back(candidate);
			}
		}

		candidate[a_d] = a_index[a_d];
		if(safe(a_d, candidate))
		{
			if(!(candidate == a_base))
			{
				a_neighbors.push_back(candidate);
			}
		}
	}
	else
	{
		candidate[a_d] = a_index[a_d] + 1;
		safe(a_d, candidate);
		if(candidate[a_d] != a_index[a_d])
		{
			addNeighborsD(a_d-1, a_neighbors, candidate, a_base);
		}

		candidate[a_d] = a_index[a_d] - 1;
		safe(a_d, candidate);
		if(candidate[a_d] != a_index[a_d])
		{
			addNeighborsD(a_d-1, a_neighbors, candidate, a_base);
		}

		candidate[a_d] = a_index[a_d];
		safe(a_d, candidate);
		addNeighborsD(a_d-1, a_neighbors, candidate, a_base);


	}
}

template<typename T, unsigned int D>
inline void Continuum<T,D>::addNeighborsDiagonal(
		std::list<t_index> &a_neighbors, const t_index &a_index) const
{
	t_index candidate = a_index;
	safe(candidate);
	if(!(candidate == a_index))
	{
		feX("Continuum<T,D>::addNeighbors",
			"invalid reference index");
	}
	addNeighborsD(D-1, a_neighbors, candidate, a_index);
}

template<typename T, unsigned int D>
inline void Continuum<T,D>::addNeighbors(std::list<t_index> &a_neighbors,
		const t_index &a_index) const
{
	t_index candidate = a_index;
	safe(candidate);
	if(!(candidate == a_index))
	{
		feX("Continuum<T,D>::addNeighbors",
			"invalid reference index");
	}
	for(unsigned int i = 0; i < D; i++)
	{
		candidate[i] = a_index[i] + 1;
		safe(i, candidate);
		if(candidate[i] != a_index[i])
		{
			a_neighbors.push_back(candidate);
		}

		candidate[i] = a_index[i] - 1;
		safe(i, candidate);
		if(candidate[i] != a_index[i])
		{
			a_neighbors.push_back(candidate);
		}

		candidate[i] = a_index[i];
	}
}

#if 0
template<typename T, unsigned int D>
inline typename Continuum<T,D>::t_span Continuum<T,D>::locate(
		const t_index &a_index) const
{
	typename Continuum<T,D>::t_span location;
	space(location, a_index);
	return location;
}
#endif



// ============================================================================
// ============================================================================



/**	sample a Continuum by returning the value at the nearest grid point with all
	dimension values lower than the sample point (basicly, the cell origin)
	@relates Continuum */
template<typename T, unsigned int D>
const T &directSample(const Continuum<T,D> &a_continuum,
		const typename Continuum<T,D>::t_span &a_location)
{
	typename Continuum<T,D>::t_index direct;
	a_continuum.direct(direct, a_location);
	return a_continuum[direct];
}

/**	sample a Continuum by returning the value at the nearest grid point.
	@relates Continuum */
template<typename T, unsigned int D>
const T &closestSample(const Continuum<T,D> &a_continuum,
		const typename Continuum<T,D>::t_span &a_location)
{
	typename Continuum<T,D>::t_index closest;
	a_continuum.closest(closest, a_location);
	a_continuum[closest];
	return a_continuum[closest];
}

struct d_bnd
{
	Real	m_lo;
	Real	m_hi;
};

template<unsigned int D>
Real rLinearSample(unsigned int a_d, const Continuum<Real,3> &a_continuum,
		typename Continuum<Real,D>::t_index &a_index,
		typename Continuum<Real,D>::t_index &a_direct, float a_mult,
		d_bnd *a_bnd)
{
	Real accum = 0.0;
	Real m;
	a_index[a_d] = a_direct[a_d];
	a_continuum.safe(a_index);
	m = a_bnd[a_d].m_hi * a_mult;
	if(m > 0.0)
	{
		if(a_d == 0)
		{
			accum += m * a_continuum[a_index];
		}
		else
		{
			accum += rLinearSample<D>(a_d-1, a_continuum, a_index, a_direct,
				m, a_bnd);
		}
	}

	a_index[a_d] = a_direct[a_d] + 1;
	a_continuum.safe(a_index);
	m = a_bnd[a_d].m_lo * a_mult;
	if(m > 0.0)
	{
		if(a_d == 0)
		{
			accum += m * a_continuum[a_index];
		}
		else
		{
			accum += rLinearSample<D>(a_d-1, a_continuum, a_index, a_direct,
				m, a_bnd);
		}
	}

	return accum;
}



/**	sample a Continuum by returning the a linear interpolated value.
	@relates Continuum */
template<unsigned int D>
Real linearSample(const Continuum<Real,D> &a_continuum,
		const typename Continuum<Real,D>::t_span &a_location)
{
	d_bnd bnd[D];

	typename Continuum<Real,D>::t_index direct;
	a_continuum.direct(direct, a_location);

	typename Continuum<Real,D>::t_span location =
			a_location - a_continuum.origin();

	for(unsigned int i = 0; i < D; i++)
	{
		if(a_continuum.cellsize()[i] == 0.0)
		{
			bnd[i].m_lo = 0.0;
			bnd[i].m_hi = 1.0;
		}
		else
		{
			bnd[i].m_lo = location[i]/a_continuum.cellsize()[i] -
							(Real)direct[i];

			bnd[i].m_hi = 1.0 - bnd[i].m_lo;
		}
	}

	Real r = 1.0;

	typename Continuum<Real,D>::t_index index;
	return rLinearSample<D>(D-1, a_continuum, index, direct, r, bnd);
}


template<unsigned int D>
Real rPolySample(	unsigned int a_d,
					const Continuum<Real,3> &a_continuum,
					const typename Continuum<Real,D>::t_span &a_location,
					typename Continuum<Real,D>::t_index &a_index,
					typename Continuum<Real,D>::t_index &a_direct,
					const typename Continuum<Real,D>::t_index &a_block)
{

	int size = a_block[a_d];
	int minus = (size / 2) - 1;
	if(minus < 0) { minus = 0; }
	int pre = 0;
	int post = 0;
	if((int)a_direct[a_d] < minus)
	{
		pre = minus - a_direct[a_d];
		size -= pre;
		minus = a_direct[a_d];
	}
	int root = (int)a_direct[a_d] - minus;
	if(root + size > (int)a_continuum.count()[a_d])
	{
		post = size - (a_continuum.count()[a_d] - root);
		size = a_continuum.count()[a_d] - root;
	}

	if(size < 1) { size = 1; }

	std::vector<Vector<2,Real> > xy(size+pre+post);
	for(int i = 0; i < size; i++)
	{
		a_index[a_d] = root + i;

		xy[i+pre][0] = a_continuum.cellsize()[a_d] * (Real)(root + i);
		if(a_d == 0)
		{
			xy[i+pre][1] = a_continuum[a_index];
		}
		else
		{
			xy[i+pre][1] = rPolySample<D>(	a_d-1,
									a_continuum,
									a_location,
									a_index,
									a_direct,
									a_block);
		}
	}
	for(int i = 0; i < pre; i++)
	{
		xy[i][0] = a_continuum.cellsize()[a_d] * (Real)(i - pre);
		xy[i][1] = xy[pre][1];
	}
	for(int i = 0; i < post; i++)
	{
		xy[size+pre+i][0] = a_continuum.cellsize()[a_d] * (Real)(i + size + root);
		xy[size+pre+i][1] = xy[size+pre-1][1];
	}

	Real y,dy;
	polyInterp<Real>(xy,a_location[a_d],y,dy);
	return y;
}

/**	sample a Continuum by returning the a polynomial interpolated value.
	@relates Continuum */
template<unsigned int D>
Real polySample(	const Continuum<Real,D> &a_continuum,
					const typename Continuum<Real,D>::t_span &a_location,
					const typename Continuum<Real,D>::t_index &a_block)
{
	typename Continuum<Real,D>::t_index index;
	typename Continuum<Real,D>::t_index direct;
	a_continuum.direct(direct, a_location);
	typename Continuum<Real,D>::t_span location =
			a_location - a_continuum.origin();
	return rPolySample<D>(	D-1,
						a_continuum,
						location,
						index,
						direct,
						a_block);
}

/**	sample a Continuum by returning the a polynomial interpolated value.
	@relates Continuum */
template<unsigned int D>
Real polySample(	const Continuum<Real,D> &a_continuum,
					const typename Continuum<Real,D>::t_span &a_location)
{
	typename Continuum<Real,D>::t_index block;
	for(unsigned int i = 0; i < D; i++)
	{
		if(a_continuum.count()[i] < 4)
		{
			block[i] = a_continuum.count()[i];
		}
		else
		{
			block[i] = 4;
		}
	}
	return polySample(a_continuum, a_location, block);
}

template<unsigned int D>
struct t_radial_set
{
	typename Continuum<Real,D>::t_span	*m_location;
	typename Continuum<Real,D>::t_span	*m_radius;
	Real								m_value;
};

template<unsigned int D>
void rad_func(Continuum<Real,D> &a_continuum, typename Continuum<Real,D>::t_index &a_index, t_radial_set<D> *a_data)
{
	Real value = 0.0;
	typename Continuum<Real,D>::t_span index_loc;
	a_continuum.space(index_loc, a_index);
	for(unsigned int i = 0; i < D; i++)
	{
		Real dist = fabs(index_loc[i] - (*(a_data->m_location))[i]);
		if(a_continuum.boundary()[i] < 0.0)
		{
			if(dist > fabs(a_continuum.boundary()[i]) / 2.0)
			{
				dist = fabs(a_continuum.boundary()[i]) - dist;
			}
		}
		dist /= (*(a_data->m_radius))[i];
		dist *= dist;
		if(dist > 1.0) return;
		value += dist;
	}
	value = 1.0-sqrt(value);
	if(value < 0.0) return;
	value = a_data->m_value*value;
	a_continuum.set(a_index, value);
}

/** Radial set function.
	@relates Continuum */
template<unsigned int D>
void radialSet(Continuum<Real,D> &a_continuum,
		const typename Continuum<Real,D>::t_span &a_location,
		const typename Continuum<Real,D>::t_span &a_radius, Real a_value)
{
	t_radial_set<D> radial_data;
	radial_data.m_location =
			const_cast<typename Continuum<Real,D>::t_span *>(&a_location);
	radial_data.m_radius =
			const_cast<typename Continuum<Real,D>::t_span *>(&a_radius);
	radial_data.m_value = a_value;
	a_continuum.iterate(rad_func<D>, &radial_data);
}


/**	Calculate the gradient of a Continuum at a point using midpoint
	differencing and polynomial interpolation.
	@relates Continuum */
template<unsigned int D>
void gradientMidPoly(Continuum<Real,D> &a_continuum,
		typename Continuum<Real,D>::t_span &a_gradient,
		const typename Continuum<Real,D>::t_span &a_location, Real a_h = 0.1)
{
	for(unsigned int i = 0; i < D; i++)
	{
		typename Continuum<Real,D>::t_span lo(a_location);
		typename Continuum<Real,D>::t_span hi(a_location);
		lo[i] -= a_h;
		hi[i] += a_h;
		a_continuum.safe(lo);
		a_continuum.safe(hi);
		Real lo_smpl = polySample(a_continuum, lo);
		Real hi_smpl = polySample(a_continuum, hi);
		a_gradient[i] = (hi_smpl - lo_smpl)/(2.0*a_h);
	}
}

/**	Calculate the gradient of a Continuum at a point using forward
	differencing and linear interpolation.
	@relates Continuum */
template<unsigned int D>
void gradientFwdLinear(Continuum<Real,D> &a_continuum,
		typename Continuum<Real,D>::t_span &a_gradient,
		const typename Continuum<Real,D>::t_span &a_location, Real a_h = 0.1)
{
	for(unsigned int i = 0; i < D; i++)
	{
		typename Continuum<Real,D>::t_span lo(a_location);
		typename Continuum<Real,D>::t_span hi(a_location);
		hi[i] += a_h;
		a_continuum.safe(lo);
		a_continuum.safe(hi);
		Real lo_smpl = linearSample(a_continuum, lo);
		Real hi_smpl = linearSample(a_continuum, hi);
		a_gradient[i] = (hi_smpl - lo_smpl)/(a_h);
	}
}

/**	Calculate the gradient of a Continuum at a point using midpoint
	differencing and linear interpolation.
	@relates Continuum */
template<unsigned int D>
void gradient(Continuum<Real,D> &a_continuum,
		typename Continuum<Real,D>::t_span &a_gradient,
		const typename Continuum<Real,D>::t_span &a_location, Real a_h = 0.1)
{
	for(unsigned int i = 0; i < D; i++)
	{
		typename Continuum<Real,D>::t_span lo(a_location);
		typename Continuum<Real,D>::t_span hi(a_location);
		lo[i] -= a_h;
		hi[i] += a_h;
		a_continuum.safe(lo);
		a_continuum.safe(hi);
		Real lo_smpl = linearSample(a_continuum, lo);
		Real hi_smpl = linearSample(a_continuum, hi);
		a_gradient[i] = (hi_smpl - lo_smpl)/(2.0*a_h);
	}
}


template<typename T, unsigned int D>
struct t_derive
{
	Continuum<T,D>	*m_vectors;
};

template<typename T, unsigned int D>
void derive_fun(Continuum<Real,D> &a_continuum, typename Continuum<Real,D>::t_index &a_index, t_derive<T,D> *a_data)
{
	for(unsigned int d = 0; d < D; d++)
	{
		if(a_continuum.cellsize()[d] > 0.0)
		{
			typename Continuum<Real,D>::t_index dn(a_index);
			dn[d] -= 1;
			a_continuum.safe(dn);
			typename Continuum<Real,D>::t_index up(a_index);
			up[d] += 1;
			a_continuum.safe(up);
			Real value = (a_continuum[up] - a_continuum[dn]) /
				(2.0*a_continuum.cellsize()[d]);
			setAt(a_data->m_vectors->access(a_index), d, value);
		}
		else
		{
			setAt(a_data->m_vectors->access(a_index), d, 0.0f);
		}
	}
}

/** Calculate a gradient vector field for a scalar field.
	@relates Continuum */
template<typename T, unsigned int D>
void derive(Continuum<T,D> &a_vector, Continuum<Real,D> &a_scalar)
{
	T dummy;
	a_vector.create(a_scalar.count(), a_scalar.boundary(), dummy);
	t_derive<T,D> derive_data;
	derive_data.m_vectors = &a_vector;
	a_scalar.iterate(derive_fun<T,D>, &derive_data);
}
#endif


} /* namespace ext */
} /* namespace fe */

#endif /* __spatial_Continuum_h__ */

