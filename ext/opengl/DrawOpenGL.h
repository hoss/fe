/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __opengl_DrawOpenGL_h__
#define __opengl_DrawOpenGL_h__

#define DGL_CYLINDER_GLU	FALSE	// replace default with glu version
#define DGL_SPHERE_GLU		TRUE	// replace default with glu version

#define DGL_DEFAULT_TEXTURE_SIZE	256

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief OpenGL-specific implementations for DrawI

	@ingroup opengl
*//***************************************************************************/
class FE_DL_EXPORT DrawOpenGL: public DrawCommon,
		public Initialize<DrawOpenGL>
{
	public:

					DrawOpenGL();
virtual				~DrawOpenGL();

		void		initialize(void);

virtual	BWORD		isDirect(void) const				{ return m_isDirect; }
virtual	void		flush(void);

virtual void		pushMatrixState(void);
virtual void		popMatrixState(void);

virtual	void		pushMatrix(MatrixMode a_mode,Real a_values[16]);
virtual	void		popMatrix(MatrixMode a_mode);

virtual	void		unbindVertexArray(void);

virtual void		setBrightness(Real a_brightness);
virtual void		setContrast(Real a_contrast);

virtual void		setDrawMode(sp<DrawMode> spMode);

virtual	sp<FontI>	font(void)
					{	assureFont();
						return m_spFont; }

virtual	Real		multiplication(void);

					using DrawCommon::drawPoints;

virtual void		drawPoints(const SpatialVector *vertex,
							const SpatialVector *normal,U32 vertices,
							BWORD multicolor,const Color *color,
							sp<DrawBufferI> spDrawBuffer);

					using DrawCommon::drawLines;

virtual void		drawLines(const SpatialVector *vertex,
							const SpatialVector *normal,U32 vertices,
							StripMode strip,
							BWORD multicolor,const Color *color,
							BWORD multiradius,const Real *radius,
							const Vector3i *element,U32 elementCount,
							sp<DrawBufferI> spDrawBuffer);

					using DrawCommon::drawTriangles;

virtual	void		drawTriangles(const SpatialVector *vertex,
							const SpatialVector *normal,
							const Vector2 *texture,U32 vertices,
							StripMode strip,BWORD multicolor,
							const Color *color,
							const Array<I32>* vertexMap,
							const Array<I32>* hullPointMap,
							const Array<Vector4i>* hullFacePoint,
							sp<DrawBufferI> spDrawBuffer);

virtual void		drawRectangles(const SpatialVector *vertex,U32 vertices,
						BWORD multicolor,const Color *color);

virtual	void		drawAlignedText(const SpatialVector &location,
							const String text,const Color &color);

#if DGL_CYLINDER_GLU
virtual	void		drawCylinder(const SpatialTransform &transform,
							const SpatialVector *scale,
							Real baseScale,const Color &color,U32 slices)
					{	drawCylinderGLU(transform,scale,baseScale,
								color,slices); }
#endif

#if DGL_SPHERE_GLU
virtual	void		drawSphere(const SpatialTransform &transform,
							const SpatialVector *scale,const Color &color)
					{	drawSphereGLU(transform,scale,color); }
#endif

					using DrawCommon::drawTransformedPoints;

virtual void		drawTransformedPoints(
							const SpatialTransform &transform,
							const SpatialVector *scale,
							const SpatialVector *vertex,
							const SpatialVector *normal,U32 vertices,
							BWORD multicolor,const Color *color,
							sp<DrawBufferI> spDrawBuffer);

					using DrawCommon::drawTransformedLines;

virtual void		drawTransformedLines(
							const SpatialTransform &transform,
							const SpatialVector *scale,
							const SpatialVector *vertex,
							const SpatialVector *normal,
							U32 vertices,StripMode strip,
							BWORD multicolor,const Color *color,
							BWORD multiradius,const Real *radius,
							const Vector3i *element,U32 elementCount,
							sp<DrawBufferI> spDrawBuffer);

					using DrawCommon::drawTransformedTriangles;

virtual void		drawTransformedTriangles(
							const SpatialTransform &transform,
							const SpatialVector *scale,
							const SpatialVector *vertex,
							const SpatialVector *normal,
							const Vector2 *texture,
							U32 vertices,StripMode strip,BWORD multicolor,
							const Color *color,
							const Array<I32>* vertexMap,
							const Array<I32>* hullPointMap,
							const Array<Vector4i>* hullFacePoint,
							sp<DrawBufferI> spDrawBuffer);

virtual	void		drawRaster(sp<ImageI> spImageI,
						const SpatialVector& location);

virtual	sp<DrawBufferI>	createBuffer(void);

	protected:

virtual	void		createDefaultView(void);

virtual	void		drawPolys(U32 grain,const SpatialVector *vertex,
							const SpatialVector *normal,
							const Vector2 *texture,
							U32 vertices,StripMode strip,
							BWORD multicolor,const Color *color,
							const Array<I32>* vertexMap,
							const Array<I32>* hullPointMap,
							const Array<Vector4i>* hullFacePoint,
							sp<DrawBufferI> spDrawBuffer);

virtual	void		drawLinesInternal(const SpatialVector *vertex,
							const SpatialVector *normal,
							const Vector2 *texture,U32 vertices,
							StripMode strip,
							BWORD multicolor,const Color *color,
							BWORD multiradius,const Real *radius,
							const Vector3i *element,U32 elementCount,
							sp<DrawBufferI> spDrawBuffer);

	class Texture:
		virtual public Component,
		public CastableAs<Texture>
	{
		public:
					Texture(void);
	virtual			~Texture(void);

			GLuint	m_id;
			GLsizei	m_width;
			GLsizei	m_height;
			GLenum	m_type;
	const	GLvoid*	m_pData;
			I32		m_serial;
	};

	class Buffer:
		public Catalog,
		public DrawBufferI,
		public CastableAs<Buffer>
	{
		public:
						Buffer(void);
	virtual				~Buffer(void);
			void		releaseAll(void);

			sp<DrawI>	m_spDrawI;
			GLuint		m_vboVertex;
			GLuint		m_vboVertexMap;
			GLuint		m_vboNormal;
			GLuint		m_vboTexture;
			GLuint		m_vboColor;

#ifdef FE_GL_OPENCL
			cl_mem		m_clMemVertex;
			cl_mem		m_clMemColor;
#endif
	};

		sp<Texture>	createTexture(void);
		sp<Texture>	getTexture(GLsizei a_width,GLsizei a_height,
							GLenum a_format,GLenum a_type,
							const GLvoid* a_pData,I32 a_serial);

		void		assureFont(void);

		void		threadLock(void);
		void		threadUnlock(void);

		void		bindTexture(sp<ImageI> spImageI,I32 imageID);

		void		drawCylinderGLU(const SpatialTransform &transform,
							const SpatialVector *scale,
							Real baseScale,const Color &color,U32 slices);

		void		drawSphereGLU(const SpatialTransform &transform,
							const SpatialVector *scale,const Color &color);

		void		beginArrays(const SpatialVector *vertex,
							const SpatialVector *normal,
							const Array<I32>* vertexMap,
							const Vector2 *texture,U32 vertices,
							BWORD multicolor,const Color *color,
							sp<DrawBufferI> spDrawBuffer,
							BWORD normals,BWORD blending);
		void		endArrays(const Array<I32>* vertexMap,
							const Vector2 *texture,
							BWORD multicolor,const Color *color,
							sp<DrawBufferI> spDrawBuffer,
							BWORD normals);

		void		setupLights(void);
		void		createDefaultTexture(void);
		void		useDefaultTexture(void);
		void		pushAntialiasPoints(void);
		void		pushAntialiasLines(void);
		void		pushBlending(void);
		void		activateBlending(void);
		void		queryFormat(sp<ImageI> spImageI,GLenum& format,
							GLenum& type,GLint& components);

		void		setup(DrawMode::DrawStyle style,BWORD lit,BWORD dual,
							BWORD texture,BWORD frontcull,BWORD backcull,
							BWORD zBuffer);

static	void		checkForError(String a_location);

		sp<ImageI>			m_spCurrentTexture;
		I32					m_currentTextureId;

		Real				m_multiplication;
		BWORD				m_isDirect;
		BWORD				m_lockThreads;
		GLUquadric*			m_pQuadric;

		sp<Texture>			m_spDefaultTexture;
		GLubyte				m_defaultTextureData[DGL_DEFAULT_TEXTURE_SIZE]
									[DGL_DEFAULT_TEXTURE_SIZE][3];

		std::map< const void*, sp<Texture> >	m_textureTable;

		sp<FontI>			m_spFont;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __opengl_DrawOpenGL_h__ */
