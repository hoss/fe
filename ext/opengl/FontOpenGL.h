
/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __opengl_FontOpenGL_h__
#define __opengl_FontOpenGL_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief OpenGL-specific implementations for FontI

	@ingroup opengl
*//***************************************************************************/
class FE_DL_EXPORT FontOpenGL: public FontI,
		public Initialize<FontOpenGL>
{
	public:
				FontOpenGL(void);
virtual			~FontOpenGL(void);

		void	initialize(void);

virtual	I32		fontHeight(I32* a_pAscent,I32* a_pDescent)
				{
					if(a_pAscent)
					{
						*a_pAscent=m_fontAscent;
					}
					if(a_pDescent)
					{
						*a_pDescent=m_fontDescent;
					}

					return m_fontAscent+m_fontDescent;
				}
virtual	I32		pixelWidth(String a_string)
				{
#if FE_2DGL==FE_2D_X_GFX
					return XTextWidth(m_pFontStruct,
							a_string.c_str(),a_string.length());
#else
					//* TODO Win32
					return 0;
#endif
				}

virtual	void	drawAlignedText(sp<Component> a_spDrawComponent,
					const SpatialVector& a_location,
					const String a_text,const Color &a_color);

	private:

		I32				m_fontBase;
		I32				m_fontRange;
		I32				m_fontAscent;
		I32				m_fontDescent;
#if FE_2DGL==FE_2D_X_GFX
		XFontStruct*	m_pFontStruct;
#endif

		Real				m_multiplication;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __opengl_FontOpenGL_h__ */
