/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __opengl_ViewOpenGL_h__
#define __opengl_ViewOpenGL_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief OpenGL-specific implementations for ViewI

	@ingroup opengl
*//***************************************************************************/
class FE_DL_EXPORT ViewOpenGL: virtual public ViewCommon
{
	public:
						ViewOpenGL();

						//* as ViewI
virtual	void			use(ViewI::Projection a_projection);

virtual	Real			pixelSize(const SpatialVector& center,
								Real radius) const;
virtual	Real			worldSize(const SpatialVector& center,
								Real pixels) const;

virtual	SpatialVector	unproject(Real winx,Real winy,Real winz,
								ViewI::Projection a_projection) const;
virtual	SpatialVector	project(const SpatialVector& point,
								ViewI::Projection a_projection) const;

virtual	void			setScissor(const Box2i* a_pBox);
virtual	const Box2i*	scissor(void) const;

	private:
		bool			currentContextIsSet(void);

		void			useOrtho(void);
		void			usePerspective(void);
		void			prepare(ViewI::Projection a_projection,
								GLdouble* model16,
								GLdouble* proj16,GLint* view) const;

		BWORD			m_scissoring;
		Box2i			m_scissor;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __opengl_ViewOpenGL_h__ */
