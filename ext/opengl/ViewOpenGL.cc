/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <opengl/opengl.pmh>

namespace fe
{
namespace ext
{

ViewOpenGL::ViewOpenGL():
	m_scissoring(FALSE)
{
}

bool ViewOpenGL::currentContextIsSet(void)
{
#if FE_2DGL==FE_2D_X_GFX	// Linux or X11-mode OSX
	return glXGetCurrentContext()!=NULL;
#elif FE_OS==FE_WIN32 || FE_OS==FE_WIN64
	return wglGetCurrentContext()!=NULL;
#endif
	return FALSE;
}

void ViewOpenGL::use(ViewI::Projection a_projection)
{
	sp<CameraI> spCamera=camera();
	if(spCamera.isNull())
	{
		return;
	}

#if FE_CODEGEN <= FE_DEBUG
	if(a_projection==ViewI::e_current &&
			projection()!=ViewI::e_perspective &&
			projection()!=ViewI::e_ortho)
	{
		feX("ViewOpenGL::use",
				"e_current not valid unless already e_perspective or e_ortho");
	}
#endif

	if(a_projection!=ViewI::e_current)
	{
		setProjection(a_projection);
	}

	switch(projection())
	{
		case ViewI::e_perspective:
			usePerspective();
			break;
		case ViewI::e_ortho:
			useOrtho();
			break;
		case ViewI::e_current:
			break;
		default:
			feX("ViewOpenGL::use", "can only use e_perspective or e_ortho");
			break;
	}
}

void ViewOpenGL::usePerspective(void)
{
	if(!currentContextIsSet())
	{
//		feLog("ViewOpenGL::usePerspective NULL context\n");
		return;
	}

	sp<CameraI> spCamera=camera();
	const SpatialTransform& cameraMatrix=spCamera->cameraMatrix();

	const Real fovy=spCamera->fov()[1];

	Real nearplane;
	Real farplane;
	spCamera->getPlanes(nearplane,farplane);

	glMatrixMode(GL_PROJECTION);
#if FE_DOUBLE_REAL
	const GLdouble aspect=
			(GLdouble)width(viewport())/(GLdouble)height(viewport());
#else
	const GLfloat aspect=
			(GLfloat)width(viewport())/(GLfloat)height(viewport());
#endif
	const Matrix<4,4,Real> proj4x4=perspective(fovy,aspect,
			nearplane,farplane);
#if FE_DOUBLE_REAL
	glLoadMatrixd(proj4x4.raw());
#else
	glLoadMatrixf(proj4x4.raw());
#endif

	glViewport(viewport()[0],viewport()[1],
			width(viewport()),height(viewport()));

	glMatrixMode(GL_MODELVIEW);
	Real matrix16[16];
	cameraMatrix.copy16(matrix16);
#if FE_DOUBLE_REAL
	glLoadMatrixd(matrix16);
#else
	glLoadMatrixf(matrix16);
#endif

#if FALSE
	feLog("\nfovy=%.6G aspect=%.6G near=%.6G far=%.6G\n",
			fovy,aspect,nearplane,farplane);
	feLog("viewport %.6G %.6G %.6G %.6G\n",
			viewport()[0],viewport()[1],width(viewport()),height(viewport()));
	GLfloat proj16[16];
	glGetFloatv(GL_PROJECTION_MATRIX,proj16);
	Matrix<4,4,Real> projMatrix;
	for(U32 m=0;m<16;m++)
		projMatrix[m]=proj16[m];
	feLog("---\n%s\n",print(projMatrix).c_str());
	projMatrix=perspective(fovy,Real(aspect),nearplane,farplane);
	feLog("---\n%s\n",print(proj4x4).c_str());
	feLog("---\n%s\n",print(cameraMatrix).c_str());

	if(FE_INVALID_SCALAR(proj16[0]))
	{
		FEASSERT(FALSE);
	}
#endif
}

void ViewOpenGL::useOrtho(void)
{
	if(!currentContextIsSet())
	{
//		feLog("ViewOpenGL::useOrtho NULL context\n");
		return;
	}

	sp<CameraI> spCamera=camera();
	const SpatialTransform& cameraMatrix=spCamera->cameraMatrix();

	Real zoom(1);
	Vector2 center(0,0);
	if(!spCamera->rasterSpace())
	{
		spCamera->getOrtho(zoom,center);
	}

	glMatrixMode(GL_PROJECTION);
	const Vector2 size(width(viewport()),height(viewport()));
	const Matrix<4,4,Real> proj4x4=computeOrtho(zoom,center,size);
#if FE_DOUBLE_REAL
	glLoadMatrixd(proj4x4.raw());
#else
	glLoadMatrixf(proj4x4.raw());
#endif

	glViewport(viewport()[0],viewport()[1],width(viewport()),
			height(viewport()));

	glMatrixMode(GL_MODELVIEW);
	if(spCamera->rasterSpace())
	{
		glLoadIdentity();
	}
	else
	{
		Real matrix16[16];
		cameraMatrix.copy16(matrix16);

#if FE_DOUBLE_REAL
		glLoadMatrixd(matrix16);
#else
		glLoadMatrixf(matrix16);
#endif
	}
}

void ViewOpenGL::prepare(ViewI::Projection a_projection,
		GLdouble* model16,GLdouble* proj16,GLint* view) const
{
	sp<CameraI> spCamera=camera();
	const SpatialTransform& cameraMatrix=spCamera->cameraMatrix();
	cameraMatrix.copy16(model16);
	spCamera->setCameraMatrix(cameraMatrix);

	view[0]=viewport()[0];
	view[1]=viewport()[1];
	view[2]=width(viewport());
	view[3]=height(viewport());

	ViewI::Projection mode_mod=(a_projection==ViewI::e_current)?
			projection(): a_projection;

	Matrix<4,4,Real> proj4x4;
	switch(mode_mod)
	{
		case ViewI::e_perspective:
		{
			const Real fovy=spCamera->fov()[1];

			Real nearplane;
			Real farplane;
			spCamera->getPlanes(nearplane,farplane);

			GLfloat aspect=(GLfloat)width(viewport())/
					(GLfloat)height(viewport());
			proj4x4=perspective(fovy,Real(aspect),nearplane,farplane);
			break;
		}
		case ViewI::e_ortho:
		{
			Real zoom;
			Vector2 center;
			spCamera->getOrtho(zoom,center);

			Vector2 size(width(viewport()),height(viewport()));
			proj4x4=computeOrtho(zoom,center,size);
			break;
		}
		default:
			//* something better than zeros?
			set(proj4x4);
	}
	for(U32 m=0;m<16;m++)
	{
		proj16[m]=proj4x4[m];
	}
}

SpatialVector ViewOpenGL::project(
		const SpatialVector& point,ViewI::Projection a_projection) const
{
	GLdouble model16[16];
	GLdouble proj16[16];
	GLint view[4];
	GLdouble win[3];

	prepare(a_projection,model16,proj16,view);

	const GLint success=gluProject(point[0],point[1],point[2],
			model16, proj16, view,
			&win[0],&win[1],&win[2]);
	if(!success)
	{
		feLog("ViewOpenGL::project gluProject failed\n");

		Matrix<4,4,Real> modelMatrix;
		Matrix<4,4,Real> projMatrix;
		for(U32 m=0;m<16;m++)
		{
			modelMatrix[m]=model16[m];
			projMatrix[m]=proj16[m];
		}

		feLog("  view %d,%d,%d,%d point %.6G,%.6G,%.6G\n"
				" modeling %s\n projection %s\n",
				view[0],view[1],view[2],view[3],
				point[0],point[1],point[2],
				print(modelMatrix).c_str(),
				print(projMatrix).c_str());
	}

	const SpatialVector result(win[0],win[1],win[2]);

	return result;
}

SpatialVector ViewOpenGL::unproject(
		Real winx,Real winy,Real winz,ViewI::Projection a_projection) const
{
	GLdouble model16[16];
	GLdouble proj16[16];
	GLint view[4];
	GLdouble object[3];

	prepare(a_projection,model16,proj16,view);

	const GLint success=gluUnProject(winx,winy,winz,
			model16,proj16,view,
			&object[0],&object[1],&object[2]);
	if(!success)
	{
		feLog("ViewOpenGL::unproject gluUnProject failed\n");
	}

	const SpatialVector result(object[0],object[1],object[2]);

#if FALSE
	feLog("\nunproject %d %d,%d,%d,%d  %.6G,%.6G,%.6G  %s\n%s\n\n%s\n",
			success,
			view[0],view[1],view[2],view[3],
			winx,winy,winz,
			print(result).c_str(),
			print(a_projection).c_str(),
			print(camera()->cameraMatrix()).c_str());
#endif

	return result;
}

Real ViewOpenGL::pixelSize(const SpatialVector& center,Real radius) const
{
	GLdouble model16[16];
	GLdouble proj16[16];
	GLint view[4];

	prepare(ViewI::e_perspective,model16,proj16,view);

	SpatialTransform model;
	set(model,model16);

	SpatialVector moved;
	transformVector(model,center,moved);

	sp<CameraI> spCamera=camera();

	const Real fovy=spCamera->fov()[1];

	return -view[3]*radius/(tanf(0.5f*fovy*degToRad)*moved[2]);
}

Real ViewOpenGL::worldSize(const SpatialVector& center,Real pixels) const
{
	GLdouble model16[16];
	GLdouble proj16[16];
	GLint view[4];

	prepare(ViewI::e_perspective,model16,proj16,view);

	SpatialTransform model;
	set(model,model16);

	SpatialVector moved;
	transformVector(model,center,moved);

	sp<CameraI> spCamera=camera();

	const Real fovy=spCamera->fov()[1];

	const Real units=fe::maximum(Real(0),
			-pixels*(tanf(0.5f*fovy*degToRad)*moved[2])/view[3]);

	return units;
}

void ViewOpenGL::setScissor(const Box2i* a_pBox)
{
	if(!a_pBox)
	{
		m_scissoring=FALSE;
		glDisable(GL_SCISSOR_TEST);
		return;
	}

	m_scissoring=TRUE;
	m_scissor= *a_pBox;

	glScissor(m_scissor[0],m_scissor[1],width(m_scissor),height(m_scissor));
	glEnable(GL_SCISSOR_TEST);
}

const Box2i* ViewOpenGL::scissor(void) const
{
	if(!m_scissoring)
	{
		return NULL;
	}

	return &m_scissor;
}

} /* namespace ext */
} /* namespace fe */
