/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <thread/thread.pmh>

#define FE_WKO_DEBUG	FALSE

#ifdef _OPENMP
#include <omp.h>
#endif

using namespace fe;
using namespace fe::ext;

void WorkOmp::initialize(void)
{
}

//* static
I32 WorkOmp::threadIndex(void)
{
#ifdef _OPENMP
	return omp_get_thread_num();
#else
	return 0;
#endif
}

//* static
I32 WorkOmp::threadCount(void)
{
#ifdef _OPENMP
	return omp_get_num_threads();
#else
	return 0;
#endif
}

void WorkOmp::run(sp<WorkI> a_spWorkI,U32 a_threads,
	sp<SpannedRange> a_spSpannedRange,String a_stage)
{
	m_spWorkI=a_spWorkI;

#if FE_WKO_DEBUG
	feLog("WorkOmp::run %d threads\n",a_threads);
#endif

	const U32 atomicCount=a_spSpannedRange->atomicCount();

	//* atomic
	#pragma omp parallel for
	for(U32 atomicIndex=0;atomicIndex<atomicCount;atomicIndex++)
	{
		const SpannedRange::MultiSpan& rAtomic=
				a_spSpannedRange->atomic(atomicIndex);

		const U32 id=threadIndex();

#if FE_WKO_DEBUG
		const U32 threads=threadCount();

		feLog("WorkOmp::run atomic %d/%d id %d/%d %s\n",
				atomicIndex,atomicCount,id,threads,rAtomic.brief().c_str());
#endif

		sp<SpannedRange> spRange(new SpannedRange());
		spRange->addAtomic().add(rAtomic);

		m_spWorkI->run(id,spRange,a_stage);
	}

	//* nonatomic
	const SpannedRange::MultiSpan& rNonAtomic=a_spSpannedRange->nonAtomic();
	const U32 spanCount=rNonAtomic.spanCount();
	for(U32 spanIndex=0;spanIndex<spanCount;spanIndex++)
	{
		const SpannedRange::Span& rSpan=rNonAtomic.span(spanIndex);

		FEASSERT(rSpan[0]>=0.0);
		FEASSERT(rSpan[1]>=0.0);
		FEASSERT(rSpan[0]<=rSpan[1]);

		//* TODO combine small spans
		const I32 start=rSpan[0];
		const I32 end=rSpan[1];

#if FE_WKO_DEBUG
		feLog("WorkOmp::run nonatomic span %d %d\n",start,end);
#endif

		#pragma omp parallel
		{
			const I32 id=threadIndex();
			const I32 threads=threadCount();

			FEASSERT(threads);

			const I32 subStart=start+(end+1-start)*id/threads;
			const I32 subEnd=(id==threads-1)? end:
					start+(end+1-start)*(id+1)/threads-1;

#if FE_WKO_DEBUG
			feLog("WorkOmp::run nonatomic id %d/%d %d %d\n",
					id,threads,subStart,subEnd);
#endif

			if(subEnd>=subStart)
			{
				sp<SpannedRange> spRange(new SpannedRange());
				spRange->nonAtomic().add(subStart,subEnd);

				m_spWorkI->run(id,spRange,a_stage);
			}
		}
	}

	const WorkI::Threading wasThreading=m_spWorkI->threading();
	m_spWorkI->setThreading(WorkI::e_singleThread);

	//* postatomic
	const SpannedRange::MultiSpan& rPostAtomic=a_spSpannedRange->postAtomic();
	if(rPostAtomic.spanCount())
	{
#if FE_WKO_DEBUG
		feLog("WorkOmp::run postatomic %s\n",rPostAtomic.brief().c_str());
#endif

		sp<SpannedRange> spRange(new SpannedRange());
		spRange->nonAtomic().add(rPostAtomic);

		m_spWorkI->run(-1,spRange,a_stage);
	}

	m_spWorkI->setThreading(wasThreading);
	m_spWorkI=NULL;

//	m_spGang->resolveException();

#if FE_WKO_DEBUG
	feLog("WorkOmp::run done\n");
#endif
}
