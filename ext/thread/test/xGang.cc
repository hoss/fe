/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "thread/thread.h"

using namespace fe;
using namespace fe::ext;

class Worker: public Thread::Functor
{
	public:
						Worker(sp< JobQueue<I32> > a_spJobQueue,
							U32 a_id,String a_stage):
							m_id(a_id),
							m_spJobQueue(a_spJobQueue)
						{
							feLog("Construct thread %d\n",m_id);
						}
virtual					~Worker(void)
						{
							feLog("Destruct thread %d\n",m_id);
						}

virtual	void			operate(void);

		void			setObject(sp<Counted> spObject)						{}

	private:
		U32					m_id;
		sp< JobQueue<I32> > m_spJobQueue;
};

inline void Worker::operate(void)
{
	feLog("Starting thread %d\n",m_id);
	I32 job=0;
	while(m_spJobQueue->take(job))
	{
		feLog("Thread %d Job %d\n",m_id,job);
		milliSleep(1);
	}
	feLog("Sync thread %d\n",m_id);
	U32 leader=m_spJobQueue->synchronize();
	feLog("Sync'd thread %d leader=%d\n",m_id,leader);
}

int main(int argc,char** argv)
{
	UNIT_START();
	BWORD completed=FALSE;

	if(argc>2)
	{
		feLog("Syntax: %s [thread_count]\n",argv[0]);
		exit(1);
	}

	const U32 threads=(argc>1)? atoi(argv[1]): Thread::hardwareConcurrency();
	feLog("%d threads\n",threads);

	try
	{
//		Mutex::confirm("fexBoostThread");
		Mutex::confirm();

		RecursiveMutex mutex;
		{
			RecursiveMutex::Guard lock(mutex);
		}
		unitTest(true,"guard");

		sp< Gang<Worker,I32> > spGang(new Gang<Worker,I32>());
		for(U32 m=0;m<threads+3;m++)
		{
			spGang->post(100+m);
		}

		const BWORD success=spGang->start(sp<Counted>(NULL),threads);
		UNIT_TEST(success);

		spGang->finish();

		Mutex::dismiss();

		completed=TRUE;
	}
	catch(Exception &e)	{ e.log(); }
	catch(...)			{ feLog("uncaught exception\n"); }

	UNIT_TEST(completed);
	UNIT_TEST(!Counted::trackerCount());
	UNIT_TRACK(4);
	UNIT_RETURN();
}

