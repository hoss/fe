/** @file */

#include "fe/data.h"
#include <chrono>

using namespace fe;
using namespace std::chrono;

#define SLEEP_MILLISECONDS 0
#define TEST_DURATION_SECONDS 1
#define MIN_THREADS 3
#define MAX_THREADS 11

sp<Master> master;
sp<Registry> registry;

AsSystem SystemAS;

/**
 * Task and Thread container.
 */
template<class Task>
struct TaskThread
{
	Task* task;
	Thread* thread;

	TaskThread(sp<RecordGroup> records)
	{
		task = new Task;
		task->records = records;
		thread = new Thread(task);
	};

	TaskThread(const TaskThread& other)
	{
		task = other.task;
		thread = other.thread;
	};

	void stop()
	{
		task->poison.start();
		thread->join();
		delete thread;
	};
};

/**
 * Thread task that rapidly increments and decrements the reference counter
 * on Records.
 *
 * This task is designed to test the thread integrity of
 * Record reference counting.
 */
class ReferenceSmashingTask : public Thread::Functor
{
public:
	void operate(void) override
	{
		std::vector<Record> foundRecords;

		// Thread loop.
		while (!poison.active())
		{
			// Delete all found records.
			// This deletes Records which decrements the reference counter.
			foundRecords.clear();

			// Find all test records.
			// This creates new Records which increments the reference counter.
			SystemAS.filter(foundRecords, records);

			milliSleep(SLEEP_MILLISECONDS);
		}

		poison.stop();
	};

	/** Poison to kill the thread */
	Poison poison;

	sp<RecordGroup> records;
};

/**
 * This thread task sweeps the number of ReferenceSmashingTask threads
 * up and down.
 */
class ThreadSweepingTask : public Thread::Functor
{
public:
	void operate(void) override
	{
		threads.reserve(MAX_THREADS);

		// We start off with 3 threads.
		for (unsigned int i = 0; i < 3; i++)
		{
			threads.emplace_back(records);
		}

		// Thread loop.
		while (!poison.active())
		{
			milliSleep(50);

			if (rateOfChange > 0)
			{
				threads.emplace_back(records);
			}
			else
			{
				threads.front().stop();
				threads.erase(threads.begin());
			}

			if (threads.size() >= MAX_THREADS || threads.size() <= MIN_THREADS)
			{
				rateOfChange *= -1;
			}

			// feLog("Thread count: %u\n", threads.size());
		}

		poison.stop();

		for(auto thread : threads)
		{
			thread.stop();
		}
	};

	/** Poison to kill the thread */
	Poison poison;

	sp<RecordGroup> records;

protected:
	std::vector<TaskThread<ReferenceSmashingTask>> threads;

	int rateOfChange = 1;
};

int main(int argc, char **argv)
{
	UnitTest unitTest;

    // Create registry.
    master = new Master;
    registry = master->registry();
    registry->manage("feAutoLoadDL");
    registry->manage("fexStdThread");

    // Assert record types.
    sp<TypeMaster> typeMaster = master->typeMaster();
    assertData(typeMaster);

	// Create layout.
    sp<Scope> scope = registry->create("Scope");
    sp<RecordGroup> records(new RecordGroup());
    sp<Layout> testLayout = scope->declare("testvoid");
    SystemAS.populate(testLayout);

	// Create a number of records.
	for(unsigned int i = 0; i < 3; i++)
	{
		records->add(scope->createRecord(testLayout));
	}

	std::vector<Record> foundRecords;
	SystemAS.filter(foundRecords, records);

	try
	{
		// Start thread sweeper.
		TaskThread<ThreadSweepingTask> thread(records);

		steady_clock::time_point startTime = steady_clock::now();
		while(steady_clock::now() - startTime < seconds{TEST_DURATION_SECONDS})
		{
			for(Record& record : foundRecords)
			{
				// The reference counter in this test should never
				// exceed these values.
				if (SystemAS.count(record) <= 0 ||
						SystemAS.count(record) > MAX_THREADS + 2)
				{
					unitTest(false, "Reference Counter");
					feLog("Record reference counter has an impossible value."
							" Record %d refcount: %d (min 1 max %d)\n",
							SystemAS.sn(record).load(),
							SystemAS.count(record).load(),
							MAX_THREADS + 2);
				}
				else
				{
					unitTest(true, "Reference Counter");
				}

				// If the serial number is -1, that means the Record was freed.
				if (SystemAS.sn(record) == -1)
				{
					unitTest(false, "Serial Number");
					feLog("Record was freed when it shouldn't have been."
							" Record %d refcount: %d\n",
							SystemAS.sn(record).load(),
							SystemAS.count(record).load());
				}
				else
				{
					unitTest(true, "Serial Number");
				}
			}

			milliSleep(100);
		}

		// Kill thread sweeper.
		thread.stop();
	}
	catch(Exception &e)	{ e.log(); }
	catch(...)			{ feLog("uncaught exception\n"); }

	return unitTest.failures();
}
