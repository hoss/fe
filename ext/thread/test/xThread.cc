/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "thread/thread.h"

using namespace fe;
using namespace fe::ext;

class Work: public WorkI
{
	public:

							Work(sp<Registry> a_spRegistry)
							{	m_spRegistry=a_spRegistry; }
virtual	WorkI::Threading	threading(void)		{ return WorkI::e_unknown; }
virtual	void				setThreading(WorkI::Threading a_threading)		{}
virtual	void				run(I32 a_id,sp<SpannedRange> a_spSpannedRange,
									String a_stage)
							{	run(a_id,a_spSpannedRange); }
virtual	void				run(I32 a_id,sp<SpannedRange> a_spSpannedRange)
							{
//								feLog("Running thread %d %s\n",a_id,
//										a_spSpannedRange->brief().c_str());

								//* just make a bunch of components
								const U32 componentCount=7777;
								sp<Component>* pspComponent=
										new sp<Component>[componentCount];
								for(U32 m=0;m<componentCount;m++)
								{
									pspComponent[m]=m_spRegistry->create(
											"WorkForceI");
								}
								for(U32 m=0;m<componentCount;m++)
								{
//									feLog("%d %d %d\n",a_id,m,
//											pspComponent[m].isValid());
									if(!pspComponent[m].isValid())
									{
										feX("invalid component");
									}
								}
								delete[] pspComponent;
							}

	private:
		sp<Registry>		m_spRegistry;
};

int main(int argc,char** argv)
{
	UNIT_START();
	BWORD completed=FALSE;

	if(argc>2)
	{
		feLog("Syntax: %s [thread_count]\n",argv[0]);
		exit(1);
	}

	const U32 threads=(argc>1)? atoi(argv[1]): Thread::hardwareConcurrency();
	feLog("%d threads\n",threads);

	Mutex::confirm();

	try
	{
		RecursiveMutex mutex;
		{
			RecursiveMutex::Guard lock(mutex);
		}
		unitTest(true,"guard");

		sp<Master> spMaster(new Master);
		sp<Registry> spRegistry=spMaster->registry();

		Result result=spRegistry->manage("fexThreadDL");
		UNIT_TEST(successful(result));

		sp<WorkForceI> spWorkForceI=spRegistry->create("WorkForceI");
		UNIT_TEST(spWorkForceI.isValid());

		if(spWorkForceI.isNull())
		{
			feX(argv[0], "couldn't create WorkForceI");
		}

		printf("WorkForceI \"%s\"\n",spWorkForceI->name().c_str());

		sp<WorkI> spWork(new Work(spRegistry));

		sp<SpannedRange> spRange(new SpannedRange());
		spRange->nonAtomic().add(100,100+threads+2);

		spWorkForceI->run(spWork,threads,spRange,"");

		completed=TRUE;
	}
	catch(Exception &e)	{ e.log(); }
	catch(...)			{ feLog("uncaught exception\n"); }

	UNIT_TEST(completed);
	UNIT_TEST(!Counted::trackerCount());
	UNIT_TRACK(5);
	UNIT_RETURN();
}

