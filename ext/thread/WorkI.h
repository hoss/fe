/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __thread_WorkI_h__
#define __thread_WorkI_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Execute a Range

	@ingroup thread
*//***************************************************************************/
class FE_DL_EXPORT WorkI:
	virtual public Component,
	public CastableAs<WorkI>
{
	public:

		enum	Threading
				{
					e_unknown,
					e_singleThread,
					e_multiThread
				};

					//* NOTE used by OperatorThreaded for SurfaceAccessibleI
virtual	Threading	threading(void)											=0;
virtual	void		setThreading(Threading a_threading)						=0;

virtual	void		run(I32 a_id,sp<SpannedRange> a_spSpannedRange)			=0;
virtual	void		run(I32 a_id,sp<SpannedRange> a_spSpannedRange,
							String a_stage)									=0;
};

} /* namespace ext */
} /* namespace fe */

#endif // __thread_WorkI_h__

