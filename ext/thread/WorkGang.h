
/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __thread_WorkGang_h__
#define __thread_WorkGang_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Execute a SpannedRange with a Gang

	@ingroup thread
*//***************************************************************************/
class FE_DL_EXPORT WorkGang: public WorkForceI,
	public Initialize<WorkGang>
{
	public:
					WorkGang(void):
						m_threadsAlive(0),
						m_waitForJobs(FALSE)								{}
virtual				~WorkGang(void);

		void		initialize(void);

virtual	sp<WorkI>	assignment(void)			{ return m_spWorkI; }
virtual	void		run(sp<WorkI> a_spWorkI,U32 a_threads,
							sp<SpannedRange> a_spSpannedRange,String a_stage);
virtual	void		stop(void);

virtual	BWORD		threadsAlive(void) const	{ return m_threadsAlive; }

virtual	BWORD		willWaitForJobs(void) const	{ return m_waitForJobs; }
virtual	void		waitForJobs(BWORD a_wait)	{ m_waitForJobs=a_wait; }

	private:

	class Worker: public Thread::Functor
	{
		public:
						Worker(sp< JobQueue< sp<SpannedRange> > > a_spJobQueue,
							U32 a_id,String a_stage);
	virtual				~Worker(void);

	virtual	void		operate(void);

			void		setObject(sp<Counted> spObject)
						{	m_spWorkForceI=spObject; }

		private:
			U32									m_id;
			String								m_stage;
			sp< JobQueue< sp<SpannedRange> > >	m_spJobQueue;
			sp<WorkForceI>						m_spWorkForceI;
	};

		sp< Gang< Worker,sp<SpannedRange> > >	m_spGang;

		sp<WorkI>			m_spWorkI;
		U32					m_threadsAlive;
		BWORD				m_waitForJobs;
};

} /* namespace ext */
} /* namespace fe */

#endif // __thread_WorkGang_h__
