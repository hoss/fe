/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <thread/thread.pmh>

#define FE_WKG_DEBUG	FALSE

using namespace fe;
using namespace fe::ext;

WorkGang::~WorkGang(void)
{
	stop();
}

void WorkGang::initialize(void)
{
	m_spGang=new Gang< WorkGang::Worker,sp<SpannedRange> >();
}

void WorkGang::run(sp<WorkI> a_spWorkI,U32 a_threads,
	sp<SpannedRange> a_spSpannedRange,String a_stage)
{
#if FE_WKG_DEBUG
	feLog("WorkGang::run\n");
#endif

	if(a_spSpannedRange.isNull())
	{
		feLog("WorkGang::run NULL range\n");
		return;
	}

	m_spWorkI=a_spWorkI;

	const U32 atomicCount=a_spSpannedRange->atomicCount();
	for(U32 atomicIndex=0;atomicIndex<atomicCount;atomicIndex++)
	{
		const SpannedRange::MultiSpan& rAtomic=
				a_spSpannedRange->atomic(atomicIndex);
		sp<SpannedRange> spSubRange(new SpannedRange());
		spSubRange->addAtomic()=rAtomic;

		//* TODO aggregate atomics when there is a whole lot of them

#if FE_WKG_DEBUG
		feLog("Post atomic %s\n",spSubRange->brief().c_str());
#endif
		m_spGang->post(spSubRange);
	}

	const SpannedRange::MultiSpan& rNonAtomic=a_spSpannedRange->nonAtomic();
	const U32 spanCount=rNonAtomic.spanCount();

	for(U32 spanIndex=0;spanIndex<spanCount;spanIndex++)
	{
		const SpannedRange::Span& rSpan=rNonAtomic.span(spanIndex);
		const U32 count=rSpan[1]-rSpan[0]+1;

//		const U32 jobs=8*a_threads;	//* TODO param?
		const U32 jobs=a_threads;

		const U32 each=(count>jobs)? (count+jobs-1)/jobs: 1;

		for(U32 m=0;m<count;m+=each)
		{
			const I32 start=rSpan[0]+m;
			I32 end=start+each-1;
			if(end>rSpan[1])
			{
				end=rSpan[1];
			}

#if FE_WKG_DEBUG
			feLog("Post non-atomic [%d,%d]\n",start,end);
#endif
			if(end>=start)
			{
				sp<SpannedRange> spSubRange(new SpannedRange());
				spSubRange->nonAtomic().add(start,end);

				m_spGang->post(spSubRange);
			}
		}
	}

	if(!m_waitForJobs || !m_threadsAlive)
	{
#if FE_WKG_DEBUG
		feLog("Start %d threads\n",a_threads);
#endif
		if(!m_spGang->start(sp<WorkForceI>(this),a_threads,a_stage))
		{
//			feLog("WorkGang::run \"%s\" threading failed"
//					" -> switching to single thread\n",name().c_str());
//			catalog<bool>("AutoThread")=FALSE;
//			a_threads=1;

			//* TODO
#if FALSE
			catalog<String>("warning")+=
					"threading failed -> using single thread;";
#endif

			m_threadsAlive=0;

			const WorkI::Threading wasThreading=m_spWorkI->threading();
			m_spWorkI->setThreading(WorkI::e_singleThread);

			m_spWorkI->run(-1,a_spSpannedRange,a_stage);

			m_spWorkI->setThreading(wasThreading);
			m_spWorkI=NULL;
			return;
		}
		if(m_waitForJobs)
		{
			m_threadsAlive=a_threads;
		}
	}

	if(m_waitForJobs)
	{
#if FE_WKG_DEBUG
		feLog("Post-sync\n");
#endif
		for(U32 m=0;m<m_threadsAlive;m++)
		{
			m_spGang->post(sp<SpannedRange>(NULL));
		}

		m_spGang->synchronize(TRUE);
	}
	else
	{
#if FE_WKG_DEBUG
		feLog("Post-stop\n");
#endif
		stop();
	}

	const WorkI::Threading wasThreading=m_spWorkI->threading();
	m_spWorkI->setThreading(WorkI::e_singleThread);

	if(atomicCount)
	{
		const SpannedRange::MultiSpan& rPostAtomic=
				a_spSpannedRange->postAtomic();
		sp<SpannedRange> spSubRange(new SpannedRange());
		spSubRange->postAtomic()=rPostAtomic;

#if FE_WKG_DEBUG
		feLog("Run post-Atomic %s\n",spSubRange->brief().c_str());
#endif
		m_spWorkI->run(-1,spSubRange,a_stage);
	}

	m_spWorkI->setThreading(wasThreading);
	m_spWorkI=NULL;

	m_spGang->resolveException();

#if FE_WKG_DEBUG
	feLog("WorkGang::run done\n");
#endif
}

void WorkGang::stop(void)
{
	const BWORD cacheWaitForJobs=m_waitForJobs;
	if(cacheWaitForJobs)
	{
		m_waitForJobs=FALSE;
	}

	for(U32 m=0;m<m_threadsAlive;m++)
	{
		m_spGang->post(sp<SpannedRange>(NULL));
	}
	if(m_spGang->workers())
	{
		m_spGang->finish();
	}
	if(cacheWaitForJobs)
	{
		m_waitForJobs=TRUE;
	}
	m_threadsAlive=0;
}

WorkGang::Worker::Worker(sp< JobQueue< sp<SpannedRange> > > a_spJobQueue,
	U32 a_id,String a_stage):
	m_id(a_id),
	m_stage(a_stage),
	m_spJobQueue(a_spJobQueue)
{
#if FE_WKG_DEBUG
	feLog("WorkGang::Worker::Worker %p %p id %d\n",
			this,(Thread::Functor*)this,m_id);
#endif
}

WorkGang::Worker::~Worker(void)
{
#if FE_WKG_DEBUG
	feLog("WorkGang::Worker::~Worker %p %p id %d\n",
			this,(Thread::Functor*)this,m_id);
#endif
}

void WorkGang::Worker::operate(void)
{
#if FE_WKG_DEBUG
	feLog("Starting thread %d\n",m_id);
#endif

	sp<WorkI> spWorkI=m_spWorkForceI->assignment();

	//* TODO confirm this couldn't change while reading
	const BWORD& rWait=m_spWorkForceI->willWaitForJobs();

	BWORD done=FALSE;
	while(!done)
	{
		sp<SpannedRange> spRange;

		if(rWait)
		{
#if FE_WKG_DEBUG
			feLog("Thread %d waiting\n",m_id);
#endif
			m_spJobQueue->waitForJob(spRange);
		}
		else
		{
			done=!m_spJobQueue->take(spRange);
		}

#if FE_WKG_DEBUG
		feLog("Thread %d Job %s\n",m_id,
				spRange.isValid()? spRange->brief().c_str(): "NULL");
#endif

		if(spRange.isNull() || spRange->empty())
		{
			if(rWait)
			{
#if FE_WKG_DEBUG
				feLog("Sync thread %d supervised\n",m_id);
#endif
				m_spJobQueue->synchronize(TRUE);
			}
			else
			{
				done=TRUE;
			}
		}
		else if(!done)
		{
			//* propagate exceptions to parent thread
			try
			{
				spWorkI->run(m_id,spRange,m_stage);
			}
			catch(const Exception &e)
			{
#if FE_WKG_DEBUG
				feLog("WorkGang::Worker::operator()"
						" caught fe::Exception %s\n",c_print(e));
#endif
				m_spJobQueue->relayException(e);
			}
			catch(const std::exception& std_e)
			{
#if FE_WKG_DEBUG
				feLog("WorkGang::Worker::operator()"
						" caught std::exception %s\n",std_e.what());
#endif
				Exception e("stdlib",std_e.what());
				m_spJobQueue->relayException(e);
			}
#ifdef FE_BOOST_EXCEPTIONS
			catch(const boost::exception& boost_e)
			{
#if FE_WKG_DEBUG
				feLog("WorkGang::Worker::operator()"
						" caught boost::exception %s\n",
						boost::diagnostic_information(boost_e).c_str());
#endif
				Exception e("boost",
						boost::diagnostic_information(boost_e).c_str());
				m_spJobQueue->relayException(e);
			}
#endif
			catch(...)
			{
#if FE_WKG_DEBUG
				feLog("WorkGang::Worker::operator()"
						" caught unknown exception\n");
#endif
				Exception e;
				m_spJobQueue->relayException(e);
			}
		}
	}

	//* NOTE TBB may not even start all threads until some finish
/*
#if FE_WKG_DEBUG
	feLog("Sync thread %d\n",m_id);
#endif

	const U32 leader=m_spJobQueue->synchronize();

#if FE_WKG_DEBUG
	feLog("Sync'd thread %d leader=%d\n",m_id,leader);
#endif
*/
}
