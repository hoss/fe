/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __thread_WorkOmp_h__
#define __thread_WorkOmp_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Execute a SpannedRange with OpenMP

	@ingroup thread
*//***************************************************************************/
class FE_DL_EXPORT WorkOmp: public WorkForceI,
	public Initialize<WorkOmp>
{
	public:
					WorkOmp(void)											{}
virtual				~WorkOmp(void)											{}

		void		initialize(void);

virtual	sp<WorkI>	assignment(void)			{ return m_spWorkI; }
virtual	void		run(sp<WorkI> a_spWorkI,U32 a_threads,
							sp<SpannedRange> a_spSpannedRange,String a_stage);
virtual	void		stop(void)												{}

virtual	BWORD		threadsAlive(void) const	{ return 0; }

virtual	BWORD		willWaitForJobs(void) const	{ return FALSE; }
virtual	void		waitForJobs(BWORD a_wait)								{}

static	I32			threadIndex(void);
static	I32			threadCount(void);

	private:

		sp<WorkI>			m_spWorkI;
};

} /* namespace ext */
} /* namespace fe */

#endif // __thread_WorkOmp_h__
