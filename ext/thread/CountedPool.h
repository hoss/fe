/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __thread_CountedPool_h__
#define __thread_CountedPool_h__

#define	FE_CP_DEBUG			FALSE

#define FE_CP_TSS			(FE_USE_TSS && FE_OS!=FE_WIN32 &&				\
							FE_OS!=FE_WIN64 && FE_OS!=FE_OSX)

#define	FE_CP_TSS_CLEANUP	FE_CP_TSS

//#include "boost/thread.hpp"

namespace fe
{
namespace ext
{

class CountedPoolCore
{
	public:
					CountedPoolCore(void)									{}
virtual				~CountedPoolCore(void)									{}

virtual	sp<Counted>	get(void)	{ return sp<Counted>(NULL); }
};

/**************************************************************************//**
	@brief Pool of Counted elements

	@ingroup architecture

	The templated type needs to provide a reset(void) method
	to facilitate reuse of objects (when they are only referenced
	by this container).

	The class is designed to be declared mutable and as such still be
	functional as a member of a const object.
	Objects of this class are non-locking thread-safe by the use of
	thread specific pointers.
	Each thread accesses an independant pool.

	If thread specific pointers are not deemed available or reliable,
	this object may devolve to simple new() allocation.
	For example, boost::thread_specific_ptr can throw thread_resource_error
	if threading is not in use.
	This is caught by CountedPool which will degenerate into no pooling.
*//***************************************************************************/
template<class T>
class CountedPool: public CountedPoolCore
{
	public:
				CountedPool(void)
				{
#if FE_CP_TSS
					try
					{
						m_pTssArray=new boost::thread_specific_ptr
								< Array< sp<T> > >(
#if FE_CP_TSS_CLEANUP
								&cleanup
#endif
								);
#if FE_CP_DEBUG
						feLog("CountedPool<T>::CountedPool %p %p %p\n",
								this,m_pTssArray,m_pTssArray->get());
#endif
					}
					catch(...)
					{
						feLog("CountedPool<T>() TSS failed\n");
						m_pTssArray=NULL;
					}
#else
//					feLog("CountedPool<T>() TSS not used\n");
#endif
				}
				~CountedPool(void)
				{
#if FE_CP_DEBUG
					feLog("CountedPool<T>::~CountedPool %p %p %p\n",
							this,m_pTssArray,m_pTssArray->get());
#endif
					//* TODO shouldn't delete m_pTssArray if any
					//* participating threads are still alive
#if FE_CP_TSS
//					m_pTssArray->reset();
					delete m_pTssArray;
#endif
				}

		sp<Counted>	get(void);

	private:

#if FE_CP_TSS_CLEANUP
static	void	cleanup(Array< sp<T> >* a_pArray);
#endif

#if FE_CP_TSS
		boost::thread_specific_ptr< Array< sp<T> > >*	m_pTssArray;
#endif
};

#if FE_CP_TSS_CLEANUP
template<class T>
void CountedPool<T>::cleanup(Array< sp<T> >* a_pArray)
{
#if FE_CP_DEBUG
	feLog("CountedPool<T>::cleanup %p\n",a_pArray);
#endif
	if(a_pArray)
	{
		a_pArray->clear();
		delete a_pArray;
	}
}
#endif

template<class T>
sp<Counted> CountedPool<T>::get(void)
{
#if FE_CP_DEBUG
	feLog("CountedPool<T>::get %p %p %p\n",
			this,m_pTssArray,m_pTssArray->get());
#endif

#if FE_CP_TSS
	if(m_pTssArray)
		{
		Array< sp<T> >* pArray;
		if(!(pArray=m_pTssArray->get()))
		{
			pArray=new Array< sp<T> >();
			m_pTssArray->reset(pArray);
#if FE_CP_DEBUG
			feLog("CountedPool<T>::get new vector %p %p %p\n",
					this,m_pTssArray,pArray);
#endif
		}
		Array< sp<T> >& array= *pArray;

		//* find an unused slot
		const U32 size=array.size();
		for(U32 m=0;m<size;m++)
		{
			FEASSERT(array[m].isValid());
			FEASSERT(array[m]->count()>0);
#if FE_CP_DEBUG
			feLog("CountedPool<T>::get %p %p %p %d/%d %d\n",
					this,m_pTssArray,pArray,m,size,array[m]->count());
#endif
			if(array[m]->count()>100)
			{
				//* probably uninitialized
				feX(e_notInitialized,"CountedPool<T>::get",
						"excessive reference count"
						" suggests uninitialized data");
			}
			if(array[m]->count()==1)
			{
				array[m]->reset();
				return array[m];
			}
		}

		//* else make a new slot
		array.resize(size+1);
		array[size]=new T();
#if FE_CP_DEBUG
		feLog("CountedPool<T>::get %p NEW %d %d\n",
				this,size,array[size]->count());
#endif
		return array[size];
	}
#endif

	return sp<T>(new T);
}

} /* namespace ext */
} /* namespace fe */

#endif /* __thread_CountedPool_h__ */
