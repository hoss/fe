/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <thread/thread.pmh>

#define FE_TRG_SPAN_DEBUG			FALSE

namespace fe
{
namespace ext
{

void SpannedRange::MultiSpan::add(Real a_start,Real a_end)
{
#if FE_TRG_SPAN_DEBUG
	feLog("SpannedRange::MultiSpan::add %.6G %.6G was %s\n",
			a_start,a_end,brief().c_str());
#endif

	FEASSERT(a_start>=0.0);
	FEASSERT(a_end>=0.0);
	FEASSERT(a_start<=a_end);

	//* common case: single span, serial append
	if(spanCount()==1)
	{
		Span& rSpan=span(0);
		if(rSpan[1]==a_start-1)
		{
#if FE_TRG_SPAN_DEBUG
			feLog(" extend end %.6G to %.6G\n",rSpan[1],a_end);
#endif
			rSpan[1]=a_end;
			return;
		}
	}

	U32 spanIndex;
	for(spanIndex=0;spanIndex<spanCount();spanIndex++)
	{
		Span& rSpan=span(spanIndex);

		FEASSERT(rSpan[0]>=0.0);
		FEASSERT(rSpan[1]>=0.0);
		FEASSERT(rSpan[0]<=rSpan[1]);

		if(a_start<=rSpan[1]+1)
		{
			if(rSpan[0]>a_start)
			{
#if FE_TRG_SPAN_DEBUG
				feLog(" prepend span %d of %.6G %.6G to %.6G\n",
						spanIndex,rSpan[0],rSpan[1],a_start);
#endif

				//* extend beginning
				rSpan[0]=a_start;

				//* merge preceding overlaps
				for(I32 preIndex=spanIndex-1;preIndex>=0;preIndex--)
				{
					Span& rOtherSpan=span(preIndex);
					if(rOtherSpan[1]<rSpan[0]-1)
					{
						break;
					}
					//* assimilate
					rSpan[0]=rOtherSpan[0];

					//* invalidate
					rOtherSpan[0]=0.0;
					rOtherSpan[1]= -1.0;
				}
			}

			if(rSpan[1]<a_end)
			{
#if FE_TRG_SPAN_DEBUG
				feLog(" append span %d of %.6G %.6G to %.6G\n",
						spanIndex,rSpan[0],rSpan[1],a_start,a_end);
#endif

				//* extend end
				rSpan[1]=a_end;

				//* merge following overlaps
				for(U32 postIndex=spanIndex+1;postIndex<spanCount();postIndex++)
				{
					Span& rOtherSpan=span(postIndex);
					if(rOtherSpan[0]>rSpan[1]+1)
					{
						break;
					}
					//* assimilate
					rSpan[1]=rOtherSpan[1];

					//* invalidate
					rOtherSpan[0]=0.0;
					rOtherSpan[1]= -1.0;
				}
			}

#if FE_TRG_SPAN_DEBUG
			feLog(" now %s\n",brief().c_str());
#endif
			//* compact
			U32 writeIndex=0;
			for(U32 readIndex=0;readIndex<spanCount();readIndex++)
			{
				const Span& rReadSpan=span(readIndex);

				if(rReadSpan[0]<=rReadSpan[1])
				{
					span(writeIndex++)=rReadSpan;
				}
			}
			m_spanArray.resize(writeIndex);

#if FE_TRG_SPAN_DEBUG
			feLog(" compact %s\n",brief().c_str());
#endif
			return;
		}
		else if(a_end<rSpan[0])
		{
			//* insert new span before this spanIndex
#if FE_TRG_SPAN_DEBUG
			feLog(" precede span %d new end %.6G < existing start %.6G\n",
					spanIndex,a_end,rSpan[0]);
#endif

			break;
		}
	}

#if FE_TRG_SPAN_DEBUG
	feLog(" insert span %d of %.6G to %.6G\n",spanIndex,a_start,a_end);
#endif

	//* check capacity
	I32 allocation=m_spanArray.capacity();
	if(m_spanArray.size()>=m_spanArray.capacity())
	{
		allocation+=16;	//* TODO param
		m_spanArray.reserve(allocation);

#if FE_TRG_SPAN_DEBUG
		feLog("SpannedRange::MultiSpan::add reserve %d\n",allocation);
#endif
	}

	//* insert new span
	m_spanArray.resize(spanCount()+1);
	for(U32 shiftIndex=spanCount()-1;shiftIndex>spanIndex;shiftIndex--)
	{
		span(shiftIndex)=span(shiftIndex-1);
	}
	FEASSERT(spanIndex<m_spanArray.size());
	set(m_spanArray[spanIndex],a_start,a_end);
}

void SpannedRange::MultiSpan::add(const MultiSpan& rOther)
{
	const U32 otherCount=rOther.spanCount();
	for(U32 m=0;m<otherCount;m++)
	{
		const Span& rOtherSpan=rOther.span(m);
		add(rOtherSpan[0],rOtherSpan[1]);
	}
}

Real SpannedRange::MultiSpan::valueAt(U32 a_valueIndex) const
{
	U32 subIndex=a_valueIndex;

	const U32 size=spanCount();
	for(U32 m=0;m<size;m++)
	{
		const Span& rSpan=span(m);
		const U32 count=rSpan[1]-rSpan[0]+1;
		if(subIndex<count)
		{
			return rSpan[0]+subIndex;
		}
		subIndex-=count;
	}

	return Real(0);
}

U32 SpannedRange::MultiSpan::valueCount(void) const
{
	U32 total=0;

	const U32 size=spanCount();
	for(U32 m=0;m<size;m++)
	{
		const Span& rSpan=span(m);
		total+=rSpan[1]-rSpan[0]+1;
	}

	return total;
}

SpannedRange::MultiSpan& SpannedRange::addAtomic(void)
{
	//* check capacity
	I32 allocation=m_atomArray.capacity();
	if(m_atomArray.size()>=m_atomArray.capacity())
	{
		allocation+=64;	//* TODO param
		m_atomArray.reserve(allocation);

#if FE_TRG_SPAN_DEBUG
		feLog("SpannedRange::addAtomic reserve %d\n",allocation);
#endif
	}

	//* create another MultiSpan
	const U32 index=m_atomArray.size();
	m_atomArray.resize(index+1);
	return m_atomArray[index];
}

U32 SpannedRange::valueCount(void) const
{
	U32 total=m_postAtomic.valueCount()+m_nonAtomic.valueCount();

	const U32 size=m_atomArray.size();
	for(U32 m=0;m<size;m++)
	{
		total+=m_atomArray[m].valueCount();
	}

	return total;
}


sp<SpannedRange> SpannedRange::combineAtoms(I32 a_size) const
{
	sp<SpannedRange> result=sp<SpannedRange>(new SpannedRange());

	result->m_postAtomic=m_postAtomic;
	result->m_nonAtomic=m_nonAtomic;

	if(!atomicCount())
	{
		return result;
	}

	U32 readIndex=0;
	BWORD newAtom=TRUE;
	while(readIndex<atomicCount())
	{
		if(newAtom)
		{
			result->addAtomic().add(atomic(readIndex));

#if FE_TRG_SPAN_DEBUG
			feLog("combine new from %d %s\n",
					readIndex,atomic(readIndex).brief().c_str())
			feLog("  to %s\n",result->brief().c_str());
#endif

			newAtom=FALSE;
			readIndex++;
			continue;
		}

		const U32 writeIndex=result->atomicCount()-1;
		MultiSpan& rWriteAtom=result->atomic(writeIndex);

		if(rWriteAtom.valueCount()>=U32(a_size))
		{
#if FE_TRG_SPAN_DEBUG
			feLog("combine full at %d\n",rWriteAtom.valueCount());
#endif

			newAtom=TRUE;
			continue;
		}

		//* merge with next

		const MultiSpan& rReadAtom=atomic(readIndex);
		rWriteAtom.add(rReadAtom);

#if FE_TRG_SPAN_DEBUG
		feLog("combine merge from %d %s\n",
				readIndex,atomic(readIndex).brief().c_str())
		feLog("  to %s\n",result->brief().c_str());
#endif

		readIndex++;
	}

	return result;
}

void SpannedRange::split(sp<SpannedRange>& rspSpan0,
	sp<SpannedRange>& rspSpan1) const
{
	rspSpan0=new SpannedRange();
	rspSpan1=new SpannedRange();

	const U32 atoms=atomicCount();
	const U32 valueCount=nonAtomic().valueCount();

	if(atoms && valueCount)
	{
		for(U32 atomicIndex=0;atomicIndex<atoms;atomicIndex++)
		{
			rspSpan0->addAtomic().add(atomic(atomicIndex));
		}

		rspSpan1->nonAtomic().add(nonAtomic());

	}
	else if(atoms)
	{
		const U32 halfway=atoms>>1;
		U32 atomicIndex;
		for(atomicIndex=0;atomicIndex<halfway;atomicIndex++)
		{
			rspSpan0->addAtomic().add(atomic(atomicIndex));
		}
		for(;atomicIndex<atoms;atomicIndex++)
		{
			rspSpan1->addAtomic().add(atomic(atomicIndex));
		}
	}
	else
	{
		const SpannedRange::MultiSpan& rMultiSpanOrig=nonAtomic();
		SpannedRange::MultiSpan& rMultiSpan0=rspSpan0->nonAtomic();
		SpannedRange::MultiSpan& rMultiSpan1=rspSpan1->nonAtomic();

		const U32 spanCount=nonAtomic().spanCount();
		if(spanCount>1)
		{
			const U32 halfway=spanCount>>1;
			U32 spanIndex;
			for(spanIndex=0;spanIndex<halfway;spanIndex++)
			{
				rMultiSpan0.add(rMultiSpanOrig.span(spanIndex));
			}
			for(;spanIndex<spanCount;spanIndex++)
			{
				rMultiSpan1.add(rMultiSpanOrig.span(spanIndex));
			}
		}
		else
		{
			FEASSERT(spanCount==1);

			const SpannedRange::Span span=rMultiSpanOrig.span(0);
			FEASSERT(span[1]-span[0]>0.0);

			const U32 spanSize=span[1]-span[0]+1;
			const U32 halfSize=spanSize>>1;
			rMultiSpan0.add(span[0],span[0]+halfSize-1);
			rMultiSpan1.add(span[0]+halfSize,span[1]);
		}
	}
}

void SpannedRange::Iterator::advance(void)
{
	m_spanIndex++;
	m_valueIndex=0;

	MultiSpan* pMultiSpan=(m_atomicIndex==m_atomicCount+1)?
			&m_spSpannedRange->nonAtomic():
			((m_atomicIndex==m_atomicCount)?
			&m_spSpannedRange->postAtomic():
			&m_spSpannedRange->atomic(m_atomicIndex));

	U32 spanCount=pMultiSpan->spanCount();
	while(m_spanIndex==I32(spanCount))
	{
		FEASSERT(m_spanIndex<=I32(spanCount));

		m_atomicIndex++;
		m_spanIndex=0;

		if(m_atomicIndex>m_atomicCount+1)
		{
			return;
		}

		pMultiSpan=(m_atomicIndex==m_atomicCount+1)?
				&m_spSpannedRange->nonAtomic():
				((m_atomicIndex==m_atomicCount)?
				&m_spSpannedRange->postAtomic():
				&m_spSpannedRange->atomic(m_atomicIndex));
		spanCount=pMultiSpan->spanCount();

		if(!spanCount)
		{
			FEASSERT(m_atomicIndex>=m_atomicCount);
			m_atomicIndex++;
		}
	}

	const Span& rSpan=pMultiSpan->span(m_spanIndex);

	m_value=rSpan[0];
	m_valueCount=rSpan[1]-rSpan[0]+1.0;
}

String SpannedRange::MultiSpan::brief(void) const
{
	if(!m_spanArray.size())
	{
		return "<empty>";
	}

	String result;
	const U32 spanCount=m_spanArray.size();
	for(U32 m=0;m<spanCount;m++)
	{
		if(m)
		{
			result.cat(" ");
		}
		const I32 start=m_spanArray[m][0];
		const I32 end=m_spanArray[m][1];
		if(start==end)
		{
			result.catf("%d",start);
		}
		else
		{
			result.catf("%d-%d",start,end);
		}
	}
	return result;

	//* TODO overall min,max if way too many spans
}

String SpannedRange::MultiSpan::dump(void) const
{
	String result;
	const U32 spanCount=m_spanArray.size();
	for(U32 m=0;m<spanCount;m++)
	{
		result.catf("  span %d: %s\n",m,
			c_print(m_spanArray[m]));
	}
	return result;
}

String SpannedRange::brief(void) const
{
	String result;
	const U32 atoms=m_atomArray.size();
	if(atoms==1)
	{
		result.sPrintf("[atomic %s, postAtomic %s, nonAtomic %s]",
				m_atomArray[0].brief().c_str(),
				m_postAtomic.brief().c_str(),
				m_nonAtomic.brief().c_str());
	}
	else
	{
		result.sPrintf("[%d atoms, postAtomic %s, nonAtomic %s]",
				m_atomArray.size(),
				m_postAtomic.brief().c_str(),
				m_nonAtomic.brief().c_str());
	}
	return result;
}

String SpannedRange::dump(void) const
{
	String result;
	const U32 atomCount=m_atomArray.size();
	for(U32 m=0;m<atomCount;m++)
	{
		result.catf("atom %d:\n%s",m,
				m_atomArray[m].dump().c_str());
	}
	result.catf("postAtomic:\n%s",
			m_postAtomic.dump().c_str());
	result.catf("nonAtomic:\n%s",
			m_nonAtomic.dump().c_str());
	return result;
}

} /* namespace ext */
} /* namespace fe */
