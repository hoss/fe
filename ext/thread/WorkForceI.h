/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __thread_WorkForceI_h__
#define __thread_WorkForceI_h__

namespace fe
{
namespace ext
{

class WorkI;

/**************************************************************************//**
	@brief Execute a Range

	@ingroup thread
*//***************************************************************************/
class FE_DL_EXPORT WorkForceI:
	virtual public Component,
	public CastableAs<WorkForceI>
{
	public:
virtual	sp<WorkI>	assignment(void)										=0;

virtual	void		run(sp<WorkI> a_spWorkI,U32 a_threads,
							sp<SpannedRange> a_spSpannedRange,
							String a_stage)									=0;
virtual	void		stop(void)												=0;

virtual	BWORD		threadsAlive(void) const								=0;

virtual	BWORD		willWaitForJobs(void) const								=0;
virtual	void		waitForJobs(BWORD a_wait)								=0;
};

} /* namespace ext */
} /* namespace fe */

#endif // __thread_WorkForceI_h__

