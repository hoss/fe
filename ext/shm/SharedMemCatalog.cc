/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "shm/shm.pmh"

#define FE_SMC_DEBUG			FALSE
#define FE_SMC_VERBOSE			FALSE

namespace fe
{
namespace ext
{

SharedMemCatalog::SharedMemCatalog(void):
	m_usePublicChannel(FALSE),
	m_pReceiverThread(NULL)
{
#if FE_SMC_DEBUG
	feLog("SharedMemCatalog::SharedMemCatalog\n");
#endif
}

SharedMemCatalog::~SharedMemCatalog(void)
{
#if FE_SMC_DEBUG
	feLog("SharedMemCatalog::~SharedMemCatalog\n");
#endif

	disconnect();
}

void SharedMemCatalog::initialize(void)
{
	m_receiverTask.m_pSharedMemCatalog=this;
}

Result SharedMemCatalog::connectAsServer(String a_address,U16 a_port)
{
	const String transport=catalogOrDefault<String>("net:transport","shm");

#if FE_SMC_DEBUG
	feLog("SharedMemCatalog::connectAsServer"
			" transport '%s' address \"%s\" port %d\n",
			transport.c_str(),a_address.c_str(),a_port);
#endif

	Result result=m_receiverTask.connectAsServer(transport,a_address,a_port);
	if(failure(result))
	{
		return result;
	}

	m_pReceiverThread=new Thread(&m_receiverTask);

	return ConnectedCatalog::connectAsServer(a_address,a_port);
}

Result SharedMemCatalog::connectAsClient(String a_address,U16 a_port)
{
	const String transport=catalogOrDefault<String>("net:transport","shm");

#if FE_SMC_DEBUG
	feLog("SharedMemCatalog::connectAsClient"
			" transport '%s' address \"%s\" port %d\n",
			transport.c_str(),a_address.c_str(),a_port);
#endif

	Result result=m_receiverTask.connectAsClient(transport,a_address,a_port);
	if(failure(result))
	{
		return result;
	}

	m_pReceiverThread=new Thread(&m_receiverTask);

	return ConnectedCatalog::connectAsClient(a_address,a_port);
}

void SharedMemCatalog::broadcastSelect(String a_name,String a_property,
	String a_message,
	I32 a_includeCount,const String* a_pIncludes,
	I32 a_excludeCount,const String* a_pExcludes,
	const U8* a_pRawBytes,I32 a_byteCount)
{
#if FE_SMC_VERBOSE
	feLog("SharedMemCatalog::broadcastSelect \"%s\" \"%s\" inc %d exc %d\n",
			a_name.c_str(),a_property.c_str(),
			a_includeCount,a_excludeCount);
#endif

	if(m_usePublicChannel)
	{
		if(a_includeCount>1)
		{
			feLog("SharedMemCatalog::broadcastSelect"
					" \"%s\" \"%s\" multiple includes not supported\n",
					a_name.c_str(),a_property.c_str());
		}
		if(a_excludeCount>1)
		{
			feLog("SharedMemCatalog::broadcastSelect"
					" \"%s\" \"%s\" multiple excludes not supported\n",
					a_name.c_str(),a_property.c_str());
		}
	}

	const BWORD isServer=(role() == "server");
	const I32 recipientCount=
			(m_usePublicChannel && isServer && !a_includeCount)?
			m_identityCount: 1;
	const I32 netRecipients=
			m_usePublicChannel? recipientCount-a_excludeCount: 1;
	if(netRecipients<1)
	{
#if FE_SMC_VERBOSE
		feLog("SharedMemCatalog::broadcastSelect"
				" \"%s\" \"%s\" no net recipients\n",
				a_name.c_str(),a_property.c_str());
#endif

		heartbeat();
		return;
	}

	const BWORD isSignal=catalogOrDefault<bool>(a_name,"signal",false);
	const BWORD isTick=(a_name=="net:tick");
	const BWORD removing=(!isTick && a_message.empty() &&
			!cataloged(a_name,a_property));

	m_receiverTask.writeBacklogs();

//	feLog("SharedMemCatalog::broadcastSelect \"%s\" \"%s\" netRecipients %d\n",
//			a_name.c_str(),a_property.c_str(),
//			netRecipients);

#if FE_SMC_VERBOSE
	if(a_excludeCount)
	{
		feLog("SharedMemCatalog::broadcastSelect exclude \"%s\"\n",
				a_pExcludes[0].c_str());
	}
#endif

	const I32 channelCount=(!isServer)? 1:
			(m_usePublicChannel? 1:
			(a_includeCount? a_includeCount: m_identityCount));
	for(I32 channelIndex=0;channelIndex<channelCount;channelIndex++)
	{
		String channelName;
		if(!isServer)
		{
			channelName="PRIVATE";
		}
		else if(a_includeCount)
		{
			channelName=a_pIncludes[channelIndex];
		}
		else if(!m_usePublicChannel)
		{
			channelName=m_identityArray[channelIndex].m_text;
		}

#if FE_SMC_VERBOSE
		feLog("SharedMemCatalog::broadcastSelect channel %d/%d \"%s\"\n",
				channelIndex,channelCount,channelName.c_str());
#endif

		//* exclude exclusions
		BWORD exclude(FALSE);
		for(I32 excludeIndex=0;excludeIndex<a_excludeCount;excludeIndex++)
		{
			if(channelName==a_pExcludes[excludeIndex])
			{
				exclude=TRUE;
				break;
			}
		}
		if(exclude)
		{
//			feLog("  exclude\n");
			continue;
		}

		if(isServer && a_includeCount)
		{
			//* exclude non-existant identities
			BWORD exists(FALSE);
			for(I32 identityIndex=0;identityIndex<m_identityCount;
					identityIndex++)
			{
				if(channelName==m_identityArray[identityIndex].m_text)
				{
					exists=TRUE;
					break;
				}
			}
			if(!exists)
			{
				feLog("SharedMemCatalog::broadcastSelect"
						" included non-existant identity \"%s\"\n",
						channelName.c_str());
				continue;
			}
		}

		Channel& rChannel=channelName.empty()?
				m_receiverTask.m_publicChannel:
				m_receiverTask.m_privateChannelMap[channelName];

		std::deque<Comm>& rCommBacklog=rChannel.m_commBacklog;

		I32& rCommIndex=
				isServer? rChannel.m_serverIndex: rChannel.m_clientIndex;
		Comm* pComm=isServer?
				&rChannel.m_pShare->m_serverComm[rCommIndex]:
				&rChannel.m_pShare->m_clientComm[rCommIndex];

		const BWORD writeNow=
				(!rCommBacklog.size() && sem_trywait(&pComm->m_semWrite)>=0);

		if(writeNow)
		{
#if FE_SMC_VERBOSE
			feLog("SharedMemCatalog::broadcastSelect writing \"%s\" %d\n",
					channelName.c_str(),
					rCommIndex);
#endif
			rCommIndex=(rCommIndex+1)%FE_SMC_COMM_COUNT;
		}
		else
		{
			const I32 backlogSize(rCommBacklog.size());

#if TRUE//FE_SMC_VERBOSE
			feLog("SharedMemCatalog::broadcastSelect"
					" backlogging \"%s\" %d \"%s\" \"%s\"\n",
					channelName.c_str(),
					backlogSize,a_name.c_str(),a_property.c_str());
#endif

			rCommBacklog.resize(backlogSize+1);
			pComm=&rCommBacklog.back();
		}

		pComm->m_serial=rChannel.m_sendSerial++;

		if(m_usePublicChannel && a_includeCount)
		{
			strncpy(pComm->m_include,a_pIncludes[channelIndex].c_str(),
					sizeof(pComm->m_include)-1);
		}
		else
		{
			pComm->m_include[0]=0;
		}

		if(m_usePublicChannel && a_excludeCount)
		{
			strncpy(pComm->m_exclude,a_pExcludes[channelIndex].c_str(),
					sizeof(pComm->m_exclude)-1);
		}
		else
		{
			pComm->m_exclude[0]=0;
		}

		strcpy(pComm->m_command,
				removing? "remove":
				(isSignal? "signal":
				(isTick? "tick": "update")));
		strncpy(pComm->m_key,a_name.c_str(),sizeof(pComm->m_key)-1);
		strncpy(pComm->m_property,a_property.c_str(),
				sizeof(pComm->m_property)-1);

		String stringValue;
		const U8* pBuffer(NULL);
		I32 bufferCount(0);

		if(!a_message.empty())
		{
			strcpy(pComm->m_type,"string");
			pBuffer=(U8*)a_message.c_str();
			bufferCount=a_message.length();

//			feLog("  send update %d \"%s\" \"%s\" message \"%s\"\n",
//					pComm->m_serial,
//					a_name.c_str(),a_property.c_str(),a_message.c_str());
		}
		else
		{
			const String typeString=catalogTypeName(a_name,a_property);
			strncpy(pComm->m_type,typeString.c_str(),sizeof(pComm->m_type)-1);

			if(typeString=="bytearray")
			{
				Array<U8>& rByteArray=
						catalog< Array<U8> >(a_name,a_property);
				pBuffer=rByteArray.data();
				bufferCount=rByteArray.size();
			}
			else if(useBinaryForType(typeString))
			{
				if(a_pRawBytes)
				{
					pBuffer=a_pRawBytes;
					bufferCount=a_byteCount;
				}
				else
				{
					Array<U8> byteArray;
					catalogBytes(a_name,a_property,byteArray);
					pBuffer=byteArray.data();
					bufferCount=byteArray.size();
				}
			}
			else
			{
				stringValue=catalogValue(a_name,a_property);
				pBuffer=(U8*)stringValue.c_str();
				bufferCount=stringValue.length();
			}

//			feLog("  send update %d \"%s\" \"%s\"\n",
//					pComm->m_serial,
//					a_name.c_str(),a_property.c_str());
		}

		//* TODO multiple blocks instead of truncating
		if(bufferCount>=(I32)sizeof(pComm->m_pData))
		{
			bufferCount=sizeof(pComm->m_pData)-1;
		}
		pComm->m_byteCount=bufferCount;

		memcpy(pComm->m_pData,pBuffer,pComm->m_byteCount);
		pComm->m_pData[pComm->m_byteCount]=0;

		pComm->m_readerCount=recipientCount;

#if FE_SMC_VERBOSE
		feLog("SharedMemCatalog::broadcastSelect"
				" %d x%d \"%s\" \"%s\" \"%s\" \"%s\" %d:\"%s\"\n",
				pComm->m_serial,recipientCount,
				pComm->m_include,pComm->m_key,pComm->m_property,
				pComm->m_type,pComm->m_byteCount,pComm->m_pData);
#endif

		if(isServer && !removing && a_name!="net:flushed")
		{
			//* TODO also properties other than "value"
			catalog<bool>(a_name,"broadcasted")=true;
		}

		heartbeat();

		if(writeNow)
		{
			if(sem_post(&pComm->m_semCount) == -1)
			{
				feLog("SharedMemCatalog::broadcastSelect"
						" failed to post on semaphore 'count'\n");
				return;
			}
			for(I32 recipientIndex=0;recipientIndex<recipientCount;
					recipientIndex++)
			{
				if(sem_post(&pComm->m_semRead) == -1)
				{
					feLog("SharedMemCatalog::broadcastSelect"
							" failed to post on semaphore 'read'\n");
					return;
				}
			}

#if FE_SMC_VERBOSE
			feLog("SharedMemCatalog::broadcastSelect posted\n");
#endif
		}
	}
}

Result SharedMemCatalog::disconnect(void)
{
#if FE_SMC_DEBUG
	feLog("SharedMemCatalog::disconnect\n");
#endif

	Result result=ConnectedCatalog::disconnect();
	if(fe::failure(result))
	{
		return result;
	}

	while(m_receiverTask.writeBacklogs())
	{
#if FE_SMC_DEBUG
		feLog("SharedMemCatalog::disconnect waiting for backlog to write\n");
#endif
		yield();
	}

	m_receiverTask.m_terminate=true;

	if(m_pReceiverThread)
	{
#if FE_SMC_DEBUG
		feLog("SharedMemCatalog::disconnect join\n");
#endif

		if(m_pReceiverThread->joinable())
		{
			m_pReceiverThread->join();
		}

#if FE_SMC_DEBUG
		feLog("SharedMemCatalog::disconnect joined\n");
#endif

		delete m_pReceiverThread;
		m_pReceiverThread=NULL;
	}

	return result;
}

Result SharedMemCatalog::dropConnection(void)
{
#if FE_SMC_DEBUG
	feLog("SharedMemCatalog::dropConnection\n");
#endif

	if(role()=="client")
	{
		m_receiverTask.reset();
	}

	return ConnectedCatalog::dropConnection();
}

Result SharedMemCatalog::removeIdentity(I32 a_index)
{
#if FE_SMC_DEBUG
	feLog("SharedMemCatalog::removeIdentity index %d\n",a_index);
#endif

	//* remove entry from m_privateChannelMap
	const String source(m_identityArray[a_index].m_text);
	m_receiverTask.m_privateChannelMap[source].m_discarded=TRUE;

	m_receiverTask.m_readerDecrementCount++;

	const String shareName(m_receiverTask.m_endpoint+":"+source);

	if(shm_unlink(shareName.c_str()) == -1)
	{
		feLog("SharedMemCatalog::removeIdentity"
				" failed to unlink \"%s\" because '%s'\n",
				shareName.c_str(),
				fe::errorString(FE_ERRNO).c_str());
	}

	return ConnectedCatalog::removeIdentity(a_index);
}

Result SharedMemCatalog::catchUp(Identity& a_rIdentity)
{
	const String serverIndexKey(
			"net:serverIndex["+a_rIdentity.m_text+"]");
	broadcastSelect(serverIndexKey,"value","",1,&a_rIdentity.m_text,0,NULL);

//	feLog("SharedMemCatalog::catchUp serverIndex %d\n",
//			catalog<I32>(serverIndexKey));
//	m_receiverTask.dumpSemaphores(m_publicChannel);

	//* no lasting purpose
	catalogRemove(serverIndexKey);

	Result result=ConnectedCatalog::catchUp(a_rIdentity);

	return result;
}

SharedMemCatalog::ReceiverTask::ReceiverTask(void):
	m_terminate(false),
	m_readerDecrementCount(0),
	m_receivePublic(FALSE)
{
#if FE_SMC_DEBUG
	feLog("SharedMemCatalog::ReceiverTask::ReceiverTask\n");
#endif
}

void SharedMemCatalog::ReceiverTask::reset(void)
{
	m_terminate=false;
	m_readerDecrementCount=0;
	m_receivePublic=FALSE;

	m_publicChannel.reset();
	m_privateChannelMap.clear();
}

SharedMemCatalog::ReceiverTask::~ReceiverTask(void)
{
#if FE_SMC_DEBUG
	feLog("SharedMemCatalog::ReceiverTask::~ReceiverTask\n");
#endif

	if(m_isServer)
	{
#if FE_SMC_DEBUG
		feLog("SharedMemCatalog::ReceiverTask::~ReceiverTask"
				" unlink \"%s\"\n",m_endpoint.c_str());
#endif

		if(shm_unlink(m_endpoint.c_str()) == -1)
		{
			feLog("SharedMemCatalog::ReceiverTask::~ReceiverTask"
					" failed to unlink \"%s\" because '%s'\n",
					m_endpoint.c_str(),
					fe::errorString(FE_ERRNO).c_str());
		}

		for(AutoHashMap<String,Channel>::iterator it=
				m_privateChannelMap.begin();
				it!=m_privateChannelMap.end(); it++)
		{
			const String& source=it->first;
			const String shareName(m_endpoint+":"+source);

#if FE_SMC_DEBUG
			feLog("SharedMemCatalog::ReceiverTask::~ReceiverTask"
					" unlink \"%s\"\n",shareName.c_str());
#endif

			if(shm_unlink(shareName.c_str()) == -1)
			{
				feLog("SharedMemCatalog::ReceiverTask::~ReceiverTask"
						" failed to unlink \"%s\" because '%s'\n",
						shareName.c_str(),
						fe::errorString(FE_ERRNO).c_str());
			}
		}
	}
}

Result SharedMemCatalog::ReceiverTask::startShare(
	String a_shareName,SharedMemCatalog::Share*& a_rpShare,BWORD a_create)
{
	if(a_create)
	{
		feLog("SharedMemCatalog::ReceiverTask::startShare create \"%s\"\n",
				a_shareName.c_str());
	}

	int fp=shm_open(a_shareName.c_str(),
			a_create? O_CREAT|O_RDWR: O_RDWR,
			S_IRUSR|S_IWUSR);
	if(fp == -1)
	{
		feLog("SharedMemCatalog::ReceiverTask::startShare"
				" failed to open \"%s\" because '%s'\n",
				a_shareName.c_str(),
				fe::errorString(FE_ERRNO).c_str());
		return e_refused;
	}

#if FE_SMC_DEBUG
	feLog("SharedMemCatalog::ReceiverTask::startShare bound to %d\n",
			fp);
#endif

	I32 fileSize(0);
	struct stat statbuf;
	if(!fstat(fp,&statbuf))
	{
		fileSize=statbuf.st_size;
	}

	const I32 shareSize=sizeof(struct Share);

#if FE_SMC_DEBUG
	feLog("SharedMemCatalog::ReceiverTask::startShare"
			" existing file size %d vs %d\n",fileSize,shareSize);
#endif

	const BWORD newShare=(fileSize!=shareSize);
	if(newShare)
	{
		if(ftruncate(fp, shareSize) == -1)
		{
			feLog("SharedMemCatalog::ReceiverTask::startShare"
					" failed to resize buffer to %d\n",shareSize);
			return e_refused;
		}
	}

	a_rpShare=(struct Share*)mmap(NULL, sizeof(*a_rpShare),
			PROT_READ|PROT_WRITE, MAP_SHARED, fp, 0);

	//* NOTE safe to close shm file after mmap
	close(fp);
	fp= -1;

	if(a_rpShare == MAP_FAILED)
	{
		feLog("SharedMemCatalog::ReceiverTask::startShare"
				" mmap failed\n");
		return e_refused;
	}

	Connect& rConnect=a_rpShare->m_connect;

	if(newShare)
	{
		if(sem_init(&rConnect.m_semStart, 1, 1) == -1)
		{
			feLog("SharedMemCatalog::ReceiverTask::startShare"
					" failed to init semaphore 'start'\n");
			return e_refused;
		}
	}
	else
	{
#if FE_SMC_DEBUG
		feLog("SharedMemCatalog::ReceiverTask::startShare"
				" wait for semaphore 'start'\n");
#endif
		struct timespec ts;
		if(clock_gettime(CLOCK_REALTIME,&ts) == -1)
		{
			feLog("SharedMemCatalog::ReceiverTask::connectAsClient"
					" failed get time\n");
		}
		ts.tv_sec+=1;	//* wait one second
		if(sem_timedwait(&rConnect.m_semStart,&ts) == -1)
		{
			//* presume it was abandoned in use
			feLog("SharedMemCatalog::ReceiverTask::startShare"
					" failed to wait on semaphore 'start' -> reset\n");
		}
	}

	rConnect.m_identity.reset();

	if(sem_init(&rConnect.m_semClient, 1, 1) == -1)
	{
		feLog("SharedMemCatalog::ReceiverTask::startShare"
				" failed to init semaphore 'client'\n");
		return e_refused;
	}
	if(sem_init(&rConnect.m_semIdentity, 1, 0) == -1)
	{
		feLog("SharedMemCatalog::ReceiverTask::startShare"
				" failed to init semaphore 'identity'\n");
		return e_refused;
	}
	if(sem_init(&rConnect.m_semAck, 1, 0) == -1)
	{
		feLog("SharedMemCatalog::ReceiverTask::startShare"
				" failed to init semaphore 'ack'\n");
		return e_refused;
	}

	for(I32 pass=0;pass<2;pass++)
	{
		for(I32 commIndex=0;commIndex<FE_SMC_COMM_COUNT;commIndex++)
		{
			Comm& rComm=pass?
					a_rpShare->m_clientComm[commIndex]:
					a_rpShare->m_serverComm[commIndex];

			if(sem_init(&rComm.m_semWrite, 1, 1) == -1)
			{
				feLog("SharedMemCatalog::ReceiverTask::startShare"
						" failed to init semaphore 'write'\n");
				return e_refused;
			}
			if(sem_init(&rComm.m_semRead, 1, 0) == -1)
			{
				feLog("SharedMemCatalog::ReceiverTask::startShare"
						" failed to init semaphore 'read'\n");
				return e_refused;
			}
			if(sem_init(&rComm.m_semCount, 1, 0) == -1)
			{
				feLog("SharedMemCatalog::ReceiverTask::startShare"
						" failed to init semaphore 'count'\n");
				return e_refused;
			}
		}
	}

	if(!newShare)
	{
#if FE_SMC_DEBUG
		feLog("SharedMemCatalog::ReceiverTask::startShare"
				" repost semaphore 'start'\n");
#endif

		if(sem_post(&rConnect.m_semStart) == -1)
		{
			feLog("SharedMemCatalog::ReceiverTask::startShare"
					" failed to post on semaphore 'start'\n");
			return e_refused;
		}
	}

#if FE_SMC_DEBUG
	feLog("SharedMemCatalog::ReceiverTask::startShare started \"%s\"\n",
			a_shareName.c_str());
#endif

	return e_ok;
}

Result SharedMemCatalog::ReceiverTask::connectAsServer(
	String a_transport,String a_address,U16 a_port)
{
	m_isServer=TRUE;
	m_endpoint=a_address;

	feLog("SharedMemCatalog::ReceiverTask::connectAsServer \"%s\"\n",
			m_endpoint.c_str());

	const BWORD create(TRUE);
	return startShare(m_endpoint,m_publicChannel.m_pShare,create);
}

Result SharedMemCatalog::ReceiverTask::connectAsClient(
	String a_transport,String a_address,U16 a_port)
{
	m_isServer=FALSE;
	m_endpoint=a_address;

	feLog("SharedMemCatalog::ReceiverTask::connectAsClient \"%s\"\n",
			m_endpoint.c_str());

	while(TRUE)
	{
		I32 openCount(0);
		int fp(-1);
		while(TRUE)
		{
			fp=shm_open(m_endpoint.c_str(), O_RDWR, 0);
			if(fp != -1)
			{
				break;
			}
			if(!openCount++)
			{
				feLog("SharedMemCatalog::ReceiverTask::connectAsClient"
						" waiting to open \"%s\" because '%s'\n",
						m_endpoint.c_str(),
						fe::errorString(FE_ERRNO).c_str());
			}
			m_pSharedMemCatalog->stall();
		}

#if FE_SMC_DEBUG
		feLog("SharedMemCatalog::ReceiverTask::connectAsClient bound to %d\n",
				fp);
#endif

		m_publicChannel.m_pShare=
				(struct Share*)mmap(NULL, sizeof(*m_publicChannel.m_pShare),
				PROT_READ|PROT_WRITE, MAP_SHARED, fp, 0);

		//* NOTE safe to close shm file after mmap
		close(fp);
		fp= -1;

		if(m_publicChannel.m_pShare == MAP_FAILED)
		{
			feLog("SharedMemCatalog::ReceiverTask::connectAsClient"
					" mmap failed\n");
			continue;
		}

		Connect& rConnect=m_publicChannel.m_pShare->m_connect;

#if FE_SMC_DEBUG
		feLog("SharedMemCatalog::ReceiverTask::connectAsClient "
				"try wait on semaphore 'start'\n");
#endif

		if(sem_trywait(&rConnect.m_semStart) == -1)
		{
			feLog("SharedMemCatalog::ReceiverTask::connectAsClient"
					" failed to wait on semaphore 'start'\n");
			m_publicChannel.reset();
			continue;
		}

#if FE_SMC_DEBUG
		feLog("SharedMemCatalog::ReceiverTask::connectAsClient "
				"try wait on semaphore 'client'\n");
#endif

		if(sem_trywait(&rConnect.m_semClient) == -1)
		{
			feLog("SharedMemCatalog::ReceiverTask::connectAsClient"
					" failed to wait on semaphore 'client'\n");

			if(sem_post(&rConnect.m_semStart) == -1)
			{
				feLog("SharedMemCatalog::ReceiverTask::connectAsClient"
						" failed to repost on semaphore 'start'\n");
			}
			m_publicChannel.reset();
			continue;
		}

		pid_t pid=getpid();

		m_identity.m_data[0]=(pid&0xFF000000)>>24;
		m_identity.m_data[1]=(pid&0x00FF0000)>>16;
		m_identity.m_data[2]=(pid&0x0000FF00)>>8;
		m_identity.m_data[3]=(pid&0x000000FF);
		m_identity.m_data[4]=0;
		m_identity.m_text.sPrintf("%d",pid);

		rConnect.m_identity=m_identity;

#if FE_SMC_DEBUG
		feLog("SharedMemCatalog::ReceiverTask::connectAsClient "
				"send identity \"%s\"\n",m_identity.m_text.c_str());
#endif

		if(sem_post(&rConnect.m_semIdentity) == -1)
		{
			feLog("SharedMemCatalog::ReceiverTask::connectAsClient"
					" failed to post on semaphore 'identity'\n");
			m_publicChannel.reset();
			continue;
		}

#if FE_SMC_DEBUG
		feLog("SharedMemCatalog::ReceiverTask::connectAsClient "
				"wait for semaphore 'ack'\n");
#endif

		struct timespec ts;
		if(clock_gettime(CLOCK_REALTIME,&ts) == -1)
		{
			feLog("SharedMemCatalog::ReceiverTask::connectAsClient"
					" failed get time\n");
		}
		ts.tv_sec+=1;	//* wait one second
		if(sem_timedwait(&rConnect.m_semAck,&ts) == -1)
		{
			feLog("SharedMemCatalog::ReceiverTask::connectAsClient"
					" failed to wait on semaphore 'ack'\n");

			if(sem_post(&rConnect.m_semClient) == -1)
			{
				feLog("SharedMemCatalog::ReceiverTask::connectAsClient"
						" failed to repost on semaphore 'client'\n");
			}

			if(sem_post(&rConnect.m_semStart) == -1)
			{
				feLog("SharedMemCatalog::ReceiverTask::connectAsClient"
						" failed to repost on semaphore 'start'\n");
			}
			m_publicChannel.reset();
			continue;
		}

		if(sem_post(&rConnect.m_semStart) == -1)
		{
			feLog("SharedMemCatalog::ReceiverTask::connectAsClient"
					" failed to repost on semaphore 'start'\n");
		}

		break;
	}

#if FE_SMC_DEBUG
	feLog("SharedMemCatalog::ReceiverTask::connectAsClient"
			" connection acknowledged\n");
#endif

	//* NOTE source of 1-to-1 updates from server is "PRIVATE" instead of pid
	Share*& rpPrivateShare=m_privateChannelMap["PRIVATE"].m_pShare;

	const String shareName(m_endpoint+":"+m_identity.m_text);
	const BWORD create(FALSE);
	Result result=startShare(shareName,rpPrivateShare,create);
	if(failure(result))
	{
		feLog("SharedMemCatalog::ReceiverTask::connectAsClient"
				" failed to start share \"%s\"\n",shareName.c_str());
	}

	return result;
}

I32 SharedMemCatalog::ReceiverTask::writeBacklogs(void)
{
	I32 remaining(0);

	for(I32 pass=0;pass<2;pass++)
	{
		for(AutoHashMap<String,Channel>::iterator it=
				m_privateChannelMap.begin();
				it!=m_privateChannelMap.end(); it++)
		{
//			const String& source=it->first;
			Channel& rPrivateChannel=it->second;

			remaining+=writeBacklog(rPrivateChannel);
		}

		remaining+=writeBacklog(m_publicChannel);
	}

	return remaining;
}

I32 SharedMemCatalog::ReceiverTask::writeBacklog(Channel& a_rChannel)
{
	std::deque<Comm>& rCommBacklog=a_rChannel.m_commBacklog;

	if(!rCommBacklog.size())
	{
		return 0;
	}

	if(!m_pSharedMemCatalog)
	{
		feLog("SharedMemCatalog::ReceiverTask::operate"
				" SharedMemCatalog disappeared\n");
		return rCommBacklog.size();
	}

	while(rCommBacklog.size())
	{
		Comm* pBackLogComm=&rCommBacklog.front();

		const BWORD usePublicChannel=m_pSharedMemCatalog->m_usePublicChannel;
		const I32 recipientCount=
				(usePublicChannel && m_isServer &&
				pBackLogComm->m_include[0]==0)?
				m_pSharedMemCatalog->m_identityCount: 1;

		const I32 excludeCount=(pBackLogComm->m_exclude[0]!=0);
		const I32 netRecipients=
				usePublicChannel? recipientCount-excludeCount: 1;
		if(netRecipients<1)
		{
#if FE_SMC_VERBOSE
			feLog("SharedMemCatalog::ReceiverTask::writeBacklog"
					" \"%s\" \"%s\" no net recipients\n",
					pBackLogComm->m_key,pBackLogComm->m_property);
#endif
			rCommBacklog.pop_front();
			continue;
		}

		I32& rCommIndex=
				m_isServer? a_rChannel.m_serverIndex: a_rChannel.m_clientIndex;
		Comm* pComm=m_isServer?
				&a_rChannel.m_pShare->m_serverComm[rCommIndex]:
				&a_rChannel.m_pShare->m_clientComm[rCommIndex];

		if(sem_trywait(&pComm->m_semWrite)<0)
		{
#if FE_SMC_VERBOSE
			feLog("SharedMemCatalog::ReceiverTask::writeBacklog"
					" can not write to \"%s\" index %d\n",
					pBackLogComm->m_include,rCommIndex);

			dumpSemaphores(a_rChannel);
#endif
			break;
		}

#if FE_SMC_VERBOSE
		feLog("SharedMemCatalog::ReceiverTask::operate"
				" sending backlog \"%s\" '%s' \"%s\" \"%s\" \"%s\" to %d\n",
				pComm->m_include,pComm->m_command,
				pComm->m_key,pComm->m_property,pComm->m_type,
				rCommIndex);
#endif

		rCommIndex=(rCommIndex+1)%FE_SMC_COMM_COUNT;

		pComm->m_serial=pBackLogComm->m_serial;

		strcpy(pComm->m_include,	pBackLogComm->m_include);
		strcpy(pComm->m_exclude,	pBackLogComm->m_exclude);
		strcpy(pComm->m_command,	pBackLogComm->m_command);
		strcpy(pComm->m_key,		pBackLogComm->m_key);
		strcpy(pComm->m_property,	pBackLogComm->m_property);
		strcpy(pComm->m_type,		pBackLogComm->m_type);
		memcpy(pComm->m_pData,		pBackLogComm->m_pData,
				pBackLogComm->m_byteCount+1);
		pComm->m_byteCount=pBackLogComm->m_byteCount;

		rCommBacklog.pop_front();

		if(sem_post(&pComm->m_semCount) == -1)
		{
			feLog("SharedMemCatalog::ReceiverTask::operate"
					" failed to post on semaphore 'count'\n");
			break;
		}

		pComm->m_readerCount=recipientCount;
		for(I32 recipientIndex=0;recipientIndex<recipientCount;
				recipientIndex++)
		{
			if(sem_post(&pComm->m_semRead) == -1)
			{
				feLog("SharedMemCatalog::ReceiverTask::operate"
						" failed to post on semaphore 'read'\n");
				break;
			}
		}
	}

	return rCommBacklog.size();
}

void SharedMemCatalog::ReceiverTask::operate(void)
{
#if FE_SMC_DEBUG
	BWORD wasConnected=FALSE;
#endif

	while(TRUE)
	{
#if FE_SMC_VERBOSE
//		feLog("SharedMemCatalog::ReceiverTask::operate checking for message\n");
#endif

		if(!m_pSharedMemCatalog)
		{
			feLog("SharedMemCatalog::ReceiverTask::operate"
					" SharedMemCatalog disappeared\n");
			return;
		}

		Connect& rConnect=m_publicChannel.m_pShare->m_connect;

		if(m_isServer && sem_trywait(&rConnect.m_semIdentity)>=0)
		{
#if FE_SMC_VERBOSE
			feLog("SharedMemCatalog::ReceiverTask::operate check identity\n");
#endif

			Identity identity=rConnect.m_identity;
			rConnect.m_identity.reset();

			I32& rIdentityCount=m_pSharedMemCatalog->m_identityCount;
			Identity *const pIdentityArray=m_pSharedMemCatalog->m_identityArray;
			I32 identityIndex=0;
			for(;identityIndex<rIdentityCount;identityIndex++)
			{
				if(identity==pIdentityArray[identityIndex])
				{
					break;
				}
			}

			if(identityIndex>=rIdentityCount)
			{
				m_pSharedMemCatalog->safeLock();

				Identity& rNewIdentity=pIdentityArray[rIdentityCount];
				rNewIdentity=identity;

#if FE_SMC_DEBUG
#endif
				feLog("SharedMemCatalog::ReceiverTask::operate"
						" new identity %d %s\n",
						rIdentityCount,rNewIdentity.m_text.c_str());

				Share*& rpReceiveShare=
						m_privateChannelMap[identity.m_text].m_pShare;
				const String shareName(m_endpoint+":"+identity.m_text);
				const BWORD create(TRUE);
				Result result=startShare(shareName,
						rpReceiveShare,create);
				if(failure(result))
				{
					feLog("SharedMemCatalog::ReceiverTask::operate"
							" failed to start share \"%s\"\n",
							shareName.c_str());
				}

				rIdentityCount++;

				//* store serverIndex now, before anything else is added
				const String serverIndexKey(
						"net:serverIndex["+identity.m_text+"]");
				m_pSharedMemCatalog->catalog<I32>(serverIndexKey)=
						m_pSharedMemCatalog->m_receiverTask
						.m_publicChannel.m_serverIndex;

				m_pSharedMemCatalog->safeUnlock();
			}

			if(sem_post(&rConnect.m_semAck) == -1)
			{
				feLog("SharedMemCatalog::ReceiverTask::operate"
						" failed to post on semaphore 'ack'\n");
				continue;
			}

#if FE_SMC_DEBUG
			feLog("SharedMemCatalog::ReceiverTask::operate"
					" connected with \"%s\"\n",identity.m_text.c_str());
#endif

			if(sem_post(&rConnect.m_semClient) == -1)
			{
				feLog("SharedMemCatalog::ReceiverTask::operate"
						" failed to post on semaphore 'client'\n");
				continue;
			}
		}

		if(!m_pSharedMemCatalog->started())
		{
			feLog("not started\n");
			m_pSharedMemCatalog->yield();
			continue;
		}

		if(!m_pSharedMemCatalog->connected())
		{
			m_pSharedMemCatalog->timerRestart();
		}

		const BWORD isServer=(m_pSharedMemCatalog->role() == "server");

		if(!isServer && m_pSharedMemCatalog->connected() &&
				m_pSharedMemCatalog->timerReached())
		{
			m_pSharedMemCatalog->dropConnection();
			return;
		}

#if FE_SMC_DEBUG
		const BWORD nowConnected=m_pSharedMemCatalog->connected();
		if(wasConnected && !nowConnected)
		{
			feLog("SharedMemCatalog::ReceiverTask::operate"
					" disconnect detected\n");
		}
		wasConnected=nowConnected;
#endif

		const I32 decrementCount=
				m_readerDecrementCount.exchange(0,std::memory_order_relaxed);
		for(I32 index=0;index<decrementCount;index++)
		{
			decrementReaderCounts();
		}

		if(m_terminate.load(std::memory_order_relaxed))
		{
#if FE_SMC_DEBUG
			feLog("SharedMemCatalog::ReceiverTask::operate"
					" terminate detected\n");
#endif
			return;
		}

		//* try to write from backlog
		if(m_publicChannel.m_commBacklog.size())
		{
			m_pSharedMemCatalog->safeLock();

			writeBacklogs();

			m_pSharedMemCatalog->safeUnlock();
		}

		BWORD received(FALSE);
		for(AutoHashMap<String,Channel>::iterator it=
				m_privateChannelMap.begin();
				it!=m_privateChannelMap.end(); it++)
		{
			const String& source=it->first;
			Channel& rPrivateChannel=it->second;

			I32& rCommIndex=m_isServer?
					rPrivateChannel.m_clientIndex:
					rPrivateChannel.m_serverIndex;

			Comm& rComm=m_isServer?
					rPrivateChannel.m_pShare->m_clientComm[rCommIndex]:
					rPrivateChannel.m_pShare->m_serverComm[rCommIndex];

//			feLog("read private index %d\n",rCommIndex);

			if(receiveUpdate(m_isServer? source: "",rPrivateChannel,rComm))
			{
				rCommIndex=(rCommIndex+1)%FE_SMC_COMM_COUNT;
				received=TRUE;
			}
		}

		if(m_receivePublic)
		{
			I32& rCommIndex=m_isServer?
					m_publicChannel.m_clientIndex:
					m_publicChannel.m_serverIndex;

			Comm& rComm=m_isServer?
					m_publicChannel.m_pShare->m_clientComm[rCommIndex]:
					m_publicChannel.m_pShare->m_serverComm[rCommIndex];

//			feLog("read public index %d\n",rCommIndex);

			if(receiveUpdate("",m_publicChannel,rComm))
			{
				rCommIndex=(rCommIndex+1)%FE_SMC_COMM_COUNT;
				received=TRUE;
			}
		}

		if(!received)
		{
			m_pSharedMemCatalog->yield();
		}
//		else
//		{
//			feLog("data received, connected %d\n",
//					m_pSharedMemCatalog->connected());
//		}

		//* remove private channels marked as discarded
		for(AutoHashMap<String,Channel>::iterator it=
				m_privateChannelMap.begin();
				it!=m_privateChannelMap.end();)
		{
			Channel& rPrivateChannel=it->second;
			if(rPrivateChannel.m_discarded)
			{
				AutoHashMap<String,Channel>::iterator pos=it++;
				m_privateChannelMap.erase(pos);
				continue;
			}
			it++;
		}
	}
}

BWORD SharedMemCatalog::ReceiverTask::receiveUpdate(
	const String& a_source,SharedMemCatalog::Channel& a_rChannel,
	SharedMemCatalog::Comm& a_rComm)
{
	if(sem_trywait(&a_rComm.m_semRead)<0)
	{
		return FALSE;
	}

	//* NOTE Comm level include/exclude only used with m_usePublicChannel

	BWORD excluded(FALSE);

	String includeId(a_rComm.m_include);
	if(!includeId.empty() && includeId!=m_identity.m_text)
	{
//		feLog("SharedMemCatalog::ReceiverTask::receiveUpdate"
//				" does not match include\n");
		excluded=TRUE;
	}

	String excludeId(a_rComm.m_exclude);
	if(!excludeId.empty() && excludeId==m_identity.m_text)
	{
//		feLog("SharedMemCatalog::ReceiverTask::receiveUpdate"
//				" matches exclude \"%s\"\n",m_identity.m_text);

		excluded=TRUE;
	}

	if(sem_wait(&a_rComm.m_semCount) == -1)
	{
		feLog("SharedMemCatalog::ReceiverTask::receiveUpdate"
				" failed to wait on semaphore 'count'\n");
		return FALSE;
	}

	if(a_rChannel.m_receiveSerial>=a_rComm.m_serial)
	{
#if TRUE//FE_SMC_VERBOSE
		feLog("SharedMemCatalog::ReceiverTask::receiveUpdate"
				" repeat read prevented (%d >= %d)\n",
				a_rChannel.m_receiveSerial,a_rComm.m_serial);
#endif

		dumpSemaphores(a_rChannel);

		if(sem_post(&a_rComm.m_semRead) == -1)
		{
			feLog("SharedMemCatalog::ReceiverTask::receiveUpdate"
					" failed to post on semaphore 'read'\n");
			return FALSE;
		}

		if(sem_post(&a_rComm.m_semCount) == -1)
		{
			feLog("SharedMemCatalog::ReceiverTask::receiveUpdate"
					" failed to post on semaphore 'count'\n");
			return FALSE;
		}

		return FALSE;
	}

	if(!excluded)
	{
		String command(a_rComm.m_command);
		String key(a_rComm.m_key);
		String property(a_rComm.m_property);
		String typeString(a_rComm.m_type);

#if FE_SMC_VERBOSE
		feLog("SharedMemCatalog::ReceiverTask::receiveUpdate"
				" reading from \"%s\" command \"%s\"\n",
				a_source.c_str(),command.c_str());
#endif

		if(!m_pSharedMemCatalog)
		{
			feLog("SharedMemCatalog::ReceiverTask::operate"
					" SharedMemCatalog disappeared\n");
			return TRUE;
		}

		m_pSharedMemCatalog->timerRestart();

		const BWORD isSignal=(command=="signal");
		if(isSignal || command=="update")
		{
			if(m_pSharedMemCatalog->useBinaryForType(typeString))
			{
				m_pSharedMemCatalog->update(isSignal? e_signal: e_update,
						a_source,key,property,typeString,"",
						a_rComm.m_pData,a_rComm.m_byteCount);
			}
			else
			{
				const String valueString(a_rComm.m_pData);

#if FE_SMC_VERBOSE
				feLog("SharedMemCatalog::ReceiverTask::receiveUpdate"
						" \"%s\" \"%s\" \"%s\" source \"%s\"\n  \"%s\"\n",
						key.c_str(),property.c_str(),typeString.c_str(),
						a_source.c_str(),valueString.c_str());
#endif

				m_pSharedMemCatalog->update(isSignal? e_signal: e_update,
						a_source,key,property,
						typeString,valueString);

				if(!strncmp(key.c_str(),"net:serverIndex[",16))
				{
					m_publicChannel.m_serverIndex=
							m_pSharedMemCatalog->catalog<I32>(key);
					m_receivePublic=TRUE;

//					feLog("SharedMemCatalog::ReceiverTask::receiveUpdate"
//							"serverIndex %d\n",m_publicChannel.m_serverIndex);
//					dumpSemaphores(a_rChannel);

					//* no lasting purpose
					m_pSharedMemCatalog->catalogRemove(key);
				}
			}
		}
		else if(command=="remove")
		{
			m_pSharedMemCatalog->update(e_remove,a_source,key,property,"","");
		}
		else if(command=="tick")
		{
//			feLog("TICK\n");
		}
		else
		{
			feLog("SharedMemCatalog::ReceiverTask::receiveUpdate"
					" unknown command \"%s\"\n",command.c_str());
		}

#if FE_SMC_VERBOSE
		feLog("SharedMemCatalog::ReceiverTask::receiveUpdate"
				" readerCount %d serial %d -> %d\n",
				a_rComm.m_readerCount,
				a_rChannel.m_receiveSerial,a_rComm.m_serial);

//		dumpSemaphores(a_rChannel);
#endif
	}

	a_rChannel.m_receiveSerial=a_rComm.m_serial;

	if(--a_rComm.m_readerCount)
	{
		//* NOTE more readers await
		if(sem_post(&a_rComm.m_semCount) == -1)
		{
			feLog("SharedMemCatalog::ReceiverTask::receiveUpdate"
					" failed to post on semaphore 'count'\n");
			return TRUE;
		}
	}
	else
	{
		//* NOTE last reader activates the write semaphore
		if(sem_post(&a_rComm.m_semWrite) == -1)
		{
			feLog("SharedMemCatalog::ReceiverTask::receiveUpdate"
					" failed to post on semaphore 'write'\n");
			return TRUE;
		}
	}

	return TRUE;
}

void SharedMemCatalog::ReceiverTask::decrementReaderCounts(void)
{
	for(AutoHashMap<String,Channel>::iterator it=
			m_privateChannelMap.begin();
			it!=m_privateChannelMap.end(); it++)
	{
//		const String& source=it->first;
		Channel& rPrivateChannel=it->second;

		for(I32 commIndex=0;commIndex<FE_SMC_COMM_COUNT;commIndex++)
		{
//			feLog("decrementReaderCount \"%s\" %d\n",
//					it->first.c_str(),commIndex);

			Comm& rComm=rPrivateChannel.m_pShare->m_serverComm[commIndex];
			decrementReaderCount(rComm);
		}
	}

	for(I32 commIndex=0;commIndex<FE_SMC_COMM_COUNT;commIndex++)
	{
//		feLog("decrementReaderCount public %d\n", commIndex);

		Comm& rComm=m_publicChannel.m_pShare->m_serverComm[commIndex];
		decrementReaderCount(rComm);
	}
}

//* degenerate of receiveUpdate()
BWORD SharedMemCatalog::ReceiverTask::decrementReaderCount(
	SharedMemCatalog::Comm& a_rComm)
{
#if FALSE
	int writeValue(-1);
	int readValue(-1);
	int countValue(-1);

	sem_getvalue(&a_rComm.m_semWrite,&writeValue);
	sem_getvalue(&a_rComm.m_semRead,&readValue);
	sem_getvalue(&a_rComm.m_semCount,&countValue);

	feLog("SharedMemCatalog::ReceiverTask::decrementReaderCount"
			" sem write %d read %d count %d\n",
			writeValue,readValue,countValue);
#endif

	if(sem_trywait(&a_rComm.m_semRead)<0)
	{
		return FALSE;
	}

	if(sem_wait(&a_rComm.m_semCount) == -1)
	{
		feLog("SharedMemCatalog::ReceiverTask::decrementReaderCount"
				" failed to wait on semaphore 'count'\n");
		return FALSE;
	}

	feLog("SharedMemCatalog::ReceiverTask::decrementReaderCount"
			" m_readerCount %d\n",a_rComm.m_readerCount);

	if(--a_rComm.m_readerCount)
	{
		//* NOTE more readers await
		if(sem_post(&a_rComm.m_semCount) == -1)
		{
			feLog("SharedMemCatalog::ReceiverTask::decrementReaderCount"
					" failed to post on semaphore 'count'\n");
			return TRUE;
		}
	}
	else
	{
		//* NOTE last reader activates the write semaphore
		if(sem_post(&a_rComm.m_semWrite) == -1)
		{
			feLog("SharedMemCatalog::ReceiverTask::receiveUpdate"
					" failed to post on semaphore 'write'\n");
			return TRUE;
		}
	}

	return TRUE;
}

void SharedMemCatalog::ReceiverTask::dumpSemaphores(
	SharedMemCatalog::Channel& a_rChannel)
{
	for(I32 commIndex=0;commIndex<FE_SMC_COMM_COUNT;commIndex++)
	{
		Comm& rComm=a_rChannel.m_pShare->m_serverComm[commIndex];

		int writeValue(-1);
		int readValue(-1);
		int countValue(-1);

		sem_getvalue(&rComm.m_semWrite,&writeValue);
		sem_getvalue(&rComm.m_semRead,&readValue);
		sem_getvalue(&rComm.m_semCount,&countValue);

		feLog("SharedMemCatalog::ReceiverTask::dumpSemaphores"
				" %d/%d write %d read %d count %d ser %d \"%s\"\n",
				commIndex,FE_SMC_COMM_COUNT,
				writeValue,readValue,countValue,
				rComm.m_serial,rComm.m_key);
	}
}

} /* namespace ext */
} /* namespace fe */

