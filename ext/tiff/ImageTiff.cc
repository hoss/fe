/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <tiff/tiff.pmh>

namespace fe
{
namespace ext
{

ImageTiff::ImageTiff(void)
{
}

ImageTiff::~ImageTiff(void)
{
}

BWORD ImageTiff::save(String filename)
{
	if(!filename.match(".*\\.tif") && !filename.match(".*\\.tiff"))
	{
		feLog("ImageTiff::save can only save .tif or .tiff files\n");
		return FALSE;
	}

	//* TODO
	const U32 samplesPerPixel=3;

	TIFF* pTiff=TIFFOpen(filename.c_str(),"w");
	if(!pTiff)
	{
		return FALSE;
	}

	TIFFSetField(pTiff,TIFFTAG_IMAGEWIDTH,width());
	TIFFSetField(pTiff,TIFFTAG_IMAGELENGTH,height());
	TIFFSetField(pTiff,TIFFTAG_SAMPLESPERPIXEL,samplesPerPixel);
	TIFFSetField(pTiff,TIFFTAG_BITSPERSAMPLE,8);
	TIFFSetField(pTiff,TIFFTAG_ORIENTATION,ORIENTATION_TOPLEFT);
	TIFFSetField(pTiff,TIFFTAG_ROWSPERSTRIP,
			TIFFDefaultStripSize(pTiff,width()*samplesPerPixel));

	//* ???
	TIFFSetField(pTiff,TIFFTAG_PLANARCONFIG,PLANARCONFIG_CONTIG);
	TIFFSetField(pTiff,TIFFTAG_PHOTOMETRIC,PHOTOMETRIC_RGB);

	tsize_t lineBytes=samplesPerPixel*width();

	unsigned char* pBuffer=
			(unsigned char *)_TIFFmalloc(TIFFScanlineSize(pTiff));
	if(!pBuffer)
	{
		TIFFClose(pTiff);
		return FALSE;
	}

	const unsigned char* pData=(unsigned char *)raw();
	if(!pData)
	{
		TIFFClose(pTiff);
		delete pBuffer;
		return FALSE;
	}

	const U32 rows=height();
	for(U32 row=0;row<rows;row++)
	{
		memcpy(pBuffer,&pData[(rows-row-1)*lineBytes],lineBytes);

		if (TIFFWriteScanline(pTiff,pBuffer,row,0) < 0)
		{
			break;
		}
	}

	TIFFClose(pTiff);
	delete pBuffer;

	return TRUE;
}

} /* namespace ext */
} /* namespace fe */