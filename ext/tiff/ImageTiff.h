/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __tiff_ImageTiff_h__
#define __tiff_ImageTiff_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
    @brief Image handling using TIFF

	@ingroup tiff

	http://www.remotesensing.org/libtiff
*//***************************************************************************/
class FE_DL_EXPORT ImageTiff: public ImageRaw
{
	public:
				ImageTiff(void);
virtual			~ImageTiff(void);

				//* As ImageI
virtual	BWORD	save(String filename);
};

} /* namespace ext */
} /* namespace fe */

#endif /* __tiff_ImageTiff_h__ */
