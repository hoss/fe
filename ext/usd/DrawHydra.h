/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __usd_DrawHydra_h__
#define __usd_DrawHydra_h__
namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Hydra-specific replacements for DrawI

	@ingroup usd

	Features not implemented using Hydra may fall back on DrawOpenGL.
*//***************************************************************************/
class FE_DL_EXPORT DrawHydra: public DrawOpenGL,
		public Initialize<DrawHydra>
{
	public:
						DrawHydra();
virtual					~DrawHydra();

		void			initialize(void);

virtual	void			flush(void);
virtual	sp<DrawBufferI>	createBuffer(void);

						using DrawOpenGL::drawPoints;

virtual	void			drawPoints(const SpatialVector *vertex,
								const SpatialVector *normal,U32 vertices,
								BWORD multicolor,const Color *color,
								sp<DrawBufferI> spDrawBuffer);

						using DrawOpenGL::drawTriangles;

virtual	void			drawTriangles(const SpatialVector *vertex,
								const SpatialVector *normal,
								const Vector2 *texture,U32 vertices,
								StripMode strip,BWORD multicolor,
								const Color *color,
								const Array<I32>* vertexMap,
								const Array<I32>* hullPointMap,
								const Array<Vector4i>* hullFacePoint,
								sp<DrawBufferI> spDrawBuffer);

						//* NOTE can't use DrawOpenGL versions of
						//* transformed draw that leverage glMultMatrix

						using DrawCommon::drawTransformedPoints;

virtual void			drawTransformedPoints(
								const SpatialTransform &transform,
								const SpatialVector *scale,
								const SpatialVector *vertex,
								const SpatialVector *normal,U32 vertices,
								BWORD multicolor,const Color *color,
								sp<DrawBufferI> spDrawBuffer);

						using DrawCommon::drawTransformedLines;

virtual void			drawTransformedLines(
								const SpatialTransform &transform,
								const SpatialVector *scale,
								const SpatialVector *vertex,
								const SpatialVector *normal,
								U32 vertices,StripMode strip,
								BWORD multicolor,const Color *color,
								BWORD multiradius,const Real *radius,
								const Vector3i *element,U32 elementCount,
								sp<DrawBufferI> spDrawBuffer);

							using DrawCommon::drawTransformedTriangles;

virtual void			drawTransformedTriangles(
								const SpatialTransform &transform,
								const SpatialVector *scale,
								const SpatialVector *vertex,
								const SpatialVector *normal,
								const Vector2 *texture,
								U32 vertices,StripMode strip,BWORD multicolor,
								const Color *color,
								const Array<I32>* vertexMap,
								const Array<I32>* hullPointMap,
								const Array<Vector4i>* hullFacePoint,
								sp<DrawBufferI> spDrawBuffer);

	protected:

static	GfMatrix4f		convertToGfMatrix4f(
								const SpatialTransform& rTransform);
static	GfMatrix4d		convertToGfMatrix4d(
								const SpatialTransform& rTransform);

		void			createMaterial1(void);
		void			createMaterial2(void);

virtual	void			drawLinesInternal(const SpatialVector *vertex,
								const SpatialVector *normal,
								const Vector2 *texture,U32 vertices,
								StripMode strip,
								BWORD multicolor,const Color *color,
								BWORD multiradius,const Real *radius,
								const Vector3i *element,U32 elementCount,
								sp<DrawBufferI> spDrawBuffer);

virtual	void			drawPolys(U32 grain,const SpatialVector *vertex,
								const SpatialVector *normal,
								const Vector2 *texture,
								U32 vertices,StripMode strip,
								BWORD multicolor,const Color *color,
								const Array<I32>* vertexMap,
								const Array<I32>* hullPointMap,
								const Array<Vector4i>* hullFacePoint,
								sp<DrawBufferI> spDrawBuffer);

	class Buffer:
		public DrawOpenGL::Buffer,
		public CastableAs<Buffer>
	{
		public:
						Buffer(void);
	virtual				~Buffer(void);
			void		releaseAll(void);

			HydraDriver*	m_pHydraDriver;
			String			m_nodeName;
			BWORD			m_current;
			I32				m_recent;
			I32				m_vertices;
			BWORD			m_multicolor;
			Color			m_firstColor;
	};

	private:

static	String			convertName(String a_name);
		void			ensureDriver(void);
		void			updateCameraAndLighting(void);
		void			updateNode(sp<Buffer> spDrawBuffer);
		void			drawNodes(void);

		HdEngine			m_engine;
		HydraDriver*		m_pHydraDriver;
		I32					m_refineLevel;
		Real				m_alphaMin;
		I32					m_shadowResolution;
		Real				m_shadowBias;

		std::map<String, sp<Buffer> >	m_bufferMap;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __usd_DrawHydra_h__ */
