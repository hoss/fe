#! /bin/bash

# git clone https://github.com/PixarAnimationStudios/USD

USE_TBB_INC=$PWD/tbb/include
USE_TBB_LIB=$PWD/tbb/lib

USE_ABC=$PWD/alembic
USE_INSTALL=$PWD/USD/install

export OPENEXR_LOCATION=$PWD/OpenEXR
export OPENSUBDIV_ROOT_DIR=$PWD/../opensubdiv/OpenSubdiv
export PTEX_LOCATION=$PWD/../ptex/ptex-2.0.41/install

export OIIO_LOCATION=$PWD/../oiio/oiio/dist/linux64

#export BOOST_ROOT=$PWD/boost
export BOOST_ROOT=/opt/boost1.55.0

#export CMAKE_CXX_COMPILER=g++
#export CMAKE_CXX_FLAGS="-std=c++98"
export CMAKE_CXX_FLAGS="-D_GLIBCXX_USE_CXX11_ABI=0"
#export CMAKE_CXX_FLAGS="-fabi-version=2 -fabi-compat-version=10"

mkdir -p USD/build
cd USD/build

rm CMakeCache.txt

cmake																	\
	-D CMAKE_INSTALL_PREFIX:PATH=$USE_INSTALL							\
	-D PXR_ENABLE_NAMESPACES:BOOL=OFF									\
	-D PXR_BUILD_ALEMBIC_PLUGIN:BOOL=OFF								\
	-D PXR_BUILD_TESTS:BOOL=OFF											\
	-D PXR_ENABLE_PTEX_SUPPORT:BOOL=OFF									\
	-D CMAKE_VERBOSE_MAKEFILE:BOOL=TRUE									\
	-D CMAKE_CXX_FLAGS:STRING="$CMAKE_CXX_FLAGS"						\
	-D TBB_INCLUDE_DIRS:PATH=$USE_TBB_INC								\
	-D TBB_tbb_LIBRARY:PATH=$USE_TBB_LIB/libtbb.so						\
	-D TBB_tbb_LIBRARY_RELEASE:PATH=$USE_TBB_LIB/libtbb.so				\
	-D TBB_tbbmalloc_LIBRARY_RELEASE:PATH=$USE_TBB_LIB/libtbbmalloc.so	\
	..


#	-D PXR_BUILD_USD_IMAGING:BOOL=OFF									\
#	-D ALEMBIC_DIR:PATH=$USE_ABC										\

export JOBS=`grep -c \^processor /proc/cpuinfo`
make -j $JOBS install

exit

# set up symlinks to Houdini copies of OpenEXR, alembic, tbb

mkdir OpenEXR
cd OpenEXR
mkdir include lib
cd include
ln -s /opt/hfs15.0.416/toolkit/include/OpenEXR .
cd ../lib
ln -s /opt/hfs15.0.416/dsolib/libHalf* .
ln -s /opt/hfs15.0.416/dsolib/libI* .
cd ../..

mkdir alembic
cd alembic
mkdir include lib
cd include
ln -s /opt/hfs15.0.416/toolkit/include/Alembic .
cd ../lib
ln -s /opt/hfs15.0.416/dsolib/libAlembic* .
ln -s /opt/hfs15.0.416/dsolib/libI* .
ln -s /opt/hfs15.0.416/dsolib/libhdf5* .
ln -s /opt/hfs15.0.416/dsolib/libjemalloc* .
cd ../..

mkdir tbb
cd tbb
ln -s /opt/hfs15.0.416/dsolib/libtbb* .
cd ..
