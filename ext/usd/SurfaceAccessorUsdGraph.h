/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __usd_SurfaceAccessorUsdGraph_h__
#define __usd_SurfaceAccessorUsdGraph_h__
namespace fe
{
namespace ext
{

/**************************************************************************//**
    @brief USD Graph Access

	@ingroup usd
*//***************************************************************************/
class FE_DL_EXPORT SurfaceAccessorUsdGraph:
	public SurfaceAccessorBase,
	public CastableAs<SurfaceAccessorUsdGraph>
{
	public:
						SurfaceAccessorUsdGraph(void)
						{	setName("SurfaceAccessorUsdGraph"); }
virtual					~SurfaceAccessorUsdGraph(void)						{}

						using SurfaceAccessorBase::set;
						using SurfaceAccessorBase::spatialVector;

						//* as SurfaceAccessorI
		BWORD			bind(SurfaceAccessibleI::Element a_element,
								SurfaceAccessibleI::Attribute a_attribute);
		BWORD			bind(SurfaceAccessibleI::Element a_element,
								const String& a_name);

virtual	U32				count(void) const;
virtual	U32				subCount(U32 index) const;

virtual	void			set(U32 a_index,U32 a_subIndex,String a_string);
virtual	String			string(U32 a_index,U32 a_subIndex=0);

virtual	I32				integer(U32 a_index,U32 a_subIndex=0);

virtual	Real			real(U32 a_index,U32 a_subIndex=0);

virtual	SpatialVector	spatialVector(U32 a_index,U32 a_subIndex=0);

	private:
		const Array< sp<SurfaceAccessibleUsd::PrimNode> >*	m_pNodeArray;

		I32				offsetOfVertex(I32 a_subIndex)
						{
							const I32 table[10]=
									{ 0,1,2,3,0, 4,5,6,7,4 };
							return table[a_subIndex];
						}

		BWORD			isBound(void) const
						{	return TRUE; }

		I32				elementCount(
								SurfaceAccessibleI::Element a_element) const;

		SpatialVector	extentCorner(I32 a_primitiveIndex,I32 a_offset);

virtual	BWORD			bindInternal(SurfaceAccessibleI::Element a_element,
								const String& a_name);
};

} /* namespace ext */
} /* namespace fe */

#endif /* __usd_SurfaceAccessorUsdGraph_h__ */
