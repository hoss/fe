//
// Copyright 2016 Pixar
//
// Licensed under the Apache License, Version 2.0 (the "Apache License")
// with the following modification; you may not use this file except in
// compliance with the Apache License and the following modification to it:
// Section 6. Trademarks. is deleted and replaced with:
//
// 6. Trademarks. This License does not grant permission to use the trade
//	names, trademarks, service marks, or product names of the Licensor
//	and its affiliates, except as required to comply with Section 4(c) of
//	the License and to reproduce the content of the NOTICE file.
//
// You may obtain a copy of the Apache License at
//
//	 http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the Apache License with the above modification is
// distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied. See the Apache License for the specific
// language governing permissions and limitations under the Apache License.

// originally from pxr/imaging/hd/unitHydraDelegate.h
// also from pxr/imaging/hdSt/unitTestDelegate.h
// adapted for Free Electron

#ifndef __usd_HydraDelegate__
#define __usd_HydraDelegate__

#include "pxr/imaging/hd/sceneDelegate.h"
#include "pxr/imaging/hd/tokens.h"
#include "pxr/imaging/pxOsd/tokens.h"
#include "pxr/imaging/glf/texture.h"

#include "pxr/base/gf/vec3f.h"
#include "pxr/base/gf/vec3d.h"
#include "pxr/base/gf/vec4f.h"
#include "pxr/base/gf/vec4d.h"
#include "pxr/base/gf/matrix4f.h"
#include "pxr/base/gf/matrix4d.h"
#include "pxr/base/vt/array.h"
#include "pxr/base/tf/staticTokens.h"

#define TEST_TOKENS		(geometryAndGuides)

TF_DECLARE_PUBLIC_TOKENS(TestTokens,TEST_TOKENS);

/// \class HydraDelegate
/// A simple delegate class for unit test driver.
class HydraDelegate: public HdSceneDelegate
{
	public:
				HydraDelegate(HdRenderIndex *parentIndex,
					SdfPath const& delegateID);

		void	SetUseInstancePrimVars(bool v) { m_hasInstancePrimVars = v; }

		void	SetRefineLevel(int level);

		void	SetCamera(GfMatrix4d const &viewMatrix,
					GfMatrix4d const &projMatrix);
		void	SetCamera(SdfPath const &id, GfMatrix4d const &viewMatrix,
					GfMatrix4d const &projMatrix);
		void	AddCamera(SdfPath const &id);

		bool	HasLight(SdfPath const &id);
		void	AddLight(SdfPath const &id, GlfSimpleLight const &light);
		void	SetLight(SdfPath const &id, TfToken const &key, VtValue value);

		void	AddRenderTask(SdfPath const &id);
		void	AddRenderSetupTask(SdfPath const &id);
		void	AddSimpleLightTask(SdfPath const &id);
		void	AddShadowTask(SdfPath const &id);
		void	AddSelectionTask(SdfPath const &id);

		void	AdjustRenderSetupTask(SdfPath const &id,BWORD a_lighting,
					const GfVec4f& viewport);

		void	SetTaskParam(SdfPath const &id,
					TfToken const &name, VtValue val);
		VtValue GetTaskParam(SdfPath const &id, TfToken const &name);

		HdTaskSharedPtrVector GetRenderTasks(void);

		void	AddMesh(SdfPath const &id,
					GfMatrix4f const &transform,
					VtVec3rArray const &points,
					VtIntArray const &numVerts,
					VtIntArray const &verts,
					VtVec3rArray const &normals,
					PxOsdSubdivTags const &subdivTags,
					VtValue const &uv,
					HdInterpolation uvInterpolation,
					VtValue const &color,
					HdInterpolation colorInterpolation,
					bool guide=false,
					SdfPath const &instancerId=SdfPath(),
					TfToken const &scheme=PxOsdOpenSubdivTokens->catmullClark,
					TfToken const &orientation=HdTokens->rightHanded,
					bool doubleSided=false);

		void	AddBasisCurves(SdfPath const &id,
					GfMatrix4f const &transform,
					VtVec3rArray const &points,
					VtIntArray const &curveVertexCounts,
					VtVec3rArray const &normals,
					TfToken const &basis,
					VtValue const &color,
					HdInterpolation colorInterpolation,
					VtValue const &width,
					HdInterpolation widthInterpolation,
					SdfPath const &instancerId=SdfPath());

		void	AddPoints(SdfPath const &id,
					GfMatrix4f const &transform,
					VtVec3rArray const &points,
					VtVec3rArray const &normals,
					VtValue const &color,
					HdInterpolation colorInterpolation,
					VtValue const &width,
					HdInterpolation widthInterpolation,
					SdfPath const &instancerId=SdfPath());

		void	AddInstancer(SdfPath const &id,
					SdfPath const &parentId=SdfPath(),
					GfMatrix4f const &rootTransform=GfMatrix4f(1));

		void	SetInstancerProperties(SdfPath const &id,
					VtIntArray const &prototypeIndex,
					VtVec3rArray const &scale,
					VtVec4fArray const &rotate,
					VtVec3rArray const &translate);

//		void	AddSurfaceShader(SdfPath const &id,
//					std::string const &source,
//					HdShaderParamVector const &params);

		void	AddTexture(SdfPath const& id,
					GlfTextureRefPtr const& texture);

				/// Material
		void	AddMaterialResource(SdfPath const &id,
						VtValue materialResource);

				/// Update a material resource
		void	UpdateMaterialResource(SdfPath const &materialId,
						VtValue materialResource);

		void	BindMaterial(SdfPath const &rprimId,
						SdfPath const &materialId);

				/// Example to update a material binding on the fly
		void	RebindMaterial(SdfPath const &rprimId,
						SdfPath const &materialId);

virtual	SdfPath	GetMaterialId(SdfPath const& rprimId) override;

virtual	VtValue	GetMaterialResource(SdfPath const &materialId) override;

		void	Remove(SdfPath const &id);

				/// Remove all
		void	Clear(void);

				/// Hides an rprim, invalidating all collections it was in.
		void	HideRprim(SdfPath const &id);

				/// Un-hides an rprim, invalidating all collections it was in.
		void	UnhideRprim(SdfPath const &id);

				/// set per-prim repr
		void	SetReprSelector(SdfPath const &id,
						HdReprSelector const &reprSelector);

				/// set per-prim refine level
		void	SetRefineLevel(SdfPath const &id, int refineLevel);

				/// Marks an rprim in the RenderIndex as dirty
				/// with the given dirty flags.
		void	MarkRprimDirty(SdfPath path,
					HdChangeTracker::RprimDirtyBits flag);

		void	UpdatePositions(SdfPath const &id, float time);
		void	UpdateRprims(float time);
		void	UpdateInstancerPrimVars(float time);
		void	UpdateInstancerPrototypes(float time);

		void	UpdateCamera(SdfPath const &id,
					TfToken const &key, VtValue value);

		void	BindSurfaceShader(SdfPath const &rprimId,
					SdfPath const &shaderId)
				{ m_surfaceShaderBindings[rprimId] = shaderId; }

		GfRange3d GetExtents(void);

virtual	bool			IsInCollection(SdfPath const& id,
							TfToken const& collectionName);
virtual	HdBasisCurvesTopology GetBasisCurvesTopology(SdfPath const& id);
virtual	PxOsdSubdivTags	GetSubdivTags(SdfPath const& id);
virtual	GfRange3d		GetExtent(SdfPath const & id) override;
virtual	GfMatrix4d		GetTransform(SdfPath const & id) override;
virtual	bool			GetVisible(SdfPath const & id) override;
virtual	HdMeshTopology	GetMeshTopology(SdfPath const& id) override;
virtual	bool			GetDoubleSided(SdfPath const & id);
//virtual	int			GetRefineLevel(SdfPath const & id);
virtual HdDisplayStyle	GetDisplayStyle(SdfPath const & id) override;
virtual	VtValue			Get(SdfPath const& id, TfToken const& key);

virtual HdReprSelector	GetReprSelector(SdfPath const &id) override;

#if HD_API_VERSION>=32
virtual HdPrimvarDescriptorVector GetPrimvarDescriptors(SdfPath const& id,
							HdInterpolation interpolation) override;
#else
virtual	TfTokenVector	GetPrimVarVertexNames(SdfPath const& id);
virtual	TfTokenVector	GetPrimVarVaryingNames(SdfPath const& id);
virtual	TfTokenVector	GetPrimVarFacevaryingNames(SdfPath const& id);
virtual	TfTokenVector	GetPrimVarUniformNames(SdfPath const& id);
virtual	TfTokenVector	GetPrimVarConstantNames(SdfPath const& id);
virtual	TfTokenVector	GetPrimVarInstanceNames(SdfPath const& id);
virtual	int				GetPrimVarDataType(SdfPath const& id,
							TfToken const& key);
virtual	int				GetPrimVarComponents(SdfPath const& id,
							TfToken const& key);
#endif

virtual	VtIntArray		GetInstanceIndices(SdfPath const& instancerId,
							SdfPath const& prototypeId) override;
virtual	GfMatrix4d		GetInstancerTransform(
							SdfPath const& instancerId) override;
virtual	VtValue			GetCameraParamValue(
							SdfPath const &cameraId,
							TfToken const &paramName) override;

//virtual	std::string		GetSurfaceShaderSource(SdfPath const &shaderId);
//virtual	TfTokenVector	GetSurfaceShaderParamNames(SdfPath const &shaderId);
//virtual	HdShaderParamVector GetSurfaceShaderParams(SdfPath const &shaderId);
//virtual	VtValue			GetSurfaceShaderParamValue(SdfPath const &shaderId,
//							TfToken const &paramName);

//virtual	HdTextureResource::ID GetTextureResourceID(SdfPath const& textureId);
//virtual	HdTextureResourceSharedPtr GetTextureResource(SdfPath const& textureId);

		void			SetTransform(SdfPath const& id,
								GfMatrix4f const &transform);

private:

	struct Mesh
	{
			Mesh(void) { }
			Mesh(TfToken const &scheme,
				TfToken const &orientation,
				GfMatrix4f const &transform,
				VtVec3rArray const &points,
				VtIntArray const &numVerts,
				VtIntArray const &verts,
				VtVec3rArray const &normals,
				PxOsdSubdivTags const &subdivTags,
				VtValue const &uv,
				HdInterpolation uvInterpolation,
				VtValue const &color,
				HdInterpolation colorInterpolation,
				bool guide,
				bool doubleSided):
				scheme(scheme),
				orientation(orientation),
				transform(transform),
				points(points),
				numVerts(numVerts),
				verts(verts),
				normals(normals),
				subdivTags(subdivTags),
				uv(uv),
				uvInterpolation(uvInterpolation),
				color(color),
				colorInterpolation(colorInterpolation),
				guide(guide),
				doubleSided(doubleSided)									{}

			TfToken			scheme;
			TfToken			orientation;
			GfMatrix4f		transform;
			VtVec3rArray	points;
			VtIntArray		numVerts;
			VtIntArray		verts;
			VtVec3rArray	normals;
			PxOsdSubdivTags	subdivTags;
			VtValue			uv;
			HdInterpolation	uvInterpolation;
			VtValue			color;
			HdInterpolation	colorInterpolation;
			bool			guide;
			bool			doubleSided;
			HdReprSelector	reprSelector;
	};

	struct Curves
	{
			Curves(void)													{}

			Curves(
				GfMatrix4f const &transform,
				VtIntArray const &curveVertexCounts,
				VtIntArray const &curveIndices,
				VtVec3rArray const &points,
				VtVec3rArray const &normals,
				TfToken const &basis,
				VtValue const &color,
				HdInterpolation colorInterpolation,
				VtValue const &width,
				HdInterpolation widthInterpolation) :
				curveVertexCounts(curveVertexCounts),
				curveIndices(curveIndices),
				points(points),
				normals(normals),
				basis(basis),
				color(color),
				colorInterpolation(colorInterpolation),
				width(width),
				widthInterpolation(widthInterpolation)						{}

			GfMatrix4f		transform;
			VtIntArray		curveVertexCounts;
			VtIntArray		curveIndices;
			VtVec3rArray	points;
			VtVec3rArray	normals;
			TfToken			basis;
			VtValue			color;
			HdInterpolation	colorInterpolation;
			VtValue			width;
			HdInterpolation	widthInterpolation;
			HdReprSelector	reprSelector;
	};

	struct Points
	{
			Points(void)													{}

			Points(
				GfMatrix4f const &transform,
				VtVec3rArray const &points,
				VtVec3rArray const &normals,
				VtValue const &color,
				HdInterpolation colorInterpolation,
				VtValue const &width,
				HdInterpolation widthInterpolation):
				points(points),
				normals(normals),
				color(color),
				colorInterpolation(colorInterpolation),
				width(width),
				widthInterpolation(widthInterpolation)						{}

			GfMatrix4f		transform;
			VtVec3rArray	points;
			VtVec3rArray	normals;
			VtValue			color;
			HdInterpolation	colorInterpolation;
			VtValue			width;
			HdInterpolation	widthInterpolation;
			HdReprSelector	reprSelector;
	};

	struct Instancer
	{
			Instancer(void)													{}

			Instancer(VtVec3rArray const &scale,
				VtVec4fArray const &rotate,
				VtVec3rArray const &translate,
				GfMatrix4f const &rootTransform):
				scale(scale),
				rotate(rotate),
				translate(translate),
				rootTransform(rootTransform)								{}

			VtVec3rArray			scale;
			VtVec4fArray			rotate;
			VtVec3rArray			translate;
			VtIntArray				prototypeIndices;
			GfMatrix4f				rootTransform;
			std::vector<SdfPath>	prototypes;
	};

//	struct SurfaceShader
//	{
//			SurfaceShader(void)												{}
//
//			SurfaceShader(std::string const &src,
//				HdShaderParamVector const &pms):
//				source(src),
//				params(pms)													{}
//
//			std::string			source;
//			HdShaderParamVector	params;
//	};

	struct Texture
	{
			Texture(void)													{}

			Texture(GlfTextureRefPtr const &tex):
				texture(tex)												{}
			GlfTextureRefPtr	texture;
	};

		std::map<SdfPath,Mesh>				m_meshes;
		std::map<SdfPath,Curves>			m_curves;
		std::map<SdfPath,Points>			m_points;
		std::map<SdfPath,Instancer>			m_instancers;
//		std::map<SdfPath,SurfaceShader>		m_surfaceShaders;
		std::map<SdfPath,SdfPath>			m_surfaceShaderBindings;
		std::map<SdfPath,Texture>			m_textures;
		std::map<SdfPath, VtValue>			m_materials;
		TfHashSet<SdfPath,SdfPath::Hash>	m_hiddenRprims;

		typedef std::map<SdfPath, SdfPath> SdfPathMap;
		SdfPathMap							m_materialBindings;

		SdfPath								m_cameraId;
		bool								m_runShadows;
		bool								m_hasInstancePrimVars;
		int									m_refineLevel;
		std::map<SdfPath, int>				m_refineLevels;

		typedef TfHashMap<TfToken, VtValue, TfToken::HashFunctor> ValueCache;
		typedef TfHashMap<SdfPath, ValueCache, SdfPath::Hash> ValueCacheMap;

		ValueCacheMap						m_valueCacheMap;
};

#endif  // __usd_HydraDelegate__
