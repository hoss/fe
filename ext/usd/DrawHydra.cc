/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <usd/hydra.pmh>

#ifdef FE_GL_OPENCL
#define FE_DHY_OPENCL TRUE
#else
#define FE_DHY_OPENCL FALSE
#endif

#if FE_DHY_OPENCL
#include "pxr/imaging/hd/drawItem.h"
#include "pxr/imaging/hd/bufferArrayRange.h"
#if HD_API_VERSION>=35
#include "pxr/imaging/hdSt/bufferArrayRange.h"
#include "pxr/imaging/hdSt/bufferResource.h"
#elif HD_API_VERSION>=32
#include "pxr/imaging/hdSt/bufferArrayRangeGL.h"
#include "pxr/imaging/hdSt/bufferResourceGL.h"
#else
#include "pxr/imaging/hd/bufferArrayRangeGL.h"
#include "pxr/imaging/hd/bufferResourceGL.h"
#endif
#endif

#include "pxr/imaging/glf/textureHandle.h"
#include "pxr/imaging/glf/textureRegistry.h"
#include "pxr/imaging/hd/material.h"
#include "pxr/usd/sdr/registry.h"

#define FE_DHY_DEBUG			FALSE
#define FE_DHY_VBO_DEBUG		FALSE
#define FE_DHY_DATA_DEBUG		FALSE
#define FE_DHY_TICKER			FALSE

namespace fe
{
namespace ext
{

//* static
String DrawHydra::convertName(String a_name)
{
	return a_name.substitute(":","_").substitute(".","_").prechop("/");
}

sp<DrawBufferI> DrawHydra::createBuffer(void)
{
	sp<Library> spLibrary(new Library());
	spLibrary->setRegistry(registry());

	sp<DrawHydra::Buffer> spDrawBuffer(
			Library::create<DrawHydra::Buffer>("DrawHydra::Buffer",
			spLibrary.raw()));

	if(spDrawBuffer.isValid())
	{
		static I32 serial=1;	//* HACK

		String bufferName;
		bufferName.sPrintf("temp%06d",serial++);
		spDrawBuffer->setName(bufferName);
	}

	return spDrawBuffer;
}

DrawHydra::Buffer::Buffer(void):
	m_pHydraDriver(NULL),
	m_current(FALSE),
	m_recent(0),
	m_vertices(0),
	m_multicolor(FALSE),
	m_firstColor(0,0,0,0)
{
}

DrawHydra::Buffer::~Buffer(void)
{
	releaseAll();

	if(m_pHydraDriver)
	{
		if(!m_nodeName.empty())
		{
//			feLog("DROP \"%s\"\n",m_nodeName.c_str());

			const SdfPath id(("/"+m_nodeName).c_str());
			m_pHydraDriver->GetDelegate().Remove(id);
		}

		m_pHydraDriver=NULL;
	}
}

void DrawHydra::Buffer::releaseAll(void)
{
	m_current=FALSE;

	DrawOpenGL::Buffer::releaseAll();
}

DrawHydra::DrawHydra(void):
	m_pHydraDriver(NULL)
{
	m_refineLevel=0;
	m_alphaMin=0.1;				//* TODO param
	m_shadowResolution=32768;	//* TODO param
	m_shadowBias= -0.001;		//* TODO param
}

DrawHydra::~DrawHydra(void)
{
}

void DrawHydra::initialize(void)
{
#if FE_DHY_DEBUG
	feLog("DrawHydra::initialize\n");
#endif

	const char* shading=(const char *)glGetString(GL_SHADING_LANGUAGE_VERSION);
	const Real version=atof(shading);
	if(version<Real(4))
	{
		feLog("DrawHydra::initialize GLSL 4 required, found \"%s\"\n",shading);

		feX(e_cannotCreate,"DrawHydra::initialize","hardware not capable");
	}
}

void DrawHydra::flush(void)
{
#if FE_DHY_DEBUG
//	feLog("DrawHydra::flush %p\n",m_pHydraDriver);
#endif

	DrawOpenGL::flush();

	drawNodes();
}

template <typename T>
static VtArray<T> _BuildArray(T values[], int numValues)
{
	VtArray<T> result(numValues);
	std::copy(values, values+numValues, result.begin());
	return result;
}

void DrawHydra::ensureDriver(void)
{
	if(!m_pHydraDriver)
	{
		m_pHydraDriver = new HydraDriver(HdReprTokens->hull);

		HydraDelegate& rDelegate=m_pHydraDriver->GetDelegate();

		m_refineLevel=drawMode()->refinement();
		rDelegate.SetRefineLevel(m_refineLevel);

		rDelegate.AddSimpleLightTask(SdfPath("/SimpleLightTask"));
		rDelegate.AddShadowTask(SdfPath("/ShadowTask"));
		rDelegate.AddRenderSetupTask(SdfPath("/RenderSetupTask"));
		rDelegate.AddRenderTask(SdfPath("/RenderTask"));

		createMaterial2();
	}

#if FE_DHY_OPENCL
	sp<Master> spMaster=registry()->master();

	const BWORD allowGLX=TRUE;
	OpenCLContext::confirm(spMaster,allowGLX);
#endif
}

//* mostly from pxr/imaging/hdx/testenv/testHdxDrawTarget.cpp
void DrawHydra::createMaterial1(void)
{
	HydraDelegate& rDelegate=m_pHydraDriver->GetDelegate();

//	const char* filename="StinsonBeach.exr";
//	static PlugPluginPtr plugin = PLUG_THIS_PLUGIN;
//	const std::string path = PlugFindPluginResource(plugin,
//			TfStringCatPaths("textures", filename));
//	TF_VERIFY(!path.empty(), "Could not find texture: %s\n", texture);

	const std::string path("doc/image/FreeElectron.png");
	static TfToken fileTexture(path);

	GlfTextureHandleRefPtr textureHandle =
			GlfTextureRegistry::GetInstance().GetTextureHandle(fileTexture);
	GlfTextureRefPtr texture=textureHandle->GetTexture();

	rDelegate.AddTexture(SdfPath("/texture"),texture);

	// add a surface shader to use the drawtarget as a texture
	SdfPath materialId("/material");
	std::string const source(
		"-- glslfx version 0.1 \n"
		"-- configuration \n"
		"{\n"
			"\"textures\" : { \n"
			"    \"texColor\": { \n"
			"        \"description\": \"DrawTarget Texture\" \n"
			"    } \n"
			"}, \n"
			"\"techniques\": {\n"
			"    \"default\": {\n"
			"        \"surfaceShader\": {\n"
			"            \"source\": [ \"testHdxDrawTarget.Surface\" ]\n"
			"        }\n"
			"    }\n"
			"}\n\n"
		"}\n"

		"-- glsl testHdxDrawTarget.Surface \n\n"

		"vec4 surfaceShader(vec4 Peye, vec3 Neye, vec4 color, vec4 patchCoord) {\n"
		"    vec2 uv = mod(Peye.xy*0.3, vec2(1));                               \n"
		"    return vec4(FallbackLighting(Peye.xyz, Neye, HdGet_texColor(uv)), 1);\n"
		"}\n"
	);

	SdfPath texturePath("/texture");

	SdrRegistry &shaderReg = SdrRegistry::GetInstance();
	SdrShaderNodeConstPtr sdrSurfaceNode =
		shaderReg.GetShaderNodeFromSourceCode(
			source,
			HioGlslfxTokens->glslfx,
			NdrTokenMap()); // metadata

	TfToken const& terminalType = HdMaterialTerminalTokens->surface;

	// Adding basic material
	HdMaterialNetworkMap material1;
	HdMaterialNetwork& network1 = material1.map[terminalType];
	HdMaterialNode terminal1;
	terminal1.path = materialId.AppendPath(SdfPath("Shader"));
	terminal1.identifier = sdrSurfaceNode->GetIdentifier();
	terminal1.parameters[TfToken("texColor")] = VtValue(GfVec3f(1));

	// Insert texture node (for sampling from attachment)
	HdMaterialNode textureNode;
	textureNode.path = texturePath;
	textureNode.identifier = TfToken("UsdUVTexture");
	textureNode.parameters[TfToken("fallback")] = VtValue(GfVec3f(1));

	// For the file path, HdSt doesn't really care what it is since it is going
	// to do a lookup of the prim via GetTextureResource on the sceneDelegate.
	// The file path cannot be empty though, because if it is empty HdSt will
	// use the fallback value of the texture node.
	//
	// Note that we do not author an SdfPath or std::string here so that
	// the HdSceneDelegate::GetTextureResource API is used rather than the
	// storm texture system.
	textureNode.parameters[TfToken("file")] =
			VtValue(texturePath);
	textureNode.parameters[TfToken("wrapS")] =
			VtValue(TfToken("repeat"));
	textureNode.parameters[TfToken("wrapT")] =
			VtValue(TfToken("repeat"));
	textureNode.parameters[TfToken("minFilter")] =
			VtValue(TfToken("linear"));
	textureNode.parameters[TfToken("magFilter")] =
			VtValue(TfToken("linear"));

	// Insert connection between texture node and terminal
	HdMaterialRelationship rel;
	rel.inputId = textureNode.path;
	rel.inputName = TfToken("rgb");
	rel.outputId = terminal1.path;
	rel.outputName = TfToken("texColor");
	network1.relationships.push_back(std::move(rel));

	// Insert texture node
	network1.nodes.push_back(std::move(textureNode));

	// Insert terminal
	material1.terminals.push_back(terminal1.path);
	network1.nodes.push_back(std::move(terminal1)); // must be last in vector
	rDelegate.AddMaterialResource(materialId,VtValue(material1));
}

void DrawHydra::createMaterial2(void)
{
	HydraDelegate& rDelegate=m_pHydraDriver->GetDelegate();

	const std::string path("doc/image/FreeElectron.png");
	TfToken const& terminalType = HdMaterialTerminalTokens->surface;

	HdMaterialNetworkMap material;
	HdMaterialNetwork& network = material.map[terminalType];

	HdMaterialNode readerNode;
	readerNode.path = SdfPath("/reader");
	readerNode.identifier = TfToken("UsdPrimvarReader_float2");
	readerNode.parameters[TfToken("varname")] = VtValue(TfToken("uv"));

	HdMaterialNode textureNode;
	textureNode.path = SdfPath("/texture");
	textureNode.identifier = TfToken("UsdUVTexture");
	textureNode.parameters[TfToken("fallback")] = VtValue(GfVec3f(1));
	textureNode.parameters[TfToken("file")] = VtValue(path);
	textureNode.parameters[TfToken("wrapS")] = VtValue(TfToken("repeat"));
	textureNode.parameters[TfToken("wrapT")] = VtValue(TfToken("repeat"));
	textureNode.parameters[TfToken("minFilter")] = VtValue(TfToken("linear"));
	textureNode.parameters[TfToken("magFilter")] = VtValue(TfToken("linear"));

	HdMaterialNode previewNode;
	previewNode.path = SdfPath("/preview");
	previewNode.identifier = TfToken("UsdPreviewSurface");

	HdMaterialRelationship rel;

	rel.inputId = readerNode.path;
	rel.inputName = TfToken("result");
	rel.outputId = textureNode.path;
	rel.outputName = TfToken("st");
	network.relationships.push_back(std::move(rel));

	rel.inputId = textureNode.path;
	rel.inputName = TfToken("rgb");
	rel.outputId = previewNode.path;
	rel.outputName = TfToken("diffuseColor");
	network.relationships.push_back(std::move(rel));

	network.nodes.push_back(std::move(readerNode));

	network.nodes.push_back(std::move(textureNode));

	material.terminals.push_back(previewNode.path);
	network.nodes.push_back(std::move(previewNode)); // must be last in vector

	rDelegate.AddMaterialResource(SdfPath("/material"),VtValue(material));
}

void DrawHydra::updateCameraAndLighting(void)
{
	sp<CameraI> spCamera=view()->camera();
	const Box2i viewport=view()->viewport();

	Matrix<4,4,Real> proj4x4;
	if(view()->projection()==ViewI::e_perspective)
	{
		const Real fovy=spCamera->fov()[1];

		Real nearplane;
		Real farplane;
		spCamera->getPlanes(nearplane,farplane);

		const GLfloat aspect=(GLfloat)width(viewport)/
				(GLfloat)height(viewport);
		proj4x4=perspective(fovy,Real(aspect),nearplane,farplane);
	}
	else
	{
		Real zoom;
		Vector2 center;
		spCamera->getOrtho(zoom,center);

		const Vector2 size(width(viewport),height(viewport));
		proj4x4=ViewCommon::computeOrtho(zoom,center,size);
	}

	const GfMatrix4d projMatrix(
			proj4x4[0],proj4x4[1],proj4x4[2],proj4x4[3],
			proj4x4[4],proj4x4[5],proj4x4[6],proj4x4[7],
			proj4x4[8],proj4x4[9],proj4x4[10],proj4x4[11],
			proj4x4[12],proj4x4[13],proj4x4[14],proj4x4[15]);

	const GfMatrix4d viewMatrix=
			convertToGfMatrix4d(spCamera->cameraMatrix());

	m_pHydraDriver->SetCamera(viewMatrix, projMatrix,
			GfVec4d(0, 0, width(viewport), height(viewport)));

	SpatialTransform cameraTransform;
	invert(cameraTransform,spCamera->cameraMatrix());

	const SpatialVector lightDirFromCamera(-1.0,-1.0,-3.0);
	SpatialVector lightDir;
	rotateVector(cameraTransform,lightDirFromCamera,lightDir);
	normalizeSafe(lightDir);

	//* NOTE reverse for Houdini solids (don't know why yet)
//	lightDir=-lightDir;

	m_brightness=fe::minimum(Real(1),fe::maximum(Real(0),m_brightness));
	m_contrast=fe::minimum(Real(2),fe::maximum(Real(0),m_contrast));

//	m_lightingShader->SetLightingStateFromOpenGL();

	GlfSimpleLightVector lights(1);
	GlfSimpleLight& rLight=lights[0];
	rLight.SetPosition(GfVec4f(-lightDir[0],-lightDir[1],-lightDir[2],0));
	rLight.SetAmbient(GfVec4f(m_brightness));
	rLight.SetDiffuse(GfVec4f(m_contrast,m_contrast,m_contrast,1));
	rLight.SetSpecular(GfVec4f(0.5));
	rLight.SetShadowResolution(m_shadowResolution);
	rLight.SetShadowBias(m_shadowBias);
//	rLight.SetShadowBlur(GfVec4f(0.1));

	rLight.SetHasShadow(drawMode()->shadows());

	const SdfPath lightPath("/Light0");

	HydraDelegate& rDelegate=m_pHydraDriver->GetDelegate();

	if(!rDelegate.HasLight(lightPath))
	{
		rDelegate.AddLight(lightPath,rLight);
	}

#if HD_API_VERSION>=32
	rDelegate.SetLight(lightPath,HdLightTokens->params,
			VtValue(rLight));
#else
	rDelegate.SetLight(lightPath,HdStLightTokens->params,
			VtValue(rLight));
#endif

	const SdfPath renderSetupPath("/RenderSetupTask");
	rDelegate.AdjustRenderSetupTask(renderSetupPath,drawMode()->lit(),
			GfVec4f(0, 0, width(viewport), height(viewport)));
}

void DrawHydra::drawPoints(const SpatialVector *vertex,
	const SpatialVector *normal,U32 vertices,
	BWORD multicolor,const Color *color,
	sp<DrawBufferI> spDrawBuffer)
{
//	if(vertices>8)
//	{
//		feLog("DrawHydra::drawPoints vertices %d buffer %p\n",
//				vertices,spDrawBuffer.raw());
//	}

	if(spDrawBuffer.isNull())
	{
//		if(vertices>8)
//		{
//			feLog("DrawHydra::drawPoints bypass %d\n",vertices);
//		}

		DrawOpenGL::drawPoints(vertex,normal,vertices,
				multicolor,color,spDrawBuffer);
		return;
	}

	sp<Buffer> spBuffer=spDrawBuffer;
	if(spBuffer.isNull())
	{
		feLog("DrawHydra::drawPoints incompatible buffer\n");
		return;
	}

	if(spBuffer->m_multicolor!=multicolor || spBuffer->m_firstColor!=color[0])
	{
		spBuffer->releaseAll();
	}

	ensureDriver();

	if(!spBuffer->m_current)
	{
		const I32 refinement=drawMode()->refinement();
		const BWORD refined=(refinement>0);

		HydraDelegate& rDelegate=m_pHydraDriver->GetDelegate();

		spBuffer->m_pHydraDriver=m_pHydraDriver;
		spBuffer->m_multicolor=multicolor;
		spBuffer->m_firstColor=color[0];
		spBuffer->m_vertices=vertices;

		const I32 pointCount=vertices;

#if FE_DHY_DEBUG
		feLog("DrawHydra::drawPoints hull points %d refine %d\n",
				pointCount,refinement);
#endif

		GfVec3r* points=new GfVec3r[pointCount];

		//* NOTE Hydra does not currently seem to care about point normals
		//* (uses normals always facing camera)
		GfVec3r* normals=NULL;
		if(FALSE&& normal)
		{
			normals=new GfVec3r[pointCount];
		}

		HdInterpolation colorInterp=HdInterpolationConstant;
		VtValue vtColor;
		VtVec4fArray vtColorArray;
		if(multicolor)
		{
			colorInterp=HdInterpolationVertex;
			vtColorArray.resize(pointCount);
		}
		else
		{
			const Color& rColor=color[0];

			vtColor=GfVec4f(rColor[0],rColor[1],rColor[2],
					fe::maximum(Real(rColor[3]),m_alphaMin));
		}

		SpatialVector min(FLT_MAX,FLT_MAX,FLT_MAX);
		SpatialVector max(FLT_MIN,FLT_MIN,FLT_MIN);

		for(I32 pointIndex=0;pointIndex<pointCount;pointIndex++)
		{
			const SpatialVector& point=vertex[pointIndex];

			points[pointIndex].Set(point.temp());

			for(I32 m=0;m<3;m++)
			{
				if(min[m]>point[m])
				{
					min[m]=point[m];
				}
				if(max[m]<point[m])
				{
					max[m]=point[m];
				}
			}

			if(normals)
			{
				normals[pointIndex].Set(normal[pointIndex].temp());
			}

			if(multicolor)
			{
				const Color& rColor=color[pointIndex];
				Real alpha=rColor[3];

#if TRUE
				//* HACK boost alpha
				alpha=pow(alpha,0.3);
				if(alpha>0.01)
				{
					alpha=fe::maximum(m_alphaMin,fe::minimum(alpha,Real(1)));
				}
#endif

				vtColorArray[pointIndex]=
						GfVec4f(rColor[0],rColor[1],rColor[2],alpha);
			}
		}

		const SpatialVector size=max-min;
		const Real largest=fe::maximum(size[0],fe::maximum(size[1],size[2]));
		const Real defaultWidth=0.01*largest;	//* NOTE tweak

		VtVec3rArray normalArray;
		if(normals)
		{
			normalArray=_BuildArray(normals,pointCount);

			delete[] normals;
			normals=NULL;
		}

		if(multicolor)
		{
			vtColor = VtValue(vtColorArray);
		}

		if(spBuffer->m_nodeName.empty())
		{
			spBuffer->m_nodeName=convertName(spBuffer->name());
		}

		const SdfPath id(("/"+spBuffer->m_nodeName.prechop("/")).c_str());
		const GfMatrix4f transform(1);

#if TRUE
		const VtValue vtWidth((float)defaultWidth);
		const HdInterpolation widthInterp=HdInterpolationConstant;
#else
		VtFloatArray vtWidthArray;
		vtWidthArray.resize(pointCount);
		for(I32 pointIndex=0;pointIndex<pointCount;pointIndex++)
		{
			vtWidthArray[pointIndex]=(pointIndex%2)? 0.01f: 0.04f;
		}
		const VtValue vtWidth=VtValue(vtWidthArray);
		const HdInterpolation widthInterp=HdInterpolationVertex;
#endif

		rDelegate.Remove(id);
		m_bufferMap.erase(spBuffer->m_nodeName);

		SdfPath instancerId;
		rDelegate.AddPoints(id,
				transform,
				_BuildArray(points,pointCount),
				normalArray,
				vtColor,colorInterp,
				vtWidth,widthInterp,
				instancerId);

		//* NOTE point modes are all equivalent
		rDelegate.SetReprSelector(id,HdReprSelector(
				refined? HdReprTokens->refined: HdReprTokens->hull));

		rDelegate.SetRefineLevel(id,drawMode()->refinement());

		spBuffer->m_current=TRUE;

#if FE_DHY_DEBUG
		feLog("DrawHydra::drawPoints new\n");
#endif

		delete[] points;
		points=NULL;
	}

	//* HACK high resolution shadows don't seem to work well for point clouds
	m_shadowBias= -0.1;
	m_shadowResolution=256;

	spBuffer->m_recent=1;

	m_bufferMap[spBuffer->m_nodeName]=spBuffer;

	updateNode(spDrawBuffer);
}

void DrawHydra::drawTriangles(const SpatialVector* vertex,
	const SpatialVector* normal,const Vector2* texture,U32 vertices,
	StripMode strip,BWORD multicolor,const Color* color,
	const Array<I32>* vertexMap,
	const Array<I32>* hullPointMap,const Array<Vector4i>* hullFacePoint,
	sp<DrawBufferI> spDrawBuffer)
{
	if(drawStyle()==DrawMode::e_pointCloud || spDrawBuffer.isNull() ||
			!hullPointMap || !hullFacePoint || isUvSpace())
	{
		DrawOpenGL::drawTriangles(vertex,normal,texture,vertices,
				strip,multicolor,color,
				vertexMap,hullPointMap,hullFacePoint,spDrawBuffer);
		return;
	}

	drawPolys(3,vertex,normal,texture,vertices,strip,
			multicolor,color,vertexMap,
			hullPointMap,hullFacePoint,spDrawBuffer);
}

void DrawHydra::drawTransformedPoints(
		const SpatialTransform &transform,
		const SpatialVector *scale,
		const SpatialVector *vertex,
		const SpatialVector *normal,
		U32 vertices,BWORD multicolor,
		const Color *color,
		sp<DrawBufferI> spDrawBuffer)
{
	sp<Buffer> spBuffer=spDrawBuffer;
	if(spBuffer.isNull())
	{
		//* NOTE send new vertices every frame
		DrawCommon::drawTransformedPoints(transform,scale,
				vertex,normal,vertices,multicolor,color,spDrawBuffer);
		return;
	}

	//* TODO if scale!=NULL, prescale vertices
	if(scale)
	{
		feLog("DrawHydra::drawTransformedPoints"
				" scale factor not implemented\n");
	}

	drawPoints(vertex,normal,vertices,multicolor,color,spDrawBuffer);

	//* update transform
	HydraDelegate& rDelegate=m_pHydraDriver->GetDelegate();
	const SdfPath id(("/"+spBuffer->m_nodeName).c_str());
	const GfMatrix4f matrix4f=convertToGfMatrix4f(transform);
	rDelegate.SetTransform(id,matrix4f);
}

void DrawHydra::drawTransformedLines(
		const SpatialTransform &transform,
		const SpatialVector *scale,
		const SpatialVector *vertex,
		const SpatialVector *normal,
		U32 vertices,StripMode strip,BWORD multicolor,
		const Color *color,
		BWORD multiradius,const Real *radius,
		const Vector3i *element,U32 elementCount,
		sp<DrawBufferI> spDrawBuffer)
{
	sp<Buffer> spBuffer=spDrawBuffer;
	if(spBuffer.isNull())
	{
		//* NOTE send new vertices every frame
		DrawCommon::drawTransformedLines(transform,scale,
				vertex,normal,vertices,strip,multicolor,color,
				multiradius,radius,element,elementCount,spDrawBuffer);
		return;
	}

	//* TODO if scale!=NULL, prescale vertices
	if(scale)
	{
		feLog("DrawHydra::drawTransformedLines"
				" scale factor not implemented\n");
	}

	drawLines(vertex,normal,vertices,strip,multicolor,color,
			multiradius,radius,element,elementCount,spDrawBuffer);

	//* update transform
	HydraDelegate& rDelegate=m_pHydraDriver->GetDelegate();
	const SdfPath id(("/"+spBuffer->m_nodeName).c_str());
	const GfMatrix4f matrix4f=convertToGfMatrix4f(transform);
	rDelegate.SetTransform(id,matrix4f);
}

void DrawHydra::drawTransformedTriangles(
		const SpatialTransform &transform,
		const SpatialVector *scale,
		const SpatialVector *vertex,
		const SpatialVector *normal,
		const Vector2 *texture,
		U32 vertices,StripMode strip,BWORD multicolor,
		const Color *color,
		const Array<I32>* vertexMap,
		const Array<I32>* hullPointMap,
		const Array<Vector4i>* hullFacePoint,
		sp<DrawBufferI> spDrawBuffer)
{
	sp<Buffer> spBuffer=spDrawBuffer;
	if(spBuffer.isNull())
	{
		//* NOTE send new vertices every frame
		DrawCommon::drawTransformedTriangles(
				transform,scale,
				vertex,normal,texture,vertices,
				strip,multicolor,color,
				vertexMap,hullPointMap,hullFacePoint,
				spDrawBuffer);
		return;
	}

	//* TODO if scale!=NULL, prescale vertices
	if(scale)
	{
		feLog("DrawHydra::drawTransformedTriangles"
				" scale factor not implemented\n");
	}

	drawTriangles(vertex,normal,texture,vertices,strip,multicolor,color,
			vertexMap,hullPointMap,hullFacePoint,spDrawBuffer);

	//* update transform
	HydraDelegate& rDelegate=m_pHydraDriver->GetDelegate();
	const SdfPath id(("/"+spBuffer->m_nodeName).c_str());
	const GfMatrix4f matrix4f=convertToGfMatrix4f(transform);
	rDelegate.SetTransform(id,matrix4f);
}

// static
GfMatrix4f DrawHydra::convertToGfMatrix4f(const SpatialTransform& rTransform)
{
	return GfMatrix4f(
			rTransform(0,0), rTransform(1,0), rTransform(2,0), 0.0,
			rTransform(0,1), rTransform(1,1), rTransform(2,1), 0.0,
			rTransform(0,2), rTransform(1,2), rTransform(2,2), 0.0,
			rTransform(0,3), rTransform(1,3), rTransform(2,3), 1.0);
}

// static
GfMatrix4d DrawHydra::convertToGfMatrix4d(const SpatialTransform& rTransform)
{
	return GfMatrix4d(
			rTransform(0,0), rTransform(1,0), rTransform(2,0), 0.0,
			rTransform(0,1), rTransform(1,1), rTransform(2,1), 0.0,
			rTransform(0,2), rTransform(1,2), rTransform(2,2), 0.0,
			rTransform(0,3), rTransform(1,3), rTransform(2,3), 1.0);
}

void DrawHydra::drawLinesInternal(const SpatialVector* vertex,
	const SpatialVector* normal,const Vector2* texture,U32 vertices,
	StripMode strip,BWORD multicolor,const Color* color,
	BWORD multiradius,const Real* radius,
	const Vector3i* element,U32 elementCount,
	sp<DrawBufferI> spDrawBuffer)
{
//	if(vertices>8)
//	{
//		feLog("DrawHydra::drawLinesInternal"
//				" vertices %d buffer %p elementCount %d\n  %s\n",
//				vertices,spDrawBuffer.raw(),elementCount,
//				c_print(drawMode()));
//	}

	if(strip==e_strip && !element)
	{
		elementCount=1;
	}

	const BWORD uvSpace=isUvSpace();
	const BWORD capable=(elementCount>0);

	//* HACK
//	if(capable && spDrawBuffer.isNull())
//	{
//		spDrawBuffer=createBuffer();
//	}

	if(!capable || spDrawBuffer.isNull())
	{
//		if(vertices>8)
//		{
//			feLog("DrawHydra::drawLinesInternal bypass %d\n",vertices);
//		}

		DrawOpenGL::drawLinesInternal(vertex,normal,texture,vertices,strip,
				multicolor,color,multiradius,radius,
				element,elementCount,spDrawBuffer);
		return;
	}

	sp<Buffer> spBuffer=spDrawBuffer;
	if(spBuffer.isNull())
	{
		feLog("DrawHydra::drawLinesInternal incompatible buffer\n");
		return;
	}

	if(spBuffer->m_multicolor!=multicolor || spBuffer->m_firstColor!=color[0])
	{
		spBuffer->releaseAll();
	}

	ensureDriver();

	if(!spBuffer->m_current)
	{
		if(uvSpace && !texture)
		{
			feLog("DrawHydra::drawLinesInternal"
					" vertices %d in UV space without UV coordinates\n",
					vertices);
			return;
		}

#if FE_DHY_TICKER
		U32 tickStart=systemTick();
#endif

		HydraDelegate& rDelegate=m_pHydraDriver->GetDelegate();

		spBuffer->m_pHydraDriver=m_pHydraDriver;
		spBuffer->m_multicolor=multicolor;
		spBuffer->m_firstColor=color[0];
		spBuffer->m_vertices=vertices;

//		const GfVec3r center=rDelegate.PopulateBasicTestSet();

#if FE_HYDRA_CURVE_INDICES
		const I32 pointCount=vertices;
#else
		const I32 pointCount=vertices+2*elementCount;
#endif

#if FE_DHY_DEBUG
		feLog("DrawHydra::drawLinesInternal"
				" hull points %d faces %d refine %d\n",
				pointCount,elementCount,drawMode()->refinement());
#endif

		GfVec3r* points=new GfVec3r[pointCount];
		int* numVerts=new int[elementCount];
		I32 pointIndex=0;

		GfVec3r* normals=NULL;
		if(FALSE&& normal)
		{
			normals=new GfVec3r[pointCount];
		}

		HdInterpolation colorInterp=HdInterpolationConstant;
		VtValue vtColor;
		VtVec4fArray vtColorArray;
		if(multicolor)
		{
			colorInterp=HdInterpolationVertex;
			vtColorArray.resize(pointCount);
		}
		else
		{
			const Color& rColor=color[0];

			vtColor=GfVec4f(rColor[0],rColor[1],rColor[2],
					fe::maximum(Real(rColor[3]),m_alphaMin));
		}

		BWORD verifiedMultiRadius=FALSE;
		if(multiradius)
		{
			Real firstRadius=radius[0];
			for(I32 pointIndex=0;pointIndex<pointCount;pointIndex++)
			{
				if(radius[pointIndex]!=firstRadius)
				{
					verifiedMultiRadius=TRUE;
					break;
				}
			}
		}

		HdInterpolation widthInterp=HdInterpolationConstant;
		VtValue vtWidth;
		VtFloatArray vtWidthArray;
		if(verifiedMultiRadius)
		{
//			widthInterp=HdInterpolationUniform;	//* per curve
			widthInterp=HdInterpolationVertex;	//* per unique point

			vtWidthArray.resize(pointCount);

			//* HACK
			for(I32 pointIndex=0;pointIndex<pointCount;pointIndex++)
			{
				vtWidthArray[pointIndex]=0.4;
			}
		}
		else if(radius)
		{
			vtWidth=float(radius[0]);
		}

		//* HACK using linear with multiradius
		//* bSpline, catmullRom, bezier, linear
		const TfToken basis=
				verifiedMultiRadius? HdTokens->linear: HdTokens->bSpline;
//		const TfToken basis=HdTokens->bSpline;

		const I32 addCvs=
				(basis==HdTokens->linear || basis==HdTokens->bezier)? 0: 2;

		SpatialVector min(FLT_MAX,FLT_MAX,FLT_MAX);
		SpatialVector max(FLT_MIN,FLT_MIN,FLT_MIN);

		for(U32 elementIndex=0;elementIndex<elementCount;elementIndex++)
		{
			I32 startIndex=0;
			I32 subCount=vertices;

			if(element)
			{
				const Vector3i& rElement=element[elementIndex];
				startIndex=rElement[0];
				subCount=rElement[1];
			}

			const I32 cvCount=subCount+addCvs;

#if FE_HYDRA_CURVE_INDICES
			const I32 offsetIndex=pointIndex;
			const I32 pointInc=subCount;
#else
			const I32 offsetIndex=pointIndex+1;
			const I32 pointInc=subCount+2;
#endif

			for(I32 subIndex=0;subIndex<subCount;subIndex++)
			{
				SpatialVector point;
				if(uvSpace)
				{
					const Vector2& rUV=texture[startIndex+subIndex];
					set(point,rUV[0],rUV[1],Real(0));
				}
				else
				{
					point=vertex[startIndex+subIndex];
				}

				points[offsetIndex+subIndex].Set(point.temp());

				for(I32 m=0;m<3;m++)
				{
					if(min[m]>point[m])
					{
						min[m]=point[m];
					}
					if(max[m]<point[m])
					{
						max[m]=point[m];
					}
				}
			}

#if !FE_HYDRA_CURVE_INDICES
			points[pointIndex]=points[pointIndex+1];
			points[pointIndex+cvCount-1]=points[pointIndex+cvCount-2];
#endif

			if(normals)
			{
				for(I32 subIndex=0;subIndex<subCount;subIndex++)
				{
					//* NOTE normals appear to be in eye space
//					normals[pointIndex+1+subIndex].Set(
//							normal[startIndex+subIndex].temp());

					normals[offsetIndex+subIndex]=
							GfVec3r(0.0f,0.0f,1.0f);
				}
#if !FE_HYDRA_CURVE_INDICES
				normals[pointIndex]=normals[pointIndex+1];
				normals[pointIndex+cvCount-1]=
						normals[pointIndex+cvCount-2];
#endif
			}

			if(multicolor)
			{
				for(I32 subIndex=0;subIndex<subCount;subIndex++)
				{
					const Color& rColor=color[startIndex+subIndex];
//					const Color rColor(1.0-subIndex/Real(subCount-1),
//							(elementIndex%3)/2.0,1.0);

					vtColorArray[offsetIndex+subIndex]=GfVec4f(
							rColor[0],rColor[1],rColor[2],
							fe::maximum(Real(rColor[3]),m_alphaMin));
				}
#if !FE_HYDRA_CURVE_INDICES
				vtColorArray[pointIndex]=vtColorArray[pointIndex+1];
				vtColorArray[pointIndex+cvCount-1]=
						vtColorArray[pointIndex+cvCount-2];
#endif
			}

#if TRUE
			if(verifiedMultiRadius)
			{
				for(I32 subIndex=0;subIndex<subCount;subIndex++)
				{
					vtWidthArray[offsetIndex+subIndex]=
							float(2.0*radius[startIndex+subIndex]);
				}

#if !FE_HYDRA_CURVE_INDICES
				vtWidthArray[pointIndex]=vtWidthArray[pointIndex+1];
				vtWidthArray[pointIndex+cvCount-1]=
						vtWidthArray[pointIndex+cvCount-2];
#endif
			}
#else
			if(verifiedMultiRadius)
			{
				const I32 reduce=0;		//* 0 or 2

				const I32 base=elementIndex*(subCount-reduce);

				for(I32 subIndex=0;subIndex<subCount-reduce;subIndex++)
				{
					vtWidthArray[base+subIndex]=
							float(0.1*(1.0-subIndex/Real(subCount-1-reduce)));
				}
			}
#endif

			numVerts[elementIndex]=cvCount;
			pointIndex+=pointInc;

			//* HACK
//			numVerts[elementIndex]-=2;
		}

#if FE_DHY_TICKER
		U32 tickEnd=systemTick();
		U32 tickDiff=SystemTicker::tickDifference(tickStart,tickEnd);
		Real ms=1e-3f*tickDiff*
				SystemTicker::microsecondsPerTick();
		feLog("DrawHydra::drawLinesInternal converted %.6G ms\n",ms);
#endif

//		const SpatialVector size=max-min;
//		const Real largest=fe::maximum(size[0],fe::maximum(size[1],size[2]));
//		const Real defaultWidth=4e-4*largest;	//* NOTE tweak
		const Real defaultWidth=3e-3;

		FEASSERT(pointIndex==pointCount);

		VtVec3rArray normalArray;
		if(normals)
		{
//			feLog("  hydra curve normals\n");
			normalArray=_BuildArray(normals,pointCount);

			delete[] normals;
			normals=NULL;
		}

		if(multicolor)
		{
//			feLog("  hydra curve multicolor %d\n",vtColorArray.size());
			vtColor = VtValue(vtColorArray);
		}

		if(verifiedMultiRadius)
		{
//			feLog("  hydra curve multiradius %d\n",vtWidthArray.size());
			vtWidth = VtValue(vtWidthArray);
		}
		else if(!radius)
		{
			vtWidth = float(defaultWidth);
		}

		if(spBuffer->m_nodeName.empty())
		{
			spBuffer->m_nodeName=convertName(spBuffer->name());
		}

		const SdfPath id(("/"+spBuffer->m_nodeName).c_str());
		const GfMatrix4f transform(1);

		rDelegate.Remove(id);
		m_bufferMap.erase(spBuffer->m_nodeName);

		SdfPath instancerId;
		rDelegate.AddBasisCurves(id,
				transform,
				_BuildArray(points,pointCount),
				_BuildArray(numVerts,elementCount),
				normalArray,
				basis,
				vtColor,colorInterp,
				vtWidth,widthInterp,
				instancerId);

		const BWORD refined=(drawMode()->refinement()>0);

		//* NOTE choices:
		//*		hull (same as smoothHull, wire, wireOnSurf, refinedWire)
		//*		refined (same as refinedWireOnSurf)
		//* see: pxr/imaging/lib/hd/renderIndex.cpp (all same for points)
		rDelegate.SetReprSelector(id,HdReprSelector(
				refined? HdReprTokens->refined: HdReprTokens->hull));

		rDelegate.SetRefineLevel(id,drawMode()->refinement());

		spBuffer->m_current=TRUE;

#if FE_DHY_DEBUG
		feLog("DrawHydra::drawLinesInternal new\n");
#endif

		delete[] numVerts;
		numVerts=NULL;

		delete[] points;
		points=NULL;

#if FE_DHY_TICKER
		tickEnd=systemTick();
		tickDiff=SystemTicker::tickDifference(tickStart,tickEnd);
		ms=1e-3f*tickDiff*
				SystemTicker::microsecondsPerTick();
		feLog("DrawHydra::drawLinesInternal done %.6G ms (total)\n",ms);
#endif
	}

	spBuffer->m_recent=1;

	m_bufferMap[spBuffer->m_nodeName]=spBuffer;

	updateNode(spDrawBuffer);
}

void DrawHydra::drawPolys(U32 grain,const SpatialVector *vertex,
	const SpatialVector *normal,const Vector2 *texture,
	U32 vertices,StripMode strip,BWORD multicolor,const Color *color,
	const Array<I32>* vertexMap,
	const Array<I32>* hullPointMap,const Array<Vector4i>* hullFacePoint,
	sp<DrawBufferI> spDrawBuffer)
{
	const BWORD hasHull=(hullPointMap && hullFacePoint);

//	if(vertices>8)
//	{
//		feLog("DrawHydra::drawPolys"
//				" vertices %d grain %d hull %d buffer %p\n  %s\n",
//				vertices,grain,hasHull,spDrawBuffer.raw(),c_print(drawMode()));
//	}

	const BWORD uvSpace=isUvSpace();
	const BWORD capable=(!uvSpace && (grain==3 || grain==4));

	//* HACK
	if(capable && spDrawBuffer.isNull())
	{
#if FE_DHY_DEBUG
		feLog("DrawHydra::drawPolys createBuffer\n");
#endif
		spDrawBuffer=createBuffer();
	}

	if(!capable || spDrawBuffer.isNull())
	{
//		if(vertices>8)
//		{
//			feLog("DrawHydra::drawPolys bypass %d\n",vertices);
//		}

		DrawOpenGL::drawPolys(grain,vertex,normal,texture,vertices,strip,
				multicolor,color,vertexMap,
				hullPointMap,hullFacePoint,spDrawBuffer);
		return;
	}

	sp<Buffer> spBuffer=spDrawBuffer;
	if(spBuffer.isNull())
	{
		feLog("DrawHydra::drawPolys incompatible buffer\n");
		return;
	}

	if(spBuffer->m_multicolor!=multicolor || spBuffer->m_firstColor!=color[0])
	{
		spBuffer->releaseAll();
	}

	ensureDriver();

	HydraDelegate& rDelegate=m_pHydraDriver->GetDelegate();

	if(!spBuffer->m_current)
	{
		if(uvSpace && !texture)
		{
			feLog("DrawHydra::drawPolys"
					" vertices %d in UV space without UV coordinates\n",
					vertices);
			return;
		}

		//* NOTE no refinement without hull
		const I32 refinement=hasHull? drawMode()->refinement(): 0;
		const BWORD refined=(refinement>0);

		spBuffer->m_pHydraDriver=m_pHydraDriver;
		spBuffer->m_multicolor=multicolor;
		spBuffer->m_firstColor=color[0];
		spBuffer->m_vertices=vertices;

//		const GfVec3r center=rDelegate.PopulateBasicTestSet();

		// VERTEX, UNIFORM, CONSTANT, FACEVARYING, VARYING
		const HdInterpolation uvInterpolation=texture?
				HdInterpolationVertex: HdInterpolationConstant;
		const HdInterpolation colorInterpolation=multicolor?
				HdInterpolationVertex: HdInterpolationConstant;

		I32 pointCount=0;
		I32 faceCount=0;
		GfVec3r* points=NULL;
		int* numVerts=NULL;
		int* verts=NULL;
		int vertCount=0;
		GfVec3r* normals=NULL;
		BWORD allTriangles=TRUE;
		VtValue vtColor;
		VtValue vtUv;
		VtVec4fArray vtColorArray;
		VtVec2fArray vtUvArray;

		TfToken orientation = HdTokens->leftHanded;
		I32 pointIndex0= -1;
		I32 pointIndex1= -1;
		I32 pointIndex2= -1;

		if(hasHull)
		{
			const Array<I32>& rHullPointMap= *hullPointMap;
			const Array<Vector4i>& rHullFacePoint= *hullFacePoint;

			pointCount=rHullPointMap.size();
			faceCount=rHullFacePoint.size();

#if FE_DHY_DEBUG
			feLog("DrawHydra::drawPolys hull points %d faces %d\n",
					pointCount,faceCount);
#endif

			points=new GfVec3r[pointCount];
			numVerts=new int[faceCount];
			verts=new int[faceCount*4];

			if(uvSpace)
			{
				for(I32 pointIndex=0;pointIndex<pointCount;pointIndex++)
				{
					const Vector2& rUV=texture[rHullPointMap[pointIndex]];
					points[pointIndex].Set(rUV[0],rUV[1],Real(0));
				}
			}
			else
			{
				for(I32 pointIndex=0;pointIndex<pointCount;pointIndex++)
				{
					points[pointIndex].Set(
							vertex[rHullPointMap[pointIndex]].temp());
				}
			}

			//* NOTE refinement generates normals (Hydra will complain)
			if(normal)
			{
				if(faceCount)
				{
					const Vector4i& rFacePoint=rHullFacePoint[0];
					pointIndex0=rHullPointMap[rFacePoint[0]];
					pointIndex1=rHullPointMap[rFacePoint[1]];
					pointIndex2=rHullPointMap[rFacePoint[2]];
				}

				if(!refined)
				{
					normals=new GfVec3r[pointCount];

					for(I32 pointIndex=0;pointIndex<pointCount;pointIndex++)
					{
						normals[pointIndex].Set(
								normal[rHullPointMap[pointIndex]].temp());
					}
				}
			}

			for(I32 faceIndex=0;faceIndex<faceCount;faceIndex++)
			{
				const Vector4i& rFacePoint=rHullFacePoint[faceIndex];
				if(rFacePoint[3]<0)
				{
					numVerts[faceIndex]=3;
					verts[vertCount++]=rFacePoint[0];
					verts[vertCount++]=rFacePoint[1];
					verts[vertCount++]=rFacePoint[2];
				}
				else
				{
					numVerts[faceIndex]=4;
					verts[vertCount++]=rFacePoint[0];
					verts[vertCount++]=rFacePoint[1];
					verts[vertCount++]=rFacePoint[2];
					verts[vertCount++]=rFacePoint[3];
					allTriangles=FALSE;
				}
			}

			const BWORD boostAlpha=(drawStyle()==DrawMode::e_wireframe);

			if(multicolor)
			{
				vtColorArray.resize(pointCount);
				for(I32 pointIndex=0;pointIndex<pointCount;pointIndex++)
				{
					const Color& rColor=color[rHullPointMap[pointIndex]];
					Real alpha=rColor[3];
					if(boostAlpha)
					{
						alpha=pow(alpha,0.5);
					}

					vtColorArray[pointIndex]=GfVec4f(
							rColor[0],rColor[1],rColor[2],
							fe::maximum(alpha,m_alphaMin));
				}
				vtColor = VtValue(vtColorArray);
			}
			else
			{
				const Color& rColor=color[0];
				Real alpha=rColor[3];
				if(boostAlpha)
				{
					alpha=pow(alpha,0.5);
				}

				vtColor=GfVec4f(rColor[0],rColor[1],rColor[2],
						fe::maximum(alpha,m_alphaMin));
			}

			if(texture)
			{
				vtUvArray.resize(pointCount);
				for(I32 pointIndex=0;pointIndex<pointCount;pointIndex++)
				{
					const Vector2& rUv=texture[rHullPointMap[pointIndex]];

					vtUvArray[pointIndex]=GfVec2f(rUv[0],rUv[1]);
				}
				vtUv = VtValue(vtUvArray);
			}
		}
		else
		{
			if(strip==DrawI::e_strip)
			{
				faceCount=vertices-2;
				vertCount=faceCount*3;
				pointCount=vertices;

#if FE_DHY_DEBUG
				feLog("DrawHydra::drawPolys tristrip points %d faces %d\n",
						pointCount,faceCount);
#endif

				verts=new int[pointCount];

				BWORD flip=FALSE;
				for(I32 faceIndex=0;faceIndex<faceCount;faceIndex++)
				{
					for(I32 m=0;m<3;m++)
					{
						verts[faceIndex*3+m]=flip? faceIndex+2-m: faceIndex+m;
					}

					flip=!flip;
				}
			}
			else
			{
				pointCount=vertices;
				faceCount=vertices/grain;

#if FE_DHY_DEBUG
				feLog("DrawHydra::drawPolys discrete points %d faces %d\n",
						pointCount,faceCount);
#endif

				verts=new int[pointCount];

				for(I32 pointIndex=0;pointIndex<pointCount;pointIndex++)
				{
					verts[pointIndex]=pointIndex;
				}

				vertCount=pointCount;
				allTriangles=(grain==3);
			}

			points=new GfVec3r[pointCount];
			numVerts=new int[faceCount];

			if(uvSpace)
			{
				for(I32 pointIndex=0;pointIndex<pointCount;pointIndex++)
				{
					const Vector2& rUV=texture[pointIndex];
					points[pointIndex].Set(rUV[0],rUV[1],Real(0));
				}
			}
			else
			{
				for(I32 pointIndex=0;pointIndex<pointCount;pointIndex++)
				{
					points[pointIndex].Set(vertex[pointIndex].temp());
				}
			}

			if(normal)
			{
				normals=new GfVec3r[pointCount];

				for(I32 pointIndex=0;pointIndex<pointCount;pointIndex++)
				{
					normals[pointIndex].Set(normal[pointIndex].temp());
				}

				pointIndex0=0;
				pointIndex1=1;
				pointIndex2=2;
			}

			for(I32 faceIndex=0;faceIndex<faceCount;faceIndex++)
			{
				numVerts[faceIndex]=grain;
			}

			if(multicolor)
			{
				vtColorArray.resize(pointCount);
				for(I32 pointIndex=0;pointIndex<pointCount;pointIndex++)
				{
					const Color& rColor=color[pointIndex];

					vtColorArray[pointIndex]=GfVec4f(
							rColor[0],rColor[1],rColor[2],
							fe::maximum(Real(rColor[3]),m_alphaMin));
				}
				vtColor = VtValue(vtColorArray);
			}
			else
			{
				const Color& rColor=color[0];

				vtColor=GfVec4f(rColor[0],rColor[1],rColor[2],
						fe::maximum(Real(rColor[3]),m_alphaMin));
			}

			if(texture)
			{
				vtUvArray.resize(pointCount);
				for(I32 pointIndex=0;pointIndex<pointCount;pointIndex++)
				{
					const Vector2& rUv=texture[pointIndex];

					vtUvArray[pointIndex]=GfVec2f(rUv[0],rUv[1]);
				}
				vtUv = VtValue(vtUvArray);
			}
		}

		if(pointIndex0>=0)
		{
			const SpatialVector& p0=vertex[pointIndex0];
			const SpatialVector& p1=vertex[pointIndex1];
			const SpatialVector& p2=vertex[pointIndex2];
			const SpatialVector normalCalc=
					unitSafe(cross(p1-p0,p2-p0));

			const SpatialVector& n0=normal[pointIndex0];
			const SpatialVector& n1=normal[pointIndex1];
			const SpatialVector& n2=normal[pointIndex2];
			const SpatialVector normalAve=
					unitSafe(n0+n1+n2);

			const Real normalDot=dot(normalCalc,normalAve);
			if(normalDot>Real(0))
			{
				orientation = HdTokens->rightHanded;
			}
		}

		VtVec3rArray normalArray;
		if(normals)
		{
			normalArray=_BuildArray(normals,pointCount);

			delete[] normals;
			normals=NULL;
		}

		if(spBuffer->m_nodeName.empty())
		{
			spBuffer->m_nodeName=convertName(spBuffer->name());
		}

		const SdfPath id(("/"+spBuffer->m_nodeName).c_str());
		const GfMatrix4f transform(1);
		const bool guide = false;
		const PxOsdSubdivTags subdivTags;
		const TfToken scheme = allTriangles?
				PxOsdOpenSubdivTokens->loop:
				PxOsdOpenSubdivTokens->catmullClark;
		const bool doubleSided=false;

		rDelegate.Remove(id);
		m_bufferMap.erase(spBuffer->m_nodeName);

//		feLog("AddMesh %p \"%s\"\n",
//				spBuffer.raw(),spBuffer->m_nodeName.c_str());

//		feLog("DrawHydra::drawPolys pointCount %d faceCount %d vertCount %d\n",
//				pointCount,faceCount,vertCount);

		SdfPath instancerId;
		rDelegate.AddMesh(id,transform,
				_BuildArray(points, pointCount),
				_BuildArray(numVerts, faceCount),
				_BuildArray(verts, vertCount),
				normalArray,
				subdivTags,
				vtUv,uvInterpolation,
				vtColor,colorInterpolation,
				guide,instancerId,scheme,orientation,doubleSided);

		//* NOTE choices: hull, smoothHull, wire, wireOnSurf,
		//*		refined, refinedWire, refinedWireOnSurf
		rDelegate.SetReprSelector(id,HdReprSelector(
				drawStyle()==DrawMode::e_wireframe?
				(refined? HdReprTokens->refinedWire: HdReprTokens->wire):
				(drawStyle()==DrawMode::e_edgedSolid?
				(refined? HdReprTokens->refinedWireOnSurf:
				HdReprTokens->wireOnSurf):
				(refined? HdReprTokens->refined: HdReprTokens->smoothHull))));

		rDelegate.SetRefineLevel(id,refinement);

		rDelegate.BindMaterial(id, SdfPath("/material"));

		spBuffer->m_current=TRUE;

#if FE_DHY_DEBUG
		feLog("DrawHydra::drawPolys new vertices %d\n  %s\n",
				vertices,c_print(drawMode()));
#endif

		delete[] verts;
		verts=NULL;

		delete[] numVerts;
		numVerts=NULL;

		delete[] points;
		points=NULL;

	}

	spBuffer->m_recent=1;

	m_bufferMap[spBuffer->m_nodeName]=spBuffer;

	updateNode(spDrawBuffer);
}

void DrawHydra::updateNode(sp<Buffer> spDrawBuffer)
{
	sp<Buffer> spBuffer=spDrawBuffer;
	if(spBuffer.isNull())
	{
		return;
	}

	if(!m_pHydraDriver)
	{
		return;
	}

#if FE_DHY_TICKER
	U32 tickStart=systemTick();
#endif

	HydraDelegate& rDelegate=m_pHydraDriver->GetDelegate();

#if FE_DHY_OPENCL
	sp<DrawOpenGL::Buffer> spOpenGLBuffer(spDrawBuffer);

	const String memName="cl:pt:P";

	sp<SurfaceAccessibleOpenCL::ClMem> spClMem=
			spOpenGLBuffer->catalog< sp<Counted> >(memName);

#if FE_DHY_VBO_DEBUG
	if(spClMem.isNull())
	{
		feLog("DrawHydra::updateNode buffer %p no point mem\n",
				spDrawBuffer.raw());
	}
#endif

	//* TODO make this the first check before getting VBO
	cl_mem clMem4=(spClMem.isValid())?
			spClMem->m_clMem: NULL;
#if FE_DHY_VBO_DEBUG
		feLog("DrawHydra::updateNode point mem %p\n",clMem4);
#endif
	if(clMem4)
	{
		GLuint vboVertex=0;

		//* TODO might not be HdReprTokens->refined

		HdTaskSharedPtrVector tasks=rDelegate.GetRenderTasks();
		const I32 taskCount=tasks.size();
		for(I32 taskIndex=0;taskIndex<taskCount;taskIndex++)
		{
			HdTaskSharedPtr task=tasks[taskIndex];
			const TfTokenVector& rRenderTags=task->GetRenderTags();

			HdRenderIndex& rRenderIndex = rDelegate.GetRenderIndex();
			HdRprimCollection collection(HdTokens->geometry,
					HdReprSelector(HdReprTokens->refined));
			const HdRenderIndex::HdDrawItemPtrVector& rHdDrawItemPtrVector=
					rRenderIndex.GetDrawItems(collection,rRenderTags);

			const I32 drawItemCount=rHdDrawItemPtrVector.size();
#if FE_DHY_VBO_DEBUG
			feLog("  drawItemCount size %d\n",drawItemCount);
#endif

			for(I32 drawItemIndex=0;drawItemIndex<drawItemCount;
					drawItemIndex++)
			{
				HdDrawItem const* pHdDrawItem=
						rHdDrawItemPtrVector[drawItemIndex];
#if FE_DHY_VBO_DEBUG
				feLog("    %d/%d %p hash %p\n",
						drawItemIndex,drawItemCount,pHdDrawItem,
						pHdDrawItem->GetBufferArraysHash());
#endif

				std::ostringstream stream;
				stream<<(*pHdDrawItem);
#if FE_DHY_VBO_DEBUG
				feLog("%s\n",stream.str().c_str());
#endif

#if HD_API_VERSION>=32
				HdBufferArrayRangeSharedPtr const&
						rHdBufferArrayRangeSharedPtr=
						pHdDrawItem->GetVertexPrimvarRange();
#else
				HdBufferArrayRangeSharedPtr const&
						rHdBufferArrayRangeSharedPtr=
						pHdDrawItem->GetVertexPrimVarRange();
#endif

				if(!rHdBufferArrayRangeSharedPtr)
				{
#if FE_DHY_VBO_DEBUG
					feLog("    NULL range\n");
#endif
					continue;
				}

#if FE_DHY_VBO_DEBUG
				const I32 elementCount=
						rHdBufferArrayRangeSharedPtr->GetNumElements();
				feLog("    elementCount %d\n",elementCount);
#endif

				//* see ext/usd/USD/pxr/imaging/lib/hdSt/mesh.cpp
#if HD_API_VERSION>=35
				HdStBufferArrayRangeSharedPtr const&
						rHdStBufferArrayRangeSharedPtr=
						std::static_pointer_cast<HdStBufferArrayRange>(
						rHdBufferArrayRangeSharedPtr);
#elif HD_API_VERSION>=32
				HdStBufferArrayRangeGLSharedPtr const&
						rHdStBufferArrayRangeGLSharedPtr=
						std::static_pointer_cast<HdStBufferArrayRangeGL>(
						rHdBufferArrayRangeSharedPtr);
#else
				HdBufferArrayRangeGLSharedPtr const&
						rHdStBufferArrayRangeGLSharedPtr=
						boost::static_pointer_cast<HdBufferArrayRangeGL>(
						rHdBufferArrayRangeSharedPtr);
#endif

#if FE_DHY_VBO_DEBUG
#if HD_API_VERSION>=35
				HdStBufferResourceGLNamedList const& resourceList=
						rHdStBufferArrayRangeSharedPtr->GetResources();
#elif HD_API_VERSION>=32
				HdStBufferResourceGLNamedList const& resourceList=
						rHdStBufferArrayRangeGLSharedPtr->GetResources();
#else
				HdBufferResourceGLNamedList const& resourceList=
						rHdStBufferArrayRangeGLSharedPtr->GetResources();
#endif
				const I32 resourceCount=resourceList.size();
				for(I32 resourceIndex=0;resourceIndex<resourceCount;
						resourceIndex++)
				{
#if HD_API_VERSION>=32
					const std::pair<TfToken, HdStBufferResourceGLSharedPtr>&
							pair=resourceList[resourceIndex];
#else
					const std::pair<TfToken, HdBufferResourceGLSharedPtr>&
							pair=resourceList[resourceIndex];
#endif
					const String name=pair.first.GetText();
					feLog("    resource %d/%d \"%s\"\n",
							resourceIndex,resourceCount,name.c_str());

#if HD_API_VERSION>=32
					HdStBufferResourceGLSharedPtr resource=pair.second;
#else
					HdBufferResourceGLSharedPtr resource=pair.second;
#endif

					const GLuint vbo=resource->GetId()->GetRawResource();
					const GLuint textureBuffer=
							resource->GetTextureBuffer();
					const uint64_t gpuAddress=
							resource->GetGPUAddress();
					feLog("      vbo %d textureBuffer %d gpuAddress %d\n",
							vbo,textureBuffer,gpuAddress);

					const int offset=resource->GetOffset();
					const int stride=resource->GetStride();
					feLog("      offset %d stride %d\n",
							offset,stride);
				}
#endif

#if HD_API_VERSION>=35
				HdStBufferResourceSharedPtr pointsResource=
						rHdStBufferArrayRangeSharedPtr->GetResource(
						HdTokens->points);
#elif HD_API_VERSION>=32
				HdStBufferResourceGLSharedPtr pointsResource=
						rHdStBufferArrayRangeGLSharedPtr->GetResource(
						HdTokens->points);
#else
				HdBufferResourceGLSharedPtr pointsResource=
						rHdStBufferArrayRangeGLSharedPtr->GetResource(
						HdTokens->points);
#endif

				if(pointsResource)
				{
#if HD_API_VERSION>=35
					vboVertex = pointsResource->GetId()->GetRawResource();
#else
					vboVertex = pointsResource->GetId();
#endif

#if FE_DHY_VBO_DEBUG
					feLog("    vboVertex %d\n",vboVertex);
#endif
				}
			}
		}

		//* NOTE mostly copied from DrawOpenGL.cc
		if(vboVertex)
		{
			sp<Master> spMaster=registry()->master();
			sp<Catalog> spMasterCatalog=spMaster->catalog();
			cl_context clContext=
					(cl_context)spMasterCatalog->catalogOrDefault<void*>(
					"clContext",NULL);
			cl_command_queue clCommandQueue=(cl_command_queue)
					spMasterCatalog->catalogOrDefault<void*>(
					"clCommandQueue2",NULL);

			if(clContext && clCommandQueue)
			{
				cl_int clError=0;

#if FALSE
				cl_uint refCount=0;
				clError=clGetContextInfo(clContext,
						CL_CONTEXT_REFERENCE_COUNT,
						sizeof(cl_uint),&refCount,NULL);
				if(clError)
				{
					feLog("clGetContextInfo vertex clError %d\n",
							clError);
				}
				feLog("clGetContextInfo refCount %d\n",refCount);
#endif

#if FALSE
				for(I32 delta= -16;delta<17;delta++)
				{
					cl_mem clMemTest=
							clCreateFromGLBuffer(clContext,
							CL_MEM_READ_WRITE,vboVertex+delta,
							&clError);
						feLog("  vbo %d clError %d\n",
								vboVertex+delta,clError);
				}
#endif

				cl_mem clMemVertex=
						clCreateFromGLBuffer(clContext,
						CL_MEM_READ_WRITE,vboVertex,
						&clError);
				if(clError)
				{
					feLog("DrawHydra::updateNode"
							" clCreateFromGLBuffer vertex clError %d"
							"  (check if cl_context created with GLX)\n",
							clError);
				}

				clError=clEnqueueAcquireGLObjects(
						clCommandQueue,1,
						&clMemVertex,
						0,NULL,NULL);
				if(clError)
				{
					feLog("clEnqueueAcquireGLObjects clError %d\n",
							clError);
				}

				feLog("---- UPDATE VBO %d ----\n",vboVertex);

#if FE_DHY_DATA_DEBUG
				const I32 vectorCount=8;
				const I32 debugSize3=vectorCount*sizeof(Vector3);
				const I32 debugCount3=vectorCount*3;
				float debugArray3[debugCount3];
				for(I32 m=0;m<debugCount3;m++)
				{
					debugArray3[m]=float(0);
				}

				clError=clEnqueueReadBuffer(clCommandQueue,clMemVertex,
						CL_TRUE,0,debugSize3,debugArray3,0,nullptr,nullptr);

				feLog("OLD GL:\n");
				for(I32 row=0;row<vectorCount;row++)
				{
					const I32 row3=row*3;
					const Vector3 vector3(
							debugArray3[row3],
							debugArray3[row3+1],
							debugArray3[row3+2]);

					feLog("  %d: %s\n",row,c_print(vector3));
				}

				const I32 debugSize4=vectorCount*sizeof(Vector4);
				const I32 debugCount4=vectorCount*4;
				float debugArray4[debugCount4];
				for(I32 m=0;m<debugCount4;m++)
				{
					debugArray4[m]=float(0);
				}

				clError=clEnqueueReadBuffer(clCommandQueue,clMem4,
						CL_TRUE,0,debugSize4,debugArray4,0,nullptr,nullptr);

				feLog("NEW CL:\n");
				for(I32 row=0;row<vectorCount;row++)
				{
					const I32 row4=row*4;
					const Vector4 vector4(
							debugArray4[row4],
							debugArray4[row4+1],
							debugArray4[row4+2],
							debugArray4[row4+3]);

					feLog("  %d: %s\n",row,c_print(vector4));
				}
#endif

				const I32 vertices=spDrawBuffer->m_vertices;

				//* convert CL 4-vector to GL 3-vector
				const size_t src_origin[3]={0,0,0};
				const size_t dest_origin[3]={0,0,0};
				const size_t region[3]={12,U32(vertices),1};
				clError=clEnqueueCopyBufferRect(
						clCommandQueue,clMem4,
						clMemVertex,
						src_origin,dest_origin,region,
						16,16*vertices,
						12,12*vertices,
						0,NULL,NULL);
				if(clError)
				{
					feLog("clEnqueueCopyBufferRect clError %d\n",
							clError);
				}

#if FE_DHY_DATA_DEBUG
				for(I32 m=0;m<debugCount3;m++)
				{
					debugArray3[m]=float(0);
				}

				clError=clEnqueueReadBuffer(clCommandQueue,clMemVertex,
						CL_TRUE,0,debugSize3,debugArray3,0,nullptr,nullptr);

				feLog("NEW GL:\n");
				for(I32 row=0;row<vectorCount;row++)
				{
					const I32 row3=row*3;
					const Vector3 vector3(
							debugArray3[row3],
							debugArray3[row3+1],
							debugArray3[row3+2]);

					feLog("  %d: %s\n",row,c_print(vector3));
				}
#endif

				clError=clEnqueueReleaseGLObjects(
						clCommandQueue,1,
						&clMemVertex,
						0,NULL,NULL);
				if(clError)
				{
					feLog("clEnqueueReleaseGLObjects clError %d\n",
							clError);
				}
			}
		}
	}
#endif

#if FE_DHY_TICKER
	U32 tickEnd=systemTick();
	U32 tickDiff=SystemTicker::tickDifference(tickStart,tickEnd);
	Real ms=1e-3f*tickDiff*
			SystemTicker::microsecondsPerTick();
	feLog("DrawHydra::updateNode transfer %.6f ms \"%s\"\n",
			ms,spBuffer->m_nodeName.c_str());
#endif
}

void DrawHydra::drawNodes(void)
{
#if FE_DHY_DEBUG
	feLog("DrawHydra::drawNodes driver %p \n",m_pHydraDriver);
#endif

	if(!m_pHydraDriver)
	{
		return;
	}

	HydraDelegate& rDelegate=m_pHydraDriver->GetDelegate();

//	feLog("---- PRUNING ----\n");

	//* prune old nodes in usd tree
	for(std::map<String, sp<Buffer> >::iterator it=m_bufferMap.begin();
			it!=m_bufferMap.end();)
	{
		String nodeName=it->first;
		sp<Buffer> spBuffer2=it->second;
		it++;

		if(spBuffer2.isNull())
		{
			feLog("DrawHydra::drawNodes invalid buffer for \"%s\"\n",
					nodeName.c_str());
			m_bufferMap.erase(nodeName);
			continue;
		}

		if(spBuffer2->m_recent<1)
		{
#if FE_DHY_DEBUG
#endif
			feLog("DrawHydra::drawNodes prune\n  \"%s\"\n",
					spBuffer2->m_nodeName.c_str());

			m_bufferMap.erase(nodeName);

			const SdfPath id(("/"+spBuffer2->m_nodeName).c_str());
			rDelegate.Remove(id);
			spBuffer2->releaseAll();
			continue;
		}

		spBuffer2->m_recent--;
	}

	const I32 newRefineLevel=drawMode()->refinement();
	if(m_refineLevel!=newRefineLevel)
	{
		//* TODO per mesh

		m_refineLevel=newRefineLevel;
//		feLog("SetRefineLevel global %d\n",m_refineLevel);
		rDelegate.SetRefineLevel(m_refineLevel);
	}

//	feLog("---- REDRAW ----\n");

	view()->use(ViewI::e_perspective);
	updateCameraAndLighting();

	//* HACK wireframe line width
//	glPushAttrib(GL_LINE_BIT);
//	glLineWidth(drawMode()->lineWidth());

	HdRenderPassStateSharedPtr const& rRenderPassState=
			m_pHydraDriver->GetRenderPassState();
	if(rRenderPassState)
	{
		//* NOTE much of this is now done in HydraDelegate with
		//* AddRenderSetupTask() and AdjustRenderSetupTask()

		//* NOTE otherwise, alpha below 0.5 just disappears
		rRenderPassState->SetAlphaToCoverageUseDefault(false);
//		rRenderPassState->SetAlphaToCoverageEnabled(false);
//		rRenderPassState->SetAlphaThreshold(0.01);

		rRenderPassState->SetLineWidth(drawMode()->lineWidth());
	}

	m_pHydraDriver->Draw(m_engine);

//	glPopAttrib();
}

} /* namespace ext */
} /* namespace fe */
