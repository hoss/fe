/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __usd_SurfaceAccessorUsd_h__
#define __usd_SurfaceAccessorUsd_h__
namespace fe
{
namespace ext
{

/**************************************************************************//**
    @brief USD Surface Editing Implementation

	@ingroup usd
*//***************************************************************************/
class FE_DL_EXPORT SurfaceAccessorUsd:
	public SurfaceAccessorBase
{
	public:
						SurfaceAccessorUsd(void)
						{	setName("SurfaceAccessorUsd"); }
virtual					~SurfaceAccessorUsd(void)
						{	writeBack(); }

						using SurfaceAccessorBase::set;
						using SurfaceAccessorBase::append;
						using SurfaceAccessorBase::spatialVector;

						//* as SurfaceAccessorI
		BWORD			bind(SurfaceAccessibleI::Element a_element,
								SurfaceAccessibleI::Attribute a_attribute);
		BWORD			bind(SurfaceAccessibleI::Element a_element,
								const String& a_name);

virtual	U32				count(void) const;
virtual	U32				subCount(U32 a_index) const;

virtual	void			set(U32 a_index,U32 a_subIndex,String a_string);
virtual	String			string(U32 a_index,U32 a_subIndex=0);

virtual	void			set(U32 a_index,U32 a_subIndex,I32 a_integer);
virtual	I32				integer(U32 a_index,U32 a_subIndex=0);

						//* TODO
virtual	I32				append(SurfaceAccessibleI::Form a_form)
						{	return -1; }
virtual	void			append(U32 a_index,I32 a_integer)					{}

virtual	void			set(U32 a_index,U32 a_subIndex,Real a_real);
virtual	Real			real(U32 a_index,U32 a_subIndex=0);

virtual	void			set(U32 a_index,U32 a_subIndex,
								const SpatialVector& a_vector);
virtual	SpatialVector	spatialVector(U32 a_index,U32 a_subIndex=0);

						//* USD specific
		void			setPrimNode(sp<SurfaceAccessibleUsd::PrimNode>
								a_spPrimNode)
						{
							m_spPrimNode=a_spPrimNode;
							if(m_spPrimNode.isValid())
							{
								m_usdPrim=m_spPrimNode->usdPrim();
							}
						}

		sp<SurfaceAccessibleUsd::PrimNode>&		primNode(void)
						{ return m_spPrimNode; }

		void			setFrame(Real a_frame)
						{	m_frame=a_frame; }

		void			setYToZ(BWORD a_ytoz)	{ m_ytoz=a_ytoz; }

	private:

virtual	BWORD			bindInternal(SurfaceAccessibleI::Element a_element,
								const String& a_name);

		I32				indexOfVertex(I32 a_index,I32 a_subIndex)
						{
							FEASSERT(a_index<I32(m_primTable.size()));
							FEASSERT(a_subIndex<m_primTable[a_index][1]);
							if(a_index>=I32(m_primTable.size()) ||
									a_subIndex>=m_primTable[a_index][1])
							{
								return -1;
							}
							const U32 tableIndex=
									m_primTable[a_index][0]+a_subIndex;

							const BWORD elementVertex=
									(m_element==SurfaceAccessibleI::e_vertex);
							FEASSERT(elementVertex ||
									tableIndex<m_intArray.size());
							return elementVertex? tableIndex:
									m_intArray[tableIndex];
						}

		void			addString(void);
		void			addInteger(void);
		void			addReal(void);
		void			addSpatialVector(void);

		I32				elementCount(
								SurfaceAccessibleI::Element a_element) const;
		I32				primElemCount(void) const;

		BWORD			isBound(void) const
						{	return m_spPrimNode.isValid() &&
									m_usdPrim.IsValid(); }

		BWORD			matchingCount(SurfaceAccessibleI::Element a_element,
								I32 m_count) const;

		SpatialTransform	getTransform(UsdPrim a_usdPrim);

		BWORD			writeBack(void);

		SpatialTransform					m_transform;
		SpatialTransform					m_inverse;
		Real								m_frame;
		BWORD								m_isCurves;
		BWORD								m_isSkeleton;

		sp<SurfaceAccessibleUsd::PrimNode>	m_spPrimNode;
		UsdPrim								m_usdPrim;
		UsdAttribute						m_usdAttribute;
		UsdAttribute						m_mappingAttribute;
		String								m_usdAttrName;

											//* first vertex, subCount
		Array<Vector2i>						m_primTable;

		BWORD								m_ytoz;
		BWORD								m_changed;
		VtArray<TfToken>					m_tokenArray;
		VtArray<bool>						m_boolArray;
		VtArray<int>						m_intArray;
		VtArray<float>						m_floatArray;
		VtArray<double>						m_doubleArray;
		VtArray<GfVec3f>					m_vec3Array;
		VtArray<GfVec2f>					m_vec2Array;
		VtArray<GfMatrix4d>					m_mat4Array;
		I32									m_row;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __usd_SurfaceAccessorUsd_h__ */
