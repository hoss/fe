/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <imgui/imgui.pmh>

namespace fe
{
namespace ext
{

ImguiHandlerCatalog::ImguiHandlerCatalog(void):
	m_valueOffset(300),
	m_typeOffset(500),
	m_scopeOffset(600)
{
}

ImguiHandlerCatalog::~ImguiHandlerCatalog(void)
{
}

void ImguiHandlerCatalog::initialize(void)
{
	m_spOscilloscope=registry()->create("*.Oscilloscope");
}

void ImguiHandlerCatalog::update(sp<Catalog> a_spCatalog,String a_label)
{
	if(a_spCatalog.isNull())
	{
		return;
	}

//	a_spCatalog->catalogDump();

	ImGuiWindowFlags window_flags = 0;
//	window_flags |= ImGuiWindowFlags_NoTitleBar;
//	window_flags |= ImGuiWindowFlags_NoScrollbar;
//	window_flags |= ImGuiWindowFlags_MenuBar;
//	window_flags |= ImGuiWindowFlags_NoMove;
//	window_flags |= ImGuiWindowFlags_NoResize;
//	window_flags |= ImGuiWindowFlags_NoCollapse;
//	window_flags |= ImGuiWindowFlags_NoNav;
	window_flags |= ImGuiWindowFlags_NoBackground;
//	window_flags |= ImGuiWindowFlags_NoBringToFrontOnFocus;
//	window_flags |= ImGuiWindowFlags_AlwaysAutoResize;
//	window_flags |= ImGuiWindowFlags_HorizontalScrollbar;

//	ImGui::SetNextWindowSize(ImVec2(1000,0));

//	bool open(true);
//	if(!ImGui::Begin("RecordGroup",&open,window_flags))
	if(!ImGui::Begin("Catalog",NULL,window_flags))
	{
		ImGui::End();
		m_drawn=FALSE;
		return;
	}

	updateCatalog(a_spCatalog,a_label);

	sp<ViewI> spView=m_spDrawI->view();
//	const I32 winx=width(spView->viewport());
	const I32 winy=height(spView->viewport());

	//* NOTE must be between Begin and End
	const ImVec2 windowPos=ImGui::GetWindowPos();
	const ImVec2 windowSize=ImGui::GetWindowSize();

	set(m_windowPosition,
			windowPos[0]*m_pixelScale[0],
			winy-(windowPos[1]+windowSize[1])*m_pixelScale[1]);
	set(m_windowSize,
			windowSize[0]*m_pixelScale[0],
			windowSize[1]*m_pixelScale[1]);

	//* click on nothing
	if(!ImGui::IsAnyItemHovered() && !ImGui::IsAnyItemActive())
	{
		m_selectedKey="";
		m_selectedProperty="";
	}

	ImGui::End();

	m_drawn=TRUE;
}

void ImguiHandlerCatalog::drawOverlay(void)
{
	if(!m_drawn)
	{
		return;
	}

	if(!m_selectedKey.empty())
	{
		m_spOscilloscope->bind(m_spDrawI);

		if(m_lastSelectedKey!=m_selectedKey ||
				m_lastSelectedProperty!=m_selectedProperty)
		{
			m_spOscilloscope->clear();
		}

		const I32 offsetX=m_windowPosition[0]+m_scopeOffset*m_pixelScale[0];

		m_spOscilloscope->setPane(offsetX,m_windowPosition[1],
				m_windowSize[0]-offsetX,m_windowSize[1]);

		const String propertyType=m_spCatalog->catalogTypeName(
				m_selectedKey,m_selectedProperty);

		sp<TypeMaster> spTypeMaster=registry()->master()->typeMaster();
		sp<BaseType> spBaseType=spTypeMaster->lookupName(propertyType);

		if(spBaseType==spTypeMaster->lookupType<bool>())
		{
			const bool value=m_spCatalog->catalog<bool>(
					m_selectedKey,m_selectedProperty);

			m_spOscilloscope->stepValue(value);
		}
		else if(spBaseType==spTypeMaster->lookupType<U8>())
		{
			const U8 value=m_spCatalog->catalog<U8>(
					m_selectedKey,m_selectedProperty);

			m_spOscilloscope->stepValue(value);
		}
		else if(spBaseType==spTypeMaster->lookupType<U32>())
		{
			const U32 value=m_spCatalog->catalog<U32>(
					m_selectedKey,m_selectedProperty);

			m_spOscilloscope->stepValue(value);
		}
		else if(spBaseType==spTypeMaster->lookupType<I32>())
		{
			const I32 value=m_spCatalog->catalog<I32>(
					m_selectedKey,m_selectedProperty);

			m_spOscilloscope->stepValue(value);
		}
		else if(spBaseType==spTypeMaster->lookupType<Real>())
		{
			const Real value=m_spCatalog->catalog<Real>(
					m_selectedKey,m_selectedProperty);

			m_spOscilloscope->stepValue(value);
		}
		else if(spBaseType==spTypeMaster->lookupType<F32>())
		{
			const F64 value=m_spCatalog->catalog<F32>(
					m_selectedKey,m_selectedProperty);

			m_spOscilloscope->stepValue(value);
		}
		else if(spBaseType==spTypeMaster->lookupType<F64>())
		{
			const F64 value=m_spCatalog->catalog<F64>(
					m_selectedKey,m_selectedProperty);

			m_spOscilloscope->stepValue(value);
		}
		else if(spBaseType==spTypeMaster->lookupType<SpatialVector>())
		{
			const SpatialVector value=m_spCatalog->catalog<SpatialVector>(
					m_selectedKey,m_selectedProperty);

			m_spOscilloscope->stepValue(value);
		}
		else if(spBaseType==spTypeMaster->lookupType<Vector3>())
		{
			const Vector3 value=m_spCatalog->catalog<Vector3>(
					m_selectedKey,m_selectedProperty);

			m_spOscilloscope->stepValue(value);
		}
		else if(spBaseType==spTypeMaster->lookupType<Vector3d>())
		{
			const Vector3d value=m_spCatalog->catalog<Vector3d>(
					m_selectedKey,m_selectedProperty);

			m_spOscilloscope->stepValue(value);
		}
		else if(spBaseType==spTypeMaster->lookupType<SpatialEuler>())
		{
			const SpatialEuler value=m_spCatalog->catalog<SpatialEuler>(
					m_selectedKey,m_selectedProperty);

			m_spOscilloscope->stepValue(value);
		}
		else if(spBaseType==spTypeMaster->lookupType<Color>())
		{
			const Color value=m_spCatalog->catalog<Color>(
					m_selectedKey,m_selectedProperty);

			m_spOscilloscope->stepValue(value);
		}
		else if(spBaseType==spTypeMaster->lookupType<SpatialQuaternion>())
		{
			const SpatialQuaternion value=
					m_spCatalog->catalog<SpatialQuaternion>(
					m_selectedKey,m_selectedProperty);

			m_spOscilloscope->stepValue(value);
		}
		else if(spBaseType==spTypeMaster->lookupType<SpatialTransform>())
		{
			const SpatialTransform value=
					m_spCatalog->catalog<SpatialTransform>(
					m_selectedKey,m_selectedProperty);

			m_spOscilloscope->stepValue(value);
		}
		else
		{
			m_spOscilloscope->stepValue();
		}
	}

	m_lastSelectedKey=m_selectedKey;
	m_lastSelectedProperty=m_selectedProperty;
}

void ImguiHandlerCatalog::updateCatalog(sp<Catalog> a_spCatalog,String a_label)
{
	const BWORD doClip=(!m_selectedKey.empty() &&
			!m_selectedProperty.empty());
	if(doClip)
	{
		const ImVec2 windowSize=ImGui::GetWindowSize();

		const ImVec2 clip_rect_min(0,0);
		const ImVec2 clip_rect_max(m_scopeOffset,windowSize[1]);
		const bool intersect_with_current_clip_rect(true);

		ImGui::PushClipRect(clip_rect_min,clip_rect_max,
				intersect_with_current_clip_rect);
	}

	if (ImGui::TreeNodeEx(a_label.c_str(),
			ImGuiTreeNodeFlags_DefaultOpen))
	{
		ImGui::Columns(3, "columns", true);

		//* HACK
		static I32 s_frame=0;
		if(!s_frame++)
		{
			ImGui::SetColumnOffset(1,m_valueOffset);
			ImGui::SetColumnOffset(2,m_typeOffset);
		}

		Array<String> keys;
		a_spCatalog->catalogKeys(keys);

		std::sort(keys.begin(),keys.end(),String::Sort());

		const U32 keyCount=keys.size();
		for(U32 keyIndex=0;keyIndex<keyCount;keyIndex++)
		{
			ImGui::PushID(keyIndex);

			const String key=keys[keyIndex];

			String propertyValue="<no value>";
			String propertyType="<no type>";

			if(a_spCatalog->cataloged(key))
			{
				propertyValue=a_spCatalog->catalogValue(key);
				propertyType=a_spCatalog->catalogTypeName(key);
			}

			//* TODO editable, resizeable
			if(propertyType=="stringarray")
			{
				ImGui::Text(key.c_str());

				ImGui::NextColumn();

				Array<String>& rStringArray=
						a_spCatalog->catalog< Array<String> >(key);
				const I32 stringCount=rStringArray.size();
				for(I32 stringIndex=0;stringIndex<stringCount;stringIndex++)
				{
					ImGui::Text(rStringArray[stringIndex].c_str());
				}

				ImGui::NextColumn();
				ImGui::Text(propertyType.c_str());

				ImGui::NextColumn();
				ImGui::PopID();
				continue;
			}

			Array<String> properties;
			a_spCatalog->catalogProperties(key,properties);

			std::sort(properties.begin(),properties.end(),String::Sort());

			const U32 propertyCount=properties.size();

			bool keyOpen=false;

			if(propertyCount==1 && a_spCatalog->cataloged(key))
			{
				const String property="value";

				const BWORD wasSelected=(key==m_selectedKey &&
						property==m_selectedProperty);
				bool selected=wasSelected;

				if(ImGui::Selectable(key.c_str(),&selected))
				{
					m_selectedKey=key;
					m_selectedProperty=property;
				}
				if(wasSelected && !selected)
				{
					m_selectedKey="";
					m_selectedProperty="";
				}
			}
			else
			{
				keyOpen = ImGui::TreeNode(
						(void*)(intptr_t)keyIndex,key.c_str());
			}

			ImGui::NextColumn();

			ImGui::SetNextItemWidth(-1.0f);	//* no space for label
			if(ImGui::InputText("##key",&propertyValue))
			{
				a_spCatalog->catalogSet(key,propertyValue);
			}

			ImGui::NextColumn();
			ImGui::Text(propertyType.c_str());

			ImGui::NextColumn();

			if(keyOpen)
			{
				for(U32 propertyIndex=0;propertyIndex<propertyCount;
						propertyIndex++)
				{
					const String property=properties[propertyIndex];

					if(!a_spCatalog->cataloged(key,property))
					{
						continue;
					}

					ImGui::PushID(propertyIndex);

					propertyType=a_spCatalog->catalogTypeName(key,property);
					propertyValue=a_spCatalog->catalogValue(key,property);

					const BWORD wasSelected=(key==m_selectedKey &&
							property==m_selectedProperty);
					bool selected=wasSelected;

					if(ImGui::Selectable(property.c_str(),&selected))
					{
//						feLog("SELECT \"%s\" \"%s\"\n",
//								key.c_str(),
//								property.c_str());

						m_selectedKey=key;
						m_selectedProperty=property;
					}
					if(wasSelected && !selected)
					{
//						feLog("DESELECT \"%s\" \"%s\"\n",
//								key.c_str(),
//								property.c_str());

						m_selectedKey="";
						m_selectedProperty="";
					}

					ImGui::NextColumn();

					ImGui::SetNextItemWidth(-1.0f);	//* no space for label
					if(ImGui::InputText("##prop",&propertyValue))
					{
						a_spCatalog->catalogSet(key,property,propertyValue);
					}

					ImGui::NextColumn();
					ImGui::Text(propertyType.c_str());

					ImGui::NextColumn();
					ImGui::PopID();
				}
				ImGui::TreePop();
			}
			ImGui::PopID();
		}

		ImGui::NextColumn();

		ImGui::NextColumn();

		const ImVec2 cursorPos=ImGui::GetCursorPos();
		m_scopeOffset=cursorPos[0]+4;

		ImGui::Columns(1);

		ImGui::TreePop();
	}

	if(doClip)
	{
		ImGui::PopClipRect();
	}
}

} /* namespace ext */
} /* namespace fe */
