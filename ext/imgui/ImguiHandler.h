/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __imgui_ImguiHandler_h__
#define __imgui_ImguiHandler_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Dear Imgui draw and event handler

	@ingroup imgui
*//***************************************************************************/
class FE_DL_EXPORT ImguiHandler:
	virtual public HandlerI,
	public CastableAs<ImguiHandler>
{
	public:
				ImguiHandler(void);
virtual			~ImguiHandler(void);

virtual void	handle(Record& render);

	protected:

virtual void	updateGUI(void)												{}
virtual void	drawOverlay(void)											{}

		sp<DrawI>					m_spDrawI;

		Vector2						m_pixelScale;
		Vector2i					m_windowPosition;
		Vector2i					m_windowSize;

	private:
		AsViewer					m_asViewer;
		WindowEvent					m_event;


		I32							m_mouseX;
		I32							m_mouseY;
		WindowEvent::MouseButtons	m_mouseButtons;
};

} /* namespace ext */
} /* namespace fe */

//* see <imgui>/misc/cpp/imgui_stdlib.h
namespace ImGui
{

// ImGui::InputText() with fe::String
// Because text input needs dynamic resizing,
// we need to setup a callback to grow the capacity
IMGUI_API bool InputText(const char* label,
		fe::String* str,
		ImGuiInputTextFlags flags = 0,
		ImGuiInputTextCallback callback = NULL,
		void* user_data = NULL);
IMGUI_API bool InputTextMultiline(const char* label,
		fe::String* str,
		const ImVec2& size = ImVec2(0, 0),
		ImGuiInputTextFlags flags = 0,
		ImGuiInputTextCallback callback = NULL,
		void* user_data = NULL);
IMGUI_API bool InputTextWithHint(const char* label,
		const char* hint,
		fe::String* str,
		ImGuiInputTextFlags flags = 0,
		ImGuiInputTextCallback callback = NULL,
		void* user_data = NULL);

} // namespace ImGui

#endif /* __imgui_ImguiHandler_h__ */
