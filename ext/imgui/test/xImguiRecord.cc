/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "imgui/imgui.pmh"
#include "viewer/viewer.h"

using namespace fe;
using namespace fe::ext;

int main(int argc,char** argv)
{
	UNIT_START();
	BWORD complete=FALSE;

	if(argc<2)
	{
		feLog("Syntax: %s <filename> [frames]\n",argv[0]);
		exit(1);
	}

	const String filename=argv[1];

	U32 frames=0;
	if(argc>2)
	{
		frames=atoi(argv[2])+1;
	}

	try
	{
		sp<Master> spMaster(new Master);
		assertMath(spMaster->typeMaster());

		sp<Registry> spRegistry=spMaster->registry();

		Result result=spRegistry->manage("feAutoLoadDL");
		UNIT_TEST(successful(result));

		{
			sp<Scope> spScope(spRegistry->create("Scope"));

			feLog("loading \"%s\"\n",filename.c_str());
			std::ifstream inFile(filename.c_str());
			if(!inFile)
			{
				feX("could not open \"%s\"\n",filename.c_str());
			}

			sp<data::StreamI> spStream(new data::AsciiStream(spScope));
			sp<RecordGroup> spRecordGroup=spStream->input(inFile);
			inFile.close();

			sp<QuickViewerI> spQuickViewerI(
					spRegistry->create("QuickViewerI"));
			if(!spQuickViewerI.isValid())
			{
				feX(argv[0], "couldn't create QuickViewerI");
			}

			spQuickViewerI->open();

			sp<DrawI> spDrawI=spQuickViewerI->getDrawI();
			if(spDrawI.isNull())
			{
				feX(argv[0], "couldn't get draw interface");
			}

			sp<ImguiHandlerRecord> spImguiHandlerRecord=
					spRegistry->create("*.ImguiHandlerRecord");
			if(spImguiHandlerRecord.isNull())
			{
				feX("could not create ImguiHandlerRecord");
			}

			spImguiHandlerRecord->bind(spRecordGroup);

			spQuickViewerI->insertDrawHandler(spImguiHandlerRecord);
			spQuickViewerI->insertEventHandler(spImguiHandlerRecord);

#if FALSE
			spQuickViewerI->run(frames);
#else
			Accessor<Real> sine(spScope,"sine");
			Accessor<SpatialVector> spin(spScope,"spin");
			Accessor<SpatialQuaternion> quaternion(spScope,"quaternion");
			Accessor<SpatialTransform> transform(spScope,"transform");

			sp<Layout> spLayout=spScope->declare("test");
			spLayout->populate(sine);
			spLayout->populate(spin);
			spLayout->populate(quaternion);
			spLayout->populate(transform);

			Record record=spScope->createRecord(spLayout);
			spRecordGroup->add(record);

			Real step(0.0);

			while(TRUE)
			{
				step+=1.0;

				const Real cycle=sin(0.02*step);
				const Real mag=20.0*sin(0.007*step);
				const Real offset=200.0*sin(0.003*step);

				sine(record)=cycle*mag+offset;

				const SpatialVector zVector(0.0,0.0,0.7);
				const SpatialQuaternion rotation(0.03*step,
						unitSafe(SpatialVector(0.1,0.2,0.4)));
				SpatialVector direction;
				rotateVector(rotation,zVector,direction);

				spin(record)=direction;
				quaternion(record)=rotation;

				SpatialTransform xform(rotation);
				translate(xform,SpatialVector(1.2,0.0,0.0));

				transform(record)=xform;

				spQuickViewerI->run(1);

				if(frames)
				{
					if(--frames<1)
					{
						break;
					}
				}
			}
#endif
		}

		complete=TRUE;
	}
	catch(Exception &e) { e.log(); }
	catch(...)			{ feLog("uncaught exception\n"); }

	UNIT_TEST(complete);
	UNIT_TEST(!Counted::trackerCount());
	UNIT_TRACK(5);
	UNIT_RETURN();
}

