/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "imgui/imgui.pmh"
#include "viewer/viewer.h"

using namespace fe;
using namespace fe::ext;

int main(int argc,char** argv)
{
	UNIT_START();
	BWORD complete=FALSE;

	if(argc<2)
	{
		feLog("Syntax: %s <filename> [frames]\n",argv[0]);
		exit(1);
	}

	const String filename=argv[1];

	U32 frames=0;
	if(argc>2)
	{
		frames=atoi(argv[2])+1;
	}

	try
	{
		sp<Master> spMaster(new Master);
		sp<Registry> spRegistry=spMaster->registry();

		Result result=spRegistry->manage("feAutoLoadDL");
		UNIT_TEST(successful(result));

		{
			sp<Catalog> spCatalog;

			sp<CatalogReaderI> spCatalogReader=
					spRegistry->create("CatalogReaderI.*.*.json");
			UNIT_TEST(spCatalogReader.isValid());

			if(spCatalogReader.isValid())
			{
				spCatalog=spMaster->createCatalog("input");
				UNIT_TEST(spCatalog.isValid());

				if(spCatalog.isValid())
				{
					feLog("loading \"%s\"\n",filename.c_str());
					spCatalogReader->load(filename,spCatalog);

					feLog("\nCatalog in:\n");
					spCatalog->catalogDump();
				}
			}

			sp<QuickViewerI> spQuickViewerI(
					spRegistry->create("QuickViewerI"));
			if(!spQuickViewerI.isValid())
			{
				feX(argv[0], "couldn't create QuickViewerI");
			}

			spQuickViewerI->open();

			sp<DrawI> spDrawI=spQuickViewerI->getDrawI();
			if(spDrawI.isNull())
			{
				feX(argv[0], "couldn't get draw interface");
			}

			sp<ImguiHandlerCatalog> spImguiHandlerCatalog=
					spRegistry->create("*.ImguiHandlerCatalog");
			if(spImguiHandlerCatalog.isNull())
			{
				feX("could not create ImguiHandlerCatalog");
			}

			spImguiHandlerCatalog->bind(spCatalog);

			spQuickViewerI->insertDrawHandler(spImguiHandlerCatalog);
			spQuickViewerI->insertEventHandler(spImguiHandlerCatalog);
			spQuickViewerI->run(frames);
		}

		complete=TRUE;
	}
	catch(Exception &e) { e.log(); }
	catch(...)			{ feLog("uncaught exception\n"); }

	UNIT_TEST(complete);
	UNIT_TEST(!Counted::trackerCount());
	UNIT_TRACK(5);
	UNIT_RETURN();
}
