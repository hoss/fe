/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <imgui/imgui.pmh>

namespace fe
{
namespace ext
{

ImguiHandlerRecord::ImguiHandlerRecord(void):
	m_columnOffset(300),
	m_scopeOffset(500),
	m_selectedAttrIndex(-1),
	m_lastSelectedAttrIndex(-1)
{
}

ImguiHandlerRecord::~ImguiHandlerRecord(void)
{
}

void ImguiHandlerRecord::initialize(void)
{
	m_spOscilloscope=registry()->create("*.Oscilloscope");
}

void ImguiHandlerRecord::update(sp<RecordGroup> a_spRecordGroup,String a_label)
{
	sp<Scope> spScope=a_spRecordGroup->scope();

	m_spRecordGroupType =
		spScope->typeMaster()->lookupType<sp<RecordGroup> >();
	m_spRecordArrayType =
		spScope->typeMaster()->lookupType<sp<RecordArray> >();
	m_spRecordType =
		spScope->typeMaster()->lookupType<Record>();
	m_spWeakRecordType =
		spScope->typeMaster()->lookupType<WeakRecord>();
	m_spU8Type =
		spScope->typeMaster()->lookupType<U8>();
	m_spU32Type =
		spScope->typeMaster()->lookupType<U32>();
	m_spI32Type =
		spScope->typeMaster()->lookupType<I32>();
	m_spRealType =
		spScope->typeMaster()->lookupType<Real>();
	m_spF64Type =
		spScope->typeMaster()->lookupType<F64>();
	m_spSpatialVectorType =
		spScope->typeMaster()->lookupType<SpatialVector>();
	m_spSpatialEulerType =
		spScope->typeMaster()->lookupType<SpatialEuler>();
	m_spColorType =
		spScope->typeMaster()->lookupType<Color>();
	m_spSpatialQuaternionType =
		spScope->typeMaster()->lookupType<SpatialQuaternion>();
	m_spSpatialTransformType =
		spScope->typeMaster()->lookupType<SpatialTransform>();
	m_spVoidType =
		spScope->typeMaster()->lookupType<void>();

	ImGuiWindowFlags window_flags = 0;
//	window_flags |= ImGuiWindowFlags_NoTitleBar;
//	window_flags |= ImGuiWindowFlags_NoScrollbar;
//	window_flags |= ImGuiWindowFlags_MenuBar;
//	window_flags |= ImGuiWindowFlags_NoMove;
//	window_flags |= ImGuiWindowFlags_NoResize;
//	window_flags |= ImGuiWindowFlags_NoCollapse;
//	window_flags |= ImGuiWindowFlags_NoNav;
	window_flags |= ImGuiWindowFlags_NoBackground;
//	window_flags |= ImGuiWindowFlags_NoBringToFrontOnFocus;
//	window_flags |= ImGuiWindowFlags_AlwaysAutoResize;
//	window_flags |= ImGuiWindowFlags_HorizontalScrollbar;

//	ImGui::SetNextWindowSize(ImVec2(1000,0));

//	bool open(true);
//	if(!ImGui::Begin("RecordGroup",&open,window_flags))
	if(!ImGui::Begin("RecordGroup",NULL,window_flags))
	{
		ImGui::End();
		m_drawn=FALSE;
		return;
	}

	ImGui::Columns(3, "columns", true);

	//* HACK
	static I32 s_frame=0;
	if(!s_frame++)
	{
		ImGui::SetColumnOffset(1,m_columnOffset);
		ImGui::SetColumnOffset(2,m_scopeOffset);
	}

	updateRecordGroup(a_spRecordGroup,a_label);

	ImGui::Columns(1);

	sp<ViewI> spView=m_spDrawI->view();
//	const I32 winx=width(spView->viewport());
	const I32 winy=height(spView->viewport());

	//* NOTE must be between Begin and End
	const ImVec2 windowPos=ImGui::GetWindowPos();
	const ImVec2 windowSize=ImGui::GetWindowSize();

	set(m_windowPosition,
			windowPos[0]*m_pixelScale[0],
			winy-(windowPos[1]+windowSize[1])*m_pixelScale[1]);
	set(m_windowSize,
			windowSize[0]*m_pixelScale[0],
			windowSize[1]*m_pixelScale[1]);

//	feLog("pos %s size %s pixelScale %s\n",
//			c_print(m_windowPosition),c_print(m_windowSize),
//			c_print(m_pixelScale));

#if FALSE
	static I32 lastAnyHovered=0;
	static I32 lastAnyActive=0;
	static I32 lastAnyFocused=0;

	if(lastAnyHovered!=ImGui::IsAnyItemHovered() ||
			lastAnyActive!=ImGui::IsAnyItemActive() ||
			lastAnyFocused!=ImGui::IsAnyItemFocused())
	{
		feLog("any %d %d %d\n",ImGui::IsAnyItemHovered(),
				ImGui::IsAnyItemActive(),ImGui::IsAnyItemFocused());
	}

	lastAnyHovered=ImGui::IsAnyItemHovered();
	lastAnyActive=ImGui::IsAnyItemActive();
	lastAnyFocused=ImGui::IsAnyItemFocused();
#endif

	//* click on nothing
	if(!ImGui::IsAnyItemHovered() && !ImGui::IsAnyItemActive())
	{
		m_selectedRecord=Record();
		m_selectedAttrIndex= -1;
	}

	ImGui::End();

	m_drawn=TRUE;
}

void ImguiHandlerRecord::drawOverlay(void)
{
	if(!m_drawn)
	{
		return;
	}

	sp<Scope> spScope=m_spRecordGroup->scope();

	if(m_selectedRecord.isValid())
	{
		m_spOscilloscope->bind(m_spDrawI);

		if(m_lastSelectedRecord!=m_selectedRecord ||
				m_lastSelectedAttrIndex!=m_selectedAttrIndex)
		{
			m_spOscilloscope->clear();
		}

		const I32 offsetX=m_windowPosition[0]+m_scopeOffset*m_pixelScale[0];

		m_spOscilloscope->setPane(offsetX,m_windowPosition[1],
				m_windowSize[0]-offsetX,m_windowSize[1]);

		sp<BaseType> spBT=spScope->attribute(m_selectedAttrIndex)->type();

		if(spBT==m_spU8Type)
		{
			void* instance=m_selectedRecord.rawAttribute(m_selectedAttrIndex);
			const U8 value=*(U8 *)instance;

			m_spOscilloscope->stepValue(value);
		}
		else if(spBT==m_spU32Type)
		{
			void* instance=m_selectedRecord.rawAttribute(m_selectedAttrIndex);
			const U32 value=*(U32 *)instance;

			m_spOscilloscope->stepValue(value);
		}
		else if(spBT==m_spI32Type)
		{
			void* instance=m_selectedRecord.rawAttribute(m_selectedAttrIndex);
			const I32 value=*(I32 *)instance;

			m_spOscilloscope->stepValue(value);
		}
		else if(spBT==m_spRealType)
		{
			void* instance=m_selectedRecord.rawAttribute(m_selectedAttrIndex);
			const Real value=*(Real *)instance;

			m_spOscilloscope->stepValue(value);
		}
		else if(spBT==m_spF64Type)
		{
			void* instance=m_selectedRecord.rawAttribute(m_selectedAttrIndex);
			const F64 value=*(F64 *)instance;

			m_spOscilloscope->stepValue(value);
		}
		else if(spBT==m_spSpatialVectorType)
		{
			void* instance=m_selectedRecord.rawAttribute(m_selectedAttrIndex);
			const SpatialVector value=*(SpatialVector *)instance;

			m_spOscilloscope->stepValue(value);
		}
		else if(spBT==m_spSpatialEulerType)
		{
			void* instance=m_selectedRecord.rawAttribute(m_selectedAttrIndex);
			const SpatialEuler value=*(SpatialEuler *)instance;

			m_spOscilloscope->stepValue(value);
		}
		else if(spBT==m_spColorType)
		{
			void* instance=m_selectedRecord.rawAttribute(m_selectedAttrIndex);
			const Color value=*(Color *)instance;

			m_spOscilloscope->stepValue(value);
		}
		else if(spBT==m_spSpatialQuaternionType)
		{
			void* instance=m_selectedRecord.rawAttribute(m_selectedAttrIndex);
			const SpatialQuaternion value=*(SpatialQuaternion *)instance;

			m_spOscilloscope->stepValue(value);
		}
		else if(spBT==m_spSpatialTransformType)
		{
			void* instance=m_selectedRecord.rawAttribute(m_selectedAttrIndex);
			const SpatialTransform value=*(SpatialTransform *)instance;

			m_spOscilloscope->stepValue(value);
		}
		else
		{
			m_spOscilloscope->stepValue();
		}
	}

	m_lastSelectedRecord=m_selectedRecord;
	m_lastSelectedAttrIndex=m_selectedAttrIndex;
}

void ImguiHandlerRecord::updateRecordGroup(sp<RecordGroup> a_spRecordGroup,
	String a_label)
{
	if(a_spRecordGroup.isNull())
	{
		return;
	}

	if(!ImGui::TreeNodeEx(a_label.c_str(),
			ImGuiTreeNodeFlags_DefaultOpen))
	{
		return;
	}

	I32 layoutIndex=0;
	for(RecordGroup::iterator it=a_spRecordGroup->begin();
			it!=a_spRecordGroup->end();it++)
	{
		sp<RecordArray> spRA(*it);
		sp<Layout> spLayout(spRA->layout());
		sp<Scope> spScope=spLayout->scope();

		ImGui::NextColumn();
		ImGui::NextColumn();
		ImGui::NextColumn();

		ImGui::PushID(layoutIndex);

		String layoutText;
		layoutText.sPrintf("Layout %s\n",spLayout->name().c_str());

		if(!ImGui::TreeNodeEx(layoutText.c_str(),
				ImGuiTreeNodeFlags_DefaultOpen))
		{
			ImGui::PopID();
			layoutIndex++;
			continue;
		}

		ImGui::NextColumn();
		ImGui::NextColumn();
		ImGui::NextColumn();

		const I32 recordCount=spRA->length();
		for(int recordIndex=0;recordIndex<recordCount;recordIndex++)
		{
			Record record=spRA->getRecord(recordIndex);

			String recordText;
			recordText.sPrintf("Record %d\n",recordIndex);

			updateRecord(record,recordText,recordIndex);
		}

		ImGui::PopID();

		ImGui::TreePop();

		layoutIndex++;
	}

	ImGui::TreePop();
}

void ImguiHandlerRecord::updateRecord(Record a_record,String a_label,
		I32 a_recordIndex)
{
	sp<Layout> spLayout(a_record.layout());
	sp<Scope> spScope=spLayout->scope();

	ImGui::PushID(a_recordIndex);

	if(!ImGui::TreeNodeEx(a_label.c_str(),
			ImGuiTreeNodeFlags_DefaultOpen))
	{
		ImGui::PopID();
		return;
	}

	ImGui::NextColumn();
	ImGui::NextColumn();
	ImGui::NextColumn();

	U32 attrCount=spScope->getAttributeCount();
	for(U32 attrIndex=0;attrIndex<attrCount;attrIndex++)
	{
		if(!spLayout->checkAttribute(attrIndex))
		{
			continue;
		}

		if(!spScope->attribute(attrIndex)->isSerialize())
		{
			continue;
		}

		sp<BaseType> spBT=spScope->attribute(attrIndex)->type();
		if(spBT==m_spVoidType)
		{
			continue;
		}

		ImGui::PushID(attrIndex);

		const String attrName=spScope->attribute(attrIndex)->name();

		if(spBT==m_spRecordGroupType)
		{
			void* instance=a_record.rawAttribute(attrIndex);
			sp<RecordGroup>* pspRG=
					reinterpret_cast<sp<RecordGroup>*>(instance);

			if(pspRG->isValid())
			{
				updateRecordGroup(*pspRG,attrName);
			}
		}
		else if(spBT == m_spRecordArrayType)
		{
			void* instance=a_record.rawAttribute(attrIndex);
			sp<RecordArray>* pspRA=
					reinterpret_cast<sp<RecordArray>*>(instance);

			if(pspRA->isValid())
			{
				const I32 recordCount=(*pspRA)->length();
				for(int recordIndex=0;recordIndex<recordCount;recordIndex++)
				{
					Record record=(*pspRA)->getRecord(recordIndex);

					String recordText;
					recordText.sPrintf("%s %d\n",
							attrName.c_str(),a_recordIndex);

					updateRecord(record,recordText,recordIndex);
				}
			}
		}
		else if(spBT == m_spRecordType)
		{
			void* instance=a_record.rawAttribute(attrIndex);
			Record* pRecord = reinterpret_cast<Record*>(instance);

			if(pRecord->isValid())
			{
				updateRecord(*pRecord,attrName,0);
			}
		}
		else if(spBT == m_spWeakRecordType)
		{
			void* instance=a_record.rawAttribute(attrIndex);
			WeakRecord *pWeakRecord=
					reinterpret_cast<WeakRecord *>(instance);

			if(pWeakRecord->isValid())
			{
				Record record(*pWeakRecord);
				updateRecord(record,attrName,0);
			}
		}
		else if(spBT->getInfo().isValid())
		{
			const BWORD wasSelected=(a_record==m_selectedRecord &&
					I32(attrIndex)==m_selectedAttrIndex);
			bool selected=wasSelected;

			if(ImGui::Selectable(attrName.c_str(),&selected))
			{
//				feLog("SELECT \"%s\" %d \"%s\"\n",
//						spLayout->name().c_str(),
//						a_recordIndex,
//						attrName.c_str());
				m_selectedRecord=a_record;
				m_selectedAttrIndex=attrIndex;
			}
			if(wasSelected && !selected)
			{
//				feLog("DESELECT \"%s\" %d \"%s\"\n",
//						spLayout->name().c_str(),
//						a_recordIndex,
//						attrName.c_str());
				m_selectedRecord=Record();
				m_selectedAttrIndex= -1;
			}

//			ImGui::Text(attrName.c_str());

			ImGui::NextColumn();

			const BWORD doClip=(m_selectedRecord.isValid());
			if(doClip)
			{
				const ImVec2 windowSize=ImGui::GetWindowSize();

				const ImVec2 clip_rect_min(0,0);
				const ImVec2 clip_rect_max(m_scopeOffset,windowSize[1]);
				const bool intersect_with_current_clip_rect(true);

				ImGui::PushClipRect(clip_rect_min,clip_rect_max,
						intersect_with_current_clip_rect);
			}

			void* instance=a_record.rawAttribute(attrIndex);

			std::ostringstream ostrm;
			if(spBT->getInfo()->output(ostrm, instance,
					BaseType::Info::e_ascii)!=BaseType::Info::c_ascii)
			{
				ImGui::Text("<NO ASCII>");
			}
			else
			{
				String value(ostrm.str().c_str());
				value=value.prechop("\"").chop("\"");

				ImGui::SetNextItemWidth(-1.0f);	//* no space for label
				if(ImGui::InputText("##key",&value))
				{
					value=value.prechop("\"").chop("\"");

//					feLog("NEW VALUE \"%s\"\n",value.c_str());

					std::istringstream istrm(value.c_str());
					spBT->getInfo()->input(istrm, instance,
							BaseType::Info::e_ascii);
				}
			}

			if(doClip)
			{
				ImGui::PopClipRect();
			}

			ImGui::NextColumn();

			const ImVec2 cursorPos=ImGui::GetCursorPos();
			m_scopeOffset=cursorPos[0]+4;

			ImGui::NextColumn();
		}

		ImGui::PopID();
	}

	ImGui::PopID();

	ImGui::TreePop();
}

} /* namespace ext */
} /* namespace fe */
