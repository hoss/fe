/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __imgui_ImguiHandlerCatalog_h__
#define __imgui_ImguiHandlerCatalog_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Imgui Handler for generic Catalog

	@ingroup imgui
*//***************************************************************************/
class FE_DL_EXPORT ImguiHandlerCatalog:
	public ImguiHandler,
	public Initialize<ImguiHandlerCatalog>
{
	public:
				ImguiHandlerCatalog(void);
virtual			~ImguiHandlerCatalog(void);

		void	initialize(void);

virtual void	updateGUI(void)
				{
					update(m_spCatalog,"Catalog");

//					ImGui::ShowDemoWindow();
				}

virtual void	drawOverlay(void);

		void	bind(sp<Catalog> a_spCatalog) { m_spCatalog=a_spCatalog; }

	protected:

		void	update(sp<Catalog> a_spCatalog,String a_label);

	private:

		void	updateCatalog(sp<Catalog> a_spCatalog,String a_label);

		sp<Catalog>				m_spCatalog;
		sp<Oscilloscope>		m_spOscilloscope;

		I32						m_valueOffset;
		I32						m_typeOffset;
		I32						m_scopeOffset;
		String					m_selectedKey;
		String					m_selectedProperty;
		String					m_lastSelectedKey;
		String					m_lastSelectedProperty;

		BWORD					m_drawn;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __imgui_ImguiHandlerCatalog_h__ */
