/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <imgui/imgui.pmh>

namespace fe
{
namespace ext
{

ImguiHandler::ImguiHandler(void):
	m_pixelScale(2.0,2.0),
	m_mouseX(0),
	m_mouseY(0),
	m_mouseButtons(WindowEvent::e_mbNone)
{
	IMGUI_CHECKVERSION();
	ImGui::CreateContext();
	ImGuiIO& io = ImGui::GetIO();

	ImGui_ImplOpenGL2_Init();

	// Build font atlas
	unsigned char* tex_pixels = NULL;
	int tex_w, tex_h;
	io.Fonts->GetTexDataAsRGBA32(&tex_pixels, &tex_w, &tex_h);

	io.DisplaySize=ImVec2(320,200);
	io.DisplayFramebufferScale=
			ImVec2(m_pixelScale[0],m_pixelScale[1]);

	ImGuiStyle& rImGuiStyle=ImGui::GetStyle();
	rImGuiStyle.WindowMinSize=ImVec2(400,50);
	rImGuiStyle.WindowBorderSize=0.0;
//	rImGuiStyle.FrameBorderSize=1.0;

	io.KeyMap[ImGuiKey_Tab] = WindowEvent::e_keyTab;
	io.KeyMap[ImGuiKey_LeftArrow] = WindowEvent::e_keyCursorLeft;
	io.KeyMap[ImGuiKey_RightArrow] = WindowEvent::e_keyCursorRight;
	io.KeyMap[ImGuiKey_UpArrow] = WindowEvent::e_keyCursorUp;
	io.KeyMap[ImGuiKey_DownArrow] = WindowEvent::e_keyCursorDown;
	io.KeyMap[ImGuiKey_PageUp] = 0;
	io.KeyMap[ImGuiKey_PageDown] = 0;
	io.KeyMap[ImGuiKey_Home] = 0;
	io.KeyMap[ImGuiKey_End] = 0;
	io.KeyMap[ImGuiKey_Insert] = 0;
	io.KeyMap[ImGuiKey_Delete] = WindowEvent::e_keyDelete;
	io.KeyMap[ImGuiKey_Backspace] = WindowEvent::e_keyBackspace;
	io.KeyMap[ImGuiKey_Space] = WindowEvent::e_keySpace;
	io.KeyMap[ImGuiKey_Enter] = WindowEvent::e_keyLineFeed;
	io.KeyMap[ImGuiKey_Escape] = WindowEvent::e_keyEscape;
	io.KeyMap[ImGuiKey_KeyPadEnter] = 0;
	io.KeyMap[ImGuiKey_A] = 'a';
	io.KeyMap[ImGuiKey_C] = 'c';
	io.KeyMap[ImGuiKey_V] = 'v';
	io.KeyMap[ImGuiKey_X] = 'x';
	io.KeyMap[ImGuiKey_Y] = 'y';
	io.KeyMap[ImGuiKey_Z] = 'z';
}

ImguiHandler::~ImguiHandler(void)
{
	ImGui_ImplOpenGL2_Shutdown();
	ImGui::DestroyContext();
}

void ImguiHandler::handle(Record& render)
{
	if(!m_asViewer.scope().isValid())
	{
		m_asViewer.bind(render.layout()->scope());
	}

	if(m_asViewer.render_draw.check(render))
	{
		m_spDrawI=m_asViewer.render_draw(render);
	}

	if(m_asViewer.viewer_layer.check(render))
	{
		const I32& rLayer=m_asViewer.viewer_layer(render);

		if(rLayer!=2)
		{
			return;
		}

		if(m_spDrawI.isValid())
		{
			const Real multiplication=m_spDrawI->multiplication();
			m_pixelScale[0]=multiplication;
			m_pixelScale[1]=multiplication;
		}

		ImGui_ImplOpenGL2_NewFrame();

		ImGuiIO& io = ImGui::GetIO();
		io.DeltaTime = 1.0f / 60.0f;

		io.DisplayFramebufferScale=ImVec2(m_pixelScale[0],m_pixelScale[1]);

		io.MousePos=ImVec2(m_mouseX/m_pixelScale[0],m_mouseY/m_pixelScale[1]);
		io.MouseDown[0]=(m_mouseButtons&WindowEvent::e_mbLeft);
		io.MouseDown[1]=(m_mouseButtons&WindowEvent::e_mbMiddle);
		io.MouseDown[2]=(m_mouseButtons&WindowEvent::e_mbRight);

//		ImGui::PushStyleColor(ImGuiCol_Border,ImVec4(1,1,1,1));

//		feLog("io set %d %d %d\n",
//				m_mouseX,m_mouseY,m_mouseButtons);

		ImGui::NewFrame();

//		ImGui::SetNextWindowPos(ImVec2(0,0));
//		ImGui::SetNextWindowBgAlpha(0.3);

		updateGUI();

		ImGui::Render();

		ImGui_ImplOpenGL2_RenderDrawData(ImGui::GetDrawData());

//		ImGui::PopStyleColor();

		drawOverlay();

		return;
	}

	m_event.bind(render);

	if(m_event.isIdleMouse() || m_event.isPoll())
	{
		return;
	}

	feLog("ImguiHandler::handle %s\n",fe::print(m_event).c_str());

	ImGuiIO& io=ImGui::GetIO();

	if(m_event.isResize())
	{
		io.DisplaySize=ImVec2(m_event.state()/m_pixelScale[0],
				m_event.state2()/m_pixelScale[1]);
		return;
	}

	if(m_event.isMouseWheel())
	{
		io.MouseWheel+=(m_event.state()>0)? 1: -1;
		return;
	}

	if(m_event.matchesSource(WindowEvent::e_sourceMousePosition) ||
			m_event.matchesSource(WindowEvent::e_sourceMouseButton))
	{
		m_mouseX=m_event.mouseX();
		m_mouseY=m_event.mouseY();
		m_mouseButtons=m_event.mouseButtons();

		return;
	}

	if(m_event.source()==WindowEvent::e_sourceKeyboard)
	{
		I32 item=m_event.item();

		BWORD shifted=FALSE;
		if(item&WindowEvent::e_keyShift)
		{
			shifted=TRUE;
			item^=WindowEvent::e_keyShift;
		}
		BWORD ctrled=FALSE;
		if(item&WindowEvent::e_keyControl)
		{
			ctrled=TRUE;
			item^=WindowEvent::e_keyControl;
		}
		BWORD alted=FALSE;
		if(item&WindowEvent::e_keyAlt)
		{
			alted=TRUE;
			item^=WindowEvent::e_keyAlt;
		}

		if(item>=WindowEvent::e_keySpace && item<WindowEvent::e_keyDelete &&
				!ctrled && !alted)
		{
			if(m_event.state()==WindowEvent::e_statePress)
			{
				io.AddInputCharacter(item);
			}
			return;
		}

		io.KeyShift = shifted;
		io.KeyCtrl = ctrled;
		io.KeyAlt = alted;
		io.KeySuper = 0;

		if(item<0 || item>=IM_ARRAYSIZE(io.KeysDown))
		{
			feLog("ImguiHandler::handle bad key %d\n",item);
			return;
		}
		io.KeysDown[item] = (m_event.state()==WindowEvent::e_statePress);

		return;
	}

}

} /* namespace ext */
} /* namespace fe */

struct InputTextCallback_UserData
{
	fe::String*				Str;
	ImGuiInputTextCallback	ChainCallback;
	void*					ChainCallbackUserData;
};

static int InputTextCallback(ImGuiInputTextCallbackData* data)
{
	InputTextCallback_UserData* user_data =
			(InputTextCallback_UserData*)data->UserData;
	if (data->EventFlag == ImGuiInputTextFlags_CallbackResize)
	{
		// Resize string callback
		// If for some reason we refuse the new length (BufTextLen) and/or
		// capacity (BufSize) we need to set them back to what we want.
		fe::String* str = user_data->Str;
		IM_ASSERT(data->Buf == str->c_str());
		str->resize(data->BufTextLen);
		data->Buf = (char*)str->c_str();
	}
	else if (user_data->ChainCallback)
	{
		// Forward to user callback, if any
		data->UserData = user_data->ChainCallbackUserData;
		return user_data->ChainCallback(data);
	}
	return 0;
}

bool ImGui::InputText(const char* label, fe::String* str,
	ImGuiInputTextFlags flags, ImGuiInputTextCallback callback,
	void* user_data)
{
	IM_ASSERT((flags & ImGuiInputTextFlags_CallbackResize) == 0);
	flags |= ImGuiInputTextFlags_CallbackResize;

	InputTextCallback_UserData cb_user_data;
	cb_user_data.Str = str;
	cb_user_data.ChainCallback = callback;
	cb_user_data.ChainCallbackUserData = user_data;

	return InputText(label, (char*)str->c_str(), str->length() + 1,
			flags, InputTextCallback, &cb_user_data);
}

bool ImGui::InputTextMultiline(const char* label, fe::String* str,
	const ImVec2& size, ImGuiInputTextFlags flags,
	ImGuiInputTextCallback callback, void* user_data)
{
	IM_ASSERT((flags & ImGuiInputTextFlags_CallbackResize) == 0);
	flags |= ImGuiInputTextFlags_CallbackResize;

	InputTextCallback_UserData cb_user_data;
	cb_user_data.Str = str;
	cb_user_data.ChainCallback = callback;
	cb_user_data.ChainCallbackUserData = user_data;

	return InputTextMultiline(label, (char*)str->c_str(), str->length() + 1,
			size, flags, InputTextCallback, &cb_user_data);
}

bool ImGui::InputTextWithHint(const char* label, const char* hint,
	fe::String* str, ImGuiInputTextFlags flags,
	ImGuiInputTextCallback callback, void* user_data)
{
	IM_ASSERT((flags & ImGuiInputTextFlags_CallbackResize) == 0);
	flags |= ImGuiInputTextFlags_CallbackResize;

	InputTextCallback_UserData cb_user_data;
	cb_user_data.Str = str;
	cb_user_data.ChainCallback = callback;
	cb_user_data.ChainCallbackUserData = user_data;

	return InputTextWithHint(label, hint, (char*)str->c_str(),
			str->length() + 1, flags, InputTextCallback, &cb_user_data);
}
