/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __evaluator_EvaluatorI_h__
#define __evaluator_EvaluatorI_h__

namespace fe
{
namespace ext
{

class Variables : public Counted
{
	public:
		t_stdvector< Record >					m_varray;

		unsigned int	size(void) { return m_varray.size(); }
		Record			&operator [] (unsigned int a_i)
							{ return m_varray[a_i]; }
		void			resize(unsigned int a_size)
							{ m_varray.resize(a_size); }

		void			copy(Variables &a_other)
		{
			m_varray.resize(a_other.size());
			for(unsigned int i = 0; i < size(); i++)
			{
				(*this)[i] = a_other[i];
			}
		}
};

typedef Variables t_variables;

/// Functional Mini Language evaluator
///
/// see @ref evaluate_evaluate
class FE_DL_EXPORT EvaluatorI : virtual public Component, public CastableAs<EvaluatorI>
{
	public:
virtual	void			bind(sp<RecordGroup> a_rg)						= 0;

		/// manage
virtual void			compile(const t_stdstring &a_buffer)			= 0;
virtual	void			evaluate(void)									= 0;

virtual	void			addFunction(const t_stdstring &a_function,
							const t_stdstring &a_component)				= 0;
virtual	sp<FunctionI>	createFunction(const t_stdstring &a_function)	= 0;

		/// access variables (input and output)
virtual	unsigned int			index(const t_stdstring &a_name)		= 0;
virtual	const Record			&variable(unsigned int a_index)			= 0;
virtual	const sp< Aggregate >	&aggregate(unsigned int a_index)		= 0;
virtual	void					assignVariable(unsigned int a_index,
									Record a_record,
									sp< Aggregate > a_aggregate)		= 0;
virtual	t_variables				&variables(void)						= 0;

virtual	Record					create(const t_stdstring &a_name,
									sp< AccessorSet > a_spSA)			= 0;
};

} /* namespace ext */
} /* namespace fe */

#endif

