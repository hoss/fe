/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __evaluator_Evaluator_h__
#define __evaluator_Evaluator_h__

namespace fe
{
namespace ext
{

class Debug : public Counted
{
	public:
		Debug(void)
		{
			m_depth = 0;
			m_pushed = false;
		}
virtual	~Debug(void) {}
		void push(void) { m_pushed = true; m_depth++; }
		void pop(void) { m_depth--; }
		void log(const char *a_group, const char *fmt, ...)
		{
			std::string prefix;
			std::string group = a_group;
			if(group == "info")
			{
				prefix = "|";
				for(unsigned int i = 1; i < m_depth; i++)
				{
					prefix += "   |";
				}
				if(m_depth>0)
				{
					if(m_pushed)
					{
						prefix += "-->|";
					}
					else
					{
						prefix += "   |";
					}
				}
				prefix += " ";
				m_pushed = false;
			}

			t_stdstring str;
			va_list ap;
			int sz=0;
			while(sz>=0)
			{
				va_start(ap, fmt);
				vsPrintf(str,fmt,ap,sz);
				va_end(ap);
			}

			feLogGroup(a_group, "%s%s\n", prefix.c_str(), str.c_str());
		}
		unsigned int	m_depth;
		bool			m_pushed;
};


class FE_DL_EXPORT AsVariable :
	public AccessorSet,
	public Initialize<AsVariable>
{
	public:
		void initialize(void)
		{
			add(value,		FE_SPEC("eval:value", "evaluate real"));
		}
		Accessor<t_eval_real>			value;
};

class FE_DL_EXPORT AsString :
	public AccessorSet,
	public Initialize<AsString>
{
	public:
		void initialize(void)
		{
			add(value,		FE_SPEC("eval:string", "evaluate string"));
		}
		Accessor<String>		value;
};


class FE_DL_EXPORT RealFunction :
	virtual public FunctionI,
	public Initialize<RealFunction>
{
	public:
		RealFunction(void)
		{
		}
		~RealFunction(void) {}

		void initialize(void);

virtual	bool compile(sp<RecordGroup> a_rg, Record &a_return, t_stdvector<WeakRecord> &a_argv, t_stdstring &a_msg);
virtual	sp<Aggregate>		&returnType(void) { return m_aggregate; }

		sp<AsVariable>	m_asVariable;
		sp<Aggregate>	m_aggregate;
		bool				m_valid;
};


typedef Registry::FactoryLocation *t_factory_location;

class Evaluator;

class Token
{
	public:
		Token(void) {}
		Token(const t_stdstring &a_str, unsigned int a_line)
		{
			m_str = a_str;
			m_line = a_line;
		}
		operator t_stdstring &()
		{
			return m_str;
		}
		const char *c_str(void)
		{
			return m_str.c_str();
		}
		t_stdstring m_str;
		unsigned int m_line;
};

typedef Token t_token;

class Node : public Counted
{
	public:
		Node(void);
virtual	~Node(void);

		void compile(t_stdlist<t_token> &a_tokens,
			Evaluator *a_evaluator,
			Node *a_parent,
			sp<Debug> a_debug);

		void		preEvaluate(Evaluator *a_evaluator, sp<Debug> a_debug);
		void		evaluate(sp<t_variables> &a_variables);
		Record		&value(void) { return m_value; }

virtual	sp<Aggregate>		&returnType(void) { return m_aggregate; }

		t_stdvector< sp<Node> >		m_children;

		unsigned int					m_leaf;

		t_token							m_token;

		sp<FunctionI>				m_function;
		t_stdvector< WeakRecord >	m_argv;
		Record						m_value;
		sp< Aggregate >				m_aggregate;

		bool							m_is_define;
		bool							m_is_jump;

		sp<Node>					m_jump_node;

		typedef enum
		{
			e_eval_none = -1,
			e_eval_function = 0,
			e_eval_assign_matched,
			e_eval_assign_mismatch,
			e_eval_single_child,
			e_eval_childless,
			e_eval_jump,
			e_eval_impossible
		} t_eval;

		t_eval							m_t_eval;

	private:
		void leafMake(Evaluator *a_evaluator, sp<Node> a_node);

};

class JumpLocation
{
	public:
		sp<Node>				m_node;
};

typedef JumpLocation t_jump_location;

class FE_DL_EXPORT Evaluator :
	virtual public EvaluatorI,
	public Initialize<Evaluator>
{
	public:
		Evaluator(void);
virtual	~Evaluator(void);

		void				initialize(void);

virtual	void				bind(sp<RecordGroup> a_rg);

		// manage
virtual void			compile(const t_stdstring &a_buffer);
virtual	void			evaluate(void);

virtual	void			addFunction(const t_stdstring &a_function,
							const t_stdstring &a_component);
virtual	sp<FunctionI>	createFunction(const t_stdstring &a_function);

		// access variables (input and output)
virtual	unsigned int			index(const t_stdstring &a_name);
virtual	const Record			&variable(unsigned int a_index)
		{ return m_variables->m_varray[a_index]; }
virtual	const sp< Aggregate >	&aggregate(unsigned int a_index)
		{ return m_aggregates[a_index]; }
virtual	void					assignVariable(unsigned int a_index,
									Record a_record,
									sp< Aggregate > a_aggregate)
								{
									m_variables->m_varray[a_index] = a_record;
									m_aggregates[a_index] = a_aggregate;
								}
virtual	void					makeVariable(Node *a_node);
virtual	t_variables				&variables(void) { return *m_variables; }

virtual	Record					create(const t_stdstring &a_name,
									sp< AccessorSet > a_spSA);

		void					dump(void);

		Record					createRecord(sp<Layout> a_layout);

	private:
		void			tokenize(t_stdlist<t_token> &a_tokens, const t_stdstring &a_buffer);

		t_stdmap< sp< AccessorSet >, sp< Layout > >	m_layouts;
		sp<t_variables>											m_variables;
		t_stdvector< sp< Aggregate > >							m_aggregates;
		t_stdvector< sp<Node> >									m_nodes;
		t_stdmap<t_stdstring, t_factory_location>				m_factory_locations;
		t_stdmap<t_stdstring, unsigned int>						m_var_names;
		unsigned int											m_output_index;
		sp< Scope >												m_scope;
		sp< RecordGroup >										m_rg;
		sp< AsVariable >										m_asVariable;
		sp< AsString >											m_asString;
		sp< Aggregate >											m_stringAggregate;
		sp< Aggregate >											m_literalAggregate;
	public:
		t_stdmap<t_stdstring, t_jump_location >					m_jump_locations;
		sp< AsVariable >				&asVariable(void) { return m_asVariable; }
		sp< AsString >					&asString(void) { return m_asString; }
		sp< Scope >						&scope(void) { return m_scope; }
		sp< RecordGroup >				&recordGroup(void) { return m_rg; }
		sp< Aggregate >					&literalAggregate(void) { return m_literalAggregate; }
		sp< Aggregate >					&stringAggregate(void) { return m_stringAggregate; }
};

class FE_DL_EXPORT EvaluatorLog : public AnnotatedLog
{
	public:
					EvaluatorLog(void)										{}
virtual				~EvaluatorLog(void)										{}

					using AnnotatedLog::log;

virtual		void	log(const std::string &a_message,
						std::map<std::string,std::string> &a_attributes);
};


} /* namespace ext */
} /* namespace fe */

#endif /* __evaluator_Evaluator_h__ */

