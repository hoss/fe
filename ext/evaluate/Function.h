/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "evaluate.pmh"
//#include "solve/solve.h"

#ifndef __evaluate_Function_h__
#define __evaluate_Function_h__

namespace fe
{
namespace ext
{

class FE_DL_EXPORT Multiply : virtual public RealFunction
{
	public:
		Multiply(void) {}
		~Multiply(void) {}
virtual	void eval(Record &a_return, t_stdvector<WeakRecord> &a_argv);
};

class FE_DL_EXPORT Add : virtual public RealFunction
{
	public:
		Add(void) {}
		~Add(void) {}
virtual	void eval(Record &a_return, t_stdvector<WeakRecord> &a_argv);
};

class FE_DL_EXPORT Subtract : virtual public RealFunction
{
	public:
		Subtract(void) {}
		~Subtract(void) {}
virtual	void eval(Record &a_return, t_stdvector<WeakRecord> &a_argv);
};

class FE_DL_EXPORT Divide : virtual public RealFunction
{
	public:
		Divide(void) {}
		~Divide(void) {}
virtual	void eval(Record &a_return, t_stdvector<WeakRecord> &a_argv);
};

class FE_DL_EXPORT AbsoluteValue : virtual public RealFunction
{
	public:
		AbsoluteValue(void) {}
		~AbsoluteValue(void) {}
virtual	void eval(Record &a_return, t_stdvector<WeakRecord> &a_argv);
};

class FE_DL_EXPORT AsSmooth :
	public AccessorSet,
	public Initialize<AsSmooth>
{
	public:
		void initialize(void)
		{
			add(value,		FE_SPEC("eval:value", "node return value"));
			add(dummy,		FE_SPEC("eval:dummy", "dummy"));
		}
		Accessor<t_eval_real>			value;
		Accessor<int>					dummy;
};

class FE_DL_EXPORT Smooth :
	virtual public FunctionI,
	public Initialize<Smooth>

{
	public:
		Smooth(void)
		{
		}
		~Smooth(void) {}

		void initialize(void);

virtual	bool compile(sp<RecordGroup> a_rg, Record &a_return, t_stdvector<WeakRecord> &a_argv, t_stdstring &a_msg);
virtual	void eval(Record &a_return, t_stdvector<WeakRecord> &a_argv);
virtual	sp<Aggregate>		&returnType(void) { return m_aggregate; }

		t_eval_real			m_smoothed;
		sp<AsSmooth>		m_asSmooth;
		sp<Aggregate>		m_aggregate;
		bool				m_valid;
};

class FE_DL_EXPORT Compressor : virtual public RealFunction
{
	public:
		Compressor(void)
		{
			m_gain = 1.0;
		}
		~Compressor(void) {}
virtual	void eval(Record &a_return, t_stdvector<WeakRecord> &a_argv);
		t_eval_real m_gain;
};

class FE_DL_EXPORT SoftClipper : virtual public RealFunction
{
	public:
		SoftClipper(void)
		{
		}
		~SoftClipper(void) {}
virtual	void eval(Record &a_return, t_stdvector<WeakRecord> &a_argv);
};


#if 0
	this->factories()["print"] = &(PrintVal::create);
	this->factories()["histogram"] = &(Histogram::create);
	this->factories()["signal_scale"] = &(SignalScale::create);
	this->factories()["hard_clip"] = &(HardClipper::create);
	this->factories()["soft_clip"] = &(SoftClipper::create);
	this->factories()["antijolt"] = &(AntiJolt::create);
	this->factories()["antijolt2"] = &(AntiJolt2::create);
	this->factories()["antivibe"] = &(AntiVibe::create);
	this->factories()["spring"] = &(Spring::create);
	this->factories()["relative"] = &(RelativeGain::create);
	this->factories()["compressor"] = &(Compressor::create);
	this->factories()["scoop"] = &(Scoop::create);
	this->factories()["tighten"] = &(TightenCenter::create);
	this->factories()["drag"] = &(BaseDrag::create);
	this->factories()["bumpstop"] = &(BumpStop::create);
	this->factories()["power"] = &(Power::create);
	this->factories()["linkage"] = &(Linkage::create);
	this->factories()["limiter"] = &(Limiter::create);
	this->factories()["jerk"] = &(Jerk::create);
	this->factories()["oscillator"] = &(Oscillator::create);
	this->factories()["split"] = &(Split::create);
	this->factories()["safety"] = &(Safety::create);
	this->factories()["anti_drag"] = &(AntiDrag::create);
	this->factories()["enhance"] = &(Enhance::create);
	this->factories()["abs"] = &(AbsFun::create);
	this->factories()["min"] = &(MinFun::create);
	this->factories()["max"] = &(MaxFun::create);
	this->factories()["if"] = &(IfFun::create);
	this->factories()["LT"] = &(LessThan::create);
	this->factories()["blend"] = &(BlendFun::create);
	this->factories()["crossover"] = &(CrossoverFun::create);
	this->factories()["up"] = &(Up::create);
	this->factories()["dn"] = &(Dn::create);
	this->factories()["ctr"] = &(Center::create);
	this->factories()["save_channel"] = &(SaveChannel::create);
	this->factories()["load_channel"] = &(LoadChannel::create);
#endif


} /* namespace ext */
} /* namespace fe */

#endif /* __evaluate_Function_h__ */
