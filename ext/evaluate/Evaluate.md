# Evaluate Guide {#EvaluateGuide}

Evaluate: Simple Functional Scripting Language

## Purpose

Provide a simple casual end user scripting solution.

  * Fast.  Most heavy lifting done in native/C++.
  * Extensible.  Easy to add native/C++ functions.
  * Simple.  Simple grammar, simple implementation.


If a full feature scripting language is needed for a particular application,
then it might be better to embed something like lua or python.  This solution
is meant to be extremely light, straighforward, and simple.

However, one significant upside for FE is that this directly operates on
src/data, and is plugged into via FE components.

## Language

The language itself is very light and somewhat lisp-like.

### Expressions

Expression are of the form:

    (a b c ...)

The first token (in the above, "a") is first checked to see if it is a
known function.  If it is, the expression is a function call.   If it
is not, then the expression is an assignment to a variable of that name.

Other fields maybe be expressions, numbers, or variables.

### Variables

There is a single global notion of variables (this is for simplicity and
performance).

Assignment is done by using an expression where the first token is the variable
name.   Access is simply by using the variable name as a field.

### Comments

Comments start with "#" and go to the end of the line.

### Define

Full lisp-like functions and lambda are not yet supported.  However, "define"
can be used to specify re-callable code.   This approximates functions, but
without arguments support (yet).

	# define a function (sort of)
	(define my-sorta-function (* 1.1 (+ arg0 arg1)))

	# call the function
	(arg0 2.2)
	(arg1 3.3)
	(result (my-sorta-function))

### Example

	# assignment of 1.1 to my-variable
	(my-variable 1.1)

	# add my-variable and 0.5, then assign to my-answer
	(my-answer (+ my-variable 0.5))

### Embedding into C++

	// create and bind an Evaluator
	sp<Evaluator> spEval = spRegistry->create("EvaluatorI");
	spEval->bind(spScope);

	// add some native functions
	spEval->addFunction("*", "FunctionI.Multiply");
	spEval->addFunction("/", "FunctionI.Divide");
	spEval->addFunction("+", "FunctionI.Add");
	spEval->addFunction("-", "FunctionI.Subtract");

	// create a variable for a result
	Record r_output = spEval->create("my-answer", spEval->asVariable());

	// "compile" a program
	spEval->compile(program_string);

	// run the program
	spEval->evaluate();

	// access the answer
	double my_answer = spEval->asVariable()->value(r_output);

Functions are added by implementing fe::ext::FunctionI.


