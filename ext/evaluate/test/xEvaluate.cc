/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "math/math.h"
#include "evaluate/evaluate.h"

#include "evaluate/Function.h"

using namespace fe;
using namespace fe::ext;

int main(int argc,char** argv,char **env)
{
	try
	{
		feLogger()->clear(".*");
		feLogger()->clear("error");
		feLogger()->clear("info");
		feLogger()->clear("warning");
		feLogger()->setLog("group", new AnnotatedLog());
		feLogger()->setLog("stdout", new GroupLog());
		feLogger()->setLog("evdb", new EvaluatorLog());
		feLogger()->bind("error", "evdb");
		feLogger()->bind("info", "evdb");
		feLogger()->bind("warning", "evdb");

		sp<Master> spMaster(new Master);
		assertMath(spMaster->typeMaster());

		sp<Registry> spRegistry=spMaster->registry();

		spRegistry->manage("feDataDL");
		spRegistry->manage("fexEvaluateDL");

		sp<Scope> spScope = spRegistry->create("Scope");

		assertMath(spMaster->typeMaster());

		std::ifstream infile("test.txt");

		t_stdostringstream oss;
		oss << infile.rdbuf();

		sp<Evaluator> spMyEval = spRegistry->create("EvaluatorI");
		// have to jam a dummy record into RG to fake 1:1 scope for now
		sp<Layout> spLayout = spScope->declare("dummy");
		Record r_dummy = spScope->createRecord(spLayout);
		sp<RecordGroup> spRG(new RecordGroup());
		spRG->add(r_dummy);
		spMyEval->bind(spRG);
		spMyEval->addFunction("smooth", "FunctionI.Smooth");
		spMyEval->addFunction("compress", "FunctionI.Compressor");
		spMyEval->addFunction("*", "FunctionI.Multiply");
		spMyEval->addFunction("/", "FunctionI.Divide");
		spMyEval->addFunction("+", "FunctionI.Add");
		spMyEval->addFunction("-", "FunctionI.Subtract");

		const String filename("output/test/evaluate/test_out.txt");
		System::createParentDirectories(filename);

		FILE *fp = fopen(filename.c_str(), "w");

		t_eval_real et = 0.0;
		t_eval_real dt = 1.0/600.0;
		t_eval_real st = 10.0;
		t_eval_real source = 0.0;

		unsigned int t0, t1;
		unsigned int hv_accum = 0;

		t_eval_real v_hv;
		Record r_hv;

		sp<AsVariable> asVariable = spMyEval->asVariable();

		Record r_source = spMyEval->create("source", asVariable);
		Record r_output = spMyEval->create("output", asVariable);
		Record r_channel = spMyEval->create("channel", asVariable);
		Record r_step = spMyEval->create("step", asVariable);

		spMyEval->compile(oss.str());

		asVariable->value(r_step) = 0.0;

		while(et < st)
		{
			source = sin(et);
			source += 0.1*sin(et*20.0);

			if(et > 5.0)
			{
				asVariable->value(r_step) = 0.0;
			}
			else if(et > 3.0)
			{
				asVariable->value(r_step) = 1.0;
			}
			else if(et > 1.0)
			{
				asVariable->value(r_step) = 2.0;
			}
			else
			{
				asVariable->value(r_step) = 0.0;
			}

			asVariable->value(r_source) = source;
			t0 = systemTick();
			spMyEval->evaluate();
			t1 = systemTick();
			v_hv = asVariable->value(r_output);
			hv_accum += t1-t0;

			fprintf(fp, "%g %g %g %g\n",
					et, source+0.02, v_hv, asVariable->value(r_channel));
			et += dt;
		}

		spMyEval->dump();

		fprintf(stderr, "accum %u\n", hv_accum);


		fclose(fp);
	}
	catch(Exception &e)
	{
		e.log();
	}
	catch(std::exception &e)
	{
		fprintf(stderr,"%s\n", e.what());
	}
	catch(...)
	{
		feLog("uncaught exception\n");
	}

	return 0;
}


