/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __sdl_ImageSDL_h__
#define __sdl_ImageSDL_h__
namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Image handling using SDL

	@ingroup sdl

	http://www.libsdl.org
*//***************************************************************************/
class FE_DL_EXPORT ImageSDL: public ImageCommon,
	public Initialize<ImageSDL>
{
	public:
				ImageSDL(void);
virtual			~ImageSDL(void);

		void	initialize(void);

				//* As ImageI
virtual	I32		createSelect(void);
virtual I32		loadSelect(String filename);
virtual I32		interpretSelect(void* data,U32 size)		{ return 0; }
virtual I32		interpretSelect(String a_source)			{ return 0; }
virtual	BWORD	save(String filename);
virtual void	select(I32 id);
virtual I32		selected(void) const	{ return m_selected; }
virtual	void	unload(I32 id);

virtual	void	setFormat(ImageI::Format set);
virtual	ImageI::Format	format(void) const;

virtual	void	resize(U32 width,U32 height,U32 depth);
virtual	void	replaceRegion(U32 x,U32 y,U32 z,
						U32 width,U32 height,U32 depth,void* data);

virtual	U32		width(void) const;
virtual	U32		height(void) const;
virtual	U32		depth(void) const;
virtual	void*	raw(void) const;

virtual	U32		regionCount(void) const						{ return 0; }
virtual	String	regionName(U32 a_regionIndex) const			{ return ""; }
virtual	Box2i	regionBox(String a_regionName) const		{ return Box2i(); }
virtual	String	pickRegion(I32 a_x,I32 a_y) const			{ return ""; }


	private:
		void	createSurface(ImageI::Format format,U32 width,U32 height);

		sp<Component>				m_spContextSDL;

		SDL_Surface*				m_pCurrentSurface;
		I32							m_selected;

		U32							m_nextID;
		std::map<U32,SDL_Surface*>	m_surfaceMap;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __sdl_ImageSDL_h__ */
