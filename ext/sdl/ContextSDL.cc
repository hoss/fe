/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <sdl/sdl.pmh>

namespace fe
{
namespace ext
{

ContextSDL::ContextSDL(void)
{
	if( SDL_Init(SDL_INIT_NOPARACHUTE) <0 )
	{
		feX("ContextSDL::ContextSDL","SDL_Init failed: %s\n",SDL_GetError());
	}
}

ContextSDL::~ContextSDL(void)
{
//	feLog("~ContextSDL\n");

	SDL_Quit();
}

} /* namespace ext */
} /* namespace fe */
