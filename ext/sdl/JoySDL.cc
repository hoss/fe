/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <sdl/sdl.pmh>

#define FE_JOY_DEBUG			FALSE

//* NOTE for Linux permissions, you can add device ids in /lib/udev/rules.d/
//* https://developer.x-plane.com/2012/09/linux-joystick-permissions/

namespace fe
{
namespace ext
{

namespace
{

//* NOTE manually sync axis and button maps with SDL2/SDL_gamecontroller.h
//* TODO game controller could be synced automatically using string names

static const WindowEvent::Item feControllerAxisMap[]=
{
	WindowEvent::e_joyLeftStickX,
	WindowEvent::e_joyLeftStickY,
	WindowEvent::e_joyRightStickX,
	WindowEvent::e_joyRightStickY,
	WindowEvent::e_joyLeftTrigger,
	WindowEvent::e_joyRightTrigger,
	WindowEvent::e_itemNull
};

static const I32 feControllerAxisScale[]=
{
	1,
	-1,
	1,
	-1,
	1,
	1
};

static const WindowEvent::Item feControllerButtonMap[]=
{
	WindowEvent::e_joyRightDown,
	WindowEvent::e_joyRightRight,
	WindowEvent::e_joyRightLeft,
	WindowEvent::e_joyRightUp,

	WindowEvent::e_joyBack,
	WindowEvent::e_joyGuide,
	WindowEvent::e_joyStart,

	WindowEvent::e_joyLeftDepress,
	WindowEvent::e_joyRightDepress,

	WindowEvent::e_joyLeftHigh,
	WindowEvent::e_joyRightHigh,

	WindowEvent::e_joyLeftUp,
	WindowEvent::e_joyLeftDown,
	WindowEvent::e_joyLeftLeft,
	WindowEvent::e_joyLeftRight,

    /* Xbox Series X share button,
		PS5 microphone button,
		Nintendo Switch Pro capture button */
    /* Xbox Elite paddle P1 */
    /* Xbox Elite paddle P3 */
    /* Xbox Elite paddle P2 */
    /* Xbox Elite paddle P4 */
    /* PS4/PS5 touchpad button */

	WindowEvent::e_itemNull
};

static const WindowEvent::Item feJoystickAxisMap[]=
{
	WindowEvent::e_joyLeftStickX,
	WindowEvent::e_joyLeftStickY,
	WindowEvent::e_joyRightStickX,
	WindowEvent::e_joyRightStickY,
	WindowEvent::e_joyLeftPadX,
	WindowEvent::e_joyLeftPadY,
	WindowEvent::e_itemNull
//	2 more reported, but unused
};

//* NOTE Logitech
static const WindowEvent::Item feWheelAxisMap[]=
{
	WindowEvent::e_JoyWheel,
	WindowEvent::e_JoyClutch,
	WindowEvent::e_JoyGasPedal,
	WindowEvent::e_JoyBrake,
	WindowEvent::e_itemNull,
	WindowEvent::e_itemNull,
	WindowEvent::e_itemNull
};

//* NOTE ThrustMaster
static const WindowEvent::Item fePedalAxisMap[]=
{
	WindowEvent::e_JoyBrake,
	WindowEvent::e_JoyGasPedal,
	WindowEvent::e_itemNull,
	WindowEvent::e_itemNull,
	WindowEvent::e_itemNull,
	WindowEvent::e_itemNull,
	WindowEvent::e_itemNull
};

static const I32 feJoystickAxisScale[]=
{
	1,
	-1,
	1,
	-1,
	1,
	-1
};

static const I32 feWheelAxisScale[]=
{
	1,
	-1,
	-1,
	-1,
	1,
	1
};

static const I32 fePedalAxisScale[]=
{
	-1,
	-1,
	1,
	1,
	1,
	1
};

static const WindowEvent::Item feJoystickButtonMap[]=
{
	WindowEvent::e_joyRightUp,
	WindowEvent::e_joyRightRight,
	WindowEvent::e_joyRightDown,
	WindowEvent::e_joyRightLeft,

	WindowEvent::e_joyLeftLow,
	WindowEvent::e_joyRightLow,

	WindowEvent::e_joyLeftHigh,
	WindowEvent::e_joyRightHigh,

	WindowEvent::e_joyStart,
	WindowEvent::e_joySelect,

	WindowEvent::e_joyLeftDepress,
	WindowEvent::e_joyRightDepress,

	WindowEvent::e_itemNull
};

}

JoySDL::JoySDL(void)
{
	I32 count=0;
	while(feControllerAxisMap[count++] != WindowEvent::e_itemNull) {};
	m_maxAxes=count-1;

	count=0;
	while(feControllerButtonMap[count++] != WindowEvent::e_itemNull) {};
	m_maxButtons=count-1;

	count=0;
	while(feJoystickAxisMap[count++] != WindowEvent::e_itemNull) {};
	m_maxAxes=fe::maximum(m_maxAxes,count-1);

	count=0;
	while(feJoystickButtonMap[count++] != WindowEvent::e_itemNull) {};
	m_maxButtons=fe::maximum(m_maxButtons,count-1);
}

void JoySDL::initialize(void)
{
	m_spContextSDL=registry()->create("*.ContextSDL");
	if(!m_spContextSDL.isValid())
	{
		feX("JoySDL::initialize","Unable to create ContextSDL\n");
	}

#if FE_SDL<2
	if(SDL_InitSubSystem(SDL_INIT_JOYSTICK)<0)
#else
	SDL_SetMainReady();

	if(SDL_InitSubSystem(SDL_INIT_GAMECONTROLLER)<0)
#endif
	{
		feX("JoySDL::initialize","Unable to init SDL_INIT_JOYSTICK: %s\n",
				SDL_GetError());
	}

	const int numSticks=fe::minimum(4,SDL_NumJoysticks());
	feLog("JoySDL::initialize numSticks %d\n",numSticks);

	m_joysticks.resize(numSticks);
	m_gameControllers.resize(numSticks);
	m_controllerTypes.resize(numSticks);
	m_pAxisState.resize(numSticks);
	m_pAxisLastState.resize(numSticks);
	m_pButtonState.resize(numSticks);

	for(I32 joystickIndex=0;joystickIndex<numSticks;joystickIndex++)
	{
		m_pAxisLastState[joystickIndex].resize(m_maxAxes);
		m_pAxisState[joystickIndex].resize(m_maxAxes);
		m_pButtonState[joystickIndex].resize(m_maxButtons);

		for(I32 count=0;count<m_maxAxes;count++)
		{
			m_pAxisLastState[joystickIndex][count]= -99999;
			m_pAxisState[joystickIndex][count]= -99999;
		}
		for(I32 count=0;count<m_maxButtons;count++)
		{
			m_pButtonState[joystickIndex][count]= -1;
		}

		m_joysticks[joystickIndex]=NULL;
		if(joystickIndex<numSticks)
		{
			m_joysticks[joystickIndex]=SDL_JoystickOpen(joystickIndex);
		}

		String joystickName="NULL";
		if(m_joysticks[joystickIndex])
		{
#if FE_SDL>1
			joystickName=SDL_JoystickName(m_joysticks[joystickIndex]);
#else
			int stickIndex=SDL_JoystickIndex(m_joysticks[joystickIndex]);
			joystickName=SDL_JoystickName(stickIndex);
#endif
		}

		m_controllerTypes[joystickIndex]=e_stick;
		if(joystickName.search("Wheel"))
		{
			m_controllerTypes[joystickIndex]=e_wheel;
		}
		else if(joystickName.search("Pedals"))
		{
			m_controllerTypes[joystickIndex]=e_pedals;
		}

		feLog("JoySDL::initialize joystick %d is \"%s\"\n",
				joystickIndex,joystickName.c_str());

#if FE_SDL>1
		m_gameControllers[joystickIndex]=NULL;
		if(SDL_IsGameController(joystickIndex))
		{
			m_gameControllers[joystickIndex]=
					SDL_GameControllerOpen(joystickIndex);
		}

#if FE_JOY_DEBUG
		feLog("JoySDL::initialize game controller %d is %p\n",
				joystickIndex,m_gameControllers[joystickIndex]);
#endif
#endif
	}
}

JoySDL::~JoySDL(void)
{
	const I32 numSticks=m_joysticks.size();
	for(I32 joystickIndex=0;joystickIndex<numSticks;joystickIndex++)
	{
#if FE_SDL>1
		if(m_gameControllers[joystickIndex])
		{
			SDL_GameControllerClose(m_gameControllers[joystickIndex]);
		}
#endif

		if(m_joysticks[joystickIndex])
		{
			SDL_JoystickClose(m_joysticks[joystickIndex]);
		}
	}

#if FE_SDL<2
	SDL_QuitSubSystem(SDL_INIT_JOYSTICK);
#else
	SDL_QuitSubSystem(SDL_INIT_GAMECONTROLLER);
#endif
}

void JoySDL::handleBind(sp<SignalerI> spSignalerI, sp<Layout> spLayout)
{
	m_hpSignalerI=spSignalerI;

	sp<Scope> spScope=spLayout->scope();
	m_joyEvent.bind(spScope);
	m_joyRecord=m_joyEvent.createRecord();
}

void JoySDL::handle(Record &record)
{
	m_event.bind(record);

#if FE_JOY_DEBUG
	if(m_event.isJoy())
	{
		feLog("JoySDL::handle %s\n",c_print(m_event));
	}
#endif

	if(m_event.isPoll())
	{
#if FE_JOY_DEBUG
		feLog("JoySDL::handle %s\n",c_print(m_event));
#endif

		//* WARNING possibly reentrant
		poll();
	}
}

JoyI::ControllerType JoySDL::getControllerType(I32 a_joystickIndex)
{
	const I32 numSticks=m_controllerTypes.size();
	if(a_joystickIndex<0 || a_joystickIndex>=numSticks)
	{
		return JoyI::e_none;
	}
	return m_controllerTypes[a_joystickIndex];
}

void JoySDL::poll(void)
{
	sp<SignalerI> spSignalerI(m_hpSignalerI);
	if(spSignalerI.isNull())
	{
		return;
	}

	const I32 numSticks=m_joysticks.size();
	for(I32 joystickIndex=0;joystickIndex<numSticks;joystickIndex++)
	{
		if(m_controllerTypes[joystickIndex]==JoyI::e_wheel ||
				m_controllerTypes[joystickIndex]==JoyI::e_pedals)
		{
			m_joyEvent.setSource(WindowEvent::Source(
					WindowEvent::e_sourceWheel0<<joystickIndex));
		}
		else
		{
			m_joyEvent.setSource(WindowEvent::Source(
					WindowEvent::e_sourceJoy0<<joystickIndex));
		}

#if FE_SDL>1
		SDL_GameController* pGameController=m_gameControllers[joystickIndex];

		if(pGameController)
		{
			SDL_GameControllerUpdate();

			for(I32 index=0;
					index<SDL_CONTROLLER_AXIS_MAX && index<m_maxAxes;index++)
			{
				SDL_GameControllerAxis axis=SDL_GameControllerAxis(index);
				const Sint16 int16=
						SDL_GameControllerGetAxis(pGameController,axis);
				I32 value=int16;
				value*=feControllerAxisScale[axis];

#if FE_JOY_DEBUG
				const char* axisName=SDL_GameControllerGetStringForAxis(axis);
				feLog("  axis %d %d \"%s\"\n",index,value,axisName);

				if(m_pAxisLastState[joystickIndex][axis]==value)
				{
					const char* axisName=
							SDL_GameControllerGetStringForAxis(axis);
					feLog("  debounce axis %d %d <-> %d \"%s\"\n",index,
							m_pAxisState[joystickIndex][axis],value,axisName);
				}
#endif

				if(m_pAxisState[joystickIndex][axis]!=value &&
						m_pAxisLastState[joystickIndex][axis]!=value)
				{
					m_pAxisLastState[joystickIndex][axis]=
							m_pAxisState[joystickIndex][axis];
					m_pAxisState[joystickIndex][axis]=value;

					m_joyEvent.setItem(feControllerAxisMap[axis]);
					m_joyEvent.setState(WindowEvent::State(value));
					spSignalerI->signal(m_joyRecord);
				}
			}

			for(I32 index=0;
					index<SDL_CONTROLLER_BUTTON_MAX && index<m_maxButtons;
					index++)
			{
				SDL_GameControllerButton button=SDL_GameControllerButton(index);
				const Uint8 value=
						SDL_GameControllerGetButton(pGameController,button);

#if FE_JOY_DEBUG
				const char* buttonName=
						SDL_GameControllerGetStringForButton(button);
				if(value)
				{
					feLog("  button %d %d \"%s\"\n",index,value,buttonName);
				}
#endif

				if(m_pButtonState[joystickIndex][button] != value)
				{
					m_pButtonState[joystickIndex][button]=value;

					m_joyEvent.setItem(feControllerButtonMap[button]);
					m_joyEvent.setState(WindowEvent::State(value));
					spSignalerI->signal(m_joyRecord);
				}
			}
			continue;
		}
#endif

		SDL_Joystick* pJoystick=m_joysticks[joystickIndex];

		if(!pJoystick || m_controllerTypes[joystickIndex]==e_none)
		{
			continue;
		}

		SDL_JoystickUpdate();

	//	feLog("[H");

		int numAxes=SDL_JoystickNumAxes(pJoystick);
		int numButtons=SDL_JoystickNumButtons(pJoystick);

#if FE_JOY_DEBUG
		int numHats=SDL_JoystickNumHats(pJoystick);
		int numBalls=SDL_JoystickNumBalls(pJoystick);
		String joystickName;

#if FE_SDL>1
		joystickName=SDL_JoystickName(pJoystick);
#else
		int stickIndex=SDL_JoystickIndex(pJoystick);
		joystickName=SDL_JoystickName(stickIndex);
#endif

		feLog("JoySDL::poll \"%s\"\n", joystickName.c_str());
		feLog("%d axes, %d buttons, %d hats, %d balls\n",
				numAxes,numButtons,numHats,numBalls);
#endif

#if FE_JOY_DEBUG
		for(I32 axis=0;axis<numAxes;axis++)
		{
			const Sint16 value=SDL_JoystickGetAxis(pJoystick,axis);
			feLog("  axis %2d %hd          \n",axis,value);
		}
#endif

		for(I32 axis=0;axis<numAxes && axis<m_maxAxes;axis++)
		{
			const Sint16 int16=SDL_JoystickGetAxis(pJoystick,axis);
			I32 value=int16;

			if(m_controllerTypes[joystickIndex]==e_wheel)
			{
				value*=feWheelAxisScale[axis];
			}
			else if(m_controllerTypes[joystickIndex]==e_pedals)
			{
				value*=fePedalAxisScale[axis];
			}
			else
			{
				value*=feJoystickAxisScale[axis];
			}

			if(m_pAxisState[joystickIndex][axis]!=value &&
					m_pAxisLastState[joystickIndex][axis]!=value)
			{
				m_pAxisLastState[joystickIndex][axis]=
						m_pAxisState[joystickIndex][axis];
				m_pAxisState[joystickIndex][axis]=value;

				if(m_controllerTypes[joystickIndex]==e_wheel)
				{
					m_joyEvent.setItem(feWheelAxisMap[axis]);
				}
				else if(m_controllerTypes[joystickIndex]==e_pedals)
				{
					m_joyEvent.setItem(fePedalAxisMap[axis]);
				}
				else
				{
					m_joyEvent.setItem(feJoystickAxisMap[axis]);
				}

				m_joyEvent.setState(WindowEvent::State(value));
				spSignalerI->signal(m_joyRecord);
			}
		}

#if FE_JOY_DEBUG
		for(I32 button=0;button<numButtons;button++)
		{
			Uint8 value=SDL_JoystickGetButton(pJoystick,button);
			if(value)
			{
				feLog("  button %2d %hhd          \n",button,value);
			}
		}
#endif

		for(I32 button=0;button<numButtons && button<m_maxButtons;button++)
		{
			Uint8 value=SDL_JoystickGetButton(pJoystick,button);
#if FE_JOY_DEBUG
//			feLog("  button %2d %hhd          \n",button,value);
#endif

			if(m_pButtonState[joystickIndex][button] != value)
			{
				m_pButtonState[joystickIndex][button]=value;

				m_joyEvent.setItem(feJoystickButtonMap[button]);
				m_joyEvent.setState(WindowEvent::State(value));
				spSignalerI->signal(m_joyRecord);
			}
		}

#if FE_JOY_DEBUG
		for(I32 hat=0;hat<numHats;hat++)
		{
			Uint8 value=SDL_JoystickGetHat(pJoystick,hat);

			feLog("  hat %2d 0x%x          \n",hat,value);
		}

		for(I32 ball=0;ball<numBalls;ball++)
		{
			int dx=0;
			int dy=0;
			int fail=SDL_JoystickGetBall(pJoystick,ball,&dx,&dy);

			feLog("  ball %2d dx=%d dy=%d fail=%d          \n",ball,dx,dy,fail);
		}
#endif
	}
}

} /* namespace ext */
} /* namespace fe */
