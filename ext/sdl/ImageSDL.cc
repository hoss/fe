/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <sdl/sdl.pmh>

namespace fe
{
namespace ext
{

ImageSDL::ImageSDL(void):
	m_pCurrentSurface(NULL),
	m_selected(0),
	m_nextID(1)
{
}

void ImageSDL::initialize(void)
{
	m_spContextSDL=registry()->create("*.ContextSDL");
	if(!m_spContextSDL.isValid())
	{
		feX("ImageSDL::initialize","Unable to create ContextSDL\n");
	}

	if( SDL_InitSubSystem(SDL_INIT_VIDEO) <0 )
	{
		feX("ImageSDL::initialize","Unable to init SDL_INIT_VIDEO: %s\n",
				SDL_GetError());
	}
}

ImageSDL::~ImageSDL(void)
{
}

void ImageSDL::createSurface(ImageI::Format format,U32 width,U32 height)
{
	Uint32 flags=SDL_SWSURFACE;
	int bitsPerPixel=24;
	Uint32 rmask=0x000000FF;
	Uint32 gmask=0x0000FF00;
	Uint32 bmask=0x00FF0000;
	Uint32 amask=0xFF000000;
	switch(format)
	{
		case ImageI::e_colorindex:
			bitsPerPixel=16;	// ????????
			break;
		case ImageI::e_rgba:
			bitsPerPixel=32;
			break;
		default:
			break;
	}
	m_pCurrentSurface=SDL_CreateRGBSurface(flags,width,height,bitsPerPixel,
			rmask,gmask,bmask,amask);
}

I32 ImageSDL::createSelect(void)
{
	createSurface(ImageI::e_rgb,1,1);

	m_selected=m_nextID++;
	m_surfaceMap[m_selected]=m_pCurrentSurface;
	incrementSerial();
	return m_selected;
}

I32 ImageSDL::loadSelect(String filename)
{
//#if FE_OS==FE_WIN32 || FE_OS==FE_WIN64
	m_pCurrentSurface=SDL_LoadBMP(filename.c_str());
//#else
//	m_pCurrentSurface=IMG_Load(filename.c_str());
//#endif

	if(!m_pCurrentSurface)
	{
		m_selected= -1;
		return m_selected;
	}

	SDL_PixelFormat fmt;
	fmt.palette=NULL;
	fmt.BitsPerPixel=24;
	fmt.BytesPerPixel=3;
	fmt.Rmask=0x0000FF;
	fmt.Gmask=0x00FF00;
	fmt.Bmask=0xFF0000;
	fmt.Amask=255;
	fmt.Rloss=0;
	fmt.Gloss=0;
	fmt.Bloss=0;
	fmt.Aloss=0;
	fmt.Rshift=0;
	fmt.Gshift=8;
	fmt.Bshift=16;
	fmt.Ashift=0;
//	fmt.colorkey=0xFFFFFFFF;
//	fmt.alpha=255;

	Uint32 flags=SDL_SWSURFACE;
	SDL_Surface* pSurface=SDL_ConvertSurface(m_pCurrentSurface,&fmt,flags);
	SDL_FreeSurface(m_pCurrentSurface);
	m_pCurrentSurface=pSurface;

	m_selected=m_nextID++;
	m_surfaceMap[m_selected]=m_pCurrentSurface;
	incrementSerial();
	return m_selected;
}

BWORD ImageSDL::save(String filename)
{
	feLog("ImageSDL::save(%s) save not supported in SDL\n",filename.c_str());
	return FALSE;
}

void ImageSDL::select(I32 id)
{
	if(m_selected!=id)
	{
		m_selected=id;
		m_pCurrentSurface=m_surfaceMap[m_selected];
	}
}

void ImageSDL::unload(I32 id)
{
	SDL_Surface* pSurface=m_surfaceMap[id];
	SDL_FreeSurface(pSurface);
	m_surfaceMap.erase(id);
}

void ImageSDL::setFormat(ImageI::Format set)
{
	if(!m_pCurrentSurface)
	{
		return;
	}

	if(set!=format())
	{
		createSurface(set,width(),height());
		m_surfaceMap[m_selected]=m_pCurrentSurface;
	}

	incrementSerial();
}

ImageI::Format ImageSDL::format(void) const
{
	if(!m_pCurrentSurface)
	{
		return ImageI::e_none;
	}

	SDL_PixelFormat* pPixelFormat=m_pCurrentSurface->format;
	if(!pPixelFormat)
	{
		return ImageI::e_none;
	}

	//* naive and presumptuous
	switch(pPixelFormat->BytesPerPixel)
	{
		case 2:
			return ImageI::e_colorindex;
		case 3:
			return ImageI::e_rgb;
		case 4:
			return ImageI::e_rgba;
	}
	return ImageI::e_none;
}

U32 ImageSDL::width(void) const
{
	if(!m_pCurrentSurface)
	{
		return 0;
	}

	SDL_Rect rect;
	SDL_GetClipRect(m_pCurrentSurface,&rect);
	return rect.w;
}

U32 ImageSDL::height(void) const
{
	if(!m_pCurrentSurface)
	{
		return 0;
	}

	SDL_Rect rect;
	SDL_GetClipRect(m_pCurrentSurface,&rect);
	return rect.h;
}

U32 ImageSDL::depth(void) const
{
	if(!m_pCurrentSurface)
	{
		return 0;
	}

	return 1;
}

void ImageSDL::resize(U32 width,U32 height,U32 depth)
{
	if(!m_pCurrentSurface)
	{
		return;
	}

	if(width!=ImageSDL::width() || height!=ImageSDL::height())
	{
		createSurface(format(),width,height);
		m_surfaceMap[m_selected]=m_pCurrentSurface;
	}

	incrementSerial();
}

void ImageSDL::replaceRegion(U32 x,U32 y,U32 z,
		U32 width,U32 height,U32 depth,void* data)
{
	if(!m_pCurrentSurface)
	{
		return;
	}

	U32 bytes=m_pCurrentSurface->format->BytesPerPixel;
	U8* buffer=(U8*)data;
	SDL_Rect rect;
	rect.w=1;
	rect.h=1;

	for(U32 iy=0;iy<height;iy++)
	{
		rect.y=y+iy;
		for(U32 ix=0;ix<width;ix++)
		{
			rect.x=x+ix;
			U8* element=&buffer[bytes*(iy*width+ix)];
			U32 word=(element[2]<<16)+(element[1]<<8)+element[0];
			if(SDL_FillRect(m_pCurrentSurface,&rect,word))
			{
				feX("ImageSDL::replaceRegion","SDL_FillRect failed");
			}
		}
	}

	incrementSerial();
}

void* ImageSDL::raw(void) const
{
	if(!m_pCurrentSurface)
	{
		return NULL;
	}

	// TODO SDL_LockSurface

	return m_pCurrentSurface->pixels;
}

/*
void ImageSDL::draw(U32 x,U32 y) const
{
	if(!m_pCurrentSurface)
	{
		return;
	}

	glRasterPos2i((GLint)x,(GLint)y);
	glPixelZoom(1.0f,-1.0f);

	SDL_Rect rect;
	SDL_GetClipRect(m_pCurrentSurface,&rect);

	BWORD lock=FALSE;
	if(SDL_MUSTLOCK(m_pCurrentSurface))
	{
		SDL_LockSurface(m_pCurrentSurface);
		lock=TRUE;
	}

	glDrawPixels(rect.w,rect.h,
			GL_RGB,GL_UNSIGNED_BYTE,m_pCurrentSurface->pixels);

	if(lock)
		SDL_UnlockSurface(m_pCurrentSurface);
}
*/

} /* namespace ext */
} /* namespace fe */
