/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __datatool_StringAccessor_h__
#define __datatool_StringAccessor_h__

namespace fe
{
namespace ext
{

/**	Access attributes as string lists, one string per sub item.

	Supported attribute types:

	- String:  1 item
	- int: 1 item
	- Real: 1 item
	- SpatialVector: 3 items, 1 per dimension


	*/
class StringAccessor
{
	public:
				StringAccessor(void);
virtual			~StringAccessor(void);

virtual	void	bind(sp<Scope> spScope);
virtual	int		setup(sp<Layout> spLayout, const String &a_attrname);
virtual	void	get(Record r_has, Array<String> &a_values);
virtual	void	set(Record r_has, const String &a_value, int a_index = 0);

virtual	void	setRealFormat(const String &a_format);
virtual	void	setIntegerFormat(const String &a_format);
virtual	void	setStringFormat(const String &a_format);

	private:
		sp<BaseType>					m_spStringType;
		sp<BaseType>					m_spRealType;
		sp<BaseType>					m_spIntType;
		sp<BaseType>					m_spSpatialVectorType;
		sp<BaseType>					m_type;
		Accessor<String>				m_aString;
		Accessor<Real>					m_aReal;
		Accessor<SpatialVector>			m_aSpatialV;
		Accessor<int>					m_aInt;
		BaseAccessor					m_aBase;
		sp<Layout>						m_layout;

		String							m_intFormat;
		String							m_strFormat;
		String							m_realFormat;

};

} /* namespace ext */
} /* namespace fe */

#endif /* __datatool_StringAccessor_h__ */

