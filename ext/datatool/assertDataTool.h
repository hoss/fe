/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __datatool_assertDataTool_h__
#define __datatool_assertDataTool_h__

namespace fe
{
namespace ext
{

FE_DL_EXPORT void assertDataTool(sp<TypeMaster> spTypeMaster);

} /* namespace ext */
} /* namespace fe */

#endif /* __datatool_assertDataTool_h__ */
