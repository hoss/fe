/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <datatool/datatool.pmh>

namespace fe
{
namespace ext
{

#if FALSE
class InfoCountedRecordView : public BaseType::Info
{
virtual	String	print(void *instance)
				{
//					sp<CountedRecordView>* pspC =
//							(sp<CountedRecordView>*)instance;
					return "";
				}

virtual	IWORD	output(std::ostream &ostrm, void *instance, t_serialMode mode)
				{
					if(mode == e_ascii) { return c_noascii; }
					return 0;
//					sp<CountedRecordView>* pspC =
//							(sp<CountedRecordView>*)instance;
				}
virtual	void	input(std::istream &istrm, void *instance, t_serialMode mode)
				{
//					sp<CountedRecordView>* pspC =
//							(sp<CountedRecordView>*)instance;
				}
virtual	IWORD	iosize(void)				{ return c_implicit; }
virtual	bool	getConstruct(void)			{ return true; }
virtual	void	construct(void *instance)
				{	new(instance)sp<CountedRecordView>; }
virtual	void	destruct(void *instance)
				{	((sp<CountedRecordView>*)instance)
							->~sp<CountedRecordView>();}
	private:
		InfoF32 helpF32;	//* huh?
};
#endif

#if FALSE
class InfoCountedRelay : public BaseType::Info
{
virtual	String	print(void *instance)
				{
//					sp<CountedRelay>* pspC = (sp<CountedRelay>*)instance;
					return "";
				}

virtual	IWORD	output(std::ostream &ostrm, void *instance, t_serialMode mode)
				{
					if(mode == e_ascii) { return c_noascii; }
					return 0;
//					sp<CountedRelay>* pspC = (sp<CountedRelay>*)instance;
				}
virtual	void	input(std::istream &istrm, void *instance, t_serialMode mode)
				{
//					sp<CountedRelay>* pspC = (sp<CountedRelay>*)instance;
				}
virtual	IWORD	iosize(void)				{ return c_implicit; }
virtual	bool	getConstruct(void)			{ return true; }
virtual	void	construct(void *instance)	{ new(instance)sp<CountedRelay>; }
virtual	void	destruct(void *instance)
				{	((sp<CountedRelay>*)instance)->~sp<CountedRelay>();}
	private:
		InfoF32 helpF32;	//* huh?
};
#endif

void assertDataTool(sp<TypeMaster> spTypeMaster)
{
	sp<BaseType> spT;

//	spT = spTypeMaster->assertType< sp<CountedRecordView> >("CountedRV");
//	spT->setInfo(new InfoCountedRecordView());

//	spT = spTypeMaster->assertType< sp<CountedRelay> >("CountedRelay");
//	spT->setInfo(new InfoCountedRelay());
}

} /* namespace ext */
} /* namespace fe */
