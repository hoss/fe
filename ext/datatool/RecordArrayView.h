/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __datatool_RecordArrayView_h__
#define __datatool_RecordArrayView_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief RecordView Iteration over a RecordArray

	@ingroup datatool

@code
RecordArrayView<MyRecordView> rav;
sp<RecordArray> spRA=spRG->getArray("my_layout");
rav.bind(spRA);

for(MyRecordView& rMyView: rav)
{
	feLog("%d\n",rMyView.myI32());
}
@endcode

	The RecordView reference obtained by dereferencing an iterator is reused.
	It will simply bind a new Record when stepping the iterator.
	A RecordArrayView instance has its own RecordView instance,
	for at() and operator[](), which is also reused.
*//***************************************************************************/
template<typename T>
class FE_DL_EXPORT RecordArrayView:
	virtual public Component,
	public CastableAs< RecordArrayView<T> >
{
	public:

	class Iterator
	{
		public:

#if FE_CPLUSPLUS >= 201103L
						Iterator(RecordArrayView<T>::Iterator&& a_iterator):
							m_spRecordArray(a_iterator.m_spRecordArray),
							m_index(a_iterator.m_index)
						{	m_recordView.bind(
									a_iterator.m_recordView.scope()); }
#endif

						Iterator(void):
							m_spRecordArray(NULL),
							m_index(0)										{}

						Iterator(sp<RecordArray> a_spRecordArray,I32 a_index):
							m_spRecordArray(a_spRecordArray),
							m_index(a_index)
						{
							sp<Scope> spScope(
									a_spRecordArray->layout()->scope());
							m_recordView.bind(spScope);
						}

			Iterator&	operator=(const Iterator& other) const
						{	m_spRecordArray=other.m_spRecordArray;
							m_index=other.m_index;
							m_recordView.bind(
									m_spRecordArray->layout()->scope()); }

			bool		operator==(const Iterator& other) const
						{	return m_spRecordArray==other.m_spRecordArray &&
									m_index==other.m_index; }

			bool		operator!=(const Iterator& other) const
						{	return m_spRecordArray!=other.m_spRecordArray ||
									m_index!=other.m_index; }

			T&			operator*(void)
						{	m_recordView.setWeakRecord(
									m_spRecordArray->getWeakRecord(m_index));
							return m_recordView; }

	const	Iterator&	operator++(void)
						{
#if FE_CODEGEN<=FE_DEBUG
							if(m_index>=m_spRecordArray->length())
							{
								feX("RecordArrayView::Iterator::operator++"
										"incrementing past end");
							}
#endif
							++m_index;
							return *this;
						}

	const	Iterator	operator++(int)
						{
#if FE_CODEGEN<=FE_DEBUG
							if(m_index>=m_spRecordArray->length())
							{
								feX("RecordArrayView::Iterator::operator++(int)"
										"incrementing past end");
							}
#endif
							return Iterator(m_spRecordArray,m_index++);
						}

	const	Iterator&	operator--(void)
						{
#if FE_CODEGEN<=FE_DEBUG
							if(m_index<=0)
							{
								feX("RecordArrayView::Iterator::operator--"
										"decrementing ahead of begin");
							}
#endif
							--m_index;
							return *this;
						}

	const	Iterator	operator--(int)
						{
#if FE_CODEGEN<=FE_DEBUG
							if(m_index<=0)
							{
								feX("RecordArrayView::Iterator::operator--(int)"
										"decrementing ahead of begin");
							}
#endif
							return Iterator(m_spRecordArray,m_index--);
						}

			I32			index(void) const	{ return m_index; }

		private:
			sp<RecordArray> m_spRecordArray;
			I32				m_index;

			T				m_recordView;
	};

					/** @brief Associate with a specific RecordArray

						Access a RecordView using an Iterator or
						the bracket operator.
						The RecordView will bind to an indexed Record
						in the RecordArray.

						Binding to a RecordArray will also bind to its Scope. */
		void		bind(const sp<RecordArray>& rspRecordArray)
					{
						m_spRecordArray=rspRecordArray;
						sp<Scope> spScope=
								m_spRecordArray->layout()->scope();
						bind(spScope);
					}

					/// @brief Check if bound to a non-zero length RecordArray
		BWORD		isBound(void) const
					{
						return m_spRecordArray.isValid() &&
								m_spRecordArray->length()>0;
					}

					/** @brief Associate with a specific Scope

						This will populate the functors in the local
						RecordView instance.
						This can be useful for makign Accessor check() calls
						before accessing individual records. */
		void		bind(sp<Scope>& rspScope)
					{	m_recordView.bind(rspScope); }

					/// @brief Return the bound Scope
		sp<Scope>	scope(void)
					{	return m_recordView.scope(); }

					/// @brief Return an Iterator pointing at the first index
		Iterator	begin(void) const
					{	return Iterator(m_spRecordArray,0); }

					/// @brief Return an invalid Iterator indicating no more
		Iterator	end(void) const
					{	return Iterator(m_spRecordArray,
								m_spRecordArray->length()); }

					/** @brief Return a RecordView bound to a Record

						This Record is obtained from the RecordArray by index.
						No bounds checking is done.

						Since this RecordView instance can be reused,
						it should only be used for immediate access.
					*/
		T&			operator[](U32 a_index)
					{
						FEASSERT(m_spRecordArray.isValid());
						FEASSERT(a_index<m_spRecordArray->length());
						m_recordView.setWeakRecord(
								m_spRecordArray->getWeakRecord(a_index));
						return m_recordView;
					}

					/** @brief Return a RecordView potentially bound to a Record

						This Record is obtained from the RecordArray by index.
						If the index goes beyond the length of the RecordArray,
						the RecordView is unbound.

						Since this RecordView instance can be reused,
						it should only be used for immediate access.
					*/
		T&			at(U32 a_index)
					{
						if(m_spRecordArray.isValid() &&
								a_index<m_spRecordArray->length())
						{
							m_recordView.setWeakRecord(
									m_spRecordArray->getWeakRecord(a_index));
						}
						else
						{
							m_recordView.unbind();
						}
						return m_recordView;
					}

					/** @brief Return the contained RecordView

						This RecordView is the same RecordView returned by
						at() and the bracket operator.

						It is mainly intended to be used for check() calls
						on its functors.
						This can be done without being bound to a RecordArray
						if a Scope has been bound.

						Since this RecordView instance can be reused,
						it should only be used for immediate access.
					*/
		T&			recordView(void)	{ return m_recordView; }

	private:
		sp<RecordArray> m_spRecordArray;
		T				m_recordView;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __datatool_RecordArrayView_h__ */
