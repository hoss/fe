/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <datatool/datatool.pmh>

namespace fe
{
namespace ext
{

void RecordPlayer::handle(Record& record)
{
	if(!m_asOperator.scope().isValid())
	{
		m_asOperator.bind(record.layout()->scope());
		m_asTemporal.bind(record.layout()->scope());
		m_aRecorder.initialize(record.layout()->scope(),FE_USE("op:recorder"));
	}

	if(!m_spRecorderI.isValid() && m_aRecorder.check(record))
	{
		m_spRecorderI=m_aRecorder(record);
	}

	const F32 time=m_asTemporal.time.check(record)?
			m_asTemporal.time(record): 0.0f;
	if(m_lastTime==time)
	{
		return;
	}

	m_lastTime=time;

	if(!m_spRecorderI.isValid())
	{
		return;
	}

	const F32 timestep=m_asTemporal.timestep.check(record)?
			m_asTemporal.timestep(record): 0.0f;
	if(timestep==0.0f)
	{
		String filename=m_spRecorderI->findNameFor(time);

		feLog("RecordPlayer::handle load \"%s\"\n",filename.c_str());

		std::ifstream infile(filename.c_str());
		if(!infile)
		{
			feLog("RecordPlayer::handle could not open input file \"%s\"\n",
					filename.c_str());
		}
		else
		{
			sp<data::StreamI> spStream(new data::AsciiStream(
					record.layout()->scope()));
			sp<RecordGroup> spInputGroup=spStream->input(infile);
			infile.close();

			m_asOperator.input(record)=spInputGroup;
		}
	}
}

} /* namespace ext */
} /* namespace fe */