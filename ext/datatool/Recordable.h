/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __datatool_Recordable_h__
#define __datatool_Recordable_h__

FE_ATTRIBUTE("rec:name",		"Name of Component");
FE_ATTRIBUTE("rec:component",	"Instance of Component");

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Recordable RecordView

	@ingroup datatool
*//***************************************************************************/
class FE_DL_EXPORT Recordable: virtual public RecordView
{
	public:
		Functor<I32>				serial;
		Functor<String>				recordableName;
		Functor< cp<Component> >	component;

				Recordable(void)		{ setName("Recordable"); }
virtual	void	addFunctors(void)
				{
					add(serial,			FE_USE(":SN"));
					add(recordableName,	FE_USE("rec:name"));
					add(component,		FE_USE("rec:component"));
				}
virtual	void	initializeRecord(void)
				{
					RecordView::initializeRecord();

					component.attribute()->setSerialize(FALSE);

//					feLog("Recordable::initializeRecord %s\n",name().c_str());
				}
virtual	void	finalizeRecord(void)
				{
					RecordView::finalizeRecord();

//					feLog("Recordable::finalizeRecord %s %s\n",
//							name().c_str(),recordableName().c_str());

					if(!recordableName().empty())
					{
						component.createAndSetComponent(recordableName());
					}

					cp<RecordableI> cpRecordableI=component();
					if(cpRecordableI.isValid())
					{
						Record thisRecord=record();
						cpRecordableI.writable()->bind(thisRecord);
					}
#if FE_CODEGEN<=FE_DEBUG
					else
					{
						feLog("Recordable::finalizeRecord"
								" %s isn't RecordableI\n",name().c_str());
					}
#endif
				}
};

} /* namespace ext */
} /* namespace fe */

#endif /* __datatool_Recordable_h__ */
