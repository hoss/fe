/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "datatool/datatool.h"

namespace fe
{
namespace ext
{

StringAccessor::StringAccessor(void)
{
	m_intFormat = "%d";
	m_strFormat = "%s";
	m_realFormat = "%f";
}

StringAccessor::~StringAccessor(void)
{
}

void StringAccessor::bind(sp<Scope> spScope)
{
	sp<TypeMaster> spTM = spScope->typeMaster();
	m_spStringType = spTM->lookupType<String>();
	m_spRealType = spTM->lookupType<Real>();
	m_spIntType = spTM->lookupType<int>();
	m_spSpatialVectorType = spTM->lookupType<SpatialVector>();
}

int StringAccessor::setup(sp<Layout> spLayout, const String &a_attrname)
{
	if(!m_spStringType.isValid())
	{
		bind(spLayout->scope());
	}

	m_layout = NULL;
	m_type = NULL;
	int items = 0;
	sp<Attribute> spAttr = spLayout->scope()->findAttribute(a_attrname);
	if(!spAttr.isValid())
	{
		return items;
	}

	if(spAttr->type() == m_spStringType)
	{
		m_aString.initialize(spLayout->scope(), spAttr);
		m_aBase = m_aString;
		items = 1;
	}

	if(spAttr->type() == m_spRealType)
	{
		m_aReal.initialize(spLayout->scope(), spAttr);
		m_aBase = m_aReal;
		items = 1;
	}

	if(spAttr->type() == m_spIntType)
	{
		m_aInt.initialize(spLayout->scope(), spAttr);
		m_aBase = m_aInt;
		items = 1;
	}

	if(spAttr->type() == m_spSpatialVectorType)
	{
		m_aSpatialV.initialize(spLayout->scope(), spAttr);
		m_aBase = m_aSpatialV;
		items = 3;
	}

	if(!m_aBase.check(spLayout))
	{
		items = 0;
	}

	if(items > 0)
	{
		m_layout = spLayout;
		m_type = spAttr->type();
	}

	return items;
}

void StringAccessor::get(Record r_has, Array<String> &a_values)
{
	if(m_layout != r_has.layout())
	{
		return;
	}

	if(m_type == m_spStringType)
	{
		a_values.resize(1);
		a_values[0].sPrintf(m_strFormat.c_str(), m_aString(r_has).c_str());
	}

	if(m_type == m_spRealType)
	{
		a_values.resize(1);
		a_values[0].sPrintf(m_realFormat.c_str(), m_aReal(r_has));
	}

	if(m_type == m_spIntType)
	{
		a_values.resize(1);
		a_values[0].sPrintf(m_intFormat.c_str(), m_aInt(r_has));
	}

	if(m_type == m_spSpatialVectorType)
	{
		a_values.resize(3);
		a_values[0].sPrintf(m_realFormat.c_str(), m_aSpatialV(r_has)[0]);
		a_values[1].sPrintf(m_realFormat.c_str(), m_aSpatialV(r_has)[1]);
		a_values[2].sPrintf(m_realFormat.c_str(), m_aSpatialV(r_has)[2]);
	}
}

void StringAccessor::set(Record r_has, const String &a_value, int a_index)
{
	if(m_layout != r_has.layout())
	{
		return;
	}

	if(m_type == m_spStringType)
	{
		m_aString(r_has) = a_value;
	}

	if(m_type == m_spSpatialVectorType)
	{
		if(a_index >=0 && a_index < 3)
		{
			m_aSpatialV(r_has)[a_index] = atof(a_value.c_str());
		}
	}
}

void StringAccessor::setRealFormat(const String &a_format)
{
	m_realFormat = a_format;
}

void StringAccessor::setIntegerFormat(const String &a_format)
{
	m_intFormat = a_format;
}

void StringAccessor::setStringFormat(const String &a_format)
{
	m_strFormat = a_format;
}

} /* namespace ext */
} /* namespace fe */