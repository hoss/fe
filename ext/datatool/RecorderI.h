/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __datatool_RecorderI_h__
#define __datatool_RecorderI_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Storage of incremental state to a resource, like a file

	@ingroup datatool
*//***************************************************************************/
class FE_DL_EXPORT RecorderI:
	virtual public Component,
	public CastableAs<RecorderI>
{
	public:
virtual	void	configure(String pathname,U32 limit)						=0;
virtual	String	findNameFor(Real scalar)									=0;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __datatool_RecorderI_h__ */
