/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __datatool_RecordView_h__
#define __datatool_RecordView_h__

#define	FE_RV_VERBOSE	(FE_CODEGEN<=FE_DEBUG)
#define	FE_RV_CHECK		(FE_CODEGEN<=FE_DEBUG)

// check that RecordArray has current index
#define FE_RV_R_CHECK	(FE_CODEGEN<=FE_DEBUG)

// check that WeakRecord's have serial numbers
#define FE_RV_WR_CHECK	(FE_CODEGEN<=FE_DEBUG)

namespace fe
{
namespace ext
{

//* TODO remove RecordArray support overhead (use RecordArrayView)

/**************************************************************************//**
	@brief Bindable collection of accessor Functors

	@ingroup datatool

	RecordView is an enhanced AccessorSet that supports inheritance
	and default values.  Like an AccessorSet, a RecordView instance
	can be reused for any number of Record instances.

	For example:

@code
class FE_DL_EXPORT Point: public RecordView
{
	public:
		Functor<SpatialVector>		location;

				Point(void)			{ setName("Point"); }
virtual	void	addFunctors(void)
				{
					add(length,		"spatial_vector",
							FE_SPEC("spc:at",
							"translation from origin"));
				}
virtual	void	initializeRecord(void)
				{
					set(location());
				}
virtual	void	finalizeRecord(void)
				{
				}
};
@endcode

	A class that derives from RecordView is expected to provide several things.
	Added attributes are provided through Functors,
	an enhancement of the Accessor.

	Provide a name for the generated Layout by setting the name
	of the RecordView, using setName().

	Functors are wired up using add() during addFunctors().
	The implementation of addFunctors() should call the like-named functions
	for any direct base classes, much like a manual constructor mechanism.
	Convention insists that attributes names are provided using
	FE_USE or FE_SPEC.  FE_USE presumes that an FE_SPEC or FE_ATTRIBUTE
	elsewhere has provided a description for the atrribute.

	As a RecordFactoryI, a RecordView can be used to instantiate Records
	by name from file.  The method initializeRecord is called before
	loading data to provide basic defaults.  A method finalizeRecord
	can be used after loading a Record for producing derived values,
	such as creating a component as named by an attribute.
	Both initializeRecord and finalizeRecord may be omitted if
	nothing needs to be done for that stage.
	As with addFunctors, both initializeRecord and finalizeRecord should
	call the like-named functions for any direct base classes.

	To be available as a RecordFactoryI, each RecordView-derived type
	should be added to a library during CreateLibrary()
	like any other Component.

@code
pLibrary->add<Point>("RecordFactoryI.Point.fe");
@endcode

	File-based defaults can be provided as a template in a record group file.

@code
TEMPLATE Point ""
	spc:at "0 0 0"
@endcode

	Attributes not represented by a template retain settings from
	initializeRecord or their type's constructor.
	Derived templates can provide new defaults that override prior values.
	In the basic case, templates will parallel provided Factories,
	including multiple inheritance (the second argument to TEMPLATE).
	However, new templates can be provided purely on multiple inheritance
	where no new attributes are added.

*//***************************************************************************/
class RecordView:
	public AccessorSet,
	virtual public RecordFactoryI
{
	protected:

	/// @brief Untyped Functor base
	class RecordHolder
	{
		public:
#if FE_RV_CHECK
								RecordHolder(void):
										m_pspRecordArray(NULL),
										m_pWeakRecord(NULL),
										m_pIndex(0)							{}
#endif
								/// NOP: RecordView will handle copying
			RecordHolder&		operator=(const RecordHolder& rRecordHolder)
								{	return *this; }

			void				bind(sp<RecordArray>& rspRecordArray,
									WeakRecord& rWeakRecord,U32& rIndex)
								{
									m_pspRecordArray=&rspRecordArray;
									m_pWeakRecord=&rWeakRecord;
									m_pIndex=&rIndex;
								}
		protected:
			sp<RecordArray>&	array(void) const
								{
#if FE_RV_R_CHECK
									if(!m_pspRecordArray)
									{
										feX("RecordView::RecordHolder::array",
												"m_pspRecordArray NULL");
									}
#endif
									return *m_pspRecordArray;
								}

			WeakRecord&			weakRecord(void) const
								{
#if FE_RV_R_CHECK
									if(!m_pWeakRecord)
									{
										feX("RecordView::RecordHolder"
												"::weakRecord",
												"m_pWeakRecord NULL");
									}
#endif
									return *m_pWeakRecord;
								}

			U32					arrayIndex(void) const	{ return *m_pIndex; }

			sp<RecordArray>*	m_pspRecordArray;
			WeakRecord*			m_pWeakRecord;
			U32*				m_pIndex;
	};

	/// @brief Bound accessor in a RecordView
	/// @ingroup datatool
	template<typename T>
	class Functor: public RecordHolder, public Accessor<T>
	{
		public:
						Functor(void)										{}
			T&			operator()(void);
	const	T			operator()(void) const;
			T&			operator()(const Record& r)
						{	return Accessor<T>::operator()(r); }
const		T			operator()(const Record& r) const
						{	return Accessor<T>::operator()(r); }
			T&			operator()(const WeakRecord& r)
						{	checkWeakRecord(r);
							return Accessor<T>::operator()(r); }
const		T			operator()(const WeakRecord& r) const
						{	checkWeakRecord(r);
							return Accessor<T>::operator()(r); }
			T&			operator()(sp<RecordArray>& rspRA, FE_UWORD index)
						{	return Accessor<T>::operator()(rspRA,index); }
const		T&			operator()(sp<RecordArray>& rspRA, FE_UWORD index) const
						{	return Accessor<T>::operator()(rspRA,index); }

			BWORD		check(void) const
						{
							FEASSERT(m_pspRecordArray);
							FEASSERT(m_pWeakRecord);
							FEASSERT((array()).isValid());
							FEASSERT(m_pIndex);
							return Accessor<T>::check(array());
						}
			BWORD		check(sp<RecordArray>& rspRA) const
						{	return Accessor<T>::check(rspRA); }
			BWORD		check(const Record& r) const
						{	return Accessor<T>::check(r); }

						/** @brief Get the value, checking first

							If the bound Record does not support the
							attribute, zero is returned, cast to the
							attribute type. */
			T			safeGet(void)
						{
							T* pValue=NULL;
							if(array().isValid() &&
									(pValue=Accessor<T>::queryAttribute(
									array(),arrayIndex())))
							{
								return *pValue;
							}
							if(weakRecord().isValid() &&
									(pValue=Accessor<T>::queryAttribute(
									weakRecord())))
							{
								return *pValue;
							}
							return T(0);
						}

						/** @brief Set the value, checking first

							If the bound Record does not support the
							attribute, nothing occurs. */
			void		safeSet(T value)
						{
							T* pValue=NULL;
							if(array().isValid() &&
									(pValue=Accessor<T>::queryAttribute(
									array(),arrayIndex())))
							{
								*pValue=value;
							}
							if(weakRecord().isValid() &&
									(pValue=Accessor<T>::queryAttribute(
									weakRecord())))
							{
								*pValue=value;
							}
						}

						/** @brief Call the attribute as a method

							If the attribute cannot be cast to HandlerI,
							nothing occurs.
							If the cast is successful, the currently indexed
							Record is passed to the HandlerI using handle().

							Trying to call an attribute whose content is not
							derived from Counted or similar may cause a
							compiler error. */
			void		call(void)
						{
							sp<HandlerI> spHandlerI=safeGet();
							if(spHandlerI.isValid())
							{
								Record record;
								if(array().isValid())
								{
									record=array()->getRecord(
											arrayIndex());
								}
								else
								{
									record=weakRecord();
								}

								spHandlerI->handle(record);
							}
						}

						/** @brief Create a named component for the attribute

							The created component is placed in the attribute.
							If the attribute is not a component,
							a compile error should occur. */
			void		createAndSetComponent(String componentName)
						{
							sp<Scope> spScope=Accessor<T>::scope();
							FEASSERT(spScope.isValid());
							sp<Component> spComponent=
									spScope->registry()->create(componentName);
							if(!spComponent.isValid())
							{
								feX("Functor<>::createAndSetComponent",
										"\"%s\" failed to create \"%s\"\n",
										name().c_str(),
										componentName.c_str());
							}
							spComponent->setName(componentName+" "+name());
							operator()()=spComponent;
						}

						/** @brief Create a RecordGroup for the attribute

							The RecordGroup is placed in the attribute.
							If the attribute is not a RecordGroup,
							a compile error should occur. */
			void		createAndSetRecordGroup(void)
						{
							FEASSERT(m_pspRecordArray);
							FEASSERT(m_pWeakRecord);
							FEASSERT(m_pIndex);
							sp<RecordGroup> spRecordGroup(new RecordGroup());
							FEASSERT(spRecordGroup.isValid());
#if FE_COUNTED_TRACK
							spRecordGroup->setName(name());
#endif
							operator()()=spRecordGroup;
						}

const	String&			name(void) const
						{	return BaseAccessor::attribute()->name(); }
		private:
		void			checkRecordExists(void) const
						{
#if FE_RV_R_CHECK
							if(weakRecord().isValid())
							{
								return;
							}
							if(!array().isValid())
							{
								feX("RecordView::checkRecordExists",
										"RecordArray invalid");
							}
							if(!array()->length())
							{
								feX("RecordView::checkRecordExists",
										"no Record bound");
							}
							else if((int)arrayIndex()>=array()->length())
							{
								feX("RecordView::checkRecordExists",
										"looking for index %d in RA length %d",
										arrayIndex(),array()->length());
							}
#endif
						}

		void			checkWeakRecord(const WeakRecord& r) const
						{
#if FE_RV_WR_CHECK
							r.demandSerialNumber();
#endif
						}
	};

	public:
					RecordView(void):
						m_tempArray(FALSE)
					{
					}

virtual				~RecordView(void)
					{
#if FE_COUNTED_TRACK
						deregisterRegion(fe_cast<Counted>(this));
#endif
					}

		void		setName(const String& name)
					{
						Component::setName(name);
#if FE_COUNTED_TRACK
						registerRegion(fe_cast<Counted>(this),
							sizeof(RecordView));
#endif
					}

const	String		verboseName(void) const
					{	return "RecordView " + name(); }

		Accessor<int>	count;

					/** @brief Associate with a Scope

						This can happen automatically by binding
						to a Record or RecordArray, but may be neccessary
						if you need to use the Functors before an
						appropriate Layout exists. */
		void		bind(sp<Scope>& rspScope)
					{
						FEASSERT(rspScope.isValid());

						if(!m_hpScope.isValid())
						{
							AccessorSet::bind(rspScope);
							addFunctors();
						}
#if FE_RV_CHECK
						else if(m_hpScope!=rspScope)
						{
							feX("RecordView::bind(sp<Scope>&) \"%s\"",
									"changed scope",name().c_str());
						}
#endif
					}

					/// @brief Associate with a Scope by Handle
		void		bind(hp<Scope>& rhpScope)
					{
						FEASSERT(rhpScope.isValid());

						if(!m_hpScope.isValid())
						{
							AccessorSet::bind(rhpScope);
							addFunctors();
						}
#if FE_RV_CHECK
						else if(m_hpScope!=rhpScope)
						{
							feX("RecordView::bind(hp<Scope>&) \"%s\"",
									"changed scope",name().c_str());
						}
#endif
					}

					/// @brief Return associated Scope
		hp<Scope>&	scope(void)						{ return m_hpScope; }

					/** @brief Associate with a specific RecordArray

						Use setIndex() or step() to associate with
						each Record. */
		void		bind(const sp<RecordArray>& rspRecordArray)
					{
						m_weakRecord=WeakRecord();
						m_tempArray=FALSE;
						m_index=0;
						m_nextIndex=0;
						if(m_spRecordArray==rspRecordArray)
							return;

						m_spRecordArray=rspRecordArray;
#if !FE_RV_CHECK
						if(!m_hpScope.isValid())
#endif
						{
							bind(rspRecordArray->layout()->scope());
						}
					}

					/** @brief Disassociate with any Record or RecordArray

						This should free all references. */
		void		unbind(void)
					{
						m_spRecordArray=NULL;
						m_weakRecord=WeakRecord();
					}

					/** @brief Associate with a specific Record

						This is only intended to be used when there is no
						containing RecordArray available.
						It can be more efficient to bind to
						a RecordArray and use setIndex() or step().

						Passing Record objects is not thread-safe. */
		void		bind(const Record record,BWORD weak=FALSE)
					{
						m_weakRecord=WeakRecord();
						m_index=0;
						m_nextIndex=0;

						if(!record.isValid())
						{
							if(m_tempArray && m_spRecordArray.isValid())
							{
								m_spRecordArray->clear();
							}
							else
							{
								m_spRecordArray=NULL;
							}
							return;
						}

						if(m_tempArray && m_spRecordArray.isValid() &&
								m_spRecordArray->layout()!=record.layout())
						{
#if 1
							feLog("RecordView::bind(Record) \"%s\" tempArray"
									" switching type from \"%s\""
									" to \"%s\"\n",
									name().c_str(),
									m_spRecordArray->layout()->name().c_str(),
									record.layout()->name().c_str());
#endif

#if 0
							m_tempArray=FALSE;
							m_spRecordArray=NULL;
#else
							// is this safe?
							m_spRecordArray->clear();
							m_spRecordArray->setLayout(record.layout());
							m_spRecordArray->setWeak(weak);
#endif
						}
						else if(!m_tempArray || !m_spRecordArray.isValid())
						{
#if 0
							feLog("RecordView::bind(Record)"
									" \"%s\" new \"%s\"\n",
									name().c_str(),
									record.layout()->name().c_str());
#endif
							m_spRecordArray=new RecordArray;
							m_spRecordArray->setWeak(weak);
						}
						else
						{
							m_spRecordArray->clear();
						}

						m_tempArray=TRUE;

						if(!m_spRecordArray.isValid() ||
								!m_spRecordArray->add(record))
						{
							feX("RecordView::bind(Record)",
									"failed to create and populate RA \"%s\"",
									name().c_str());
						}
#if !FE_RV_CHECK
						if(!m_hpScope.isValid())
#endif
						{
							bind(record.layout()->scope());
						}
					}

		void		bind(const WeakRecord weakRecord,BWORD weak=FALSE)
					{
						m_weakRecord=weakRecord;

						m_index=0;
						m_nextIndex=0;
						m_spRecordArray=NULL;
						m_tempArray=FALSE;

#if !FE_RV_CHECK
						if(!m_hpScope.isValid())
#endif
						{
							bind(weakRecord.layout()->scope());
						}
					}

					/**	@brief Set the index into the bound RecordArray

						The first step() after setIndex() does not increment
						the index. */
		void		setIndex(U32 index)
					{
						assignIndex(index);
						m_nextIndex=index;
					}

					/**	@brief Step through the records in the array

						The first step will stay on the first record.
						Later steps increment the index.
						Stepping past the last record will return FALSE.

						In the future, filters may allow step() to
						skip records. */
		BWORD		step(void)
					{
						if(m_spRecordArray.isValid() &&
								I32(m_nextIndex)<m_spRecordArray->length())
						{
							assignIndex(m_nextIndex);
							m_nextIndex++;
							return !m_spRecordArray->isWeak() ||
									record().isValid();
						}

						return FALSE;
					}

					///	@brief Return the current index into the RecordArray
		U32			index(void)
					{
						return m_index;
					}

					/**	@brief Produce a Record using all the attributes

						The Record is unbound after initialization. */
virtual	void		produce(Record& rRecord)
					{
						if(rRecord.isValid())
						{
							bind(rRecord);
							initializeRecord();
						}
						else
						{
							rRecord=createRecord();
						}
						unbind();
					}

					/**	@brief Finalize a Record using all the attributes

						This step follows after the record is fully
						initialized and potentially serialized in.

						The Record is unbound after initialization. */
virtual	void		finalize(Record& rRecord)
					{
						if(rRecord.isValid())
						{
							bind(rRecord);
							finalizeRecord();
						}
						unbind();
					}

					/**	@brief Create a Record using all the attributes

						The Record is automatically bound. */
		Record		createRecord(void)
					{
						if(!m_hpScope.isValid())
						{
							feX("RecordView::createRecord",
									"\"%s\" has invalid scope\n",
									name().c_str());
						}

						Record record=m_hpScope->createRecord(layout());
						if(!m_hpLayout.isValid())
						{
							feX("RecordView::createRecord",
									"\"%s\" failed to create layout\n",
									name().c_str());
						}
						if(!record.isValid())
						{
							feX("RecordView::createRecord",
									"\"%s\" failed to create record\n",
									name().c_str());
						}

						bind(record);
						initializeRecord();
						return record;
					}

					///	@brief Access the record by weak (fast) reference
		WeakRecord	record(void) const
					{
						if(m_spRecordArray.isValid())
							return m_spRecordArray->getWeakRecord(m_index);

						return m_weakRecord;
					}

					///	@brief Access a Layout of all the attributes
		sp<Layout>	layout(void)
					{
						if(!m_hpScope.isValid())
						{
							feLog("RecordView::layout \"%s\" invalid scope\n",
									name().c_str());
							return sp<Layout>(NULL);
						}
						if(!m_hpLayout.isValid())
						{
//							feLog("RecordView::layout \"%s\" createLayout\n",
//									name().c_str());
							m_hpLayout=createLayout();
						}
						return m_hpLayout;
					}

					///	@brief Spew attributes and state for the bound Record
		void		dump(void)
					{
						U32 number=size();
						for(U32 m=0;m<number;m++)
						{
							FEASSERT((*this)[m]);
							if(!record().isValid())
							{
								feLog("%2d %-16s %-16s <invalid record>\n",m,
										nameOf(*(*this)[m]).c_str(),
										typeOf(*(*this)[m]).c_str());
							}
// valid record should always be locked anyway
#if 0
							else if(!record().layout()->locked())
							{
								feLog("%2d %-16s %-16s <not locked>\n",m,
										nameOf(*(*this)[m]).c_str(),
										typeOf(*(*this)[m]).c_str());
							}
#endif
							else
							{
								feLog("%2d %-16s %-16s %s\n",m,
										nameOf(*(*this)[m]).c_str(),
										typeOf(*(*this)[m]).c_str(),
										printOf(*(*this)[m]).c_str());
							}
						}
					}

					/**	@brief Called at instantiation to add functors

						In addition to using add() to explicitly adding all
						the functors for a particular class,
						the implementation of this function should first call
						addFunctors() for all directly inherited classes
						that are indirectly or directly derived from
						RecordView(). */
virtual	void		addFunctors(void)										=0;

					/**	@brief Called at instantiation to initialize
						attributes

						In addition to setting any attributes, the
						implementation of this function should first call
						initializeRecord() for all directly inherited classes
						that are indirectly or directly derived from
						RecordView().

						Functors can be used as accessors at this point. */
virtual	void		initializeRecord(void)									{}

					/**	@brief Called right after instantiation to finalize
						complex attributes

						In addition to adjusting any attributes, the
						implementation of this function should first call
						finalizeRecord() for all directly inherited classes
						that are indirectly or directly derived from
						RecordView().

						Functors can be used as accessors at this point. */
virtual	void		finalizeRecord(void)									{}

					/** @brief Indicate a RecordFactoryI for a Scope

						The template type is presumably a RecordView.
						This function extracts the relevant arguments
						to pass to the Scope.

						It is usually preferable to add the RecordView
						as a Scope-generic RecordFactoryI using the
						general registry mechanism.

						This is a convenience that doesn't really relate
						to RecordView. */
					template <typename T>
static	void		registerFactory(sp<Scope> spScope)
					{
						T* pRV=new T();
						sp<RecordFactoryI> spRecordFactoryI(pRV);
						spScope->registerFactory(pRV->name(),spRecordFactoryI);
					}

					/** @brief Load the root RecordGroup from a file

						This is a convenience that doesn't really relate
						to RecordView. */
static	sp<RecordGroup> loadRecordGroup(sp<Scope> spScope,
							String filename,BWORD a_binary=FALSE)
					{
						std::ifstream inFile(filename.c_str());
						if(!inFile)
						{
#if FE_RV_VERBOSE
							feLog("RecordView::loadRecordGroup"
									" could not open\n  \"%s\"\n",
									filename.c_str());
#endif
							return sp<RecordGroup>();
						}

						sp<data::StreamI> spStream(a_binary?
								sp<data::StreamI>(
								new data::BinaryStream(spScope)):
								sp<data::StreamI>(
								new data::AsciiStream(spScope)));
						sp<RecordGroup> spRecordGroup=spStream->input(inFile);
						inFile.close();

						return spRecordGroup;
					}

static	sp<RecordGroup> loadRecordGroupFromBuffer(sp<Scope> spScope,
							String buffer,BWORD a_binary=FALSE)
					{
						std::istringstream inString(buffer.c_str());
						if(!inString)
						{
#if FE_RV_VERBOSE
							feLog("RecordView::loadRecordGroupFromBuffer"
									" could not open string buffer\n");
#endif
							return sp<RecordGroup>();
						}

						sp<data::StreamI> spStream=a_binary?
								sp<data::StreamI>(
								new data::BinaryStream(spScope)):
								sp<data::StreamI>(
								new data::AsciiStream(spScope));
						sp<RecordGroup> spRecordGroup=spStream->input(inString);

						return spRecordGroup;
					}

					/** @brief Save the root RecordGroup to a file

						This is a convenience that doesn't really relate
						to RecordView. */
static	void		saveRecordGroup(sp<Scope> spScope,
							sp<RecordGroup>& rspRecordGroup,
							String filename,BWORD a_binary=FALSE)
					{
						std::ofstream outfile(filename.c_str());
						if(!outfile)
						{
							feLog("RecordView::saveRecordGroup"
									" could not open test output file\n");
						}
						else
						{
							sp<data::StreamI> spStream=a_binary?
									sp<data::StreamI>(
									new data::BinaryStream(spScope)):
									sp<data::StreamI>(
									new data::AsciiStream(spScope));
							spStream->output(outfile, rspRecordGroup);
							outfile.close();
						}
					}

	protected:
					/**	@brief Add an attribute Functor

						The functor should be a member variable.
						The name can be arbitrary, but conventions
						may be wise. */
		void		add(BaseAccessor& rFunctor,const char* name)
					{
						rFunctor.setup(m_hpScope,name);
//						m_hpScope->support(name,type);
						AccessorSet::add(rFunctor,name);
						RecordHolder* pHolder=
								static_cast<RecordHolder*>(
								static_cast<Functor<int>*>(&rFunctor));
						pHolder->bind(m_spRecordArray,m_weakRecord,m_index);
					}
	private:
//					RecordView(const RecordView& rRecordView) :
//						Initialized(), Component(), PopulateI(),
//						AccessorSet(), RecordFactoryI()
					RecordView(const RecordView& rRecordView):
						AccessorSet()										{}

		void		assignIndex(U32 index)
					{
#if FE_RV_CHECK
						if(!m_spRecordArray.isValid())
						{
							feLog("RecordView::assignIndex \"%s\""
									" no m_spRecordArray bound\n",
									name().c_str());
							return;
						}
#endif
#if FE_RV_CHECK || 0//FE_PREFETCHING
						const U32 length=m_spRecordArray->length();
#endif
#if FE_RV_CHECK
						if(U32(index) >= length)
						{
							feLog("RecordView::assignIndex \"%s\""
									" index out of bounds (%d/%d)\n",
									name().c_str(),index,
									m_spRecordArray->length());
						}
#endif
#if 0//FE_PREFETCHING
						if(index+2<length)
						{
							FEPREFETCH_READ(m_spRecordArray->data(index+2));
						}
#endif
						m_index=index;
					}

		sp<Layout>	createLayout(void)
					{
						if(!m_hpScope.isValid())
						{
							feX("AccessorArray::createLayout",
									"\"%s\" has invalid scope\n",
									name().c_str());
						}

						sp<Layout> spLayout=m_hpScope->declare(name());
						if(!spLayout.isValid())
						{
							feX("AccessorArray::createLayout",
									"\"%s\" failed to create layout\n",
									name().c_str());
						}

						populate(spLayout);
						return spLayout;
					}

		String		nameOf(BaseAccessor& accessor) const
					{
						return accessor.attribute()->name();
					}

		String		typeOf(BaseAccessor& accessor) const
					{
						std::list<String> nameList;
						m_hpScope->registry()->master()->typeMaster()
								->reverseLookup(baseTypeOf(accessor),nameList);
						return nameList.front();
					}

		String		printOf(BaseAccessor& accessor)
					{
						void* pVoid=record().rawAttribute(accessor.index());

						sp<BaseType::Info> spInfo=helpOf(accessor);
						return spInfo->print(pVoid);
					}

		sp<BaseType::Info> helpOf(BaseAccessor& accessor) const
					{
						sp<BaseType::Info> spInfo=
								baseTypeOf(accessor)->getInfo();
						FEASSERT(spInfo.isValid());
						return spInfo;
					}

		sp<BaseType> baseTypeOf(BaseAccessor& accessor) const
					{
						sp<Attribute> spAttribute=accessor.attribute();
						return spAttribute->type();
					}

		hp<Layout>		m_hpLayout;

		sp<RecordArray>	m_spRecordArray;
		WeakRecord		m_weakRecord;
		U32				m_index;
		U32				m_nextIndex;
		BWORD			m_tempArray;
};

template<typename T>
inline T& RecordView::Functor<T>::operator()(void)
{
#if FE_RV_CHECK
	// Accessor already checks
//	FEASSERT(check());
#endif
#if FE_RV_R_CHECK
	checkRecordExists();
#endif
	if(array().isValid())
	{
		return Accessor<T>::operator()(array(),arrayIndex());
	}
	return Accessor<T>::operator()(weakRecord());
}

template<typename T>
inline const T RecordView::Functor<T>::operator()(void) const
{
#if FE_RV_CHECK
	// Accessor already checks
//	FEASSERT(check());
#endif
#if FE_RV_R_CHECK
	checkRecordExists();
#endif
	if(array().isValid())
	{
		return Accessor<T>::operator()(array(),arrayIndex());
	}
	return Accessor<T>::operator()(weakRecord());
}

template<>
inline WeakRecord& RecordView::Functor<WeakRecord>::operator()(void)
{
#if FE_RV_CHECK
	// Accessor already checks
//	FEASSERT(check());
#endif
#if FE_RV_R_CHECK
	checkRecordExists();
#endif

	if(array().isValid())
	{
#if FE_RV_WR_CHECK
		checkWeakRecord(Accessor<WeakRecord>::operator()(array(),arrayIndex()));
#endif
		return Accessor<WeakRecord>::operator()(array(),arrayIndex());
	}

#if FE_RV_WR_CHECK
	checkWeakRecord(Accessor<WeakRecord>::operator()(weakRecord()));
#endif
	return Accessor<WeakRecord>::operator()(weakRecord());
}

template<>
inline const WeakRecord RecordView::Functor<WeakRecord>::operator()(void) const
{
#if FE_RV_CHECK
	// Accessor already checks
//	FEASSERT(check());
#endif
#if FE_RV_R_CHECK
	checkRecordExists();
#endif

	if(array().isValid())
	{
#if FE_RV_WR_CHECK
		checkWeakRecord(Accessor<WeakRecord>::operator()(array(),arrayIndex()));
#endif
		return Accessor<WeakRecord>::operator()(array(),arrayIndex());
	}

#if FE_RV_WR_CHECK
	checkWeakRecord(Accessor<WeakRecord>::operator()(weakRecord()));
#endif
	return Accessor<WeakRecord>::operator()(weakRecord());
}

inline BWORD operator==(const RecordView& rRecordView1,
		const RecordView& rRecordView2)
{
	return rRecordView1.record()==rRecordView2.record();
}

inline BWORD operator!=(const RecordView& rRecordView1,
		const RecordView& rRecordView2)
{
	return rRecordView1.record()!=rRecordView2.record();
}

} /* namespace ext */
} /* namespace fe */

#endif /* __datatool_RecordView_h__ */

