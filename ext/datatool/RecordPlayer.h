/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __datatool_RecordPlayer_h__
#define __datatool_RecordPlayer_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Reloads Record's op:input for every handle call that time is stopped

	@ingroup datatool

	Requires the Record to provide a RecordRecorder in op:recorder
	or it will stay dormant.  The RecordRecorder associates filenames
	with the desired time stamps.
*//***************************************************************************/
class FE_DL_EXPORT RecordPlayer: virtual public HandlerI
{
	public:
				RecordPlayer(void):
					m_lastTime(-1.0f)										{}

				//* as HandlerI
virtual void	handle(Record &record);

	private:
		sp<RecorderI>				m_spRecorderI;
		F32							m_lastTime;

		AsOperator					m_asOperator;
		AsTemporal					m_asTemporal;
		Accessor< sp<Component> >	m_aRecorder;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __datatool_RecordPlayer_h__ */
