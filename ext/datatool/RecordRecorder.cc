/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <datatool/datatool.pmh>

#include <math.h>

namespace fe
{
namespace ext
{

void RecordRecorder::handle(Record& record)
{
	if(!m_asOperator.scope().isValid())
	{
		m_asOperator.bind(record.layout()->scope());
		m_asTemporal.bind(record.layout()->scope());
		m_aRecordRecorder.initialize(record.layout()->scope(),
				FE_USE("op:recorder"));

		if(m_aRecordRecorder.check(record))
		{
			m_aRecordRecorder(record)=sp<RecordRecorder>(this);
		}

		System::createDirectory(m_basename.c_str());
	}

	if(!m_asTemporal.timestep.check(record))
	{
		return;
	}

	const Real timestep=m_asTemporal.timestep(record);
	if(timestep==0.0f)
	{
		return;
	}

	if(m_index>=m_ringSize)
	{
		m_index=0;
	}

	String filename;
	filename.sPrintf("%s/record.%d.rg",m_basename.c_str(),m_index);

	std::ofstream outfile(filename.c_str());
	if(!outfile)
	{
		feLog("RecordRecorder::handle could not open output file \"%s\"\n",
				filename.c_str());
	}
	else
	{
		sp<RecordGroup> spOutputGroup=m_asOperator.input(record);

		sp<data::StreamI> spStream(new data::AsciiStream(
				record.layout()->scope()));
		spStream->output(outfile, spOutputGroup);
		outfile.close();

		const Real time=m_asTemporal.time.check(record)?
				m_asTemporal.time(record): 0.0f;
		m_timeStamp[m_index]=time;
	}

	m_index++;
}

String RecordRecorder::findNameFor(Real scalar)
{
	U32 index=0;
	Real best=fabsf(m_timeStamp[0]-scalar);
	for(U32 m=0;m<m_ringSize;m++)
	{
		Real error=fabsf(m_timeStamp[m]-scalar);
		if(error<best)
		{
			index=m;
			best=error;
		}
	}

	String filename;
	filename.sPrintf("%s/record.%d.rg",m_basename.c_str(),index);
	return filename;
}

} /* namespace ext */
} /* namespace fe */
