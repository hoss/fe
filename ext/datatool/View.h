/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __datatool_View_h__
#define __datatool_View_h__

namespace fe
{
namespace ext
{

/**	Base class for generated views
	*/
class View
{
	public:
		View(void)
		{
		}
virtual	~View(void)
		{
		}
		void	bind(Record &a_record)
		{
			m_record = a_record;
		}
		bool	check(sp<Layout> spLayout)
		{
			for(unsigned int i = 0; i < m_accessors.size(); i++)
			{
				if(!m_accessors[i].check(spLayout))
				{
					return false;
				}
			}
			return true;
		}
		bool	check(Record &a_record)
		{
			for(unsigned int i = 0; i < m_accessors.size(); i++)
			{
				if(!m_accessors[i].check(a_record))
				{
					return false;
				}
			}
			return true;
		}
		bool	check(sp<RecordArray> spRA)
		{
			for(unsigned int i = 0; i < m_accessors.size(); i++)
			{
				if(!m_accessors[i].check(spRA))
				{
					return false;
				}
			}
			return true;
		}
		bool	check(void)
		{
			return check(m_record);
		}
	protected:
		Array<BaseAccessor>		m_accessors;
		Record					m_record;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __datatool_View_h__ */

