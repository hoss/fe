/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __shape_Operator_h__
#define __shape_Operator_h__

FE_ATTRIBUTE("op:choice","Record of particular interest during an operation");

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Operator RecordView

	A step in a pipeline.
*//***************************************************************************/
class FE_DL_EXPORT Operator: virtual public RecordView
{
	public:
		Functor< sp<RecordGroup> >		input;
		Functor< sp<RecordGroup> >		output;
		Functor<Record>					choice;
		Functor<I32>					serial;

				Operator(void)		{ setName("Operator"); }
		void	addFunctors(void)
				{
					add(input,	FE_USE("op:input"));
					add(output,	FE_USE("op:output"));
					add(choice,	FE_USE("op:choice"));
					add(serial,	FE_USE(":SN"));
				}
virtual	void	initializeRecord(void)
				{
				}
};

} /* namespace ext */
} /* namespace fe */

#endif /* __shape_Operator_h__ */
