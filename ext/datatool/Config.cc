/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "datatool/datatool.h"

namespace fe
{
namespace ext
{

Config::Config(void)
{
}

Config::~Config(void)
{
}

void Config::configSet(const String &a_key, Instance &a_instance)
{
	m_catalog[a_key] = a_instance;
}

void Config::configSet(const String &a_key, Record &a_record,
		const String &a_attr)
{
	Instance instance;
	if(!a_record.isValid())
	{
		feX("Config::configSet",
			"invalid record");
	}
	if(!a_record.extractInstance(instance, a_attr))
	{
		feX("Config::configSet",
			"could not extract %s from a %s",
			a_attr.c_str(), a_record.layout()->name().c_str());
	}
	m_catalog[a_key] = instance;
}

bool Config::configLookup(const String &a_key, Instance &a_instance)
{
	bool rv = false;
	// lookup local
	std::map<String, Instance>::iterator i_v = m_catalog.find(a_key);
	if(i_v == m_catalog.end())
	{
		// recursive parent check
		if(m_parent.isValid())
		{
			rv = m_parent->configLookup(a_key, a_instance);
		}
		else
		{
			rv = false;
		}
	}
	else
	{
		a_instance = i_v->second;
		rv = true;
	}

	// lookup in the registry catalog
	if(!rv)
	{
		if(registry().isValid() && registry()->master().isValid())
		{
			rv = registry()->master()->catalog()->catalogLookup(
					a_key, a_instance);
		}
	}

	return rv;
}


void Config::configParent(sp<ConfigI> a_parent)
{
	m_parent = a_parent;
}

} /* namespace ext */
} /* namespace fe */

