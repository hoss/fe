/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __datatool_test_xRecordView_h__
#define __datatool_test_xRecordView_h__


using namespace fe;
using namespace fe::ext;

class MyRecordView: public RecordView
{
	private:
	class Print;

	public:
		Functor<F32>				myF32;
		Functor<I32>				myI32;
		Functor< sp<Component> >	myMethod;

				MyRecordView(void)	{ setName("MyRecordView"); }
virtual	void	addFunctors(void)
				{
					add(myF32,		"myF32");
					add(myI32,		"myI32");
					add(myMethod,	"myMethod");
				}
virtual	void	initializeRecord(void)
				{
					myF32()= 4.5f;
					myI32()= -3;
					myMethod()=Library::create<MyRecordView::Print>(
							"MyRecordView::Print");
				}
};

class MyRecordView::Print: public HandlerI
{
	public:
		void	handle(Record& rRecord)
				{
					m_myRecordView.bind(rRecord);
					feLog("MyRecordView::Print::handle myF32=%.6G myI32=%d\n",
							m_myRecordView.myF32(),
							m_myRecordView.myI32());
					m_myRecordView.bind(Record());
				};

	MyRecordView	m_myRecordView;
};

class MyDerivedView: public MyRecordView
{
	public:
		Functor<F32>	moreF32;

				MyDerivedView(void)	{ setName("MyDerivedView"); }
virtual	void	addFunctors(void)
				{
					MyRecordView::addFunctors();

					add(moreF32,"moreF32");
				}
virtual	void	initializeRecord(void)
				{
					MyRecordView::initializeRecord();

					myF32()=7.8f;
					moreF32()=9.1f;
				}
};

#endif // __datatool_test_xRecordView_h__
