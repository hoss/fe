/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "datatool/datatool.h"

#include "datatool/Relay.h"

#include "xRelay.h"
#include "MyView.h"

using namespace fe;
using namespace fe::ext;

static BWORD gs_ok=FALSE;


class MyHandler: public HandlerI
{
	public:
				MyHandler(void)												{}
virtual			~MyHandler(void)											{}

				//* As HandlerI
virtual	void	handle(Record &record)
				{
					m_relay.bind(record);
					m_relay.dump();

					gs_ok=TRUE;
					if(m_relay.get<I32>(MyRelay::e_myI32)!= -5)
						gs_ok=FALSE;
					if(m_relay.get<F32>(MyRelay::e_myF32)!=42.17f)
						gs_ok=FALSE;
				}

	private:
		MyRelay	m_relay;
};


int main(int argc,char** argv)
{
	UNIT_START();

	BWORD complete=FALSE;

	try
	{
		sp<Master> spMaster(new Master);
		sp<Registry> spRegistry=spMaster->registry();

		Result result=spRegistry->manage("fexSignalDL");
		UNIT_TEST(successful(result));

		{
			sp<SignalerI> spSignalerI(spRegistry->create("SignalerI"));
			sp<Scope> spScope(spRegistry->create("Scope"));

			if(!spScope.isValid() || !spSignalerI.isValid())
				feX(argv[0], "can't continue");

			DerivedRelay relay;
			relay.bind(spScope);
			Record record(relay.createRecord());
			relay.bind(record);

			relay.set(MyRelay::e_myI32,-5);
			relay.set(DerivedRelay::e_moreF32,-8.9f);
			relay.access<F32>(DerivedRelay::e_moreF32)=6.7f;

			BWORD check=relay.checkType<I32>(MyRelay::e_myI32);
			UNIT_TEST(check);

#if !FE_RELAY_ASSERT
			BWORD excepted=FALSE;
			check=TRUE;
			try
			{
				check=relay.checkType<F32>(MyRelay::e_myI32);
			}
			catch(Exception &e)	{ excepted=TRUE; }
			UNIT_TEST(excepted || !check);
#endif

			sp<HandlerI> spHandler(Library::create<MyHandler>("MyHandler"));
			spSignalerI->insert(spHandler,relay.layout());

			AccessView a(spScope);
			a.bind(record);
			a.MyF32() = 17.42f;
			a.MyF32(record) = 42.17f;
			UNIT_TEST(a.MyF32(record)==42.17f);

			spSignalerI->signal(record);
			UNIT_TEST(gs_ok);

			Peeker peek;
			peek(*spScope);
			feLog(peek.output().c_str());
			feLog("\n");
		}

		complete=TRUE;
	}
	catch(Exception &e)	{ e.log(); }
	catch(...)			{ feLog("uncaught exception\n"); }

	UNIT_TEST(complete);
	UNIT_TEST(!Counted::trackerCount());
	UNIT_TRACK(8+(FE_RELAY_ASSERT? 0: 1));
	UNIT_RETURN();
}
