/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __datatool_IntegerI_h__
#define __datatool_IntegerI_h__

namespace fe
{
namespace ext
{

class FE_DL_EXPORT IntegerI:
	virtual public Component,
	public CastableAs<IntegerI>
{
	public:
virtual	int		&integer(void)												= 0;
};

} /* namespace ext */
} /* namespace fe */


#endif /* __datatool_IntegerI_h__ */

