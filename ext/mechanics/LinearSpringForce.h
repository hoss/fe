/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __mechanics_LinearSpringForce_h__
#define __mechanics_LinearSpringForce_h__

#include "signal/signal.h"
#include "datatool/datatool.h"
#include "shape/shape.h"
#include "solve/solve.h"
#include "solve/SemiImplicit.h"
#include "mechanicsAS.h"

namespace fe
{
namespace ext
{

class FE_DL_EXPORT LinearSpringForce : public SemiImplicit::Force,
	public CastableAs<LinearSpringForce>
{
	public:
			LinearSpringForce(sp<SemiImplicit> a_integrator);
virtual		~LinearSpringForce(void);
virtual		void clear(void);
virtual		void accumulate(void);
virtual		bool validate(void);

virtual		void compile(	sp<RecordGroup> rg_input,
							std::vector<SemiImplicit::Particle> &a_particles,
							SemiImplicit::CompileMatrix &a_compileMatrix);

virtual		void pairs(		sp<RecordGroup> rg_input,
							SemiImplicit::t_pairs &a_pairs);

	public:
		class FE_DL_EXPORT Spring
		{
			public:
				t_solve_real				m_restlength;
				t_solve_real				m_stiffness;
				t_solve_real				m_damping;
				t_solve_v3					*m_location[2];
				//SpatialVector					*m_prev_location[2];
				//Real							m_distance;
				t_solve_real				m_prev_distance;
				t_solve_v3					*m_velocity[2];
				t_solve_v3					*m_force[2];
				t_solve_matrix				*m_diagonal_dfdx[2];
				t_solve_matrix				*m_diagonal_dfdv[2];
				t_solve_matrix				*m_offdiagonal_dfdx;
				t_solve_matrix				*m_offdiagonal_dfdv;
				unsigned int				m_flags;
		};

		hp<SemiImplicit>		m_integrator;

		std::vector<Spring>		m_springs;

		t_solve_real			m_perturbation;
};

} /* namespace */
} /* namespace */


#endif /* __mechanics_LinearSpringForce_h__ */
