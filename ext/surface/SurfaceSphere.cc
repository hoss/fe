/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <surface/surface.pmh>

namespace fe
{
namespace ext
{

SurfaceSphere::SurfaceSphere(void):
	m_radius(1.0),
	m_shininess(1.0)
{
	set(m_center);
	set(m_emissive);
	set(m_ambient);
	set(m_diffuse);
	set(m_specular);
}

void SurfaceSphere::bind(Record& a_rRecord)
{
	feLog("SurfaceSphere::bind\n");

	m_record=a_rRecord;
//	m_cached=FALSE;
}

Protectable* SurfaceSphere::clone(Protectable* pInstance)
{
	feLog("SurfaceSphere::clone\n");

	SurfaceSphere* pSurfaceSphere= pInstance?
			fe_cast<SurfaceSphere>(pInstance): new SurfaceSphere();

	checkCache();

	pSurfaceSphere->m_center=m_center;
	pSurfaceSphere->m_radius=m_radius;
	return pSurfaceSphere;
}

void SurfaceSphere::cache(void)
{
	if(!m_record.isValid())
	{
#if FE_CODEGEN<=FE_DEBUG
//		feLog("SurfaceSphere::cache record invalid\n");
#endif
		return;
	}

	Sphere sphereRV;
	sphereRV.bind(m_record);

	m_radius=sphereRV.radius();
	m_center=sphereRV.location();

	Material materialRV;
	materialRV.bind(m_record);

	m_emissive=materialRV.emissive();
	m_ambient=materialRV.ambient();
	m_diffuse=materialRV.diffuse();
	m_specular=materialRV.specular();
	m_shininess=materialRV.shininess();

//	feLog("SurfaceSphere::cache radius %.6G location %s\n",
//			m_radius,c_print(m_center));
}

SpatialTransform SurfaceSphere::sample(Vector2 a_uv) const
{
	SpatialTransform transform;

	//* TODO
	setIdentity(transform);

	return transform;
}

void SurfaceSphere::draw(const SpatialTransform& a_rTransform,
	sp<DrawI> a_spDrawI,const fe::Color* a_color,
	sp<DrawBufferI> a_spDrawBuffer,
	sp<PartitionI> a_spPartition) const
{
	const SpatialVector scale(m_radius,m_radius,m_radius);
	SpatialTransform transform=a_rTransform;
	translate(transform,m_center);

	a_spDrawI->drawSphere(transform,&scale,a_color? *a_color: m_diffuse);
}

} /* namespace ext */
} /* namespace fe */
