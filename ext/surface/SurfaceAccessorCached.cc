/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <surface/surface.pmh>

namespace fe
{
namespace ext
{

SurfaceAccessorCached::SurfaceAccessorCached(void):
	m_attrType(e_none),
	m_count(0),
	m_size(0)
{
	setName("SurfaceAccessorCached");

#if FE_SACH_DEBUG
	feLog("SurfaceAccessorCached\n");
#endif
}

SurfaceAccessorCached::~SurfaceAccessorCached(void)
{
#if FE_SACH_DEBUG
	feLog("~SurfaceAccessorCached type %d\n",m_attrType);
#endif

	if(m_spAccessorChain.isNull())
	{
		return;
	}

	m_spSurfaceAccessibleI->lock();

	if(m_attrType==e_string)
	{
		for(U32 index=0;index<m_count;index++)
		{
			if(m_changed[index])
			{
#if FE_SACH_USE_INSTANCE
				m_spAccessorChain->set(index,m_value[index].cast<String>());
#else
				m_spAccessorChain->set(index,m_string[index]);
#endif
			}
		}
	}
	else if(m_attrType==e_integer)
	{
		for(U32 index=0;index<m_count;index++)
		{
			if(m_changed[index])
			{
#if FE_SACH_USE_INSTANCE
				m_spAccessorChain->set(index,m_value[index].cast<I32>());
#else
				m_spAccessorChain->set(index,m_integer[index]);
#endif
			}
		}
	}
	else if(m_attrType==e_real)
	{
		for(U32 index=0;index<m_count;index++)
		{
			if(m_changed[index])
			{
#if FE_SACH_USE_INSTANCE
				m_spAccessorChain->set(index,m_value[index].cast<Real>());
#else
				m_spAccessorChain->set(index,m_real[index]);
#endif
			}
		}
	}
	else if(m_attrType==e_spatialVector)
	{
		if(m_element==SurfaceAccessibleI::e_primitiveGroup ||
				m_attribute==SurfaceAccessibleI::e_vertices)
		{
			for(U32 index=0;index<m_count;index++)
			{
				if(m_changed[index])
				{
#if FE_SACH_USE_INSTANCE
					m_spAccessorChain->set(m_integer[index],m_subIndex[index],
							m_value[index].cast<SpatialVector>());
#else
					m_spAccessorChain->set(m_integer[index],m_subIndex[index],
							m_spatialVector[index]);
#endif
				}
			}
		}
		else
		{
			for(U32 index=0;index<m_count;index++)
			{
				if(m_changed[index])
				{
#if FE_SACH_USE_INSTANCE
					m_spAccessorChain->set(index,
							m_value[index].cast<SpatialVector>());
#else
					m_spAccessorChain->set(index,m_spatialVector[index]);
#endif
				}
			}
		}
	}

	m_spSurfaceAccessibleI->unlock();
}

} /* namespace ext */
} /* namespace fe */