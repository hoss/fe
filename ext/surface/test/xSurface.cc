/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "surface/surface.h"
#include "viewer/viewer.h"

#define	SHOW_NEAREST		TRUE
#define	TRACK_AUTO_CAMERA	FALSE

using namespace fe;
using namespace fe::ext;

class MyDraw:
	virtual public HandlerI,
	public CastableAs<MyDraw>
{
	public:
				MyDraw(void):
					m_stepIndex(0)											{}

		void	bind(sp<Master> spMaster)
				{
					sp<Scope> spSimScope=
							spMaster->catalog()->catalogComponent(
							"Scope","SimScope");
					String longname=spMaster->catalog()->catalog<String>(
							"path:media")+"/"+name();
					feLog("Trying to load %s\n",longname.c_str());
					m_spSurfaceGroup=
							RecordView::loadRecordGroup(spSimScope,longname);
					if(!m_spSurfaceGroup.isValid())
					{
						longname=spMaster->catalog()->catalog<String>(
								"path:media")+"/surface/"+name()+".rg";
						feLog("Retrying as %s\n",longname.c_str());
						m_spSurfaceGroup=RecordView::loadRecordGroup(
								spSimScope,longname);
					}
					m_surfaceModelRAV.bind(spSimScope);

					//* template test: load into alternate scope (not drawn)
					sp<Scope> spAltScope=spMaster->catalog()->catalogComponent(
							"Scope","AltScope");
					longname=spMaster->catalog()->catalog<String>(
							"path:media")+"/surface/sphere.rg";
					RecordView::loadRecordGroup(spAltScope,longname);
				}

		void	bind(sp<DrawI> spDrawI)
				{
					m_spDrawI=spDrawI;
				}

		void	bind(sp<CameraControllerI> spCameraControllerI)
				{
					m_spCameraControllerI=spCameraControllerI;
					m_spCameraControllerI->setLookAngle(80.0,20.0,100.0f);
				}

virtual void	handle(Record& render)
				{
					if(!m_asViewer.scope().isValid())
					{
						m_asViewer.bind(render.layout()->scope());
					}
					if(m_asViewer.viewer_layer.check(render) &&
							m_asViewer.viewer_spatial.check(render))
					{
						const I32& rPerspective=
								m_asViewer.viewer_spatial(render);
						if(rPerspective==1)
						{
							m_spDrawI->drawAxes(5.0f);

#if FALSE
							const Color white(1.0f,1.0f,1.0f,1.0f);
							const SpatialVector scale(3.0f,3.0f,3.0f);
							SpatialTransform transform;
							setIdentity(transform);
							m_spDrawI->drawSphere(transform,&scale,white);
							return;
#endif

#if	TRACK_AUTO_CAMERA
							BWORD first=TRUE;
#endif
							FEASSERT(m_spSurfaceGroup.isValid());
							for(RecordGroup::iterator it=
									m_spSurfaceGroup->begin();
									it!=m_spSurfaceGroup->end();it++)
							{
								sp<RecordArray> spRA= *it;
								m_surfaceModelRAV.bind(spRA);

								if(!m_surfaceModelRAV.recordView()
										.component.check(spRA))
								{
									continue;
								}

								for(SurfaceModel& surfaceModelRV:
										m_surfaceModelRAV)
								{
									sp<Component> spComponent=
											surfaceModelRV.component();
									m_spDrawI->draw(spComponent,NULL);

#if	SHOW_NEAREST
									const Color red(1.0f,0.0f,0.0f,1.0f);
									const Color grey(0.7f,0.7f,0.7f,1.0f);
									const Color yellow(1.0f,1.0f,0.0f,1.0f);
									const SpatialVector center(0.0f,0.0f,0.0f);
									const Real width=2.0;//1.0;
									const Real inc=1.0;//0.5;
									const Real radial=8.0;
									const Real angleInc=0.01;

									cp<SurfaceI> cpSurfaceI=
											surfaceModelRV.component();
									if(cpSurfaceI.isValid())
									{
										static Real angle=0.0;
										const SpatialVector offset=
												center+radial*SpatialVector(
												cos(angle),sin(angle));

										const BWORD paused=
												m_spCameraControllerI->keyCount(
												'p');
										const BWORD forward=
												m_spCameraControllerI->keyCount(
												WindowEvent::e_keyCursorRight);
										const BWORD reverse=
												m_spCameraControllerI->keyCount(
												WindowEvent::e_keyCursorLeft);
										if(!paused || forward)
										{
											angle+=angleInc;
										}
										else if(reverse)
										{
											angle-=angleInc;
										}
										m_spCameraControllerI->setKeyCount(
												WindowEvent::e_keyCursorLeft,0);
										m_spCameraControllerI->setKeyCount(
												WindowEvent::e_keyCursorRight,
												0);
										const Real halfWidth=0.5*width;
										SpatialVector vertex[2];

										for(Real x= -halfWidth;
												x<=halfWidth+0.01;x+=inc)
										{
											for(Real y= -halfWidth;
													y<=halfWidth+0.01;y+=inc)
											{
												set(vertex[0],offset[0]+x,
														offset[1]+y,offset[2]);
												sp<SurfaceI::ImpactI> spImpactI=
														cpSurfaceI->
														nearestPoint(vertex[0]);
												if(spImpactI.isValid())
												{
													vertex[1]=spImpactI->
															intersection();
													m_spDrawI->drawLines(
															vertex,NULL,2,
															DrawI::e_discrete,
															FALSE,&grey);

													m_spDrawI->drawPoints(
															vertex,NULL,2,
															FALSE,&yellow);

													vertex[0]=vertex[1]+
															spImpactI->
															normal();
													m_spDrawI->drawLines(
															vertex,NULL,2,
															DrawI::e_discrete,
															FALSE,&red);
												}
											}
										}
									}
#endif
#if	TRACK_AUTO_CAMERA
									if(!first)
									{
										continue;
									}
									cp<SurfaceI> cpSurfaceI=
											surfaceModelRV.component();
									if(cpSurfaceI.isValid())
									{
										const U32 steps=300;
										const Vector2 uv(
												m_stepIndex/Real(steps),0.5f);
										const SpatialVector& rTranslation=
												cpSurfaceI->samplePoint(uv);
										m_spCameraControllerI->setLookPoint(
												rTranslation[0],
												rTranslation[1],
												rTranslation[2]);

										m_stepIndex=(m_stepIndex+1)%steps;
										first=FALSE;
									}
#endif
								}
							}
						}
					}
				}

	private:
		RecordArrayView<SurfaceModel>	m_surfaceModelRAV;
		AsViewer						m_asViewer;
		sp<RecordGroup>					m_spSurfaceGroup;
		sp<DrawI>						m_spDrawI;
		sp<CameraControllerI>			m_spCameraControllerI;
		U32								m_stepIndex;
};

int main(int argc,char** argv)
{
	if(argc<2)
	{
		feLog("%s: No surface filename specified\n\n"
				"Try: %s <filename> [draw] [frames]\n\n",
				argv[0],argv[0]);
		return 1;
	}

	UNIT_START();

	BWORD complete=FALSE;
	U32 tests=4;

	const char* filename=argv[1];
	String componentName="";
	U32 frames=0;
	if(argc>3)
	{
		componentName=argv[2];
		frames=atoi(argv[3]);
	}
	else if(argc>2)
	{
		frames=atoi(argv[2]);
		if(frames==0)
		{
			componentName=argv[2];
		}
	}

	try
	{
		sp<Master> spMaster(new Master);
		sp<Registry> spRegistry=spMaster->registry();

		Result result=spRegistry->manage("fexViewerDL");
		UNIT_TEST(successful(result));
		result=spRegistry->manage("fexSurfaceDL");
		UNIT_TEST(successful(result));

		result=spRegistry->manage("fexTerrainDL");
		if(!successful(result))
		{
			feLog("Failed to load terrain extensions\n");
		}

		result=spRegistry->manage("fexOSGDL");
		if(!successful(result))
		{
			feLog("Failed to load OSG support\n");
		}

		{
#if TRUE
			spMaster->catalog()->catalog<String>("hint:DrawI")="*.DrawCached";
#endif

			sp<QuickViewerI> spQuickViewerI(
					spRegistry->create("QuickViewerI"));
			if(!spQuickViewerI.isValid())
			{
				feX(argv[0], "couldn't create components");
			}

			spQuickViewerI->open();

			sp<DrawI> spDrawI;
			if(!componentName.empty())
			{
				spDrawI=spRegistry->create(componentName);
				if(!spDrawI.isValid())
				{
					result=spRegistry->manage("fexRayDL");
					UNIT_TEST(successful(result));
					tests++;

					spDrawI=spRegistry->create(componentName);
				}
				UNIT_TEST(spDrawI.isValid());
				tests++;
				spQuickViewerI->bind(spDrawI);
			}
			if(!spDrawI.isValid())
			{
				spDrawI=spQuickViewerI->getDrawI();
			}
			if(spDrawI.isNull())
			{
				feX(argv[0], "couldn't get draw interface");
			}

			sp<DrawMode> spDrawMode=spDrawI->drawMode();
			if(spDrawMode.isValid())
			{
				spDrawMode->setLineWidth(1.5f);
				spDrawMode->setPointSize(8.0f);
//				spDrawMode->setDrawStyle(DrawMode::e_wireframe);
			}

			sp<MyDraw> spMyDraw(Library::create<MyDraw>("MyDraw"));
			spMyDraw->setName(filename);
			spMyDraw->bind(spMaster);
			spMyDraw->bind(spDrawI);
			spMyDraw->bind(spQuickViewerI->getCameraControllerI());

			sp<HandlerI> spHandlerI(spDrawI);
			if(spHandlerI.isValid())
			{
				spQuickViewerI->insertEventHandler(spDrawI);
			}

			spQuickViewerI->insertDrawHandler(spMyDraw);

			feLog("running %d frames\n",frames);
			spQuickViewerI->run(frames);

			sp<Scope> spSimScope=
					spMaster->catalog()->catalogComponent("Scope","SimScope");
			Peeker peeker;
			spSimScope->peek(peeker);
			feLog("\nSim Scope:%s\n",peeker.output().c_str());

			sp<Scope> spAltScope=
					spMaster->catalog()->catalogComponent("Scope","AltScope");
			peeker.clear();
			spAltScope->peek(peeker);
			feLog("\nAlt Scope:%s\n",peeker.output().c_str());
		}

		complete=TRUE;
	}
	catch(Exception &e) { e.log(); }
	catch(...)			{ feLog("uncaught exception\n"); }

	UNIT_TEST(complete);
	UNIT_TEST(!Counted::trackerCount());
	UNIT_TRACK(tests);
	UNIT_RETURN();
}
