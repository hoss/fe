/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "surface/surface.h"
#include "geometry/geometry.h"

using namespace fe;
using namespace fe::ext;

int main(int argc,char** argv)
{
	UNIT_START();

	BWORD complete=FALSE;

	try
	{
		sp<Master> spMaster(new Master);
		sp<Registry> spRegistry=spMaster->registry();

		{
			const SpatialVector center(10.0f,10.0f,10.0f);
			const Real radius(2.0f);
			const SpatialVector origin(0.0f,9.0f,0.0f);
			const SpatialVector direction(0.707f,0.0f,0.707f);

			RaySphereIntersect<Real> raySphereIntersect;
			Real range=raySphereIntersect.solve(center,radius,
					origin,direction);

			SpatialVector intersection=origin+direction*range;
			feLog("sphere range %.6G at %s\n",
					range,print(intersection).c_str());

			SpatialVector expected(8.785f,9.0f,8.785f);
			Real expected_range=12.426f;
			const Real margin=1e-3f;
			UNIT_TEST(fabs(range-expected_range)<margin);
			UNIT_TEST(equivalent(intersection,expected,margin));

			PointSphereNearest<Real> pointSphereNearest;
			SpatialVector toward;
			range=pointSphereNearest.solve(center,radius,origin,toward);

			intersection=origin+toward*range;
			feLog("sphere nearest %.6G toward %s at %s\n",
					range,print(toward).c_str(),
					print(intersection).c_str());

			set(expected,8.589f,9.859f,8.589f);
			expected_range=12.177f;
			UNIT_TEST(fabs(range-expected_range)<margin);
			UNIT_TEST(equivalent(intersection,expected,margin));

			const SpatialVector span(1.0f,0.0f,0.0f);

			RayDiskIntersect<Real> rayDiskIntersect;
			range=rayDiskIntersect.solve(center,span,radius,
					origin,direction,intersection);

			feLog("disk range %.6G at %s\n",
					range,print(intersection).c_str());

			set(expected,10.0,9.0f,10.0f);
			expected_range=14.144f;
			UNIT_TEST(fabs(range-expected_range)<margin);
			UNIT_TEST(equivalent(intersection,expected,margin));

			PointDiskNearest<Real> pointDiskNearest;
			range=pointDiskNearest.solve(center,span,radius,origin,toward,
					intersection);

			feLog("disk nearest %.6G toward %s at %s\n",
					range,print(toward).c_str(),
					print(intersection).c_str());

			set(expected,10.0,9.801f,8.010f);
			expected_range=12.838f;
			UNIT_TEST(fabs(range-expected_range)<margin);
			UNIT_TEST(equivalent(intersection,expected,margin));

			const SpatialVector base(10.0f,8.0f,10.0f);
			const SpatialVector axis(0.0f,2.0f,0.0f);

			RayCylinderIntersect<Real> rayCylinderIntersect;
			Real along;
			range=rayCylinderIntersect.solve(base,axis,radius,
					origin,direction,along,intersection);

			feLog("cylinder range %.6G at %s\n",
					range,print(intersection).c_str());

			set(expected,8.586,9.0f,8.586f);
			expected_range=12.144;
			UNIT_TEST(fabs(range-expected_range)<margin);
			UNIT_TEST(equivalent(intersection,expected,margin));

			PointCylinderNearest<Real> pointCylinderNearest;
			range=pointCylinderNearest.solve(center,span,radius,origin,toward,
					along,intersection);

			feLog("cylinder nearest %.6G toward %s\n  at %s along %.6G\n",
					range,print(toward).c_str(),
					print(intersection).c_str(),along);

			set(expected,10.0,9.801f,8.010f);
			expected_range=12.838f;
			UNIT_TEST(fabs(range-expected_range)<margin);
			UNIT_TEST(equivalent(intersection,expected,margin));
		}

		complete=TRUE;
	}
	catch(Exception &e) { e.log(); }
	catch(...)			{ feLog("uncaught exception\n"); }

	UNIT_TEST(complete);
	UNIT_TEST(!Counted::trackerCount());
	UNIT_TRACK(14);
	UNIT_RETURN();
}
