/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <surface/surface.pmh>

#define FE_SAJ_DEBUG				FALSE

#define FE_SAJ_UNIFORM_TRIANGLES	FALSE
#define FE_SAJ_UNIFORM_SCALE		0.1

namespace fe
{
namespace ext
{

void SurfaceAccessibleJoint::reset(void)
{
	SurfaceAccessibleBase::reset();

	m_frame=0.0;
	m_spFilter=NULL;

	resetJoints();
}

void SurfaceAccessibleJoint::resetJoints(void)
{
#if FALSE
	if(m_spScope.isNull())
	{
		m_spScope=registry()->create("Scope");
		m_spScope->setLocking(FALSE);
	}

	SurfaceAccessibleRecord::reset();
#else
	SurfaceAccessibleCatalog::reset();
#endif

	m_nameList.clear();
	m_parentMap.clear();
	m_colorMap.clear();
	m_targetMap.clear();
	m_childMap.clear();
	m_childrenMap.clear();
	m_leafMap.clear();

	m_refMap.clear();
	m_refTweakMap.clear();
	m_animMap.clear();
	m_animTweakMap.clear();
	m_animScaleMap.clear();
}

void SurfaceAccessibleJoint::clearAccessors(void)
{
	m_spOutputPoint=NULL;
	m_spOutputColor=NULL;
	m_spOutputNormal=NULL;
	m_spOutputUV=NULL;
	m_spOutputPart=NULL;
	m_spOutputRadius=NULL;
	m_spOutputName=NULL;
	m_spOutputParent=NULL;
	m_spOutputRefX=NULL;
	m_spOutputRefY=NULL;
	m_spOutputRefZ=NULL;
	m_spOutputRefT=NULL;
	m_spOutputAnimX=NULL;
	m_spOutputAnimY=NULL;
	m_spOutputAnimZ=NULL;
	m_spOutputAnimT=NULL;
	m_spOutputAnimS=NULL;
	m_spOutputVertices=NULL;
	m_spOutputProperties=NULL;
}

void SurfaceAccessibleJoint::setOptions(String a_options)
{
#if FE_SAJ_DEBUG
	feLog("SurfaceAccessibleJoint::setOptions \"%s\"\n",a_options.c_str());
#endif

	m_optionMap.clear();

	String buffer=a_options;
	String option;
	while(!(option=buffer.parse()).empty())
	{
		const String property=option.parse("\"","=");
		const String value=option.parse("\"","=");

#if FE_SAJ_DEBUG
		feLog("SurfaceAccessibleJoint::setOptions parse \"%s\" \"%s\"\n",
				property.c_str(),value.c_str());
#endif

		m_optionMap[property]=value;
	}
}

void SurfaceAccessibleJoint::startJoints(void)
{
	resetJoints();

	if(m_optionMap.find("filter")!=m_optionMap.end())
	{
		const String filterFile=m_optionMap["filter"];

		if(!filterFile.empty())
		{
#if FE_SAJ_DEBUG
			feLog("SurfaceAccessibleJoint::startJoints use filter \"%s\"\n",
					filterFile.c_str());
#endif
			const String filterFormat=
				String(strrchr(filterFile.c_str(),'.')).prechop(".");

			sp<StringFilterI> spStringFilter=
					registry()->create("StringFilterI.*.*."+filterFormat);
			if(spStringFilter.isValid())
			{
				if(m_optionMap.find("spec")!=m_optionMap.end())
				{
					const String filterSpec=m_optionMap["spec"];
					spStringFilter->configure(filterSpec);
				}
				if(!spStringFilter->load(filterFile))
				{
					spStringFilter=NULL;
				}
			}

			if(spStringFilter.isValid())
			{
				m_spFilter=spStringFilter;
			}
		}
	}

	const SurfaceAccessibleI::Creation creation=
			SurfaceAccessibleI::e_createMissing;

	m_spOutputPoint=accessor(e_point,e_position,creation);
	m_spOutputColor=accessor(e_point,e_color,creation);
	m_spOutputNormal=accessor(e_point,e_normal,creation);
	m_spOutputName=accessor(e_primitive,"name",creation);
	m_spOutputParent=accessor(e_primitive,"parent",creation);
	m_spOutputRefX=accessor(e_primitive,"refX",creation);
	m_spOutputRefY=accessor(e_primitive,"refY",creation);
	m_spOutputRefZ=accessor(e_primitive,"refZ",creation);
	m_spOutputRefT=accessor(e_primitive,"refT",creation);
	m_spOutputAnimX=accessor(e_primitive,"animX",creation);
	m_spOutputAnimY=accessor(e_primitive,"animY",creation);
	m_spOutputAnimZ=accessor(e_primitive,"animZ",creation);
	m_spOutputAnimT=accessor(e_primitive,"animT",creation);
	m_spOutputAnimS=accessor(e_primitive,"animS",creation);
	m_spOutputVertices=accessor(e_primitive,e_vertices,creation);
	m_spOutputProperties=accessor(e_primitive,e_properties,creation);
}

BWORD SurfaceAccessibleJoint::setJoint(
	String a_name,String a_parentName,
	const SpatialTransform& a_rXformRelative,
	const SpatialVector& a_rScaleLocal,
	const SpatialTransform& a_rRefWorld,Color a_color)
{
#if FE_SAJ_DEBUG
	feLog("SurfaceAccessibleJoint::setJoint \"%s\" parent \"%s\"\n",
			a_name.c_str(),a_parentName.c_str());
	feLog("  rel\n%s\n",c_print(a_rXformRelative));
#endif

	SpatialTransform animParent;
	SpatialVector scaleParent;

	const BWORD hasParent=(m_animMap.find(a_parentName)!=m_animMap.end());
	if(hasParent)
	{
		animParent=m_animMap[a_parentName];
		scaleParent=m_animScaleMap[a_parentName];
	}
	else
	{
		setIdentity(animParent);
		set(scaleParent,1.0,1.0,1.0);
	}

	SpatialTransform xformRelative=a_rXformRelative;
	SpatialVector& rTranslation=xformRelative.translation();
	rTranslation[0]*=scaleParent[0];
	rTranslation[1]*=scaleParent[1];
	rTranslation[2]*=scaleParent[2];

	const SpatialTransform animWorld=xformRelative*animParent;

#if FE_SAJ_DEBUG
	feLog("  animWorld\n%s\n",c_print(animWorld));
#endif

	m_refMap[a_name]=a_rRefWorld;
	m_animMap[a_name]=animWorld;
	m_animScaleMap[a_name]=a_rScaleLocal;

//	feLog("SurfaceAccessibleJoint::setJoint filter \"%s\" result \"%s\"\n",
//			a_name.c_str(),
//			m_spFilter.isValid()? m_spFilter->filter(a_name).c_str(): "INVALID")

	String reParent=a_parentName;
	while(!reParent.empty() &&
		m_colorMap.find(reParent)==m_colorMap.end())
	{
		reParent=m_parentMap[reParent];
	}

	if(m_leafMap.find(reParent)==m_leafMap.end())
	{
		//* no parent leaf entry

		m_targetMap[reParent]=a_rRefWorld.translation();
		m_childrenMap[reParent]=1;
		m_leafMap[reParent]=TRUE;
	}
	else if(m_leafMap[reParent])
	{
		//* parent is a leaf

		m_targetMap[reParent]+=a_rRefWorld.translation();
		m_childrenMap[reParent]++;
	}

	if(m_spFilter.isValid() && m_spFilter->filter(a_name).empty())
	{
		m_refTweakMap[a_name]=a_rRefWorld;
		m_animTweakMap[a_name]=animWorld;
		m_parentMap[a_name]=a_parentName;
		return FALSE;
	}

	m_nameList.push_back(a_name);
	m_colorMap[a_name]=a_color;

	m_parentMap[a_name]=reParent;
	m_childMap[reParent].push_back(a_name);

	if(m_targetMap.find(reParent)==m_targetMap.end() || m_leafMap[reParent])
	{
		//* parent's first unfiltered child

		m_targetMap[reParent]=a_rRefWorld.translation();
		m_childrenMap[reParent]=1;
		m_leafMap[reParent]=FALSE;
	}
	else
	{
		//* parent's additional unfiltered child

		m_targetMap[reParent]+=a_rRefWorld.translation();
		m_childrenMap[reParent]++;
	}

	return TRUE;
}

void SurfaceAccessibleJoint::completeJoints(void)
{
	const U32 jointCount=m_nameList.size();
	for(U32 jointIndex=0;jointIndex<jointCount;jointIndex++)
	{
		const String& jointName=m_nameList[jointIndex];
		const String& parentName=m_parentMap[jointName];

		const SpatialTransform& refWorld=m_refMap[jointName];
		const SpatialTransform& animWorld=m_animMap[jointName];

		SpatialTransform invRefWorld;
		invert(invRefWorld,refWorld);

		const SpatialTransform delta=invRefWorld*animWorld;

		const BWORD hasParent=(m_animMap.find(parentName)!=m_animMap.end());

		if(!hasParent)
		{
			m_refTweakMap[jointName]=refWorld;
			m_animTweakMap[jointName]=animWorld;

			const Real rootLength=1.0;

			addJoint(jointName,parentName,animWorld,animWorld,
					m_animScaleMap[jointName],refWorld,
					rootLength,m_colorMap[jointName]);

			continue;
		}

		const SpatialTransform xformParentRefTweak=m_refTweakMap[parentName];
		const SpatialTransform xformParentTweak=m_animTweakMap[parentName];

		//* find local tweak in reference

		SpatialTransform invParentRefTweak;
		invert(invParentRefTweak,xformParentRefTweak);

		SpatialTransform refTweak=xformParentRefTweak;

		SpatialVector toWorld;
		if(m_targetMap.find(jointName)==m_targetMap.end())
		{
			toWorld=refWorld.translation()-xformParentRefTweak.translation();
		}
		else
		{
			toWorld=m_targetMap[jointName]/m_childrenMap[jointName]-
					refWorld.translation();
		}

		const SpatialVector toLocal=rotateVector(invParentRefTweak,toWorld);
		const Real localLength=magnitude(toLocal);
		if(localLength>1e-3)
		{
			const SpatialVector fromLocal(0.0,1.0,0.0);

			SpatialQuaternion quatLocal;
			set(quatLocal,fromLocal,toLocal/localLength);

			const SpatialTransform localTweak(quatLocal);

			refTweak=localTweak*refTweak;
		}
		setTranslation(refTweak,refWorld.translation());

		SpatialTransform animTweak=refTweak*delta;
		setTranslation(animTweak,m_animMap[jointName].translation());

		m_refTweakMap[jointName]=refTweak;
		m_animTweakMap[jointName]=animTweak;

		SpatialTransform invParentTweak;
		invert(invParentTweak,xformParentTweak);

		const SpatialTransform xformLocal=animTweak*invParentTweak;
		const SpatialTransform xformLocalRef=refTweak*invParentRefTweak;

		addJoint(jointName,parentName,xformLocal,animTweak,
				m_animScaleMap[jointName],xformLocalRef,
				localLength,m_colorMap[jointName]);
	}

	clearAccessors();
}

BWORD SurfaceAccessibleJoint::addJoint(String a_name,String a_parentName,
	const SpatialTransform& a_rAnimLocal,
	const SpatialTransform& a_rAnimWorld,
	const SpatialVector& a_rScaleLocal,
	const SpatialTransform& a_rRefLocal,
	Real a_length,Color a_color)
{
	const BWORD success=addJoint(a_name,a_parentName,
			a_rAnimLocal,a_rAnimWorld,a_rScaleLocal,a_length,a_color);

	if(success)
	{
		const I32 primitiveIndex=m_spOutputVertices->count()-1;

		m_spOutputRefX->set(primitiveIndex,a_rRefLocal.column(0));
		m_spOutputRefY->set(primitiveIndex,a_rRefLocal.column(1));
		m_spOutputRefZ->set(primitiveIndex,a_rRefLocal.column(2));
		m_spOutputRefT->set(primitiveIndex,a_rRefLocal.translation());
	}

	return success;
}

BWORD SurfaceAccessibleJoint::addJoint(String a_name,String a_parentName,
	const SpatialTransform& a_rAnimLocal,
	const SpatialTransform& a_rAnimWorld,
	const SpatialVector& a_rScaleLocal,
	Real a_length,Color a_color)
{
#if FE_SAJ_DEBUG
	feLog("SurfaceAccessibleJoint::addJoint \"%s\" parent \"%s\"\n",
			a_name.c_str(),a_parentName.c_str());
	feLog("%s\n",c_print(a_rAnimLocal));
#endif
	const Real minBoneLength=0.1;

	const SpatialVector point=a_rAnimWorld.translation();
	const SpatialVector side=a_rAnimWorld.column(0);
	const SpatialVector direction=a_rAnimWorld.column(1);
//	const SpatialVector norm=a_rAnimWorld.column(2);

	I32 primitiveIndex=0;

	//* check for 'line' in the spec
	if(m_optionMap.find("line")!=m_optionMap.end())
	{
		const std::vector<String>& rChildNames=m_childMap[a_name];
		const I32 childCount=I32(rChildNames.size());

		//* one bone per child
		const I32 boneCount=childCount? childCount: 1;

		const I32 pointCount=13*boneCount;
		primitiveIndex=m_spOutputVertices->append(pointCount,
				SurfaceAccessibleI::e_open);

		I32 subIndex=0;

		I32 pointIndex[13];

		for(I32 childIndex=0;childIndex<boneCount;childIndex++)
		{
			SpatialVector childPoint;
			if(childCount)
			{
				const String& rChildname=rChildNames[childIndex];
				childPoint=m_animMap[rChildname].translation();
			}
			else
			{
				childPoint=point+a_rScaleLocal[1]*a_length*direction;
			}

			const Real length=magnitude(childPoint-point);
			if(length<minBoneLength)
			{
				childPoint=point+minBoneLength*unitSafe(direction);
			}

			const SpatialVector along=unitSafe(childPoint-point);

			SpatialTransform frame;
			makeFrameNormalY(frame,point,side,along);

			const Real radius=fe::maximum(0.5*minBoneLength,
					0.04*magnitude(childPoint-point));
			const SpatialVector midPoint=point+radius*along;
			const SpatialVector unitSide=unitSafe(frame.column(0));
			const SpatialVector unitNorm=unitSafe(frame.column(2));
			const SpatialVector mid0=midPoint+radius*unitSide;
			const SpatialVector mid1=midPoint+radius*unitNorm;
			const SpatialVector mid2=midPoint-radius*unitSide;
			const SpatialVector mid3=midPoint-radius*unitNorm;

#if FALSE
			feLog("%d %d/%d radius %.6G\n",
					primitiveIndex,childIndex,childCount,radius);
			feLog("point %s childPoint %s\n",
					c_print(point),c_print(childPoint));
			feLog("mid0 %s mid1 %s\n",c_print(mid0),c_print(mid1));
			feLog("mid2 %s mid3 %s\n",c_print(mid2),c_print(mid3));
#endif

			for(I32 index=0;index<13;index++)
			{
				pointIndex[index]=
						m_spOutputVertices->integer(primitiveIndex,subIndex++);
				m_spOutputColor->set(pointIndex[index],a_color);
			}

			m_spOutputPoint->set(pointIndex[0],point);
			m_spOutputPoint->set(pointIndex[1],mid0);
			m_spOutputPoint->set(pointIndex[2],childPoint);
			m_spOutputPoint->set(pointIndex[3],mid2);
			m_spOutputPoint->set(pointIndex[4],mid1);
			m_spOutputPoint->set(pointIndex[5],childPoint);
			m_spOutputPoint->set(pointIndex[6],mid3);
			m_spOutputPoint->set(pointIndex[7],mid0);
			m_spOutputPoint->set(pointIndex[8],mid1);
			m_spOutputPoint->set(pointIndex[9],point);
			m_spOutputPoint->set(pointIndex[10],mid2);
			m_spOutputPoint->set(pointIndex[11],mid3);
			m_spOutputPoint->set(pointIndex[12],point);

			m_spOutputNormal->set(pointIndex[0],-along);
			m_spOutputNormal->set(pointIndex[1],unitSide);
			m_spOutputNormal->set(pointIndex[2],along);
			m_spOutputNormal->set(pointIndex[3],-unitSide);
			m_spOutputNormal->set(pointIndex[4],unitNorm);
			m_spOutputNormal->set(pointIndex[5],along);
			m_spOutputNormal->set(pointIndex[6],-unitNorm);
			m_spOutputNormal->set(pointIndex[7],unitSide);
			m_spOutputNormal->set(pointIndex[8],unitNorm);
			m_spOutputNormal->set(pointIndex[9],-along);
			m_spOutputNormal->set(pointIndex[10],-unitSide);
			m_spOutputNormal->set(pointIndex[11],-unitNorm);
			m_spOutputNormal->set(pointIndex[12],-along);
		}
	}
	else
	{
		//* transform triangle

		const Color red(1.0,0.0,0.0);
		const Color green(0.0,1.0,0.0);
		const Color blue(0.0,0.0,1.0);

		//* TODO param
		const Real triScale=1.0;

		primitiveIndex=m_spOutputVertices->append(3);

		const I32 pointIndex0=m_spOutputVertices->integer(primitiveIndex,0);
		const I32 pointIndex1=m_spOutputVertices->integer(primitiveIndex,1);
		const I32 pointIndex2=m_spOutputVertices->integer(primitiveIndex,2);

		m_spOutputPoint->set(pointIndex0,point);
#if FE_SAJ_UNIFORM_TRIANGLES
		m_spOutputPoint->set(pointIndex1,
				point+triScale*FE_SAJ_UNIFORM_SCALE*
				unitSafe(a_rAnimWorld.column(0)));
		m_spOutputPoint->set(pointIndex2,
				point+triScale*FE_SAJ_UNIFORM_SCALE*
				unitSafe(a_rAnimWorld.column(2)));
#else
		m_spOutputPoint->set(pointIndex1,point+triScale*a_rAnimWorld.column(0));
		m_spOutputPoint->set(pointIndex2,point+triScale*a_rAnimWorld.column(2));
#endif

		m_spOutputColor->set(pointIndex0,a_color);
		m_spOutputColor->set(pointIndex1,red);
		m_spOutputColor->set(pointIndex2,blue);

		m_spOutputNormal->set(pointIndex0,direction);
		m_spOutputNormal->set(pointIndex1,direction);
		m_spOutputNormal->set(pointIndex2,direction);
	}

	m_spOutputAnimX->set(primitiveIndex,a_rAnimLocal.column(0));
	m_spOutputAnimY->set(primitiveIndex,a_rAnimLocal.column(1));
	m_spOutputAnimZ->set(primitiveIndex,a_rAnimLocal.column(2));
	m_spOutputAnimT->set(primitiveIndex,a_rAnimLocal.translation());
	m_spOutputAnimS->set(primitiveIndex,a_rScaleLocal);

	m_spOutputName->set(primitiveIndex,a_name);
	m_spOutputParent->set(primitiveIndex,a_parentName);

	return TRUE;
}

void SurfaceAccessibleJoint::startPoints(void)
{
	resetJoints();

	const SurfaceAccessibleI::Creation creation=
			SurfaceAccessibleI::e_createMissing;

	m_spOutputPoint=accessor(e_point,e_position,creation);
	m_spOutputColor=accessor(e_point,e_color,creation);
	m_spOutputNormal=accessor(e_point,e_normal,creation);
}

void SurfaceAccessibleJoint::completePoints(void)
{
	clearAccessors();
}

void SurfaceAccessibleJoint::startMeshes(void)
{
	startPoints();

	const SurfaceAccessibleI::Creation creation=
			SurfaceAccessibleI::e_createMissing;

	m_spOutputVertices=accessor(e_primitive,e_vertices,creation);
	m_spOutputPart=accessor(e_primitive,"part",creation);
	m_spOutputProperties=accessor(e_primitive,e_properties,creation);
	m_spOutputName=accessor(e_primitive,"name",creation);
}

void SurfaceAccessibleJoint::completeMeshes(void)
{
	completePoints();
}

void SurfaceAccessibleJoint::startCurves(void)
{
	startMeshes();

	const SurfaceAccessibleI::Creation creation=
			SurfaceAccessibleI::e_createMissing;

	m_spOutputRadius=accessor(e_point,"radius",creation);
}

void SurfaceAccessibleJoint::completeCurves(void)
{
	completeMeshes();
}

} /* namespace ext */
} /* namespace fe */
