/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <surface/surface.pmh>

namespace fe
{
namespace ext
{

SurfacePlane::SurfacePlane(void)
{
	feLog("SurfacePlane()\n");
}

Protectable* SurfacePlane::clone(Protectable* pInstance)
{
	feLog("SurfacePlane::clone\n");

	SurfacePlane* pSurfacePlane= pInstance?
			fe_cast<SurfacePlane>(pInstance): new SurfacePlane();

	checkCache();

	SurfaceDisk::clone(pSurfacePlane);
	return pSurfacePlane;
}

void SurfacePlane::draw(sp<DrawI> a_spDrawI,const Color* color,
	sp<PartitionI> a_spPartition) const
{
	const Color white(1.0f,1.0f,1.0f,1.0f);

	SpatialVector scale(m_radius,m_radius,m_radius);

	a_spDrawI->drawCircle(m_transform,&scale,color? *color: white);
}

} /* namespace ext */
} /* namespace fe */
