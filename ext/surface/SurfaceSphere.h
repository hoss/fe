/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __surface_SurfaceSphere_h__
#define __surface_SurfaceSphere_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Spherical Surface

	@ingroup surface
*//***************************************************************************/
class FE_DL_EXPORT SurfaceSphere:
	public SurfaceBase,
	public CastableAs<SurfaceSphere>
{
	protected:

	class Impact:
		public SurfaceBase::Impact,
		public CastableAs<Impact>
	{
		public:
								Impact(void):
									m_resolved(FALSE)
								{
#if FE_COUNTED_STORE_TRACKER
									setName("SurfaceSphere::Impact");
#endif
								}
	virtual						~Impact(void)								{}

	virtual	void				reset(void)
								{
									m_resolved=FALSE;
								}

	virtual	SpatialVector		intersection(void)
								{
									if(!m_resolved)
									{
										resolve();
									}
									return transformVector(m_transform,
											m_intersection);
								}
			SpatialVector		intersectionLocal(void)
								{
									if(!m_resolved)
									{
										resolve();
									}
									return m_intersection;
								}
	virtual	void				setIntersectionLocal(
									SpatialVector a_intersection)
								{	m_intersection=a_intersection; }

	virtual	SpatialVector		normal(void)
								{
									if(!m_resolved)
									{
										resolve();
									}
									return rotateVector(m_transform,m_normal);
								}
			void				setNormalLocal(SpatialVector a_normal)
								{	m_normal=a_normal; }

		protected:

	virtual	void				resolve(void)
								{
									sp<SurfaceSphere>
											spSurfaceSphere(m_hpSurface);
									if(spSurfaceSphere.isValid())
									{
										spSurfaceSphere->resolveImpact(
												sp<ImpactI>(this));
										m_resolved=TRUE;
									}
								}

			SpatialVector			m_intersection;
			SpatialVector			m_normal;
			BWORD					m_resolved;
	};

	public:
							SurfaceSphere(void);
virtual						~SurfaceSphere(void)							{}

							//* As Protectable
virtual	Protectable*		clone(Protectable* pInstance=NULL);

							//* As SurfaceI
virtual SpatialVector		center(void)
							{	checkCache();
								return m_center; }
virtual SpatialVector		center(void) const
							{	return m_center; }
virtual Real				radius(void)
							{	checkCache();
								return m_radius; }
virtual Real				radius(void) const
							{	return m_radius; }
virtual Color				diffuse(void) const	{ return m_diffuse; }

							using SurfaceBase::sample;

virtual SpatialTransform	sample(Vector2 a_uv) const;

							using SurfaceBase::nearestPoint;

virtual	sp<ImpactI>			nearestPoint(const SpatialVector& a_origin,
									Real a_maxDistance) const
							{
								SpatialVector direction;
								Real distance=
										PointSphereNearest<Real>::solve(
										m_center,m_radius,a_origin,direction);
								if(a_maxDistance>0.0 && distance>a_maxDistance)
								{
									return sp<Impact>(NULL);
								}

								sp<Impact> spImpact=m_sphereImpactPool.get();
								FEASSERT(spImpact.isValid());

								spImpact->setSurface(this);
								spImpact->setLocationLocal(m_center);
								spImpact->setDiffuse(m_diffuse);
								spImpact->setRadius(m_radius);
								spImpact->setOrigin(a_origin);
								spImpact->setDirection(direction);
								spImpact->setDistance(distance);

								return spImpact;
							}

							using SurfaceBase::rayImpact;

virtual	sp<ImpactI>			rayImpact(const SpatialVector& a_origin,
									const SpatialVector& a_direction,
									Real a_maxDistance,BWORD a_anyHit) const
							{
								const Real distance=
										RaySphereIntersect<Real>::solve(
										m_center,m_radius,
										a_origin,a_direction);

								if(distance<=0.0 ||
										(a_maxDistance>0.0 &&
										distance>a_maxDistance))
								{
									return sp<Impact>(NULL);
								}

								sp<Impact> spImpact=m_sphereImpactPool.get();
								FEASSERT(spImpact.isValid());

								spImpact->setSurface(this);
								spImpact->setLocationLocal(m_center);
								spImpact->setDiffuse(m_diffuse);
								spImpact->setRadius(m_radius);
								spImpact->setOrigin(a_origin);
								spImpact->setDirection(a_direction);
								spImpact->setDistance(distance);
								return spImpact;
							}

							using SurfaceBase::draw;

virtual	void				draw(const SpatialTransform&,sp<DrawI> a_spDrawI,
									const fe::Color* a_color,
									sp<DrawBufferI> a_spDrawBuffer,
									sp<PartitionI> a_spPartition) const;

							using SurfaceBase::bind;

							//* as RecordableI
virtual	void				bind(Record& rRecord);

		void				setRadius(Real a_radius)
							{	m_radius=a_radius; }
		void				setCenter(const SpatialVector& a_rCenter)
							{	m_center=a_rCenter; }

	protected:

virtual	void				cache(void);
virtual	void				resolveImpact(sp<ImpactI> a_spImpactI) const
							{
								sp<Impact> spImpact(a_spImpactI);

								SpatialVector intersection;
								SpatialVector normal;
								RaySphereIntersect<Real>::resolveContact(
										spImpact->locationLocal(),
										spImpact->radius(),
										spImpact->origin(),
										spImpact->direction(),
										spImpact->distance(),
										intersection,normal);
								spImpact->setIntersectionLocal(intersection);
								spImpact->setNormalLocal(normal);
							}

		WeakRecord			m_record;

		SpatialVector		m_center;
		Real				m_radius;

		Color				m_emissive;
		Color				m_ambient;
		Color				m_diffuse;
		Color				m_specular;
		Real				m_shininess;

mutable	CountedPool<Impact>	m_sphereImpactPool;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __surface_SurfaceSphere_h__ */

