/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <surface/surface.pmh>

namespace fe
{
namespace ext
{

SpatialTreeBase::SpatialTreeBase(void):
	m_accuracy(SurfaceI::e_triangle)
{
//	feLog("SpatialTreeBase::SpatialTreeBase\n");
}

SpatialTreeBase::~SpatialTreeBase(void)
{
}

} /* namespace ext */
} /* namespace fe */