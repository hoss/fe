/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __surface_SurfaceDisk_h__
#define __surface_SurfaceDisk_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Flat Circular Surface

	@ingroup surface
*//***************************************************************************/
class FE_DL_EXPORT SurfaceDisk:
	public SurfaceSphere,
	public CastableAs<SurfaceDisk>
{
	protected:
	class Impact:
		public SurfaceSphere::Impact,
		public CastableAs<Impact>
	{
		public:
							Impact(void)
							{
#if FE_COUNTED_STORE_TRACKER
								setName("SurfaceDisk::Impact");
#endif
							}
	virtual					~Impact(void)									{}

	virtual	SpatialVector	intersection(void)
							{
								return transformVector(m_transform,
										m_intersection);
							}
	virtual	SpatialVector	intersectionLocal(void) const
							{	return m_intersection; }

	virtual	SpatialVector	normal(void)
							{	return m_normal; }

	virtual	SpatialVector	axis(void)
							{	return m_normal; }
			void			setAxis(SpatialVector a_axis)
							{	m_normal=a_axis; }
	};

	public:
							SurfaceDisk(void);
virtual						~SurfaceDisk(void)								{}

							//* As Protectable
virtual	Protectable*		clone(Protectable* pInstance=NULL);

							using SurfaceSphere::sample;

							//* As SurfaceI
virtual SpatialTransform	sample(Vector2 a_uv) const;

							using SurfaceSphere::nearestPoint;

virtual	sp<ImpactI>			nearestPoint(const SpatialVector& a_origin,
									Real a_maxDistance) const
							{
								SpatialVector direction;
								SpatialVector intersection;
								Real distance=
										PointDiskNearest<Real>::solve(
										m_center,m_span,m_radius,
										a_origin,direction,intersection);
								if(a_maxDistance>0.0 && distance>a_maxDistance)
								{
									return sp<Impact>(NULL);
								}

								sp<Impact> spImpact=m_diskImpactPool.get();
								spImpact->setSurface(this);
								spImpact->setLocationLocal(m_center);
								spImpact->setAxis(m_span/m_range);
								spImpact->setRadius(m_radius);
								spImpact->setOrigin(a_origin);
								spImpact->setDirection(direction);
								spImpact->setIntersectionLocal(intersection);
								spImpact->setDistance(distance);
								return spImpact;
							}

							using SurfaceSphere::rayImpact;

virtual	sp<ImpactI>			rayImpact(const SpatialVector& a_origin,
									const SpatialVector& a_direction,
									Real a_maxDistance,BWORD a_anyHit) const
							{
								SpatialVector intersection;
								Real distance=
										RayDiskIntersect<Real>::solve(
										m_center,m_span,m_radius,
										a_origin,a_direction,intersection);
								if(a_maxDistance>0.0 && distance>a_maxDistance)
								{
									return sp<Impact>(NULL);
								}

								sp<Impact> spImpact=m_diskImpactPool.get();
								spImpact->setSurface(this);
								spImpact->setLocationLocal(m_center);
								spImpact->setAxis(m_span/m_range);
								spImpact->setRadius(m_radius);
								spImpact->setOrigin(a_origin);
								spImpact->setDirection(a_direction);
								spImpact->setIntersectionLocal(intersection);
								spImpact->setDistance(distance);
								return spImpact;
							}

							using SurfaceSphere::draw;

virtual	void				draw(const SpatialTransform& a_transform,
									sp<DrawI> a_spDrawI,
									const fe::Color* a_color,
									sp<DrawBufferI> a_spDrawBuffer,
									sp<PartitionI> a_spPartition) const;

		void				setSpan(const SpatialVector& a_rSpan)
							{	m_span=a_rSpan;
								m_range=magnitude(m_span);
								updateSphere(); }
		SpatialVector		span(void)
							{	checkCache();
								return m_span; }
		SpatialVector		span(void) const
							{	return m_span; }

	protected:

virtual	void				updateSphere(void)								{}
virtual	void				cache(void);
virtual	void				resolveImpact(sp<ImpactI> a_spImpactI) const
							{
/*
								sp<Impact> spImpact(a_spImpactI);

								SpatialVector normal;
								RayDiskIntersect<Real>::resolveContact(
										spImpact->locationLocal(),
										spImpact->axis(),
										spImpact->radius(),
										spImpact->origin(),
										spImpact->direction(),
										spImpact->distance(),
										spImpact->intersectionLocal(),
										normal);
*/
							}

		SpatialTransform	m_transform;
		SpatialVector		m_span;
		Real				m_range;	//* magnitude of span (used by cylinder)

	private:
mutable	CountedPool<Impact>	m_diskImpactPool;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __surface_SurfaceDisk_h__ */


