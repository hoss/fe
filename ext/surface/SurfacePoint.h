/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __surface_SurfacePoint_h__
#define __surface_SurfacePoint_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief SurfacePoint RecordView

	@ingroup surface
*//***************************************************************************/
class FE_DL_EXPORT SurfacePoint:
	virtual public RecordView,
	public CastableAs<SurfacePoint>
{
	public:
		Functor<SpatialVector>		up;
		Functor<SpatialVector>		at;
		Functor<SpatialVector>		uvw;
		Functor<Color>				color;

				SurfacePoint(void)			{ setName("SurfacePoint"); }
virtual	void	addFunctors(void)
				{
					add(up,		FE_SPEC("spc:up",
							"Orientation up from direction"));
					add(at,		FE_USE("spc:at"));
					add(uvw,	FE_SPEC("surf:uvw",
							"Texture Coordinate"));
					add(color,	FE_SPEC("surf:color",
							"RGB Color"));
				}
virtual	void	initializeRecord(void)
				{
				}
};

} /* namespace ext */
} /* namespace fe */

#endif /* __surface_SurfacePoint_h__ */




