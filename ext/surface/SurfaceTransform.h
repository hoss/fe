/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __surface_SurfaceTransform_h__
#define __surface_SurfaceTransform_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief SurfaceTransform RecordView

	@ingroup surface
*//***************************************************************************/
class FE_DL_EXPORT SurfaceTransform:
	virtual public SurfacePoint,
	public CastableAs<SurfaceTransform>
{
	public:
		Functor<SpatialTransform>	transform;
		Functor<SpatialVector>		direction;
		Functor<SpatialVector>		left;

				SurfaceTransform(void)		{ setName("SurfaceTransform"); }
virtual	void	addFunctors(void)
				{
					SurfacePoint::addFunctors();

					add(transform,	FE_SPEC("spc:transform",
							"Spatial position"));
					add(direction,	FE_SPEC("spc:dir",
							"Orientation facing forward"));
					add(left,		FE_SPEC("spc:left",
							"Orientation to side from direction"));
				}
virtual	void	populate(sp<Layout> spLayout)
				{
					SurfacePoint::populate(spLayout);

// TODO: Jason, these are no longer embedded.  Probably needs some fixing
#ifdef DEPRECATE
					//* embed sub-vectors
					scope()->within(spLayout->name(), "spc:dir",
							"spc:transform", 0);
					scope()->within(spLayout->name(), "spc:left",
							"spc:transform", sizeof(SpatialVector));
					scope()->within(spLayout->name(), "spc:up",
							"spc:transform", 2*sizeof(SpatialVector));
					scope()->within(spLayout->name(), "spc:at",
							"spc:transform", 3*sizeof(SpatialVector));
#endif
				}
virtual	void	initializeRecord(void)
				{
					SurfacePoint::initializeRecord();

					transform.attribute()->setSerialize(FALSE);

					set(uvw(),0.0f,0.0f,1.0f);
					setIdentity(transform());
				}
};

} /* namespace ext */
} /* namespace fe */

#endif /* __surface_SurfaceTransform_h__ */



