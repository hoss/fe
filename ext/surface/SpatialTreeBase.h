/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __surface_SpatialTreeBase_h__
#define __surface_SpatialTreeBase_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Triangular storage using a simple array

	@ingroup surface
*//***************************************************************************/
class FE_DL_EXPORT SpatialTreeBase: virtual public SpatialTreeI
{
	public:

	class Hit:
		public SpatialTreeI::Hit,
		public CastableAs<Hit>
	{
		public:
								Hit(void)
								{	reset(); }

		void					reset(void)
								{
									m_face= -1;
									m_triangleIndex= -1;
									m_pointIndex= -1;
									m_primitiveIndex= -1;
									m_partitionIndex= -1;
									m_distance= -1.0;
									set(m_intersection);
									set(m_direction);
									set(m_barycenter);
									set(m_uv);
									set(m_color,1.0,1.0,1.0);
									set(m_du);
									set(m_dv);
									m_pPointIndex[0]=0;
									m_pPointIndex[1]=0;
									m_pPointIndex[2]=0;
									set(m_pVertex[0]);
									set(m_pVertex[1]);
									set(m_pVertex[2]);
									set(m_pNormal[0]);
									set(m_pNormal[1]);
									set(m_pNormal[2]);
								}

	virtual
	const	SpatialVector&		intersection(void) const
								{	return m_intersection; }
	virtual
	const	SpatialVector&		direction(void) const
								{	return m_direction; }
	virtual	Real				distance(void) const
								{	return m_distance; }
	virtual I32					face(void) const
								{	return m_face; }
	virtual I32					triangleIndex(void) const
								{	return m_triangleIndex; }
	virtual I32					primitiveIndex(void) const
								{	return m_primitiveIndex; }
	virtual I32					partitionIndex(void) const
								{	return m_partitionIndex; }
	virtual
	const	SpatialBary&		barycenter(void) const
								{	return m_barycenter; }
	virtual
	const	Vector2&			uv(void) const
								{	return m_uv; }
	virtual
	const	SpatialVector&		du(void) const
								{	return m_du; }
	virtual
	const	SpatialVector&		dv(void) const
								{	return m_dv; }
	virtual
	const	Color&				color(void) const
								{	return m_color; }
	virtual
	const	I32*				pointIndex(void) const
								{	return m_pPointIndex; }
	virtual
	const	SpatialVector*		vertex(void) const
								{	return m_pVertex; }
	virtual
	const	SpatialVector*		normal(void) const
								{	return m_pNormal; }

			void				copy(const sp<Hit>& a_rspOther)
								{
									m_intersection=a_rspOther->intersection();
									m_direction=a_rspOther->direction();
									m_distance=a_rspOther->distance();
									m_face=a_rspOther->face();
									m_triangleIndex=a_rspOther->triangleIndex();
									m_primitiveIndex=
											a_rspOther->primitiveIndex();
									m_partitionIndex=
											a_rspOther->partitionIndex();
									m_barycenter=a_rspOther->barycenter();
									m_uv=a_rspOther->uv();
									m_du=a_rspOther->du();
									m_dv=a_rspOther->dv();
									m_color=a_rspOther->color();
									m_pPointIndex[0]=
											a_rspOther->pointIndex()[0];
									m_pPointIndex[1]=
											a_rspOther->pointIndex()[1];
									m_pPointIndex[2]=
											a_rspOther->pointIndex()[2];
									m_pVertex[0]=a_rspOther->vertex()[0];
									m_pVertex[1]=a_rspOther->vertex()[1];
									m_pVertex[2]=a_rspOther->vertex()[2];
									m_pNormal[0]=a_rspOther->normal()[0];
									m_pNormal[1]=a_rspOther->normal()[1];
									m_pNormal[2]=a_rspOther->normal()[2];
								}

			SpatialVector		m_intersection;
			SpatialVector		m_direction;
			Real				m_distance;
			I32					m_face;
			I32					m_triangleIndex;
			I32					m_pointIndex;
			I32					m_primitiveIndex;
			I32					m_partitionIndex;
			SpatialBary			m_barycenter;
			Vector2				m_uv;
			SpatialVector		m_du;
			SpatialVector		m_dv;
			Color				m_color;
			I32					m_pPointIndex[3];
			SpatialVector		m_pVertex[3];
			SpatialVector		m_pNormal[3];
	};

				SpatialTreeBase(void);
virtual			~SpatialTreeBase(void);

virtual	void	setAccuracy(SurfaceI::Accuracy a_accuracy)
				{	m_accuracy=a_accuracy; }

virtual	void	draw(sp<DrawI>,const fe::Color*) const						{}

	protected:

		SurfaceI::Accuracy	m_accuracy;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __surface_SpatialTreeBase_h__ */
