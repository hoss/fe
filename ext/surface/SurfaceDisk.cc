/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <surface/surface.pmh>

namespace fe
{
namespace ext
{

SurfaceDisk::SurfaceDisk(void):
	m_range(1.0f)
{
	feLog("SurfaceDisk()\n");
	set(m_span,0.0,0.0,1.0);
	setIdentity(m_transform);
}

Protectable* SurfaceDisk::clone(Protectable* pInstance)
{
	feLog("SurfaceDisk::clone\n");

	SurfaceDisk* pSurfaceDisk= pInstance?
			fe_cast<SurfaceDisk>(pInstance): new SurfaceDisk();

	checkCache();

	SurfaceSphere::clone(pSurfaceDisk);
	pSurfaceDisk->m_transform=m_transform;
	pSurfaceDisk->m_span=m_span;
	pSurfaceDisk->m_range=m_range;
	return pSurfaceDisk;
}

void SurfaceDisk::cache(void)
{
	if(!m_record.isValid())
	{
#if FE_CODEGEN<=FE_DEBUG
//		feLog("SurfaceDisk::cache record invalid\n");
#endif
		return;
	}

	SurfaceSphere::cache();

	Disk diskRV;
	diskRV.bind(m_record);

	m_span=diskRV.span();
	m_range=magnitude(m_span);
	if(m_range<=0.0f)
	{
		feLog("SurfaceDisk::cache zero magnitude span\n");
		m_range=1.0f;
	}

	SpatialQuaternion rotation;
	set(rotation,SpatialVector(0.0f,0.0f,1.0f),m_span/m_range);

	m_transform=rotation;
	setTranslation(m_transform,m_center);

//	feLog("SurfaceDisk::cache span %s\n",c_print(m_span));
}

SpatialTransform SurfaceDisk::sample(Vector2 a_uv) const
{
	SpatialTransform transform;

	//* TODO
	setIdentity(transform);

	return transform;
}

void SurfaceDisk::draw(const SpatialTransform& a_transform,
	sp<DrawI> a_spDrawI,const Color* color,
	sp<DrawBufferI> a_spDrawBuffer,
	sp<PartitionI> a_spPartition) const
{
	const Color white(1.0f,1.0f,1.0f,1.0f);

	SpatialVector scale(m_radius,m_radius,m_radius);

	a_spDrawI->drawCircle(a_transform*m_transform,&scale,color? *color: white);
}

} /* namespace ext */
} /* namespace fe */

