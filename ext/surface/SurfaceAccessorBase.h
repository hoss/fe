/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __surface_SurfaceAccessorBase_h__
#define __surface_SurfaceAccessorBase_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
    @brief Common Functionality for Accessor Surface

	@ingroup surface
*//***************************************************************************/
class FE_DL_EXPORT SurfaceAccessorBase:
	virtual public SurfaceAccessorI,
	public CastableAs<SurfaceAccessorBase>
{
	public:
						SurfaceAccessorBase(void);
virtual					~SurfaceAccessorBase(void);

						//* as SurfaceAccessorI
virtual	BWORD			writable(void) const	{ return m_writable; }
virtual	void			setWritable(BWORD a_writable)
						{	m_writable=a_writable; }

virtual	U32				count(void) const									=0;

virtual	String			type(void) const				{ return m_attrType; }

virtual	void			set(U32 a_index,U32 a_subIndex,String a_string)		{}
virtual	void			set(U32 a_index,String a_string)
						{	set(a_index,U32(0),a_string); }

virtual	void			set(U32 a_index,U32 a_subIndex,I32 a_integer)		{}
virtual	void			set(U32 a_index,I32 a_integer)
						{	set(a_index,U32(0),a_integer); }

virtual	I32				duplicate(U32 a_index,U32 a_subIndex=0)	{ return -1; }

virtual	I32				append(void)
						{	return append(SurfaceAccessibleI::e_closed); }
virtual	I32				append(SurfaceAccessibleI::Form a_form)	{ return -1; }
virtual	I32				append(I32 a_integer)
						{	return append(a_integer,
									SurfaceAccessibleI::e_closed); }
virtual	I32				append(I32 a_integer,SurfaceAccessibleI::Form a_form);
virtual	void			append(U32 index,I32 a_integer)						{}
virtual	void			append(Array< Array<I32> >& a_rPrimVerts);

virtual	void			remove(U32 a_index,I32 a_integer=0)					{}

virtual	void			set(U32 a_index,U32 a_subIndex,Real a_real)			{}
virtual	void			set(U32 a_index,Real a_real)
						{	set(a_index,U32(0),a_real); }

virtual	void			set(U32 a_index,U32 a_subIndex,
								const SpatialVector& a_vector)				{}
virtual	void			set(U32 a_index,const SpatialVector& a_vector)
						{	set(a_index,U32(0),a_vector); }
virtual	SpatialVector	spatialVector(U32 index,U32 subIndex=0)
						{	return SpatialVector(0.0,0.0,0.0); }
virtual	BWORD			spatialVector(SpatialVector* a_pVectorArray,
								const Vector2i* a_pIndexArray,I32 a_arrayCount);

virtual	void			fragmentWith(SurfaceAccessibleI::Element a_element,
								String a_attributeName,String a_groupName);
virtual	U32				fragmentCount(void) const
						{	return m_fragmentMap.size(); }
virtual	String			fragment(U32 a_index) const
						{
							return (a_index<m_fragmentList.size())?
									m_fragmentList[a_index]: "";
						}
virtual	BWORD			filterWith(String a_filterString,
							sp<SurfaceAccessibleI::FilterI>& a_rspFilter) const;

		void			setSurfaceAccessible(
								sp<SurfaceAccessibleI> a_spSurfaceAccessibleI)
						{	m_spSurfaceAccessibleI=a_spSurfaceAccessibleI; }

virtual	SurfaceAccessibleI::Element		element(void)	{ return m_element; }
virtual	SurfaceAccessibleI::Attribute	attribute(void)	{ return m_attribute; }

virtual	String			attributeName(void)		{ return m_attrName; }

virtual	void			appendGroupSpans(
								SpannedRange::MultiSpan& a_rMultiSpan);

	protected:

		BWORD							m_writable;
		SurfaceAccessibleI::Element		m_element;
		SurfaceAccessibleI::Attribute	m_attribute;
		String							m_attrName;
		String							m_attrType;
		sp<SurfaceAccessibleI>			m_spSurfaceAccessibleI;

	private:

	class Fragment
	{
		public:
			Array<U32>	m_indexArray;
	};
	class FragmentFilter:
		virtual public SurfaceAccessibleI::FilterI,
		public CastableAs<FragmentFilter>
	{
		public:
					FragmentFilter(void)									{}
	virtual			~FragmentFilter(void)									{}

	virtual	I32		filterCount(void) const
					{
						return m_pFragment?
								m_pFragment->m_indexArray.size(): -1;
					}
	virtual	I32		filter(I32 a_filterIndex) const
					{
						if(!m_pFragment)
						{
							return -1;
						}
						const Array<U32>& rIndexArray=
								m_pFragment->m_indexArray;
						if(a_filterIndex>=I32(rIndexArray.size()))
						{
							return -1;
						}
						return rIndexArray[a_filterIndex];
					}

					/// @internal
			void	setFragment(const Fragment* a_pFragment)
					{	m_pFragment=a_pFragment; }

		private:
			const Fragment*			m_pFragment;
	};

		AutoHashMap<String,Fragment>	m_fragmentMap;
		Array<String>					m_fragmentList;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __surface_SurfaceAccessorBase_h__ */
