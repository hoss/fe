/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __surface_SurfaceAccessibleCatalog_h__
#define __surface_SurfaceAccessibleCatalog_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
    @brief Catalog Surface Binding

	@ingroup surface
*//***************************************************************************/
class FE_DL_EXPORT SurfaceAccessibleCatalog:
	public SurfaceAccessibleBase,
	public CastableAs<SurfaceAccessibleCatalog>
{
	public:
								SurfaceAccessibleCatalog(void);
virtual							~SurfaceAccessibleCatalog(void);

								//* As Protectable
virtual	Protectable*			clone(Protectable* pInstance=NULL);

								using SurfaceAccessibleBase::bind;

								//* as SurfaceAccessibleI
virtual	void					bind(Instance a_instance)
								{	setCatalog(
											a_instance.cast< sp<Catalog> >()); }
virtual	BWORD					isBound(void)
								{	return catalog().isValid(); }

								using SurfaceAccessibleBase::load;

virtual	BWORD					load(String a_filename,
										sp<Catalog> a_spSettings);

								using SurfaceAccessibleBase::save;

virtual	BWORD					save(String a_filename,
										sp<Catalog> a_spSettings);

								using SurfaceAccessibleBase::attributeSpecs;

virtual	void					attributeSpecs(
										Array<SurfaceAccessibleI::Spec>&
										a_rSpecs,
										String a_node,
										SurfaceAccessibleI::Element
										a_element) const;

								using SurfaceAccessibleBase::accessor;

virtual sp<SurfaceAccessorI>	accessor(String a_node,
										SurfaceAccessibleI::Element a_element,
										String a_name,
										SurfaceAccessibleI::Creation a_create,
										SurfaceAccessibleI::Writable
										a_writable);
virtual sp<SurfaceAccessorI>	accessor(String a_node,
										SurfaceAccessibleI::Element a_element,
										SurfaceAccessibleI::Attribute
										a_attribute,
										SurfaceAccessibleI::Creation a_create,
										SurfaceAccessibleI::Writable
										a_writable);

								using SurfaceAccessibleBase::discard;

virtual	BWORD					discard(SurfaceAccessibleI::Element a_element,
										String a_name);

								//* Catalog specific
		sp<Catalog>				catalog(void);
		void					setCatalog(sp<Catalog> a_spCatalog)
								{	m_spCatalog=a_spCatalog; }

		void					setKey(String a_key)
								{	m_key=a_key; }
		String					key(void)
								{	return m_key; }

		BWORD					persistent(void)
								{	return m_persistent; }
		void					setPersistent(BWORD a_persistent)
								{	m_persistent=a_persistent; }

	protected:
virtual	void					reset(void);

	private:
		sp<SurfaceAccessorI>	createAccessor(void);

		sp<Catalog>				m_spCatalog;
		String					m_key;
		BWORD					m_persistent;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __surface_SurfaceAccessibleCatalog_h__ */
