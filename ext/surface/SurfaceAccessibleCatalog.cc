/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <surface/surface.pmh>

namespace fe
{
namespace ext
{

SurfaceAccessibleCatalog::SurfaceAccessibleCatalog(void):
	m_persistent(FALSE)
{
}

SurfaceAccessibleCatalog::~SurfaceAccessibleCatalog(void)
{
	//* TODO consider reference counting
	if(!m_persistent)
	{
		reset();
	}
}

sp<Catalog> SurfaceAccessibleCatalog::catalog(void)
{
	if(!m_spCatalog.isValid())
	{
		//* auto-bind a private catalog
		m_spCatalog=registry()->master()->createCatalog("Surface Catalog");
		m_key="surface";
	}

	return m_spCatalog;
}

void SurfaceAccessibleCatalog::reset(void)
{
	SurfaceAccessibleBase::reset();

	sp<Catalog> spCatalog=catalog();
	if(spCatalog.isValid())
	{
		spCatalog->catalogRemove(m_key);
	}
}

Protectable* SurfaceAccessibleCatalog::clone(Protectable* pInstance)
{
	SurfaceAccessibleCatalog* pSurfaceAccessibleCatalog= pInstance?
			fe_cast<SurfaceAccessibleCatalog>(pInstance):
			new SurfaceAccessibleCatalog();

	pSurfaceAccessibleCatalog->setLibrary(library());
	pSurfaceAccessibleCatalog->setFactoryIndex(factoryIndex());

	pSurfaceAccessibleCatalog->setCatalog(catalog()->catalogDeepCopy());

	return pSurfaceAccessibleCatalog;
}

//* NOTE to save/load multiple surfaces in one file,
//* just use setCatalog() and setKey() on a shared Catalog
//* and then save/load using some CatalogWriterI/CatalogReaderI

//* TODO load should change m_key to first key in loaded data
BWORD SurfaceAccessibleCatalog::load(
	String a_filename,sp<Catalog> a_spSettings)
{
#if	FE_SAR_DEBUG
	feLog("SurfaceAccessibleCatalog::load \"%s\"\n",a_filename.c_str());
#endif

	const String format=String(strrchr(a_filename.c_str(),'.')).prechop(".");
	const String implementation("CatalogReaderI.*.*."+format);

	sp<CatalogReaderI> spCatalogReaderI=registry()->create(implementation);
	if(spCatalogReaderI.isNull())
	{
		feLog("SurfaceAccessibleCatalog::load format \"%s\" not supported\n",
				format.c_str());
		return FALSE;
	}

	//* make sure it exists and clear it if it did
	catalog()->catalogClear();

	return spCatalogReaderI->load(a_filename,m_spCatalog);
}

//* TODO save should be limited to m_key,
//* not whole Catalog, potentially shared
BWORD SurfaceAccessibleCatalog::save(
	String a_filename,sp<Catalog> a_spSettings)
{
#if	FE_SAR_DEBUG
	feLog("SurfaceAccessibleCatalog::save \"%s\"\n",a_filename.c_str());
#endif

	const String format=String(strrchr(a_filename.c_str(),'.')).prechop(".");
	const String implementation("CatalogWriterI.*.*."+format);

	sp<CatalogWriterI> spCatalogWriterI=registry()->create(implementation);
	if(spCatalogWriterI.isNull())
	{
		feLog("SurfaceAccessibleCatalog::save format \"%s\" not supported\n",
				format.c_str());
		return FALSE;
	}

	return spCatalogWriterI->save(a_filename,m_spCatalog);
}

void SurfaceAccessibleCatalog::attributeSpecs(
	Array<SurfaceAccessibleI::Spec>& a_rSpecs,
	String a_node,
	SurfaceAccessibleI::Element a_element) const
{
//	feLog("SurfaceAccessibleCatalog::attributeSpecs element %d\n",a_element);

	a_rSpecs.clear();

	//* NOTE a_node ignored

	if(!m_spCatalog.isValid())
	{
		return;
	}

	const String elementName=elementLayout(a_element);

	Array<String> properties;
	m_spCatalog->catalogProperties(m_key,properties);

	const U32 propertyCount=properties.size();
	for(U32 propertyIndex=0;propertyIndex<propertyCount;propertyIndex++)
	{
		const String attrName=properties[propertyIndex].replace("\\[0\\]$","");

		if(attrName=="surf:points")
		{
			continue;
		}

		if(!strncmp(attrName.c_str(),"type:",5) ||
				!strncmp(attrName.c_str(),":",1) )
		{
			continue;
		}

		const String rateName=m_spCatalog->catalogOrDefault<String>(
				m_key,"rate:"+attrName,"");
		if(rateName!=elementName)
		{
			continue;
		}

		const String typeName=m_spCatalog->catalogOrDefault<String>(
				m_key,"type:"+attrName,"");

//		feLog("SurfaceAccessibleCatalog::attributeSpecs %d/%d"
//				" rate \"%s\" type \"%s\" attr \"%s\"\n",
//				propertyIndex,propertyCount,
//				rateName.c_str(),typeName.c_str(),attrName.c_str());

		SurfaceAccessibleI::Spec spec;
		spec.set(commonName(attrName),typeName);

		a_rSpecs.push_back(spec);
	}
}

sp<SurfaceAccessorI> SurfaceAccessibleCatalog::accessor(
	String a_node,SurfaceAccessibleI::Element a_element,
	String a_name,SurfaceAccessibleI::Creation a_create,
	SurfaceAccessibleI::Writable a_writable)
{
	sp<SurfaceAccessorCatalog> spAccessor=createAccessor();
	if(spAccessor.isValid())
	{
		spAccessor->setWritable(a_writable);
		spAccessor->setSurfaceAccessible(sp<SurfaceAccessibleI>(this));

		if(spAccessor->bind(a_element,a_name) ||
				a_create==SurfaceAccessibleI::e_createMissing)
		{
			return spAccessor;
		}
	}
	return sp<SurfaceAccessorI>(NULL);
}

sp<SurfaceAccessorI> SurfaceAccessibleCatalog::accessor(
	String a_node,SurfaceAccessibleI::Element a_element,
	SurfaceAccessibleI::Attribute a_attribute,
	SurfaceAccessibleI::Creation a_create,
	SurfaceAccessibleI::Writable a_writable)
{
	sp<SurfaceAccessorCatalog> spAccessor=createAccessor();
	if(spAccessor.isValid())
	{
		spAccessor->setWritable(a_writable);
		spAccessor->setSurfaceAccessible(sp<SurfaceAccessibleI>(this));

		if(spAccessor->bind(a_element,a_attribute) ||
				a_create==SurfaceAccessibleI::e_createMissing)
		{
			return spAccessor;
		}
	}
	return sp<SurfaceAccessorI>(NULL);
}

sp<SurfaceAccessorI> SurfaceAccessibleCatalog::createAccessor(void)
{
	sp<Catalog> spCatalog=catalog();

	if(!spCatalog.isValid())
	{
		return sp<SurfaceAccessorI>(NULL);
	}
	sp<SurfaceAccessorCatalog> spAccessorCatalog(new SurfaceAccessorCatalog());
	if(spAccessorCatalog.isValid())
	{
		spAccessorCatalog->setCatalog(spCatalog);

		//* NOTE intentionally duplicating String buffer to be thread safe
		spAccessorCatalog->setKey(m_key.c_str());
	}

	return spAccessorCatalog;
}

BWORD SurfaceAccessibleCatalog::discard(
	SurfaceAccessibleI::Element a_element,String a_name)
{
	if(!m_spCatalog.isValid())
	{
		return FALSE;
	}

	m_spCatalog->catalogRemove(m_key,a_name);
	return TRUE;
}

} /* namespace ext */
} /* namespace fe */
