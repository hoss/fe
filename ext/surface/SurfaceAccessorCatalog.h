/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __surface_SurfaceAccessorCatalog_h__
#define __surface_SurfaceAccessorCatalog_h__

namespace fe
{
namespace ext
{

//* TODO coordinate counts between accessors

/**************************************************************************//**
    @brief Accessor backed with a Catalog

	@ingroup surface
*//***************************************************************************/
class FE_DL_EXPORT SurfaceAccessorCatalog:
	virtual public SurfaceAccessorBase,
	public CastableAs<SurfaceAccessorCatalog>
{
	public:
						SurfaceAccessorCatalog(void);
virtual					~SurfaceAccessorCatalog(void);

						using SurfaceAccessorBase::set;
						using SurfaceAccessorBase::append;
						using SurfaceAccessorBase::spatialVector;

						//* as SurfaceAccessorI
virtual	U32				count(void) const;
virtual	U32				subCount(U32 a_index) const;

virtual	void			set(U32 a_index,U32 subIndex,String a_string);
virtual	String			string(U32 a_index,U32 a_subIndex=0);

virtual	void			set(U32 a_index,U32 a_subIndex,I32 a_integer);
virtual	I32				integer(U32 a_index,U32 a_subIndex=0);

virtual	I32				append(I32 a_integer);
virtual	I32				append(void);
virtual	I32				append(SurfaceAccessibleI::Form a_form);
virtual	void			append(U32 a_index,I32 a_integer);
virtual	void			append(Array< Array<I32> >& a_rPrimVerts);

virtual	void			set(U32 a_index,U32 a_subIndex,Real a_real);
virtual	Real			real(U32 a_index,U32 a_subIndex=0);

virtual	void			set(U32 a_index,U32 a_subIndex,
								const SpatialVector& a_spatialVector);
virtual	SpatialVector	spatialVector(U32 a_index,U32 a_subIndex=0);

		BWORD			bind(SurfaceAccessibleI::Element a_element,
								SurfaceAccessibleI::Attribute a_attribute)
						{
							m_attribute=a_attribute;

							String name;
							switch(a_attribute)
							{
								case SurfaceAccessibleI::e_generic:
								case SurfaceAccessibleI::e_position:
									name="spc:at";
									break;
								case SurfaceAccessibleI::e_normal:
									name="spc:up";
									break;
								case SurfaceAccessibleI::e_uv:
									name="surf:uvw";
									break;
								case SurfaceAccessibleI::e_color:
									name="surf:color";
									break;
								case SurfaceAccessibleI::e_vertices:
									name="surf:points";
									break;
//									m_attrName="vertices";
//									FEASSERT(a_element==
//											SurfaceAccessibleI::e_primitive);
//									m_element=a_element;
//									return TRUE;
								case SurfaceAccessibleI::e_properties:
									name="properties";
									break;
//									m_attrName="properties";
//									FEASSERT(a_element==
//											SurfaceAccessibleI::e_primitive);
//									m_element=a_element;
//									return TRUE;
							}
							return bindInternal(a_element,name);
						}
		BWORD			bind(SurfaceAccessibleI::Element a_element,
								const String& a_name)
						{
							String name=a_name;
							m_attribute=SurfaceAccessibleI::e_generic;

							if(name=="P")
							{
								name="spc:at";
								m_attribute=SurfaceAccessibleI::e_position;
							}
							else if(name=="N")
							{
								name="spc:up";
								m_attribute=SurfaceAccessibleI::e_normal;
							}
							else if(name=="uv")
							{
								name="surf:uvw";
								m_attribute=SurfaceAccessibleI::e_uv;
							}
							else if(name=="Cd")
							{
								name="surf:color";
								m_attribute=SurfaceAccessibleI::e_color;
							}

							return bindInternal(a_element,name);
						}

						//* Catalog specific
		void			setCatalog(sp<Catalog> a_spCatalog)
						{	m_spCatalog=a_spCatalog; }
		void			setKey(String a_key)
						{	m_key=a_key; }

	private:

		void			bindVector(U32 a_subIndex,BWORD a_create);

virtual	BWORD			bindInternal(SurfaceAccessibleI::Element a_element,
								const String& a_name);

		void			setAttrType(String a_attrType);

		sp<Catalog>						m_spCatalog;
		String							m_key;

		Array<String>*					m_pStringList;
		Array<I32>*						m_pIntegerList;
		Array<Real>*					m_pRealList;

		Array< Array<SpatialVector>* >	m_vectorListArray;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __surface_SurfaceAccessorCatalog_h__ */
