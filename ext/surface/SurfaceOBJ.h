/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __surface_SurfaceOBJ_h__
#define __surface_SurfaceOBJ_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Surface in raw OBJ format

	@ingroup surface
*//***************************************************************************/
class FE_DL_EXPORT SurfaceOBJ:
	public SurfaceTriangles,
	public Initialize<SurfaceOBJ>
{
	public:
							SurfaceOBJ(void);
virtual						~SurfaceOBJ(void)								{}

void						initialize(void);

							//* As Protectable
virtual	Protectable*		clone(Protectable* pInstance=NULL);

	private:
virtual	void				cache(void);
};

} /* namespace ext */
} /* namespace fe */

#endif /* __surface_SurfaceOBJ_h__ */


