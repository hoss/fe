/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <surface/surface.pmh>

#define FE_SOBJ_CACHE_DEBUG		FALSE
#define FE_SOBJ_FILTER			FALSE
#define FE_SOBJ_Z_IS_UP			FALSE

namespace fe
{
namespace ext
{

SurfaceOBJ::SurfaceOBJ(void)
{
	feLog("SurfaceOBJ()\n");
}

void SurfaceOBJ::initialize(void)
{
	feLog("SurfaceOBJ:initialize\n");
}

Protectable* SurfaceOBJ::clone(Protectable* pInstance)
{
	feLog("SurfaceOBJ::clone\n");

	SurfaceOBJ* pSurfaceOBJ= pInstance?
			fe_cast<SurfaceOBJ>(pInstance): new SurfaceOBJ();

	checkCache();

	SurfaceTriangles::clone(pSurfaceOBJ);

	//* copy attributes

	return pSurfaceOBJ;
}

/***
# OBJ file created by ply_to_obj.c
#
g Object001

v  40.6266  28.3457  -1.10804
v  40.0714  30.4443  -1.10804

vn  -0.966742  -0.255752  9.97231e-09
vn  -0.966824  0.255443  3.11149e-08

f  7  6  1
f  1  2  7
*/

void SurfaceOBJ::cache(void)
{
	feLog("SurfaceOBJ::cache\n");

	if(!m_record.isValid())
	{
		feLog("SurfaceOBJ::cache record invalid\n");
		return;
	}

	SurfaceTriangles::cache();

	SurfaceFile surfaceFileRV;
	surfaceFileRV.bind(m_record);

	String filename=surfaceFileRV.filename();
	if(filename.empty())
	{
		feLog("SurfaceOBJ::cache no filename\n");
		return;
	}

	std::ifstream stream;

	stream.open(filename.c_str(),std::fstream::in);
	if(!stream.good() && filename.c_str()[0]!='/')
	{
		const String path=
				registry()->master()->catalog()->catalog<String>("path:media")+
				"/"+filename;
		stream.open(path.c_str(),std::fstream::in);
	}

	if(!stream.good())
	{
		feLog("SurfaceOBJ::cache failed to open \"%s\"\n",filename.c_str());
		return;
	}

	U32 file_vertices=0;
	U32 file_normals=0;
	U32 file_faces=0;
	char line[256];
	char type[256];
	char face0[256];
	char face1[256];
	char face2[256];
	char face3[256];

	//* first pass: just count
	while(stream.good())
	{
		type[0]=0;
		stream.getline(line,256);
		sscanf(line,"%s",type);
		if(!strcmp(type,"g"))
		{
			char objname[256];

			objname[0]=0;
			sscanf(line,"%8s%64s",type,objname);

			setName(objname);
		}
		if(!strcmp(type,"v"))
		{
			file_vertices++;
		}
		if(!strcmp(type,"vn"))
		{
			file_normals++;
		}
		if(!strcmp(type,"f"))
		{
			file_faces++;
		}
	}

	U32 partitionIndex= -1;

	//* TODO allow 5+ verts

	//* TODO use a mesh, not discrete triangles

	m_elements=file_faces*2;
	m_vertices=file_faces*6;
	U32 elementAllocated=0;
	U32 vertexAllocated=0;

	U32 total=0;
	U32 primitiveIndex=0;

	feLog("SurfaceOBJ::cache file_vertices=%d file_normals=%d file_faces=%d\n",
			file_vertices,file_normals,file_faces);

	if(file_vertices<file_normals)
	{
		feLog("SurfaceOBJ::cache missing vertices will be zeroed\n");
	}
	if(file_normals<file_vertices)
	{
		feLog("SurfaceOBJ::cache missing normals will be set to unit Z\n");
	}

	SpatialVector* pVertices=new SpatialVector[file_vertices];
	SpatialVector* pNormals=new SpatialVector[file_normals];

	resizeFor(m_elements,m_vertices,elementAllocated,vertexAllocated);

	//* back to beginning
	stream.clear();
	stream.seekg(0,std::ios::beg);

	U32 file_vertex=0;
	U32 file_normal=0;
	U32 file_face=0;

	//* second pass: convert
	while(!stream.eof())
	{
		stream.getline(line,256);
		sscanf(line,"%8s",type);
		if(!strcmp(type,"v") && file_vertex<file_vertices)
		{
			float x;
			float y;
			float z;
			sscanf(line,"%8s%f%f%f",type,&x,&y,&z);

			SpatialVector& rFileVertex=pVertices[file_vertex++];
#if FE_SOBJ_Z_IS_UP
			set(rFileVertex,x,-z,y);
#else
			set(rFileVertex,x,y,z);
#endif

#if FE_SOBJ_CACHE_DEBUG
			feLog("vertex %d %s\n",file_vertex-1,c_print(rFileVertex));
#endif
		}
		if(!strcmp(type,"vn") && file_normal<file_normals)
		{
			float x;
			float y;
			float z;
			sscanf(line,"%8s%f%f%f",type,&x,&y,&z);

			SpatialVector& rFileNormal=pNormals[file_normal++];
#if FE_SOBJ_Z_IS_UP
			set(rFileNormal,x,-z,y);
#else
			set(rFileNormal,x,y,z);
#endif

#if FE_SOBJ_CACHE_DEBUG
			feLog("normal %d %s\n",file_normal-1,c_print(rFileNormal));
#endif
		}
		if(!strcmp(type,"f") && file_face<file_faces && total+2<m_vertices)
		{
			Vector4i vid;
			Vector4i tid;
			Vector4i nid;
			sscanf(line,"%8s%s%s%s%s",type,face0,face1,face2,face3);

			set(vid);
			set(tid);
			set(nid);
			sscanf(face0,"%d/%d/%d",&vid[0],&tid[0],&nid[0]);
			sscanf(face1,"%d/%d/%d",&vid[1],&tid[1],&nid[1]);
			sscanf(face2,"%d/%d/%d",&vid[2],&tid[2],&nid[2]);

			const I32 subCount=face3[0]? 4: 3;

			if(subCount>3)
			{
				sscanf(face3,"%d/%d/%d",&vid[3],&tid[3],&nid[3]);
			}

#if FE_SOBJ_CACHE_DEBUG
			feLog("face at %d: %d %d %d\n",total,vid[0],vid[1],vid[2]);
#endif

			if(!vid[0] || !vid[1] || !vid[2] ||
					vid[0]==vid[1] || vid[0]==vid[2] || vid[1]==vid[2])
			{
				feLog("SurfaceOBJ::cache bad face at %d: %d %d %d\n",
						total,vid[0],vid[1],vid[2]);
				continue;
			}

			m_pElementArray[primitiveIndex]=Vector3i(total,3,FALSE);

			BWORD bad=FALSE;
			for(I32 subIndex=0;subIndex<subCount && !bad;subIndex++)
			{
				const U32 vv=vid[subIndex]-1;
				m_pVertexArray[total]=(vv<file_vertices)?
						pVertices[vv]: SpatialVector(0.0,0.0,0.0);
				const U32 nn=(nid[subIndex])? nid[subIndex]-1: vv;
				m_pNormalArray[total]=(nn<file_normals)?
						pNormals[nn]: SpatialVector(0.0,0.0,1.0);

				m_pTriangleIndexArray[total]=primitiveIndex;
				m_pPointIndexArray[total]=total;
				m_pPrimitiveIndexArray[total]=primitiveIndex;
				m_pPartitionIndexArray[total]=partitionIndex;

#if FE_SOBJ_CACHE_DEBUG
				feLog(" v %s\n",c_print(m_pVertexArray[total]));
				feLog(" n %s\n",c_print(m_pNormalArray[total]));
#endif

#if FE_SOBJ_FILTER
				if(subIndex)
				{
					const U32 vv2=vid[subIndex-1]-1;
					SpatialVector other=(vv2<file_vertices)?
							pVertices[vv2]: SpatialVector(0.0,0.0,0.0);
					const float dist2 =
						magnitudeSquared(other-m_pVertexArray[total]);
					if(dist2<1e-6)
					{
						feLog("SurfaceOBJ::cache face at %d"
								" coincident verts %d %d  %s  %s\n",
								total,vv+1,vv2+1,
								c_print(other),
								c_print(m_pVertexArray[total]));
						bad=TRUE;
					}
				}
#endif
				if(bad)
				{
					continue;
				}

				total++;
			}

			if(!bad)
			{
				primitiveIndex++;
			}

			if(!face3[0])
			{
				continue;
			}

			m_pElementArray[primitiveIndex]=Vector3i(total-1,3,FALSE);

			m_pVertexArray[total]=m_pVertexArray[total-4];
			m_pNormalArray[total]=m_pNormalArray[total-4];
			m_pTriangleIndexArray[total]=primitiveIndex;
			m_pPointIndexArray[total]=total;
			m_pPrimitiveIndexArray[total]=primitiveIndex;
			m_pPartitionIndexArray[total]=partitionIndex;
			total++;

			m_pVertexArray[total]=m_pVertexArray[total-3];
			m_pNormalArray[total]=m_pNormalArray[total-3];
			m_pTriangleIndexArray[total]=primitiveIndex;
			m_pPointIndexArray[total]=total;
			m_pPrimitiveIndexArray[total]=primitiveIndex;
			m_pPartitionIndexArray[total]=partitionIndex;
			total++;

			primitiveIndex++;
		}
	}

	delete[] pNormals;
	delete[] pVertices;

	stream.close();

	m_vertices=total;
	m_elements=primitiveIndex;

	feLog("SurfaceOBJ::cache name %s verts %d faces %d\n",
			verboseName().c_str(),m_vertices,m_elements);

	resizeFor(m_elements,m_vertices,elementAllocated,vertexAllocated);

	//* TODO uv
	deallocate(m_pUVArray);
	m_pUVArray=NULL;

	//* TODO color
	deallocate(m_pColorArray);
	m_pColorArray=NULL;

	calcBoundingSphere();

	feLog("SurfaceOBJ::cache center %s radius %.6G\n",
			c_print(m_center),m_radius);
}

} /* namespace ext */
} /* namespace fe */
