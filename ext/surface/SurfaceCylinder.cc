/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <surface/surface.pmh>

namespace fe
{
namespace ext
{

SurfaceCylinder::SurfaceCylinder(void):
	m_baseRadius(1.0),
	m_endRadius(1.0)
{
	feLog("SurfaceCylinder()\n");

	set(m_location);
	updateSphere();
}

Protectable* SurfaceCylinder::clone(Protectable* pInstance)
{
	feLog("SurfaceCylinder::clone\n");

	SurfaceCylinder* pSurfaceCylinder= pInstance?
			fe_cast<SurfaceCylinder>(pInstance): new SurfaceCylinder();

	checkCache();

	SurfaceDisk::clone(pSurfaceCylinder);
	pSurfaceCylinder->m_baseRadius=m_baseRadius;
	pSurfaceCylinder->m_endRadius=m_endRadius;
	return pSurfaceCylinder;
}

void SurfaceCylinder::cache(void)
{
	if(!m_record.isValid())
	{
#if FE_CODEGEN<=FE_DEBUG
//		feLog("SurfaceCylinder::cache record invalid\n");
#endif
		return;
	}

	SurfaceDisk::cache();

	Cylinder cylinderRV;
	cylinderRV.bind(m_record);

	m_baseRadius=cylinderRV.baseRadius();
	m_endRadius=cylinderRV.endRadius();

	m_location=cylinderRV.location();

	updateSphere();
}

void SurfaceCylinder::updateSphere(void)
{
	m_center=m_location+0.5f*m_span;

	const Real maxRadius=m_endRadius>m_baseRadius? m_endRadius: m_baseRadius;
	const Real halfLength=0.5f*m_range;
	m_radius=sqrtf(maxRadius*maxRadius+halfLength*halfLength);

//	feLog("SurfaceCylinder::updateSphere location %s span %s\n",
//			c_print(m_location),c_print(m_span));
//	feLog("  baseRadius %.6G endRadius %.6G center %s radius %.6G\n",
//			m_baseRadius,m_endRadius,c_print(m_center),m_radius);
}

SpatialTransform SurfaceCylinder::sample(Vector2 a_uv) const
{
	SpatialTransform transform;

	//* TODO
	setIdentity(transform);

	return transform;
}

void SurfaceCylinder::draw(const SpatialTransform& a_transform,
	sp<DrawI> a_spDrawI,const Color* color,sp<DrawBufferI> a_spDrawBuffer,
	sp<PartitionI> a_spPartition) const
{
	const Color white(1.0f,1.0f,1.0f,1.0f);

	SpatialVector scale(m_endRadius,m_endRadius,m_range);

	a_spDrawI->drawCylinder(a_transform*m_transform,&scale,
			m_baseRadius/m_endRadius, color? *color: white,0);
}

} /* namespace ext */
} /* namespace fe */
