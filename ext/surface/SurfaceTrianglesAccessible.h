/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __surface_SurfaceTrianglesAccessible_h__
#define __surface_SurfaceTrianglesAccessible_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Triangle Surface from generic SurfaceAccessibleI

	@ingroup surface
*//***************************************************************************/
class FE_DL_EXPORT SurfaceTrianglesAccessible:
	public SurfaceTriangles,
	public CastableAs<SurfaceTrianglesAccessible>
{
	public:

	class Gauge: virtual public GaugeI
	{
		public:
								Gauge(sp<SurfaceI> a_spSurfaceI):
									m_spSurfaceI(a_spSurfaceI)
								{
#if FE_COUNTED_STORE_TRACKER
									setName("SurfaceTriangles::Gauge");
#endif
								}
	virtual						~Gauge(void)								{}

	virtual	void				pick(SpatialVector a_source,
										Real a_maxDistance);
	virtual	Real				distanceTo(SpatialVector a_target);

			sp<Geodesic::Mesh>	createMesh(Array<F64>& a_rPoints,
										Array<U32>& a_rFaces);
			void				setMesh(sp<Geodesic::Mesh> a_spMesh);
			sp<Geodesic::Mesh>	mesh(void);

		private:
			sp<SurfaceI>		m_spSurfaceI;
			Geodesic			m_geodesic;
			SpatialVector		m_source;
			Real				m_maxDistance;
	};

								SurfaceTrianglesAccessible(void)			{}
virtual							~SurfaceTrianglesAccessible(void)			{}

virtual	void					cache(void);

								using SurfaceSphere::bind;

		void					bind(sp<SurfaceAccessibleI>
										a_spSurfaceAccessibleI);

virtual	sp<GaugeI>				gauge(void);

virtual	void					resolveImpact(sp<ImpactI> a_spImpactI) const;

	protected:
		sp<SurfaceAccessibleI>	m_spSurfaceAccessibleI;

	private:
		sp<Geodesic::Mesh>		m_spMesh;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __surface_SurfaceTrianglesAccessible_h__ */
