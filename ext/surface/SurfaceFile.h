/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __osg_SurfaceFile_h__
#define __osg_SurfaceFile_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief SurfaceFile RecordView

	@ingroup osg
*//***************************************************************************/
class FE_DL_EXPORT SurfaceFile:
	virtual public RecordView,
	public CastableAs<SurfaceFile>
{
	public:
		Functor<String>				filename;

				SurfaceFile(void)			{ setName("SurfaceFile"); }
virtual	void	addFunctors(void)
				{
					add(filename,			FE_SPEC("surf:filename",
							"name of OpenSceneGraph compatible file"));
				}
virtual	void	initializeRecord(void)
				{
					RecordView::initializeRecord();
				}
};

} /* namespace ext */
} /* namespace fe */

#endif /* __osg_SurfaceFile_h__ */
