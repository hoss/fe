/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <surface/surface.pmh>

#define	FE_SCA_DEBUG	FALSE
#define	FE_SCA_PROFILE	FALSE

namespace fe
{
namespace ext
{

void SurfaceCurvesAccessible::bind(
	sp<SurfaceAccessibleI> a_spSurfaceAccessibleI)
{
	m_spSurfaceAccessibleI=a_spSurfaceAccessibleI;
}

void SurfaceCurvesAccessible::cache(void)
{
#if FE_SCA_DEBUG
	feLog("SurfaceCurvesAccessible::cache\n");
#endif

	//* populate surface with generic accessors

#if FE_SCA_PROFILE
	sp<Profiler> spProfiler(new Profiler("SurfaceCurvesAccessible"));
	sp<Profiler::Profile> spProfilePrep(
			new Profiler::Profile(spProfiler,"Prep"));
	sp<Profiler::Profile> spProfileLoop(
			new Profiler::Profile(spProfiler,"Loop"));
	sp<Profiler::Profile> spProfileResolve(
			new Profiler::Profile(spProfiler,"Resolve"));
	spProfiler->begin();
	spProfilePrep->start();
#endif

	if(m_spSurfaceAccessibleI.isNull())
	{
		feLog("SurfaceCurvesAccessible::cache m_spSurfaceAccessibleI NULL\n");
		return;
	}

	sp<SurfaceAccessorI> spVertices=m_spSurfaceAccessibleI->accessor(
			nodeName(),
			SurfaceAccessibleI::e_primitive,SurfaceAccessibleI::e_vertices,
			SurfaceAccessibleI::e_refuseMissing);
	if(spVertices.isNull())
	{
		feLog("SurfaceCurvesAccessible::cache no vertices\n");
		return;
	}

	sp<SurfaceAccessorI> spPointPosition=m_spSurfaceAccessibleI->accessor(
			nodeName(),
			SurfaceAccessibleI::e_point,SurfaceAccessibleI::e_position,
			SurfaceAccessibleI::e_refuseMissing);

	sp<SurfaceAccessorI> spPointNormal=m_spSurfaceAccessibleI->accessor(
			nodeName(),
			SurfaceAccessibleI::e_point,SurfaceAccessibleI::e_normal,
			SurfaceAccessibleI::e_refuseMissing);

	sp<SurfaceAccessorI> spPointColor=m_spSurfaceAccessibleI->accessor(
			nodeName(),
			SurfaceAccessibleI::e_point,SurfaceAccessibleI::e_color,
			SurfaceAccessibleI::e_refuseMissing);

	sp<SurfaceAccessorI> spPointRadius=m_spSurfaceAccessibleI->accessor(
			nodeName(),
			SurfaceAccessibleI::e_point,"radius",
			SurfaceAccessibleI::e_refuseMissing);

	sp<SurfaceAccessorI> spPointAlpha=m_spSurfaceAccessibleI->accessor(
			nodeName(),
			SurfaceAccessibleI::e_point,"Alpha",
			SurfaceAccessibleI::e_refuseMissing);
	if(spPointAlpha.isNull())
	{
		spPointAlpha=m_spSurfaceAccessibleI->accessor(
				SurfaceAccessibleI::e_point,"alpha",
				SurfaceAccessibleI::e_refuseMissing);
	}

	Arrays arrays=Arrays(e_arrayColor|e_arrayUV);
	if(spPointRadius.isValid())
	{
		arrays=Arrays(arrays|e_arrayRadius);
	}

	setOptionalArrays(arrays);

	const U32 primitiveCount=spVertices->count();
	const U32 curveCount=
			primitiveCount? primitiveCount: spPointPosition->count();

	sp<SurfaceAccessorI> spElementUV;
	if(primitiveCount)
	{
		spElementUV=m_spSurfaceAccessibleI->accessor(
				nodeName(),
				SurfaceAccessibleI::e_primitive,SurfaceAccessibleI::e_uv,
				SurfaceAccessibleI::e_refuseMissing);
	}
	const BWORD primitiveUVs=spElementUV.isValid();
	if(!primitiveUVs)
	{
		spElementUV=m_spSurfaceAccessibleI->accessor(
				nodeName(),
				SurfaceAccessibleI::e_point,SurfaceAccessibleI::e_uv,
				SurfaceAccessibleI::e_refuseMissing);
	}

	sp<SurfaceAccessorI> spElementPartition;
	if(!m_partitionAttr.empty())
	{
#if FE_SCA_DEBUG
		feLog("SurfaceCurvesAccessible::cache partitionAttr \"%s\"\n",
				m_partitionAttr.c_str());
#endif

		spElementPartition=m_spSurfaceAccessibleI->accessor(
				nodeName(),
				primitiveCount?
				SurfaceAccessibleI::e_primitive:
				SurfaceAccessibleI::e_point,
				m_partitionAttr.c_str(),
				SurfaceAccessibleI::e_refuseMissing);
	}

#if FE_SCA_DEBUG
	feLog("SurfaceCurvesAccessible::cache primitiveCount %d\n",
			primitiveCount);
#endif

	m_elements=curveCount;
	m_vertices=0;
	U32 elementAllocated=0;
	U32 vertexAllocated=0;

	U32 total=0;

	const SpatialVector zero(0.0,0.0,0.0);
	const Color white(1.0,1.0,1.0);

	U32 pointCount=0;
	if(primitiveCount)
	{
		for(U32 primitiveIndex=0;primitiveIndex<primitiveCount;primitiveIndex++)
		{
			pointCount+=spVertices->subCount(primitiveIndex);
		}
	}
	else
	{
		pointCount=curveCount;
	}

	resizeFor(curveCount,pointCount,
			elementAllocated,vertexAllocated);

#if FE_SCA_PROFILE
	spProfileLoop->replace(spProfilePrep);
#endif

	for(U32 curveIndex=0;curveIndex<curveCount;curveIndex++)
	{
		const U32 subCount=primitiveCount? spVertices->subCount(curveIndex): 1;

#if FE_SCA_DEBUG
		feLog("SurfaceCurvesAccessible::cache %d/%d point %d/%d verts %d\n",
				curveIndex,curveCount,total,vertexAllocated,subCount);
#endif

		if(!subCount)
		{
			continue;
		}

		m_vertices+=subCount;

		FEASSERT(m_vertices<=vertexAllocated);
		FEASSERT(curveIndex<elementAllocated);

		I32 partitionIndex= -1;
		if(spElementPartition.isValid())
		{
			const String partition=
					spElementPartition->string(curveIndex);
			partitionIndex=lookupPartition(partition);

//			feLog("SurfaceCurvesAccessible::cache partition %d \"%s\"\n",
//					partitionIndex,partition.c_str());
		}

		m_pElementArray[curveIndex]=Vector3i(total,subCount,TRUE);

		for(U32 subIndex=0;subIndex<subCount;subIndex++)
		{
			FEASSERT(total<vertexAllocated);

			const I32 pointIndex=primitiveCount?
					spVertices->integer(curveIndex,subIndex): curveIndex;

			m_pVertexArray[total]=
					spPointPosition->spatialVector(pointIndex);

			m_pNormalArray[total]=(spPointNormal.isValid())?
					spPointNormal->spatialVector(pointIndex): zero;

			if(spElementUV.isNull())
			{
				set(m_pUVArray[total],subIndex/(subCount-1.0),0.0);
			}
			else if(primitiveUVs)
			{
				m_pUVArray[total]=
						spElementUV->spatialVector(curveIndex,subIndex);
			}
			else
			{
				m_pUVArray[total]=spElementUV->spatialVector(pointIndex);
			}

			if(spPointColor.isNull())
			{
				m_pColorArray[total]=white;
			}
			else
			{
				m_pColorArray[total]=spPointColor->spatialVector(pointIndex);
			}

			if(spPointRadius.isValid())
			{
				m_pRadiusArray[total]=spPointRadius->real(pointIndex);
			}

			if(spPointAlpha.isValid())
			{
				m_pColorArray[total][3]=spPointAlpha->real(pointIndex);
			}

			m_pPointIndexArray[total]=pointIndex;
			m_pPartitionIndexArray[total]=partitionIndex;

			if(primitiveCount)
			{
				m_pTriangleIndexArray[total]=curveIndex;
				m_pPrimitiveIndexArray[total]=curveIndex;
			}
			else
			{
				m_pTriangleIndexArray[total]= -1;
				m_pPrimitiveIndexArray[total]= -1;
			}

#if FE_SCA_DEBUG
			feLog("  %d/%d v %s n %s uv%s %s tri %d pt %d pr %d part %d\n",
					subIndex,subCount,
					c_print(m_pVertexArray[total]),
					c_print(m_pNormalArray[total]),
					spElementUV.isNull()? "": (primitiveUVs? "(pr)": "(pt)"),
					c_print(m_pUVArray[total]),
					m_pTriangleIndexArray[total],
					m_pPointIndexArray[total],
					m_pPrimitiveIndexArray[total],
					m_pPartitionIndexArray[total]);
#endif

			total++;
		}

		FEASSERT(total==m_vertices);
	}

#if FE_SCA_PROFILE
	spProfileResolve->replace(spProfileLoop);
#endif

	m_elements=curveCount;
	FEASSERT(m_vertices==pointCount);

//	if(spElementUV.isNull())
//	{
//		deallocate(m_pUVArray);
//		m_pUVArray=NULL;
//	}

	if(spPointColor.isNull() && spPointAlpha.isNull())
	{
		deallocate(m_pColorArray);
		m_pColorArray=NULL;
	}

	calcBoundingSphere();

#if FE_SCA_PROFILE
	spProfileResolve->finish();
	spProfiler->end();

	feLog("%s:\n%s\n",spProfiler->name().c_str(),
			spProfiler->report().c_str());
#endif
}

} /* namespace ext */
} /* namespace fe */
