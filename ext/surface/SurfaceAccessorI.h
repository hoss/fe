/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __surface_SurfaceAccessorI_h__
#define __surface_SurfaceAccessorI_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Surface Element Handle

	Accessor implementations are allowed to cache changes and are not required
	to update the accessed data until destruction of the accessor.
	As such, only one accessor for each surface's attribute should be used
	at one time.
	Otherwise, changes through one accessor may not be seen by the other,
	and the flushing of data may happen with arbitrary collisions.

	@ingroup surface
*//***************************************************************************/
class FE_DL_EXPORT SurfaceAccessorI:
	virtual public Component,
	public CastableAs<SurfaceAccessorI>
{
	public:
virtual	BWORD			writable(void) const								=0;
virtual	void			setWritable(BWORD a_writable)						=0;

virtual	String			type(void) const									=0;
virtual	U32				count(void) const									=0;
virtual	U32				subCount(U32 index) const							=0;

virtual	void			set(U32 index,String a_string)						=0;
virtual	void			set(U32 index,U32 subIndex,String a_string)			=0;
virtual	String			string(U32 index,U32 subIndex=0)					=0;

virtual	void			set(U32 index,I32 a_integer)						=0;
virtual	void			set(U32 index,U32 subIndex,I32 a_integer)			=0;
virtual	I32				integer(U32 index,U32 subIndex=0)					=0;

virtual	I32				duplicate(U32 index,U32 subIndex=0)					=0;

virtual	I32				append(void)										=0;
virtual	I32				append(SurfaceAccessibleI::Form a_form)				=0;
virtual	I32				append(I32 a_integer)								=0;
virtual	I32				append(I32 a_integer,
								SurfaceAccessibleI::Form a_form)			=0;
virtual	void			append(U32 index,I32 a_integer)						=0;

						//* point indices for each vertex per primitive
virtual	void			append(Array< Array<I32> >&
								a_rPrimVerts)								=0;

virtual	void			remove(U32 a_index,I32 a_integer=0)					=0;

virtual	void			set(U32 index,Real a_real)							=0;
virtual	void			set(U32 index,U32 subIndex,Real a_real)				=0;
virtual	Real			real(U32 index,U32 subIndex=0)						=0;

virtual	void			set(U32 index,const SpatialVector& a_vector)		=0;
virtual	void			set(U32 index,U32 subIndex,
								const SpatialVector& a_vector)				=0;
virtual	SpatialVector	spatialVector(U32 index,U32 subIndex=0)				=0;
virtual	BWORD			spatialVector(SpatialVector* a_pVectorArray,
								const Vector2i* a_pIndexArray,
								I32 a_arrayCount)							=0;

virtual	void			fragmentWith(SurfaceAccessibleI::Element a_element,
								String a_attributeName,String a_groupName)	=0;
virtual	U32				fragmentCount(void) const							=0;
virtual	String			fragment(U32 a_index) const							=0;
virtual	BWORD			filterWith(String a_filterString,
								sp<SurfaceAccessibleI::FilterI>&
								a_rspFilter) const							=0;

virtual	SurfaceAccessibleI::Element		element(void)						=0;
virtual	SurfaceAccessibleI::Attribute	attribute(void)						=0;

virtual	String			attributeName(void)									=0;

virtual	void			appendGroupSpans(
								SpannedRange::MultiSpan& a_rMultiSpan)		=0;

};

} /* namespace ext */
} /* namespace fe */

#endif /* __surface_SurfaceAccessorI_h__ */
