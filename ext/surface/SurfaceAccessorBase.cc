/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <surface/surface.pmh>

#define FE_SAB_FILTER_DEBUG		FALSE

namespace fe
{
namespace ext
{

SurfaceAccessorBase::SurfaceAccessorBase(void):
	m_writable(TRUE)
{
	setName("SurfaceAccessorBase");

#if FE_COUNTED_STORE_TRACKER
	registerRegion(this,sizeof(SurfaceAccessorBase));
#endif
}

SurfaceAccessorBase::~SurfaceAccessorBase(void)
{
}

I32 SurfaceAccessorBase::append(I32 a_integer,SurfaceAccessibleI::Form a_form)
{
	//* lot of points or primitives
	if(m_element==SurfaceAccessibleI::e_point ||
			(m_element==SurfaceAccessibleI::e_primitive &&
			m_attribute!=SurfaceAccessibleI::e_vertices))
	{
		if(a_integer<1)
		{
			return -1;
		}
		const I32 first=count();
		for(I32 index=0;index<a_integer;index++)
		{
			append();
		}
		return first;
	}

	if(m_element!=SurfaceAccessibleI::e_primitive ||
			m_attribute!=SurfaceAccessibleI::e_vertices)
	{
		append(U32(0),a_integer);
		return 0;
	}

	//* append a primitive with the specified number of vertices
	sp<SurfaceAccessorI> spPointAccessor=m_spSurfaceAccessibleI->accessor(
			SurfaceAccessibleI::e_point,SurfaceAccessibleI::e_position);

	const I32 primitiveIndex=append(a_form);
	for(I32 vertIndex=0;vertIndex<a_integer;vertIndex++)
	{
		const I32 pointIndex=spPointAccessor->append();
		append(primitiveIndex,pointIndex);
	}

	sp<SurfaceAccessorI> spPropertyAccessor=m_spSurfaceAccessibleI->accessor(
			SurfaceAccessibleI::e_primitive,SurfaceAccessibleI::e_properties);
	spPropertyAccessor->set(primitiveIndex,SurfaceAccessibleI::e_openCurve,
			I32(a_form==SurfaceAccessibleI::e_open));

	return primitiveIndex;
}

void SurfaceAccessorBase::append(Array< Array<I32> >& a_rPrimVerts)
{
	if(m_element!=SurfaceAccessibleI::e_primitive ||
			m_attribute!=SurfaceAccessibleI::e_vertices)
	{
		return;
	}

	const U32 appendCount=a_rPrimVerts.size();

	m_attribute=SurfaceAccessibleI::e_generic;

	I32 primitiveIndex=append(appendCount);	//* add primitives

	m_attribute=SurfaceAccessibleI::e_vertices;

	for(U32 appendIndex=0;appendIndex<appendCount;appendIndex++)
	{
		const Array<I32>& rVertList=a_rPrimVerts[appendIndex];

		const U32 subCount=rVertList.size();
		for(U32 subIndex=0;subIndex<subCount;subIndex++)
		{
			const I32 pointIndex=rVertList[subIndex];

			append(primitiveIndex,pointIndex);	//* add vertex to primitive
		}

		primitiveIndex++;
	}
}

BWORD SurfaceAccessorBase::spatialVector(SpatialVector* a_pVectorArray,
		const Vector2i* a_pIndexArray,I32 a_arrayCount)
{
	for(I32 arrayIndex=0;arrayIndex<a_arrayCount;arrayIndex++)
	{
		const Vector2i& rIndexPair=a_pIndexArray[arrayIndex];

		a_pVectorArray[arrayIndex]=spatialVector(rIndexPair[0],rIndexPair[1]);
	}

	return TRUE;
}

void SurfaceAccessorBase::fragmentWith(SurfaceAccessibleI::Element a_element,
	String a_attributeName,String a_groupName)
{
#if FE_SAB_FILTER_DEBUG
	feLog("SurfaceAccessorBase::fragmentWith '%s' \"%s\" group \"%s\"\n",
			SurfaceAccessibleBase::elementLayout(a_element).c_str(),
			a_attributeName.c_str(),a_groupName.c_str());
#endif

	m_fragmentMap.clear();
	m_fragmentList.clear();
	if(m_spSurfaceAccessibleI.isNull())
	{
		feLog("SurfaceAccessorBase::fragmentWith"
				" m_spSurfaceAccessibleI not set\n");
		return;
	}

	Array< sp<SurfaceAccessorI> > fragmentAccessors;
	String buffer=a_attributeName;
	String token;
	while(!(token=buffer.parse()).empty())
	{
#if FE_SAB_FILTER_DEBUG
		feLog("SurfaceAccessorBase::fragmentWith \"%s\" token \"%s\"\n",
				a_attributeName.c_str(),token.c_str());
#endif

		sp<SurfaceAccessorI> spFragmentAccessor=
				m_spSurfaceAccessibleI->accessor(
				a_element,token);
		if(spFragmentAccessor.isNull())
		{
			feLog("SurfaceAccessorBase::fragmentWith"
					" access to attribute \"%s\" failed\n",
					a_attributeName.c_str());
			return;
		}
		fragmentAccessors.push_back(spFragmentAccessor);
	}
	const U32 accessorCount=fragmentAccessors.size();
	if(!accessorCount)
	{
		feLog("SurfaceAccessorBase::fragmentWith"
				" no accessors created\n",
				a_attributeName.c_str());
		return;
	}

	sp<SurfaceAccessorI> spGroupAccessor;
	if(!a_groupName.empty())
	{
		const SurfaceAccessibleI::Element groupElement=
				(a_element==SurfaceAccessibleI::e_primitive)?
				SurfaceAccessibleI::e_primitiveGroup:
				SurfaceAccessibleI::e_pointGroup;
		spGroupAccessor=
				m_spSurfaceAccessibleI->accessor(groupElement,a_groupName);
	}

	U32 elementCount=spGroupAccessor.isValid()?
			spGroupAccessor->count(): fragmentAccessors[0]->count();

	for(U32 elementIndex=0;elementIndex<elementCount;elementIndex++)
	{
		const U32 reIndex=spGroupAccessor.isValid()?
				spGroupAccessor->integer(elementIndex): elementIndex;

		String fragmentKey;
		for(U32 accessorIndex=0;accessorIndex<accessorCount;accessorIndex++)
		{
			const String fragment=
					fragmentAccessors[accessorIndex]->string(reIndex);
			fragmentKey+=(accessorIndex? " ": "")+fragment;
		}

		Fragment& rFragment=m_fragmentMap[fragmentKey];
		rFragment.m_indexArray.push_back(reIndex);

#if FE_SAB_FILTER_DEBUG
		feLog("SurfaceAccessorBase::fragmentWith"
				" %d/%d as %d fragmentKey \"%s\"\n",
				elementIndex,elementCount,reIndex,fragmentKey.c_str());
#endif
	}

	m_fragmentList.resize(elementCount);

	U32 nameIndex=0;
	for(AutoHashMap<String,Fragment>::const_iterator it=m_fragmentMap.begin();
			it!=m_fragmentMap.end();it++)
	{
		m_fragmentList[nameIndex++]=it->first;
	}
}

BWORD SurfaceAccessorBase::filterWith(String a_filterString,
	sp<SurfaceAccessibleI::FilterI>& a_rspFilter) const
{
#if FE_SAB_FILTER_DEBUG
	feLog("SurfaceAccessorBase::filterWith \"%s\"\n",a_filterString.c_str());
#endif

	AutoHashMap<String,Fragment>::const_iterator it=
			m_fragmentMap.find(a_filterString);
	if(it==m_fragmentMap.end())
	{
#if FE_SAB_FILTER_DEBUG
	feLog("SurfaceAccessorBase::filterWith NOT FOUND\n");
#endif
		a_rspFilter=NULL;
		return FALSE;
	}
	sp<FragmentFilter> spFragmentFilter=a_rspFilter;
	if(spFragmentFilter.isNull())
	{
		spFragmentFilter=sp<FragmentFilter>(
				new FragmentFilter());
		FEASSERT(spFragmentFilter.isValid());
	}
	spFragmentFilter->setFragment(&(it->second));
	a_rspFilter=spFragmentFilter;
	return TRUE;
}

void SurfaceAccessorBase::appendGroupSpans(
	SpannedRange::MultiSpan& a_rMultiSpan)
{
	if(m_element!=SurfaceAccessibleI::e_pointGroup &&
			m_element!=SurfaceAccessibleI::e_primitiveGroup)
	{
		feLog("SurfaceAccessorBase::appendGroupSpans not a group accessor");
		return;
	}

	const U32 groupCount=count();
	for(U32 groupIndex=0;groupIndex<groupCount;groupIndex++)
	{
		const U32 elementIndex=integer(groupIndex);
		a_rMultiSpan.add(elementIndex,elementIndex);
	}
}

} /* namespace ext */
} /* namespace fe */
