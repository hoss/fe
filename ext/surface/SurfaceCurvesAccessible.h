/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __surface_SurfaceCurvesAccessible_h__
#define __surface_SurfaceCurvesAccessible_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Curve Surface from generic SurfaceAccessibleI

	@ingroup surface
*//***************************************************************************/
class FE_DL_EXPORT SurfaceCurvesAccessible:
	public SurfaceCurves,
	public CastableAs<SurfaceCurvesAccessible>
{
	public:

								SurfaceCurvesAccessible(void)				{}
virtual							~SurfaceCurvesAccessible(void)				{}

virtual	void					cache(void);

								using SurfaceCurves::bind;

		void					bind(sp<SurfaceAccessibleI>
										a_spSurfaceAccessibleI);

	protected:
		sp<SurfaceAccessibleI>	m_spSurfaceAccessibleI;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __surface_SurfaceCurvesAccessible_h__ */
