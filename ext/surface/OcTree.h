/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __surface_OcTree_h__
#define __surface_OcTree_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Triangular storage using divisions by eight sections

	@ingroup surface
*//***************************************************************************/
class FE_DL_EXPORT OcTree: public QuadTree
{
	public:
				OcTree(void)
				{
//					feLog("OcTree::OcTree\n");
					set(m_divisions,2,2,2);
				}
};

} /* namespace ext */
} /* namespace fe */

#endif /* __surface_OcTree_h__ */
