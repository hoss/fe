/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __surface_Sphere_h__
#define __surface_Sphere_h__

FE_ATTRIBUTE("surf:emissive",	"");
FE_ATTRIBUTE("surf:ambient",	"");
FE_ATTRIBUTE("surf:diffuse",	"");
FE_ATTRIBUTE("surf:specular",	"");
FE_ATTRIBUTE("surf:shininess",	"");

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Material RecordView

	@ingroup surface
*//***************************************************************************/
class FE_DL_EXPORT Material: virtual public RecordView
{
	public:
		Functor<Color>	emissive;
		Functor<Color>	ambient;
		Functor<Color>	diffuse;
		Functor<Color>	specular;
		Functor<F32>	shininess;

				Material(void)		{ setName("Material"); }
virtual	void	addFunctors(void)
				{
					add(emissive,	FE_USE("surf:emissive"));
					add(ambient,	FE_USE("surf:ambient"));
					add(diffuse,	FE_USE("surf:diffuse"));
					add(specular,	FE_USE("surf:specular"));
					add(shininess,	FE_USE("surf:shininess"));
				}
virtual	void	initializeRecord(void)
				{
					set(emissive());
					set(ambient());
					set(diffuse(),1.0f,1.0f,1.0f,1.0f);
					set(specular());
					shininess()=1.0f;
				}
virtual	void	finalizeRecord(void)
				{
					if(diffuse()[3]==Real(0) &&
							magnitudeSquared(diffuse())>Real(0))
					{
						diffuse()[3]=1.0;
					}
				}
};

} /* namespace ext */
} /* namespace fe */

#endif /* __surface_Sphere_h__ */

