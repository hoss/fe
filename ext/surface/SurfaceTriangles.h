/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __surface_SurfaceTriangles_h__
#define __surface_SurfaceTriangles_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Discrete Triangle Surface

	@ingroup surface
*//***************************************************************************/
class FE_DL_EXPORT SurfaceTriangles:
	public SurfaceSearchable,
	public CastableAs<SurfaceTriangles>
{
	public:

	class FE_DL_EXPORT Impact:
		public SurfaceSearchable::Impact,
		public CastableAs<Impact>
	{
		public:
								Impact(void)
								{
#if FE_COUNTED_STORE_TRACKER
									setName("SurfaceTriangles::Impact");
#endif
								}
	virtual						~Impact(void)								{}

	virtual	I32					pointIndex2(void) const
								{	return m_pointIndex2; }
			void				setPointIndex2(I32 a_pointIndex2)
								{	m_pointIndex2=a_pointIndex2; }

	virtual	SpatialVector		vertex2(void) const
								{	return m_vertex2; }
			void				setVertex2(SpatialVector a_vertex2)
								{	m_vertex2=a_vertex2; }

	virtual	SpatialVector		normal2(void) const
								{	return m_normal2; }
			void				setNormal2(SpatialVector a_normal2)
								{	m_normal2=a_normal2; }

	virtual	I32					face(void)
								{	return m_face; }
			void				setFace(I32 a_face)
								{	m_face=a_face; }

	virtual	void				copy(sp<SpatialTreeI::Hit>& a_rspHit);

								using SurfaceSearchable::Impact::draw;

	virtual	void				draw(const SpatialTransform& a_rTransform,
										sp<DrawI> a_spDrawI,
										const fe::Color* a_pColor,
										sp<DrawBufferI> a_spDrawBuffer,
										sp<PartitionI> a_spPartition) const;

		private:
			I32						m_pointIndex2;
			SpatialVector			m_vertex2;
			SpatialVector			m_normal2;
			I32						m_face;
	};


							SurfaceTriangles(void);
virtual						~SurfaceTriangles(void);

							//* As SurfaceI
virtual	void				setTriangulation(Triangulation a_triangulation)
							{	m_triangulation=a_triangulation; }

							using SurfaceSearchable::sampleImpact;
							using SurfaceSearchable::sample;

virtual sp<ImpactI>			sampleImpact(I32 a_triangleIndex,
									SpatialBary a_barycenter,
									SpatialVector a_tangent) const;
virtual SpatialTransform	sample(I32 a_triangleIndex,SpatialBary a_barycenter,
									SpatialVector a_tangent) const;

virtual	void				drawInternal(BWORD a_transformed,
									const SpatialTransform* a_pTransform,
									sp<DrawI> a_spDrawI,
									const fe::Color* a_pColor,
									sp<DrawBufferI> a_spDrawBuffer,
									sp<PartitionI> a_spPartition) const;

		void				setGroup(String a_group)
							{	m_group=a_group; }
		String				group(void) const
							{	return m_group; }

	protected:

virtual	void				resolveImpact(sp<ImpactI> a_spImpactI) const;

		Triangulation		m_triangulation;

	private:
mutable	CountedPool<Impact>	m_triangleImpactPool;
		String				m_group;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __surface_SurfaceTriangles_h__ */
