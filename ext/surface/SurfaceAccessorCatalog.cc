/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <surface/surface.pmh>

#define	FE_SAC_BIND_DEBUG	FALSE

#define	FE_SAC_MAX_VERTS	0	//* was 6 for common polys, 0 = any number
#define	FE_SAC_CHECK_LIMIT	FALSE

namespace fe
{
namespace ext
{

//* TODO shouldn't auto-expanding an array also update count values?

SurfaceAccessorCatalog::SurfaceAccessorCatalog(void):
	m_pStringList(NULL),
	m_pIntegerList(NULL),
	m_pRealList(NULL)
{
	setName("SurfaceAccessorCatalog");

	m_vectorListArray.clear();
}

SurfaceAccessorCatalog::~SurfaceAccessorCatalog(void)
{
}

void SurfaceAccessorCatalog::setAttrType(String a_attrType)
{
	if(m_attrType==a_attrType)
	{
		return;
	}
	if(!m_attrType.empty())
	{
		feLog("SurfaceAccessorCatalog::setAttrType"
				" changing type from \"%s\" to \"%s\"\n",
				m_attrType.c_str(),a_attrType.c_str());
	}

	m_attrType=a_attrType;

	m_spCatalog->catalog<String>(m_key,"type:"+m_attrName)=a_attrType;
	m_spCatalog->catalog<String>(m_key,"rate:"+m_attrName)=
			SurfaceAccessibleBase::elementLayout(m_element);

	//* TODO all types
	String attrIndexed=m_attrName;
	if(m_element==SurfaceAccessibleI::e_vertex)
	{
		attrIndexed+="[0]";
	}

	if(m_attrType=="string")
	{
		m_pStringList=&m_spCatalog->catalog< Array<String> >(
				m_key,attrIndexed);
	}
	else if(m_attrType=="integer")
	{
		m_pIntegerList=&m_spCatalog->catalog< Array<I32> >(
				m_key,attrIndexed);
	}
	else if(m_attrType=="real")
	{
		m_pRealList=&m_spCatalog->catalog< Array<Real> >(
				m_key,attrIndexed);
	}
	else if(m_attrType=="vector3")
	{
#if FE_SAC_MAX_VERTS
		if(m_attribute==SurfaceAccessibleI::e_vertices)
		{
			m_pIntegerList=&m_spCatalog->catalog< Array<I32> >(
					m_key,attrIndexed);
		}
#endif
	}
}

U32 SurfaceAccessorCatalog::count(void) const
{
	switch(m_element)
	{
		case SurfaceAccessibleI::e_point:
			return m_spCatalog->catalogOrDefault<I32>(m_key,":point_count",0);
			break;
		case SurfaceAccessibleI::e_vertex:
			break;
		case SurfaceAccessibleI::e_primitive:
			return m_spCatalog->catalogOrDefault<I32>(m_key,":prim_count",0);
			break;
		case SurfaceAccessibleI::e_detail:
			break;
		default:
			;
	}

	return 0;
}

U32 SurfaceAccessorCatalog::subCount(U32 a_index) const
{
	if(m_attribute==SurfaceAccessibleI::e_vertices)
	{
#if FE_SAC_MAX_VERTS
		const_cast<SurfaceAccessorCatalog*>(this)->setAttrType("vector3");

		FEASSERT(m_pIntegerList);

		const I32 grain=(FE_SAC_MAX_VERTS+1);
		const I32 offset=a_index*grain;

		const U32 size=m_pIntegerList->size();
		if(offset>=size)
		{
			return 0;
		}

		//* first index in granule is number of entries
		return (*m_pIntegerList)[offset];
#else
		const Array< Array<I32> >& rIntegerIntegerList=
				m_spCatalog->catalog<Array< Array<I32> > >(
				m_key,m_attrName);

		const U32 size=rIntegerIntegerList.size();
		if(a_index>=size)
		{
			return 0;
		}

		const Array<I32>& rIntegerList=
				rIntegerIntegerList[a_index];
		return rIntegerList.size();
#endif
	}

	return 0;
}

void SurfaceAccessorCatalog::set(U32 a_index,U32 subIndex,String a_string)
{
	setAttrType("string");

#if FE_SAC_CHECK_LIMIT
	if(a_index>=count())
	{
		return;
	}
#endif

	FEASSERT(m_pStringList);

	const U32 size=m_pStringList->size();
	if(a_index>=size)
	{
		m_pStringList->resize(fe::maximum(a_index+1,count()));
	}
	(*m_pStringList)[a_index]=a_string;

	for(U32 index=size;index<a_index;index++)
	{
		(*m_pStringList)[index]="";
	}
}

String SurfaceAccessorCatalog::string(U32 a_index,U32 a_subIndex)
{
	setAttrType("string");

	FEASSERT(m_pStringList);

	if(a_index<m_pStringList->size())
	{
		return (*m_pStringList)[a_index];
	}

	return "";
}

void SurfaceAccessorCatalog::set(U32 a_index,U32 a_subIndex,I32 a_integer)
{
//	feLog("SurfaceAccessorCatalog::set %d %d %d %d\n",
//			a_index,a_subIndex,a_integer,
//			(m_attribute==SurfaceAccessibleI::e_vertices));

#if FE_SAC_CHECK_LIMIT
	if(a_index>=count())
	{
		return;
	}
#endif

	if(m_attribute==SurfaceAccessibleI::e_vertices)
	{
#if FE_SAC_MAX_VERTS
		if(a_subIndex>=FE_SAC_MAX_VERTS)
		{
			feX("SurfaceAccessorCatalog::set","subIndex %d exceeds max %d",
					a_subIndex,FE_SAC_MAX_VERTS-1);
			return;
		}

		setAttrType("vector3");

		FEASSERT(m_pIntegerList);

		const I32 grain=(FE_SAC_MAX_VERTS+1);
		const I32 offset=a_index*grain;

		const U32 size=m_pIntegerList->size();
		if(offset>=size)
		{
			m_pIntegerList->resize(
					fe::maximum(offset+grain,I32(count()*grain)));
		}

		//* first index in granule is number of entries
		I32& rEntryCount=(*m_pIntegerList)[offset];
		if(rEntryCount<a_subIndex+1)
		{
			rEntryCount=a_subIndex+1;
		}
		(*m_pIntegerList)[offset+1+a_subIndex]=a_integer;
#else
		Array< Array<I32> >& rIntegerIntegerList=
				m_spCatalog->catalog< Array< Array<I32> > >(
				m_key,m_attrName);

		const U32 size=rIntegerIntegerList.size();
		if(a_index>=size)
		{
			rIntegerIntegerList.resize(fe::maximum(a_index+1,count()));
			m_spCatalog->catalog<String>(m_key,"rate:"+m_attrName)="prim";
		}

		Array<I32>& rIntegerList=rIntegerIntegerList[a_index];

		const U32 subSize=rIntegerList.size();
		if(a_subIndex>=subSize)
		{
			rIntegerList.resize(a_subIndex+1);
		}
		rIntegerList[a_subIndex]=a_integer;
#endif
		return;
	}

	setAttrType("integer");

	FEASSERT(m_pIntegerList);

	const U32 size=m_pIntegerList->size();
	if(a_index>=size)
	{
		m_pIntegerList->resize(fe::maximum(a_index+1,count()));
	}
	(*m_pIntegerList)[a_index]=a_integer;

	for(U32 index=size;index<a_index;index++)
	{
		(*m_pIntegerList)[index]=0;
	}
}

I32 SurfaceAccessorCatalog::integer(U32 a_index,U32 a_subIndex)
{
	if(m_attribute==SurfaceAccessibleI::e_vertices)
	{
#if FE_SAC_MAX_VERTS
		if(a_subIndex>=FE_SAC_MAX_VERTS)
		{
			feX("SurfaceAccessorCatalog::integer","subIndex %d exceeds max %d",
					a_subIndex,FE_SAC_MAX_VERTS-1);
			return -1;
		}

		setAttrType("vector3");

		FEASSERT(m_pIntegerList);

		const I32 grain=(FE_SAC_MAX_VERTS+1);
		const I32 offset=a_index*grain;

		const U32 size=m_pIntegerList->size();
		if(offset>=size)
		{
			return -1;
		}

		//* first index in granule is number of entries
		const U32 entryCount=(*m_pIntegerList)[offset];
		if(a_subIndex>=entryCount)
		{
			return -1;
		}
		return (*m_pIntegerList)[offset+1+a_subIndex];
#else
		const Array< Array<I32> >& rIntegerIntegerList=
				m_spCatalog->catalog< Array< Array<I32> > >(
				m_key,m_attrName);

		const Array<I32>& rIntegerList=rIntegerIntegerList[a_index];
		return rIntegerList[a_subIndex];
#endif
	}

	setAttrType("integer");

	FEASSERT(m_pIntegerList);

	if(a_index<m_pIntegerList->size())
	{
		return (*m_pIntegerList)[a_index];
	}

	return 0;
}

//* coordinate with SurfaceAccessorBase::append(I32)
I32 SurfaceAccessorCatalog::append(I32 a_integer)
{
	//* lot of points or primitives
	if(m_element==SurfaceAccessibleI::e_point ||
			(m_element==SurfaceAccessibleI::e_primitive &&
			m_attribute!=SurfaceAccessibleI::e_vertices))
	{
		if(a_integer<1)
		{
			return -1;
		}

		const I32 first=count();

		switch(m_element)
		{
			case SurfaceAccessibleI::e_point:
				m_spCatalog->catalog<I32>(m_key,":point_count")=first+a_integer;
				break;
			case SurfaceAccessibleI::e_vertex:
				break;
			case SurfaceAccessibleI::e_primitive:
				m_spCatalog->catalog<I32>(m_key,":prim_count")=first+a_integer;
				break;
			case SurfaceAccessibleI::e_detail:
				break;
			default:
				return -1;
		}

		return first;
	}

	if(m_element!=SurfaceAccessibleI::e_primitive ||
			m_attribute!=SurfaceAccessibleI::e_vertices)
	{
		append(U32(0),a_integer);
		return 0;
	}

	//* append a primitive with the specified number of vertices
	sp<SurfaceAccessorI> spPointAccessor=m_spSurfaceAccessibleI->accessor(
			SurfaceAccessibleI::e_point,SurfaceAccessibleI::e_position);

	const I32 primitiveIndex=append();
	for(I32 vertIndex=0;vertIndex<a_integer;vertIndex++)
	{
		const I32 pointIndex=spPointAccessor->append();
		append(primitiveIndex,pointIndex);
	}

	return primitiveIndex;
}

I32 SurfaceAccessorCatalog::append(void)
{
	//* point or primitive

	const U32 next=count();

	switch(m_element)
	{
		case SurfaceAccessibleI::e_point:
			m_spCatalog->catalog<I32>(m_key,":point_count")=next+1;
			break;
		case SurfaceAccessibleI::e_vertex:
			break;
		case SurfaceAccessibleI::e_primitive:
			m_spCatalog->catalog<I32>(m_key,":prim_count")=next+1;
			break;
		case SurfaceAccessibleI::e_detail:
			break;
		default:
			break;
	}

	return next;
}

I32 SurfaceAccessorCatalog::append(SurfaceAccessibleI::Form a_form)
{
	//* TODO
	return -1;
}

void SurfaceAccessorCatalog::append(U32 a_index,I32 a_integer)
{
	//* vertex of primitive
	const I32 vertIndex=subCount(a_index);
	set(a_index,vertIndex,a_integer);
}

//* coordinate with SurfaceAccessorBase::append(a_rPrimVerts)
void SurfaceAccessorCatalog::append(
	Array< Array<I32> >& a_rPrimVerts)
{
	if(m_element!=SurfaceAccessibleI::e_primitive ||
			m_attribute!=SurfaceAccessibleI::e_vertices)
	{
		return;
	}

	const U32 appendCount=a_rPrimVerts.size();

	I32 primitiveIndex=
			m_spCatalog->catalogOrDefault<I32>(m_key,":prim_count",0);
	m_spCatalog->catalog<I32>(m_key,":prim_count")=primitiveIndex+appendCount;

#if FE_SAC_MAX_VERTS
	const I32 grain=(FE_SAC_MAX_VERTS+1);

	setAttrType("vector3");

	FEASSERT(m_pIntegerList);

	for(U32 appendIndex=0;appendIndex<appendCount;appendIndex++)
	{
		const Array<I32>& rVertList=a_rPrimVerts[appendIndex];

		const I32 offset=primitiveIndex*grain;

		const U32 size=m_pIntegerList->size();
		if(offset>=size)
		{
			m_pIntegerList->resize(
					fe::maximum(offset+grain,I32(count()*grain)));
		}

		const U32 trueCount=rVertList.size();
		const U32 subCount=fe::minimum(I32(trueCount),FE_SAC_MAX_VERTS);

		if(subCount<trueCount)
		{
			feLog("SurfaceAccessorCatalog::append"
					" vertex count %d exceeds limit %d\n",
					trueCount,FE_SAC_MAX_VERTS);
		}

		//* first index in granule is number of entries
		(*m_pIntegerList)[offset]=subCount;
		for(U32 subIndex=0;subIndex<subCount;subIndex++)
		{
			(*m_pIntegerList)[offset+1+subIndex]=rVertList[subIndex];
		}
		primitiveIndex++;
	}
#else
	for(U32 appendIndex=0;appendIndex<appendCount;appendIndex++)
	{
		const Array<I32>& rVertList=a_rPrimVerts[appendIndex];

		const U32 subCount=rVertList.size();
		for(U32 subIndex=0;subIndex<subCount;subIndex++)
		{
			const I32 pointIndex=rVertList[subIndex];

			//* TODO direct
			append(primitiveIndex,pointIndex);	//* add vertex to primitive
		}
		primitiveIndex++;
	}
#endif

}

void SurfaceAccessorCatalog::set(U32 a_index,U32 a_subIndex,Real a_real)
{
	setAttrType("real");

	FEASSERT(m_pRealList);

#if FE_SAC_CHECK_LIMIT
	if(a_index>=count())
	{
		return;
	}
#endif

	const U32 size=m_pRealList->size();
	if(a_index>=size)
	{
		m_pRealList->resize(fe::maximum(a_index+1,count()));
	}
	(*m_pRealList)[a_index]=a_real;

	for(U32 index=size;index<a_index;index++)
	{
		(*m_pRealList)[index]=0.0;
	}
}

Real SurfaceAccessorCatalog::real(U32 a_index,U32 a_subIndex)
{
	setAttrType("real");

	FEASSERT(m_pRealList);

	if(a_index<m_pRealList->size())
	{
		return (*m_pRealList)[a_index];
	}

	return 0.0;
}

void SurfaceAccessorCatalog::set(U32 a_index,U32 a_subIndex,
								const SpatialVector& a_spatialVector)
{
//	feLog("SurfaceAccessorCatalog::set %d %d <%s> %d\n",
//			a_index,a_subIndex,c_print(a_spatialVector),
//			(m_attribute==SurfaceAccessibleI::e_vertices));

	setAttrType("vector3");

	const U32 arrayIndex=(m_attribute==SurfaceAccessibleI::e_vertices)?
			0: a_subIndex;

	bindVector(arrayIndex,TRUE);

	FEASSERT(arrayIndex<m_vectorListArray.size());

	Array<SpatialVector>* pVectorList=m_vectorListArray[arrayIndex];
	if(!pVectorList)
	{
		feLog("SurfaceAccessorCatalog::set null pVectorList\n");
		return;
	}

	U32 reIndex=a_index;
	if(m_attribute==SurfaceAccessibleI::e_vertices)
	{
		const I32 pointIndex=integer(a_index,a_subIndex);

		reIndex=pointIndex;
	}

#if FE_SAC_CHECK_LIMIT
	if(reIndex>=count())
	{
		return;
	}
#endif

	const U32 size=pVectorList->size();
	if(reIndex>=size)
	{
		pVectorList->resize(fe::maximum(reIndex+1,count()));
	}
	(*pVectorList)[reIndex]=a_spatialVector;

	for(U32 index=size;index<reIndex;index++)
	{
		fe::set((*pVectorList)[index]);
	}

//	feLog("SurfaceAccessorCatalog::set isPrim %d attr \"%s\" %d %d v %s\n",
//			m_element==SurfaceAccessibleI::e_primitive,
//			m_attrName.c_str(),
//			a_index,a_subIndex,c_print(a_spatialVector));

	return;
}
SpatialVector SurfaceAccessorCatalog::spatialVector(
	U32 a_index,U32 a_subIndex)
{
	setAttrType("vector3");

	const U32 arrayIndex=(m_attribute==SurfaceAccessibleI::e_vertices)?
			0: a_subIndex;

	bindVector(arrayIndex,FALSE);

	FEASSERT(arrayIndex<m_vectorListArray.size());

	Array<SpatialVector>* pVectorList=m_vectorListArray[arrayIndex];

	if(pVectorList)
	{
		U32 reIndex=a_index;
		if(m_attribute==SurfaceAccessibleI::e_vertices)
		{
			const I32 pointIndex=integer(a_index,a_subIndex);

			reIndex=pointIndex;
		}

		if(reIndex<pVectorList->size())
		{
//			feLog("SurfaceAccessorCatalog::spatialVector"
//					" isPrim %d attr \"%s\" %d %d v %s\n",
//					m_element==SurfaceAccessibleI::e_primitive,
//					m_attrName.c_str(),
//					a_index,a_subIndex,c_print((*pVectorList)[reIndex]));

			return (*pVectorList)[reIndex];
		}
	}

	return SpatialVector(0.0,0.0,0.0);
}

//* NOTE should have something similar for non-SpatialVector types,
//* but vertex-rate attributes are usually just normals and uvs
void SurfaceAccessorCatalog::bindVector(U32 a_subIndex,BWORD a_create)
{
	String rename=m_attrName;

	if(m_attribute==SurfaceAccessibleI::e_vertices)
	{
		rename="spc:at";
	}

	const U32 arrayCount=m_vectorListArray.size();
	if(arrayCount<=a_subIndex)
	{
		m_vectorListArray.resize(a_subIndex+1);
		for(U32 arrayIndex=arrayCount;arrayIndex<a_subIndex+1;arrayIndex++)
		{
			String attrIndexed=rename;
			if(m_element==SurfaceAccessibleI::e_vertex)
			{
				attrIndexed.sPrintf("%s[%d]",rename.c_str(),arrayIndex);
			}

			m_vectorListArray[arrayIndex]=
					(!a_create && !m_spCatalog->cataloged(m_key,attrIndexed))?
					NULL:
					&m_spCatalog->catalog< Array<SpatialVector> >(
					m_key,attrIndexed);
		}
	}
}

BWORD SurfaceAccessorCatalog::bindInternal(
	SurfaceAccessibleI::Element a_element,const String& a_name)
{
	m_element=a_element;
	m_attrName=a_name;

#if FE_SAC_BIND_DEBUG
	feLog("SurfaceAccessorCatalog::bindInternal \"%s\" \"%s\"\n",
			SurfaceAccessibleBase::elementLayout(a_element).c_str(),
			a_name.c_str());
	m_spCatalog->catalogDump();
#endif

	//* TODO handle actual groups

	if(m_element==SurfaceAccessibleI::e_pointGroup)
	{
		if(a_name.empty())
		{
			m_element=SurfaceAccessibleI::e_point;
			m_attribute=SurfaceAccessibleI::e_position;
			m_attrName="spc:at";
		}
		else
		{
			feLog("SurfaceAccessorCatalog::bindInternal"
					" point groups not implemented (\"%s\")\n",
					a_name.c_str());
		}
	}

	if(m_element==SurfaceAccessibleI::e_primitiveGroup)
	{
		if(a_name.empty())
		{
			m_element=SurfaceAccessibleI::e_primitive;
			m_attribute=SurfaceAccessibleI::e_vertices;
			m_attrName="surf:points";
		}
		else
		{
			feLog("SurfaceAccessorCatalog::bindInternal"
					" primitive groups not implemented (\"%s\")\n",
					a_name.c_str());
		}
	}

	String attrIndexed=m_attrName;

	if(m_element==SurfaceAccessibleI::e_vertex)
	{
#if FE_SAC_MAX_VERTS
		attrIndexed+="[0]";
#else
		if(m_attrName!="surf:points")
		{
			attrIndexed+="[0]";
		}
#endif
	}

	if(!m_spCatalog->cataloged(m_key,attrIndexed))
	{
#if FE_SAC_BIND_DEBUG
		feLog("SurfaceAccessorCatalog::bindInternal no \"%s\"\n",
				attrIndexed.c_str());
#endif
		return FALSE;
	}

	const String existingRate=
			m_spCatalog->catalog<String>(m_key,"rate:"+m_attrName);
	const String requestedRate=SurfaceAccessibleBase::elementLayout(m_element);
	if(existingRate!=requestedRate)
	{
#if FE_SAC_BIND_DEBUG
		feLog("SurfaceAccessorCatalog::bindInternal"
				" \"%s\" \"%s\" does not match existing rate \"%s\"\n",
				requestedRate.c_str(),attrIndexed.c_str(),
				existingRate.c_str());
#endif
		return FALSE;
	}

	if(!m_spCatalog.isValid())
	{
		feLog("SurfaceAccessorCatalog::bindInternal invalid Catalog\n");
		return FALSE;
	}

	return TRUE;
}

} /* namespace ext */
} /* namespace fe */
