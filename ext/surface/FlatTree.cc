/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <surface/surface.pmh>

namespace fe
{
namespace ext
{

FlatTree::FlatTree(void):
	m_pVertexArray(NULL),
	m_vertices(0),
	m_pBallArray(NULL)
{
//	feLog("FlatTree::FlatTree\n");
}

FlatTree::~FlatTree(void)
{
	if(m_pBallArray)
	{
		deallocate(m_pBallArray);
	}
}

void FlatTree::populate(
	const Vector3i* a_pElementArray,
	const SpatialVector* a_pVertexArray,
	const SpatialVector* a_pNormalArray,
	const Vector2* a_pUVArray,
	const Color* a_pColorArray,
	const I32* a_pTriangleIndexArray,
	const I32* a_pPointIndexArray,
	const I32* a_pPrimitiveIndexArray,
	const I32* a_pPartitionIndexArray,
	U32 a_primitives,U32 a_vertices,
	const SpatialVector& a_center,F32 a_radius)
{
	//* TODO cache a copy?
	m_pElementArray=a_pElementArray;
	m_pVertexArray=a_pVertexArray;
	m_pNormalArray=a_pNormalArray;
	m_pUVArray=a_pUVArray;
	m_pColorArray=a_pColorArray;
	m_pTriangleIndexArray=a_pTriangleIndexArray;
	m_pPointIndexArray=a_pPointIndexArray;
	m_pPrimitiveIndexArray=a_pPrimitiveIndexArray;
	m_pPartitionIndexArray=a_pPartitionIndexArray;
	m_vertices=a_vertices;

	cacheSpheres();
}

void FlatTree::cacheSpheres(void)
{
	m_ballCount=m_primitives;
	m_pBallArray=(Ball*)reallocate(m_pBallArray,m_ballCount*sizeof(Ball));
	for(U32 m=0;m<m_ballCount;m++)
	{
		SpatialVector center;
		Real radius;
		BoundingSphere<Real>::solve(&m_pVertexArray[m*3],3,center,radius);
		Vector4& rSphere=m_pBallArray[m].m_sphere;
		rSphere=center;
		rSphere[3]=radius;

//		for(U32 n=0;n<3;n++)
//		{
//			feLog("  %s\n",c_print(m_pVertexArray[m*3+n]));
//		}
//		feLog("SurfaceTriangles::cacheSpheres %d %s\n",
//				m,c_print(m_pBallArray[m].m_sphere));
	}
}

Real FlatTree::nearestPoint(const SpatialVector& a_origin,
	Real a_maxDistance,BWORD a_anyHit,U32 a_hitLimit,
	const sp<PartitionI>& a_rspPartition,
	Array< sp<SpatialTreeI::Hit> >& a_rHitArray) const
{
	if(!a_hitLimit)
	{
		return 0.0;
	}

	for(U32 primitiveIndex=0;primitiveIndex<m_primitives;primitiveIndex++)
	{
		const Vector4& rSphere=m_pBallArray[primitiveIndex].m_sphere;
		const U32 startIndex=m_pElementArray[primitiveIndex][0];
		nearestAccumulate(startIndex,
				m_pTriangleIndexArray? m_pTriangleIndexArray[startIndex]: -1,
				m_pPrimitiveIndexArray? m_pPrimitiveIndexArray[startIndex]: -1,
				m_pPartitionIndexArray? m_pPartitionIndexArray[startIndex]: -1,
				a_origin,a_hitLimit,a_rHitArray,
				rSphere,rSphere[3],
				&m_pPointIndexArray[startIndex],
				&m_pVertexArray[startIndex],
				&m_pNormalArray[startIndex]);
	}
	return a_rHitArray.size()? a_rHitArray[0]->distance(): 0.0;
}

void FlatTree::nearestAccumulate(I32 a_face,I32 a_triangleIndex,
	I32 a_primitiveIndex,I32 a_partitionIndex,
	const SpatialVector& a_origin,U32 a_hitLimit,
	Array< sp<SpatialTreeI::Hit> >& a_rHitArray,
	const SpatialVector& a_rCenter,Real a_radius,
	const I32* a_pPointIndex,
	const SpatialVector* a_pVertex,
	const SpatialVector* a_pNormal) const
{
	U32 hits=a_rHitArray.size();
	Real margin_distance=hits? a_rHitArray[hits-1]->distance(): -1.0;

	SpatialVector direction;
	if(margin_distance>=0.0)
	{
		const Real distance=PointSphereNearest<Real>::solve(a_rCenter,a_radius,
				a_origin,direction);
		if(distance>margin_distance)
		{
			return;
		}
	}

	SpatialBary barycenter;
	Vector2 uv;
	SpatialVector du;
	SpatialVector dv;
	Color color;
	SpatialVector intersection;
	Real distance=0.0;
	const I32 subCount=3;

	if(m_accuracy>SurfaceI::e_sphere)
	{
		distance=PointTriangleNearest<Real>::solve(
				a_pVertex[0],a_pVertex[1],a_pVertex[2],
				a_origin,direction,intersection,barycenter);
	}

	//* TODO
	set(uv);
	set(du);
	set(dv);
	set(color,1.0,1.0,1.0);

	if(distance>=0 && (margin_distance<0 || distance<margin_distance))
	{
		addHit(a_hitLimit,a_rHitArray,m_hitPool,
				distance,direction,intersection,
				a_face,a_triangleIndex,
				a_primitiveIndex,a_partitionIndex,
				barycenter,uv,du,dv,color,subCount,
				a_pPointIndex,a_pVertex,a_pNormal);
	}
}

void FlatTree::addHit(U32 a_hitLimit,
	Array< sp<SpatialTreeI::Hit> >& a_rHitArray,
	CountedPool<Hit>& a_rHitPool,Real a_distance,
	const SpatialVector& a_rDirection,
	const SpatialVector& a_rIntersection,
	I32 a_face,I32 a_triangleIndex,
	I32 a_primitiveIndex,I32 a_partitionIndex,
	const SpatialBary& a_rBarycenter,
	const Vector2& a_rUV,const SpatialVector& a_rDu,const SpatialVector& a_rDv,
	const Color& a_rColor,
	const I32 a_subCount,
	const I32* a_pPointIndex,
	const SpatialVector* a_pVertex,
	const SpatialVector* a_pNormal)
{
	U32 hits=a_rHitArray.size();
	if(hits<a_hitLimit)
	{
		hits++;
		a_rHitArray.resize(hits);
		a_rHitArray[hits-1]=a_rHitPool.get();
	}
	sp<Hit> spHit(a_rHitArray[hits-1]);
	spHit->m_pPointIndex[0]=a_pPointIndex? a_pPointIndex[0]: -1;
	spHit->m_pPointIndex[1]=a_pPointIndex? a_pPointIndex[1]: -1;
	spHit->m_pPointIndex[2]=a_pPointIndex? a_pPointIndex[2]: -1;
	spHit->m_pVertex[0]=a_subCount>0? a_pVertex[0]: SpatialVector(0.0,0.0,0.0);
	spHit->m_pVertex[1]=a_subCount>1? a_pVertex[1]: SpatialVector(0.0,0.0,0.0);
	spHit->m_pVertex[2]=a_subCount>2? a_pVertex[2]: SpatialVector(0.0,0.0,0.0);
	spHit->m_pNormal[0]=a_subCount>0? a_pNormal[0]: SpatialVector(0.0,1.0,0.0);
	spHit->m_pNormal[1]=a_subCount>1? a_pNormal[1]: SpatialVector(0.0,1.0,0.0);
	spHit->m_pNormal[2]=a_subCount>2? a_pNormal[2]: SpatialVector(0.0,1.0,0.0);
	spHit->m_face=a_face;
	spHit->m_uv=a_rUV;
	spHit->m_du=a_rDu;
	spHit->m_dv=a_rDv;
	spHit->m_color=a_rColor;
	spHit->m_triangleIndex=a_triangleIndex;
	spHit->m_primitiveIndex=a_primitiveIndex;
	spHit->m_partitionIndex=a_partitionIndex;
	spHit->m_barycenter=a_rBarycenter;
	spHit->m_intersection=a_rIntersection;
	spHit->m_direction=a_rDirection;
	spHit->m_distance=a_distance;

	std::sort(a_rHitArray.begin(),a_rHitArray.end(),Sort());
}

Real FlatTree::rayImpact(const SpatialVector& a_origin,
	const SpatialVector& a_direction,Real a_maxDistance,BWORD a_anyHit,
	U32 a_hitLimit,const sp<PartitionI>& a_rspPartition,
	Array< sp<SpatialTreeI::Hit> >& a_rHitArray) const
{
	if(!a_hitLimit)
	{
		return 0.0;
	}

	Real best_distance= -1.0f;

	for(U32 primitiveIndex=0;primitiveIndex<m_primitives;primitiveIndex++)
	{
		const Vector4& rSphere=m_pBallArray[primitiveIndex].m_sphere;
		const U32 startIndex=m_pElementArray[primitiveIndex][0];
		const SpatialVector center=rSphere;
		const Real radius=rSphere[3];
		Real distance=RaySphereIntersect<Real>::solve(center,radius,
				a_origin,a_direction);
		if(distance<0.0f)
		{
			continue;
		}

		SpatialBary barycenter(0.0,1.0,0.0);
		if(m_accuracy>SurfaceI::e_sphere)
		{
			distance=RayTriangleIntersect<Real>::solve(
					m_pVertexArray[startIndex],
					m_pVertexArray[startIndex+1],
					m_pVertexArray[startIndex+2],
					a_origin,a_direction,barycenter);
		}
		else
		{
			distance=0.0;
		}

		if(distance>=0.0 && (best_distance<0.0f ||
				distance<best_distance))
		{
			best_distance=distance;

			sp<Hit> spHit=m_hitPool.get();
			a_rHitArray.resize(1);
			a_rHitArray[0]=spHit;
			spHit->m_pPointIndex[0]=m_pPointIndexArray[startIndex];
			spHit->m_pPointIndex[1]=m_pPointIndexArray[startIndex+1];
			spHit->m_pPointIndex[2]=m_pPointIndexArray[startIndex+2];
			spHit->m_pVertex[0]=m_pVertexArray[startIndex];
			spHit->m_pVertex[1]=m_pVertexArray[startIndex+1];
			spHit->m_pVertex[2]=m_pVertexArray[startIndex+2];
			spHit->m_pNormal[0]=m_pNormalArray[startIndex];
			spHit->m_pNormal[1]=m_pNormalArray[startIndex+1];
			spHit->m_pNormal[2]=m_pNormalArray[startIndex+2];
			spHit->m_face=startIndex;
			spHit->m_uv=m_pUVArray? m_pUVArray[startIndex]: Vector2(0.0,0.0);
			spHit->m_color=m_pColorArray?
					m_pColorArray[startIndex]: Color(1.0,1.0,1.0);
			spHit->m_triangleIndex=m_pTriangleIndexArray?
					m_pTriangleIndexArray[startIndex]: -1;
			spHit->m_pointIndex=m_pPointIndexArray?
					m_pPointIndexArray[startIndex]: -1;
			spHit->m_primitiveIndex=m_pPrimitiveIndexArray?
					m_pPrimitiveIndexArray[startIndex]: -1;
			spHit->m_partitionIndex=m_pPartitionIndexArray?
					m_pPartitionIndexArray[startIndex]: -1;
			spHit->m_barycenter=barycenter;
			spHit->m_direction=a_direction;
			spHit->m_intersection=a_origin+distance*a_direction;
			spHit->m_distance=distance;

			if(a_anyHit)
			{
				return best_distance;
			}
		}
	}

	return best_distance;
}

} /* namespace ext */
} /* namespace fe */