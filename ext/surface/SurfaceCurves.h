/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __surface_SurfaceCurve_h__
#define __surface_SurfaceCurve_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Segmented Path Surface

	@ingroup surface

	This surface presumes uniform cv counts.
*//***************************************************************************/
class FE_DL_EXPORT SurfaceCurves:
	public SurfaceSearchable,
	public CastableAs<SurfaceCurves>
{
	public:
	class FE_DL_EXPORT Impact:
		public SurfaceSearchable::Impact,
		public CastableAs<Impact>
	{
		public:
								Impact(void)
								{
#if FE_COUNTED_STORE_TRACKER
									setName("SurfaceCurves::Impact");
#endif
								}
	virtual						~Impact(void)								{}

								using SurfaceSearchable::Impact::draw;

	virtual	void				draw(const SpatialTransform& a_rTransform,
										sp<DrawI> a_spDrawI,
										const fe::Color* a_pColor,
										sp<DrawBufferI> a_spDrawBuffer,
										sp<PartitionI> a_spPartition) const;
	};

							SurfaceCurves(void);
virtual						~SurfaceCurves(void)							{}

							//* As Protectable
virtual	Protectable*		clone(Protectable* pInstance=NULL);

							using SurfaceSearchable::sampleImpact;
							using SurfaceSearchable::sample;

							//* As SurfaceI
virtual sp<ImpactI>			sampleImpact(I32 a_triangleIndex,
									SpatialBary a_barycenter,
									SpatialVector a_tangent) const;
virtual	SpatialTransform	sample(I32 a_triangleIndex,SpatialBary a_barycenter,
									SpatialVector a_tangent) const;

virtual SpatialTransform	sample(Vector2 a_uv) const;

	protected:

virtual	void				drawInternal(BWORD a_transformed,
									const SpatialTransform* a_pTransform,
									sp<DrawI> a_spDrawI,
									const fe::Color* a_pColor,
									sp<DrawBufferI> a_spDrawBuffer,
									sp<PartitionI> a_spPartition) const;

/*
	class Curve
	{
		public:
							Curve(U32 a_start, U32 a_count):
								m_start(a_start),
								m_count(a_count)							{}

			U32				start(void) const	{ return m_start; }
			U32				count(void) const	{ return m_count; }

		private:
			U32				m_start;
			U32				m_count;
	};
*/

//		Array<Curve>		m_curveArray;

virtual	void				cache(void);

virtual	void				resolveImpact(sp<ImpactI> a_spImpactI) const;

	private:
mutable	CountedPool<Impact>	m_curveImpactPool;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __surface_SurfaceCurve_h__ */


