/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __surface_MapTree_h__
#define __surface_MapTree_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Map of named subtrees

	@ingroup surface

	Each named partition is converted to an independent subtree.
	This should effective when most searches are for one simple name at a time.
	This could be very inefficient when patterns match many partitions at once.

	While the notion is like a map, the partitions have been converted to
	consecutive indexes, so the subtrees are actually stored in an array.
*//***************************************************************************/
class FE_DL_EXPORT MapTree: public SpatialTreeBase
{
	public:

				MapTree(void);
virtual			~MapTree(void);

virtual	void	setRefinement(U32 a_refinement)	{ m_refinement=a_refinement; }

virtual	void	populate(
						const Vector3i* a_pElementArray,
						const SpatialVector* a_pVertexArray,
						const SpatialVector* a_pNormalArray,
						const Vector2* a_pUVArray,
						const Color* a_pColorArray,
						const I32* a_pTriangleIndexArray,
						const I32* a_pPointIndexArray,
						const I32* a_pPrimitiveIndexArray,
						const I32* a_pPartitionIndexArray,
						U32 a_primitives,U32 a_vertices,
						const SpatialVector& a_center,F32 a_radius);
virtual	Real	nearestPoint(const SpatialVector& a_origin,
						Real a_maxDistance,BWORD a_anyHit,U32 a_hitLimit,
						const sp<PartitionI>& a_rspPartition,
						Array< sp<SpatialTreeI::Hit> >& a_rHitArray)
						const;
virtual	Real	rayImpact(const SpatialVector& a_origin,
						const SpatialVector& a_direction,
						Real a_maxDistance,BWORD a_anyHit,U32 a_hitLimit,
						const sp<PartitionI>& a_rspPartition,
						Array< sp<SpatialTreeI::Hit> >& a_rHitArray)
						const;

	private:

		void	accumulateHits(Array< sp<SpatialTreeI::Hit> >& a_rDestination,
						Array< sp<SpatialTreeI::Hit> >& a_rSource,
						U32 a_hitLimit) const;

		std::vector< sp<SpatialTreeI> >			m_treeArray;
		std::vector< std::vector<Vector3i> >	m_elementArrays;

		U32										m_refinement;

		void	populateSubTree(I32 a_treeIndex);

		const Vector3i*							m_pElementArray;
		const SpatialVector*					m_pVertexArray;
		const SpatialVector*					m_pNormalArray;
		const Vector2*							m_pUVArray;
		const Color*							m_pColorArray;
		const I32*								m_pTriangleIndexArray;
		const I32*								m_pPointIndexArray;
		const I32*								m_pPrimitiveIndexArray;
		const I32*								m_pPartitionIndexArray;
		U32										m_primitives;
		U32										m_vertices;
		SpatialVector							m_center;
		F32										m_radius;

	class Populator: public Thread::Functor
	{
		public:
							Populator(sp< JobQueue<I32> > a_spJobQueue,
									U32 a_id,String a_stage):
								m_spJobQueue(a_spJobQueue),
								m_id(a_id)									{}

			void			operate(void);

			void			setObject(sp<Component> spMapTree)
							{	m_spMapTree=spMapTree; }

		private:
			sp< JobQueue<I32> > m_spJobQueue;
			U32					m_id;
			sp<MapTree>			m_spMapTree;
	};
};

} /* namespace ext */
} /* namespace fe */

#endif /* __surface_MapTree_h__ */
