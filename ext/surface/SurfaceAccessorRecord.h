/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __surface_SurfaceAccessorRecord_h__
#define __surface_SurfaceAccessorRecord_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
    @brief Accessor for a Record-based Surface

	@ingroup surface
*//***************************************************************************/
class FE_DL_EXPORT SurfaceAccessorRecord:
	virtual public SurfaceAccessorBase,
	public CastableAs<SurfaceAccessorRecord>
{
	public:
						SurfaceAccessorRecord(void);
virtual					~SurfaceAccessorRecord(void);

						using SurfaceAccessorBase::set;
						using SurfaceAccessorBase::append;
						using SurfaceAccessorBase::spatialVector;

						//* as SurfaceAccessorI
virtual	void			setWritable(BWORD a_writable);

virtual	U32				count(void) const;
virtual	U32				subCount(U32 a_index) const;

virtual	String			type(void) const;

virtual	void			set(U32 a_index,U32 a_subIndex,String a_string);
virtual	String			string(U32 a_index,U32 a_subIndex=0);

virtual	void			set(U32 a_index,U32 a_subIndex,I32 a_integer);
virtual	I32				integer(U32 a_index,U32 a_subIndex=0);

virtual	void			append(U32 a_index,I32 a_integer);
virtual	I32				append(I32 a_integer,SurfaceAccessibleI::Form a_form);
virtual	I32				append(SurfaceAccessibleI::Form a_form);
virtual	void			append(Array< Array<I32> >& a_rPrimVerts);

virtual	void			remove(U32 a_index,I32 a_integer=0);

virtual	void			set(U32 a_index,U32 a_subIndex,Real a_real);
virtual	Real			real(U32 a_index,U32 a_subIndex=0);

virtual	void			set(U32 a_index,U32 a_subIndex,
								const SpatialVector& a_vector);
virtual	SpatialVector	spatialVector(U32 a_index,U32 a_subIndex=0);
virtual	BWORD			spatialVector(SpatialVector* a_pVectorArray,
								const Vector2i* a_pIndexArray,I32 a_arrayCount);

		BWORD			bind(SurfaceAccessibleI::Element a_element,
								SurfaceAccessibleI::Attribute a_attribute)
						{
							m_attribute=a_attribute;

							String name;
							switch(a_attribute)
							{
								case SurfaceAccessibleI::e_generic:
								case SurfaceAccessibleI::e_position:
									name="spc:at";
									break;
								case SurfaceAccessibleI::e_normal:
									name="spc:up";
									break;
								case SurfaceAccessibleI::e_uv:
									name="surf:uvw";
									break;
								case SurfaceAccessibleI::e_color:
									name="surf:color";
									break;
								case SurfaceAccessibleI::e_vertices:
									name="surf:points";
									break;
//									m_attrName="vertices";
//									FEASSERT(a_element==
//											SurfaceAccessibleI::e_primitive);
//									m_element=a_element;
//									return TRUE;
								case SurfaceAccessibleI::e_properties:
									name="properties";
									break;
//									m_attrName="properties";
//									FEASSERT(a_element==
//											SurfaceAccessibleI::e_primitive);
//									m_element=a_element;
//									return TRUE;
							}
							return bindInternal(a_element,name);
						}
		BWORD			bind(SurfaceAccessibleI::Element a_element,
								const String& a_name)
						{
							String name=a_name;
							m_attribute=SurfaceAccessibleI::e_generic;

							if(name=="P")
							{
								name="spc:at";
								m_attribute=SurfaceAccessibleI::e_position;
							}
							else if(name=="N")
							{
								name="spc:up";
								m_attribute=SurfaceAccessibleI::e_normal;
							}
							else if(name=="uv")
							{
								name="surf:uvw";
								m_attribute=SurfaceAccessibleI::e_uv;
							}
							else if(name=="Cd")
							{
								name="surf:color";
								m_attribute=SurfaceAccessibleI::e_color;
							}

							return bindInternal(a_element,name);
						}

		void			bind(WeakRecord& a_rRecord)
						{	m_weakRecord=a_rRecord; }

	private:

virtual	BWORD			bindInternal(SurfaceAccessibleI::Element a_element,
								const String& a_name);

		sp<RecordArray>	recordArray(void) const;
		void			ensureType(String a_attrType);
		BWORD			ensurePrimitiveArray(void);
		void			ensureGroupSetRecord(BWORD a_create);
		I32				ensureVertex(U32 a_index,U32 a_subIndex);
		BWORD			ensurePointRV(void);

		WeakRecord						m_weakRecord;
		sp<RecordGroup>					m_spRG;
		sp<RecordArray>					m_spRecordArray;
		sp<RecordGroup>					m_spPrimitiveRG;	//* for e_vertex
		sp<RecordArray>					m_spPrimitiveArray;	//* for e_vertex
		sp<Layout>						m_spLayout;
		RecordArrayView<SurfacePoint>	m_surfaceElementRAV;
		RecordArrayView<SurfacePoint>	m_surfacePointRAV;	//* for e_vertices

		Accessor<String>				m_accessorString;
		Accessor<I32>					m_accessorInteger;
		Accessor<Real>					m_accessorReal;
		Accessor<SpatialVector>			m_accessorSpatialVector;
		Accessor<Color>					m_accessorColor;

		Accessor< Array<I32> >			m_vertexIndexAccessor;

		Record							m_groupSetRecord;
		Accessor<String>				m_groupNameAccessor;
		Accessor< std::set<I32> >		m_groupSetAccessor;

		sp<SurfaceAccessibleBase::MultiGroup>	m_spMultiGroup;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __surface_SurfaceAccessorRecord_h__ */
