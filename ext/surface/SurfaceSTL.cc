/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <surface/surface.pmh>

#define FE_SSTL_CACHE_DEBUG		FALSE
#define FE_SSTL_CACHE_VERBOSE	FALSE

namespace fe
{
namespace ext
{

SurfaceSTL::SurfaceSTL(void)
{
#if FE_SSTL_CACHE_DEBUG
	feLog("SurfaceSTL()\n");
#endif
}

void SurfaceSTL::initialize(void)
{
#if FE_SSTL_CACHE_DEBUG
	feLog("SurfaceSTL:initialize\n");
#endif
}

Protectable* SurfaceSTL::clone(Protectable* pInstance)
{
#if FE_SSTL_CACHE_DEBUG
	feLog("SurfaceSTL::clone\n");
#endif

	SurfaceSTL* pSurfaceSTL= pInstance?
			fe_cast<SurfaceSTL>(pInstance): new SurfaceSTL();

	checkCache();

	SurfaceTriangles::clone(pSurfaceSTL);

	//* copy attributes

	return pSurfaceSTL;
}

/***
`STL ascii example:'

solid NAME

facet normal ni nj nk
    outer loop
        vertex v1x v1y v1z
        vertex v2x v2y v2z
        vertex v3x v3y v3z
    endloop
	[repeat ...]
endfacet

endsolid NAME

`STL binary layout:'

UINT8[80]    - Header                 -     80 bytes
UINT32       - Number of triangles    -      4 bytes

foreach triangle                      - 50 bytes:
    REAL32[3] - Normal vector             - 12 bytes
    REAL32[3] - Vertex 1                  - 12 bytes
    REAL32[3] - Vertex 2                  - 12 bytes
    REAL32[3] - Vertex 3                  - 12 bytes
    UINT16    - Attribute byte count      -  2 bytes
end
*/

void SurfaceSTL::cache(void)
{
#if FE_SSTL_CACHE_DEBUG
	feLog("SurfaceSTL::cache\n");
#endif

	if(!m_record.isValid())
	{
		feLog("SurfaceSTL::cache record invalid\n");
		return;
	}

	SurfaceTriangles::cache();

	SurfaceFile surfaceFileRV;
	surfaceFileRV.bind(m_record);

	String filename=surfaceFileRV.filename();
	if(filename.empty())
	{
		feLog("SurfaceSTL::cache no filename\n");
		return;
	}

	std::ifstream stream;

	stream.open(filename.c_str(),std::fstream::in|std::fstream::binary);
	if(!stream.good() && filename.c_str()[0]!='/')
	{
		const String path=
				registry()->master()->catalog()->catalog<String>("path:media")+
				"/"+filename;
		stream.open(path.c_str(),std::fstream::in|std::fstream::binary);
	}

	if(!stream.good())
	{
		feLog("SurfaceSTL::cache failed to open \"%s\"\n",filename.c_str());
		return;
	}

	//* TODO fuse vertices into a mesh, not just discrete triangles

	char firstFive[5];
	stream.read(firstFive,5);
	if(!strncmp(firstFive,"solid",5))
	{
		feLog("SurfaceSTL::cache acsii STL not supported, reading \"%s\"\n",
				filename.c_str());
		return;
	}

	//* skip past header
	stream.clear();
	stream.seekg(80,std::ios::beg);

	U32 triangleCount(13);
	stream.read(reinterpret_cast<char*>(&triangleCount),sizeof(triangleCount));

#if FE_SSTL_CACHE_DEBUG
	feLog("SurfaceSTL::cache triangleCount %d\n",triangleCount);
#endif

	U32 partitionIndex= -1;

	m_elements=triangleCount;
	m_vertices=triangleCount*3;
	U32 elementAllocated=0;
	U32 vertexAllocated=0;

	U32 total=0;
	U32 primitiveIndex=0;

	resizeFor(m_elements,m_vertices,elementAllocated,vertexAllocated);

	Vector3f norm;
	Vector3f vert[3];
	U16 attributeByteCount;

	for(U32 triangleIndex=0;triangleIndex<triangleCount;triangleIndex++)
	{
		stream.read(reinterpret_cast<char*>(&norm),sizeof(norm));
		stream.read(reinterpret_cast<char*>(&vert[0]),sizeof(vert[0]));
		stream.read(reinterpret_cast<char*>(&vert[1]),sizeof(vert[1]));
		stream.read(reinterpret_cast<char*>(&vert[2]),sizeof(vert[2]));
		stream.read(reinterpret_cast<char*>(&attributeByteCount),
				sizeof(attributeByteCount));

#if FE_SSTL_CACHE_VERBOSE
		feLog("SurfaceSTL::cache triangle %d/%d\n",
				triangleIndex,triangleCount);
		feLog("SurfaceSTL::cache   norm %s\n",c_print(norm));
		feLog("SurfaceSTL::cache   vert[0] %s\n",c_print(vert[0]));
		feLog("SurfaceSTL::cache   vert[1] %s\n",c_print(vert[1]));
		feLog("SurfaceSTL::cache   vert[2] %s\n",c_print(vert[2]));
		feLog("SurfaceSTL::cache   abc %d\n",c_print(attributeByteCount));
#endif

		m_pElementArray[primitiveIndex]=Vector3i(total,3,FALSE);

		for(I32 subIndex=0;subIndex<3;subIndex++)
		{
			m_pVertexArray[total]=vert[subIndex];
			m_pNormalArray[total]=norm;

			m_pTriangleIndexArray[total]=primitiveIndex;
			m_pPointIndexArray[total]=total;
			m_pPrimitiveIndexArray[total]=primitiveIndex;
			m_pPartitionIndexArray[total]=partitionIndex;

			total++;
		}

		primitiveIndex++;
	}

	stream.close();

	m_vertices=total;
	m_elements=primitiveIndex;

#if FE_SSTL_CACHE_DEBUG
	feLog("SurfaceSTL::cache name %s verts %d faces %d\n",
			verboseName().c_str(),m_vertices,m_elements);
#endif

	//* TODO uv
	deallocate(m_pUVArray);
	m_pUVArray=NULL;

	//* TODO color
	deallocate(m_pColorArray);
	m_pColorArray=NULL;

	calcBoundingSphere();

#if FE_SSTL_CACHE_DEBUG
	feLog("SurfaceSTL::cache center %s radius %.6G\n",
			c_print(m_center),m_radius);
#endif
}

} /* namespace ext */
} /* namespace fe */
