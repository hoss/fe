/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __surface_Raster_h__
#define __surface_Raster_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Raster RecordView

	@ingroup surface
*//***************************************************************************/
class FE_DL_EXPORT Raster: virtual public RecordView
{
	public:
		Functor<SpatialVector>			minimum;
		Functor<SpatialVector>			maximum;
		Functor<Vector4i>				span;
		Functor< Array<Real> >			data;

				Raster(void)			{ setName("Raster"); }
virtual	void	addFunctors(void)
				{
					add(minimum,	FE_SPEC("surf:min",
							""));
					add(maximum,	FE_SPEC("surf:max",
							""));
					add(span,		FE_SPEC("surf:span",
							""));
					add(data,		FE_SPEC("surf:data",
							""));
				}
virtual	void	initializeRecord(void)
				{
					feLog("Raster::initializeRecord\n");

					set(minimum());
					set(maximum());
					set(span());
				}
};

} /* namespace ext */
} /* namespace fe */

#endif /* __surface_Raster_h__ */




