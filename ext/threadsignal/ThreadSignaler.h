/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __threadsignal_ThreadSignaler_h__
#define __threadsignal_ThreadSignaler_h__

namespace fe
{
namespace ext
{

/**	@brief SignalerI that calls registered HandlerI instances in parallel

	@ingroup signal
*/
class FE_DL_EXPORT ThreadSignaler:
		public ChainSignaler,
		public Initialize<ThreadSignaler>
{
	public:
					ThreadSignaler(void);
virtual				~ThreadSignaler(void);

		void		initialize(void);

	protected:

virtual	void		signalEntries(Record &signal, t_entrymap *a_entryMap);

	private:

	class SignalWorker: public Thread::Functor
	{
		public:
							SignalWorker(
								sp< JobQueue< sp<HandlerI> > > a_spJobQueue,
								U32 a_id,String a_stage):
								m_id(a_id),
								m_hpJobQueue(a_spJobQueue)					{}

	virtual					~SignalWorker(void)								{}

	virtual	void			operate(void);

			void			setObject(sp<Component> spObject)
							{	m_hpThreadSignaler=spObject; }

		private:
			U32								m_id;
			hp< JobQueue< sp<HandlerI> > >	m_hpJobQueue;
			hp<ThreadSignaler>				m_hpThreadSignaler;
	};

		Record	m_signal;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __threadsignal_ThreadSignaler_h__ */

