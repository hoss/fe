/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "driveline/driveline.pmh"

namespace fe
{
namespace ext
{

DrivelineItem::DrivelineItem(void)
{
}

DrivelineItem::~DrivelineItem(void)
{
}

SemiImplicitDriveline::SemiImplicitDriveline(void)
{
}

SemiImplicitDriveline::~SemiImplicitDriveline(void)
{
}

void SemiImplicitDriveline::initialize(void)
{
}

void SemiImplicitDriveline::step(t_moa_real a_dt)
{
	t_solve_real totalConstraintForce;
//m_spSemiImplicit->setRatio("dummy", 1111.1);
	m_spSemiImplicit->step(a_dt, totalConstraintForce);
}


void SemiImplicitDriveline::sync(void)
{

	// extract is expensive, avoid inner loops
	//m_spSemiImplicit->shiftDomain(2.0*fe::pi, m_asSolverParticle.index(r_shaft));
//m_spSemiImplicit->propogateRatiosFrom(0);
//m_spSemiImplicit->calculateShiftPeriod();
m_spSemiImplicit->reconfigure();

	m_spSemiImplicit->extract(m_rg_driveline);
	for( std::map< String, Shaft >::iterator i_shafts = m_shafts.begin(); i_shafts != m_shafts.end(); i_shafts++)
	{
		Record r_shaft = i_shafts->second.m_r_parent;
#if 0
		t_solve_real fspins = i_shafts->second.m_spHookForce->location() / (2.0 * fe::pi);
		int spins = (int)fspins;
		t_solve_real phase = i_shafts->second.m_spHookForce->location() - (2.0*fe::pi * (t_solve_real)spins);
		if(phase < 0.0) { phase += 2.0*fe::pi; }
		fe_fprintf(stderr, "SHAFT R [%s] %d %g %g | %d %4.0f\n",
			i_shafts->first.c_str(), m_asParticle1D.check(r_shaft),
			m_asParticle1D.location(r_shaft),
			i_shafts->second.m_spHookForce->location(),
			spins, phase / (2.0 * fe::pi) * 360.0);
#endif

		//i_shafts->second.m_spHookForce->set(0.1);
		//m_asSolverParticle.externalForce(r_shaft) = 1.0f;
		if(i_shafts->second.m_has_externalTorque)
		{
//t_solve_real ext_t = m_asExternalTorque.externalTorque(r_shaft);
//fe_fprintf(stderr, "ext tor %g\n", ext_t);
			i_shafts->second.m_spHookForce->set(m_asExternalTorque.externalTorque(r_shaft));
		}
	}

//m_spSemiImplicit->shiftDomain(2.0*fe::pi*8.0, 3);
m_spSemiImplicit->shiftDomain();
m_spSemiImplicit->extract(m_rg_driveline);

#if 0
	for( std::map< String, Shaft >::iterator i_shafts = m_shafts.begin(); i_shafts != m_shafts.end(); i_shafts++)
	{
		Record r_shaft = i_shafts->second.m_r_parent;
		t_solve_real fspins = i_shafts->second.m_spHookForce->location() / (2.0 * fe::pi);
		int spins = (int)fspins;
		t_solve_real phase = i_shafts->second.m_spHookForce->location() - (2.0*fe::pi * (t_solve_real)spins);
		if(phase < 0.0) { phase += 2.0*fe::pi; }
		fe_fprintf(stderr, ">SHAFT R [%s] %d %g %g | %d %4.0f | %g\n",
			i_shafts->first.c_str(), m_asParticle1D.check(r_shaft),
			m_asParticle1D.location(r_shaft),
			i_shafts->second.m_spHookForce->location(),
			spins, phase / (2.0 * fe::pi) * 360.0,
			i_shafts->second.m_spHookForce->velocity());
	}
#endif

}


bool SemiImplicitDriveline::compile(fe::Record r_driveline)
{
fe_fprintf(stderr, "----- SemiImplicitDriveline::compile\n");
	sp<Scope> spScope = r_driveline.layout()->scope();
	m_spSemiImplicit = new SemiImplicit1D();
	m_spSemiImplicit->initialize(spScope);

	{
		sp<DrivelineItem> spForce(new TorsionSpring(m_spSemiImplicit));
		m_spSemiImplicit->addForce(spForce, true);
		//spForce->setDriveline( sp<DrivelineI>(this) );
	}

	AsDriveline asDriveline;
	asDriveline.bind(spScope);

	m_asParticle1D.bind(spScope);
	m_asShaft.bind(spScope);
	m_asSolverParticle.bind(spScope);
	m_asExternalTorque.bind(spScope);

	m_rg_driveline = asDriveline.contents(r_driveline);
	if(!m_rg_driveline.isValid())
	{
		return false;
	}

	m_spSemiImplicit->compile(m_rg_driveline);
	std::vector<SemiImplicit1D::Particle> &particles =
		m_spSemiImplicit->particles();

	for(RecordGroup::iterator i_rg = m_rg_driveline->begin();
			i_rg != m_rg_driveline->end(); i_rg++)
	{
		sp<RecordArray> spRA = *i_rg;
		if(m_asShaft.check(spRA))
		{
			bool externalTorque = false;
			if(m_asExternalTorque.check(spRA))
			{
				externalTorque = true;
			}
			for(unsigned int i = 0; i < spRA->length(); i++)
			{
				Record r_shaft = spRA->getRecord(i);
				m_shafts[m_asShaft.name(r_shaft)].m_r_parent = r_shaft;


				unsigned int i_p = m_asSolverParticle.index(r_shaft);

				if(i == 0)
				{
					particles[i_p].m_shift_root = true;
				}

				sp<SemiImplicit1D::Force> spForce(new ShaftForce(m_spSemiImplicit));
				m_spSemiImplicit->addForce(spForce, true);

				m_shafts[m_asShaft.name(r_shaft)].m_spHookForce = spForce;
				m_shafts[m_asShaft.name(r_shaft)].m_spHookForce->bind(&particles[i_p]);
				m_shafts[m_asShaft.name(r_shaft)].m_spHookForce->set(0.0);

				m_shafts[m_asShaft.name(r_shaft)].m_has_externalTorque = externalTorque;
			}
		}
	}
	return true;
}

} /* namespace ext */
} /* namespace fe */
