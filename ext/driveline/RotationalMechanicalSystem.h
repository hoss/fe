/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __driveline_RotationalMechanicalSystem_h__
#define __driveline_RotationalMechanicalSystem_h__

namespace fe
{
namespace ext
{

/// @brief Rotational Mechanical Solver
class FE_DL_EXPORT RotationalMechanicalSystem
	: virtual public Stepper,
	public CastableAs<RotationalMechanicalSystem>
{
	public:
					RotationalMechanicalSystem(void);
virtual				~RotationalMechanicalSystem(void);

virtual	void		initialize(void);

virtual	void	prepare(sp<RecordGroup> rg_input);
virtual	void	react(const t_note_id &a_note_id);
virtual	void	step(t_moa_real a_dt);
virtual	void	connectOrchestrator(Orchestrator *a_pOrchestrator);

	private:
		class SolveGroup
		{
			public:
				AsSolveGroupLive			m_asSolveGroupLive;
				AsShaft						m_asShaft;
				std::map< String, Record >	m_shafts;
				sp<SemiImplicit1D>			m_spSemiImplicit;
				sp<RecordGroup>				m_rg_contents;

			public:
				bool	compile(Record r_solveGroup);
		};

		AsParticle1D				m_asParticle1D;
		sp<RecordGroup>				m_rg_dataset;
		sp<Scope>					m_spScope;
		std::vector< Record >		m_rv_solveGroups;

		std::vector< SolveGroup >	m_solveGroups;

};

} /* namespace ext */
} /* namespace fe */

#endif /* __driveline_RotationalMechanicalSystem_h__ */
