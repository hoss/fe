/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "driveline/driveline.pmh"

namespace fe
{
namespace ext
{

DrivelineRigProducer::DrivelineRigProducer(void)
{
}

DrivelineRigProducer::~DrivelineRigProducer(void)
{
}

void DrivelineRigProducer::initialize(void)
{
}

void DrivelineRigProducer::prepare(sp<RecordGroup> a_rg_dataset)
{
	loadFileInto(a_rg_dataset, "driveline.rg");

	AsDatasetMeta asDatasetMeta;
	asDatasetMeta.digest(a_rg_dataset);
	//TODO: de-hard string name
	asDatasetMeta.set("splatpath", "data");
}

sp<OrchestratorI> DrivelineRigProducer::createOrchestrator(sp<RecordGroup> a_rg)
{
	sp<Registry> spRegistry = registry();

	sp<OrchestratorI> spOrchestrator(
		spRegistry->create("OrchestratorI.Orchestrator.fe"));
	spOrchestrator->datasetInitialize(a_rg);

	spOrchestrator->append(spRegistry->create("SystemI.MultiSweep.fe"));

	spOrchestrator->append(spRegistry->create("SystemI.DrivelineISystem.fe"));

	sp<SystemI> spEvaluateSystem =
		spRegistry->create("SystemI.EvaluateSystem.fe");
	spOrchestrator->append(spEvaluateSystem);
	spOrchestrator->connect(spEvaluateSystem, FE_NOTE_DATAPOINT);

	sp<SystemI> spSplatterLine(
		spRegistry->create("SystemI.SplatLineSystem.fe"));
	spOrchestrator->append(spSplatterLine);

	spOrchestrator->compile();

	return spOrchestrator;
}

} /* namespace ext */
} /* namespace fe */
