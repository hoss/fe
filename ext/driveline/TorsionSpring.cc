/*	Copyright (C) 2003-2014 Free Electron Organization
	Any use of this software requires a license. */

/** @file */

#include "driveline/driveline.pmh"

namespace fe
{
namespace ext
{

TorsionSpring::TorsionSpring(sp<SemiImplicit1D> a_integrator)
{
	m_integrator = a_integrator;
	m_perturbation = sqrt(fe::tol);
}

TorsionSpring::~TorsionSpring(void)
{
}

#if 0
void TorsionSpring::clear(void)
{
}
#endif

void TorsionSpring::pairs(sp<RecordGroup> rg_input, SemiImplicit1D::t_pairs &a_pairs)
{
	AsLinearSpring		asLinearSpring;
	AsForcePoint1D		asForcePoint;
	AsSolverParticle1D	asSolverParticle;

	sp<Scope> spScope = rg_input->scope();
	m_asGearTrain.bind(spScope);

	/* find size and set size of spring structure */
	m_springs.clear();
	unsigned int n = 0;
	for(RecordGroup::iterator i_rg = rg_input->begin();
			i_rg != rg_input->end(); i_rg++)
	{
		sp<RecordArray> spRA = *i_rg;
		asLinearSpring.bind(spRA->layout()->scope());

		if(asLinearSpring.check(spRA))
		{
			n += (unsigned int)spRA->length();
		}
	}
	m_springs.resize(n);

	unsigned int ii = 0;
	for(RecordGroup::iterator i_rg = rg_input->begin();
			i_rg != rg_input->end(); i_rg++)
	{
		sp<RecordArray> spRA = *i_rg;
		asLinearSpring.bind(spRA->layout()->scope());
		asForcePoint.bind(spRA->layout()->scope());
		asSolverParticle.bind(spRA->layout()->scope());

		if(asLinearSpring.check(spRA))
		{
			for(int i = 0; i < spRA->length(); i++)
			{
				Record r_spring = spRA->getRecord(i);
				WeakRecord &r_left = asLinearSpring.left(r_spring);
				WeakRecord &r_right = asLinearSpring.right(r_spring);

				int i_L = asSolverParticle.index(r_left);
				int i_R = asSolverParticle.index(r_right);

				Spring &spring = m_springs[ii];
				ii++;

spring.m_r_parent = r_spring;
fe_fprintf(stderr, "PAIRS %d | %d %d\n", ii, r_spring.isValid(), spring.m_r_parent.isValid());
//fe::sp< fe::ext::SemiImplicit1D::t_pair> m_spPair;
				spring.m_spPair = new SemiImplicit1D::t_pair(i_L, i_R);

//TODO: TorsionSpring shouldn't really do ratio-ing at all
				if(m_asGearTrain.check(spRA))
				{
					spring.m_spPair->setGearRatio((int)m_asGearTrain.left_gear(r_spring),
						(int)m_asGearTrain.right_gear(r_spring));
				}

				a_pairs.push_back(spring.m_spPair);
			}
		}
	}
}

void TorsionSpring::compile(sp<RecordGroup> rg_input, std::vector<SemiImplicit1D::Particle> &a_particles, SemiImplicit1D::CompileMatrix &a_compileMatrix)
{
	AsLinearSpringExtended		asLinearSpring;
	AsForcePoint1D				asForcePoint;
	AsSolverParticle1D			asSolverParticle;
	AsLinearSpringForceReturn	asLSFReturn;
	AsNamed						asNamed;

	sp<Scope> spScope = rg_input->scope();
fe_fprintf(stderr, "SCOPE %d\n", rg_input.isValid());
	asLinearSpring.bind(spScope);
	asForcePoint.bind(spScope);
	asSolverParticle.bind(spScope);
	asLSFReturn.bind(spScope);
	asNamed.bind(spScope);

	unsigned int n = (unsigned int)m_springs.size();
	for(unsigned int i = 0; i < n; i++)
	{
		Spring &spring = m_springs[i];
		Record r_spring = spring.m_r_parent;
fe_fprintf(stderr, "REC %d | %d %d\n", i, r_spring.isValid(), spring.m_r_parent.isValid());
		WeakRecord &r_left = asLinearSpring.left(r_spring);
		WeakRecord &r_right = asLinearSpring.right(r_spring);

//TODO: cannot resize -- need to preallocate since the pointers are hard used
		if(asLSFReturn.check(r_spring))
		{
			asLSFReturn.spring(r_spring) = (void *)&(m_springs[i]);
		}

		if(asNamed.check(r_spring))
		{
			m_springmap[asNamed.name(r_spring)] = &(m_springs[i]);
		}

		spring.m_flags = asLinearSpring.flags(r_spring);

		spring.m_restlength = asLinearSpring.length(r_spring);
		spring.m_stiffness = asLinearSpring.stiffness(r_spring);
		spring.m_damping = asLinearSpring.damping(r_spring);

		int i_L = asSolverParticle.index(r_left);
		int i_R = asSolverParticle.index(r_right);

		int i_0, i_1;
		if(i_L < i_R)
		{
			i_0 = i_L;
			i_1 = i_R;
		}
		else
		{
			i_0 = i_R;
			i_1 = i_L;
		}

		spring.m_location[0] = &(a_particles[i_0].m_location);
		spring.m_location[1] = &(a_particles[i_1].m_location);

		spring.m_ratio[0] = &(a_particles[i_0].m_ratio);
		spring.m_ratio[1] = &(a_particles[i_1].m_ratio);

		spring.m_velocity[0] = &(a_particles[i_0].m_velocity);
		spring.m_velocity[1] = &(a_particles[i_1].m_velocity);

		spring.m_force[0] = &(a_particles[i_0].m_force);
		spring.m_force[1] = &(a_particles[i_1].m_force);

		a_compileMatrix.entry(i_0, i_0).first
			.push_back(&(spring.m_diagonal_dfdx[0]));
		a_compileMatrix.entry(i_0, i_0).second
			.push_back(&(spring.m_diagonal_dfdv[0]));

		a_compileMatrix.entry(i_1, i_1).first
			.push_back(&(spring.m_diagonal_dfdx[1]));
		a_compileMatrix.entry(i_1, i_1).second
			.push_back(&(spring.m_diagonal_dfdv[1]));

		a_compileMatrix.entry(i_0, i_1).first
			.push_back(&(spring.m_offdiagonal_dfdx));
		a_compileMatrix.entry(i_0, i_1).second
			.push_back(&(spring.m_offdiagonal_dfdv));
	}

#if 0
	unsigned int ii = 0;
	for(RecordGroup::iterator i_rg = rg_input->begin();
			i_rg != rg_input->end(); i_rg++)
	{
		sp<RecordArray> spRA = *i_rg;
		asLinearSpring.bind(spRA->layout()->scope());
		asForcePoint.bind(spRA->layout()->scope());
		asSolverParticle.bind(spRA->layout()->scope());
		asLSFReturn.bind(spRA->layout()->scope());
		asNamed.bind(spRA->layout()->scope());

		if(asLinearSpring.check(spRA))
		{
			for(int i = 0; i < spRA->length(); i++)
			{
				Record r_spring = spRA->getRecord(i);
				WeakRecord &r_left = asLinearSpring.left(r_spring);
				WeakRecord &r_right = asLinearSpring.right(r_spring);

//TODO: cannot resize -- need to preallocate since the pointers are hard used
				if(asLSFReturn.check(r_spring))
				{
					asLSFReturn.spring(r_spring) = (void *)&(m_springs[ii]);
				}
				Spring &spring = m_springs[ii];

				if(asNamed.check(r_spring))
				{
					m_springmap[asNamed.name(r_spring)] = &(m_springs[ii]);
				}

				ii++;

m_asGearTrain.bind(spRA->layout()->scope());
spring.m_r_parent = r_spring;
				spring.m_flags = asLinearSpring.flags(r_spring);

				spring.m_restlength = asLinearSpring.length(r_spring);
				spring.m_stiffness = asLinearSpring.stiffness(r_spring);
				spring.m_damping = asLinearSpring.damping(r_spring);

				int i_L = asSolverParticle.index(r_left);
				int i_R = asSolverParticle.index(r_right);

				int i_0, i_1;
				if(i_L < i_R)
				{
					i_0 = i_L;
					i_1 = i_R;
				}
				else
				{
					i_0 = i_R;
					i_1 = i_L;
				}

				spring.m_location[0] = &(a_particles[i_0].m_location);
				spring.m_location[1] = &(a_particles[i_1].m_location);

				spring.m_ratio[0] = &(a_particles[i_0].m_ratio);
				spring.m_ratio[1] = &(a_particles[i_1].m_ratio);

				spring.m_velocity[0] = &(a_particles[i_0].m_velocity);
				spring.m_velocity[1] = &(a_particles[i_1].m_velocity);

				spring.m_force[0] = &(a_particles[i_0].m_force);
				spring.m_force[1] = &(a_particles[i_1].m_force);

				a_compileMatrix.entry(i_0, i_0).first
					.push_back(&(spring.m_diagonal_dfdx[0]));
				a_compileMatrix.entry(i_0, i_0).second
					.push_back(&(spring.m_diagonal_dfdv[0]));

				a_compileMatrix.entry(i_1, i_1).first
					.push_back(&(spring.m_diagonal_dfdx[1]));
				a_compileMatrix.entry(i_1, i_1).second
					.push_back(&(spring.m_diagonal_dfdv[1]));

				a_compileMatrix.entry(i_0, i_1).first
					.push_back(&(spring.m_offdiagonal_dfdx));
				a_compileMatrix.entry(i_0, i_1).second
					.push_back(&(spring.m_offdiagonal_dfdv));

			}
		}
	}
#endif
}

void TorsionSpring::reconfigure(void)
{
	fe_fprintf(stderr, "reconfigure\n");
	unsigned int n = (unsigned int)m_springs.size();
	for(unsigned int i = 0; i < n; i++)
	{
		Spring &spring = m_springs[i];
		spring.m_spPair->setGearRatio(
			m_asGearTrain.left_gear(spring.m_r_parent),
			m_asGearTrain.right_gear(spring.m_r_parent));
	}
}


bool TorsionSpring::validate(void)
{
	unsigned int n = (unsigned int)m_springs.size();
	for(unsigned int i = 0; i < n; i++)
	{
		Spring &spring = m_springs[i];
		t_solve_real x1[2];
		x1[0] = *(spring.m_location[0]);
		x1[1] = *(spring.m_location[1]);

		t_solve_real dx1;
		dx1 = x1[1] - x1[0];

#if 0
		t_solve_v3 x0[2];
		x0[0] = *(spring.m_prev_location[0]);
		x0[1] = *(spring.m_prev_location[1]);

		t_solve_v3 dx0;
		dx0 = x0[1] - x0[0];
#endif

		// TODO: expose allowed...maybe via per spring attr
		// TODO: or maybe a hueristic from stiffness/damping
		t_solve_real allowed = 0.1;
		//t_solve_real deltaX = magnitude(dx1) - magnitude(dx0);
		t_solve_real deltaX = spring.m_prev_distance - fabs(dx1);
		t_solve_real delta = fabs(deltaX);


		if(delta > allowed)
		{
			return false;
		}
	}
	return true;
}


void TorsionSpring::accumulate(void)
{

	t_solve_real dfdx;
	t_solve_real dfdv;

	unsigned int n = (unsigned int)m_springs.size();
	for(unsigned int i = 0; i < n; i++)
	{
		t_solve_real l[2];

		Spring &spring = m_springs[i];
//t_solve_real pp = m_asGearTrain.left_gear(spring.m_r_parent);
//fe_fprintf(stderr, "leftgear %g\n", pp);

		l[0] = *(spring.m_location[0]);
		l[1] = *(spring.m_location[1]);

		t_solve_real force;
		force = 0.0;

		// consider ratio as that of shaft 0
		t_solve_real ratio = *(spring.m_ratio[0]);

		t_solve_real length = l_ext(spring.m_restlength, ratio);

		t_solve_real delta = (l[1] - l[0]);

		// TODO: maybe avoid coiincident points altogether
		// slightly perturb coiincident points
		if(isZero(delta))
		{
			// if rest length is actually zero, continue
			if(isZero(length))
			{
				continue;
			}
			l[1] += m_perturbation;

			delta = (l[1] - l[0]);
		}

		t_solve_real distance = fabs(delta);
		//t_solve_real dx = delta;
		//t_solve_real d = distance;
		spring.m_prev_distance = distance;

		delta *= 1.0/distance;
		if(length < 0.0) { length = distance; }
		distance -= length;


		t_solve_real stiffness = k_ext(spring.m_stiffness, ratio);
		t_solve_real damping = d_ext(spring.m_damping, ratio);

		force += stiffness * delta * distance;

		t_solve_real v[2];
		v[0] = *(spring.m_velocity[0]);
		v[1] = *(spring.m_velocity[1]);

		t_solve_real dv = 0.0;
		if(!isZero(v[1] - v[0]))
		{
			dv = v[1] - v[0];
			force += dv * damping;
		}

		*(spring.m_force[0]) += force;
		*(spring.m_force[1]) += -force;

		dfdx = -stiffness;
		dfdv = -damping;

		*(spring.m_diagonal_dfdx[0]) = *(spring.m_diagonal_dfdx[0]) + dfdx;
		*(spring.m_diagonal_dfdx[1]) = *(spring.m_diagonal_dfdx[1]) + dfdx;
		dfdx *= -1;
		*(spring.m_offdiagonal_dfdx) = *(spring.m_offdiagonal_dfdx) + dfdx;

		*(spring.m_diagonal_dfdv[0]) = *(spring.m_diagonal_dfdv[0]) + dfdv;
		*(spring.m_diagonal_dfdv[1]) = *(spring.m_diagonal_dfdv[1]) + dfdv;
		dfdv *= -1;
		*(spring.m_offdiagonal_dfdv) = *(spring.m_offdiagonal_dfdv) + dfdv;
	}
}

TorsionSpring::Spring *TorsionSpring::spring(const String &a_name)
{
	t_stdmap<String, Spring *>::iterator i_spring = m_springmap.find(a_name);

	if(i_spring == m_springmap.end())
	{
		return NULL;
	}

	return i_spring->second;
}

} /* namespace ext */
} /* namespace fe */

