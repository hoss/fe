/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "fe/data.h"
#include "driveline/driveline.h"

using namespace fe;
using namespace ext;

int main(int argc, char *argv[])
{
	UnitTest unitTest;
	try
	{
		feLogger()->clear(".*");
		feLogger()->clear("error");
		feLogger()->clear("info");
		feLogger()->clear("warning");

		sp<Master> spMaster(new Master);
		assertMath(spMaster->typeMaster());
		sp<Registry> spRegistry = spMaster->registry();
		spRegistry->manage("fexMechanicsDL");
		spRegistry->manage("fexMoaDL");
		spRegistry->manage("fexEvaluateDL");
		spRegistry->manage("fexSurfaceDL");
		spRegistry->manage("feDataDL");
		spRegistry->manage("fexViewerDL");
		spRegistry->manage("fexDrivelineDL");

		sp<Scope> spScope = spRegistry->create("Scope");

		Overlayer overlayer(spScope);

		overlayer.load("driveline.rg");

		for(sp<RecordGroup> rg_clone : overlayer.bakedDatasets())
		{
			// >>>>>>>>> CREATE ORCHESTRATOR
			sp<OrchestratorI> spOrchestrator(
				spRegistry->create("OrchestratorI.Orchestrator.fe"));
			spOrchestrator->datasetInitialize(rg_clone);

			spOrchestrator->append(spRegistry->create("SystemI.MultiSweep.fe"));
			spOrchestrator->append(spRegistry->create("SystemI.DrivelineISystem.fe"));
			spOrchestrator->append(spRegistry->create("SystemI.Driveline.ISI.fe"));

			sp<SystemI> spEvaluateSystem =
				spRegistry->create("SystemI.EvaluateSystem.fe");
			spOrchestrator->append(spEvaluateSystem);
			spOrchestrator->connect(spEvaluateSystem, FE_NOTE_DATAPOINT);

			sp<SystemI> spSplatterLine(
				spRegistry->create("SystemI.SplatLineSystem.fe"));
			spOrchestrator->append(spSplatterLine);

			if(!spOrchestrator->compile())
			{
				fprintf(stderr, "compile failed\n");
				exit(101);
			}

			// >>>>>>>>> START NOTE
			t_note_id note_start = spOrchestrator->note_id(FE_NOTE_START);
			spOrchestrator->perform(note_start);
		}
	}
	catch(Exception &e)
	{
		e.log();
	}
	return unitTest.failures();
}

