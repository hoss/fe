INFO 5

ATTRIBUTE name string "=unset"
ATTRIBUTE driveline:model string
ATTRIBUTE driveline:component string
ATTRIBUTE composer:step string
ATTRIBUTE composer:home string
ATTRIBUTE composer:outer stringarray "=0 0 0"
ATTRIBUTE composer:inner stringarray
ATTRIBUTE composer:linger real =100.0
ATTRIBUTE composer:frequency real =180.0
ATTRIBUTE composer:runup real =4.0
#ATTRIBUTE composer:events group
ATTRIBUTE driveline:momentOfInertia atomic =1.0
ATTRIBUTE driveline:externalTorque atomic =0.0

ATTRIBUTE driveline:contents group

ATTRIBUTE spc:location1d	real =0.0
ATTRIBUTE spc:velocity1d	real =0.0
ATTRIBUTE sim:force1d		real =0.0
ATTRIBUTE sim:mass			real =2.0

ATTRIBUTE pair:left WeakRecord
ATTRIBUTE pair:right WeakRecord
ATTRIBUTE mat:stiffness real =0.1
ATTRIBUTE mat:damping real =0.05
ATTRIBUTE mat:flags integer
ATTRIBUTE bdy:length real =0.0
ATTRIBUTE lsf:spring voidstar

ATTRIBUTE driveline:left_gear int =1
ATTRIBUTE driveline:right_gear int =1

ATTRIBUTE dataset:group group

ATTRIBUTE splat:expression string
ATTRIBUTE splat:x_range vector2
ATTRIBUTE splat:y_range vector2
ATTRIBUTE splat:color vector3 "=1 1 1"
ATTRIBUTE splat:splat string
ATTRIBUTE splatline:is void

ATTRIBUTE mixer:inputs stringarray
ATTRIBUTE dataset:active boolean =false

ATTRIBUTE unidir:gear int =1
ATTRIBUTE unidir:drive_mode int =3
ATTRIBUTE unidir:clutch_friction real =0.0
ATTRIBUTE unidir:clutch_torque real =1000.0
ATTRIBUTE unidir:auto_clutch real =0.0
ATTRIBUTE unidir:manual_clutch real =0.0
ATTRIBUTE unidir:max_engine_rps real =1000000.0
ATTRIBUTE unidir:throttle real =1.0
ATTRIBUTE unidir:ram_current_speed real =0.0
ATTRIBUTE unidir:baulk_torque real =1000.0
ATTRIBUTE unidir:gear_ratios realarray "=3,10.0,20.0,50.0"
ATTRIBUTE unidir:diff_preload_torque real  =10.0
ATTRIBUTE unidir:diff_pump_torque real =10.0
ATTRIBUTE unidir:diff_power_fract real =0.1
ATTRIBUTE unidir:diff_coast_fract real =0.1
ATTRIBUTE unidir:diff_4wd_rear_torque_split real
ATTRIBUTE unidir:diff_4wd_pump_effect vector3
ATTRIBUTE unidir:diff_4wd_preload_effect vector3
ATTRIBUTE unidir:diff_4wd_power_effect vector3
ATTRIBUTE unidir:diff_4wd_coast_effect vector3
ATTRIBUTE unidir:handbrake real =0.0
ATTRIBUTE unidir:handbrake_4wd_disco real =0.0
ATTRIBUTE unidir:clutch_inertia real =1.0
ATTRIBUTE unidir:engine_inertia real =1.0
ATTRIBUTE unidir:recip_diff_ratio real =1.0
ATTRIBUTE aspect:label string "=unset"

LAYOUT aspect
	aspect:label

LAYOUT unidir_driveline
	name
	unidir:gear
	unidir:drive_mode
	unidir:clutch_friction
	unidir:clutch_torque
	unidir:auto_clutch
	unidir:manual_clutch
	unidir:max_engine_rps
	unidir:throttle
	unidir:ram_current_speed
	unidir:baulk_torque
	unidir:gear_ratios
	unidir:diff_preload_torque
	unidir:diff_pump_torque
	unidir:diff_power_fract
	unidir:diff_coast_fract
	unidir:diff_4wd_rear_torque_split
	unidir:diff_4wd_pump_effect
	unidir:diff_4wd_preload_effect
	unidir:diff_4wd_power_effect
	unidir:diff_4wd_coast_effect
	unidir:handbrake
	unidir:handbrake_4wd_disco
	unidir:clutch_inertia
	unidir:engine_inertia
	unidir:recip_diff_ratio

LAYOUT splat
	name
	splat:x_range
	splat:y_range
	splat:expression
	splat:splat
	splat:color
	splatline:is

ATTRIBUTE time:time real
ATTRIBUTE event:action string
LAYOUT event
	time:time
	event:action

LAYOUT multisweep
	name
	composer:step
	composer:outer
	composer:inner
	composer:linger
	composer:frequency
	composer:runup
	#composer:events

LAYOUT driveline
	name
	driveline:model
	driveline:contents

LAYOUT driveline_model
	name
	driveline:component

LAYOUT driveline_shaft
	name
	driveline:momentOfInertia
	spc:location1d
	spc:velocity1d
	sim:force1d
	sim:mass

LAYOUT driveline_shaft_forced
	name
	driveline:momentOfInertia
	driveline:externalTorque
	spc:location1d
	spc:velocity1d
	sim:force1d
	sim:mass

LAYOUT torsion_spring
	name
	pair:left
	pair:right
	mat:stiffness
	mat:damping
	mat:flags
	bdy:length
	lsf:spring
	driveline:left_gear
	driveline:right_gear

LAYOUT dataset_mixer
	name
	mixer:inputs
	dataset:active

LAYOUT dataset
	name
	dataset:group

DEFAULTGROUP 1


RECORD * dataset
	name		"old_setup"
	dataset:group		OLD_SETUP_RG

RECORD * dataset
	name		"fe_dataset"
	dataset:group		FE_DL_RG

RECORD * dataset
	name		"fe_test_driveline"
	dataset:group		FE_TEST_DRIVELINE

RECORD * dataset_mixer
	name			"Old Setup"
	mixer:inputs	"old_setup fe_test_driveline"
	dataset:active  true

RECORD * dataset_mixer
	name			"New Setup"
	mixer:inputs	"fe_dataset fe_test_driveline"
	dataset:active  true

DEFAULTGROUP 1

RECORD * dataset
	name		"UNIDIR_dataset"
	dataset:group		UNIDIR_RG

RECORD * dataset_mixer
	name			"UNIDIR overlay"
	mixer:inputs	"UNIDIR_dataset"
	dataset:active  true


DEFAULTGROUP UNIDIR_RG 

RECORD * aspect
	aspect:label "UNIDIR Driveline Test"

RECORD * unidir_driveline
	name "UNIDIR:driveline"

RECORD * multisweep
	name			"UNIDIR Driveline Run"
	composer:frequency			160.0
	composer:inner	"1 200 1"
	composer:linger		1.0
	composer:runup		1.0

RECORD * splat
	name			"UNIDIR clutch"
	splat:x_range	"0.0		200.0"
	splat:y_range	"-20.0	10000.0"
	splat:expression "	(X (get		'sim_clock.time:time')		)
						(Y (get		'UNIDIR:driveline.unidir:clutch_rps')	)"
	splat:color		"1 1 0.6"

RECORD * splat
	name			"UNIDIR RR Wheel"
	splat:splat		"UNIDIR clutch"
	splat:x_range	"0.0		200.0"
	splat:y_range	"-1000.0	1000.0"
	splat:expression "	(X (get		'sim_clock.time:time')		)
						(Y (get		'UNIDIR:driveline.unidir:wheel_rps[3]')	)"
	splat:color		"1.0 1.0 0.0"

RECORD * splat
	name			"UNIDIR RL Wheel"
	splat:splat		"UNIDIR clutch"
	splat:x_range	"0.0		200.0"
	splat:y_range	"-1000.0	1000.0"
	splat:expression "	(X (get		'sim_clock.time:time')		)
						(Y (get		'UNIDIR:driveline.unidir:wheel_rps[2]')	)"
	splat:color		"0.0 1.0 1.0"

RECORD * event
	time:time		80.0
	event:action	"(set 'UNIDIR:driveline.unidir:gear' -1)"

RECORD * event
	time:time		82.0
	event:action	"(set 'UNIDIR:driveline.unidir:gear' 2)"

RECORD * event
	time:time		100.0
	event:action	"(set 'UNIDIR:driveline.unidir:brake_torque[2]' 2000.0)
					 (set 'UNIDIR:driveline.unidir:brake_torque[3]' 1000.0)"

RECORD * event
	time:time		120.0
	event:action	"(set 'UNIDIR:driveline.unidir:brake_torque[2]' 1000.0)
					 (set 'UNIDIR:driveline.unidir:brake_torque[3]' 2000.0)"

DEFAULTGROUP FE_DL_RG 

RECORD * aspect
	aspect:label "New Driveline Test"

RECORD * splat
	name			"shaft0"
	splat:x_range	"0.0		200.0"
	splat:y_range	"-50.0	1000.0"
	splat:expression "	(X (get		'sim_clock.time:time')		)
						(Y (get		'dummyshaft0.spc:velocity1d')	)"
	splat:color		"1.0 1.0 1.0"

RECORD * splat
	name			"shaft1"
	splat:splat		"shaft0"
	splat:x_range	"0.0		200.0"
	splat:y_range	"-50.0	1000.0"
	splat:expression "	(X (get		'sim_clock.time:time')		)
						(Y (get		'dummyshaft1.spc:velocity1d')	)"
	splat:color		"1.0 0.5 0.5"

RECORD * splat
	name			"shaft2"
	splat:splat		"shaft0"
	splat:x_range	"0.0		200.0"
	splat:y_range	"-50.0	1000.0"
	splat:expression "	(X (get		'sim_clock.time:time')		)
						(Y (get		'dummyshaft2.spc:velocity1d')	)"
	splat:color		"0.5 1.0 0.5"

RECORD * splat
	name			"shaft3"
	splat:splat		"shaft0"
	splat:x_range	"0.0		200.0"
	splat:y_range	"-50.0	1000.0"
	splat:expression "	(X (get		'sim_clock.time:time')		)
						(Y (get		'dummyshaft3.spc:velocity1d')	)"
	splat:color		"0.5 0.5 1.0"

RECORD * multisweep
	name			"Clean Sweep"
	composer:frequency			160.0
	composer:inner	"1 200 1"
	#composer:step	"	(set	'coupling12.driveline:right_gear' 51)
	#					(set	'coupling12.driveline:left_gear' (* INNER 0.1))  "
	composer:linger		1.0
	composer:runup		1.0

RECORD * event
	time:time		0.0
	event:action	"(set 'coupling12.driveline:right_gear' 11)"
RECORD * event
	time:time		0.0
	event:action	"(set 'coupling12.driveline:left_gear' 11)"
RECORD * event
	time:time		50.0
	event:action	"(set 'coupling12.driveline:right_gear' 37)"
RECORD * event
	time:time		2.0
	event:action	"(set 'dummyshaft0.driveline:externalTorque' 10.0)"
RECORD * event
	time:time		49.0
	event:action	"(set 'dummyshaft0.driveline:externalTorque' 0.0)"
RECORD * event
	time:time		51.0
	event:action	"(set 'dummyshaft0.driveline:externalTorque' 10.0)"


DEFAULTGROUP OLD_SETUP_RG 

RECORD * aspect
	aspect:label "Old Driveline Test"

RECORD * splat
	name			"location"
	splat:x_range	"0.0		200.0"
	splat:y_range	"-20.0	1000.0"
	splat:expression "	(X (get		'sim_clock.time:time')		)
						(Y (get		'dummyshaft1.spc:location1d')	)"
	splat:color		"1 0.5 0.6"

RECORD * splat
	name			"zoomed in"
	splat:x_range	"0.0		20.0"
	splat:y_range	"-20.0	100.0"
	splat:expression "	(X (get		'sim_clock.time:time')		)
						(Y (get		'dummyshaft1.spc:location1d')	)"
	splat:color		"1 0.5 0.6"

RECORD * splat
	name			"velocity"
	splat:x_range	"0.0		200.0"
	splat:y_range	"-20.0	300.0"
	splat:expression "	(X (get		'sim_clock.time:time')		)
						(Y (get		'dummyshaft1.spc:velocity1d')	)"
	splat:color		"1 0 0.6"

RECORD * multisweep
	name			"Driveline Run"
	composer:frequency			160.0
	composer:inner	"1 200 1"
	composer:step	"	(set	'coupling12.driveline:right_gear' 51)
						(set	'coupling12.driveline:left_gear' (* INNER 0.1))  "
	composer:linger		1.0
	composer:runup		1.0

RECORD * event
	time:time		0.0
	event:action	"(set 'dummyshaft0.driveline:externalTorque' 1.0)"
RECORD * event
	time:time		0.0
	event:action	"(set 'dummyshaft1.driveline:externalTorque' 1.0)"
RECORD * event
	time:time		0.0
	event:action	"(set 'dummyshaft2.driveline:externalTorque' 1.0)"
RECORD * event
	time:time		0.0
	event:action	"(set 'dummyshaft3.driveline:externalTorque' 1.0)"
RECORD * event
	time:time		100.0
	event:action	"(set 'dummyshaft3.driveline:externalTorque' 100.0)"
RECORD * event
	time:time		100.1
	event:action	"(set 'dummyshaft3.driveline:externalTorque' 100.0)"
RECORD * event
	time:time		10.5
RECORD * event
	time:time		13.1
RECORD * event
	time:time		10.8
RECORD * event
	time:time		0.1
RECORD * event
	time:time		0.01

DEFAULTGROUP FE_TEST_DRIVELINE

RECORD * driveline
	name "test driveline"
	driveline:model	"test driveline model"
	driveline:contents DRIVELINE_PARTS

RECORD * driveline_model
	name "test driveline model"
	driveline:component "DrivelineI.SemiImplicit.fe"

RECORD dummy_shaft_0 driveline_shaft_forced
	name "dummyshaft0"
	driveline:externalTorque 0.0

RECORD dummy_shaft_1 driveline_shaft_forced
	name "dummyshaft1"
	driveline:externalTorque 0.0

RECORD dummy_shaft_2 driveline_shaft_forced
	name "dummyshaft2"
	driveline:externalTorque 0.0

RECORD dummy_shaft_3 driveline_shaft_forced
	name "dummyshaft3"
	driveline:externalTorque 0.0

RECORD coupling_01 torsion_spring
	name "coupling 01"
	pair:left dummy_shaft_0
	pair:right dummy_shaft_1

RECORD coupling_23 torsion_spring
	name "coupling 23"
	pair:left dummy_shaft_2
	pair:right dummy_shaft_3

RECORD coupling_12 torsion_spring
	name "coupling12"
	pair:left dummy_shaft_1
	pair:right dummy_shaft_2
	driveline:left_gear 3
	driveline:right_gear 3

RECORDGROUP DRIVELINE_PARTS
	dummy_shaft_0
	dummy_shaft_1
	coupling_01
	dummy_shaft_2
	dummy_shaft_3
	coupling_23
	coupling_12


END

