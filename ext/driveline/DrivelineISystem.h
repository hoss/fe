/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __driveline_DrivelineISystem_h__
#define __driveline_DrivelineISystem_h__

namespace fe
{
namespace ext
{

/// @brief Driveline System
class FE_DL_EXPORT DrivelineISystem :
	virtual public Stepper,
	public Initialize<DrivelineISystem>
{
	public:
				DrivelineISystem(void);
virtual			~DrivelineISystem(void);
		void	initialize(void);

virtual void		compile(const t_note_id &a_note_id);
virtual	void		perform(const t_note_id &a_note_id);
virtual	void		step(t_moa_real a_dt);
virtual	void		connectOrchestrator(sp<OrchestratorI> a_spOrchestrator);

	private:
		sp<Scope>						m_spScope;
		std::vector< Record >			m_r_drivelines;
		AsDrivelineLive					m_asDrivelineLive;
		t_note_id						m_note_datapoint;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __driveline_DrivelineISystem_h__ */

