/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "driveline/driveline.pmh"

namespace fe
{
namespace ext
{

DrivelineISystem::DrivelineISystem(void)
{
}

DrivelineISystem::~DrivelineISystem(void)
{
}

void DrivelineISystem::initialize(void)
{
}

void DrivelineISystem::connectOrchestrator(
	fe::sp<OrchestratorI> a_spOrchestrator)
{
	Stepper::connectOrchestrator(a_spOrchestrator);

	m_note_datapoint = a_spOrchestrator->connect(this, FE_NOTE_DATAPOINT);
}

void DrivelineISystem::step(t_moa_real a_dt)
{
	for(unsigned int i_dl = 0; i_dl < m_r_drivelines.size(); i_dl++)
	{
		Record &r_dl = m_r_drivelines[i_dl];
		sp<DrivelineI> spDriveline = m_asDrivelineLive.spDrivelineI(r_dl);
		spDriveline->step(a_dt);
	}
}

void DrivelineISystem::compile(const t_note_id &a_note_id)
{
fe_fprintf(stderr, "DrivelineISystem::prepare\n");
	m_spScope = m_rg_dataset->scope();

	if(!m_spScope.isValid()) { return; }

	enforce<AsDriveline,AsDrivelineLive>(m_rg_dataset);

	// Dictionary for all driveline models available
	RecordDictionary<AsDrivelineModel> models(m_rg_dataset);
	AsDrivelineModel asDrivelineModel;
	asDrivelineModel.bind(m_spScope);

	AsDrivelineLive asDrivelineLive(m_rg_dataset->scope());

	m_asDrivelineLive.bind(m_rg_dataset->scope());

	// Find all drivelines
	std::vector<Record>	drivelines;
	asDrivelineLive.filter(drivelines, m_rg_dataset);

	for(unsigned int i_dl = 0; i_dl < drivelines.size(); i_dl++)
	{
		Record r_driveline = drivelines[i_dl];
fe_fprintf(stderr, "need to make a driveline ******\n");
		Record r_model = models[asDrivelineLive.model(r_driveline)];

		sp<DrivelineI> spDriveline(registry()->create(
				asDrivelineModel.component(r_model)));
fe_fprintf(stderr, "DL V %d\n", spDriveline.isValid());

		if(spDriveline.isValid())
		{
			spDriveline->compile(r_driveline);
			m_r_drivelines.push_back(r_driveline);
			asDrivelineLive.spDrivelineI(r_driveline) = spDriveline;
		}
	}
}

void DrivelineISystem::perform(const t_note_id &a_note_id)
{
	if(a_note_id == m_note_step)
	{
		Stepper::perform(a_note_id);// this ends up calling DrivelineISystem::step
	}
	else if(a_note_id == m_note_datapoint)
	{
		for(unsigned int i_dl = 0; i_dl < m_r_drivelines.size(); i_dl++)
		{
			Record &r_dl = m_r_drivelines[i_dl];
			sp<DrivelineI> spDriveline = m_asDrivelineLive.spDrivelineI(r_dl);
			//spDriveline->react(a_note_id);
			spDriveline->sync();
		}
	}
#if 0
	if(r_time.isValid())
	{
		t_moa_real dt = m_asTime.time(r_time) - m_time;
		step(dt);
		m_time += dt;
	}
#endif
}

} /* namespace ext */
} /* namespace fe */
