/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __driveline_DrivelineI_h__
#define __driveline_DrivelineI_h__

namespace fe
{
namespace ext
{

class FE_DL_EXPORT DrivelineI
	: virtual public Component,
	public CastableAs<DrivelineI>
{
	public:
virtual	void	step(t_moa_real a_dt)										= 0;
//virtual	void	react(const t_note_id &a_note_id) {}
virtual void	sync(void) {}
virtual	bool	compile(Record r_driveline)									= 0;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __driveline_DrivelineI_h__ */


