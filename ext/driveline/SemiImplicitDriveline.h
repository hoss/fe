/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __driveline_SemiImplicitDriveline_h__
#define __driveline_SemiImplicitDriveline_h__

namespace fe
{
namespace ext
{

class FE_DL_EXPORT DrivelineItem : public SemiImplicit1D::Force,
	public CastableAs<DrivelineItem>
{
	public:
		DrivelineItem(void);
virtual ~DrivelineItem(void);

		void	setDriveline(sp<DrivelineI> a_spDriveLine)
					{ m_spDriveLine = a_spDriveLine; }

	protected:
		hp<DrivelineI> m_spDriveLine;

};

class ShaftForce : public DrivelineItem, public CastableAs<ShaftForce>
{
	public:
		ShaftForce(sp<SemiImplicit1D> a_integrator)
		{
			m_force = 0.0;
		}
virtual	~ShaftForce(void) {}

		void bind(SemiImplicit1D::Particle *a_particle)
		{
			m_particle = a_particle;
		}

virtual	void accumulate(void)
		{
			m_particle->m_force += (t_solve_real)(m_force);
		}

		void	set(const t_solve_real &a_force)
		{
			m_force = a_force * m_particle->m_ratio;
fe_fprintf(stderr, "FRC %g %g\n", m_force, a_force);
		}
		t_solve_real	get(void)
		{
			return m_particle->m_force / m_particle->m_ratio;
		}
		t_solve_real	location(void)
		{
			return m_particle->m_location_ratio;
		}
		t_solve_real	velocity(void)
		{
			return m_particle->m_velocity * m_particle->m_ratio;
		}
		t_solve_real	coreVelocity(void)
		{
			return m_particle->m_velocity;
		}
		void	setvelocity(const t_solve_real &a_value)
		{
			m_particle->m_velocity = a_value / m_particle->m_ratio;
		}

	private:
		SemiImplicit1D::Particle	*m_particle;
		t_solve_real					m_force;
};

/// @brief Driveline Solver
class FE_DL_EXPORT SemiImplicitDriveline
	: virtual public DrivelineI,
	public CastableAs<SemiImplicitDriveline>
{
	public:
					SemiImplicitDriveline(void);
virtual				~SemiImplicitDriveline(void);

virtual	void		initialize(void);

virtual	void	step(t_moa_real a_dt);
virtual	void	sync(void);
virtual	bool	compile(Record r_driveline);

	class Shaft
	{
		public:
			Record				m_r_parent;
			sp<ShaftForce>		m_spHookForce;
			bool				m_has_externalTorque;
	};

	private:
		sp<SemiImplicit1D>			m_spSemiImplicit;
		sp<RecordGroup>				m_rg_driveline;
		AsParticle1D				m_asParticle1D;
		AsSolverParticle1D			m_asSolverParticle;
		AsShaft						m_asShaft;
		AsExternalTorque			m_asExternalTorque;
		std::map< String, Shaft >	m_shafts;

};

} /* namespace ext */
} /* namespace fe */

#endif /* __driveline_SemiImplicitDriveline_h__ */

