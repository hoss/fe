/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <operator/operator.pmh>

#define FE_OSD_DEBUG		FALSE

using namespace fe;
using namespace fe::ext;

void OpenSubdivOp::initialize(void)
{
	catalog<String>("icon")="FE_alpha";

	catalog<I32>("depth")=1;
	catalog<I32>("depth","high")=8;
	catalog<I32>("depth","max")=32;
	catalog<String>("depth","label")="Depth";
	catalog<String>("depth","hint")=
			"Number of subdivision iterations.";

	catalog< sp<Component> >("Input Surface");
	catalog<BWORD>("Input Surface","optional")=TRUE;

	catalog< sp<Component> >("Output Surface");
	catalog<String>("Output Surface","IO")="output";
}

void OpenSubdivOp::handle(Record& a_rSignal)
{
#if FE_OSD_DEBUG
	feLog("OpenSubdivOp::handle node \"%s\"\n",name().c_str());
#endif

	String& rSummary=catalog<String>("summary");
	rSummary="FAIL";

	const I32 depth=catalog<I32>("depth");

	sp<SurfaceAccessibleI> spOutputAccessible;
	if(!accessOutput(spOutputAccessible,a_rSignal)) return;

	sp<SurfaceAccessibleI> spInputAccessible;
	if(!access(spInputAccessible,"Input Surface")) return;

	sp<SurfaceAccessibleI> spSubdivAccessible=
			registry()->create("SurfaceAccessibleI.*.*.opensubdiv");

	if(spSubdivAccessible.isNull())
	{
		feLog("OpenSubdivOp::handle failed to access OpenSubdiv\n");
		return;
	}

	spSubdivAccessible->copy(spInputAccessible);

	sp<SurfaceAccessorI> spSubdivProperties;
	if(!access(spSubdivProperties,spSubdivAccessible,e_detail,e_properties))
	{
		feLog("OpenSubdivOp::handle failed to access subdiv properties\n");
		return;
	}

	spSubdivProperties->set(e_depth,0,depth);

	spOutputAccessible->copy(spSubdivAccessible);

	rSummary=depth? String("depth ")+depth: String("");

#if FE_OSD_DEBUG
	feLog("OpenSubdivOp::handle node \"%s\" done\n",name().c_str());
#endif
}
