/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __operator_StashOp_h__
#define __operator_StashOp_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Store a copy of the input in the master catalog

	@ingroup operator
*//***************************************************************************/
class FE_DL_EXPORT StashOp:
	public OperatorSurfaceCommon,
	public Initialize<StashOp>
{
	public:

					StashOp(void)											{}
virtual				~StashOp(void)											{}

		void		initialize(void);

					//* As HandlerI
virtual	void		handle(Record& a_rSignal);

	private:

		sp<SurfaceAccessibleI>	m_spStashAccessible;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __operator_StashOp_h__ */
