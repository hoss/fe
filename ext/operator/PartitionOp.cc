/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <operator/operator.pmh>

#define FE_PART_DEBUG	FALSE

namespace fe
{
namespace ext
{

void PartitionOp::initialize(void)
{
	catalog<String>("icon")="FE_alpha";

	catalog<String>("mode")="group";
	catalog<String>("mode","label")="Mode";
	catalog<String>("mode","choice:0")="group";
	catalog<String>("mode","label:0")="Any Group";
	catalog<String>("mode","choice:1")="connectivity";
	catalog<String>("mode","label:1")="Connectivity";
	catalog<String>("mode","choice:2")="shader";
	catalog<String>("mode","label:2")="Shader";

	catalog<String>("class")="primitive";
	catalog<String>("class","label")="Class";
	catalog<String>("class","choice:0")="point";
	catalog<String>("class","label:0")="Point";
	catalog<String>("class","choice:1")="primitive";
	catalog<String>("class","label:1")="Primitive";

	catalog<String>("partitionAttr")="part";
	catalog<String>("partitionAttr","label")="Partition Attr";

	catalog<String>("partitionText")="PART";
	catalog<String>("partitionText","label")="Partition Text";
	catalog<bool>("partitionText","joined")=true;

	catalog<String>("pattern")="PART";
	catalog<String>("pattern","label")="Replace Pattern";
	catalog<String>("pattern","hint")=
			"Replace this string in the Partition Text with"
			" a string unique to each partition.";

	catalog< sp<Component> >("Input Surface");
}

void PartitionOp::handle(Record& a_rSignal)
{
	catalog<String>("summary")="";

	const String mode=catalog<String>("mode");
	const String partitionAttr=catalog<String>("partitionAttr");
	const String partitionText=catalog<String>("partitionText");
	const String pattern=catalog<String>("pattern");
	const String attrClass=catalog<String>("class");
	catalog<String>("summary")=partitionAttr;

	const Element element=(attrClass=="primitive")? e_primitive: e_point;

	sp<SurfaceAccessibleI> spOutputAccessible;
	if(!accessOutput(spOutputAccessible,a_rSignal)) return;

	sp<SurfaceAccessibleI> spInputAccessible;
	if(!access(spInputAccessible,"Input Surface")) return;

	sp<SurfaceAccessorI> spOutputPartition;
	if(!access(spOutputPartition,spOutputAccessible,
			element,partitionAttr)) return;

	sp<SurfaceAccessorI> spOutputVertices;
	if(!access(spOutputVertices,spOutputAccessible,
			e_primitive,e_vertices)) return;

	catalog<String>("summary")=mode+" to "+partitionAttr;

	if(mode=="group")
	{
		const Element groupElement=
				(attrClass=="primitive")? e_primitiveGroup: e_pointGroup;

		Array<String> groupNames;
		spInputAccessible->groupNames(groupNames,
				SurfaceAccessibleI::Element(element));

		const U32 groupCount=groupNames.size();

#if FE_PART_DEBUG
		feLog("PartitionOp::handle groupCount %d\n",groupCount);
#endif

		for(U32 groupIndex=0;groupIndex<groupCount;groupIndex++)
		{
			if(interrupted())
			{
				break;
			}

			const String groupName=groupNames[groupIndex];

			sp<SurfaceAccessorI> spInputGroup;
			if(!access(spInputGroup,spInputAccessible,
					groupElement,groupName)) continue;

			const U32 elementCount=spInputGroup->count();

#if FE_PART_DEBUG
			feLog("PartitionOp::handle group %d/%d \"%s\" count %d elements:",
					groupIndex,groupCount,groupName.c_str(),elementCount);
#endif

			for(U32 elementIndex=0;elementIndex<elementCount;elementIndex++)
			{
				const U32 reIndex=spInputGroup->integer(elementIndex);
				spOutputPartition->set(reIndex,groupName);

#if FE_PART_DEBUG
				feLog(" %d",reIndex);
#endif
			}

#if FE_PART_DEBUG
			feLog("\n");
#endif
		}

		return;
	}

	if(mode=="connectivity")
	{
		const SurfaceAccessibleI::AtomicChange atomicChange=
				SurfaceAccessibleI::e_pointsOfPrimitives;

		const String group;
		const U32 desiredCount=0;
		const BWORD checkPages=FALSE;

		sp<SpannedRange> spSpannedRange=
				spOutputAccessible->atomizeConnectivity(
				atomicChange,group,desiredCount,checkPages);

#if FE_PART_DEBUG
		feLog("PartitionOp::handle count %d SpannedRange\n%s\n",
				spOutputPartition->count(),
				spSpannedRange->dump().c_str());
#endif

		const U32 atomicCount=spSpannedRange->atomicCount();
		for(U32 atomicIndex=0;atomicIndex<atomicCount;atomicIndex++)
		{
			if(interrupted())
			{
				break;
			}

			String number;
			number.sPrintf("%d",atomicIndex);
			const String partition=partitionText.replace(pattern,number);

			const SpannedRange::MultiSpan& rMultiSpan=
					spSpannedRange->atomic(atomicIndex);

			const U32 spanCount=rMultiSpan.spanCount();
			for(U32 spanIndex=0;spanIndex<spanCount;spanIndex++)
			{
				const SpannedRange::Span& rSpan=rMultiSpan.span(spanIndex);
				const U32 start=rSpan[0];
				const U32 end=rSpan[1];

				for(U32 primitiveIndex=start;primitiveIndex<=end;
						primitiveIndex++)
				{
					if(attrClass=="primitive")
					{
						spOutputPartition->set(primitiveIndex,partition);
					}
					else
					{
						const U32 subCount=
								spOutputVertices->subCount(primitiveIndex);
						for(U32 subIndex=0;subIndex<subCount;subIndex++)
						{
							const U32 pointIndex=spOutputVertices->integer(
									primitiveIndex,subIndex);
							spOutputPartition->set(pointIndex,partition);
						}
					}
				}
			}
		}
		return;
	}

	if(mode=="shader")
	{
		sp<SurfaceAccessorI> spOutputShader;
		if(!access(spOutputShader,spOutputAccessible,
				e_primitive,".shader")) return;

		const U32 primitiveCount=spOutputShader->count();
		for(U32 primitiveIndex=0;primitiveIndex<primitiveCount;primitiveIndex++)
		{
			if(interrupted())
			{
				break;
			}

			const String shaderName=spOutputShader->string(primitiveIndex);
			const String partition=partitionText.replace(pattern,shaderName);

#if FE_PART_DEBUG
			feLog("PartitionOp::handle primitive %d/%d shader \"%s\"\n",
					primitiveIndex,primitiveCount,shaderName.c_str());
#endif

			if(attrClass=="primitive")
			{
				spOutputPartition->set(primitiveIndex,partition);
			}
			else
			{
				const U32 subCount=spOutputVertices->subCount(primitiveIndex);
				for(U32 subIndex=0;subIndex<subCount;subIndex++)
				{
					const U32 pointIndex=
							spOutputVertices->integer(primitiveIndex,subIndex);
					spOutputPartition->set(pointIndex,partition);
				}
			}
		}

		return;
	}
}

} /* namespace ext */
} /* namespace fe */
