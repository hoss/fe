/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __operator_QuiltOp_h__
#define __operator_QuiltOp_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Operator to add a back side surfaces

	@ingroup operator
*//***************************************************************************/
class FE_DL_EXPORT QuiltOp:
	public OperatorSurfaceCommon,
	public Initialize<QuiltOp>
{
	public:

					QuiltOp(void)											{}
virtual				~QuiltOp(void)											{}

		void		initialize(void);

					//* As HandlerI
virtual	void		handle(Record& a_rSignal);

	private:
};

} /* namespace ext */
} /* namespace fe */

#endif /* __operator_QuiltOp_h__ */

