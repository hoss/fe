/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __operator_TubeOp_h__
#define __operator_TubeOp_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief generate tubes from curves

	@ingroup operator
*//***************************************************************************/
class FE_DL_EXPORT TubeOp:
	public OperatorSurfaceCommon,
	public Initialize<TubeOp>
{
	public:

					TubeOp(void)											{}
virtual				~TubeOp(void)											{}

		void		initialize(void);

					//* As HandlerI
virtual	void		handle(Record& a_rSignal);
};

} /* namespace ext */
} /* namespace fe */

#endif /* __operator_TubeOp_h__ */
