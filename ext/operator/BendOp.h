/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __operator_BendOp_h__
#define __operator_BendOp_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Bend surface to the side

	@ingroup operator
*//***************************************************************************/
class FE_DL_EXPORT BendOp:
	public OperatorSurfaceCommon,
	public Initialize<BendOp>
{
	public:

					BendOp(void)											{}
virtual				~BendOp(void)											{}

		void		initialize(void);

					//* As HandlerI
virtual	void		handle(Record& a_rSignal);

	private:

		void		solve(SpatialTransform& a_rPartial,
						const SpatialTransform& a_rDelta,Real a_power) const;

		MatrixPower<SpatialTransform>	m_matrixPower;
		MatrixBezier<SpatialTransform>	m_matrixBezier;
		BWORD							m_useBezier;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __operator_BendOp_h__ */
