/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <operator/operator.pmh>

#define FE_SPO_DEBUG		FALSE

using namespace fe;
using namespace fe::ext;

void SurfaceProxyOp::initialize(void)
{
	catalog<String>("icon")="FE_alpha";

	catalog<bool>("fragment")=false;
	catalog<String>("fragment","label")="Fragment";
	catalog<bool>("fragment","joined")=true;
	catalog<String>("fragment","hint")=
			"Associate collections of primitives which have"
			" matching a fragment attribute.";

	catalog<String>("fragmentAttr")="part";
	catalog<String>("fragmentAttr","label")="By Attr";
	catalog<String>("fragmentAttr","enabler")="fragment";
	catalog<String>("fragmentAttr","hint")=
			"Name of string attribute describing membership"
			" in a collection of primitives.";

	catalog<I32>("segments")=2;
	catalog<I32>("segments","min")=2;
	catalog<I32>("segments","high")=8;
	catalog<I32>("segments","max")=100;
	catalog<String>("segments","label")="Spine Segments";
	catalog<String>("segments","hint")=
			"Number of edges down the center line of the result.";

	catalog<bool>("snap")=false;
	catalog<String>("snap","label")="Snap To Input";
	catalog<String>("snap","hint")=
			"Snap output to nearest existing input point.";

	catalog< sp<Component> >("Input Surface");

	catalog< sp<Component> >("Output Surface");
	catalog<String>("Output Surface","IO")="output";
}

void SurfaceProxyOp::handle(Record& a_rSignal)
{
#if FE_SPO_DEBUG
	feLog("SurfaceProxyOp::handle \"%s\"\n",name().c_str());
#endif

	const String fragmentAttr=catalog<String>("fragmentAttr");
	BWORD fragmented=(!fragmentAttr.empty() && catalog<bool>("fragment"));

	const I32 segments=catalog<I32>("segments");
	const BWORD snap=catalog<bool>("snap");

	String& rSummary=catalog<String>("summary");
	rSummary=fragmented? fragmentAttr: "";

	sp<SurfaceAccessibleI> spOutputAccessible;
	if(!accessOutput(spOutputAccessible,a_rSignal)) return;

	sp<SurfaceAccessorI> spOutputPoint;
	if(!access(spOutputPoint,spOutputAccessible,
			e_point,e_position)) return;

	sp<SurfaceAccessorI> spOutputVertices;
	if(!access(spOutputVertices,spOutputAccessible,
			e_primitive,e_vertices)) return;

	sp<SurfaceAccessibleI> spInputAccessible;
	if(!access(spInputAccessible,"Input Surface")) return;

	sp<SurfaceAccessorI> spInputPoint;
	if(!access(spInputPoint,spInputAccessible,e_point,e_position)) return;

	sp<SurfaceAccessorI> spInputVertices;
	if(!access(spInputVertices,spInputAccessible,
			e_primitive,e_vertices)) return;

	sp<SurfaceAccessorI> spOutputFragment;

	I32 fragmentCount=1;

	if(fragmented)
	{
		const String inputGroup="";	//* TODO

		spInputVertices->fragmentWith(SurfaceAccessibleI::e_primitive,
				fragmentAttr,inputGroup);
		fragmentCount=spInputVertices->fragmentCount();
		if(!fragmentCount)
		{
			fragmentCount=1;
			fragmented=FALSE;
		}

		access(spOutputFragment,spOutputAccessible,e_primitive,fragmentAttr);
	}

	const I32 outputPrimitiveCount=fragmentCount*segments*2;
	Array< Array<I32> > primVerts(outputPrimitiveCount);
	Array<String> fragmentOfPrimitive(outputPrimitiveCount);

	sp<SurfaceAccessibleI::FilterI> spFilter;

	for(I32 fragmentIndex=0;fragmentIndex<fragmentCount;fragmentIndex++)
	{
		if(interrupted())
		{
			break;
		}

		String fragmentName;
		I32 filterCount;
		if(fragmented)
		{
			fragmentName=spInputVertices->fragment(fragmentIndex);

			if(!spInputVertices->filterWith(fragmentName,spFilter) ||
					spFilter.isNull())
			{
				continue;
			}

			filterCount=spFilter->filterCount();
		}
		else
		{
			filterCount=spInputVertices->count();
		}

		if(!filterCount)
		{
			continue;
		}

		std::set<I32> pointSet;

		SpatialVector base(0.0,0.0,0.0);
		SpatialVector tip(0.0,0.0,0.0);
		Real farthest2=0.0;
		BWORD first=TRUE;

		for(I32 filterIndex=0;filterIndex<filterCount;filterIndex++)
		{
			const I32 reIndex=
					fragmented? spFilter->filter(filterIndex): filterIndex;

			const U32 subCount=spInputVertices->subCount(reIndex);
			if(!subCount)
			{
				continue;
			}

			if(first)
			{
				I32 pointIndex=spInputVertices->integer(reIndex,0);

				//* base is the lowest point index of the first primitive
				for(U32 subIndex=1;subIndex<subCount;subIndex++)
				{
					const I32 otherIndex=
							spInputVertices->integer(reIndex,subIndex);
					if(pointIndex>otherIndex)
					{
						pointIndex=otherIndex;
					}
				}

				base=spInputPoint->spatialVector(pointIndex);
				tip=base;
				first=FALSE;
			}

			for(U32 subIndex=0;subIndex<subCount;subIndex++)
			{
				const SpatialVector point=
						spInputVertices->spatialVector(reIndex,subIndex);
				const Real distance2=magnitudeSquared(point-base);
				if(farthest2<distance2)
				{
					farthest2=distance2;
					tip=point;
				}

				I32 pointIndex=spInputVertices->integer(reIndex,subIndex);
				pointSet.insert(pointIndex);
			}
		}

		SpatialVector sum(0.0,0.0,0.0);
		I32 count=0;

		for(std::set<I32>::iterator it=pointSet.begin();
				it!=pointSet.end();it++)
		{
			const I32 pointIndex= *it;

			sum+=spInputPoint->spatialVector(pointIndex);
			count++;
		}

		const SpatialVector center=sum/Real(count);

		const SpatialVector stem=unitSafe(tip-base);
		SpatialVector aside(0.0,0.0,0.0);
		Real widest2=0.0;

		for(std::set<I32>::iterator it=pointSet.begin();
				it!=pointSet.end();it++)
		{
			const I32 pointIndex= *it;

			const SpatialVector point=
					spInputPoint->spatialVector(pointIndex);
			const SpatialVector away=point-center;
			const Real along=dot(away,stem);
			const SpatialVector apart=away-along*stem;
			const Real wide2=magnitudeSquared(apart);
			if(widest2<wide2)
			{
				widest2=wide2;
				aside=apart;
			}
		}

		const SpatialVector side=center+aside;

		SpatialVector opposition(0.0,0.0,0.0);
		widest2=0.0;

		for(std::set<I32>::iterator it=pointSet.begin();
				it!=pointSet.end();it++)
		{
			const I32 pointIndex= *it;

			const SpatialVector point=
					spInputPoint->spatialVector(pointIndex);

			const SpatialVector away=point-side;
			const Real along=dot(away,stem);
			const SpatialVector apart=away-along*stem;
			const Real wide2=magnitudeSquared(apart);
			if(widest2<wide2)
			{
				widest2=wide2;

				const SpatialVector awayB=side+apart-center;
				const Real alongB=dot(awayB,stem);

				opposition=awayB-alongB*stem;
			}
		}

		SpatialVector control[3];
		control[0]=base;
		control[1]=center;
		control[2]=tip;
		ConvergentSpline spline;
		const Real scaleT=spline.configure(3,control,NULL);

		const Real endScale=profile(0.4/Real(segments));
		const SpatialVector shortSide=endScale*aside;
		const SpatialVector shortOppose=endScale*opposition;

		const I32 addCount=6+3*(segments-1);
		const I32 pointIndexOut=spOutputPoint->append(addCount);

		I32 pointIndex=pointIndexOut;

		const SpatialVector spineAA=
				spline.solve(scaleT*0.4/Real(segments));
		spOutputPoint->set(pointIndex++,base);
		spOutputPoint->set(pointIndex++,spineAA+shortOppose);
		spOutputPoint->set(pointIndex++,spineAA+shortSide);

		SpatialVector spineB=base;
		for(I32 segmentIndex=0;segmentIndex<segments-1;segmentIndex++)
		{
			const Real unitT=(segmentIndex+1)/Real(segments);
			const Real t=scaleT*unitT;

//			const SpatialVector spineA=spineB;
			spineB=spline.solve(t);

			const Real sideScale=profile(unitT);

			spOutputPoint->set(pointIndex++,spineB+sideScale*opposition);
			spOutputPoint->set(pointIndex++,spineB);
			spOutputPoint->set(pointIndex++,spineB+sideScale*aside);
		}

		const SpatialVector spineBB=
				spline.solve(scaleT*(1.0-0.4/Real(segments)));
		spOutputPoint->set(pointIndex++,spineBB+shortOppose);
		spOutputPoint->set(pointIndex++,spineBB+shortSide);
		spOutputPoint->set(pointIndex++,tip);

		pointIndex=pointIndexOut;
		I32 primitiveIndex=fragmentIndex*segments*2;

		//* base
		primVerts[primitiveIndex].resize(4);
		primVerts[primitiveIndex][0]=pointIndex+4;
		primVerts[primitiveIndex][1]=pointIndex;
		primVerts[primitiveIndex][2]=pointIndex+2;
		primVerts[primitiveIndex][3]=pointIndex+5;
		fragmentOfPrimitive[primitiveIndex]=fragmentName;
		primitiveIndex++;

		primVerts[primitiveIndex].resize(4);
		primVerts[primitiveIndex][0]=pointIndex+1;
		primVerts[primitiveIndex][1]=pointIndex;
		primVerts[primitiveIndex][2]=pointIndex+4;
		primVerts[primitiveIndex][3]=pointIndex+3;
		fragmentOfPrimitive[primitiveIndex]=fragmentName;
		primitiveIndex++;

		pointIndex+=3;

		for(I32 middleIndex=0;middleIndex<segments-2;middleIndex++)
		{
			//* middle
			primVerts[primitiveIndex].resize(4);
			primVerts[primitiveIndex][0]=pointIndex+1;
			primVerts[primitiveIndex][1]=pointIndex+2;
			primVerts[primitiveIndex][2]=pointIndex+5;
			primVerts[primitiveIndex][3]=pointIndex+4;
			fragmentOfPrimitive[primitiveIndex]=fragmentName;
			primitiveIndex++;

			primVerts[primitiveIndex].resize(4);
			primVerts[primitiveIndex][0]=pointIndex+3;
			primVerts[primitiveIndex][1]=pointIndex;
			primVerts[primitiveIndex][2]=pointIndex+1;
			primVerts[primitiveIndex][3]=pointIndex+4;
			fragmentOfPrimitive[primitiveIndex]=fragmentName;
			primitiveIndex++;

			pointIndex+=3;
		}

		//* tip
		primVerts[primitiveIndex].resize(4);
		primVerts[primitiveIndex][0]=pointIndex+1;
		primVerts[primitiveIndex][1]=pointIndex+2;
		primVerts[primitiveIndex][2]=pointIndex+4;
		primVerts[primitiveIndex][3]=pointIndex+5;
		fragmentOfPrimitive[primitiveIndex]=fragmentName;
		primitiveIndex++;

		primVerts[primitiveIndex].resize(4);
		primVerts[primitiveIndex][0]=pointIndex+3;
		primVerts[primitiveIndex][1]=pointIndex;
		primVerts[primitiveIndex][2]=pointIndex+1;
		primVerts[primitiveIndex][3]=pointIndex+5;
		fragmentOfPrimitive[primitiveIndex]=fragmentName;
		primitiveIndex++;

		if(snap)
		{
			for(I32 addIndex=0;addIndex<addCount;addIndex++)
			{
				const I32 pointIndex=pointIndexOut+addIndex;
				SpatialVector point=spOutputPoint->spatialVector(pointIndex);

				SpatialVector closestPoint=point;
				Real closestDistance2= -1.0;

				for(std::set<I32>::iterator it=pointSet.begin();
						it!=pointSet.end();it++)
				{
					const I32 pointIndex= *it;

					const SpatialVector other=
							spInputPoint->spatialVector(pointIndex);

					const Real distance2=magnitudeSquared(other-point);

					if(closestDistance2>distance2 || closestDistance2<0.0)
					{
						closestDistance2=distance2;
						closestPoint=other;
					}
				}

				spOutputPoint->set(pointIndex,closestPoint);
			}
		}
	}

	spOutputVertices->append(primVerts);

	if(spOutputFragment.isValid())
	{
		for(I32 primitiveIndex=0;primitiveIndex<outputPrimitiveCount;
				primitiveIndex++)
		{
			spOutputFragment->set(primitiveIndex,
					fragmentOfPrimitive[primitiveIndex]);
		}
	}

#if FE_HGO_DEBUG
	feLog("SurfaceProxyOp::handle done\n");
#endif
}

Real SurfaceProxyOp::profile(Real a_fraction)
{
	return powf(cosf(fe::pi*fabs(0.5-a_fraction)),0.5);
}
