/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <operator/operator.pmh>

#define FE_TBO_DEBUG		FALSE

using namespace fe;
using namespace fe::ext;

void TubeOp::initialize(void)
{
	catalog<String>("icon")="FE_alpha";

	catalog<String>("copyAttr")="";
	catalog<String>("copyAttr","label")="Copy Attributes";
	catalog<bool>("copyAttr","joined")=true;
	catalog<String>("copyAttr","hint")=
			"Copy these Template point attributes to output primitives."
			"  Each space-delimited substring is an individual pattern."
			"  An attribute qualifies if it matches"
			" any of the substring patterns."
			"  Wildcard matching uses globbing unless Regex is turned on.";

	catalog<bool>("copyRegex")=false;
	catalog<String>("copyRegex","label")="Regex";
	catalog<String>("copyRegex","hint")=
			"Pass the pattern strings directly as true regular expressions."
			"  If this option is off,"
			" presumed glob patterns are converted to regex."
			"  Every '.' is first escaped as '\\.'."
			"  Then, every '?', indicating 'any character',"
			" is converted to '.'."
			"  Every '*', indicating 'any number of any characters',"
			" is converted to '.*'."
			"  Every '[!', indicating 'not' inside of square brackets,"
			" is converted to '[^'.";

	catalog< sp<Component> >("Input Curves");

	//* NOTE no auto-copy
	catalog< sp<Component> >("Output Surface");
	catalog<String>("Output Surface","IO")="output";
}

void TubeOp::handle(Record& a_rSignal)
{
#if FE_TBO_DEBUG
	feLog("TubeOp::handle node \"%s\"\n",name().c_str());
#endif

	String& rSummary=catalog<String>("summary");
	rSummary="";

	sp<DrawI> spDrawI;
	if(!accessDraw(spDrawI,a_rSignal)) return;

	sp<SurfaceAccessibleI> spInputAccessible;
	if(!access(spInputAccessible,"Input Curves")) return;

	sp<SurfaceAccessorI> spInputVertices;
	if(!access(spInputVertices,spInputAccessible,
				e_primitive,e_vertices)) return;

	sp<SurfaceAccessorI> spInputColor;
	access(spInputColor,spInputAccessible,e_point,e_color,e_warning);

	sp<SurfaceAccessorI> spInputRadius;
	access(spInputRadius,spInputAccessible,e_point,"radius",e_warning);

	const I32 primitiveCount=spInputVertices->count();

#if FE_TBO_DEBUG
	feLog("TubeOp::handle primitiveCount %d\n",primitiveCount);
#endif

	for(I32 primitiveIndex=0;primitiveIndex<primitiveCount;primitiveIndex++)
	{
		const I32 subCount=spInputVertices->subCount(primitiveIndex);

#if FE_TBO_DEBUG
		feLog("TubeOp::handle primitive %d/%d subCount %d\n",
				primitiveIndex,primitiveCount,subCount);
#endif

		if(subCount<2)
		{
			continue;
		}

		SpatialVector* line=new SpatialVector[subCount];
		Color* color=new Color[subCount];
		Real* radius=new Real[subCount];

		for(I32 subIndex=0;subIndex<subCount;subIndex++)
		{
			line[subIndex]=
					spInputVertices->spatialVector(primitiveIndex,subIndex);

			const I32 pointIndex=
					spInputVertices->integer(primitiveIndex,subIndex);

			if(spInputRadius.isValid())
			{
				radius[subIndex]=
						spInputRadius->real(pointIndex);
			}

			if(spInputColor.isValid())
			{
				color[subIndex]=
						spInputColor->spatialVector(pointIndex);
			}
		}

		//* TODO
		const SpatialVector towardCamera(0.0,0.0,0.0);

		OperateCommon::drawTube(spDrawI,line,NULL,
				spInputRadius.isValid()? radius: NULL,
				subCount,OperateCommon::e_tube,TRUE,
				spInputColor.isValid()? color: NULL,
				towardCamera);

		delete[] radius;
		delete[] color;
		delete[] line;
	}

	String copyAttr=catalog<String>("copyAttr");
	if(!copyAttr.empty())
	{
		if(!catalog<bool>("copyRegex"))
		{
			copyAttr=copyAttr.convertGlobToRegex();
		}

		sp<SurfaceAccessibleI> spOutputAccessible;
		if(!accessOutput(spOutputAccessible,a_rSignal)) return;

		const I32 grain=1;
		I32 outputStart=0;

		for(I32 inputPrimitiveIndex=0;inputPrimitiveIndex<primitiveCount;
				inputPrimitiveIndex++)
		{
			const I32 templateStart=
					spInputVertices->integer(inputPrimitiveIndex,0);
			const I32 templateCount=
					spInputVertices->subCount(inputPrimitiveIndex);

			SurfaceCopyOp::copyAttributes(copyAttr,spInputAccessible,
					spOutputAccessible,templateStart,templateCount-1,
					outputStart,grain);

			outputStart+=templateCount-1;
		}
	}

	rSummary.sPrintf("%d tubes",primitiveCount);

#if FE_TBO_DEBUG
	feLog("TubeOp::handle node \"%s\" done\n",name().c_str());
#endif
}
