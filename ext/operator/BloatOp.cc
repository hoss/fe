/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <operator/operator.pmh>

#define FE_BLO_DEBUG		FALSE

using namespace fe;
using namespace fe::ext;

void BloatOp::initialize(void)
{
	catalog<String>("Group");
	catalog<String>("Group","label")="Point Group";
	catalog<String>("Group","suggest")="pointGroups";

	catalog<Real>("Envelope")=1.0;
	catalog<bool>("Envelope","paintable")=true;
	catalog<String>("Envelope","hint")="Linearly adjust all effects.";

	catalog<bool>("Weighted")=false;
	catalog<bool>("Weighted","joined")=true;
	catalog<String>("Weighted","hint")=
			"Use locking attribute to resist change.";

	catalog<String>("weightAttr")="bloat";
	catalog<String>("weightAttr","label")="Weight Attribute";
	catalog<String>("weightAttr","enabler")="Weighted";
	catalog<String>("weightAttr","hint")=
			"Name of real attribute describing participation in change"
			" (1.0 is full effect).";

	catalog<Real>("weightOffset")=0.0;
	catalog<Real>("weightOffset","min")= -1e6;
	catalog<Real>("weightOffset","low")= -1.0;
	catalog<Real>("weightOffset","high")=1.0;
	catalog<Real>("weightOffset","max")=1e6;
	catalog<String>("weightOffset","label")="Weight Offset";
	catalog<String>("weightOffset","enabler")="Weighted";
	catalog<String>("weightOffset","hint")=
			"Add this value to all the weight values.";

	catalog<Real>("Scaling")=1.0;
	catalog<Real>("Scaling","min")= -100.0;
	catalog<Real>("Scaling","low")= 0.0;
	catalog<Real>("Scaling","high")=2.0;
	catalog<Real>("Scaling","max")=100.0;
	catalog<String>("Scaling","hint")=
			"Scale evenly around center, like a common transform.";

	catalog<Real>("Expansion")=0.0;
	catalog<Real>("Expansion","min")= -100.0;
	catalog<Real>("Expansion","low")= -1.0;
	catalog<Real>("Expansion","high")=1.0;
	catalog<Real>("Expansion","max")=100.0;
	catalog<String>("Expansion","hint")=
			"Scale away from center, with a uniform distance.";

	catalog<Real>("Bloat")=0.0;
	catalog<Real>("Bloat","min")= -100.0;
	catalog<Real>("Bloat","low")= -1.0;
	catalog<Real>("Bloat","high")=1.0;
	catalog<Real>("Bloat","max")=100.0;
	catalog<String>("Bloat","hint")=
			"Scale in direction of normals, with a uniform distance.";

	catalog< sp<Component> >("Input Surface");
	catalog<String>("Input Surface","iterate")="Input Surfaces";

	catalog< sp<Component> >("Output Surface");
	catalog<String>("Output Surface","IO")="output";
	catalog<String>("Output Surface","copy")="Input Surface";
	catalog<String>("Output Surface","iterate")="Output Surfaces";

	catalog< Array< sp<Component> > >("Input Surfaces");

	catalog< Array< sp<Component> > >("Output Surfaces");
	catalog<String>("Output Surfaces","IO")="output";
}

void BloatOp::handle(Record& a_rSignal)
{
#if FE_BLO_DEBUG
	feLog("BloatOp::handle\n");
#endif

	const Real envelope=catalog<Real>("Envelope");
	const Array<Real> envelopePainting=
			catalog< Array<Real> >("Envelope","painting");
	const U32 envPaintSize=envelopePainting.size();

	const String weightAttr=catalog<String>("weightAttr");
	const BWORD weighted=(catalog<bool>("Weighted") && !weightAttr.empty());

	const Real weightOffset=catalog<Real>("weightOffset");
	const Real bloat=catalog<Real>("Bloat");
	const Real expansion=catalog<Real>("Expansion");
	const Real scaling=catalog<Real>("Scaling");

	const String group=catalog<String>("Group");
	const BWORD grouped=(!group.empty());

	catalog<String>("summary")=group;

	sp<SurfaceAccessibleI> spOutputAccessible;
	if(!accessOutput(spOutputAccessible,a_rSignal)) return;

	sp<SurfaceAccessibleI> spInputAccessible;
	if(!access(spInputAccessible,"Input Surface")) return;

	sp<SurfaceAccessorI> spInputWeight;
	if(weighted)
	{
		access(spInputWeight,spInputAccessible,e_point,weightAttr,e_warning);
	}

	sp<SurfaceAccessorI> spInputPoint;
	if(grouped)
	{
		if(!access(spInputPoint,spInputAccessible,e_pointGroup,group)) return;
	}
	else
	{
		if(!access(spInputPoint,spInputAccessible,e_point,e_position)) return;
	}

	sp<SurfaceAccessorI> spOutputPoint;
	if(!access(spOutputPoint,spOutputAccessible,e_point,e_position)) return;

	const U32 count=spInputPoint->count();
	if(!count)
	{
		catalog<String>("warning")="no points in input surface";
		return;
	}

#if FE_BLO_DEBUG
	feLog("BloatOp::handle count %d bloat %.6G expansion %.6G scaling %.6G\n",
			count,bloat,expansion,scaling);
#endif

	sp<SurfaceAccessorI> spInputNormal;
	if(bloat!=0.0)
	{
		access(spInputNormal,spInputAccessible, e_point,e_normal,e_warning);
	}

	BWORD normalWarning=FALSE;

	if(expansion!=0.0 || scaling!=1.0)
	{
		SpatialVector min=spInputPoint->spatialVector(0);
		SpatialVector max=min;
		for(U32 index=1;index<count;index++)
		{
			if(interrupted())
			{
				break;
			}

			const SpatialVector point=spInputPoint->spatialVector(index);

			for(U32 m=0;m<3;m++)
			{
				const Real value=point[m];
				if(min[m]>value)
				{
					min[m]=value;
				}
				if(max[m]<value)
				{
					max[m]=value;
				}
			}
		}
		const SpatialVector center=0.5*(min+max);

		for(U32 index=0;index<count;index++)
		{
			if(interrupted())
			{
				break;
			}

			const SpatialVector point=spInputPoint->spatialVector(index);
			const I32 pointIndex=grouped? spInputPoint->integer(index): index;
			const SpatialVector normal=spInputNormal.isValid()?
					spInputNormal->spatialVector(pointIndex):
					SpatialVector(0.0,0.0,0.0);
			const Real weight=(spInputWeight.isValid())?
					spInputWeight->real(pointIndex)+weightOffset: 1.0;
			const Real fraction=envelope*weight*
					(index<envPaintSize? envelopePainting[pointIndex]: 1.0f);

			const SpatialVector bloated=center+
					((scaling-1.0)*fraction+1.0)*(point-center)+
					fraction*(unitSafe(point-center)*expansion+normal*bloat);

			spOutputPoint->set(pointIndex,bloated);

			if(bloat!=0.0 &&
					fabs(normal[0])+fabs(normal[1])+fabs(normal[2])<1e-6)
			{
				normalWarning=TRUE;
			}

#if FE_BLO_DEBUG
			feLog("BloatOp::handle %d/%d %s -> %s weight %.6G\n",
					index,count,c_print(point),c_print(bloated),weight);
#endif
		}
	}
	else
	{
		//* simplified version (normal-based bloat only)
		for(U32 index=0;index<count;index++)
		{
			if(interrupted())
			{
				break;
			}

			const SpatialVector point=spInputPoint->spatialVector(index);
			const I32 pointIndex=grouped? spInputPoint->integer(index): index;
			const SpatialVector normal=spInputNormal.isValid()?
					spInputNormal->spatialVector(pointIndex):
					SpatialVector(0.0,0.0,0.0);
			const Real weight=(spInputWeight.isValid())?
					spInputWeight->real(pointIndex)+weightOffset: 1.0;
			const Real fraction=envelope*weight*
					(pointIndex<I32(envPaintSize)?
					envelopePainting[pointIndex]: 1.0f);

			if(fabs(normal[0])+fabs(normal[1])+fabs(normal[2])<1e-6)
			{
				normalWarning=TRUE;
			}

			const SpatialVector bloated=point+fraction*normal*bloat;

			spOutputPoint->set(pointIndex,bloated);

#if FE_BLO_DEBUG
			feLog("BloatOp::handle simple %d/%d %s -> %s weight %.6G\n",
					index,count,c_print(point),c_print(bloated),weight);
#endif
		}
	}

	if(normalWarning)
	{
		catalog<String>("warning")+="null normals detected;";
	}

#if FE_BLO_DEBUG
	feLog("BloatOp::handle done\n");
#endif
}
