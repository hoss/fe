/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <operator/operator.pmh>

namespace fe
{
namespace ext
{

void CurveCreateOp::initialize(void)
{
	catalog<String>("icon")="FE_alpha";

	catalog<bool>("partitionOnly")=FALSE;
	catalog<String>("partitionOnly","label")="Single Part";
	catalog<bool>("partitionOnly","joined")=true;
	catalog<String>("partitionOnly","hint")=
			"Only trace through points with attribute value"
			" matching a given string.";

	catalog<String>("partitionAttr")="part";
	catalog<String>("partitionAttr","label")="Part Attribute";
	catalog<String>("partitionAttr","enabler")="partitionOnly";
	catalog<bool>("partitionAttr","joined")=true;
	catalog<String>("partitionAttr","hint")=
			"Look to the given point attribute for the subset of points.";

	catalog<String>("partitionName")="";
	catalog<String>("partitionName","label")="Match";
	catalog<String>("partitionName","enabler")="partitionOnly";
	catalog<String>("partitionName","hint")=
			"Only use input points with the attribute matching this string.";

	catalog<String>("trace")="points";
	catalog<String>("trace","label")="Trace Through";
	catalog<String>("trace","choice:0")="points";
	catalog<String>("trace","label:0")="Each Point";
	catalog<String>("trace","choice:1")="connectivity";
	catalog<String>("trace","label:1")="Connected Patches";

	catalog<String>("interpolation")="power";
	catalog<String>("interpolation","label")="Interpolation";
	catalog<String>("interpolation","choice:0")="linear";
	catalog<String>("interpolation","label:0")="Linear";
	catalog<String>("interpolation","choice:1")="pointNormal";
	catalog<String>("interpolation","label:1")="Point Normal";
	catalog<String>("interpolation","choice:2")="cardinal";
	catalog<String>("interpolation","label:2")="Cardinal Spline";
	catalog<String>("interpolation","choice:3")="basis";
	catalog<String>("interpolation","label:3")="Basis Spline";
	catalog<String>("interpolation","choice:4")="bezier";
	catalog<String>("interpolation","label:4")="Bezier";
	catalog<String>("interpolation","choice:5")="power";
	catalog<String>("interpolation","label:5")="Power";

	catalog<bool>("converge")=FALSE;
	catalog<String>("converge","label")="Converge";
	catalog<bool>("converge","joined")=true;
	catalog<String>("converge","hint")=
			"For Basis Spline, push curve towards control points.";

	catalog<I32>("iterations")=100;
	catalog<I32>("iterations","high")=100;
	catalog<I32>("iterations","max")=10000;
	catalog<String>("iterations","label")="Iterations";
	catalog<String>("iterations","enabler")="converge";
	catalog<bool>("iterations","joined")=true;
	catalog<String>("iterations","hint")=
			"Maximum steps towards convergence."
			"  Iteration will stop if the threshold is met.";

	catalog<Real>("threshold")=1e-3;
	catalog<Real>("threshold","high")=1.0;
	catalog<Real>("threshold","min")=0.0;
	catalog<Real>("threshold","max")=1e6;
	catalog<String>("threshold","label")="Threshold";
	catalog<bool>("threshold","slider")=false;
	catalog<String>("threshold","enabler")="converge";
	catalog<String>("threshold","hint")=
			"Minimum change to continue convergence (very small, like 0.001).";

	catalog<bool>("bisect")=false;
	catalog<String>("bisect","label")="Bisect";
	catalog<bool>("bisect","joined")=true;

	catalog<bool>("blend")=false;
	catalog<String>("blend","label")="Blend";
	catalog<bool>("blend","joined")=true;

	catalog<Real>("blending")=1.0;
	catalog<String>("blending","label")="Blending";
	catalog<Real>("blending","high")=2.0;
	catalog<Real>("blending","max")=100.0;
	catalog<String>("blending","enabler")="blend";

	catalog<String>("method")="length";
	catalog<String>("method","label")="Spacing Method";
	catalog<String>("method","choice:0")="length";
	catalog<String>("method","label:0")="Length";
	catalog<String>("method","choice:1")="count";
	catalog<String>("method","label:1")="Count";

	catalog<Real>("length")=1.0;
	catalog<Real>("length","low")=0.0;
	catalog<Real>("length","high")=1.0;
	catalog<Real>("length","min")=0.0;
	catalog<Real>("length","max")=1e3;
	catalog<String>("length","label")="Curve Length";
	catalog<bool>("length","joined")=true;

	catalog<bool>("tweakLength")=false;
	catalog<String>("tweakLength","label")="Tweak";
	catalog<String>("tweakLength","hint")=
			"For length Spacing Method, adjust the overall spacing"
			" to avoid a large difference on the last segment.";

	catalog<I32>("segments")=4;
	catalog<I32>("segments","low")=1;
	catalog<I32>("segments","high")=100;
	catalog<I32>("segments","min")=1;
	catalog<I32>("segments","max")=1000000;
	catalog<String>("segments","label")="Segment Count";
	catalog<bool>("segments","joined")=true;

	catalog<bool>("enforceSegments")=false;
	catalog<String>("enforceSegments","label")="Enforce Count";
	catalog<String>("enforceSegments","hint")=
			"For length method, stop adding points after"
			" reaching the segment count.";

	catalog<Real>("linearity")=0.0;
	catalog<Real>("linearity","max")=1.0;
	catalog<String>("linearity","label")="Linearity";

	catalog<String>("linearityInAttr")="linearityIn";
	catalog<String>("linearityInAttr","label")="Linearity Attr In";
	catalog<bool>("linearityInAttr","joined")=true;
	catalog<String>("linearityInAttr","hint")=
			"Name of real point attribute boosting linearity leading in.";

	catalog<String>("linearityOutAttr")="linearityOut";
	catalog<String>("linearityOutAttr","label")="Out";
	catalog<String>("linearityOutAttr","hint")=
			"Name of real point attribute boosting linearity trailing out.";

	catalog<String>("minDistInAttr")="minDistIn";
	catalog<String>("minDistInAttr","label")="Min Dist Attr In";
	catalog<bool>("minDistInAttr","joined")=true;
	catalog<String>("minDistInAttr","hint")=
			"Name of real point attribute of minimum length leading in.";

	catalog<String>("maxDistInAttr")="maxDistIn";
	catalog<String>("maxDistInAttr","label")="Max";
	catalog<String>("maxDistInAttr","hint")=
			"Name of real point attribute of maximum length leading in.";

	catalog<bool>("showInterpolation")=false;
	catalog<String>("showInterpolation","label")="Show Interpolation";
	catalog<String>("showInterpolation","hint")=
			"Show interpolation feedback.";

	catalog< sp<Component> >("Input Surface");
	catalog<bool>("Input Surface","optional")=true;

	//* no auto-copy
	catalog< sp<Component> >("Output Surface");
	catalog<String>("Output Surface","IO")="output";
	catalog<String>("Output Surface","copy")="";
}

void CurveCreateOp::handle(Record& a_rSignal)
{
	String& rSummary=catalog<String>("summary");
	rSummary="OFF";

	const String partitionAttr=catalog<String>("partitionAttr");
	const String partitionName=catalog<String>("partitionName");
	const BWORD partitionOnly=
			(catalog<bool>("partitionOnly") && !partitionAttr.empty());
	const String trace=catalog<String>("trace");
	const String interpolation=catalog<String>("interpolation");
	const BWORD blend=catalog<bool>("blend");
	const Real blending=catalog<Real>("blending");
	const BWORD bisect=catalog<bool>("bisect");
	const String method=catalog<String>("method");
	const Real length=catalog<Real>("length");
	const BWORD tweakLength=
			(method!="length" || catalog<bool>("tweakLength"));
	const BWORD enforceSegments=catalog<bool>("enforceSegments");
	const I32 segments=fe::maximum(I32(1),catalog<I32>("segments"));
	const Real linearity=catalog<Real>("linearity");
	const String linearityInAttr=catalog<String>("linearityInAttr");
	const String linearityOutAttr=catalog<String>("linearityOutAttr");
	const String minDistInAttr=catalog<String>("minDistInAttr");
	const String maxDistInAttr=catalog<String>("maxDistInAttr");

	const BWORD doConnectivity=(trace=="connectivity");

	const BWORD doLinear=(interpolation=="linear");
	const BWORD doCardinal=(interpolation=="cardinal");
	const BWORD doPointNormal=(interpolation=="pointNormal");
	const BWORD doBezier=(interpolation=="bezier");
	const BWORD doPower=(interpolation=="power");
	const BWORD doBasis=(interpolation=="basis");

	const Real segmentLength=length/Real(segments);

	const Real nonlinearity=Real(1)-linearity;

	sp<DrawI> spDrawI;
	if(!accessDraw(spDrawI,a_rSignal)) return;

	sp<DrawI> spDrawGuide;
	accessGuide(spDrawGuide,a_rSignal,e_warning);

	I32 pointCount=0;

	sp<SurfaceAccessibleI> spInputAccessible;
	sp<SurfaceAccessorI> spInputMatch;
	sp<SurfaceAccessorI> spInputPosition;
	sp<SurfaceAccessorI> spInputWaypoint;
	sp<SurfaceAccessorI> spInputNormal;
	sp<SurfaceAccessorI> spInputLinearityIn;
	sp<SurfaceAccessorI> spInputLinearityOut;
	sp<SurfaceAccessorI> spInputMinDistIn;
	sp<SurfaceAccessorI> spInputMaxDistIn;

	access(spInputAccessible,"Input Surface",e_quiet);
	if(spInputAccessible.isValid())
	{
		if(!doConnectivity && partitionOnly)
		{
			access(spInputMatch,spInputAccessible,
					e_point,partitionAttr,e_warning);
		}

		access(spInputPosition,spInputAccessible,
				e_point,e_position,e_warning);

		if(spInputPosition.isValid())
		{
			pointCount=spInputPosition->count();
		}

		access(spInputWaypoint,spInputAccessible,
				e_point,"waypoint",e_warning);

		if(doPointNormal || doBezier || doPower)
		{
			access(spInputNormal,spInputAccessible,
					e_point,e_normal,e_warning);
		}

		if(!linearityInAttr.empty())
		{
			access(spInputLinearityIn,spInputAccessible,
					e_point,linearityInAttr,e_warning);
		}

		if(!linearityOutAttr.empty())
		{
			access(spInputLinearityOut,spInputAccessible,
					e_point,linearityOutAttr,e_warning);
		}

		if(!minDistInAttr.empty())
		{
			access(spInputMinDistIn,spInputAccessible,
					e_point,minDistInAttr,e_warning);
		}

		if(!maxDistInAttr.empty())
		{
			access(spInputMaxDistIn,spInputAccessible,
					e_point,maxDistInAttr,e_warning);
		}
	}

	Array<I32> reindexArray;
	if(!doConnectivity)
	{
		if(spInputMatch.isNull())
		{
			reindexArray.resize(pointCount);
			for(I32 pointIndex=0;pointIndex<pointCount;pointIndex++)
			{
				reindexArray[pointIndex]=pointIndex;
			}
		}
		else
		{
			for(I32 pointIndex=0;pointIndex<pointCount;pointIndex++)
			{
				const String value=spInputMatch->string(pointIndex);
				if(value==partitionName)
				{
					reindexArray.push_back(pointIndex);
				}
			}

//			feLog("CurveCreateOp::handle matching pointCount %d -> %d\n",
//					pointCount,reindexArray.size());

			pointCount=reindexArray.size();
		}
	}

	if(pointCount<2)
	{
		//* no input path -> generate straight line

		SpatialVector* resultArray=new SpatialVector[segments+1];

		for(I32 index=0;index<=segments;index++)
		{
//			set(resultArray[index],0.001*index*index,index*segmentLength);
			set(resultArray[index],0.0,index*segmentLength);
		}

		spDrawI->drawLines(resultArray,NULL,segments+1,
				DrawI::e_strip,false,NULL);

		delete[] resultArray;

		rSummary.sPrintf("%d segment line",segments);

		return;
	}

	sp<SpannedRange> spSpannedRange;
	sp<SurfaceAccessorI> spInputVertices;

	if(doConnectivity)
	{
		if(!access(spInputVertices,spInputAccessible,
				e_primitive,e_vertices)) return;

		const SurfaceAccessibleI::AtomicChange atomicChange=
				SurfaceAccessibleI::e_pointsOfPrimitives;

		const String group;
		const U32 desiredCount=0;
		const BWORD checkPages=FALSE;

		spSpannedRange=spInputAccessible->atomizeConnectivity(
				atomicChange,group,desiredCount,checkPages);

		pointCount=spSpannedRange->atomicCount();
	}

	SpatialVector* pointArray=new SpatialVector[pointCount];
	SpatialVector* normalArray=new SpatialVector[pointCount];
	String* waypointArray=new String[pointCount];

	Real* linearityInArray=new Real[pointCount];
	Real* linearityOutArray=new Real[pointCount];
	Real* minDistInArray=new Real[pointCount];
	Real* maxDistInArray=new Real[pointCount];

	if(doConnectivity)
	{
		for(I32 atomicIndex=0;atomicIndex<pointCount;atomicIndex++)
		{
			if(interrupted())
			{
				break;
			}

			//* compute center
			SpatialVector pointSum(0.0,0.0,0.0);
			I32 pointTotal=0;

			const SpannedRange::MultiSpan& rMultiSpan=
					spSpannedRange->atomic(atomicIndex);

			const U32 spanCount=rMultiSpan.spanCount();
			for(U32 spanIndex=0;spanIndex<spanCount;spanIndex++)
			{
				const SpannedRange::Span& rSpan=rMultiSpan.span(spanIndex);
				const U32 start=rSpan[0];
				const U32 end=rSpan[1];

				for(U32 primitiveIndex=start;primitiveIndex<=end;
						primitiveIndex++)
				{
					const U32 subCount=
							spInputVertices->subCount(primitiveIndex);
					for(U32 subIndex=0;subIndex<subCount;subIndex++)
					{
						pointSum+=spInputVertices->spatialVector(
								primitiveIndex,subIndex);
						pointTotal++;
					}
				}
			}

			pointArray[atomicIndex]=pointSum*
				(1.0/Real(pointTotal? pointTotal: 1));

			//* TODO
			set(normalArray[atomicIndex],0.0,0.0,1.0);
		}
	}
	else
	{
		if(spInputPosition.isValid())
		{
			for(I32 pointIndex=0;pointIndex<pointCount;pointIndex++)
			{
				pointArray[pointIndex]=spInputPosition->spatialVector(
						reindexArray[pointIndex]);
			}
		}
		if(spInputNormal.isValid())
		{
			for(I32 pointIndex=0;pointIndex<pointCount;pointIndex++)
			{
				normalArray[pointIndex]=spInputNormal->spatialVector(
						reindexArray[pointIndex]);
			}
		}
		if(spInputWaypoint.isValid())
		{
			for(I32 pointIndex=0;pointIndex<pointCount;pointIndex++)
			{
				waypointArray[pointIndex]=spInputWaypoint->string(
						reindexArray[pointIndex]);
			}
		}
		if(spInputLinearityIn.isValid())
		{
			for(I32 pointIndex=0;pointIndex<pointCount;pointIndex++)
			{
				linearityInArray[pointIndex]=spInputLinearityIn->real(
						reindexArray[pointIndex]);
			}
		}
		if(spInputLinearityOut.isValid())
		{
			for(I32 pointIndex=0;pointIndex<pointCount;pointIndex++)
			{
				linearityOutArray[pointIndex]=spInputLinearityOut->real(
						reindexArray[pointIndex]);
			}
		}
		if(spInputMinDistIn.isValid())
		{
			for(I32 pointIndex=0;pointIndex<pointCount;pointIndex++)
			{
				minDistInArray[pointIndex]=spInputMinDistIn->real(
						reindexArray[pointIndex]);
			}
		}
		if(spInputMaxDistIn.isValid())
		{
			for(I32 pointIndex=0;pointIndex<pointCount;pointIndex++)
			{
				maxDistInArray[pointIndex]=spInputMaxDistIn->real(
						reindexArray[pointIndex]);
			}
		}
	}

	Real totalDistance=0.0;
	Real* distanceArray=new Real[pointCount];

	SpatialVector point0=pointArray[0];
	distanceArray[0]=0.0;

	for(I32 pointIndex=1;pointIndex<pointCount;pointIndex++)
	{
		const SpatialVector point1=pointArray[pointIndex];

		totalDistance+=magnitude(point1-point0);
		distanceArray[pointIndex]=totalDistance;

		point0=point1;
	}

	const I32 trueSegments=(method=="length" && segmentLength>0.0)?
			(enforceSegments?
			fe::minimum(segments,I32(totalDistance/segmentLength)):
			totalDistance/segmentLength): segments;

	ConvergentSpline convergentSpline;
	if(doBasis)
	{
		I32 iterations=0;
		Real threshold=0.0;

		if(catalog<bool>("converge"))
		{
			iterations=catalog<I32>("iterations");
			threshold=catalog<Real>("threshold");
		}

		convergentSpline.setIterations(iterations);
		convergentSpline.setThreshold(threshold);
		convergentSpline.configure(pointCount,pointArray,normalArray);
	}

	//* for power modes, precompute cv transforms
	SpatialTransform* xformArray=new SpatialTransform[pointCount];
	SpatialTransform* middleArray=new SpatialTransform[pointCount];

	if(doBezier || doPower)
	{
		for(I32 pointIndex=0;pointIndex<pointCount;pointIndex++)
		{
			if(interrupted())
			{
				break;
			}

			const I32 cv1=pointIndex;
			const I32 cv0=fe::maximum(cv1-1,I32(0));
//			const I32 cv00=fe::maximum(cv1-2,0);
			const I32 cv2=fe::minimum(cv1+1,pointCount-1);
//			const I32 cv3=fe::minimum(cv1+2,pointCount-1);

//			const SpatialVector p00=pointArray[cv00];
			const SpatialVector p0=pointArray[cv0];
			const SpatialVector p1=pointArray[cv1];
			const SpatialVector p2=pointArray[cv2];
//			const SpatialVector p3=pointArray[cv3];

			SpatialVector n0(0.0,1.0,0.0);
			SpatialVector n1(0.0,1.0,0.0);
			SpatialVector n2(0.0,1.0,0.0);

			if(spInputNormal.isValid())
			{
				n0=normalArray[cv0];
				n1=normalArray[cv1];
				n2=normalArray[cv2];
			}

			//* vertex tangent is parallel to connection of neighbors
//			const SpatialVector t1=unitSafe(p2-p0);

			//* vertex tangent is average of incoming and outgoing segments
			const SpatialVector t1=
					unitSafe(unitSafe(p2-p1)+unitSafe(p1-p0));

			//* vertex tangent uses sqrt between neighbors
//			const SpatialVector t0=
//					(cv1>cv00)? unitSafe(p1-p00): unitSafe(p2-p00);
//			const SpatialVector t2=
//					(cv3>cv1)? unitSafe(p3-p1): unitSafe(p3-p0);

//			SpatialTransform xform0;
//			makeFrameTangentX(xform0,p0,t0,n0);
//
//			SpatialTransform xform2;
//			makeFrameTangentX(xform2,p2,t2,n2);
//
//			SpatialTransform xform1;
//			halfTransform(xform1,xform0,xform2);
//
//			SpatialTransform xform1plus;
//			interpolateTransform(xform1plus,xform0,xform2,0.51);
//
//			const SpatialVector t1=unitSafe(
//					xform1plus.translation()-xform1.translation());

			makeFrameTangentX(xformArray[pointIndex],p1,t1,n1);
		}

		if(bisect)
		{
			for(I32 pointIndex=0;pointIndex<pointCount-1;pointIndex++)
			{
				if(interrupted())
				{
					break;
				}

				const I32 cv1=pointIndex;
				const I32 cv2=fe::minimum(cv1+1,pointCount-1);

				const SpatialVector p1=pointArray[cv1];
				const SpatialVector p2=pointArray[cv2];

				SpatialVector n1(0.0,1.0,0.0);
				SpatialVector n2(0.0,1.0,0.0);

				if(spInputNormal.isValid())
				{
					n1=normalArray[cv1];
					n2=normalArray[cv2];
				}

				const SpatialTransform xform1=xformArray[pointIndex];
				const SpatialTransform xform2=xformArray[pointIndex+1];

				//* halfway point is for position only
				SpatialTransform halfway;
				halfTransform(halfway,xform1,xform2);

				const SpatialVector ptMid=halfway.translation();
				const SpatialVector nMid=unitSafe(n1+n2);

#if FALSE
				const SpatialVector tMid=unitSafe(p2-p1);
#else
				const SpatialVector tMidPre=unitSafe(p2-p1);

				SpatialTransform middlePlus;
				makeFrameTangentX(middlePlus,ptMid,tMidPre,nMid);

				SpatialTransform halfwayPlus;
				interpolateTransform(halfwayPlus,middlePlus,xform2,
						0.01,doBezier);

				const SpatialVector pMidPlus=halfwayPlus.translation();

				const SpatialVector tMid=unitSafe(pMidPlus-ptMid);
#endif

				makeFrameTangentX(middleArray[pointIndex],ptMid,tMid,nMid);
			}
		}
		middleArray[pointCount-1]=middleArray[pointCount-2];
	}

	std::map<String,bool> compoundWaypoint;
	if(spInputWaypoint.isValid())
	{
		for(I32 pointIndex=0;pointIndex<pointCount;pointIndex++)
		{
			const String waypoint=waypointArray[pointIndex];
			compoundWaypoint[waypoint]=FALSE;
		}

		for(I32 pointIndex=1;pointIndex<pointCount;pointIndex++)
		{
			const I32 cv1=pointIndex-1;
			const I32 cv2=pointIndex;

			const String waypoint1=waypointArray[cv1];
			const String waypoint2=waypointArray[cv2];

			if(waypoint1==waypoint2)
			{
				compoundWaypoint[waypoint1]=TRUE;
			}
		}
	}

	SpatialVector* resultArray=new SpatialVector[trueSegments+1];
	SpatialVector* linearArray=new SpatialVector[trueSegments+1];
	String* wayResultArray=new String[trueSegments+1];

	Real* wayPartialArray=new Real[trueSegments+1];
	std::set<String> waypointSet;
	Array<I32> endIndexOfSpan;
	Array<I32> cv2OfSpan;

	I32 cv2=0;

	for(I32 index=0;index<=trueSegments;index++)
	{
		if(interrupted())
		{
			break;
		}

		Real distance;
		Real nextDistance;
		if(index==trueSegments)
		{
			distance=totalDistance;
			nextDistance=totalDistance;
		}
		else if(tweakLength)
		{
			const Real along=index/Real(trueSegments);
			distance=along*totalDistance;

			const Real nextAlong=(index+1)/Real(trueSegments);
			nextDistance=nextAlong*totalDistance;
		}
		else
		{
			distance=index*segmentLength;
			nextDistance=distance+segmentLength;
		}

		I32 lastCv2=cv2;
		while(distanceArray[cv2]<=distance && cv2<pointCount-1)
		{
			cv2++;
		}

		const I32 cv1=fe::maximum(cv2-1,I32(0));

		const Real delta=distanceArray[cv2]-distanceArray[cv1];
		const Real t=(delta>Real(0))?
				(distance-distanceArray[cv1])/delta: Real(0);
		const Real nextT=(delta>Real(0))?
				(nextDistance-distanceArray[cv1])/delta: Real(0);

		const Real t1=Real(1)-t;

		String waypoint;
		if(spInputWaypoint.isValid())
		{
			const I32 cvW=(index==trueSegments || nextT>1.0)? cv2: cv1;

			const Real partial=(index && nextT>t)? t1/(nextT-t): Real(0);

//			feLog("index %d cvW %d segmentLength %.6G dist %.6G next %.6G"
//					" t1 %.6G nextT %.6G t %.6G partial %.6G\n",
//					index,cvW,segmentLength,distance,nextDistance,
//					t1,nextT,t,partial);

			waypoint=spInputWaypoint->string(cvW);
			if(waypointSet.find(waypoint)==waypointSet.end())
			{
				waypointSet.insert(waypoint);
				wayResultArray[index]=waypoint;
				wayPartialArray[index]=partial;
			}
		}

		if(lastCv2>0 && cv2!=lastCv2 &&
				(waypoint=="" || !compoundWaypoint[waypoint]))
		{
			endIndexOfSpan.push_back(index);
			cv2OfSpan.push_back(lastCv2);
		}

		const SpatialVector p1=pointArray[cv1];
		const SpatialVector p2=pointArray[cv2];

		const SpatialVector ptLinear=p1*t1+p2*t;

		linearArray[index]=ptLinear;

		if(doLinear)
		{
			resultArray[index]=ptLinear;
			FEASSERT(checkValid(resultArray[index]));
			continue;
		}

		Real nonlinearityOut1=1.0;
		Real nonlinearityIn2=1.0;
		if(spInputLinearityOut.isValid())
		{
			nonlinearityOut1=Real(1)-linearityOutArray[cv1];
		}
		if(spInputLinearityIn.isValid())
		{
			nonlinearityIn2=Real(1)-linearityInArray[cv2];
		}

		const Real oneNonlinearity=
				nonlinearity*(nonlinearityOut1*t1+nonlinearityIn2*t);
		const Real oneLinearity=Real(1)-oneNonlinearity;

		if(doPointNormal)
		{
			SpatialVector n1(0.0,1.0,0.0);
			SpatialVector n2(0.0,1.0,0.0);

			if(spInputNormal.isValid())
			{
				n1=normalArray[cv1];
				n2=normalArray[cv2];
			}

			TrianglePN<Real> trianglePN;
			trianglePN.configure(p1,p2,p2,n1,n2,n2);

			const SpatialBary bary(t1,t,Real(0));
			SpatialVector point;
			SpatialVector norm;
			trianglePN.solve(bary,point,norm);

			resultArray[index]=oneLinearity*ptLinear+oneNonlinearity*point;
			FEASSERT(checkValid(resultArray[index]));
			continue;
		}

		if(doBasis)
		{
			resultArray[index]=oneLinearity*ptLinear+
					oneNonlinearity*convergentSpline.solve(distance);
			FEASSERT(checkValid(resultArray[index]));
			continue;
		}

		const I32 cv0=fe::maximum(cv2-2,I32(0));
		const I32 cv3=fe::minimum(cv2+1,pointCount-1);

		if(doCardinal)
		{
			const SpatialVector p0=pointArray[cv0];
			const SpatialVector p3=pointArray[cv3];

			resultArray[index]=oneLinearity*ptLinear+oneNonlinearity*
					Spline<SpatialVector,Real>::Cardinal2D(t,p0,p1,p2,p3);
			FEASSERT(checkValid(resultArray[index]));
			continue;
		}

		const SpatialTransform xform0=xformArray[cv0];
		const SpatialTransform xform1=xformArray[cv1];
		const SpatialTransform xform2=xformArray[cv2];
		const SpatialTransform xform3=xformArray[cv3];

		SpatialTransform between;
		SpatialTransform middle;

		SpatialTransform xformA=xform0;
		SpatialTransform xformB=xform1;
		SpatialTransform xformC=xform2;
		SpatialTransform xformD=xform3;
		Real tt=t;

		if(bisect)
		{
			if(t<0.5)
			{
				//* first half
				xformA=middleArray[cv0];

//*				xformB=xform1;	//* NOTE already is

				middle=middleArray[cv1];
				xformC=middle;

				xformD=xform2;

				tt=t*Real(2);
			}
			else
			{
				//* second half
				xformA=xform1;

				middle=middleArray[cv1];
				xformB=middle;

//*				xformC=xform2;	//* NOTE already is

				xformD=middleArray[cv2];

				tt=t*Real(2)-Real(1);
			}
		}

		if(blend)
		{
			const Real distBC=magnitude(
					xformC.translation()-xformB.translation());

			const Real leftT=pow(tt,blending);
			const Real rightT=Real(1)-pow(Real(1)-tt,blending);

#if TRUE
			//* NOTE keep curving

			const Real distAB=magnitude(
					xformB.translation()-xformA.translation());
			const Real distCD=magnitude(
					xformD.translation()-xformC.translation());

			//* accounting for non-uniform CV spacing
			const Real scaleAB=fe::minimum(1.0,
					(distAB>0.0)? distBC/distAB: 1.0);
			const Real scaleCD=fe::minimum(1.0,
					(distCD>0.0)? distBC/distCD: 1.0);

			SpatialTransform betweenAB;
			interpolateTransform(betweenAB,xformA,xformB,
					Real(1)+scaleAB*tt,doBezier);

			SpatialTransform betweenCD;
			interpolateTransform(betweenCD,xformD,xformC,
					Real(1)+scaleCD*(Real(1)-tt),doBezier);
#else
			//* NOTE extrapolate in a line

			SpatialTransform slightAB;
			interpolateTransform(slightAB,xformA,xformB,1.01,doBezier);

			SpatialTransform slightCD;
			interpolateTransform(slightCD,xformD,xformC,1.01,doBezier);

			const SpatialVector dirAB=unitSafe(
					slightAB.translation()-xformB.translation());
			const SpatialVector dirCD=unitSafe(
					slightAB.translation()-xformB.translation());

			SpatialTransform betweenAB=xformB;
			SpatialTransform betweenCD=xformC;

			betweenAB.translation()+=tt*distBC*dirAB;
			betweenCD.translation()+=(Real(1)-tt)*distBC*dirCD;
#endif

			SpatialTransform betweenBC;
			interpolateTransform(betweenBC,xformB,xformC,tt,doBezier);

			SpatialTransform betweenLeft;
			interpolateTransform(betweenLeft,betweenAB,betweenBC,
					leftT,doBezier);

			SpatialTransform betweenRight;
			interpolateTransform(betweenRight,betweenBC,betweenCD,
					rightT,doBezier);

			halfTransform(between,betweenLeft,betweenRight);
		}
		else
		{
			interpolateTransform(between,xformB,xformC,tt,doBezier);
		}

		resultArray[index]=
				oneLinearity*ptLinear+oneNonlinearity*between.translation();
		FEASSERT(checkValid(resultArray[index]));

		if(spDrawGuide.isValid() && catalog<bool>("showInterpolation"))
		{
			const Color lightred(1.0,0.3,0.3,1.0);
			const Color lightgreen(0.3,1.0,0.3,1.0);
			const Color lightblue(0.3,0.3,1.0,1.0);

			SpatialVector line[2];

			const U32 passes=bisect? 4: 3;

			for(U32 pass=0;pass<passes;pass++)
			{
				const SpatialTransform step=
						pass==0? xform1:
						(pass==1? xform2:
						(pass==2? between: middle));

				const Real guidelength=10.0;
				const Real scalar=guidelength*
						(pass<2? 1.0: (pass==2? 0.3: 0.5));

				line[0]=step.translation();

				for(U32 n=0;n<3;n++)
				{
					const Color color=
							n==0? lightred: (n==1? lightgreen: lightblue);

					line[1]=line[0]+scalar*step.column(n);

					spDrawGuide->drawLines(line,NULL,2,
							DrawI::e_strip,false,&color);
				}
			}
		}
	}

	delete[] distanceArray;
	delete[] middleArray;
	delete[] xformArray;

	endIndexOfSpan.push_back(trueSegments);
	cv2OfSpan.push_back(cv2);

	const I32 spanCount=endIndexOfSpan.size();
	for(I32 span=0;span<spanCount;span++)
	{
		if(interrupted())
		{
			break;
		}

		const I32 startIndex=span? endIndexOfSpan[span-1]+1: 0;
		const I32 endIndex=endIndexOfSpan[span];
		const I32 cv2=cv2OfSpan[span];

//		feLog("span %d/%d from %d to %d cv2 %d\n",
//				span,spanCount,startIndex,endIndex,cv2);

		Real minDistIn= -1.0;
		Real maxDistIn= -1.0;
		if(spInputMinDistIn.isValid())
		{
			minDistIn=minDistInArray[cv2];
		}
		if(spInputMaxDistIn.isValid())
		{
			maxDistIn=maxDistInArray[cv2];
		}
		if(minDistIn>maxDistIn && maxDistIn>0.0)
		{
			minDistIn=maxDistIn;
		}
		if(maxDistIn<minDistIn && minDistIn>0.0)
		{
			maxDistIn=1e6;
		}
		if(maxDistIn>0.0)
		{
			SpatialVector linearOrigin=resultArray[startIndex];
			SpatialVector linearDir=
					resultArray[endIndex]-linearOrigin;
			const Real linearLength=magnitude(linearDir);
			linearDir*=(linearLength>0.0)? 1.0/linearLength: 1.0;

			Real targetLength=0.0;

			const I32 passes=8;
			for(I32 pass=0;pass<passes;pass++)
			{
				Real curveLength=0.0;
				for(I32 index=startIndex;index<=endIndex;index++)
				{
					if(index>0)
					{
						curveLength+=magnitude(
								resultArray[index]-resultArray[index-1]);
					}
				}

//				feLog("pass %d/%d curve %.6G linear %.6G"
//						" min %.6G max %.6G\n",
//						pass,passes,curveLength,linearLength,
//						minDistIn,maxDistIn);

				if(!pass)
				{
					if(curveLength<maxDistIn && curveLength>=minDistIn)
					{
						break;
					}

					targetLength=
							(curveLength>maxDistIn)? maxDistIn: minDistIn;
				}

				Real ratio=(targetLength-linearLength)/
						(curveLength-linearLength);
				ratio=pow(fe::maximum(ratio,Real(0)),0.5);

//				feLog("-> target %.6G ratio %.6G\n",targetLength,ratio);

				for(I32 index=startIndex;index<=endIndex;index++)
				{
					const SpatialVector curvePoint=pointArray[index];
					const Real linearDot=
							dot(curvePoint-linearOrigin,linearDir);
					const SpatialVector linearPoint=
							linearOrigin+linearDir*linearDot;

					resultArray[index]=
							linearPoint+ratio*(curvePoint-linearPoint);
					FEASSERT(checkValid(resultArray[index]));
				}
				if(ratio<1e-3 || fabs(ratio-1.0)<1e-3)
				{
					break;
				}
			}
		}
	}

	delete[] linearityInArray;
	delete[] linearityOutArray;
	delete[] minDistInArray;
	delete[] maxDistInArray;

	delete[] waypointArray;
	delete[] normalArray;
	delete[] pointArray;

	delete[] linearArray;

	spDrawI->drawLines(resultArray,NULL,trueSegments+1,
			DrawI::e_strip,false,NULL);

	delete[] resultArray;

	if(spInputWaypoint.isValid())
	{
		sp<SurfaceAccessorI> spOutputWaypoint;
		accessOutput(spOutputWaypoint,a_rSignal,
				e_point,"waypoint",e_warning);

		sp<SurfaceAccessorI> spOutputWayPartial;
		accessOutput(spOutputWayPartial,a_rSignal,
				e_point,"waypartial",e_warning);

		for(I32 index=0;index<=trueSegments;index++)
		{
			if(interrupted())
			{
				break;
			}

			const String& rWaypoint=wayResultArray[index];
			if(!rWaypoint.empty())
			{
				const Real partial=wayPartialArray[index];

//				feLog("%d/%d waypoint \"%s\" partial %.6G\n",
//						index,trueSegments+1,
//						rWaypoint.c_str(),partial);

				spOutputWaypoint->set(index,rWaypoint);
				spOutputWayPartial->set(index,partial);
			}
		}
	}

	delete[] wayPartialArray;
	delete[] wayResultArray;

	rSummary.sPrintf("%d segment",trueSegments);
	if(doConnectivity)
	{
		rSummary+=String(rSummary.empty()? "": " ")+"trace";
	}
	if(bisect)
	{
		rSummary+=String(rSummary.empty()? "": " ")+"bisect";
	}
	if(blend)
	{
		rSummary+=String(rSummary.empty()? "": " ")+"blend";
	}
	rSummary+=String(rSummary.empty()? "": " ")+interpolation;
}

//* static
void CurveCreateOp::halfTransform(SpatialTransform& a_result,
	const SpatialTransform& a_matrix1,const SpatialTransform& a_matrix2)
{
	MatrixSqrt<SpatialTransform> matrixSqrt;

	SpatialTransform inv1;
	invert(inv1,a_matrix1);

	const SpatialTransform delta12=inv1*a_matrix2;

	SpatialTransform partial12;
	matrixSqrt.solve(partial12,delta12);

	a_result=a_matrix1*partial12;
}

//* static
void CurveCreateOp::interpolateTransform(SpatialTransform& a_result,
	const SpatialTransform& a_matrix1,const SpatialTransform& a_matrix2,
	Real a_fraction,BWORD a_bezier)
{
//	feLog("CurveCreateOp::interpolateTransform fraction %.6G\n"
//			"from\n%s\nto\n%s\n",
//			a_fraction,c_print(a_matrix1),c_print(a_matrix2));

	SpatialTransform inv1;
	invert(inv1,a_matrix1);

	const SpatialTransform delta12=a_matrix2*inv1;

	SpatialTransform partial12;

	if(a_bezier)
	{
		MatrixBezier<SpatialTransform> matrixBezier;
		matrixBezier.solve(partial12,delta12,a_fraction);
	}
	else
	{
		MatrixPower<SpatialTransform> matrixPower;
		matrixPower.solve(partial12,delta12,a_fraction);
	}

	a_result=partial12*a_matrix1;
}

} /* namespace ext */
} /* namespace fe */
