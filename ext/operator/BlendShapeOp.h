/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __operator_BlendShapeOp_h__
#define __operator_BlendShapeOp_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
    @brief Weighted Average of Multiple Surfaces

	@ingroup operator
*//***************************************************************************/
class FE_DL_EXPORT BlendShapeOp:
	public OperatorSurfaceCommon,
	public Initialize<BlendShapeOp>
{
	public:
				BlendShapeOp(void)											{}
virtual			~BlendShapeOp(void)											{}

		void	initialize(void);

				//* As HandlerI
virtual void	handle(Record& a_rSignal);
};

} /* namespace ext */
} /* namespace fe */

#endif /* __operator_BlendShapeOp_h__ */
