/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __operator_NullOp_h__
#define __operator_NullOp_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief pass input to output

	@ingroup operator
*//***************************************************************************/
class FE_DL_EXPORT NullOp:
	public OperatorSurfaceCommon,
	public Initialize<NullOp>
{
	public:

					NullOp(void)											{}
virtual				~NullOp(void)											{}

		void		initialize(void);

					//* As HandlerI
virtual	void		handle(Record& a_rSignal);
};

} /* namespace ext */
} /* namespace fe */

#endif /* __operator_NullOp_h__ */
