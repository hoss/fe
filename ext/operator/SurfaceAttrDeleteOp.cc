/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <operator/operator.pmh>

#define FE_SAD_DEBUG	FALSE

namespace fe
{
namespace ext
{

void SurfaceAttrDeleteOp::initialize(void)
{
	catalog<String>("icon")="FE_alpha";

	catalog<String>("pointAttributes")="";
	catalog<String>("pointAttributes","label")="Point Attributes";
	catalog<bool>("pointAttributes","joined")=true;
	catalog<String>("pointAttributes","hint")=
		"Space delimited list of point attributes to remove.";

	catalog<bool>("pointRegex")=false;
	catalog<String>("pointRegex","label")="Regex";
	catalog<String>("pointRegex","hint")=
			"Pass the pattern strings directly as true regular expressions."
			"  If this option is off,"
			" presumed glob patterns are converted to regex."
			"  Every '.' is first escaped as '\\.'."
			"  Then, every '?', indicating 'any character',"
			" is converted to '.'."
			"  Every '*', indicating 'any number of any characters',"
			" is converted to '.*'."
			"  Every '[!', indicating 'not' inside of square brackets,"
			" is converted to '[^'.";

	catalog<String>("vertexAttributes")="";
	catalog<String>("vertexAttributes","label")="Vertex Attributes";
	catalog<bool>("vertexAttributes","joined")=true;
	catalog<String>("vertexAttributes","hint")=
		"Space delimited list of vertex attributes to remove.";

	catalog<bool>("vertexRegex")=false;
	catalog<String>("vertexRegex","label")="Regex";
	catalog<String>("vertexRegex","hint")=
			catalog<String>("pointRegex","hint");

	catalog<String>("primitiveAttributes")="";
	catalog<String>("primitiveAttributes","label")="Primitive Attributes";
	catalog<bool>("primitiveAttributes","joined")=true;
	catalog<String>("primitiveAttributes","hint")=
		"Space delimited list of primitive attributes to remove.";

	catalog<bool>("primitiveRegex")=false;
	catalog<String>("primitiveRegex","label")="Regex";
	catalog<String>("primitiveRegex","hint")=
			catalog<String>("pointRegex","hint");

	catalog<String>("detailAttributes")="";
	catalog<String>("detailAttributes","label")="Detail Attributes";
	catalog<bool>("detailAttributes","joined")=true;
	catalog<String>("detailAttributes","hint")=
		"Space delimited list of detail attributes to remove.";

	catalog<bool>("detailRegex")=false;
	catalog<String>("detailRegex","label")="Regex";
	catalog<String>("detailRegex","hint")=
			catalog<String>("pointRegex","hint");

	catalog< sp<Component> >("Input Surface");

	catalog< sp<Component> >("Output Surface");
	catalog<String>("Output Surface","IO")="output";
	catalog<String>("Output Surface","copy")="Input Surface";
	catalog<bool>("Output Surface","clobber")=true;
}

void SurfaceAttrDeleteOp::handle(Record& a_rSignal)
{
#if FE_SAD_DEBUG
	feLog("SurfaceAttrDeleteOp::handle\n");
#endif

	String pointAttributes=catalog<String>("pointAttributes");
	String vertexAttributes=catalog<String>("vertexAttributes");
	String primitiveAttributes=catalog<String>("primitiveAttributes");
	String detailAttributes=catalog<String>("detailAttributes");

	if(!pointAttributes.empty() && !catalog<bool>("pointRegex"))
	{
		pointAttributes=pointAttributes.convertGlobToRegex();
	}

	if(!vertexAttributes.empty() && !catalog<bool>("vertexRegex"))
	{
		vertexAttributes=vertexAttributes.convertGlobToRegex();
	}

	if(!primitiveAttributes.empty() && !catalog<bool>("primitiveRegex"))
	{
		primitiveAttributes=primitiveAttributes.convertGlobToRegex();
	}

	if(!detailAttributes.empty() && !catalog<bool>("detailRegex"))
	{
		detailAttributes=detailAttributes.convertGlobToRegex();
	}

	sp<SurfaceAccessibleI> spOutputAccessible;
	if(!accessOutput(spOutputAccessible,a_rSignal)) return;

	I32 discardCount(0);

	const I32 classCount=4;
	for(I32 classIndex=0;classIndex<classCount;classIndex++)
	{
		Element element(e_point);
		String attributes=pointAttributes;

		switch(classIndex)
		{
			case 1:
				element=e_vertex;
				attributes=vertexAttributes;
				break;
			case 2:
				element=e_primitive;
				attributes=primitiveAttributes;
				break;
			case 3:
				element=e_detail;
				attributes=detailAttributes;
				break;
		}

		discardCount+=discardPattern(spOutputAccessible,element,attributes);
	}

	String summary;
	summary.sPrintf("%d attribute%s",discardCount,discardCount==1? "": "s");
	catalog<String>("summary")=summary;

#if FE_SAD_DEBUG
	feLog("SurfaceAttrDeleteOp::handle done\n");
#endif
}

} /* namespace ext */
} /* namespace fe */
