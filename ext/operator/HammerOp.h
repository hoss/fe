/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __operator_HammerOp_h__
#define __operator_HammerOp_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Operator to alter regions of a surface

	@ingroup operator
*//***************************************************************************/
class FE_DL_EXPORT HammerOp:
	public OperatorSurfaceCommon,
	public Initialize<HammerOp>
{
	public:

							HammerOp(void);
virtual						~HammerOp(void);

		void				initialize(void);

							//* As HandlerI
virtual	void				handle(Record& a_rSignal);

virtual	BWORD				undo(String a_change,sp<Counted> a_spCounted);
virtual	BWORD				redo(String a_change,sp<Counted> a_spCounted);

	protected:

		I32					getTriIndex(void);
		SpatialBary			getBarycenter(void);
		SpatialTransform	getParamPivot(void);
		void				setParamPivot(SpatialTransform a_transform,
								BWORD a_anticipate=FALSE);
		SpatialTransform	getParamDeform(void);
		void				setParamDeform(SpatialTransform a_transform,
								BWORD a_anticipate=FALSE);
		void				applyThreshold(SpatialVector& a_rVector,
								Real a_default);

		BWORD				located(void);
		BWORD				anchorPicked(void);
		String				anchorLabel(void);
		String&				anchorParent(void);
		String				anchorWeightAttr(void);
		Real				anchorRadius(void);
		Real				anchorPower(void);
		SpatialTransform	evaluateLocator(void);
		SpatialTransform	evaluateConcatenation(void);

		void				resetManipulator(void);
		void				pushToManipulator(void);
		void				pullFromManipulator(BWORD a_anticipate=FALSE);

		BWORD				m_anchorless;

	private:

	class State:
		public Counted,
		CastableAs<State>
	{
		public:
			enum		Space
						{
							e_pivot,
							e_deform
						};

			Space				m_space;
			SpatialTransform	m_transform;
	};

		enum		EditMode
					{
						e_pickAnchor,
						e_moveAnchor,
						e_manipulator
					};

		BWORD				restore(sp<State> a_spState);

		String				scanPeers(sp<DrawI>& a_rspDrawOverlay,
									const SpatialVector& a_cameraPos,
									const SpatialVector& a_cameraDir,
									BWORD a_draw,BWORD a_labels,
									I32 a_mouseX,I32 a_mouseY);

		void				changeMode(EditMode a_editMode);

		EditMode						m_editMode;

		sp<ManipulatorI>				m_spManipulator;
		sp<SurfaceAccessibleI>			m_spOutputAccessible;
		sp<SurfaceI>					m_spInput;
		sp<SurfaceI>					m_spDriver;
		sp<SurfaceI>					m_spParent;
		WindowEvent						m_event;

		sp<DrawMode>					m_spWireframe;
		sp<DrawMode>					m_spWide;
		sp<DrawMode>					m_spSolid;

		SpatialTransform				m_locator;
		SpatialTransform				m_lastPivot;
		SpatialTransform				m_lastDeform;
		Real							m_lastFrame;
		BWORD							m_brushed;
		I32								m_pickTriIndex;
		SpatialBary						m_pickBarycenter;
		String							m_highlightNode;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __operator_HammerOp_h__ */
