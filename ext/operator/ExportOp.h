/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __operator_ExportOp_h__
#define __operator_ExportOp_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Save a surface in an arbitrary format

	@ingroup operator
*//***************************************************************************/
class FE_DL_EXPORT ExportOp:
	public OperatorSurfaceCommon,
	public Initialize<ExportOp>
{
	public:

					ExportOp(void)											{}
virtual				~ExportOp(void)											{}

		void		initialize(void);

					//* As HandlerI
virtual	void		handle(Record& a_rSignal);
};

} /* namespace ext */
} /* namespace fe */

#endif /* __operator_ExportOp_h__ */
