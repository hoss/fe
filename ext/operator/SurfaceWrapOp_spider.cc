/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <operator/operator.pmh>

#define FE_SWO_DEBUG	FALSE
#define FE_SWO_VERBOSE	FALSE

using namespace fe;
using namespace fe::ext;

void SurfaceWrapOp::initialize(void)
{
	//* TODO option to also rotate normals

	//* NOTE inherits attributes from SurfaceBindOp

	catalogRemove("LimitDistance");
	catalogRemove("MaxDistance");
	catalogRemove("PartitionDriver");
	catalogRemove("InputPartitionAttr");
	catalogRemove("DriverPartitionAttr");
	catalogRemove("PivotPoint");

	catalog<String>("BindPrefix","hint")=
			"Prefix of attribute names on input surface describing"
			" where each point on the input surface is attached to"
			" the associated driver.";

	catalog<Real>("Distance Bias")=1.0;
	catalog<Real>("Distance Bias","min")=0.0;
	catalog<Real>("Distance Bias","high")=2.0;
	catalog<Real>("Distance Bias","max")=10.0;
	catalog<String>("Distance Bias","hint")=
			"Dominance of closer faces towards weighting";

	catalog<bool>("checkDeformed")=false;
	catalog<String>("checkDeformed","label")="Check Deformed Driver";
	catalog<bool>("checkDeformed","joined")=true;
	catalog<String>("checkDeformed","hint")=
			"Verify that the deformed surface is like the driver surface."
			"  These checks take time, so this option should probably"
			" only be used temporarily.";

	catalog<I32>("checkLimit")=1;
	catalog<I32>("checkLimit","min")=0;
	catalog<I32>("checkLimit","high")=10;
	catalog<I32>("checkLimit","max")=100;
	catalog<String>("checkLimit","enabler")="checkDeformed";
	catalog<String>("checkLimit","label")="Message Limit";
	catalog<String>("checkLimit","hint")=
			"Stop reporting mismatches after the given number of errors.";

	catalog<bool>("Debug Bind")=false;
	catalog<bool>("Debug Bind","joined")=true;
	catalog<String>("Debug Bind","hint")=
			"Draws lines over the output showing the attachments."
			"  This feedback may only be visible is this node is"
			" \"selected\" and/or \"displayed\".";

	catalog<I32>("Debug Index")=-1;
	catalog<I32>("Debug Index","min")=-1;
	catalog<I32>("Debug Index","high")=1000;
	catalog<I32>("Debug Index","max")=1000000;
	catalog<String>("Debug Index","enabler")="Debug Bind";
	catalog<String>("Debug Index","hint")=
			"Index of element to examine (-1 to see them all).";

	catalog<String>("Input Surface","label")="Bound Input Surface";

	catalog< sp<Component> >("Deformed Surface");
	catalog<String>("Deformed Surface","label")="Deformed Driver Surface";

	catalog< sp<Component> >("Output Surface");
	catalog<String>("Output Surface","IO")="output";
	catalog<String>("Output Surface","copy")="Input Surface";
	catalog<bool>("Output Surface","recycle")=true;

	m_spDebugGroup=new DrawMode();
	m_spDebugGroup->setGroup("debug");
}

void SurfaceWrapOp::handle(Record& a_rSignal)
{
#if FE_SWO_DEBUG
	feLog("SurfaceWrapOp::handle\n");
#endif

	//* output curves is copy of input surface
	if(!accessOutput(m_spOutputAccessible,a_rSignal)) return;

	if(catalog<BWORD>("Debug Bind"))
	{
		if(!accessGuide(m_spDrawDebug,a_rSignal)) return;
	}
	else
	{
		m_spDrawDebug=NULL;
	}

	if(!access(m_spInputAccessible,"Input Surface")) return;

	sp<SurfaceAccessorI> spDriverVertices;
	if(!access(spDriverVertices,"Driver Surface",
			e_primitive,e_vertices)) return;

	sp<SurfaceAccessorI> spDeformedVertices;
	if(!access(spDeformedVertices,"Deformed Surface",
			e_primitive,e_vertices)) return;

	const BWORD checkDeformed=catalog<bool>("checkDeformed");
	const U32 driverPrimitiveCount=spDriverVertices->count();
	const U32 deformedPrimitiveCount=spDeformedVertices->count();
	if(deformedPrimitiveCount != driverPrimitiveCount)
	{
		String message;
		message.sPrintf("Deformed driver surface has a different number"
				" of primitives (%d vs %d);",
				deformedPrimitiveCount,driverPrimitiveCount);
		catalog<String>(checkDeformed? "error": "warning")+=message;
		if(checkDeformed)
		{
			return;
		}
	}
	else if(checkDeformed)
	{
		const I32 checkLimit=catalog<I32>("checkLimit");
		U32 mismatches=0;
		for(U32 primitiveIndex=0;
				primitiveIndex<driverPrimitiveCount && mismatches<checkLimit;
				primitiveIndex++)
		{
			const U32 driverSubCount=
					spDriverVertices->subCount(primitiveIndex);
			const U32 deformedSubCount=
					spDeformedVertices->subCount(primitiveIndex);
			if(driverSubCount!=deformedSubCount)
			{
				String message;
				message.sPrintf("  primitive %d has different"
						" number of vertices (%d vs %d);\n",primitiveIndex,
						deformedPrimitiveCount,driverPrimitiveCount);
				catalog<String>("error")+=message;
				mismatches++;
			}
			else
			{
				for(U32 subIndex=0;
						subIndex<driverSubCount && mismatches<checkLimit;
						subIndex++)
				{
					const U32 driverPointIndex=spDriverVertices->integer(
							primitiveIndex,subIndex);
					const U32 deformedPointIndex=spDeformedVertices->integer(
							primitiveIndex,subIndex);
					if(driverPointIndex!=deformedPointIndex)
					{
						String message;
						message.sPrintf("  primitive %d has mismatched"
								" vertex %d (%d vs %d);\n",
								primitiveIndex,subIndex,
								deformedPointIndex,driverPointIndex);
						catalog<String>("error")+=message;
						mismatches++;
					}
				}
			}
		}
		if(mismatches)
		{
			String message;
			message.sPrintf("Deformed driver has %d%s mismatches:\n",
					mismatches,mismatches>=checkLimit? "+": "");
			catalog<String>("error")=message+catalog<String>("error");
			return;
		}
	}

	const String driverGroup=catalog<String>("DriverGroup");

	//* original driver
	if(!access(m_spDriver,"Driver Surface",driverGroup)) return;

	m_spDriver->setRefinement(0);
	m_spDriver->prepareForSample();
	if(m_spDriver->radius()<1e-6)
	{
		catalog<String>("error")="Reference Driver Surface appears empty"
				" (zero radius bounding sphere);";
		return;
	}

	//* deformed driver
	if(!access(m_spDeformed,"Deformed Surface",driverGroup)) return;

	m_spDeformed->setRefinement(0);
	m_spDeformed->prepareForSample();
	if(m_spDeformed->radius()<1e-6)
	{
		catalog<String>("error")="Deformed Driver Surface appears empty"
				" (zero radius bounding sphere);";
		return;
	}

	const bool bindPrimitives=catalog<bool>("Bind Primitives");

	m_element=bindPrimitives? e_primitive: e_point;
	m_attribute=bindPrimitives? e_vertices: e_position;

	sp<SurfaceAccessorI> spOutputElements;
	if(!access(spOutputElements,m_spOutputAccessible,
			m_element,m_attribute)) return;

	const U32 elementCount=spOutputElements->count();

	if(m_spDrawDebug.isValid())
	{
		m_spDrawDebug->pushDrawMode(m_spDebugGroup);
	}

	U32 runCount=elementCount;
	m_fragmented=catalog<bool>("Bind Fragments");
	m_spFragmentAccessor=NULL;
	if(m_fragmented)
	{
		if(access(m_spFragmentAccessor,m_spInputAccessible,
				m_element,m_attribute))
		{
			const String fragmentAttribute=
					catalog<String>("FragmentAttr");
			m_spFragmentAccessor->fragmentWith(
					SurfaceAccessibleI::Element(m_element),fragmentAttribute);
			runCount=m_spFragmentAccessor->fragmentCount();
			if(runCount<1)	//* was (runCount<2), but there can be one fragment
			{
				catalog<String>("warning")+=
						"fragmentation failed to divide surface -> reverting";
				m_fragmented=FALSE;
				runCount=elementCount;
			}
		}
		else
		{
			catalog<String>("warning")+=
					"fragmentation access failed -> ignoring";
			m_fragmented=FALSE;
			runCount=elementCount;
		}
	}

	//* TODO fragment-aware ranges
	if(m_fragmented)
	{
		//* serial
		sp<SurfaceRange> spFullRange=sp<SurfaceRange>(new SurfaceRange());
		spFullRange->nonAtomic().add(0,runCount-1);
		run(-1,spFullRange);
	}
	else
	{
		setAtomicRange(m_spOutputAccessible,
				bindPrimitives? e_pointsOfPrimitives: e_pointsOnly);
		OperatorThreaded::handle(a_rSignal);
	}

	if(m_spDrawDebug.isValid())
	{
		m_spDrawDebug->popDrawMode();
	}

#if FE_SWO_DEBUG
	feLog("SurfaceWrapOp::handle done\n");
#endif
}

void SurfaceWrapOp::run(I32 a_id,sp<SurfaceRange> a_spRange)
{
	const Real distanceBias=catalog<Real>("Distance Bias");
	const I32 debugIndex=catalog<I32>("Debug Index");

#if FE_SWO_DEBUG
	feLog("SurfaceWrapOp::run running thread %d %s\n",
			a_id,a_spRange->brief().c_str());
#endif

	sp<SurfaceAccessorI> spInputElements;
	if(!access(spInputElements,m_spInputAccessible,
			m_element,m_attribute)) return;

	sp<SurfaceAccessorI> spOutputElements;
	if(!access(spOutputElements,m_spOutputAccessible,
			m_element,m_attribute)) return;

	std::vector< sp<SurfaceAccessorI> > inputFaces;
	std::vector< sp<SurfaceAccessorI> > inputBarys;
	const I32 bindings=accessBindings(m_spInputAccessible,
			inputFaces,inputBarys,FALSE);
	if(bindings<1)
	{
		return;
	}

	const bool spiderWrap=catalog<bool>("Spider");
	const BWORD singular=
			(m_fragmented && m_element==SurfaceAccessibleI::e_point);

	sp<SurfaceAccessorI::FilterI> spFilter;

	for(SurfaceRange::Iterator it=a_spRange->begin();!it.atEnd();it.step())
	{
		const U32 index=it.value();

		I32 filterCount=1;
		U32 pointCount=0;
		if(m_fragmented)
		{
			const String fragment=m_spFragmentAccessor->fragment(index);
			if(!m_spFragmentAccessor->filterWith(fragment,spFilter) ||
					spFilter.isNull())
			{
				continue;
			}
			filterCount=spFilter->filterCount();
			if(filterCount<1)
			{
				continue;
			}
			if(singular)
			{
				pointCount+=filterCount;
			}
			else
			{
				for(I32 filterIndex=0;filterIndex<filterCount;filterIndex++)
				{
					const I32 filtered=spFilter->filter(filterIndex);
					FEASSERT(filtered>=0);
					pointCount+=spInputElements->subCount(filtered);
				}
			}
		}
		else
		{
			pointCount=spInputElements->subCount(index);
		}

		if(pointCount<1)
		{
			continue;
		}

		SpatialVector point[pointCount];
		SpatialVector result[pointCount];
		Real totalWeight[pointCount];

		U32 pointIndex=0;
		for(U32 filterIndex=0;filterIndex<filterCount;filterIndex++)
		{
			const I32 filtered=
					m_fragmented? spFilter->filter(filterIndex): index;
			FEASSERT(filtered>=0);
			const U32 subCount=singular? 1: spInputElements->subCount(filtered);
			for(U32 subIndex=0;subIndex<subCount;subIndex++)
			{
				point[pointIndex]=
						spInputElements->spatialVector(filtered,subIndex);
				set(result[pointIndex]);
				totalWeight[pointIndex]=0.0;
				pointIndex++;
			}
		}

		const I32 keyIndex=m_fragmented? spFilter->filter(0): index;
		FEASSERT(keyIndex>=0);

		for(U32 binding=0;binding<bindings;binding++)
		{
			const I32 face=inputFaces[binding]->integer(keyIndex);
			if(face<0)
			{
				continue;
			}

			SpatialBary barycenter(0.33,0.33);
			if(!binding && !spiderWrap)
			{
				barycenter=inputBarys[binding]->spatialVector(keyIndex);
			}

			const SpatialTransform xformDriver=
					m_spDriver->sample(face,barycenter);
			const SpatialTransform xformDeformed=
					m_spDeformed->sample(face,barycenter);

			SpatialTransform invDriver;
			invert(invDriver,xformDriver);
			const SpatialTransform conversion=invDriver*xformDeformed;

			const Real distance=
					magnitude(xformDriver.translation()-point[0]);
			const Real weight=1.0/(pow(distance,distanceBias)+1e-9);

			for(pointIndex=0;pointIndex<pointCount;pointIndex++)
			{
				SpatialVector transformed;
				transformVector(conversion,point[pointIndex],transformed);
				result[pointIndex]+=weight*transformed;
				totalWeight[pointIndex]+=weight;

#if FE_SWO_VERBOSE
				feLog("SurfaceWrapOp::handle element %d/(%d,%d) bind %d/%d"
						" point %d/%d face %d bary %s from %s to %s\n",
						index,start,end,binding,bindings,
						pointIndex,pointCount,
						face,c_print(barycenter),
						c_print(point[pointIndex]),c_print(transformed));
				feLog("conversion\n%s\n",c_print(conversion));
				feLog("xformDriver\n%s\n",c_print(xformDriver));
				feLog("xformDeformed\n%s\n",c_print(xformDeformed));
#endif
			}

			if(m_spDrawDebug.isValid() && (debugIndex<0 || debugIndex==index))
			{
				SpatialVector transformed;
				transformVector(conversion,point[0],transformed);

				const Color yellow(1.0,1.0,0.0,1.0);
				SpatialVector line[2];

				line[0]=transformed;
				line[1]=xformDeformed.translation();

				SAFEGUARD;
				m_spDrawDebug->drawLines(line,NULL,2,DrawI::e_strip,
						false,&yellow);
			}
		}

		pointIndex=0;
		for(U32 filterIndex=0;filterIndex<filterCount;filterIndex++)
		{
			const I32 filtered=
					m_fragmented? spFilter->filter(filterIndex): index;
			FEASSERT(filtered>=0);
			const U32 subCount=singular? 1: spInputElements->subCount(filtered);
			for(U32 subIndex=0;subIndex<subCount;subIndex++)
			{
				if(totalWeight[pointIndex]>0.0)
				{
					spOutputElements->set(filtered,subIndex,
							result[pointIndex]/totalWeight[pointIndex]);
				}

				pointIndex++;
			}
		}
	}

#if FE_SWO_DEBUG
	feLog("SurfaceWrapOp::run done running thread %d\n",a_id);
#endif
}
