/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __operator_GroupOp_h__
#define __operator_GroupOp_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
    @brief Manipulate point and primitive groups

	@ingroup operator
*//***************************************************************************/
class FE_DL_EXPORT GroupOp:
	public OperatorThreaded,
	public Initialize<GroupOp>
{
	public:
				GroupOp(void)												{}
virtual			~GroupOp(void)												{}

		void	initialize(void);

				//* As HandlerI
virtual void	handle(Record& a_rSignal);

				using OperatorThreaded::run;

virtual	void	run(I32 a_id,sp<SpannedRange> a_spRange);

	private:
		void	groupElement(I32 a_index);
		void	groupElement(sp<SurfaceAccessorI>& a_rspElementPosition,
						sp<SurfaceAccessorI>& a_rspElementAttribute,
						I32 a_index);

		sp<SurfaceAccessibleI>	m_spOutputAccessible;
		sp<SurfaceI>			m_spBounds;
		String					m_shape;
		String					m_combine;
		String					m_defaultGroup;
		BWORD					m_doPrimitive;
		BWORD					m_combined;

		std::vector<String>		m_groupAssignment;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __operator_GroupOp_h__ */

