/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __operator_PoisonOp_h__
#define __operator_PoisonOp_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
    @brief Mark undesirable points

	@ingroup operator
*//***************************************************************************/
class FE_DL_EXPORT PoisonOp:
	public OperatorSurfaceCommon,
	public Initialize<PoisonOp>
{
	public:
				PoisonOp(void)											{}
virtual			~PoisonOp(void)											{}

		void	initialize(void);

				//* As HandlerI
virtual void	handle(Record& a_rSignal);

	private:

	class Edge
	{
		public:
								Edge(void):
									m_faceCount(0)						{}

			I32					m_faceCount;
	};

		//* face count per edge
		std::map<U64,Edge>		m_edgeMap;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __operator_PoisonOp_h__ */
