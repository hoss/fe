/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __operator_OffsetOp_h__
#define __operator_OffsetOp_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Operator to transfer displacements on one surface to another

	@ingroup operator
*//***************************************************************************/
class FE_DL_EXPORT OffsetOp:
	public OperatorSurfaceCommon,
	public Initialize<OffsetOp>
{
	public:

					OffsetOp(void)											{}
virtual				~OffsetOp(void)											{}

		void		initialize(void);

					//* As HandlerI
virtual	void		handle(Record& a_rSignal);

	private:
};

} /* namespace ext */
} /* namespace fe */

#endif /* __operator_OffsetOp_h__ */
