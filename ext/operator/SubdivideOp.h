/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __operator_SubdivideOp_h__
#define __operator_SubdivideOp_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
    @brief Increase resolution of a mesh

	@ingroup operator
*//***************************************************************************/
class FE_DL_EXPORT SubdivideOp:
	public OperatorThreaded,
	public Initialize<SubdivideOp>
{
	public:
				SubdivideOp(void)											{}
virtual			~SubdivideOp(void)											{}

		void	initialize(void);

				//* As HandlerI
virtual	void	handleBind(sp<SignalerI> a_spSignalerI,sp<Layout> a_spLayout);
virtual void	handle(Record& a_rSignal);

				using OperatorThreaded::run;

virtual	void	run(I32 a_id,sp<SpannedRange> a_spRange,String a_stage);

	protected:

		void	clearData(void);

		void	run_edge(I32 a_id,sp<SpannedRange> a_spRange);
		void	run_point(I32 a_id,sp<SpannedRange> a_spRange);

		void	expandIncremental(void);
		void	expandPost(void);

	private:

	class Edge
	{
		public:
								Edge(void):
									m_pointIndex(-1)						{}

			std::set<I32>		m_faceSet;
			I32					m_pointIndex;
	};

		sp<SurfaceAccessibleI>				m_spInputAccessible;
		sp<SurfaceAccessibleI>				m_spOutputAccessible;

		sp<OperatorSurfaceI>				m_spSurfaceNormalOp;

		BWORD								m_flat;
		OperatorSurfaceCommon::Element		m_uvRate;

		SpatialVector*						m_pointsIn;
		SpatialVector*						m_normalsIn;

		Array<I32>							m_facePointIndex;

		Array<SpatialVector>				m_facePointSum;
		Array<I32>							m_faceCount;

		Array<SpatialVector>				m_midPointSum;
		Array<I32>							m_midpointCount;

		Array<SpatialVector>				m_boundaryPointSum;
		Array<I32>							m_boundaryCount;

		Array<Real>							m_expand;

		//* face list per edge
		std::map<U64,Edge>					m_edgeMap;

		sp<Profiler>						m_spProfiler;
		sp<Profiler::Profile>				m_spProfileAccess;
		sp<Profiler::Profile>				m_spProfileResize;
		sp<Profiler::Profile>				m_spProfileSlot;
		sp<Profiler::Profile>				m_spProfileFace;
		sp<Profiler::Profile>				m_spProfileFacePoint;
		sp<Profiler::Profile>				m_spProfileVertSize;
		sp<Profiler::Profile>				m_spProfileEdgeMap;
		sp<Profiler::Profile>				m_spProfileEdgePoint;
		sp<Profiler::Profile>				m_spProfileVertex;
		sp<Profiler::Profile>				m_spProfileAppend;
		sp<Profiler::Profile>				m_spProfileEdge;
		sp<Profiler::Profile>				m_spProfilePoint;
		sp<Profiler::Profile>				m_spProfileExpand;
		sp<Profiler::Profile>				m_spProfileNormal;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __operator_SubdivideOp_h__ */
