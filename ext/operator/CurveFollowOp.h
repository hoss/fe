/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __operator_CurveFollowOp_h__
#define __operator_CurveFollowOp_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Operator to provide target along curve

	@ingroup operator
*//***************************************************************************/
class FE_DL_EXPORT CurveFollowOp:
	public OperatorSurfaceCommon,
	public Initialize<CurveFollowOp>
{
	public:

	class Feedback
	{
		public:
			SpatialVector	m_location;
			String			m_text;
	};

					CurveFollowOp(void)										{}
virtual				~CurveFollowOp(void)									{}

		void		initialize(void);

					//* As HandlerI
virtual	void		handle(Record& a_rSignal);

	private:

		sp<SurfaceAccessibleI>	m_spInputAccessible;
		sp<SurfaceI>			m_spCurve;

		sp<DrawMode>			m_spSolid;
		sp<DrawMode>			m_spOverlay;

		Array<Feedback>			m_feedbackArray;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __operator_CurveFollowOp_h__ */

