/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __operator_TransformOp_h__
#define __operator_TransformOp_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Apply a transformation to an entire surface

	@ingroup operator
*//***************************************************************************/
class FE_DL_EXPORT TransformOp:
	public HammerOp,
	public Initialize<TransformOp>
{
	public:

					TransformOp(void)
					{	m_anchorless=TRUE; }

virtual				~TransformOp(void)										{}

		void		initialize(void);

					//* As HandlerI
virtual	void		handle(Record& a_rSignal);
};

} /* namespace ext */
} /* namespace fe */

#endif /* __operator_TransformOp_h__ */
