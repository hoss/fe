/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __operator_HingeOp_h__
#define __operator_HingeOp_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Operator to rotate a surface out of another

	@ingroup operator
*//***************************************************************************/
class FE_DL_EXPORT HingeOp:
	public OperatorThreaded,
	public Initialize<HingeOp>
{
	public:

					HingeOp(void):
						m_frame(-1),
						m_lastPointCount(-1),
						m_brushed(FALSE),
						m_selectIndex(-1)									{}
virtual				~HingeOp(void)											{}

		void		initialize(void);

					//* As HandlerI
virtual	void		handle(Record& a_rSignal);

					using OperatorThreaded::run;

virtual	void		run(I32 a_id,sp<SpannedRange> a_spRange);

	private:
static	void		calcConversion(SpatialTransform& a_rTransform,
							const Real a_angle,const SpatialVector& a_rAxis,
							const SpatialVector& a_rPivot);

	class Hit
	{
		public:
			I32				m_partitionIndex;
			I32				m_pointIndex0;
			I32				m_pointIndex1;
			I32				m_pointIndex2;
			SpatialVector	m_barycenter;
			SpatialVector	m_intersection;
	};

		I32							m_frame;
		I32							m_lastPointCount;
		BWORD						m_fragmented;
		BWORD						m_selfCollide;
		BWORD						m_replaceHits;
		Real						m_passage;

		sp<SurfaceAccessibleI>		m_spOutputAccessible;
		sp<SurfaceI>				m_spOutput;
		sp<SurfaceI>				m_spObstacle;
		sp<SurfaceI>				m_spCollider;
		sp<SurfaceI>				m_spDriver;
		sp<SurfaceI>				m_spInputRef;
		sp<SurfaceI>				m_spDriverRef;

		sp<DrawMode>				m_spBrush;
		sp<DrawMode>				m_spOverlay;

		BWORD						m_brushed;
		WindowEvent					m_event;
		I32							m_selectIndex;

		Array<SpatialVector>		m_inputCache;
		Array<SpatialVector>		m_outputCache;
		Array<Real>					m_totalAngle;
		Array<Real>					m_pointAngle;
		Array<Real>					m_lastAngle;

		Array<SpatialVector>		m_fragPivotRefA;
		Array<SpatialVector>		m_fragPivotRefB;
		Array<SpatialVector>		m_fragPivotRefC;
		Array<SpatialVector>		m_fragPivotA;
		Array<SpatialVector>		m_fragPivotB;
		Array<SpatialVector>		m_fragPivotC;
		Array<Real>					m_fragLength;
		Array<I32>					m_fragFarIndex;
		Array<I32>					m_fragmentOfPoint;
		Array<I32>					m_fragmentOfPartition;
		Array<String>				m_nameOfFragment;
		Array<String>				m_driverOfFragment;

		std::map<String, sp<PartitionI> >	m_partitionMap;
		std::map<String, sp<PartitionI> >	m_colliderPartitionMap;

		Array< Array<Hit> >			m_colliderHitTable;
		Array< Array<Hit> >			m_outputHitTable;

		sp<DrawI>					m_spDrawGuide;

		sp<Profiler>				m_spProfiler;
		sp<Profiler::Profile>		m_spProfileAccess;
		sp<Profiler::Profile>		m_spProfileInit;
		sp<Profiler::Profile>		m_spProfileClear;
		sp<Profiler::Profile>		m_spProfileCopy;
		sp<Profiler::Profile>		m_spProfileFragment;
		sp<Profiler::Profile>		m_spProfilePartition;
		sp<Profiler::Profile>		m_spProfilePivots;
		sp<Profiler::Profile>		m_spProfileLoop;
		sp<Profiler::Profile>		m_spProfileRun;
		sp<Profiler::Profile>		m_spProfilePush;
		sp<Profiler::Profile>		m_spProfilePull;
		sp<Profiler::Profile>		m_spProfileRotate;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __operator_HingeOp_h__ */

