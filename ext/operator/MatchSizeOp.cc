/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <operator/operator.pmh>

using namespace fe;
using namespace fe::ext;

void MatchSizeOp::initialize(void)
{
	catalog<String>("icon")="FE_alpha";

	catalog<bool>("doTranslate")=true;
	catalog<String>("doTranslate","label")="Translate";
	catalog<bool>("doTranslate","joined")=true;
	catalog<String>("doTranslate","hint")=
			"Move the input surface.";

	catalog<bool>("translateX")=true;
	catalog<String>("translateX","label")="X";
	catalog<String>("translateX","enabler")="doTranslate";
	catalog<bool>("translateX","joined")=true;
	catalog<String>("translateX","hint")=
			"Move along the X axis.";

	catalog<bool>("translateY")=true;
	catalog<String>("translateY","label")="Y";
	catalog<String>("translateY","enabler")="doTranslate";
	catalog<bool>("translateY","joined")=true;
	catalog<String>("translateY","hint")=
			"Move along the Y axis.";

	catalog<bool>("translateZ")=true;
	catalog<String>("translateZ","label")="Z";
	catalog<String>("translateZ","enabler")="doTranslate";
	catalog<String>("translateZ","hint")=
			"Move along the Z axis.";

	catalog<bool>("doScale")=true;
	catalog<String>("doScale","label")="Scale";
	catalog<bool>("doScale","joined")=true;
	catalog<String>("doScale","hint")=
			"Resize the input surface.";

	catalog<bool>("uniformScale")=false;
	catalog<String>("uniformScale","label")="Uniform";
	catalog<String>("uniformScale","enabler")="doScale";
	catalog<bool>("uniformScale","joined")=true;
	catalog<String>("uniformScale","hint")=
			"Resize all axes the same amount.";

	catalog<bool>("scaleX")=true;
	catalog<String>("scaleX","label")="X";
	catalog<String>("scaleX","enabler")="doScale && !uniformScale";
	catalog<bool>("scaleX","joined")=true;
	catalog<String>("scaleX","hint")=
			"Move along the X axis.";

	catalog<bool>("scaleY")=true;
	catalog<String>("scaleY","label")="Y";
	catalog<String>("scaleY","enabler")="doScale && !uniformScale";
	catalog<bool>("scaleY","joined")=true;
	catalog<String>("scaleY","hint")=
			"Move along the Y axis.";

	catalog<bool>("scaleZ")=true;
	catalog<String>("scaleZ","label")="Z";
	catalog<String>("scaleZ","enabler")="doScale && !uniformScale";
	catalog<String>("scaleZ","hint")=
			"Move along the Z axis.";

	catalog<String>("uniformRule")="minimum";
	catalog<String>("uniformRule","label")="Uniform Rule";
	catalog<String>("uniformRule","enabler")="uniformScale";
	catalog<String>("uniformRule","choice:0")="minimum";
	catalog<String>("uniformRule","label:0")="Minimum";
	catalog<String>("uniformRule","choice:1")="maximum";
	catalog<String>("uniformRule","label:1")="Maximum";
	catalog<String>("uniformRule","choice:2")="average";
	catalog<String>("uniformRule","label:2")="Average";

	catalog< sp<Component> >("Input Surface");

	catalog< sp<Component> >("Reference Surface");

	catalog< sp<Component> >("Output Surface");
	catalog<String>("Output Surface","IO")="output";
	catalog<String>("Output Surface","copy")="Input Surface";
}

void MatchSizeOp::handle(Record& a_rSignal)
{
	catalog<String>("summary")="OFF";

	const BWORD translateX=catalog<bool>("translateX");
	const BWORD translateY=catalog<bool>("translateY");
	const BWORD translateZ=catalog<bool>("translateZ");
	const BWORD doTranslate=(catalog<bool>("doTranslate") &&
			(translateX || translateY || translateZ));

	const BWORD uniformScale=catalog<bool>("uniformScale");
	const BWORD scaleX=catalog<bool>("scaleX");
	const BWORD scaleY=catalog<bool>("scaleY");
	const BWORD scaleZ=catalog<bool>("scaleZ");
	const BWORD doScale=(catalog<bool>("doScale") &&
			(uniformScale || scaleX || scaleY || scaleZ));

	const String uniformRule=catalog<String>("uniformRule");

	sp<SurfaceAccessibleI> spOutputAccessible;
	if(!accessOutput(spOutputAccessible,a_rSignal)) return;

	sp<SurfaceAccessibleI> spReferenceAccessible;
	if(!access(spReferenceAccessible,"Reference Surface")) return;

	sp<SurfaceAccessorI> spOutputPoints;
	if(!access(spOutputPoints,spOutputAccessible,
			e_point,e_position)) return;

	sp<SurfaceAccessorI> spReferencePoints;
	if(!access(spReferencePoints,spReferenceAccessible,
			e_point,e_position)) return;

	sp<DrawI> spDrawGuide;
	accessGuide(spDrawGuide,a_rSignal,e_warning);

	const I32 pointCount=spOutputPoints->count();
	if(pointCount<1)
	{
		catalog<String>("warning")+="No input points;";
		return;
	}

	const I32 pointCountRef=spReferencePoints->count();
	if(pointCountRef<1)
	{
		catalog<String>("warning")+="No reference points;";
		return;
	}

	if(!doTranslate && !doScale)
	{
		catalog<String>("warning")+="No translation or scaling selected;";
		return;
	}

	SpatialVector lowerIn=spOutputPoints->spatialVector(0);
	SpatialVector upperIn=lowerIn;
	for(I32 pointIndex=1;pointIndex<pointCount;pointIndex++)
	{
		const SpatialVector point=spOutputPoints->spatialVector(pointIndex);

		for(I32 m=0;m<3;m++)
		{
			if(lowerIn[m]>point[m])
			{
				lowerIn[m]=point[m];
			}
			if(upperIn[m]<point[m])
			{
				upperIn[m]=point[m];
			}
		}
	}

	SpatialVector lowerRef=spReferencePoints->spatialVector(0);
	SpatialVector upperRef=lowerRef;
	for(I32 pointIndex=1;pointIndex<pointCountRef;pointIndex++)
	{
		const SpatialVector point=spReferencePoints->spatialVector(pointIndex);

		for(I32 m=0;m<3;m++)
		{
			if(lowerRef[m]>point[m])
			{
				lowerRef[m]=point[m];
			}
			if(upperRef[m]<point[m])
			{
				upperRef[m]=point[m];
			}
		}
	}

	const SpatialVector sizeIn=upperIn-lowerIn;
	const SpatialVector sizeRef=upperRef-lowerRef;

	const SpatialVector centerIn=0.5*(upperIn+lowerIn);
	const SpatialVector centerRef=0.5*(upperRef+lowerRef);

	SpatialVector scaling(1,1,1);
	if(doScale)
	{
		const SpatialVector ratio=SpatialVector(
				sizeIn[0]>Real(0)? sizeRef[0]/sizeIn[0]: Real(1),
				sizeIn[1]>Real(0)? sizeRef[1]/sizeIn[1]: Real(1),
				sizeIn[2]>Real(0)? sizeRef[2]/sizeIn[2]: Real(1));

		if(uniformScale)
		{
			Real scaleFactor(1);

			if(uniformRule=="minimum")
			{
				BWORD factorSet(FALSE);

				if(sizeIn[0]>Real(0))
				{
					scaleFactor=ratio[0];
					factorSet=TRUE;
				}
				if(sizeIn[1]>Real(0) && (!factorSet || scaleFactor>ratio[1]))
				{
					scaleFactor=ratio[1];
					factorSet=TRUE;
				}
				if(sizeIn[2]>Real(0) && (!factorSet || scaleFactor>ratio[2]))
				{
					scaleFactor=ratio[2];
					factorSet=TRUE;
				}
			}
			else if(uniformRule=="maximum")
			{
				BWORD factorSet(FALSE);

				if(sizeIn[0]>Real(0))
				{
					scaleFactor=ratio[0];
					factorSet=TRUE;
				}
				if(sizeIn[1]>Real(0) && (!factorSet || scaleFactor<ratio[1]))
				{
					scaleFactor=ratio[1];
					factorSet=TRUE;
				}
				if(sizeIn[2]>Real(0) && (!factorSet || scaleFactor<ratio[2]))
				{
					scaleFactor=ratio[2];
					factorSet=TRUE;
				}
			}
			else if(uniformRule=="average")
			{
				Real sum(0);
				scaleFactor=Real(0);

				if(sizeIn[0]>Real(0))
				{
					scaleFactor+=ratio[0];
					sum+=Real(1);
				}
				if(sizeIn[1]>Real(0))
				{
					scaleFactor+=ratio[1];
					sum+=Real(1);
				}
				if(sizeIn[2]>Real(0))
				{
					scaleFactor+=ratio[2];
					sum+=Real(1);
				}

				if(sum>Real(0))
				{
					scaleFactor/=sum;
				}
			}

			scaling=SpatialVector(scaleFactor,scaleFactor,scaleFactor);
		}
		else
		{
			scaling=SpatialVector(
					scaleX? ratio[0]: Real(1),
					scaleY? ratio[1]: Real(1),
					scaleZ? ratio[2]: Real(1));
		}
	}

	const SpatialVector translateRef(
			doTranslate && translateX? centerRef[0]: centerIn[0],
			doTranslate && translateY? centerRef[1]: centerIn[1],
			doTranslate && translateZ? centerRef[2]: centerIn[2]);

	SpatialTransform xform;
	setIdentity(xform);
	translate(xform,translateRef);
	scale(xform,scaling);
	translate(xform,-centerIn);

	for(I32 pointIndex=0;pointIndex<pointCount;pointIndex++)
	{
		const SpatialVector point=spOutputPoints->spatialVector(pointIndex);

		const SpatialVector transformed=transformVector(xform,point);

		spOutputPoints->set(pointIndex,transformed);
	}

	if(spDrawGuide.isValid())
	{
		const Color red(1,0,0);
		const Color green(0,1,0);
		const Color blue(0,0,1);

		const SpatialVector lowerOut=transformVector(xform,lowerIn);
		const SpatialVector upperOut=transformVector(xform,upperIn);

		const Box3 boxIn(lowerIn,sizeIn);
		const Box3 boxRef(lowerRef,sizeRef);
		const Box3 boxOut(lowerOut,upperOut-lowerOut);

		spDrawGuide->drawBox(boxIn,red);
		spDrawGuide->drawBox(boxRef,green);
		spDrawGuide->drawBox(boxOut,blue);
	}

	const SpatialVector translation=translateRef-centerIn;

	String textT;
	if(doTranslate)
	{
		textT.sPrintf("T %.1f %.1f %.1f",
				translation[0],translation[1],translation[2]);
	}

	String textS;
	if(doScale)
	{
		if(uniformScale)
		{
			textS.sPrintf("%sS %.1f",doTranslate? " ": "",scaling[0]);
		}
		else
		{
			textS.sPrintf("%sS %.1f %.1f %.1f",
					doTranslate? " ": "",scaling[0],scaling[1],scaling[2]);
		}
	}
	catalog<String>("summary")=textT+textS;
}
