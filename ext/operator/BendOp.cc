/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <operator/operator.pmh>

#define FE_BND_DEBUG		FALSE

using namespace fe;
using namespace fe::ext;

void BendOp::initialize(void)
{
	catalog<String>("icon")="FE_alpha";

	catalog<Real>("bend")=0.0;
	catalog<Real>("bend","min")= -180.0;
	catalog<Real>("bend","low")= -90.0;
	catalog<Real>("bend","high")=90;
	catalog<Real>("bend","max")=180;
	catalog<String>("bend","label")="Bend";
	catalog<String>("bend","hint")=
			"Rotate the tip of the shape by this angle,"
			" deforming the whole shape evenly.";

	catalog<Real>("twist")=0.0;
	catalog<Real>("twist","min")= -180.0;
	catalog<Real>("twist","low")= -90.0;
	catalog<Real>("twist","high")=90;
	catalog<Real>("twist","max")=180;
	catalog<String>("twist","label")="Twist";
	catalog<String>("twist","hint")=
			"Spin the shape along its length during the bend by this angle.";

	catalog<SpatialVector>("base")=SpatialVector(0,0,0);
	catalog<String>("base","label")="Base";
	catalog<String>("base","hint")=
			"The start of the input shape."
			"  Points near the base will deform very little.";

	catalog<SpatialVector>("tip")=SpatialVector(0,1,0);
	catalog<String>("tip","label")="Tip";
	catalog<String>("tip","hint")=
			"The end of the input shape."
			"  Points near the tip (or beyond) will deform the most.";

	catalog<SpatialVector>("facing")=SpatialVector(-1,0,0);
	catalog<String>("facing","label")="Facing";
	catalog<String>("facing","hint")=
			"The shape will bend towards this direction,"
			" although large angles may cause it to loop back."
			"  This should be roughly perpendicular to the line"
			" from base to tip, although it will be corrected to such"
			" unless it is too degenerate.";

	//* TODO lots of other methods, like in CurveCreateOp
	catalog<String>("interpolation")="power";
	catalog<String>("interpolation","label")="Interpolation";
	catalog<String>("interpolation","choice:0")="bezier";
	catalog<String>("interpolation","label:0")="Bezier";
	catalog<String>("interpolation","choice:1")="power";
	catalog<String>("interpolation","label:1")="Power";
	catalog<String>("interpolation","hint")=
			"Bezier will approximate the power computation much faster,"
			" but will extrapolate poorly."
			"  For surfaces that reach outside the base to tip span,"
			" power mode should produce much better results.";

	catalog< sp<Component> >("Input Surface");

	catalog< sp<Component> >("Output Surface");
	catalog<String>("Output Surface","IO")="output";
	catalog<String>("Output Surface","copy")="Input Surface";
}

void BendOp::solve(SpatialTransform& a_rPartial,
	const SpatialTransform& a_rDelta,Real a_power) const
{
	if(m_useBezier)
	{
		m_matrixBezier.solve(a_rPartial,a_rDelta,a_power);
	}
	else
	{
		m_matrixPower.solve(a_rPartial,a_rDelta,a_power);
	}
}

void BendOp::handle(Record& a_rSignal)
{
	const Real bend=catalog<Real>("bend");
	const Real twist=catalog<Real>("twist");
	const SpatialVector base=catalog<SpatialVector>("base");
	const SpatialVector tip=catalog<SpatialVector>("tip");
	const SpatialVector facing=catalog<SpatialVector>("facing");
	const String interpolation=catalog<String>("interpolation");

	m_useBezier=(interpolation=="bezier");

	String summary;
	summary.sPrintf("bend %.2f twist %.2f",bend,twist);
	catalog<String>("summary")=summary;

	const Real length=magnitude(tip-base);
	if(length<=Real(0))
	{
		return;
	}

	sp<SurfaceAccessibleI> spOutputAccessible;
	if(!accessOutput(spOutputAccessible,a_rSignal)) return;

	sp<SurfaceAccessorI> spOutputPoint;
	if(!access(spOutputPoint,spOutputAccessible,e_point,e_position)) return;

	sp<DrawI> spDrawGuide;
	accessGuide(spDrawGuide,a_rSignal);

	const SpatialVector tangent=unitSafe(tip-base);
	SpatialVector direction=unitSafe(facing);
	const SpatialVector side=unitSafe(cross(tangent,direction));
	direction=unitSafe(cross(side,tangent));

	SpatialTransform xformBase;
	makeFrameTangentX(xformBase,base,tangent,direction);

	SpatialTransform invBase;
	invert(invBase,xformBase);

	SpatialTransform xformTip=xformBase;
	rotate(xformTip,0.5*bend*fe::degToRad,e_zAxis);
	translate(xformTip,SpatialVector(length,0,0));
	rotate(xformTip,twist*fe::degToRad,e_xAxis);
	rotate(xformTip,0.5*bend*fe::degToRad,e_zAxis);
	SpatialTransform xformDelta=xformTip*invBase;

#if FE_BND_DEBUG
	feLog("base\n%s\n",c_print(xformBase));
	feLog("tip\n%s\n",c_print(xformTip));
	feLog("delta\n%s\n",c_print(xformDelta));
#endif

	const I32 subSteps(16);
	SpatialVector lastTranslation=xformBase.translation();
	Real distance(0);
	for(I32 subStep=0;subStep<subSteps;subStep++)
	{
		const Real power=subStep/(subSteps-1.0);

		SpatialTransform partialDelta;
		solve(partialDelta,xformDelta,power);

		const SpatialTransform xformBetween=partialDelta*xformBase;

		distance+=magnitude(xformBetween.translation()-lastTranslation);
		lastTranslation=xformBetween.translation();
	}

#if FE_BND_DEBUG
	feLog("distance %.6G length %.6G\n",distance,length);
#endif

	//* NOTE redo to fix distance
	xformTip=xformBase;
	rotate(xformTip,0.5*bend*fe::degToRad,e_zAxis);
	translate(xformTip,SpatialVector(length/distance,0,0));
	rotate(xformTip,twist*fe::degToRad,e_xAxis);
	rotate(xformTip,0.5*bend*fe::degToRad,e_zAxis);
	xformDelta=xformTip*invBase;

	if(spDrawGuide.isValid())
	{
		const Color red(1.0,0.0,0.0);
		const Color green(0.0,1.0,0.0);
		const Color blue(0.0,0.0,1.0);

		Color colors[6];
		colors[0]=red;
		colors[1]=red;
		colors[2]=green;
		colors[3]=green;
		colors[4]=blue;
		colors[5]=blue;

		SpatialVector line[6];

		const I32 subSteps(5);
		for(I32 subStep=0;subStep<subSteps;subStep++)
		{
			const Real power=subStep/(subSteps-1.0);

			SpatialTransform partialDelta;
			solve(partialDelta,xformDelta,power);

			const SpatialTransform xformBetween=partialDelta*xformBase;

			line[0]=xformBetween.translation();
			line[1]=line[0]+xformBetween.column(0);
			line[2]=line[0];
			line[3]=line[0]+xformBetween.column(1);
			line[4]=line[0];
			line[5]=line[0]+xformBetween.column(2);

			spDrawGuide->drawLines(line,NULL,6,DrawI::e_discrete,
					TRUE,colors);
		}
	}

	const I32 pointCount=spOutputPoint->count();
	for(I32 pointIndex=0;pointIndex<pointCount;pointIndex++)
	{
		const SpatialVector point=spOutputPoint->spatialVector(pointIndex);

		SpatialVector local=transformVector(invBase,point);

		const Real along=local[0];
		const Real power=along/length;

		local[0]=Real(0);

		SpatialTransform partialDelta;
		solve(partialDelta,xformDelta,power);

		const SpatialTransform xformBetween=partialDelta*xformBase;

		const SpatialVector transformed=transformVector(xformBetween,local);

		spOutputPoint->set(pointIndex,transformed);
	}
}
