/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <operator/operator.pmh>

#define FE_CSO_DEBUG		FALSE

using namespace fe;
using namespace fe::ext;

void CurveSampleOp::initialize(void)
{
	catalog<String>("icon")="FE_alpha";

	catalog<bool>("uniform")=false;
	catalog<String>("uniform","label")="Start with Uniform Spacing";

	catalog<Real>("gamma")=1.0;
	catalog<Real>("gamma","min")=0.0;
	catalog<Real>("gamma","low")=0.25;
	catalog<Real>("gamma","high")=4.0;
	catalog<Real>("gamma","max")=1e6;
	catalog<String>("gamma","label")="Gamma";
	catalog<String>("gamma","hint")="Overall bias towards root.";

	catalog<bool>("densityRamped")=true;
	catalog<String>("densityRamped","label")="Plot Density";

	catalog< sp<Component> >("densityRamp");
	catalog<String>("densityRamp","implementation")="RampI";
	catalog<String>("densityRamp","default")="one";
	catalog<String>("densityRamp","label")="Density Plot";
	catalog<String>("densityRamp","enabler")="densityRamped";

	catalog< sp<Component> >("Input Curves");

	catalog< sp<Component> >("Output Curves");
	catalog<String>("Output Curves","IO")="output";
	catalog<String>("Output Curves","copy")="Input Curves";
}

void CurveSampleOp::handle(Record& a_rSignal)
{
#if FE_CSO_DEBUG
	feLog("CurveSampleOp::handle\n");
#endif

	const BWORD uniform=catalog<bool>("uniform");
	const Real gamma=catalog<Real>("gamma");

	sp<RampI> spRampDensity;
	if(catalog<bool>("densityRamped"))
	{
		spRampDensity=catalog< sp<Component> >("densityRamp");
	}

	sp<SurfaceAccessibleI> spOutputAccessible;
	if(!access(spOutputAccessible,"Output Curves")) return;

	sp<SurfaceAccessorI> spOutputVertices;
	if(!access(spOutputVertices,spOutputAccessible,
			e_primitive,e_vertices)) return;

	const I32 divisions=10000;
	Array<Real> accumulation(divisions);

	if(spRampDensity.isValid())
	{
		Real sum=Real(0);

		for(I32 sample=0;sample<divisions;sample++)
		{
			const Real t=sample/Real(divisions-1);

			const Real density=spRampDensity->eval(t);
			sum+=density;

			accumulation[sample]=sum;
		}

		const Real invSum=1/sum;

		for(I32 sample=0;sample<divisions;sample++)
		{
			accumulation[sample]*=invSum;
		}
	}

	ConvergentSpline convergentSpline;
	Array<Real> segmentLength;
	Array<SpatialVector> pointArray;

	I32 curveCount=0;

	const I32 primitiveCount=spOutputVertices->count();
	for(I32 primitiveIndex=0;primitiveIndex<primitiveCount;primitiveIndex++)
	{
		const I32 subCount=spOutputVertices->subCount(primitiveIndex);
		if(subCount<2)
		{
			continue;
		}

		if(I32(segmentLength.size())<subCount)
		{
			segmentLength.resize(subCount);
		}

		SpatialVector* pointArray=new SpatialVector[subCount];

		Real curveLength=Real(0);
		SpatialVector lastPoint=
				spOutputVertices->spatialVector(primitiveIndex,0);
		pointArray[0]=lastPoint;

		for(I32 subIndex=1;subIndex<subCount;subIndex++)
		{
			const SpatialVector point=
					spOutputVertices->spatialVector(primitiveIndex,subIndex);
			const Real length=magnitude(point-lastPoint);

			pointArray[subIndex]=point;
			segmentLength[subIndex-1]=length;
			curveLength+=length;

			lastPoint=point;
		}

		if(curveLength<=Real(0))
		{
			continue;
		}

		convergentSpline.configure(subCount,pointArray,NULL);

		I32 sample=0;
		Real along=segmentLength[0];

		//* NOTE not affecting first or last
		for(I32 subIndex=1;subIndex<subCount-1;subIndex++)
		{
			const Real unitDistance=uniform?
					subIndex/Real(subCount-1):
					along/curveLength;
			const Real unitPower=pow(unitDistance,gamma);

			Real unitDistributed=unitPower;
			if(spRampDensity.isValid())
			{
				while(sample<divisions-1 && accumulation[sample]<unitPower)
				{
					sample++;
				}
				unitDistributed=sample/Real(divisions-1);
			}

			const SpatialVector ramped=
					convergentSpline.solve(unitDistributed*curveLength);

			spOutputVertices->set(primitiveIndex,subIndex,ramped);

			if(subIndex<subCount-1)
			{
				along+=segmentLength[subIndex];
			}
		}

		curveCount++;

		delete[] pointArray;
	}

	String summary;
	summary.sPrintf("%d curve%s\n",curveCount,curveCount==1? "": "s");
	catalog<String>("summary")=summary;

#if FE_CSO_DEBUG
	feLog("CurveSampleOp::handle done\n");
#endif
}
