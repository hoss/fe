/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <operator/operator.pmh>

#define FE_FCO_DEBUG		FALSE
#define FE_FCO_EDIT_DEBUG	FALSE
#define FE_FCO_OUTPUT_DEBUG	FALSE

using namespace fe;
using namespace fe::ext;

void FollicleOp::initialize(void)
{
	catalog<String>("icon")="FE_alpha";

	//* page Data

	catalog<String>("inputMode")="point";
	catalog<String>("inputMode","label")="Affect";
	catalog<String>("inputMode","choice:0")="point";
	catalog<String>("inputMode","label:0")="Points";
	catalog<String>("inputMode","choice:1")="primitive";
	catalog<String>("inputMode","label:1")="Primitives";
	catalog<String>("inputMode","page")="Data";
	catalog<String>("inputMode","hint")=
			"Change points directly or create proxy points for primitives."
			"  For proxy points, move primitives using a rigid wrap.";

	//* TODO could also have fragments of points
	catalog<bool>("inputFragment")=true;
	catalog<String>("inputFragment","label")="Input Fragment";
	catalog<String>("inputFragment","page")="Data";
	catalog<bool>("inputFragment","joined")=true;
	catalog<String>("inputFragment","enabler")="inputMode==primitive";
	catalog<String>("inputFragment","hint")=
			"Associate collections of primitives with"
			" matching fragment attribute.";

	catalog<String>("inputFragmentAttr")="part";
	catalog<String>("inputFragmentAttr","label")="Attr";
	catalog<String>("inputFragmentAttr","page")="Data";
	catalog<String>("inputFragmentAttr","enabler")=
			"inputMode==primitive && inputFragment";
	catalog<String>("inputFragmentAttr","hint")=
			"Name of string attribute describing membership"
			" in a collection of primitives.";

	catalog<String>("proxyMode")="lowest";
	catalog<String>("proxyMode","label")="Proxy Mode";
	catalog<String>("proxyMode","choice:0")="lowest";
	catalog<String>("proxyMode","label:0")="Lowest Point Index";
	catalog<String>("proxyMode","choice:1")="closest";
	catalog<String>("proxyMode","label:1")="Closest Point";
	catalog<String>("proxyMode","page")="Data";
	catalog<String>("proxyMode","hint")=
			"How to select a proxy point from primitives or fragments.";

	//* TODO sync to id, uv
	catalog<String>("syncMode")="location";
	catalog<String>("syncMode","label")="Sync To";
	catalog<String>("syncMode","choice:0")="index";
	catalog<String>("syncMode","label:0")="Index";
	catalog<String>("syncMode","choice:1")="location";
	catalog<String>("syncMode","label:1")="Location";
	catalog<bool>("syncMode","joined")=true;
	catalog<String>("syncMode","page")="Data";
	catalog<String>("snapMode","hint")=
			"How to associate input points with change records"
			" when the input changes.";

	catalog<String>("snapMode")="location";
	catalog<String>("snapMode","label")="Snap To";
	catalog<String>("snapMode","choice:0")="location";
	catalog<String>("snapMode","label:0")="Location";
	catalog<String>("snapMode","choice:1")="uv";
	catalog<String>("snapMode","label:1")="UV";
	catalog<String>("snapMode","page")="Data";
	catalog<String>("snapMode","hint")=
			"Make changes by snapping to the surface in world space"
			" or by following UV coordinates.";

//	catalogMoveToEnd("IdAttr");
	catalogRemove("IdAttr");

	catalog<bool>("affectUV")=true;
	catalog<String>("affectUV","label")="Affect UV";
	catalog<bool>("affectUV","joined")=true;
	catalog<String>("affectUV","page")="Data";
	catalog<String>("affectUV","hint")=
			"Write UV changes to output.";

	catalog<bool>("customUV")=false;
	catalog<String>("customUV","label")="Custom";
	catalog<bool>("customUV","joined")=true;
	catalog<String>("customUV","enabler")="affectUV";
	catalog<String>("customUV","page")="Data";
	catalog<String>("customUV","hint")=
			"Write UV changes to a particular named attribute."
			"  Otherwise, use the standard UV storage for"
			" the application in use.";

	catalog<String>("uvAttribute")="surface_uvw";
	catalog<String>("uvAttribute","label")="Attribute";
	catalog<String>("uvAttribute","enabler")="affectUV && customUV";
	catalog<String>("uvAttribute","page")="Data";
	catalog<String>("uvAttribute","hint")=
			"The name of the custom UV attribute.";

	catalog<bool>("affectAttribute")=false;
	catalog<String>("affectAttribute","label")="Paint";
	catalog<bool>("affectAttribute","joined")=true;
	catalog<String>("affectAttribute","page")="Data";
	catalog<String>("affectAttribute","hint")=
			"Paint a particular attribute (in Paint mode).";

	catalog<String>("attribute");
	catalog<String>("attribute","label")="Attribute";
	catalog<String>("attribute","enabler")="affectAttribute";
	catalog<String>("attribute","page")="Data";
	catalog<String>("attribute","hint")=
			"The name of the attribute to paint."
			"  Currently, this can be a real or spatial vector.";

	catalog<String>("hideGroup")="hide";
	catalog<String>("hideGroup","label")="Hide To Group";
	catalog<String>("hideGroup","page")="Data";
	catalog<String>("hideGroup","hint")=
			"Put all hidden points in this new group.";

	catalog<bool>("PartitionDriver")=false;
	catalog<String>("PartitionDriver","label")="Partition Driver";
	catalog<String>("PartitionDriver","page")="Data";
	catalog<bool>("PartitionDriver","joined")=true;
	catalog<String>("PartitionDriver","hint")=
			"Distiguish regions by matching attribute values.";

	catalog<String>("DriverPartitionAttr")="part";
	catalog<String>("DriverPartitionAttr","label")="Driver Attr";
	catalog<String>("DriverPartitionAttr","page")="Data";
	catalog<String>("DriverPartitionAttr","enabler")="PartitionDriver";
	catalog<String>("DriverPartitionAttr","hint")=
			"Primitive string attribute on driver used to distiguish regions.";

	catalog<bool>("createFragment")=true;
	catalog<String>("createFragment","label")="Set Fragment";
	catalog<String>("createFragment","page")="Data";
	catalog<bool>("createFragment","joined")=true;
	catalog<String>("createFragment","hint")=
			"Add a string to each output point or primitive.";

	catalog<String>("fragmentAttr")="part";
	catalog<String>("fragmentAttr","label")="Attr";
	catalog<String>("fragmentAttr","page")="Data";
	catalog<String>("fragmentAttr","enabler")="createFragment";
	catalog<String>("fragmentAttr","hint")=
			"String added to each point or primitive.";

	catalog<String>("fragmentName")="follicleINDEX";
	catalog<String>("fragmentName","label")="Fragment Name";
	catalog<String>("fragmentName","page")="Data";
	catalog<String>("fragmentName","enabler")="createFragment";
	catalog<bool>("fragmentName","joined")=true;
	catalog<String>("fragmentName","hint")=
			"String added to each point or primitive.";

	catalog<String>("pattern")="INDEX";
	catalog<String>("pattern","label")="Replacing";
	catalog<String>("pattern","page")="Data";
	catalog<String>("pattern","enabler")="createFragment";
	catalog<String>("pattern","hint")=
			"Replace this string in the fragment name with"
			" the point or primitive index.";

	catalog<bool>("readStash")=true;
	catalog<String>("readStash","label")="Read Stash For Highlight";
	catalog<String>("readStash","page")="Data";
	catalog<bool>("readStash","joined")=true;

	catalog<String>("stashKey")="stash";
	catalog<String>("stashKey","label")="Key";
	catalog<String>("stashKey","suggest")="word";
	catalog<String>("stashKey","page")="Data";
	catalog<String>("stashKey","enabler")="readStash";
	catalog<bool>("stashKey","joined")=true;
	catalog<String>("stashKey","hint")=
			"Global name that the mesh is stored under.";

	catalog<bool>("clearStash")=true;
	catalog<String>("clearStash","label")="Remove Stale";
	catalog<String>("clearStash","page")="Data";
	catalog<String>("clearStash","enabler")="readStash";
	catalog<String>("clearStash","hint")=
			"Clear the out-of-line stash data after each time it is read."
			"  If the stash is not thereafter updated,"
			" no highlight will be shown.";

	catalog<I32>("resetAll")=0;
	catalog<String>("resetAll","label")="Reset All Changes";
	catalog<String>("resetAll","suggest")="button";
	catalog<String>("resetAll","page")="Data";
	catalog<String>("resetAll","hint")="Discard all modifications.";

	//* page Edit

	catalog<String>("pickMode")="geodesic";
	catalog<String>("pickMode","label")="Pick Method";
	catalog<String>("pickMode","choice:0")="spatial";
	catalog<String>("pickMode","label:0")="Spatial";
	catalog<String>("pickMode","choice:1")="geodesic";
	catalog<String>("pickMode","label:1")="Geodesic";
	catalog<bool>("pickMode","joined")=true;
	catalog<String>("pickMode","page")="Edit";
	catalog<String>("pickMode","hint")=
			"Method to determine which points are near the mouse pointer.";

	catalog<bool>("backfacing")=false;
	catalog<String>("backfacing","label")="Include Backfacing";
	catalog<bool>("backfacing","joined")=true;
	catalog<String>("backfacing","page")="Edit";
	catalog<String>("backfacing","hint")=
			"Allow picking of backfacing points.";

	catalog<bool>("showStash")=true;
	catalog<String>("showStash","label")="Show Highlight";
	catalog<String>("showStash","IO")="input output";
	catalog<String>("showStash","page")="Edit";
	catalog<String>("showStash","enabler")="createFragment";	//* TODO exact
	catalog<String>("showStash","hint")=
			"Show stash geometry, if available.";

	catalog<String>("modifyMode")="drag";
	catalog<String>("modifyMode","label")="Modification";
	catalog<String>("modifyMode","choice:0")="paint";
	catalog<String>("modifyMode","label:0")="Paint Values";
	catalog<String>("modifyMode","choice:1")="drag";
	catalog<String>("modifyMode","label:1")="Drag Positions";
	catalog<bool>("modifyMode","joined")=true;
	catalog<String>("modifyMode","page")="Edit";
	catalog<String>("modifyMode","hint")=
			"Adjusts interactive tools to either to move data points"
			" or change their values.";

	catalog<String>("action")="replace";
	catalog<String>("action","label")="Paint Action";
	catalog<String>("action","choice:0")="replace";
	catalog<String>("action","label:0")="Replace/Restore";
	catalog<String>("action","choice:1")="smooth";
	catalog<String>("action","label:1")="Smooth/Saturate";
	catalog<String>("action","choice:2")="add";
	catalog<String>("action","label:2")="Add/Subtract";
	catalog<String>("action","choice:3")="scale";
	catalog<String>("action","label:3")="Scale/InvScale";
	catalog<String>("action","choice:4")="conform";
	catalog<String>("action","label:4")="Conform/Revert";
	catalog<String>("action","page")="Edit";
	catalog<String>("action","IO")="input output";
	catalog<String>("action","enabler")="modifyMode==paint";
	catalog<String>("action","hint")=
			"In Paint mode, this specifies how the values will be affected.";

	catalog<Real>("pickRadius")=1.0;
	catalog<Real>("pickRadius","high")=10.0;
	catalog<Real>("pickRadius","max")=1e3;
	catalog<String>("pickRadius","label")="Pick Radius";
	catalog<String>("pickRadius","IO")="input output";
	catalog<bool>("pickRadius","joined")=true;
	catalog<String>("pickRadius","page")="Edit";
	catalog<String>("pickRadius","hint")="Outer scope of area select.";

	catalog<Real>("falloff")=0.5;
	catalog<Real>("falloff","max")=1.0;
	catalog<String>("falloff","label")="Falloff";
	catalog<String>("falloff","IO")="input output";
	catalog<bool>("falloff","joined")=true;
	catalog<String>("falloff","page")="Edit";
	catalog<String>("falloff","hint")="Falloff portion of area select.";

	catalog<Real>("fuzzy")=0.0;
	catalog<Real>("fuzzy","max")=1.0;
	catalog<String>("fuzzy","label")="Fuzzy Weights";
	catalog<String>("fuzzy","page")="Edit";
	catalog<String>("fuzzy","hint")="Selection randomness.";

	catalog<SpatialVector>("replacement")=SpatialVector(1.0,1.0,1.0);
	catalog<String>("replacement","label")="Replacement";
	catalog<String>("replacement","IO")="input output";
	catalog<String>("replacement","suggest")="color";
	catalog<String>("replacement","page")="Edit";
	catalog<String>("replacement","enabler")="modifyMode==paint";
	catalog<bool>("replacement","joined")=true;
	catalog<String>("replacement","hint")=
			"In Paint mode, this is the componentized value used to"
			" affect the existing value,"
			" except scaling, which has an independent Scale value.";

	catalog<Real>("magnitude")=1.0;
	catalog<Real>("magnitude","low")=0.0;
	catalog<Real>("magnitude","high")=1.0;
	catalog<Real>("magnitude","min")= -1e6;
	catalog<Real>("magnitude","max")=1e6;
	catalog<String>("magnitude","label")="Magnitude";
	catalog<String>("magnitude","page")="Edit";
	catalog<String>("magnitude","enabler")="modifyMode==paint";
	catalog<String>("magnitude","hint")=
			"In Paint mode, this adjusts all components of the"
			" effective replacement value.";

	catalog<SpatialVector>("mask")=SpatialVector(1.0,1.0,1.0);
	catalog<String>("mask","label")="Mask";
	catalog<String>("mask","suggest")="color";
	catalog<String>("mask","page")="Edit";
	catalog<String>("mask","enabler")="modifyMode==paint";
	catalog<bool>("mask","joined")=true;
	catalog<String>("mask","hint")=
			"In Paint mode, this limits the application of the"
			" replacement value."
			"  A value of zero for a component blocks changes"
			" to that component.";

	catalog<Real>("opacity")=0.1;
	catalog<Real>("opacity","high")=1.0;
	catalog<String>("opacity","label")="Opacity";
	catalog<String>("opacity","IO")="input output";
	catalog<String>("opacity","page")="Edit";
	catalog<String>("opacity","enabler")="modifyMode==paint";
	catalog<String>("opacity","hint")=
			"In Paint mode, this determines how quickly changes are made.";

	catalog<Real>("scalar")=1.1;
	catalog<Real>("scalar","low")=0.0;
	catalog<Real>("scalar","high")=2.0;
	catalog<Real>("scalar","min")= -1e6;
	catalog<Real>("scalar","max")=1e6;
	catalog<String>("scalar","label")="Scale";
	catalog<String>("scalar","page")="Edit";
	catalog<String>("scalar","enabler")="modifyMode==paint";
	catalog<String>("scalar","hint")=
			"In Paint mode, for the Scale action,"
			" this is the magnitude of scaling.";

	//* TODO clamp min,max

	//* TODO display min,max

	//* page Display

	catalog<String>("displayStyle")="pretty";
	catalog<String>("displayStyle","label")="Display Style";
	catalog<String>("displayStyle","choice:0")="fast";
	catalog<String>("displayStyle","label:0")="Fast";
	catalog<String>("displayStyle","choice:1")="balanced";
	catalog<String>("displayStyle","label:1")="Balanced";
	catalog<String>("displayStyle","choice:2")="pretty";
	catalog<String>("displayStyle","label:2")="Pretty";
	catalog<bool>("displayStyle","joined")=true;
	catalog<String>("displayStyle","page")="Display";
	catalog<String>("displayStyle","hint")=
			"Approach to drawing display elements.";

	catalog<bool>("showPrompt")=true;
	catalog<String>("showPrompt","label")="Show Prompt";
	catalog<String>("showPrompt","page")="Display";
	catalog<String>("showPrompt","hint")=
			"Describe controls where applicable."
			"  In some cases, this may be distracting.";

	catalog<String>("showPaint")="always";
	catalog<String>("showPaint","label")="Show Paint";
	catalog<String>("showPaint","choice:0")="never";
	catalog<String>("showPaint","label:0")="Never";
	catalog<String>("showPaint","choice:1")="drag";
	catalog<String>("showPaint","label:1")="In Drag Mode";
	catalog<String>("showPaint","choice:2")="paint";
	catalog<String>("showPaint","label:2")="In Paint Mode";
	catalog<String>("showPaint","choice:3")="always";
	catalog<String>("showPaint","label:3")="Always";
	catalog<bool>("showPaint","joined")=true;
	catalog<String>("showPaint","page")="Display";
	catalog<String>("showPaint","hint")=
			"When to show polygonized values in the 3D viewer.";

	catalog<String>("showSquares")="always";
	catalog<String>("showSquares","label")="Show Squares";
	catalog<String>("showSquares","choice:0")="never";
	catalog<String>("showSquares","label:0")="Never";
	catalog<String>("showSquares","choice:1")="drag";
	catalog<String>("showSquares","label:1")="In Drag Mode";
	catalog<String>("showSquares","choice:2")="paint";
	catalog<String>("showSquares","label:2")="In Paint Mode";
	catalog<String>("showSquares","choice:3")="always";
	catalog<String>("showSquares","label:3")="Always";
	catalog<bool>("showSquares","joined")=true;
	catalog<String>("showSquares","page")="Display";
	catalog<String>("showSquares","hint")=
			"When to show data points in the 3D viewer.";

	catalog<bool>("showPointIndex")=false;
	catalog<String>("showPointIndex","label")="Show Index";
	catalog<String>("showPointIndex","page")="Display";

	catalog<String>("showMap")="always";
	catalog<String>("showMap","label")="Show Map";
	catalog<String>("showMap","choice:0")="never";
	catalog<String>("showMap","label:0")="Never";
	catalog<String>("showMap","choice:1")="drag";
	catalog<String>("showMap","label:1")="In Drag Mode";
	catalog<String>("showMap","choice:2")="paint";
	catalog<String>("showMap","label:2")="In Paint Mode";
	catalog<String>("showMap","choice:3")="always";
	catalog<String>("showMap","label:3")="Always";
	catalog<bool>("showMap","joined")=true;
	catalog<String>("showMap","page")="Display";
	catalog<String>("showMap","hint")=
			"When to show polygonized values in the 2D overlay.";

	catalog<String>("showDots")="always";
	catalog<String>("showDots","label")="Show Dots";
	catalog<String>("showDots","choice:0")="never";
	catalog<String>("showDots","label:0")="Never";
	catalog<String>("showDots","choice:1")="drag";
	catalog<String>("showDots","label:1")="In Drag Mode";
	catalog<String>("showDots","choice:2")="paint";
	catalog<String>("showDots","label:2")="In Paint Mode";
	catalog<String>("showDots","choice:3")="always";
	catalog<String>("showDots","label:3")="Always";
	catalog<String>("showDots","page")="Display";
	catalog<String>("showDots","hint")=
			"When to show data points in the 2D overlay.";

	catalog<Real>("mapScale")=128.0;
	catalog<Real>("mapScale","min")=1.0;
	catalog<Real>("mapScale","max")=1e3;
	catalog<String>("mapScale","label")="Map Scale";
	catalog<String>("mapScale","page")="Display";
	catalog<String>("mapScale","hint")=
			"Pixel width of a unit UV square.";

	catalog<Real>("singleRadius")=0.2;
	catalog<String>("singleRadius","label")="Single Radius";
	catalog<String>("singleRadius","page")="Display";
	catalog<String>("singleRadius","hint")=
			"Brush radius during single select.";

	catalog< sp<Component> >("Brush");
	catalog<String>("Brush","implementation")="DrawI";
	catalog<String>("Brush","visible")="any";

	catalog< sp<Component> >("Input Surface");

	catalog< sp<Component> >("Driver Surface");

	//* TODO reuse will need to be able to delete points from mesh
	catalog< sp<Component> >("Output Surface");
	catalog<String>("Output Surface","IO")="output";
	catalog<String>("Output Surface","copy")="Input Surface";
	catalog<bool>("Output Surface","recycle")=true;

	m_spWireframe=new DrawMode();
	m_spWireframe->setDrawStyle(DrawMode::e_wireframe);
	m_spWireframe->setAntialias(TRUE);
	m_spWireframe->setLineWidth(2.0);	//* 2D pick circle
	m_spWireframe->setBackfaceCulling(FALSE);

	m_spSolid=new DrawMode();
	m_spSolid->copy(m_spWireframe);
	m_spSolid->setDrawStyle(DrawMode::e_solid);
	m_spSolid->setLit(FALSE);
	m_spSolid->setPointSize(2.0);		//* sparkle
	m_spSolid->setLineWidth(1.0);		//* squares

	m_spNarrowWire=new DrawMode();
	m_spNarrowWire->copy(m_spWireframe);
	m_spNarrowWire->setLineWidth(1.0);	//* stash pick

	m_spWideWire=new DrawMode();
	m_spWideWire->copy(m_spSolid);
	m_spWideWire->setLineWidth(16.0);	//* stash halo
	m_spWideWire->setZBuffering(FALSE);

	m_spForeshadow=new DrawMode();
	m_spForeshadow->copy(m_spSolid);
	m_spForeshadow->setDrawStyle(DrawMode::e_foreshadow);
	m_spForeshadow->setAntialias(TRUE);
	m_spForeshadow->setLayer(1);
	m_spForeshadow->setLineWidth(3.0);	//* 3D pick circle

	m_spHazy=new DrawMode();
	m_spHazy->copy(m_spForeshadow);
	m_spHazy->setDrawStyle(DrawMode::e_solid);
	m_spHazy->setZBuffering(FALSE);		//* pick drag

	m_spLittleDot=new DrawMode();
	m_spLittleDot->copy(m_spSolid);
	m_spLittleDot->setPointSize(3.0);	//* map dot

	m_spBigDot=new DrawMode();
	m_spBigDot->copy(m_spSolid);
	m_spBigDot->setPointSize(5.0);		//* map dot surround

	m_spBehind=new DrawMode();
	m_spBehind->copy(m_spSolid);
	m_spBehind->setLayer(-1);			//* map background
}

void FollicleOp::handle(Record& a_rSignal)
{
	const String inputMode=catalog<String>("inputMode");
	const BWORD inputFragment=catalog<bool>("inputFragment");
	const String inputFragmentAttr=catalog<String>("inputFragmentAttr");

	const BWORD backfacing=catalog<bool>("backfacing");
	const String modifyMode=catalog<String>("modifyMode");
	const SpatialVector mask=catalog<SpatialVector>("mask");
	const Real magnitude=catalog<Real>("magnitude");
	const Real scalar=catalog<Real>("scalar");
	const Real singleRadius=catalog<Real>("singleRadius");
	const Real fuzzy=catalog<Real>("fuzzy");

	SpatialVector& rReplacement=catalog<SpatialVector>("replacement");
	String& rAction=catalog<String>("action");
	Real& rPickRadius=catalog<Real>("pickRadius");
	Real& rFalloff=catalog<Real>("falloff");
	Real& rOpacity=catalog<Real>("opacity");

	const String displayStyle=catalog<String>("displayStyle");
	const BWORD showPrompt=catalog<bool>("showPrompt");
	const String showPaint=catalog<String>("showPaint");
	const String showSquares=catalog<String>("showSquares");
	const String showMap=catalog<String>("showMap");
	const String showDots=catalog<String>("showDots");
	const BWORD readStash=catalog<bool>("readStash");
	BWORD& rShowStash=catalog<bool>("showStash");
	const BWORD clearStash=(readStash && catalog<bool>("clearStash"));

	const Real mapScale=catalog<Real>("mapScale");

	const BWORD affectAttribute=catalog<bool>("affectAttribute");
	const String attribute=catalog<String>("attribute");

	const BWORD partitionDriver=catalog<bool>("PartitionDriver");
	const String driverPartitionAttr=catalog<String>("DriverPartitionAttr");

	const BWORD createFragment=catalog<bool>("createFragment");
	const String fragmentAttr=catalog<String>("fragmentAttr");
	const String fragmentName=catalog<String>("fragmentName");
	const String pattern=catalog<String>("pattern");

	const BWORD pointMode=(inputMode=="point");
	const BWORD fragmented=(!pointMode && inputFragment &&
			!inputFragmentAttr.empty());
	const BWORD displayPretty=(displayStyle=="pretty");
	const BWORD displayBalanced=(displayStyle=="balanced");
	const BWORD drawPaint=
			(showPaint=="always" || showPaint==modifyMode);
	const BWORD drawSquares=
			(showSquares=="always" || showSquares==modifyMode);
	const BWORD drawPointIndex=
			(drawSquares && catalog<bool>("showPointIndex"));
	const BWORD drawMap=(showMap=="always" || showMap==modifyMode);
	const BWORD drawDots=(showDots=="always" || showDots==modifyMode);

	//* TODO param
	const Real mapX=8.0;
	const Real mapY=64.0;

	sp<DrawI> spDrawBrush;
	accessBrush(spDrawBrush,a_rSignal,e_quiet);

	sp<DrawI> spDrawOverlay;
	accessBrushOverlay(spDrawOverlay,a_rSignal,e_quiet);

#if FE_FCO_DEBUG
	feLog("FollicleOp::handle brush %d brushed %d\n",
			spDrawBrush.isValid(),m_brushed);
#endif

	sp<Catalog> spMasterCatalog=registry()->master()->catalog();

	if(spDrawBrush.isValid())
	{
		spDrawBrush->pushDrawMode(m_spForeshadow);
		spDrawOverlay->pushDrawMode(m_spSolid);

		const Color blank(0.0,0.0,0.0,0.0);
		const Color black(0.0,0.0,0.0);
		const Color white(1.0,1.0,1.0);
		const Color grey(0.1,0.1,0.1);
		const Color red(1.0,0.0,0.0);
		const Color darkGreen(0.0,0.5,0.0);
		const Color green(0.0,0.8,0.0);
		const Color blue(0.0,0.0,1.0);
		const Color steelBlue(0.1,0.1,0.6);
		const Color cyan(0.0,1.0,1.0);
		const Color magenta(1.0,0.6,1.0);
		const Color purple(0.5,0.0,1.0);
		const Color yellow(1.0,1.0,0.0);
		const Color lightYellow(1.0,1.0,0.6);
		const Color sparkle(1.0,1.0,0.6,0.8);

		const Real foreAlpha=0.5;
		const Color foreYellow(1.0,1.0,0.0,foreAlpha);
		const Color foreOrange(1.0,0.5,0.0,foreAlpha);

		const SpatialVector up(0.0,1.0,0.0);

		sp<ViewI> spView=spDrawOverlay->view();
		const Box2i viewport=spView->viewport();
		const I32 viewWidth=viewport.size()[0];
		const I32 viewHeight=viewport.size()[1];

		sp<CameraI> spCamera=spView->camera();

		if(m_spDriver.isValid())
		{
			BWORD changed=FALSE;

			const SpatialVector& rRayOrigin=rayOrigin(a_rSignal);
			const SpatialVector& rRayDirection=rayDirection(a_rSignal);

			m_event.bind(windowEvent(a_rSignal));

#if FE_FCO_DEBUG
			feLog("%s\n",c_print(m_event));
#endif

			SpatialVector intersection;
			set(intersection);
			SpatialVector hitNormal;
			set(hitNormal);
			BWORD impacted=FALSE;
			I32 hitPartition= -1;

			const Real maxDistance=100.0;
			sp<SurfaceI::ImpactI> spImpact=m_spDriver->rayImpact(
					rRayOrigin,rRayDirection,maxDistance);
			if(spImpact.isValid())
			{
				sp<SurfaceTriangles::Impact> spTriImpact=spImpact;
				if(spTriImpact.isValid())
				{
					intersection=spTriImpact->intersection();
					hitNormal=spTriImpact->normal();
					hitPartition=spTriImpact->partitionIndex();
#if FE_FCO_DEBUG
					const I32 face=spTriImpact->face();
					const I32 pickIndex=spTriImpact->triangleIndex();
					feLog("FollicleOp::handle face %d pick %d %s\n",
							face,pickIndex,c_print(intersection));
#endif

					impacted=TRUE;
				}
			}

			SpatialTransform cameraMatrix=spCamera->cameraMatrix();
			SpatialTransform cameraTransform;
			invert(cameraTransform,cameraMatrix);

			I32 closest= -1;
			Real closestDist= -1.0;

			const I32 outputCount=m_outputPosition.size();
			m_weightArray.resize(outputCount);
			m_colorRim.resize(outputCount);
			m_colorCenter.resize(outputCount);

			std::set<I32> hiddenSet;

			sp<RecordArray> spRA=m_spState->getArray("Hide");
			if(spRA.isValid())
			{
				for(int i=0;i<spRA->length();i++)
				{
					const I32 elementIndex=m_aIndex(spRA,i);
					hiddenSet.insert(elementIndex);
				}
			}

			U32 item=m_event.item();
			BWORD shifted=FALSE;
			if(item&WindowEvent::e_keyShift)
			{
				shifted=TRUE;
				item^=WindowEvent::e_keyShift;
			}
			BWORD ctrled=FALSE;
			if(item&WindowEvent::e_keyControl)
			{
				ctrled=TRUE;
				item^=WindowEvent::e_keyControl;
			}

			if(m_event.isKeyPress(WindowEvent::e_itemAny))
			{
				if(item==WindowEvent::e_keyCarriageReturn)
				{
					if(shifted)
					{
						m_addShowStash++;
						rShowStash=!rShowStash;
						changed=TRUE;
					}
					else
					{
						m_areaSelect=!m_areaSelect;
					}
				}

				if(modifyMode=="drag")
				{
					if(item==WindowEvent::e_keyEscape)
					{
						m_pickArray.clear();
					}

					if(item==WindowEvent::e_keyDelete)
					{
						const I32 pickCount=m_pickArray.size();

						if(pickCount)
						{
							if(ctrled)
							{
								anticipate("FollicleOp revert drag");
								for(I32 pickIndex=0;pickIndex<pickCount;
										pickIndex++)
								{
									const Pick& rPick=m_pickArray[pickIndex];
									const I32 elementIndex=rPick.m_elementId;

									revert(elementIndex);
								}
								resolve();
							}

							else
							{
								anticipate("FollicleOp hide toggle");
								for(I32 pickIndex=0;pickIndex<pickCount;
										pickIndex++)
								{
									const Pick& rPick=m_pickArray[pickIndex];
									const I32 elementIndex=rPick.m_elementId;

									hideToggle(elementIndex);
								}
								resolve();
							}
						}

						m_pickArray.clear();
						changed=TRUE;
					}
				}
			}

			const I32 inputCount=m_spInputElement->count();

			const WindowEvent::MouseButtons buttons=m_event.mouseButtons();
			const BWORD updateCenter=
					!(((buttons&WindowEvent::e_mbMiddle) &&
					(shifted || (modifyMode=="drag" && m_event.isMouseDrag())))
					||
					(!shifted && !ctrled &&
					modifyMode=="drag" && m_event.isMouseDrag() &&
					(buttons&WindowEvent::e_mbLeft)));

			if(updateCenter)
			{
				if(I32(m_facingArray.size())!=outputCount)
				{
					m_facingArray.resize(outputCount);

					for(I32 elementIndex=0;elementIndex<outputCount;
							elementIndex++)
					{
						const SpatialVector& point=
								m_outputPosition[elementIndex];

						sp<SurfaceI::ImpactI> spImpact=
								m_spDriver->nearestPoint(point);
						if(spImpact.isValid())
						{
							m_facingArray[elementIndex]=spImpact->normal();
						}
						else
						{
							set(m_facingArray[elementIndex],0.0,1.0,0.0);
						}
					}
				}

				const Real extent=rPickRadius;
				const Real extent2=extent*extent;

				if(m_spDriverGauge.isValid())
				{
					m_spDriverGauge->pick(intersection,rPickRadius);
				}

				for(I32 elementIndex=0;elementIndex<outputCount;elementIndex++)
				{
					m_weightArray[elementIndex]=0.0;

					Color color=blank;

					if(m_inputValue.size() && modifyMode=="paint")
					{
						if(m_attributeType=="real")
						{
							const Real value=
									m_inputValue[elementIndex][0];
							set(color,value,value,value);
						}
						else if(m_attributeType.match("vector."))
						{
							color=m_inputValue[elementIndex];
						}
					}

					m_colorCenter[elementIndex]=color;

					const SpatialVector facing=m_facingArray[elementIndex];
					const Real hitDot=dot(facing,cameraTransform.column(2));

					const BWORD facingBack=(hitDot<0.0);
					if(facingBack && !backfacing)
					{
						m_colorRim[elementIndex]=blank;
						continue;
					}

					m_colorRim[elementIndex]=facingBack? grey:
							(elementIndex<inputCount? steelBlue: purple);

					if(impacted)
					{
						const SpatialVector& point=
								m_outputPosition[elementIndex];
						const Real dist2=magnitudeSquared(point-intersection);
						if(dist2>extent2)
						{
							continue;
						}

						const Real distance=m_spDriverGauge.isValid()?
								m_spDriverGauge->distanceTo(point):
								sqrt(dist2);
						if(distance<0.0 || distance>extent)
						{
							continue;
						}

						if(m_areaSelect)
						{
							Real weight=1.0;

							const Real innerRadius=
									rPickRadius*(1.0-rFalloff);

							if(rFalloff>0.0 && distance>innerRadius)
							{
								weight-=(distance-innerRadius)/
										(rFalloff*rPickRadius);
							}

							if(fuzzy)
							{
								weight-=randomReal(0.0,fuzzy);
							}

							m_weightArray[elementIndex]=weight;
							m_colorRim[elementIndex]=m_areaSelect?
									Color(green*weight): green;
							m_colorRim[elementIndex][3]=1.0;
						}

						//* hidden ignored unless ctrl and shift both held
						if((closest<0 || closestDist>distance) &&
							((shifted && ctrled) ||
							hiddenSet.find(elementIndex)==hiddenSet.end()))
						{
							closest=elementIndex;
							closestDist=distance;
						}
					}
				}
				if(!m_areaSelect && closest>=0)
				{
					m_weightArray[closest]=1.0;
					m_colorRim[closest]=green;
				}
			}

#if FALSE
			spDrawBrush->pushDrawMode(m_spSolid);
			spDrawBrush->draw(m_spDriver,&white);
			spDrawBrush->popDrawMode();
#endif

			spRA=m_spState->getArray("Hide");
			if(spRA.isValid())
			{
				for(int i=0;i<spRA->length();i++)
				{
					const I32 elementIndex=m_aIndex(spRA,i);
					hiddenSet.insert(elementIndex);
				}
			}

			//* drag mode left click
			if(modifyMode=="drag" &&
					((!m_dragging &&
					m_event.isMouseRelease(WindowEvent::e_itemLeft)) ||
					(m_dragging &&
					(shifted || ctrled) && (buttons&WindowEvent::e_mbLeft))))
			{
				if(closest>=0)
				{
					if(ctrled)
					{
						if(m_areaSelect)
						{
							for(I32 elementIndex=0;elementIndex<outputCount;
									elementIndex++)
							{
								if(m_weightArray[elementIndex]>0.0 &&
										hiddenSet.find(elementIndex)==
										hiddenSet.end() &&
										picked(elementIndex))
								{
										pickToggle(elementIndex);
								}
							}
						}
						else if(hiddenSet.find(closest)==hiddenSet.end())
						{
							//* TODO add unpick function
							if(picked(closest))
							{
								pickToggle(closest);
							}
						}
						else if(shifted)
						{
							//* NOTE can ctrl-shift-pick hidden
							pickToggle(closest);
						}
					}
					else if(picked(closest)<1.0)
					{
						if(!shifted)
						{
							m_pickArray.clear();
						}
						if(m_areaSelect)
						{
							for(I32 elementIndex=0;elementIndex<outputCount;
									elementIndex++)
							{
								if(m_weightArray[elementIndex]>0.0 &&
										hiddenSet.find(elementIndex)==
										hiddenSet.end())
								{
									pick(elementIndex,
											m_weightArray[elementIndex]);
								}
							}
						}
						if(hiddenSet.find(closest)==hiddenSet.end())
						{
							pick(closest,1.0);
						}
					}
				}
				else if(!ctrled)
				{
					m_pickArray.clear();
				}
#if FE_FCO_EDIT_DEBUG
				feLog("FollicleOp::handle pickArray");
				const I32 pickCount=m_pickArray.size();
				for(I32 pickIndex=0;pickIndex<pickCount;pickIndex++)
				{
					const Pick& rPick=m_pickArray[pickIndex];
					const I32 elementIndex=rPick.m_elementId;
					const Real pointWeight=rPick.m_weight;
					feLog(" %d:%.2f",elementIndex,pointWeight);
				}
				feLog("\n");
#endif
			}

			if(m_event.isMouseWheel())
			{
				if(shifted)
				{
					m_addOpacity+=(m_event.state()>0)? 0.1: -0.1;
				}
				else
				{
					m_addWheel+=(m_event.state()>0)? 1: -1;
					updateAction(rAction);
				}
				changed=TRUE;
			}

			//* rebake after resizing area
			if(m_dragging && (shifted || ctrled) &&
					m_event.isMouseRelease(WindowEvent::e_itemMiddle))
			{
				changed=TRUE;
			}

			if(m_event.isMouseRelease() && !m_changing.empty())
			{
				resolve(m_changing);
			}

			if(buttons)
			{
				const SpatialVector deltaX=cameraTransform.column(0);
				const SpatialVector deltaY=cameraTransform.column(1);

				const Real cameraDist=fe::magnitude(intersection-rRayOrigin);

				const Real fovY=spCamera->fov()[1]*fe::degToRad;

				const Real fovX=fovY*viewWidth/viewHeight;

				const Real scaleX=cameraDist*tan(fovX/viewWidth);
				const Real scaleY=cameraDist*tan(fovY/viewHeight);

				const I32 mouseX=m_event.mouseX();
				const I32 mouseY=m_event.mouseY();

				const I32 moveX=mouseX-m_lastX;
				const I32 moveY=mouseY-m_lastY;

				const Vector2 uv=spImpact.isValid()?
						spImpact->uv(): Vector2(0.0,0.0);

				const BWORD snapUV=(catalog<String>("snapMode")=="uv");

				const SpatialVector delta=snapUV?
						SpatialVector(uv-m_lastUV):
						scaleX*deltaX*moveX+scaleY*deltaY*moveY;

				if(shifted && (buttons&WindowEvent::e_mbMiddle))
				{
					//* TODO smart increment scale

					if(m_areaSelect && m_event.isMouseDrag())
					{
						if(ctrled)
						{
							m_addFalloff+=0.01*moveX;

							if(m_addFalloff<-rFalloff)
							{
								m_addFalloff=-rFalloff;
							}
							else if(m_addFalloff>1.0-rFalloff)
							{
								m_addFalloff=1.0-rFalloff;
							}
						}
						else
						{
							m_addRadius+=0.01*moveX;

							if(m_addRadius<-rPickRadius)
							{
								m_addRadius=-rPickRadius;
							}
						}
					}
				}
				else if(modifyMode=="drag")
				{
					if((buttons&WindowEvent::e_mbLeft) &&
							!shifted && !ctrled)
					{
						if(m_event.isMousePress())
						{
							m_changing="FollicleOp move";
							anticipate(m_changing);
						}
						else if(m_event.isMouseDrag())
						{
							movePicked(delta);
							m_clearOnRelease=TRUE;
							changed=TRUE;
						}
					}

					if(buttons&WindowEvent::e_mbMiddle)
					{
						if(impacted && m_event.isMousePress())
						{
							m_changing="FollicleOp create";
							anticipate(m_changing);

							createFollicle(intersection);
							m_clearOnRelease=TRUE;
							changed=TRUE;
						}
					}
				}
				else if(modifyMode=="paint")
				{
					if(closest>=0)
					{
						const BWORD sample=shifted;
						const BWORD flood=ctrled;

						if(rAction=="replace")
						{
							if(buttons&WindowEvent::e_mbLeft)
							{
								if(sample)
								{
									if(m_event.isMousePress())
									{
										m_changing="FollicleOp sample";
										anticipate(m_changing);
									}
									Vector2 uv;
									SpatialVector sample;
									if(sampleValue(intersection,1,uv,sample))
									{
										m_addReplacement=(sample-rReplacement);
										return;
									}
								}
								else
								{
									if(m_event.isMousePress())
									{
										m_changing="FollicleOp paint";
										anticipate(m_changing);
									}
									paintValue(magnitude*rReplacement,
											rOpacity*mask,flood);
									changed=TRUE;
								}
							}
							else if(buttons&WindowEvent::e_mbMiddle)
							{
								if(m_event.isMousePress())
								{
									m_changing="FollicleOp restore";
									anticipate(m_changing);
								}
								restoreValue(rOpacity*mask,flood);
								changed=TRUE;
							}
						}
						else if(rAction=="smooth")
						{
							if(buttons&WindowEvent::e_mbLeft)
							{
								if(m_event.isMousePress())
								{
									m_changing="FollicleOp smooth";
									anticipate(m_changing);
								}
								smoothValue(rOpacity*mask,flood);
//								convergeValue(rOpacity*mask,flood);
								changed=TRUE;
							}
							else if(buttons&WindowEvent::e_mbMiddle)
							{
								if(m_event.isMousePress())
								{
									m_changing="FollicleOp saturate";
									anticipate(m_changing);
								}
								smoothValue(-rOpacity*mask,flood);
//								divergeValue(rOpacity*mask,flood);
								changed=TRUE;
							}
						}
						else if(rAction=="add")
						{
							if(buttons&WindowEvent::e_mbLeft)
							{
								if(m_event.isMousePress())
								{
									m_changing="FollicleOp add";
									anticipate(m_changing);
								}
								addValue(magnitude*rReplacement,
										rOpacity*mask,flood);
								changed=TRUE;
							}
							else if(buttons&WindowEvent::e_mbMiddle)
							{
								if(m_event.isMousePress())
								{
									m_changing="FollicleOp subtract";
									anticipate(m_changing);
								}
								addValue(-magnitude*rReplacement,
										rOpacity*mask,flood);
								changed=TRUE;
							}
						}
						else if(rAction=="scale")
						{
							if(buttons&WindowEvent::e_mbLeft)
							{
								if(m_event.isMousePress())
								{
									m_changing="FollicleOp scale";
									anticipate(m_changing);
								}
								scaleValue(scalar,rOpacity*mask,flood);
								changed=TRUE;
							}
							else if(buttons&WindowEvent::e_mbMiddle)
							{
								if(scalar>0.0)
								{
									if(m_event.isMousePress())
									{
										m_changing=
												"FollicleOp inverse scale";
										anticipate(m_changing);
									}
									scaleValue(1.0/scalar,rOpacity*mask,flood);
									changed=TRUE;
								}
							}
						}
						else if(rAction=="conform")
						{
							if(buttons&WindowEvent::e_mbLeft)
							{
								if(m_event.isMousePress())
								{
									m_changing="FollicleOp conform";
									anticipate(m_changing);
								}
								conformValue(flood);
								changed=TRUE;
							}
							else if(buttons&WindowEvent::e_mbMiddle)
							{
								if(m_event.isMousePress())
								{
									m_changing="FollicleOp revert paint";
									anticipate(m_changing);
								}

								//* TODO separate revert for drag and paint
								revertValue(flood);
								changed=TRUE;
							}
						}
					}
				}

				m_lastX=mouseX;
				m_lastY=mouseY;
				m_lastUV=uv;
				m_dragging=m_event.isMouseDrag();
			}

			if(m_clearOnRelease &&
					m_event.isMouseRelease(WindowEvent::e_itemAny))
			{
				m_clearOnRelease=FALSE;
				clearPositionalCaches();
			}

			if(updateCenter)
			{
				m_pickPoint=intersection;
				m_pickNormal=hitNormal;
			}

			const Real circleRadius=5.0;
			const SpatialVector circleScale(
					circleRadius,circleRadius,circleRadius);

			SpatialTransform circleTransform;
			setIdentity(circleTransform);

			Vector2 pickerUV(0.0,0.0);

			if(impacted)
			{
				const Real offset=0.01;	//TODO param
				const I32 depth=0;

				const SpatialVector center=m_pickPoint+offset*m_pickNormal;

				const BWORD hazyPicker=
						((buttons&WindowEvent::e_mbLeft) &&
						!shifted && !ctrled &&
						modifyMode=="drag" && m_event.isMouseDrag());
				if(hazyPicker)
				{
					spDrawBrush->pushDrawMode(m_spHazy);
				}

				if(m_areaSelect)
				{
					const Real newPickRadius=rPickRadius+m_addRadius;
					const Real newFalloff=rFalloff+m_addFalloff;

					if(newFalloff>0.0)
					{
						drawPicker(spDrawBrush,center,up,m_pickNormal,
								newPickRadius,depth,
								&foreOrange);
					}

					drawPicker(spDrawBrush,center,up,m_pickNormal,
							newPickRadius*(1.0-0.99*newFalloff),depth,
							&foreYellow);
				}
				else
				{
					drawPicker(spDrawBrush,center,up,m_pickNormal,
							singleRadius,depth,
							&foreYellow);
				}

				if(hazyPicker)
				{
					spDrawBrush->popDrawMode();
				}

				pickerUV=spImpact->uv();
			}

			const BWORD realValue=(m_attributeType=="real");
			const BWORD vectorValue=m_attributeType.match("vector.");

//			spRA=m_spState->getArray("Hide");
//			if(spRA.isValid())
//			{
//				for(int i=0;i<spRA->length();i++)
//				{
//					const I32 elementIndex=m_aIndex(spRA,i);
//
//					const SpatialVector point=m_inputPosition[elementIndex];
//					sp<SurfaceI::ImpactI> spImpact=
//							m_spDriver->nearestPoint(point);
//					if(!spImpact.isValid())
//					{
//						continue;
//					}
//
//					const Vector2 uv=spImpact->uv();
//					const SpatialVector pixel(mapX+mapScale*uv[0],
//							mapY+mapScale*uv[1]);
//
//					spDrawOverlay->drawPoints(&pixel,NULL,1,FALSE,&red);
//				}
//			}

			spRA=m_spState->getArray("Move");
			if(spRA.isValid())
			{
				for(int i=0;i<spRA->length();i++)
				{
					const I32 elementIndex=m_aIndex(spRA,i);
					if(elementIndex<0 || elementIndex>=outputCount)
					{
						continue;
					}

					if(modifyMode=="paint")
					{
						const SpatialVector value=m_aValue(spRA,i);

						Color color(1.0,1.0,1.0);

						if(realValue)
						{
							set(color,value[0],value[0],value[0]);
						}
						else if(vectorValue)
						{
							set(color,value[0],value[1],value[2]);
						}

						m_colorCenter[elementIndex]=color;
					}
				}
			}

			if(m_spOutputAttribute.isValid() && (drawPaint || drawMap))
			{
				const I32 partitionCount=
						partitionDriver? m_spDriver->partitionCount(): 1;

				spDrawBrush->pushDrawMode(m_spSolid);

				if(I32(m_impactArray.size())!=outputCount)
				{
					m_impactArray.resize(outputCount);

					for(I32 elementIndex=0;elementIndex<outputCount;
							elementIndex++)
					{
						const SpatialVector& point=
								m_outputPosition[elementIndex];

						m_impactArray[elementIndex]=
								m_spDriver->nearestPoint(point);
					}
				}

				m_triangleArrays.resize(partitionCount);

				SpatialVector* locationArray=new SpatialVector[outputCount];
				SpatialVector* normalArray=new SpatialVector[outputCount];
				Color* colorArray=new Color[outputCount];
				SpatialVector* pixelArray=new SpatialVector[outputCount];
				SpatialVector* hiddenUvArray=new SpatialVector[outputCount];

				I32* indexArray=new I32[outputCount];

				Array<Vector2> uvArray(outputCount);

				for(I32 partitionIndex=0;partitionIndex<partitionCount;
						partitionIndex++)
				{
					uvArray.resize(outputCount);

					//* TODO use maxUV
					Vector2 minUV;
					Vector2 maxUV;

					U32 visibleCount=0;
					U32 hiddenCount=0;

					for(I32 elementIndex=0;elementIndex<outputCount;
							elementIndex++)
					{
						//* TODO read UV attribute (also update those uvs)

						sp<SurfaceI::ImpactI> spImpact=
								m_impactArray[elementIndex];
						if(!spImpact.isValid())
						{
							continue;
						}

						if(partitionDriver)
						{
							const I32 hitPartitionIndex=
									spImpact->partitionIndex();
							if(hitPartitionIndex!=partitionIndex)
							{
								continue;
							}
						}

						const Vector2 uv=spImpact->uv();

						const BWORD isHidden=
								(hiddenSet.find(elementIndex)!=hiddenSet.end());
						if(isHidden)
						{
							hiddenUvArray[hiddenCount]=uv;
							hiddenCount++;
							continue;
						}

						if(!visibleCount)
						{
							minUV=uv;
							maxUV=uv;
						}
						else
						{
							if(minUV[0]>uv[0])
							{
								minUV[0]=uv[0];
							}
							if(minUV[1]>uv[1])
							{
								minUV[1]=uv[1];
							}
							if(maxUV[0]<uv[0])
							{
								maxUV[0]=uv[0];
							}
							if(maxUV[1]<uv[1])
							{
								maxUV[1]=uv[1];
							}
						}

						Color sampleColor(1.0,1.0,1.0);

						if(realValue)
						{
							const Real value1=
									m_spOutputAttribute->real(elementIndex);
							set(sampleColor,value1,value1,value1);
						}
						else if(vectorValue)
						{
							const SpatialVector value3=
									m_spOutputAttribute->spatialVector(
									elementIndex);
							set(sampleColor,value3[0],value3[1],value3[2]);
						}

						locationArray[visibleCount]=spImpact->intersection();
						normalArray[visibleCount]=spImpact->normal();
						uvArray[visibleCount]=uv;
						colorArray[visibleCount]=sampleColor;
						indexArray[visibleCount]=elementIndex;
						visibleCount++;
					}

					uvArray.resize(visibleCount);

					const BWORD isOverlayPartition=
							(partitionCount<2 || partitionIndex==hitPartition);

					if((drawMap || drawDots) && isOverlayPartition)
					{
						spDrawOverlay->pushDrawMode(m_spLittleDot);

						if(hiddenCount)
						{
							for(U32 hiddenIndex=0;hiddenIndex<hiddenCount;
									hiddenIndex++)
							{
								const Vector2 rUV=hiddenUvArray[hiddenIndex];

								const SpatialVector pixel(
										mapX+mapScale*(rUV[0]-minUV[0]),
										mapY+mapScale*(rUV[1]-minUV[1]));

								pixelArray[hiddenIndex]=pixel;
							}

							if(drawDots)
							{
								spDrawOverlay->pushDrawMode(m_spBigDot);

								spDrawOverlay->drawPoints(pixelArray,NULL,
										hiddenCount,FALSE,&black);

								spDrawOverlay->popDrawMode();

								spDrawOverlay->drawPoints(pixelArray,NULL,
										hiddenCount,FALSE,&red);
							}
						}

						if(visibleCount)
						{
							Color* colorArray=new Color[visibleCount];

							for(U32 visibleIndex=0;visibleIndex<visibleCount;
									visibleIndex++)
							{
								const Vector2 rUV=uvArray[visibleIndex];

								const SpatialVector pixel(
										mapX+mapScale*(rUV[0]-minUV[0]),
										mapY+mapScale*(rUV[1]-minUV[1]));

								pixelArray[visibleIndex]=pixel;

								const I32 elementIndex=indexArray[visibleIndex];

								colorArray[visibleIndex]=
										picked(elementIndex)? yellow:
										(elementIndex<inputCount?
										blue: magenta);
							}

							if(drawDots)
							{
								spDrawOverlay->pushDrawMode(m_spBigDot);

								spDrawOverlay->drawPoints(pixelArray,NULL,
										visibleCount,FALSE,&black);

								spDrawOverlay->popDrawMode();

								spDrawOverlay->drawPoints(pixelArray,NULL,
										visibleCount,TRUE,colorArray);
							}

							delete[] colorArray;
						}

						spDrawOverlay->popDrawMode();
					}

					Array<Vector3i>& rTriangleArray=
							m_triangleArrays[partitionIndex];

					if(!visibleCount || m_spDriverUV.isNull())
					{
						rTriangleArray.clear();
					}
					else if(!rTriangleArray.size())
					{
						//* Delaunay
						PointConnect::solve(uvArray,rTriangleArray);
					}

					const U32 primitiveCount=rTriangleArray.size();

					if(primitiveCount)
					{
						const U32 primitiveCount3=primitiveCount*3;

						SpatialVector* vertexPixelArray=
								new SpatialVector[primitiveCount3];
						SpatialVector* vertexPointArray=
								new SpatialVector[primitiveCount3];
						SpatialVector* vertexNormalArray=
								new SpatialVector[primitiveCount3];
						Color* vertexColorArray=new Color[primitiveCount3];

						for(U32 primitiveIndex=0;primitiveIndex<primitiveCount;
								primitiveIndex++)
						{
							const Vector3i& rTriangle=
									rTriangleArray[primitiveIndex];

							const I32 tri0=rTriangle[0];
							const I32 tri1=rTriangle[1];
							const I32 tri2=rTriangle[2];

							const U32 primitiveIndex3=primitiveIndex*3;

							SpatialVector* vertexPoint=
									&vertexPointArray[primitiveIndex3];
							vertexPoint[0]=locationArray[tri0];
							vertexPoint[1]=locationArray[tri1];
							vertexPoint[2]=locationArray[tri2];

							SpatialVector* vertexNormal=
									&vertexNormalArray[primitiveIndex3];
							vertexNormal[0]=normalArray[tri0];
							vertexNormal[1]=normalArray[tri1];
							vertexNormal[2]=normalArray[tri2];

							Color* vertexColor=
									&vertexColorArray[primitiveIndex3];
							vertexColor[0]=colorArray[tri0];
							vertexColor[1]=colorArray[tri1];
							vertexColor[2]=colorArray[tri2];

							SpatialVector* vertexPixel=
									&vertexPixelArray[primitiveIndex3];
							vertexPixel[0]=pixelArray[tri0];
							vertexPixel[1]=pixelArray[tri1];
							vertexPixel[2]=pixelArray[tri2];
						}

						if(drawPaint)
						{
							spDrawBrush->drawTriangles(vertexPointArray,
									vertexNormalArray,NULL,primitiveCount3,
									DrawI::e_discrete,TRUE,vertexColorArray);
						}

						if((drawMap || drawDots) && isOverlayPartition)
						{
							const String partitionName=
									m_spDriver->partitionName(partitionIndex);

							const U32 margin=2;
							const SpatialVector textPoint(mapX+0.5*mapScale,
									mapY-10);

							drawLabel(spDrawOverlay,textPoint,
								partitionName,TRUE,margin,white,NULL,&black);
						}

						if(drawMap && isOverlayPartition)
						{
							spDrawOverlay->pushDrawMode(m_spBehind);

							spDrawOverlay->drawTriangles(vertexPixelArray,
									NULL,NULL,primitiveCount3,
									DrawI::e_discrete,TRUE,vertexColorArray);

							spDrawOverlay->popDrawMode();

							spDrawOverlay->pushDrawMode(m_spWireframe);

							const SpatialVector pixel(
									mapX+mapScale*(pickerUV[0]-minUV[0]),
									mapY+mapScale*(pickerUV[1]-minUV[1]));

							setTranslation(circleTransform,pixel);
							spDrawOverlay->drawCircle(circleTransform,
									&circleScale,yellow);

							spDrawOverlay->popDrawMode();
						}

						delete[] vertexColorArray;
						delete[] vertexNormalArray;
						delete[] vertexPointArray;
						delete[] vertexPixelArray;
					}
				}

				spDrawBrush->popDrawMode();

				delete[] indexArray;
				delete[] hiddenUvArray;
				delete[] pixelArray;
				delete[] colorArray;
				delete[] normalArray;
				delete[] locationArray;
			}

			if(changed)
			{
				catalog<String>("Brush","cook")="once";
				m_brushed=TRUE;
			}
		}

		const I32 outputCount=m_outputPosition.size();
		if(outputCount)
		{
			if(modifyMode=="drag")
			{
				const I32 pickCount=m_pickArray.size();
				for(I32 pickIndex=0;pickIndex<pickCount;pickIndex++)
				{
					const Pick& rPick=m_pickArray[pickIndex];
					const I32 elementIndex=rPick.m_elementId;
					const Real pointWeight=rPick.m_weight;
					if(elementIndex>=0 && elementIndex<outputCount)
					{
						m_colorCenter[elementIndex]=
								Color(1.0,pointWeight,0.0,1.0);
					}
				}
			}

			sp<RecordArray> spRA=m_spState->getArray("Hide");
			if(spRA.isValid())
			{
				for(int i=0;i<spRA->length();i++)
				{
					const I32 elementIndex=m_aIndex(spRA,i);
					if(elementIndex>=0 && elementIndex<outputCount)
					{
						m_colorRim[elementIndex]=red;
					}
				}
			}

			std::set<I32> movedSet;
			spRA=m_spState->getArray("Move");
			if(spRA.isValid())
			{
				for(int i=0;i<spRA->length();i++)
				{
					movedSet.insert(m_aIndex(spRA,i));
				}
			}

			const U32 margin=2;

			if(drawSquares)
			{
				const I32 rimSize=4*spDrawOverlay->multiplication();
				BWORD bevel=displayPretty;

				for(I32 elementIndex=0;elementIndex<outputCount;elementIndex++)
				{
					const Color& rColorCenter=m_colorCenter[elementIndex];
					const Color& rColorRim=m_colorRim[elementIndex];
					if(rColorRim[3]<=0.0)
					{
						continue;
					}

					const SpatialVector& point=m_outputPosition[elementIndex];

					if(drawPointIndex)
					{
						const Vector2i projected=SpatialVector(0,-8)+
								spView->project(point,ViewI::e_perspective);

						String text;
						text.sPrintf("%d",elementIndex);
						drawLabel(spDrawOverlay,projected,
								text,TRUE,1,white,NULL,&black);
					}

					const BWORD isMoved=
							(movedSet.find(elementIndex)!=movedSet.end());

					if(displayBalanced)
					{
						bevel=(rColorRim[1]>rColorRim[2]);
					}

//					const Color shadow=cyan;
					drawSquare(spDrawOverlay,point,rimSize,bevel,
							rColorCenter,rColorRim,
							NULL, isMoved? &sparkle: NULL);
//							isMoved? &shadow: NULL, NULL);
				}
			}

			SpatialVector textPoint(8,viewHeight-100);
			String text;
			text.sPrintf("%s %s",m_areaSelect? "Area": "Single",
					modifyMode=="drag"? "Drag": "Paint");
			drawLabel(spDrawOverlay,textPoint,
					text,FALSE,margin,white,NULL,&black);

			if(modifyMode=="drag")
			{
//				textPoint[1]-=16;
//				drawLabel(spDrawOverlay,textPoint,"Drag",FALSE,margin,
//						lightYellow,NULL,&black);
			}
			else if(modifyMode=="paint")
			{
				textPoint[1]-=16;
				drawLabel(spDrawOverlay,textPoint,"Replace",FALSE,margin,
						rAction=="replace"? lightYellow: blue,NULL,&black);

				textPoint[1]-=12;
				drawLabel(spDrawOverlay,textPoint,"Smooth",FALSE,margin,
						rAction=="smooth"? lightYellow: blue,NULL,&black);

				textPoint[1]-=12;
				drawLabel(spDrawOverlay,textPoint,"Add",FALSE,margin,
						rAction=="add"? lightYellow: blue,NULL,&black);

				textPoint[1]-=12;
				drawLabel(spDrawOverlay,textPoint,"Scale",FALSE,margin,
						rAction=="scale"? lightYellow: blue,NULL,&black);

				textPoint[1]-=12;
				drawLabel(spDrawOverlay,textPoint,"Conform",FALSE,margin,
						rAction=="conform"? lightYellow: blue,NULL,&black);

				SpatialVector corner[2];
				Color color=rReplacement+m_addReplacement;

				textPoint[1]-=32;
				corner[0]=textPoint;
				corner[1]=textPoint+SpatialVector(24.0,24.0);

				spDrawOverlay->drawRectangles(corner,2,FALSE,&color);

				color=mask;
				corner[0]+=SpatialVector(12.0,-12.0);
				corner[1]+=SpatialVector(12.0,-12.0);

				spDrawOverlay->drawRectangles(corner,2,FALSE,&color);

				color=(rReplacement+m_addReplacement)*mask;
				corner[0]+=SpatialVector(0.0,12.0);
				corner[1]+=SpatialVector(-12.0,0.0);

				spDrawOverlay->drawRectangles(corner,2,FALSE,&color);

				textPoint[1]-=24;

				corner[0]=textPoint;
				corner[1]=textPoint+SpatialVector(36.0,8.0);

				spDrawOverlay->drawRectangles(corner,2,FALSE,&white);

				corner[0]+=SpatialVector(1.0,1.0);
				corner[1]+=SpatialVector(-1.0,-1.0);

				spDrawOverlay->drawRectangles(corner,2,FALSE,&black);

				const Real trueOpacity=fe::minimum(fe::maximum(
						rOpacity+m_addOpacity,Real(0)),Real(1));

				if(rOpacity>0.0)
				{
					corner[1]=textPoint+SpatialVector(trueOpacity*35.0,7.0);

					spDrawOverlay->drawRectangles(corner,2,FALSE,&white);
				}
			}

			if(rShowStash && createFragment && (readStash || fragmented))
			{
				sp<SurfaceAccessibleI> spStashAccessible;

				if(readStash)
				{
					const String stashKey=catalog<String>("stashKey");

					hp<Component>& rhpStashComponent=
							spMasterCatalog->catalog< hp<Component> >(
							"Stash:"+stashKey);

					spStashAccessible=rhpStashComponent;
				}
				else
				{
					spStashAccessible=m_spOutputAccessible;
				}

				if(spStashAccessible.isValid())
				{
					sp<SurfaceAccessorI> spStashVertices;
					sp<SurfaceAccessorI> spStashFragment;
					if(access(spStashVertices,spStashAccessible,
							e_primitive,e_vertices) &&
							access(spStashFragment,spStashAccessible,
							e_primitive,fragmentAttr))
					{
						sp<SurfaceAccessorI> spStashProperties;
						access(spStashProperties,spStashAccessible,
								e_primitive,e_properties);

						spDrawBrush->pushDrawMode(m_spWideWire);

						String lastFragment;
						BWORD drawFragment=FALSE;
						Color fragmentHighlight=black;
						Color fragmentColor=black;

						const I32 primitiveCount=spStashVertices->count();
						for(I32 primitiveIndex=0;primitiveIndex<primitiveCount;
								primitiveIndex++)
						{
							const I32 subCount=
									spStashVertices->subCount(primitiveIndex);

							//* TODO just points
							if(subCount<2)
							{
								continue;
							}

							const String fragment=
									spStashFragment->string(primitiveIndex);

							//* NOTE likely to be a lot of consecutive repeats
							if(fragment!=lastFragment)
							{
								drawFragment=FALSE;

								const I32 outputCount=m_outputPosition.size();
								for(I32 elementIndex=0;elementIndex<outputCount;
										elementIndex++)
								{
									const BWORD fragmentPicked=
											(m_colorCenter[elementIndex][3]>
											0.0);
									if(!fragmentPicked &&
											m_weightArray[elementIndex]<=0.0)
									{
										continue;
									}
									String number;
									number.sPrintf("%d",elementIndex);
									const String versus=fragmentName.replace(
											pattern,number);
									if(fragment==versus)
									{
										fragmentColor=fragmentPicked?
												m_colorCenter[elementIndex]:
												blank;

										//* alpha fade
										fragmentHighlight=fragmentPicked?
												fragmentColor: green;
										fragmentHighlight[3]=
												0.5*m_weightArray[elementIndex];

										drawFragment=TRUE;
										break;
									}
								}

								lastFragment=fragment;
							}

							if(!drawFragment)
							{
								continue;
							}

							const BWORD openCurve=
									(spStashProperties.isValid() &&
									spStashProperties->integer(
									primitiveIndex,e_openCurve));

							SpatialVector* vert=new SpatialVector[subCount+1];
							for(I32 subIndex=0;subIndex<subCount;subIndex++)
							{
								vert[subIndex]=spStashVertices->spatialVector(
										primitiveIndex,subIndex);
							}
							if(!openCurve)
							{
								vert[subCount]=vert[0];
							}

							if(fragmentHighlight[3]>0.0)
							{
								if(subCount==3)
								{
									spDrawBrush->drawTriangles(vert,NULL,NULL,3,
											DrawI::e_discrete,FALSE,
											&fragmentHighlight);
								}
								else if(subCount==4)
								{
									spDrawBrush->drawTriangles(vert,NULL,NULL,3,
											DrawI::e_discrete,FALSE,
											&fragmentHighlight);

									SpatialVector otherVert[3];
									otherVert[0]=vert[0];
									otherVert[1]=vert[2];
									otherVert[2]=vert[3];

									spDrawBrush->drawTriangles(
											otherVert,NULL,NULL,3,
											DrawI::e_discrete,FALSE,
											&fragmentHighlight);
								}
								else
								{
									spDrawBrush->drawLines(vert,NULL,
											openCurve? subCount: subCount+1,
											DrawI::e_strip,FALSE,
											&fragmentHighlight);
								}
							}

							if(fragmentColor[3]>0.0)
							{
								spDrawBrush->pushDrawMode(m_spNarrowWire);
								spDrawBrush->drawLines(vert,NULL,
										openCurve? subCount: subCount+1,
										DrawI::e_strip,FALSE,&fragmentColor);
								spDrawBrush->popDrawMode();
							}

							delete[] vert;
						}
						spDrawBrush->popDrawMode();
					}
				}
			}
		}

		spDrawOverlay->popDrawMode();
		spDrawBrush->popDrawMode();

		return;
	}

	/***
		TODO consider separate hide mode

		available: left middle wheel shift ctrl esc return del

		Drag Mode:

		select/move				Left, Drag
		unselect				Esc or click off object
		also-select				shift-Left
		deselect				ctrl-Left
		toggle-select			shift-ctrl-Left (including hidden)
		drag-select				shift-area + drag
		create					Middle
		hide selection			Del
		revert					ctrl-Del

		Paint Mode:

		rotate operation		Wheel

		replace/restore			Left/Middle
		smooth/saturate
		add/subtract
		scale/invScale

		sample					shift-Left
		flood/altFlood			ctrl-Left/ctrl-Middle

		Any Mode:

		toggle single/area		Enter

		area radius				shift-Middle
		soft falloff			shift-ctrl-Middle
		opacity					shift-Wheel
		fuzzy-select			Noise-Weighting

		soft-select and area-select are dot-aligned

		soft available in area mode
		fuzzy is parameter of soft

		inner box - selected (color by weight)
		outline box - to select
	*/

	catalog<String>("Brush","prompt")="";

	if(showPrompt)
	{
		if(modifyMode=="drag")
		{
			catalog<String>("Brush","prompt")=
					"LMB selects."
					"  shift-LMB multi-selects."
					"  ctrl-LMB deselects."
					"  shift-ctrl-LMB toggles."
					"  drag-LMB moves."
					"  MMB creates."
					"  ESC deselects."
					"  DEL hides."
					"  ctrl-DEL reverts.";
		}
		else if(modifyMode=="paint")
		{
			catalog<String>("Brush","prompt")=
					"LMB paints."
					"  MMB unpaints."
					"  ctrl-MB floods."
					"  WHEEL picks action."
					"  shift-LMB samples.";
		}

		catalog<String>("Brush","prompt")+=
				"  Enter toggles area select."
				"  shift-MMB resizes."
				"  ctrl-shift-MMB sets falloff."
				"  shift-WHEEL sets opacity."
				"  shift-Enter toggles highlight.";
	}

	if(m_addShowStash)
	{
		rShowStash=!rShowStash;
	}

	rReplacement+=m_addReplacement;
	rPickRadius+=m_addRadius;
	rFalloff+=m_addFalloff;
	rOpacity+=m_addOpacity;

	if(rOpacity>1.0)
	{
		rOpacity=1.0;
	}
	else if(rOpacity<0.0)
	{
		rOpacity=0.0;
	}

	m_addShowStash=0;
	set(m_addReplacement);
	m_addRadius=0.0;
	m_addFalloff=0.0;
	m_addOpacity=0.0;

	updateAction(rAction);
	m_addWheel=0;
	m_addOpacity=0;

	String& rSummary=catalog<String>("summary");
	rSummary="";

	sp<SurfaceAccessibleI> spInputAccessible;
	if(!access(spInputAccessible,"Input Surface")) return;

	const I32 pointCount=spInputAccessible->count(
			SurfaceAccessibleI::e_point);
	const I32 primitiveCount=spInputAccessible->count(
			SurfaceAccessibleI::e_primitive);
	if(pointMode && primitiveCount)
	{
		catalog<String>("warning")+=
				"input primitives not expected when affecting points;";
	}
	else if(!pointMode && !primitiveCount)
	{
		catalog<String>("error")+=
				"input primitives are required when affecting primitives;";
		return;
	}

	catalog<String>("summary")=affectAttribute? attribute: "";

	if(!accessOutput(m_spOutputAccessible,a_rSignal)) return;

	const BWORD paramChanged=!m_brushed;
	m_brushed=FALSE;

	const BWORD rebuild=(paramChanged ||
			catalog<bool>("Output Surface","replaced"));

	//* TODO recycle in pointMode
	BWORD duplicate=(pointMode && !rebuild);

	I32& rResetAll=catalog<I32>("resetAll");
	if(rResetAll)
	{
		anticipate("FollicleOp reset");

		m_spState=new RecordGroup();
		m_spEditMap=m_spState->lookupMap(m_aIndex);
		clearPositionalCaches();
		m_inputPosition.clear();
		m_inputValue.clear();
		m_pickArray.clear();
//		catalog<String>("Brush","cook")="once";
		dirty(FALSE);
		duplicate=TRUE;

		rResetAll=0;
		resolve();
	}

	if(duplicate)
	{
		//* force copy
		m_spOutputAccessible->copy(spInputAccessible);
	}

	sp<SurfaceAccessorI> spOutputPoint;
	if(!access(spOutputPoint,m_spOutputAccessible,e_point,e_position)) return;

	const BWORD affectUV=catalog<bool>("affectUV");
	const BWORD customUV=catalog<bool>("customUV");
	const String uvAttribute=catalog<String>("uvAttribute");

	m_spOutputUV=NULL;
	if(affectUV)
	{
		if(customUV && !uvAttribute.empty())
		{
			access(m_spOutputUV,m_spOutputAccessible,
					pointMode? e_point: e_primitive,uvAttribute,
					e_warning,e_refuseMissing);
		}
		else
		{
			access(m_spOutputUV,m_spOutputAccessible,
					pointMode? e_point: e_primitive,e_uv,
					e_warning,e_refuseMissing);
		}
	}

	sp<SurfaceAccessorI> spOutputFragment;
	if(createFragment && !fragmentAttr.empty())
	{
		access(spOutputFragment,m_spOutputAccessible,
				pointMode? e_point: e_primitive,fragmentAttr);
	}

	m_attributeType="";
	m_spInputAttribute=NULL;
	m_spOutputAttribute=NULL;
	if(affectAttribute && !attribute.empty())
	{
		if(!access(m_spInputAttribute,spInputAccessible,
				pointMode? e_point: e_primitive,attribute))
		{
			catalog<String>("warning")+=
					"missing point attribute "+attribute+";";
		}
		if(m_spInputAttribute.isValid())
		{
			m_attributeType=m_spInputAttribute->type();
		}

		access(m_spOutputAttribute,m_spOutputAccessible,
				pointMode? e_point: e_primitive,attribute);
	}

	const BWORD realValue=(m_attributeType=="real");
	const BWORD vectorValue=m_attributeType.match("vector.");

	const BWORD inputReplaced=catalog<bool>("Input Surface","replaced");

	const BWORD resetInput=(paramChanged || inputReplaced);
	if(resetInput)
	{
		if(!access(m_spPoints,"Input Surface")) return;

		clearPositionalCaches();
		m_inputPosition.clear();
		m_inputValue.clear();

		if(pointMode)
		{
			if(!access(m_spInputElement,spInputAccessible,
					e_point,e_position)) return;
		}
		else
		{
			if(!access(m_spInputElement,spInputAccessible,
					e_primitive,e_vertices)) return;
			if(fragmented)
			{
				const String inputGroup;
				m_spInputElement->fragmentWith(SurfaceAccessibleI::e_primitive,
						inputFragmentAttr,inputGroup);
			}
		}
	}

	sp<SurfaceAccessorI> spInputPoint;

	I32 fragmentCount=1;
	if(pointMode)
	{
		rSummary.sPrintf("%d points",pointCount);
	}
	else
	{
		if(fragmented)
		{
			fragmentCount=m_spInputElement->fragmentCount();
			rSummary.sPrintf("%d fragments by %s",
					fragmentCount,inputFragmentAttr.c_str());
		}
		else
		{
			rSummary.sPrintf("%d primitives",primitiveCount);
		}

		if(!access(spInputPoint,spInputAccessible,e_point,e_position)) return;
	}

	if(affectAttribute && !attribute.empty())
	{
		rSummary+=" for "+attribute;
	}

	const String pickMode=catalog<String>("pickMode");
	if(pickMode!="geodesic")
	{
		m_spDriverGauge=NULL;
	}

	if(paramChanged || catalog<bool>("Driver Surface","replaced"))
	{
		if(!access(m_spDriver,"Driver Surface")) return;

		if(partitionDriver)
		{
			m_spDriver->partitionWith(driverPartitionAttr);
			m_spDriver->setPartitionFilter(".*");
		}

		if(pickMode=="geodesic")
		{
			sp<SurfaceAccessibleI> spDriverAccessible;
			if(!access(spDriverAccessible,"Driver Surface")) return;

			sp<SurfaceTrianglesAccessible> spDriverGeodesic=
					registry()->create("*.SurfaceTrianglesAccessible");
			spDriverGeodesic->bind(spDriverAccessible);

			m_spDriverGauge=spDriverGeodesic->gauge();
		}
	}

	sp<SurfaceAccessorI> spDriverNormal;
	if(!access(spDriverNormal,"Driver Surface",e_point,e_normal,e_quiet) &&
			!access(spDriverNormal,"Driver Surface",e_vertex,e_normal,e_quiet))
	{
		catalog<String>("warning")+=
				"driver needs point or vertex normals;";
	}

	m_spDriverUV=NULL;
	if(!access(m_spDriverUV,"Driver Surface",e_point,e_uv,e_quiet) &&
			!access(m_spDriverUV,"Driver Surface",e_vertex,e_uv,e_quiet))
	{
		catalog<String>("warning")+=
				"cannot triangulate without point or vertex uvs on driver;";
	}

	I32 elementCount=m_inputPosition.size();
	BWORD copyCache=(I32(m_outputPosition.size())<elementCount);

	if(!elementCount || (m_spInputAttribute.isValid() &&
			I32(m_inputValue.size())<elementCount))
	{
		elementCount=fragmented? fragmentCount: m_spInputElement->count();
		m_inputPosition.resize(elementCount);
		m_inputValue.resize(m_spInputAttribute.isValid()? elementCount: 0);

		I32 totalCount=0;

		if(pointMode)
		{
			m_elementOfPoint.clear();
			for(I32 elementIndex=0;elementIndex<elementCount;elementIndex++)
			{
				m_inputPosition[elementIndex]=
						m_spInputElement->spatialVector(elementIndex);
			}
			if(m_spInputAttribute.isValid())
			{
				for(I32 elementIndex=0;elementIndex<elementCount;elementIndex++)
				{
					m_inputValue[elementIndex]=
							m_spInputAttribute->spatialVector(elementIndex);
				}
			}
			totalCount=elementCount;
		}
		else
		{
			m_elementTransformed.resize(elementCount);
			m_elementDirty.resize(elementCount);
			m_elementTransform.resize(elementCount);

			m_elementOfPoint.resize(pointCount);

			sp<SurfaceAccessibleI::FilterI> spFilter;
			for(I32 elementIndex=0;elementIndex<elementCount;elementIndex++)
			{
				I32 filterCount=1;
				if(fragmented)
				{
					const String fragment=
							m_spInputElement->fragment(elementIndex);
					if(!m_spInputElement->filterWith(fragment,spFilter) ||
							spFilter.isNull())
					{
						feLog("FollicleOp::handle"
								" fragment %d/%d \"%s\" no filter\n",
								elementIndex,elementCount,fragment.c_str());
						continue;
					}

					filterCount=spFilter->filterCount();
					if(!filterCount)
					{
						feLog("FollicleOp::handle"
								" fragment %d/%d \"%s\" zero filter count\n",
								elementIndex,elementCount,fragment.c_str());
						continue;
					}
				}

				SpatialVector sum(0.0,0.0,0.0);
				I32 total=0;

				SpatialVector value(0.0,0.0,0.0);

				const String proxyMode=catalog<String>("proxyMode");
				const BWORD lowestMode=(proxyMode=="lowest");

				SpatialVector closest;
				Real closestDistance= -1.0;

				for(I32 filterIndex=0;filterIndex<filterCount;filterIndex++)
				{
					const I32 reIndex=fragmented?
							spFilter->filter(filterIndex): elementIndex;

					const I32 subCount=
							m_spInputElement->subCount(reIndex);
					for(I32 subIndex=0;subIndex<subCount;subIndex++)
					{
						const I32 pointIndex=m_spInputElement->integer(
								reIndex,subIndex);
						FEASSERT(pointIndex<pointCount);

						m_elementOfPoint[pointIndex]=elementIndex;

						const SpatialVector point=
								m_spInputElement->spatialVector(
								reIndex,subIndex);

						sum+=point;
						total++;

						if(lowestMode)
						{
							const Real distance=Real(pointIndex);
							if(closestDistance<0.0 ||
									closestDistance>distance)
							{
								closest=point;
								closestDistance=distance;
							}
						}
						else if(m_spDriver.isValid())
						{
							sp<SurfaceI::ImpactI> spImpact=
									m_spDriver->nearestPoint(point);
							if(spImpact.isValid())
							{
								const Real distance=spImpact->distance();
								if(closestDistance<0.0 ||
										closestDistance>distance)
								{
									closest=spImpact->intersection();
									closestDistance=distance;
								}
							}
						}
					}

					//* NOTE first value in fragment
					if(!filterIndex && m_spInputAttribute.isValid())
					{
						if(realValue)
						{
							const Real value1=
									m_spInputAttribute->real(reIndex);
							set(value,value1,value1,value1);
						}
						else if(vectorValue)
						{
							value=m_spInputAttribute->spatialVector(reIndex);
						}
					}
				}

				if(closestDistance>=0.0)
				{
					if(lowestMode && m_spDriver.isValid())
					{
						sp<SurfaceI::ImpactI> spImpact=
								m_spDriver->nearestPoint(closest);
						if(spImpact.isValid())
						{
							closest=spImpact->intersection();
						}
					}
					m_inputPosition[totalCount]=closest;
				}
				else if(total)
				{
					m_inputPosition[totalCount]=sum*(1.0/Real(total));
				}
				else
				{
					continue;
				}

				if(m_spInputAttribute.isValid())
				{
					m_inputValue[totalCount]=value;
				}

				totalCount++;
			}
		}

		elementCount=totalCount;
		m_inputPosition.resize(elementCount);
		if(m_spInputAttribute.isValid())
		{
			m_inputValue.resize(elementCount);
		}
		copyCache=TRUE;
	}

	if(resetInput)
	{
		syncToInput();
	}

	m_outputPosition.resize(elementCount);

	if(copyCache)
	{
		for(I32 elementIndex=0;elementIndex<elementCount;elementIndex++)
		{
			m_outputPosition[elementIndex]=m_inputPosition[elementIndex];
		}
	}

	if(!pointMode)
	{
		for(I32 elementIndex=0;elementIndex<elementCount;elementIndex++)
		{
			m_elementTransformed[elementIndex]=FALSE;
		}
	}

	const String hideGroup=catalog<String>("hideGroup");
	if(!hideGroup.empty())
	{
		if(!accessOutput(m_spHideGroup,a_rSignal,
				e_pointGroup,hideGroup)) return;

		const I32 outputCount=m_outputPosition.size();

		sp<RecordArray> spRA=m_spState->getArray("Hide");
		if(spRA.isValid())
		{
			for(int i=0;i<spRA->length();i++)
			{
				const I32 elementIndex=m_aIndex(spRA,i);
				if(elementIndex>=0 && elementIndex<outputCount)
				{
					m_spHideGroup->append(elementIndex);
				}
			}
		}
	}

	sp<RecordArray> spRA=m_spState->getArray("Move");
	if(spRA.isValid())
	{
#if FE_FCO_OUTPUT_DEBUG
		feLog("FollicleOp::handle move count %d\n",spRA->length());
#endif
		for(int i=0;i<spRA->length();i++)
		{
			const I32 elementIndex=m_aIndex(spRA,i);

			if(elementIndex<0 || elementIndex>=elementCount)
			{
				continue;
			}

			Record edit=spRA->getRecord(i);

#if FE_FCO_OUTPUT_DEBUG
			feLog("FollicleOp::handle move %d/%d prim %d/%d\n",
					i,spRA->length(),elementIndex,m_outputPosition.size());
#endif

			const SpatialVector inputPoint=m_inputPosition[elementIndex];
			const SpatialVector original=inputValue(elementIndex);

			if(magnitudeSquared(m_aLocation(edit)-inputPoint)<1e-6 &&
					magnitudeSquared(m_aValue(edit)-original)<1e-6)
			{
				//* remove non-contributing record
				spRA->remove(i);
				i--;
				continue;
			}

			const SpatialVector point=m_aLocation(edit);
			m_outputPosition[elementIndex]=point;

			if(pointMode)
			{
				spOutputPoint->set(elementIndex,point);
			}
			else
			{
				SpatialTransform& rTransform=m_elementTransform[elementIndex];
				const SpatialVector& outputPoint=m_aLocation(edit);

				if(m_spDriver.isValid())
				{
					sp<SurfaceI::ImpactI> spImpactInput=
							m_spDriver->nearestPoint(inputPoint);
					if(spImpactInput.isNull())
					{
						continue;
					}

					sp<SurfaceI::ImpactI> spImpactOutput=
							m_spDriver->nearestPoint(outputPoint);
					if(spImpactOutput.isNull())
					{
						continue;
					}

					const I32 inputFace=spImpactInput->triangleIndex();
					const SpatialBary inputBary=spImpactInput->barycenter();
					const SpatialTransform xformInput=
							m_spDriver->sample(inputFace,inputBary);

					const I32 outputFace=spImpactOutput->triangleIndex();
					const SpatialBary outputBary=spImpactOutput->barycenter();
					const SpatialTransform xformOutput=
							m_spDriver->sample(outputFace,outputBary);

					SpatialTransform invInput;
					invert(invInput,xformInput);
					rTransform=invInput*xformOutput;

					m_elementTransformed[elementIndex]=TRUE;
				}
				else
				{
					//* NOTE translation only
					const SpatialVector translation=outputPoint-inputPoint;
					setIdentity(rTransform);
					translate(rTransform,translation);
					m_elementTransformed[elementIndex]=TRUE;
				}
			}
			if(m_spOutputUV.isValid())
			{
				m_spOutputUV->set(elementIndex,m_aUV(edit));
			}
			if(m_spOutputAttribute.isValid())
			{
				if(realValue)
				{
					m_spOutputAttribute->set(elementIndex,m_aValue(edit)[0]);
				}
				else if(vectorValue)
				{
					m_spOutputAttribute->set(elementIndex,m_aValue(edit));
				}
			}
		}
	}

	if(!pointMode)
	{
		SpatialVector transformed;

		for(I32 pointIndex=0;pointIndex<pointCount;pointIndex++)
		{
			const I32 elementIndex=m_elementOfPoint[pointIndex];
			if(m_elementTransformed[elementIndex] &&
					(rebuild || m_elementDirty[elementIndex]))
			{
				transformVector(m_elementTransform[elementIndex],
						spInputPoint->spatialVector(pointIndex),
						transformed);
				spOutputPoint->set(pointIndex,transformed);
			}
		}

		for(I32 elementIndex=0;elementIndex<elementCount;elementIndex++)
		{
			m_elementDirty[elementIndex]=FALSE;
		}
	}

	if(!accessDraw(m_spOutputDraw,a_rSignal)) return;

	if(pointMode)
	{
		spRA=m_spState->getArray("Create");
		if(spRA.isValid())
		{
#if FE_FCO_OUTPUT_DEBUG
			feLog("FollicleOp::handle append count %d\n",spRA->length());
#endif

			for(int i=0;i<spRA->length();i++)
			{
				Record edit=spRA->getRecord(i);

#if FE_FCO_OUTPUT_DEBUG
				const I32 elementIndex=m_aIndex(spRA,i);
				feLog("FollicleOp::handle append %d/%d point %d/%d\n",
						i,spRA->length(),elementIndex,m_outputPosition.size());
#endif

				appendPoint(edit);
			}
		}
	}

	if(spOutputFragment.isValid())
	{
		if(pointMode)
		{
			const I32 outputPointCount=spOutputFragment->count();
			for(I32 pointIndex=0;pointIndex<outputPointCount;pointIndex++)
			{
				String number;
				number.sPrintf("%d",pointIndex);
				const String fragment=fragmentName.replace(pattern,number);

				spOutputFragment->set(pointIndex,fragment);
			}
		}
		else if(rebuild)
		{
			sp<SurfaceAccessibleI::FilterI> spFilter;
			for(I32 elementIndex=0;elementIndex<elementCount;elementIndex++)
			{
				I32 filterCount=1;
				if(fragmented)
				{
					const String fragment=
							m_spInputElement->fragment(elementIndex);
					if(!m_spInputElement->filterWith(fragment,spFilter) ||
							spFilter.isNull())
					{
						continue;
					}

					filterCount=spFilter->filterCount();
					if(!filterCount)
					{
						continue;
					}
				}

				String number;
				number.sPrintf("%d",elementIndex);
				const String fragment=fragmentName.replace(pattern,number);

				for(I32 filterIndex=0;filterIndex<filterCount;filterIndex++)
				{
					const I32 reIndex=fragmented?
							spFilter->filter(filterIndex): elementIndex;

					spOutputFragment->set(reIndex,fragment);
				}
			}
		}
	}

	//* NOTE ensure stash is not stale
	if(clearStash)
	{
		const String stashKey=catalog<String>("stashKey");

		hp<Component>& rhpStashComponent=
				spMasterCatalog->catalog< hp<Component> >(
				"Stash:"+stashKey);

		rhpStashComponent=NULL;
	}

	return;
}

void FollicleOp::appendPoint(Record a_edit)
{
	const String inputMode=catalog<String>("inputMode");
	if(inputMode!="point")
	{
		return;
	}

	const SpatialVector point=m_aLocation(a_edit);

	//* add point to output
	m_spOutputDraw->drawPoints(&point,NULL,1,FALSE,NULL);

	const I32 elementIndex=m_aIndex(a_edit);

	m_outputPosition.push_back(point);

	if(m_spOutputUV.isValid())
	{
		m_spOutputUV->set(elementIndex,m_aUV(a_edit));
	}

	if(m_spOutputAttribute.isValid())
	{
		const BWORD realValue=(m_attributeType=="real");
		const BWORD vectorValue=m_attributeType.match("vector.");

		if(realValue)
		{
			m_spOutputAttribute->set(elementIndex,m_aValue(a_edit)[0]);
		}
		else if(vectorValue)
		{
			m_spOutputAttribute->set(elementIndex,m_aValue(a_edit));
		}
	}
}

BWORD FollicleOp::undo(String a_label,sp<Counted> a_spCounted)
{
	if(!OperatorState::undo(a_label,a_spCounted))
	{
		return FALSE;
	}

	m_spEditMap=m_spState->lookupMap(m_aIndex);

	//* NOTE doesn't seem to help
	dirty(TRUE);

	return TRUE;
}

BWORD FollicleOp::redo(String a_label,sp<Counted> a_spCounted)
{
	if(!OperatorState::redo(a_label,a_spCounted))
	{
		return FALSE;
	}

	m_spEditMap=m_spState->lookupMap(m_aIndex);

	//* NOTE doesn't seem to help
	dirty(TRUE);

	return TRUE;
}

//* TODO selective clear
void FollicleOp::clearPositionalCaches(void)
{
	m_facingArray.clear();
	m_neighborTable.clear();
	m_impactArray.clear();
	m_triangleArrays.clear();
}

void FollicleOp::updateAction(String& a_rAction)
{
	const String modifyMode=catalog<String>("modifyMode");

	if(m_addWheel && modifyMode=="paint")
	{
		I32 intAction=0;	//* "replace"
		if(a_rAction=="smooth")
		{
			intAction=1;
		}
		if(a_rAction=="add")
		{
			intAction=2;
		}
		if(a_rAction=="scale")
		{
			intAction=3;
		}
		if(a_rAction=="conform")
		{
			intAction=4;
		}

		intAction=fe::minimum(fe::maximum(intAction-m_addWheel,I32(0)),I32(4));

		switch(intAction)
		{
			case 0:
				a_rAction="replace";
				break;
			case 1:
				a_rAction="smooth";
				break;
			case 2:
				a_rAction="add";
				break;
			case 3:
				a_rAction="scale";
				break;
			case 4:
				a_rAction="conform";
				break;
		}
	}
}

BWORD FollicleOp::revert(I32 a_index)
{
	BWORD result=FALSE;
	if(hidden(a_index))
	{
		hideToggle(a_index);
		result=TRUE;
	}
	sp<RecordArray> spRA=m_spState->getArray("Move");
	if(spRA.isValid())
	{
		for(int i=0;i<spRA->length();i++)
		{
			const I32 elementIndex=m_aIndex(spRA,i);
			if(a_index==elementIndex)
			{
				spRA->remove(i);
				result=TRUE;
				break;
			}
		}
	}

	if(result)
	{
		clearPositionalCaches();
		m_outputPosition[a_index]=m_inputPosition[a_index];
	}

	return result;
}

void FollicleOp::setValueLimited(Record a_edit,SpatialVector a_value)
{
	//* TODO param min,max

	for(I32 m=0;m<3;m++)
	{
		if(a_value[m]>1.0)
		{
			a_value[m]=1.0;
		}
		if(a_value[m]<0.0)
		{
			a_value[m]=0.0;
		}
	}

	m_aValue(a_edit)=a_value;
}

void FollicleOp::addValue(SpatialVector a_increase,
	SpatialVector a_mask,BWORD a_flood)
{
	const I32 outputCount=m_outputPosition.size();
	for(I32 elementIndex=0;elementIndex<outputCount;elementIndex++)
	{
		const Real weight=a_flood? 1.0: m_weightArray[elementIndex];
		if(weight<=0.0)
		{
			continue;
		}

		Record edit=getEdit(elementIndex);
		if(!m_aValue.check(edit))
		{
			continue;
		}

		const SpatialVector weightedMask=weight*a_mask;

		const SpatialVector oldValue=m_aValue(edit);

		const SpatialVector newValue(
				oldValue[0]+weightedMask[0]*a_increase[0],
				oldValue[1]+weightedMask[1]*a_increase[1],
				oldValue[2]+weightedMask[2]*a_increase[2]);

		setValueLimited(edit,newValue);
	}
}

void FollicleOp::scaleValue(Real a_scale,SpatialVector a_mask,BWORD a_flood)
{
	const I32 outputCount=m_outputPosition.size();
	for(I32 elementIndex=0;elementIndex<outputCount;elementIndex++)
	{
		const Real weight=a_flood? 1.0: m_weightArray[elementIndex];
		if(weight<=0.0)
		{
			continue;
		}

		Record edit=getEdit(elementIndex);
		if(!m_aValue.check(edit))
		{
			continue;
		}

		const SpatialVector weightedMask=weight*a_mask;

		const SpatialVector oldValue=m_aValue(edit);

		const SpatialVector newValue(
				oldValue[0]*(1.0+(a_scale-1.0)*weightedMask[0]),
				oldValue[1]*(1.0+(a_scale-1.0)*weightedMask[1]),
				oldValue[2]*(1.0+(a_scale-1.0)*weightedMask[2]));

		setValueLimited(edit,newValue);
	}
}

void FollicleOp::paintValue(SpatialVector a_replacement,
		SpatialVector a_mask,BWORD a_flood)
{
	const I32 outputCount=m_outputPosition.size();
	for(I32 elementIndex=0;elementIndex<outputCount;elementIndex++)
	{
		const Real weight=a_flood? 1.0: m_weightArray[elementIndex];
		if(weight<=0.0)
		{
			continue;
		}

		Record edit=getEdit(elementIndex);
		if(!m_aValue.check(edit))
		{
			continue;
		}

		const SpatialVector weightedMask=weight*a_mask;

		const SpatialVector oldValue=m_aValue(edit);

		const SpatialVector newValue(
				(1.0-weightedMask[0])*oldValue[0]+
				weightedMask[0]*a_replacement[0],
				(1.0-weightedMask[1])*oldValue[1]+
				weightedMask[1]*a_replacement[1],
				(1.0-weightedMask[2])*oldValue[2]+
				weightedMask[2]*a_replacement[2]);

		setValueLimited(edit,newValue);
	}
}

void FollicleOp::restoreValue(SpatialVector a_mask,BWORD a_flood)
{
	const I32 outputCount=m_outputPosition.size();
	for(I32 elementIndex=0;elementIndex<outputCount;elementIndex++)
	{
		const Real weight=a_flood? 1.0: m_weightArray[elementIndex];
		if(weight<=0.0)
		{
			continue;
		}

		Record edit=m_spEditMap->lookup(elementIndex);
		if(!edit.isValid() || !m_aValue.check(edit))
		{
			continue;
		}

		const SpatialVector weightedMask=weight*a_mask;

		const SpatialVector oldValue=m_aValue(edit);

		//* TODO decide how to restore a created follicle
		const SpatialVector origValue=inputValue(elementIndex);

		const SpatialVector newValue(
				(1.0-weightedMask[0])*oldValue[0]+
				weightedMask[0]*origValue[0],
				(1.0-weightedMask[1])*oldValue[1]+
				weightedMask[1]*origValue[1],
				(1.0-weightedMask[2])*oldValue[2]+
				weightedMask[2]*origValue[2]);

		setValueLimited(edit,newValue);
	}
}

BWORD FollicleOp::calcWeightedAverage(SpatialVector& rAverage,BWORD a_flood)
{
	const I32 outputCount=m_outputPosition.size();

	SpatialVector sum(0.0,0.0,0.0);
	Real totalWeight=0.0;
	for(I32 elementIndex=0;elementIndex<outputCount;elementIndex++)
	{
		const Real weight=a_flood? 1.0: m_weightArray[elementIndex];
		if(weight<=0.0)
		{
			continue;
		}

		Record edit=getEdit(elementIndex);
		if(!m_aValue.check(edit))
		{
			continue;
		}

		sum+=m_aValue(edit)*weight;
		totalWeight+=weight;
	}
	if(totalWeight<=1e-6)
	{
		set(rAverage);
		return FALSE;
	}

	rAverage=sum/totalWeight;
	return TRUE;
}

BWORD FollicleOp::calcLimits(SpatialVector& rMinimum,
		SpatialVector& rMaximum,BWORD a_flood)
{
	const I32 outputCount=m_outputPosition.size();

	BWORD found=FALSE;
	for(I32 elementIndex=0;elementIndex<outputCount;elementIndex++)
	{
		const Real weight=a_flood? 1.0: m_weightArray[elementIndex];
		if(weight<=0.0)
		{
			continue;
		}

		Record edit=getEdit(elementIndex);
		if(!m_aValue.check(edit))
		{
			continue;
		}

		const SpatialVector value=m_aValue(edit);

		if(!found)
		{
			rMinimum=value;
			rMaximum=value;
			found=TRUE;
			continue;
		}

		for(I32 m=0;m<3;m++)
		{
			if(rMinimum[m]>value[m])
			{
				rMinimum[m]=value[m];
			}
			if(rMaximum[m]<value[m])
			{
				rMaximum[m]=value[m];
			}
		}
	}

	return found;
}

void FollicleOp::updateNeighbors(void)
{
	const I32 outputCount=m_outputPosition.size();

	m_neighborTable.clear();
	m_neighborTable.resize(outputCount);

	for(I32 elementIndex=0;elementIndex<outputCount;elementIndex++)
	{
		Array<I32>& rNeighborArray=m_neighborTable[elementIndex];

		if(m_spOutputUV.isNull())
		{
//			feLog("%d/%d no uv\n",elementIndex,outputCount);
			continue;
		}

		const Vector2 uv=m_spOutputUV->spatialVector(elementIndex);

//		feLog("%d/%d uv %s\n",elementIndex,outputCount,c_print(uv));

		const Real maxDistance= -1.0;
		const I32 hitLimit=4;
		Array< sp<SurfaceI::ImpactI> > hitArray=
				m_spPoints->nearestPoints(uv,maxDistance,hitLimit);

		const U32 hitCount=hitArray.size();
		rNeighborArray.resize(hitCount);

		for(U32 impactIndex=0;impactIndex<hitCount;impactIndex++)
		{
			sp<SurfaceI::ImpactI> spImpact=hitArray[impactIndex];
			const I32 primitiveIndex=spImpact->primitiveIndex();

			rNeighborArray[impactIndex]=primitiveIndex;

//			feLog("%d/%d neighbor %d/%d %d uv %s vs %s\n",
//					elementIndex,outputCount,
//					impactIndex,hitCount,primitiveIndex,
//					c_print(spImpact->uv()),c_print(uv));
		}
	}
}

void FollicleOp::smoothValue(SpatialVector a_mask,BWORD a_flood)
{
	if(!m_neighborTable.size())
	{
		updateNeighbors();
	}

	const BWORD realValue=(m_attributeType=="real");
	const BWORD vectorValue=m_attributeType.match("vector.");

	const I32 outputCount=m_outputPosition.size();

	SpatialVector* existingValue=new SpatialVector[outputCount];

	for(I32 elementIndex=0;elementIndex<outputCount;elementIndex++)
	{
		Record edit=m_spEditMap->lookup(elementIndex);
		if(edit.isValid() && m_aValue.check(edit))
		{
			existingValue[elementIndex]=m_aValue(edit);
			continue;
		}

		if(realValue)
		{
			const Real value1=
					m_spOutputAttribute->real(elementIndex);
			set(existingValue[elementIndex],value1,value1,value1);
		}
		else if(vectorValue)
		{
			existingValue[elementIndex]=
					m_spOutputAttribute->spatialVector(elementIndex);
		}
	}

	for(I32 elementIndex=0;elementIndex<outputCount;elementIndex++)
	{
		const Real weight=a_flood? 1.0: m_weightArray[elementIndex];
		if(weight<=0.0)
		{
			continue;
		}

		const SpatialVector weightedMask=weight*a_mask;

		const SpatialVector oldValue=existingValue[elementIndex];

		SpatialVector sum=oldValue;
		I32 total=1;

		//* TODO distance weighted

		Array<I32>& rNeighborArray=m_neighborTable[elementIndex];
		const I32 neighborCount=rNeighborArray.size();
		for(I32 neighborIndex=0;neighborIndex<neighborCount;neighborIndex++)
		{

			const I32 otherIndex=rNeighborArray[neighborIndex];
			sum+=existingValue[otherIndex];
			total++;

//			feLog("%d other %d/%d %d sum %s total %d\n",
//					elementIndex,neighborIndex,neighborCount,otherIndex,
//					c_print(sum),total);
		}

		const SpatialVector average=sum/Real(total);

		const SpatialVector newValue(
				(1.0-weightedMask[0])*oldValue[0]+
				weightedMask[0]*average[0],
				(1.0-weightedMask[1])*oldValue[1]+
				weightedMask[1]*average[1],
				(1.0-weightedMask[2])*oldValue[2]+
				weightedMask[2]*average[2]);

		//* TODO param
		if(magnitudeSquared(newValue-oldValue)<1e-6)
		{
			continue;
		}

		Record edit=getEdit(elementIndex);
		if(!m_aValue.check(edit))
		{
			continue;
		}

		setValueLimited(edit,newValue);
	}

	delete[] existingValue;
}

//* TODO flood
void FollicleOp::conformValue(BWORD a_flood)
{
	if(!m_neighborTable.size())
	{
		updateNeighbors();
	}

	const BWORD realValue=(m_attributeType=="real");
	const BWORD vectorValue=m_attributeType.match("vector.");

	const I32 outputCount=m_outputPosition.size();

	BWORD* hadEdit=new BWORD[outputCount];

	SpatialVector* existingValue=new SpatialVector[outputCount];

	for(I32 elementIndex=0;elementIndex<outputCount;elementIndex++)
	{
		Record edit=m_spEditMap->lookup(elementIndex);
		if(edit.isValid() && m_aValue.check(edit))
		{
			hadEdit[elementIndex]=TRUE;
			existingValue[elementIndex]=m_aValue(edit);
			continue;
		}

		hadEdit[elementIndex]=FALSE;

		if(realValue)
		{
			const Real value1=
					m_spOutputAttribute->real(elementIndex);
			set(existingValue[elementIndex],value1,value1,value1);
		}
		else if(vectorValue)
		{
			existingValue[elementIndex]=
					m_spOutputAttribute->spatialVector(elementIndex);
		}
	}

	for(I32 elementIndex=0;elementIndex<outputCount;elementIndex++)
	{
		if(hadEdit[elementIndex])
		{
			continue;
		}

		if(!picked(elementIndex))
		{
			continue;
		}

		const SpatialVector oldValue=existingValue[elementIndex];

		SpatialVector sum=oldValue;
		I32 total=1;

		//* TODO distance weighted

		Array<I32>& rNeighborArray=m_neighborTable[elementIndex];
		const I32 neighborCount=rNeighborArray.size();
		for(I32 neighborIndex=0;neighborIndex<neighborCount;neighborIndex++)
		{
			const I32 otherIndex=rNeighborArray[neighborIndex];

			if(hadEdit[otherIndex])
			{
				sum+=existingValue[otherIndex];
				total++;
			}

//			feLog("%d other %d/%d %d sum %s total %d\n",
//					elementIndex,neighborIndex,neighborCount,otherIndex,
//					c_print(sum),total);
		}

		const SpatialVector average=sum/Real(total);

		//* TODO param
		if(magnitudeSquared(average-oldValue)<1e-6)
		{
			continue;
		}

		Record edit=getEdit(elementIndex);
		if(!m_aValue.check(edit))
		{
			continue;
		}

		setValueLimited(edit,average);
	}


	delete[] hadEdit;
	delete[] existingValue;
}

void FollicleOp::revertValue(BWORD a_flood)
{
	const I32 outputCount=m_outputPosition.size();

	for(I32 elementIndex=0;elementIndex<outputCount;elementIndex++)
	{
		const Real weight=a_flood? 1.0: m_weightArray[elementIndex];
		if(weight<=0.0)
		{
			continue;
		}

		revert(elementIndex);
	}
}

void FollicleOp::convergeValue(SpatialVector a_mask,BWORD a_flood)
{
	SpatialVector average;
	if(!calcWeightedAverage(average,a_flood))
	{
		return;
	}

//	feLog("FollicleOp::convergeValue sum %.6G total %.6G average %.6G\n",
//			sum,totalWeight,average);

//	sum=0.0;

	const I32 outputCount=m_outputPosition.size();
	for(I32 elementIndex=0;elementIndex<outputCount;elementIndex++)
	{
		const Real weight=a_flood? 1.0: m_weightArray[elementIndex];
		if(weight<=0.0)
		{
			continue;
		}

		Record edit=getEdit(elementIndex);
		if(!m_aValue.check(edit))
		{
			continue;
		}

		const SpatialVector weightedMask=weight*a_mask;

		const SpatialVector oldValue=m_aValue(edit);

		const SpatialVector newValue(
				(1.0-weightedMask[0])*oldValue[0]+
				weightedMask[0]*average[0],
				(1.0-weightedMask[1])*oldValue[1]+
				weightedMask[1]*average[1],
				(1.0-weightedMask[2])*oldValue[2]+
				weightedMask[2]*average[2]);

		setValueLimited(edit,newValue);
	}

//	feLog("FollicleOp::convergeValue new sum %.6G mean %.6G\n",
//			sum,sum/totalWeight);
}

void FollicleOp::divergeValue(SpatialVector a_mask,BWORD a_flood)
{
	SpatialVector average;
	if(!calcWeightedAverage(average,a_flood))
	{
		return;
	}

	SpatialVector min;
	SpatialVector max;
	if(!calcLimits(min,max,a_flood))
	{
		return;
	}

	SpatialVector scaleFactor(1.0,1.0,1.0);
	for(I32 m=0;m<3;m++)
	{
		if(average[m]>min[m] && average[m]<max[m])
		{
			scaleFactor[m]=fe::maximum(
					(1.0f-average[m])/(max[m]-average[m]),
					(average[m])/(average[m]-min[m]));
		}
	}

	const I32 outputCount=m_outputPosition.size();
	for(I32 elementIndex=0;elementIndex<outputCount;elementIndex++)
	{
		const Real weight=a_flood? 1.0: m_weightArray[elementIndex];
		if(weight<=0.0)
		{
			continue;
		}

		Record edit=getEdit(elementIndex);
		if(!m_aValue.check(edit))
		{
			continue;
		}

		const SpatialVector weightedMask=weight*a_mask;

		const SpatialVector oldValue=m_aValue(edit);

		SpatialVector newValue;

		for(I32 m=0;m<3;m++)
		{
			newValue[m]=(1.0-weightedMask[m])*oldValue[m]+weightedMask[m]*
					(average[m]+scaleFactor[m]*(oldValue[m]-average[m]));
		}

		setValueLimited(edit,newValue);
	}
}

SpatialVector FollicleOp::inputValue(I32 a_pointIndex)
{
	FEASSERT(a_pointIndex>=0);

	SpatialVector original(0.0,0.0,0.0);

	if(a_pointIndex<I32(m_inputValue.size()))
	{
		original=m_inputValue[a_pointIndex];
	}

	return original;
}

BWORD FollicleOp::hideToggle(I32 a_index)
{
	sp<RecordArray> spRA=m_spState->getArray("Hide");
	if(spRA.isValid())
	{
		for(int i=0;i<spRA->length();i++)
		{
			const I32 elementIndex=m_aIndex(spRA,i);
			if(a_index==elementIndex)
			{
				spRA->remove(i);
				return TRUE;
			}
		}
	}

	hide(a_index);
	return FALSE;
}

void FollicleOp::hide(I32 a_index)
{
	//* remove if point was an add
	sp<RecordArray> spRA=m_spState->getArray("Create");
	if(spRA.isValid())
	{
		for(int i=0;i<spRA->length();i++)
		{
			const I32 elementIndex=m_aIndex(spRA,i);
			if(a_index==elementIndex)
			{
				spRA->remove(i);
//				Record record=spRA->getRecord(i);
//				m_spState->remove(record);
				return;
			}
		}
	}
	//* no change if already hidden
	spRA=m_spState->getArray("Hide");
	if(spRA.isValid())
	{
		for(int i=0;i<spRA->length();i++)
		{
			const I32 elementIndex=m_aIndex(spRA,i);
			if(a_index==elementIndex)
			{
				return;
			}
		}
	}
	Record hide=m_spScope->createRecord("Hide");
	m_aIndex(hide)=a_index;
	m_aSyncLocation(hide)=m_inputPosition[a_index];

	m_spState->add(hide);

	clearPositionalCaches();
}

//* NOTE slow search
BWORD FollicleOp::hidden(I32 a_index)
{
	sp<RecordArray> spRA=m_spState->getArray("Hide");
	if(spRA.isValid())
	{
		for(int i=0;i<spRA->length();i++)
		{
			const I32 elementIndex=m_aIndex(spRA,i);
			if(a_index==elementIndex)
			{
				return TRUE;
			}
		}
	}
	return FALSE;
}

//* NOTE slow search
BWORD FollicleOp::moved(I32 a_index)
{
	sp<RecordArray> spRA=m_spState->getArray("Move");
	if(spRA.isValid())
	{
		for(int i=0;i<spRA->length();i++)
		{
			const I32 elementIndex=m_aIndex(spRA,i);
			if(a_index==elementIndex)
			{
				return TRUE;
			}
		}
	}
	return FALSE;
}

Record FollicleOp::getEdit(I32 a_index)
{
	Record edit=m_spEditMap->lookup(a_index);
	if(!edit.isValid())
	{
		const SpatialVector inputPoint=m_inputPosition[a_index];

		edit=m_spScope->createRecord("Move");
		m_aIndex(edit)=a_index;
		m_aSyncLocation(edit)=inputPoint;
		m_aLocation(edit)=inputPoint;
		m_aValue(edit)=inputValue(a_index);

		sp<SurfaceI::ImpactI> spImpact=m_spDriver->nearestPoint(inputPoint);
		m_aUV(edit)=spImpact.isValid()? spImpact->uv(): Vector2(0.0,0.0);

		m_spState->add(edit);

//		feLog("FollicleOp::getEdit %d created\n",a_index);
	}

	return edit;
}

void FollicleOp::createFollicle(const SpatialVector& a_intersection)
{
	I32 maxIndex=m_outputPosition.size();

	const I32 pickCount=m_pickArray.size();
	for(I32 pickIndex=0;pickIndex<pickCount;pickIndex++)
	{
		const Pick& rPick=m_pickArray[pickIndex];
		const I32 elementIndex=rPick.m_elementId;
		if(maxIndex<elementIndex)
		{
			maxIndex=elementIndex;
		}
	}

	Record edit=m_spScope->createRecord("Create");
	m_aIndex(edit)=maxIndex;
	m_aLocation(edit)=a_intersection;
	set(m_aValue(edit));

	Vector2 uv;
	SpatialVector sample;
	if(!sampleValue(a_intersection,4,uv,sample))
	{
		return;
	}

	m_aUV(edit)=uv;
	setValueLimited(edit,sample);

	m_spState->add(edit);

	//* short term, just for next cache refresh
	appendPoint(edit);

	clearPositionalCaches();
}

BWORD FollicleOp::sampleValue(SpatialVector a_point,I32 a_hitLimit,
		Vector2& a_rUv,SpatialVector& a_rSample)
{
	//* interpolate neighbor values

	Array< sp<SurfaceI::ImpactI> > hitArray;
	const Real maxDistance= -1.0;

	if(m_spDriverUV.isValid())
	{
		sp<SurfaceI::ImpactI> spImpact=m_spDriver->nearestPoint(a_point);
		a_rUv=spImpact.isValid()? spImpact->uv(): Vector2(0.0,0.0);

		hitArray=m_spPoints->nearestPoints(a_rUv,maxDistance,a_hitLimit);
	}
	else
	{
		set(a_rUv);
		hitArray=m_spPoints->nearestPoints(a_point,maxDistance,a_hitLimit);
	}

	const U32 hitCount=hitArray.size();
	if(!hitCount)
	{
		set(a_rSample);
		return FALSE;
	}

	const BWORD realValue=(m_attributeType=="real");
	const BWORD vectorValue=m_attributeType.match("vector.");

	SpatialVector sum(0.0,0.0,0.0);

	for(U32 impactIndex=0;impactIndex<hitCount;impactIndex++)
	{
		sp<SurfaceI::ImpactI> spImpact=hitArray[impactIndex];
		const I32 otherIndex=spImpact->primitiveIndex();

		Record edit=m_spEditMap->lookup(otherIndex);
		if(edit.isValid() && m_aValue.check(edit))
		{
			sum+=m_aValue(edit);
		}
		else if(realValue)
		{
			const Real value1=
					m_spOutputAttribute->real(otherIndex);
			sum+=SpatialVector(value1,value1,value1);
		}
		else if(vectorValue)
		{
			sum+=m_spOutputAttribute->spatialVector(otherIndex);
		}
	}

	a_rSample=sum/Real(hitCount);

	return TRUE;
}

void FollicleOp::movePicked(const SpatialVector& a_rDelta)
{
#if FE_FCO_EDIT_DEBUG
	feLog("FollicleOp::movePicked %s\n",c_print(a_rDelta));
#endif

	const BWORD snapUV=(catalog<String>("snapMode")=="uv");

	const I32 pickCount=m_pickArray.size();
	for(I32 pickIndex=0;pickIndex<pickCount;pickIndex++)
	{
		const Pick& rPick=m_pickArray[pickIndex];
		const I32 elementIndex=rPick.m_elementId;
		const Real pointWeight=rPick.m_weight;
		Record edit=getEdit(elementIndex);

		if(snapUV)
		{
			if(!m_aUV.check(edit))
			{
				continue;
			}

			slideOnUV(elementIndex,a_rDelta*pointWeight);
		}
		else
		{
			if(!m_aLocation.check(edit))
			{
				continue;
			}

			m_aLocation(edit)+=a_rDelta*pointWeight;

#if FE_FCO_EDIT_DEBUG
			SpatialVector inputPoint;
			set(inputPoint);
			const I32 inputCount=m_inputPosition.size();
			if(elementIndex<inputCount)
			{
				inputPoint=m_inputPosition[elementIndex];
			}

			const SpatialVector outputPoint=m_aLocation(edit);

			feLog("FollicleOp::movePicked %d %d at %s by %s to %s\n",
					pickIndex,elementIndex,c_print(inputPoint),
					c_print(m_aLocation(edit)),c_print(outputPoint));
#endif

			snapToSurface(elementIndex);

#if FE_FCO_EDIT_DEBUG
			feLog("  snap %s\n",c_print(m_aLocation(edit)));
#endif
		}

		if(elementIndex<I32(m_elementDirty.size()))
		{
			m_elementDirty[elementIndex]=TRUE;
		}

#if FE_FCO_EDIT_DEBUG
	feLog("FollicleOp::movePicked done\n");
#endif

	}

//	clearPositionalCaches();
}

void FollicleOp::slideOnUV(I32 a_index,const Vector2 a_deltaUV)
{
#if FE_FCO_EDIT_DEBUG
	feLog("FollicleOp::slideOnUV %d %s\n",a_index,c_print(a_deltaUV));
#endif

	const I32 inputCount=m_inputPosition.size();

	SpatialVector point;
	set(point);

	Vector2 origUV;
	set(origUV);

	if(a_index<inputCount)
	{
		point=m_inputPosition[a_index];

		sp<SurfaceI::ImpactI> spImpact=m_spDriver->nearestPoint(point);
		if(!spImpact.isValid())
		{
			return;
		}
		origUV=spImpact->uv();
	}

	Record edit=getEdit(a_index);

	m_aUV(edit)+=a_deltaUV;
	Vector2 uv=m_aUV(edit);

	const SpatialVector sample=m_spDriver->samplePoint(uv);

#if FE_FCO_EDIT_DEBUG
	feLog("  orig %s add %s uv %s\n",
			c_print(origUV),c_print(m_aUV(edit)),c_print(uv));
	feLog("  point %s sample %s\n",
			c_print(point),c_print(sample));
	feLog("  was %s now %s\n",
			c_print(m_aLocation(edit)),c_print(sample-point));
#endif

	m_aLocation(edit)=sample;
}

void FollicleOp::snapToSurface(I32 a_index)
{
	const I32 inputCount=m_inputPosition.size();

	SpatialVector point;
	set(point);
	if(a_index<inputCount)
	{
		point=m_inputPosition[a_index];
	}

	Record edit=getEdit(a_index);

	sp<SurfaceI::ImpactI> spImpact=
			m_spDriver->nearestPoint(m_aLocation(edit));
	if(!spImpact.isValid())
	{
		return;
	}
	m_aLocation(edit)=spImpact->intersection();
	m_aUV(edit)=spImpact->uv();
}

void FollicleOp::setupState(void)
{
	//* TODO driver mesh name for overlapping UV spaces

	m_aIndex.populate(m_spScope,"Move","Index");
	m_aSyncLocation.populate(m_spScope,"Move","SyncLocation");
	m_aLocation.populate(m_spScope,"Move","Location");
	m_aUV.populate(m_spScope,"Move","UV");
	m_aValue.populate(m_spScope,"Move","Value");

	m_aIndex.populate(m_spScope,"Create","Index");
	m_aLocation.populate(m_spScope,"Create","Location");
	m_aUV.populate(m_spScope,"Create","UV");
	m_aValue.populate(m_spScope,"Create","Value");

	m_aIndex.populate(m_spScope,"Hide","Index");
	m_aSyncLocation.populate(m_spScope,"Hide","SyncLocation");

	m_spEditMap=m_spState->lookupMap(m_aIndex);
}

BWORD FollicleOp::loadState(const String& rBuffer)
{
	const BWORD success=OperatorState::loadState(rBuffer);

	m_spEditMap=m_spState->lookupMap(m_aIndex);

	return success;
}

void FollicleOp::syncToInput(void)
{
	const String syncMode=catalog<String>("syncMode");

//	feLog("FollicleOp::syncToInput %s\n",syncMode.c_str());

	const I32 elementCount=m_inputPosition.size();

	if(syncMode=="index")
	{
		for(I32 elementIndex=0;elementIndex<elementCount;elementIndex++)
		{
			Record edit=m_spEditMap->lookup(elementIndex);
			if(edit.isValid())
			{
				m_aSyncLocation(edit)=m_inputPosition[elementIndex];
			}
		}
	}
	else if(syncMode=="location")
	{
		SpatialVector min(0.0,0.0,0.0);
		SpatialVector max(0.0,0.0,0.0);

		if(elementCount)
		{
			min=m_inputPosition[0];
			min=max;

			for(I32 elementIndex=0;elementIndex<elementCount;elementIndex++)
			{
				const SpatialVector point=m_inputPosition[elementIndex];
				for(U32 m=0;m<3;m++)
				{
					if(min[m]>point[m])
					{
						min[m]=point[m];
					}
					if(max[m]<point[m])
					{
						max[m]=point[m];
					}
				}
			}
		}

		const SpatialVector size=max-min;
		Real maxWidth=size[0];
		if(maxWidth>size[1])
		{
			maxWidth=size[1];
		}
		if(maxWidth>size[2])
		{
			maxWidth=size[2];
		}

		const Real maxDistSquared=0.01*maxWidth;

		sp<RecordGroup> spNewState(new RecordGroup());
		std::map<I32,Record> assignment;

		for(RecordGroup::iterator it=m_spState->begin();
				it!=m_spState->end();it++)
		{
			sp<RecordArray>& rspRA= *it;

			if(rspRA.isValid())
			{
				//* pass along all adds
				if(rspRA->layout()->name()=="Create")
				{
					for(int i=0;i<rspRA->length();i++)
					{
						Record edit=rspRA->getRecord(i);
						spNewState->add(edit);
					}

					continue;
				}

				for(int i=0;i<rspRA->length();i++)
				{
					Record edit=rspRA->getRecord(i);
					if(!m_aSyncLocation.check(edit))
					{
						continue;
					}

					BWORD adjusting=TRUE;
					while(adjusting)
					{
						const SpatialVector syncLocation=m_aSyncLocation(edit);

						//* find nearest point
						I32 nearestIndex= -1;
						Real nearestSquared=0.0;
						for(I32 elementIndex=0;elementIndex<elementCount;
								elementIndex++)
						{
							const SpatialVector point=
									m_inputPosition[elementIndex];
							const Real distanceSquared=
									magnitudeSquared(point-syncLocation);

							if(distanceSquared>maxDistSquared)
							{
								continue;
							}

							if(nearestIndex>=0 &&
									nearestSquared<distanceSquared)
							{
								continue;
							}

							//* TODO a later edit should steal the
							//* elementIndex if closer

							//* avoid repeats
							if(assignment.find(elementIndex)!=assignment.end())
							{
								continue;
							}

							nearestIndex=elementIndex;
							nearestSquared=distanceSquared;
						}

//						feLog("record %d index %d -> %d value %s\n",
//								i,m_aIndex(edit),nearestIndex,
//								c_print(m_aValue(edit)));

						m_aIndex(edit)=nearestIndex;
						if(nearestIndex>=0)
						{
							assignment[nearestIndex]=edit;
						}

						//* TODO tweak Location, UV

						spNewState->add(edit);

						adjusting=FALSE;
					}
				}
			}
		}

		//* NOTE unmatched records are thrown out
		m_spState=spNewState;
		m_spEditMap=m_spState->lookupMap(m_aIndex);
	}
}
