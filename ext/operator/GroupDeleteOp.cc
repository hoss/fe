/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <operator/operator.pmh>

#define FE_GRD_DEBUG	FALSE

namespace fe
{
namespace ext
{

void GroupDeleteOp::initialize(void)
{
	catalog<String>("icon")="FE_alpha";

	catalog<String>("pointGroups")="";
	catalog<String>("pointGroups","label")="Point Groups";
	catalog<String>("pointGroups","suggest")="pointGroups";
	catalog<bool>("pointGroups","joined")=true;
	catalog<String>("pointGroups","hint")=
		"Space delimited list of point groups to remove.";

	catalog<bool>("pointRegex")=false;
	catalog<String>("pointRegex","label")="Regex";
	catalog<String>("pointRegex","hint")=
			"Pass the pattern strings directly as true regular expressions."
			"  If this option is off,"
			" presumed glob patterns are converted to regex."
			"  Every '.' is first escaped as '\\.'."
			"  Then, every '?', indicating 'any character',"
			" is converted to '.'."
			"  Every '*', indicating 'any number of any characters',"
			" is converted to '.*'."
			"  Every '[!', indicating 'not' inside of square brackets,"
			" is converted to '[^'.";

	catalog<String>("primitiveGroups")="";
	catalog<String>("primitiveGroups","label")="Primitive Groups";
	catalog<String>("pointGroups","suggest")="primitiveGroups";
	catalog<bool>("primitiveGroups","joined")=true;
	catalog<String>("primitiveGroups","hint")=
		"Space delimited list of primitive groups to remove.";

	catalog<bool>("primitiveRegex")=false;
	catalog<String>("primitiveRegex","label")="Regex";
	catalog<String>("primitiveRegex","hint")=
			catalog<String>("pointRegex","hint");

	catalog< sp<Component> >("Input Surface");

	catalog< sp<Component> >("Output Surface");
	catalog<String>("Output Surface","IO")="output";
	catalog<String>("Output Surface","copy")="Input Surface";
	catalog<bool>("Output Surface","clobber")=true;
}

void GroupDeleteOp::handle(Record& a_rSignal)
{
#if FE_GRD_DEBUG
	feLog("GroupDeleteOp::handle\n");
#endif

	String pointGroups=catalog<String>("pointGroups");
	String primitiveGroups=catalog<String>("primitiveGroups");

	if(!pointGroups.empty() && !catalog<bool>("pointRegex"))
	{
		pointGroups=pointGroups.convertGlobToRegex();
	}

	if(!primitiveGroups.empty() && !catalog<bool>("primitiveRegex"))
	{
		primitiveGroups=primitiveGroups.convertGlobToRegex();
	}

	sp<SurfaceAccessibleI> spOutputAccessible;
	if(!accessOutput(spOutputAccessible,a_rSignal)) return;

	I32 discardCount(0);

	const I32 classCount=2;
	for(I32 classIndex=0;classIndex<classCount;classIndex++)
	{
		Element element=classIndex? e_primitiveGroup: e_pointGroup;
		String groups=classIndex? primitiveGroups: pointGroups;

		while(TRUE)
		{
			String token=groups.parse();
			if(token.empty())
			{
				break;
			}

//			feLog("GroupDeleteOp::handle discard %s \"%s\"\n",
//					elementText(element).c_str(),token.c_str());

			discardCount+=discardPattern(spOutputAccessible,element,token);
		}
	}

	String summary;
	summary.sPrintf("%d group%s",discardCount,discardCount==1? "": "s");
	catalog<String>("summary")=summary;

#if FE_GRD_DEBUG
	feLog("GroupDeleteOp::handle done\n");
#endif
}

} /* namespace ext */
} /* namespace fe */
