/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __operator_NoiseOp_h__
#define __operator_NoiseOp_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
    @brief apply Perlin noise to points in space

	@ingroup operator
*//***************************************************************************/
class FE_DL_EXPORT NoiseOp:
	public OperatorSurfaceCommon,
	public Initialize<NoiseOp>
{
	public:
				NoiseOp(void)												{}
virtual			~NoiseOp(void)												{}

		void	initialize(void);

				//* As HandlerI
virtual void	handle(Record& a_rSignal);

	private:
		Noise	m_noise;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __operator_NoiseOp_h__ */
