/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <operator/operator.pmh>

#define FE_SAL_DEBUG		FALSE
#define FE_SAL_PICK_DEBUG	FALSE

//* TODO investigate latency of changes

namespace fe
{
namespace ext
{

void SurfaceAttrLabOp::initialize(void)
{
	catalog<String>("icon")="FE_beta";

	//* page Config
	catalog<String>("Element")="Point";
	catalog<String>("Element","page")="Config";
	catalog<String>("Element","choice:0")="Point";
	catalog<String>("Element","choice:1")="Primitive";

	catalog<String>("Name")="uv";
	catalog<String>("Name","page")="Config";

	//* page Edit
	catalog<bool>("Saturate")=false;
	catalog<String>("Saturate","page")="Edit";
	catalog<bool>("Saturate","joined")=true;

	catalog<Real>("SaturateCenter")=0.5;
	catalog<Real>("SaturateCenter","min")= -1e6;
	catalog<Real>("SaturateCenter","max")=1e6;
	catalog<String>("SaturateCenter","label")="Threshold";
	catalog<String>("SaturateCenter","enabler")="Saturate";
	catalog<String>("SaturateCenter","page")="Edit";

	catalog<Real>("SaturateToMin")=0.0;
	catalog<Real>("SaturateToMin","high")=1.0;
	catalog<Real>("SaturateToMin","min")= -1e6;
	catalog<Real>("SaturateToMin","max")=1e6;
	catalog<String>("SaturateToMin","label")="To Min";
	catalog<String>("SaturateToMin","enabler")="Saturate";
	catalog<String>("SaturateToMin","page")="Edit";
	catalog<bool>("SaturateToMin","joined")=true;

	catalog<Real>("SaturateToMax")=1.0;
	catalog<Real>("SaturateToMax","high")=1.0;
	catalog<Real>("SaturateToMax","min")= -1e6;
	catalog<Real>("SaturateToMax","max")=1e6;
	catalog<String>("SaturateToMax","label")="To Max";
	catalog<String>("SaturateToMax","enabler")="Saturate";
	catalog<String>("SaturateToMax","page")="Edit";

	catalog<bool>("Ramp")=false;
	catalog<String>("Ramp","page")="Edit";
	catalog<bool>("Ramp","joined")=true;

	catalog<Real>("RampToMin")=0.0;
	catalog<Real>("RampToMin","low")=0.0;
	catalog<Real>("RampToMin","high")=1.0;
	catalog<Real>("RampToMin","min")= -1e6;
	catalog<Real>("RampToMin","max")=1e6;
	catalog<String>("RampToMin","label")="To Min";
	catalog<String>("RampToMin","enabler")="Ramp";
	catalog<String>("RampToMin","page")="Edit";
	catalog<bool>("RampToMin","joined")=true;

	catalog<Real>("RampToMax")=1.0;
	catalog<Real>("RampToMax","low")=0.0;
	catalog<Real>("RampToMax","high")=1.0;
	catalog<Real>("RampToMax","min")= -1e6;
	catalog<Real>("RampToMax","max")=1e6;
	catalog<String>("RampToMax","label")="To Max";
	catalog<String>("RampToMax","enabler")="Ramp";
	catalog<String>("RampToMax","page")="Edit";

	catalog< sp<Component> >("RampFunction");
	catalog<String>("RampFunction","implementation")="RampI";
	catalog<String>("RampFunction","Label")="Ramp Function";
	catalog<String>("RampFunction","page")="Edit";
	catalog<String>("RampFunction","enabler")="Ramp";

	catalog<bool>("Gradient")=false;
	catalog<String>("Gradient","page")="Edit";
	catalog<bool>("Gradient","joined")=true;

	catalog<Real>("GradientMax")=1.0;
	catalog<Real>("GradientMax","high")=1.0;
	catalog<String>("GradientMax","label")="Threshold";
	catalog<String>("GradientMax","page")="Edit";
	catalog<String>("GradientMax","enabler")="Gradient";

	catalog<I32>("GradientIterations")=1;
	catalog<String>("GradientIterations","label")="Iterations";
	catalog<String>("GradientIterations","page")="Edit";
	catalog<String>("GradientIterations","enabler")="Gradient";

	catalog<bool>("Smooth")=false;
	catalog<String>("Smooth","page")="Edit";
	catalog<bool>("Smooth","joined")=true;

	catalog<Real>("Smoothing")=1.0;
	catalog<Real>("Smoothing","high")=1.0;
	catalog<String>("Smoothing","page")="Edit";
	catalog<String>("Smoothing","enabler")="Smooth";

	catalog<I32>("SmoothIterations")=1;
	catalog<String>("SmoothIterations","label")="Iterations";
	catalog<String>("SmoothIterations","page")="Edit";
	catalog<String>("SmoothIterations","enabler")="Smooth";

	//* page Display
	catalog<bool>("DisplayText")=true;
	catalog<String>("DisplayText","label")="Overlay Text";
	catalog<String>("DisplayText","page")="Display";
	catalog<bool>("DisplayText","joined")=true;

	catalog<bool>("DisplaySinglePoint")=true;
	catalog<String>("DisplaySinglePoint","label")="Pick Point";
	catalog<String>("DisplaySinglePoint","page")="Display";
	catalog<String>("DisplaySinglePoint","enabler")="DisplayText";
	catalog<bool>("DisplaySinglePoint","joined")=true;

	catalog<bool>("DisplaySinglePrimitive")=true;
	catalog<String>("DisplaySinglePrimitive","label")="Pick Primitive";
	catalog<String>("DisplaySinglePrimitive","page")="Display";
	catalog<String>("DisplaySinglePrimitive","enabler")="DisplayText";

	catalog<bool>("DisplayValue")=true;
	catalog<String>("DisplayValue","label")="Value Text";
	catalog<String>("DisplayValue","page")="Display";
	catalog<String>("DisplayValue","enabler")="DisplayText";
	catalog<bool>("DisplayValue","joined")=true;

	catalog<bool>("DisplayPointIndex")=false;
	catalog<String>("DisplayPointIndex","label")="Point Index";
	catalog<String>("DisplayPointIndex","page")="Display";
	catalog<String>("DisplayPointIndex","enabler")="DisplayText";
	catalog<bool>("DisplayPointIndex","joined")=true;

	catalog<bool>("DisplayVertexIndex")=false;
	catalog<String>("DisplayVertexIndex","label")="Vertex Index";
	catalog<String>("DisplayVertexIndex","page")="Display";
	catalog<String>("DisplayVertexIndex","enabler")="DisplayText";
	catalog<bool>("DisplayVertexIndex","joined")=true;

	catalog<bool>("DisplayPrimitiveIndex")=false;
	catalog<String>("DisplayPrimitiveIndex","label")="Primitive Index";
	catalog<String>("DisplayPrimitiveIndex","page")="Display";
	catalog<String>("DisplayPrimitiveIndex","enabler")="DisplayText";

	catalog<bool>("DisplayMag")=true;
	catalog<String>("DisplayMag","label")="Mag Guides";
	catalog<String>("DisplayMag","page")="Display";
	catalog<bool>("DisplayMag","joined")=true;

	catalog<Real>("DisplayMagScale")=1.0;
	catalog<String>("DisplayMagScale","label")="Scale";
	catalog<String>("DisplayMagScale","enabler")="DisplayMag";
	catalog<String>("DisplayMagScale","page")="Display";

	catalog<bool>("DisplayShape")=true;
	catalog<String>("DisplayShape","label")="Shape";
	catalog<String>("DisplayShape","page")="Display";
	catalog<bool>("DisplayShape","joined")=true;

	catalog<Real>("DisplayShapeRadius")=0.1;
	catalog<String>("DisplayShapeRadius","label")="Radius";
	catalog<String>("DisplayShapeRadius","enabler")="DisplayShape";
	catalog<String>("DisplayShapeRadius","page")="Display";
	catalog<bool>("DisplayShapeRadius","joined")=true;

	catalog<bool>("DisplayShapeCentered")=false;
	catalog<String>("DisplayShapeCentered","label")="Centered";
	catalog<String>("DisplayShapeCentered","enabler")="DisplayShape";
	catalog<String>("DisplayShapeCentered","page")="Display";
	catalog<bool>("DisplayShapeCentered","joined")=true;

	catalog<Real>("DisplayShapeLength")=1.0;
	catalog<String>("DisplayShapeLength","label")="Length";
	catalog<String>("DisplayShapeLength","enabler")="DisplayShape";
	catalog<String>("DisplayShapeLength","page")="Display";

	catalog< sp<Component> >("Brush");
	catalog<String>("Brush","implementation")="DrawI";
	catalog<String>("Brush","visible")="any";

	catalog< sp<Component> >("Input Surface");

	m_spSolid=new DrawMode();
	m_spSolid->setDrawStyle(DrawMode::e_solid);
	m_spSolid->setBackfaceCulling(FALSE);
	m_spSolid->setLineWidth(1.0);

	m_spOverlay=new DrawMode();
	m_spOverlay->setDrawStyle(DrawMode::e_solid);
	m_spOverlay->setBackfaceCulling(FALSE);
	m_spOverlay->setLineWidth(1.0);
	m_spOverlay->setLit(FALSE);
}

void SurfaceAttrLabOp::handle(Record& a_rSignal)
{
#if FE_SAL_DEBUG
	feLog("SurfaceAttrLabOp::handle\n");
#endif

	const String attrName=catalog<String>("Name");

	catalog<String>("summary")=attrName;

	if(attrName.empty())
	{
		feLog("SurfaceAttrLabOp::handle no name\n");
		return;
	}

	const BWORD doPrimitive=(catalog<String>("Element")=="Primitive");

	sp<DrawI> spDrawBrush;
	accessBrush(spDrawBrush,a_rSignal,e_quiet);

	sp<DrawI> spDrawOverlay;
	accessBrushOverlay(spDrawOverlay,a_rSignal,e_quiet);

	if(spDrawBrush.isValid())
	{
		spDrawBrush->pushDrawMode(m_spSolid);
		spDrawOverlay->pushDrawMode(m_spOverlay);

		if(m_spInput.isValid() && m_spOutputElement.isValid())
		{
			const Color black(0.0,0.0,0.0,1.0);
			const Color red(0.6,0.0,0.0,1.0);
			const Color green(0.4,1.0,0.4,1.0);
			const Color blue(0.0,0.0,0.6,1.0);
			const Color cyan(0.4,1.0,1.0,1.0);
			const Color magenta(1.0,0.4,1.0,1.0);
			const Color yellow(1.0,1.0,0.0,1.0);
			const Color white(1.0,1.0,1.0,1.0);

			const SpatialVector& rRayOrigin=rayOrigin(a_rSignal);
			const SpatialVector& rRayDirection=rayDirection(a_rSignal);

			const String attrType=m_spOutputElement->type();

#if FE_SAL_PICK_DEBUG
			feLog("SurfaceAttrLabOp::handle ray %s  %s\n",
					c_print(rRayOrigin),c_print(rRayDirection));
#endif
			const BWORD displaySinglePrimitive=
					catalog<bool>("DisplaySinglePrimitive");

			const Real maxDistance=100.0;
			sp<SurfaceI::ImpactI> spImpact=m_spInput->rayImpact(
					rRayOrigin,rRayDirection,maxDistance);
			if(spImpact.isValid() || !displaySinglePrimitive)
			{
#if FE_SAL_PICK_DEBUG
				feLog("SurfaceAttrLabOp::handle hit\n");
#endif
				const BWORD displayText=catalog<bool>("DisplayText");
				const BWORD displayValue=catalog<bool>("DisplayValue");
				const BWORD displaySinglePoint=
						catalog<bool>("DisplaySinglePoint");
				const BWORD displayVertexIndex=
						catalog<bool>("DisplayVertexIndex");
				const BWORD displayPointIndex=
						catalog<bool>("DisplayPointIndex");
				const BWORD displayPrimitiveIndex=
						catalog<bool>("DisplayPrimitiveIndex");

				m_event.bind(windowEvent(a_rSignal));
//				feLog("SurfaceAttrLabOp::handle %s\n",c_print(m_event));

				sp<ViewI> spView=spDrawOverlay->view();

				I32 primitiveIndex= -1;
				I32 showPointIndex= -1;

				sp<SurfaceCurves::Impact> spCurveImpact=spImpact;
				if(spCurveImpact.isValid())
				{
					primitiveIndex=spCurveImpact->primitiveIndex();
					const Vector2 uv=spCurveImpact->uv();
					const U32 subCount=
							m_spOutputVertices->subCount(primitiveIndex);
					showPointIndex=uv[0]*(subCount-1)+0.5;

#if FE_SAL_PICK_DEBUG
					feLog("  curve pick %d uv %s show %d/%d\n",
							primitiveIndex,c_print(uv),showPointIndex,subCount);
#endif
				}

				sp<SurfaceTriangles::Impact> spTriImpact=spImpact;
				if(spTriImpact.isValid())
				{
					primitiveIndex=spTriImpact->primitiveIndex();
					const Barycenter<Real> bary=spTriImpact->barycenter();
					const Real bary2=1.0-bary[0]-bary[1];
					showPointIndex=bary[0]>bary[1]?
							(bary[0]>bary2? 0: 2):
							(bary[1]>bary2? 1: 2);

#if FE_SAL_PICK_DEBUG
					feLog("  triangle pick %d bary %s show %d\n",
							primitiveIndex,c_print(bary),showPointIndex);
#endif
				}

				if(!displaySinglePrimitive || primitiveIndex>=0)
				{
					I32 fontHeight=16;
					sp<FontI> spFontI=spDrawOverlay->font();
					if(spFontI.isValid())
					{
						fontHeight=spFontI->fontHeight(NULL,NULL);
					}

					const U32 margin=1;
					const Vector2i step(0,fontHeight);

					const I32 displayCount=displaySinglePrimitive?
							1: m_spOutputVertices->count();
					for(I32 displayIndex=0;displayIndex<displayCount;
							displayIndex++)
					{
						if(!displaySinglePrimitive)
						{
							primitiveIndex=displayIndex;
						}

						SpatialVector average;
						set(average);

						const I32 subCount=
								m_spOutputVertices->subCount(primitiveIndex);
						for(I32 subIndex=0;subIndex<subCount;subIndex++)
						{
							const I32 pointIndex=m_spOutputVertices->integer(
									primitiveIndex,subIndex);
							const SpatialVector point=
									m_spOutputPoint->spatialVector(pointIndex);

							average+=point;

							Vector2i projected=spView->project(point,
									ViewI::e_perspective);

							if((!displaySinglePoint || subIndex==showPointIndex)
									&& displayText)
							{
								if(displayValue && !doPrimitive)
								{
									const String text=interpretElement(attrType,
											pointIndex);

									drawLabel(spDrawOverlay,projected,text,
											FALSE,margin,white,NULL,&black);

									projected+=step;
								}

								if(displayPointIndex)
								{
									String text2;
									text2.sPrintf("%d",pointIndex);
									drawLabel(spDrawOverlay,projected,text2,
											FALSE,margin,cyan,NULL,&black);

									projected+=step;
								}

								if(displayVertexIndex)
								{
									String text2;
									text2.sPrintf("%d",subIndex);
									drawLabel(spDrawOverlay,projected,text2,
											FALSE,margin,green,NULL,&black);
								}
							}
						}
						if(subCount && displayText)
						{
							average*=1.0/subCount;

							Vector2i projected=spView->project(
									average,ViewI::e_perspective);

							if(displayValue && doPrimitive)
							{
								const String text=interpretElement(attrType,
										primitiveIndex);

								drawLabel(spDrawOverlay,projected,text,
										FALSE,margin,white,NULL,&blue);

								projected+=step;
							}

							if(displayPrimitiveIndex)
							{
								String text2;
								text2.sPrintf("%d",primitiveIndex);

								drawLabel(spDrawOverlay,projected,
										text2,FALSE,margin,magenta,NULL,&black);
							}
						}
					}
				}
			}

			if(catalog<bool>("DisplayShape"))
			{
				const Real displayShapeRadius=
						catalog<Real>("DisplayShapeRadius");
				const Real displayShapeLength=
						catalog<Real>("DisplayShapeLength");
				const BWORD displayShapeCentered=
						catalog<bool>("DisplayShapeCentered");
				const BWORD isString=(attrType=="string");
				const BWORD isReal=(attrType=="real");
				const BWORD isInteger=(attrType=="integer");

				if(isString || isReal || isInteger)
				{
					const I32 primitiveCount=m_spOutputVertices->count();
					for(I32 primitiveIndex=0;primitiveIndex<primitiveCount;
							primitiveIndex++)
					{
						const I32 subCount=doPrimitive? 1:
								m_spOutputVertices->subCount(primitiveIndex);
						for(I32 subIndex=0;subIndex<subCount;subIndex++)
						{
							const I32 elementIndex=doPrimitive? primitiveIndex:
									m_spOutputVertices->integer(
									primitiveIndex,subIndex);
							const SpatialVector point=doPrimitive?
									primitiveCenter(primitiveIndex):
									m_spOutputPoint->spatialVector(
											elementIndex);
							const Real value=isString? 1.0:
									(isReal?
									m_spOutputElement->real(elementIndex):
									m_spOutputElement->integer(elementIndex));
							Color sphereColor=yellow;
							if(isString)
							{
								const String text=
										m_spOutputElement->string(elementIndex);
								sphereColor=colorOf(text);
							}

							SpatialTransform sphereTransform;
							setIdentity(sphereTransform);
							setTranslation(sphereTransform,point);

							const Real sphereRadius=displayShapeRadius*value;
							const SpatialVector sphereScale(sphereRadius,
									sphereRadius,sphereRadius);

							spDrawBrush->drawSphere(sphereTransform,
									&sphereScale,sphereColor);
						}
					}
				}
				else if(attrType.match("vector."))
				{
					const I32 primitiveCount=m_spOutputVertices->count();
					for(I32 primitiveIndex=0;primitiveIndex<primitiveCount;
							primitiveIndex++)
					{
						const I32 subCount=doPrimitive? 1:
								m_spOutputVertices->subCount(primitiveIndex);
						for(I32 subIndex=0;subIndex<subCount;subIndex++)
						{
							const I32 elementIndex=doPrimitive? primitiveIndex:
									m_spOutputVertices->integer(
									primitiveIndex,subIndex);
							const SpatialVector point=doPrimitive?
									primitiveCenter(primitiveIndex):
									m_spOutputPoint->spatialVector(
											elementIndex);
							const SpatialVector value=displayShapeLength*
									m_spOutputElement->spatialVector(
									elementIndex);

							if(magnitudeSquared(value)<1e-6)
							{
								continue;
							}

							SpatialVector location1;
							SpatialVector location2;
							if(displayShapeCentered)
							{
								location1=point-0.5*value;
								location2=point+0.5*value;
							}
							else
							{
								location1=point;
								location2=point+value;
							}

							const Real radius1=displayShapeRadius;
							const Real radius2=displayShapeRadius*0.01;
							const U32 slices=8;

							spDrawBrush->drawCylinder(location1,location2,
								radius1,radius2,yellow,slices);
						}
					}
				}
			}
		}

		m_keepInput=TRUE;

		spDrawOverlay->popDrawMode();
		spDrawBrush->popDrawMode();

		return;
	}

	if(!m_keepInput)
	{
		access(m_spInput,"Input Surface",e_warning);
	}
	m_keepInput=FALSE;

	if(!accessOutput(m_spOutputPoint,a_rSignal,e_point,e_position)) return;

	if(!accessOutput(m_spOutputElement,a_rSignal,
			doPrimitive? e_primitive: e_point,
			attrName,e_error,e_refuseMissing)) return;

	if(!accessOutput(m_spOutputVertices,a_rSignal,
			e_primitive,e_vertices)) return;

	const I32 elementCount=m_spOutputElement->count();
	const String attrType=m_spOutputElement->type();
	const BWORD isVector=attrType.match("vector.");
	const BWORD isReal=(attrType=="real");

	const BWORD displayMag=(catalog<bool>("DisplayMag") &&
			(isVector || isReal || attrType=="integer"));

	sp<SurfaceAccessorI> spInputNormal;
	if(displayMag)
	{
		access(spInputNormal,"Input Surface",
				doPrimitive? e_primitive: e_point,e_normal,e_warning);
	}

	const I32 normalCount=spInputNormal.isValid()? spInputNormal->count(): 0;

#if FE_SAL_DEBUG
	feLog("SurfaceAttrLabOp::handle type %s elementCount %d normalCount %d\n",
			attrType.c_str(),elementCount,normalCount);
#endif

	if(catalog<bool>("Saturate"))
	{
		const Real saturateCenter=catalog<Real>("SaturateCenter");
		const Real saturateToMin=catalog<Real>("SaturateToMin");
		const Real saturateToMax=catalog<Real>("SaturateToMax");

		for(I32 index=0;index<elementCount;index++)
		{
			if(isVector)
			{
				SpatialVector value=m_spOutputElement->spatialVector(index);
				const Real mag=magnitude(value);
				if(mag>0.0)
				{
					const Real newMag=
							mag<saturateCenter? saturateToMin: saturateToMax;
					value*=newMag/mag;
					m_spOutputElement->set(index,value);
				}
			}
			else if(isReal)
			{
				const Real mag=m_spOutputElement->real(index);

				m_spOutputElement->set(index,
						mag<saturateCenter? saturateToMin: saturateToMax);
			}
			else
			{
				const I32 mag=m_spOutputElement->integer(index);

				m_spOutputElement->set(index,
						I32(mag<saturateCenter? saturateToMin: saturateToMax));
			}
		}
	}

	if(catalog<bool>("Ramp"))
	{
		//* TODO normalize domain, then rescale result to min/max
//		const Real rampToMin=catalog<Real>("RampToMin");
//		const Real rampToMax=catalog<Real>("RampToMax");

		const sp<RampI> spRampI=catalog< sp<Component> >("RampFunction");

		for(I32 index=0;index<elementCount;index++)
		{
			if(isVector)
			{
				SpatialVector value=m_spOutputElement->spatialVector(index);
				const Real mag=magnitude(value);
				if(mag>0.0)
				{
					const Real newMag=spRampI->eval(mag);
					value*=newMag/mag;
					m_spOutputElement->set(index,value);
				}
			}
			else if(isReal)
			{
				//* TODO ramp itself should take care of out of bounds values
				const Real mag=
						fmin(1.0,fmax(0.0,m_spOutputElement->real(index)));

				m_spOutputElement->set(index,spRampI->eval(mag));
			}
			else
			{
				const I32 mag=m_spOutputElement->integer(index);

				m_spOutputElement->set(index,I32(spRampI->eval(mag)));
			}
		}
	}

	if(catalog<bool>("Gradient"))
	{
		//* TODO primitive mode

		const Real gradientMax=catalog<Real>("GradientMax");
		const Real gradientMax2=gradientMax*gradientMax;
		const I32 gradientIterations=catalog<I32>("GradientIterations");

		const I32 primitiveCount=m_spOutputVertices->count();

		for(I32 pass=0;pass<gradientIterations;pass++)
		{
			for(I32 index=0;index<primitiveCount;index++)
			{
				const U32 subCount=m_spOutputVertices->subCount(index);
				for(U32 subIndex=0;subIndex<subCount;subIndex++)
				{
					const U32 lastSubIndex=subIndex? subIndex-1: subCount-1;

					const I32 vertexIndexA=
							m_spOutputVertices->integer(index,lastSubIndex);
					const I32 vertexIndexB=
							m_spOutputVertices->integer(index,subIndex);

					if(isVector)
					{
						SpatialVector elementA=
								m_spOutputElement->spatialVector(vertexIndexA);
						SpatialVector elementB=
								m_spOutputElement->spatialVector(vertexIndexB);

						const SpatialVector delta=elementB-elementA;
						const Real dist2=magnitudeSquared(delta);
						if(dist2<=gradientMax2)
						{
							continue;
						}
						const Real dist=sqrt(dist2);

						const SpatialVector adjust=
							0.5*(dist-gradientMax)/dist*delta;

						m_spOutputElement->set(vertexIndexA,elementA+adjust);
						m_spOutputElement->set(vertexIndexB,elementB-adjust);
					}
					else if(isReal)
					{
						Real elementA=m_spOutputElement->real(vertexIndexA);
						Real elementB=m_spOutputElement->real(vertexIndexB);

						const Real rise=elementB-elementA;
						if(fabs(rise)<=gradientMax)
						{
							continue;
						}

						const Real adjust=0.5*
								(rise>0? rise-gradientMax: rise+gradientMax);

						m_spOutputElement->set(vertexIndexA,elementA+adjust);
						m_spOutputElement->set(vertexIndexB,elementB-adjust);
					}
					else
					{
						I32 elementA=m_spOutputElement->integer(vertexIndexA);
						I32 elementB=m_spOutputElement->integer(vertexIndexB);

						const I32 rise=elementB-elementA;
						if(abs(rise)<=gradientMax)
						{
							continue;
						}

						const I32 adjust=0.5*
								(rise>0? rise-gradientMax: rise+gradientMax);

						m_spOutputElement->set(vertexIndexA,elementA+adjust);
						m_spOutputElement->set(vertexIndexB,elementB-adjust);
					}
				}
			}
		}
	}

	if(catalog<bool>("Smooth"))
	{
		//* TODO primitive mode

		const Real smoothing=catalog<Real>("Smoothing");
		const I32 smoothIterations=catalog<I32>("SmoothIterations");

		const I32 primitiveCount=m_spOutputVertices->count();

		for(I32 pass=0;pass<smoothIterations;pass++)
		{
			for(I32 index=0;index<primitiveCount;index++)
			{
				const U32 subCount=m_spOutputVertices->subCount(index);
				for(U32 subIndex=0;subIndex<subCount;subIndex++)
				{
					const U32 lastSubIndex=subIndex? subIndex-1: subCount-1;

					const I32 vertexIndexA=
							m_spOutputVertices->integer(index,lastSubIndex);
					const I32 vertexIndexB=
							m_spOutputVertices->integer(index,subIndex);

					if(isVector)
					{
						SpatialVector elementA=
								m_spOutputElement->spatialVector(vertexIndexA);
						SpatialVector elementB=
								m_spOutputElement->spatialVector(vertexIndexB);

						const SpatialVector average=0.5*(elementA+elementB);

						elementA+=smoothing*(average-elementA);
						elementB+=smoothing*(average-elementB);

						m_spOutputElement->set(vertexIndexA,elementA);
						m_spOutputElement->set(vertexIndexB,elementB);
					}
					else if(isReal)
					{
						Real elementA=m_spOutputElement->real(vertexIndexA);
						Real elementB=m_spOutputElement->real(vertexIndexB);

						const Real average=0.5*(elementA+elementB);

						elementA+=smoothing*(average-elementA);
						elementB+=smoothing*(average-elementB);

						m_spOutputElement->set(vertexIndexA,elementA);
						m_spOutputElement->set(vertexIndexB,elementB);
					}
					else
					{
						I32 elementA=m_spOutputElement->real(vertexIndexA);
						I32 elementB=m_spOutputElement->real(vertexIndexB);

						const Real average=0.5*(elementA+elementB);

						elementA+=smoothing*(average-elementA);
						elementB+=smoothing*(average-elementB);

						m_spOutputElement->set(vertexIndexA,elementA);
						m_spOutputElement->set(vertexIndexB,elementB);
					}
				}
			}
		}
	}

	if(displayMag)
	{
		const Real displayMagScale=catalog<Real>("DisplayMagScale");

		sp<DrawI> spDrawGuide;
		if(!accessGuide(spDrawGuide,a_rSignal)) return;

		for(I32 index=0;index<elementCount;index++)
		{
			const Real mag=isVector?
					magnitude(m_spOutputElement->spatialVector(index)):
					(isReal? m_spOutputElement->real(index):
					m_spOutputElement->integer(index));

			const SpatialVector norm=index<normalCount?
					spInputNormal->spatialVector(index,0):
					SpatialVector(0.0,1.0,0.0);

#if FE_SAL_DEBUG
			feLog("SurfaceAttrLabOp::handle %d/%d mag %.6G norm %s\n",
					index,elementCount,mag,c_print(norm));
#endif

			SpatialVector line[2];
			line[0]=doPrimitive?
					primitiveCenter(index):
					m_spOutputPoint->spatialVector(index);
			line[1]=line[0]+displayMagScale*mag*norm;

#if FE_SAL_DEBUG
			feLog("  line %s  %s\n",c_print(line[0]),c_print(line[1]));
#endif

			spDrawGuide->drawLines(line,NULL,2,DrawI::e_strip,false,NULL);
		}
	}

#if FE_SAL_DEBUG
	feLog("SurfaceAttrLabOp::handle done\n");
#endif
}

String SurfaceAttrLabOp::interpretElement(String a_attrType,I32 a_index)
{
	String text;

	if(a_attrType=="string")
	{
		text=m_spOutputElement->string(a_index);
	}
	else if(a_attrType=="real")
	{
		const Real realValue=m_spOutputElement->real(a_index);

		text.sPrintf("%.3G",realValue);
	}
	else if(a_attrType=="integer")
	{
		const I32 intValue=m_spOutputElement->integer(a_index);

		text.sPrintf("%d",intValue);
	}
	else
	{
		const SpatialVector vectorValue=
				m_spOutputElement->spatialVector(a_index);

		text.sPrintf("%s",c_print(vectorValue));
	}

	return text;
}

SpatialVector SurfaceAttrLabOp::primitiveCenter(I32 a_primitiveIndex)
{
	//* average vertices
	const I32 subCount=m_spOutputVertices->subCount(a_primitiveIndex);
	if(!subCount)
	{
		return SpatialVector(0.0,0.0,0.0);
	}

	SpatialVector sum;
	set(sum);
	for(I32 subIndex=0;subIndex<subCount;subIndex++)
	{
		sum+=m_spOutputVertices->spatialVector(a_primitiveIndex,subIndex);
	}
	return sum/Real(subCount);
}

//* static
Color SurfaceAttrLabOp::colorOf(const String& a_rString)
{
	const U32 len=a_rString.length();
	const unsigned char* buffer=a_rString.rawU8();

	//* TODO optimize RGB spread

	U32 hash=0;
	for(U32 m=0;m<len;m++)
	{
		//* djb2 hash
//		hash=((hash<<5) + hash) + buffer[m];

		//* sdbm hash
		hash=buffer[m] + (hash<<6) + (hash<<16) - hash;
	}

	const Color result(
			((hash&0x00FF0000)>>16)	/Real(0xFF),
			((hash&0x0000FF00)>>8)	/Real(0xFF),
			 (hash&0x000000FF)		/Real(0xFF),
			1.0);

//	feLog("hash 0x%x %d color %s\n",
//			hash,hash,c_print(result));

	return result;
}

} /* namespace ext */
} /* namespace fe */
