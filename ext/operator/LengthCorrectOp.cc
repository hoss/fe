/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <operator/operator.pmh>

//* NOTE refinement with multithreading will crash

#define FE_LCO_DEBUG	FALSE

using namespace fe;
using namespace fe::ext;

//* TODO Extrapolating pre-scale - rescale whole curve along
//*			root to tip vector to restore curve length

void LengthCorrectOp::initialize(void)
{
	catalog<String>("Threads","page")="Config";

	catalog<String>("AutoThread","page")="Config";

	catalog<String>("Paging","page")="Config";

	catalog<String>("StayAlive","page")="Config";

	catalog<String>("Work","page")="Config";

	catalog<bool>("fragment")=false;
	catalog<String>("fragment","label")="Fragment";
	catalog<String>("fragment","page")="Config";
	catalog<bool>("fragment","joined")=true;
	catalog<String>("fragment","hint")=
			"Associate collections of primitives which have"
			" matching a fragment attribute."
			"  Each fragment is considered an independent hinge."
			"  Without fragmentation,"
			" the entire input is treat as a single hinge.";

	catalog<String>("fragmentAttr")="part";
	catalog<String>("fragmentAttr","label")="By Attr";
	catalog<String>("fragmentAttr","page")="Config";
	catalog<String>("fragmentAttr","enabler")="fragment";
	catalog<bool>("fragmentAttr","joined")=true;
	catalog<String>("fragmentAttr","hint")=
			"Name of string attribute describing membership"
			" in a collection of primitives.";

	catalog<bool>("Root Lock")=false;
	catalog<String>("Root Lock","page")="Config";
	catalog<String>("Root Lock","hint")=
			"Leave first vertex of each primitive untouched."
			"  This can lock down rooted curves, like hair.";

	catalog<bool>("Locking")=false;
	catalog<bool>("Locking","joined")=true;
	catalog<String>("Locking","page")="Config";
	catalog<String>("Locking","hint")=
			"Use locking attribute to resist change.";

	catalog<String>("LockAttr")="lock";
	catalog<String>("LockAttr","label")="Lock Attribute";
	catalog<String>("LockAttr","enabler")="Locking";
	catalog<String>("LockAttr","page")="Config";
	catalog<String>("LockAttr","hint")=
			"Name of real point attribute describing resistance to change"
			" (1.0 is full lock).";

	catalog<I32>("Iterations")=1;
	catalog<I32>("Iterations","high")=10;
	catalog<I32>("Iterations","max")=100;
	catalog<String>("Iterations","page")="Behavior";

	catalog<Real>("Tension Rate")=1.0;
	catalog<Real>("Tension Rate","high")=1.0;
	catalog<String>("Tension Rate","page")="Behavior";
	catalog<String>("Tension Rate","hint")=
			"Influence how quickly each iteration pulls towards"
			" the correct length.";

	catalog<Real>("Compression Rate")=1.0;
	catalog<Real>("Compression Rate","high")=1.0;
	catalog<String>("Compression Rate","page")="Behavior";
	catalog<String>("Compression Rate","hint")=
			"Influence how quickly each iteration pushes towards"
			" the correct length.";

	catalog<Real>("Unfolding Rate")=0.0;
	catalog<Real>("Unfolding Rate","high")=1.0;
	catalog<String>("Unfolding Rate","page")="Behavior";
	catalog<String>("Unfolding Rate","hint")=
			"Influence how quickly each iteration pushes open"
			" faces collapsed around an edge.";

	catalog<Real>("Refolding Rate")=0.0;
	catalog<Real>("Refolding Rate","high")=1.0;
	catalog<String>("Refolding Rate","page")="Behavior";
	catalog<String>("Refolding Rate","hint")=
			"Influence how quickly each iteration pulls together"
			" faces flattened around an edge.";

	catalog<Real>("Correction Bias")=0.5;
	catalog<Real>("Correction Bias","max")=1.0;
	catalog<String>("Correction Bias","page")="Behavior";
	catalog<String>("Correction Bias","hint")=
			"Influence how much each point on an edge gets changed."
			"  Try lower numbers for rooted curves, like 0.3,"
			" but generally leave at 0.5 for polygons.";

	catalog<Real>("Orientation Bias")=0.0;
	catalog<Real>("Orientation Bias","max")=1.0;
	catalog<String>("Orientation Bias","page")="Behavior";
	catalog<String>("Orientation Bias","hint")=
			"For fragmented input, bias changes towards root"
			" (point with lowest point index) away from tip"
			" (furthest from root).";

	catalog<Real>("Restraint")=0.0;
	catalog<Real>("Restraint","max")=1.0;
	catalog<String>("Restraint","page")="Behavior";
	catalog<String>("Restraint","hint")=
			"Restrain changes perpendicular to each point's normal.";

	catalog<Real>("Restoration")=0.0;
	catalog<Real>("Restoration","max")=1.0;
	catalog<String>("Restoration","page")="Behavior";
	catalog<String>("Restoration","hint")=
			"Push points toward their original displacement"
			" from their neighbors.";

	catalog<Real>("Suspension")=0.0;
	catalog<String>("Suspension","label")="Sliding Snap";
	catalog<Real>("Suspension","max")=1.0;
	catalog<String>("Suspension","page")="Behavior";
	catalog<String>("Suspension","hint")=
			"Restrain changes to slide along original input.";

	catalog<bool>("Temporal")=false;
	catalog<String>("Temporal","page")="Behavior";
	catalog<String>("Temporal","hint")=
			"Tries to retain state of the prior frame."
			"  If the prior frame was not the last evaluated,"
			" the node may choose not to use temporal data.";
	catalog<bool>("Temporal","joined")=true;

	catalog<Real>("Response")=1.0;
	catalog<String>("Response","enabler")="Temporal";
	catalog<String>("Response","page")="Behavior";
	catalog<String>("Response","hint")=
			"When temporal, this defines how quickly the surface returns"
			" to its original shape.";

	catalog<SpatialVector>("temporalOffset")=SpatialVector(0.0,0.0,0.0);
	catalog<String>("temporalOffset","label")="Temporal Offset";
	catalog<String>("temporalOffset","enabler")="Temporal";
	catalog<String>("temporalOffset","page")="Behavior";
	catalog<String>("temporalOffset","hint")=
			"When temporal, this translates cached points each frame.";

	catalog<bool>("Collide")=false;
	catalog<String>("Collide","label")="Collision Avoidance";
	catalog<String>("Collide","page")="Collision";
	catalog<bool>("Collide","joined")=true;
	catalog<String>("Collide","hint")=
			"Push resulting surface out of collider.";

	catalog<Real>("Margin")=0.1;
	catalog<Real>("Margin","high")=1.0;
	catalog<Real>("Margin","max")=1e3;
	catalog<String>("Margin","enabler")="Collide";
	catalog<String>("Margin","page")="Collision";
	catalog<bool>("Margin","joined")=true;
	catalog<String>("Margin","hint")=
			"Minimum distance to keep from collider surface.";

	catalog<Real>("CollideRate")=1.0;
	catalog<String>("CollideRate","label")="Rate";
	catalog<Real>("CollideRate","high")=1.0;
	catalog<String>("CollideRate","enabler")="Collide";
	catalog<String>("CollideRate","page")="Collision";
	catalog<String>("CollideRate","hint")=
			"Amount of collision response per iteration.";

	catalog<bool>("ranged")=false;
	catalog<String>("ranged","label")="Range Limit";
	catalog<String>("ranged","enabler")="Collide";
	catalog<String>("ranged","page")="Collision";
	catalog<bool>("ranged","joined")=true;
	catalog<String>("ranged","hint")=
			"Limit distance for detecting collisions.";

	catalog<Real>("maxDist")=1.0;
	catalog<Real>("maxDist","high")=100.0;
	catalog<Real>("maxDist","max")=1e6;
	catalog<String>("maxDist","enabler")="Collide";
	catalog<String>("maxDist","page")="Collision";
	catalog<String>("maxDist","hint")=
			"Maximum distance for detecting collisions.";

	catalog<String>("collideElement")="Point";
	catalog<String>("collideElement","label")="Collide With";
	catalog<String>("collideElement","page")="Collision";
	catalog<String>("collideElement","choice:0")="Point";
	catalog<String>("collideElement","label:0")="Each Point";
	catalog<String>("collideElement","choice:1")="Primitive";
	catalog<String>("collideElement","label:1")="Primitive Bounds";
	catalog<String>("collideElement","choice:2")="CurveRoot";
	catalog<String>("collideElement","label:2")="Curve Bounds around Root";
	catalog<String>("collideElement","enabler")="Collide";
	catalog<bool>("collideElement","joined")=true;

	catalog<I32>("hitLimit")=1;
	catalog<String>("hitLimit","label")="Hit Limit";
	catalog<I32>("hitLimit","min")=1;
	catalog<I32>("hitLimit","high")=10;
	catalog<I32>("hitLimit","max")=100;
	catalog<String>("hitLimit","page")="Collision";
	catalog<String>("hitLimit","enabler")="Collide";

	catalog<Real>("shellRoundness")=1.0;
	catalog<String>("shellRoundness","label")="Shell Roundness";
	catalog<Real>("shellRoundness","high")=1.0;
	catalog<String>("shellRoundness","enabler")="Collide";
	catalog<String>("shellRoundness","page")="Collision";
	catalog<String>("shellRoundness","hint")=
			"When not colliding against each point,"
			" collapse the collision sphere"
			" perpendicular to the general curve direction.";

	catalog<bool>("spreading")=false;
	catalog<String>("spreading","label")="Spread";
	catalog<Real>("spreading","high")=1.0;
	catalog<Real>("spreading","max")=10.0;
	catalog<bool>("spreading","joined")=true;
	catalog<String>("spreading","enabler")="Collide";
	catalog<String>("spreading","page")="Collision";
	catalog<String>("spreading","hint")=
			"Propagate collisions.";

	catalog<I32>("spreadRange")=1;
	catalog<String>("spreadRange","label")="Range";
	catalog<I32>("spreadRange","min")=1;
	catalog<I32>("spreadRange","high")=10;
	catalog<I32>("spreadRange","max")=100;
	catalog<bool>("spreadRange","joined")=true;
	catalog<String>("spreadRange","enabler")="Collide";
	catalog<String>("spreadRange","page")="Collision";
	catalog<String>("spreadRange","hint")=
			"Reach to neighbors during spread.";

	catalog<Real>("spreadRate")=1.0;
	catalog<String>("spreadRate","label")="Rate";
	catalog<Real>("spreadRate","max")=1.0;
	catalog<String>("spreadRate","enabler")="Collide";
	catalog<String>("spreadRate","page")="Collision";
	catalog<String>("spreadRate","hint")=
			"Scale of collision propagation per iteration.";

	catalog<bool>("routing")=false;
	catalog<String>("routing","label")="Routing";
	catalog<Real>("routing","high")=1.0;
	catalog<Real>("routing","max")=10.0;
	catalog<bool>("routing","joined")=true;
	catalog<String>("routing","enabler")="Collide";
	catalog<String>("routing","page")="Collision";
	catalog<String>("routing","hint")=
			"Straighten routes between waypoints.";

	catalog<Real>("routeRate")=1.0;
	catalog<String>("routeRate","label")="Rate";
	catalog<Real>("routeRate","max")=1.0;
	catalog<bool>("routeRate","joined")=true;
	catalog<String>("routeRate","enabler")="Collide";
	catalog<String>("routeRate","page")="Collision";
	catalog<String>("routeRate","hint")=
			"Scale of routing adjustment per iteration.";

	catalog<String>("routeAttr")="waypoint";
	catalog<String>("routeAttr","label")="Attr";
	catalog<String>("routeAttr","enabler")="Collide";
	catalog<String>("routeAttr","page")="Collision";
	catalog<String>("routeAttr","hint")=
			"Name of string point attribute indicating waypoints."
			"  Each point that is a waypoint should have a unique name."
			"  All other points should have an empty string.";

	catalog<I32>("Refinement")=0;
	catalog<String>("Refinement","label")="Collider Refinement";
	catalog<String>("Refinement","enabler")="Collide";
	catalog<String>("Refinement","page")="Collision";

	catalog<bool>("debugCollide")=false;
	catalog<String>("debugCollide","label")="Debug Collisions";
	catalog<String>("debugCollide","page")="Collision";
	catalog<String>("debugCollide","hint")=
			"Draws lines over the output showing the collisions.";

	catalog< sp<Component> >("Input Surface");

	catalog< sp<Component> >("Reference Surface");

	catalog< sp<Component> >("Collider Surface");
	catalog<bool>("Collider Surface","optional")=true;

	catalog< sp<Component> >("Output Surface");
	catalog<String>("Output Surface","IO")="output";
	catalog<String>("Output Surface","copy")="Input Surface";
	catalog<bool>("Output Surface","recycle")=true;
}

void LengthCorrectOp::handle(Record& a_rSignal)
{
#if FE_LCO_DEBUG
	feLog("LengthCorrectOp::handle\n");
#endif

	String& rSummary=catalog<String>("summary");
	rSummary="";

	m_spDrawDebug=NULL;
	if(catalog<bool>("debugCollide"))
	{
		accessGuide(m_spDrawDebug,a_rSignal);
	}

	const Real unfoldingRate=catalog<Real>("Unfolding Rate");
	const Real refoldingRate=catalog<Real>("Refolding Rate");
	const Real restoration=catalog<Real>("Restoration");
	const Real suspension=catalog<Real>("Suspension");
	const BWORD collide=catalog<bool>("Collide");
	const String fragmentAttr=catalog<String>("fragmentAttr");
	const Real orientationBias=catalog<Real>("Orientation Bias");

	//* output surface is copy of input surface
	if(!accessOutput(m_spOutputAccessible,a_rSignal)) return;

	if(!access(m_spInputAccessible,"Input Surface")) return;

	if(!access(m_spReferenceAccessible,"Reference Surface",e_quiet))
	{
		m_spReferenceAccessible=m_spInputAccessible;

		catalog<String>("warning")=
				"Substituting Input Surface for missing Reference Surface.";
	}

	sp<SurfaceAccessorI> spOutputVertices;
	if(!access(spOutputVertices,m_spOutputAccessible,
			e_primitive,e_vertices)) return;

	sp<SurfaceAccessorI> spRefVertices;
	if(!access(spRefVertices,m_spReferenceAccessible,
			e_primitive,e_vertices)) return;

	if(suspension>0.0)
	{
		//* access input as driver surface
		if(!access(m_spDriver,m_spInputAccessible)) return;

		m_spDriver->setRefinement(0);
		m_spDriver->prepareForSearch();
	}
	else
	{
		m_spDriver=NULL;
	}

	if(catalogOrDefault<bool>("Collider Surface","replaced",true))
	{
		m_spCollider=NULL;
		m_spColliderAccessible=NULL;
	}

	if(collide && (m_spCollider.isNull() || m_spColliderAccessible.isNull()))
	{
		access(m_spCollider,"Collider Surface",e_warning);
		if(m_spCollider.isValid())
		{
			const BWORD collidePrimitive=
					(catalog<String>("collideElement")!="Point");

			m_spCollider->setAccuracy(
					collidePrimitive? SurfaceI::e_sphere: SurfaceI::e_triangle);

			m_spCollider->setRefinement(catalog<I32>("Refinement"));
			m_spCollider->prepareForSearch();

			access(m_spColliderAccessible,"Collider Surface");
		}
	}

	//* just for the warning
	if(collide && catalog<bool>("routing"))
	{
		const String routeAttr=catalog<String>("routeAttr");
		if(!routeAttr.empty())
		{
			sp<SurfaceAccessorI> spInputWaypoint;
			access(spInputWaypoint,m_spInputAccessible,
					e_point,routeAttr,e_warning);
		}
	}

	const I32 primitiveCount=spOutputVertices->count();
	if(!primitiveCount)
	{
		catalog<String>("warning")+="no input primitives";
		return;
	}

	const I32 primitiveCountRef=spRefVertices->count();
	if(primitiveCount!=primitiveCountRef)
	{
		feLog("LengthCorrectOp::handle"
				" mismatched primitive count %d vs %d ref\n",
				primitiveCount,primitiveCountRef);
		catalog<String>("error")=
				"mismatched reference surface (primitive count)";
		return;
	}

#if FE_LCO_DEBUG
	feLog("LengthCorrectOp::handle primitives %d\n", primitiveCount);
#endif

	sp<SurfaceAccessorI> spOutputPoint;
	if(!access(spOutputPoint,m_spOutputAccessible,e_point,e_position)) return;

	const I32 pointCount=spOutputPoint->count();

	sp<SurfaceAccessorI> spInputPoint;
	if(!access(spInputPoint,m_spInputAccessible,e_point,e_position)) return;

	sp<SurfaceAccessorI> spRefPoint;
	if(!access(spRefPoint,m_spReferenceAccessible,e_point,e_position)) return;

	if(I32(spRefPoint->count())!=pointCount)
	{
		catalog<String>("error")="mismatched reference surface (point count)";
		return;
	}

	if(restoration>0.0)
	{
		m_normalCacheRef.resize(pointCount);
		m_normalCache.resize(pointCount);
		m_duCacheRef.resize(pointCount);
		m_duCache.resize(pointCount);
		m_tweakCache.resize(pointCount);
	}
	else
	{
		m_normalCacheRef.clear();
		m_normalCache.clear();
		m_duCacheRef.clear();
		m_duCache.clear();
		m_tweakCache.clear();
	}

	m_edgeMap.clear();
	if(unfoldingRate>0.0 || refoldingRate>0.0)
	{
		for(I32 primitiveIndex=0;primitiveIndex<primitiveCount;primitiveIndex++)
		{
			const U32 subCount=spOutputVertices->subCount(primitiveIndex);
			for(U32 subIndex=0;subIndex<subCount;subIndex++)
			{
				const U32 lastSubIndex=
						subIndex? subIndex-1: subCount-1;
				const U32 nextSubIndex=(subIndex+1)%subCount;
				const U32 lastSubIndex2=(subIndex-2+subCount)%subCount;

				const U32 pointIndexA=
						spOutputVertices->integer(primitiveIndex,lastSubIndex);
				const U32 pointIndexB=
						spOutputVertices->integer(primitiveIndex,subIndex);
				const U32 pointIndexC=
						spOutputVertices->integer(primitiveIndex,nextSubIndex);
				const U32 pointIndexD=
						spOutputVertices->integer(primitiveIndex,lastSubIndex2);

				const BWORD highA=(pointIndexA>pointIndexB);
				const U64 edgeKey=highA?
						(U64(pointIndexA)<<32)+pointIndexB:
						(U64(pointIndexB)<<32)+pointIndexA;

				Edge& rEdge=m_edgeMap[edgeKey];
				if(rEdge.m_pointIndexLow0<0)
				{
					if(subCount<4)
					{
						rEdge.m_pointIndexLow0=pointIndexC;
					}
					else if(highA)
					{
						rEdge.m_pointIndexLow0=pointIndexD;
						rEdge.m_pointIndexLow1=pointIndexC;
					}
					else
					{
						rEdge.m_pointIndexLow0=pointIndexC;
						rEdge.m_pointIndexLow1=pointIndexD;
					}
				}
				else
				{
					if(subCount<4)
					{
						rEdge.m_pointIndexHigh0=pointIndexC;
					}
					else if(highA)
					{
						rEdge.m_pointIndexHigh0=pointIndexD;
						rEdge.m_pointIndexHigh1=pointIndexC;
					}
					else
					{
						rEdge.m_pointIndexHigh0=pointIndexC;
						rEdge.m_pointIndexHigh1=pointIndexD;
					}
				}
			}
		}
	}

	BWORD fragmented=(catalog<bool>("fragment") &&
			!fragmentAttr.empty() && orientationBias>0.0);
	I32 fragmentCount=1;
	if(fragmented)
	{
		const String inputGroup="";	//* TODO

		spRefPoint->fragmentWith(SurfaceAccessibleI::e_point,
				fragmentAttr,inputGroup);
		fragmentCount=spRefPoint->fragmentCount();
		if(!fragmentCount)
		{
			fragmentCount=1;
			fragmented=FALSE;
		}

	}
	if(fragmented)
	{
		m_fragmentOfPoint.resize(pointCount);
		m_orientationOfFragment.resize(fragmentCount);

		for(I32 fragmentIndex=0;fragmentIndex<fragmentCount;fragmentIndex++)
		{
			SpatialVector& rOrientation=m_orientationOfFragment[fragmentIndex];
			set(rOrientation);

			const String fragmentName=
					spRefPoint->fragment(fragmentIndex);

			sp<SurfaceAccessibleI::FilterI> spFilter;
			if(!spRefPoint->filterWith(fragmentName,spFilter) ||
					spFilter.isNull())
			{
				continue;
			}
			const I32 filterCount=spFilter->filterCount();
			if(!filterCount)
			{
				continue;
			}

			I32 minPointIndex=pointCount;

			for(I32 filterIndex=0;filterIndex<filterCount;filterIndex++)
			{
				const I32 pointIndex=spFilter->filter(filterIndex);

				if(minPointIndex>pointIndex)
				{
					minPointIndex=pointIndex;
				}
				m_fragmentOfPoint[pointIndex]=fragmentIndex;
			}
			if(minPointIndex>=pointCount)
			{
				continue;
			}

			const SpatialVector rootPoint=
					spRefPoint->spatialVector(minPointIndex);
			SpatialVector farPoint=rootPoint;
			Real farDistance2=0.0;

			for(I32 filterIndex=0;filterIndex<filterCount;filterIndex++)
			{
				const I32 pointIndex=spFilter->filter(filterIndex);

				const SpatialVector otherPoint=
						spRefPoint->spatialVector(pointIndex);
				const Real distance2=
						magnitudeSquared(otherPoint-rootPoint);
				if(farDistance2<distance2)
				{
					farDistance2=distance2;
					farPoint=otherPoint;
				}
			}
			if(farDistance2>0.0)
			{
				rOrientation=unitSafe(farPoint-rootPoint);
			}
		}
	}
	else
	{
		m_fragmentOfPoint.resize(0);
		m_orientationOfFragment.resize(0);
	}

	const BWORD temporal=catalog<bool>("Temporal");
	const Real response=catalog<Real>("Response");

	const BWORD doTemporal=(temporal && response<1.0);

	if(doTemporal)
	{
		rSummary+=String(rSummary.empty()? "": " ")+"temporal";
	}
	if(m_spCollider.isValid())
	{
		rSummary+=String(rSummary.empty()? "": " ")+"collide";
	}
	if(fragmented)
	{
		String text;
		text.sPrintf("%d fragments",fragmentCount);
		rSummary+=String(rSummary.empty()? "": " ")+text;
	}

	const Real frame=currentFrame(a_rSignal);
	const BWORD outputReplaced=catalog<bool>("Output Surface","replaced");
	const BWORD useCache=doTemporal && I32(m_pointCache.size())==pointCount &&
			(frame==m_frame+1 || (frame==m_frame && !outputReplaced));

#if FE_LCO_DEBUG
	feLog("LengthCorrectOp::handle frame %.6G->%.6G outputReplaced %d"
			" pointCount %d pointCache %d useCache %d\n",
			m_frame,frame,outputReplaced,
			pointCount,m_pointCache.size(),useCache);
#endif

	if(useCache)
	{
		const SpatialVector temporalOffset=
				catalog<SpatialVector>("temporalOffset");

		const String lockAttr=catalog<String>("LockAttr");
		const BWORD locking=(catalog<bool>("Locking") && !lockAttr.empty());

		sp<SurfaceAccessorI> spInputLock;
		if(locking)
		{
			access(spInputLock,m_spInputAccessible,e_point,lockAttr,e_warning);
		}

		FEASSERT(I32(m_pointCache.size())==pointCount);
		FEASSERT(I32(m_velocityCache.size())==pointCount);

		const BWORD rootLock=catalog<bool>("Root Lock");

		//* output = history:prior blended with pure input
		for(I32 pointIndex=0;pointIndex<pointCount;pointIndex++)
		{
			const Real lock=(rootLock && !pointIndex)? 1.0:
					(spInputLock.isValid()?
					spInputLock->real(pointIndex): 0.0);

			const Real replace=lock+response*(1.0-lock);

			const SpatialVector inputPoint=
					spInputPoint->spatialVector(pointIndex);

			const SpatialVector temporalPoint=
					m_pointCache[pointIndex]+m_velocityCache[pointIndex];

			spOutputPoint->set(pointIndex,
					(1.0-replace)*(temporalPoint+temporalOffset)+
					replace*inputPoint);
		}
	}
	else
	{
		m_pointCache.resize(pointCount);
		m_velocityCache.resize(pointCount);

		for(I32 pointIndex=0;pointIndex<pointCount;pointIndex++)
		{
			spOutputPoint->set(pointIndex,
					spInputPoint->spatialVector(pointIndex));

			set(m_velocityCache[pointIndex]);
		}
	}
	m_frame=frame;

	adjustThreads();
	const I32 threadCount=catalog<I32>("Threads");
	if(threadCount>1)
	{
		sp<SpannedRange> spConnectivity=m_spOutputAccessible->atomize(
				SurfaceAccessibleI::e_pointsOfPrimitives,"",8*threadCount);

		setFullRange(spConnectivity);
		OperatorThreaded::handle(a_rSignal);
	}
	else
	{
		//* serial
		sp<SpannedRange> spFullRange=sp<SpannedRange>(new SpannedRange());
		spFullRange->nonAtomic().add(0,primitiveCount-1);
		run(-1,spFullRange);
	}

	for(I32 pointIndex=0;pointIndex<pointCount;pointIndex++)
	{
		const SpatialVector previousPoint=m_pointCache[pointIndex];
		const SpatialVector currentPoint=
				spOutputPoint->spatialVector(pointIndex);

		m_pointCache[pointIndex]=currentPoint;

		if(useCache)
		{
			m_velocityCache[pointIndex]=currentPoint-previousPoint;
		}
	}

#if FE_LCO_DEBUG
	feLog("LengthCorrectOp::handle done\n");
#endif
}

void LengthCorrectOp::run(I32 a_id,sp<SpannedRange> a_spRange)
{
	BWORD applyRestoration=(m_normalCacheRef.size() &&
			cacheDuN(m_spReferenceAccessible,m_normalCacheRef,
			m_duCacheRef,a_spRange));

	const I32 iterations=catalog<I32>("Iterations");
	for(I32 iteration=0;iteration<iterations;iteration++)
	{
		if(interrupted())
		{
			break;
		}

#if FE_LCO_DEBUG
		feLog("LengthCorrectOp::run %d iteration %d/%d\n",
				a_id,iteration,iterations);
#endif

		if(applyRestoration &&
				!cacheDuN(m_spOutputAccessible,m_normalCache,
				m_duCache,a_spRange))
		{
			applyRestoration=FALSE;
		}

		BWORD anythingChanged=relength(a_id,a_spRange,applyRestoration);

		if(m_spDriver.isValid() || m_spCollider.isValid())
		{
			anythingChanged =
					suspendAndCollide(a_id,a_spRange) || anythingChanged;
		}

		if(m_spCollider.isValid())
		{
			anythingChanged = reroute(a_id,a_spRange) || anythingChanged;
		}

		if(!anythingChanged)
		{
//			feLog("LengthCorrectOp::run %d iteration %d/%d nothing changed\n",
//					a_id,iteration,iterations);
			break;
		}
	}
}

BWORD LengthCorrectOp::relength(I32 a_id,sp<SpannedRange> a_spRange,
	BWORD a_applyRestoration)
{
	BWORD anythingChanged=FALSE;

	const BWORD rootLock=catalog<bool>("Root Lock");
	const Real tensionRate=catalog<Real>("Tension Rate");
	const Real compressionRate=catalog<Real>("Compression Rate");
	const Real refoldingRate=catalog<Real>("Refolding Rate");
	const Real unfoldingRate=catalog<Real>("Unfolding Rate");
	const Real correctionBias=catalog<Real>("Correction Bias");
	const Real orientationBias=catalog<Real>("Orientation Bias");
	const Real restraint=catalog<Real>("Restraint");
	const String lockAttr=catalog<String>("LockAttr");
	const BWORD locking=(catalog<bool>("Locking") && !lockAttr.empty());

	sp<SurfaceAccessorI> spInputProperties;
	if(!access(spInputProperties,m_spInputAccessible,
			e_primitive,e_properties)) return FALSE;

	sp<SurfaceAccessorI> spInputLock;
	if(locking)
	{
		access(spInputLock,m_spInputAccessible,e_point,lockAttr,e_warning);
	}

	sp<SurfaceAccessorI> spInputNormal;
	if(restraint>0.0)
	{
		access(spInputNormal,m_spInputAccessible,e_point,e_normal,e_warning);
	}

	sp<SurfaceAccessorI> spRefPoint;
	if(!access(spRefPoint,m_spReferenceAccessible,
			e_point,e_position)) return FALSE;

	sp<SurfaceAccessorI> spRefVertices;
	if(!access(spRefVertices,m_spReferenceAccessible,
			e_primitive,e_vertices)) return FALSE;

	sp<SurfaceAccessorI> spOutputPoint;
	if(!access(spOutputPoint,m_spOutputAccessible,
			e_point,e_position)) return FALSE;

	sp<SurfaceAccessorI> spOutputVertices;
	if(!access(spOutputVertices,m_spOutputAccessible,
			e_primitive,e_vertices)) return FALSE;

	const BWORD buffered=FALSE;	//* TODO param
	Array<SpatialVector> pointDelta;
	if(buffered)
	{
		const I32 pointCount=spOutputPoint->count();
		pointDelta.resize(pointCount);
		for(I32 pointIndex=0;pointIndex<pointCount;pointIndex++)
		{
			set(pointDelta[pointIndex]);
		}
	}

	std::set<U32> pointSet;

	SpannedRange::Iterator itPrim=a_spRange->begin();
	std::map<U64,Edge>::iterator itEdge=m_edgeMap.begin();

	for(I32 pass=0;pass<2;pass++)
	{
		if(interrupted())
		{
			break;
		}

		while(TRUE)
		{
			if(interrupted())
			{
				break;
			}

			BWORD openCurve=FALSE;
			I32 primitiveIndex= -1;
			I32 subCount=1;
			Edge edge;

			if(pass)
			{
				if(itEdge==m_edgeMap.end())
				{
					break;
				}

				edge=itEdge->second;
				if(edge.m_pointIndexHigh0<0)
				{
					itEdge++;
					continue;
				}

				subCount=1+
						(edge.m_pointIndexLow1>=0 || edge.m_pointIndexHigh1>=0);
			}
			else
			{
				if(itPrim.atEnd())
				{
					break;
				}

				primitiveIndex=itPrim.value();
				openCurve=spInputProperties->integer(
						primitiveIndex,e_openCurve);

				subCount=spOutputVertices->subCount(primitiveIndex);
				const I32 subCountRef=spRefVertices->subCount(primitiveIndex);
				if(subCount!=subCountRef)
				{
					feLog("LengthCorrectOp::relength"
							" primitive %d mismatched subCount %d vs %d ref\n",
							primitiveIndex,subCount,subCountRef);
					itPrim.step();
					continue;
				}

#if FE_LCO_DEBUG
				const U32 primitiveCount=spOutputVertices->count();
				feLog("LengthCorrectOp::relength primitive %d/%d subCount %d\n",
						primitiveIndex,primitiveCount,subCount);
#endif

				for(I32 subIndex=0;subIndex<subCount;subIndex++)
				{
					const I32 pointIndex=
							spOutputVertices->integer(primitiveIndex,subIndex);

					pointSet.insert(pointIndex);
				}
			}

			//* NOTE skip root point with rootLock or any open curves
			for(I32 subIndex=(rootLock||openCurve);subIndex<subCount;subIndex++)
			{
				const I32 otherCount=(pass || openCurve)? 1: subIndex;
				for(I32 otherIndex=0;otherIndex<otherCount;otherIndex++)
				{
					I32 pointIndexA= -1;
					I32 pointIndexB= -1;

					if(pass)
					{
						//*     0		   0		0--*--1
						//*    / \		  / \		|     |
						//*  A-----B	A-----B		A-----B
						//*    \ /		|     |		|     |
						//*     0		0--*--1		0--*--1
						//*
						//*		0-0		0-0 0-1		0-0 1-1

						pointIndexA=(subIndex && edge.m_pointIndexLow1>=0)?
								edge.m_pointIndexLow1:
								edge.m_pointIndexLow0;
						pointIndexB=(subIndex && edge.m_pointIndexHigh1>=0)?
								edge.m_pointIndexHigh1:
								edge.m_pointIndexHigh0;
					}
					else
					{
						const U32 lastSubIndex=
								subIndex? subIndex-1-otherIndex: subCount-1;

						FEASSERT(primitiveIndex>=0);
						pointIndexA=spOutputVertices->integer(
								primitiveIndex,lastSubIndex);
						pointIndexB=spOutputVertices->integer(
								primitiveIndex,subIndex);
					}

					FEASSERT(pointIndexA>=0);
					FEASSERT(pointIndexB>=0);

					const SpatialVector pointA=
							spOutputPoint->spatialVector(pointIndexA);
					const SpatialVector pointB=
							spOutputPoint->spatialVector(pointIndexB);


#if FE_LCO_DEBUG
					feLog("LengthCorrectOp::relength"
							" %d/%d %d/%d pointA %s pointB %s\n",
							subIndex,subCount,otherIndex,otherCount,
							c_print(pointA),c_print(pointB));
#endif

					const SpatialVector delta=pointB-pointA;
					const Real length=magnitude(delta);
					if(length>0.0)
					{
						Real lockA=0.0;
						Real lockB=0.0;
						if(spInputLock.isValid())
						{
							lockA=spInputLock->real(pointIndexA);
							lockB=spInputLock->real(pointIndexB);
						}
						const BWORD rootLocked=(rootLock && subIndex<2);
						if(rootLocked)
						{
							lockA=1.0;
						}

						const Real lockBias=0.5*(1.0+lockB-lockA);
						Real bias=lockBias+correctionBias-0.5;
						if(bias<0.0)
						{
							bias=0.0;
						}
						if(bias>1.0)
						{
							bias=1.0;
						}

						const SpatialVector pointRefA=
								spRefPoint->spatialVector(pointIndexA);
						const SpatialVector pointRefB=
								spRefPoint->spatialVector(pointIndexB);
						const Real lengthRef=magnitude(pointRefB-pointRefA);

						const Real rate=(length>lengthRef)?
								(pass? refoldingRate: tensionRate):
								(pass? unfoldingRate: compressionRate);
						const Real scalar=lengthRef/length-1.0;
						const SpatialVector adjust=scalar*delta*rate;

						if(m_fragmentOfPoint.size())
						{
							const SpatialVector orientation=
									(pointRefB-pointRefA)*(Real(1)/length);

							//* arbitrarily use fragment of lower point index
							const I32 fragmentIndex=(pointIndexA<pointIndexB)?
									m_fragmentOfPoint[pointIndexA]:
									m_fragmentOfPoint[pointIndexB];

							const SpatialVector orientationRef=
									m_orientationOfFragment[fragmentIndex];
							const Real alignment=
									dot(orientation,orientationRef);
							const Real reBias=Real(1)-
									Real(0.5)*(alignment+Real(1));

							bias=(Real(1)-orientationBias)*bias+
									orientationBias*reBias;
						}

#if FE_LCO_DEBUG
						feLog("LengthCorrectOp::relength"
								"   ref pointA %s pointB %s\n",
								c_print(pointRefA),c_print(pointRefB));
#endif

						const Real antibias=1.0-bias;

						SpatialVector changeA;
						SpatialVector changeB;

						if(rootLocked)
						{
							set(changeA);
							changeB=(1.0-lockB)*adjust;
						}
						else
						{
							changeA=(1.0-lockA)*bias*adjust;
							changeB=(1.0-lockB)*antibias*adjust;
						}

						if(spInputNormal.isValid())
						{
							const SpatialVector normalA=
									spInputNormal->spatialVector(pointIndexA);
							const SpatialVector normalB=
									spInputNormal->spatialVector(pointIndexB);

							const Real dotA=dot(normalA,changeA);
							const Real dotB=dot(normalB,changeB);

							const SpatialVector parallelA=normalA*dotA;
							const SpatialVector parallelB=normalB*dotB;

							const SpatialVector perpendicularA=
									changeA-parallelA;
							const SpatialVector perpendicularB=
									changeB-parallelB;

							const Real restraint1=Real(1)-restraint;

							changeA=perpendicularA+restraint1*parallelA;
							changeB=perpendicularB+restraint1*parallelB;
						}

						if(buffered)
						{
							pointDelta[pointIndexA]-=changeA;
							pointDelta[pointIndexB]+=changeB;
						}
						else
						{
							spOutputPoint->set(pointIndexA,pointA-changeA);
							spOutputPoint->set(pointIndexB,pointB+changeB);
						}

						//* TODO param
						if(fabs(scalar)>1e-3)
						{
							anythingChanged=TRUE;
						}

#if FE_LCO_DEBUG
						feLog("LengthCorrectOp::relength"
								"   bias %.6G lock pointA %.6G pointB %.6G\n",
								bias,lockA,lockB);
						feLog("LengthCorrectOp::relength"
								"   now pointA %s pointB %s\n",
								c_print(pointA-changeA),
								c_print(pointB+changeB));
						feLog("LengthCorrectOp::relength"
								"   check pointA %s pointB %s\n",
								c_print(spOutputPoint->spatialVector(
								pointIndexA)),
								c_print(spOutputPoint->spatialVector(
								pointIndexB)));
#endif
					}
				}
			}
			if(pass)
			{
				itEdge++;
			}
			else
			{
				itPrim.step();
			}
		}
	}

	if(buffered)
	{
		for(std::set<U32>::iterator itPoint=pointSet.begin();
				itPoint!=pointSet.end();itPoint++)
		{
			const I32 pointIndex=*itPoint;
			spOutputPoint->set(pointIndex,
					spOutputPoint->spatialVector(pointIndex)+
					pointDelta[pointIndex]);
		}
	}

	if(a_applyRestoration)
	{
		const Real restoration=catalog<Real>("Restoration");

		sp<SurfaceAccessorI> spOutputPoint;
		if(!access(spOutputPoint,m_spOutputAccessible,
				e_point,e_position)) return anythingChanged;

		//* clear tweaks
		for(std::set<U32>::iterator itPoint=pointSet.begin();
				itPoint!=pointSet.end();itPoint++)
		{
			const U32 pointIndex=*itPoint;
			set(m_tweakCache[pointIndex]);
		}

		for(SpannedRange::Iterator itPrim=a_spRange->begin();!itPrim.atEnd();
				itPrim.step())
		{
			if(interrupted())
			{
				break;
			}

			const U32 primitiveIndex=itPrim.value();
			const BWORD openCurve=spInputProperties->integer(
					primitiveIndex,e_openCurve);
			const U32 subCount=spOutputVertices->subCount(primitiveIndex);

			//* NOTE skip root point with rootLock or any open curves
			for(U32 subIndex=(rootLock||openCurve);subIndex<subCount;subIndex++)
			{
				const U32 lastSubIndex=subIndex? subIndex-1: subCount-1;

				const I32 pointIndexA=spOutputVertices->integer(
						primitiveIndex,lastSubIndex);
				const I32 pointIndexB=spOutputVertices->integer(
						primitiveIndex,subIndex);

				//* HACK attempt to eliminate redundancy
				if(pointIndexA>pointIndexB)
				{
					continue;
				}

				SpatialVector pointRefA=spRefVertices->spatialVector(
						primitiveIndex,lastSubIndex);
				SpatialVector pointRefB=spRefVertices->spatialVector(
						primitiveIndex,subIndex);

				SpatialVector pointA=spOutputVertices->spatialVector(
						primitiveIndex,lastSubIndex);
				SpatialVector pointB=spOutputVertices->spatialVector(
						primitiveIndex,subIndex);

				const SpatialVector& normalRefA=m_normalCacheRef[pointIndexA];
				const SpatialVector& normalRefB=m_normalCacheRef[pointIndexB];

				const SpatialVector& normalA=m_normalCache[pointIndexA];
				const SpatialVector& normalB=m_normalCache[pointIndexB];

				const SpatialVector& duRefA=m_duCacheRef[pointIndexA];
				const SpatialVector& duRefB=m_duCacheRef[pointIndexB];

				const SpatialVector& duA=m_duCache[pointIndexA];
				const SpatialVector& duB=m_duCache[pointIndexB];

				//* TODO cache transforms
				SpatialTransform xformRefA;
				SpatialTransform xformRefB;

				SpatialTransform xformA;
				SpatialTransform xformB;

				makeFrameNormalY(xformRefA,pointRefA,duRefA,normalRefA);
				makeFrameNormalY(xformRefB,pointRefB,duRefB,normalRefB);

				makeFrameNormalY(xformA,pointA,duA,normalA);
				makeFrameNormalY(xformB,pointB,duB,normalB);

				SpatialTransform invRefA;
				SpatialTransform invRefB;

				invert(invRefA,xformRefA);
				invert(invRefB,xformRefB);

				//* TODO cache local offset
				SpatialVector offsetA;
				SpatialVector offsetB;

				transformVector(invRefB,pointRefA,offsetA);
				transformVector(invRefA,pointRefB,offsetB);

				SpatialVector targetA;
				SpatialVector targetB;

				transformVector(xformB,offsetA,targetA);
				transformVector(xformA,offsetB,targetB);

				const SpatialVector tweakA=targetA-pointA;
				const SpatialVector tweakB=targetB-pointB;

				m_tweakCache[pointIndexA]+=restoration*tweakA;
				m_tweakCache[pointIndexB]+=restoration*tweakB;

#if TRUE
				if(pointIndexA==30 || pointIndexB==30)
				{
					feLog("LengthCorrectOp::relength prim %d pt %d %d\n",
							primitiveIndex,pointIndexA,pointIndexB);
					feLog("  point %s  %s\n",
							c_print(pointA),c_print(pointB));
					feLog("    ref %s  %s\n",
							c_print(pointRefA),c_print(pointRefB));
					feLog("  norm  %s  %s\n",
							c_print(normalA),c_print(normalB));
					feLog("    ref %s  %s\n",
							c_print(normalRefA),
							c_print(normalRefB));
					feLog("  du    %s  %s\n",
							c_print(duA),c_print(duB));
					feLog("    ref %s  %s\n",
							c_print(duRefA),c_print(duRefB));
					feLog("  targ  %s  %s\n",
							c_print(targetA),c_print(targetB));
					feLog("  tweak %s  %s\n",
							c_print(tweakA),c_print(tweakB));
					feLog("  cache %s  %s\n",
							c_print(m_tweakCache[pointIndexA]),
							c_print(m_tweakCache[pointIndexB]));
				}
#endif
			}
		}

		//* apply tweaks
		for(std::set<U32>::iterator itPoint=pointSet.begin();
				itPoint!=pointSet.end();itPoint++)
		{
			const U32 pointIndex=*itPoint;

			const SpatialVector point=spOutputPoint->spatialVector(pointIndex);
			spOutputPoint->set(pointIndex,point+m_tweakCache[pointIndex]);
		}
	}

	return anythingChanged;
}

BWORD LengthCorrectOp::suspendAndCollide(I32 a_id,sp<SpannedRange> a_spRange)
{
	BWORD anythingChanged=FALSE;

	const Real suspension=catalog<Real>("Suspension");
	const String lockAttr=catalog<String>("LockAttr");
	const BWORD locking=(catalog<bool>("Locking") && !lockAttr.empty());
	const Real margin=catalog<Real>("Margin");
	const Real collideRate=catalog<Real>("CollideRate");
	const I32 hitLimit=catalog<I32>("hitLimit");
	const BWORD spreading=
			m_spCollider.isValid()? catalog<bool>("spreading"): false;
	const I32 spreadRange=spreading? catalog<I32>("spreadRange"): 0;
	const Real spreadRate=spreading? catalog<Real>("spreadRate"): 0.0;
	const BWORD collidePrimitive=
			(catalog<String>("collideElement")!="Point");
	const BWORD rootBounds=
			(catalog<String>("collideElement")=="CurveRoot");
	const Real shellRoundness=catalog<Real>("shellRoundness");
	const Real maxDistance=
			catalog<bool>("ranged")? catalog<Real>("maxDist"): -1.0;

	sp<SurfaceAccessorI> spInputPoint;
	if(!access(spInputPoint,m_spInputAccessible,
			e_point,e_position)) return FALSE;

	sp<SurfaceAccessorI> spOutputPoint;
	if(!access(spOutputPoint,m_spOutputAccessible,
			e_point,e_position)) return FALSE;

//~	sp<SurfaceAccessorI> spInputNormal;
//~	if(m_spCollider.isValid())
//~	{
//~		access(spInputNormal,m_spInputAccessible,e_point,e_normal,e_quiet);
//~	}

	sp<SurfaceAccessorI> spColliderVertices;
	sp<SurfaceAccessorI> spColliderNormals;
	if(collidePrimitive)
	{
		if(access(spColliderVertices,m_spColliderAccessible,
				e_primitive,e_vertices))
		{
			access(spColliderNormals,m_spColliderAccessible,
					e_point,e_normal);
		}
	}

	const U32 pointCount=spOutputPoint->count();
	BWORD* pointChanged=new BWORD[pointCount];
	for(U32 pointIndex=0;pointIndex<pointCount;pointIndex++)
	{
		pointChanged[pointIndex]=FALSE;
	}

	sp<SurfaceAccessorI> spOutputVertices;
	if(!access(spOutputVertices,m_spOutputAccessible,
			e_primitive,e_vertices)) return FALSE;

	sp<SurfaceAccessorI> spInputLock;
	if(locking)
	{
		access(spInputLock,m_spInputAccessible,e_point,lockAttr,e_quiet);
	}

	SpatialVector driverCenter;
//~	Real driverRange2=0.0;
	if(m_spCollider.isValid())
	{
		driverCenter=m_spCollider->center();
//~		const Real range=m_spCollider->radius()+margin;
//~		driverRange2=range*range;
	}

	//* TODO only indices needed
	SpatialVector* push=new SpatialVector[pointCount];

	const BWORD doSpread=(spreading && spreadRange && spreadRate>1e-6);
	if(doSpread)
	{
		for(U32 pointIndex=0;pointIndex<pointCount;pointIndex++)
		{
			set(push[pointIndex]);
		}
	}

#if FE_LCO_DEBUG
	feLog("LengthCorrectOp::suspendAndCollide range %s\n",
			a_spRange->dump().c_str());
#endif

	//* NOTE collide by point

	std::set<U32> pointSet;
	for(SpannedRange::Iterator it=a_spRange->begin();!it.atEnd();it.step())
	{
		if(interrupted())
		{
			break;
		}

		const U32 primitiveIndex=it.value();

		const U32 subCount=spOutputVertices->subCount(primitiveIndex);
		for(U32 subIndex=0;subIndex<subCount;subIndex++)
		{
			const U32 pointIndex=
					spOutputVertices->integer(primitiveIndex,subIndex);

			pointSet.insert(pointIndex);
		}
	}

#if FALSE
	for(SpannedRange::Iterator it=a_spRange->begin();!it.atEnd();it.step())
	{
		const U32 primitiveIndex=it.value();

		const U32 subCount=spOutputVertices->subCount(primitiveIndex);
		for(U32 subIndex=0;subIndex<subCount;subIndex++)
		{
			const U32 pointIndex=
					spOutputVertices->integer(primitiveIndex,subIndex);
#else
	{
		for(std::set<U32>::iterator it=pointSet.begin();it!=pointSet.end();it++)
		{
			const U32 pointIndex=*it;
#endif
			BWORD& rChanged=pointChanged[pointIndex];
			if(rChanged)
			{
				continue;
			}

			SpatialVector point=spOutputPoint->spatialVector(pointIndex);
			Real lock=0.0;
			if(spInputLock.isValid())
			{
				lock=spInputLock->real(pointIndex);
				if(lock>=1.0)
				{
					continue;
				}
			}

			if(m_spDriver.isValid())
			{
				const SpatialVector original=
						spInputPoint->spatialVector(pointIndex);
				if(magnitudeSquared(point-original)>1e-6)
				{
					sp<SurfaceI::ImpactI> spImpact=
							m_spDriver->nearestPoint(point);
					if(spImpact.isValid())
					{
						point+=(1.0-lock)*suspension*
								(spImpact->intersection()-point);
						rChanged=TRUE;
					}
				}
			}

			const Real collideScalar=collideRate*(1.0-lock);

			if(m_spCollider.isValid())
			{
//~				const SpatialVector norm=spInputNormal.isValid()?
//~						spInputNormal->spatialVector(pointIndex):
//~						SpatialVector(0.0,0.0,0.0);

				//* TODO maxRadius arg to nearestPoint

//~				if(magnitudeSquared(point-driverCenter)<driverRange2)
				{
					Array< sp<SurfaceI::ImpactI> > impactArray=
							m_spCollider->nearestPoints(point,
							maxDistance,hitLimit);

					const U32 hitCount=impactArray.size();

					I32 pushCount=0;

					for(U32 hitIndex=0;hitIndex<hitCount;hitIndex++)
					{
						sp<SurfaceI::ImpactI> spImpact=impactArray[hitIndex];

						if(spImpact.isValid())
						{
							sp<SurfaceSearchable::Impact> spSearchableImpact=
									spImpact;
							if(spColliderNormals.isValid() &&
									spSearchableImpact.isValid())
							{
								//* collide with whole curve
								FEASSERT(collidePrimitive);

								const I32 colliderPrimitiveIndex=
										spSearchableImpact->primitiveIndex();
								const I32 curveSubCount=
										spColliderVertices->subCount(
										colliderPrimitiveIndex);

								Real collideFade=1.0;

								if(curveSubCount)
								{
									const SpatialVector pointA=
											spColliderVertices->spatialVector(
											colliderPrimitiveIndex,0);
									const SpatialVector pointB=
											spColliderVertices->spatialVector(
											colliderPrimitiveIndex,
											curveSubCount-1);

									SpatialVector delta=pointB-pointA;

//~									//* TODO maybe use skin normal of collider
//~									if(spInputNormal.isValid())
//~									{
//~										const Real deltaDot=dot(delta,norm);
//~										delta=norm*deltaDot;
//~									}
//~
//~									const SpatialVector center=pointB-delta;
//~
//~									const Real radius=magnitude(delta);
//~
//~									const SpatialVector curveDir=(radius>0.0)?
//~											delta/radius:
//~											SpatialVector(0.0,0.0,0.0);

									//* bounding sphere of all points in curve

									SpatialVector* points=
											new SpatialVector[curveSubCount];

									for(I32 curveSubIndex=0;
											curveSubIndex<curveSubCount;
											curveSubIndex++)
									{
										points[curveSubIndex]=
												spColliderVertices
												->spatialVector(
												colliderPrimitiveIndex,
												curveSubIndex);
									}

									SpatialVector center;
									Real radius;

									BoundingSphere<Real>::solve(
											points,curveSubCount,center,radius);

									delete[] points;

									const SpatialVector curveDir=
											unitSafe(delta);

									if(rootBounds)
									{
										//* center on root
										center=pointA;
										radius*=2.0;
									}

									SpatialVector pushDir=
											unitSafe(point-center);

									const Real normDot=dot(pushDir,curveDir);

									if(normDot<0.0)
									{
										continue;
									}

//~									const BWORD curveDirFlip=FALSE;
//~									if(curveDirFlip)
//~									{
//~										if(normDot<0.0)
//~										{
//~											const SpatialVector along=
//~													normDot*curveDir;
//~											pushDir-=2.0*along;
//~										}
//~									}

									collideFade=2.0-magnitude(point-center)/
											(radius+margin);
									collideFade=
											fe::maximum(collideFade,Real(0));
									collideFade=
											fe::minimum(collideFade,Real(1));

									//* pseudo-ellipse
									const Real shellRoundness2=shellRoundness*
											(rootBounds? 0.5: 1.0);

									const SpatialVector shellAlong=
											normDot*curveDir;
									const SpatialVector shellSide=
											pushDir-shellAlong;
									const SpatialVector shellDir=
											shellAlong+
											shellRoundness2*shellSide;

									const SpatialVector target=center+shellDir*
											(radius+margin);

//~									const Real ellipseScalar=shellRoundness2+
//~											(1.0-shellRoundness2)*fabs(normDot);
//~
//~									const SpatialVector target=center+pushDir*
//~											(radius*ellipseScalar+margin);

									if(m_spDrawDebug.isValid())
									{
										const Color black(0.0,0.0,0.0,1.0);
										const Color yellow(1.0,1.0,0.0,1.0);
										const Color green(0.0,1.0,0.0,1.0);

										Color colors[2];
										colors[0]=black;
										colors[1]=yellow;

										SpatialVector line[2];
										line[0]=center;
										line[1]=target;

										m_spDrawDebug->drawLines(line,NULL,2,
												DrawI::e_strip,true,colors);

										colors[1]=green;

										line[0]=target;
										line[1]=point;

										m_spDrawDebug->drawLines(line,NULL,2,
												DrawI::e_strip,true,colors);
									}

									if(collidePoint(point,target,pushDir,
											collideScalar*collideFade,
											push[pointIndex]))
									{
										rChanged=TRUE;
										pushCount++;
									}
								}
							}
							else
							{
								const SpatialVector norm=spImpact->normal();

								const SpatialVector target=
										spImpact->intersection()+margin*norm;

								if(collidePoint(point,target,norm,
										collideScalar,push[pointIndex]))
								{
									rChanged=TRUE;
									pushCount++;
								}
							}
						}
					}
					if(pushCount)
					{
						push[pointIndex]*=1.0/Real(pushCount);
						point+=push[pointIndex];
					}
				}
			}

			if(rChanged)
			{
				spOutputPoint->set(pointIndex,point);
				anythingChanged=TRUE;
			}

			//* do not repeat each point
			rChanged=TRUE;
		}
	}

	delete[] pointChanged;

	if(doSpread)
	{
		sp<SurfaceAccessorI> spInputProperties;
		if(!access(spInputProperties,m_spInputAccessible,
				e_primitive,e_properties))
		{
			delete[] push;
			return FALSE;
		}

		SpatialVector* change=new SpatialVector[pointCount];

		for(U32 pointIndex=0;pointIndex<pointCount;pointIndex++)
		{
			change[pointIndex]=push[pointIndex];
		}

		for(SpannedRange::Iterator it=a_spRange->begin();!it.atEnd();it.step())
		{
			if(interrupted())
			{
				break;
			}

			const U32 primitiveIndex=it.value();
			const BWORD openCurve=spInputProperties->integer(
					primitiveIndex,e_openCurve);

			const I32 subCount=spOutputVertices->subCount(primitiveIndex);
			for(I32 subIndex=0;subIndex<subCount;subIndex++)
			{
				const U32 pointIndexA=
						spOutputVertices->integer(primitiveIndex,subIndex);

				for(I32 spreadIndex= -spreadRange;
						spreadIndex<=spreadRange;spreadIndex++)
				{
					if(!spreadIndex)
					{
						continue;
					}

					const Real fadeScale=
							1.0-fabs(spreadIndex)/Real(spreadRange);

					I32 otherIndex=subIndex+spreadIndex;
					if(openCurve && (otherIndex<0 || otherIndex>=subCount))
					{
						//* no wrap-around if open curve
						continue;
					}
					otherIndex=(otherIndex+subCount)%subCount;

					if((otherIndex<0 || otherIndex>=subCount))
					{
						//* spread too far
						continue;
					}

					Real lock=0.0;
					if(spInputLock.isValid())
					{
						lock=spInputLock->real(otherIndex);
						if(lock>=1.0)
						{
							continue;
						}
					}

					const U32 pointIndexB=spOutputVertices->integer(
							primitiveIndex,otherIndex);

					const SpatialVector& rPushA=push[pointIndexA];
					SpatialVector& rChangeB=change[pointIndexB];

					const SpatialVector unitA=unitSafe(rPushA);

					if(magnitude(change[pointIndexB])<1e-6)
					{
						rChangeB=1e-3*unitA;
					}
					const SpatialVector unitB=unitSafe(rChangeB);

					const SpatialVector delta=(rPushA-rChangeB);
					const Real changeDot=dot(unitB,delta);

					if(changeDot>0.0)
					{
						const SpatialVector point=
								spOutputPoint->spatialVector(pointIndexB);
						const SpatialVector boost=
								unitB*changeDot*spreadRate*fadeScale*(1.0-lock);

						rChangeB+=boost;
						spOutputPoint->set(pointIndexB,point+boost);
						anythingChanged=TRUE;
					}
				}
			}
		}

		delete[] change;
	}

	delete[] push;

	return anythingChanged;
}

BWORD LengthCorrectOp::collidePoint(SpatialVector a_point,
	SpatialVector a_target,SpatialVector a_dir,
	Real a_scalar,SpatialVector& a_rPush)
{
	const SpatialVector toTarget=a_target-a_point;
	const Real along=dot(toTarget,a_dir);
	if(along>0.0)
	{
#if FE_LCO_DEBUG
		feLog("LengthCorrectOp::collidePoint"
				"   point %s dir %s along %.6G target %s\n",
				c_print(a_point),c_print(a_dir),along,c_print(toTarget));
#endif

		SpatialVector push=a_scalar*along*a_dir;

		//* HACK dot fade
//		push*=along/magnitude(toTarget);

		a_rPush+=push;

#if FE_LCO_DEBUG
		feLog("   push %s\n",c_print(push));
#endif
		return TRUE;
	}

	return FALSE;
}

BWORD LengthCorrectOp::reroute(I32 a_id,sp<SpannedRange> a_spRange)
{
	BWORD anythingChanged=FALSE;

	if(!catalog<bool>("routing"))
	{
		return FALSE;
	}

	const Real routeRate=catalog<Real>("routeRate");
	if(routeRate<=0.0)
	{
		return FALSE;
	}

	const String routeAttr=catalog<String>("routeAttr");
	if(routeAttr.empty())
	{
		return FALSE;
	}

	sp<SurfaceAccessorI> spInputWaypoint;
	if(!access(spInputWaypoint,m_spInputAccessible,
			e_point,routeAttr,e_quiet)) return FALSE;

	sp<SurfaceAccessorI> spInputPoint;
	if(!access(spInputPoint,m_spInputAccessible,
			e_point,e_position)) return FALSE;

	sp<SurfaceAccessorI> spInputProperties;
	if(!access(spInputProperties,m_spInputAccessible,
			e_primitive,e_properties)) return FALSE;

	sp<SurfaceAccessorI> spOutputPoint;
	if(!access(spOutputPoint,m_spOutputAccessible,
			e_point,e_position)) return FALSE;

	sp<SurfaceAccessorI> spOutputVertices;
	if(!access(spOutputVertices,m_spOutputAccessible,
			e_primitive,e_vertices)) return FALSE;

	for(SpannedRange::Iterator it=a_spRange->begin();!it.atEnd();it.step())
	{
		if(interrupted())
		{
			break;
		}

		const U32 primitiveIndex=it.value();

		const BWORD openCurve=spInputProperties->integer(
				primitiveIndex,e_openCurve);
		if(!openCurve)
		{
			continue;
		}

		Array<String> waypoints;
		Array<I32> waystarts;

		const U32 subCount=spOutputVertices->subCount(primitiveIndex);
		for(U32 subIndex=0;subIndex<subCount;subIndex++)
		{
			const U32 pointIndex=
					spOutputVertices->integer(primitiveIndex,subIndex);

			const String waypoint=spInputWaypoint->string(pointIndex);

			if(waypoint!="")
			{
				waypoints.push_back(waypoint);
				waystarts.push_back(subIndex);
			}
		}

		const I32 spanCount=waypoints.size()-1;
		for(I32 spanIndex=0;spanIndex<spanCount;spanIndex++)
		{
			const String waypointA=waypoints[spanIndex];
			const String waypointB=waypoints[spanIndex+1];

			const I32 waystartA=waystarts[spanIndex];
			const I32 waystartB=waystarts[spanIndex+1];

			const I32 wayspan=waystartB-waystartA+1;

			SpatialVector* pointIn=new SpatialVector[wayspan];
			SpatialVector* pointOut=new SpatialVector[wayspan];
			SpatialVector* offset=new SpatialVector[wayspan];

//			feLog("span %d/%d waypoints \"%s\" \"%s\" waystart %d %d\n",
//					spanIndex,spanCount,waypointA.c_str(),waypointB.c_str(),
//					waystartA,waystartB);

			for(I32 subIndex=waystartA;subIndex<=waystartB;subIndex++)
			{
				const U32 pointIndex=
						spOutputVertices->integer(primitiveIndex,subIndex);

				const U32 tableIndex=subIndex-waystartA;

				pointIn[tableIndex]=spInputPoint->spatialVector(pointIndex);
				pointOut[tableIndex]=spOutputPoint->spatialVector(pointIndex);
				offset[tableIndex]=pointOut[tableIndex]-pointIn[tableIndex];
			}

			const U32 tableIndexA=0;
			const U32 tableIndexB=waystartB-waystartA;

			const Real length=
					magnitude(pointIn[tableIndexB]-pointIn[tableIndexA]);

			for(I32 subIndex=waystartA+1;subIndex<waystartB;subIndex++)
			{
				const U32 tableIndex=subIndex-waystartA;

				const Real along=
						magnitude(pointIn[tableIndex]-pointIn[tableIndexA]);

				SpatialVector orthoDirA(0.0,0.0,0.0);
				SpatialVector orthoDirB(0.0,0.0,0.0);

				for(I32 subIndex2=waystartA;subIndex2<=subIndex;subIndex2++)
				{
					const U32 tableIndex2=subIndex2-waystartA;

					orthoDirA+=offset[tableIndex2];
				}
				for(I32 subIndex2=subIndex;subIndex2<=waystartB;subIndex2++)
				{
					const U32 tableIndex2=subIndex2-waystartA;

					orthoDirB+=offset[tableIndex2];
				}

				normalizeSafe(orthoDirA);
				normalizeSafe(orthoDirB);

				Real orthoMagA=0.0;
				Real orthoMagB=0.0;

				const Real alignedA=dot(offset[tableIndexA],orthoDirB);
				const Real alignedB=dot(offset[tableIndexB],orthoDirA);

				for(I32 subIndex2=waystartA;subIndex2<=subIndex;subIndex2++)
				{
					const U32 tableIndex2=subIndex2-waystartA;

					const Real along2=magnitude(
							pointIn[tableIndex2]-pointIn[tableIndexB]);

					const Real aligned=dot(offset[tableIndex2],orthoDirA);
					const Real projection=
							alignedB+(aligned-alignedB)*(length-along)/along2;

					if(orthoMagA<projection)
					{
						orthoMagA=projection;
					}
				}
				for(I32 subIndex2=subIndex;subIndex2<=waystartB;subIndex2++)
				{
					const U32 tableIndex2=subIndex2-waystartA;

					const Real along2=magnitude(
							pointIn[tableIndex2]-pointIn[tableIndexA]);

					const Real aligned=dot(offset[tableIndex2],orthoDirB);
					const Real projection=
							alignedA+(aligned-alignedA)*along/along2;

					if(orthoMagB<projection)
					{
						orthoMagB=projection;
					}
				}

				if(orthoMagA==0.0 && orthoMagB==0.0)
				{
					continue;
				}

				const Real weightA=orthoMagA/(orthoMagA+orthoMagB);
				const Real weightB=1.0-weightA;

				const SpatialVector adjust=weightA*orthoMagA*orthoDirA+
						weightB*orthoMagB*orthoDirB;
				const SpatialVector target=pointIn[tableIndex]+adjust;
				const SpatialVector change=target-pointOut[tableIndex];
				const SpatialVector result=
						pointOut[tableIndex]+routeRate*change;

				spOutputVertices->set(primitiveIndex,subIndex,result);
				anythingChanged=TRUE;
			}

			delete[] offset;
			delete[] pointOut;
			delete[] pointIn;
		}
	}

	return anythingChanged;
}

BWORD LengthCorrectOp::cacheDuN(sp<SurfaceAccessibleI>& a_rspAccessible,
	Array<SpatialVector>& a_rNormalCache,Array<SpatialVector>& a_rDuCache,
	sp<SpannedRange> a_spRange)
{
	sp<SurfaceAccessorI> spVertices;
	if(!access(spVertices,a_rspAccessible,e_primitive,e_vertices)) return FALSE;

	BWORD vertexUV=TRUE;
	sp<SurfaceAccessorI> spElementUV;
	if(!access(spElementUV,a_rspAccessible,e_vertex,e_uv))
	{
		vertexUV=FALSE;
		if(!access(spElementUV,a_rspAccessible,e_point,e_uv)) return FALSE;
	}

	std::set<U32> pointSet;

	for(SpannedRange::Iterator it=a_spRange->begin();!it.atEnd();it.step())
	{
		const U32 primitiveIndex=it.value();
		const I32 subCount=spVertices->subCount(primitiveIndex);

		for(I32 subIndex=0;subIndex<subCount;subIndex++)
		{
			const I32 pointIndex=spVertices->integer(primitiveIndex,subIndex);
			pointSet.insert(pointIndex);
		}
	}

	for(std::set<U32>::iterator it=pointSet.begin();it!=pointSet.end();it++)
	{
		const U32 pointIndex=*it;
		set(a_rNormalCache[pointIndex]);
		set(a_rDuCache[pointIndex]);
	}

	for(SpannedRange::Iterator it=a_spRange->begin();!it.atEnd();it.step())
	{
		if(interrupted())
		{
			break;
		}

		const U32 primitiveIndex=it.value();
		const I32 subCount=spVertices->subCount(primitiveIndex);

		//* TODO evaluate using more than first three vertices
		if(subCount<3)
		{
			continue;
		}

		const SpatialVector point0=spVertices->spatialVector(primitiveIndex,0);
		const SpatialVector point1=spVertices->spatialVector(primitiveIndex,1);
		const SpatialVector point2=spVertices->spatialVector(primitiveIndex,2);

		SpatialVector uv0;
		SpatialVector uv1;
		SpatialVector uv2;

		if(vertexUV)
		{
			uv0=spElementUV->spatialVector(primitiveIndex,0);
			uv1=spElementUV->spatialVector(primitiveIndex,1);
			uv2=spElementUV->spatialVector(primitiveIndex,2);
		}
		else
		{
			const I32 pointIndex0=spVertices->integer(primitiveIndex,0);
			const I32 pointIndex1=spVertices->integer(primitiveIndex,1);
			const I32 pointIndex2=spVertices->integer(primitiveIndex,2);

			uv0=spElementUV->spatialVector(pointIndex0);
			uv1=spElementUV->spatialVector(pointIndex1);
			uv2=spElementUV->spatialVector(pointIndex2);
		}

		SpatialVector du;
		SpatialVector dv;

		triangleDuDv(du,dv,point0,point1,point2,uv0,uv1,uv2);

//		feLog("LengthCorrectOp::cacheDuN %d vertexUV %d\n",
//				primitiveIndex,vertexUV);
//		feLog("  pt %s  %s  %s\n",
//				c_print(point0),c_print(point1),c_print(point2));
//		feLog("  uv %s  %s  %s\n",
//				c_print(uv0),c_print(uv1),c_print(uv2));

		const SpatialVector norm=
				unitSafe(cross(point2-point0,point1-point0));

		for(I32 subIndex=0;subIndex<subCount;subIndex++)
		{
			const I32 pointIndex=spVertices->integer(primitiveIndex,subIndex);
			a_rNormalCache[pointIndex]+=norm;
			a_rDuCache[pointIndex]+=du;

//			feLog("  sub %d %d\n",subIndex,pointIndex);
//			feLog("  norm %s -> %s\n",c_print(norm),
//					c_print(a_rNormalCache[pointIndex]));
//			feLog("  du   %s -> %s\n",c_print(du),
//					c_print(a_rDuCache[pointIndex]));
		}
	}

	for(std::set<U32>::iterator it=pointSet.begin();it!=pointSet.end();it++)
	{
		const U32 pointIndex=*it;
		normalizeSafe(a_rNormalCache[pointIndex]);
		normalizeSafe(a_rDuCache[pointIndex]);
	}

	return TRUE;
}
