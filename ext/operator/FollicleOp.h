/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __operator_FollicleOp_h__
#define __operator_FollicleOp_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Operator to edit surface locations

	@ingroup operator
*//***************************************************************************/
class FE_DL_EXPORT FollicleOp:
	public OperatorState,
	public Initialize<FollicleOp>
{
	public:

						FollicleOp(void):
							m_brushed(FALSE),
							m_lastX(-1),
							m_lastY(-1),
							m_lastUV(-1,-1),
							m_dragging(FALSE),
							m_clearOnRelease(FALSE),
							m_areaSelect(FALSE),
							m_addShowStash(0),
							m_addReplacement(0.0,0.0,0.0),
							m_addRadius(0.0),
							m_addFalloff(0.0),
							m_addWheel(0),
							m_addOpacity(0)									{}

virtual					~FollicleOp(void)									{}

		void			initialize(void);

						//* As HandlerI
virtual	void			handle(Record& a_rSignal);

	protected:

virtual	BWORD			undo(String a_label,sp<Counted> a_spCounted);
virtual	BWORD			redo(String a_label,sp<Counted> a_spCounted);

		void			syncToInput(void);

		void			createFollicle(const SpatialVector& a_intersection);

		Record			getEdit(I32 a_index);
		void			movePicked(const SpatialVector& a_rDelta);
		void			slideOnUV(I32 a_index,const Vector2 a_deltaUV);
		void			snapToSurface(I32 a_index);

		void			setValueLimited(Record a_edit,SpatialVector a_value);
		void			addValue(SpatialVector a_increase,
								SpatialVector a_mask,BWORD a_flood);
		void			scaleValue(Real a_scale,
								SpatialVector a_mask,BWORD a_flood);
		void			paintValue(SpatialVector a_replacement,
								SpatialVector a_mask,BWORD a_flood);
		void			restoreValue(SpatialVector a_mask,BWORD a_flood);

		BWORD			calcWeightedAverage(SpatialVector& rAverage,
								BWORD a_flood);
		BWORD			calcLimits(SpatialVector& rMinimum,
								SpatialVector& rMaximum,BWORD a_flood);
		void			smoothValue(SpatialVector a_mask,BWORD a_flood);

		void			conformValue(BWORD a_flood);
		void			revertValue(BWORD a_flood);

		void			convergeValue(SpatialVector a_mask,BWORD a_flood);
		void			divergeValue(SpatialVector a_mask,BWORD a_flood);

		BWORD			revert(I32 a_index);
		BWORD			hideToggle(I32 a_index);
		BWORD			hidden(I32 a_index);
		void			hide(I32 a_index);

		BWORD			moved(I32 a_index);

virtual	void			setupState(void);
virtual	BWORD			loadState(const String& rBuffer);

	private:
		BWORD			sampleValue(SpatialVector a_point,I32 a_hitLimit,
								Vector2& a_rUv,SpatialVector& a_rSample);
		void			appendPoint(Record a_edit);

		void			clearPositionalCaches(void);

		void			updateAction(String& a_rAction);
		void			updateNeighbors(void);

		SpatialVector	inputValue(I32 a_pointIndex);

		sp<SurfaceAccessibleI>			m_spOutputAccessible;
		sp<SurfaceAccessorI>			m_spInputElement;
		sp<SurfaceAccessorI>			m_spOutputUV;
		sp<SurfaceAccessorI>			m_spInputAttribute;
		sp<SurfaceAccessorI>			m_spOutputAttribute;
		sp<SurfaceAccessorI>			m_spHideGroup;
		sp<SurfaceAccessorI>			m_spDriverUV;
		sp<SurfaceI>					m_spDriver;
		sp<SurfaceI>					m_spPoints;
		sp<SurfaceI::GaugeI>			m_spDriverGauge;
		WindowEvent						m_event;
		String							m_attributeType;
		Array<SpatialVector>			m_inputPosition;
		Array<SpatialVector>			m_outputPosition;
		Array<SpatialVector>			m_inputValue;
		Array<I32>						m_elementTransformed;
		Array<SpatialTransform>			m_elementTransform;
		Array<I32>						m_elementOfPoint;
		Array<I32>						m_elementDirty;

		sp<DrawI>						m_spOutputDraw;
		sp<DrawMode>					m_spWireframe;
		sp<DrawMode>					m_spNarrowWire;
		sp<DrawMode>					m_spWideWire;
		sp<DrawMode>					m_spSolid;
		sp<DrawMode>					m_spHazy;
		sp<DrawMode>					m_spForeshadow;
		sp<DrawMode>					m_spLittleDot;
		sp<DrawMode>					m_spBigDot;
		sp<DrawMode>					m_spBehind;

		sp< RecordMap<I32> >			m_spEditMap;

		Accessor<SpatialVector>			m_aSyncLocation;
		Accessor<SpatialVector>			m_aLocation;
		Accessor<Vector2>				m_aUV;
		Accessor<SpatialVector>			m_aValue;

		BWORD							m_brushed;
		I32								m_lastX;
		I32								m_lastY;
		Vector2							m_lastUV;
		SpatialVector					m_pickPoint;
		SpatialVector					m_pickNormal;
		String							m_changing;

		Array<SpatialVector>			m_facingArray;
		Array< Array<I32> >				m_neighborTable;
		Array< sp<SurfaceI::ImpactI> >	m_impactArray;
		Array< Array<Vector3i> >		m_triangleArrays;

		Array<Real>						m_weightArray;
		Array<Color>					m_colorRim;
		Array<Color>					m_colorCenter;
		BWORD							m_dragging;
		BWORD							m_clearOnRelease;
		BWORD							m_areaSelect;
		I32								m_addShowStash;
		SpatialVector					m_addReplacement;
		Real							m_addRadius;
		Real							m_addFalloff;
		I32								m_addWheel;
		Real							m_addOpacity;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __operator_FollicleOp_h__ */
