/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __operator_MirrorOp_h__
#define __operator_MirrorOp_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Copy points across an axis

	@ingroup operator
*//***************************************************************************/
class FE_DL_EXPORT MirrorOp:
	public OperatorThreaded,
	public Initialize<MirrorOp>
{
	public:

					MirrorOp(void)											{}
virtual				~MirrorOp(void)											{}

		void		initialize(void);

					//* As HandlerI
virtual	void		handle(Record& a_rSignal);

					using OperatorThreaded::run;

virtual	void		run(I32 a_id,sp<SpannedRange> a_spRange);

	private:

		sp<SurfaceAccessibleI>	m_spInputAccessible;
		sp<SurfaceI>			m_spDriver;

		Array<String>			m_partitionOf;
		Array<I32>				m_shouldDelete;
		Array<I32>				m_shouldMirror;
		Array<SpatialVector>	m_mirrorOf;

		Array<String>			m_deletePatternArray;
		Array<String>			m_mirrorPatternArray;
		BWORD					m_deletingParts;
		BWORD					m_mirroringParts;

		I32						m_mirrorAxis;
		String					m_lastPartitionAttr;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __operator_MirrorOp_h__ */
