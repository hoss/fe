/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __operator_OperatorSurfaceMetric_h__
#define __operator_OperatorSurfaceMetric_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
    @brief Evaluate some aspect of a number of surfaces

	@ingroup operator
*//***************************************************************************/
class FE_DL_EXPORT SurfaceMetricOp:
	public OperatorSurfaceCommon,
	public Initialize<SurfaceMetricOp>
{
	public:
				SurfaceMetricOp(void)										{}
virtual			~SurfaceMetricOp(void)										{}

		void	initialize(void);

				//* As HandlerI
virtual void	handle(Record& a_rSignal);
};

} /* namespace ext */
} /* namespace fe */

#endif /* __operator_OperatorSurfaceMetric_h__ */
