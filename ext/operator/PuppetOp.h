/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __operator_PuppetOp_h__
#define __operator_PuppetOp_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief manipulate joint transforms

	@ingroup operator
*//***************************************************************************/
class FE_DL_EXPORT PuppetOp:
	public OperatorState,
	public Initialize<PuppetOp>
{
	public:

						PuppetOp(void):
							m_brushed(FALSE),
							m_dragging(FALSE),
							m_showAll(FALSE)								{}

virtual					~PuppetOp(void)										{}

		void			initialize(void);

						//* As HandlerI
virtual	void			handle(Record& a_rSignal);

	protected:

virtual	void			setupState(void);
virtual	BWORD			loadState(const String& a_rBuffer);

	private:
		void			addParam(String a_key,String a_label,Real a_value,
							Real a_min,Real a_max,Color a_color);

		Color			makeColor(Color a_color,BWORD a_bright);
		void			drawJoint(sp<DrawI>& a_rspDrawI,
							const SpatialVector& a_rTip,
							const SpatialVector* a_pCorner,
							const Color* a_pColor);

		Record			lookupJoint(String a_jointName);
		void			deleteJoint(String a_jointName);
		void			deleteJoints(void);

		Real			lookupShift(Record a_record,U32 a_axis);
		Real			lookupStretch(Record a_record,U32 a_axis);
		Real			lookupBend(Record a_record,U32 a_axis);
		Real			lookupResize(Record a_record,U32 a_axis);
		Real			lookupRescale(Record a_record,U32 a_axis);
		Real			lookupForward(Record a_record,U32 a_axis);
		Real			lookupBackward(Record a_record,U32 a_axis);

		sp<ManipulatorI>					m_spManipulator;
		sp<SurfaceAccessibleI>				m_spOutputAccessible;

		sp<DrawMode>						m_spWireframe;
		sp<DrawMode>						m_spThick;
		sp<DrawMode>						m_spOverlay;
		sp<DrawMode>						m_spSolid;

		BWORD								m_brushed;
		WindowEvent							m_event;
		BWORD								m_dragging;
		I32									m_lastX;
		I32									m_lastY;

		BWORD								m_showAll;
		String								m_pickName;

		std::map<String,SpatialTransform>	m_origMap;
		std::map<String,SpatialTransform>	m_animMap;
		std::map<String,I32>				m_posePrimitiveMap;
		std::map<String,I32>				m_animPrimitiveMap;
		std::map<String,I32>				m_offsetPrimitiveMap;

		sp< RecordMap<String> >				m_spJointMap;
		Accessor<String>					m_aName;
		Accessor<SpatialVector>				m_aShift;
		Accessor<SpatialVector>				m_aStretch;
		Accessor<SpatialVector>				m_aBend;
		Accessor<SpatialVector>				m_aResize;
		Accessor<SpatialVector>				m_aRescale;
		Accessor<SpatialVector>				m_aForward;
		Accessor<SpatialVector>				m_aBackward;

};

} /* namespace ext */
} /* namespace fe */

#endif /* __operator_PuppetOp_h__ */
