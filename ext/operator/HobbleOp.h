/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __operator_HobbleOp_h__
#define __operator_HobbleOp_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Operator to restrain surfaces from walking away

	@ingroup operator
*//***************************************************************************/
class FE_DL_EXPORT HobbleOp:
	public OperatorSurfaceCommon,
	public Initialize<HobbleOp>
{
	public:

					HobbleOp(void)											{}
virtual				~HobbleOp(void)											{}

		void		initialize(void);

					//* As HandlerI
virtual	void		handle(Record& a_rSignal);

	private:
};

} /* namespace ext */
} /* namespace fe */

#endif /* __operator_HobbleOp_h__ */
