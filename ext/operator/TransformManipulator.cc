/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <operator/operator.pmh>

#define FE_TRM_DEBUG		FALSE

using namespace fe;
using namespace fe::ext;

void TransformManipulator::initialize(void)
{
	SpatialTransform& rAnchor=catalog<SpatialTransform>("anchor");
	setIdentity(rAnchor);

	SpatialTransform& rPivot=catalog<SpatialTransform>("pivot");
	setIdentity(rPivot);

	SpatialTransform& rTransform=catalog<SpatialTransform>("transform");
	setIdentity(rTransform);

	catalog<Real>("tx")=0.0;
	catalog<Real>("ty")=0.0;
	catalog<Real>("tz")=0.0;
	catalog<Real>("rx")=0.0;
	catalog<Real>("ry")=0.0;
	catalog<Real>("rz")=0.0;
	catalog<Real>("px")=0.0;
	catalog<Real>("py")=0.0;
	catalog<Real>("halo")=0.0;
	catalog<Real>("sx")=0.0;
	catalog<Real>("sy")=0.0;
	catalog<Real>("sz")=0.0;

	catalog<bool>("pivotTranslate")=true;
	catalog<bool>("pivotRotate")=true;
	catalog<bool>("pivotScale")=false;

	catalog<bool>("deformTranslate")=true;
	catalog<bool>("deformRotate")=true;
	catalog<bool>("deformScale")=true;

	//* t*3 t2*3 r*3 r2*3 p*2 h*2 s*3
	m_gripArray.resize(18);

	Grip& rGripTx=m_gripArray[0];
	rGripTx.m_key[0]="tx";
	rGripTx.m_message="Translate X";
	rGripTx.m_label="tX";

	Grip& rGripTy=m_gripArray[1];
	rGripTy.m_key[0]="ty";
	rGripTy.m_message="Translate Y";
	rGripTy.m_label="tY";

	Grip& rGripTz=m_gripArray[2];
	rGripTz.m_key[0]="tz";
	rGripTz.m_message="Translate Z";
	rGripTz.m_label="tZ";

	Grip& rGripTxy=m_gripArray[3];
	rGripTxy.m_key[0]="tx";
	rGripTxy.m_key[1]="ty";
	rGripTxy.m_message="Translate XY";
	rGripTxy.m_label="tXY";

	Grip& rGripTyz=m_gripArray[4];
	rGripTyz.m_key[0]="ty";
	rGripTyz.m_key[1]="tz";
	rGripTyz.m_message="Translate YZ";
	rGripTyz.m_label="tYZ";

	Grip& rGripTzx=m_gripArray[5];
	rGripTzx.m_key[0]="tz";
	rGripTzx.m_key[1]="tx";
	rGripTzx.m_message="Translate XZ";
	rGripTzx.m_label="tXZ";

	Grip& rGripRx=m_gripArray[6];
	rGripRx.m_key[0]="rx";
	rGripRx.m_message="Rotate X";
	rGripRx.m_label="rX";

	Grip& rGripRy=m_gripArray[7];
	rGripRy.m_key[0]="ry";
	rGripRy.m_message="Rotate Y";
	rGripRy.m_label="rY";

	Grip& rGripRz=m_gripArray[8];
	rGripRz.m_key[0]="rz";
	rGripRz.m_message="Rotate Z";
	rGripRz.m_label="rZ";

	Grip& rGripRyz=m_gripArray[9];
	rGripRyz.m_key[0]="ry";
	rGripRyz.m_key[1]="rz";
	rGripRyz.m_message="Rotate YZ";
	rGripRyz.m_label="rYZ";

	Grip& rGripRzx=m_gripArray[10];
	rGripRzx.m_key[0]="rz";
	rGripRzx.m_key[1]="rx";
	rGripRzx.m_message="Rotate XZ";
	rGripRzx.m_label="rXZ";

	Grip& rGripRxy=m_gripArray[11];
	rGripRxy.m_key[0]="rx";
	rGripRxy.m_key[1]="ry";
	rGripRxy.m_message="Rotate XY";
	rGripRxy.m_label="rXY";

	Grip& rGripP=m_gripArray[12];
	rGripP.m_key[0]="px";
	rGripP.m_key[1]="py";
	rGripP.m_message="Screen Pan";
	rGripP.m_label="pan";

	Grip& rGripH0=m_gripArray[13];
	rGripH0.m_key[0]="halo";
	rGripH0.m_message="Screen Rotate";
	rGripH0.m_label="spin";

	Grip& rGripH1=m_gripArray[14];
	rGripH1.m_key[0]="halo";
	rGripH1.m_message="Screen Rotate";
	rGripH1.m_label="spin";

	Grip& rGripSx=m_gripArray[15];
	rGripSx.m_key[0]="sx";
	rGripSx.m_message="Scale X";
	rGripSx.m_label="sX";

	Grip& rGripSy=m_gripArray[16];
	rGripSy.m_key[0]="sy";
	rGripSy.m_message="Scale Y";
	rGripSy.m_label="sY";

	Grip& rGripSz=m_gripArray[17];
	rGripSz.m_key[0]="sz";
	rGripSz.m_message="Scale Z";
	rGripSz.m_label="sZ";

	m_spCircleAura=new DrawMode();
	m_spCircleAura->setDrawStyle(DrawMode::e_solid);
	m_spCircleAura->setAntialias(TRUE);
	m_spCircleAura->setLit(FALSE);
}

void TransformManipulator::handle(Record& a_rSignal)
{
	//* HACK (should use signaler)
	sp<SignalerI> spSignaler;
	sp<Layout> spLayout=a_rSignal.layout();
	sp<Scope> spScope=spLayout->scope();
	handleBind(spSignaler,spLayout);

	m_event.bind(windowEvent(a_rSignal));

	if(m_event.isKeyPress(WindowEvent::e_keyCarriageReturn))
	{
		m_hotGrip= -1;
		m_lastGrip= -1;

		m_mode=(m_mode==e_pivot)? e_deform: e_pivot;

		if(m_mode==e_deform &&
				!catalog<bool>("deformTranslate") &&
				!catalog<bool>("deformRotate") &&
				!catalog<bool>("deformScale"))
		{
			m_mode=e_pivot;
		}
		if(m_mode==e_pivot &&
				!catalog<bool>("pivotTranslate") &&
				!catalog<bool>("pivotRotate") &&
				!catalog<bool>("pivotScale"))
		{
			m_mode=e_deform;
		}

		return;
	}

	ManipulatorCommon::handle(a_rSignal);
}

void TransformManipulator::updateTransform(void)
{
	const SpatialTransform& anchor=catalog<SpatialTransform>("anchor");
	SpatialTransform& rPivot=catalog<SpatialTransform>("pivot");
	SpatialTransform& rTransform=catalog<SpatialTransform>("transform");
	const SpatialTransform space=(m_mode==e_pivot)? anchor: rPivot*anchor;
	SpatialTransform spaceInv;
	invert(spaceInv,space);
	const SpatialTransform local=(m_mode==e_pivot)? rPivot: rTransform;

/*	NOTE Scale is the magnitude each of first three columns.
	Normalize those columns to get a clean rotation.  */

	m_unscaled=local;

	set(m_scaling,
			magnitude(m_unscaled.column(0)),
			magnitude(m_unscaled.column(1)),
			magnitude(m_unscaled.column(2)));

	m_unscaled.column(0)*=1.0/m_scaling[0];
	m_unscaled.column(1)*=1.0/m_scaling[1];
	m_unscaled.column(2)*=1.0/m_scaling[2];

	m_unscaled=m_unscaled*space;

#if FE_TRM_DEBUG
	feLog("anchor:\n%s\n",c_print(anchor));
	feLog("pivot:\n%s\n",c_print(rPivot));
	feLog("space:\n%s\n",c_print(space));
	feLog("transform:\n%s\n",c_print(rTransform));
	feLog("scaling %s\n",c_print(m_scaling));
	feLog("local:\n%s\n",c_print(local));
	feLog("local*space:\n%s\n",c_print(local*space));
	feLog("unscaled:\n%s\n",c_print(m_unscaled));
	feLog("rightways:\n%s\n",c_print(m_rightways));
	feLog("upwards:\n%s\n",c_print(m_upwards));
#endif

	Real& rTx=catalog<Real>("tx");
	Real& rTy=catalog<Real>("ty");
	Real& rTz=catalog<Real>("tz");
	Real& rRx=catalog<Real>("rx");
	Real& rRy=catalog<Real>("ry");
	Real& rRz=catalog<Real>("rz");
	Real& rSx=catalog<Real>("sx");
	Real& rSy=catalog<Real>("sy");
	Real& rSz=catalog<Real>("sz");
	Real& rPx=catalog<Real>("px");
	Real& rPy=catalog<Real>("py");
	Real& rHalo=catalog<Real>("halo");

#if FE_TRM_DEBUG
	feLog("rotating %.6G %.6G %.6G\n",rRx,rRy,rRz);
	feLog("translating %.6G %.6G %.6G\n",rTx,rTy,rTz);
	feLog("scaling %.6G %.6G %.6G\n",rSx,rSy,rSz);
	feLog("screen %.6G %.6G halo %.6G\n",rPx,rPy,rHalo);
#endif

	const SpatialQuaternion haloRotation(rHalo,m_facing);
	const SpatialTransform preEuler=
			m_unscaled*SpatialTransform(haloRotation)*spaceInv;
	SpatialEuler localEuler=preEuler;
	const SpatialVector localTranslation=local.translation();

//	localEuler+=SpatialVector(rRx,rRy,rRz);
	localEuler=SpatialTransform(SpatialEuler(rRx,rRy,rRz))*
			SpatialTransform(localEuler);

	m_unscaled=localEuler;
	setTranslation(m_unscaled,localTranslation);

#if FE_TRM_DEBUG
	feLog("preEuler:\n%s\n",c_print(preEuler));
	feLog("post Euler:\n%s\n",c_print(m_unscaled));
#endif

	m_unscaled=m_unscaled*space;

	const SpatialVector addTranslation=SpatialVector(rTx,rTy,rTz);
	translate(m_unscaled,addTranslation);
	m_unscaled.translation()+=rPx*m_rightways+rPy*m_upwards;

	m_scaling+=SpatialVector(rSx,rSy,rSz);

	//* TEMP keep scale>0
	const Real scaleMin=1e-3;
	for(U32 m=0;m<3;m++)
	{
		if(m_scaling[m]<scaleMin)
		{
			m_scaling[m]=scaleMin;
		}
	}

	//* TODO add proper scale operations to Matrix3x4 class

	SpatialTransform scaleMatrix;
	setIdentity(scaleMatrix);
	scale(scaleMatrix,m_scaling);

	if(m_mode==e_pivot)
	{
		rPivot=m_unscaled*spaceInv;
	}
	else
	{
		rTransform=scaleMatrix*m_unscaled*spaceInv;
	}

#if FE_TRM_DEBUG
	feLog("localEuler:\n%s\n",c_print(localEuler));
	feLog("new unscaled:\n%s\n",c_print(m_unscaled));
	feLog("new transform:\n%s\n",c_print(rTransform));
#endif

	rTx=0.0;
	rTy=0.0;
	rTz=0.0;
	rRx=0.0;
	rRy=0.0;
	rRz=0.0;
	rSx=0.0;
	rSy=0.0;
	rSz=0.0;
	rPx=0.0;
	rPy=0.0;
	rHalo=0.0;
}

void TransformManipulator::updateGrips(void)
{
#if FE_TRM_DEBUG
	feLog("TransformManipulator::updateGrips\n");
#endif

	ManipulatorCommon::updateGrips();

	updateTransform();

	const SpatialTransform& anchor=catalog<SpatialTransform>("anchor");
	const SpatialTransform& pivot=catalog<SpatialTransform>("pivot");
	const SpatialTransform& transform=catalog<SpatialTransform>("transform");

	sp<ViewI> spView=m_spDrawCache->view();
	const Box2i viewport=spView->viewport();
	const I32 width=viewport.size()[0];
	const I32 height=viewport.size()[1];

	const Real dark=1.0-dimming();
	const Real alpha=foreshadow();
	const Real gripScalar=gripScale();

	const Color black(0.0,0.0,0.0,alpha);
	const Color red(1.0,0.0,0.0,alpha);
	const Color green(0.0,1.0,0.0,alpha);
	const Color blue(0.0,0.0,1.0,alpha);
	const Color cyan(0.0,1.0,1.0,alpha);
	const Color magenta(1.0,0.0,1.0,alpha);
	const Color yellow(1.0,1.0,0.0,alpha);
	const Color white(1.0,1.0,1.0,alpha);
	const Color lightblue(0.5,0.5,1.0,alpha);

	const Color auraColor(1.0,1.0,0.0,aura());

	const SpatialVector unitAxis[3]={
			SpatialVector(1.0,0.0,0.0),
			SpatialVector(0.0,1.0,0.0),
			SpatialVector(0.0,0.0,1.0)};

	const OperateCommon::CurveMode curveMode=OperateCommon::e_tube;

	const SpatialVector towardCamera(0.0,0.0,1.0);

	const U32 resolution=16;

	const Real spikeLength=1.0*gripScalar;
	const Real spikeRadius=0.05*gripScalar;	//* TODO tweak

	SpatialVector pointArray[resolution];
	SpatialVector normalArray[resolution];
	Real radiusArray[resolution];
	Color colorArray[resolution];

	SpatialVector hotPoint;
	SpatialVector gripStart;
	SpatialVector gripEnd;
	I32 hotIndex=0;

	const SpatialVector textPoint(0.5*width,
			height-4*m_spOverlayCache->font()->fontHeight(NULL,NULL),1.0);
	drawLabel(m_spOverlayCache,textPoint,
			(m_mode==e_pivot)? "Pivot": "Deform",
			TRUE,3,green,NULL,&black);

	//* anchor
	Color anchorColor=lightblue;
	anchorColor[3]=alpha;
	drawAnchor(m_spDrawCache,anchor,anchorScale(),resolution,anchorColor);

	//* flame, outward or inward
	const Real scaleAxes=0.3*gripScalar;				//* tweak
	const Real radiusAxes=0.1*scaleAxes;				//* tweak
	const Real peakOffset=(m_mode==e_pivot)? 0.3: -0.3;	//* tweak

	Array<Real> profile(resolution);
	for(U32 m=0;m<resolution;m++)
	{
		const Real fraction=m/(resolution-1.0);
		profile[m]=radiusAxes*flameProfile(peakOffset,fraction);
	}

	SpatialTransform secondary=(m_mode==e_deform)?
			transform*pivot*anchor: pivot*anchor;
	normalizeSafe(secondary.column(0));
	normalizeSafe(secondary.column(1));
	normalizeSafe(secondary.column(2));
	drawTubeAxes(m_spDrawCache,secondary,scaleAxes,foreshadow(),
			profile,curveMode);

	const Real spikeOffset=(m_mode==e_pivot)? -0.2: 0.4;	//* tweak

	SpatialTransform xform;

	Real angleOffset=20.0*degToRad;	//* TODO tweak
	const Real angleSpan=90.0*degToRad-2.0*angleOffset;

	if((m_mode==e_pivot && !catalog<bool>("pivotTranslate")) ||
			(m_mode==e_deform && !catalog<bool>("deformTranslate")))
	{
		for(U32 axis=0;axis<6;axis++)
		{
			set(m_gripArray[hotIndex++].m_hotSpot,-1,-1);
		}
	}
	else
	{
		//* translation spikes
		for(U32 axis=0;axis<3;axis++)
		{
			Real maxRadius=1e-6;

			SpatialVector dir;
			rotateVector(m_unscaled,unitAxis[axis],dir);
			const SpatialVector side=cross(dir,m_facing);
			const SpatialVector norm=unitSafe(cross(side,dir));

			for(U32 m=0;m<resolution;m++)
			{
				const Real fraction=m/(resolution-1.0);

				set(pointArray[m]);
				pointArray[m][axis]=0.8*spikeLength*fraction;
				transformVector(m_unscaled,pointArray[m],pointArray[m]);

				normalArray[m]=norm;
//				set(normalArray[m]);
//				normalArray[m][(axis+1)%3]=1.0;
//				rotateVector(m_unscaled,normalArray[m],normalArray[m]);

				radiusArray[m]=spikeRadius*flameProfile(spikeOffset,fraction);

				if(maxRadius<radiusArray[m])
				{
					maxRadius=radiusArray[m];
				}
			}

			for(U32 m=0;m<resolution;m++)
			{
				set(colorArray[m]);
				colorArray[m][axis]=pow(radiusArray[m]/maxRadius,0.5);
				if(m_hotGrip!=hotIndex)
				{
					colorArray[m]*=dark;
				}

				colorArray[m][3]=alpha;
			}

			colorArray[resolution-1]=black;

			const BWORD multicolor=TRUE;
			drawTubeWithAura(m_spDrawCache,
					pointArray,normalArray,radiusArray,
					resolution,curveMode,multicolor,colorArray,towardCamera,
					(hotIndex==m_lastGrip)? 1.5: 0.0);

			set(gripStart);
			set(gripEnd);
			gripEnd[axis]=1.0;
			transformVector(m_unscaled,gripStart,gripStart);
			transformVector(m_unscaled,gripEnd,gripEnd);
			m_gripArray[hotIndex].m_alignment[0]=gripEnd-gripStart;
			m_gripArray[hotIndex].m_planeCenter=gripStart;
			m_gripArray[hotIndex].m_planeFacing=normalArray[0];

			set(hotPoint);
			hotPoint[axis]=spikeLength*((m_mode==e_pivot)? 0.2: 0.6);
			transformVector(m_unscaled,hotPoint,hotPoint);
			m_gripArray[hotIndex++].m_hotSpot=
					spView->project(hotPoint,ViewI::e_perspective);
		}

		//* dual-translation disks

		//* tweak
		const Real diskRadius=gripScalar*((m_mode==e_pivot)? 0.05: 0.1);
		const Real diskOffset=gripScalar*((m_mode==e_pivot)? 0.5: 0.2);

		const SpatialVector diskScale(diskRadius,diskRadius,diskRadius);
		const SpatialVector diskScaleAura=1.3*diskScale;
		const SpatialVector tinyZ(0.0,0.0,1e-3);

		const Color t2Color[3]= { yellow, cyan, magenta };

		setIdentity(xform);

		for(U32 axis=0;axis<3;axis++)
		{
			SpatialVector arm(diskOffset,diskOffset,diskOffset);
			arm[(axis+2)%3]=0.0;
			if(axis)
			{
				xform=(axis==2)? SpatialQuaternion(-90.0*degToRad,e_xAxis):
						SpatialQuaternion(90.0*degToRad,e_yAxis);
			}
			setTranslation(xform,arm);
			SpatialTransform cat=xform*m_unscaled;

			const Real axisDot=dot(cat.column(2),m_facing);
			if(axisDot<0.0)
			{
				rotate(xform,180.0*degToRad,Axis(axis%2));
				cat=xform*m_unscaled;
			}

			Color diskColor=t2Color[axis]*(m_hotGrip==hotIndex? 1.0: dark);
			diskColor[3]=alpha;

			m_spDrawCache->drawCircle(cat,&diskScale,diskColor);

			m_gripArray[hotIndex].m_alignment[0]=
					m_gripArray[hotIndex-3].m_alignment[0];
			m_gripArray[hotIndex].m_alignment[1]=
					m_gripArray[(hotIndex+1)%3].m_alignment[0];
			m_gripArray[hotIndex].m_planeCenter=gripStart;
			m_gripArray[hotIndex].m_planeFacing=
					m_gripArray[(hotIndex+2)%3].m_alignment[0];
			m_gripArray[hotIndex].m_hotSpot=
					spView->project(cat.translation(),ViewI::e_perspective);

			if(hotIndex==m_lastGrip)
			{
				translate(cat,-tinyZ);
				m_spDrawCache->pushDrawMode(m_spCircleAura);
				m_spDrawCache->drawCircle(cat,&diskScaleAura,auraColor);
				m_spDrawCache->popDrawMode();
			}

			hotIndex++;
		}
	}

	if((m_mode==e_pivot && !catalog<bool>("pivotRotate")) ||
			(m_mode==e_deform && !catalog<bool>("deformRotate")))
	{
		for(U32 axis=0;axis<6;axis++)
		{
			set(m_gripArray[hotIndex++].m_hotSpot,-1,-1);
		}
	}
	else
	{
		//* rotation tubes
		for(U32 axis=0;axis<3;axis++)
		{
			SpatialVector arm(0.0,0.0,0.0);
			arm[(axis+1)%3]=spikeLength;

			for(U32 m=0;m<resolution;m++)
			{
				const Real fraction=m/(resolution-1.0);
				const Real angle=angleOffset+angleSpan*fraction;

				xform=SpatialQuaternion(angle,(Axis)axis);

				transformVector(xform*m_unscaled,arm,pointArray[m]);

				set(normalArray[m]);
				normalArray[m][axis]=1.0;

				set(colorArray[m]);
				colorArray[m][axis]=1.0;
				if(m_hotGrip!=hotIndex)
				{
					colorArray[m]*=dark;
				}
				colorArray[m][3]=alpha;

				radiusArray[m]=0.3*spikeRadius;
			}

			const BWORD multicolor=TRUE;
			drawTubeWithAura(m_spDrawCache,
					pointArray,normalArray,radiusArray,
					resolution,curveMode,multicolor,colorArray,towardCamera,
					(hotIndex==m_lastGrip)? 2.0: 0.0);

			gripStart=pointArray[0];
			gripEnd=pointArray[resolution-1];
			hotPoint=pointArray[resolution/2];

			m_gripArray[hotIndex].m_alignment[0]=gripEnd-gripStart;
			m_gripArray[hotIndex].m_planeCenter=gripStart;
//			m_gripArray[hotIndex].m_planeFacing=
//					m_gripArray[hotIndex%3].m_alignment[0];

			m_gripArray[hotIndex++].m_hotSpot=
					spView->project(hotPoint,ViewI::e_perspective);
		}

		//* dual-rotation ellipses (YZ, ZX, XY)

		const Color rot2Color[3]= { cyan, magenta, yellow };
		const Real ellipseLength=2.0*spikeRadius;
		const Real ellipseRadius=spikeRadius;

		for(U32 axis=0;axis<3;axis++)
		{
			for(U32 m=0;m<resolution;m++)
			{
				const Real fraction=m/(resolution-1.0);

				set(pointArray[m]);
				pointArray[m][axis]=gripScalar+ellipseLength*(fraction-0.5);
				transformVector(m_unscaled,pointArray[m],pointArray[m]);

				set(normalArray[m]);
				normalArray[m][(axis+1)%3]=1.0;
				rotateVector(m_unscaled,normalArray[m],normalArray[m]);

				const Real f2=fraction*2.0-1.0;
				const Real mag=sqrt(1.0-f2*f2);

				radiusArray[m]=ellipseRadius*mag;

				colorArray[m]=rot2Color[axis]*mag;
				if(m_hotGrip!=hotIndex)
				{
					colorArray[m]*=dark;
				}
				colorArray[m][3]=alpha;
			}

			hotPoint=pointArray[resolution/2];

			const BWORD multicolor=TRUE;
			drawTubeWithAura(m_spDrawCache,
					pointArray,normalArray,radiusArray,
					resolution,curveMode,multicolor,colorArray,towardCamera,
					(hotIndex==m_lastGrip)? 1.5: 0.0);

			m_gripArray[hotIndex].m_alignment[0]=
					-m_gripArray[(hotIndex+2)%3].m_alignment[0];
			m_gripArray[hotIndex].m_alignment[1]=
					m_gripArray[(hotIndex+1)%3].m_alignment[0];
			m_gripArray[hotIndex].m_planeCenter=hotPoint;
//			m_gripArray[hotIndex].m_planeFacing=
//					m_gripArray[hotIndex%3].m_alignment[0];
			m_gripArray[hotIndex++].m_hotSpot=
					spView->project(hotPoint,ViewI::e_perspective);
		}
	}

	const SpatialVector absFacing(fabs(m_facing[0]),
			fabs(m_facing[1]),fabs(m_facing[2]));

	SpatialVector notFacing=SpatialVector(1.0,1.0,1.0)-absFacing;
	Real max=notFacing[0]>notFacing[1]?
		(notFacing[0]>notFacing[2]? notFacing[0]: notFacing[2]):
		(notFacing[1]>notFacing[2]? notFacing[1]: notFacing[2]);
	notFacing*=1.0/max;

	SpatialTransform xformScreen;
	makeFrameNormalY(xformScreen,m_unscaled.translation(),
			m_unscaled.column(1),m_facing);

	if((m_mode==e_pivot && !catalog<bool>("pivotTranslate")) ||
			(m_mode==e_deform && !catalog<bool>("deformTranslate")))
	{
		set(m_gripArray[hotIndex++].m_hotSpot,-1,-1);
	}
	else
	{
		//* panning box

		SpatialQuaternion rotation;
		set(rotation,unitAxis[2],m_facing);

		xform=rotation;
//		setTranslation(xform,m_unscaled.translation()-1.8*m_upwards);
		setTranslation(xform,
				m_unscaled.translation()+1.8*gripScalar*xformScreen.column(0));

		Color panColor(notFacing[0],notFacing[1],notFacing[2],1.0);
		if(m_hotGrip!=hotIndex)
		{
			panColor*=dark;
		}
		panColor[3]=1.0;

		const Vector2i projected=
				spView->project(xform.translation(),ViewI::e_perspective);
		const Real pixels=spView->pixelSize(xform.translation(),spikeRadius);

		SpatialVector corner[2];
		corner[0]=projected-Vector2i(pixels,pixels);
		corner[1]=projected+Vector2i(pixels,pixels);
		corner[0][2]=0.0;
		corner[1][2]=0.0;
		m_spOverlayCache->drawRectangles(corner,2,FALSE,&panColor);

		m_gripArray[hotIndex].m_alignment[0]=m_rightways;
		m_gripArray[hotIndex].m_alignment[1]=m_upwards;
		m_gripArray[hotIndex].m_planeCenter=xform.translation();
		m_gripArray[hotIndex].m_planeFacing=m_facing;
		m_gripArray[hotIndex].m_hotSpot=
				spView->project(xform.translation(),ViewI::e_perspective);

		if(hotIndex==m_lastGrip)
		{
			corner[0]-=SpatialVector(2.0,2.0);
			corner[1]+=SpatialVector(2.0,2.0);
			corner[0][2]=1e-3;
			corner[1][2]=1e-3;

			m_spOverlayCache->pushDrawMode(m_spCircleAura);
			m_spOverlayCache->drawRectangles(corner,2,FALSE,&auraColor);
			m_spOverlayCache->popDrawMode();
		}

		hotIndex++;
	}

	if((m_mode==e_pivot && !catalog<bool>("pivotRotate")) ||
			(m_mode==e_deform && !catalog<bool>("deformRotate")))
	{
		for(U32 axis=0;axis<2;axis++)
		{
			set(m_gripArray[hotIndex++].m_hotSpot,-1,-1);
		}
	}
	else
	{
		//* halo (screen space rotation)

		SpatialVector satFacing=absFacing;
		max=satFacing[0]>satFacing[1]?
			(satFacing[0]>satFacing[2]? satFacing[0]: satFacing[2]):
			(satFacing[1]>satFacing[2]? satFacing[1]: satFacing[2]);
		satFacing*=1.0/max;

		const Color haloColor(satFacing[0],satFacing[1],satFacing[2],1.0);

		angleOffset=30.0*degToRad-0.5*angleSpan;

		SpatialVector arm(0.0,0.0,1.6*spikeLength);

		for(U32 arc=0;arc<2;arc++)
		{
			for(U32 m=0;m<resolution;m++)
			{
				const Real fraction=m/(resolution-1.0);
				const Real angle=angleOffset+angleSpan*fraction;

				const SpatialTransform spin(SpatialQuaternion(angle,e_yAxis));

				transformVector(spin*xformScreen,arm,pointArray[m]);

				normalArray[m]=m_facing;

				colorArray[m]=haloColor;
				if(m_hotGrip!=hotIndex)
				{
					colorArray[m]*=dark;
				}
				colorArray[m][3]=alpha;

				radiusArray[m]=0.3*spikeRadius;
			}

			const BWORD multicolor=TRUE;
			drawTubeWithAura(m_spDrawCache,
					pointArray,normalArray,radiusArray,
					resolution,curveMode,multicolor,colorArray,towardCamera,
					(hotIndex==m_lastGrip)? 2.0: 0.0);

			gripStart=pointArray[0];
			gripEnd=pointArray[resolution-1];
			hotPoint=pointArray[resolution/2];

			m_gripArray[hotIndex].m_alignment[0]=gripEnd-gripStart;
			m_gripArray[hotIndex].m_planeCenter=gripStart;
			m_gripArray[hotIndex].m_planeFacing=m_facing;
			m_gripArray[hotIndex++].m_hotSpot=
					spView->project(hotPoint,ViewI::e_perspective);

			angleOffset+=120.0*degToRad;
		}
	}

	if((m_mode==e_pivot && !catalog<bool>("pivotScale")) ||
			(m_mode==e_deform && !catalog<bool>("deformScale")))
	{
		for(U32 axis=0;axis<3;axis++)
		{
			set(m_gripArray[hotIndex++].m_hotSpot,-1,-1);
		}
	}
	else
	{
		//* scaling cylinders

		const Real cylinderRadius=0.1*gripScalar;
		const Real cylinderLength[4]=
		{
			Real(-0.01*cylinderRadius),
			Real(0),
			Real(2.0*cylinderRadius),
			Real(2.01*cylinderRadius)
		};

		for(U32 axis=0;axis<3;axis++)
		{
			for(U32 m=0;m<4;m++)
			{
				set(pointArray[m]);
				pointArray[m][axis]=1.3*spikeLength+cylinderLength[m];
				transformVector(m_unscaled,pointArray[m],pointArray[m]);

				set(normalArray[m]);
				normalArray[m][(axis+1)%3]=1.0;
				rotateVector(m_unscaled,normalArray[m],normalArray[m]);

				set(colorArray[m]);
				colorArray[m][axis]=1.0;
				if(m_hotGrip!=hotIndex)
				{
					colorArray[m]*=dark;
				}
				colorArray[m][3]=alpha;

				if(m==2)
				{
					hotPoint=pointArray[m];
				}
			}

			//* TODO negative
			if(m_scaling[axis]<1.0)
			{
				radiusArray[1]=cylinderRadius;
				radiusArray[2]=cylinderRadius*m_scaling[axis];
			}
			else
			{
				radiusArray[1]=cylinderRadius/m_scaling[axis];
				radiusArray[2]=cylinderRadius;
			}

			radiusArray[0]=0.0;
			radiusArray[3]=0.0;

			colorArray[0]=black;
			colorArray[3]=black;

			const BWORD multicolor=TRUE;
			drawTubeWithAura(m_spDrawCache,
					pointArray,normalArray,radiusArray,
					4,curveMode,multicolor,colorArray,towardCamera,
					(hotIndex==m_lastGrip)? 1.2: 0.0);

			gripStart=pointArray[0];
			gripEnd=pointArray[3];

			m_gripArray[hotIndex].m_alignment[0]=gripEnd-gripStart;
//			m_gripArray[hotIndex].m_alignment[0]=
//					m_gripArray[hotIndex%3].m_alignment[0];

			m_gripArray[hotIndex].m_planeCenter=
					m_gripArray[hotIndex%3].m_planeCenter;
			m_gripArray[hotIndex].m_planeFacing=
					m_gripArray[hotIndex%3].m_planeFacing;

			m_gripArray[hotIndex++].m_hotSpot=
					spView->project(hotPoint,ViewI::e_perspective);
		}
	}
}
