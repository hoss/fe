/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <operator/operator.pmh>

namespace fe
{
namespace ext
{

void SurfaceWalkOp::initialize(void)
{
	catalog<String>("icon")="FE_alpha";

	catalog<I32>("firstIndex")=0;
	catalog<I32>("firstIndex","high")=100;
	catalog<I32>("firstIndex","max")=1e6;
	catalog<String>("firstIndex","label")="First Index";

	catalog<I32>("secondIndex")=1;
	catalog<I32>("secondIndex","high")=100;
	catalog<I32>("secondIndex","max")=1e6;
	catalog<String>("secondIndex","label")="Second Index";

	catalog<Real>("maxTurn")=60.0;
	catalog<Real>("maxTurn","max")=180.0;
	catalog<String>("maxTurn","label")="Max Turn Angle";

	catalog<I32>("maxPoints")=1024;
	catalog<I32>("maxPoints","high")=1e4;
	catalog<I32>("maxPoints","max")=1e6;
	catalog<String>("maxPoints","label")="Max Output Points";

	catalog< sp<Component> >("Input Surface");

	catalog< sp<Component> >("Reference Surface");
	 catalog<bool>("Reference Surface","optional")=TRUE;

	catalog< sp<Component> >("Output Curve");
	catalog<String>("Output Curve","IO")="output";
}

void SurfaceWalkOp::handle(Record& a_rSignal)
{
	const I32 firstIndex=catalog<I32>("firstIndex");
	const I32 secondIndex=catalog<I32>("secondIndex");
	const Real maxTurn=catalog<Real>("maxTurn");

	const Real minDot=cos(maxTurn*fe::degToRad);

	catalog<String>("summary")="no result";

	if(firstIndex==secondIndex)
	{
		catalog<String>("warning")="firstIndex and secondIndex are the same";
		return;
	}

	sp<SurfaceAccessibleI> spInputAccessible;
	if(!access(spInputAccessible,"Input Surface")) return;

	sp<SurfaceAccessibleI> spRefAccessible;
	if(!access(spRefAccessible,"Reference Surface",e_quiet))
	{
		spRefAccessible=spInputAccessible;
	}

	sp<SurfaceAccessibleI> spOutputAccessible;
	if(!accessOutput(spOutputAccessible,a_rSignal)) return;

	sp<DrawI> spDrawOutput;
	if(!accessDraw(spDrawOutput,a_rSignal)) return;

	sp<SurfaceAccessorI> spInputPosition;
	if(!access(spInputPosition,spInputAccessible,
			e_point,e_position)) return;

	sp<SurfaceAccessorI> spRefPosition;
	if(!access(spRefPosition,spRefAccessible,
			e_point,e_position)) return;

	sp<SurfaceAccessorI> spRefVertices;
	if(!access(spRefVertices,spRefAccessible,
			e_primitive,e_vertices)) return;

	const I32 pointCount=spInputPosition->count();
	const I32 refPointCount=spRefPosition->count();

	if(firstIndex>=pointCount || firstIndex>=refPointCount)
	{
		catalog<String>("warning")="firstIndex exceeds point count";
		return;
	}
	if(secondIndex>=pointCount || secondIndex>=refPointCount)
	{
		catalog<String>("warning")="secondIndex exceeds point count";
		return;
	}

	//* TODO cache point indexes of path while reference unchanged

	Array< std::set<I32> > pointNeighbors;
	Array< std::set<I32> > pointFaces;
	determineNeighborhood(spRefAccessible,pointFaces,pointNeighbors);

	std::set<I32> pointHit;

	I32 vertCount=0;
	I32 maxCount=catalog<I32>("maxPoints");
	SpatialVector* pCurve=new SpatialVector[maxCount];

	pCurve[vertCount++]=spInputPosition->spatialVector(firstIndex);

	SpatialVector lastRefPoint=spRefPosition->spatialVector(firstIndex);
	pointHit.insert(firstIndex);

	I32 pointIndex=secondIndex;
	while(vertCount<maxCount)
	{
		if(interrupted())
		{
			break;
		}

		pCurve[vertCount++]=spInputPosition->spatialVector(pointIndex);

		const SpatialVector refPoint=spRefPosition->spatialVector(pointIndex);
		pointHit.insert(pointIndex);

		const SpatialVector lastDir=unitSafe(refPoint-lastRefPoint);

		Real bestDot= -1.0;
		I32 bestIndex= -1;

		const std::set<I32>& rNeighbors=pointNeighbors[pointIndex];
		for(std::set<I32>::iterator it=rNeighbors.begin();
				it!=rNeighbors.end();it++)
		{
			const I32 otherIndex=*it;
			if(otherIndex>=pointCount ||
					pointHit.find(otherIndex)!=pointHit.end())
			{
				continue;
			}

			const SpatialVector otherPoint=
					spRefPosition->spatialVector(otherIndex);

			const SpatialVector candidateDir=unitSafe(otherPoint-refPoint);
			const Real candidateDot=dot(lastDir,candidateDir);

			if(candidateDot<minDot)
			{
				continue;
			}

			if(bestDot<candidateDot)
			{
				bestDot=candidateDot;
				bestIndex=otherIndex;
			}
		}

		if(bestIndex<0)
		{
			break;
		}

		pointIndex=bestIndex;

		lastRefPoint=refPoint;
	}

	if(vertCount>1)
	{
		spDrawOutput->drawLines(pCurve,NULL,vertCount,
				DrawI::e_strip,false,NULL);
	}

	delete[] pCurve;

	String summary;
	summary.sPrintf("%d points",vertCount);
	catalog<String>("summary")=summary;
}

} /* namespace ext */
} /* namespace fe */
