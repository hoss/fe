/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <operator/operator.pmh>

namespace fe
{
namespace ext
{

void ClaspOp::initialize(void)
{
	catalog<String>("icon")="FE_alpha";

	catalog<String>("locatorPrefix")="loc";
	catalog<String>("locatorPrefix","label")="Locator Attr Prefix";
	catalog<String>("locatorPrefix","hint")=
			"Prefix of detail attribute names expected on input surface"
			" describing transforms of attachment points.";

	catalog<String>("locator0")="locator0";
	catalog<String>("locator0","label")="Locator 0";
	catalog<bool>("locator0","joined")=true;
	catalog<String>("locator0","hint")=
			"String point attribute indicating the first attachment.";

	catalog<String>("locator1")="locator1";
	catalog<String>("locator1","label")="Locator 1";
	catalog<String>("locator1","hint")=
			"String point attribute indicating the second attachment.";

	catalog<Real>("normalInfluence")=false;
	catalog<String>("normalInfluence","label")="Normal Influence";
	catalog<bool>("normalInfluence","joined")=true;
	catalog<String>("normalInfluence","hint")=
			"Amount that locator normal differences affect result."
			"  This influence is generally evident as twisting"
			" about the axis between each pair of locators.";

	catalog<Real>("tangentInfluence")=false;
	catalog<String>("tangentInfluence","label")="Tangent Influence";
	catalog<String>("tangentInfluence","hint")=
			"Amount that locator tangents affect result."
			"  This influence is generally evident as leaning"
			" towards and away from each opposing locator.";

	catalog<bool>("collide")=false;
	catalog<String>("collide","label")="Collision Avoidance";
	catalog<bool>("collide","joined")=true;
	catalog<String>("collide","hint")=
			"Push resulting surface out of collider.";

	catalog<I32>("archSteps")=16;
	catalog<I32>("archSteps","high")=32;
	catalog<I32>("archSteps","max")=1000;
	catalog<String>("archSteps","label")="Arch Steps";
	catalog<String>("archSteps","enabler")="collide";
	catalog<String>("archSteps","hint")=
			"Number of iterations to a solution for each clasp."
			"  Note that each iteration chops the solution space in half,"
			" so it takes far fewer steps than a method of uniform"
			" increments from the minimum to maximum.";

	catalog<Real>("archMin")=0.0;
	catalog<Real>("archMin","min")=0.0;
	catalog<Real>("archMin","max")=1.0;
	catalog<String>("archMin","label")="Arch Min";
	catalog<String>("archMin","enabler")="collide";
	catalog<bool>("archMin","joined")=true;
	catalog<String>("archMin","hint")=
			"Starting value to push the input surface away from the collider."
			"  While normally zero, this setting may be used to examine the"
			" arching limits interactively or pre-arch a little to reduce"
			" the action observed.";

	catalog<Real>("archMax")=0.5;
	catalog<Real>("archMax","min")=1e-9;
	catalog<Real>("archMax","max")=1.0;
	catalog<String>("archMax","label")="Arch Max";
	catalog<String>("archMax","enabler")="collide";
	catalog<String>("archMax","hint")=
			"Upper limit of how much the input surface can be"
			" pushed away from the collider."
			"  Lowering this limit should allow for a more precise solution"
			" in a given number of iterations.";

	catalog<Real>("deadZone")=0.1;
	catalog<Real>("deadZone","min")=0.0;
	catalog<Real>("deadZone","max")=0.5;
	catalog<String>("deadZone","label")="Dead Zone";
	catalog<String>("deadZone","enabler")="collide";
	catalog<String>("deadZone","hint")=
			"Unit distance from each locator towards the other locator"
			" for which collisions are not checked."
			"  This can be used to avoid unintended impacts where"
			" the clasp ties intentionally penetrate the collider surface.";

	catalog<bool>("discardAttributes")=true;
	catalog<String>("discardAttributes","label")="Discard Locator Attr";
	catalog<bool>("discardAttributes","joined")=true;
	catalog<String>("discardAttributes","hint")=
			"Remove point attributes naming locators from the output.";

	catalog<bool>("discardLocators")=true;
	catalog<String>("discardLocators","label")="Discard Locators";
	catalog<String>("discardLocators","hint")=
			"Remove used locator detail attributes from the output.";

	catalog< sp<Component> >("Input Surface");

	catalog< sp<Component> >("Reference Surface");

	catalog< sp<Component> >("Collider Surface");
	catalog<bool>("Collider Surface","optional")=true;
}

void ClaspOp::handle(Record& a_rSignal)
{
	const String locatorPrefix=catalog<String>("locatorPrefix");
	const String locator0=catalog<String>("locator0");
	const String locator1=catalog<String>("locator1");
	const Real normalInfluence=catalog<Real>("normalInfluence");
	const Real tangentInfluence=catalog<Real>("tangentInfluence");
	const BWORD collide=catalog<bool>("collide");
	const I32 archSteps=catalog<I32>("archSteps");
	const Real archMin=catalog<Real>("archMin");
	const Real archMax=catalog<Real>("archMax");
	const Real deadZone=catalog<Real>("deadZone");
	const BWORD discardAttributes=catalog<bool>("discardAttributes");
	const BWORD discardLocators=catalog<bool>("discardLocators");

	catalog<String>("summary")=locator0+" "+locator1;

	sp<DrawI> spDrawGuide;
	accessGuide(spDrawGuide,a_rSignal);

	sp<SurfaceAccessibleI> spOutputAccessible;
	if(!accessOutput(spOutputAccessible,a_rSignal)) return;

	sp<SurfaceAccessibleI> spReferenceAccessible;
	if(!access(spReferenceAccessible,"Reference Surface")) return;

	sp<SurfaceAccessorI> spRefPoint;
	if(!access(spRefPoint,spReferenceAccessible,e_point,e_position)) return;

	sp<SurfaceAccessorI> spOutputPoint;
	if(!access(spOutputPoint,spOutputAccessible,e_point,e_position)) return;

	sp<SurfaceI> spCollider;
	if(collide)
	{
		access(spCollider,"Collider Surface");
	}

	std::set<String> claspSet;

	std::map<String,SpatialTransform> refMap;
	std::map<String,SpatialTransform> defMap;
	std::map<String,bool> foundMap;

	setIdentity(refMap[""]);
	setIdentity(defMap[""]);
	foundMap[""]=true;

	const String groupName="";

	spOutputPoint->fragmentWith(SurfaceAccessibleI::e_point,
			locator0+" "+locator1,groupName);
	const U32 fragmentCount=spOutputPoint->fragmentCount();

	sp<SurfaceAccessibleI::FilterI> spFilter;

	for(U32 fragmentIndex=0;fragmentIndex<fragmentCount;fragmentIndex++)
	{
		const String fragmentKey=spOutputPoint->fragment(fragmentIndex);
		if(!spOutputPoint->filterWith(fragmentKey,spFilter) ||
				spFilter.isNull())
		{
			continue;
		}

		String buffer=fragmentKey;
		const String clasp0=buffer.parse();
		const String clasp1=buffer.parse();

		if(clasp0.empty() && clasp1.empty())
		{
			continue;
		}

		claspSet.insert(clasp0);
		claspSet.insert(clasp1);

		for(U32 pass=0;pass<2;pass++)
		{
			const String clasp=pass? clasp1: clasp0;
			if(!foundMap[clasp])
			{
				const String preface=locatorPrefix+"_"+clasp+"_";

				sp<SurfaceAccessorI> spRefPosition;
				if(!access(spRefPosition,spReferenceAccessible,
						e_detail,preface+"P")) return;

				sp<SurfaceAccessorI> spRefNormal;
				if(!access(spRefNormal,spReferenceAccessible,
						e_detail,preface+"N")) return;

				sp<SurfaceAccessorI> spRefTangent;
				if(!access(spRefTangent,spReferenceAccessible,
						e_detail,preface+"T")) return;

				sp<SurfaceAccessorI> spDefPosition;
				if(!access(spDefPosition,spOutputAccessible,
						e_detail,preface+"P")) return;

				sp<SurfaceAccessorI> spDefNormal;
				if(!access(spDefNormal,spOutputAccessible,
						e_detail,preface+"N")) return;

				sp<SurfaceAccessorI> spDefTangent;
				if(!access(spDefTangent,spOutputAccessible,
						e_detail,preface+"T")) return;

				const SpatialVector refPoint=
						spRefPosition->spatialVector(0);
				const SpatialVector refNormal=
						spRefNormal->spatialVector(0);
				const SpatialVector refTangent=
						spRefTangent->spatialVector(0);

				const SpatialVector defPoint=
						spDefPosition->spatialVector(0);
				const SpatialVector defNormal=
						spDefNormal->spatialVector(0);
				const SpatialVector defTangent=
						spDefTangent->spatialVector(0);

				SpatialTransform xformRef;
				makeFrameNormalY(xformRef,refPoint,refTangent,refNormal);

				SpatialTransform xformDef;
				makeFrameNormalY(xformDef,defPoint,defTangent,defNormal);

				refMap[clasp]=xformRef;
				defMap[clasp]=xformDef;
				foundMap[clasp]=true;

				if(spDrawGuide.isValid())
				{
					const Color red(1.0,0.0,0.0);
					const Color green(0.0,1.0,0.0);
					const Color blue(0.0,0.0,1.0);

					const Color lightred(1.0,0.5,0.5);
					const Color lightgreen(0.5,1.0,0.5);
					const Color lightblue(0.5,0.5,1.0);

					SpatialVector line[2];

					line[0]=refPoint;
					line[1]=refPoint+refTangent;

					spDrawGuide->drawLines(line,NULL,2,DrawI::e_strip,
							false,&lightgreen);

					line[0]=refPoint;
					line[1]=refPoint+refNormal;

					spDrawGuide->drawLines(line,NULL,2,DrawI::e_strip,
							false,&lightred);

					line[0]=defPoint;
					line[1]=defPoint+defTangent;

					spDrawGuide->drawLines(line,NULL,2,DrawI::e_strip,
							false,&green);

					line[0]=defPoint;
					line[1]=defPoint+defNormal;

					spDrawGuide->drawLines(line,NULL,2,DrawI::e_strip,
							false,&red);
				}
			}
		}

		const SpatialTransform& rRef0=refMap[clasp0];
		const SpatialTransform& rRef1=refMap[clasp1];

		const SpatialTransform& rDef0=defMap[clasp0];
		const SpatialTransform& rDef1=defMap[clasp1];

		const SpatialVector center0=rRef0.translation();
		const SpatialVector center1=rRef1.translation();

		const SpatialVector span=center1-center0;
		const Real distance=magnitude(span);
		if(distance<=0.0)
		{
			continue;
		}

		SpatialTransform conversion0;
		SpatialTransform conversion1;

		Real archLow=archMin;
		Real archHigh=archMax;

		Real arch=archMin;

		const U32 attemptCount=archSteps;
		for(U32 attemptIndex=0;attemptIndex<attemptCount;attemptIndex++)
		{
//			feLog("arch %.6G low %.6G high %.6G\n",arch,archLow,archHigh)

			if(arch==0.0 && normalInfluence>=1.0 && tangentInfluence>=1.0)
			{
				SpatialTransform invRef0;
				SpatialTransform invRef1;
				invert(invRef0,rRef0);
				invert(invRef1,rRef1);

				conversion0=invRef0*rDef0;
				conversion1=invRef1*rDef1;
			}
			else
			{
				SpatialVector refPoint0=rRef0.translation();
				SpatialVector refNormal0=rRef0.column(1);
				SpatialVector refTangent0=rRef0.column(0);

				SpatialVector refPoint1=rRef1.translation();
				SpatialVector refNormal1=rRef1.column(1);
				SpatialVector refTangent1=rRef1.column(0);

				SpatialVector defPoint0=rDef0.translation();
				SpatialVector defNormal0=rDef0.column(1);
				SpatialVector defTangent0=rDef0.column(0);

				SpatialVector defPoint1=rDef1.translation();
				SpatialVector defNormal1=rDef1.column(1);
				SpatialVector defTangent1=rDef1.column(0);

				SpatialVector refSpan=unitSafe(refPoint1-refPoint0);
				SpatialVector defSpan=unitSafe(defPoint1-defPoint0);

				//* TODO arch

				if(tangentInfluence<1.0)
				{
					const Real tangentMinus=1.0-tangentInfluence;

					refTangent0=tangentInfluence*refTangent0+
							tangentMinus*refSpan;
					refTangent1=tangentInfluence*refTangent1+
							tangentMinus*refSpan;

					defTangent0=tangentInfluence*defTangent0+
							tangentMinus*defSpan;
					defTangent1=tangentInfluence*defTangent1+
							tangentMinus*defSpan;

					const SpatialVector refSide0=
							cross(refTangent0,refNormal0);
					const SpatialVector refSide1=
							cross(refTangent1,refNormal1);

					const SpatialVector defSide0=
							cross(defTangent0,defNormal0);
					const SpatialVector defSide1=
							cross(defTangent1,defNormal1);

					refNormal0=cross(refSide0,refTangent0);
					refNormal1=cross(refSide1,refTangent1);

					defNormal0=cross(defSide0,defTangent0);
					defNormal1=cross(defSide1,defTangent1);

				}

				if(normalInfluence<1.0)
				{
					const Real normalMinus=1.0-normalInfluence;

					const SpatialVector refAverage=
							unitSafe(refNormal0+refNormal1);

					refNormal0=normalInfluence*refNormal0+
							normalMinus*refAverage;
					refNormal1=normalInfluence*refNormal1+
							normalMinus*refAverage;

					const SpatialVector defAverage=
							unitSafe(defNormal0+defNormal1);

					defNormal0=normalInfluence*defNormal0+
							normalMinus*defAverage;
					defNormal1=normalInfluence*defNormal1+
							normalMinus*defAverage;
				}

				if(arch>0.0)
				{
					const SpatialVector defSide0=
							cross(defTangent0,defNormal0);
					const SpatialVector defSide1=
							cross(defTangent1,defNormal1);

					const SpatialQuaternion rotate0(arch,defSide0);
					const SpatialQuaternion rotate1(-arch,defSide1);

					rotateVector(rotate0,defNormal0,defNormal0);
					rotateVector(rotate1,defNormal1,defNormal1);
				}

				SpatialTransform modRef0;
				SpatialTransform modRef1;
				makeFrameNormalY(modRef0,
						refPoint0,refTangent0,refNormal0);
				makeFrameNormalY(modRef1,
						refPoint1,refTangent1,refNormal1);

				SpatialTransform modDef0;
				SpatialTransform modDef1;
				makeFrameNormalY(modDef0,
						defPoint0,defTangent0,defNormal0);
				makeFrameNormalY(modDef1,
						defPoint1,defTangent1,defNormal1);

				SpatialTransform invRef0;
				SpatialTransform invRef1;
				invert(invRef0,modRef0);
				invert(invRef1,modRef1);

				conversion0=invRef0*modDef0;
				conversion1=invRef1*modDef1;
			}

			const SpatialVector axis=span/distance;

			const BWORD lastAttempt=(attemptIndex==attemptCount-1);

			BWORD collided=FALSE;

			const U32 filterCount=spFilter->filterCount();
			for(U32 filterIndex=0;filterIndex<filterCount;filterIndex++)
			{
				if(interrupted())
				{
					fragmentIndex=fragmentCount;
					attemptIndex=attemptCount;
					break;
				}

				const U32 pointIndex=spFilter->filter(filterIndex);

//				feLog("fragment %d/%d \"%s\" filter %d/%d point %d"
//						" clasp \"%s\" \"%s\" \n",
//						fragmentIndex,fragmentCount,fragmentKey.c_str(),
//						filterIndex,filterCount,pointIndex,
//						clasp0.c_str(),clasp1.c_str());


				const SpatialVector point=
						spRefPoint->spatialVector(pointIndex);

				const Real fraction=dot(axis,point-center0)/distance;

				SpatialVector result0;
				transformVector(conversion0,point,result0);

				SpatialVector result1;
				transformVector(conversion1,point,result1);

				const SpatialVector result=result0+fraction*(result1-result0);

				if(!lastAttempt && spCollider.isValid() &&
						fraction>=deadZone && fraction<=(1.0-deadZone))
				{
					sp<SurfaceI::ImpactI> spImpact=
							spCollider->nearestPoint(result);
					if(spImpact.isValid())
					{
						const SpatialVector normal=spImpact->normal();
						const SpatialVector intersection=
								spImpact->intersection();

						const SpatialVector towards=result-intersection;

						if(dot(normal,towards)<0.0)
						{
							collided=TRUE;
							break;
						}
					}
				}

				spOutputPoint->set(pointIndex,result);
			}

			if(!attemptIndex && !collided)
			{
				//* all clear
				break;
			}
			if(collided)
			{
				if(!attemptIndex)
				{
					//* check max
					arch=archHigh;
					continue;
				}
				else if(attemptIndex==1)
				{
					//* maxing out
					attemptIndex=attemptCount-2;
					continue;
				}
				else
				{
					archLow=arch;
				}
			}
			else
			{
				archHigh=arch;
			}

			arch=0.5*(archLow+archHigh);
		}
	}

	if(discardAttributes)
	{
		discard(spOutputAccessible,e_point,locator0);
		discard(spOutputAccessible,e_point,locator1);
	}

	if(discardLocators)
	{
		for(std::set<String>::iterator it=claspSet.begin();
				it!=claspSet.end();it++)
		{
			const String clasp= *it;
			const String preface=locatorPrefix+"_"+clasp+"_";

			discard(spOutputAccessible,e_detail,preface+"P");
			discard(spOutputAccessible,e_detail,preface+"N");
			discard(spOutputAccessible,e_detail,preface+"T");
		}
	}
}

} /* namespace ext */
} /* namespace fe */
