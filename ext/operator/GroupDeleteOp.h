/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __operator_GroupDeleteOp_h__
#define __operator_GroupDeleteOp_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
    @brief Remove group from a Surface

	The elements in the groups are not removed.

	@ingroup operator
*//***************************************************************************/
class FE_DL_EXPORT GroupDeleteOp:
	public OperatorSurfaceCommon,
	public Initialize<GroupDeleteOp>
{
	public:
				GroupDeleteOp(void)											{}
virtual			~GroupDeleteOp(void)										{}

		void	initialize(void);

				//* As HandlerI
virtual void	handle(Record& a_rSignal);
};

} /* namespace ext */
} /* namespace fe */

#endif /* __operator_GroupDeleteOp_h__ */
