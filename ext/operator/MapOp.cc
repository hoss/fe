/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <operator/operator.pmh>

#define FE_MPO_DEBUG		FALSE

using namespace fe;
using namespace fe::ext;

void MapOp::initialize(void)
{
	catalog<String>("icon")="FE_alpha";

	catalog<String>("template")="texture.<UDIM>.tx";
	catalog<String>("template","label")="Load File";
	catalog<String>("template","suggest")="filename";
	catalog<String>("template","hint")="Full pathname of textures.";

	catalog<String>("attribute")="";
	catalog<String>("attribute","label")="Attribute";
	catalog<String>("attribute","hint")="Point attribute to create.";

	catalog<String>("class")="point";
	catalog<String>("class","choice:0")="point";
	catalog<String>("class","label:0")="Point";
	catalog<String>("class","choice:1")="primitive";
	catalog<String>("class","label:1")="Primitive";
	catalog<String>("class","label")="Class";

	catalog<String>("type")="real";
	catalog<String>("type","choice:0")="real";
	catalog<String>("type","label:0")="Real";
	catalog<String>("type","choice:1")="integer";
	catalog<String>("type","label:1")="Integer";
	catalog<String>("type","choice:2")="vector";
	catalog<String>("type","label:2")="Vector";
	catalog<String>("type","choice:3")="string";
	catalog<String>("type","label:3")="String";
	catalog<String>("type","label")="Type";

	catalog<Real>("toMin")=0.0;
	catalog<Real>("toMin","min")=-1e9;
	catalog<Real>("toMin","low")=0.0;
	catalog<Real>("toMin","high")=100.0;
	catalog<Real>("toMin","max")=1e9;
	catalog<String>("toMin","label")="Map To Min";
	catalog<bool>("toMin","joined")=true;

	catalog<Real>("toMax")=1.0;
	catalog<Real>("toMax","min")=-1e9;
	catalog<Real>("toMax","low")=0.0;
	catalog<Real>("toMax","high")=100.0;
	catalog<Real>("toMax","max")=1e9;
	catalog<String>("toMax","label")="Max";

	catalog<Real>("random")=0.0;
	catalog<Real>("random","high")=10.0;
	catalog<Real>("random","max")=1e9;
	catalog<String>("random","label")="Random";

	catalog< sp<Component> >("Input Surface");

	catalog< sp<Component> >("Output Surface");
	catalog<String>("Output Surface","IO")="output";
	catalog<String>("Output Surface","copy")="Input Surface";
	catalog<bool>("Output Surface","clobber")=true;
}

void MapOp::handle(Record& a_rSignal)
{
#if FE_MPO_DEBUG
	feLog("MapOp::handle node \"%s\"\n",name().c_str());
#endif

	const String templateFile=catalog<String>("template");
	const String attribute=catalog<String>("attribute");
	const String attrClass=catalog<String>("class");
	const String attrType=catalog<String>("type");
	const Real toMin=catalog<Real>("toMin");
	const Real toMax=catalog<Real>("toMax");
	const Real random=catalog<Real>("random");

	const BWORD toPrimitive=(attrClass=="primitive");
	const BWORD toReal=(attrType=="real");
	const BWORD toVector=(attrType=="vector");
	const BWORD toInteger=(attrType=="integer");

	catalog<String>("summary")="";

	if(templateFile.empty())
	{
#if FE_MPO_DEBUG
		feLog("MapOp::handle loadfile empty\n");
#endif
		return;
	}

	if(attribute.empty())
	{
#if FE_MPO_DEBUG
		feLog("MapOp::handle attribute name empty\n");
#endif
		return;
	}

#if FE_MPO_DEBUG
	feLog("MapOp::handle load \"%s\"\n",templateFile.c_str());
#endif

	sp<SurfaceAccessibleI> spOutputAccessible;
	if(!accessOutput(spOutputAccessible,a_rSignal)) return;

	//* TODO search more rates
	sp<SurfaceAccessorI> spOutputUV;
	if(!access(spOutputUV,spOutputAccessible,
			toPrimitive? e_primitive: e_point,e_uv)) return;

	sp<SurfaceAccessorI> spOutputVertices;
	if(!access(spOutputVertices,spOutputAccessible,
			e_primitive,e_vertices)) return;

	sp<SurfaceAccessorI> spOutputValue;
	if(!access(spOutputValue,spOutputAccessible,
			toPrimitive? e_primitive: e_point,attribute)) return;

	const I32 elementCount=toPrimitive? spOutputVertices->count():
			spOutputUV->count();

#if FE_MPO_DEBUG
	feLog("MapOp::handle elementCount %d\n",elementCount);
#endif

	Raster raster(registry());
	raster.setTemplate(templateFile);
	raster.setMinMax(toMin,toMax,random);

	for(I32 elementIndex=0;elementIndex<elementCount;elementIndex++)
	{
		if(toPrimitive)
		{
			const I32 subCount=spOutputVertices->subCount(elementIndex);
			if(!subCount)
			{
				continue;
			}
		}

		const Vector2 uv=spOutputUV->spatialVector(elementIndex);

		if(toReal)
		{
			spOutputValue->set(elementIndex,raster.real(uv));
		}
		else if(toVector)
		{
			spOutputValue->set(elementIndex,raster.spatialVector(uv));
		}
		else if(toInteger)
		{
			spOutputValue->set(elementIndex,raster.integer(uv));
		}
		else
		{
			spOutputValue->set(elementIndex,raster.string(uv));
		}
	}

//	spOutputAccessible->save("mapped.rg");

	catalog<String>("summary")=templateFile.basename();

#if FE_MPO_DEBUG
	feLog("MapOp::handle node \"%s\" done\n",name().c_str());
#endif
}

Real MapOp::Raster::low1(const Vector2& a_rUV)
{
	if(m_random<=Real(0))
	{
		return m_min;
	}

	Vector2d noiseInput(1e2*a_rUV[0],1e2*a_rUV[1]);
	const Real result=m_min+m_random*m_noise.whiteNoise2d(noiseInput);

	return result;
}

SpatialVector MapOp::Raster::low3(const Vector2& a_rUV)
{
	if(m_random<=Real(0))
	{
		return SpatialVector(m_min,m_min,m_min);
	}

	SpatialVector result;

	Vector2d noiseInput(1e2*a_rUV[0],1e2*a_rUV[1]);
	result[0]=m_min+m_random*m_noise.whiteNoise2d(noiseInput);

	noiseInput[0]+=1e4;
	result[1]=m_min+m_random*m_noise.whiteNoise2d(noiseInput);

	noiseInput[0]+=1e4;
	result[2]=m_min+m_random*m_noise.whiteNoise2d(noiseInput);

	return result;
}

String MapOp::Raster::string(const Vector2& a_rUV)
{
	const SpatialVector low=low3(a_rUV);

	String text;

	U8 data[4];
	if(lookup(a_rUV,data))
	{
		text.sPrintf("%.6G %.6G %.6G",
				low[0]+m_range*(data[0]/255.0),
				low[1]+m_range*(data[1]/255.0),
				low[2]+m_range*(data[2]/255.0));
	}
	else
	{
		text.sPrintf("%.6G %.6G %.6G",
				low[0],low[1],low[2]);
	}

	return text;
}

I32 MapOp::Raster::integer(const Vector2& a_rUV)
{
	return I32(real(a_rUV));
}

Real MapOp::Raster::real(const Vector2& a_rUV)
{
	U8 data[4];
	if(!lookup(a_rUV,data))
	{
		return low1(a_rUV);
	}

	return low1(a_rUV)+m_range*(data[0]/255.0);
}

SpatialVector MapOp::Raster::spatialVector(const Vector2& a_rUV)
{
	const SpatialVector low=low3(a_rUV);

	U8 data[4];
	if(!lookup(a_rUV,data))
	{
		return SpatialVector(low[0],low[1],low[2]);
	}

	return SpatialVector(
			low[0]+m_range*(data[0]/255.0),
			low[1]+m_range*(data[1]/255.0),
			low[2]+m_range*(data[2]/255.0));
}

BWORD MapOp::Raster::lookup(const Vector2& a_rUV,U8* a_pData)
{
	const I32 index=I32(a_rUV[0])+I32(a_rUV[1])*10;
	if(I32(m_mapArray.size())<index+1)
	{
		m_mapArray.resize(index+1);
		m_idArray.resize(index+1,0);
	}

	sp<ImageI>& rspImageI=m_mapArray[index];
	U32& rImageId=m_idArray[index];
	if(rspImageI.isNull())
	{
		//* see RasterOp

		String suffix;

		String buffer=m_template;
		String token;
		while(!(token=buffer.parse("",".")).empty())
		{
			suffix=token;
		}

		if(!suffix.empty())
		{
			rspImageI=m_spRegistry->create("ImageI.*.*."+suffix);
		}

		if(rspImageI.isNull())
		{
			rspImageI=m_spRegistry->create("ImageI");
		}

		if(rspImageI.isNull())
		{
			return FALSE;
		}

		//* TODO check if using UDIM

		String udim;
		udim.sPrintf("%d",1001+index);

		const String filename=m_template.substitute("<UDIM>",udim);

		rImageId=rspImageI->loadSelect(filename);
		const U8* raw=(U8*)rspImageI->raw();

#if FE_MPO_DEBUG
		const ImageI::Format format=rspImageI->format();
		const U32 width=rspImageI->width();
		const U32 height=rspImageI->height();

		feLog("MapOp loaded %d %dx%d format %d raw %p\n  \"%s\"\n",
				rImageId,width,height,format,raw,filename.c_str());
#endif

		if(!raw)
		{
			rspImageI->unload(rImageId);
			rspImageI=NULL;
			rImageId=0;
			return FALSE;
		}

//		if(raw)
//		{
//			const I32 byteCount=width*height*4;
//			for(I32 byteIndex=0;byteIndex<byteCount;byteIndex+=4)
//			{
//				feLog("%3d %3d %3d %3d |",
//						raw[byteIndex],raw[byteIndex+1],
//						raw[byteIndex+2],raw[byteIndex+3]);
//				if((byteIndex%16)>10)
//				{
//					feLog("\n");
//				}
//			}
//			feLog("\n");
//		}
	}

	FEASSERT(rspImageI.isValid());

	//* see ImageI::Format
	const I32 formatBytes[4]={0,1,3,4};

	rspImageI->select(rImageId);

	const ImageI::Format format=rspImageI->format();
	const U32 width=rspImageI->width();
	const U32 height=rspImageI->height();
	const U8* raw=(U8*)rspImageI->raw();

	const U32 byteCount=formatBytes[I32(format)];

	const Real uu=a_rUV[0]-int(a_rUV[0]);
	const Real vv=a_rUV[1]-int(a_rUV[1]);

	const U32 x=uu*width;
	const U32 y=vv*height;

	const U32 yy=y*width;
	const U32 xx=(yy+x)*byteCount;

	const U8* bytes=&raw[xx];

//	if(bytes[0] || bytes[1] || bytes[2] || bytes[3]!=255)
//	{
//		feLog("%d: %d %d %d %d\n",elementIndex,
//				bytes[0],bytes[1],bytes[2],bytes[3]);
//	}

#if FE_MPO_DEBUG
	if(elementIndex<32)
	{
		feLog("map %d %d %d data %p\n",width,height,byteCount,raw);
		feLog("%d/%d uv %s index %d xy %d %d byte %d is %d\n",
				elementIndex,elementCount,c_print(a_rUV),
				index,x,y,xx,bytes[0]);
	}
#endif

	memcpy(a_pData,bytes,byteCount);

	return TRUE;
}
