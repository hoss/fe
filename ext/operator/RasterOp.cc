/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <operator/operator.pmh>

namespace fe
{
namespace ext
{

//* TODO draw raster as 3D triangles

void RasterOp::initialize(void)
{
	catalog<String>("icon")="FE_beta";

	catalog<String>("rasterPrefix")="raster";
	catalog<String>("rasterPrefix","label")="Raster Attr Prefix";
	catalog<String>("rasterPrefix","hint")=
			"Prefix of attribute names added to output surface describing"
			" the size and format of the raster data.";

	catalog<String>("valueAttr")="value";
	catalog<String>("valueAttr","label")="Value Attribute";
	catalog<String>("valueAttr","hint")=
			"Name of point attribute to place results.";

	catalog<bool>("invertY")=true;
	catalog<String>("invertY","label")="Bottom Up";
	catalog<bool>("invertY","joined")=true;
	catalog<String>("invertY","hint")=
			"Raw image data usually starts from the upper left corner."
			"  This option puts the UV origin in the lower left corner.";

	catalog<bool>("colorize")=false;
	catalog<String>("colorize","label")="Colorize";

	catalog<String>("pattern")="PART";
	catalog<String>("pattern","label")="Replace Pattern";
	catalog<String>("pattern","hint")=
			"Replace this string in the following filename with"
			" each unique partition name.";

	catalog<bool>("save")=false;
	catalog<String>("save","label")="Save";
	catalog<bool>("save","joined")=true;

	catalog<String>("savefile")="raster.PART.$F.png";
	catalog<String>("savefile","label")="File";
	catalog<String>("savefile","suggest")="filename";

	catalog<bool>("mkdir")=false;
	catalog<String>("mkdir","label")="Make Directory";

	catalog<bool>("load")=false;
	catalog<String>("load","label")="Load";
	catalog<bool>("load","joined")=true;

	catalog<String>("loadfile")="raster.PART.$F.png";
	catalog<String>("loadfile","label")="File";
	catalog<String>("loadfile","suggest")="filename";

	catalog<String>("partitionAttr")="part";
	catalog<String>("partitionAttr","label")="Partition Attribute";
	catalog<String>("partitionAttr","enabler")="load";
	catalog<String>("partitionAttr","hint")=
			"Primitive string attribute on the driver surface"
			" describing membership in a non-overlapping UV space.";

	catalog< sp<Component> >("Input Surface");
	catalog<bool>("Input Surface","optional")=true;

	catalog< sp<Component> >("Driver Surface");
	catalog<bool>("Driver Surface","optional")=true;

	//* don't copy input to output
//	catalog< sp<Component> >("Output Surface");
//	catalog<String>("Output Surface","IO")="output";
}

void RasterOp::handle(Record& a_rSignal)
{
	const BWORD save=catalog<bool>("save");
	const String savefile=save? catalog<String>("savefile"): "";

	const BWORD load=catalog<bool>("load");
	const String loadfile=load? catalog<String>("loadfile"): "";

	if(!savefile.empty())
	{
		catalog<String>("summary")="save "+savefile.basename();
	}
	else if(!loadfile.empty())
	{
		catalog<String>("summary")="load "+loadfile.basename();
	}
	else
	{
		catalog<String>("summary")="";
	}

	if(!accessOutput(m_spOutputAccessible,a_rSignal)) return;

	if(save)
	{
		access(m_spInputAccessible,"Input Surface",e_quiet);

		if(m_spInputAccessible.isNull())
		{
			catalog<String>("warning")+="no input for saving images;";
		}
		else if(savefile.empty())
		{
			catalog<String>("warning")+="no filename for saving images;";
		}
		else
		{
			saveRaster(savefile);
		}
	}

	if(load)
	{
		access(m_spDriverAccessible,"Driver Surface",e_quiet);

		if(!accessDraw(m_spDrawI,a_rSignal)) return;

		if(loadfile.empty())
		{
			catalog<String>("warning")+="no filename for loading images;";
		}
		else
		{
			m_spOutputAccessible->clear();

			loadRaster(loadfile);
		}
	}
}

void RasterOp::saveRaster(String a_savefile)
{
	const String valueAttr=catalog<String>("valueAttr");
	const String rasterPrefix=catalog<String>("rasterPrefix");
	const String pattern=catalog<String>("pattern");
	const BWORD invertY=catalog<bool>("invertY");
	const BWORD colorize=catalog<bool>("colorize");
	const BWORD makeDir=catalog<bool>("mkdir");

	sp<SurfaceAccessorI> spOutputColor;
	if(!access(spOutputColor,m_spOutputAccessible,
			e_point,e_color)) return;

	sp<SurfaceAccessorI> spInputMin;
	if(!access(spInputMin,m_spInputAccessible,
			e_detail,rasterPrefix+"_min")) return;

	sp<SurfaceAccessorI> spInputMax;
	if(!access(spInputMax,m_spInputAccessible,
			e_detail,rasterPrefix+"_max")) return;

	sp<SurfaceAccessorI> spInputPartAttr;
	if(!access(spInputPartAttr,m_spInputAccessible,
			e_detail,rasterPrefix+"_partition")) return;

	sp<SurfaceAccessorI> spInputValue;
	if(!access(spInputValue,m_spInputAccessible,e_point,valueAttr)) return;

	const Real min=spInputMin->real(0);
	const Real max=spInputMax->real(0);
	const String partitionAttr=spInputPartAttr->string(0);

	sp<SurfaceAccessorI> spInputPartition;
	if(!partitionAttr.empty())
	{
		if(!access(spInputPartition,m_spInputAccessible,
				e_point,partitionAttr)) return;
	}

	sp<ImageI> spImage=createImage(a_savefile);
	if(spImage.isValid())
	{
		feLog("RasterOp::saveRaster using \"%s\"\n",spImage->name().c_str());
	}

	const U32 pointCount=spInputValue->count();

	//* TODO could use std::unordered_set when available

	std::set<String> partitionSet;
	if(spInputPartition.isValid())
	{
		for(U32 pointIndex=0;pointIndex<pointCount;pointIndex++)
		{
			const String partition=spInputPartition->string(pointIndex);
			if(partitionSet.find(partition)==partitionSet.end())
			{
				partitionSet.insert(partition);
			}
		}
	}
	else
	{
		partitionSet.insert("");
	}

	//* TODO create each image in parallel

	for(std::set<String>::iterator it=partitionSet.begin();
			it!=partitionSet.end();it++)
	{
		const String partition= *it;
		const String partitionFile=a_savefile.replace(pattern,partition);

		feLog("RasterOp::handle partition \"%s\" file \"%s\"\n",
				partition.c_str(),partitionFile.c_str());

		const String addition=!partition.empty()? "_"+partitionAttr: "";

		sp<SurfaceAccessorI> spInputWidth;
		if(!access(spInputWidth,m_spInputAccessible,
				e_detail,rasterPrefix+"_width"+addition,e_quiet))
		{
			if(!access(spInputWidth,m_spInputAccessible,
					e_detail,rasterPrefix+"_width")) continue;
		}

		sp<SurfaceAccessorI> spInputHeight;
		if(!access(spInputHeight,m_spInputAccessible,
				e_detail,rasterPrefix+"_height"+addition,e_quiet))
		{
			if(!access(spInputHeight,m_spInputAccessible,
					e_detail,rasterPrefix+"_height")) continue;
		}

		if(makeDir)
		{
			const String directory=partitionFile.pathname();

//			const BWORD success=
					System::createDirectory(directory);

//			feLog("RasterOp::handle mkdir \"%s\" success %d\n",
//					directory.c_str(),success);
		}

		const I32 width=spInputWidth->integer(0);
		const I32 height=spInputHeight->integer(0);

//~		U8* data=new U8[height][width][3];
		U8* data=new U8[height*width*3];

		U32 pixelIndex=0;

		for(U32 pointIndex=0;pointIndex<pointCount;pointIndex++)
		{
			if(interrupted())
			{
				it=partitionSet.end();
				break;
			}

			if(spInputPartition.isValid() &&
					spInputPartition->string(pointIndex)!=partition)
			{
				continue;
			}

			const I32 uIndex=pixelIndex%width;
			const I32 vIndex=invertY? (height-1-pixelIndex/width):
					pixelIndex/width;

			if(uIndex>=width || vIndex>=height)
			{
				feLog("%d u %d/%d v %d/%d\n",
						pointIndex,uIndex,width,vIndex,height);
				continue;
			}

//~			U8* pPixel=data[vIndex][uIndex];
			U8* pPixel= &data[(vIndex*width+uIndex)*3];

			//* TODO verify correct uv
//			const Real uCoord=uIndex/(width-1.0);
//			const Real vCoord=vIndex/(height-1.0);

			const Real value=spInputValue->real(pointIndex);

			I32 byte=255*(value-min)/(max-min);
			if(byte<0)
			{
				byte=0;
			}
			if(byte>255)
			{
				byte=255;
			}

			pPixel[0]=byte;

			if(colorize)
			{
				pPixel[1]=value>0.0? 0: byte;
				pPixel[2]=byte<5? 255: 0;
			}
			else
			{
				pPixel[1]=byte;
				pPixel[2]=byte;
			}

			const Color color=(1.0/255.0)*Color(pPixel[0],pPixel[1],pPixel[2]);
			spOutputColor->set(pointIndex,color);

//			feLog("%d pixel %d u %d/%d v %d/%d bytes %d %d %d\n",
//					pointIndex,pixelIndex,uIndex,width,vIndex,height,
//					pPixel[0],pPixel[1],pPixel[2]);

			pixelIndex++;
		}

		if(spImage.isValid())
		{
			const U32 imageId=spImage->createSelect();
			spImage->setFormat(ImageI::e_rgb);
			spImage->resize(width,height,1);
			spImage->replaceRegion(0,0,0,width,height,1,data);

			if(!spImage->save(partitionFile))
			{
				catalog<String>("warning")+=
						"failed to save \"" + partitionFile +"\";";
			}

			//* TODO try without clearing last; need ref count on ImageRaw data
			spImage->unload(imageId);
		}

		delete[] data;
	}
}

void RasterOp::loadRaster(String a_loadfile)
{
	const String valueAttr=catalog<String>("valueAttr");
	const String rasterPrefix=catalog<String>("rasterPrefix");
	const String pattern=catalog<String>("pattern");
	const String partitionAttr=catalog<String>("partitionAttr");

	sp<SurfaceAccessorI> spOutputUV;
	if(!access(spOutputUV,m_spOutputAccessible,
			e_point,e_uv)) return;

	sp<SurfaceAccessorI> spOutputMin;
	if(!access(spOutputMin,m_spOutputAccessible,
			e_detail,rasterPrefix+"_min")) return;

	sp<SurfaceAccessorI> spOutputMax;
	if(!access(spOutputMax,m_spOutputAccessible,
			e_detail,rasterPrefix+"_max")) return;
	sp<SurfaceAccessorI> spOutputPartAttr;
	if(!access(spOutputPartAttr,m_spOutputAccessible,
			e_detail,rasterPrefix+"_partition")) return;

	spOutputMin->set(0,Real(0));
	spOutputMax->set(0,Real(1));
	spOutputPartAttr->set(0,partitionAttr);

	sp<SurfaceAccessorI> spOutputValue;
	if(!access(spOutputValue,m_spOutputAccessible,e_point,valueAttr)) return;

	const BWORD partitionInput=(!partitionAttr.empty());

	U32 mapCount=1;

	sp<SurfaceI> spDriver;
	if(m_spDriverAccessible.isValid() && access(spDriver,m_spDriverAccessible))
	{
		spDriver->setRefinement(0);

		if(partitionInput)
		{
			spDriver->partitionWith(partitionAttr);
			spDriver->prepareForUVSearch();
			mapCount=spDriver->partitionCount();
		}
	}

	sp<ImageI> spImage=createImage(a_loadfile);
	if(spImage.isNull())
	{
		feLog("RasterOp::loadRaster failed to create image component\n");
		return;
	}

	feLog("RasterOp::loadRaster using \"%s\"\n",spImage->name().c_str());

	const U32 byteCount=3;	//* TODO

	U32 pointCount=0;

	for(U32 mapIndex=0;mapIndex<mapCount;mapIndex++)
	{
		String partition;
		if(partitionInput && spDriver.isValid())
		{
			partition=spDriver->partitionName(mapIndex);
			spDriver->setPartitionFilter(partition);
		}

		const String partitionFile=a_loadfile.replace(pattern,partition);

		const U32 imageId=spImage->loadSelect(partitionFile);
		const ImageI::Format format=spImage->format();
		const U32 width=spImage->width();
		const U32 height=spImage->height();
		const U8* raw=(U8*)spImage->raw();

		feLog("loaded %d %dx%d format %d raw %p\n",
				imageId,width,height,format,raw);

		if(!raw)
		{
			spImage->unload(imageId);
			continue;
		}

		for(U32 y=0;y<height;y++)
		{
			const U32 yy=y*width;
			for(U32 x=0;x<width;x++)
			{
				const U32 xx=(yy+x)*byteCount;

				const U8* bytes=&raw[xx];

				const SpatialVector uv(x/Real(width),y/Real(height),0.0);

				const SpatialVector point=(spDriver.isValid())?
						spDriver->samplePoint(uv):
						SpatialVector(Real(x),Real(height-1-y),0.0);

				const SpatialVector value(
						bytes[0]/255.0,bytes[1]/255.0,bytes[2]/255.0);

				m_spDrawI->drawPoints(&point,NULL,1,false,NULL);

				spOutputValue->set(pointCount,value);
				spOutputUV->set(pointCount,uv);

				pointCount++;
			}
		}

		spImage->unload(imageId);

		const String addition=partitionInput? "_"+partition: "";

		sp<SurfaceAccessorI> spOutputWidth;
		if(access(spOutputWidth,m_spOutputAccessible,
				e_detail,rasterPrefix+"_width"+addition))
		{
			spOutputWidth->set(0,I32(width));
		}

		sp<SurfaceAccessorI> spOutputHeight;
		if(!access(spOutputHeight,m_spOutputAccessible,
				e_detail,rasterPrefix+"_height"+addition)) return;
		{
			spOutputHeight->set(0,I32(height));
		}

	}

	//* TODO set detail attributes
}

//* NOTE argument is just to guess the format
sp<ImageI> RasterOp::createImage(String a_filename)
{
	String format;

	String buffer=a_filename;
	String token;
	while(!(token=buffer.parse("",".")).empty())
	{
		format=token;
	}

	sp<ImageI> spImage;

	if(!format.empty())
	{
		spImage=registry()->create("ImageI.*.*."+format);
	}
	if(spImage.isNull())
	{
		spImage=registry()->create("ImageI");
	}
	if(spImage.isNull())
	{
		catalog<String>("warning")+="could not initialize output image;";
	}

	return spImage;
}

} /* namespace ext */
} /* namespace fe */
