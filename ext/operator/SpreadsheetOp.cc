/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <operator/operator.pmh>

#define FE_SHO_DEBUG		FALSE

using namespace fe;
using namespace fe::ext;

//* TODO color bands

void SpreadsheetOp::initialize(void)
{
	catalog<String>("icon")="FE_alpha";

	catalog<String>("class")="point";
	catalog<String>("class","label")="Class";
	catalog<String>("class","choice:0")="point";
	catalog<String>("class","label:0")="Point";
	catalog<String>("class","choice:1")="vertex";
	catalog<String>("class","label:1")="Vertex";
	catalog<String>("class","choice:2")="primitive";
	catalog<String>("class","label:2")="Primitive";
	catalog<String>("class","choice:3")="detail";
	catalog<String>("class","label:3")="Detail";

	catalog<String>("attributes")="";
	catalog<String>("attributes","label")="Attributes";
	catalog<bool>("attributes","joined")=true;

	catalog<bool>("invert")=false;
	catalog<String>("invert","label")="Not In List";

	catalog<I32>("start")=0;
	catalog<I32>("start","high")=100;
	catalog<String>("start","label")="Start";
	catalog<String>("start","IO")="input output";
	catalog<bool>("start","joined")=true;

	catalog<I32>("length")=10;
	catalog<I32>("length","high")=100;
	catalog<String>("length","label")="Length";

	catalog<I32>("offsetY")=24;
	catalog<I32>("offsetY","high")=128;
	catalog<I32>("offsetY","max")=4096;
	catalog<String>("offsetY","label")="Display Offset";

	catalog<Real>("scrollScale")=1.0;
	catalog<Real>("scrollScale","low")=0.0;
	catalog<Real>("scrollScale","high")=10.0;
	catalog<Real>("scrollScale","min")= -100.0;
	catalog<Real>("scrollScale","max")=100.0;
	catalog<String>("scrollScale","label")="Scroll Scale";

	catalog<bool>("logConsole")=false;
	catalog<String>("logConsole","label")="Log To Console";

	catalog< sp<Component> >("Brush");
	catalog<String>("Brush","implementation")="DrawI";

	catalog< sp<Component> >("Input Surface");

	m_spSolid=new DrawMode();
	m_spSolid->setDrawStyle(DrawMode::e_solid);
	m_spSolid->setLit(FALSE);
}

void SpreadsheetOp::handle(Record& a_rSignal)
{
	const String attrClass=catalog<String>("class");
	I32& rStart=catalog<I32>("start");
	const I32 length=catalog<I32>("length");
	const Real scrollScale=catalog<Real>("scrollScale");
	const I32 offsetY=catalog<I32>("offsetY");
	const BWORD logConsole=catalog<bool>("logConsole");

	Element element=e_point;
	Attribute attribute=e_generic;

	if(attrClass=="point")
	{
		attribute=e_position;
	}
	else if(attrClass=="vertex")
	{
		element=e_vertex;
		attribute=e_generic;
	}
	else if(attrClass=="primitive")
	{
		element=e_primitive;
		attribute=e_vertices;
	}
	else if(attrClass=="detail")
	{
		element=e_detail;
	}

	catalog< sp<Component> >("Brush");
	sp<DrawI> spDrawOverlay;
	accessBrushOverlay(spDrawOverlay,a_rSignal,e_quiet);

	if(spDrawOverlay.isValid())
	{
#if FE_SHO_DEBUG
		feLog("SpreadsheetOp::handle brush\n");
#endif
		m_brushed=TRUE;

		sp<ViewI> spView=spDrawOverlay->view();
		const Box2i viewport=spView->viewport();
		const I32 viewWidth=viewport.size()[0];
		const I32 viewHeight=viewport.size()[1];

		spDrawOverlay->pushDrawMode(m_spSolid);

		m_event.bind(windowEvent(a_rSignal));

//		feLog("%s\n",c_print(m_event));

		U32 item=m_event.item();
//		BWORD shifted=FALSE;
		if(item&WindowEvent::e_keyShift)
		{
//			shifted=TRUE;
			item^=WindowEvent::e_keyShift;
		}
//		BWORD ctrled=FALSE;
		if(item&WindowEvent::e_keyControl)
		{
//			ctrled=TRUE;
			item^=WindowEvent::e_keyControl;
		}

		if(m_event.isMouseWheel())
		{
			m_scroll-=(m_event.state()>0)? 1.0: -1.0;
		}

		const WindowEvent::MouseButtons buttons=m_event.mouseButtons();
		if(buttons)
		{
			const I32 mouseX=m_event.mouseX();
			const I32 mouseY=m_event.mouseY();

			const I32 moveX=mouseX-m_lastX;
//			const I32 moveY=mouseY-m_lastY;

			if(buttons&WindowEvent::e_mbLeft)
			{
				if(m_event.isMouseDrag())
				{
					m_scroll+=scrollScale*moveX;
				}
			}

			m_lastX=mouseX;
			m_lastY=mouseY;
		}

		I32 nextStart=rStart+I32(m_scroll);
		if(nextStart<0)
		{
			m_scroll-=nextStart;
		}
		if(nextStart>=m_elementCount)
		{
			m_scroll-=nextStart-m_elementCount-1;
		}

		I32 elementMax=0;
		limitBounds(nextStart,elementMax,length,m_elementCount);

//		feLog("nextStart %d length %d elementMax %d m_elementCount %d\n",
//				nextStart,length,elementMax,m_elementCount);

		const Color black(0.0,0.0,0.0);
		const Color blue(0.0,0.0,1.0);
		const Color lightblue(0.5,0.5,1.0);
		const Color white(1.0,1.0,1.0);

		SpatialVector location(0.0,viewHeight-1.0-offsetY);

		if(length<m_elementCount)
		{
			SpatialVector line[2];
			line[0]=location;
			line[1]=location+SpatialVector(viewWidth-1.0,0.0);
			spDrawOverlay->drawLines(line,NULL,2,DrawI::e_strip,false,&black);

			line[0][1]-=2.0;
			line[1][1]-=2.0;
			spDrawOverlay->drawLines(line,NULL,2,DrawI::e_strip,false,&black);

			line[0][1]+=1.0;
			line[1][1]+=1.0;
			spDrawOverlay->drawLines(line,NULL,2,DrawI::e_strip,false,&white);

			const Real viewStep=(viewWidth-1.0)/Real(m_elementCount-1);
			line[0][0]+=viewStep*nextStart;
			line[1][0]=line[0][0]+viewStep*(elementMax-nextStart);
			spDrawOverlay->drawLines(line,NULL,2,DrawI::e_strip,false,&blue);
		}

		location[1]-=12.0;

		const BWORD centered=FALSE;
		const U32 margin=2;
		const Color textColor=white;
		const Color* pBorderColor=NULL;
		const Color* pBackground=&black;

		String countString;
		countString.sPrintf("[%d]",m_elementCount);

		String text;
		text.sPrintf("%16s",countString.c_str());
		drawLabel(spDrawOverlay,location,text,centered,margin,
				lightblue,pBorderColor,pBackground);

		text=dump(element,"",0,nextStart,elementMax);
		drawLabel(spDrawOverlay,location,text,centered,margin,
				lightblue,pBorderColor,pBackground);
		location[1]-=11.0;

		String attrParse=m_attributes;
		String attrName;
		while(!(attrName=attrParse.parse()).empty())
		{
			U32 component=0;
			while(TRUE)
			{
				text=dump(element,attrName,component,nextStart,elementMax);
				if(text.empty())
				{
					break;
				}

				drawLabel(spDrawOverlay,location,text,centered,margin,
						textColor,pBorderColor,pBackground);

				location[1]-=11.0;
				component++;
			}
		}

		spDrawOverlay->popDrawMode();

		return;
	}

	const BWORD paramChanged=!m_brushed;
	m_brushed=FALSE;

#if FE_SHO_DEBUG
	feLog("SpreadsheetOp::handle changed %d\n",paramChanged);
#endif

	if(!paramChanged && fabs(m_scroll)<1.0)
	{
		return;
	}

	access(m_spInputAccessible,"Input Surface");

	m_attributes=catalog<String>("attributes");
	if(catalog<bool>("invert"))
	{
		const String rejects=m_attributes;
		m_attributes="";

		Array<SurfaceAccessibleI::Spec> specs;
		m_spInputAccessible->attributeSpecs(specs,
				(SurfaceAccessibleI::Element)element);

		const U32 specCount=specs.size();
		for(U32 specIndex=0;specIndex<specCount;specIndex++)
		{
			const SurfaceAccessibleI::Spec& rSpec=specs[specIndex];

			const String pattern=".*\\b"+rSpec.name()+"\\b.*";
			if(!rejects.match(pattern))
			{
				m_attributes+=rSpec.name()+" ";
			}
		}
	}

	m_elementCount=1;
	if(attrClass!="detail")
	{
		sp<SurfaceAccessorI> spInputElement;
		access(spInputElement,m_spInputAccessible,element,attribute,e_warning);

		m_elementCount=spInputElement.isValid()? spInputElement->count(): 0;
	}

//	feLog("rStart %d/%d m_scroll %.6G\n",rStart,m_elementCount,m_scroll);

	if(fabs(m_scroll)>=1.0)
	{
		const I32 change=m_scroll;

		m_scroll-=change;

		rStart+=change;
	}

	I32 elementMax=0;
	limitBounds(rStart,elementMax,length,m_elementCount);

//	feLog("rStart %d length %d elementMax %d m_elementCount %d\n",
//			rStart,length,elementMax,m_elementCount);

	if(logConsole)
	{
		String text=dump(element,"",0,rStart,elementMax);
		text=text.prechop("                ");	//* 16 spaces

		String countString;
		countString.sPrintf("[%d]",m_elementCount);

		feLog("\n%16s%s\n",countString.c_str(),text.c_str());

		String attrParse=m_attributes;
		String attrName;
		while(!(attrName=attrParse.parse()).empty())
		{
			U32 component=0;
			while(TRUE)
			{
				text=dump(element,attrName,component,rStart,elementMax);
				if(text.empty())
				{
					break;
				}

				feLog("%s\n",text.c_str());

				component++;
			}
		}
	}
}

void SpreadsheetOp::limitBounds(I32& a_rStart,I32& a_rEnd,
		I32 a_length,I32 a_count)
{
	if(a_rStart>a_count-a_length)
	{
		a_rStart=a_count-a_length;
	}
	if(a_rStart<0)
	{
		a_rStart=0;
	}

	a_rEnd=a_rStart+a_length-1;
	if(a_rEnd>a_count-1)
	{
		a_rEnd=a_count-1;
	}
	if(a_rEnd<0)
	{
		a_rEnd=0;
	}
}

String SpreadsheetOp::dump(Element a_element,String a_attrName,
	U32 a_component,U32 a_start,U32 a_end)
{
	String result;

	if(a_component>2)
	{
		return result;
	}

	if(a_attrName.empty())
	{
		result.catf("%16s","");
		for(U32 elementIndex=a_start;elementIndex<=a_end;elementIndex++)
		{
			result.catf("%10d",elementIndex);
		}

		return result;
	}

	sp<SurfaceAccessorI> spInputElement;
	if(!access(spInputElement,m_spInputAccessible,
			a_element,a_attrName,e_warning))
	{
		return result;
	}

	const String attrType=spInputElement->type();

	if(a_component && attrType!="vector3")
	{
		return result;
	}

	if(attrType=="string")
	{
		result.catf("%16s",a_attrName.c_str());

		for(U32 elementIndex=a_start;elementIndex<=a_end;elementIndex++)
		{
			const String value=spInputElement->string(elementIndex);
			result.catf("%10s",value.c_str());
		}
	}
	else if(attrType=="integer")
	{
		result.catf("%16s",a_attrName.c_str());

		for(U32 elementIndex=a_start;elementIndex<=a_end;elementIndex++)
		{
			const I32 value=spInputElement->integer(elementIndex);
			result.catf("%10d",value);
		}
	}
	else if(attrType=="real")
	{
		result.catf("%16s",a_attrName.c_str());

		for(U32 elementIndex=a_start;elementIndex<=a_end;elementIndex++)
		{
			const Real value=spInputElement->real(elementIndex);
			result.catf("%10.4G",value);
		}
	}
	else if(attrType=="vector3")
	{
		result.catf("%13s[%d]",a_attrName.c_str(),a_component);
		for(U32 elementIndex=a_start;elementIndex<=a_end;elementIndex++)
		{
			const SpatialVector value=
					spInputElement->spatialVector(elementIndex);
			result.catf("%10.4G",value[a_component]);
		}
	}

	return result;
}
