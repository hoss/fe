/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __operator_ClaspOp_h__
#define __operator_ClaspOp_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
    @brief Connect ties between buttons

	@ingroup operator
*//***************************************************************************/
class FE_DL_EXPORT ClaspOp:
	public OperatorSurfaceCommon,
	public Initialize<ClaspOp>
{
	public:
				ClaspOp(void)												{}
virtual			~ClaspOp(void)												{}

		void	initialize(void);

				//* As HandlerI
virtual void	handle(Record& a_rSignal);
};

} /* namespace ext */
} /* namespace fe */

#endif /* __operator_ClaspOp_h__ */

