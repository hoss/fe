/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <operator/operator.pmh>

#define FE_SBO_DEBUG	FALSE
#define FE_SBO_VERBOSE	FALSE

using namespace fe;
using namespace fe::ext;

void SurfaceBindOp::initialize(void)
{
	catalog<String>("DriverGroup")="";
	catalog<String>("DriverGroup","label")="Driver Group";
	catalog<String>("DriverGroup","hint")=
			"Primitive Group in Driver Surface that encompass the binding."
			"  Primitives outside this group will not be"
			" considered during binding."
			"  If no group is specified, the entire driver is made available.";

	catalog<I32>("Bindings")=1;
	catalog<I32>("Bindings","min")=1;
	catalog<I32>("Bindings","high")=8;
	catalog<I32>("Bindings","max")=1e3;
	catalog<bool>("Bindings","joined")=true;
	catalog<String>("Bindings","hint")=
			"Maximum number of attachment contacts.";

	catalog<bool>("Bind Primitives")=true;
	catalog<bool>("Bind Primitives","joined")=true;
	catalog<String>("Bind Primitives","hint")=
			"Affect whole primitives instead of individual points";

	catalog<bool>("Bind Fragments")=true;
	catalog<String>("Bind Fragments","hint")=
			"Bind collections of points or primitives with"
			" matching fragment attribute";

	catalog<String>("BindPrefix")="bind";
	catalog<String>("BindPrefix","label")="Bind Attribute Prefix";
	catalog<bool>("BindPrefix","joined")=true;
	catalog<String>("BindPrefix","hint")=
			"Prefix of attribute names added to output surface describing"
			" where each point on the input surface is attached to"
			" the associated driver";

	catalog<String>("FragmentAttr")="part";
	catalog<String>("FragmentAttr","label")="Fragment Attribute";
	catalog<String>("FragmentAttr","hint")=
			"Name of string attribute describing membership"
			" in a collection of primitives";

	catalog<bool>("LimitDistance")=false;
	catalog<String>("LimitDistance","label")="Limit Distance";
	catalog<bool>("LimitDistance","joined")=true;
	catalog<String>("LimitDistance","hint")=
			"Dismiss potential bindings beyond a specific distance.";

	catalog<Real>("MaxDistance")=1.0;
	catalog<Real>("MaxDistance","high")=10.0;
	catalog<Real>("MaxDistance","max")=1e6;
	catalog<String>("MaxDistance","label")="To";
	catalog<String>("MaxDistance","enabler")="LimitDistance";
	catalog<String>("MaxDistance","hint")=
			"Disallow bindings beyond this distance.";

	catalog<bool>("PartitionDriver")=false;
	catalog<String>("PartitionDriver","label")="Partition Driver";
	catalog<bool>("PartitionDriver","joined")=true;
	catalog<String>("PartitionDriver","hint")=
			"Restrict bindings by matching attribute values.";

	catalog<String>("DriverPartitionAttr")="partition";
	catalog<String>("DriverPartitionAttr","label")="Driver Attr";
	catalog<String>("DriverPartitionAttr","enabler")="PartitionDriver";
	catalog<bool>("DriverPartitionAttr","joined")=true;
	catalog<String>("DriverPartitionAttr","hint")=
			"Primitive string attribute on driver to use for bind matching."
			"  Generally, these values are just simple names.";

	catalog<String>("InputPartitionAttr")="partition";
	catalog<String>("InputPartitionAttr","label")="Input Attr";
	catalog<String>("InputPartitionAttr","enabler")="PartitionDriver";
	catalog<String>("InputPartitionAttr","hint")=
			"Point string attribute on input to use for bind matching."
			"  Each value can be a simple name or a full regex pattern."
			"  For example, use \"prefix.*\" for every name starting"
			" with particular value"
			" (the '.' is required to indicate 'any character' and"
			" the '*' means 'repeated without limit')"
			" or \"one|two|three\" to allow any one name from a list."
			"  Be careful with non-letter characters that may have"
			" a special meaning in regular expressions.";

	catalog<String>("PivotPoint")="First";
	catalog<String>("PivotPoint","label")="Pivot Point";
	catalog<String>("PivotPoint","choice:0")="First";
	catalog<String>("PivotPoint","choice:1")="Center";
	catalog<String>("PivotPoint","choice:2")="Nearest";
	catalog<String>("PivotPoint","hint")=
			"Location used to determine attachment.";

	catalog<bool>("Spider")=false;
	catalog<String>("Spider","label")="Spider Wrap";
	catalog<String>("Spider","hint")=
			"Track a spread of distant points.";

#if FE_CODEGEN>FE_DEBUG
	catalog<bool>("Spider","hidden")=true;
#else
	catalog<bool>("PivotPoint","joined")=true;
#endif

	catalog< sp<Component> >("Input Surface");
	catalog<String>("Input Surface","label")="Reference Input Surface";

	catalog< sp<Component> >("Driver Surface");
	catalog<String>("Driver Surface","label")="Reference Driver Surface";
}

void SurfaceBindOp::handle(Record& a_rSignal)
{
#if FE_SBO_DEBUG
	feLog("SurfaceBindOp::handle\n");
#endif

	//* output surface is copy of input surface
	sp<SurfaceAccessibleI> spOutputAccessible;
	if(!accessOutput(spOutputAccessible,a_rSignal)) return;

	const String driverGroup=catalog<String>("DriverGroup");
	sp<SurfaceI> spDriver;
	if(!access(spDriver,"Driver Surface",driverGroup)) return;

	sp<SurfaceAccessorI> spDriverNormals;
	if(!access(spDriverNormals,"Driver Surface",e_point,e_normal) ||
			!spDriverNormals->count())
	{
		catalog<String>("warning")=
				"Driver Surface has no normals (expect rotational problems);";
	}
	spDriverNormals=NULL;

	spDriver->setRefinement(0);

	if(spDriver->radius()<1e-6)
	{
		catalog<String>("error")=
				"Driver Surface appears empty (zero radius bounding sphere);";
		return;
	}

	const bool bindPrimitives=catalog<bool>("Bind Primitives");
	const String pivotPoint=catalog<String>("PivotPoint");
	const bool spiderWrap=catalog<bool>("Spider");
	const String fragmentAttribute=catalog<String>("FragmentAttr");
	const bool partitionDriver=catalog<bool>("PartitionDriver");
	const String inputPartitionAttr=catalog<String>("InputPartitionAttr");
	const String driverPartitionAttr=catalog<String>("DriverPartitionAttr");
	const BWORD limitDistance=catalog<bool>("LimitDistance");
	const Real maxDistance=catalog<Real>("MaxDistance");

	sp<SurfaceAccessorI> spOutputPrimitives;
	if((bindPrimitives || spiderWrap) && !access(spOutputPrimitives,
			spOutputAccessible,e_primitive,e_vertices)) return;

	sp<SurfaceAccessorI> spOutputElements;
	if(bindPrimitives)
	{
		spOutputElements=spOutputPrimitives;
	}
	else
	{
		if(!access(spOutputElements,spOutputAccessible,
				e_point,e_position)) return;
	}

	const bool fragmented=
			(catalog<bool>("Bind Fragments") && spOutputElements.isValid());

	I32 fragmentCount=1;
	if(fragmented)
	{
		const SurfaceAccessibleI::Element element=bindPrimitives?
				SurfaceAccessibleI::e_primitive: SurfaceAccessibleI::e_point;

		spOutputElements->fragmentWith(element,fragmentAttribute);
		fragmentCount=spOutputElements->fragmentCount();
	}

	sp<SurfaceAccessorI> spInputPartition;
	if(partitionDriver)
	{
		if(!access(spInputPartition,"Input Surface",
				bindPrimitives? e_primitive: e_point,inputPartitionAttr,
				e_error)) return;
		spDriver->partitionWith(driverPartitionAttr);
	}

	sp<SurfaceAccessorI> spOutputNormals;
	if(spiderWrap)
	{
		access(spOutputNormals,spOutputAccessible,e_point,e_normal,e_quiet);
	}

	std::vector< sp<SurfaceAccessorI> > outputFaces;
	std::vector< sp<SurfaceAccessorI> > outputBarys;
	const I32 bindings=accessBindings(spOutputAccessible,
			outputFaces,outputBarys,TRUE);
	if(bindings<1)
	{
		return;
	}

	I32 face= -1;
	Barycenter<Real> barycenter;

	const U32 elementCount=spOutputElements->count();

	SpatialVector* pOneEdge=NULL;
	if(spiderWrap)
	{
		pOneEdge=new SpatialVector[elementCount];
		for(U32 index=0;index<elementCount;index++)
		{
			set(pOneEdge[index],0.0,1.0,0.0);
		}
		const U32 primitiveCount=spOutputPrimitives->count();
		for(U32 index=0;index<primitiveCount;index++)
		{
			const U32 subCount=spOutputElements->subCount(index);
			if(subCount<2)
			{
				continue;
			}
			SpatialVector lastPoint=
					spOutputPrimitives->spatialVector(index,subCount-1);
			for(U32 subIndex=0;subIndex<subCount;subIndex++)
			{
				const I32 vertexIndex=
						spOutputPrimitives->integer(index,subIndex);
				const SpatialVector point=
						spOutputPrimitives->spatialVector(index,subIndex);
				pOneEdge[vertexIndex]=unit(point-lastPoint);
			}
		}
	}

	sp<SurfaceAccessorI::FilterI> spFilter;

	const U32 atomicCount=fragmented? fragmentCount: elementCount;
	for(U32 index=0;index<atomicCount;index++)
	{
		I32 filterCount=1;
		if(fragmented)
		{
			const String fragment=spOutputElements->fragment(index);
			if(!spOutputElements->filterWith(fragment,spFilter) ||
					spFilter.isNull())
			{
				continue;
			}
			filterCount=spFilter->filterCount();
			if(!filterCount)
			{
				continue;
			}
		}

		if(partitionDriver)
		{
			const I32 reIndex=fragmented? spFilter->filter(0): index;

			const String partition=spInputPartition->string(reIndex);
			spDriver->setPartitionFilter(partition);

#if FE_SBO_VERBOSE
			feLog("SurfaceBindOp::handle element %d/%d filter \"%s\"\n",
					index,atomicCount,filter.c_str());
#endif
		}

		SpatialVector pivot;

		if(pivotPoint=="Nearest")
		{
			set(pivot);
			Real bestDistance= -1.0;
			for(I32 filterIndex=0;filterIndex<filterCount;filterIndex++)
			{
				const I32 reIndex=fragmented?
						spFilter->filter(filterIndex): index;
				const U32 subCount=spOutputElements->subCount(reIndex);

				for(U32 subIndex=0;subIndex<subCount;subIndex++)
				{
					const SpatialVector point=
							spOutputElements->spatialVector(reIndex,subIndex);
					sp<SurfaceI::ImpactI> impact=spDriver->nearestPoint(point);
					const Real distance=impact->distance();
					if(bestDistance<0.0 || bestDistance>distance)
					{
						bestDistance=distance;
						pivot=point;
					}
				}
			}
		}
		else if(pivotPoint=="Center")
		{
			U32 sum=0;
			set(pivot);
			for(I32 filterIndex=0;filterIndex<filterCount;filterIndex++)
			{
				const I32 reIndex=fragmented?
						spFilter->filter(filterIndex): index;
				const U32 subCount=spOutputElements->subCount(reIndex);

				for(U32 subIndex=0;subIndex<subCount;subIndex++)
				{
					pivot+=spOutputElements->spatialVector(reIndex,subIndex);
				}
				sum+=subCount;
			}
			if(!sum)
			{
				continue;
			}
			pivot*=1.0/Real(sum);
		}
		else
		{
			const I32 reIndex=fragmented? spFilter->filter(0): index;
			if(!spOutputElements->subCount(reIndex))
			{
				continue;
			}
			pivot=spOutputElements->spatialVector(reIndex,0);
		}

		if(spiderWrap)
		{
			const SpatialVector norm=spOutputNormals.isValid()?
					spOutputNormals->spatialVector(index,0):
					SpatialVector(0.0,1.0,0.0);
			SpatialVector tangent=pOneEdge[index];
			SpatialVector side;
			cross(side,norm,tangent);
			cross(tangent,side,norm);

			SpatialVector sample[4];
			sample[0]=pivot;
			sample[1]=pivot+tangent;
			sample[2]=pivot-tangent+side;
			sample[3]=pivot-tangent-side;

			feLog("oneEdge %s tangent %s\n",
					c_print(pOneEdge[index]),c_print(tangent));

			for(U32 binding=0;binding<4;binding++)
			{
				std::vector< sp<SurfaceI::ImpactI> > impactArray=
						spDriver->nearestPoints(sample[binding],1);

				face= -1;
				set(barycenter);

				if(impactArray.size())
				{
					sp<SurfaceI::ImpactI>& rspImpact=impactArray[0];
					const Real distance=rspImpact->distance();
					if(!limitDistance || distance<=maxDistance)
					{
						face=rspImpact->triangleIndex();
						barycenter=rspImpact->barycenter();
					}
				}

				outputFaces[binding]->set(index,face);
				outputBarys[binding]->set(index,barycenter);
			}
		}
		else
		{
			std::vector< sp<SurfaceI::ImpactI> > impactArray=
					spDriver->nearestPoints(pivot,bindings);
			const U32 impacts=impactArray.size();

			for(U32 binding=0;binding<bindings;binding++)
			{
				face= -1;
				set(barycenter);

				if(binding<impacts && impactArray[binding].isValid())
				{
					sp<SurfaceI::ImpactI>& rspImpact=impactArray[binding];
					const Real distance=rspImpact->distance();
					if(!limitDistance || distance<=maxDistance)
					{
						face=rspImpact->triangleIndex();
						barycenter=rspImpact->barycenter();
					}
				}

				if(fragmented)
				{
					for(I32 filterIndex=0;filterIndex<filterCount;filterIndex++)
					{
						const I32 filtered=spFilter->filter(filterIndex);

						outputFaces[binding]->set(filtered,face);
						outputBarys[binding]->set(filtered,barycenter);
					}
				}
				else
				{
					outputFaces[binding]->set(index,face);
					outputBarys[binding]->set(index,barycenter);
				}
#if FE_SBO_VERBOSE
				feLog("SurfaceBindOp::handle element %d/%d bind %d/%d"
						" pivot %s face %d bary %s\n",
						index,atomicCount,binding,bindings,
						c_print(pivot),face,c_print(barycenter));
#endif
			}
		}
	}

	delete[] pOneEdge;

#if FE_SBO_DEBUG
	feLog("SurfaceBindOp::handle done\n");
#endif
}

I32 SurfaceBindOp::accessBindings(sp<SurfaceAccessibleI>& a_rspAccessibleI,
	std::vector< sp<SurfaceAccessorI> >&a_rFaces,
	std::vector< sp<SurfaceAccessorI> >&a_rBarys,
	BWORD a_allowCreation)
{
	const String bindPrefix=catalog<String>("BindPrefix");
	const bool spiderWrap=catalog<bool>("Spider");
	const I32 bindings=spiderWrap? 4: catalog<I32>("Bindings");
	const bool bindPrimitives=catalog<bool>("Bind Primitives");

	const SurfaceAccessibleI::Element element=bindPrimitives?
			SurfaceAccessibleI::e_primitive:
			SurfaceAccessibleI::e_point;

	const SurfaceAccessibleI::Creation create=a_allowCreation?
			SurfaceAccessibleI::e_createMissing:
			SurfaceAccessibleI::e_refuseMissing;

	a_rFaces.resize(bindings);
	a_rBarys.resize(bindings);
	U32 bindCount=0;
	for(U32 binding=0;binding<bindings;binding++)
	{
		String faceName;
		String baryName;
		faceName.sPrintf("%sFace%d",bindPrefix.c_str(),binding);
		baryName.sPrintf("%sBary%d",bindPrefix.c_str(),binding);

		a_rFaces[binding]=a_rspAccessibleI->accessor(element,
				faceName,create);
		if(a_rFaces[binding].isNull())
		{
			if(a_allowCreation)
			{
				feLog("SurfaceBindOp face attribute"
						" \"%s\" not accessible\n",faceName.c_str());
				catalog<String>("error")=
						"inaccessible face attribute \""+faceName+"\"";
				return -1;
			}
			break;
		}

		a_rBarys[binding]=a_rspAccessibleI->accessor(element,
				baryName,create);
		if(a_rBarys[binding].isNull())
		{
			if(a_allowCreation)
			{
				feLog("SurfaceBindOp barycentric attribute"
						" \"%s\" not accessible\n",baryName.c_str());
				catalog<String>("error")="inaccessible barycentric"
						" attribute \""+baryName+"\"";
				return -1;
			}
			break;
		}

		bindCount++;
	}

	return bindCount;
}
