/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <operator/operator.pmh>

//* TODO pick two points: show locations and distance

#define FE_RUL_DEBUG		FALSE
#define FE_RUL_FORE_ALPHA	0.1
#define FE_RUL_HAZE_ALPHA	0.05

namespace fe
{
namespace ext
{

RulerOp::RulerOp(void):
	m_hasCumulative(FALSE),
	m_pickedA(FALSE),
	m_pickedB(FALSE)
{
}

void RulerOp::initialize(void)
{
	catalog<String>("icon")="FE_alpha";

	catalog<bool>("orthoGrid")=true;
	catalog<String>("orthoGrid","label")="Ortho Grid";

	catalog<bool>("behind")=true;
	catalog<String>("behind","label")="Behind Object";
	catalog<String>("behind","enabler")="orthoGrid";

	catalog<bool>("splatter")=true;
	catalog<String>("splatter","label")="Splatter Points";
	catalog<String>("splatter","enabler")="orthoGrid";

	catalog<String>("walls")="all";
	catalog<String>("walls","label")="Show Walls";
	catalog<String>("walls","choice:0")="xy";
	catalog<String>("walls","label:0")="XY Wall";
	catalog<String>("walls","choice:1")="yz";
	catalog<String>("walls","label:1")="YZ Wall";
	catalog<String>("walls","choice:2")="xz";
	catalog<String>("walls","label:2")="XZ Wall";
	catalog<String>("walls","choice:3")="all";
	catalog<String>("walls","label:3")="All Walls";
	catalog<String>("walls","enabler")="orthoGrid";

	catalog<bool>("crossGrid")=false;
	catalog<String>("crossGrid","label")="Cross Grid";

	catalog<bool>("outline")=false;
	catalog<String>("outline","label")="Outline";

	catalog<bool>("cumulative")=true;
	catalog<String>("cumulative","label")="Cumulative Bounds";
	catalog<bool>("cumulative","joined")=true;

	catalog<I32>("clear")=0;
	catalog<String>("clear","label")="Clear";
	catalog<String>("clear","suggest")="button";

	catalog<Real>("base")=10.0;
	catalog<Real>("base","min")=2.0;
	catalog<Real>("base","max")=10.0;
	catalog<String>("base","label")="Base";

	catalog<Real>("excess")=0.0;
	catalog<Real>("excess","high")=10.0;
	catalog<Real>("excess","max")=100.0;
	catalog<String>("excess","label")="Excess";

	catalog< sp<Component> >("Brush");
	catalog<String>("Brush","implementation")="DrawI";
	catalog<String>("Brush","visible")="any";
	catalog<String>("Brush","prompt")="Prompt here.";

	catalog< sp<Component> >("Input Surface");

	m_spBrush=new DrawMode();
	m_spBrush->setDrawStyle(DrawMode::e_foreshadow);
	m_spBrush->setLineWidth(2.0);
	m_spBrush->setAntialias(TRUE);
	m_spBrush->setLit(FALSE);

	m_spHaze=new DrawMode();
	m_spHaze->setDrawStyle(DrawMode::e_solid);
	m_spHaze->setLineWidth(2.0);
	m_spHaze->setAntialias(TRUE);
	m_spHaze->setBackfaceCulling(FALSE);
	m_spHaze->setLit(FALSE);

	m_spOverlay=new DrawMode();
	m_spOverlay->setDrawStyle(DrawMode::e_solid);
	m_spOverlay->setLit(FALSE);

	m_spSplatter=new DrawMode();
	m_spSplatter->setDrawStyle(DrawMode::e_solid);
	m_spSplatter->setLit(FALSE);
	m_spSplatter->setPointSize(FALSE);
}

void RulerOp::handle(Record& a_rSignal)
{
#if FE_RUL_DEBUG
	feLog("RulerOp::handle\n");
#endif

	sp<DrawI> spDrawBrush;
	accessBrush(spDrawBrush,a_rSignal,e_quiet);

	sp<DrawI> spDrawOverlay;
	accessBrushOverlay(spDrawOverlay,a_rSignal,e_quiet);

	if(spDrawBrush.isValid())
	{
		if(m_spInputAccessible.isNull())
		{
			feLog("RulerOp::handle input surface not accessible\n");
			return;
		}

		const BWORD orthoGrid=catalog<bool>("orthoGrid");
		const BWORD outline=catalog<bool>("outline");

		const SpatialVector& rRayOrigin=rayOrigin(a_rSignal);
		const SpatialVector& rRayDirection=rayDirection(a_rSignal);

		sp<ViewI> spView=spDrawOverlay->view();
		sp<CameraI> spCameraI=spView->camera();
		const Box2i viewport=spView->viewport();

		const I32 width=viewport.size()[0];
		const I32 height=viewport.size()[1];

		SpatialTransform cameraMatrix=spCameraI->cameraMatrix();
		SpatialTransform cameraTransform;
		invert(cameraTransform,cameraMatrix);

//		const SpatialVector cameraDir= -cameraTransform.column(2);
		const SpatialVector cameraPos=cameraTransform.column(3);

		m_event.bind(windowEvent(a_rSignal));

#if FE_RUL_DEBUG
		feLog("  %s\n",c_print(m_event));
#endif

		sp<SurfaceAccessorI> spInputPosition;
		if(!access(spInputPosition,m_spInputAccessible,
				e_point,e_position)) return;

		const I32 pointCount=spInputPosition->count();
		if(!pointCount)
		{
			catalog<String>("warning")+="No input points;";
			return;
		}

		sp<SurfaceAccessorI> spInputVertices;
		if(!access(spInputVertices,m_spInputAccessible,
				e_primitive,e_vertices)) return;

		const Color hardBlack(0.0,0.0,0.0);
		const Color hardGrey(0.6,0.6,0.6);
		const Color hardWhite(1.0,1.0,1.0);

		const Real foreAlpha=FE_RUL_FORE_ALPHA;
		const Color blue(0.0,0.0,1.0,foreAlpha);
		const Color cyan(0.0,1.0,1.0,foreAlpha);
		const Color magenta(1.0,0.0,1.0,foreAlpha);
		const Color yellow(1.0,1.0,0.0,foreAlpha);

		scan(spDrawOverlay);

		spDrawOverlay->pushDrawMode(m_spOverlay);

		if(orthoGrid)
		{
			drawOrtho(spDrawBrush,spDrawOverlay);
		}

		spDrawBrush->pushDrawMode(m_spBrush);

		m_spSplatter->setPointSize(spDrawBrush->multiplication());

		m_spBrush->setLineWidth(2*spDrawBrush->multiplication());
		m_spHaze->setLineWidth(2*spDrawBrush->multiplication());

		SpatialVector line[2];
		const U32 margin=1;
		String text;

		SpatialVector planeCenter(0.0,0.0,0.0);
		SpatialVector planeNormal(0.0,1.0,0.0);

		const BWORD centered=TRUE;
		const Real dotRadius=3.0;

		sp<FontI> spFont=spDrawBrush->font();
		if(spFont.isNull())
		{
			feLog("RulerOp::handle invalid font\n");
		}

		const I32 yInc=spFont.isValid()?
				1.5*spFont->fontHeight(NULL,NULL): 8;

		Vector2i location(0.5*width,height-yInc);

		BWORD impacted=FALSE;
		SpatialVector intersection(0.0,0.0,0.0);
		SpatialVector norm(0.0,1.0,0.0);
		I32 triIndex= -1;
		SpatialBary barycenter(0.0,0.0);

		if(m_spInput.isValid())
		{
			const Real maxDistance= -1.0;
			sp<SurfaceI::ImpactI> spImpact=m_spInput->rayImpact(
					rRayOrigin,rRayDirection,maxDistance);
			if(spImpact.isValid())
			{
				impacted=TRUE;
				intersection=spImpact->intersection();
				norm=spImpact->normal();
				barycenter=spImpact->barycenter();

				sp<SurfaceSearchable::Impact> spSearchImpact=spImpact;
				if(spSearchImpact.isValid())
				{
					triIndex=spSearchImpact->triangleIndex();
				}

				line[0]=intersection;

				for(U32 m=0;m<3;m++)
				{
					line[1]=line[0];
					line[1][m]=m_back[m];

					Color axisColor=hardBlack;
					axisColor[m]=1.0;
					axisColor[3]=foreAlpha;

					spDrawBrush->drawLines(line,NULL,2,
							DrawI::e_discrete,false,&axisColor);
				}

				drawDot(spView,spDrawOverlay,line[0],dotRadius,hardWhite);
			}
		}

		if(m_event.isKeyPress(WindowEvent::e_keyEscape))
		{
			m_pickedA=FALSE;
			m_pickedB=FALSE;
		}

		if(m_event.isMouseRelease(WindowEvent::e_itemLeft))
		{
			if(impacted)
			{
				m_pointA=intersection;
				m_normA=norm;
				m_triIndexA=triIndex;
				m_barycenterA=barycenter;
				m_pickedA=TRUE;
			}
			else
			{
				m_pickedA=FALSE;
			}
		}

		if(m_event.isMouseRelease(WindowEvent::e_itemMiddle))
		{
			if(impacted)
			{
				m_pointB=intersection;
				m_normB=norm;
				m_triIndexB=triIndex;
				m_barycenterB=barycenter;
				m_pickedB=TRUE;
			}
			else
			{
				m_pickedB=FALSE;
			}
		}

		planeCenter=intersection;
		planeNormal=norm;
		BWORD triPlane=TRUE;
		BWORD showCrossSection=impacted;

		if(m_pickedA)
		{
			if(m_triIndexA>=0 && m_spInput.isValid())
			{
				sp<SurfaceI::ImpactI> spImpact=m_spInput->sampleImpact(
						m_triIndexA,m_barycenterA);
				m_pointA=spImpact->intersection();
				m_normA=spImpact->normal();
			}

			drawDot(spView,spDrawOverlay,m_pointA,dotRadius,hardBlack);

			text.sPrintf("A %s",c_print(m_pointA));

			drawLabel(spDrawOverlay,location,text,
					centered,margin,hardWhite,NULL,&blue);
			location[1]-=yInc;

			if(!m_pickedB)
			{
				planeCenter=m_pointA;
				showCrossSection=TRUE;
			}
		}

		if(m_pickedB)
		{
			if(m_triIndexB>=0 && m_spInput.isValid())
			{
				sp<SurfaceI::ImpactI> spImpact=m_spInput->sampleImpact(
						m_triIndexB,m_barycenterB);
				m_pointB=spImpact->intersection();
				m_normB=spImpact->normal();
			}

			drawDot(spView,spDrawOverlay,m_pointB,dotRadius,hardGrey);

			text.sPrintf("B %s",c_print(m_pointB));

			drawLabel(spDrawOverlay,location,text,
					centered,margin,hardWhite,NULL,&blue);
			location[1]-=yInc;

			if(!m_pickedA)
			{
				const SpatialVector away=intersection-m_pointB;
				const Real awayDot=dot(away,m_normB);
				planeCenter=m_pointB+awayDot*m_normB;
				planeNormal=m_normB;
				triPlane=FALSE;
			}
		}

		if(m_pickedA && m_pickedB)
		{
			const SpatialVector delta=m_pointB-m_pointA;
			const Real distance=magnitude(delta);

			text.sPrintf("delta %s",c_print(delta));

			drawLabel(spDrawOverlay,location,text,
					centered,margin,hardWhite,NULL,&blue);
			location[1]-=yInc;

			text.sPrintf("distance %.6G",distance);

			drawLabel(spDrawOverlay,location,text,
					centered,margin,hardWhite,NULL,&blue);
			location[1]-=yInc;

			planeNormal=unitSafe(cross(delta,m_normA+m_normB));
			triPlane=FALSE;
		}

		if(impacted)
		{
			text.sPrintf("%s",c_print(intersection));

			drawLabel(spDrawOverlay,location,text,
					centered,margin,hardWhite,NULL,&blue);
			location[1]-=yInc;
		}

		if(showCrossSection)
		{
			if(triPlane)
			{
				const SpatialVector origCenter=planeCenter;

				set(planeCenter,origCenter[0],0.0,0.0);
				set(planeNormal,1.0,0.0,0.0);
				drawCrossSection(spDrawBrush,spInputVertices,
						planeCenter,planeNormal,cyan);

				set(planeCenter,0.0,origCenter[1],0.0);
				set(planeNormal,0.0,1.0,0.0);
				drawCrossSection(spDrawBrush,spInputVertices,
						planeCenter,planeNormal,magenta);

				set(planeCenter,0.0,0.0,origCenter[2]);
				set(planeNormal,0.0,0.0,1.0);
				drawCrossSection(spDrawBrush,spInputVertices,
						planeCenter,planeNormal,yellow);
			}
			else
			{
				drawCrossSection(spDrawBrush,spInputVertices,
					planeCenter,planeNormal,yellow);
			}
		}

		if(outline)
		{
			const Color alphaBlack(0.0,0.0,0.0,foreAlpha);

			drawOutline(spDrawBrush,m_spInputAccessible,"","",
					cameraPos,alphaBlack);
		}

		spDrawOverlay->popDrawMode();
		spDrawBrush->popDrawMode();

#if FE_RUL_DEBUG
		feLog("RulerOp::handle brush done\n");
#endif

		return;
	}

	const BWORD inputReplaced=
			catalogOrDefault<bool>("Input Surface","replaced",true);
	if(inputReplaced || m_spInput.isNull())
	{
		if(!access(m_spInput,"Input Surface",e_warning)) return;
	}

	if(!access(m_spInputAccessible,"Input Surface")) return;

#if FE_RUL_DEBUG
	feLog("RulerOp::handle done\n");
#endif
}

void RulerOp::drawOrthoGrid(sp<DrawI> a_spDraw3D,sp<DrawI> a_spDraw2D,
	sp<SurfaceAccessibleI> a_spSurfaceAccessibleI)
{
	if((a_spDraw3D.isNull() && a_spDraw2D.isNull()) ||
			a_spSurfaceAccessibleI.isNull())
	{
		return;
	}

	m_spInputAccessible=a_spSurfaceAccessibleI;
	scan(a_spDraw3D.isValid()? a_spDraw3D: a_spDraw2D);
	drawOrtho(a_spDraw3D,a_spDraw2D);
}

void RulerOp::scan(sp<DrawI> a_spDraw)
{
	sp<SurfaceAccessorI> spInputPosition;
	access(spInputPosition,m_spInputAccessible,e_point,e_position);

	const I32 pointCount=
			spInputPosition.isValid()? spInputPosition->count(): 0;

	if(pointCount)
	{
		for(I32 pointIndex=0;pointIndex<pointCount;pointIndex++)
		{
			const SpatialVector point=
					spInputPosition->spatialVector(pointIndex);
			for(U32 m=0;m<3;m++)
			{
				if(!pointIndex || m_minCorner[m]>point[m])
				{
					m_minCorner[m]=point[m];
				}
				if(!pointIndex || m_maxCorner[m]<point[m])
				{
					m_maxCorner[m]=point[m];
				}
			}
		}
	}
	else
	{
		sp<SurfaceI> spInputSurface;
		if(!access(spInputSurface,m_spInputAccessible)) return;

		const SpatialVector center=spInputSurface->center();
		const Real radius=spInputSurface->radius();
		const SpatialVector radiusVector(radius,radius,radius);

		m_minCorner=center-radiusVector;
		m_maxCorner=center+radiusVector;
	}

//	feLog("RulerOp::scan %s %s\n",c_print(m_minCorner),c_print(m_maxCorner));

	I32& rClearCount=catalog<I32>("clear");
	if(rClearCount)
	{
		m_hasCumulative=FALSE;
		rClearCount=0;
	}
	if(catalog<bool>("cumulative"))
	{
		if(m_hasCumulative)
		{
			for(I32 m=0;m<3;m++)
			{
				if(m_minCumulative[m]>m_minCorner[m])
				{
					m_minCumulative[m]=m_minCorner[m];
				}
				if(m_maxCumulative[m]<m_maxCorner[m])
				{
					m_maxCumulative[m]=m_maxCorner[m];
				}
			}
			m_minCorner=m_minCumulative;
			m_maxCorner=m_maxCumulative;
		}
		else
		{
			m_minCumulative=m_minCorner;
			m_maxCumulative=m_maxCorner;
			m_hasCumulative=TRUE;
		}
	}
	else
	{
		m_hasCumulative=FALSE;
	}

	const SpatialVector boundSize=m_maxCorner-m_minCorner;
	const SpatialVector center=m_minCorner+0.5*boundSize;

	sp<ViewI> spView=a_spDraw->view();
	sp<CameraI> spCameraI=spView->camera();

	SpatialTransform cameraMatrix=spCameraI->cameraMatrix();
	SpatialTransform cameraTransform;
	invert(cameraTransform,cameraMatrix);

	const SpatialVector cameraDir= -cameraTransform.column(2);

	const I32 pixels=256;
	const Real worldSize=spView->worldSize(center,pixels);

	const BWORD behind=catalog<bool>("behind");
	const Real excess=catalog<Real>("excess");
	const Real base=catalog<Real>("base");

	//* log_n(x) = log_d(x)/log_d(b)  (for any convenient d)
	const I32 intLog=(worldSize>0.0)? (I32)(log(worldSize)/log(base)): 0;
	m_step=pow(base,intLog);

	const Real beyond=0.01*m_step;

	const Real startX=floor((m_minCorner[0]-excess)/m_step)*m_step;
	const Real endX=floor((m_maxCorner[0]+excess)/m_step+1.0)*m_step+beyond;

	const Real startY=floor((m_minCorner[1]-excess)/m_step)*m_step;
	const Real endY=floor((m_maxCorner[1]+excess)/m_step+1.0)*m_step+beyond;

	const Real startZ=floor((m_minCorner[2]-excess)/m_step)*m_step;
	const Real endZ=floor((m_maxCorner[2]+excess)/m_step+1.0)*m_step+beyond;

	const BWORD highX=((cameraDir[0]>0.0) == behind);
	const BWORD highY=((cameraDir[1]>0.0) == behind);
	const BWORD highZ=((cameraDir[2]>0.0) == behind);

	const Real backX=highX? endX: startX;
	const Real backY=highY? endY: startY;
	const Real backZ=highZ? endZ: startZ;

	set(m_back,backX,backY,backZ);
	set(m_start,startX,startY,startZ);
	set(m_end,endX,endY,endZ);

#if FALSE	//FE_RUL_DEBUG
	feLog("RulerOp::handle bounds min %s max %s\n",
			c_print(m_minCorner),c_print(m_maxCorner));
	feLog("  worldSize %.6G intLog %d m_step %.6G\n",
			worldSize,intLog,m_step);
	feLog("  x %.6G to %.6G back %.6G\n",startX,endX,backX);
	feLog("  y %.6G to %.6G back %.6G\n",startY,endY,backY);
	feLog("  z %.6G to %.6G back %.6G\n",startZ,endZ,backZ);
#endif
}

void RulerOp::drawOrtho(sp<DrawI> a_spDraw3D,sp<DrawI> a_spDraw2D)
{
	sp<SurfaceAccessorI> spInputPosition;
	access(spInputPosition,m_spInputAccessible,e_point,e_position);

	const I32 pointCount=
			spInputPosition.isValid()? spInputPosition->count(): 0;

	sp<SurfaceAccessorI> spInputVertices;
	access(spInputVertices,m_spInputAccessible,e_primitive,e_vertices);

	const BWORD crossGrid=catalog<bool>("crossGrid");
	const String walls=catalog<String>("walls");
	const BWORD wallXY=(walls=="all" || walls=="xy");
	const BWORD wallYZ=(walls=="all" || walls=="yz");
	const BWORD wallXZ=(walls=="all" || walls=="xz");

	const Color hardBlack(0.0,0.0,0.0);
	const Color hardWhite(1.0,1.0,1.0);

	const Real foreAlpha=FE_RUL_FORE_ALPHA;
	const Color red(1.0,0.0,0.0,foreAlpha);
	const Color green(0.0,1.0,0.0,foreAlpha);
	const Color blue(0.0,0.0,1.0,foreAlpha);
	const Color cyan(0.0,1.0,1.0,foreAlpha);
	const Color magenta(1.0,0.0,1.0,foreAlpha);
	const Color yellow(1.0,1.0,0.0,foreAlpha);

	const Real hazeAlpha=FE_RUL_HAZE_ALPHA;
	const Color hazeCyan(0.0,1.0,1.0,hazeAlpha);
	const Color hazeMagenta(1.0,0.0,1.0,hazeAlpha);
	const Color hazeYellow(1.0,1.0,0.0,hazeAlpha);

	if(a_spDraw3D.isValid())
	{
		a_spDraw3D->pushDrawMode(m_spHaze);

		SpatialVector wallVert[4];
		SpatialVector wallNorm[4];

		if(wallYZ)
		{
			set(wallVert[0],m_back[0],m_start[1],m_start[2]);
			set(wallVert[1],m_back[0],m_start[1],m_end[2]);
			set(wallVert[2],m_back[0],m_end[1],m_start[2]);
			set(wallVert[3],m_back[0],m_end[1],m_end[2]);

			set(wallNorm[0],1.0,0.0,0.0);
			set(wallNorm[1],1.0,0.0,0.0);
			set(wallNorm[2],1.0,0.0,0.0);
			set(wallNorm[3],1.0,0.0,0.0);

			a_spDraw3D->drawTriangles(wallVert,wallNorm,NULL,4,
					DrawI::e_strip,FALSE,&hazeCyan);
		}

		if(wallXZ)
		{
			set(wallVert[0],m_start[0],m_back[1],m_start[2]);
			set(wallVert[1],m_start[0],m_back[1],m_end[2]);
			set(wallVert[2],m_end[0],m_back[1],m_start[2]);
			set(wallVert[3],m_end[0],m_back[1],m_end[2]);

			set(wallNorm[0],0.0,1.0,0.0);
			set(wallNorm[1],0.0,1.0,0.0);
			set(wallNorm[2],0.0,1.0,0.0);
			set(wallNorm[3],0.0,1.0,0.0);

			a_spDraw3D->drawTriangles(wallVert,wallNorm,NULL,4,
					DrawI::e_strip,FALSE,&hazeMagenta);
		}

		if(wallXY)
		{
			set(wallVert[0],m_start[0],m_start[1],m_back[2]);
			set(wallVert[1],m_start[0],m_end[1],m_back[2]);
			set(wallVert[2],m_end[0],m_start[1],m_back[2]);
			set(wallVert[3],m_end[0],m_end[1],m_back[2]);

			set(wallNorm[0],0.0,0.0,1.0);
			set(wallNorm[1],0.0,0.0,1.0);
			set(wallNorm[2],0.0,0.0,1.0);
			set(wallNorm[3],0.0,0.0,1.0);

			a_spDraw3D->drawTriangles(wallVert,wallNorm,NULL,4,
					DrawI::e_strip,FALSE,&hazeYellow);
		}

		a_spDraw3D->popDrawMode();

		a_spDraw3D->pushDrawMode(m_spBrush);
	}

	const Real stepShort=0.1*m_step;

	sp<ViewI> spView;
	if(a_spDraw2D.isValid())
	{
		spView=a_spDraw2D->view();
	}

	//* TODO loop through X,Y,Z

	SpatialVector line[2];
	const U32 margin=1;
	String text;

	SpatialVector planeCenter(0.0,0.0,0.0);
	SpatialVector planeNormal(0.0,1.0,0.0);

	for(Real gridX=m_start[0];gridX<m_end[0];gridX+=m_step)
	{
		const BWORD onEdge=(fabs(gridX-m_back[0])<stepShort);

		if(a_spDraw3D.isValid() && (wallXY || (wallYZ && onEdge)))
		{
			set(line[0],gridX,m_start[1],m_back[2]);
			set(line[1],gridX,m_end[1],m_back[2]);

			a_spDraw3D->drawLines(line,NULL,2,
					DrawI::e_discrete,false,onEdge? &green: &yellow);
		}

		if(!onEdge)
		{
			if(a_spDraw3D.isValid() && wallXZ)
			{
				set(line[0],gridX,m_back[1],m_start[2]);
				set(line[1],gridX,m_back[1],m_end[2]);

				a_spDraw3D->drawLines(line,NULL,
						2,DrawI::e_discrete,false,&magenta);
			}

			if(spView.isValid() && (wallXY || wallXZ))
			{
				text.sPrintf("%.6G",gridX);

				const SpatialVector edgePoint(gridX,m_back[1],m_back[2]);

				const SpatialVector projected=spView->project(
						edgePoint,ViewI::e_perspective);
				const Vector2i win(projected);

				drawLabel(a_spDraw2D,win,text,
						FALSE,margin,hardWhite,NULL,&blue);
			}
		}

		if(crossGrid && a_spDraw3D.isValid() && wallYZ)
		{
			set(planeCenter,gridX,0.0,0.0);
			set(planeNormal,1.0,0.0,0.0);
			drawCrossSection(a_spDraw3D,spInputVertices,
					planeCenter,planeNormal,cyan);
		}
	}

	for(Real gridY=m_start[1];gridY<m_end[1];gridY+=m_step)
	{
		const BWORD onEdge=(fabs(gridY-m_back[1])<stepShort);

		if(a_spDraw3D.isValid() && (wallYZ || (wallXZ && onEdge)))
		{
			set(line[0],m_back[0],gridY,m_start[2]);
			set(line[1],m_back[0],gridY,m_end[2]);

			a_spDraw3D->drawLines(line,NULL,2,
					DrawI::e_discrete,false,onEdge? &blue: &cyan);
		}

		if(!onEdge)
		{
			if(a_spDraw3D.isValid() && wallXY)
			{
				set(line[0],m_start[0],gridY,m_back[2]);
				set(line[1],m_end[0],gridY,m_back[2]);

				a_spDraw3D->drawLines(line,NULL,2,
						DrawI::e_discrete,false,&yellow);
			}

			if(spView.isValid() && (wallXY || wallYZ))
			{
				text.sPrintf("%.6G",gridY);

				const SpatialVector edgePoint(m_back[0],gridY,m_back[2]);

				const Vector2i projected=spView->project(
						edgePoint,ViewI::e_perspective);

				drawLabel(a_spDraw2D,projected,text,
						FALSE,margin,hardWhite,NULL,&blue);
			}
		}

		if(crossGrid && a_spDraw3D.isValid() && wallXZ)
		{
			set(planeCenter,0.0,gridY,0.0);
			set(planeNormal,0.0,1.0,0.0);
			drawCrossSection(a_spDraw3D,spInputVertices,
					planeCenter,planeNormal,magenta);
		}
	}

	for(Real gridZ=m_start[2];gridZ<m_end[2];gridZ+=m_step)
	{
		const BWORD onEdge=(fabs(gridZ-m_back[2])<stepShort);

		if(a_spDraw3D.isValid() && (wallXZ || (wallXY && onEdge)))
		{
			set(line[0],m_start[0],m_back[1],gridZ);
			set(line[1],m_end[0],m_back[1],gridZ);

			a_spDraw3D->drawLines(line,NULL,2,
					DrawI::e_discrete,false,onEdge? &red: &magenta);
		}

		if(!onEdge)
		{
			if(a_spDraw3D.isValid() && wallYZ)
			{
				set(line[0],m_back[0],m_start[1],gridZ);
				set(line[1],m_back[0],m_end[1],gridZ);

				a_spDraw3D->drawLines(line,NULL,2,
						DrawI::e_discrete,false,&cyan);
			}

			if(spView.isValid() && (wallYZ || wallXZ))
			{
				text.sPrintf("%.6G",gridZ);

				const SpatialVector edgePoint(m_back[0],m_back[1],gridZ);

				const Vector2i projected=spView->project(
						edgePoint,ViewI::e_perspective);

				drawLabel(a_spDraw2D,projected,text,
						FALSE,margin,hardWhite,NULL,&blue);
			}
		}

		if(crossGrid && a_spDraw3D.isValid() && wallXY)
		{
			set(planeCenter,0.0,0.0,gridZ);
			set(planeNormal,0.0,0.0,1.0);
			drawCrossSection(a_spDraw3D,spInputVertices,
					planeCenter,planeNormal,yellow);
		}
	}

	if(a_spDraw3D.isValid())
	{
		a_spDraw3D->popDrawMode();

		if(catalog<bool>("splatter"))
		{
			a_spDraw3D->pushDrawMode(m_spSplatter);

			const SpatialVector boundSize=m_maxCorner-m_minCorner;

			const I32 preferredCount=10000;
			const I32 stride=fe::maximum(I32(1),pointCount/preferredCount);

			const I32 reducedCount=pointCount/stride;

//			feLog("pointCount %d stride %d reducedCount %d\n",
//					pointCount,stride,reducedCount);

			SpatialVector* pointArray=new SpatialVector[pointCount];
			Color* colorArray=new Color[pointCount];

			for(U32 m=0;m<3;m++)
			{
				if((m==0 && !wallYZ) || (m==1 && !wallXZ) || (m==2 && !wallXY))
				{
					continue;
				}

				for(I32 reIndex=0;reIndex<reducedCount;reIndex++)
				{
					const I32 pointIndex=reIndex*stride;
					FEASSERT(pointIndex<pointCount);

					const SpatialVector point=
							spInputPosition->spatialVector(pointIndex);

					pointArray[reIndex]=point;
					colorArray[reIndex]=hardBlack;

					pointArray[reIndex][m]=m_back[m];
					colorArray[reIndex][m]=
							(point[m]-m_minCorner[m])/boundSize[m];
				}

				a_spDraw3D->drawPoints(pointArray,NULL,
						reducedCount,true,colorArray);
			}

			delete[] colorArray;
			delete[] pointArray;

			a_spDraw3D->popDrawMode();
		}
	}
}

} /* namespace ext */
} /* namespace fe */
