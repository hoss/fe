/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <operator/operator.pmh>

#define FE_GPO_DEBUG	FALSE

namespace fe
{
namespace ext
{

void GroupPromoteOp::initialize(void)
{
	catalog<String>("icon")="FE_alpha";

	catalog<String>("groups")="";
	catalog<String>("groups","label")="Groups";
	catalog<bool>("groups","joined")=true;
	catalog<String>("groups","hint")=
		"Space delimited list of groups to promote.";

	catalog<bool>("regex")=false;
	catalog<String>("regex","label")="Regex";
	catalog<String>("regex","hint")=
			"Pass the pattern strings directly as true regular expressions."
			"  If this option is off,"
			" presumed glob patterns are converted to regex."
			"  Every '.' is first escaped as '\\.'."
			"  Then, every '?', indicating 'any character',"
			" is converted to '.'."
			"  Every '*', indicating 'any number of any characters',"
			" is converted to '.*'."
			"  Every '[!', indicating 'not' inside of square brackets,"
			" is converted to '[^'.";

	catalog<String>("fromClass")="auto";
	catalog<String>("fromClass","choice:0")="auto";
	catalog<String>("fromClass","label:0")="Auto";
	catalog<String>("fromClass","choice:1")="point";
	catalog<String>("fromClass","label:1")="Point";
	catalog<String>("fromClass","choice:2")="primitive";
	catalog<String>("fromClass","label:2")="Primitive";
	catalog<String>("fromClass","label")="From";

	catalog<String>("toClass")="auto";
	catalog<String>("toClass","choice:0")="auto";
	catalog<String>("toClass","label:0")="Auto";
	catalog<String>("toClass","choice:1")="point";
	catalog<String>("toClass","label:1")="Point";
	catalog<String>("toClass","choice:2")="primitive";
	catalog<String>("toClass","label:2")="Primitive";
	catalog<String>("toClass","label")="To";

	catalog< sp<Component> >("Input Surface");

	catalog< sp<Component> >("Output Surface");
	catalog<String>("Output Surface","IO")="output";
	catalog<String>("Output Surface","copy")="Input Surface";
	catalog<bool>("Output Surface","clobber")=true;
}

void GroupPromoteOp::handle(Record& a_rSignal)
{
#if FE_GPO_DEBUG
	feLog("GroupPromoteOp::handle\n");
#endif

	String groups=catalog<String>("groups");
	if(!groups.empty() && !catalog<bool>("regex"))
	{
		groups=groups.convertGlobToRegex();
	}

	const String fromClass=catalog<String>("fromClass");
	const String toClass=catalog<String>("toClass");

	sp<SurfaceAccessibleI> spOutputAccessible;
	if(!accessOutput(spOutputAccessible,a_rSignal)) return;

	Array<String> patterns;
	while(TRUE)
	{
		String token=groups.parse();
		if(token.empty())
		{
			break;
		}

		patterns.push_back(token);
	}

	sp<SurfaceAccessorI> spOutputVertices;
	if(!access(spOutputVertices,spOutputAccessible,
			e_primitive,e_vertices)) return;

	const I32 primitiveCount=spOutputVertices->count();

	I32 promotionCount(0);

	Array<String> pointGroupNames;
	spOutputAccessible->groupNames(pointGroupNames,
			SurfaceAccessibleI::e_pointGroup);

	Array<String> primitiveGroupNames;
	spOutputAccessible->groupNames(primitiveGroupNames,
			SurfaceAccessibleI::e_primitiveGroup);

	if(fromClass!="primitive" && toClass!="point")
	{
		for(String& rName: pointGroupNames)
		{
			for(String& rPattern: patterns)
			{
				if(rName.match(rPattern))
				{
					//* create group of primitives that contain points

					std::set<I32> pointIndices;

					sp<SurfaceAccessorI> spPointGroup;
					if(!access(spPointGroup,spOutputAccessible,
							e_pointGroup,rName,e_warning,e_refuseMissing))
					{
						continue;
					}

					const I32 entryCount=spPointGroup->count();
					for(I32 entryIndex=0;entryIndex<entryCount;entryIndex++)
					{
						const I32 pointIndex=
								spPointGroup->integer(entryIndex);
						pointIndices.insert(pointIndex);
					}

					spPointGroup=NULL;

					discard(spOutputAccessible,e_pointGroup,rName);

					sp<SurfaceAccessorI> spPrimitiveGroup;
					if(!access(spPrimitiveGroup,spOutputAccessible,
							e_primitiveGroup,rName,e_warning,e_refuseMissing))
					{
						continue;
					}

					std::set<I32> primitiveIndices;

					for(I32 primitiveIndex=0;primitiveIndex<primitiveCount;
							primitiveIndex++)
					{
						const I32 subCount=
								spOutputVertices->subCount(primitiveIndex);
						for(I32 subIndex=0;subIndex<subCount;subIndex++)
						{
							const I32 pointIndex=spOutputVertices->integer(
									primitiveIndex,subIndex);
							if(pointIndices.find(pointIndex)!=
									pointIndices.end())
							{
								primitiveIndices.insert(primitiveIndex);
								break;
							}
						}
					}

					for(I32 primitiveIndex: primitiveIndices)
					{
						spPrimitiveGroup->append(primitiveIndex);
					}

					promotionCount++;
				}
			}
		}
	}

	if(fromClass!="point" && toClass!="primitive")
	{
		for(String& rName: primitiveGroupNames)
		{
			for(String& rPattern: patterns)
			{
				if(rName.match(rPattern))
				{
					//* create group of points in primitives

					std::set<I32> indices;

					sp<SurfaceAccessorI> spPrimitiveGroup;
					if(!access(spPrimitiveGroup,spOutputAccessible,
							e_primitiveGroup,rName,e_warning,e_refuseMissing))
					{
						continue;
					}

					const I32 entryCount=spPrimitiveGroup->count();
					for(I32 entryIndex=0;entryIndex<entryCount;entryIndex++)
					{
						const I32 primitiveIndex=
								spPrimitiveGroup->integer(entryIndex);
						const I32 subCount=
								spOutputVertices->subCount(primitiveIndex);
						for(I32 subIndex=0;subIndex<subCount;subIndex++)
						{
							const I32 pointIndex=spOutputVertices->integer(
									primitiveIndex,subIndex);
							indices.insert(pointIndex);
						}
					}

					spPrimitiveGroup=NULL;

					discard(spOutputAccessible,e_primitiveGroup,rName);

					sp<SurfaceAccessorI> spPointGroup;
					if(!access(spPointGroup,spOutputAccessible,
							e_pointGroup,rName,e_quiet,e_createMissing))
					{
						continue;
					}

					for(I32 index: indices)
					{
						spPointGroup->append(index);
					}

					promotionCount++;
				}
			}
		}
	}

	String summary;
	summary.sPrintf("%s %s %d promotion%s",
			fromClass.c_str(),toClass.c_str(),
			promotionCount,promotionCount==1? "": "s");
	catalog<String>("summary")=summary;

#if FE_GPO_DEBUG
	feLog("GroupPromoteOp::handle done\n");
#endif
}

} /* namespace ext */
} /* namespace fe */
