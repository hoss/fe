/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <operator/operator.pmh>

#define FE_STO_DEBUG		FALSE

using namespace fe;
using namespace fe::ext;

void StashOp::initialize(void)
{
	catalog<String>("icon")="FE_alpha";

	catalog<String>("stashKey")="stash";
	catalog<String>("stashKey","label")="Stash Key";
	catalog<String>("stashKey","suggest")="word";
	catalog<String>("stashKey","hint")=
			"Global name that the mesh is stored under.";

	catalog< sp<Component> >("Input Surface");
}

void StashOp::handle(Record& a_rSignal)
{
#if FE_STO_DEBUG
	feLog("StashOp::handle node \"%s\"\n",name().c_str());
#endif

	const String stashKey=catalog<String>("stashKey");

	String& rSummary=catalog<String>("summary");
	rSummary=stashKey;

	sp<SurfaceAccessibleI> spInputAccessible;
	if(!access(spInputAccessible,"Input Surface")) return;

	m_spStashAccessible=registry()->create("*.SurfaceAccessibleCatalog");
	m_spStashAccessible->copy(spInputAccessible);

	sp<Catalog> spMasterCatalog=
			registry()->master()->catalog();
	hp<Component>& rhpStashComponent=
			spMasterCatalog->catalog< hp<Component> >("Stash:"+stashKey);
	rhpStashComponent=m_spStashAccessible;

#if FE_STO_DEBUG
	feLog("StashOp::handle node \"%s\" done\n",name().c_str());
#endif
}
