/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __operator_LengthCorrectOp_h__
#define __operator_LengthCorrectOp_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Handler to resize curve segments to match an original

	@ingroup operator
*//***************************************************************************/
class FE_DL_EXPORT LengthCorrectOp:
	public OperatorThreaded,
	public Initialize<LengthCorrectOp>
{
	public:

					LengthCorrectOp(void):
						m_frame(-1.0)										{}
virtual				~LengthCorrectOp(void)									{}

		void		initialize(void);

					//* As HandlerI
virtual	void		handle(Record& a_rSignal);

					using OperatorThreaded::run;

virtual	void		run(I32 a_id,sp<SpannedRange> a_spRange);

	private:

	class Edge
	{
		public:
								Edge(void):
									m_pointIndexLow0(-1),
									m_pointIndexLow1(-1),
									m_pointIndexHigh0(-1),
									m_pointIndexHigh1(-1)					{}

			I32					m_pointIndexLow0;
			I32					m_pointIndexLow1;
			I32					m_pointIndexHigh0;
			I32					m_pointIndexHigh1;
	};

		BWORD		relength(I32 a_id,sp<SpannedRange> a_spRange,
					BWORD a_applyRestoration);
		BWORD		suspendAndCollide(I32 a_id,sp<SpannedRange> a_spRange);
		BWORD		reroute(I32 a_id,sp<SpannedRange> a_spRange);
		BWORD		cacheDuN(sp<SurfaceAccessibleI>& a_rspAccessible,
						Array<SpatialVector>& a_rNormalCache,
						Array<SpatialVector>& a_rDuCache,
						sp<SpannedRange> a_spRange);

		BWORD		collidePoint(SpatialVector a_point,
						SpatialVector a_target,SpatialVector a_dir,
						Real a_scalar,SpatialVector& a_rPush);

		sp<SurfaceAccessibleI>	m_spInputAccessible;
		sp<SurfaceAccessibleI>	m_spReferenceAccessible;
		sp<SurfaceAccessibleI>	m_spOutputAccessible;
		sp<SurfaceAccessibleI>	m_spColliderAccessible;
		sp<SurfaceI>			m_spDriver;
		sp<SurfaceI>			m_spCollider;

		sp<DrawI>				m_spDrawDebug;

		Real					m_frame;
		Array<SpatialVector>	m_pointCache;
		Array<SpatialVector>	m_velocityCache;

		Array<SpatialVector>	m_normalCacheRef;
		Array<SpatialVector>	m_normalCache;
		Array<SpatialVector>	m_duCacheRef;
		Array<SpatialVector>	m_duCache;
		Array<SpatialVector>	m_tweakCache;

		Array<I32>				m_fragmentOfPoint;
		Array<SpatialVector>	m_orientationOfFragment;

		//* opposing points across and edge
		std::map<U64,Edge>		m_edgeMap;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __operator_LengthCorrectOp_h__ */
