/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __operator_MatchSizeOp_h__
#define __operator_MatchSizeOp_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Transform the first input to the location and size of the second

	@ingroup operator
*//***************************************************************************/
class FE_DL_EXPORT MatchSizeOp:
	public OperatorSurfaceCommon,
	public Initialize<MatchSizeOp>
{
	public:

					MatchSizeOp(void)										{}
virtual				~MatchSizeOp(void)										{}

		void		initialize(void);

					//* As HandlerI
virtual	void		handle(Record& a_rSignal);
};

} /* namespace ext */
} /* namespace fe */

#endif /* __operator_MatchSizeOp_h__ */
