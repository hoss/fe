/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <operator/operator.pmh>

#define FE_QTO_DEBUG		FALSE

using namespace fe;
using namespace fe::ext;

void QuiltOp::initialize(void)
{
	catalog<String>("icon")="FE_alpha";

	catalog<bool>("Tacking")=false;
	catalog<bool>("Tacking","joined")=true;
	catalog<String>("Tacking","hint")=
			"Keep certain points as shared.";

	catalog<String>("TackingGroup");
	catalog<String>("TackingGroup","label")="Group";
	catalog<String>("TackingGroup","suggest")="pointGroups";
	catalog<String>("TackingGroup","enabler")="Tacking";
	catalog<bool>("TackingGroup","joined")=true;
	catalog<String>("Tacking","hint")=
			"All points in group will be tacked.";

	catalog<bool>("TackingFusion")=false;
	catalog<String>("TackingFusion","label")="Fusion";
	catalog<String>("TackingFusion","enabler")="Tacking";
	catalog<String>("Tacking","hint")=
			"All points in group will be tacked.";

	catalog<String>("Boundary")="Bind";
	catalog<String>("Boundary","label")="Boundary";
	catalog<String>("Boundary","choice:0")="Break";
	catalog<String>("Boundary","choice:1")="Bind";
	catalog<String>("Boundary","choice:2")="Expand";
	catalog<String>("Boundary","hint")=
			"How to treat surface boundaries.";

	catalog<String>("BoundaryGroup")="boundary";
	catalog<String>("BoundaryGroup","label")="Boundary Group";
	catalog<String>("Boundary","hint")=
			"Create new group with all boundary points.";

	catalog< sp<Component> >("Input Surface");
}

void QuiltOp::handle(Record& a_rSignal)
{
#if FE_QTO_DEBUG
	feLog("QuiltOp::handle\n");
#endif

	String& rSummary=catalog<String>("summary");
	rSummary="";

	const String tackingGroup=catalog<String>("TackingGroup");
	const BWORD tacking=(catalog<bool>("Tacking") && !tackingGroup.empty());
	const BWORD tackingFusion=(tacking && catalog<bool>("TackingFusion"));
	const String boundary=catalog<String>("Boundary");
	const String boundaryGroup=catalog<String>("BoundaryGroup");

	sp<SurfaceAccessibleI> spOutputAccessible;
	if(!accessOutput(spOutputAccessible,a_rSignal)) return;

	sp<SurfaceAccessorI> spOutputPoint;
	if(!access(spOutputPoint,spOutputAccessible,e_point,e_position)) return;

	sp<SurfaceAccessorI> spOutputNormal;
	if(!access(spOutputNormal,spOutputAccessible,e_point,e_normal)) return;

	sp<SurfaceAccessorI> spOutputVertices;
	if(!access(spOutputVertices,spOutputAccessible,
			e_primitive,e_vertices)) return;

	sp<SurfaceAccessibleI> spInputAccessible;
	if(!access(spInputAccessible,"Input Surface")) return;

	sp<SurfaceAccessorI> spInputNormal;
	if(!access(spInputNormal,spInputAccessible,e_point,e_normal)) return;

	const U32 pointCount=spOutputPoint->count();

	Array<I32> pointTacked(pointCount,FALSE);

	sp<SurfaceAccessorI> spOutputTacking;

	if(tacking)
	{
		if(!access(spOutputTacking,spOutputAccessible,
				e_pointGroup,tackingGroup)) return;

		const U32 tackingCount=spOutputTacking->count();
		for(U32 tackingIndex=0;tackingIndex<tackingCount;tackingIndex++)
		{
			const I32 pointIndex=spOutputTacking->integer(tackingIndex);
			pointTacked[pointIndex]=TRUE;
		}
	}

	sp<SurfaceAccessorI> spOutputBoundGroup;
	if(!boundaryGroup.empty() &&
			!access(spOutputBoundGroup,spOutputAccessible,
			e_pointGroup,boundaryGroup)) return;

	Array< std::set<I32> > pointFaces;
	Array< std::set<I32> > pointNeighbors;
	determineNeighborhood(spOutputAccessible,pointFaces,pointNeighbors);

	Array<I32> pointPair(pointCount,-1);
	Array<I32> pointMid(pointCount,-1);

	const BWORD tied=(boundary=="Bind");
	const BWORD expand=(boundary=="Expand");

	Array<I32> pointBoundary(pointCount);

	for(U32 pointIndex=0;pointIndex<pointCount;pointIndex++)
	{
		if(interrupted())
		{
			break;
		}

		const U32 faceValence=pointFaces[pointIndex].size();
		const U32 edgeValence=pointNeighbors[pointIndex].size();
		const BWORD boundary=(faceValence==edgeValence-1);

		pointBoundary[pointIndex]=boundary;

		if(tackingFusion && pointTacked[pointIndex])
		{
			//* tacked point
		}
		else if(tied && boundary)
		{
			//* tied boundary point
		}
		else if(boundary || faceValence==edgeValence)
		{
			//* encased point, not tacked

			const SpatialVector point=
					spOutputPoint->spatialVector(pointIndex);

			pointPair[pointIndex]=spOutputPoint->append();
			spOutputPoint->set(pointPair[pointIndex],point);

			if(expand && boundary)
			{
				pointMid[pointIndex]=spOutputPoint->append();
				spOutputPoint->set(pointMid[pointIndex],point);
			}
		}
		else
		{
			feLog("QuiltOp::handle unexpected valences for point %d"
					" (face %d edge %d)\n",
					pointIndex,faceValence,edgeValence);
		}
	}

	if(spOutputTacking.isValid())
	{
		for(U32 pointIndex=0;pointIndex<pointCount;pointIndex++)
		{
			if(pointTacked[pointIndex])
			{
				if(interrupted())
				{
					break;
				}

				const I32 pairIndex=pointPair[pointIndex];
				if(pairIndex>=0)
				{
					spOutputTacking->append(pairIndex);
				}
			}
		}
	}

	if(spOutputBoundGroup.isValid())
	{
		for(U32 pointIndex=0;pointIndex<pointCount;pointIndex++)
		{
			if(pointBoundary[pointIndex])
			{
				if(interrupted())
				{
					break;
				}

				spOutputBoundGroup->append(pointIndex);

				const I32 pairIndex=pointPair[pointIndex];
				if(pairIndex>=0)
				{
					spOutputBoundGroup->append(pairIndex);
				}

				const I32 midIndex=pointMid[pointIndex];
				if(midIndex>=0)
				{
					spOutputBoundGroup->append(midIndex);
				}
			}
		}
	}

	const U32 primitiveCount=spOutputVertices->count();
	U32 primVertCount=primitiveCount;
	Array< Array<I32> > primVerts(primVertCount);

	for(U32 primitiveIndex=0;primitiveIndex<primitiveCount;primitiveIndex++)
	{
		if(interrupted())
		{
			break;
		}

		const U32 subCount=spOutputVertices->subCount(primitiveIndex);
		if(!subCount)
		{
			continue;
		}

		Array<I32>& rVerts=primVerts[primitiveIndex];
		rVerts.resize(subCount);

		for(U32 subIndex=0;subIndex<subCount;subIndex++)
		{
			const I32 reverseIndex=subCount-1-subIndex;
					spOutputVertices->integer(primitiveIndex,subIndex);
			const I32 pointIndex=
					spOutputVertices->integer(primitiveIndex,subIndex);
			const I32 pairIndex=pointPair[pointIndex];
			const I32 midIndex=pointMid[pointIndex];

			const SpatialVector point=
					spOutputPoint->spatialVector(pointIndex);

			SpatialVector combinedNormal(0.0,0.0,0.0);

			if(pairIndex<0 || midIndex>=0)
			{
				const std::set<I32>& rNeighbors=pointNeighbors[pointIndex];
				for(std::set<I32>::iterator it=rNeighbors.begin();
						it!=rNeighbors.end();it++)
				{
					const I32 otherIndex=*it;
					const SpatialVector otherPoint=
							spOutputPoint->spatialVector(otherIndex);

					combinedNormal+=unitSafe(point-otherPoint);
				}
				normalizeSafe(combinedNormal);

				if(pairIndex<0)
				{
					rVerts[reverseIndex]=pointIndex;

					spOutputNormal->set(pointIndex,combinedNormal);
				}

				if(midIndex>=0)
				{
					spOutputNormal->set(midIndex,combinedNormal);
				}
			}

			if(pairIndex>=0)
			{
				rVerts[reverseIndex]=pairIndex;

				const SpatialVector normal=
						spInputNormal->spatialVector(pointIndex);

				if(midIndex>=0)
				{
					spOutputNormal->set(pointIndex,
							unitSafe(combinedNormal+normal));
					spOutputNormal->set(pairIndex,
							unitSafe(combinedNormal-normal));
				}
				else
				{
					spOutputNormal->set(pairIndex,-normal);
				}
			}
		}
	}

	spOutputVertices->append(primVerts);

	if(expand)
	{
		primVerts.clear();
		primVertCount=0;

		for(U32 pointIndex=0;pointIndex<pointCount;pointIndex++)
		{
			if(interrupted())
			{
				break;
			}

			if(!pointBoundary[pointIndex])
			{
				continue;
			}

			const std::set<I32>& rNeighbors=pointNeighbors[pointIndex];
			for(std::set<I32>::iterator it=rNeighbors.begin();
					it!=rNeighbors.end();it++)
			{
				const I32 otherIndex=*it;
				if(otherIndex<I32(pointIndex) || !pointBoundary[otherIndex])
				{
					continue;
				}

				primVertCount+=2;
				primVerts.resize(primVertCount);

				Array<I32>& rVertsNear=primVerts[primVertCount-2];
				Array<I32>& rVertsFar=primVerts[primVertCount-1];
				rVertsNear.resize(4);
				rVertsFar.resize(4);

				rVertsNear[0]=pointIndex;
				rVertsNear[1]=otherIndex;
				rVertsNear[2]=pointMid[otherIndex];
				rVertsNear[3]=pointMid[pointIndex];

				rVertsFar[0]=pointPair[pointIndex];
				rVertsFar[1]=pointPair[otherIndex];
				rVertsFar[2]=pointMid[otherIndex];
				rVertsFar[3]=pointMid[pointIndex];
			}
		}

		spOutputVertices->append(primVerts);
	}

	if(tacking)
	{
		rSummary+=String(rSummary.empty()? "": " ")+tackingGroup;
	}
	if(!boundaryGroup.empty())
	{
		rSummary+=String(rSummary.empty()? "": " + ")+boundaryGroup;
	}

#if FE_QTO_DEBUG
	feLog("QuiltOp::handle done\n");
#endif
}
