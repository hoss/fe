/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <operator/operator.pmh>

namespace fe
{
namespace ext
{

void SurfaceCopyOp::initialize(void)
{
	catalog<String>("icon")="FE_alpha";

	catalog<bool>("rotate")=true;
	catalog<String>("rotate","label")="Rotate";
	catalog<bool>("rotate","joined")=true;

	catalog<bool>("scale")=true;
	catalog<String>("scale","label")="Scale";

	catalog<SpatialVector>("defaultTangent")=SpatialVector(1.0,0.0,0.0);
	catalog<String>("defaultTangent","label")="Default Tangent";
	catalog<String>("defaultTangent","enabler")="rotate";

	catalog<SpatialVector>("defaultNormal")=SpatialVector(0.0,1.0,0.0);
	catalog<String>("defaultNormal","label")="Default Normal";
	catalog<String>("defaultNormal","enabler")="rotate";

	catalog<SpatialVector>("defaultScale")=SpatialVector(1.0,1.0,1.0);
	catalog<String>("defaultScale","label")="Default Scale";
	catalog<String>("defaultScale","enabler")="scale";

	catalog<String>("tangentAttr")="tangent";
	catalog<String>("tangentAttr","label")="Tangent Attribute";
	catalog<String>("tangentAttr","enabler")="rotate";
	catalog<String>("tangentAttr","hint")="Local X vector.";

	catalog<String>("normalAttr")="N";
	catalog<String>("normalAttr","label")="Normal Attribute";
	catalog<String>("normalAttr","enabler")="rotate";
	catalog<String>("normalAttr","hint")="Local Y vector.";

	catalog<String>("scaleAttr")="scale";
	catalog<String>("scaleAttr","label")="Scale Attribute";
	catalog<String>("scaleAttr","enabler")="scale";
	catalog<String>("scaleAttr","hint")="Alters size.";

	catalog<String>("copyAttr")="";
	catalog<String>("copyAttr","label")="Copy Attributes";
	catalog<bool>("copyAttr","joined")=true;
	catalog<String>("copyAttr","hint")=
			"Copy these Template point attributes to output primitives."
			"  Each space-delimited substring is an individual pattern."
			"  An attribute qualifies if it matches"
			" any of the substring patterns."
			"  Wildcard matching uses globbing unless Regex is turned on.";

	catalog<bool>("copyRegex")=false;
	catalog<String>("copyRegex","label")="Regex";
	catalog<String>("copyRegex","hint")=
			"Pass the pattern strings directly as true regular expressions."
			"  If this option is off,"
			" presumed glob patterns are converted to regex."
			"  Every '.' is first escaped as '\\.'."
			"  Then, every '?', indicating 'any character',"
			" is converted to '.'."
			"  Every '*', indicating 'any number of any characters',"
			" is converted to '.*'."
			"  Every '[!', indicating 'not' inside of square brackets,"
			" is converted to '[^'.";

	catalog< sp<Component> >("Input Surface");

	catalog< sp<Component> >("Template Points");

	//* don't copy input to output
	catalog< sp<Component> >("Output Surface");
	catalog<String>("Output Surface","IO")="output";
}

void SurfaceCopyOp::handle(Record& a_rSignal)
{
	const BWORD doRotate=catalog<bool>("rotate");
	const BWORD doScale=catalog<bool>("scale");
	const SpatialVector defaultTangent=catalog<SpatialVector>("defaultTangent");
	const SpatialVector defaultNormal=catalog<SpatialVector>("defaultNormal");
	const SpatialVector defaultScale=catalog<SpatialVector>("defaultScale");
	const String tangentAttr=catalog<String>("tangentAttr");
	const String normalAttr=catalog<String>("normalAttr");
	const String scaleAttr=catalog<String>("scaleAttr");

	sp<SurfaceAccessibleI> spOutputAccessible;
	if(!accessOutput(spOutputAccessible,a_rSignal)) return;

	sp<SurfaceAccessibleI> spInputAccessible;
	if(!access(spInputAccessible,"Input Surface")) return;

	sp<SurfaceAccessorI> spInputVertices;
	if(!access(spInputVertices,spInputAccessible,
			e_primitive,e_vertices)) return;
	const I32 inputPrimitiveCount=spInputVertices->count();

	sp<SurfaceAccessibleI> spTemplateAccessible;
	if(!access(spTemplateAccessible,"Template Points")) return;

	sp<SurfaceAccessorI> spTemplatePoint;
	if(!access(spTemplatePoint,spTemplateAccessible,e_point,e_position)) return;

	sp<SurfaceAccessorI> spTemplateTangent;
	sp<SurfaceAccessorI> spTemplateNormal;

	if(doRotate)
	{
		access(spTemplateTangent,"Template Points",
				e_point,tangentAttr,e_warning);
		access(spTemplateNormal,"Template Points",
				e_point,normalAttr,e_warning);
	}

	BWORD vectorScaling=FALSE;
	sp<SurfaceAccessorI> spTemplateScale;
	if(doScale)
	{
		access(spTemplateScale,"Template Points",
				e_point,scaleAttr,e_warning);
		if(spTemplateScale.isValid())
		{
			const String attrType=spTemplateScale->type();

			if(attrType=="vector3")
			{
				vectorScaling=TRUE;
			}
			else if(attrType!="real")
			{
				spTemplateScale=NULL;
			}
		}
	}

	const I32 templatePointCount=spTemplatePoint->count();

	Array<SpatialTransform> transformArray;
	transformArray.resize(templatePointCount);

	SpatialVector norm(0.0,1.0,0.0);
	SpatialVector tangent(1.0,0.0,0.0);
	SpatialVector scaling=defaultScale;

	if(doRotate || doScale)
	{
		for(I32 pointIndex=0;pointIndex<templatePointCount;pointIndex++)
		{
			const SpatialVector point=
					spTemplatePoint->spatialVector(pointIndex);

			if(spTemplateTangent.isValid())
			{
				tangent=spTemplateTangent->spatialVector(pointIndex);
				if(isZero(tangent))
				{
					tangent=defaultTangent;
				}
			}
			if(spTemplateNormal.isValid())
			{
				norm=spTemplateNormal->spatialVector(pointIndex);
				if(isZero(norm))
				{
					norm=defaultNormal;
				}
			}
			if(spTemplateScale.isValid())
			{
				if(vectorScaling)
				{
					scaling=spTemplateScale->spatialVector(pointIndex);
				}
				else
				{
					const Real value=spTemplateScale->real(pointIndex);
					set(scaling,value,value,value);
				}
			}

			SpatialTransform& rTransform=transformArray[pointIndex];

			makeFrameNormalY(rTransform,point,tangent,norm);

			if(doScale)
			{
				SpatialTransform scaleTransform;
				setIdentity(scaleTransform);
				scale(scaleTransform,scaling);

				rTransform=scaleTransform*rTransform;
			}
		}
	}
	else
	{
		for(I32 pointIndex=0;pointIndex<templatePointCount;pointIndex++)
		{
			const SpatialVector point=
					spTemplatePoint->spatialVector(pointIndex);

			SpatialTransform& rTransform=transformArray[pointIndex];

			setIdentity(rTransform);
			setTranslation(rTransform,point);
		}
	}

	spOutputAccessible->instance(spInputAccessible,transformArray);

	String copyAttr=catalog<String>("copyAttr");
	if(!copyAttr.empty())
	{
		if(!catalog<bool>("copyRegex"))
		{
			copyAttr=copyAttr.convertGlobToRegex();
		}

		copyAttributes(copyAttr,spTemplateAccessible,spOutputAccessible,
				0,templatePointCount,0,inputPrimitiveCount);
	}

	String& rSummary=catalog<String>("summary");
	rSummary.sPrintf("%d",templatePointCount);
	if(doRotate)
	{
		rSummary+=" rotate";
	}
	if(doScale)
	{
		rSummary+=" scale";
	}
}

//* static
void SurfaceCopyOp::copyAttributes(String a_attrRegex,
	sp<SurfaceAccessibleI> a_spTemplateAccessible,
	sp<SurfaceAccessibleI> a_spOutputAccessible,
	I32 a_templateStart,I32 a_templateCount,I32 a_outputStart,I32 a_grain)
{
	Array<String> copyPatternArray;
	String buffer=a_attrRegex;
	String pattern;
	while(!buffer.empty())
	{
		copyPatternArray.push_back(buffer.parse());
	}

	Array<SurfaceAccessibleI::Spec> specs;
	a_spTemplateAccessible->attributeSpecs(specs,SurfaceAccessibleI::e_point);
	const U32 specCount=specs.size();

	for(U32 specIndex=0;specIndex<specCount;specIndex++)
	{
		const SurfaceAccessibleI::Spec& spec=specs[specIndex];
		const String& specName=spec.name();
		const String& specType=spec.typeName();

		if(specName=="P")
		{
			continue;
		}

		BWORD match=FALSE;
		const U32 copyPatternCount=copyPatternArray.size();
		for(U32 patternIndex=0;patternIndex<copyPatternCount;patternIndex++)
		{
			const String pattern=copyPatternArray[patternIndex];

			if(specName.match(pattern))
			{
				match=TRUE;
				break;
			}
		}

		if(!match)
		{
			continue;
		}

		sp<SurfaceAccessorI> spTemplateAccessor=
				a_spTemplateAccessible->accessor(
				SurfaceAccessibleI::e_point,specName);

		sp<SurfaceAccessorI> spOutputAccessor=
				a_spOutputAccessible->accessor(
				SurfaceAccessibleI::e_primitive,specName,
				SurfaceAccessibleI::e_createMissing);

		if(spTemplateAccessor.isNull() || spOutputAccessor.isNull())
		{
			feLog("SurfaceCopyOp::handle %s %s accessor failure\n",
					specType.c_str(),specName.c_str());
			continue;
		}

		spOutputAccessor->setWritable(TRUE);

		if(specType=="vector3")
		{
			for(I32 fromIndex=0;fromIndex<a_templateCount;fromIndex++)
			{
				const SpatialVector vector3=spTemplateAccessor->spatialVector(
						a_templateStart+fromIndex);

				const I32 startIndex=a_outputStart+fromIndex*a_grain;
				const I32 uptoIndex=startIndex+a_grain;
				for(I32 toIndex=startIndex;toIndex<uptoIndex;toIndex++)
				{
					spOutputAccessor->set(toIndex,vector3);
				}
			}
		}
		else if(specType=="real")
		{
			for(I32 fromIndex=0;fromIndex<a_templateCount;fromIndex++)
			{
				const Real real=spTemplateAccessor->real(
						a_templateStart+fromIndex);

				const I32 startIndex=a_outputStart+fromIndex*a_grain;
				const I32 uptoIndex=startIndex+a_grain;
				for(I32 toIndex=startIndex;toIndex<uptoIndex;toIndex++)
				{
					spOutputAccessor->set(toIndex,real);
				}
			}
		}
		else if(specType=="integer")
		{
			for(I32 fromIndex=0;fromIndex<a_templateCount;fromIndex++)
			{
				const I32 integer=spTemplateAccessor->integer(
						a_templateStart+fromIndex);

				const I32 startIndex=a_outputStart+fromIndex*a_grain;
				const I32 uptoIndex=startIndex+a_grain;

				for(I32 toIndex=startIndex;toIndex<uptoIndex;toIndex++)
				{
					spOutputAccessor->set(toIndex,integer);
				}
			}
		}
		else if(specType=="string")
		{
			for(I32 fromIndex=0;fromIndex<a_templateCount;fromIndex++)
			{
				const String string=spTemplateAccessor->string(
						a_templateStart+fromIndex);

				const I32 startIndex=a_outputStart+fromIndex*a_grain;
				const I32 uptoIndex=startIndex+a_grain;
				for(I32 toIndex=startIndex;toIndex<uptoIndex;toIndex++)
				{
					spOutputAccessor->set(toIndex,string);
				}
			}
		}

		//* don't need to check for another match
//		break;
	}
}

} /* namespace ext */
} /* namespace fe */
