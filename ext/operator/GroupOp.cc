/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <operator/operator.pmh>

#define FE_GRO_DEBUG		FALSE

namespace fe
{
namespace ext
{

//* TODO brush manipulation

//* TODO highlight elements

void GroupOp::initialize(void)
{
	catalog<String>("Threads","page")="Config";

	catalog<String>("AutoThread","page")="Config";

	catalog<String>("Paging","page")="Config";

	catalog<String>("StayAlive","page")="Config";

	catalog<String>("Work","page")="Config";

	catalog<String>("Combine")="";
	catalog<String>("Combine","label")="Input Groups";
	catalog<String>("Combine","page")="Config";
	catalog<bool>("Combine","joined")=true;
	catalog<String>("Combine","hint")=
			"Input group names from which to create a group."
			"  If no names are specified, all elements are considered."
			"  Each space-delimited substring is an individual pattern."
			"  A group qualifies if it matches any of the substring patterns."
			"  Wildcard matching uses globbing unless Regex is turned on.";

	catalog<bool>("Regex")=false;
	catalog<String>("Regex","page")="Config";
	catalog<String>("Regex","hint")=
			"Pass the pattern strings directly as true regular expressions."
			"  If this option is off,"
			" presumed glob patterns are converted to regex."
			"  Every '.' is first escaped as '\\.'."
			"  Then, every '?', indicating 'any character',"
			" is converted to '.'."
			"  Every '*', indicating 'any number of any characters',"
			" is converted to '.*'."
			"  Every '[!', indicating 'not' inside of square brackets,"
			" is converted to '[^'.";

	catalog<String>("Group")="group";
	catalog<String>("Group","label")="Create Group";
	catalog<String>("Group","page")="Config";
	catalog<String>("Group","hint")=
			"The name of the default group to be created."
			"  If no 'From Attribute' is specified,"
			" this is the only group created.";

	catalog<String>("Attribute")="";
	catalog<String>("Attribute","label")="From Attribute";
	catalog<String>("Attribute","page")="Config";
	catalog<String>("Attribute","hint")=
			"Use the string in the named attribute as the group name"
			" for each element."
			"  If no attribute is specified,"
			" or the attribute does not exist,"
			" or the attribute is empty for an element,"
			" the 'Create Group' name is used.";

	catalog<String>("Element")="Point";
	catalog<String>("Element","page")="Config";
	catalog<String>("Element","choice:0")="Point";
	catalog<String>("Element","choice:1")="Primitive";

	catalog<bool>("EnableBounds")=false;
	catalog<String>("EnableBounds","label")="Enable";
	catalog<String>("EnableBounds","page")="Bounds";
	catalog<bool>("EnableBounds","joined")=true;

	catalog<bool>("Inverse")=false;
	catalog<String>("Inverse","enabler")="EnableBounds";
	catalog<String>("Inverse","page")="Bounds";
	catalog<bool>("Inverse","joined")=true;
	catalog<String>("Inverse","hint")=
		"Include everything that the bounds check would otherwise not include.";

	catalog<bool>("Partial")=false;
	catalog<String>("Partial","enabler")="EnableBounds";
	catalog<String>("Partial","page")="Bounds";
	catalog<String>("Partial","hint")=
		"Include a primitive when any of its points are in bounds."
		"  Otherwise, all points in a primitive need to be in bounds."
		"  This option is ignored when grouping points.";

	catalog<String>("Shape")="Box";
	catalog<String>("Shape","enabler")="EnableBounds";
	catalog<String>("Shape","page")="Bounds";
	catalog<String>("Shape","choice:0")="Box";
	catalog<String>("Shape","choice:1")="Sphere";
	catalog<String>("Shape","choice:2")="Surface";

	catalog<SpatialVector>("Center");
	catalog<String>("Center","enabler")="EnableBounds";
	catalog<String>("Center","page")="Bounds";
	catalog<String>("Center","hint")=
			"This is the center of the bounding box or sphere."
			"  For an arbitrary bounding surface,"
			" this will apply a translational"
			" displacement if Displace Surface is turned on.";

	catalog<SpatialVector>("Size")=SpatialVector(1.0,1.0,1.0);
	catalog<String>("Size","enabler")="EnableBounds";
	catalog<String>("Size","page")="Bounds";
	catalog<String>("Size","hint")=
			"This is the dimensions of the bounding box,"
			" the scaling of a unit sphere."
			"  For an arbitrary bounding surface, this will apply scaling"
			" if Scale Surface is turned on.";

	catalog<bool>("DisplaceSurface")=false;
	catalog<String>("DisplaceSurface","label")="Displace Surface";
	catalog<String>("DisplaceSurface","enabler")="Displace";
	catalog<String>("DisplaceSurface","enabler")="EnableBounds";
	catalog<String>("DisplaceSurface","page")="Bounds";
	catalog<bool>("DisplaceSurface","joined")=true;
	catalog<String>("DisplaceSurface","hint")=
			"Use Center as a translation when the Shape is"
			" an arbitrary bounding surface.";

	catalog<bool>("ScaleSurface")=false;
	catalog<String>("ScaleSurface","label")="Scale Surface";
	catalog<String>("ScaleSurface","enabler")="Displace";
	catalog<String>("ScaleSurface","enabler")="EnableBounds";
	catalog<String>("ScaleSurface","page")="Bounds";
	catalog<String>("ScaleSurface","hint")=
			"Use Size as scaling factor when the Shape is"
			" an arbitrary bounding surface.";

	catalog< sp<Component> >("Input Surface");

	catalog< sp<Component> >("Bounding Surface");
	catalog<bool>("Bounding Surface","optional")=true;
}

void GroupOp::handle(Record& a_rSignal)
{
#if FE_GRO_DEBUG
	feLog("GroupOp::handle\n");
#endif

	const SpatialVector center=catalog<SpatialVector>("Center");
	const SpatialVector size=catalog<SpatialVector>("Size");
	const BWORD inverse=catalog<bool>("Inverse");
	const BWORD displaceSurface=catalog<bool>("DisplaceSurface");
	const BWORD scaleSurface=catalog<bool>("ScaleSurface");

	m_combine=catalog<String>("Combine");
	if(!m_combine.empty() && !catalog<bool>("Regex"))
	{
		m_combine=m_combine.convertGlobToRegex();

#if FE_GRO_DEBUG
		feLog("GroupOp::handle converted \"%s\" to \"%s\"\n",
				catalog<String>("Combine").c_str(),m_combine.c_str());
#endif
	}

	m_combined=(!m_combine.empty());

	m_shape=catalog<String>("Shape");
	if(catalog<bool>("EnableBounds"))
	{
		if(m_shape!="Surface" && (size[0]<=0.0 || size[1]<=0.0 || size[2]<=0.0))
		{
			if(!inverse)
			{
				return;
			}

			//* inverse nothing = everything
			m_shape="All";
		}
	}
	else
	{
//~		if(inverse)
//~		{
//~			return;
//~		}

		//* boundless = everything
		m_shape="All";
	}

	m_doPrimitive=(catalog<String>("Element")=="Primitive");

	if(!accessOutput(m_spOutputAccessible,a_rSignal)) return;

	//* NOTE warn if attribute is missing
	const String attribute=catalog<String>("Attribute");
	if(!attribute.empty())
	{
		sp<SurfaceAccessorI> spElementAttribute;
		access(spElementAttribute,"Input Surface",
				m_doPrimitive? e_primitive: e_point,attribute,e_warning);
	}

	if(m_shape=="Surface")
	{
		if(!access(m_spBounds,"Bounding Surface")) return;

		m_spBounds->setRefinement(0);
		m_spBounds->prepareForSearch();

		//* NOTE warn if there are no normals
		//* TODO don't warn for VDB
		sp<SurfaceAccessorI> spBoundNormal;
		access(spBoundNormal,"Bounding Surface",e_point,e_normal,e_warning);
	}
	else
	{
		m_spBounds=NULL;
	}

	sp<SurfaceAccessorI> spElementAll;
	if(!access(spElementAll,m_spOutputAccessible,
			m_doPrimitive? e_primitive: e_point,
			m_doPrimitive? e_vertices: e_position)) return;

	const U32 fullCount=spElementAll->count();

	sp<SurfaceAccessorI> spElementPosition;
	if(m_combined)
	{
		if(!access(spElementPosition,m_spOutputAccessible,
				m_doPrimitive? e_primitiveGroup: e_pointGroup,
				m_combine)) return;
	}
	else
	{
		spElementPosition=spElementAll;
	}

	const Color black(0.0,0.0,0.0);
	const Color red(1.0,0.0,0.0);
	const Color green(0.0,1.0,0.0);
	const Color blue(0.0,0.0,1.0);
	const Color cyan(0.0,1.0,1.0);
	const Color magenta(1.0,0.0,1.0);
	const Color yellow(1.0,1.0,0.0);

	sp<DrawI> spDrawGuide;
	accessGuide(spDrawGuide,a_rSignal,e_warning);

	if(m_shape!="All")
	{
		if(spDrawGuide.isValid())
		{
			if(m_shape=="Surface")
			{
				sp<DrawableI> spDrawable(m_spBounds);
				if(spDrawable.isValid())
				{
					SpatialTransform xform;
					setIdentity(xform);
					if(scaleSurface)
					{
						scale(xform,size);
					}
					if(displaceSurface)
					{
						setTranslation(xform,center);
					}

					spDrawable->draw(xform,spDrawGuide,NULL);
				}
			}
			else
			{
				//* TODO generalize box and sphere draw into OperateCommon

				if(m_shape=="Box")
				{
					SpatialVector line[8];

					line[0]=center-0.5*size;
					line[1]=line[0]+SpatialVector(size[0],0.0,0.0);
					line[2]=line[1]+SpatialVector(0.0,0.0,size[2]);
					line[3]=line[0]+SpatialVector(0.0,0.0,size[2]);
					line[4]=line[0];

					spDrawGuide->drawLines(line,NULL,5,
							DrawI::e_strip,FALSE,&blue);

					line[0][1]+=size[1];
					line[1][1]+=size[1];
					line[2][1]+=size[1];
					line[3][1]+=size[1];
					line[4][1]+=size[1];

					spDrawGuide->drawLines(line,NULL,5,
							DrawI::e_strip,FALSE,&green);

					Color colors[8];
					colors[0]=blue;
					colors[1]=green;
					colors[2]=blue;
					colors[3]=green;
					colors[4]=blue;
					colors[5]=green;
					colors[6]=blue;
					colors[7]=green;

					line[0]=center-0.5*size;
					line[1]=line[0]+SpatialVector(0.0,size[1],0.0);

					line[2]=line[0]+SpatialVector(size[0],0.0,0.0);
					line[3]=line[1]+SpatialVector(size[0],0.0,0.0);

					line[4]=line[0]+SpatialVector(size[0],0.0,size[2]);
					line[5]=line[1]+SpatialVector(size[0],0.0,size[2]);

					line[6]=line[0]+SpatialVector(0.0,0.0,size[2]);
					line[7]=line[1]+SpatialVector(0.0,0.0,size[2]);

					spDrawGuide->drawLines(line,NULL,8,
							DrawI::e_discrete,TRUE,colors);

				}
				else if(m_shape=="Sphere")
				{
					//* triple circles per axis

					const Real aside=0.1;
					const Real downsize=cos(M_PI*aside);

					//* Z axis

					SpatialTransform xform;
					setIdentity(xform);
					setTranslation(xform,center);

					SpatialVector circleScale=0.5*size;

					spDrawGuide->drawCircle(xform,&circleScale,yellow);

					translate(xform,SpatialVector(0.0,0.0,aside*size[2]));
					circleScale*=downsize;

					spDrawGuide->drawCircle(xform,&circleScale,yellow);

					translate(xform,SpatialVector(0.0,0.0,-2.0*aside*size[2]));

					spDrawGuide->drawCircle(xform,&circleScale,yellow);

					//* Y axis

					setIdentity(xform);
					rotate(xform,-90.0*degToRad,e_xAxis);
					setTranslation(xform,center);

					circleScale=0.5*SpatialVector(size[0],size[2],size[1]);

					spDrawGuide->drawCircle(xform,&circleScale,magenta);

					translate(xform,SpatialVector(0.0,0.0,aside*size[1]));
					circleScale*=downsize;

					spDrawGuide->drawCircle(xform,&circleScale,magenta);

					translate(xform,SpatialVector(0.0,0.0,-2.0*aside*size[1]));

					spDrawGuide->drawCircle(xform,&circleScale,magenta);

					//* X axis

					setIdentity(xform);
					rotate(xform,-90.0*degToRad,e_yAxis);
					setTranslation(xform,center);

					circleScale=0.5*SpatialVector(size[2],size[1],size[0]);

					spDrawGuide->drawCircle(xform,&circleScale,cyan);

					translate(xform,SpatialVector(0.0,0.0,aside*size[0]));
					circleScale*=downsize;

					spDrawGuide->drawCircle(xform,&circleScale,cyan);

					translate(xform,SpatialVector(0.0,0.0,-2.0*aside*size[0]));

					spDrawGuide->drawCircle(xform,&circleScale,cyan);
				}
			}
		}
	}

	const U32 elementCount=spElementPosition->count();

	m_defaultGroup=catalog<String>("Group");

	catalog<String>("summary")=attribute.empty()? m_defaultGroup:
			"ATTRIBUTE " + attribute;

	std::map<String, sp<SurfaceAccessorI> > groupMap;

	m_groupAssignment.clear();
	m_groupAssignment.resize(fullCount);

	setAtomicRange(m_spOutputAccessible,
			m_doPrimitive? e_primitivesOnly: e_pointsOnly,m_combine);

	adjustThreads();
	const I32 threadCount=catalog<I32>("Threads");
	if(threadCount>1)
	{
		OperatorThreaded::handle(a_rSignal);
	}
	else
	{
		//* serial
		run(-1,fullRange());
	}

#if FE_GRO_DEBUG
	feLog("GroupOp::handle resolve\n");
#endif

	SpatialVector* pointArray=new SpatialVector[elementCount*2];

	I32 drawCount=0;

	for(U32 elementIndex=0;elementIndex<elementCount;elementIndex++)
	{
		const I32 reIndex=m_combined?
				spElementPosition->integer(elementIndex): elementIndex;

		const String groupName=m_groupAssignment[reIndex];
		if(groupName.empty())
		{
			continue;
		}

//		feLog("assign %d/%d re %d to \"%s\"\n",
//				elementIndex,elementCount,reIndex,groupName.c_str());

		sp<SurfaceAccessorI>& rspElementGroup=groupMap[groupName];
		if(rspElementGroup.isNull() &&
					!access(rspElementGroup,m_spOutputAccessible,
					m_doPrimitive? e_primitiveGroup: e_pointGroup,groupName))
		{
			continue;
		}

#if FE_GRO_DEBUG
		feLog("GroupOp::handle append %d\n",reIndex);
#endif

		rspElementGroup->append(reIndex);

		if(spDrawGuide.isValid())
		{
			SpatialVector point(0.0,0.0,0.0);

			if(m_doPrimitive)
			{
				SpatialVector sum(0.0,0.0,0.0);

				const U32 subCount=spElementPosition->subCount(elementIndex);
				for(U32 subIndex=0;subIndex<subCount;subIndex++)
				{
					sum+=spElementPosition->spatialVector(
							elementIndex,subIndex);
				}
				if(subCount)
				{
					sum*=1.0/Real(subCount);
				}
				point=sum;
			}
			else
			{
				point=spElementPosition->spatialVector(elementIndex);
			}

			pointArray[drawCount++]=1.02*point-0.02*center;
			pointArray[drawCount++]=0.98*point+0.02*center;
		}
	}

	if(drawCount)
	{
		FEASSERT(spDrawGuide.isValid());

		spDrawGuide->drawLines(pointArray,NULL,drawCount,
				DrawI::e_discrete,FALSE,&yellow);
	}

	m_groupAssignment.clear();
	m_spBounds=NULL;

	delete[] pointArray;

#if FE_GRO_DEBUG
	feLog("GroupOp::handle done\n");
#endif
}

void GroupOp::run(I32 a_id,sp<SpannedRange> a_spRange)
{
//	feLog("GroupOp::run %d range %s\n",a_id,a_spRange->dump().c_str());

	const String attribute=catalog<String>("Attribute");
	const BWORD inverse=catalog<bool>("Inverse");
	const BWORD partial=catalog<bool>("Partial");
	const SpatialVector center=catalog<SpatialVector>("Center");
	const SpatialVector size=catalog<SpatialVector>("Size");
	const BWORD displaceSurface=catalog<bool>("DisplaceSurface");
	const BWORD scaleSurface=catalog<bool>("ScaleSurface");

	//* NOTE already converted to true indexes (if grouped)
	sp<SurfaceAccessorI> spElementPosition;
	if(!access(spElementPosition,m_spOutputAccessible,
			m_doPrimitive? e_primitive: e_point,
			m_doPrimitive? e_vertices: e_position)) return;

	sp<SurfaceAccessorI> spElementAttribute;
	if(!attribute.empty())
	{
		access(spElementAttribute,"Input Surface",
				m_doPrimitive? e_primitive: e_point,attribute,e_quiet);
	}

	const SpatialBox box(center-0.5*size,size);

	const SpatialVector reCenter=
			displaceSurface? center: SpatialVector(0.0,0.0,0.0);
	const SpatialVector reSize=
			scaleSurface? size: SpatialVector(1.0,1.0,1.0);

	for(SpannedRange::Iterator it=a_spRange->begin();!it.atEnd();it.step())
	{
		if(interrupted())
		{
			break;
		}

		const U32 elementIndex=it.value();

		FEASSERT(elementIndex<m_groupAssignment.size());

		if(m_shape=="All")
		{
			//* boundless
			groupElement(spElementPosition,spElementAttribute,elementIndex);
		}
		else
		{
			const U32 subCount=m_doPrimitive?
					spElementPosition->subCount(elementIndex): 1;

			BWORD hit=partial? FALSE: TRUE;

			if(m_shape=="Box")
			{
				for(U32 subIndex=0;subIndex<subCount;subIndex++)
				{
					const SpatialVector point=spElementPosition->spatialVector(
							elementIndex,subIndex);

					if((intersecting(box,point)) != inverse)
					{
						if(partial)
						{
							hit=TRUE;
							break;
						}
					}
					else if(!partial)
					{
						hit=FALSE;
						break;
					}
				}
			}
			else if(m_shape=="Sphere")
			{
				for(U32 subIndex=0;subIndex<subCount;subIndex++)
				{
					const SpatialVector point=spElementPosition->spatialVector(
							elementIndex,subIndex);

					const SpatialVector transformed=2.0*SpatialVector(
							(point[0]-center[0])/size[0],
							(point[1]-center[1])/size[1],
							(point[2]-center[2])/size[2]);

					if((magnitudeSquared(transformed)<1.0) != inverse)
					{
						if(partial)
						{
							hit=TRUE;
							break;
						}
					}
					else if(!partial)
					{
						hit=FALSE;
						break;
					}
				}
			}
			else if(m_shape=="Surface")
			{
				for(U32 subIndex=0;subIndex<subCount;subIndex++)
				{
					const SpatialVector point=spElementPosition->spatialVector(
							elementIndex,subIndex);

					const SpatialVector transformed=SpatialVector(
							(point[0]-reCenter[0])/reSize[0],
							(point[1]-reCenter[1])/reSize[1],
							(point[2]-reCenter[2])/reSize[2]);

					const SurfaceI::Containment containment=
							m_spBounds->containment(transformed);

					if((containment==SurfaceI::e_inside) != inverse)
					{
						if(partial)
						{
							hit=TRUE;
							break;
						}
					}
					else if(!partial)
					{
						hit=FALSE;
						break;
					}
				}
			}

			if(hit)
			{
				groupElement(spElementPosition,spElementAttribute,
						elementIndex);
			}
		}
	}
}

void GroupOp::groupElement(sp<SurfaceAccessorI>& a_rspElementPosition,
	sp<SurfaceAccessorI>& a_rspElementAttribute,I32 a_index)
{
	String groupName;
	if(a_rspElementAttribute.isValid())
	{
		groupName=a_rspElementAttribute->string(a_index);
		if(groupName.empty())
		{
			groupName=m_defaultGroup;
		}
	}
	else
	{
		groupName=m_defaultGroup;
	}

	if(!groupName.empty())
	{
		FEASSERT(a_index>=0);
		FEASSERT(a_index<I32(m_groupAssignment.size()));
		FEASSERT(m_groupAssignment[a_index].empty());
		m_groupAssignment[a_index]=groupName;
	}
}

} /* namespace ext */
} /* namespace fe */
