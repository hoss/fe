/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __operator_SurfaceWalkOp_h__
#define __operator_SurfaceWalkOp_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
    @brief Create a curve that tries to follows a straight line across edges

	@ingroup operator
*//***************************************************************************/
class FE_DL_EXPORT SurfaceWalkOp:
	public OperatorSurfaceCommon,
	public Initialize<SurfaceWalkOp>
{
	public:
				SurfaceWalkOp(void)											{}
virtual			~SurfaceWalkOp(void)											{}

		void	initialize(void);

				//* As HandlerI
virtual void	handle(Record& a_rSignal);
};

} /* namespace ext */
} /* namespace fe */

#endif /* __operator_SurfaceWalkOp_h__ */
