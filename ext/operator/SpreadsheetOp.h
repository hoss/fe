/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __operator_SpreadsheetOp_h__
#define __operator_SpreadsheetOp_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Operator to show a table of attributes

	@ingroup operator
*//***************************************************************************/
class FE_DL_EXPORT SpreadsheetOp:
	public OperatorSurfaceCommon,
	public Initialize<SpreadsheetOp>
{
	public:

						SpreadsheetOp(void):
							m_brushed(FALSE),
							m_scroll(0.0)									{}

virtual					~SpreadsheetOp(void)								{}

		void			initialize(void);

						//* As HandlerI
virtual	void			handle(Record& a_rSignal);

	private:
		void			limitBounds(I32& a_rStart,I32& a_rEnd,
								I32 a_length,I32 a_count);
		String			dump(Element a_element,String a_attrName,
								U32 a_component,U32 a_start,U32 a_end);

		sp<SurfaceAccessibleI>		m_spInputAccessible;
		sp<DrawMode>				m_spSolid;

		WindowEvent					m_event;
		BWORD						m_brushed;
		I32							m_lastX;
		I32							m_lastY;
		Real						m_scroll;

		String						m_attributes;
		I32							m_elementCount;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __operator_SpreadsheetOp_h__ */
