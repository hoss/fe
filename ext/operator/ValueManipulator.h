/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __operator_ValueManipulator_h__
#define __operator_ValueManipulator_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Manipulator to adjust a list of values

	@ingroup operator
*//***************************************************************************/
class FE_DL_EXPORT ValueManipulator:
	public ManipulatorCommon,
	public Initialize<ValueManipulator>
{
	public:

					ValueManipulator(void)									{}
virtual				~ValueManipulator(void)									{}

		void		initialize(void);

					//* As HandlerI (window events)
virtual	void		handle(Record& a_rSignal);

	protected:
					//* As ManipulatorCommon
virtual	void		updateGrips(void);

	private:

};

} /* namespace ext */
} /* namespace fe */

#endif /* __operator_ValueManipulator_h__ */
