/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <operator/operator.pmh>

using namespace fe;
using namespace fe::ext;

int main(int argc,char** argv)
{
	UNIT_START();

	BWORD completed=FALSE;

	try
	{
		sp<Master> spMaster(new Master);
		sp<Registry> spRegistry=spMaster->registry();

		Result result=spRegistry->manage("fexOperatorDL");
		UNIT_TEST(successful(result));

		sp<Component> spComponent(
				spRegistry->create("*.OperatorSurfaceWave"));
		UNIT_TEST(spComponent.isValid());

		sp<OperatorSurfaceI> spOperatorSurfaceI=spComponent;
		UNIT_TEST(spOperatorSurfaceI.isValid());

		completed=TRUE;
	}
	catch(Exception &e)	{ e.log(); }
	catch(...)			{ feLog("uncaught exception\n"); }

	UNIT_TEST(completed);
	UNIT_TRACK(4);
	UNIT_RETURN();
}

