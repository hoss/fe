/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __operator_SurfaceNormalOp_h__
#define __operator_SurfaceNormalOp_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
    @brief Add or replace normals

	@ingroup operator
*//***************************************************************************/
class FE_DL_EXPORT SurfaceNormalOp:
	public OperatorSurfaceCommon,
	public Initialize<SurfaceNormalOp>
{
	public:
				SurfaceNormalOp(void)										{}
virtual			~SurfaceNormalOp(void)										{}

		void	initialize(void);

				//* As HandlerI
virtual void	handle(Record& a_rSignal);
};

} /* namespace ext */
} /* namespace fe */

#endif /* __operator_SurfaceNormalOp_h__ */
