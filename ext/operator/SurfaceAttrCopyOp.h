/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __operator_SurfaceAttrCopyOp_h__
#define __operator_SurfaceAttrCopyOp_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
    @brief Copy attributes from another Surface

	@ingroup operator
*//***************************************************************************/
class FE_DL_EXPORT SurfaceAttrCopyOp:
	public OperatorSurfaceCommon,
	public Initialize<SurfaceAttrCopyOp>
{
	public:
				SurfaceAttrCopyOp(void)										{}
virtual			~SurfaceAttrCopyOp(void)									{}

		void	initialize(void);

				//* As HandlerI
virtual void	handle(Record& a_rSignal);
};

} /* namespace ext */
} /* namespace fe */

#endif /* __operator_SurfaceAttrCopyOp_h__ */
