/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __operator_TransformManipulator_h__
#define __operator_TransformManipulator_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Manipulator to adjust a SpatialTransform

	@ingroup operator
*//***************************************************************************/
class FE_DL_EXPORT TransformManipulator:
	public ManipulatorCommon,
	public Initialize<TransformManipulator>
{
	public:

					TransformManipulator(void):
						m_mode(e_deform)									{}
virtual				~TransformManipulator(void)								{}

		void		initialize(void);

					//* As HandlerI (window events)
virtual	void		handle(Record& a_rSignal);

virtual	String		mode(void) const
					{	return m_mode==e_deform? "deform": "pivot"; }

	protected:
					//* As ManipulatorCommon
virtual	void		updateGrips(void);

	private:

		void		updateTransform(void);

		enum		Mode
					{
						e_deform,
						e_pivot
					};

		Mode				m_mode;
		sp<DrawMode>		m_spCircleAura;

		SpatialTransform	m_unscaled;
		SpatialVector		m_scaling;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __operator_TransformManipulator_h__ */
