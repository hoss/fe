/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __operator_OperatorGraduateOp_h__
#define __operator_OperatorGraduateOp_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
    @brief Draw panels of lines behind an object

	@ingroup operator
*//***************************************************************************/
class FE_DL_EXPORT RulerOp:
	public OperatorSurfaceCommon,
	public Initialize<RulerOp>
{
	public:
				RulerOp(void);
virtual			~RulerOp(void)												{}

		void	initialize(void);

				//* As HandlerI
virtual void	handle(Record& a_rSignal);

		void	drawOrthoGrid(sp<DrawI> a_spDraw3D,sp<DrawI> a_spDraw2D,
					sp<SurfaceAccessibleI> a_spSurfaceAccessibleI);

	private:

		void	scan(sp<DrawI> a_spDraw2D);
		void	drawOrtho(sp<DrawI> a_spDraw3D,sp<DrawI> a_spDraw2D);

		sp<SurfaceAccessibleI>	m_spInputAccessible;
		sp<SurfaceI>			m_spInput;

		sp<DrawMode>			m_spOverlay;
		sp<DrawMode>			m_spBrush;
		sp<DrawMode>			m_spHaze;
		sp<DrawMode>			m_spSplatter;

		WindowEvent				m_event;

		SpatialVector			m_minCorner;
		SpatialVector			m_maxCorner;
		SpatialVector			m_back;
		SpatialVector			m_start;
		SpatialVector			m_end;
		Real					m_step;

		BWORD					m_hasCumulative;
		SpatialVector			m_minCumulative;
		SpatialVector			m_maxCumulative;

		BWORD					m_pickedA;
		BWORD					m_pickedB;
		SpatialVector			m_pointA;
		SpatialVector			m_pointB;
		SpatialVector			m_normA;
		SpatialVector			m_normB;
		I32						m_triIndexA;
		I32						m_triIndexB;
		SpatialBary				m_barycenterA;
		SpatialBary				m_barycenterB;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __operator_OperatorGraduateOp_h__ */
