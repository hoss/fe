/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __operator_OperatorSurfaceCurve_h__
#define __operator_OperatorSurfaceCurve_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
    @brief Create a basic curve

	@ingroup operator
*//***************************************************************************/
class FE_DL_EXPORT CurveCreateOp:
	public OperatorSurfaceCommon,
	public Initialize<CurveCreateOp>
{
	public:
				CurveCreateOp(void)											{}
virtual			~CurveCreateOp(void)										{}

		void	initialize(void);

				//* As HandlerI
virtual void	handle(Record& a_rSignal);

	private:

static	void	halfTransform(SpatialTransform& a_result,
					const SpatialTransform& a_matrix1,
					const SpatialTransform& a_matrix2);
static	void	interpolateTransform(SpatialTransform& a_result,
					const SpatialTransform& a_matrix1,
					const SpatialTransform& a_matrix2,
					Real a_fraction,BWORD a_bezier);
};

} /* namespace ext */
} /* namespace fe */

#endif /* __operator_OperatorSurfaceCurve_h__ */
