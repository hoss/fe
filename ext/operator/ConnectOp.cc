/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <operator/operator.pmh>

#define FE_CON_DEBUG	FALSE

using namespace fe;
using namespace fe::ext;

//* copies driver UV sample onto output points
//* puts original point index in W of UVW

//* TODO optional UV wrap around

//* TODO optionally remove triangle over driver holes
//*	- uv distance
//*	- world distance
//*	- any edge midpoint not near driver

void ConnectOp::initialize(void)
{
	catalog<String>("icon")="FE_beta";

	catalog<bool>("PartitionDriver")=false;
	catalog<String>("PartitionDriver","label")="Partition Driver";
	catalog<bool>("PartitionDriver","joined")=true;
	catalog<String>("PartitionDriver","hint")=
			"Use multiple UV spaces.";

	catalog<String>("DriverPartitionAttr")="partition";
	catalog<String>("DriverPartitionAttr","label")="Driver Attr";
	catalog<String>("DriverPartitionAttr","enabler")="PartitionDriver";
	catalog<bool>("DriverPartitionAttr","joined")=true;
	catalog<String>("DriverPartitionAttr","hint")=
			"Primitive string attribute on driver to use for bind matching."
			"  Generally, these values are just simple names.";

	catalog< sp<Component> >("Input Surface");

	catalog< sp<Component> >("Driver Surface");
	catalog<bool>("Driver Surface","optional")=true;
}

void ConnectOp::handle(Record& a_rSignal)
{
	const bool partitionDriver=catalog<bool>("PartitionDriver");
	const String driverPartitionAttr=catalog<String>("DriverPartitionAttr");

	sp<SurfaceAccessorI> spOutputPoint;
	if(!accessOutput(spOutputPoint,a_rSignal,e_point,e_position)) return;

	sp<SurfaceAccessorI> spOutputPrimitive;
	if(!accessOutput(spOutputPrimitive,a_rSignal,
			e_primitive,e_vertices)) return;

	sp<SurfaceI> spDriver;
	sp<SurfaceAccessorI> spInputUV;
	sp<SurfaceAccessorI> spOutputUV;
	sp<SurfaceAccessorI> spInputNormal;
	sp<SurfaceAccessorI> spDriverPart;
	U32 partitionCount=1;

	if(access(spDriver,"Driver Surface",e_quiet))
	{
		if(partitionDriver && !driverPartitionAttr.empty() &&
				access(spDriverPart,"Driver Surface",
				e_primitive,driverPartitionAttr))
		{
			spDriver->partitionWith(driverPartitionAttr);
			spDriver->prepareForSearch();
			partitionCount=spDriver->partitionCount();
			spDriver->setPartitionFilter(".*");
		}
	}
	else
	{
		if(!access(spInputUV,"Input Surface",e_point,e_uv)) return;
		access(spInputNormal,"Input Surface",e_point,e_normal);
	}
	if(!accessOutput(spOutputUV,a_rSignal,e_point,e_uv)) return;

	const U32 outputCount=spOutputPoint->count();

	//* find driver partition name nearest to each input point
	Array<String> pointPartition;
	if(partitionCount>1)
	{
		pointPartition.resize(outputCount);

		for(U32 pointIndex=0;pointIndex<outputCount;pointIndex++)
		{
			const SpatialVector point=
					spOutputPoint->spatialVector(pointIndex);
			sp<SurfaceI::ImpactI> spImpact=spDriver->nearestPoint(point);
			if(spImpact.isValid())
			{
				const I32 primitive=spImpact->primitiveIndex();
				pointPartition[pointIndex]=spDriverPart->string(primitive);
			}
			else
			{
				pointPartition[pointIndex]="";
			}
		}
	}

#if FE_CON_DEBUG
	feLog("ConnectOp::handle partitionCount %d\n",partitionCount);
#endif

	for(U32 partitionIndex=0;partitionIndex<partitionCount;partitionIndex++)
	{
		String partitionName;
		if(partitionCount>1)
		{
			partitionName=spDriver->partitionName(partitionIndex);
			spDriver->setPartitionFilter(partitionName);
		}

		U32 dotCount=0;

		//* collect points
		Array<I32> indexArray(outputCount);
		Array<Vector2> locationArray(outputCount);
		Array<SpatialVector> normalArray(outputCount);
		if(spDriver.isValid())
		{
			for(U32 pointIndex=0;pointIndex<outputCount;pointIndex++)
			{
				if(partitionCount>1 &&
						pointPartition[pointIndex]!=partitionName)
				{
					continue;
				}
				indexArray[dotCount]=pointIndex;

				const SpatialVector point=
						spOutputPoint->spatialVector(pointIndex);
				sp<SurfaceI::ImpactI> spImpact=spDriver->nearestPoint(point);
				if(spImpact.isValid())
				{
					locationArray[dotCount]=spImpact->uv();
					normalArray[dotCount]=spImpact->normal();

					//* copy sampled UV and set W to pointIndex
					SpatialVector uvw=spImpact->uv();
					uvw[2]=Real(pointIndex);
					spOutputUV->set(pointIndex,uvw);
				}
				else
				{
					set(locationArray[dotCount],point[0],point[2]);
					normalArray[dotCount]=spInputNormal.isValid()?
							spInputNormal->spatialVector(pointIndex):
							unitSafe(point);

					//* set W to pointIndex
					SpatialVector uvw=spOutputUV->spatialVector(pointIndex);
					uvw[2]=Real(pointIndex);
					spOutputUV->set(pointIndex,uvw);
				}
				dotCount++;
			}
		}
		else
		{
			for(U32 pointIndex=0;pointIndex<outputCount;pointIndex++)
			{
				indexArray[dotCount]=pointIndex;
				locationArray[dotCount]=
						spInputUV->spatialVector(pointIndex);
				normalArray[dotCount]=spInputNormal.isValid()?
						spInputNormal->spatialVector(pointIndex):
						unitSafe(spOutputPoint->spatialVector(pointIndex));

				//* set W to pointIndex
				SpatialVector uvw=spOutputUV->spatialVector(pointIndex);
				uvw[2]=Real(pointIndex);
				spOutputUV->set(pointIndex,uvw);

				dotCount++;
			}
		}

#if FE_CON_DEBUG
		feLog("ConnectOp::handle partition %d/%d \"%s\" count %d/%d\n",
				partitionIndex,partitionCount,partitionName.c_str(),
				dotCount,outputCount);
#endif

		if(dotCount<3)
		{
			continue;
		}

		locationArray.resize(dotCount);

		//* Delaunay triangulation
		Array<Vector3i> triangleArray;
		PointConnect::solve(locationArray,triangleArray);

		//* append one primitive per triangle
		const U32 primitiveCount=triangleArray.size();
		for(U32 primitiveIndex=0;primitiveIndex<primitiveCount;primitiveIndex++)
		{
			const Vector3i& rTriangle=triangleArray[primitiveIndex];
			const I32 index0=rTriangle[0];
			const I32 index1=rTriangle[1];
			const I32 index2=rTriangle[2];
			const I32 p0=indexArray[index0];
			const I32 p1=indexArray[index1];
			const I32 p2=indexArray[index2];

			//* flip cycle if CCW normal opposes average original
			const SpatialVector point0=spOutputPoint->spatialVector(p0);
			const SpatialVector point1=spOutputPoint->spatialVector(p1);
			const SpatialVector point2=spOutputPoint->spatialVector(p2);
			const SpatialVector triNorm=
					unitSafe(cross(point1-point0,point2-point0));
			const SpatialVector aveNorm=unitSafe(normalArray[index0]+
					normalArray[index1]+normalArray[index2]);
			const BWORD flip=(dot(triNorm,aveNorm)<0.0);

			const I32 createdIndex=spOutputPrimitive->append();
			spOutputPrimitive->append(createdIndex,p0);
			spOutputPrimitive->append(createdIndex,flip? p1: p2);
			spOutputPrimitive->append(createdIndex,flip? p2: p1);
		}
	}
}
