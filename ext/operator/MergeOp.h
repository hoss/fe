/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __operator_MergeOp_h__
#define __operator_MergeOp_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Append surfaces to the first

	@ingroup operator
*//***************************************************************************/
class FE_DL_EXPORT MergeOp:
	public OperatorSurfaceCommon,
	public Initialize<MergeOp>
{
	public:

					MergeOp(void)											{}
virtual				~MergeOp(void)											{}

		void		initialize(void);

					//* As HandlerI
virtual	void		handle(Record& a_rSignal);
};

} /* namespace ext */
} /* namespace fe */

#endif /* __operator_MergeOp_h__ */
