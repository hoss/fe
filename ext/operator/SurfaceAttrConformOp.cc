/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <operator/operator.pmh>

#define FE_SACO_DEBUG		FALSE

using namespace fe;
using namespace fe::ext;

//* TODO other types than String

void SurfaceAttrConformOp::initialize(void)
{
	catalog<String>("icon")="FE_alpha";

	catalog<String>("conformAttr");
	catalog<String>("conformAttr","label")="Conform Attr";
	catalog<String>("conformAttr","hint")=
			"Name of string attribute to conform.";

	catalog<bool>("fragment")=false;
	catalog<String>("fragment","label")="Fragment";
	catalog<bool>("fragment","joined")=true;
	catalog<String>("fragment","hint")=
			"Associate collections of points which have"
			" matching a fragment attribute.";

	catalog<String>("fragmentAttr")="part";
	catalog<String>("fragmentAttr","label")="Fragment Attr";
	catalog<String>("fragmentAttr","enabler")="fragment";
	catalog<String>("fragmentAttr","hint")=
			"Name of string attribute describing membership"
			" in a collection of points.";

	catalog< sp<Component> >("Input Surface");
}

void SurfaceAttrConformOp::handle(Record& a_rSignal)
{
#if FE_SACO_DEBUG
	feLog("SurfaceAttrConformOp::handle \"%s\"\n",name().c_str());
#endif

	const String conformAttr=catalog<String>("conformAttr");
	const String fragmentAttr=catalog<String>("fragmentAttr");
	BWORD fragmented=(!fragmentAttr.empty() && catalog<bool>("fragment"));

	String& rSummary=catalog<String>("summary");
	rSummary=conformAttr;
	if(fragmented)
	{
		rSummary+=" by " + fragmentAttr;
	}

	if(conformAttr.empty())
	{
		catalog<String>("error")="Conform attribute name required;";
		return;
	}

	sp<SurfaceAccessibleI> spOutputAccessible;
	if(!accessOutput(spOutputAccessible,a_rSignal)) return;

	sp<SurfaceAccessorI> spOutputAttribute;
	if(!access(spOutputAttribute,spOutputAccessible,
			e_point,conformAttr,e_warning,e_refuseMissing))
	{
		catalog<String>("error")="Conform attribute is inaccessible;";
		return;
	}

	sp<SurfaceAccessorI> spOutputFragment;

	I32 fragmentCount=1;

	if(fragmented)
	{
		const String inputGroup="";	//* TODO

		spOutputAttribute->fragmentWith(SurfaceAccessibleI::e_point,
				fragmentAttr,inputGroup);
		fragmentCount=spOutputAttribute->fragmentCount();
		if(!fragmentCount)
		{
			fragmentCount=1;
			fragmented=FALSE;
		}

		access(spOutputFragment,spOutputAccessible,e_point,fragmentAttr);
	}

	sp<SurfaceAccessibleI::FilterI> spFilter;

	for(I32 fragmentIndex=0;fragmentIndex<fragmentCount;fragmentIndex++)
	{
		if(interrupted())
		{
			break;
		}

		std::map<String,I32> histogram;

		String fragmentName;
		I32 filterCount;
		if(fragmented)
		{
			fragmentName=spOutputAttribute->fragment(fragmentIndex);

			if(!spOutputAttribute->filterWith(fragmentName,spFilter) ||
					spFilter.isNull())
			{
				continue;
			}

			filterCount=spFilter->filterCount();
		}
		else
		{
			filterCount=spOutputAttribute->count();
		}

		if(!filterCount)
		{
			continue;
		}

		for(I32 filterIndex=0;filterIndex<filterCount;filterIndex++)
		{
			const I32 pointIndex=
					fragmented? spFilter->filter(filterIndex): filterIndex;

			const String value=spOutputAttribute->string(pointIndex);

			if(histogram.find(value)==histogram.end())
			{
				histogram[value]=0;
			}

			histogram[value]++;
		}

		String winner;
		I32 popularity=0;
		I32 total=0;

		for(std::map<String,I32>::iterator it=histogram.begin();
				it!=histogram.end();it++)
		{
			const String candidate=it->first;
			const I32 votes=it->second;

			if(votes>popularity)
			{
				winner=candidate;
				popularity=votes;
			}

			total+=votes;
		}

#if FE_SACO_DEBUG
		feLog("fragment %d/%d winner \"%s\" popularity %d/%d\n",
				fragmentIndex,fragmentCount,winner.c_str(),popularity,total);
#endif

		for(I32 filterIndex=0;filterIndex<filterCount;filterIndex++)
		{
			const I32 pointIndex=
					fragmented? spFilter->filter(filterIndex): filterIndex;

			spOutputAttribute->set(pointIndex,winner);
		}
	}

#if FE_SACO_DEBUG
	feLog("SurfaceAttrConformOp::handle done\n");
#endif
}
