/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <operator/operator.pmh>

#define FE_MRO_DEBUG		FALSE

//* TODO cache before/after positions, only address points that have moved

//* TODO multi-thread both searches (which is almost all the cost)

using namespace fe;
using namespace fe::ext;

void MirrorOp::initialize(void)
{
	catalog<String>("icon")="FE_alpha";

	catalog<String>("axis")="x";
	catalog<String>("axis","label")="Mirror Over";
	catalog<String>("axis","choice:0")="x";
	catalog<String>("axis","label:0")="X Axis";
	catalog<String>("axis","choice:1")="y";
	catalog<String>("axis","label:1")="Y Axis";
	catalog<String>("axis","choice:2")="z";
	catalog<String>("axis","label:2")="Z Axis";
	catalog<bool>("axis","joined")=true;

	catalog<bool>("snap")=false;
	catalog<String>("snap","label")="Snap to Surface";
	catalog<String>("snap","hint")=
			"Move mirror points onto driver surface.";

	catalog<bool>("PartitionDriver")=false;
	catalog<String>("PartitionDriver","label")="Partition Driver";
	catalog<bool>("PartitionDriver","joined")=true;
	catalog<String>("PartitionDriver","hint")=
			"Distiguish regions by matching attribute values.";

	catalog<String>("DriverPartitionAttr")="part";
	catalog<String>("DriverPartitionAttr","label")="Driver Attr";
	catalog<String>("DriverPartitionAttr","enabler")="PartitionDriver";
	catalog<String>("DriverPartitionAttr","hint")=
			"Primitive string attribute on driver used to distiguish regions.";

	catalog<String>("deleteParts")="";
	catalog<String>("deleteParts","label")="Delete From";
	catalog<bool>("deleteParts","joined")=true;
	catalog<String>("deleteParts","hint")=
			"Driver partition names on which points are removed."
			"  If no names are specified, no elements are considered."
			"  Each space-delimited substring is an individual pattern."
			"  A partition qualifies if it matches"
			" any of the substring patterns."
			"  Wildcard matching uses globbing unless Regex is turned on.";

	catalog<bool>("deleteRegex")=false;
	catalog<String>("deleteRegex","label")="Regex";
	catalog<String>("deleteRegex","hint")=
			"Pass the pattern strings directly as true regular expressions."
			"  If this option is off,"
			" presumed glob patterns are converted to regex."
			"  Every '.' is first escaped as '\\.'."
			"  Then, every '?', indicating 'any character',"
			" is converted to '.'."
			"  Every '*', indicating 'any number of any characters',"
			" is converted to '.*'."
			"  Every '[!', indicating 'not' inside of square brackets,"
			" is converted to '[^'.";

	catalog<String>("mirrorParts")="";
	catalog<String>("mirrorParts","label")="Mirror From";
	catalog<bool>("mirrorParts","joined")=true;
	catalog<String>("mirrorParts","hint")=
			"Driver partition names from which points are mirrored."
			"  If no names are specified, all elements are considered."
			"  Each space-delimited substring is an individual pattern."
			"  A partition qualifies if it matches"
			" any of the substring patterns."
			"  Wildcard matching uses globbing unless Regex is turned on.";

	catalog<bool>("mirrorRegex")=false;
	catalog<String>("mirrorRegex","label")="Regex";
	catalog<String>("mirrorRegex","hint")=
			"Pass the pattern strings directly as true regular expressions."
			"  If this option is off,"
			" presumed glob patterns are converted to regex."
			"  Every '.' is first escaped as '\\.'."
			"  Then, every '?', indicating 'any character',"
			" is converted to '.'."
			"  Every '*', indicating 'any number of any characters',"
			" is converted to '.*'."
			"  Every '[!', indicating 'not' inside of square brackets,"
			" is converted to '[^'.";

	catalog< sp<Component> >("Input Surface");

	catalog< sp<Component> >("Driver Surface");
}

void MirrorOp::handle(Record& a_rSignal)
{
#if FE_MRO_DEBUG
	feLog("MirrorOp::handle\n");
#endif

	const String axis=catalog<String>("axis");
	const BWORD partitionDriver=catalog<bool>("PartitionDriver");
	const String driverPartitionAttr=catalog<String>("DriverPartitionAttr");

	String& rSummary=catalog<String>("summary");
	rSummary="";

	if(partitionDriver && !driverPartitionAttr.empty())
	{
		rSummary+="by "+driverPartitionAttr+" ";
	}

	m_mirrorAxis=0;
	if(axis=="x")
	{
		rSummary+="in X";
	}
	if(axis=="y")
	{
		m_mirrorAxis=1;
		rSummary+="in Y";
	}
	else if(axis=="z")
	{
		m_mirrorAxis=2;
		rSummary+="in Z";
	}

	m_deletingParts=FALSE;
	m_mirroringParts=FALSE;
	if(partitionDriver)
	{
		String deleteParts=catalog<String>("deleteParts");
		if(!deleteParts.empty() && !catalog<bool>("deleteRegex"))
		{
			deleteParts=deleteParts.convertGlobToRegex();
		}

		String mirrorParts=catalog<String>("mirrorParts");
		if(!mirrorParts.empty() && !catalog<bool>("mirrorRegex"))
		{
			mirrorParts=mirrorParts.convertGlobToRegex();
		}

		m_deletingParts=(!deleteParts.empty());
		m_mirroringParts=(!mirrorParts.empty());

		String buffer=deleteParts;
		while(!buffer.empty())
		{
			m_deletePatternArray.push_back(buffer.parse());
		}

		buffer=mirrorParts;
		while(!buffer.empty())
		{
			m_mirrorPatternArray.push_back(buffer.parse());
		}
	}

	if(!access(m_spInputAccessible,"Input Surface")) return;

	sp<SurfaceAccessorI> spInputPoints;
	if(!access(spInputPoints,m_spInputAccessible,e_point,e_position)) return;

	sp<SurfaceAccessibleI> spOutputAccessible;
	if(!accessOutput(spOutputAccessible,a_rSignal)) return;

	sp<SurfaceAccessorI> spOutputPoints;
	if(!access(spOutputPoints,spOutputAccessible,e_point,e_position)) return;

	if(driverPartitionAttr!=m_lastPartitionAttr ||
			catalogOrDefault<bool>("Driver Surface","replaced",true))
	{
		m_spDriver=NULL;
	}

	if(m_spDriver.isNull())
	{
		if(!access(m_spDriver,"Driver Surface")) return;

		if(partitionDriver)
		{
			m_spDriver->partitionWith(driverPartitionAttr);
			m_spDriver->setPartitionFilter(".*");
		}
		m_spDriver->prepareForSearch();
	}

	m_lastPartitionAttr=driverPartitionAttr;

	sp<SurfaceAccessorI> spOutputPartition;

	if(partitionDriver)
	{
		access(spOutputPartition,spOutputAccessible,
				e_point,driverPartitionAttr);
	}

	const I32 pointCount=spInputPoints->count();
	m_partitionOf.resize(pointCount);
	m_shouldDelete.resize(pointCount);
	m_shouldMirror.resize(pointCount);
	m_mirrorOf.resize(pointCount);

	setNonAtomicRange(0,pointCount-1);

	adjustThreads();
	const I32 threadCount=catalog<I32>("Threads");

	if(threadCount>1)
	{
		OperatorThreaded::handle(a_rSignal);
	}
	else
	{
		//* serial
		run(-1,fullRange());
	}

	I32* mirrorIndex=new I32[pointCount];
	I32* copyIndex=new I32[pointCount];

	I32 addCount=0;
	I32 copyCount=0;

	std::set<I32> deleteSet;

	for(I32 pointIndex=0;pointIndex<pointCount;pointIndex++)
	{
		if(m_shouldDelete[pointIndex])
		{
			deleteSet.insert(pointIndex);
			deleteSet.insert(pointCount+pointIndex);
		}
		else
		{
			copyIndex[copyCount++]=pointIndex;
		}

		if(m_shouldMirror[pointIndex])
		{
			mirrorIndex[addCount++]=pointIndex;
		}
		else
		{
			deleteSet.insert(pointCount+pointIndex);
		}
	}

	spOutputAccessible->append(m_spInputAccessible,NULL);

	if(deleteSet.size())
	{
		deleteElements(spOutputAccessible,e_pointGroup,deleteSet,FALSE);
	}

	const I32 mirrorStart=copyCount;

	for(I32 m=0;m<addCount;m++)
	{
		copyIndex[copyCount++]=mirrorIndex[m];
	}

	if(spOutputPartition.isValid())
	{
		for(I32 m=0;m<copyCount;m++)
		{
			spOutputPartition->set(m,m_partitionOf[copyIndex[m]]);
		}
	}

	for(I32 toIndex=mirrorStart;toIndex<copyCount;toIndex++)
	{
		const I32 fromIndex=copyIndex[toIndex];

		spOutputPoints->set(toIndex,m_mirrorOf[fromIndex]);
	}

	Array<SurfaceAccessibleI::Spec> specs;
	m_spInputAccessible->attributeSpecs(specs,SurfaceAccessibleI::e_point);
	const U32 specCount=specs.size();

	for(U32 specIndex=0;specIndex<specCount;specIndex++)
	{
		const SurfaceAccessibleI::Spec& spec=specs[specIndex];
		const String& specName=spec.name();
		const String& specType=spec.typeName();

//		feLog("spec %d/%d \"%s\" \"%s\"\n",specIndex,specCount,
//				specType.c_str(),specName.c_str());

		if(specName=="P")
		{
			continue;
		}

		sp<SurfaceAccessorI> spInputAccessor=m_spInputAccessible->accessor(
				SurfaceAccessibleI::e_point,specName);

		sp<SurfaceAccessorI> spOutputAccessor=spOutputAccessible->accessor(
				SurfaceAccessibleI::e_point,specName,
				SurfaceAccessibleI::e_createMissing);
		FEASSERT(spOutputAccessor.isValid());

		if(spInputAccessor.isNull() || spOutputAccessor.isNull())
		{
			feLog("MirrorOp::handle %s %s accessor failure\n",
					specType.c_str(),specName.c_str());
			continue;
		}

		spOutputAccessor->setWritable(TRUE);

		if(specType=="vector3")
		{
			const BWORD isNormal=(specName=="N");

			for(I32 toIndex=0;toIndex<copyCount;toIndex++)
			{
				const I32 fromIndex=copyIndex[toIndex];
				SpatialVector vector3=spInputAccessor->spatialVector(fromIndex);

				if(isNormal && toIndex>=mirrorStart)
				{
					vector3[m_mirrorAxis]= -vector3[m_mirrorAxis];
				}

				spOutputAccessor->set(toIndex,vector3);
			}
		}
/*
		NOTE transfers only needed for value that need to be "mirrored"

		else if(specType=="real")
		{
			for(I32 toIndex=0;toIndex<copyCount;toIndex++)
			{
				const I32 fromIndex=copyIndex[toIndex];
				spOutputAccessor->set(toIndex,
						spInputAccessor->real(fromIndex));
			}
		}
		else if(specType=="integer")
		{
			for(I32 toIndex=0;toIndex<copyCount;toIndex++)
			{
				const I32 fromIndex=copyIndex[toIndex];
				spOutputAccessor->set(toIndex,
						spInputAccessor->integer(fromIndex));
			}
		}
		else if(specType=="string")
		{
			for(I32 toIndex=0;toIndex<copyCount;toIndex++)
			{
				const I32 fromIndex=copyIndex[toIndex];
				spOutputAccessor->set(toIndex,
						spInputAccessor->string(fromIndex));
			}
		}
*/
	}

	delete[] mirrorIndex;
	delete[] copyIndex;

#if FE_MRO_DEBUG
	feLog("MirrorOp::handle done\n");
#endif
}

void MirrorOp::run(I32 a_id,sp<SpannedRange> a_spRange)
{
	const BWORD snap=catalog<bool>("snap");
	const BWORD partitionDriver=catalog<bool>("PartitionDriver");

	sp<SurfaceAccessorI> spInputPoints;
	if(!access(spInputPoints,m_spInputAccessible,e_point,e_position)) return;

	String partitionName;
	I32 lastPartitionIndex= -1;

	std::map<String,BWORD> deleteMatch;
	std::map<String,BWORD> mirrorMatch;

	BWORD deletePoint=FALSE;
	BWORD mirrorPoint=TRUE;

	for(SpannedRange::Iterator it=a_spRange->begin();!it.atEnd();it.step())
	{
		if(interrupted())
		{
			return;
		}

		const I32 pointIndex=it.value();

		const SpatialVector point=spInputPoints->spatialVector(pointIndex);

		if(m_mirroringParts || m_deletingParts)
		{
			sp<SurfaceI::ImpactI> spImpact=m_spDriver->nearestPoint(point);
			if(spImpact.isValid())
			{
				if(partitionDriver)
				{
					const I32 partitionIndex=spImpact->partitionIndex();

					if(partitionIndex!=lastPartitionIndex)
					{
						partitionName=m_spDriver->partitionName(partitionIndex);

						deletePoint=FALSE;
						mirrorPoint=TRUE;

						if(m_deletingParts)
						{
							std::map<String,BWORD>::iterator it=
									deleteMatch.find(partitionName);
							if(it!=deleteMatch.end())
							{
								deletePoint=it->second;
							}
							else
							{
								const U32 deletePatternCount=
										m_deletePatternArray.size();
								for(U32 patternIndex=0;
										patternIndex<deletePatternCount;
										patternIndex++)
								{
									const String pattern=
											m_deletePatternArray[patternIndex];
									if(partitionName.match(pattern))
									{
										deletePoint=TRUE;
										break;
									}
								}
								deleteMatch[partitionName]=deletePoint;
							}
						}
						if(m_mirroringParts)
						{
							std::map<String,BWORD>::iterator it=
									mirrorMatch.find(partitionName);
							if(it!=mirrorMatch.end())
							{
								mirrorPoint=it->second;
							}
							else
							{
								mirrorPoint=FALSE;

								const U32 mirrorPatternCount=
										m_mirrorPatternArray.size();
								for(U32 patternIndex=0;
										patternIndex<mirrorPatternCount;
										patternIndex++)
								{
									const String pattern=
											m_mirrorPatternArray[patternIndex];
									if(partitionName.match(pattern))
									{
										mirrorPoint=TRUE;
										break;
									}
								}
								mirrorMatch[partitionName]=mirrorPoint;
							}
						}
						else
						{
							mirrorPoint=!deletePoint;
						}

//						feLog("point %d part %d \"%s\" delete %d mirror %d\n",
//								pointIndex,partitionIndex,partitionName.c_str(),
//								deletePoint,mirrorPoint);

						lastPartitionIndex=partitionIndex;
					}

					m_partitionOf[pointIndex]=partitionName;
				}
			}
		}

		m_shouldDelete[pointIndex]=deletePoint;
		m_shouldMirror[pointIndex]=mirrorPoint;
		m_mirrorOf[pointIndex]=point;

		if(mirrorPoint)
		{
			SpatialVector mirrored=point;

			mirrored[m_mirrorAxis]= -mirrored[m_mirrorAxis];

			if(snap)
			{
				sp<SurfaceI::ImpactI> spImpact=
						m_spDriver->nearestPoint(mirrored);
				if(spImpact.isValid())
				{
					mirrored=spImpact->intersection();
				}
			}

			m_mirrorOf[pointIndex]=mirrored;
		}
	}
}
