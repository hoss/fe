/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "solve/solve.pmh"
#include "SemiImplicit.h"
#include <algorithm>

//#define FE_ENABLE_SI_BANDWIDTH_NARROWING

#ifdef FE_ENABLE_SI_BANDWIDTH_NARROWING
#include "math/rcm.h"
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/cuthill_mckee_ordering.hpp>
#include <boost/graph/properties.hpp>
#include <boost/graph/bandwidth.hpp>
#endif

#if 0
#include <boost/numeric/ublas/vector.hpp>
#include <boost/numeric/ublas/vector_proxy.hpp>
#include <boost/numeric/ublas/matrix.hpp>
#include <boost/numeric/ublas/triangular.hpp>
#include <boost/numeric/ublas/lu.hpp>
#include <boost/numeric/ublas/io.hpp>
#endif

//#include "fasp_functs.h"
extern "C" void fasp_amg_(int *n, int *nnz, int *ia, int *ja, double *a, double *b, double *u, double *tol, int *maxit, int *ptrlvl);
extern "C" void fasp_krylov_amg_(int *n, int *nnz, int *ia, int *ja, double *a, double *b, double *u, double *tol, int *maxit, int *ptrlvl);


namespace fe
{
namespace ext
{


#if 0
namespace ublas = boost::numeric::ublas;

template<class T>
bool InvertMatrix(const ublas::matrix<T>& input, ublas::matrix<T>& inverse)
{
	using namespace boost::numeric::ublas;
	typedef permutation_matrix<std::size_t> pmatrix;

	// create a working copy of the input
	matrix<T> A(input);
	// create a permutation matrix for the LU-factorization
	pmatrix pm(A.size1());

	// perform LU-factorization
	int res = lu_factorize(A,pm);
	if( res != 0 ) return false;

	// create identity matrix of "inverse"
	inverse.assign(ublas::identity_matrix<T>(A.size1()));

	// backsubstitute to get the inverse
	lu_substitute(A, pm, inverse);

	//ublas::identity_matrix<T> I(A.size1());

	//std::cout << prod(input, inverse) << "\n";

	return true;
}
#endif


SemiImplicit::SemiImplicit(void)
{
	m_dxImplicitness = 1.0;
	m_dvImplicitness = 1.0;
	m_dv2dxRatio = 1.0;
	m_ratio = 1.0;
	m_subdivcnt = 0;
	m_subdivsz = 10;
	m_subdivmult = 1.1;
	m_gravity = t_solve_v3(0.0,0.0,-9.81);
	m_direct_solve = false;
	m_rayleigh_damping = false;
	m_rayleigh_stiffness = 0.00001;
	m_rayleigh_mass = -1000.0;
	m_2D = false;
}

SemiImplicit::~SemiImplicit(void)
{
}

void SemiImplicit::prestep(void)
{
	//SystemTicker wticker("SemiImplicit wholestep");
#ifdef TICKER
	SystemTicker ticker("SemiImplicit step");
	ticker.log("pre");
#endif
#if 0
	if(m_subdivcnt > 0)
	{
		m_subdivcnt--;
		if(!m_subdivcnt)
		{
			m_ratio *= m_subdivmult;
			if(m_ratio >= 1.0)
			{
				m_ratio = 1.0;
			}
			else
			{
				m_subdivcnt = m_subdivsz;
			}
		}
	}
	t_solve_real sim_ratio = m_ratio;
#endif
	// assumes m_dfdx, m_dfdv, and m_lhs are topologically identical

	/* Clear --------------------------------------------------------------- */
	unsigned int c = (unsigned int)m_lhs->blocks().size();
	for(unsigned int i = 0; i < c; i++)
	{
		setAll(m_lhs->blocks()[i], 0.0);
		setAll(m_dfdx->blocks()[i], 0.0);
		setAll(m_dfdv->blocks()[i], 0.0);
	}
	for(unsigned int i = 0; i < m_particles.size(); i++)
	{
		set(m_particles[i].m_force);
		set(m_particles[i].m_force_external);
		set(m_particles[i].m_force_weak);
	}
#if 0
FILE *fpa = fopen("newdfdx", "a");
for(unsigned int i = 0; i < m_n; i++)
{
fe_fprintf(fpa, "L %f %f %f V %f %f %f\n", m_particles[i].m_location[0], m_particles[i].m_location[1], m_particles[i].m_location[2], m_particles[i].m_velocity[0], m_particles[i].m_velocity[1], m_particles[i].m_velocity[2]);
}
fclose(fpa);
#endif

#if 1
	for(unsigned int i = 0; i < m_particles.size(); i++)
	{
		m_particles[i].m_force_weak +=
			m_particles[i].m_mass * m_gravity;
		m_particles[i].m_force_external +=
			m_particles[i].m_mass * m_gravity;
	}
#endif
#ifdef TICKER
	ticker.log("clear");
#endif

	/* Accumulate ---------------------------------------------------------- */
	for(unsigned int i = 0; i < m_forces_add_damping.size(); i++)
	{
		m_forces_add_damping[i]->accumulate();
	}

#ifdef TICKER
	ticker.log("accumulate");
#endif
//TODO: re experiment with rayleigh and IC solve
	if(m_rayleigh_damping)
	{
		for(unsigned int i = 0; i < m_n_sim; i++)
		{
			m_dv[i] = m_particles[i].m_velocity;
		}
		m_dfdx->multiply(m_tmp, m_dv);
		for(unsigned int i = 0; i < m_n_sim; i++)
		{
			m_particles[i].m_force += m_rayleigh_stiffness * m_tmp[i];
			m_particles[i].m_force += m_rayleigh_mass * m_particles[i].m_mass * m_dv[i];
		}
		t_solve_matrix identity;
		setIdentity(identity);
		for(unsigned int i = 0; i < m_n_sim; i++)
		{
			unsigned int i_block = m_dfdv->rowptr()[i];
			add(m_dfdv->blocks()[i_block], m_dfdv->blocks()[i_block],
				m_rayleigh_mass * m_particles[i].m_mass * identity);
		}
		for(unsigned int i = 0; i < m_dfdx->blocks().size(); i++)
		{
			add(m_dfdv->blocks()[i],m_dfdv->blocks()[i],m_rayleigh_stiffness*m_dfdx->blocks()[i]);
		}
	}
#ifdef TICKER
	ticker.log("rayleigh");
#endif

	/* Accumulate ---------------------------------------------------------- */
	for(unsigned int i = 0; i < m_forces_as_is.size(); i++)
	{
		m_forces_as_is[i]->accumulate();
	}
#ifdef TICKER
	ticker.log("accumulate 2");
#endif

#if 0
	for(unsigned int i = 0; i < m_n_sim; i++)
	{
		if(m_particles[i].m_force[0] != 0.0 || m_particles[i].m_force[1] != 0.0 || m_particles[i].m_force[2] != 0.0)
		fe_fprintf(stderr, "F %d %g %g %g\n", i, m_particles[i].m_force[0], m_particles[i].m_force[1], m_particles[i].m_force[2]);
	}
#endif
}

//#define TICKER
void SemiImplicit::step(t_solve_real a_timestep, t_solve_v3 &a_totalConstraintForce)
{
	t_solve_real h = a_timestep;
	t_solve_real h_sqr = h * h;

	prestep();

	/* LHS ----------------------------------------------------------------- */
#ifdef USE_FILTERS
	std::vector<unsigned int> massless_ids;
#endif
	t_solve_real dx_dv_impl = m_dxImplicitness * m_dvImplicitness;
	t_solve_matrix lhs_mat;
	for(unsigned int i = 0; i < m_n_sim; i++)
	{

		for(unsigned int i_block = m_lhs->rowptr()[i];
			i_block < m_lhs->rowptr()[i+1]; i_block++)
		{
			unsigned int j = m_lhs->colind()[i_block];
			assert(j >= i); // assert upper triangular

			t_solve_matrix &lhs = m_lhs->blocks()[i_block];
			t_solve_matrix &dfdx = m_dfdx->blocks()[i_block];
			t_solve_matrix &dfdv = m_dfdv->blocks()[i_block];

#if 0
FILE *fp = fopen("newdfdx", "a");
String ss;
ss = print(dfdx);
fe_fprintf(fp, "DFDX%s\n", ss.c_str());
ss = print(dfdv);
fe_fprintf(fp, "DFDV%s\n", ss.c_str());
fclose(fp);
#endif

			// -h * dv_impl * df/dv
			lhs = -h * m_dvImplicitness * dfdv;

			// -h^2 * dv_impl * dx_impl * df/dx
			subtract(lhs, lhs, h_sqr * dx_dv_impl * dfdx);

			// M (on diagonal only) and block jacobi preconditioner
			if(i == j)
			{
				t_solve_real mass = m_particles[i].m_mass;
				if(mass != 0.0)
				{
					lhs(0,0) += mass;
					lhs(1,1) += mass;
					lhs(2,2) += mass;

					if(!inverted(m_preconditioner.diagonal()[i], lhs))
					{
						setIdentity(m_preconditioner.diagonal()[i]);
					}

				}
				else
				{
					setIdentity(m_preconditioner.diagonal()[i]);
#ifdef USE_FILTERS
					massless_ids.push_back(i);
#endif
				}
			}
		}
	}
#ifdef TICKER
	ticker.log("LHS");
#endif

#ifdef USE_FILTERS
	/* Automatic Constraints ----------------------------------------------- */
	std::vector<FilterConstraint> filters(massless_ids.size());
	for(unsigned int i = 0; i < massless_ids.size(); i++)
	{
		filters[i].makePointConstraint();
		filters[i].setIndex(massless_ids[i]);
	}
#else
	//std::vector<FilterConstraint> filters(massless_ids.size());
	std::vector<FilterConstraint< t_solve_real> > &filters = m_dummy_filters;
#endif

#ifdef TICKER
	ticker.log("auto filters");
#endif

	/* RHS (phase 1) ------------------------------------------------------- */
	for(unsigned int i = 0; i < m_n_sim; i++)
	{
		m_dv[i] = solveZeroVector;
	}

	/* Plugin Constraints -------------------------------------------------- */
	for(unsigned int i = 0; i < m_n_sim; i++)
	{
		m_perturb[i] = solveZeroVector;
	}
	for(unsigned int i = 0; i < m_forces_add_damping.size(); i++)
	{
		m_forces_add_damping[i]->constrain(filters, m_dv, m_perturb, a_timestep);
	}
	for(unsigned int i = 0; i < m_forces_as_is.size(); i++)
	{
		m_forces_as_is[i]->constrain(filters, m_dv, m_perturb, a_timestep);
	}
#ifndef USE_FILTERS
	assert(!filters.size());
#endif
#ifdef TICKER
	ticker.log("constraints");
#endif

	/* RHS (phase 2) ------------------------------------------------------- */
	for(unsigned int i = 0; i < m_n_sim; i++)
	{
		m_rhs[i] = solveZeroVector;
		m_tmp[i] = h_sqr * m_dvImplicitness * m_particles[i].m_velocity;
	}
	m_dfdx->multiply(m_rhs, m_tmp);
	for(unsigned int i = 0; i < m_n_sim; i++)
	{
		m_tmp[i] = h * m_perturb[i];
	}
	m_dfdx->multiply(m_tmp, m_tmp);

	for(unsigned int i = 0; i < m_n_sim; i++)
	{
		t_solve_v3 &f0 = m_particles[i].m_force;
		m_rhs[i] += h * f0 + m_tmp[i];
	}

#ifdef TICKER
	ticker.log("RHS");
#endif

#if 0
fe_fprintf(stderr, "%d\n", m_n);
	unsigned int cnt = 132;
	//unsigned int cnt = 528;
	//unsigned int cnt = 1056;
	unsigned int M = m_n / cnt;
	unsigned int m0 = 0;
	unsigned int m1 = M;
	unsigned int pad = 0;
	m_preconditioner2.block().resize(cnt);
	for(unsigned int a = 0; a < cnt; a++)
	{
		sp<UpperTriangularBCRS> subLHS(new UpperTriangularBCRS());
		std::vector<t_solve_v3> subB;
		std::vector<t_solve_v3> subX;
		BlockDiagonalPreconditioner subP;
		std::vector<FilterConstraint> sub_filters;
		int iL = (int)m0 - (int)pad;
		if(iL < 0) {iL = 0;}
		unsigned int L = iL;

		unsigned int H = m1 + pad;
		if(H > m_n) { H = m_n; }

		subB.resize(H-L);
		subX.resize(H-L);
		subP.diagonal().resize(H-L);

//fe_fprintf(stderr, "L %d H %d m0 %d m1 %d\n", L, H, m0, m1);

		bool skip = false;
		for(unsigned int i = L; i < H; i++)
		{
			t_solve_real mass = m_particles[i].m_mass;
			if(mass == 0.0)
			{
				skip = true;
			}
			unsigned int ii = i - L;
			subLHS->startrow();
			for(unsigned int i_block = m_lhs->rowptr()[i];
				i_block < m_lhs->rowptr()[i+1]; i_block++)
			{
				unsigned int j = m_lhs->colind()[i_block];
				if(j < L) continue;
				if(j >= H) continue;
				assert(j >= i); // assert upper triangular

				t_solve_matrix &lhs = m_lhs->blocks()[i_block];

				unsigned int jj = j - L;

				subLHS->insert(jj, lhs);
			}

			subB[ii] = m_rhs[i];
			//subX[ii] = m_dv[i];
			subX[ii] = solveZeroVector;
			subP.diagonal()[ii] = m_preconditioner.diagonal()[i];
		}
		subLHS->done();

		unsigned int u_n = (H-L)*3;
		ublas::matrix<t_solve_real> umatrix(u_n, u_n);
		for(unsigned int i = 0; i < u_n; i++)
		{
			for(unsigned int j = 0; j < u_n; j++)
			{
				umatrix(i,j) = 0.0;
			}
		}
		for(unsigned int i = 0; i < (H-L); i++)
		{
			for(unsigned int i_block = subLHS->rowptr()[i];
				i_block < subLHS->rowptr()[i+1]; i_block++)
			{
				unsigned int j = subLHS->colind()[i_block];
				t_solve_matrix &lhs = subLHS->blocks()[i_block];

				for(unsigned int ii = 0; ii < 3; ii++)
				{
					unsigned int iii = i*3+ii;
					for(unsigned int jj = 0; jj < 3; jj++)
					{
						unsigned int jjj = j*3+jj;
						umatrix(iii,jjj) = lhs(ii,jj);
					}
				}
			}
		}
		m_preconditioner2.block()[a].m_inverse.resize(u_n, u_n);
		skip = true;
		if(!skip)
		{
fe_fprintf(stderr, "+");
			InvertMatrix(umatrix, m_preconditioner2.block()[a].m_inverse);
		}
		else
		{
fe_fprintf(stderr, "-");
			m_preconditioner2.block()[a].m_inverse.assign(ublas::identity_matrix<t_solve_real>(u_n));
		}
		m_preconditioner2.block()[a].m_L = L;
		m_preconditioner2.block()[a].m_H = H;
		m_preconditioner2.block()[a].m_m0 = m0;
		m_preconditioner2.block()[a].m_m1 = m1;

		for(unsigned int f = 0; f < filters.size(); f++)
		{
			if(filters[f].index() >= L && filters[f].index() < H)
			{
				sub_filters.resize(sub_filters.size()+1);
				FilterConstraint &fc = sub_filters.back();
				fc.makePointConstraint();
				fc.setIndex(filters[f].index() - L);
			}
		}

#if 0
		for(unsigned int i = 0; i < 2; i++)
		{
			sub_filters.resize(sub_filters.size()+1);
			FilterConstraint &fc = sub_filters.back();
			fc.makePointConstraint();
			fc.setIndex(i);
		}

		for(unsigned int i = H-L-2; i < H-L; i++)
		{
			sub_filters.resize(sub_filters.size()+1);
			FilterConstraint &fc = sub_filters.back();
			fc.makePointConstraint();
			fc.setIndex(i);
		}
#endif

#if 0
		BlockPCG solver;
		bool rv = solver.solve(subX, subLHS, subB, subP, sub_filters);

		for(unsigned int i = m0; i < m1; i++)
		{
			unsigned int ii = i - L;
//fe_fprintf(stderr, "%d %d %d\n", i, ii, subX.size(););
			m_dv[i] = subX[ii];
		}
#endif



		m0 += M;
		m1 += M;
	}
#endif
#if 0
	for(unsigned int i = 0; i < m_n; i++)
	{
		t_solve_matrix I;
		setIdentity(I);
		if(! (I == m_preconditioner.diagonal()[i]))
		multiply(m_dv[i], m_preconditioner.diagonal()[i], m_rhs[i]);
	}
#endif


	/* Solve --------------------------------------------------------------- */
	if(!m_direct_solve)
	{
#ifdef TICKER
		ticker.log("pre CG");
#endif
		/* bool rv = */ m_solver.solve(m_dv, m_lhs, m_rhs, m_preconditioner, filters);
#ifdef TICKER
		ticker.log("post CG");
#endif
	}

#if 0
	m_lhs->writeDense("x_lhs");
	m_lhs->write("x_lhs_sp");
	sp<UpperTriangularBCRS> spT(new UpperTriangularBCRS());
#endif

#if 0
	/* Call signature for AMG:
		void fasp_amg_(
			int *n,
			int *nnz,
			int *ia,
			int *ja,
			double *a,
			double *b,
			double *u,
			double *tol,
			int *maxit,
			int *ptrlvl)
		*/

	CRS lhs_crs;
	m_lhs->write(lhs_crs);

	std::vector<double> rhs;
	rhs.resize(m_rhs.size() * 3);
	for(unsigned int i = 0; i < m_rhs.size(); i++)
	{
		rhs[i*3 + 0] = m_rhs[i][0];
		rhs[i*3 + 1] = m_rhs[i][1];
		rhs[i*3 + 2] = m_rhs[i][2];
	}

	std::vector<double> u;
	u.resize(rhs.size());

	int n, nnz, ia[3], ja[2], maxit, ptrlvl;
	double a[2], b[2], x[2], tol;
	//fasp_amg_(&n, &nnz, &ia, &ja, &a, &b, &u, &tol, &maxit, &ptrlvl);
	n = lhs_crs.m_rowptr.size()-1;
	nnz = lhs_crs.m_values.size();
	tol = 1.0e-6;
	maxit = 10;
	ptrlvl = 100;
	n = 2;
	nnz = 2;
	ia[0] = 0;
	ia[1] = 1;
	ia[2] = 2;
	ja[0] = 0;
	ja[1] = 1;
	a[0] = 1.0;
	a[1] = 1.0;
	b[0] = 2.0;
	b[1] = 2.0;
	x[0] = 1.0;
	x[1] = 1.0;


#if 1
	fasp_amg_(
		&n,
		&nnz,
		ia, //&(lhs_crs.m_rowptr[0]),
		ja, //&(lhs_crs.m_colind[0]),
		a, //&(lhs_crs.m_values[0]),
		b, //&(rhs[0]),
		x, //&(u[0]),
		&tol,
		&maxit,
		&ptrlvl);
#endif

	fe_fprintf(stderr, "x %g\n", x[0]);

#if 0
	arma::mat arma_mat(m_n_sim*3,m_n_sim*3);
fe_fprintf(stderr, "m_n_sim %d\n", m_n_sim);
	arma_mat.load("x_lhs");
	arma::mat arma_chol = arma::chol(arma_mat);
	arma_chol.save("x_lhs_sqrt", arma::raw_ascii);
	spT->readDense("x_lhs_sqrt", m_n_sim, m_n_sim);
	spT->backSolve(m_dv, m_rhs);
#endif
#endif

#if 0
	sp<UpperTriangularBCRS> spL(new UpperTriangularBCRS());
	spL->sparse(spT);
	spT->readDense("x_lhs", m_n_sim, m_n_sim);
	unsigned int L = spL->blocks().size();
	for(unsigned int i = 0; i < L; i++)
	{
		setAll(spL->blocks()[i], 0.0);
	}
	spL->project(spT);
	spL->writeDense("x_L");
	spL->incompleteSqrt();
	spL->writeDense("x_L_sqrt");
	//spL->readDense("x_L_sqrt", m_n_sim, m_n_sim);
	spL->backSolve(m_dv, m_rhs);
#endif

#if 0
	sp<UpperTriangularBCRS> spA(new UpperTriangularBCRS());
	sp<UpperTriangularBCRS> spB(new UpperTriangularBCRS());
	spA->readDense("x_lhs_sqrt", m_n_sim, m_n_sim);
	spB->readDense("x_L_sqrt", m_n_sim, m_n_sim);
	for(unsigned int i = 0; i < spA->blocks().size(); i++)
	{
		subtract(spA->blocks()[i], spA->blocks()[i], spB->blocks()[i]);
	}
	spA->writeDense("x_diff");

fe_fprintf(stderr, "SIZES %d %d\n", spA->blocks().size(), m_n_sim);
static int cnt = 0;
//if(++cnt > 1) exit(101);
#endif

#if 0
	static int cnt = 0;
	if(cnt == 0)
	{
		m_lhs_snapshot = new UpperTriangularBCRS();
		m_lhs->writeDense("x_lhs");
		m_lhs_snapshot->readDense("x_lhs", m_n_sim, m_n_sim);
		m_lhs_snapshot->incompleteSqrt();
	}
	if(!(cnt%1))
	{
		m_lhs_snapshot = new UpperTriangularBCRS();
		m_lhs->writeDense("x_lhs");
		m_lhs_snapshot->readDense("x_lhs", m_n_sim, m_n_sim);
		m_lhs_snapshot->incompleteSqrt();
	}
	cnt++;
	ticker.log("pre ICD");
	//spT->readDense("x_lhs", m_n_sim, m_n_sim);
	//spT->read("x_lhs_sp");
	//m_lhs->incompleteSqrt();
	ticker.log("post ICD (incompleteSqrt)");
	//spT->writeDense("x_lhs_isqrt");
	//m_lhs->backSolve(m_dv, m_rhs);
	m_lhs_snapshot->backSolve(m_dv, m_rhs);
	ticker.log("post ICD (backsolve)");
#endif

	// This One
#if 1
	if(m_direct_solve)
	{
		//ticker.log("pre ICD");
		//m_lhs->writeDense("x_lhs");
		m_lhs->incompleteSqrt();
		//ticker.log("post ICD (incompleteSqrt)");
		//m_lhs->backSolve(m_dv, m_rhs);
		m_lhs->backSolve(m_dv, m_rhs);
		//ticker.log("post ICD");
	}
#endif

#if 0
	spT->readDense("x_lhs", m_n_sim, m_n_sim);
	ticker.log("pre ICD");
	spT->incompleteSqrt();
	ticker.log("post ICD (incompleteSqrt)");
	spT->backSolve(m_dv, m_rhs);
	ticker.log("post ICD (backsolve)");
#endif

#if 0
	IncompleteSparse solver;
	bool rv = solver.solve(m_dv, m_lhs, m_rhs);
	if(!rv)
	{
		feLog("WARNING: solver failed\n");
	}
#endif



#if 0
FILE *fp = fopen("newdfdx", "a");
for(unsigned int i = 0; i < m_n; i++)
{
fe_fprintf(fp, "DV %d %f %f %f -- V %f %f %f\n", i, m_dv[i][0], m_dv[i][1], m_dv[i][2], m_particles[i].m_velocity[0], m_particles[i].m_velocity[1], m_particles[i].m_velocity[2]);
fe_fprintf(fp, "L %f %f %f\n", m_particles[i].m_location[0], m_particles[i].m_location[1], m_particles[i].m_location[2]);
fe_fprintf(fp, "RHS %f %f %f\n", m_rhs[i][0], m_rhs[i][1], m_rhs[i][2]);
fe_fprintf(fp, "TMP %f %f %f\n", m_tmp[i][0], m_tmp[i][1], m_tmp[i][2]);
}
fclose(fp);
#endif

	const std::vector<t_solve_v3> &residual = m_solver.residual();
	// apply solve
	t_solve_real total_mass = 0.0;
	for(unsigned int i = 0; i < m_n_sim; i++)
	{

//#define EXPLICIT
#ifdef EXPLICIT
		m_dv[i] = t_solve_v3(0.0,0.0,0.0);
		m_particles[i].m_force_weak += m_particles[i].m_force;
#endif
		// weak force
		t_solve_v3 weak_dv(0.0,0.0,0.0);
		if(m_particles[i].m_mass > 0.0)
		{
			weak_dv = h * m_particles[i].m_force_weak / m_particles[i].m_mass;
		}


		t_solve_v3 v0 = m_particles[i].m_velocity;
		t_solve_v3 v1 = v0 + m_dv[i] + weak_dv;

		t_solve_v3 x1 = m_particles[i].m_location +
			h * (v0 + m_dv2dxRatio * m_dv[i] + weak_dv);

		m_particles[i].m_prev_velocity = m_particles[i].m_velocity;
		m_particles[i].m_prev_location = m_particles[i].m_location;
		m_particles[i].m_velocity = v1;
		m_particles[i].m_location = x1 + m_perturb[i];

		if(m_2D)
		{
			m_particles[i].m_velocity[2] = 0.0;
			m_particles[i].m_location[2] = 0.0;
		}

		//m_particles[i].m_velocity += weak_dv;

		total_mass += m_particles[i].m_mass;

		m_particles[i].m_constraint_force = solveZeroVector;

	}
#if 0
FILE *fp1 = fopen("newdfdx", "a");
for(unsigned int i = 0; i < m_n; i++)
{
fe_fprintf(fp1, "V %d %f %f %f\n", i, m_particles[i].m_velocity[0], m_particles[i].m_velocity[1], m_particles[i].m_velocity[2]);
}
fclose(fp1);
#endif
	a_totalConstraintForce = solveZeroVector;
	if(h > 0.0)
	{
		for(unsigned int i = 0; i < filters.size(); i++)
		{
			t_solve_v3 F = residual[filters[i].index()] / h;
			a_totalConstraintForce += F;
			m_particles[filters[i].index()].m_constraint_force += F;
		}
	}

// TODO make validation optional
#if 0
	/* Validate ------------------------------------------------------------ */
	bool valid = true;
	for(unsigned int i = 0; i < m_forces.size(); i++)
	{
		if(!m_forces[i]->validate())
		{
			valid = false;
		}
	}

	t_solve_real eh = a_timestep * (1.0 - sim_ratio);
	if(!valid)
	{
		sim_ratio = 0.0;
		m_ratio *= 0.5;
		m_subdivcnt = m_subdivsz;
		eh = a_timestep * (1.0 - sim_ratio);
	}

	if(sim_ratio != 1.0)
	{
		// TODO: maybe do angular vel in this rigid body mode too, although if
		//       this happens enough for it to matter, we have bigger problems
		t_solve_v3 average_vel(0.0,0.0,0.0);
		t_solve_v3 total_force(-a_totalConstraintForce);
		t_solve_real total_mass = 0.0;
		unsigned long n = 0;
		for(unsigned int i = 0; i < m_n; i++)
		{
			average_vel += m_particles[i].m_prev_velocity;
			total_force += m_particles[i].m_force_external;
			total_mass += m_particles[i].m_mass;
			n++;
		}
		average_vel *= 1.0 / (t_solve_real)n;

		for(unsigned int i = 0; i < m_n; i++)
		{
			if(m_particles[i].m_mass <= 0.0) { continue; }
			t_solve_v3 dx = m_particles[i].m_location -
				m_particles[i].m_prev_location;
			dx *= sim_ratio;
			m_particles[i].m_location =
				m_particles[i].m_prev_location + dx;

			t_solve_v3 fdv = total_force/total_mass * eh;

			t_solve_v3 dv = m_particles[i].m_velocity -
				m_particles[i].m_prev_velocity;
			dv *= sim_ratio;
			m_particles[i].m_velocity =
				m_particles[i].m_prev_velocity + dv + fdv;

			m_particles[i].m_location +=
				eh *
				(average_vel  + fdv);
		}
	}
#endif
#ifdef TICKER
	ticker.log("remainder");
#endif
	//wticker.log("whole");
}

void SemiImplicit::extract(sp<RecordGroup> rg_output)
{
	for(RecordGroup::iterator i_rg = rg_output->begin();
		i_rg != rg_output->end(); i_rg++)
	{
		sp<RecordArray> spRA = *i_rg;
		sp<Scope> spScope = spRA->layout()->scope();
		m_asSolverParticle.bind(spScope);
		m_asParticle.bind(spScope);
#ifdef PARTICLE_DEBUG
		m_asColored.bind(spScope);
#endif
		if(m_asSolverParticle.check(spRA))
		{
			for(int i = 0; i < spRA->length(); i++)
			{
				Record r_particle = spRA->getRecord(i);
				const unsigned int &index= m_asSolverParticle.index(r_particle);

#if 1
				m_asParticle.location(r_particle)[0] = m_particles[index].m_location[0];
				m_asParticle.location(r_particle)[1] = m_particles[index].m_location[1];
				m_asParticle.location(r_particle)[2] = m_particles[index].m_location[2];
//fe_fprintf(stderr, "extract %g %g %g\n", m_asParticle.location(r_particle)[0], m_asParticle.location(r_particle)[1], m_asParticle.location(r_particle)[2]);

				m_asParticle.velocity(r_particle)[0] = m_particles[index].m_velocity[0];
				m_asParticle.velocity(r_particle)[1] = m_particles[index].m_velocity[1];
				m_asParticle.velocity(r_particle)[2] = m_particles[index].m_velocity[2];
#endif
#ifdef PARTICLE_DEBUG
				m_asColored.color(r_particle) =
					m_particles[index].m_color;
#endif
			}
		}
	}
}

void SemiImplicit::initialize(sp<Scope> a_spScope)
{
	m_asForcePoint.bind(a_spScope);
	m_asSolverParticle.bind(a_spScope);
	m_asForcePoint.enforceHaving(m_asSolverParticle);
}

void SemiImplicit::compile(sp<RecordGroup> rg_input)
{

	m_n = 0;
	m_n_sim = 0;
	/* Particles ----------------------------------------------------------- */
	m_recordToParticle.clear();
	m_particles.clear();
	unsigned int index = 0;


	/* Pre Compile --------------------------------------------------------- */
	/*	The purpose of this stage is for forces to have the opportunity
		to manipulate the input, such as adding particles or constraints.
		*/
	for(unsigned int i = 0; i < m_forces_add_damping.size(); i++)
	{
		m_forces_add_damping[i]->precompile(rg_input);
	}
	for(unsigned int i = 0; i < m_forces_as_is.size(); i++)
	{
		m_forces_as_is[i]->precompile(rg_input);
	}


	// pass for actual particles
	for(RecordGroup::iterator i_rg = rg_input->begin();
			i_rg != rg_input->end(); i_rg++)
	{
		sp<RecordArray> spRA = *i_rg;
		sp<Scope> spScope = spRA->layout()->scope();
		m_asParticle.bind(spScope);
		m_asForcePoint.bind(spScope);
		m_asSolverParticle.bind(spScope);
#ifdef PARTICLE_DEBUG
		m_asColored.bind(spScope);
#endif

		m_asForcePoint.enforceHaving(m_asSolverParticle);
		if(m_asForcePoint.check(spRA) && m_asParticle.check(spRA))
		{
			for(int i = 0; i < spRA->length(); i++)
			{
				Record r_particle = spRA->getRecord(i);

				m_recordToParticle[r_particle.idr()] = (unsigned int)m_particles.size();

				m_particles.resize(m_particles.size()+1);
				Particle &particle = m_particles.back();

				particle.m_location[0] = m_asForcePoint.location(r_particle)[0];
				particle.m_location[1] = m_asForcePoint.location(r_particle)[1];
				particle.m_location[2] = m_asForcePoint.location(r_particle)[2];
				particle.m_velocity[0] = m_asForcePoint.velocity(r_particle)[0];
				particle.m_velocity[1] = m_asForcePoint.velocity(r_particle)[1];
				particle.m_velocity[2] = m_asForcePoint.velocity(r_particle)[2];
				particle.m_force = solveZeroVector;
				particle.m_constraint_force = solveZeroVector;
#ifdef PARTICLE_DEBUG
				particle.m_color = m_asColored.color(r_particle);
#endif

				particle.m_mass = m_asParticle.mass(r_particle);

				m_asSolverParticle.index(r_particle) = index;
				index++;
			}
		}
	}

	m_n_sim = (unsigned int)m_particles.size();

	// pass for constraint particles
	for(RecordGroup::iterator i_rg = rg_input->begin();
			i_rg != rg_input->end(); i_rg++)
	{
		sp<RecordArray> spRA = *i_rg;
		sp<Scope> spScope = spRA->layout()->scope();
		m_asParticle.bind(spScope);
		m_asForcePoint.bind(spScope);
		m_asSolverParticle.bind(spScope);
#ifdef PARTICLE_DEBUG
		m_asColored.bind(spScope);
#endif

		m_asForcePoint.enforceHaving(m_asSolverParticle);
		if(m_asForcePoint.check(spRA) && !m_asParticle.check(spRA))
		{
			for(int i = 0; i < spRA->length(); i++)
			{
				Record r_particle = spRA->getRecord(i);

				m_recordToParticle[r_particle.idr()] = (unsigned int)m_particles.size();
//fe_fprintf(stderr, "0x%x C %d = %d\n", this, r_particle.idr(), m_particles.size());

				m_particles.resize(m_particles.size()+1);
				Particle &particle = m_particles.back();

				particle.m_location[0] = m_asForcePoint.location(r_particle)[0];
				particle.m_location[1] = m_asForcePoint.location(r_particle)[1];
				particle.m_location[2] = m_asForcePoint.location(r_particle)[2];
				particle.m_velocity[0] = m_asForcePoint.velocity(r_particle)[0];
				particle.m_velocity[1] = m_asForcePoint.velocity(r_particle)[1];
				particle.m_velocity[2] = m_asForcePoint.velocity(r_particle)[2];
				particle.m_force = solveZeroVector;
				particle.m_constraint_force = solveZeroVector;
#ifdef PARTICLE_DEBUG
				particle.m_color = m_asColored.color(r_particle);
#endif

				particle.m_mass = 0.0;

				m_asSolverParticle.index(r_particle) = index;
				index++;
			}
		}
	}


#ifdef USE_FILTERS
	m_n_sim = (unsigned int)m_particles.size();
#endif
	m_n = (unsigned int)m_particles.size();
	m_preconditioner.diagonal().resize(m_n_sim);
	m_rhs.resize(m_n_sim);
	m_dv.resize(m_n_sim);
	m_tmp.resize(m_n_sim);
	m_perturb.resize(m_n_sim);



	/* Express Connectivity ------------------------------------------------ */
	/*	The purpose of this stage is for forces to express connectivity so
		that reordering has something to work with.

		Altering rg_input at this point is precarious.
		*/
	t_pairs pairs;
	for(unsigned int i = 0; i < m_forces_add_damping.size(); i++)
	{
		m_forces_add_damping[i]->pairs(rg_input, pairs);
	}
	for(unsigned int i = 0; i < m_forces_as_is.size(); i++)
	{
		m_forces_as_is[i]->pairs(rg_input, pairs);
	}

#ifdef FE_ENABLE_SI_BANDWIDTH_NARROWING
// TODO: turn this back on (and reimplement to not reorder constraints)
	/* Reorder ------------------------------------------------------------- */
	t_pairs dynamic_only_pairs;
	for(unsigned int i = 0; i < pairs.size(); i++)
	{
		if(pairs[i].first >= m_n_sim) { continue; }
		if(pairs[i].second >= m_n_sim) { continue; }
		dynamic_only_pairs.push_back(pairs[i]);
	}
	std::vector<unsigned int> order;
	reorder(order, dynamic_only_pairs);
	for(RecordGroup::iterator i_rg = rg_input->begin();
			i_rg != rg_input->end(); i_rg++)
	{
		sp<RecordArray> spRA = *i_rg;
		sp<Scope> spScope = spRA->layout()->scope();
		m_asParticle.bind(spScope);
		m_asForcePoint.bind(spScope);
		m_asSolverParticle.bind(spScope);

		if(m_asForcePoint.check(spRA))
		{
			for(int i = 0; i < spRA->length(); i++)
			{
				Record r_particle = spRA->getRecord(i);

//fe_fprintf(stderr, "solver index new %d <- old %d\n", order[m_asSolverParticle.index(r_particle)], m_asSolverParticle.index(r_particle));
				if(m_asSolverParticle.index(r_particle) < (int)m_n_sim)
				{
					m_asSolverParticle.index(r_particle) =
						order[m_asSolverParticle.index(r_particle)];
				}
			}
		}
	}
#endif


	/* Matrix Create ------------------------------------------------------- */
	CompileMatrix compileMatrix;
	compileMatrix.setRows(m_n);

	/* Forces -------------------------------------------------------------- */
	for(unsigned int i = 0; i < m_forces_add_damping.size(); i++)
	{
		m_forces_add_damping[i]->compile(rg_input, m_particles, compileMatrix);
	}
	for(unsigned int i = 0; i < m_forces_as_is.size(); i++)
	{
		m_forces_as_is[i]->compile(rg_input, m_particles, compileMatrix);
	}

	/* Matrix -------------------------------------------------------------- */
	m_lhs = new UpperTriangularBCRS<t_solve_real>();
	m_dfdx = new UpperTriangularBCRS<t_solve_real>();
	m_dfdv = new UpperTriangularBCRS<t_solve_real>();
	t_solve_matrix blank;
	// force diagonal to exist
	for(unsigned int i = 0; i < m_n_sim; i++)
	{
		compileMatrix.entry(i,i);
	}

	if(m_direct_solve)
	{
		compileMatrix.symbolicFill();
	}

	for(unsigned int i = 0; i < m_n_sim; i++)
	{
		m_dfdx->startrow();
		m_dfdv->startrow();
		m_lhs->startrow();

		CompileMatrix::t_row &row = compileMatrix.row(i);

		for(CompileMatrix::t_row::iterator i_row = row.begin();
			i_row != row.end(); i_row++)
		{
			unsigned int j = (*i_row).first;
			assert(j >= i); // assert upper triangular

			if(j >= m_n_sim) { continue; }

			//t_solve_matrix &lhs_block = m_lhs->insert(j, blank);
			//t_solve_matrix &dfdx_block = m_dfdx->insert(j, blank);
			//t_solve_matrix &dfdv_block = m_dfdv->insert(j, blank);
			m_lhs->insert(j, blank);
			m_dfdx->insert(j, blank);
			m_dfdv->insert(j, blank);

#if 0
			CompileMatrix::t_entry &entry = (*i_row).second;

			CompileMatrix::t_dfdx_array &dfdx_array = entry.first;
			CompileMatrix::t_dfdv_array &dfdv_array = entry.second;

			for(unsigned int k = 0; k < dfdx_array.size(); k++)
			{
				*(dfdx_array[k]) = &dfdx_block;
			}

fe_fprintf(stderr, "entry sz %d %d\n", dfdx_array.size(), dfdv_array.size());
			for(unsigned int k = 0; k < dfdv_array.size(); k++)
			{
fe_fprintf(stderr, "(%d %d) 0x%x\n", i, j, (dfdv_array[k]));
				*(dfdv_array[k]) = &dfdv_block;
			}
#endif
		}
	}
	m_lhs->done();
	m_dfdx->done();
	m_dfdv->done();


	for(unsigned int i = 0; i < m_n_sim; i++)
	{
		CompileMatrix::t_row &row = compileMatrix.row(i);

		unsigned int i_lhs_row = m_lhs->rowptr()[i];

		for(CompileMatrix::t_row::iterator i_row = row.begin();
			i_row != row.end(); i_row++)
		{
			unsigned int j = (*i_row).first;
			assert(j >= i); // assert upper triangular

			if(j >= m_n_sim)
			{
				CompileMatrix::t_entry &entry = (*i_row).second;

				CompileMatrix::t_dfdx_array &dfdx_array = entry.first;
				CompileMatrix::t_dfdv_array &dfdv_array = entry.second;

				for(unsigned int k = 0; k < dfdx_array.size(); k++)
				{
					*(dfdx_array[k]) = &m_dummy_block;
				}

				for(unsigned int k = 0; k < dfdv_array.size(); k++)
				{
					*(dfdv_array[k]) = &m_dummy_block;
				}
			}
			else
			{
				assert(j == m_lhs->colind()[i_lhs_row]); // assert consistency

				/*t_solve_matrix &lhs_block =*/ m_lhs->blocks()[i_lhs_row];
				t_solve_matrix &dfdx_block = m_dfdx->blocks()[i_lhs_row];
				t_solve_matrix &dfdv_block = m_dfdv->blocks()[i_lhs_row];

				i_lhs_row++;

				CompileMatrix::t_entry &entry = (*i_row).second;

				CompileMatrix::t_dfdx_array &dfdx_array = entry.first;
				CompileMatrix::t_dfdv_array &dfdv_array = entry.second;

				for(unsigned int k = 0; k < dfdx_array.size(); k++)
				{
					*(dfdx_array[k]) = &dfdx_block;
				}

				for(unsigned int k = 0; k < dfdv_array.size(); k++)
				{
					*(dfdv_array[k]) = &dfdv_block;
				}
			}
		}
	}

	for(unsigned int i = m_n_sim; i < m_n; i++)
	{
		CompileMatrix::t_row &row = compileMatrix.row(i);

		//unsigned int i_lhs_row = m_lhs->rowptr()[i];

		for(CompileMatrix::t_row::iterator i_row = row.begin();
			i_row != row.end(); i_row++)
		{
			assert((*i_row).first >= i); // assert upper triangular

			CompileMatrix::t_entry &entry = (*i_row).second;

			CompileMatrix::t_dfdx_array &dfdx_array = entry.first;
			CompileMatrix::t_dfdv_array &dfdv_array = entry.second;

			for(unsigned int k = 0; k < dfdx_array.size(); k++)
			{
				*(dfdx_array[k]) = &m_dummy_block;
			}

			for(unsigned int k = 0; k < dfdv_array.size(); k++)
			{
				*(dfdv_array[k]) = &m_dummy_block;
			}
		}
	}

}

void SemiImplicit::addForce(sp<Force> a_force, bool a_add_damping)
{
	if(a_add_damping)
	{
		m_forces_add_damping.push_back(a_force);
	}
	else
	{
		m_forces_as_is.push_back(a_force);
	}
}

#if 0
void SemiImplicit::reorder(std::vector<unsigned int> &a_order,  SemiImplicit::t_pairs &a_pairs)
{
#ifdef FE_ENABLE_SI_BANDWIDTH_NARROWING
	using namespace boost;
	using namespace std;

	std::vector<Particle> particles;
	particles.resize(m_n_sim);
	for(unsigned int i = 0; i < m_n_sim; i++)
	{
		particles[i] = m_particles[i];
	}

	typedef adjacency_list<vecS, vecS, undirectedS,
		property<vertex_color_t, default_color_type,
		property<vertex_degree_t,int> > > t_graph;
	typedef graph_traits<t_graph>::vertex_descriptor t_vertex;
	typedef graph_traits<t_graph>::vertices_size_type t_size_type;

	t_graph G(m_n_sim);

	for(unsigned int i = 0; i < a_pairs.size(); i++)
	{
		add_edge(a_pairs[i].first, a_pairs[i].second, G);
	}

	graph_traits<t_graph>::vertex_iterator ui, ui_end;

	property_map<t_graph,vertex_degree_t>::type deg = get(vertex_degree, G);
	for (boost::tie(ui, ui_end) = vertices(G); ui != ui_end; ++ui)
		deg[*ui] = degree(*ui, G);

	property_map<t_graph, vertex_index_t>::type
		index_map = get(vertex_index, G);

//fe_fprintf(stderr, "original bandwidth: %d\n", bandwidth(G));

	std::vector<t_vertex> inv_perm(num_vertices(G));
	std::vector<t_size_type> perm(num_vertices(G));
	{
		cuthill_mckee_ordering(G, inv_perm.rbegin(), get(vertex_color, G),
			make_degree_map(G));

		a_order.resize(num_vertices(G));
		unsigned int a = 0;
		//cout << "Reverse Cuthill-McKee ordering:" << endl;
		//cout << "  ";
		for (std::vector<t_vertex>::const_iterator i=inv_perm.begin();
			i != inv_perm.end(); ++i)
		{
			//cout << index_map[*i] << " ";
			m_particles[a] = particles[index_map[*i]];
//fe_fprintf(stderr, "new %d <- old %d\n", a, index_map[*i]);
			a_order[index_map[*i]] = a;
			a++;
		}
		//cout << endl;

#if 0
		for (t_size_type c = 0; c != inv_perm.size(); ++c)
		{
			perm[index_map[inv_perm[c]]] = c;
		}
#endif

#if 0
		fe_fprintf(stderr, "bandwidth: %d\n",
			bandwidth(G,
				make_iterator_property_map(&perm[0], index_map, perm[0])));
#endif
	}
#endif
}
#endif

#ifdef FE_ENABLE_SI_BANDWIDTH_NARROWING
void SemiImplicit::reorder(std::vector<unsigned int> &a_order,  SemiImplicit::t_pairs &a_pairs)
{
	std::vector<Particle> particles;
	particles.resize(m_n_sim);
	for(unsigned int i = 0; i < m_n_sim; i++)
	{
		particles[i] = m_particles[i];
	}

	{
		int *xadj = new int [m_n_sim+1];
		int *adj = new int [a_pairs.size()*2];
		int *perm = new int [m_n_sim];
		int *deg = new int [m_n_sim];
		signed char *mask = new signed char [m_n_sim];

		std::vector< std::set<int> > adj_assembly;
		adj_assembly.resize(m_n_sim);

		for(unsigned int i = 0; i < m_n_sim; i++)
		{
			perm[i] = i;
			deg[i] = 10;
			mask[i] = 0;
		}

		for(unsigned int i = 0; i < a_pairs.size(); i++)
		{
			adj_assembly[a_pairs[i].first].insert(a_pairs[i].second);
			adj_assembly[a_pairs[i].second].insert(a_pairs[i].first);
		}

		int idx = 0;
		for(unsigned int i = 0; i < adj_assembly.size(); i++)
		{
			xadj[i] = idx;
			for(std::set<int>::iterator i_set = adj_assembly[i].begin();
				i_set != adj_assembly[i].end(); i_set++)
			{
				adj[idx] = *i_set;
				idx++;
			}
		}
		xadj[m_n_sim] = idx;

		genrcmi((int)m_n_sim, RCM_INSERTION_SORT, xadj, adj, perm, mask, deg);

		a_order.resize(m_n_sim);
		for(unsigned int i = 0; i < m_n_sim; i++)
		{
			m_particles[i] = particles[perm[i]];
			a_order[perm[i]] = i;
		}

		delete [] xadj;
		delete [] adj;
		delete [] perm;
		delete [] deg;
		delete [] mask;

	}
}
#else
void SemiImplicit::reorder(std::vector<unsigned int> &,  SemiImplicit::t_pairs &)
{
}
#endif

bool SemiImplicit::lookupIndex(unsigned int &a_particle, Record &r_particle)
{
//fe_fprintf(stderr, "0x%x lookup %d\n", this, r_particle.idr());

	std::map<FE_UWORD, unsigned int>::iterator i_particle = m_recordToParticle.find(r_particle.idr());

	if(i_particle == m_recordToParticle.end())
	{
//fe_fprintf(stderr, "fail %d\n", m_recordToParticle.size());
		return false;
	}

	a_particle = i_particle->second;
	return true;
}


/* CompileMatrix =========================================================== */

SemiImplicit::CompileMatrix::CompileMatrix(void)
{
}

SemiImplicit::CompileMatrix::~CompileMatrix(void)
{
}

void SemiImplicit::CompileMatrix::clear(void)
{
	m_rows.clear();
}

void SemiImplicit::CompileMatrix::setRows(unsigned int a_count)
{
	m_rows.resize(a_count);
}

unsigned int SemiImplicit::CompileMatrix::rows(void)
{
	return (unsigned int)m_rows.size();
}

SemiImplicit::CompileMatrix::t_row &SemiImplicit::CompileMatrix::row(unsigned int a_index)
{
	return m_rows[a_index];
}

SemiImplicit::CompileMatrix::t_entry &SemiImplicit::CompileMatrix::entry(unsigned int a_i, unsigned int a_j)
{
	t_row::iterator i_row = m_rows[a_i].find(a_j);
	if(i_row != m_rows[a_i].end())
	{
		return i_row->second;
	}
	t_entry &v = m_rows[a_i][a_j];
	return v;
}

void SemiImplicit::CompileMatrix::symbolicFill(void)
{
	unsigned int n = (unsigned int)m_rows.size();

	t_nonzero_pattern A(n);
	t_nonzero_pattern L(n);
	t_nonzero_set t;


	for(unsigned int i = 0; i < n; i++)
	{
		for(t_row::iterator i_row = m_rows[i].begin();
			i_row != m_rows[i].end(); i_row++)
		{
			A[i].insert(i_row->first);
			//A[i_row->first].insert(i);
		}
	}

	std::vector<unsigned int> parent(m_rows.size());
	for(unsigned int i = 0; i < n; i++) { parent[i] = n; }

	for(unsigned int i = 0; i < n; i++)
	{
		L[i] = A[i];
		for(unsigned int j = 0; j < n; j++)
		{
			if(parent[j] == i)
			{
				L[i].insert(L[j].begin(), L[j].end());
				L[i].erase(j);
			}

		}
		t = L[i];
		t.erase(i);
		t_nonzero_set::iterator i_s = t.begin();
#if 0
		parent[i] = n;
		for(;i_s != t.end(); i_s++)
		{
			if(*i_s < n && *i_s != i)
			{
				parent[i] = *i_s;
			}
		}
#endif
		if(i_s != t.end())
		{
			parent[i] = *i_s;
			assert(parent[i] != i);
		}
	}

	for(unsigned int i = 0; i < n; i++)
	{
		for(t_nonzero_set::iterator i_s = L[i].begin();
			i_s != L[i].end(); i_s++)
		{
			unsigned int j = *i_s;
			entry(i, j);
		}
		for(unsigned int j = 0; j < n; j++)
		{
			//if(j >= i) { entry(i, j); }
		}
	}
}


} /* namespace */
} /* namespace */

