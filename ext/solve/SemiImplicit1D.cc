/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "solve.pmh"
#include "SemiImplicit1D.h"
#include <algorithm>

namespace fe
{
namespace ext
{

SemiImplicit1D::SemiImplicit1D(void)
{
	m_dxImplicitness = 1.0;
	m_dvImplicitness = 1.0;
	m_dv2dxRatio = 1.0;
	m_subdivcnt = 0;
	m_subdivsz = 10;
	m_subdivmult = 1.1;
	m_rayleigh_damping = false;
	m_rayleigh_stiffness = 0.00001;
	m_rayleigh_mass = -1000.0;
	//m_rayleigh_stiffness = 1.0e1;
	m_rayleigh_mass = -100.0;
	//m_rayleigh_mass = 0.0;
}

SemiImplicit1D::~SemiImplicit1D(void)
{
}

void SemiImplicit1D::reset(void)
{
	for(unsigned int i = 0; i < m_particles.size(); i++)
	{
		m_particles[i].m_velocity = 0.0;
		m_particles[i].m_location = 0.0;
		m_particles[i].m_prev_velocity = 0.0;
		m_particles[i].m_prev_location = 0.0;
		m_particles[i].m_adjustment = 0.0;
	}
}

//#define TICKER
void SemiImplicit1D::step(t_solve_real a_timestep, t_solve_real &a_totalConstraintForce)
{
	// assumes m_dfdx, m_dfdv, and m_lhs are topologically identical

	/* Clear --------------------------------------------------------------- */
	unsigned int c = (unsigned int)m_lhs->blocks().size();
	for(unsigned int i = 0; i < c; i++)
	{
		m_lhs->blocks()[i] = 0.0;
		m_dfdx->blocks()[i] = 0.0;
		m_dfdv->blocks()[i] = 0.0;
	}
	t_solve_real h = a_timestep;
	t_solve_real h_sqr = h * h;
	for(unsigned int i = 0; i < m_particles.size(); i++)
	{
		m_particles[i].m_force = 0.0;
		m_particles[i].m_force_external = 0.0;
		m_particles[i].m_force_weak = 0.0;
	}

	/* Accumulate ---------------------------------------------------------- */
	for(unsigned int i = 0; i < m_forces_add_damping.size(); i++)
	{
		m_forces_add_damping[i]->setTimestep(h);
		m_forces_add_damping[i]->accumulate();
	}

	if(m_rayleigh_damping)
	{
		for(unsigned int i = 0; i < m_n_sim; i++)
		{
			m_dv[i] = m_particles[i].m_velocity;
		}
		m_dfdx->multiply(m_tmp, m_dv);
		for(unsigned int i = 0; i < m_n_sim; i++)
		{
			m_particles[i].m_force += m_rayleigh_stiffness * m_tmp[i];
			m_particles[i].m_force += m_rayleigh_mass * m_int(m_particles[i].m_mass, m_particles[i].m_ratio) * m_dv[i];
		}
		for(unsigned int i = 0; i < m_n_sim; i++)
		{
			unsigned int i_block = m_dfdv->rowptr()[i];
			m_dfdv->blocks()[i_block] = m_dfdv->blocks()[i_block] +
				m_rayleigh_mass * m_int(m_particles[i].m_mass, m_particles[i].m_ratio);
		}
		for(unsigned int i = 0; i < m_dfdx->blocks().size(); i++)
		{
			m_dfdv->blocks()[i] = m_dfdv->blocks()[i] + m_rayleigh_stiffness*m_dfdx->blocks()[i];
		}
	}

	/* Accumulate ---------------------------------------------------------- */
	for(unsigned int i = 0; i < m_forces_as_is.size(); i++)
	{
		m_forces_as_is[i]->setTimestep(h);
		m_forces_as_is[i]->accumulate();
	}


	/* LHS ----------------------------------------------------------------- */
	t_solve_real dx_dv_impl = m_dxImplicitness * m_dvImplicitness;
	for(unsigned int i = 0; i < m_n_sim; i++)
	{

		for(unsigned int i_block = m_lhs->rowptr()[i];
			i_block < m_lhs->rowptr()[i+1]; i_block++)
		{
			unsigned int j = m_lhs->colind()[i_block];
			assert(j >= i); // assert upper triangular

			t_solve_real &lhs = m_lhs->blocks()[i_block];
			t_solve_real &dfdx = m_dfdx->blocks()[i_block];
			t_solve_real &dfdv = m_dfdv->blocks()[i_block];

			// -h * dv_impl * df/dv
			lhs = -h * m_dvImplicitness * dfdv;

			// -h^2 * dv_impl * dx_impl * df/dx
			lhs = lhs - (h_sqr * dx_dv_impl * dfdx);

			// M (on diagonal only) and block jacobi preconditioner
			if(i == j)
			{
				t_solve_real mass = m_int(m_particles[i].m_mass, m_particles[i].m_ratio);
				if(mass != 0.0)
				{
					lhs += mass;
				}
			}
		}
	}


	/* RHS (phase 1) ------------------------------------------------------- */
	for(unsigned int i = 0; i < m_n_sim; i++)
	{
		m_dv[i] = 0.0;
	}

	/* Plugin Constraints -------------------------------------------------- */
	for(unsigned int i = 0; i < m_n_sim; i++)
	{
		m_perturb[i] = 0.0;
	}

	/* RHS (phase 2) ------------------------------------------------------- */
	for(unsigned int i = 0; i < m_n_sim; i++)
	{
		m_rhs[i] = 0.0;
		m_tmp[i] = h_sqr * m_dvImplicitness * m_particles[i].m_velocity;
	}
	m_dfdx->multiply(m_rhs, m_tmp);
	for(unsigned int i = 0; i < m_n_sim; i++)
	{
		m_tmp[i] = h * m_perturb[i];
	}
	m_dfdx->multiply(m_tmp, m_tmp);

	for(unsigned int i = 0; i < m_n_sim; i++)
	{
		t_solve_real &f0 = m_particles[i].m_force;
		m_rhs[i] += h * f0 + m_tmp[i];
	}

	m_lhs->incompleteSqrt();
	m_lhs->backSolve(m_dv, m_rhs);

	// apply solve
	//t_solve_real total_mass = 0.0;
	for(unsigned int i = 0; i < m_n_sim; i++)
	{

//#define EXPLICIT
#ifdef EXPLICIT
		m_dv[i] = 0.0;
		m_particles[i].m_force_weak += m_particles[i].m_force;
#endif
		// weak force
		t_solve_real weak_dv = 0.0;
		if(m_int(m_particles[i].m_mass, m_particles[i].m_ratio) > 0.0)
		{
			weak_dv = h * m_particles[i].m_force_weak / (m_int(m_particles[i].m_mass, m_particles[i].m_ratio));
		}


		t_solve_real v0 = m_particles[i].m_velocity;
		t_solve_real v1 = v0 + m_dv[i] + weak_dv;

		t_solve_real x1 = m_particles[i].m_location +
			h * (v0 + m_dv2dxRatio * m_dv[i] + weak_dv);

		m_particles[i].m_prev_velocity = m_particles[i].m_velocity;
		m_particles[i].m_prev_location = m_particles[i].m_location;
		m_particles[i].m_velocity = v1;
		m_particles[i].m_location = x1 + m_perturb[i];

		m_particles[i].m_location_ratio += (m_particles[i].m_location-m_particles[i].m_prev_location) * m_particles[i].m_ratio;

		//total_mass += m_int(m_particles[i].m_mass, m_particles[i].m_ratio);

		m_particles[i].m_constraint_force = 0.0;
		m_particles[i].m_adjustment = 0.0;
	}
	a_totalConstraintForce = 0.0;

}

void SemiImplicit1D::extract(sp<RecordGroup> rg_output)
{
	for(RecordGroup::iterator i_rg = rg_output->begin();
		i_rg != rg_output->end(); i_rg++)
	{
		sp<RecordArray> spRA = *i_rg;
		sp<Scope> spScope = spRA->layout()->scope();
		m_asSolverParticle.bind(spScope);
		m_asParticle.bind(spScope);
#ifdef PARTICLE_DEBUG
		m_asColored.bind(spScope);
#endif
		if(m_asSolverParticle.check(spRA))
		{
#ifdef PARTICLE_DEBUG
			bool do_color = m_asColored.check(spRA);
#endif
			for(int i = 0; i < spRA->length(); i++)
			{
				Record r_particle = spRA->getRecord(i);
				const unsigned int &index= m_asSolverParticle.index(r_particle);

				m_asParticle.location(r_particle) = m_particles[index].m_location;
				m_asParticle.velocity(r_particle) = m_particles[index].m_velocity;
				m_asParticle.force(r_particle) = m_particles[index].m_force;
fe_fprintf(stderr, "  EXTRACT %g %g %g\n", m_asParticle.location(r_particle), m_asParticle.velocity(r_particle), m_asParticle.force(r_particle));
#ifdef PARTICLE_DEBUG
				if(do_color)
				{
					m_asColored.color(r_particle) = m_particles[index].m_color;
				}
#endif
			}
		}
	}
}

void SemiImplicit1D::initialize(sp<Scope> a_spScope)
{
	m_asForcePoint.bind(a_spScope);
	m_asSolverParticle.bind(a_spScope);
	m_asForcePoint.enforceHaving(m_asSolverParticle);
}

void SemiImplicit1D::compile(sp<RecordGroup> rg_input)
{

	m_n = 0;
	m_n_sim = 0;
	/* Particles ----------------------------------------------------------- */
	m_recordToParticle.clear();
	m_particles.clear();
	unsigned int index = 0;


	/* Pre Compile --------------------------------------------------------- */
	/*	The purpose of this stage is for forces to have the opportunity
		to manipulate the input, such as adding particles or constraints.
		*/
	for(unsigned int i = 0; i < m_forces_add_damping.size(); i++)
	{
fe_fprintf(stderr, "pre compile iV %d %d\n", i, m_forces_add_damping[i].isValid());
		m_forces_add_damping[i]->precompile(rg_input);
	}
	for(unsigned int i = 0; i < m_forces_as_is.size(); i++)
	{
		m_forces_as_is[i]->precompile(rg_input);
	}


	// pass for actual particles
	for(RecordGroup::iterator i_rg = rg_input->begin();
			i_rg != rg_input->end(); i_rg++)
	{
		sp<RecordArray> spRA = *i_rg;
		sp<Scope> spScope = spRA->layout()->scope();
		m_asParticle.bind(spScope);
		m_asForcePoint.bind(spScope);
		m_asSolverParticle.bind(spScope);
#ifdef PARTICLE_DEBUG
		m_asColored.bind(spScope);
#endif

		m_asForcePoint.enforceHaving(m_asSolverParticle);
		if(m_asForcePoint.check(spRA) && m_asParticle.check(spRA))
		{
			for(int i = 0; i < spRA->length(); i++)
			{
				Record r_particle = spRA->getRecord(i);

				m_recordToParticle[r_particle.idr()] = (unsigned int)m_particles.size();

				m_particles.resize(m_particles.size()+1);
				Particle &particle = m_particles.back();

				particle.m_location = m_asForcePoint.location(r_particle);
				particle.m_prev_location = m_asForcePoint.location(r_particle);
				particle.m_velocity = m_asForcePoint.velocity(r_particle);
				particle.m_force = 0.0;
				particle.m_constraint_force = 0.0;
#ifdef PARTICLE_DEBUG
				if(m_asColored.check(r_particle))
				{
					particle.m_color = m_asColored.color(r_particle);
				}
#endif

				particle.m_mass = m_asParticle.mass(r_particle);
				particle.m_ratio = 1.0;
				particle.m_location_ratio = particle.m_location;
				particle.m_adjustable = true;
				particle.m_shift_root = false;
				particle.m_adjustment = 0.0;

				m_asSolverParticle.index(r_particle) = index;
				index++;
			}
		}
	}

	m_n_sim = (unsigned int)m_particles.size();

	// pass for constraint particles
	for(RecordGroup::iterator i_rg = rg_input->begin();
			i_rg != rg_input->end(); i_rg++)
	{
		sp<RecordArray> spRA = *i_rg;
		sp<Scope> spScope = spRA->layout()->scope();
		m_asParticle.bind(spScope);
		m_asForcePoint.bind(spScope);
		m_asSolverParticle.bind(spScope);
#ifdef PARTICLE_DEBUG
		m_asColored.bind(spScope);
#endif

		m_asForcePoint.enforceHaving(m_asSolverParticle);
		if(m_asForcePoint.check(spRA) && !m_asParticle.check(spRA))
		{
			for(int i = 0; i < spRA->length(); i++)
			{
				Record r_particle = spRA->getRecord(i);

				m_recordToParticle[r_particle.idr()] = (unsigned int)m_particles.size();

				m_particles.resize(m_particles.size()+1);
				Particle &particle = m_particles.back();

				particle.m_location = m_asForcePoint.location(r_particle);
				particle.m_prev_location = m_asForcePoint.location(r_particle);
				particle.m_velocity = m_asForcePoint.velocity(r_particle);
				particle.m_force = 0.0;
				particle.m_constraint_force = 0.0;
#ifdef PARTICLE_DEBUG
				particle.m_color = m_asColored.color(r_particle);
#endif

				particle.m_mass = 0.0;
				particle.m_ratio = 1.0;
				particle.m_location_ratio = particle.m_location;
				particle.m_adjustable = true;
				particle.m_adjustment = 0.0;

				m_asSolverParticle.index(r_particle) = index;
				index++;
			}
		}
	}


#ifdef USE_FILTERS
	m_n_sim = (unsigned int)m_particles.size();
#endif
	m_n = (unsigned int)m_particles.size();
	m_rhs.resize(m_n_sim);
	m_dv.resize(m_n_sim);
	m_tmp.resize(m_n_sim);
	m_perturb.resize(m_n_sim);



	/* Express Connectivity ------------------------------------------------ */
	/*	The purpose of this stage is for forces to express connectivity so
		that reordering has something to work with.

		Altering rg_input at this point is precarious.
		*/
//FILE *fp = fopen("neighbor.txt", "a");
	t_pairs pairs;
	//t_ratio_specs ratio_specs;
	for(unsigned int i = 0; i < m_forces_add_damping.size(); i++)
	{
		//m_forces_add_damping[i]->pairs(rg_input, pairs, ratio_specs);
		m_forces_add_damping[i]->pairs(rg_input, pairs);
	}
	for(unsigned int i = 0; i < m_forces_as_is.size(); i++)
	{
		//m_forces_as_is[i]->pairs(rg_input, pairs, ratio_specs);
		m_forces_as_is[i]->pairs(rg_input, pairs);
	}

	t_neighbors neighbors(m_n);
	std::vector<bool> mark(m_n);
	for(unsigned int i = 0; i < pairs.size(); i++)
	{
		neighbors[pairs[i]->first].push_back(pairs[i]->second);
		neighbors[pairs[i]->second].push_back(pairs[i]->first);
	}

	for(unsigned int i = 0; i < m_n; i++)
	{
		mark[i] = false;
	}

	for(unsigned int i = 0; i < m_n; i++)
	{
		if(!mark[i])
		{
			std::vector<unsigned int> queue;
			queue.push_back(i);
			unsigned int s = (unsigned int)m_shift.size();
			m_shift.resize(s+1);

			while(queue.size() > 0)
			{
				unsigned int k = queue[queue.size()-1];
				queue.pop_back();
				if(mark[k])
				{
					continue;
				}
				mark[k] = true;
				m_shift[s].push_back(k);
//TODO: this is just for debugging? :
//m_particles[k].m_shift_group = s;
				for(unsigned j = 0; j < neighbors[k].size(); j++)
				{
					if(!mark[neighbors[k][j]])
					{
						queue.push_back(neighbors[k][j]);
					}
				}
			}
		}
	}

//#define RATIO_GROUPS_APPROACH
#ifdef RATIO_GROUPS_APPROACH
	for(t_ratio_specs::iterator i_ratio_specs = ratio_specs.begin();
		i_ratio_specs != ratio_specs.end(); i_ratio_specs++)
	{
		std::vector<unsigned int> mark_mass(m_n);
		std::vector< std::vector<t_size> > i_mass(m_n);
		for(unsigned int i = 0; i < pairs.size(); i++)
		{
			i_mass[pairs[i]->first].push_back(i);
			i_mass[pairs[i]->second].push_back(i);
		}
		for(unsigned int i = 0; i < m_n; i++)
		{
			mark_mass[i] = 0;
		}

		std::vector<t_size> ratio_mass;
		std::vector<t_size> mass_q;
		for(unsigned int i = 0; i < i_ratio_specs->second.m_ratio_particles.size(); i++)
		{
			mass_q.push_back(i_ratio_specs->second.m_ratio_particles[i]);
			mark_mass[i_ratio_specs->second.m_ratio_particles[i]] = 1;
		}
		for(unsigned int i = 0; i < i_ratio_specs->second.m_block_particles.size(); i++)
		{
			mark_mass[i_ratio_specs->second.m_block_particles[i]] = 1;
		}

		while(mass_q.size() > 0)
		{
			unsigned int i_gb = mass_q.back();
			mass_q.pop_back();
			ratio_mass.push_back(i_gb);
			for(unsigned int i = 0; i < i_mass[i_gb].size(); i++)
			{
				unsigned int i_pair = i_mass[i_gb][i];
				if(pairs[i_pair]->first != i_gb)
				{
					if(!mark_mass[pairs[i_pair]->first])
					{
						mark_mass[pairs[i_pair]->first] = 1;
						mass_q.push_back(pairs[i_pair]->first);
					}
				}
				if(pairs[i_pair]->second != i_gb)
				{
					if(!mark_mass[pairs[i_pair]->second])
					{
						mark_mass[pairs[i_pair]->second] = 1;
						mass_q.push_back(pairs[i_pair]->second);
					}
				}
			}
		}

		for(unsigned int i = 0; i < ratio_mass.size(); i++)
		{
			m_ratio_groups[i_ratio_specs->first].m_particles.push_back(ratio_mass[i]);
			m_ratio_groups[i_ratio_specs->first].m_ratio = 1.0;
		}
	}

	for( std::map< String, RatioInfo >::iterator i_r_g = m_ratio_groups.begin();
		i_r_g != m_ratio_groups.end(); i_r_g++)
	{
		RatioInfo &ratio_info = i_r_g->second;
		fe_fprintf(stderr, "m_ratio_groups [%s] %g\n", i_r_g->first.c_str(), ratio_info.m_ratio);
		for(unsigned int i = 0; i < ratio_info.m_particles.size(); i++)
		{
			fe_fprintf(stderr, "   %d\n", ratio_info.m_particles[i]);
		}
	}
#else
	for(unsigned int i = 0; i < pairs.size(); i++)
	{
		m_particles[pairs[i]->first].m_pairs.push_back(pairs[i]);
		m_particles[pairs[i]->second].m_pairs.push_back(pairs[i]);
	}


#endif

#ifdef FE_ENABLE_SI_BANDWIDTH_NARROWING
// This seems pretty broken right now due to not keeping up to date
// mainly keeping it here for reference if we decide to re-do it
// old_todo: turn this back on (and reimplement to not reorder constraints)
	/* Reorder ------------------------------------------------------------- */
	t_pairs dynamic_only_pairs;
	for(unsigned int i = 0; i < pairs.size(); i++)
	{
		if(pairs[i]->first >= m_n_sim) { continue; }
		if(pairs[i]->second >= m_n_sim) { continue; }
		dynamic_only_pairs.push_back(pairs[i]);
	}
	std::vector<unsigned int> order;
	reorder(order, dynamic_only_pairs);
	for(RecordGroup::iterator i_rg = rg_input->begin();
			i_rg != rg_input->end(); i_rg++)
	{
		sp<RecordArray> spRA = *i_rg;
		sp<Scope> spScope = spRA->layout()->scope();
		m_asParticle.bind(spScope);
		m_asForcePoint.bind(spScope);
		m_asSolverParticle.bind(spScope);

		if(m_asForcePoint.check(spRA))
		{
			for(int i = 0; i < spRA->length(); i++)
			{
				Record r_particle = spRA->getRecord(i);

				if(m_asSolverParticle.index(r_particle) < (int)m_n_sim)
				{
					m_asSolverParticle.index(r_particle) =
						order[m_asSolverParticle.index(r_particle)];
				}
			}
		}
	}
#endif


	/* Matrix Create ------------------------------------------------------- */
	CompileMatrix compileMatrix;
	compileMatrix.setRows(m_n);

	/* Forces -------------------------------------------------------------- */
	for(unsigned int i = 0; i < m_forces_add_damping.size(); i++)
	{
		m_forces_add_damping[i]->compile(rg_input, m_particles, compileMatrix);
	}
	for(unsigned int i = 0; i < m_forces_as_is.size(); i++)
	{
		m_forces_as_is[i]->compile(rg_input, m_particles, compileMatrix);
	}

	/* Matrix -------------------------------------------------------------- */
	m_lhs = new UpperTriangularBCRS1<t_solve_real>();
	m_dfdx = new UpperTriangularBCRS1<t_solve_real>();
	m_dfdv = new UpperTriangularBCRS1<t_solve_real>();
	t_solve_real blank;
	// force diagonal to exist
	for(unsigned int i = 0; i < m_n_sim; i++)
	{
		compileMatrix.entry(i,i);
	}

	compileMatrix.symbolicFill();

	for(unsigned int i = 0; i < m_n_sim; i++)
	{
		m_dfdx->startrow();
		m_dfdv->startrow();
		m_lhs->startrow();

		SemiImplicit1D::CompileMatrix::t_row &row = compileMatrix.row(i);

		for(SemiImplicit1D::CompileMatrix::t_row::iterator i_row = row.begin();
			i_row != row.end(); i_row++)
		{
			unsigned int j = (*i_row).first;
			assert(j >= i); // assert upper triangular

			if(j >= m_n_sim) { continue; }

			m_lhs->insert(j, blank);
			m_dfdx->insert(j, blank);
			m_dfdv->insert(j, blank);
		}
	}
	m_lhs->done();
	m_dfdx->done();
	m_dfdv->done();


	for(unsigned int i = 0; i < m_n_sim; i++)
	{
		SemiImplicit1D::CompileMatrix::t_row &row = compileMatrix.row(i);

		unsigned int i_lhs_row = m_lhs->rowptr()[i];

		for(SemiImplicit1D::CompileMatrix::t_row::iterator i_row = row.begin();
			i_row != row.end(); i_row++)
		{
			unsigned int j = (*i_row).first;
			assert(j >= i); // assert upper triangular

			if(j >= m_n_sim)
			{
				SemiImplicit1D::CompileMatrix::t_entry &entry = (*i_row).second;

				SemiImplicit1D::CompileMatrix::t_dfdx_array &dfdx_array = entry.first;
				SemiImplicit1D::CompileMatrix::t_dfdv_array &dfdv_array = entry.second;

				for(unsigned int k = 0; k < dfdx_array.size(); k++)
				{
					*(dfdx_array[k]) = &m_dummy_block;
				}

				for(unsigned int k = 0; k < dfdv_array.size(); k++)
				{
					*(dfdv_array[k]) = &m_dummy_block;
				}
			}
			else
			{
				assert(j == m_lhs->colind()[i_lhs_row]); // assert consistency

				/*t_solve_matrix &lhs_block =*/ m_lhs->blocks()[i_lhs_row];
				t_solve_real &dfdx_block = m_dfdx->blocks()[i_lhs_row];
				t_solve_real &dfdv_block = m_dfdv->blocks()[i_lhs_row];

				i_lhs_row++;

				SemiImplicit1D::CompileMatrix::t_entry &entry = (*i_row).second;

				SemiImplicit1D::CompileMatrix::t_dfdx_array &dfdx_array = entry.first;
				SemiImplicit1D::CompileMatrix::t_dfdv_array &dfdv_array = entry.second;

				for(unsigned int k = 0; k < dfdx_array.size(); k++)
				{
					*(dfdx_array[k]) = &dfdx_block;
				}

				for(unsigned int k = 0; k < dfdv_array.size(); k++)
				{
					*(dfdv_array[k]) = &dfdv_block;
				}
			}
		}
	}

	for(unsigned int i = m_n_sim; i < m_n; i++)
	{
		SemiImplicit1D::CompileMatrix::t_row &row = compileMatrix.row(i);

		for(SemiImplicit1D::CompileMatrix::t_row::iterator i_row = row.begin();
			i_row != row.end(); i_row++)
		{
			assert((*i_row).first >= i); // assert upper triangular

			SemiImplicit1D::CompileMatrix::t_entry &entry = (*i_row).second;

			SemiImplicit1D::CompileMatrix::t_dfdx_array &dfdx_array = entry.first;
			SemiImplicit1D::CompileMatrix::t_dfdv_array &dfdv_array = entry.second;

			for(unsigned int k = 0; k < dfdx_array.size(); k++)
			{
				*(dfdx_array[k]) = &m_dummy_block;
			}

			for(unsigned int k = 0; k < dfdv_array.size(); k++)
			{
				*(dfdv_array[k]) = &m_dummy_block;
			}
		}
	}

	reconfigure();
}

void SemiImplicit1D::addForce(sp<Force> a_force, bool a_add_damping)
{
	if(a_add_damping)
	{
		m_forces_add_damping.push_back(a_force);
	}
	else
	{
		m_forces_as_is.push_back(a_force);
	}
}


void SemiImplicit1D::reorder(std::vector<unsigned int> &,  SemiImplicit1D::t_pairs &)
{
}

bool SemiImplicit1D::lookupIndex(unsigned int &a_particle, Record &r_particle)
{

	std::map<FE_UWORD, unsigned int>::iterator i_particle = m_recordToParticle.find(r_particle.idr());

	if(i_particle == m_recordToParticle.end())
	{
		return false;
	}

	a_particle = i_particle->second;
	return true;
}


/* CompileMatrix =========================================================== */

SemiImplicit1D::CompileMatrix::CompileMatrix(void)
{
}

SemiImplicit1D::CompileMatrix::~CompileMatrix(void)
{
}

void SemiImplicit1D::CompileMatrix::clear(void)
{
	m_rows.clear();
}

void SemiImplicit1D::CompileMatrix::setRows(unsigned int a_count)
{
	m_rows.resize(a_count);
}

unsigned int SemiImplicit1D::CompileMatrix::rows(void)
{
	return (unsigned int)m_rows.size();
}

SemiImplicit1D::CompileMatrix::t_row &SemiImplicit1D::CompileMatrix::row(unsigned int a_index)
{
	return m_rows[a_index];
}

SemiImplicit1D::CompileMatrix::t_entry &SemiImplicit1D::CompileMatrix::entry(unsigned int a_i, unsigned int a_j)
{
	t_row::iterator i_row = m_rows[a_i].find(a_j);
	if(i_row != m_rows[a_i].end())
	{
		return i_row->second;
	}
	t_entry &v = m_rows[a_i][a_j];
	return v;
}

void SemiImplicit1D::CompileMatrix::symbolicFill(void)
{
	unsigned int n = (unsigned int)m_rows.size();

	t_nonzero_pattern A(n);
	t_nonzero_pattern L(n);
	t_nonzero_set t;


	for(unsigned int i = 0; i < n; i++)
	{
		for(t_row::iterator i_row = m_rows[i].begin();
			i_row != m_rows[i].end(); i_row++)
		{
			A[i].insert(i_row->first);
		}
	}

	std::vector<unsigned int> parent(m_rows.size());
	for(unsigned int i = 0; i < n; i++) { parent[i] = n; }

	for(unsigned int i = 0; i < n; i++)
	{
		L[i] = A[i];
		for(unsigned int j = 0; j < n; j++)
		{
			if(parent[j] == i)
			{
				L[i].insert(L[j].begin(), L[j].end());
				L[i].erase(j);
			}

		}
		t = L[i];
		t.erase(i);
		t_nonzero_set::iterator i_s = t.begin();
		if(i_s != t.end())
		{
			parent[i] = *i_s;
			assert(parent[i] != i);
		}
	}

	for(unsigned int i = 0; i < n; i++)
	{
		for(t_nonzero_set::iterator i_s = L[i].begin();
			i_s != L[i].end(); i_s++)
		{
			unsigned int j = *i_s;
			entry(i, j);
		}
		for(unsigned int j = 0; j < n; j++)
		{
			//if(j >= i) { entry(i, j); }
		}
	}
}


t_solve_real gcd(t_solve_real a, t_solve_real b)
{
	if (a < b) return gcd(b, a);

	if (fabs(b) < 1.0e-4)
	{
		return a;
	}
	else
	{
		return (gcd(b, a - floor(a / b) * b));
	}
}

t_solve_real lcm(t_solve_real a, t_solve_real b)
{
	return fabs(a*b) / gcd(a,b);
}


void SemiImplicit1D::shiftDomain(void)
{
//return;
	for(unsigned int i = 0; i < (unsigned int)m_shift.size(); i++)
	{
		t_solve_real m = 1.0 / m_particles[m_shift[i][0]].m_ratio;
		{
			for(unsigned int j = 1; j < (unsigned int)m_shift[i].size(); j++)
			{
				m = lcm(m, 1.0 / m_particles[m_shift[i][j]].m_ratio);
			}
		}
fe_fprintf(stderr, " SHIFT GRP %d   LCM %g\n", i, m);

		//shiftDomain(2.0*fe::pi*m, m_shift[i][0]);
		shiftDomain(2.0*fe::pi*m, i);
	}
}

void SemiImplicit1D::shiftDomain(t_solve_real a_period, unsigned int a_g)
{
	//unsigned int i = m_particles[a_p].m_shift_group;
	unsigned int i = a_g;

	unsigned int a_p = m_shift[i][0];
	t_solve_real period = a_period * m_particles[a_p].m_ratio;

	t_solve_real adj = 0.0;
	if(m_particles[a_p].m_adjustable)
	{
		adj = -m_particles[a_p].m_location_ratio;

		if(period > 0.0)
		{
			int n = (int)(adj / period);
			adj = period*(t_solve_real)n;

			// make it land in first positive period
			if(n > 0) { adj += a_period; }
		}

		adj = adj / m_particles[a_p].m_ratio;
	}

	if(fabs(adj) > 1.0e-6)
	{
		for(unsigned int j = 0; j < (unsigned int)m_shift[i].size(); j++)
		{
			unsigned int p = m_shift[i][j];

			if(m_particles[p].m_adjustable)
			{
				m_particles[p].m_prev_location = m_particles[p].m_prev_location + adj;
				m_particles[p].m_location = m_particles[p].m_location + adj;
				m_particles[p].m_location_ratio = m_particles[p].m_location_ratio + adj*m_particles[p].m_ratio;
				m_particles[p].m_adjustment = adj;
			}
		}
	}
}


void SemiImplicit1D::reconfigure(void)
{
	// allow Forces to reconfigure
	for(unsigned int i = 0; i < m_forces_add_damping.size(); i++)
	{
		m_forces_add_damping[i]->reconfigure();
	}
	for(unsigned int i = 0; i < m_forces_as_is.size(); i++)
	{
		m_forces_as_is[i]->reconfigure();
	}


	// propogate ratios from first particle in each continguous group
	for(unsigned int i = 0; i < (unsigned int)m_shift.size(); i++)
	{
		propogateRatiosFrom(m_shift[i][0]);
	}
}

void SemiImplicit1D::propogateRatiosFrom(unsigned int a_p)
{
	std::vector<bool> mark(m_n);

	for(unsigned int i = 0; i < m_n; i++)
	{
		mark[i] = false;
	}

	std::vector<unsigned int> queue;
	queue.push_back(a_p);

fe_fprintf(stderr, "--------------\n");
	while(queue.size() > 0)
	{
		unsigned int k = queue[queue.size()-1];
		queue.pop_back();

		if(mark[k]) { continue; }

		mark[k] = true;

		Particle &p = m_particles[k];
		for(unsigned j = 0; j < p.m_pairs.size(); j++)
		{
			sp<t_pair> &spPair = p.m_pairs[j];

			if(spPair->first == k)
			{
				if(!mark[spPair->second])
				{
					queue.push_back(spPair->second);
					//fe_fprintf(stderr, "  PAIR forward from %d to %d\n", k, spPair->second);

					t_solve_real old_ratio = m_particles[spPair->second].m_ratio;
					m_particles[spPair->second].m_ratio = p.m_ratio * spPair->m_ratio;
					m_particles[spPair->second].m_velocity *=
						old_ratio / m_particles[spPair->second].m_ratio;
				}
			}
			else if(spPair->second == k)
			{
				if(!mark[spPair->first])
				{
					queue.push_back(spPair->first);
					//fe_fprintf(stderr, "  PAIR backward from %d to %d\n", k, spPair->first);

					t_solve_real old_ratio = m_particles[spPair->first].m_ratio;
					m_particles[spPair->first].m_ratio = p.m_ratio / spPair->m_ratio;
					m_particles[spPair->first].m_velocity *=
						old_ratio / m_particles[spPair->second].m_ratio;
				}
			}
			else
			{
				assert(0); // should not get here
			}
		}
	}


	for(unsigned int i = 0; i < m_particles.size(); i++)
	{
		fe_fprintf(stderr, "  | P %d ratio %g\n", i, m_particles[i].m_ratio);
	}
}


} /* namespace */
} /* namespace */

