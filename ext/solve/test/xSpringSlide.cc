/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "solve/solve.h"

using namespace fe;
using namespace fe::ext;

/******************************************************************************
	Simple one-dimensional spring system

	  r0    r1
	s^^^^x0^^^^x1

	s = fixed constraint
	r = rest length
	x = locations

	Since we only have 2 DOF, we can fit this in the common 3D vectors.
******************************************************************************/

int main(int argc,char** argv,char** envp)
{
	UNIT_START();
	BWORD completed=FALSE;
	U32 frames=(argc>1)? atoi(argv[1])+1: 0;

	try
	{
		typedef Matrix<4,4,Real> Matrix4x4;

//		BiconjugateGradient<Matrix4x4,Vector4> solver;
//		BCGThreaded<Matrix4x4,Vector4> solver;
		ConjugateGradient<Matrix4x4,Vector4> solver;

		const Real s=0.0f;
		const Real k=10.0f;		//* spring
		const Real c=0.1f;		//* drag

		const Vector4 rest(1.0f, 1.0f);
		Vector4 x(1.0f, 3.0f);
		Vector4 v(0.0f, 0.0f);

		Matrix4x4 id;
		setIdentity(id);

		const Matrix4x4 im=id;	//* inverse mass

		SystemTicker ticker("A");
		ticker.log("a");

		Real h=0.1f;
		for(U32 frame=1;frame!=frames;frame++)
		{
			Vector4 f;
			set(f);
			f[0]=s+x[1]-2.0f*x[0]+rest[0]-rest[1];
			f[1]=x[0]-x[1]+rest[1];
			f*=k;
			f-=c*v;

			Matrix4x4 dfdx;
			setAll(dfdx,0.0f);
			dfdx(0,0)= -2.0f;
			dfdx(1,0)= 1.0f;
			dfdx(0,1)= 1.0f;
			dfdx(1,1)= -1.0f;
			dfdx*=k;

			Matrix4x4 dfdv;
			setAll(dfdv,0.0f);
			dfdv(0,0)= -1.0f;
			dfdv(1,1)= -1.0f;
			dfdv*=c;

			const Matrix4x4 A=id-h*im*dfdv-(h*h)*im*dfdx;
			const Vector4 b=h*im*(f+h*dfdx*v);

#if FALSE
			feLog("dfdx\n%s\ndfdv\n%s\nA\n%s\nv %s\nf %s\nx %s\nb %s\n",
					print(dfdx).c_str(),print(dfdv).c_str(),print(A).c_str(),
					print(v).c_str(),print(f).c_str(),
					print(x).c_str(),print(b).c_str());
#endif

			Vector4 dV;
//			solve(dV,A,b);
			solver.solve(dV,A,b);
			UNIT_TEST(equivalent(A*dV,b,1e-3f));

#if FALSE
			feLog("dV %s\nb' %s\n",print(dV).c_str(),print(A*dV).c_str());
#endif

			v+=dV;
			const Vector4 dX=h*v;
			x+=dX;

			const Real min=0.0f;
			const Real max=4.0f;
			const U32 linewidth=50;
			char line[linewidth+1];
			line[linewidth]=0;

			for(U32 m=0;m<linewidth;m++)
			{
				line[m]=' ';
			}

			for(U32 m=0;m<2;m++)
			{
				if(x[m]>min && x[m]<max)
				{
					line[U32((x[m]-min)/(max-min)*linewidth)]='0'+m;
				}
			}

			feLog("%s <%.4f %.4f> <%.4f %.4f>\n",line,x[0],x[1],v[0],v[1]);
		}

		ticker.log("end");

		Vector4 settle(s+rest[0],s+rest[0]+rest[1]);
		BWORD settled=equivalent(x,settle,0.1f);
		UNIT_TEST(settled);

		if(!settled)
		{
			feLog("Not Settled: expected <%s>, got <%s>\n",
					print(settle).c_str(),print(x).c_str());
		}

		completed=TRUE;
	}
	catch(Exception &e)	{ e.log(); }
	catch(...)			{ feLog("uncaught exception\n"); }

	UNIT_TEST(completed);
	UNIT_TRACK(frames+1);
	UNIT_RETURN();
}
