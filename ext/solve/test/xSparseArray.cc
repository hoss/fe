/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "solve/solve.h"

#define PREALLOC	3

using namespace fe;
using namespace fe::ext;

void print_sa(String message,SparseArray<F32>& sa)
{
	feLog("%s size=%d entries=%d\n  %s\n",message.c_str(),
			sa.size(),sa.entries(),print(sa,TRUE).c_str());
	feLog("  %s\n",print(sa).c_str());
}

int main(void)
{
	UNIT_START();
	BWORD completed=FALSE;

	try
	{
		SparseArray<F32>	x(PREALLOC);

		print_sa("INIT:",x);

		x[7]=1.2f;

		print_sa("SET 7:",x);

		x[12]=3.4f;
		x[3]=5.6f;
		x[8]=7.8f;

		print_sa("SET 12 3 8:",x);

		completed=TRUE;
	}
	catch(Exception &e)	{ e.log(); }
	catch(...)			{ feLog("uncaught exception\n"); }

	UNIT_TEST(completed);
	UNIT_TRACK(1);
	UNIT_RETURN();
}
