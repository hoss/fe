/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "solve/solve.h"
#include "window/window.h"
#include "window/WindowEvent.h"
#include "draw/draw.h"

using namespace fe;
using namespace fe::ext;

#define SPARSE	TRUE
#define DETAIL	FALSE
#define CHAIN	FALSE
#define MONO	(!DETAIL)

#if CHAIN
#define LEVELS	300
#define SPLITS	1
#define STICKS	LEVELS
#else
#define LEVELS	5
#define SPLITS	2
#define STICKS	((1<<LEVELS)-1)
#endif

#define DELTA_T	(1.0f/60.0f)

#if SPARSE
typedef DenseVector<F32> VectorN;
typedef SparseMatrix<F32> MatrixN;
#define FIXED_ARG	STICKS
#define SPARSE_ARG	STICKS,STICKS
#else
typedef Vector<STICKS,F32> VectorN;
typedef Matrix<STICKS,STICKS,F32> MatrixN;
#define FIXED_ARG
#define SPARSE_ARG
#endif

#define	BOOST	1.05f	//* about 1 to 4
#define	KINK	2.0f	//* about 1 to 4

// poly alternatives
// gnuplot> plot [x=-1:2] [-1:2] 0, -x, (0.7*x-0.33)**2, 2*(0.7*x-0.33) * 0.7
// gnuplot> plot [x=-1:2] [-1:1] 0, -x, -(0.6*x-0.4)**3,
// -3*(0.6*x-0.4)**2 * 0.6, -6*(0.6*x-0.4) * 0.6*0.6

// gnuplot> plot [x=-2:1] [-1:1] 0, -x, 0.5*(1-tanh(2*x+1)), -1/(cosh(2*x+1)**2)
F32 collision_force(F32 displacement)
{
	return (1.0f/KINK)*(1.0f-tanhf(KINK*displacement+1.0));
}

F32 collision_dfdx(F32 displacement)
{
	F32 ch=coshf(KINK*displacement+1.0);
	return -1.0f/(ch*ch);
}

class Tree: virtual public HandlerI, public Initialize<Tree>
{
	public:
					//* As HandlerI
virtual	void		handle(Record &record);

					Tree(void):
							m_sizeX(1),
							m_sizeY(1),
							m_A(SPARSE_ARG),
							m_him(SPARSE_ARG),
							m_dfdx(SPARSE_ARG),
							m_hdfdx(SPARSE_ARG)
					{	m_pLines=new SpatialVector[STICKS*2]; }

virtual				~Tree(void)
					{	delete[] m_pLines; }

		void		initialize(void)
					{	set(m_effector);
						generate(DELTA_T); }

		void		generate(const F32 deltaT);
		void		update(const F32 deltaT);
		void		draw(sp<DrawI> spDrawI);

	private:

	class Stick
	{
		public:
							Stick(void):
								m_pParent(NULL),
								m_index(0),
								m_mass(1.0f),
								m_length(0.2f),
								m_spring(1e3f),
								m_drag(0.0f),
								m_rest(0.0f),
								m_phi(0.0f),
								m_omega(0.0f),
								m_scalar(0.0f)
							{	set(m_base);
								set(m_contact);
								m_children.setAutoDestruct(TRUE); }

			void			attach(Stick* pChild)
							{
								m_children.append(pChild);
								pChild->m_pParent=this;
							}

			void			grow(const U32 level,U32& index);
			void			populate_static(const Tree& rTree,
									MatrixN& dfdx,MatrixN& dfdv,
									MatrixN& invMass);
			void			populate_dynamic(const Tree& rTree,
									VectorN& force,MatrixN& dfdx2,
									VectorN& velocity,
									VectorN& addPosition,
									VectorN& addVelocity);
			void			forward_kine(const F32 deltaT,
									const VectorN& deltaV,
									const VectorN& addPosition);
			void			draw(SpatialVector* pLines,sp<DrawI> spDrawI);

			Stick*			m_pParent;
			List<Stick*>	m_children;
			U32				m_index;

			SpatialVector	m_base;
			F32				m_mass;
			F32				m_length;
			F32				m_spring;
			F32				m_drag;
			F32				m_rest;
			F32				m_phi;
			F32				m_omega;

							//* debug
			SpatialVector	m_contact;
			SpatialVector	m_contact2;
			F32				m_scalar;
	};

		Stick			m_root;
		Vector<4,Real>	m_effector;
		WindowEvent		m_event;
		I32				m_sizeX;
		I32				m_sizeY;
		SpatialVector*	m_pLines;

		MatrixN			m_A;
		MatrixN			m_him;
		MatrixN			m_dfdx;
		MatrixN			m_hdfdx;

		BiconjugateGradient<MatrixN,VectorN>	m_biconjugateGradient;
};

void Tree::handle(Record &record)
{
	m_event.bind(record);

#if FALSE
	if(!m_event.isIdleMouse() && !m_event.isPoll())
	{
		feLog("%p Tree::handle %s\n",(U32)this,m_event.out(TRUE).c_str());
	}
#endif

	if(m_event.isResize())
	{
		m_sizeX=m_event.state();
		m_sizeY=m_event.state2();

//		feLog("resize %d %d\n",m_sizeX,m_sizeY);
	}

	if(m_event.isMousePress() || m_event.isMouseMotion())
	{
		set(m_effector,
				2.0f*m_event.mouseX()/m_sizeX-1.0f,
				-(2.0f*m_event.mouseY()/m_sizeY-1.0f),
				0.0f,
				m_event.mouseButtons()*0.1f);
	}
	if(m_event.isMouseRelease())
	{
		set(m_effector);
	}
}

void Tree::Stick::grow(const U32 level,U32& index)
{
	m_index=index++;

	if(level+1<LEVELS)
	{
		for(U32 m=0;m<SPLITS;m++)
		{
			Stick* pStick=new Stick();
			attach(pStick);

#if CHAIN
			pStick->m_mass=1.0;
			pStick->m_length=0.01;
			pStick->m_spring=1e4;
			pStick->m_rest=0.01;
#else
			const F32 scale=1.0f/(level+2.0f);
			pStick->m_mass=scale;
			pStick->m_spring*=scale;
			pStick->m_rest=m? 0.5: -0.3;
#endif

			pStick->grow(level+1,index);
		}
	}
}

void Tree::generate(const F32 deltaT)
{
	U32 index=0;
	m_root.grow(0,index);

	FEASSERT(index==STICKS);

	MatrixN invMass(SPARSE_ARG);
	set(invMass);

	MatrixN dfdv(SPARSE_ARG);
	set(dfdv);

	set(m_dfdx);

	MatrixN id(SPARSE_ARG);
	setIdentity(id);

	m_root.populate_static(*this,m_dfdx,dfdv,invMass);

	const F32& h=deltaT;

	MatrixN temp(SPARSE_ARG);
	m_A=id-h*premultiplyDiagonal(temp,invMass,dfdv)
			-(h*h)*premultiplyDiagonal(temp,invMass,m_dfdx);

	m_him=h*invMass;
	m_hdfdx=h*m_dfdx;
}

void Tree::update(const F32 deltaT)
{
	VectorN force(FIXED_ARG);
	set(force);

	MatrixN dfdx2(SPARSE_ARG);	// instantaneous add-ons
	set(dfdx2);

	VectorN velocity(FIXED_ARG);
	set(velocity);

	VectorN addPosition(FIXED_ARG);
	set(addPosition);

	VectorN addVelocity(FIXED_ARG);
	set(addVelocity);

	m_root.populate_dynamic(*this,force,dfdx2,velocity,addPosition,addVelocity);

#if FALSE
	feLog("m_him\n%s\n m_dfdx\n%s\nforce<%s>\nvelocity<%s>\n"
			"addPosition<%s>\naddVelocity<%s>\n",
			print(m_him).c_str(),print(m_dfdx).c_str(),
			print(force).c_str(),print(velocity).c_str(),
			print(addPosition).c_str(),print(addVelocity).c_str());
#endif

	const F32& h=deltaT;

	MatrixN temp(SPARSE_ARG);
	MatrixN A2=m_A-h*premultiplyDiagonal(temp,m_him,dfdx2);
	VectorN b=m_him*(force+(m_hdfdx+h*dfdx2)*velocity+
			(m_dfdx+dfdx2)*addPosition);

	VectorN deltaV;
	m_biconjugateGradient.solve(deltaV,A2,b);

	m_root.forward_kine(deltaT,deltaV+addVelocity,addPosition);
}

void Tree::Stick::populate_static(const Tree& rTree,
		MatrixN& dfdx,MatrixN& dfdv,MatrixN& invMass)
{
	if(m_pParent)
	{
		U32 parent=m_pParent->m_index;

		dfdx(parent,parent)-=m_spring;

		dfdx(m_index,parent)+=m_spring;
		dfdx(parent,m_index)+=m_spring;
	}

	dfdx(m_index,m_index)-=m_spring;
	invMass(m_index,m_index)=1.0f/m_mass;

	Stick* node;
	List<Stick*>::Iterator iterator(m_children);
	while((node= *iterator++)!=NULL)
	{
		node->populate_static(rTree,dfdx,dfdv,invMass);
	}
}

void Tree::Stick::populate_dynamic(const Tree& rTree,VectorN& force,
		MatrixN& dfdx2,VectorN& velocity,
		VectorN& addPosition,VectorN& addVelocity)
{
//	feLog("%.6G (%.6G)\n",m_phi,m_phi/degToRad);

	F32 radius=rTree.m_effector[3];
	SpatialVector effector=rTree.m_effector;

	//* point line-segment distance
	const SpatialVector span(m_length*sinf(m_phi),m_length*cosf(m_phi));
	const SpatialVector toEffect=effector-m_base;
	F32 along=dot(toEffect,span)/(m_length*m_length);
	SpatialVector effect;
	if(along<0.0f)
	{
		m_contact=m_base;
		effect=m_base-effector;
	}
	else if(along>1.0f)
	{
		m_contact=m_base+span;
		effect=m_base+span-effector;
	}
	else
	{
		SpatialVector projection=m_base+along*span;
		m_contact=projection;
		effect=projection-effector;
	}

	F32 parent_phi=0.0f;
	if(m_pParent)
	{
		U32 parent=m_pParent->m_index;

		parent_phi=m_pParent->m_phi;

		force[parent]+=m_spring*(m_phi-m_rest-parent_phi);
	}

	const F32 gravity=10.0f;
	force[m_index]+=gravity*m_mass*sinf(m_phi);

	force[m_index]-=m_spring*(m_phi-m_rest-parent_phi);
	velocity[m_index]=m_omega;

	F32 distance=magnitude(effect);
	F32 boosted=radius*BOOST;
//	if(radius>0.0f && magnitude(toEffect)>1e-3f)
	if(distance>0.0f && distance<boosted)
	{
#if FALSE
		const F32 phiSpring=1e3f;

		m_contact2=m_contact-(1.0f-boosted/distance)*effect;

		SpatialVector toEdge=m_contact2-m_base;
		SpatialVector centerCross=cross3(unit(toEffect),unit(span));
		F32 dotproduct=dot(unit(toEdge),unit(span));
		SpatialVector edgeCross=cross3(unit(toEdge),unit(span));
		F32 phiEffect=acosf(dotproduct*0.999999f);
		if(edgeCross[2]<0.0f)
		{
			phiEffect= -phiEffect;
		}

		//* if not strictly next to, fade based on glancing angle
		F32 fadeScale=1.0f;
		if(along<0.0f)
		{
//			fadeScale=distance/(distance-along*m_length);
			fadeScale=(boosted+along*m_length)/boosted;
		}
		if(along>1.0f)
		{
//			fadeScale=distance/(distance+(along-1.0f)*m_length);
			fadeScale=(boosted-(along-1.0f)*m_length)/boosted;
		}
		F32 spring=phiSpring*fadeScale;

		F32 phiLead=phiEffect;
		if(centerCross[2]<0.0f)
		{
			phiLead= -phiLead;
			spring= -spring;
		}

//		phiLead=maximum(-0.5f,phiLead);

		//* choose the discretion error in our favor (stability)
		BWORD relaxing=(m_omega<0.0f) == (phiEffect>0.0f);
		F32 phiLeadMod=relaxing? phiLead: phiLead+DELTA_T*m_omega;

		F32 cf=spring*collision_force(phiLead);
		F32 cdf=spring*collision_dfdx(phiLeadMod);
		if(FE_INVALID_SCALAR(cf) || FE_INVALID_SCALAR(cdf))
		{
			feX("Tree::Stick::populate_dynamic","invalid collision");
		}

		m_scalar= -1e-2*cf;

		if(m_index==30)
		{
			feLog("%2d ef %6.3f ld %6.3f lm %6.3f om %6.3f"
					" fd %6.3f sp %6.3f fo %6.3f df %6.3f\n",
					m_index,phiEffect,phiLead,phiLeadMod,m_omega,
					fadeScale,spring,cf,cdf);
		}

		force[m_index]-=cf;
		dfdx2(m_index,m_index)-=cdf;

		//* try to account for change in parent angles to our force
		//* TODO if this works, it needs to scale with some real angles
/*
		Stick* pParent=m_pParent;
		if(pParent)
		{
			dfdx2(m_index,pParent->m_index)+=cdf;
			pParent=pParent->m_pParent;
		}
*/

#endif

		/****/

#if TRUE
		effect*=1.0f/distance;

		F32 perpendicular=dot(effect,SpatialVector(cosf(m_phi),-sinf(m_phi)));
		F32 intensity=perpendicular*(1.0f-distance/boosted);

		m_scalar=0.3f*intensity;
		m_contact2=effector;

		//* raw penalty force
		const F32 push=0.0f;	// 100.0f
		force[m_index]+=push*intensity;

		//* for Baraff's Position Alteration
		//* TODO non-arbitrary scale
		addPosition[m_index]= 1.0f*intensity;
		addVelocity[m_index]= -1.0f*m_omega;
#endif
	}
	else
	{
		set(m_contact);
		set(m_contact2);
		m_scalar=0.0f;
	}

	Stick* node;
	List<Stick*>::Iterator iterator(m_children);
	while((node= *iterator++)!=NULL)
	{
		node->populate_dynamic(rTree,force,dfdx2,velocity,
				addPosition,addVelocity);
	}
}

void Tree::Stick::forward_kine(const F32 deltaT,const VectorN& deltaV,
		const VectorN& addPosition)
{
	m_omega+=deltaV[m_index];
	m_phi+=deltaT*m_omega+addPosition[m_index];

	if(FE_INVALID_SCALAR(m_omega))
	{
		feX("Tree::Stick::forward_kine","invalid m_omega");
	}

	SpatialVector tip=m_base+SpatialVector(m_length*sinf(m_phi),
			m_length*cosf(m_phi));

	Stick* node;
	List<Stick*>::Iterator iterator(m_children);
	while((node= *iterator++)!=NULL)
	{
		node->m_base=tip;
		node->forward_kine(deltaT,deltaV,addPosition);
	}
}

void Tree::Stick::draw(SpatialVector* pLines,sp<DrawI> spDrawI)
{
	SpatialVector tip=m_base+SpatialVector(m_length*sinf(m_phi),
			m_length*cosf(m_phi));

	const Color cyan(0.0f,1.0f,1.0f);
	const Color blue(0.0f,0.0f,1.0f);
	const Color green(0.0f,1.0f,0.0f);
	const Color grey(0.4f,0.4f,0.4f);

	pLines[2*m_index]=m_base;
	pLines[2*m_index+1]=tip;

#if DETAIL
	if(fabs(m_scalar)>=1e-2f)
	{
		SpatialVector line[2];
		line[0]=tip;
		line[1]=tip+SpatialVector(m_scalar,0.0f);
		spDrawI->drawLines(line,NULL,2,DrawI::e_discrete,false,&blue);

		line[0]=m_contact;
		line[1]=m_contact2;
		spDrawI->drawLines(line,NULL,2,DrawI::e_discrete,false,&grey);

		spDrawI->drawPoints(&m_contact,NULL,1,false,&cyan);
		spDrawI->drawPoints(&m_contact2,NULL,1,false,&green);

		char text[16];
		sprintf(text," %d",m_index);
		spDrawI->drawAlignedText(tip,text,cyan);
	}
#endif

	Stick* node;
	List<Stick*>::Iterator iterator(m_children);
	while((node= *iterator++)!=NULL)
	{
		node->draw(pLines,spDrawI);
	}
}

void Tree::draw(sp<DrawI> spDrawI)
{
	if(!spDrawI.isValid())
	{
		return;
	}

	const Color black(0.0f,0.0f,0.0f,1.0f);
	const Color grey(0.5f,0.5f,0.5f,1.0f);
	const Color red(1.0f,0.0f,0.0f,1.0f);
	const Color pink(1.0f,0.5f,0.5f);
	const Color point(0.1f,0.1f,0.0f);

	Box2i box;
	set(box,0,0,m_sizeX,m_sizeY);
	spDrawI->view()->setViewport(box);

/*
	String text;
	text.sPrintf("tree");
	spDrawI->drawAlignedText(point,text,pink);
	spDrawI->drawAxes(0.2f);
*/

	m_root.draw(m_pLines,spDrawI);

	const Color yellow(1.0f,1.0f,0.0f);
	spDrawI->drawLines(m_pLines,NULL,2*STICKS,DrawI::e_discrete,false,
			MONO? &black: &yellow);

	const F32 radius=m_effector[3];
	if(radius!=0.0f)
	{
		SpatialVector scale(radius,radius,radius);
		SpatialTransform transform;
		setIdentity(transform);
		translate(transform,m_effector);
		spDrawI->drawCircle(transform,&scale,MONO? grey: red);
	}
}

int main(int argc,char** argv)
{
	UNIT_START();

	BWORD complete=FALSE;
	U32 limit=(argc>1)? atoi(argv[1])+1: 0;

	try
	{
		sp<Master> spMaster(new Master);
		sp<Registry> spRegistry=spMaster->registry();

		Result result=spRegistry->manage("fexNativeWindowDL");
		UNIT_TEST(successful(result));

		result=spRegistry->manage("fexOpenGLDL");
		UNIT_TEST(successful(result));

		{
			sp<WindowI> spWindowI(spRegistry->create("WindowI"));
			sp<SignalerI> spSignalerI(spRegistry->create("SignalerI"));
			sp<ViewI> spViewI(spRegistry->create("ViewI"));
			sp<Scope> spScope(spRegistry->create("Scope"));

			if(!spWindowI.isValid() ||
					!spScope.isValid() || !spSignalerI.isValid())
			{
				feX(argv[0], "can't continue");
			}

			spWindowI->open("Wiggle");

			sp<DrawI> spDrawI(spRegistry->create("DrawI"));
			spDrawI->setView(spViewI);
			spDrawI->view()->setWindow(spWindowI);

#if MONO
			const Color white(1.0f,1.0f,1.0f,1.0f);
			spWindowI->setBackground(white);
#endif

			sp<DrawMode> spWireframe(new DrawMode());
			spWireframe->setDrawStyle(DrawMode::e_wireframe);
			spWireframe->setLineWidth(DETAIL? 1.5f: 3.0f);
			spDrawI->setDrawMode(spWireframe);

			sp<Layout> spBeatLayout=spScope->declare("BEAT");
			spSignalerI->insert(spWindowI->getEventContextI(),spBeatLayout);
			Record beat=spScope->createRecord(spBeatLayout);

			sp<Layout> spEventLayout=
					spScope->lookupLayout(FE_EVENT_LAYOUT);
			UNIT_TEST(spEventLayout.isValid());

			if(spDrawI.isValid())
			{
				spDrawI->drawMode()->setPointSize(4.0f);
			}

			sp<Tree> spTree(Library::create<Tree>("Tree"));
			spSignalerI->insert(spTree,spEventLayout);

			U32 count=0;
			while(++count!=limit)
			{
				if(limit)
				{
//					feLog("Frame %d ------------------------------\n",count);
				}
				spSignalerI->signal(beat);

				spWindowI->makeCurrent();
				spWindowI->clear();

				spTree->update(DELTA_T);
				spTree->draw(spDrawI);

				spWindowI->swapBuffers();

				if(!spDrawI->isDirect())
				{
					//* don't flood a remote connection
					milliSleep(100);
				}
			}
		}

		complete=TRUE;
	}
	catch(Exception &e)	{ e.log(); }
	catch(...)			{ feLog("uncaught exception\n"); }

	UNIT_TEST(complete);
	UNIT_TEST(!Counted::trackerCount());
	UNIT_TRACK(5);
	UNIT_RETURN();
}
