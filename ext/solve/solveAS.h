/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __solve_solveAS_h__
#define __solve_solveAS_h__

namespace fe
{
namespace ext
{

class FE_DL_EXPORT AsBodyPoint :
	public AccessorSet,
	public Initialize<AsBodyPoint>
{
	public:
		void initialize(void)
		{
			add(locator,		FE_USE("bdy:locator0"));
			add(body,			FE_USE("bdy:body"));
			add(offset,			FE_USE("bdy:offset"));
			add(active,			FE_USE("bdy:active"));
		}
		/// locator for authoring a body point
		Accessor<Record>			locator;
		/// body record
		Accessor<WeakRecord>		body;
		/// transform relative to body
		Accessor<SpatialVector>		offset;
		/// active
		Accessor<bool>				active;
};

/// clear signal
class AsClear
	: public AccessorSet, public Initialize<AsClear>
{
	public:
		void initialize(void)
		{
			add( dataset,		FE_USE("group"));
			add( is,			FE_USE("sim:clear"));
			add( dfdx,			FE_USE("sim:dfdx"));
			add( dfdv,			FE_USE("sim:dfdv"));
			add( timestep,		FE_USE("sim:timestep"));
		}
		/// dataset to accumulate force on
		Accessor<sp<RecordGroup> >	dataset;
		Accessor<void>				is;
		Accessor< sp<Component> >	dfdx;
		Accessor< sp<Component> >	dfdv;
		Accessor<Real>				timestep;
};

/// accumulate signal
class AsAccumulate
	: public AccessorSet, public Initialize<AsAccumulate>
{
	public:
		void initialize(void)
		{
			add( dataset,		FE_USE("group"));
			add( is,			FE_USE("sim:accumulate"));
			add( timestep,		FE_USE("sim:timestep"));
			add( dfdx,			FE_USE("sim:dfdx"));
			add( dfdv,			FE_USE("sim:dfdv"));
		}
		/// dataset to accumulate force on
		Accessor<sp<RecordGroup> >	dataset;
		Accessor<void>				is;
		Accessor<Real>				timestep;
		Accessor< sp<Component> >	dfdx;
		Accessor< sp<Component> >	dfdv;
};

/// validate signal
class AsValidate
	: public AccessorSet, public Initialize<AsValidate>
{
	public:
		void initialize(void)
		{
			add( dataset,		FE_USE("group"));
			add( timestep,		FE_USE("sim:timestep"));
			add( valid,			FE_USE("sim:valid"));
		}
		/// dataset to accumulate force on
		Accessor<sp<RecordGroup> >	dataset;
		Accessor<Real>				timestep;
		Accessor<bool>				valid;
};

/// update state signal
class AsUpdate
	: public AccessorSet, public Initialize<AsUpdate>
{
	public:
		void initialize(void)
		{
			add( dataset,		FE_USE("group"));
			add( timestep,		FE_USE("sim:timestep"));
			add( is,			FE_USE("sim:update"));
		}
		Accessor<sp<RecordGroup> >	dataset;
		Accessor<Real>				timestep;
		Accessor<void>				is;
};

class AsSolverParticle
	: public AccessorSet, public Initialize<AsSolverParticle>
{
	public:
		void initialize(void)
		{
			add( index,			FE_USE("sim:index"));
			add( prevVelocity,	FE_USE("sim:prevVelocity"));
			add( prevLocation,	FE_USE("sim:prevLocation"));
			add( externalForce,	FE_USE("sim:externalForce"));
		}
		Accessor<int>				index;
		Accessor<SpatialVector>		prevVelocity;
		Accessor<SpatialVector>		prevLocation;
		Accessor<SpatialVector>		externalForce;
};

class AsSolverParticle1D
	: public AccessorSet, public Initialize<AsSolverParticle1D>
{
	public:
		void initialize(void)
		{
			add( index,			FE_USE("sim:index"));
			add( prevVelocity,	FE_USE("sim:prevVelocity1d"));
			add( prevLocation,	FE_USE("sim:prevLocation1d"));
			add( externalForce,	FE_USE("sim:externalForce1d"));
		}
		Accessor<int>		index;
		Accessor<Real>		prevVelocity;
		Accessor<Real>		prevLocation;
		Accessor<Real>		externalForce;
};


class AsRK2
	: public AccessorSet, public Initialize<AsRK2>
{
	public:
		void initialize(void)
		{
			add( f0,		FE_USE("rk2:forceTemp"));
			add( v0,		FE_USE("rk2:velocityTemp"));
			add( l0,		FE_USE("rk2:locationTemp"));
		}
		Accessor<SpatialVector>		f0;
		Accessor<SpatialVector>		v0;
		Accessor<SpatialVector>		l0;
};

class AsForceFilter
	: public AccessorSet, public Initialize<AsForceFilter>
{
	public:
		void initialize(void)
		{
			add( impulse,	FE_USE("sim:impulse"));
			add( cap,		FE_USE("sim:forceCap"));
			add( bleed,		FE_USE("sim:impulseBleed"));
		}
		Accessor<SpatialVector>		impulse;
		Accessor<Real>				cap;
		Accessor<Real>				bleed;
};

class AsLineConstrained
	: public AccessorSet, public Initialize<AsLineConstrained>
{
	public:
		void initialize(void)
		{
			add( direction,	FE_USE("sim:direction"));
		}
		Accessor<SpatialVector>		direction;
};

class AsPlaneConstrained
	: public AccessorSet, public Initialize<AsPlaneConstrained>
{
	public:
		void initialize(void)
		{
			add( normal,	FE_USE("spc:normal"));
		}
		Accessor<SpatialVector>		normal;
};


} /* namespace ext */
} /* namespace fe */

#endif /* __solve_solveAS_h__ */

