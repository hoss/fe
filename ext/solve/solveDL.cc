/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <solve/solve.pmh>

#include "ExplicitInertial.h"
#include "SemiImplicitInertial.h"
#include "platform/dlCore.cc"

using namespace fe;
using namespace fe::ext;

extern "C"
{

FE_DL_EXPORT void ListDependencies(List<String*>& list)
{
	list.append(new String("fexSignalDL"));
	list.append(new String("fexDataToolDL"));
}

FE_DL_EXPORT Library* CreateLibrary(sp<Master> spMaster)
{
	return CreateSolveLibrary(spMaster);
}

FE_DL_EXPORT void InitializeLibrary(sp<Library> spLibrary)
{
}

}
