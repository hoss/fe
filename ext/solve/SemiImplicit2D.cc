/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "solve.pmh"
#include "SemiImplicit2D.h"
#include <algorithm>

//#define FE_ENABLE_SI_BANDWIDTH_NARROWING

#ifdef FE_ENABLE_SI_BANDWIDTH_NARROWING
#include "math/rcm.h"
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/cuthill_mckee_ordering.hpp>
#include <boost/graph/properties.hpp>
#include <boost/graph/bandwidth.hpp>
#endif


namespace fe
{
namespace ext
{

//*************************************************************************************************
//## void incompleteSqrtOpt( void )
//
//  Description:
//    Custom optimised version of incompleteSqrt, hard coded to 2x2 can be moved to the inline
//		header if ok
//
//*************************************************************************************************
template <typename T>
inline void UpperTriangularBCRS2<T>::incompleteSqrtOpt( void )
{
	unsigned int d;
	const unsigned int n = (unsigned int)(FullBCRS2<T>::m_rowptr.size() - 1);

	t_matrix inv_z;
	t_matrix z;

	for( unsigned int k = 0; k < n - 1; k++ )
	{
		d = FullBCRS2<T>::m_rowptr[k];

		t_matrix &blocksD = FullBCRS2<T>::m_blocks[d];

		//--------------------
		// # just_upper( FullBCRS2<T>::m_blocks[d] );
		//
		// inlined the just_upper
		blocksD( 1, 0 ) = 0.0;
		//--------------------

		//----------------------------------
		// # squareroot( blocksD, blocksD );
		//
		// unrolled and inlined the squareroot call. removed initial copy as args are same mtx

		blocksD( 0, 0 ) = sqrt( blocksD( 0, 0 ) );

		t_real recipZ = 1.0 / blocksD( 0, 0 );

		blocksD( 0, 1 ) *= recipZ;

		blocksD( 1, 1 ) -= blocksD( 0, 1 ) * blocksD( 0, 1 );

		blocksD( 1, 1 )  = sqrt( blocksD( 1, 1 ) );

		z = blocksD;

		//--------------------
		// just_upper( z );
		z( 1, 0 ) = 0.0;
		//--------------------

		//--------------------
		// invert( inv_z, z );
		//
		// unrolled and inlined the invert call
		inv_z( 0, 0 ) =  z( 1, 1 );
		inv_z( 1, 1 ) =  z( 0, 0 );
		inv_z( 0, 1 ) = -z( 0, 1 );
		inv_z( 1, 0 ) = -z( 1, 0 );

		Real det = z( 0, 0 )*z( 1, 1 ) - z( 1, 0 )*z( 0, 1 );

#if !defined(GOLD)
		if( fabs( det ) < fe::tol )
		{
			feX( "attempt to invert singular 2x2 matrix" );
		}
#endif

		inv_z *= 1.0 / det;

		for( unsigned int i = d + 1; i < FullBCRS2<T>::m_rowptr[k + 1]; i++ )
		{
			fe::multiply( z, FullBCRS2<T>::m_blocks[i], inv_z );

			//--------------------
			// setTranspose( z );
			// FullBCRS2<T>::m_blocks[i] = z;

			// transpose and assign in same operation
			t_matrix &transMtx = FullBCRS2<T>::m_blocks[i];

			transMtx(0,0) = z(0,0);
			transMtx(1,1) = z(1,1);
			transMtx(1,0) = z(0,1);
			transMtx(0,1) = z(1,0);
			//--------------------
		}

		for( unsigned int i = d + 1; i < FullBCRS2<T>::m_rowptr[k + 1]; i++ )
		{
			//------------
			// z = transpose( FullBCRS2<T>::m_blocks[i] );
			//
			// unroll and inline transpose
			const t_matrix &mtx = FullBCRS2<T>::m_blocks[i];

			z( 0, 0 ) = mtx( 0, 0 );
			z( 0, 1 ) = mtx( 1, 0 );
			z( 1, 0 ) = mtx( 0, 1 );
			z( 1, 1 ) = mtx( 1, 1 );
			//------------

			const unsigned int h = FullBCRS2<T>::m_colind[i];
			unsigned int g = i;

			t_matrix t;

			for( unsigned int j = FullBCRS2<T>::m_rowptr[h]; j < FullBCRS2<T>::m_rowptr[h + 1]; j++ )
			{
				for( ; g < FullBCRS2<T>::m_rowptr[k + 1] && FullBCRS2<T>::m_colind[g] <= FullBCRS2<T>::m_colind[j]; g++ )
				{
					if( FullBCRS2<T>::m_colind[g] == FullBCRS2<T>::m_colind[j] )
					{
#if 0
						fe::multiply( t, z, FullBCRS2<T>::m_blocks[g] );
						setTranspose( t );
						subtract( FullBCRS2<T>::m_blocks[j], FullBCRS2<T>::m_blocks[j], t );
#endif

#if 1
						const t_matrix &A = z;
						const t_matrix &B = FullBCRS2<T>::m_blocks[g];
						for( unsigned int ii = 0; ii < 2; ii++ )
						{
							for( unsigned int jj = 0; jj < 2; jj++ )
							{
								t_real sum = 0.0;
								sum += A( ii, 0 )*B( 0, jj );
								sum += A( ii, 1 )*B( 1, jj );
								t( ii, jj ) = sum;
							}
						}

						t_matrix &R = FullBCRS2<T>::m_blocks[j];
						R( 0, 0 ) = R( 0, 0 ) - t( 0, 0 );
						R( 0, 1 ) = R( 0, 1 ) - t( 1, 0 );
						R( 1, 0 ) = R( 1, 0 ) - t( 0, 1 );
						R( 1, 1 ) = R( 1, 1 ) - t( 1, 1 );
#endif
					}
				}
			}
		}
	}

	d = FullBCRS2<T>::m_rowptr[n - 1];

	t_matrix tmp;
	just_upper( FullBCRS2<T>::m_blocks[d] );

	//---------
	// squareroot( tmp, FullBCRS2<T>::m_blocks[d] );
	//
	// unrolls/inlined squareroot
	tmp = FullBCRS2<T>::m_blocks[d];

	tmp( 0, 0 ) = sqrt( tmp( 0, 0 ) );

	t_real recipZd = 1.0 / tmp( 0, 0 );

	tmp( 0, 1 ) *= recipZd;
	tmp( 1, 1 ) -= tmp( 0, 1 ) * tmp( 0, 1 );
	tmp( 1, 1 ) = sqrt( tmp( 1, 1 ) );
	//-------------------

	FullBCRS2<T>::m_blocks[d] = tmp;
}


SemiImplicit2D::SemiImplicit2D(void)
{
	m_dxImplicitness = 1.0;
	m_dvImplicitness = 1.0;
	m_dv2dxRatio = 1.0;
	m_ratio = 1.0;
	m_subdivcnt = 0;
	m_subdivsz = 10;
	m_subdivmult = 1.1;
	m_rayleigh_damping = false;
	m_rayleigh_stiffness = 0.00001;
	m_rayleigh_mass = -1000.0;
	m_force_only = false;
}

SemiImplicit2D::~SemiImplicit2D(void)
{
}

void SemiImplicit2D::prestep(bool a_force_only)
{
	//SystemTicker wticker("SemiImplicit2D wholestep");
//#define TICKER
#ifdef TICKER
	SystemTicker ticker("SemiImplicit2D step");
	ticker.log("pre");
#endif

	m_force_only = a_force_only;

	// assumes m_dfdx, m_dfdv, and m_lhs are topologically identical

	/* Clear --------------------------------------------------------------- */
	if(!m_force_only)
	{
		unsigned int c = (unsigned int)m_lhs->blocks().size();
		for(unsigned int i = 0; i < c; i++)
		{
			setAll(m_lhs->blocks()[i], 0.0);
			setAll(m_dfdx->blocks()[i], 0.0);
			setAll(m_dfdv->blocks()[i], 0.0);
		}
		for(unsigned int i = 0; i < m_particles.size(); i++)
		{
			set(m_particles[i].m_force);
			set(m_particles[i].m_force_external);
			set(m_particles[i].m_force_weak);
		}
	}
	else
	{
		for(unsigned int i = 0; i < m_particles.size(); i++)
		{
			set(m_particles[i].m_force);
		}
	}

#ifdef TICKER
	ticker.log("clear");
#endif

	/* Accumulate ---------------------------------------------------------- */
	for(unsigned int i = 0; i < m_forces_add_damping.size(); i++)
	{
		m_forces_add_damping[i]->accumulate();
	}

#ifdef TICKER
	ticker.log("accumulate");
#endif
	if(m_rayleigh_damping)
	{
		if(m_force_only)
		{
		}
		else
		{
			for(unsigned int i = 0; i < m_n_sim; i++)
			{
				m_dv[i] = m_particles[i].m_velocity;
			}
			m_dfdx->multiply(m_tmp, m_dv);
			for(unsigned int i = 0; i < m_n_sim; i++)
			{
				m_particles[i].m_force += m_rayleigh_stiffness * m_tmp[i];
				m_particles[i].m_force += m_rayleigh_mass * m_particles[i].m_mass * m_dv[i];
			}
			t_solve_m2 identity;
			setIdentity(identity);
			for(unsigned int i = 0; i < m_n_sim; i++)
			{
				unsigned int i_block = m_dfdv->rowptr()[i];
				add(m_dfdv->blocks()[i_block], m_dfdv->blocks()[i_block], m_rayleigh_mass * m_particles[i].m_mass * identity);
			}
			for(unsigned int i = 0; i < m_dfdx->blocks().size(); i++)
			{
				add(m_dfdv->blocks()[i],m_dfdv->blocks()[i],m_rayleigh_stiffness*m_dfdx->blocks()[i]);
			}
		}
	}
#ifdef TICKER
	ticker.log("rayleigh");
#endif

	/* Accumulate ---------------------------------------------------------- */
	for(unsigned int i = 0; i < m_forces_as_is.size(); i++)
	{
		m_forces_as_is[i]->accumulate();
	}
#ifdef TICKER
	ticker.log("accumulate 2");
#endif

}

//#define TICKER
void SemiImplicit2D::step(t_solve_real a_timestep, t_solve_v2 &a_totalConstraintForce)
{
	t_solve_real h = a_timestep;
	t_solve_real h_sqr = h * h;

	prestep(false);

	/* LHS ----------------------------------------------------------------- */
	t_solve_real dx_dv_impl = m_dxImplicitness * m_dvImplicitness;
	t_solve_m2 lhs_mat;
	for(unsigned int i = 0; i < m_n_sim; i++)
	{

		for(unsigned int i_block = m_lhs->rowptr()[i];
			i_block < m_lhs->rowptr()[i+1]; i_block++)
		{
			unsigned int j = m_lhs->colind()[i_block];
			assert(j >= i); // assert upper triangular

			t_solve_m2 &lhs = m_lhs->blocks()[i_block];
			t_solve_m2 &dfdx = m_dfdx->blocks()[i_block];
			t_solve_m2 &dfdv = m_dfdv->blocks()[i_block];

			// -h * dv_impl * df/dv
			lhs = -h * m_dvImplicitness * dfdv;

			// -h^2 * dv_impl * dx_impl * df/dx
			subtract(lhs, lhs, h_sqr * dx_dv_impl * dfdx);

			// M (on diagonal only) and block jacobi preconditioner
			if(i == j)
			{
				t_solve_real mass = m_particles[i].m_mass;
				if(mass != 0.0)
				{
					lhs(0,0) += mass;
					lhs(1,1) += mass;
				}
			}
		}
	}

	/* RHS (phase 1) ------------------------------------------------------- */
	for(unsigned int i = 0; i < m_n_sim; i++)
	{
		m_dv[i] = solveZeroVector;
	}

	/* Plugin Constraints -------------------------------------------------- */
	for(unsigned int i = 0; i < m_n_sim; i++)
	{
		m_perturb[i] = solveZeroVector;
	}

	/* RHS (phase 2) ------------------------------------------------------- */
	for(unsigned int i = 0; i < m_n_sim; i++)
	{
		m_rhs[i] = solveZeroVector;
		m_tmp[i] = h_sqr * m_dvImplicitness * m_particles[i].m_velocity;
	}
	m_dfdx->multiply(m_rhs, m_tmp);
	for(unsigned int i = 0; i < m_n_sim; i++)
	{
		m_tmp[i] = h * m_perturb[i];
	}
	m_dfdx->multiply(m_tmp, m_tmp);

	for(unsigned int i = 0; i < m_n_sim; i++)
	{
		t_solve_v2 &f0 = m_particles[i].m_force;
		m_rhs[i] += h * f0 + m_tmp[i];
	}

	//m_lhs->incompleteSqrt();
	m_lhs->incompleteSqrtOpt();
	m_lhs->backSolve(m_dv, m_rhs);

	// apply solve
	t_solve_real total_mass = 0.0;
	for(unsigned int i = 0; i < m_n_sim; i++)
	{

//#define EXPLICIT
#ifdef EXPLICIT
		m_dv[i] = t_solve_v2(0.0,0.0);
		m_particles[i].m_force_weak += m_particles[i].m_force;
#endif
		// weak force
		t_solve_v2 weak_dv(0.0,0.0);
		if(m_particles[i].m_mass > 0.0)
		{
			weak_dv = h * m_particles[i].m_force_weak / m_particles[i].m_mass;
		}


		t_solve_v2 v0 = m_particles[i].m_velocity;
		t_solve_v2 v1 = v0 + m_dv[i] + weak_dv;


		t_solve_v2 x1 = m_particles[i].m_location +
			h * (v0 + m_dv2dxRatio * m_dv[i] + weak_dv);

		m_particles[i].m_prev_velocity = m_particles[i].m_velocity;
		m_particles[i].m_prev_location = m_particles[i].m_location;
		m_particles[i].m_velocity = v1;
		m_particles[i].m_location = x1 + m_perturb[i];

		total_mass += m_particles[i].m_mass;

		m_particles[i].m_constraint_force = solveZeroVector;

	}
	a_totalConstraintForce = solveZeroVector;

}

void SemiImplicit2D::extract(sp<RecordGroup> rg_output)
{
	for(RecordGroup::iterator i_rg = rg_output->begin();
		i_rg != rg_output->end(); i_rg++)
	{
		sp<RecordArray> spRA = *i_rg;
		sp<Scope> spScope = spRA->layout()->scope();
		m_asSolverParticle.bind(spScope);
		m_asParticle.bind(spScope);
#ifdef PARTICLE_DEBUG
		m_asColored.bind(spScope);
#endif
		if(m_asSolverParticle.check(spRA))
		{
			for(int i = 0; i < spRA->length(); i++)
			{
				Record r_particle = spRA->getRecord(i);
				const unsigned int &index= m_asSolverParticle.index(r_particle);

				m_asParticle.location(r_particle)[0] = m_particles[index].m_location[0];
				m_asParticle.location(r_particle)[1] = m_particles[index].m_location[1];
				m_asParticle.location(r_particle)[2] = 0.0;

#ifdef PARTICLE_DEBUG
				m_asColored.color(r_particle) =
					m_particles[index].m_color;
#endif
			}
		}
	}
}

void SemiImplicit2D::initialize(sp<Scope> a_spScope)
{
	m_asForcePoint.bind(a_spScope);
	m_asSolverParticle.bind(a_spScope);
	m_asForcePoint.enforceHaving(m_asSolverParticle);
}

void SemiImplicit2D::compile(sp<RecordGroup> rg_input)
{

	m_n = 0;
	m_n_sim = 0;
	/* Particles ----------------------------------------------------------- */
	m_recordToParticle.clear();
	m_particles.clear();
	unsigned int index = 0;


	/* Pre Compile --------------------------------------------------------- */
	/*	The purpose of this stage is for forces to have the opportunity
		to manipulate the input, such as adding particles or constraints.
		*/
	for(unsigned int i = 0; i < m_forces_add_damping.size(); i++)
	{
		m_forces_add_damping[i]->precompile(rg_input);
	}
	for(unsigned int i = 0; i < m_forces_as_is.size(); i++)
	{
		m_forces_as_is[i]->precompile(rg_input);
	}


	// pass for actual particles
	for(RecordGroup::iterator i_rg = rg_input->begin();
			i_rg != rg_input->end(); i_rg++)
	{
		sp<RecordArray> spRA = *i_rg;
		sp<Scope> spScope = spRA->layout()->scope();
		m_asParticle.bind(spScope);
		m_asForcePoint.bind(spScope);
		m_asSolverParticle.bind(spScope);
#ifdef PARTICLE_DEBUG
		m_asColored.bind(spScope);
#endif

		m_asForcePoint.enforceHaving(m_asSolverParticle);
		if(m_asForcePoint.check(spRA) && m_asParticle.check(spRA))
		{
			for(int i = 0; i < spRA->length(); i++)
			{
				Record r_particle = spRA->getRecord(i);

				m_recordToParticle[r_particle.idr()] = (unsigned int)m_particles.size();

				m_particles.resize(m_particles.size()+1);
				Particle &particle = m_particles.back();

				particle.m_location[0] = m_asForcePoint.location(r_particle)[0];
				particle.m_location[1] = m_asForcePoint.location(r_particle)[1];
				particle.m_velocity[0] = m_asForcePoint.velocity(r_particle)[0];
				particle.m_velocity[1] = m_asForcePoint.velocity(r_particle)[1];
				particle.m_force = solveZeroVector;
				particle.m_constraint_force = solveZeroVector;
#ifdef PARTICLE_DEBUG
				particle.m_color = m_asColored.color(r_particle);
#endif

				particle.m_mass = m_asParticle.mass(r_particle);

				m_asSolverParticle.index(r_particle) = index;
				index++;
			}
		}
	}

	m_n_sim = (unsigned int)m_particles.size();

	// pass for constraint particles
	for(RecordGroup::iterator i_rg = rg_input->begin();
			i_rg != rg_input->end(); i_rg++)
	{
		sp<RecordArray> spRA = *i_rg;
		sp<Scope> spScope = spRA->layout()->scope();
		m_asParticle.bind(spScope);
		m_asForcePoint.bind(spScope);
		m_asSolverParticle.bind(spScope);
#ifdef PARTICLE_DEBUG
		m_asColored.bind(spScope);
#endif

		m_asForcePoint.enforceHaving(m_asSolverParticle);
		if(m_asForcePoint.check(spRA) && !m_asParticle.check(spRA))
		{
			for(int i = 0; i < spRA->length(); i++)
			{
				Record r_particle = spRA->getRecord(i);

				m_recordToParticle[r_particle.idr()] = (unsigned int)m_particles.size();
//fe_fprintf(stderr, "0x%x C %d = %d\n", this, r_particle.idr(), m_particles.size());

				m_particles.resize(m_particles.size()+1);
				Particle &particle = m_particles.back();

				particle.m_location[0] = m_asForcePoint.location(r_particle)[0];
				particle.m_location[1] = m_asForcePoint.location(r_particle)[1];
				particle.m_velocity[0] = m_asForcePoint.velocity(r_particle)[0];
				particle.m_velocity[1] = m_asForcePoint.velocity(r_particle)[1];
				particle.m_force = solveZeroVector;
				particle.m_constraint_force = solveZeroVector;
#ifdef PARTICLE_DEBUG
				particle.m_color = m_asColored.color(r_particle);
#endif

				particle.m_mass = 0.0;

				m_asSolverParticle.index(r_particle) = index;
				index++;
			}
		}
	}


	m_n = (unsigned int)m_particles.size();
	m_rhs.resize(m_n_sim);
	m_dv.resize(m_n_sim);
	m_tmp.resize(m_n_sim);
	m_perturb.resize(m_n_sim);



	/* Express Connectivity ------------------------------------------------ */
	/*	The purpose of this stage is for forces to express connectivity so
		that reordering has something to work with.

		Altering rg_input at this point is precarious.
		*/
	t_pairs pairs;
	for(unsigned int i = 0; i < m_forces_add_damping.size(); i++)
	{
		m_forces_add_damping[i]->pairs(rg_input, pairs);
	}
	for(unsigned int i = 0; i < m_forces_as_is.size(); i++)
	{
		m_forces_as_is[i]->pairs(rg_input, pairs);
	}

#ifdef FE_ENABLE_SI_BANDWIDTH_NARROWING
// TODO: turn this back on (and reimplement to not reorder constraints)
	/* Reorder ------------------------------------------------------------- */
	t_pairs dynamic_only_pairs;
	for(unsigned int i = 0; i < pairs.size(); i++)
	{
		if(pairs[i].first >= m_n_sim) { continue; }
		if(pairs[i].second >= m_n_sim) { continue; }
		dynamic_only_pairs.push_back(pairs[i]);
	}
	std::vector<unsigned int> order;
	reorder(order, dynamic_only_pairs);
	for(RecordGroup::iterator i_rg = rg_input->begin();
			i_rg != rg_input->end(); i_rg++)
	{
		sp<RecordArray> spRA = *i_rg;
		sp<Scope> spScope = spRA->layout()->scope();
		m_asParticle.bind(spScope);
		m_asForcePoint.bind(spScope);
		m_asSolverParticle.bind(spScope);

		if(m_asForcePoint.check(spRA))
		{
			for(int i = 0; i < spRA->length(); i++)
			{
				Record r_particle = spRA->getRecord(i);

//fe_fprintf(stderr, "solver index new %d <- old %d\n", order[m_asSolverParticle.index(r_particle)], m_asSolverParticle.index(r_particle));
				if(m_asSolverParticle.index(r_particle) < (int)m_n_sim)
				{
					m_asSolverParticle.index(r_particle) =
						order[m_asSolverParticle.index(r_particle)];
				}
			}
		}
	}
#endif


	/* Matrix Create ------------------------------------------------------- */
	CompileMatrix compileMatrix;
	compileMatrix.setRows(m_n);

	/* Forces -------------------------------------------------------------- */
	for(unsigned int i = 0; i < m_forces_add_damping.size(); i++)
	{
		m_forces_add_damping[i]->compile(rg_input, m_particles, compileMatrix);
	}
	for(unsigned int i = 0; i < m_forces_as_is.size(); i++)
	{
		m_forces_as_is[i]->compile(rg_input, m_particles, compileMatrix);
	}

	/* Matrix -------------------------------------------------------------- */
	m_lhs = new UpperTriangularBCRS2<t_solve_real>();
	m_dfdx = new UpperTriangularBCRS2<t_solve_real>();
	m_dfdv = new UpperTriangularBCRS2<t_solve_real>();
	t_solve_m2 blank;
	// force diagonal to exist
	for(unsigned int i = 0; i < m_n_sim; i++)
	{
		compileMatrix.entry(i,i);
	}

	compileMatrix.symbolicFill();

	for(unsigned int i = 0; i < m_n_sim; i++)
	{
		m_dfdx->startrow();
		m_dfdv->startrow();
		m_lhs->startrow();

		CompileMatrix::t_row &row = compileMatrix.row(i);

		for(CompileMatrix::t_row::iterator i_row = row.begin();
			i_row != row.end(); i_row++)
		{
			unsigned int j = (*i_row).first;
			assert(j >= i); // assert upper triangular

			if(j >= m_n_sim) { continue; }

			m_lhs->insert(j, blank);
			m_dfdx->insert(j, blank);
			m_dfdv->insert(j, blank);
		}
	}
	m_lhs->done();
	m_dfdx->done();
	m_dfdv->done();


	for(unsigned int i = 0; i < m_n_sim; i++)
	{
		CompileMatrix::t_row &row = compileMatrix.row(i);

		unsigned int i_lhs_row = m_lhs->rowptr()[i];

		for(CompileMatrix::t_row::iterator i_row = row.begin();
			i_row != row.end(); i_row++)
		{
			unsigned int j = (*i_row).first;
			assert(j >= i); // assert upper triangular

			if(j >= m_n_sim)
			{
				CompileMatrix::t_entry &entry = (*i_row).second;

				CompileMatrix::t_dfdx_array &dfdx_array = entry.first;
				CompileMatrix::t_dfdv_array &dfdv_array = entry.second;

				for(unsigned int k = 0; k < dfdx_array.size(); k++)
				{
					*(dfdx_array[k]) = &m_dummy_block;
				}

				for(unsigned int k = 0; k < dfdv_array.size(); k++)
				{
					*(dfdv_array[k]) = &m_dummy_block;
				}
			}
			else
			{
				assert(j == m_lhs->colind()[i_lhs_row]); // assert consistency

				/*t_solve_m2 &lhs_block =*/ m_lhs->blocks()[i_lhs_row];
				t_solve_m2 &dfdx_block = m_dfdx->blocks()[i_lhs_row];
				t_solve_m2 &dfdv_block = m_dfdv->blocks()[i_lhs_row];

				i_lhs_row++;

				CompileMatrix::t_entry &entry = (*i_row).second;

				CompileMatrix::t_dfdx_array &dfdx_array = entry.first;
				CompileMatrix::t_dfdv_array &dfdv_array = entry.second;

				for(unsigned int k = 0; k < dfdx_array.size(); k++)
				{
					*(dfdx_array[k]) = &dfdx_block;
				}

				for(unsigned int k = 0; k < dfdv_array.size(); k++)
				{
					*(dfdv_array[k]) = &dfdv_block;
				}
			}
		}
	}

	for(unsigned int i = m_n_sim; i < m_n; i++)
	{
		CompileMatrix::t_row &row = compileMatrix.row(i);

		for(CompileMatrix::t_row::iterator i_row = row.begin();
			i_row != row.end(); i_row++)
		{
			assert((*i_row).first >= i); // assert upper triangular

			CompileMatrix::t_entry &entry = (*i_row).second;

			CompileMatrix::t_dfdx_array &dfdx_array = entry.first;
			CompileMatrix::t_dfdv_array &dfdv_array = entry.second;

			for(unsigned int k = 0; k < dfdx_array.size(); k++)
			{
				*(dfdx_array[k]) = &m_dummy_block;
			}

			for(unsigned int k = 0; k < dfdv_array.size(); k++)
			{
				*(dfdv_array[k]) = &m_dummy_block;
			}
		}
	}

}

void SemiImplicit2D::addForce(sp<Force> a_force, bool a_add_damping)
{
	if(a_add_damping)
	{
		m_forces_add_damping.push_back(a_force);
	}
	else
	{
		m_forces_as_is.push_back(a_force);
	}
}

#ifdef FE_ENABLE_SI_BANDWIDTH_NARROWING
void SemiImplicit2D::reorder(std::vector<unsigned int> &a_order,  SemiImplicit2D::t_pairs &a_pairs)
{
	std::vector<Particle> particles;
	particles.resize(m_n_sim);
	for(unsigned int i = 0; i < m_n_sim; i++)
	{
		particles[i] = m_particles[i];
	}

	{
		int *xadj = new int [m_n_sim+1];
		int *adj = new int [a_pairs.size()*2];
		int *perm = new int [m_n_sim];
		int *deg = new int [m_n_sim];
		signed char *mask = new signed char [m_n_sim];

		std::vector< std::set<int> > adj_assembly;
		adj_assembly.resize(m_n_sim);

		for(unsigned int i = 0; i < m_n_sim; i++)
		{
			perm[i] = i;
			deg[i] = 10;
			mask[i] = 0;
		}

		for(unsigned int i = 0; i < a_pairs.size(); i++)
		{
			adj_assembly[a_pairs[i].first].insert(a_pairs[i].second);
			adj_assembly[a_pairs[i].second].insert(a_pairs[i].first);
		}

		int idx = 0;
		for(unsigned int i = 0; i < adj_assembly.size(); i++)
		{
			xadj[i] = idx;
			for(std::set<int>::iterator i_set = adj_assembly[i].begin();
				i_set != adj_assembly[i].end(); i_set++)
			{
				adj[idx] = *i_set;
				idx++;
			}
		}
		xadj[m_n_sim] = idx;

		genrcmi((int)m_n_sim, RCM_INSERTION_SORT, xadj, adj, perm, mask, deg);

		a_order.resize(m_n_sim);
		for(unsigned int i = 0; i < m_n_sim; i++)
		{
			m_particles[i] = particles[perm[i]];
			a_order[perm[i]] = i;
		}

		delete [] xadj;
		delete [] adj;
		delete [] perm;
		delete [] deg;
		delete [] mask;

	}
}
#else
void SemiImplicit2D::reorder(std::vector<unsigned int> &,  SemiImplicit2D::t_pairs &)
{
}
#endif

bool SemiImplicit2D::lookupIndex(unsigned int &a_particle, Record &r_particle)
{
//fe_fprintf(stderr, "0x%x lookup %d\n", this, r_particle.idr());

	std::map<FE_UWORD, unsigned int>::iterator i_particle = m_recordToParticle.find(r_particle.idr());

	if(i_particle == m_recordToParticle.end())
	{
//fe_fprintf(stderr, "fail %d\n", m_recordToParticle.size());
		return false;
	}

	a_particle = i_particle->second;
	return true;
}


/* CompileMatrix =========================================================== */

SemiImplicit2D::CompileMatrix::CompileMatrix(void)
{
}

SemiImplicit2D::CompileMatrix::~CompileMatrix(void)
{
}

void SemiImplicit2D::CompileMatrix::clear(void)
{
	m_rows.clear();
}

void SemiImplicit2D::CompileMatrix::setRows(unsigned int a_count)
{
	m_rows.resize(a_count);
}

unsigned int SemiImplicit2D::CompileMatrix::rows(void)
{
	return (unsigned int)m_rows.size();
}

SemiImplicit2D::CompileMatrix::t_row &SemiImplicit2D::CompileMatrix::row(unsigned int a_index)
{
	return m_rows[a_index];
}

SemiImplicit2D::CompileMatrix::t_entry &SemiImplicit2D::CompileMatrix::entry(unsigned int a_i, unsigned int a_j)
{
	t_row::iterator i_row = m_rows[a_i].find(a_j);
	if(i_row != m_rows[a_i].end())
	{
		return i_row->second;
	}
	t_entry &v = m_rows[a_i][a_j];
	return v;
}

void SemiImplicit2D::CompileMatrix::symbolicFill(void)
{
	unsigned int n = (unsigned int)m_rows.size();

	t_nonzero_pattern A(n);
	t_nonzero_pattern L(n);
	t_nonzero_set t;


	for(unsigned int i = 0; i < n; i++)
	{
		for(t_row::iterator i_row = m_rows[i].begin();
			i_row != m_rows[i].end(); i_row++)
		{
			A[i].insert(i_row->first);
		}
	}

	std::vector<unsigned int> parent(m_rows.size());
	for(unsigned int i = 0; i < n; i++) { parent[i] = n; }

	for(unsigned int i = 0; i < n; i++)
	{
		L[i] = A[i];
		for(unsigned int j = 0; j < n; j++)
		{
			if(parent[j] == i)
			{
				L[i].insert(L[j].begin(), L[j].end());
				L[i].erase(j);
			}

		}
		t = L[i];
		t.erase(i);
		t_nonzero_set::iterator i_s = t.begin();
		if(i_s != t.end())
		{
			parent[i] = *i_s;
			assert(parent[i] != i);
		}
	}

	for(unsigned int i = 0; i < n; i++)
	{
		for(t_nonzero_set::iterator i_s = L[i].begin();
			i_s != L[i].end(); i_s++)
		{
			unsigned int j = *i_s;
			entry(i, j);
		}
		for(unsigned int j = 0; j < n; j++)
		{
			//if(j >= i) { entry(i, j); }
		}
	}
}


} /* namespace */
} /* namespace */

