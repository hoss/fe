/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __solve_IncompleteSparse_h__
#define __solve_IncompleteSparse_h__

namespace fe
{
namespace ext
{

class IncompleteSparse
{
	public:
			IncompleteSparse(void);
virtual		~IncompleteSparse(void);

		bool	solve(		std::vector<t_solve_v3> &a_x,
							sp< UpperTriangularBCRS<t_solve_real> > a_A,
							const std::vector<t_solve_v3> &a_b);

	private:
};

} /* namespace ext */
} /* namespace fe */

#endif /* __solve_IncompleteSparse_h__ */

