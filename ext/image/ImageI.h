/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __image_ImageI_h__
#define __image_ImageI_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
    @brief Image and texture handling

	@ingroup image

	For functions without an id argument, the currently selected image
	is affected.

	Depth refers to 3D images, such as with voxels,
	Bit planes refer to the magnitude of storage per element.
*//***************************************************************************/
class FE_DL_EXPORT ImageI:
	virtual public Component,
	public CastableAs<ImageI>
{
	public:
		enum	Format
				{
					e_none,
					e_colorindex,
					e_rgb,
					e_rgba
				};

				/// @brief create an empty image, returning its ID
virtual	I32		createSelect(void)											=0;

				/// @brief load an image from file, returning its ID
virtual	I32		loadSelect(String filename)									=0;

				/// @brief create an image from raw bytes, returning its ID
virtual I32		interpretSelect(void* data,U32 size)						=0;

				/// @brief create an image from text, returning its ID
virtual I32		interpretSelect(String a_source)							=0;

				/// @brief save the selected image to file
virtual	BWORD	save(String filename)										=0;

				/// @brief select an image for further access
virtual	void	select(I32 id)												=0;

				/// @brief return the selected image ID
virtual	I32		selected(void) const										=0;

				/// @brief remove an image from memory
virtual	void	unload(I32 id)												=0;

				/// @brief choose a format for the selected image
virtual	void	setFormat(ImageI::Format format)							=0;

				/// @brief return the format for the selected image
virtual	ImageI::Format	format(void) const									=0;

				/// @brief change the dimensions of the selcted image
virtual	void	resize(U32 width,U32 height,U32 depth)						=0;

				/// @brief replace data in part of the image
virtual	void	replaceRegion(U32 x,U32 y,U32 z,
						U32 width,U32 height,U32 depth,void* data)			=0;

				/// @brief return the X dimension of the image
virtual	U32		width(void) const											=0;

				/// @brief return the Y dimension of the image
virtual	U32		height(void) const											=0;

				/// @brief return the Z dimension of the image
virtual	U32		depth(void) const											=0;

				/// @brief return the raw byte buffer of the image
virtual	void*	raw(void) const												=0;

				/** @brief return the change serial number

					This number should increment each time the selected image
					is change. */
virtual	I32		serial(void) const											=0;

virtual	U32		regionCount(void) const										=0;
virtual	String	regionName(U32 a_regionIndex) const							=0;
virtual	Box2i	regionBox(String a_regionName) const						=0;
virtual	String	pickRegion(I32 a_x,I32 a_y) const							=0;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __image_ImageI_h__ */
