/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __image_ImageCommon_h__
#define __image_ImageCommon_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
    @brief General functionality for image support

	@ingroup image
*//***************************************************************************/
class FE_DL_EXPORT ImageCommon: virtual public ImageI
{
	public:
				ImageCommon(void);
virtual			~ImageCommon(void);

				//* As ImageI
virtual	I32		createSelect(void)						{ return -1; }
virtual I32		loadSelect(String filename)				{ return -1; }
virtual I32		interpretSelect(void* data,U32 size)	{ return -1; }
virtual I32		interpretSelect(String a_source)		{ return -1; }

virtual I32		serial(void) const;

virtual	BWORD	save(String filename)					{ return FALSE; }

virtual	U32		regionCount(void) const					{ return 0; }
virtual	String	regionName(U32 a_regionIndex) const		{ return ""; }
virtual	Box2i	regionBox(String a_regionName) const
				{	return Box2i(Vector2i(0,0),Vector2i(0,0)); }
virtual	String	pickRegion(I32 a_x,I32 a_y) const		{ return ""; }

	protected:

		void	incrementSerial(void);

	private:

		Array<I32>	m_serialArray;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __image_ImageCommon_h__ */
