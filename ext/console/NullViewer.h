/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __console_NullViewer_h__
#define __console_NullViewer_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Non-rendering viewer

	@ingroup console
*//***************************************************************************/
class FE_DL_EXPORT NullViewer:
		public Initialize<NullViewer>,
		virtual public QuickViewerI
{
	public:
						NullViewer(void):
								m_drawName("*.DrawNull")
						{	set(m_center); }
virtual					~NullViewer(void)									{}
		void			initialize(void);

						//* As QuickViewerI
virtual	void			bind(sp<DrawI> spDrawI)		{ m_spDrawI=spDrawI; }
virtual	sp<DrawI>		getDrawI(void) const		{ return m_spDrawI; }
virtual	sp<WindowI>		getWindowI(void) const
						{	return sp<WindowI>(NULL); }
virtual	sp<CameraControllerI>	getCameraControllerI(void) const
						{	return sp<CameraControllerI>(NULL); }
virtual	SpatialVector	interestPoint(void) const	{ return m_center; }
virtual	SpatialVector	auxilliaryPoint(void) const	{ return m_center; }
virtual	void			insertBeatHandler(sp<HandlerI> spHandlerI)			{}
virtual	void			insertEventHandler(sp<HandlerI> spHandlerI)			{}
virtual void			insertDrawHandler(sp<HandlerI> spHandlerI);
virtual	void			open(void)											{}
virtual	void			reopen(void)										{}
virtual	void			run(U32 frames);
virtual Real			frameRate(void) const		{ return 0.0; }

	protected:
		String				m_drawName;

	private:

		sp<DrawI>			m_spDrawI;
		sp<SignalerI>		m_spSignalerI;
		sp<Scope>			m_spScope;
		sp<Layout>			m_spBeatLayout;
		sp<Layout>			m_spUnderlayLayout;
		sp<Layout>			m_spRenderLayout;
		sp<Layout>			m_spOverlayLayout;
		SpatialVector		m_center;
		AsViewer			m_asViewer;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __console_NullViewer_h__ */
