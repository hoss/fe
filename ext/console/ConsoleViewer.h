/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __console_ConsoleViewer_h__
#define __console_ConsoleViewer_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Text-based viewer

	@ingroup console
*//***************************************************************************/
class FE_DL_EXPORT ConsoleViewer:
		public NullViewer
{
	public:
					ConsoleViewer(void)
					{
						m_drawName="*.DrawConsole";
					}
virtual				~ConsoleViewer(void)									{}

};

} /* namespace ext */
} /* namespace fe */

#endif /* __console_ConsoleViewer_h__ */
