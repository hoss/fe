/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __console_DrawConsole_h__
#define __console_DrawConsole_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Text-based implementations for DrawI

	@ingroup console
*//***************************************************************************/
class FE_DL_EXPORT DrawConsole: public DrawNull
{
	public:
				DrawConsole(void);
virtual			~DrawConsole(void);

virtual	void	flush(void);
virtual	void	drawAlignedText(const SpatialVector &location,
						const String text,const Color &color);
virtual	void	drawSphere(const SpatialTransform &transform,
						const SpatialVector *scale,const Color &color);

				using DrawNull::drawCylinder;
virtual	void	drawCylinder(const SpatialTransform &transform,
						const SpatialVector *scale,Real baseScale,
						const Color &color,U32 slices);

	private:
		void	clear(void);

		enum
		{
			e_width=90,
			e_height=45,
		};

		char	m_raster[e_height][e_width+1];
		U32		m_frame;
const	Real	m_scalarX;
const	Real	m_scalarY;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __console_DrawConsole_h__ */
