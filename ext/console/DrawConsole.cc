/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <console/console.pmh>

namespace fe
{
namespace ext
{

DrawConsole::DrawConsole(void):
		m_frame(0),
		m_scalarX(e_width*0.001f),
		m_scalarY(e_height*0.001f)
{
	clear();
}

DrawConsole::~DrawConsole(void)
{
}

void DrawConsole::flush(void)
{
	feLog("[2J");	//* clear screen
	feLog("[H");	//* home position
	for(U32 y=0;y<e_height;y++)
	{
		feLog("%s\n",m_raster[e_height-1-y]);
	}
	feLog("%d frames %60c\n",m_frame," ");
	clear();
	m_frame++;
}

void DrawConsole::clear(void)
{
	memset(m_raster,			'_',e_width+1);
	memset(m_raster[1],			' ',(e_height-2)*(e_width+1));
	memset(m_raster[e_height-1],'-',e_width+1);

	for(U32 y=0;y<e_height;y++)
	{
		m_raster[y][0]='|';
		m_raster[y][e_width-1]='|';
		m_raster[y][e_width]=0;
	}
	m_raster[e_height>>1][e_width>>1]='+';
}

void DrawConsole::drawAlignedText(const SpatialVector &location,
		const String text,const Color &color)
{
	const I32 x=I32(location[0]*m_scalarX+(e_width>>1));
	const I32 y=I32(location[1]*m_scalarY+(e_height>>1));

	const I32 length=text.length();
	for(I32 m=0;m<length;m++)
	{
		I32 xx=x+m+1;
		if(xx>=0 && xx<e_width && y>=0 && y<e_height &&
				m_raster[y][xx]!='*' && m_raster[y][xx]!='.')
		{
			m_raster[y][xx]=text.c_str()[m];
		}
	}
}

void DrawConsole::drawSphere(const SpatialTransform &transform,
		const SpatialVector *scale,const Color &color)
{
	const I32 x=I32(transform.translation()[0]*m_scalarX+(e_width>>1));
	const I32 y=I32(transform.translation()[1]*m_scalarY+(e_height>>1));

//	feLog("%d %d\n",x,y);

	if(x>=0 && x<e_width && y>=0 && y<e_height)
	{
		m_raster[y][x]='*';
	}
}

void DrawConsole::drawCylinder(const SpatialTransform &transform,
		const SpatialVector *scale,Real baseScale,const Color &color,U32 slices)
{
	Real length=1.0f;
	if(scale)
	{
		length=(*scale)[2];
	}

	SpatialVector span;
	rotateVector(transform,SpatialVector(0.0f,0.0f,1.0f),span);

	Real inc=1.0f/m_scalarX;
	for(Real delta=0.0f;delta<length;delta+=inc)
	{
		const I32 x=I32((transform.translation()[0]+
				delta*span[0])*m_scalarX+(e_width>>1));
		const I32 y=I32((transform.translation()[1]+
				delta*span[1])*m_scalarY+(e_height>>1));

		if(x>=0 && x<e_width && y>=0 && y<e_height && m_raster[y][x]!='*')
		{
			m_raster[y][x]='.';
		}
	}
}

} /* namespace ext */
} /* namespace fe */
