/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <alembic/alembic.pmh>

#define FE_SAA_DEBUG			FALSE
#define FE_SAA_VERBOSE			FALSE

#define FE_SAA_INDENTION		"  "

//* TODO properly use the per-vertex normals/uvs (need better RG vertex support)
#define FE_SAA_UNIQUE_POINTS	FALSE

namespace fe
{
namespace ext
{

SurfaceAccessibleAbc::FileContext::FileContext(void)
{
}

SurfaceAccessibleAbc::FileContext::~FileContext(void)
{
	m_archive.reset();
}

BWORD SurfaceAccessibleAbc::load(String a_filename,sp<Catalog> a_spSettings)
{
#if FE_SAA_DEBUG
	feLog("SurfaceAccessibleAbc::load file \"%s\"\n",a_filename.c_str());
#endif

	//* TODO option to pick mesh, curve, or joint

	if(a_spSettings.isValid())
	{
		m_frame=a_spSettings->catalog<Real>("frame");
		m_spFilter=a_spSettings->catalog< sp<Component> >("filter");

		const String options=a_spSettings->catalog<String>("options");

#if FE_SAA_DEBUG
		feLog("SurfaceAccessibleAbc::load options \"%s\"\n",options.c_str());
#endif

		if(!options.empty())
		{
			setOptions(options);
		}
	}
	else
	{
		m_frame=0.0;
		m_spFilter=NULL;
	}

	m_fps= -1.0;

	m_minStep= -1.0;

	//* read position from xgen_Pref
	m_refMode=(m_optionMap.find("ref")!=m_optionMap.end());

	if(m_optionMap.find("fps")!=m_optionMap.end())
	{
		m_fps=atof(m_optionMap["fps"].c_str());
	}

	m_pathArray.clear();
	if(m_optionMap.find("paths")!=m_optionMap.end())
	{
		String pathList=m_optionMap["paths"];
		String path;

		while(!(path=pathList.parse()).empty())
		{
			if(path.c_str()[0]!='/')
			{
				path="/"+path;
			}

			m_pathArray.push_back(path);
		}
	}

	m_matchArray.clear();
	if(m_optionMap.find("match")!=m_optionMap.end())
	{
		String matchList=m_optionMap["match"];
		String match;

		while(!(match=matchList.parse()).empty())
		{
			m_matchArray.push_back(match);
		}
	}

	if(m_filename!=a_filename)
	{
		m_spContext=NULL;
		m_filename=a_filename;
	}

	if(m_spContext.isValid() && !m_spContext->m_archive)
	{
		m_spContext=NULL;
	}

#if FE_SAA_DEBUG
	feLog("SurfaceAccessibleAbc::load frame %.6G\n",m_frame);
#endif

	if(m_spContext.isNull())
	{

		setContext(a_filename);
	}

	if(m_spContext.isNull() || !m_spContext->m_archive.valid())
	{
		feLog("SurfaceAccessibleAbc::load failed to read \"%s\"\n",
				a_filename.c_str());

		return FALSE;
	}

	Real minTimePerSample= -1.0;

	const I32 samplerCount=m_spContext->m_archive.getNumTimeSamplings();
	for(I32 samplerIndex=0;samplerIndex<samplerCount;samplerIndex++)
	{
		Alembic::AbcGeom::TimeSamplingPtr sampler=
				m_spContext->m_archive.getTimeSampling(samplerIndex);

		Alembic::AbcGeom::TimeSamplingType samplerType=
				sampler->getTimeSamplingType();

		const BWORD isUniform=samplerType.isUniform();
		const Real timePerCycle=samplerType.getTimePerCycle();

#if FE_SAA_DEBUG
		const I32 timeCount=sampler->getNumStoredTimes();
		const BWORD isCyclic=samplerType.isCyclic();
		const BWORD isAcyclic=samplerType.isAcyclic();
		const U32 samplesPerCycle=samplerType.getNumSamplesPerCycle();
		const Real fps=Real(1)/timePerCycle;

		feLog("sampler %d/%d times %d uni %d cyc %d acyc %d"
				" SPC %d TPC %.6G FPS %.6G\n",
				samplerIndex,samplerCount,timeCount,
				isUniform,isCyclic,isAcyclic,
				samplesPerCycle,timePerCycle,fps);
#endif

		if(!isUniform)
		{
			feLog("SurfaceAccessibleAbc::load"
					" non-uniform sampling detected\n");
		}

		if(timePerCycle>1e-3 &&
				(minTimePerSample<0.0 || minTimePerSample>timePerCycle))
		{
			minTimePerSample=timePerCycle;
		}
	}

	m_mode=e_scan;
	m_xformCount=0;
	m_meshCount=0;
	m_curveCount=0;
	m_instanceCount=0;
	outlineClear();

	Alembic::AbcGeom::IObject iObj=m_spContext->m_archive.getTop();

	visit(iObj);

#if FE_SAA_DEBUG
	feLog("SurfaceAccessibleAbc::load"
			" scan xformCount %d meshCount %d curveCount %d instanceCount %d"
			" minStep %.6G minTimePerSample %.6G\n",
			m_xformCount,m_meshCount,m_curveCount,m_instanceCount,
			m_minStep,minTimePerSample);
#endif

	//* auto-detected frames per second
	if(m_fps<0.0)
	{
		if(m_minStep>0.0)
		{
			m_fps=Real(1)/m_minStep;
		}
		else if(minTimePerSample>0.0)
		{
			m_fps=Real(1)/minTimePerSample;
		}
		else
		{
			m_fps=Real(24.0);
		}

#if FE_SAA_DEBUG
		feLog("SurfaceAccessibleAbc::load presuming %.6G fps\n",m_fps);
#endif
	}

	m_firstFaceMap.clear();

	if(m_instanceCount)
	{
		feLog("SurfaceAccessibleAbc::load found %d instances\n",
				m_instanceCount);
	}

	//* NOTE trying to auto-detect intended content
	if(m_meshCount)
	{
//		feLog("MESH MODE\n");
		if(m_curveCount)
		{
			feLog("SurfaceAccessibleAbc::load ignoring curves in mesh mode\n");
		}

		m_mode=e_meshes;

		startMeshes();

#if !FE_SAA_UNIQUE_POINTS
		m_spOutputUV=accessor(e_vertex,e_uv,
				SurfaceAccessibleI::e_createMissing);
#endif

		visit(iObj);
		completeMeshes();
	}
	else if(m_curveCount)
	{
		m_mode=e_curves;

		startCurves();
		visit(iObj);
		completeCurves();
	}
	else if(m_xformCount>1)
	{
		m_mode=e_joints;

		startJoints();
		visit(iObj);
		completeJoints();
	}
	else
	{
		m_mode=e_points;

		startPoints();
		visit(iObj);
		completePoints();
	}

//	SurfaceAccessibleRecord::save("abc.rg");

#if FE_SAA_DEBUG
//	feLog("Outline:\n");
//	const I32 outlineCount=m_outline.size();
//	for(I32 outlineIndex=0;outlineIndex<outlineCount;outlineIndex++)
//	{
//		feLog("    %s\n",m_outline[outlineIndex].c_str());
//	}

	feLog("SurfaceAccessibleAbc::load done\n");
#endif

	return TRUE;
}

BWORD SurfaceAccessibleAbc::setContext(String a_filename)
{
	sp<Catalog> spMasterCatalog=registry()->master()->catalog();
	const String key="SurfaceAccessibleAbc:"+a_filename;

	m_spContext=spMasterCatalog->catalog< hp<Component> >(key);

	if(m_spContext.isValid())
	{
		return TRUE;
	}

#if FE_SFB_DEBUG
	feLog("SurfaceAccessibleAbc::setContext"
			" creating new FileContext\n");
#endif

	m_spContext=new FileContext();
	spMasterCatalog->catalog< hp<Component> >(key)=m_spContext;

	if(m_spContext.isNull())
	{
		feLog("SurfaceAccessibleAbc::setContext"
				" unable to create FileContext\n");
		return FALSE;
	}

#if FE_SAA_DEBUG
	feLog("SurfaceAccessibleAbc::setContext opening archive\n");
#endif

	Alembic::AbcGeom::ErrorHandler::Policy policy=
//			Alembic::AbcGeom::ErrorHandler::kQuietNoopPolicy:
			Alembic::AbcGeom::ErrorHandler::kNoisyNoopPolicy;

#if ALEMBIC_LIBRARY_VERSION < 10500
	Alembic::AbcGeom::IArchive* pArchive=new Alembic::AbcGeom::IArchive(
			Alembic::AbcCoreHDF5::ReadArchive(),a_filename.c_str(),policy);

	//* TODO verify that this is properly ref-counted underneath
	m_spContext->m_archive= *pArchive;
	delete pArchive;
#else
	Alembic::AbcCoreFactory::IFactory factory;
	factory.setPolicy(policy);
	m_spContext->m_archive=factory.getArchive(a_filename.c_str());
#endif

	if(!m_spContext->m_archive.valid())
	{
		m_spContext=NULL;
		return FALSE;
	}

#if FE_SAA_DEBUG
	std::string appName;
	std::string libraryVersionString;
	Alembic::Util::uint32_t libraryVersion;
	std::string whenWritten;
	std::string userDescription;

	Alembic::Abc::GetArchiveInfo(m_spContext->m_archive,appName,
			libraryVersionString,libraryVersion,whenWritten,userDescription);

	feLog("  written by    \"%s\"\n",appName.c_str());
	feLog("  using Alembic %d \"%s\"\n",
			libraryVersion,libraryVersionString.c_str());
	feLog("  written on    \"%s\"\n",whenWritten.c_str());
	feLog("  description   \"%s\"\n",userDescription.c_str());
#endif

	return TRUE;
}

void SurfaceAccessibleAbc::visit(Alembic::AbcGeom::IObject a_iObj)
{
	const String parentPath=a_iObj.getParent().getFullName().c_str();
	const String path=a_iObj.getFullName().c_str();
	const U32 childCount=a_iObj.getNumChildren();

#if FE_SAA_DEBUG
	if(m_mode!=e_scan)
	{
		feLog("\nobject \"%s\" childCount %d\n",
				path.c_str(),a_iObj.getPtr().get(),childCount);
	}
#endif

#if ALEMBIC_LIBRARY_VERSION >= 10500
	if(a_iObj.isInstanceRoot())
	{
		//* this node is an instance of a different node

		if(m_mode==e_scan)
		{
			m_instanceCount++;
		}
		else
		{
//			feLog("INSTANCE \"%s\"\n%s\n",
//					a_iObj.instanceSourcePath().c_str(),
//					c_print(m_currentXform));

			if(m_spOutputVertices.isValid())
			{
				const I32 primitiveIndex=m_spOutputVertices->append();

				m_spOutputName->set(primitiveIndex,path.c_str());
				m_spOutputPart->set(primitiveIndex,basenameOf(path));

				sp<SurfaceAccessorI> spOutputInstance=
						accessor(e_primitive,"instance",
						SurfaceAccessibleI::e_createMissing);
				if(spOutputInstance.isValid())
				{
					spOutputInstance->set(primitiveIndex,
							a_iObj.instanceSourcePath().c_str());
				}

				if(m_spOutputAnimX.isNull())
				{
					m_spOutputAnimX=accessor(e_primitive,"animX",
							SurfaceAccessibleI::e_createMissing);
					m_spOutputAnimY=accessor(e_primitive,"animY",
							SurfaceAccessibleI::e_createMissing);
					m_spOutputAnimZ=accessor(e_primitive,"animZ",
							SurfaceAccessibleI::e_createMissing);
					m_spOutputAnimT=accessor(e_primitive,"animT",
							SurfaceAccessibleI::e_createMissing);
//					m_spOutputAnimS=accessor(e_primitive,"animS",
//							SurfaceAccessibleI::e_createMissing);
				}
				if(m_spOutputAnimX.isValid() &&
						m_spOutputAnimY.isValid() &&
						m_spOutputAnimZ.isValid() &&
						m_spOutputAnimT.isValid()
//						&& m_spOutputAnimS.isValid()
						)
				{
					//* TODO separate scale

					m_spOutputAnimX->set(primitiveIndex,
							m_currentXform.column(0));
					m_spOutputAnimY->set(primitiveIndex,
							m_currentXform.column(1));
					m_spOutputAnimZ->set(primitiveIndex,
							m_currentXform.column(2));
					m_spOutputAnimT->set(primitiveIndex,
							m_currentXform.column(3));
//					m_spOutputAnimS->set(primitiveIndex,
//							SpatialVector(1.0,1.0,1.0));
				}
			}
		}

		return;
	}
#endif

	if(parentPath=="/")
	{
		setIdentity(m_currentXform);
	}

	setIdentity(m_refWorld);

	Alembic::AbcGeom::ICompoundProperty properties=a_iObj.getProperties();
	String indent=FE_SAA_INDENTION;

	m_pointArray.clear();
	m_pointRefArray.clear();
	m_colorArray.clear();
	m_normalArray.clear();
	m_uvArray.clear();
	m_uvLookupArray.clear();
	m_vertCountArray.clear();
	m_vertIndexArray.clear();
	m_scopeMap.clear();
	m_stringArrayMap.clear();
	m_integerArrayMap.clear();
	m_realArrayMap.clear();
	m_vectorArrayMap.clear();

	m_normalScope=Alembic::AbcGeom::kUnknownScope;
	m_colorScope=Alembic::AbcGeom::kUnknownScope;
	m_uvScope=Alembic::AbcGeom::kUnknownScope;

	BWORD allowed=TRUE;

	const I32 pathCount=m_pathArray.size();
	if(pathCount)
	{
		I32 pathIndex=0;
		for(;pathIndex<pathCount;pathIndex++)
		{
			const String& rPath=m_pathArray[pathIndex];
			if(path==rPath || path.match(rPath+"/.*"))
			{
//				feLog("PATH MATCH \"%s\" vs \"%s\"\n",
//						path.c_str(),rPath.c_str());
				break;
			}
		}
		if(pathIndex>=pathCount)
		{
			allowed=FALSE;
		}
	}
	else
	{
		//* NOTE default rules if no paths specified

		if(m_refMode)
		{
			//* reject if not first level
			//* and first level doesn't end with ".ref"
			//* and second level level doesn't end with "Shape" (for XGen)
			if(path.match("^/[^/]*/.*") &&
					!path.match("^/[^/]*\\.ref($|/.*)") &&
					!path.match("^/[^/]*/[^/]*Shape($|/.*)"))
			{
				allowed=FALSE;
			}
		}
		else
		{
			//* reject if first level ends with ".ref"
			if(path.match("^/[^/]*\\.ref($|/.*)"))
			{
				allowed=FALSE;
			}
		}

		//* reject if first level starts with "collision"
		if(path.match("^/collision.*"))
		{
			allowed=FALSE;
		}

		//* reject if first level ends with "_standin" or "_standin.ref"
		if(path.match("^/[^/]*_standin($|/.*)") ||
				path.match("^/[^/]*_standin.ref($|/.*)"))
		{
			allowed=FALSE;
		}

		//* reject "outer" eye
		if(path.match("^/[^/]*eye[^/]*/[^/]*outer.*"))
		{
			allowed=FALSE;
		}

//		feLog("PATH CHECK \"%s\" %s\n",path.c_str(),allowed? "": "REJECT");
	}

	const I32 matchCount=m_matchArray.size();
	if(allowed && matchCount)
	{
		I32 matchIndex=0;
		for(;matchIndex<matchCount;matchIndex++)
		{
			const String& rMatch=m_matchArray[matchIndex];
			if(path.match(rMatch))
			{
//				feLog("REGEX MATCH \"%s\" vs \"%s\"\n",
//						path.c_str(),rMatch.c_str());
				break;
			}
		}
		if(matchIndex>=matchCount)
		{
			allowed=FALSE;
		}
	}

	if(allowed && m_mode!=e_scan)
	{
		outlineAppend(path);

		visitProperties(properties,indent,parentPath,path);
	}

	const BWORD isPoints=
			Alembic::AbcGeom::IPoints::matches(a_iObj.getHeader());
	const BWORD isCurves=
			Alembic::AbcGeom::ICurves::matches(a_iObj.getHeader());
	const BWORD isMeshes=
			Alembic::AbcGeom::IPolyMesh::matches(a_iObj.getHeader());
	const BWORD isSubdiv=
			Alembic::AbcGeom::ISubD::matches(a_iObj.getHeader());

	//* TODO add targets

	if(Alembic::AbcGeom::IXform::matches(a_iObj.getHeader()))
	{
#if FE_SAA_DEBUG
		if(m_mode!=e_scan)
		{
			feLog("TRANSFORM\n");
		}
#endif
		Alembic::AbcGeom::IXform ixform(a_iObj,
				Alembic::AbcGeom::kWrapExisting);

		const I32 sampleCount=ixform.getSchema().getNumSamples();

		Alembic::AbcGeom::TimeSamplingPtr sampler=
				ixform.getSchema().getTimeSampling();

		if(m_mode==e_scan)
		{
			m_xformCount++;

			if(sampleCount>1)
			{
				const Alembic::AbcGeom::chrono_t time0=
						sampler->getSampleTime(0);
				const Alembic::AbcGeom::chrono_t time1=
						sampler->getSampleTime(1);
				const Real step=time1-time0;

				if(step>1e-3 && (m_minStep<0.0 || m_minStep>step))
				{
					m_minStep=step;
				}
			}
		}

#if FALSE // FE_SAA_DEBUG
		for(I32 sampleIndex=0;sampleIndex<sampleCount;sampleIndex++)
		{
			const Alembic::AbcGeom::chrono_t time=
					sampler->getSampleTime(sampleIndex);
			feLog("sample %d/%d frame %.6G time %.6G\n",
					sampleIndex,sampleCount,time*m_fps,time);
		}
#endif

		std::pair<Alembic::AbcGeom::index_t,Alembic::AbcGeom::chrono_t>
				nearIndex=sampler->getNearIndex(m_frame/m_fps,sampleCount);

		I32 sampleIndex=nearIndex.first;
#if FE_SAA_VERBOSE
		feLog("sampleIndex %d/%d near %.6G\n",
				sampleIndex,sampleCount,m_frame/m_fps);
#endif
		FEASSERT(sampleIndex>=0);
		FEASSERT(!sampleCount || sampleIndex<sampleCount);

		Alembic::AbcGeom::XformSample sample;
		ixform.getSchema().get(sample,
				Alembic::Abc::ISampleSelector(
				Alembic::Abc::index_t(sampleIndex)));

		Alembic::AbcGeom::M44d m44d=sample.getMatrix();
		double* pRaw=m44d.getValue();

		SpatialTransform xformRelative;
		set(xformRelative,pRaw);

#if FE_SAA_VERBOSE
		feLog("%s\n",c_print(xformRelative));
#endif

		if(m_mode==e_joints)
		{
			const Color color(0.0,0.0,1.0);			//* TODO
			const Color localScale(1.0,1.0,1.0);	//* TODO

			const String name=path.basename();
			const String parentName=path.pathname().basename();

			setJoint(name,parentName,xformRelative,localScale,
					m_refWorld,color);
		}
		else
		{
			//* TODO should allow for stacked transforms
			m_currentXform=xformRelative;
		}
	}
	else if(!allowed)
	{
		//* SKIP
	}
	else if(isPoints || isCurves || isMeshes || isSubdiv)
	{

#if FE_SAA_DEBUG
		if(m_mode!=e_scan)
		{
			if(isPoints)
			{
				feLog("POINTS\n");
			}
			else if(isCurves)
			{
				feLog("CURVES\n");
			}
			else if(isMeshes)
			{
				feLog("MESH\n");
			}
			else
			{
				feLog("SUBDIV\n");
			}
		}
#endif
		if(m_mode==e_scan)
		{
			if(isCurves)
			{
				m_curveCount++;
			}
			else if(!isPoints)
			{
				m_meshCount++;
			}
		}

		if(((isMeshes || isSubdiv) && m_mode==e_meshes) ||
				(isCurves && m_mode==e_curves) ||
				(isPoints && m_mode==e_points))
		{
			const I32 existingPointCount=m_spOutputPoint->count();
			const I32 existingFaceCount=m_spOutputVertices.isValid()?
					m_spOutputVertices->count(): 0;

			m_firstFaceMap[path]=existingFaceCount;

			if(isPoints)
			{
				m_vertCountArray.resize(0);
				m_vertIndexArray.resize(0);
			}
			else if(isCurves)
			{
				const I32 cvCount=m_pointArray.size();
				m_vertIndexArray.resize(cvCount);
				for(I32 cvIndex=0;cvIndex<cvCount;cvIndex++)
				{
					m_vertIndexArray[cvIndex]=cvIndex;
				}

				const I32 faceCount=m_vertCountArray.size();
				if(faceCount<1)
				{
					m_vertCountArray.resize(1);
					m_vertCountArray[0]=cvCount;
				}
			}

			const I32 addFaceCount=m_vertCountArray.size();

			const Array<SpatialVector>& rPointArray=
					(m_refMode && m_pointRefArray.size())?
					m_pointRefArray: m_pointArray;

#if FE_SAA_UNIQUE_POINTS
			I32 addPointCount=0;
			for(I32 faceIndex=0;faceIndex<addFaceCount;faceIndex++)
			{
				addPointCount+=m_vertCountArray[faceIndex];
			}

			m_spOutputPoint->append(addPointCount);

			for(I32 index=0;index<addPointCount;index++)
			{
				const I32 lookupIndex=m_vertIndexArray[index];
				const I32 pointIndex=existingPointCount+index;
				m_spOutputPoint->set(pointIndex,
						transformVector(m_currentXform,
						rPointArray[lookupIndex]));
			}
#else
			const I32 addPointCount=rPointArray.size();
			m_spOutputPoint->append(addPointCount);

			for(I32 index=0;index<addPointCount;index++)
			{
				const I32 pointIndex=existingPointCount+index;
				m_spOutputPoint->set(pointIndex,
						transformVector(m_currentXform,
						rPointArray[index]));
			}
#endif

#if FE_SAA_DEBUG
			feLog("  %d points %d faces\n",addPointCount,addFaceCount);
#endif

			const I32 newPointCount=existingPointCount+addPointCount;

			m_primVerts.resize(addFaceCount);
			for(I32 faceIndex=0;faceIndex<addFaceCount;faceIndex++)
			{
				m_primVerts[faceIndex].resize(m_vertCountArray[faceIndex]);
			}

			I32 vertIndex=0;

			const I32 addVertexCount=m_vertIndexArray.size();

			for(I32 faceIndex=0;faceIndex<addFaceCount;faceIndex++)
			{
				Array<I32>& rVerts=m_primVerts[faceIndex];
				const U32 vertexCount=rVerts.size();

				for(U32 vertexIndex=0;vertexIndex<vertexCount;vertexIndex++)
				{
					if(vertIndex>=addVertexCount)
					{
						feLog("SurfaceAccessibleAbc::visit"
								" face counts exceeds face indices\n");
						faceIndex=addFaceCount;
						break;
					}
					if(vertIndex>=addVertexCount)
					{
						feLog("SurfaceAccessibleAbc::visit"
								" face counts exceeds face indices\n");
						faceIndex=addFaceCount;
						break;
					}
#if FE_SAA_UNIQUE_POINTS
					rVerts[vertexIndex]=
							existingPointCount+(vertIndex++);
#else
					rVerts[vertexIndex]=
							existingPointCount+m_vertIndexArray[vertIndex++];
#endif
					if(rVerts[vertexIndex]>=newPointCount)
					{
						feLog("SurfaceAccessibleAbc::visit"
								" vertex data exceeds point data\n");
						return;
					}
				}
			}

			if(m_spOutputVertices.isValid())
			{
				m_spOutputVertices->append(m_primVerts);
			}

			if(m_mode==e_curves)
			{
				for(I32 faceIndex=0;faceIndex<addFaceCount;faceIndex++)
				{
					const I32 primitiveIndex=existingFaceCount+faceIndex;

					m_spOutputProperties->set(primitiveIndex,e_openCurve,TRUE);
				}
			}

			//* NOTE actual name/part attribute can overwrite these
			for(I32 faceIndex=0;faceIndex<addFaceCount;faceIndex++)
			{
				const I32 primitiveIndex=existingFaceCount+faceIndex;

				m_spOutputName->set(primitiveIndex,path.c_str());
				m_spOutputPart->set(primitiveIndex,basenameOf(path));
			}

			if(m_colorArray.size())
			{
				const BWORD constantRate=
						(m_colorScope==Alembic::AbcGeom::kConstantScope);
				const BWORD faceRate=
						(m_colorScope==Alembic::AbcGeom::kUniformScope);
				const BWORD pointRate=
						(m_colorScope==Alembic::AbcGeom::kVertexScope);

				if(isPoints)
				{
					for(I32 index=0;index<addPointCount;index++)
					{
						const I32 pointIndex=existingPointCount+index;
						m_spOutputColor->set(pointIndex,
								m_colorArray[constantRate? 0: index]);
					}
				}

				vertIndex=0;

				for(I32 faceIndex=0;faceIndex<addFaceCount;faceIndex++)
				{
					const I32 primitiveIndex=existingFaceCount+faceIndex;

					Array<I32>& rVerts=m_primVerts[faceIndex];
					const U32 vertexCount=rVerts.size();
					for(U32 vertexIndex=0;vertexIndex<vertexCount;vertexIndex++)
					{
#if FE_SAA_UNIQUE_POINTS
						const I32 pointIndex=existingPointCount+vertIndex;
#else
						const I32 pointIndex=m_spOutputVertices->integer(
								primitiveIndex,vertexIndex);
#endif
						if(pointIndex>=newPointCount)
						{
							feLog("SurfaceAccessibleAbc::visit"
									" color data exceeded point count %d\n",
									addPointCount)
							faceIndex=addFaceCount;
							break;
						}
						const I32 lookupIndex=constantRate? 0:
								(pointRate? pointIndex:
								(faceRate? faceIndex: vertIndex));
						if(lookupIndex<I32(m_colorArray.size()))
						{
							m_spOutputColor->set(pointIndex,
									m_colorArray[lookupIndex]);
						}
						else
						{
							feLog("SurfaceAccessibleAbc::visit"
									" color lookup %d exceeds size %d\n",
									lookupIndex,m_colorArray.size())
							feLog("   (constantRate %d faceRate %d"
									" pointRate %d)\n",
									constantRate,faceRate,pointRate);
						}

						vertIndex++;
					}
				}
			}

			if(m_radiusArray.size())
			{
				for(I32 index=0;index<addPointCount;index++)
				{
					const I32 pointIndex=existingPointCount+index;
					m_spOutputRadius->set(pointIndex,
							m_radiusArray[index]);
				}
			}

			if(m_normalArray.size() &&
					m_normalArray.size()==m_pointArray.size() &&
					(m_normalScope==Alembic::AbcGeom::kVertexScope ||
					m_normalScope==Alembic::AbcGeom::kVaryingScope))
			{
				//* one normal per sharable point
				for(I32 index=0;index<addPointCount;index++)
				{
					const I32 pointIndex=existingPointCount+index;
					m_spOutputNormal->set(pointIndex,
							rotateVector(m_currentXform,
							m_normalArray[index]));
				}
			}
			else if(m_normalArray.size() &&
					m_normalArray.size()==m_vertIndexArray.size() &&
					m_normalScope==Alembic::AbcGeom::kFacevaryingScope)
			{
				//* one normal per face vert
				vertIndex=0;

				for(I32 faceIndex=0;faceIndex<addFaceCount;faceIndex++)
				{
					const I32 primitiveIndex=existingFaceCount+faceIndex;

					Array<I32>& rVerts=m_primVerts[faceIndex];
					const U32 vertexCount=rVerts.size();
					for(U32 vertexIndex=0;vertexIndex<vertexCount;vertexIndex++)
					{
#if FE_SAA_UNIQUE_POINTS
						const I32 pointIndex=existingPointCount+vertIndex;
#else
						const I32 pointIndex=m_spOutputVertices->integer(
								primitiveIndex,vertexIndex);
#endif
						if(pointIndex>=newPointCount)
						{
							feLog("SurfaceAccessibleAbc::visit"
									" normal data exceeded point count %d\n",
									addPointCount)
							faceIndex=addFaceCount;
							break;
						}
						m_spOutputNormal->set(pointIndex,
								rotateVector(m_currentXform,
								m_normalArray[vertIndex++]));
					}
				}
			}
			else if(!isCurves)
			{
				//* regenerate new poly normals

				vertIndex=0;

				for(I32 faceIndex=0;faceIndex<addFaceCount;faceIndex++)
				{
					const I32 primitiveIndex=existingFaceCount+faceIndex;

					Array<I32>& rVerts=m_primVerts[faceIndex];

					const I32 index0=m_vertIndexArray[vertIndex];
					const I32 index1=m_vertIndexArray[vertIndex+1];
					const I32 index2=m_vertIndexArray[vertIndex+2];

					if(index0>=addPointCount || index1>=addPointCount ||
							index2>=addPointCount)
					{
						feLog("SurfaceAccessibleAbc::visit"
								" vert data exceeded point count %d\n",
								addPointCount)
						break;
					}

					const SpatialVector point0=rPointArray[index0];
					const SpatialVector point1=rPointArray[index1];
					const SpatialVector point2=rPointArray[index2];

					const SpatialVector norm=
							unitSafe(cross(point2-point0,point1-point0));

					//* NOTE using face normal on points is particularly sloppy

					const U32 vertexCount=rVerts.size();
					for(U32 vertexIndex=0;vertexIndex<vertexCount;vertexIndex++)
					{
#if FE_SAA_UNIQUE_POINTS
						const I32 pointIndex=existingPointCount+vertIndex;
#else
						const I32 pointIndex=m_spOutputVertices->integer(
								primitiveIndex,vertexIndex);
#endif
						if(pointIndex>=newPointCount)
						{
							feLog("SurfaceAccessibleAbc::visit"
									" normal data exceeded point count %d\n",
									addPointCount)
							faceIndex=addFaceCount;
							break;
						}
						m_spOutputNormal->set(pointIndex,norm);

						vertIndex++;
					}
				}
			}
			else
			{
				//* regenerate new curve normals

				for(I32 faceIndex=0;faceIndex<addFaceCount;faceIndex++)
				{
					const I32 primitiveIndex=existingFaceCount+faceIndex;

					const I32 subCount=
							m_spOutputVertices->subCount(primitiveIndex);
					if(subCount<2)
					{
						continue;
					}

					SpatialVector previousPoint =
							m_spOutputVertices->spatialVector(primitiveIndex,0);
					SpatialVector norm(0.0,0.0,0.0);
					for(I32 subIndex=1;subIndex<subCount;subIndex++)
					{
						const SpatialVector point=
								m_spOutputVertices->spatialVector(
								primitiveIndex,subIndex);

						//* NOTE doesn't need to be normalized
						const SpatialVector tangent=point-previousPoint;

						if(subIndex==1)
						{
							norm[0]=tangent[1];
							norm[1]=tangent[2];
							norm[2]=tangent[0];
						}

						const SpatialVector side=cross(tangent,norm);
						norm=unitSafe(cross(side,tangent));

						const I32 pointIndex=m_spOutputVertices->integer(
								primitiveIndex,subIndex);
						m_spOutputNormal->set(pointIndex,norm);

						if(subIndex==1)
						{
							const I32 pointIndex=m_spOutputVertices->integer(
									primitiveIndex,0);
							m_spOutputNormal->set(pointIndex,norm);
						}

						previousPoint = point;
					}
				}
			}

			if(m_uvArray.size())
			{
				if(isPoints)
				{
					const BWORD constantRate=
							(m_colorScope==Alembic::AbcGeom::kConstantScope);
					for(I32 index=0;index<addPointCount;index++)
					{
						const I32 pointIndex=existingPointCount+index;
						m_spOutputUV->set(pointIndex,
								m_uvArray[constantRate? 0: index]);
					}
				}

				vertIndex=0;

				if(m_uvScope!=Alembic::AbcGeom::kFacevaryingScope)
				{
					feLog("SurfaceAccessibleAbc::visit"
							" WARNING: uv data is not kFacevaryingScope\n");
				}

				for(I32 faceIndex=0;faceIndex<addFaceCount;faceIndex++)
				{
					const I32 primitiveIndex=existingFaceCount+faceIndex;

					Array<I32>& rVerts=m_primVerts[faceIndex];
					const U32 vertexCount=rVerts.size();
					for(U32 vertexIndex=0;vertexIndex<vertexCount;vertexIndex++)
					{
						const I32 uvIndex=m_uvLookupArray[vertIndex++];
						const SpatialVector uvValue=m_uvArray[uvIndex];

#if FE_SAA_UNIQUE_POINTS
						const I32 pointIndex=existingPointCount+vertIndex;

						if(pointIndex>=newPointCount)
						{
							feLog("SurfaceAccessibleAbc::visit"
									" uv data exceeded point count %d\n",
									addPointCount)
							faceIndex=addFaceCount;
							break;
						}

						m_spOutputUV->set(pointIndex,uvValue);
#else
//						const I32 pointIndex=m_spOutputVertices->integer(
//								primitiveIndex,vertexIndex);

						m_spOutputUV->set(primitiveIndex,vertexIndex,uvValue);
#endif


#if FALSE
						const SpatialVector uvColor(
								0.125*uvValue[0],0.5*uvValue[1],0.0);
						m_spOutputColor->set(pointIndex,uvColor);
#endif

//						feLog("pr %d %d pt %d lk %d uv %s\n",
//								primitiveIndex,vertexIndex,pointIndex,
//								uvIndex,c_print(uvValue));

					}
				}
			}

			//* TODO kConstantScope can sometimes imply a rate as an array

			//* custom string attributes
			for(std::map<String, Array<String> >::iterator it=
					m_stringArrayMap.begin();it!=m_stringArrayMap.end();it++)
			{
				const String propName=it->first;
				Array<String>& rStringArray=it->second;

				//* NOTE avoid blank names (use default of path)
				const I32 checkCount=rStringArray.size();
				if(propName=="name")
				{
					BWORD blank=TRUE;
					for(I32 index=0;index<checkCount;index++)
					{
						if(!rStringArray[index].empty())
						{
							blank=FALSE;
							break;
						}
					}
					if(blank)
					{
						continue;
					}
				}

				Alembic::AbcGeom::GeometryScope& rScope=m_scopeMap[propName];

				if(rScope==Alembic::AbcGeom::kVertexScope ||
						rScope==Alembic::AbcGeom::kVaryingScope)
				{
					sp<SurfaceAccessorI> spOutputCustom=
							accessor(e_point,propName,
							SurfaceAccessibleI::e_createMissing);

					for(I32 index=0;index<addPointCount;index++)
					{
						const I32 pointIndex=existingPointCount+index;
						spOutputCustom->set(pointIndex,rStringArray[index]);
					}
				}
				else if(rScope==Alembic::AbcGeom::kConstantScope ||
						rScope==Alembic::AbcGeom::kUniformScope)
				{
					sp<SurfaceAccessorI> spOutputCustom=
							accessor(e_primitive,propName,
							SurfaceAccessibleI::e_createMissing);

					const String value=rStringArray[0];

					for(I32 faceIndex=0;faceIndex<addFaceCount;faceIndex++)
					{
						const I32 primitiveIndex=existingFaceCount+faceIndex;

						spOutputCustom->set(primitiveIndex,value);
					}
				}
				else if(rScope==Alembic::AbcGeom::kUnknownScope)
				{
					sp<SurfaceAccessorI> spOutputCustom=
							accessor(e_detail,propName,
							SurfaceAccessibleI::e_createMissing);

					spOutputCustom->set(0,rStringArray[0]);
				}
			}

			//* custom integer attributes
			for(std::map<String, Array<I32> >::iterator it=
					m_integerArrayMap.begin();it!=m_integerArrayMap.end();it++)
			{
				const String propName=it->first;
				Array<I32>& rIntegerArray=it->second;
				Alembic::AbcGeom::GeometryScope& rScope=m_scopeMap[propName];

				if(rScope==Alembic::AbcGeom::kVertexScope ||
						rScope==Alembic::AbcGeom::kVaryingScope)
				{
					sp<SurfaceAccessorI> spOutputCustom=
							accessor(e_point,propName,
							SurfaceAccessibleI::e_createMissing);

					for(I32 index=0;index<addPointCount;index++)
					{
						const I32 pointIndex=existingPointCount+index;
						spOutputCustom->set(pointIndex,rIntegerArray[index]);
					}
				}
				else if(rScope==Alembic::AbcGeom::kConstantScope ||
						rScope==Alembic::AbcGeom::kUniformScope)
				{
					sp<SurfaceAccessorI> spOutputCustom=
							accessor(e_primitive,propName,
							SurfaceAccessibleI::e_createMissing);

					const I32 value=rIntegerArray[0];

					for(I32 faceIndex=0;faceIndex<addFaceCount;faceIndex++)
					{
						const I32 primitiveIndex=existingFaceCount+faceIndex;

						spOutputCustom->set(primitiveIndex,value);
					}
				}
				else if(rScope==Alembic::AbcGeom::kUnknownScope)
				{
					sp<SurfaceAccessorI> spOutputCustom=
							accessor(e_detail,propName,
							SurfaceAccessibleI::e_createMissing);

					spOutputCustom->set(0,rIntegerArray[0]);
				}
			}

			//* custom real attributes
			for(std::map<String, Array<Real> >::iterator it=
					m_realArrayMap.begin();it!=m_realArrayMap.end();it++)
			{
				const String propName=it->first;
				Array<Real>& rRealArray=it->second;
				Alembic::AbcGeom::GeometryScope& rScope=m_scopeMap[propName];

				if(rScope==Alembic::AbcGeom::kVertexScope ||
						rScope==Alembic::AbcGeom::kVaryingScope)
				{
					sp<SurfaceAccessorI> spOutputCustom=
							accessor(e_point,propName,
							SurfaceAccessibleI::e_createMissing);

					for(I32 index=0;index<addPointCount;index++)
					{
						const I32 pointIndex=existingPointCount+index;
						spOutputCustom->set(pointIndex,rRealArray[index]);
					}
				}
				else if(rScope==Alembic::AbcGeom::kConstantScope ||
						rScope==Alembic::AbcGeom::kUniformScope)
				{
					sp<SurfaceAccessorI> spOutputCustom=
							accessor(e_primitive,propName,
							SurfaceAccessibleI::e_createMissing);

					const Real value=rRealArray[0];

					for(I32 faceIndex=0;faceIndex<addFaceCount;faceIndex++)
					{
						const I32 primitiveIndex=existingFaceCount+faceIndex;

						spOutputCustom->set(primitiveIndex,value);
					}
				}
			}

			//* custom vector3 attributes
			for(std::map<String, Array<SpatialVector> >::iterator it=
					m_vectorArrayMap.begin();it!=m_vectorArrayMap.end();it++)
			{
				const String propName=it->first;
				Array<SpatialVector>& rVectorArray=it->second;
				Alembic::AbcGeom::GeometryScope& rScope=m_scopeMap[propName];

				if(rScope==Alembic::AbcGeom::kVertexScope ||
						rScope==Alembic::AbcGeom::kVaryingScope)
				{
					sp<SurfaceAccessorI> spOutputCustom=
							accessor(e_point,propName,
							SurfaceAccessibleI::e_createMissing);

					for(I32 index=0;index<addPointCount;index++)
					{
						const I32 pointIndex=existingPointCount+index;
						spOutputCustom->set(pointIndex,rVectorArray[index]);
					}
				}
				else if(rScope==Alembic::AbcGeom::kConstantScope ||
						rScope==Alembic::AbcGeom::kUniformScope)
				{
					sp<SurfaceAccessorI> spOutputCustom=
							accessor(e_primitive,propName,
							SurfaceAccessibleI::e_createMissing);

					const SpatialVector value=rVectorArray[0];

					for(I32 faceIndex=0;faceIndex<addFaceCount;faceIndex++)
					{
						const I32 primitiveIndex=existingFaceCount+faceIndex;

						spOutputCustom->set(primitiveIndex,value);
					}
				}
				else if(rScope==Alembic::AbcGeom::kUnknownScope)
				{
					sp<SurfaceAccessorI> spOutputCustom=
							accessor(e_detail,propName,
							SurfaceAccessibleI::e_createMissing);

					spOutputCustom->set(0,rVectorArray[0]);
				}
			}
		}
	}
/*
	else if(Alembic::AbcGeom::ICurves::matches(a_iObj.getHeader()))
	{
#if FE_SAA_DEBUG
		if(m_mode!=e_scan)
		{
			feLog("CURVES\n");
		}
#endif
		if(m_mode==e_scan)
		{
			m_curveCount++;
		}
		if(m_mode==e_curves)
		{
			const I32 existingPointCount=m_spOutputPoint->count();

			const I32 addPointCount=m_pointArray.size();
			m_spOutputPoint->append(addPointCount);

#if FE_SAA_DEBUG
			feLog("  %d points\n",addPointCount);
#endif

			for(I32 index=0;index<addPointCount;index++)
			{
				const I32 pointIndex=existingPointCount+index;
				m_spOutputPoint->set(pointIndex,m_pointArray[index]);
			}

			const I32 existingFaceCount=m_spOutputVertices->count();

			m_primVerts.resize(1);
			m_primVerts[0].resize(addPointCount);

			Array<I32>& rVerts=m_primVerts[0];

			for(I32 vertexIndex=0;vertexIndex<addPointCount;vertexIndex++)
			{
				rVerts[vertexIndex]=existingPointCount+vertexIndex;
			}

			m_spOutputVertices->append(m_primVerts);

			m_spOutputProperties->set(existingFaceCount,e_openCurve,TRUE);

			if(m_radiusArray.size())
			{
				for(I32 vertexIndex=0;vertexIndex<addPointCount;vertexIndex++)
				{
					const I32 pointIndex=existingPointCount+vertexIndex;
					m_spOutputRadius->set(pointIndex,
							m_radiusArray[vertexIndex]);
				}
			}
		}
	}
*/
	else if(Alembic::AbcGeom::IFaceSet::matches(a_iObj.getHeader()))
	{
#if FE_SAA_DEBUG
		if(m_mode!=e_scan)
		{
			feLog("FACE SET\n");
		}
#endif
	}
	else if(Alembic::AbcGeom::IXform::matches(a_iObj.getHeader()))
	{
#if FE_SAA_DEBUG
		if(m_mode!=e_scan)
		{
			feLog("UNRECOGNIZED\n");
		}
#endif
	}

	for(U32 childIndex=0;childIndex<childCount;childIndex++)
	{
		Alembic::AbcGeom::IObject iChild(a_iObj,
				a_iObj.getChildHeader(childIndex).getName());

#if FE_SAA_DEBUG
		if(m_mode!=e_scan)
		{
			feLog("\nin \"%s\" child %d/%d \"%s\"\n",
					path.c_str(),childIndex,childCount,
					iChild.getName().c_str());
		}
#endif

		visit(iChild);
	}
}

void SurfaceAccessibleAbc::visitProperties(
	Alembic::AbcGeom::ICompoundProperty a_parent,
	String &a_rIndent,String a_parentPath,String a_path)
{
	String oldIndent=a_rIndent;

	for(U32 propIndex=0;propIndex<a_parent.getNumProperties();propIndex++)
	{
		Alembic::AbcGeom::PropertyHeader header=
				a_parent.getPropertyHeader(propIndex);

		if(header.isCompound())
		{
			visitProperty(Alembic::AbcGeom::ICompoundProperty(a_parent,
					header.getName()),a_rIndent,a_parentPath,a_path);
		}
		else
		{
			Alembic::AbcGeom::GeometryScope geometryScope=
					Alembic::AbcGeom::kUnknownScope;

#if FE_SAA_DEBUG
			String typeName="????";
#endif

			if(Alembic::AbcGeom::IV3dGeomParam::matches(header))
			{
#if FE_SAA_DEBUG
				typeName="IV3dGeomParam";
#endif

				Alembic::AbcGeom::IV3dGeomParam param(
						a_parent,header.getName());
				geometryScope=param.getScope();
			}
			else if (Alembic::AbcGeom::IV3fGeomParam::matches(header))
			{
#if FE_SAA_DEBUG
				typeName="IV3fGeomParam";
#endif

				Alembic::AbcGeom::IV3fGeomParam param(
						a_parent,header.getName());
				geometryScope=param.getScope();
			}
			else if (Alembic::AbcGeom::IV2fGeomParam::matches(header))
			{
#if FE_SAA_DEBUG
				typeName="IV2fGeomParam";
#endif

				Alembic::AbcGeom::IV2fGeomParam param(
						a_parent,header.getName());
				geometryScope=param.getScope();
			}
			else if (Alembic::AbcGeom::IP3fGeomParam::matches(header))
			{
#if FE_SAA_DEBUG
				typeName="IP3fGeomParam";
#endif

				Alembic::AbcGeom::IP3fGeomParam param(
						a_parent,header.getName());
				geometryScope=param.getScope();
			}
			else if (Alembic::AbcGeom::IN3fGeomParam::matches(header))
			{
#if FE_SAA_DEBUG
				typeName="IN3fGeomParam";
#endif

				Alembic::AbcGeom::IN3fGeomParam param(
						a_parent,header.getName());
				geometryScope=param.getScope();
			}
			else if (Alembic::AbcGeom::IC3fGeomParam::matches(header))
			{
#if FE_SAA_DEBUG
				typeName="IC3fGeomParam";
#endif

				Alembic::AbcGeom::IC3fGeomParam param(
						a_parent,header.getName());
				geometryScope=param.getScope();
			}
			if (Alembic::AbcGeom::IBoolGeomParam::matches(header))
			{
#if FE_SAA_DEBUG
				typeName="IBoolGeomParam";
#endif

				Alembic::AbcGeom::IBoolGeomParam param(
						a_parent,header.getName());
				geometryScope=param.getScope();
			}
			else if (Alembic::AbcGeom::IUcharGeomParam::matches(header))
			{
#if FE_SAA_DEBUG
				typeName="IUcharGeomParam";
#endif

				Alembic::AbcGeom::IUcharGeomParam param(
						a_parent,header.getName());
				geometryScope=param.getScope();
			}
			else if (Alembic::AbcGeom::ICharGeomParam::matches(header))
			{
#if FE_SAA_DEBUG
				typeName="ICharGeomParam";
#endif

				Alembic::AbcGeom::ICharGeomParam param(
						a_parent,header.getName());
				geometryScope=param.getScope();
			}
			else if (Alembic::AbcGeom::IUInt16GeomParam::matches(header))
			{
#if FE_SAA_DEBUG
				typeName="IUInt16GeomParam";
#endif

				Alembic::AbcGeom::IUInt16GeomParam param(
						a_parent,header.getName());
				geometryScope=param.getScope();
			}
			else if (Alembic::AbcGeom::IInt16GeomParam::matches(header))
			{
#if FE_SAA_DEBUG
				typeName="IInt16GeomParam";
#endif

				Alembic::AbcGeom::IInt16GeomParam param(
						a_parent,header.getName());
				geometryScope=param.getScope();
			}
			else if (Alembic::AbcGeom::IUInt32GeomParam::matches(header))
			{
#if FE_SAA_DEBUG
				typeName="IUInt32GeomParam";
#endif

				Alembic::AbcGeom::IUInt32GeomParam param(
						a_parent,header.getName());
				geometryScope=param.getScope();
			}
			else if (Alembic::AbcGeom::IInt32GeomParam::matches(header))
			{
#if FE_SAA_DEBUG
				typeName="IInt32GeomParam";
#endif

				Alembic::AbcGeom::IInt32GeomParam param(
						a_parent,header.getName());
				geometryScope=param.getScope();
			}
			else if (Alembic::AbcGeom::IUInt64GeomParam::matches(header))
			{
#if FE_SAA_DEBUG
				typeName="IUInt64GeomParam";
#endif

				Alembic::AbcGeom::IUInt64GeomParam param(
						a_parent,header.getName());
				geometryScope=param.getScope();
			}
			else if (Alembic::AbcGeom::IInt64GeomParam::matches(header))
			{
#if FE_SAA_DEBUG
				typeName="IInt64GeomParam";
#endif

				Alembic::AbcGeom::IInt64GeomParam param(
						a_parent,header.getName());
				geometryScope=param.getScope();
			}
			else if (Alembic::AbcGeom::IHalfGeomParam::matches(header))
			{
#if FE_SAA_DEBUG
				typeName="IHalfGeomParam";
#endif

				Alembic::AbcGeom::IHalfGeomParam param(
						a_parent,header.getName());
				geometryScope=param.getScope();
			}
			else if (Alembic::AbcGeom::IFloatGeomParam::matches(header))
			{
#if FE_SAA_DEBUG
				typeName="IFloatGeomParam";
#endif

				Alembic::AbcGeom::IFloatGeomParam param(
						a_parent,header.getName());
				geometryScope=param.getScope();
			}
			else if (Alembic::AbcGeom::IDoubleGeomParam::matches(header))
			{
#if FE_SAA_DEBUG
				typeName="IDoubleGeomParam";
#endif

				Alembic::AbcGeom::IDoubleGeomParam param(
						a_parent,header.getName());
				geometryScope=param.getScope();
			}
			else if (Alembic::AbcGeom::IStringGeomParam::matches(header))
			{
#if FE_SAA_DEBUG
				typeName="IStringGeomParam";
#endif

				Alembic::AbcGeom::IStringGeomParam param(
						a_parent,header.getName());
				geometryScope=param.getScope();
			}

			if(header.isScalar())
			{
				visitProperty(Alembic::AbcGeom::IScalarProperty(a_parent,
						header.getName()),a_rIndent,a_parentPath,a_path,
						geometryScope);
			}
			else
			{
				FEASSERT(header.isArray());

				visitProperty(Alembic::AbcGeom::IArrayProperty(a_parent,
						header.getName()),a_rIndent,a_parentPath,a_path,
						geometryScope);
			}

#if FE_SAA_DEBUG
			feLog("%s  datatype \"%s\" %s\n",a_rIndent.c_str(),typeName.c_str(),
					scopeClass(geometryScope).c_str());
#endif
		}
	}

	a_rIndent=oldIndent;
}

//* static
String SurfaceAccessibleAbc::scopeClass(
		Alembic::AbcGeom::GeometryScope a_geometryScope)
{
	switch(a_geometryScope)
	{
		case Alembic::AbcGeom::kConstantScope:
			return("kConstantScope");
		case Alembic::AbcGeom::kUniformScope:
			return("kUniformScope");
		case Alembic::AbcGeom::kVaryingScope:
			return("kVaryingScope");
		case Alembic::AbcGeom::kVertexScope:
			return("kVertexScope");
		case Alembic::AbcGeom::kFacevaryingScope:
			return("kFacevaryingScope");
		case Alembic::AbcGeom::kUnknownScope:
			return("kUnknownScope");
		default:
			;
	}

	return("<invalid scope>");
}

//* static
String SurfaceAccessibleAbc::scopeRate(
		Alembic::AbcGeom::GeometryScope a_geometryScope)
{
	switch(a_geometryScope)
	{
		case Alembic::AbcGeom::kConstantScope:
			return("de");
		case Alembic::AbcGeom::kUniformScope:
			return("pr");
		case Alembic::AbcGeom::kVaryingScope:
			return("va");
		case Alembic::AbcGeom::kVertexScope:
			return("pt");
		case Alembic::AbcGeom::kFacevaryingScope:
			return("vt");
		case Alembic::AbcGeom::kUnknownScope:
			return("UNKNOWN_RATE");
		default:
			;
	}

	return("INVALID_RATE");
}

void SurfaceAccessibleAbc::visitProperty(
	Alembic::AbcGeom::IScalarProperty a_properties,
	String a_indent,String a_parentPath,String a_path,
	Alembic::AbcGeom::GeometryScope a_geometryScope)
{
	FEASSERT(!a_properties.isArray());

#if FALSE
	const Alembic::AbcCoreAbstract::DataType &dataType=
			a_properties.getDataType();
	const Alembic::Util::uint8_t extent=dataType.getExtent();
	Alembic::Util::Dimensions dimensions(extent);
	Alembic::AbcCoreAbstract::ArraySamplePtr samplePtr=
			Alembic::AbcCoreAbstract::AllocateArraySample(dataType,dimensions);
	const U32 sampleCount=a_properties.getNumSamples();
	for(U32 sampleIndex=0;sampleIndex<sampleCount;++sampleIndex)
	{
		a_properties.get(samplePtr->getData(),
				Alembic::Abc::ISampleSelector(
				Alembic::Abc::index_t(sampleIndex)));
		arraySize=samplePtr->size();
	};
#endif

	const String propName=a_properties.getName().c_str();

	const String interpretation=
			a_properties.getMetaData().get("interpretation").c_str();

	std::stringstream dtype;
	dtype << a_properties.getDataType();
	const String dataType=dtype.str().c_str();

	const I32 sampleCount=a_properties.getNumSamples();

#if FE_SAA_DEBUG
	feLog("%sscalar \"%s\" interp '%s' type '%s' samples %d rate %s\n",
			a_indent.c_str(),propName.c_str(),
			interpretation.c_str(),dataType.c_str(),sampleCount,
			scopeRate(a_geometryScope).c_str());
#endif

	Alembic::AbcGeom::TimeSamplingPtr sampler=
			a_properties.getTimeSampling();
	std::pair<Alembic::AbcGeom::index_t,Alembic::AbcGeom::chrono_t>
			nearIndex=sampler->getNearIndex(m_frame/m_fps,sampleCount);

	I32 sampleIndex=nearIndex.first;
#if FE_SAA_DEBUG
	feLog("%s  sampleIndex %d/%d near %.6G\n",
			a_indent.c_str(),sampleIndex,sampleCount,m_frame/m_fps);
#endif
	FEASSERT(sampleIndex>=0);
	FEASSERT(!sampleCount || sampleIndex<sampleCount);

	//* NOTE Alembic::AbcGeom::IScalarProperty::get() can except
	if(sampleCount<1)
	{
		return;
	}

	if(dataType=="string")
	{
		std::string sample;
		a_properties.get(&sample,
				Alembic::Abc::ISampleSelector(
				Alembic::Abc::index_t(sampleIndex)));

		m_scopeMap[propName]=a_geometryScope;

		Array<String>& rStringArray=m_stringArrayMap[propName];
		rStringArray.resize(1);
		rStringArray[0]=sample.c_str();
	}
	else if(dataType=="int32_t")
	{
		I32 sample=0;
		a_properties.get(&sample,
				Alembic::Abc::ISampleSelector(
				Alembic::Abc::index_t(sampleIndex)));

		m_scopeMap[propName]=a_geometryScope;

		Array<I32>& rIntegerArray=m_integerArrayMap[propName];
		rIntegerArray.resize(1);
		rIntegerArray[0]=sample;
	}
	else if(dataType=="uint8_t")
	{
		U8 sample=0;
		a_properties.get(&sample,
				Alembic::Abc::ISampleSelector(
				Alembic::Abc::index_t(sampleIndex)));

		m_scopeMap[propName]=a_geometryScope;

		Array<I32>& rIntegerArray=m_integerArrayMap[propName];
		rIntegerArray.resize(1);
		rIntegerArray[0]=sample;
	}
	else if(dataType=="bool_t")
	{
		bool sample=false;
		a_properties.get(&sample,
				Alembic::Abc::ISampleSelector(
				Alembic::Abc::index_t(sampleIndex)));

		m_scopeMap[propName]=a_geometryScope;

		Array<I32>& rIntegerArray=m_integerArrayMap[propName];
		rIntegerArray.resize(1);
		rIntegerArray[0]=sample;
	}
	else if(dataType=="float64_t[3]")
	{
		F64 sample[3];
		a_properties.get(&sample,
				Alembic::Abc::ISampleSelector(
				Alembic::Abc::index_t(sampleIndex)));

		m_scopeMap[propName]=a_geometryScope;

		Array<SpatialVector>& rVectorArray=m_vectorArrayMap[propName];
		rVectorArray.resize(1);
		set(rVectorArray[0],sample[0],sample[1],sample[2]);
	}

	outlineProperty(a_indent,propName,interpretation,dataType,"",0,sampleCount);
}

void SurfaceAccessibleAbc::visitProperty(
	Alembic::AbcGeom::IArrayProperty a_properties,
	String a_indent,String a_parentPath,String a_path,
	Alembic::AbcGeom::GeometryScope a_geometryScope)
{
	FEASSERT(a_properties.isArray());

	const String parentName=a_properties.getParent().getName().c_str();
	const String propName=a_properties.getName().c_str();

	std::stringstream dtype;
	dtype << a_properties.getDataType();
	const String dataType=dtype.str().c_str();

	Alembic::AbcCoreAbstract::ArraySamplePtr samplePtr;
	const I32 sampleCount = a_properties.getNumSamples();
	const String interpretation=
			a_properties.getMetaData().get("interpretation").c_str();

#if FE_SAA_DEBUG
	feLog("%sarray \"%s\" interp '%s' type '%s' samples %d\n",
			a_indent.c_str(),propName.c_str(),
			interpretation.c_str(),dataType.c_str(),sampleCount);
#endif

	Alembic::AbcGeom::TimeSamplingPtr sampler=
			a_properties.getTimeSampling();
	std::pair<Alembic::AbcGeom::index_t,Alembic::AbcGeom::chrono_t>
			nearIndex=sampler->getNearIndex(m_frame/m_fps,sampleCount);

	I32 sampleIndex=nearIndex.first;
#if FE_SAA_DEBUG
	feLog("%s  sampleIndex %d/%d near %.6G\n",
			a_indent.c_str(),sampleIndex,sampleCount,m_frame/m_fps);
#endif
	FEASSERT(sampleIndex>=0);
	FEASSERT(!sampleCount || sampleIndex<sampleCount);

	a_properties.get(samplePtr,
			Alembic::Abc::ISampleSelector(
			Alembic::Abc::index_t(sampleIndex)));

	const U32 arrayCount=samplePtr->size();
#if FE_SAA_DEBUG
	feLog("%s  arrayCount %d\n",a_indent.c_str(),arrayCount);
#endif
	if(!arrayCount)
	{
		return;
	}

	outlineProperty(a_indent,propName,interpretation,dataType,
			scopeRate(a_geometryScope),arrayCount,sampleCount);

	if(dataType=="string")
	{
		std::string* strArray=(std::string*)samplePtr->getData();
		const String string=strArray[0].c_str();

#if FE_SAA_VERBOSE
		for(U32 arrayIndex=0;arrayIndex<arrayCount && arrayIndex<8;arrayIndex++)
		{
			const String nextString=strArray[arrayIndex].c_str();
			if(!arrayIndex || string!=nextString)
			{
				feLog("%s  %d/%d \"%s\"\n",a_indent.c_str(),
						arrayIndex,arrayCount,nextString.c_str());
			}
		}
#endif

		if(propName=="worldreference")
		{
			Real val[16];
			sscanf(string.c_str(),
					"%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f",
					&val[0],&val[1],&val[2],&val[3],
					&val[4],&val[5],&val[6],&val[7],
					&val[8],&val[9],&val[10],&val[11],
					&val[12],&val[13],&val[14],&val[15]);

			set(m_refWorld,val);
		}
		else
		{
			m_scopeMap[propName]=a_geometryScope;

			Array<String>& rStringArray=m_stringArrayMap[propName];
			rStringArray.resize(arrayCount);
			for(U32 arrayIndex=0;arrayIndex<arrayCount;arrayIndex++)
			{
				rStringArray[arrayIndex]=strArray[arrayIndex].c_str();
			}
		}
	}
	else if(dataType=="int32_t")
	{
		I32* i32Array=(I32*)samplePtr->getData();

#if FE_SAA_VERBOSE
		I32 minIndex=0;
		I32 maxIndex=0;
		I32 lastInteger=i32Array[0];

		for(U32 arrayIndex=0;arrayIndex<arrayCount;arrayIndex++)
		{
			const I32 nextInteger=i32Array[arrayIndex];
			if((!arrayIndex || nextInteger!=lastInteger) && arrayIndex<8)
			{
				feLog("%s  %d/%d %d\n",a_indent.c_str(),
						arrayIndex,arrayCount,nextInteger);
			}
			lastInteger=nextInteger;

			if(!arrayIndex || minIndex>nextInteger)
			{
				minIndex=nextInteger;
			}
			if(!arrayIndex || maxIndex<nextInteger)
			{
				maxIndex=nextInteger;
			}
		}
		feLog("%s  min %d max %d\n",a_indent.c_str(),
				minIndex,maxIndex);
#endif

		if(propName==".faceCounts" || propName=="nVertices")
		{
			m_vertCountArray.resize(arrayCount);
			for(U32 arrayIndex=0;arrayIndex<arrayCount;arrayIndex++)
			{
				m_vertCountArray[arrayIndex]=i32Array[arrayIndex];
			}
		}
		else if(propName==".faceIndices")
		{
			m_vertIndexArray.resize(arrayCount);
			for(U32 arrayIndex=0;arrayIndex<arrayCount;arrayIndex++)
			{
				m_vertIndexArray[arrayIndex]=i32Array[arrayIndex];
			}
		}
		else if(propName==".faces")
		{
			const I32 firstFace=m_firstFaceMap[a_parentPath];
			for(U32 arrayIndex=0;arrayIndex<arrayCount;arrayIndex++)
			{
//				feLog("face %d/%d %d+%d %s %s\n",arrayIndex,arrayCount,
//						firstFace,i32Array[arrayIndex],
//						a_parentPath.c_str(),a_path.c_str());

				const I32 primitiveIndex=firstFace+i32Array[arrayIndex];
				m_spOutputName->set(primitiveIndex,a_path);
				m_spOutputPart->set(primitiveIndex,basenameOf(a_path));
			}
		}
		else
		{
			m_scopeMap[propName]=a_geometryScope;

			Array<I32>& rIntegerArray=m_integerArrayMap[propName];
			rIntegerArray.resize(arrayCount);
			for(U32 arrayIndex=0;arrayIndex<arrayCount;arrayIndex++)
			{
				rIntegerArray[arrayIndex]=i32Array[arrayIndex];
			}
		}
	}
	else if(dataType=="uint32_t")
	{
		U32* u32Array=(U32*)samplePtr->getData();

#if FE_SAA_VERBOSE
		U32 minIndex=0;
		U32 maxIndex=0;
		U32 lastInteger=u32Array[0];

		for(U32 arrayIndex=0;arrayIndex<arrayCount;arrayIndex++)
		{
			const U32 nextInteger=u32Array[arrayIndex];
			if((!arrayIndex || nextInteger!=lastInteger) && arrayIndex<8)
			{
				feLog("%s  %d/%d %d\n",a_indent.c_str(),
						arrayIndex,arrayCount,nextInteger);
			}
			lastInteger=nextInteger;

			if(!arrayIndex || minIndex>nextInteger)
			{
				minIndex=nextInteger;
			}
			if(!arrayIndex || maxIndex<nextInteger)
			{
				maxIndex=nextInteger;
			}
		}
		feLog("%s  min %d max %d\n",a_indent.c_str(),
				minIndex,maxIndex);
#endif

		if(parentName=="uv" && propName==".indices")
		{
			m_uvLookupArray.resize(arrayCount);

			for(U32 arrayIndex=0;arrayIndex<arrayCount;arrayIndex++)
			{
				m_uvLookupArray[arrayIndex]=u32Array[arrayIndex];
			}
		}
		else
		{
			m_scopeMap[propName]=a_geometryScope;

			Array<I32>& rIntegerArray=m_integerArrayMap[propName];
			rIntegerArray.resize(arrayCount);
			for(U32 arrayIndex=0;arrayIndex<arrayCount;arrayIndex++)
			{
				rIntegerArray[arrayIndex]=u32Array[arrayIndex];
			}
		}
	}
	else if(dataType=="uint8_t")
	{
		U8* u8Array=(U8*)samplePtr->getData();

		m_scopeMap[propName]=a_geometryScope;

		Array<I32>& rIntegerArray=m_integerArrayMap[propName];
		rIntegerArray.resize(arrayCount);
		for(U32 arrayIndex=0;arrayIndex<arrayCount;arrayIndex++)
		{
			rIntegerArray[arrayIndex]=u8Array[arrayIndex];
		}
	}
	else if(dataType=="float32_t[2]")
	{
		F32* f32Array=(F32*)samplePtr->getData();

#if FE_SAA_VERBOSE
		for(U32 arrayIndex=0;arrayIndex<arrayCount && arrayIndex<8;arrayIndex++)
		{
			const F32* pVector=&f32Array[arrayIndex*2];
			const Vector2 value(pVector[0],pVector[1]);

			feLog("%s  %d/%d %s\n",a_indent.c_str(),
					arrayIndex,arrayCount,c_print(value));
		}
#endif

		if(parentName=="uv" && propName==".vals")
		{
			m_uvScope=a_geometryScope;

			m_uvArray.resize(arrayCount);

			for(U32 arrayIndex=0;arrayIndex<arrayCount;arrayIndex++)
			{
				const F32* pVector=&f32Array[arrayIndex*2];

				set(m_uvArray[arrayIndex],
						pVector[0],pVector[1],0.0);
			}
		}
	}
	else if(dataType=="float32_t[3]")
	{
		F32* f32Array=(F32*)samplePtr->getData();

#if FE_SAA_VERBOSE
		for(U32 arrayIndex=0;arrayIndex<arrayCount && arrayIndex<8;arrayIndex++)
		{
			const F32* pVector=&f32Array[arrayIndex*3];
			const SpatialVector value(pVector[0],pVector[1],pVector[2]);

			feLog("%s  %d/%d %s\n",a_indent.c_str(),
					arrayIndex,arrayCount,c_print(value));
		}
#endif

		if(propName=="P")
		{
			m_pointArray.resize(arrayCount);

			for(U32 arrayIndex=0;arrayIndex<arrayCount;arrayIndex++)
			{
				const F32* pVector=&f32Array[arrayIndex*3];

				set(m_pointArray[arrayIndex],
						pVector[0],pVector[1],pVector[2]);
			}
		}
		else if(propName=="N" || propName=="normal")
		{
			m_normalScope=a_geometryScope;

			m_normalArray.resize(arrayCount);

			for(U32 arrayIndex=0;arrayIndex<arrayCount;arrayIndex++)
			{
				const F32* pVector=&f32Array[arrayIndex*3];

				set(m_normalArray[arrayIndex],
						pVector[0],pVector[1],pVector[2]);
			}
		}
		else if(propName=="Cd" || propName=="color")
		{
			m_colorScope=a_geometryScope;

			m_colorArray.resize(arrayCount);

			for(U32 arrayIndex=0;arrayIndex<arrayCount;arrayIndex++)
			{
				const F32* pVector=&f32Array[arrayIndex*3];

				set(m_colorArray[arrayIndex],
						pVector[0],pVector[1],pVector[2]);
			}
		}
		else
		{
			m_scopeMap[propName]=a_geometryScope;

			Array<SpatialVector>& rVectorArray=m_vectorArrayMap[propName];
			rVectorArray.resize(arrayCount);

			for(U32 arrayIndex=0;arrayIndex<arrayCount;arrayIndex++)
			{
				const F32* pVector=&f32Array[arrayIndex*3];

				set(rVectorArray[arrayIndex],
						pVector[0],pVector[1],pVector[2]);
			}
		}
	}
	else if(dataType=="float64_t[3]")
	{
		F64* f64Array=(F64*)samplePtr->getData();

#if FE_SAA_VERBOSE
		for(U32 arrayIndex=0;arrayIndex<arrayCount && arrayIndex<8;arrayIndex++)
		{
			const F64* pVector=&f64Array[arrayIndex*3];
			const SpatialVector value(pVector[0],pVector[1],pVector[2]);

			feLog("%s  %d/%d %s\n",a_indent.c_str(),
					arrayIndex,arrayCount,c_print(value));
		}
#endif

		if(propName=="xgen_Pref")
		{
			m_pointRefArray.resize(arrayCount);

			for(U32 arrayIndex=0;arrayIndex<arrayCount;arrayIndex++)
			{
				const F64* pVector=&f64Array[arrayIndex*3];

				set(m_pointRefArray[arrayIndex],
						pVector[0],pVector[1],pVector[2]);
			}
		}

		if(!m_refMode || propName!="xgen_Pref")
		{
			m_scopeMap[propName]=a_geometryScope;

			Array<SpatialVector>& rVectorArray=m_vectorArrayMap[propName];
			rVectorArray.resize(arrayCount);

			for(U32 arrayIndex=0;arrayIndex<arrayCount;arrayIndex++)
			{
				const F64* pVector=&f64Array[arrayIndex*3];

				set(rVectorArray[arrayIndex],
						pVector[0],pVector[1],pVector[2]);
			}
		}
	}
	else if(dataType=="float64_t")
	{
		F64* f64Array=(F64*)samplePtr->getData();

#if FE_SAA_VERBOSE
		for(U32 arrayIndex=0;arrayIndex<arrayCount && arrayIndex<8;arrayIndex++)
		{
			const F64 value=f64Array[arrayIndex];

			feLog("%s  %d/%d %.6G\n",a_indent.c_str(),
					arrayIndex,arrayCount,value);
		}
#endif

		if(propName=="radiusf")
		{
			m_radiusArray.resize(arrayCount);

			for(U32 arrayIndex=0;arrayIndex<arrayCount;arrayIndex++)
			{
				m_radiusArray[arrayIndex]=f64Array[arrayIndex];
			}
		}
		else
		{
			m_scopeMap[propName]=a_geometryScope;

			Array<Real>& rRealArray=m_realArrayMap[propName];
			rRealArray.resize(arrayCount);
			for(U32 arrayIndex=0;arrayIndex<arrayCount;arrayIndex++)
			{
				rRealArray[arrayIndex]=f64Array[arrayIndex];
			}
		}
	}
	else if(dataType=="float32_t")
	{
		F32* f32Array=(F32*)samplePtr->getData();

#if FE_SAA_VERBOSE
		for(U32 arrayIndex=0;arrayIndex<arrayCount && arrayIndex<8;arrayIndex++)
		{
			const F32 value=f32Array[arrayIndex];

			feLog("%s  %d/%d %.6G\n",a_indent.c_str(),
					arrayIndex,arrayCount,value);
		}
#endif

		m_scopeMap[propName]=a_geometryScope;

		Array<Real>& rRealArray=m_realArrayMap[propName];
		rRealArray.resize(arrayCount);
		for(U32 arrayIndex=0;arrayIndex<arrayCount;arrayIndex++)
		{
			rRealArray[arrayIndex]=f32Array[arrayIndex];
		}
	}
}

void SurfaceAccessibleAbc::visitProperty(
	Alembic::AbcGeom::ICompoundProperty a_properties,
	String &a_rIndent,String a_parentPath,String a_path)
{
	const String propName=a_properties.getName().c_str();
	String schema=a_properties.getMetaData().get("schema").c_str();

	outlineProperty(a_rIndent,propName,schema,"","",0,0);

#if FE_SAA_DEBUG
	feLog("%scompound \"%s\" schema '%s'\n",
			a_rIndent.c_str(),propName.c_str(),schema.c_str());
#endif

	String oldIndent=a_rIndent;
	a_rIndent+=FE_SAA_INDENTION;

	visitProperties(a_properties,a_rIndent,a_parentPath,a_path);

	a_rIndent=oldIndent;
}

void SurfaceAccessibleAbc::outlineProperty(String a_indent,String a_propName,
	String a_interpretation,String a_dataType,String a_rate,I32 a_arrayCount,
	I32 a_sampleCount)
{
	const String briefType=a_dataType
			.substitute("string","str")
			.substitute("float","f")
			.substitute("uint","u")
			.substitute("int","i")
			.substitute("_t[","[")
			.chop("_t");

	String line;
	line.sPrintf("%s%s",a_indent.c_str(),a_propName.c_str());
	if(!a_interpretation.empty())
	{
		line.catf(" (%s)",a_interpretation.c_str());
	}
	if(!briefType.empty())
	{
		line.catf(" %s",briefType.c_str());
	}
	if(!a_rate.empty())
	{
		line.catf(" %s",a_rate.c_str());
	}
	if(a_arrayCount)
	{
		line.catf("%s[%d]",a_rate.empty()? " ": "",a_arrayCount);
	}
	if(a_sampleCount)
	{
		line.catf(" x%d",a_sampleCount);
	}
	outlineAppend(line);
}

//* static
String SurfaceAccessibleAbc::basenameOf(String a_path)
{
	String base;
	String buffer=a_path;
	String token;
	while(!(token=buffer.parse("\"","/")).empty())
	{
		base=token;
	}
	return base;
}

} /* namespace ext */
} /* namespace fe */
