/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __oplab_FusionOp_h__
#define __oplab_FusionOp_h__
namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Operator to cleanly rebuild intersections between surfaces

	@ingroup oplab
*//***************************************************************************/
class FE_DL_EXPORT FusionOp:
	public OperatorSurfaceCommon,
	public Initialize<FusionOp>
{
	public:

					FusionOp(void)											{}
virtual				~FusionOp(void)											{}

		void		initialize(void);

					//* As HandlerI
virtual	void		handle(Record& a_rSignal);

	private:
};

} /* namespace ext */
} /* namespace fe */

#endif /* __oplab_FusionOp_h__ */
