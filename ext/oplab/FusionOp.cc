/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <oplab/oplab.pmh>

using namespace fe;
using namespace fe::ext;

void FusionOp::initialize(void)
{
	catalog<String>("icon")="FE_alpha";

	catalog<bool>("limitSteps")=false;
	catalog<String>("limitSteps","label")="Limit Steps";
	catalog<bool>("limitSteps","joined")=true;
	catalog<String>("limitSteps","hint")=
			"Examine results at any particular step.";

	catalog<I32>("stepLimit")=1;
	catalog<I32>("stepLimit","min")=0;
	catalog<I32>("stepLimit","high")=10;
	catalog<I32>("stepLimit","max")=100;
	catalog<String>("stepLimit","enabler")="limitSteps";
	catalog<String>("stepLimit","label")="Step Limit";
	catalog<String>("stepLimit","hint")=
			"Stop processing after the given number of steps.";

	catalog< sp<Component> >("Input Surface");
}

void FusionOp::handle(Record& a_rSignal)
{
//	catalog<String>("warning")="This tool is not ready for testing.";

	const BWORD limitSteps=catalog<bool>("limitSteps");
	const I32 stepLimit=catalog<I32>("stepLimit");

	sp<SurfaceAccessorI> spOutputVertices;
	if(!accessOutput(spOutputVertices,a_rSignal,e_primitive,e_vertices)) return;

	sp<SurfaceAccessorI> spOutputPoint;
	if(!accessOutput(spOutputPoint,a_rSignal,e_point,e_position)) return;

	sp<SurfaceI> spInput;
	if(!access(spInput,"Input Surface")) return;

	sp<DrawI> spDrawDebug;
	if(!accessGuide(spDrawDebug,a_rSignal)) return;

	//* TODO store split edges to fix later

	std::map<U32, Array<I32> > rebuildMap;

	I32 steps=0;
	while(!limitSteps || steps<stepLimit)
	{
		U32 changes=0;
		const U32 count=spOutputVertices->count();
		for(U32 index=0;index<count && (!limitSteps || steps<stepLimit);index++)
		{
			const I32 subCount=spOutputVertices->subCount(index);
			feLog("FusionOp::handle prim %d/%d subCount %d\n",
					index,count,subCount);
			if(!subCount)
			{
				continue;
			}
			for(I32 subIndex0=0;
					subIndex0<subCount && (!limitSteps || steps<stepLimit);
					subIndex0++)
			{
				const I32 subIndex1=(subIndex0+1)%subCount;
				const I32 subIndex2=(subIndex0+2)%subCount;

				const SpatialVector point0=
						spOutputVertices->spatialVector(index,subIndex0);
				const SpatialVector point1=
						spOutputVertices->spatialVector(index,subIndex1);
				const SpatialVector delta=point1-point0;
				const Real maxDistance=magnitude(delta);
				const SpatialVector direction=
						delta*(maxDistance>0.0? 1.0/maxDistance: 1.0);

				feLog("  sub %d/%d %s ray %s\n",subIndex0,subCount,
						c_print(point0),c_print(direction));

				sp<SurfaceTriangles::Impact> spTriImpact=
						spInput->rayImpact(point0,direction,0.999*maxDistance);
				if(!spTriImpact.isValid())
				{
					continue;
				}

				I32 reIndex=spTriImpact->primitiveIndex();
				Real distance=spTriImpact->distance();
				Barycenter<Real> barycenter=spTriImpact->barycenter();

				feLog("  hit %d dist %.6G bary %s\n",
						reIndex,distance,c_print(barycenter));
				if(barycenter[0]<1e-6 || barycenter[1]<1e-6 ||
					barycenter[0]+barycenter[1]>1.0-1e-6)
				{
					//* just clipping an edge
					continue;
				}

				if(rebuildMap.find(reIndex)!=rebuildMap.end())
				{
					//* calc directly in case the triangle changed
					distance=RayTriangleIntersect<Real>::solve(
							spOutputVertices->spatialVector(reIndex,0),
							spOutputVertices->spatialVector(reIndex,1),
							spOutputVertices->spatialVector(reIndex,2),
							point0,direction,barycenter);

					feLog("  rebuild dist %.6G bary %s\n",
							distance,c_print(barycenter));

					if(distance<1e-6 ||
							barycenter[0]<1e-6 || barycenter[1]<1e-6 ||
							barycenter[0]+barycenter[1]>1.0-1e-6)
					{
						Array<I32>& rRebuilds=rebuildMap[reIndex];
						const U32 rebuildCount=rRebuilds.size();
						feLog("  rebuild count %d\n",rebuildCount);

						U32 rebuild;
						for(rebuild=0;rebuild<rebuildCount;rebuild++)
						{
							reIndex=rRebuilds[rebuild];
							distance=RayTriangleIntersect<Real>::solve(
									spOutputVertices->spatialVector(reIndex,0),
									spOutputVertices->spatialVector(reIndex,1),
									spOutputVertices->spatialVector(reIndex,2),
									point0,direction,barycenter);
							feLog("  rebuild %d/%d prim %d dist %.6G bary %s\n",
									rebuild,rebuildCount,reIndex,
									distance,c_print(barycenter));
							if(barycenter[0]<1e-6 || barycenter[1]<1e-6 ||
								barycenter[0]+barycenter[1]>1.0-1e-6)
							{
								//* just clipping an edge
//								continue;
							}
							if(distance>1e-6)
							{
								break;
							}
						}
						if(rebuild!=rebuildCount)
						{
							break;
						}
						BWORD found=FALSE;
						for(rebuild=0;rebuild<rebuildCount && !found;rebuild++)
						{
							reIndex=rRebuilds[rebuild];

							for(U32 subIndex=0;subIndex<3;subIndex++)
							{
								const SpatialVector subPoint0=
										spOutputVertices->spatialVector(
										reIndex,subIndex);

								SpatialVector subDirection;
								SpatialVector subIntersection;
								const Real subDistance=
										PointTriangleNearest<Real>::solve(
										spOutputVertices->spatialVector(
										reIndex,0),
										spOutputVertices->spatialVector(
										reIndex,1),
										spOutputVertices->spatialVector(
										reIndex,2),
										subPoint0,subDirection,
										subIntersection,barycenter);
								feLog("  subIntersection %s"
										" near %.6G bary %s\n",
										c_print(subIntersection),
										subDistance,c_print(barycenter));
								if(subDistance<1e6)
								{
									distance=magnitude(subIntersection-point0);
									found=TRUE;
									break;
								}
							}
						}
						if(rebuild==rebuildCount)
						{
							continue;
						}
					}
				}

				const SpatialVector intersection=point0+distance*direction;

				//* find impact from other primitive
				const U32 reSubCount=spOutputVertices->subCount(reIndex);

				feLog("  intersection %s prim %d subCount %d\n",
						c_print(intersection),reIndex,reSubCount);

				if(!reSubCount)
				{
					continue;
				}

				const SpatialVector vertex0=
						spOutputVertices->spatialVector(index,0);
				const SpatialVector vertex1=
						spOutputVertices->spatialVector(index,1);
				const SpatialVector vertex2=
						spOutputVertices->spatialVector(index,2);

				Barycenter<Real> reBarycenter;
				SpatialVector reIntersection;
				U32 reSubIndex0;
				U32 reSubIndex1;
				for(reSubIndex0=0;reSubIndex0<reSubCount;reSubIndex0++)
				{
					reSubIndex1=(reSubIndex0+1)%reSubCount;

					const SpatialVector rePoint0=
							spOutputVertices->spatialVector(
							reIndex,reSubIndex0);
					const SpatialVector rePoint1=
							spOutputVertices->spatialVector(
							reIndex,reSubIndex1);
					const SpatialVector reDelta=rePoint1-rePoint0;
					const Real reMaxDistance=magnitude(reDelta);
					const SpatialVector reDirection=
							reDelta*(reMaxDistance>0.0? 1.0/reMaxDistance: 1.0);

					const Real reDistance=RayTriangleIntersect<Real>::solve(
							vertex0,vertex1,vertex2,rePoint0,reDirection,
							reBarycenter);

					feLog("  re %d/%d %s ray %s dist %.6G vs %.6G\n",
							reSubIndex0,reSubCount,
							c_print(rePoint0),c_print(reDirection),
							reDistance,reMaxDistance);

					if(reDistance>=1e-6 && reDistance<0.999*reMaxDistance)
					{
						reIntersection=rePoint0+reDistance*reDirection;

						feLog("  reIntersection %s\n",c_print(reIntersection));
						break;
					}
				}
				const BWORD onEdge=(reSubIndex0==reSubCount);
				if(onEdge)
				{
					for(reSubIndex0=0;reSubIndex0<reSubCount;reSubIndex0++)
					{
						const SpatialVector rePoint0=
								spOutputVertices->spatialVector(
								reIndex,reSubIndex0);

						SpatialVector reDirection;
						const Real reDistance=PointTriangleNearest<Real>::solve(
								vertex0,vertex1,vertex2,rePoint0,
								reDirection,reIntersection,reBarycenter);
						feLog("  reIntersection %s near %.6G bary %s\n",
								c_print(reIntersection),
								reDistance,c_print(reBarycenter));
						if(reDistance<1e6)
						{
							break;
						}
					}

					if(reSubIndex0==reSubCount)
					{
						continue;
					}

					reSubIndex1=(reSubIndex0+1)%reSubCount;
				}
				const U32 reSubIndex2=(reSubIndex0+2)%reSubCount;

				const BWORD singular=
						(magnitude(reIntersection-intersection) < 1e-6);

				const Color green(0.0,0.5,0.0,1.0);
				SpatialVector line[2];

				line[0]=intersection;
				line[1]=reIntersection;

				spDrawDebug->drawLines(line,NULL,2,DrawI::e_strip,false,&green);

				//* add both points to surface
				const I32 pointIndex0=
						spOutputVertices->integer(index,subIndex0);
				const I32 pointIndex1=
						spOutputVertices->integer(index,subIndex1);
				const I32 pointIndex2=
						spOutputVertices->integer(index,subIndex2);

				feLog(" was prim %d vert %d %d %d\n",
						index,pointIndex0,pointIndex1,pointIndex2);

				const I32 rePointIndex0=
						spOutputVertices->integer(reIndex,reSubIndex0);
				const I32 rePointIndex1=
						spOutputVertices->integer(reIndex,reSubIndex1);
				const I32 rePointIndex2=
						spOutputVertices->integer(reIndex,reSubIndex2);

				feLog(" was re prim %d vert %d %d %d\n",
						index,rePointIndex0,rePointIndex1,rePointIndex2);

				const I32 pointDuplicate=onEdge? rePointIndex0:
						spOutputPoint->duplicate(pointIndex0);
				const I32 rePointDuplicate=singular? pointDuplicate:
						spOutputPoint->duplicate(rePointIndex0);

				if(!onEdge)
				{
					spOutputPoint->set(pointDuplicate,reIntersection);
				}
				if(!singular)
				{
					spOutputPoint->set(rePointDuplicate,intersection);
				}

				//* rewire two existing triangles
				spOutputVertices->set(index,0,pointDuplicate);
				spOutputVertices->set(index,1,pointIndex1);
				spOutputVertices->set(index,2,pointIndex2);
				feLog(" rewire prim %d vert %d %d %d\n",
						index,pointDuplicate,pointIndex1,pointIndex2);

				spOutputVertices->set(reIndex,0,rePointDuplicate);
				spOutputVertices->set(reIndex,1,rePointIndex1);
				spOutputVertices->set(reIndex,2,rePointIndex2);
				feLog(" rewire prim %d vert %d %d %d\n",
						reIndex,rePointDuplicate,rePointIndex1,rePointIndex2);

				//* build six new triangles, three for each existing

				if(!onEdge || singular)
				{
					const I32 triDuplicate0=spOutputVertices->duplicate(index);
					rebuildMap[index].push_back(triDuplicate0);

					spOutputVertices->set(triDuplicate0,0,pointDuplicate);
					spOutputVertices->set(triDuplicate0,1,pointIndex2);
					spOutputVertices->set(triDuplicate0,2,pointIndex0);
					feLog(" new prim %d vert %d %d %d\n",triDuplicate0,
							pointDuplicate,pointIndex2,pointIndex0);
				}

				if(!singular)
				{
					const I32 triDuplicate1=spOutputVertices->duplicate(index);
					rebuildMap[index].push_back(triDuplicate1);

					spOutputVertices->set(triDuplicate1,0,pointDuplicate);
					spOutputVertices->set(triDuplicate1,1,rePointDuplicate);
					spOutputVertices->set(triDuplicate1,2,pointIndex1);
					feLog(" new prim %d vert %d %d %d\n",triDuplicate1,
							pointDuplicate,rePointDuplicate,pointIndex1);

					const I32 triDuplicate2=spOutputVertices->duplicate(index);
					rebuildMap[index].push_back(triDuplicate2);

					spOutputVertices->set(triDuplicate2,0,pointIndex0);
					spOutputVertices->set(triDuplicate2,1,rePointDuplicate);
					spOutputVertices->set(triDuplicate2,2,pointDuplicate);
					feLog(" new prim %d vert %d %d %d\n",triDuplicate2,
							pointIndex0,rePointDuplicate,pointDuplicate);

					if(!onEdge)
					{
						const I32 reTriDuplicate0=
								spOutputVertices->duplicate(reIndex);
						rebuildMap[reIndex].push_back(reTriDuplicate0);

						spOutputVertices->set(reTriDuplicate0,0,rePointIndex0);
						spOutputVertices->set(reTriDuplicate0,1,pointDuplicate);
						spOutputVertices->set(reTriDuplicate0,2,rePointDuplicate);
						feLog(" new prim %d vert %d %d %d\n",reTriDuplicate0,
								rePointIndex0,pointDuplicate,rePointDuplicate);
					}

					const I32 reTriDuplicate1=
							spOutputVertices->duplicate(reIndex);
					rebuildMap[reIndex].push_back(reTriDuplicate1);

					spOutputVertices->set(reTriDuplicate1,0,rePointDuplicate);
					spOutputVertices->set(reTriDuplicate1,1,pointDuplicate);
					spOutputVertices->set(reTriDuplicate1,2,rePointIndex1);
					feLog(" new prim %d vert %d %d %d\n",reTriDuplicate1,
							rePointDuplicate,pointDuplicate,rePointIndex1);

					const I32 reTriDuplicate2=
							spOutputVertices->duplicate(reIndex);
					rebuildMap[reIndex].push_back(reTriDuplicate2);

					spOutputVertices->set(reTriDuplicate2,0,rePointDuplicate);
					spOutputVertices->set(reTriDuplicate2,1,rePointIndex2);
					spOutputVertices->set(reTriDuplicate2,2,rePointIndex0);
					feLog(" new prim %d vert %d %d %d\n",reTriDuplicate2,
							rePointDuplicate,rePointIndex2,rePointIndex0);
				}

				steps++;
				changes++;
			}
		}
		if(!changes)
		{
			break;
		}
	}
}
