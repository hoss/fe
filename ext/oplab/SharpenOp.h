/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __oplab_SharpenOp_h__
#define __oplab_SharpenOp_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
    @brief Sharpen based on neighbors

	@ingroup oplab
*//***************************************************************************/
class FE_DL_EXPORT SharpenOp:
	public OperatorSurfaceCommon,
	public Initialize<SharpenOp>
{
	public:
				SharpenOp(void)												{}
virtual			~SharpenOp(void)											{}

		void	initialize(void);

				//* As HandlerI
virtual void	handle(Record& a_rSignal);

	private:
		sp<DrawMode>			m_spDots;

								//* feedback dots
		Array<SpatialVector>	m_locationArray;
		Array<Color>			m_colorArray;
		Array<Real>				m_heightArray;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __oplab_SharpenOp_h__ */
