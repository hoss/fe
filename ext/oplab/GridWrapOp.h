/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __oplab_GridWrapOp_h__
#define __oplab_GridWrapOp_h__
namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Operator to move an attachable surface using a driving surface

	@ingroup oplab
*//***************************************************************************/
class FE_DL_EXPORT GridWrapOp:
	public OperatorThreaded,
	public Initialize<GridWrapOp>
{
	public:

					GridWrapOp(void)										{}
virtual				~GridWrapOp(void)										{}

		void		initialize(void);

					//* As HandlerI
virtual	void		handle(Record& a_rSignal);

					using OperatorThreaded::run;

					// TODO
virtual	void		run(I32 a_id,sp<SpannedRange> a_spRange)				{}

	private:

	class GridNode
	{
		public:
								GridNode(void):
									m_weight(0.0)
								{	setIdentity(m_transform); }

			SpatialTransform	m_transform;
			Real				m_weight;
	};

		MatrixPower<SpatialTransform>	m_matrixPower;
		sp<DrawI>						m_spDrawDebug;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __oplab_GridWrapOp_h__ */
