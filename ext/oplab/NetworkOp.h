/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __oplab_NetworkOp_h__
#define __oplab_NetworkOp_h__
namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Communicate a mesh as a RecordGroup

	@ingroup oplab
*//***************************************************************************/
class FE_DL_EXPORT NetworkOp:
	public OperatorSurfaceCommon,
	public Initialize<NetworkOp>
{
	public:

	class Handler:
		public ObjectSafeShared<Handler>,
		virtual public HandlerI
	{
		public:
					Handler(void)											{}
	virtual			~Handler(void)											{}

	virtual	void	handle(Record &a_signal);

			void	bind(sp<Catalog> a_spCatalog)
					{	m_spActionCatalog=a_spCatalog; }

			void	bind(sp<NetworkOp> a_spNetworkOp)
					{	m_hpNetworkOp=a_spNetworkOp; }

			String	state(void) const;
			void	setState(String a_state);

			String	action(String a_key) const;
			void	setAction(String a_key,String a_state);

			Record	record(void) const;

			void	setSelf(String a_source)	{ m_self=a_source; }

		private:
			sp<Catalog>			m_spActionCatalog;
			hp<NetworkOp>		m_hpNetworkOp;
			String				m_self;
			String				m_state;
			Record				m_record;
	};

								NetworkOp(void):
									m_brushed(FALSE),
									m_lastFrame(0.0),
									m_commandPid(0)							{}
virtual							~NetworkOp(void)								{}

		void					initialize(void);

								//* As HandlerI
virtual	void					handleBind(sp<SignalerI> a_spSignalerI,
									sp<Layout> a_spLayout);
virtual	void					handle(Record& a_rSignal);

		Accessor<int>			m_ttl;
		Accessor<String>		m_source;
		Accessor<String>		m_text;
		Accessor<Record>		m_model;

	private:
		void					sendText(Record a_signal,String a_message);

		sp<SurfaceAccessibleI>	importSurface(Record& a_rSignal,String a_args);
		void					relayMessages(void);

		BWORD					m_brushed;
		Real					m_lastFrame;
		I32						m_commandPid;

		sp<Scope>				m_spScope;
		sp<Layout>				m_spLayoutHB;
		sp<Layout>				m_spLayoutMessage;
		sp<Layout>				m_spLayoutSurface;

		sp<SignalerI>			m_spSignaler;
		sp<Handler>				m_spHandler;
		sp<ServerI>				m_spServer;
		sp<ClientI>				m_spClient;
		sp<OperatorSurfaceI>	m_spImportOp;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __oplab_NetworkOp_h__ */
