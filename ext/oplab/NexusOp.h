/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __oplab_NexusOp_h__
#define __oplab_NexusOp_h__
namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Operator to adjust operator connections

	@ingroup oplab
*//***************************************************************************/
class FE_DL_EXPORT NexusOp:
	public OperatorSurfaceCommon,
	public Initialize<NexusOp>
{
	public:

					NexusOp(void):
						m_dragging(FALSE)
					{
						set(m_from);
						set(m_to);
					}

virtual				~NexusOp(void)											{}

		void		initialize(void);

					//* As HandlerI
virtual	void		handle(Record& a_rSignal);

	private:

		WindowEvent		m_event;

		sp<DrawMode>	m_spBox;

		BWORD			m_dragging;

		String			m_pickType;
		String			m_pickNode;
		String			m_pickConnector;

		SpatialVector	m_from;
		String			m_fromType;
		String			m_fromNode;
		String			m_fromConnector;

		SpatialVector	m_to;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __oplab_NexusOp_h__ */
