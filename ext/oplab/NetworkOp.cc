/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <oplab/oplab.pmh>

#include "signal.h"

#define	FE_RCO_DEBUG		FALSE
#define	FE_RCO_TMPFILE		"/tmp/NetworkOp_server.%d"

#define	FE_RCO_MESSAGE_TIME	Real(1)
#define	FE_RCO_STALL_TIME	Real(0.1)
#define	FE_RCO_STALL		milliSleep(I32(1000*FE_RCO_STALL_TIME))

using namespace fe;
using namespace fe::ext;

void NetworkOp::Handler::handle(Record &a_signal)
{
	safeLock();

#if FE_RCO_DEBUG
	feLog("NetworkOp::Handler::handle ttl %d\n",m_hpNetworkOp->m_ttl(a_signal));
#endif

	if(m_hpNetworkOp->m_source.check(a_signal))
	{
		const String source=m_hpNetworkOp->m_source(a_signal);

#if FE_RCO_DEBUG
		feLog("  source \"%s\" vs \"%s\"\n",
				source.c_str(),m_self.c_str());
#endif

		if(source==m_self)
		{
			safeUnlock();
			return;
		}
	}
	if(m_hpNetworkOp->m_text.check(a_signal))
	{
		m_state="";

		String message=m_hpNetworkOp->m_text(a_signal).c_str();

#if FE_RCO_DEBUG
		feLog("  text '%s'\n",message.c_str());
#endif

		const String action=message.parse();
		const String allArgs=message;
		const String firstArg=message.parse();

		if(firstArg=="")
		{
			m_state=action;
		}
		else
		{
			m_spActionCatalog->catalog<String>(action)=allArgs;
		}
	}
	if(m_hpNetworkOp->m_model.check(a_signal))
	{
		m_record=m_hpNetworkOp->m_model(a_signal);
		m_state="changed";

		const BWORD aggresive=FALSE;
		m_hpNetworkOp->dirty(aggresive);

#if FE_RCO_DEBUG
		feLog("  model %d\n",m_record.isValid());
#endif
	}

	safeUnlock();
}

String NetworkOp::Handler::state(void) const
{
	safeLock();
	String result=m_state;
	safeUnlock();

	return result;
}

void NetworkOp::Handler::setState(String a_state)
{
	safeLock();
	m_state=a_state;
	safeUnlock();
}

String NetworkOp::Handler::action(String a_key) const
{
	safeLock();
	String result=m_spActionCatalog.isValid()?
			m_spActionCatalog->catalogOrDefault<String>(a_key,""): "";
	safeUnlock();

	return result;
}

void NetworkOp::Handler::setAction(String a_key,String a_state)
{
	safeLock();
	if(m_spActionCatalog.isValid())
	{
		m_spActionCatalog->catalog<String>(a_key)=a_state;
	}
	safeUnlock();
}

Record NetworkOp::Handler::record(void) const
{
	safeLock();
	Record result=m_record;
	safeUnlock();

	return result;
}

void NetworkOp::sendText(Record a_signal,String a_message)
{
	m_text(a_signal)=a_message;
	m_ttl(a_signal)=2;

#if FE_RCO_DEBUG
	feLog("NetworkOp::sendText signaling '%s'\n",
			m_text(a_signal).c_str());
#endif

	m_spSignaler->signal(a_signal);
}

void NetworkOp::initialize(void)
{
	catalog<String>("icon")="FE_alpha";

	//* page Behavior
	catalog<String>("savefile")="";
	catalog<String>("savefile","label")="Save Input to File";
	catalog<String>("savefile","suggest")="filename";
	catalog<String>("savefile","page")="Behavior";

	catalog<String>("role")="None";
	catalog<String>("role","label")="Role";
	catalog<String>("role","choice:0")="None";
	catalog<String>("role","choice:1")="Server";
	catalog<String>("role","choice:2")="Client";
	catalog<String>("role","page")="Behavior";

	catalog<String>("address")="127.0.0.1";
	catalog<String>("address","label")="Address";
	catalog<String>("address","page")="Behavior";

	catalog<I32>("port")=7876;
	catalog<I32>("port","max")=65535;
	catalog<String>("port","label")="Port";
	catalog<String>("port","page")="Behavior";

	catalog<String>("command")="";
	catalog<String>("command","label")="Run Command";
	catalog<String>("command","suggest")="filename";
	catalog<String>("command","page")="Behavior";
	catalog<String>("command","hint")=
			"Run a program in a shell, potentially a server.";

	catalog<String>("request")="";
	catalog<String>("request","label")="Request";
	catalog<String>("request","page")="Behavior";
	catalog<String>("request","hint")=
			"For a client, send this text to the server.";

	catalog<String>("target")="";
	catalog<String>("target","label")="Target";
	catalog<String>("target","suggest")="filename";
	catalog<String>("target","page")="Behavior";
	catalog<String>("target","hint")=
			"Append this to the request, potentially a filename.";

	catalog<String>("options")="";
	catalog<String>("options","label")="Options";
	catalog<String>("options","page")="Behavior";
	catalog<String>("options","hint")=
			"Add these options to the request.";

	catalog<bool>("wait")=false;
	catalog<String>("wait","label")="Wait For Result";
	catalog<String>("wait","page")="Behavior";
	catalog<bool>("wait","joined")=true;
	catalog<String>("wait","hint")=
			"Stall processing, allowing the command to start.";

	catalog<Real>("timeout")=1.0;
	catalog<Real>("timeout","high")=30.0;
	catalog<Real>("timeout","max")=1e3;
	catalog<String>("timeout","label")="Max Time";
	catalog<String>("timeout","enabler")="wait";
	catalog<String>("timeout","page")="Behavior";
	catalog<String>("timeout","hint")="Wait time limit in seconds.";

	catalog<String>("loadfile")="";
	catalog<String>("loadfile","label")="Load Result From File";
	catalog<String>("loadfile","suggest")="filename";
	catalog<String>("loadfile","page")="Behavior";

	//* page Info
	catalog<String>("state")="";
	catalog<String>("state","label")="State";
	catalog<String>("state","IO")="output";
	catalog<String>("state","page")="Info";

	catalog< sp<Component> >("Brush");
	catalog<String>("Brush","implementation")="DrawI";
	catalog<String>("Brush","prompt")="watching for messages";
	catalog<String>("Brush","idle")="any";

	catalog< sp<Component> >("Input Surface");
	catalog<bool>("Input Surface","optional")=true;

	catalog< sp<Component> >("Output Surface");
	catalog<String>("Output Surface","IO")="output";
//	catalog<String>("Output Surface","copy")="Input Surface";
	catalog<bool>("Output Surface","recycle")=true;

	m_spScope=registry()->create("Scope");

	m_ttl.setup(m_spScope,FE_USE(":TTL"));
	m_source.setup(m_spScope,"source");
	m_text.setup(m_spScope,"text");
	m_model.setup(m_spScope,"model");

	m_spLayoutHB=m_spScope->declare("heartbeat");

	m_spLayoutMessage=m_spScope->declare("message");
	m_spLayoutMessage->populate(m_ttl);
	m_spLayoutMessage->populate(m_source);
	m_spLayoutMessage->populate(m_text);

	m_spLayoutSurface=m_spScope->declare("surface");
	m_spLayoutSurface->populate(m_ttl);
	m_spLayoutSurface->populate(m_source);
	m_spLayoutSurface->populate(m_model);

	m_spImportOp=registry()->create("OperatorSurfaceI.ImportOp");
}

void NetworkOp::handleBind(sp<SignalerI> a_spSignalerI,sp<Layout> a_spLayout)
{
	OperatorSurfaceCommon::handleBind(a_spSignalerI,a_spLayout);

	sp<OperatorSurfaceCommon> spCommon=m_spImportOp;
	if(spCommon.isValid())
	{
		spCommon->handleBind(a_spSignalerI,a_spLayout);
	}
}

void NetworkOp::handle(Record& a_rSignal)
{
	const String savefile=catalog<String>("savefile");
	const String loadfile=catalog<String>("loadfile");
	const String role=catalog<String>("role");
	const String address=catalog<String>("address");

	m_spScope->setName(name()+".Scope");

	sp<DrawI> spDrawBrush;
	accessBrush(spDrawBrush,a_rSignal,e_quiet);

	if(spDrawBrush.isValid())
	{
		m_brushed=TRUE;

		if(role=="Client")
		{
			Record heartbeat=m_spScope->createRecord(m_spLayoutHB);
			m_spSignaler->signal(heartbeat);
		}
		return;
	}

	const Real frame=currentFrame(a_rSignal);
	const BWORD frameChanged=(frame!= m_lastFrame);
	const BWORD replaced=(catalog<bool>("Output Surface","replaced"));
	const BWORD paramChanged=(!m_brushed && !replaced && !frameChanged);
	m_brushed=FALSE;
	m_lastFrame=frame;

	const BWORD rebuild=(paramChanged || frameChanged || replaced);

#if FE_RCO_DEBUG
	feLog("NetworkOp::handle frame %.6G changed %d replaced %d"
			" param %d rebuild %d brushed %d role \"%s\"\n",
			frame,frameChanged,replaced,paramChanged,rebuild,
			m_brushed,role.c_str());
#endif

	sp<SurfaceAccessibleI> spInputAccessible;
	sp<SurfaceAccessibleRecord> spInputRecord;

	if(!savefile.empty() || role=="Server")
	{
#if FE_RCO_DEBUG
		feLog("NetworkOp::handle access input\n");
#endif

		access(spInputAccessible,"Input Surface",e_warning);

		spInputRecord=spInputAccessible;

		if(spInputRecord.isNull()) {
			spInputRecord=registry()->create("*.SurfaceAccessibleRecord");

			if(spInputRecord.isValid())
			{
				spInputRecord->bind(m_spScope);

				if(spInputAccessible.isValid())
				{
#if FE_RCO_DEBUG
					feLog("NetworkOp::handle convert input\n");
#endif

					spInputRecord->copy(spInputAccessible);
				}
			}
		}
	}

	sp<SurfaceAccessibleRecord> spOutputRecord;

	if(!savefile.empty() && spInputRecord.isValid())
	{
#if FE_RCO_DEBUG
		feLog("NetworkOp::handle save \"%s\"\n",savefile.c_str());
#endif

		spInputRecord->save(savefile);
	}

	if(m_spServer.isValid() && role!="Server")
	{
		m_spServer=NULL;
	}
	if(m_spClient.isValid() && role!="Client")
	{
		m_spClient=NULL;
	}

	if(role=="None")
	{
		m_spHandler=NULL;
		m_spSignaler=NULL;
	}
	else
	{
		//* HACK force specific thread support
		Mutex::confirm("fexBoostThread1_56");

		Record heartbeat=m_spScope->createRecord(m_spLayoutHB);

		Record signalMessage=m_spScope->createRecord(m_spLayoutMessage);
		m_source(signalMessage)=role;

		if(m_spHandler.isNull())
		{
			sp<Catalog> spActionCatalog=
					registry()->master()->createCatalog("Server Actions");

			m_spHandler=new Handler;
			m_spHandler->bind(spActionCatalog);
			m_spHandler->bind(sp<NetworkOp>(this));
			m_spHandler->setName(name()+".Handler");
			m_spHandler->setSelf(role);
		}

		if(m_spSignaler.isNull())
		{
			m_spSignaler=registry()->create("SignalerI");
			m_spSignaler->insert(m_spHandler,m_spLayoutMessage);
			m_spSignaler->insert(m_spHandler,m_spLayoutSurface);
		}
		m_spSignaler->setName(name()+".Signaler");

		if(role=="Server" && spInputRecord.isValid())
		{
			String tempName;

			if(m_spServer.isNull())
			{
				m_spServer=registry()->create("ServerI");
				if(!m_spServer.isValid())
				{
					feX("NetworkOp::handle","could not create ServerI");
				}

				m_spServer->addLayout(m_spLayoutMessage);
				m_spServer->addLayout(m_spLayoutSurface);

				short port=catalog<I32>("port");

				BWORD success=FALSE;

				const I32 scanLimit=100;	//* TODO param
				for(I32 scanIndex=0;scanIndex<scanLimit;scanIndex++)
				{
#if FE_RCO_DEBUG
					feLog("NetworkOp::handle server try port %d\n",port);
#endif

					try
					{
						m_spServer->start(m_spSignaler,port,m_spLayoutHB);

#if FE_RCO_DEBUG
						feLog("NetworkOp::handle server using port %d\n",port);
#endif

						//* TODO figure out which pid was reported to parent
						const pid_t pid=getppid();

						//* NOTE save port number to temp file
						tempName.sPrintf(FE_RCO_TMPFILE,pid);

						FILE* tempFile=fopen(tempName.c_str(),"w");
						if(tempFile)
						{
							fe_fprintf(tempFile,"%hd\n",port);
							fclose(tempFile);
						}
						else
						{
							tempName="";
						}

						success=TRUE;
						break;
					}
					catch(...)
					{
						feLog("NetworkOp::handle server no connection\n");
						m_spServer->stop();
					}

					port++;
					FE_RCO_STALL;
				}

				if(!success)
				{
					feLog("NetworkOp::handle server failed to start\n");
					m_spServer=NULL;
				}

#if FE_RCO_DEBUG
				feLog("NetworkOp::handle server create done\n");
#endif
			}

#if FE_RCO_DEBUG
			feLog("NetworkOp::handle server signal HB; wait for 'ready'\n");
#endif

			while(m_spHandler->state()!="ready")
			{
				m_spSignaler->signal(heartbeat);
				FE_RCO_STALL;
			}

			if(!tempName.empty())
			{
				if(unlink(tempName.c_str())==0)
				{
					tempName="";
				}
			}

			String loadMessage=m_spHandler->action("import");
			if(!loadMessage.empty())
			{
				//* import surface and copy into spInputRecord

				m_spHandler->setAction("import","");

				const String frameString=m_spHandler->action("frame");
				if(!frameString.empty())
				{
					loadMessage+=" -frame "+frameString;
				}

#if FE_RCO_DEBUG
				feLog("NetworkOp::handle server import '%s'\n",
						loadMessage.c_str());
#endif

				sp<SurfaceAccessibleI> spImportedAccessible=
						importSurface(a_rSignal,loadMessage);

				if(spImportedAccessible.isNull())
				{
					feLog("NetworkOp::handle server import failed '%s'\n",
							loadMessage.c_str());
				}
				else
				{
					sp<SurfaceAccessibleRecord> spImportedAccessibleRecord=
							spImportedAccessible;

					if(spImportedAccessibleRecord.isValid())
					{
						spInputRecord=spImportedAccessibleRecord;
					}
					else
					{
						spInputRecord->copy(spImportedAccessible);
					}
				}
			}

#if FE_RCO_DEBUG
			feLog("NetworkOp::handle server signal surface\n");
#endif

			Record surfaceRecord=spInputRecord->record();

			Record signalSurface=m_spScope->createRecord(m_spLayoutSurface);
			m_ttl(signalSurface)=2;
			m_source(signalSurface)=role;
			m_model(signalSurface)=surfaceRecord;

			m_spSignaler->signal(signalSurface);
			m_spSignaler->signal(heartbeat);
		}

		if(role=="Client")
		{
			const String command=catalog<String>("command");

			if(!command.empty())
			{
				if(!m_commandPid)
				{
					m_spHandler->setState("");

#if FE_RCO_DEBUG
					feLog("NetworkOp::handle client command '%s'\n",
							command.c_str());
#endif

					//* TODO abstract fork
					String buffer=command;

					Array<String> argArray;
					String arg;
					I32 argCount=0;
					while(!(arg=buffer.parse()).empty())
					{
						argArray.resize(argCount+1);
						argArray[argCount++]=arg;
					}

					char *argv[argCount+1];
					for(I32 argIndex=0;argIndex<argCount;argIndex++)
					{
						argv[argIndex]=strdup(argArray[argIndex].c_str());
					}
					argv[argCount]=NULL;

					pid_t pid=vfork();
					if(!pid)
					{
						//* child

						execvp(argv[0],argv);

						//* NOTE should not continue

#if FE_RCO_DEBUG
						feLog("NetworkOp::handle"
								" client child exec failed \"%s\"\n",
								errorString(FE_ERRNO).c_str());
#endif
						exit(1);
					}
					else if(pid<0)
					{
						//* fail
#if FE_RCO_DEBUG
						feLog("NetworkOp::handle"
								" client child fork failed, pid %d\n",pid);
#endif
					}
					else
					{
						//* parent
#if FE_RCO_DEBUG
						feLog("NetworkOp::handle client forked pid %d\n",pid);
#endif
						m_commandPid=pid;
					}

					for(I32 argIndex=0;argIndex<argCount;argIndex++)
					{
						free(argv[argIndex]);
					}

#if FE_RCO_DEBUG
					feLog("NetworkOp::handle client command sent\n");
#endif
				}
			}

			const BWORD wait=catalog<bool>("wait");
			Real timeout=wait? catalog<Real>("timeout"): 0.001;

			if(m_spClient.isNull())
			{
				m_spClient=registry()->create("ClientI");
				if(!m_spClient.isValid())
				{
					feX("NetworkOp::handle","could not create ClientI");
				}

				m_spClient->addLayout(m_spLayoutMessage);
				m_spClient->addLayout(m_spLayoutSurface);
//				m_spClient->setScope(m_spScope);

#if FE_RCO_DEBUG
				feLog("NetworkOp::handle client start\n");
#endif
				//* TODO conditionally look up in temp file
//				short port=catalog<I32>("port");
				short port=0;

				String tempName;
				tempName.sPrintf(FE_RCO_TMPFILE,m_commandPid);

				//* stall processing, hoping a server will connect

				FILE* tempFile=NULL;

				BWORD success=FALSE;
				while(timeout>Real(0))
				{
					if(!tempFile)
					{
#if FE_RCO_DEBUG
						feLog("NetworkOp::handle"
								" client wait for %.6Gs for file \"%s\"\n",
								timeout,tempName.c_str());
#endif

						tempFile=fopen(tempName.c_str(),"r");
						if(tempFile)
						{
							fscanf(tempFile,"%hd",&port);
							fclose(tempFile);
						}
					}

					if(port)
					{
#if FE_RCO_DEBUG
						feLog("NetworkOp::handle client look for %.6Gs for"
								" server on port %d\n",timeout,port);
#endif

						try
						{
							m_spClient->start(m_spSignaler,
									address,(short)port,m_spLayoutHB);
							success=TRUE;
							break;
						}
						catch(...)
						{
							feLog("NetworkOp::handle client no connection\n");
							m_spClient->stop();
						}
					}

					FE_RCO_STALL;
					timeout-=FE_RCO_STALL_TIME;
				}

				if(!success)
				{
					feLog("NetworkOp::handle client failed to start\n");
					m_spClient=NULL;
				}

#if FE_RCO_DEBUG
				feLog("NetworkOp::handle client create done\n");
#endif
			}

			const String request=catalog<String>("request");
			const String target=catalog<String>("target");
			const String options=catalog<String>("options");

			if(!request.empty())
			{
				String fullRequest=request;

				if(!options.empty())
				{
					const String escaped=options.substitute("\"","\\\"");
					fullRequest+=" -options \""+escaped+"\"";
				}

				fullRequest+=" "+target;

#if FE_RCO_DEBUG
				feLog("NetworkOp::handle client request '%s'\n",
						fullRequest.c_str());
#endif

				sendText(signalMessage,fullRequest);
			}

			String frameString;
			frameString.sPrintf("frame %.6G",frame);

			sendText(signalMessage,frameString);

			sendText(signalMessage,"ready");

			m_spSignaler->signal(heartbeat);
			FE_RCO_STALL;

#if FE_RCO_DEBUG
			feLog("NetworkOp::handle client check for 'changed'\n");
			Real messageTime=0.0;
#endif

			//* stall processing, hoping the server will respond
			if(wait)
			{
				while(timeout>Real(0) && m_spHandler->state()!="changed")
				{
#if FE_RCO_DEBUG
					if(messageTime<=0.0)
					{
						feLog("NetworkOp::handle"
								" client wait %.6Gs for 'changed' vs \"%s\"\n",
								timeout,m_spHandler->state().c_str());

						messageTime=FE_RCO_MESSAGE_TIME;
					}
					messageTime-=FE_RCO_STALL_TIME;
#endif

					FE_RCO_STALL;
					timeout-=FE_RCO_STALL_TIME;
				}
			}

			if(rebuild || m_spHandler->state()=="changed")
			{
				Record surfaceRecord=m_spHandler->record();
				if(surfaceRecord.isValid())
				{
					spOutputRecord=registry()->create(
							"*.SurfaceAccessibleRecord");
					spOutputRecord->bind(surfaceRecord);
				}

				m_spHandler->setState(
						spOutputRecord.isValid() &&
						spOutputRecord->count(SurfaceAccessibleI::e_point)?
						"current": "empty");
				if(wait)
				{
					//* NOTE reset for next time

#if FE_RCO_DEBUG
					feLog("NetworkOp::handle client close\n");
#endif
					m_spClient=NULL;

#if FE_RCO_DEBUG
					feLog("NetworkOp::handle client kill server pid %d\n",
							m_commandPid);
#endif

					// see /usr/include/bits/signum.h
					if(kill(m_commandPid,SIGTERM)<0)
					{
						feLog("NetworkOp::handle"
								" client kill server, SIGTERM failed\n");
						if(kill(m_commandPid,SIGKILL)<0)
						{
							feLog("NetworkOp::handle"
									" client kill server, SIGKILL failed\n");
						}
					}

					m_commandPid=0;
				}
			}
		}
	}

	if(!loadfile.empty())
	{
#if FE_RCO_DEBUG
		feLog("NetworkOp::handle load \"%s\"\n",loadfile.c_str());
#endif

		spOutputRecord=registry()->create("*.SurfaceAccessibleRecord");
		spOutputRecord->bind(m_spScope);
		if(!spOutputRecord->load(loadfile))
		{
			spOutputRecord=NULL;
		}
	}

	if(spOutputRecord.isValid())
	{
#if FE_RCO_DEBUG
		feLog("NetworkOp::handle convert output\n");
#endif
		sp<SurfaceAccessibleI> spOutputAccessible;
		if(!accessOutput(spOutputAccessible,a_rSignal)) return;

		sp<SurfaceAccessibleRecord> spOutputAccessibleRecord=spOutputAccessible;
		if(spOutputAccessibleRecord.isValid())
		{
			spOutputAccessibleRecord->bindLike(spOutputRecord);
		}
		else
		{
			spOutputAccessible->copy(spOutputRecord);
		}
	}

	catalog<String>("state")=
			m_spHandler.isValid()? m_spHandler->state(): "";
}

sp<SurfaceAccessibleI> NetworkOp::importSurface(
	Record& a_rSignal,String a_args)
{
	Real frame=currentFrame(a_rSignal);

	String loadfile;
	String options;

	String arg;
	while(!(arg=a_args.parse()).empty())
	{
		if(arg=="-options")
		{
			options=a_args.parse();
		}
		else if(arg=="-frame")
		{
			frame=a_args.parse().real();
		}
		else
		{
			loadfile=arg;
		}
	}

#if FE_RCO_DEBUG
	feLog("NetworkOp::importSurface options '%s'\n",options.c_str());
#endif

	sp<SurfaceAccessibleI> spImportedAccessible;

	sp<Catalog> spOtherCatalog=m_spImportOp;
	if(spOtherCatalog.isValid())
	{
		spImportedAccessible=registry()->create("*.SurfaceAccessibleCatalog");

		spOtherCatalog->catalog<String>("loadfile")=loadfile;
		spOtherCatalog->catalog<String>("options")=options;
		spOtherCatalog->catalog<Real>("frame")=frame;
		spOtherCatalog->catalog< sp<Component> >("Output Surface")=
				spImportedAccessible;

		sp<Component> spPriorOutput=surfaceOutput(a_rSignal);
		const Real priorFrame=currentFrame(a_rSignal);

		setSurfaceOutput(a_rSignal,spImportedAccessible);
		setCurrentFrame(a_rSignal,frame);

		try
		{
			m_spImportOp->handle(a_rSignal);
		}
		catch(const Exception& e)
		{
			relayMessages();
			throw e;
		}
		relayMessages();

		setSurfaceOutput(a_rSignal,spPriorOutput);
		setCurrentFrame(a_rSignal,priorFrame);
	}

#if FE_RCO_DEBUG
	if(spImportedAccessible.isNull())
	{
		feLog("NetworkOp::importSurface failed \"%s\" options \"%s\"\n",
				loadfile.c_str(),options.c_str());
	}
#endif

	return spImportedAccessible;
}

void NetworkOp::relayMessages(void)
{
	sp<Catalog> spOtherCatalog=m_spImportOp;
	if(spOtherCatalog.isValid())
	{
		String text=spOtherCatalog->catalog<String>("message");
		if(!text.empty())
		{
			catalog<String>("message")+="[ImportOp] "+text;
			spOtherCatalog->catalog<String>("message")="";
		}

		text=spOtherCatalog->catalog<String>("warning");
		if(!text.empty())
		{
			catalog<String>("warning")+="[ImportOp] "+text;
			spOtherCatalog->catalog<String>("warning")="";
		}

		text=spOtherCatalog->catalog<String>("error");
		if(!text.empty())
		{
			catalog<String>("error")+="[ImportOp] "+text;
			spOtherCatalog->catalog<String>("error")="";
		}
	}
}

