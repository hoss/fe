/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <oplab/oplab.pmh>

#define FE_XRO_DEBUG		FALSE

using namespace fe;
using namespace fe::ext;

void XRayOp::initialize(void)
{
	catalog<String>("icon")="FE_alpha";

	catalog<I32>("ImageScale")=2;
	catalog<String>("ImageScale","label")="Image Scale";
	catalog<I32>("ImageScale","min")=1;
	catalog<I32>("ImageScale","high")=8;
	catalog<I32>("ImageScale","max")=32;

	catalog< sp<Component> >("Brush");
	catalog<String>("Brush","implementation")="DrawI";

	catalog< sp<Component> >("Input Surface");

	m_spSolid=new DrawMode();
	m_spSolid->setDrawStyle(DrawMode::e_solid);
}

void XRayOp::handle(Record& a_rSignal)
{
	if(m_spRayTrace.isNull())
	{
		m_spRayTrace=registry()->create("DrawI.DrawRayTrace");
	}

	const I32 size=catalog<I32>("ImageScale")*64;

	sp<DrawI> spDrawOverlay;
	accessBrushOverlay(spDrawOverlay,a_rSignal,e_quiet);

	if(spDrawOverlay.isValid())
	{
#if FE_XRO_DEBUG
		feLog("XRayOp::handle brush\n");
#endif
		m_brushed=TRUE;

		spDrawOverlay->pushDrawMode(m_spSolid);

		m_event.bind(windowEvent(a_rSignal));
		const I32 mouseX=m_event.mouseX();
		const I32 mouseY=m_event.mouseY();

		const Color red(1.0,0.0,0.0,1.0);

		const SpatialVector& rRayOrigin=rayOrigin(a_rSignal);
		const SpatialVector& rRayDirection=rayDirection(a_rSignal);

		//* temporarily alter camera
		sp<ViewI> spView=spDrawOverlay->view();
		sp<CameraI> spCamera=spView->camera();

		const Box2i origViewport=spView->viewport();

		Real origFovy=spCamera->fov()[1];

		SpatialTransform cameraMatrixOrig;
		invert(cameraMatrixOrig,spCamera->cameraMatrix());
		const SpatialVector cameraY=cameraMatrixOrig.column(1);

		const SpatialVector corner(mouseX-size/2,mouseY-size/2);

		Box2i viewport;
		set(viewport,corner[0],corner[1],size,size);
		spView->setViewport(viewport);

		SpatialTransform offset;
		setIdentity(offset);
		translate(offset,corner);

		const Real fovy=origFovy*size/height(origViewport);

		SpatialTransform cameraTransform;
		makeFrameCameraZ(cameraTransform,rRayOrigin,cameraY,rRayDirection);

		SpatialTransform cameraMatrix;
		invert(cameraMatrix,cameraTransform);
		spCamera->setCameraMatrix(cameraMatrix);
		spCamera->setFov(Vector2(0.0,fovy));

//		m_spRayTrace->setTransform(offset);
		m_spRayTrace->setDrawChain(spDrawOverlay);

		const I32 surfaceCount=m_surfaceArray.size();

#if FE_XRO_DEBUG
		feLog("XRayOp::handle fovy %.6G draw %d surfaces\n",
				fovy,surfaceCount);
#endif

		for(I32 surfaceIndex=0;surfaceIndex<surfaceCount;surfaceIndex++)
		{
			m_spRayTrace->draw(m_surfaceArray[surfaceIndex],&red);
		}

		m_spRayTrace->flush();

		//* restore
		spView->setViewport(origViewport);
		spCamera->setFov(Vector2(0.0,origFovy));

		spDrawOverlay->popDrawMode();

		return;
	}

	const BWORD paramChanged=!m_brushed;
	m_brushed=FALSE;

#if FE_XRO_DEBUG
	feLog("XRayOp::handle changed %d\n",paramChanged);
#endif

	if(paramChanged)
	{
		m_surfaceArray.clear();

		I32 surfaceIndex=0;
		while(TRUE)
		{
			sp<SurfaceI> spInput;
			if(!access(spInput,"Input Surface",e_quiet,surfaceIndex++))
			{
				break;
			}

#if FE_XRO_DEBUG
			feLog("XRayOp::handle prepare surface %d\n",surfaceIndex);
#endif

			spInput->prepareForSearch();
			m_surfaceArray.push_back(spInput);
		}
	}
}
