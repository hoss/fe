/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __oplab_PreviewOp_h__
#define __oplab_PreviewOp_h__

#include "operator/ImportOp.h"
namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Operator to merely draw imported surface

	@ingroup oplab
*//***************************************************************************/
class FE_DL_EXPORT PreviewOp:
	public ImportOp,
	public Initialize<PreviewOp>
{
	public:

					PreviewOp(void)											{}
virtual				~PreviewOp(void)										{}

		void		initialize(void);

					//* As HandlerI
virtual	void		handle(Record& a_rSignal);

	private:
		sp<DrawableI>	m_spDrawable;
		sp<DrawBufferI>	m_spDrawBuffer;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __oplab_PreviewOp_h__ */
