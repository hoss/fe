/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __oplab_BindEditOp_h__
#define __oplab_BindEditOp_h__
namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Operator to interactively change wrap bindings

	@ingroup oplab
*//***************************************************************************/
class FE_DL_EXPORT BindEditOp:
	public OperatorState,
	public Initialize<BindEditOp>
{
	public:

						BindEditOp(void):
							m_rayDepth(0),
							m_ctrled(FALSE)									{}
virtual					~BindEditOp(void)									{}

		void			initialize(void);

						//* As HandlerI
virtual	void			handle(Record& a_rSignal);

	protected:

virtual	void			setupState(void);
virtual	BWORD			loadState(const String& rBuffer);

	private:

		sp< RecordMap<I32> >	m_spEditMap;
		Accessor<I32>			m_aIndex;
		Accessor<I32>			m_aBinding;
		Accessor<I32>			m_aFace;
		Accessor<SpatialVector>	m_aBarycenter;

		sp<SurfaceI>			m_spInput;
		sp<SurfaceI>			m_spDriver;
		WindowEvent				m_event;
		I32						m_rayDepth;
		BWORD					m_ctrled;

		SpatialVector			m_selectPoint;
		SpatialVector			m_selectNormal;
		SpatialVector			m_bindPoint;
		SpatialVector			m_bindNormal;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __oplab_BindEditOp_h__ */
