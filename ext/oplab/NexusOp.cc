/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <oplab/oplab.pmh>

using namespace fe;
using namespace fe::ext;

void NexusOp::initialize(void)
{
	catalog<String>("icon")="FE_alpha";

	catalog< sp<Component> >("Brush");
	catalog<String>("Brush","implementation")="DrawI";

	catalog< sp<Component> >("Graph");
	catalog<String>("Graph","implementation")="OperatorGraphI";

	m_spBox=new DrawMode();
	m_spBox->setDrawStyle(DrawMode::e_solid);
	m_spBox->setLineWidth(2.0);
}

void NexusOp::handle(Record& a_rSignal)
{
	sp<DrawI> spDrawOverlay;
	accessBrushOverlay(spDrawOverlay,a_rSignal,e_quiet);

	sp<OperatorGraphI> spGraph=catalog< sp<Component> >("Graph");

	if(spDrawOverlay.isValid() && spGraph.isValid())
	{
		sp<ImageI> spImageGraph=spGraph->image();
		if(spImageGraph.isNull())
		{
			return;
		}

		spDrawOverlay->pushDrawMode(m_spBox);

		const Color red(1.0,0.0,0.0);
		const Color yellow(0.7,0.7,0.0);
		const Color darkblue(0.0,0.0,0.5);
		const Color blue(0.3,0.3,1.0);
		const Color white(1.0,1.0,1.0);
		const Color black(0.0,0.0,0.0);

		const Real circleRadius=8.0;
		const SpatialVector circleScale(circleRadius,
				circleRadius,circleRadius);

		BWORD tryConnect=FALSE;
		BWORD resolved=FALSE;

		m_event.bind(windowEvent(a_rSignal));
//		feLog("NexusOp::handle %s\n",c_print(m_event));

		if(m_event.isMousePress(WindowEvent::e_itemLeft))
		{
			m_dragging=TRUE;
			set(m_from,m_event.mouseX(),m_event.mouseY());

			tryConnect=TRUE;
			feLog("  tryConnect\n");
		}
		else if(m_event.isMouseRelease(WindowEvent::e_itemLeft))
		{
			m_dragging=FALSE;

			tryConnect=TRUE;
			feLog("  tryConnect\n");
		}
		else if(m_event.isKeyPress(WindowEvent::e_keyDelete))
		{
			if(m_fromType=="in")
			{
				spGraph->disconnect(m_fromNode,m_fromConnector);
				resolved=TRUE;
			}
			else if(m_pickType=="in")
			{
				spGraph->disconnect(m_pickNode,m_pickConnector);
				resolved=TRUE;
			}
		}

		if(tryConnect && !m_fromNode.empty() && !m_pickNode.empty() &&
				m_fromType!="node" && m_fromType!=m_pickType &&
				m_fromNode!=m_pickNode)
		{
			feLog(" connect \"%s\" \"%s\" -> \"%s\" \"%s\"\n",
					m_fromNode.c_str(),m_fromConnector.c_str(),
					m_pickNode.c_str(),m_pickConnector.c_str());
			if(m_fromType=="out")
			{
				spGraph->connect(m_fromNode,m_fromConnector,
						m_pickNode,m_pickConnector);
			}
			else
			{
				spGraph->connect(m_pickNode,m_pickConnector,
						m_fromNode,m_fromConnector);
			}
			resolved=TRUE;
		}

		if(resolved)
		{
			m_fromType="";
			m_fromNode="";
			m_fromConnector="";
		}

		if(m_event.mouseX() && m_event.mouseY())
		{
			set(m_to,m_event.mouseX(),m_event.mouseY());
		}

		SpatialVector line[2];
		line[1]=m_to;

//		const U32 width=spImageGraph->width();
		const U32 height=spImageGraph->height();

		m_pickType="";
		m_pickNode=spImageGraph->pickRegion(m_event.mouseX(),m_event.mouseY());
		m_pickConnector="";

		if(!resolved && !m_pickNode.empty())
		{
			const I32 sidebar=12; //* can we query this somewhere?
			const Box2i box=spImageGraph->regionBox(m_pickNode);
			I32 zone=3.0*(m_event.mouseY()-box[1])/box.size()[1];

			if(m_event.mouseX()<box[0]+sidebar)
			{
				//* left sidebar
				zone=3;
			}
			else if(m_event.mouseX()>=box[0]+box.size()[0]-sidebar)
			{
				//* right sidebar
				zone=4;
			}

			SpatialTransform xform;
			setIdentity(xform);

			switch(zone)
			{
				case 3:
				{
					//* left sidebar
					set(line[1],box[0],box[1]+0.5*box.size()[1]);
					translate(xform,line[1]);
					spDrawOverlay->drawCircle(xform,&circleScale,yellow);
					break;
				}
				case 4:
				{
					//* right sidebar
					set(line[1],box[0]+box.size()[0],box[1]+0.5*box.size()[1]);
					translate(xform,line[1]);
					spDrawOverlay->drawCircle(xform,&circleScale,blue);
					break;
				}
				case 0:
				{
					//* output
					const String outputConnector=
							spGraph->outputConnector(m_pickNode,0);

//					const I32 outputCount=spGraph->outputCount(m_pickNode);
//					feLog("pick output 0/%d \"%s\"\n",
//							outputCount,outputConnector.c_str());

					set(line[1],box[0]+0.5*box.size()[0],box[1]);
					translate(xform,line[1]);
					spDrawOverlay->drawCircle(xform,&circleScale,red);
					if(m_event.isMousePress(WindowEvent::e_itemLeft))
					{
						m_from=line[1];
						m_fromType="out";
						m_fromNode=m_pickNode;
						m_fromConnector=outputConnector;
					}
					else
					{
						m_pickType="out";
						m_pickConnector=outputConnector;
					}

					drawLabel(spDrawOverlay,line[1]+SpatialVector(8,-16),
							outputConnector,FALSE,2,darkblue,&blue,&white);
					break;
				}
				case 2:
				{
					//* input
					const U32 inputCount=spGraph->inputCount(m_pickNode);
					const U32 inputIndex=inputCount*
							(m_event.mouseX()-box[0]-sidebar)/
							(box.size()[0]-2.0*sidebar);
					const String inputConnector=
							spGraph->inputConnector(m_pickNode,inputIndex);

//					feLog("pick input %d/%d \"%s\"\n",
//							inputIndex,inputCount,inputConnector.c_str());

					set(line[1],
							box[0]+sidebar+(box.size()[0]-sidebar*2)*
							(inputIndex*2.0+1)/Real(inputCount*2.0),
							box[1]+box.size()[1]);
					translate(xform,line[1]);
					spDrawOverlay->drawCircle(xform,&circleScale,red);
					if(m_event.isMousePress(WindowEvent::e_itemLeft))
					{
						m_from=line[1];
						m_fromType="in";
						m_fromNode=m_pickNode;
						m_fromConnector=inputConnector;
					}
					else
					{
						m_pickType="in";
						m_pickConnector=inputConnector;
					}

					drawLabel(spDrawOverlay,line[1]+SpatialVector(8,8),
							inputConnector,FALSE,2,darkblue,&blue,&white);

					break;
				}
				default:
				{
					//* midline
					Box2i modbox=box;
					modbox-=Vector2i(4,4);
					modbox.size()+=Vector2i(8,8);
					spDrawOverlay->drawBox(modbox,red);

					drawLabel(spDrawOverlay,
							SpatialVector(modbox)+SpatialVector(4,-14),
							m_pickNode,FALSE,2,darkblue,&blue,&white);

					if(m_event.isMousePress(WindowEvent::e_itemLeft))
					{
						m_from=line[1];
						m_fromType="node";
						m_fromNode=m_pickNode;
						m_fromConnector="";
					}
				}
			}
		}
		else if(m_event.isMousePress(WindowEvent::e_itemLeft))
		{
			m_fromType="";
			m_fromNode="";
			m_fromConnector="";
		}

		if(!m_fromNode.empty() && m_fromType!="node" &&
				m_fromType!=m_pickType && m_fromNode!=m_pickNode)
		{
			line[0]=m_from;

			SpatialTransform xform;
			setIdentity(xform);
			translate(xform,line[0]);
			spDrawOverlay->drawCircle(xform,&circleScale,red);

			drawLabel(spDrawOverlay,
					m_from+SpatialVector(8,m_fromType=="in"? 8: -16),
					m_fromConnector,FALSE,2,darkblue,&blue,&white);

			spDrawOverlay->drawLines(line,NULL,2,DrawI::e_strip,false,&red);
		}

		spDrawOverlay->drawRaster(spImageGraph,SpatialVector(0,height,1.0));

		spDrawOverlay->popDrawMode();

//		feLog("  drag %d from \"%s\" \"%s\" \"%s\"\n",
//				m_dragging,m_fromType.c_str(),
//				m_fromNode.c_str(),m_fromConnector.c_str());

		return;
	}
}
