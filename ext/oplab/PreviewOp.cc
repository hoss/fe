/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <oplab/oplab.pmh>

using namespace fe;
using namespace fe::ext;

void PreviewOp::initialize(void)
{
	catalog<String>("icon")="FE_alpha";

	catalog<bool>("writeOutput")=FALSE;
	catalog<bool>("writeOutput","hidden")=FALSE;

	catalog< sp<Component> >("Brush");
	catalog<String>("Brush","implementation")="DrawI";

	sp<Catalog> spMasterCatalog=registry()->master()->catalog();
	spMasterCatalog->catalog<bool>("hint:openclWaitForGLX")=TRUE;
}

void PreviewOp::handle(Record& a_rSignal)
{
	sp<DrawI> spDrawBrush;
	accessBrush(spDrawBrush,a_rSignal,e_quiet);

	if(spDrawBrush.isValid())
	{
		if(m_spLoadAccessible.isNull())
		{
			return;
		}

		if(m_spDrawable.isNull())
		{
			m_spDrawable=m_spLoadAccessible->surface();
		}

		if(m_spDrawBuffer.isNull())
		{
			m_spDrawBuffer=spDrawBrush->createBuffer();
			m_spDrawBuffer->setName(m_spDrawable->name());
		}

		spDrawBrush->draw(m_spDrawable,NULL,m_spDrawBuffer);

		return;
	}

	ImportOp::handle(a_rSignal);

	sp<Component> spPayload;
	if(m_spLoadAccessible.isValid())
	{
		spPayload=m_spLoadAccessible->payload();
		if(spPayload.isValid())
		{
			feLog("PreviewOp::handle payload of \"%s\"\n",
					spPayload->name().c_str());
		}
	}
	if(spPayload.isNull())
	{
		spPayload=m_spLoadAccessible;
	}

	Array< sp<Component> >& rPayload=
			catalog< Array< sp<Component> > >("Output Surface","payload");
	rPayload.resize(1);
	rPayload[0]=spPayload;
}
