/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <oplab/oplab.pmh>

#define FE_CVO_DEBUG		FALSE
#define FE_CVO_EDIT_DEBUG	FALSE

using namespace fe;
using namespace fe::ext;

void CurvaceousOp::initialize(void)
{
	catalog<String>("icon")="FE_alpha";

	catalog<String>("modifyMode")="Drag";
	catalog<String>("modifyMode","label")="Mode";
	catalog<String>("modifyMode","choice:0")="None";
	catalog<String>("modifyMode","choice:1")="Drag";
	catalog<String>("modifyMode","choice:2")="Comb";
	catalog<String>("modifyMode","page")="Edit";

	catalog<Real>("pickRadius")=0.0;
	catalog<Real>("pickRadius","high")=1.0;
	catalog<String>("pickRadius","label")="Pick Radius";
	catalog<String>("pickRadius","page")="Edit";
	catalog<String>("pickRadius","hint")="Inner region of area select.";

	catalog<Real>("pickFalloff")=1.0;
	catalog<String>("pickFalloff","label")="Pick Falloff";
	catalog<String>("pickFalloff","page")="Edit";
	catalog<String>("pickFalloff","hint")="Falloff extension of area select.";

	catalog<Real>("uRange")=0.0;
	catalog<Real>("uRange","high")=1.0;
	catalog<String>("uRange","label")="Unit CV Range";
	catalog<String>("uRange","page")="Edit";
	catalog<String>("uRange","hint")="Inner region of cv select.";

	catalog<Real>("uFalloff")=1.0;
	catalog<String>("uFalloff","label")="Unit CV Falloff";
	catalog<String>("uFalloff","page")="Edit";
	catalog<String>("uFalloff","hint")="Falloff extension of cv select.";

	catalog<String>("displayMode")="None";
	catalog<String>("displayMode","label")="Enhancement";
	catalog<String>("displayMode","choice:0")="None";
	catalog<String>("displayMode","choice:1")="Lines";
	catalog<String>("displayMode","label:1")="Wide Lines";
	catalog<String>("displayMode","choice:2")="Panels";
	catalog<String>("displayMode","choice:3")="Cylinders";
	catalog<String>("displayMode","choice:4")="Tubes";
	catalog<String>("displayMode","page")="Display";

	catalog<String>("radiusAttr")="radius";
	catalog<String>("radiusAttr","label")="Radius Attribute";
	catalog<String>("radiusAttr","page")="Display";
	catalog<String>("radiusAttr","hint")=
			"Name of float attribute specfiying cross-section"
			" radius at each curve sample.";

	catalog<Real>("radiusScale")=1.0;
	catalog<Real>("radiusScale","high")=10.0;
	catalog<String>("radiusScale","label")="Radius Scale";
	catalog<String>("radiusScale","page")="Display";
	catalog<String>("radiusScale","hint")=
			"Multiplier to radius for every curve point.";

	catalog<Real>("normalBias")=0.5;
	catalog<Real>("normalBias","high")=1.0;
	catalog<String>("normalBias","label")="Normal Bias";
	catalog<String>("normalBias","page")="Display";
	catalog<String>("normalBias","hint")=
			"Bend normals towards the camera to alter lighting.";

	catalog< sp<Component> >("Brush");
	catalog<String>("Brush","implementation")="DrawI";
	catalog<String>("Brush","visible")="any";
	catalog<String>("Brush","prompt")="Prompt here.";

	catalog< sp<Component> >("Input Surface");

	catalog< sp<Component> >("Driver Surface");
	catalog<bool>("Driver Surface","optional")=true;

	catalog<bool>("Output Surface","recycle")=true;

	m_spSolid=new DrawMode();
	m_spSolid->setDrawStyle(DrawMode::e_solid);
	m_spSolid->setLineWidth(3.0);

	for(I32 m=0;m<5;m++)
	{
		m_spWideLine[m]=new DrawMode();
		m_spWideLine[m]->copy(m_spSolid);
		m_spWideLine[m]->setLineWidth(m+1.0);
		m_spWideLine[m]->setLit(FALSE);
		m_spWideLine[m]->setLayer(m);
	}

	//* nested operator
	m_spLengthCorrect=registry()->create("OperatorSurfaceI.LengthCorrectOp");
	sp<Catalog> spOtherCatalog=m_spLengthCorrect;
	if(spOtherCatalog.isValid())
	{
		spOtherCatalog->catalog<bool>("Root Lock")=true;
		spOtherCatalog->catalog<I32>("Iterations")=20;
		spOtherCatalog->catalog<Real>("Correction Bias")=0.3;
		spOtherCatalog->catalog<Real>("Suspension")=1.0;
	}
}

void CurvaceousOp::handleBind(sp<SignalerI> a_spSignalerI,sp<Layout> a_spLayout)
{
	OperatorSurfaceCommon::handleBind(a_spSignalerI,a_spLayout);

	sp<OperatorSurfaceCommon> spCommon=m_spLengthCorrect;
	if(spCommon.isValid())
	{
		spCommon->handleBind(a_spSignalerI,a_spLayout);
	}
}

void CurvaceousOp::handle(Record& a_rSignal)
{
	const String modifyMode=catalog<String>("modifyMode");
	const Real pickRadius=catalog<Real>("pickRadius");
	const Real pickFalloff=catalog<Real>("pickFalloff");
	const Real uRange=catalog<Real>("uRange");
	const Real uFalloff=catalog<Real>("uFalloff");
	const String displayMode=catalog<String>("displayMode");
	const String radiusAttr=catalog<String>("radiusAttr");
	const Real radiusScale=catalog<Real>("radiusScale");
	const Real normalBias=catalog<Real>("normalBias");

	sp<DrawI> spDrawBrush;
	accessBrush(spDrawBrush,a_rSignal,e_quiet);

	sp<DrawI> spDrawOverlay;
	accessBrushOverlay(spDrawOverlay,a_rSignal,e_quiet);

#if FE_CVO_DEBUG
	feLog("CurvaceousOp::handle brush %d brushed %d frame %.6G\n",
			spDrawBrush.isValid(),m_brushed,m_frame);
#endif

	if(spDrawBrush.isValid())
	{
		//* HACK
		I32 pickPrimitive= -1;
		I32 pickVertex= -1;
		Real pickU=0.0;

		spDrawBrush->pushDrawMode(m_spSolid);

		m_brushed=TRUE;

		const Color black(0.0,0.0,0.0);
		const Color white(1.0,1.0,0.8);
		const Color grey(0.8,0.8,1.0);
		const Color darkgrey(0.5,0.5,0.5);
		const Color red(0.8,0.0,0.0);
		const Color green(0.0,0.5,0.0);
		const Color blue(0.0,0.0,1.0);
		const Color cyan(0.0,1.0,1.0);
		const Color magenta(1.0,0.0,1.0);
		const Color pink(0.8,0.4,0.4);
		const Color brown(0.6,0.3,0.0);
		const Color yellow(1.0,0.5,0.0);

		const SpatialVector up(0.0,1.0,0.0);

		sp<ViewI> spView=spDrawOverlay->view();
		sp<CameraI> spCameraI=spView->camera();
		SpatialTransform cameraMatrix=spCameraI->cameraMatrix();
		SpatialTransform cameraTransform;
		invert(cameraTransform,cameraMatrix);

		const SpatialVector deltaX=cameraTransform.column(0);
		const SpatialVector deltaY=cameraTransform.column(1);
		const SpatialVector towardCamera=cameraTransform.column(2);

		const SpatialVector& rRayOrigin=rayOrigin(a_rSignal);
		const SpatialVector& rRayDirection=rayDirection(a_rSignal);

		if(m_spOutput.isValid() && modifyMode!="None")
		{
			m_event.bind(windowEvent(a_rSignal));

#if FE_CVO_DEBUG
			feLog("%s\n",c_print(m_event));
			feLog("ray %s  %s\n",c_print(rRayOrigin),c_print(rRayDirection));
#endif

			const Real maxDistance=100.0;
			sp<SurfaceI::ImpactI> spImpact=m_spOutput->rayImpact(
					rRayOrigin,rRayDirection,maxDistance);

			SpatialVector intersection;

			if(spImpact.isValid())
			{
				sp<SurfaceSearchable::Impact> spSearchableImpact=spImpact;
				if(spSearchableImpact.isValid())
				{
					pickPrimitive=spSearchableImpact->triangleIndex();
					pickU=spSearchableImpact->uv()[0];

					const I32 subCount=
							m_spOutputVertices->subCount(pickPrimitive);
					pickVertex=pickU*(subCount-1.0)+0.499;
				}

#if FE_CVO_DEBUG
				intersection=spImpact->intersection();
				feLog("intersection %s\n",c_print(intersection));
#endif
			}
			else
			{
				set(intersection);
			}

			U32 item=m_event.item();
//			BWORD shifted=FALSE;
			if(item&WindowEvent::e_keyShift)
			{
//				shifted=TRUE;
				item^=WindowEvent::e_keyShift;
			}
//			BWORD ctrled=FALSE;
			if(item&WindowEvent::e_keyControl)
			{
//				ctrled=TRUE;
				item^=WindowEvent::e_keyControl;
			}

			SpatialTransform pickedTip;

			if(m_event.isKeyPress(WindowEvent::e_itemAny))
			{
				if(item==WindowEvent::e_keyEscape)
				{
					m_pickId= -1;
					m_pickVertex= -1;
					m_pickU=0.0;
					m_pickDistance=0.0;
					pickPrimitive= -1;
				}
				if(item==WindowEvent::e_keyCarriageReturn)
				{
				}
				if(item==WindowEvent::e_keyDelete)
				{
				}
			}

			if(!m_dragging && m_event.isMouseRelease(WindowEvent::e_itemLeft))
			{
				m_pickId=lookupId(pickPrimitive);
				m_pickVertex=pickVertex;
				m_pickU=pickU;
				m_pickDistance=magnitude(intersection-rRayOrigin);
				if(m_pickId<0)
				{
					m_pickVertex= -1;
					m_pickU=0.0;
					m_pickDistance=0.0;
				}
			}
			const SpatialVector target=rRayOrigin+rRayDirection*m_pickDistance;

			const WindowEvent::MouseButtons buttons=m_event.mouseButtons();
			if(buttons)
			{
				//* TODO screen aware scale
				const Real moveScale=1e-2;

				const I32 mouseX=m_event.mouseX();
				const I32 mouseY=m_event.mouseY();

				const I32 moveX=mouseX-m_lastX;
				const I32 moveY=mouseY-m_lastY;

				m_lastX=mouseX;
				m_lastY=mouseY;
				m_dragging=m_event.isMouseDrag();

				if(m_dragging)
				{
					const SpatialVector delta=
							moveScale*(deltaX*moveX+deltaY*moveY);

					if(modifyMode=="Drag")
					{
						movePicked(delta);
					}
					else
					{
						combPicked(target,delta);
					}
				}
			}
		}

		if(spDrawOverlay.isValid())
		{
			spDrawOverlay->pushDrawMode(m_spSolid);

			if(m_pickId>=0)
			{
				pickPrimitive=lookupIndex(m_pickId);
				pickVertex=m_pickVertex;
				pickU=m_pickU;

				m_pickArray.clear();	//* TODO avoid redundant repeats
			}

			CurveMode curveMode=OperatorSurfaceCommon::e_null;
			if(displayMode=="Lines")
			{
				curveMode=OperatorSurfaceCommon::e_lines;
			}
			else if(displayMode=="Cylinders")
			{
				curveMode=OperatorSurfaceCommon::e_cylinders;
			}
			else if(displayMode=="Tubes")
			{
				curveMode=OperatorSurfaceCommon::e_tube;
			}
			else if(displayMode=="panels")
			{
				curveMode=OperatorSurfaceCommon::e_panels;
			}

			const I32 primitiveCount=m_spOutputVertices->count();

			I32 maxCount=0;
			for(I32 primitiveIndex=0;primitiveIndex<primitiveCount;
					primitiveIndex++)
			{
				const I32 subCount=
						m_spOutputVertices->subCount(primitiveIndex);
				if(maxCount<subCount)
				{
					maxCount=subCount;
				}
			}

			SpatialVector* pointArray=new SpatialVector[maxCount];
			SpatialVector* normalArray=new SpatialVector[maxCount];
			Color* colorArray=new Color[maxCount];
			Real* radiusArray=new Real[maxCount];

			for(I32 primitiveIndex=0;primitiveIndex<primitiveCount;
					primitiveIndex++)
			{
				const I32 subCount=
						m_spOutputVertices->subCount(primitiveIndex);


				for(I32 subIndex=0;subIndex<subCount;subIndex++)
				{
					pointArray[subIndex]=m_spOutputVertices->spatialVector(
							primitiveIndex,subIndex);
					const I32 pointIndex=m_spOutputVertices->integer(
							primitiveIndex,subIndex);

					if(m_spInputColor.isValid())
					{
						Vector4 vector4=
								m_spInputColor->spatialVector(pointIndex);
						vector4[3]=1.0;
						colorArray[subIndex]=vector4;
					}
					else
					{
						colorArray[subIndex]=cyan;
					}

					const SpatialVector towards=
							unitSafe(pointArray[subIndex]-rRayOrigin);
					const Real brighten=1.0-
							pow(1.0-fabs(dot(towards,rRayDirection)),0.1);
					colorArray[subIndex]+=
							brighten*(white-colorArray[subIndex]);

					const Real modNormalBias=
							pow(normalBias,1.0-brighten);

					normalArray[subIndex]=unitSafe(
							modNormalBias*towardCamera+
							(1.0-modNormalBias)*normalArray[subIndex]);

					radiusArray[subIndex]=radiusScale*
							(m_spInputRadius.isValid()?
							m_spInputRadius->real(pointIndex): 1.0);
				}

				const BWORD multicolor=TRUE;
				drawTube(spDrawBrush,pointArray,normalArray,radiusArray,
						subCount,curveMode,multicolor,colorArray,towardCamera);
			}

			delete[] radiusArray;
			delete[] colorArray;
			delete[] normalArray;
			delete[] pointArray;

			if(pickPrimitive>=0)
			{
				highlightCurve(spView,spDrawOverlay,m_spOutputVertices,
						pickPrimitive,pickVertex,&cyan,&black,&yellow);

				const SpatialVector searchPoint=
						m_spOutputVertices->spatialVector(pickPrimitive,0);
//				const SpatialVector pickPoint=
//						m_spOutputVertices->spatialVector(
//						pickPrimitive,pickVertex);

				const Real extent=pickRadius+pickFalloff;
				const Real extent2=extent*extent;

				const Real range=uRange+uFalloff;
				const Real unitVertex=pickVertex/
							(m_spOutputVertices->subCount(pickPrimitive)-1.0);

				if(m_spDriver.isValid())
				{
					sp<SurfaceI::ImpactI> spImpact=
							m_spDriver->nearestPoint(searchPoint);
					if(spImpact.isValid())
					{
						const Vector2 uv=spImpact->uv();
						SpatialTransform xform=m_spDriver->sample(uv);
						rotate(xform,-90.0*degToRad,e_xAxis);

						const SpatialVector circleScale(extent,
								extent,extent);

						spDrawBrush->drawCircle(xform,&circleScale,green);
					}
				}

				for(I32 primitiveIndex=0;primitiveIndex<primitiveCount;
						primitiveIndex++)
				{
					const SpatialVector rootPoint=
							m_spOutputVertices->spatialVector(primitiveIndex,0);
					const Real dist2=magnitudeSquared(rootPoint-searchPoint);

					if(dist2>extent2)
					{
						continue;
					}

					const Real dist=sqrt(dist2);
					Real primitiveWeight=1.0;
					if(primitiveIndex!=pickPrimitive &&
							pickFalloff>0.0 && dist>pickRadius)
					{
						primitiveWeight-=(dist-pickRadius)/pickFalloff;
					}
					const Color blueish(0.0,primitiveWeight,
							0.3+0.7*primitiveWeight);

					highlightCurve(spView,spDrawOverlay,m_spOutputVertices,
							primitiveIndex,-1,&blueish,&black,NULL);

					const I32 subCount=
							m_spOutputVertices->subCount(primitiveIndex);
					for(I32 subIndex=1;subIndex<subCount;subIndex++)
					{
						const SpatialVector point=
								m_spOutputVertices->spatialVector(
								primitiveIndex,subIndex);

						const Real unitIndex=subIndex/(subCount-1.0);
						const Real pointDist=fabs(unitIndex-unitVertex);

						if(pointDist>range)
						{
							continue;
						}

						Real pointWeight=1.0;
						if(!(primitiveIndex==pickPrimitive &&
								subIndex==pickVertex) &&
								uFalloff>0.0 && pointDist>uRange)
						{
							pointWeight-=(pointDist-uRange)/uFalloff;
						}

						const Real productWeight=primitiveWeight*pointWeight;
						if(productWeight<0.01)	//* tweak
						{
							continue;
						}

						const Color reddish(1.0,productWeight,0.0);

						drawDot(spView,spDrawOverlay,point,3.0,reddish);

						pick(lookupId(primitiveIndex),subIndex,productWeight);
					}
				}
			}

			spDrawOverlay->popDrawMode();
		}

		spDrawBrush->popDrawMode();

		return;
	}

	access(m_spInputPoint,"Input Surface",e_point,e_position,e_quiet);

	access(m_spInputVertices,"Input Surface",e_primitive,e_vertices,e_quiet);

	access(m_spInputColor,"Input Surface",e_point,e_color,e_quiet);

	access(m_spInputRadius,"Input Surface",e_point,radiusAttr,e_quiet);

	if(!accessOutput(m_spOutputVertices,a_rSignal,
			e_primitive,e_vertices)) return;

	if(!accessOutput(m_spOutputPoint,a_rSignal,
			e_point,e_position)) return;

	accessOutput(m_spOutputNormal,a_rSignal,e_point,e_normal,e_quiet);

	if(!accessOutput(m_spOutput,a_rSignal)) return;

	access(m_spDriver,"Driver Surface",e_quiet);

	m_frame=currentFrame(a_rSignal);

	m_brushed=FALSE;

#if FE_CVO_DEBUG
	const BWORD frameChanged=(m_frame!= m_lastFrame);
	const BWORD replaced=(catalog<bool>("Output Surface","replaced"));
	const BWORD paramChanged=(!m_brushed && !replaced && !frameChanged);

	feLog("CurvaceousOp::handle frame %.6G->%.6G paramChanged %d"
			" frameChanged %d replaced %d\n",
			m_lastFrame,m_frame,paramChanged,frameChanged,replaced);
#endif

//	const BWORD rebuild=(paramChanged || frameChanged || replaced);

	m_lastFrame=m_frame;

	if(m_spLengthCorrect.isValid())
	{
		sp<Catalog> spOtherCatalog=m_spLengthCorrect;
		if(spOtherCatalog.isValid())
		{
			spOtherCatalog->catalog< sp<Component> >("Input Surface")=
					catalog< sp<Component> >("Input Surface");
		}
		m_spLengthCorrect->handle(a_rSignal);
	}
}

void CurvaceousOp::setupState(void)
{
	m_aIndex.populate(m_spScope,"Move","Index");
	m_aOffset.populate(m_spScope,"Move","Offset");

	m_spEditMap=m_spState->lookupMap(m_aIndex);
}

BWORD CurvaceousOp::loadState(const String& rBuffer)
{
	const BWORD success=OperatorState::loadState(rBuffer);

	m_spEditMap=m_spState->lookupMap(m_aIndex);

	return success;
}

void CurvaceousOp::movePicked(const SpatialVector& a_rDelta)
{
#if FE_CVO_EDIT_DEBUG
	feLog("CurvaceousOp::movePicked %s\n",c_print(a_rDelta));
#endif

	const I32 pickCount=m_pickArray.size();
	for(I32 pickIndex=0;pickIndex<pickCount;pickIndex++)
	{
		const Pick& rPick=m_pickArray[pickIndex];
		const I32 primitiveIndex=rPick.m_elementId;
		const I32 subIndex=rPick.m_subIndex;
		const Real pointWeight=rPick.m_weight;

		const I32 primitiveCount=m_spOutputVertices->count();
		if(subIndex<0 || primitiveIndex>=primitiveCount)
		{
			continue;
		}

		const I32 pointIndex=
				m_spOutputVertices->integer(primitiveIndex,subIndex);

		const I32 pointCount=m_spOutputPoint->count();
		if(pointIndex>=pointCount)
		{
			continue;
		}

		SpatialVector point=m_spOutputPoint->spatialVector(pointIndex);
		point+=pointWeight*a_rDelta;

		m_spOutputPoint->set(pointIndex,point);

#if FE_CVO_EDIT_DEBUG
		feLog("CurvaceousOp::movePicked %d vert %d:%d pt %d to %s\n",
				pickIndex,primitiveIndex,subIndex,pointIndex,c_print(point));
#endif
	}
}

void CurvaceousOp::combPicked(const SpatialVector& a_rTarget,
	const SpatialVector& a_rDelta)
{
#if FE_CVO_EDIT_DEBUG
	feLog("CurvaceousOp::combPicked %s by %s\n",
			c_print(a_rTarget),c_print(a_rDelta));
#endif

	const I32 pickCount=m_pickArray.size();
	for(I32 pickIndex=0;pickIndex<pickCount;pickIndex++)
	{
		const Pick& rPick=m_pickArray[pickIndex];
		const I32 primitiveIndex=rPick.m_elementId;
		const I32 subIndex=rPick.m_subIndex;
//		const Real pointWeight=rPick.m_weight;

		const I32 primitiveCount=m_spOutputVertices->count();
		if(subIndex<0 || primitiveIndex>=primitiveCount)
		{
			continue;
		}

		const I32 pointIndex=
				m_spOutputVertices->integer(primitiveIndex,subIndex);

		const I32 pointCount=m_spOutputPoint->count();
		if(pointIndex>=pointCount)
		{
			continue;
		}

		SpatialVector point=m_spOutputPoint->spatialVector(pointIndex);

		const Real pointWeight=1.0/(1.0+10.0*magnitude(point-a_rTarget));

		point+=pointWeight*a_rDelta;

		m_spOutputPoint->set(pointIndex,point);

#if FE_CVO_EDIT_DEBUG
		feLog("CurvaceousOp::combPicked %d vert %d:%d pt %d to %s\n",
				pickIndex,primitiveIndex,subIndex,pointIndex,c_print(point));
#endif
	}
}
