/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __oplab_XRayOp_h__
#define __oplab_XRayOp_h__
namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Operator to look inside primitives

	@ingroup oplab
*//***************************************************************************/
class FE_DL_EXPORT XRayOp:
	public OperatorSurfaceCommon,
	public Initialize<XRayOp>
{
	public:

						XRayOp(void):
							m_brushed(FALSE)								{}

virtual					~XRayOp(void)										{}

		void			initialize(void);

						//* As HandlerI
virtual	void			handle(Record& a_rSignal);

	private:
		sp<DrawMode>				m_spSolid;

		Array< sp<SurfaceI> >		m_surfaceArray;
		sp<DrawI>					m_spRayTrace;
		WindowEvent					m_event;

		BWORD						m_brushed;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __oplab_XRayOp_h__ */
