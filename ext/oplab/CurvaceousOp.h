/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __oplab_CurvaceousOp_h__
#define __oplab_CurvaceousOp_h__
namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Operator to interactively alter curves

	@ingroup oplab
*//***************************************************************************/
class FE_DL_EXPORT CurvaceousOp:
	public OperatorState,
	public Initialize<CurvaceousOp>
{
	public:

						CurvaceousOp(void):
							m_lastFrame(0.0),
							m_brushed(FALSE),
							m_dragging(FALSE),
							m_pickId(-1),
							m_pickVertex(-1),
							m_pickU(0.0),
							m_pickDistance(0.0)								{}

virtual					~CurvaceousOp(void)									{}

		void			initialize(void);

						//* As HandlerI
virtual	void			handleBind(sp<SignalerI> a_spSignalerI,
								sp<Layout> a_spLayout);
virtual	void			handle(Record& a_rSignal);

	protected:

virtual	void			setupState(void);
virtual	BWORD			loadState(const String& rBuffer);

	private:

		void			movePicked(const SpatialVector& a_rDelta);
		void			combPicked(const SpatialVector& a_rTarget,
								const SpatialVector& a_rDelta);

		sp<OperatorSurfaceI>		m_spLengthCorrect;

		sp<SurfaceI>				m_spOutput;
		sp<SurfaceI>				m_spDriver;
		sp<SurfaceAccessorI>		m_spInputIds;
		sp<SurfaceAccessorI>		m_spInputPoint;
		sp<SurfaceAccessorI>		m_spInputColor;
		sp<SurfaceAccessorI>		m_spInputRadius;
		sp<SurfaceAccessorI>		m_spOutputPoint;
		sp<SurfaceAccessorI>		m_spInputVertices;
		sp<SurfaceAccessorI>		m_spOutputVertices;
		sp<SurfaceAccessorI>		m_spOutputNormal;
		WindowEvent					m_event;

		sp<DrawMode>				m_spSolid;
		sp<DrawMode>				m_spWideLine[5];

		sp< RecordMap<I32> >		m_spEditMap;
		Accessor<SpatialVector>		m_aOffset;

		Real						m_frame;
		Real						m_lastFrame;

		BWORD						m_brushed;
		BWORD						m_dragging;

		I32							m_pickId;
		I32							m_pickVertex;
		Real						m_pickU;
		Real						m_pickDistance;

		I32							m_lastX;
		I32							m_lastY;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __oplab_CurvaceousOp_h__ */
