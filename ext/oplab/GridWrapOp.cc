/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <oplab/oplab.pmh>

#define FE_GWO_DEBUG	FALSE

using namespace fe;
using namespace fe::ext;

void GridWrapOp::initialize(void)
{
	catalog<String>("icon")="FE_alpha";

	//* don't show thread attributes until we start using them
	catalog<bool>("Threads","hidden")=true;
	catalog<bool>("AutoThread","hidden")=true;
	catalog<bool>("Paging","hidden")=true;
	catalog<bool>("StayAlive","hidden")=true;

	catalog<I32>("Grid Width")=10;
	catalog<I32>("Grid Width","high")=100;
	catalog<I32>("Grid Width","max")=1000;
	catalog<bool>("Grid Width","join")=true;

	catalog<I32>("Grid Height")=10;
	catalog<I32>("Grid Height","high")=100;
	catalog<I32>("Grid Height","max")=1000;

	catalog<I32>("Convolutions")=1;
	catalog<I32>("Convolutions","high")=10;
	catalog<I32>("Convolutions","max")=100;

	catalog<String>("Coordinate Attribute")="uv";

	catalog<bool>("Debug")=false;

	catalog< sp<Component> >("Input Surface");

	catalog< sp<Component> >("Driver Surface");

	catalog< sp<Component> >("Deformed Surface");

	catalog< sp<Component> >("Output Surface");
	catalog<String>("Output Surface","IO")="output";
	catalog<String>("Output Surface","copy")="Input Surface";
	catalog<bool>("Output Surface","recycle")=true;

	m_matrixPower.setIterations(8);		//* default 16
}

void GridWrapOp::handle(Record& a_rSignal)
{
#if FE_GWO_DEBUG
	feLog("GridWrapOp::handle\n");
#endif

	sp<DrawI> spDrawDebug;
	if(catalog<BWORD>("Debug") && !accessGuide(spDrawDebug,a_rSignal)) return;

	sp<SurfaceAccessibleI> spInputAccessible;
	if(!access(spInputAccessible,"Input Surface")) return;

	sp<SurfaceI> spDriver;
	if(!access(spDriver,"Driver Surface")) return;

	spDriver->setRefinement(0);
	spDriver->prepareForSample();

	sp<SurfaceI> spDeformed;
	if(!access(spDeformed,"Deformed Surface")) return;

	spDeformed->setRefinement(0);
	spDeformed->prepareForSample();

	sp<SurfaceAccessorI> spInputPoint;
	if(!access(spInputPoint,spInputAccessible,e_point,e_position)) return;

	sp<SurfaceAccessorI> spOutputPoint;
	if(!accessOutput(spOutputPoint,a_rSignal,e_point,e_position)) return;

	const U32 count=spInputPoint->count();
	if(!count)
	{
		feLog("GridWrapOp input point count is zero\n");
		catalog<String>("error")="input points required";
		return;
	}

	const String coordinateAttribute=catalog<String>("Coordinate Attribute");

	sp<SurfaceAccessorI> spInputUV;
	if(!access(spInputUV,spInputAccessible,e_point,coordinateAttribute)) return;

	const I32 gridWidth=catalog<I32>("Grid Width");
	const I32 gridHeight=catalog<I32>("Grid Height");
	const I32 convolutions=catalog<I32>("Convolutions");

	const U32 gridCount=gridWidth*gridHeight;

	GridNode* grid[2];
	grid[0]=new GridNode[gridCount];
	grid[1]=new GridNode[gridCount];

	U32 writeIndex=0;
	GridNode* writeGrid=grid[writeIndex];
	GridNode* readGrid=grid[!writeIndex];

	for(U32 gridIndex=0;gridIndex<gridCount;gridIndex++)
	{
		setIdentity(writeGrid[gridIndex].m_transform);
		setIdentity(readGrid[gridIndex].m_transform);

		writeGrid[gridIndex].m_weight=0.0;
		readGrid[gridIndex].m_weight=0.0;
	}

	for(U32 index=0;index<count;index++)
	{
		const SpatialVector inputPoint=spInputPoint->spatialVector(index);
		const SpatialVector inputUV=spInputUV->spatialVector(index);

		sp<SurfaceI::ImpactI> spImpact=spDriver->nearestPoint(inputPoint);
		if(!spImpact.isValid())
		{
			continue;
		}

		I32 triangleIndex=spImpact->triangleIndex();
		Barycenter<Real> barycenter=spImpact->barycenter();

#if FE_GWO_DEBUG
		feLog("\nGridWrapOp::handle %d tri %d bary %s\n",
				index,triangleIndex,c_print(barycenter));
		feLog("  uv %s du %s dv %s\n",c_print(inputUV),
				c_print(spImpact->du()),c_print(spImpact->dv()));
#endif

		const SpatialTransform xformDriver=
				spDriver->sample(triangleIndex,barycenter);
		const SpatialTransform xformDeformed=
				spDeformed->sample(triangleIndex,barycenter);
		SpatialTransform invDriver;
		invert(invDriver,xformDriver);
		const SpatialTransform conversion=invDriver*xformDeformed;

		const I32 gridU=gridWidth*inputUV[0]-1e-3;
		const I32 gridV=gridHeight*inputUV[1]-1e-3;

		if(gridU<0 || gridU>=gridWidth || gridV<0 || gridV>=gridHeight)
		{
			continue;
		}

		const I32 gridIndex=gridWidth*gridV+gridU;

		//* TODO cumulative
		writeGrid[gridIndex].m_transform=conversion;
		writeGrid[gridIndex].m_weight+=1.0;

#if FE_GWO_DEBUG
		feLog("  scan %d,%d %d\n",
				gridU,gridV,gridIndex);
		feLog("driver\n%s\ndeformed\n%s\nconversion\n%s\n",
				c_print(xformDriver),c_print(xformDeformed),
				c_print(writeGrid[gridIndex].m_transform));
#endif

		if(spDrawDebug.isValid())
		{
			const Color red(1.0,0.0,0.0);
			const Color yellow(1.0,1.0,0.0);
			const Color magenta(1.0,0.0,1.0);
			const Color cyan(0.0,1.0,1.0);

			SpatialVector line[2];

			line[0]=inputPoint;
			line[1]=line[0]+spImpact->du();

			spDrawDebug->drawLines(line,NULL,2,DrawI::e_strip,false,&magenta);

			line[0]=inputPoint;
			line[1]=line[0]+spImpact->dv();

			spDrawDebug->drawLines(line,NULL,2,DrawI::e_strip,false,&cyan);

			line[0]=inputPoint;
			line[1]=xformDriver.translation();

			spDrawDebug->drawLines(line,NULL,2,DrawI::e_strip,false,&yellow);

			line[0]=xformDriver.translation();
			line[1]=xformDeformed.translation();

			spDrawDebug->drawLines(line,NULL,2,DrawI::e_strip,false,&red);
		}
	}

	for(I32 convolution=0;convolution<convolutions;convolution++)
	{
		writeIndex=!writeIndex;
		writeGrid=grid[writeIndex];
		readGrid=grid[!writeIndex];

		for(I32 gridV=0;gridV<gridHeight;gridV++)
		{
			const I32 gridVV=gridV*gridWidth;
			for(I32 gridU=0;gridU<gridWidth;gridU++)
			{
				GridNode& rGridWrite=writeGrid[gridVV+gridU];

				SpatialTransform blend=readGrid[gridVV+gridU].m_transform;
				Real sumWeight=1.0;

#if FE_GWO_DEBUG
				feLog("GridWrapOp::handle convolution at %d %d/%d %d/%d\n",
						convolution,gridU,gridWidth,gridV,gridHeight);
#endif

				const I32 maxV=gridV<gridHeight-1? 2: 1;
				for(I32 kernelV=(gridV? -1: 0);kernelV<maxV;kernelV++)
				{
					const I32 kernelVV=gridVV+kernelV*gridWidth+gridU;

					const I32 maxU=gridU<gridWidth-1? 2: 1;
					for(I32 kernelU=(gridU? -1: 0);kernelU<maxU;kernelU++)
					{
						GridNode& rGridRead=readGrid[kernelVV+kernelU];
						if(rGridRead.m_weight<=0.0)
						{
#if FE_GWO_DEBUG
							feLog("GridWrapOp::handle"
									" convolution skip %d,%d %d,%d\n",
									gridU,gridV,kernelU,kernelV);
#endif
							continue;
						}

						const SpatialTransform& rTransform=
								rGridRead.m_transform;

#if FE_GWO_DEBUG
						feLog("GridWrapOp::handle"
								" convolution accum %d,%d %d,%d\n%s\n",
								gridU,gridV,kernelU,kernelV,
								c_print(rTransform));
#endif

						//* NOTE transform aren't really commutative

						sumWeight+=1.0;

						SpatialTransform invBlend;
						invert(invBlend,blend);
						const SpatialTransform delta=invBlend*rTransform;
						const Real fraction=1.0/sumWeight;

						SpatialTransform partial;
						m_matrixPower.solve(partial,delta,fraction);
						blend*=partial;

#if FE_GWO_DEBUG
						feLog("GridWrapOp::handle"
								" weight %.6G\n"
								" blend\n%s\n",
								sumWeight,
								c_print(blend));
#endif
					}
				}

				rGridWrite.m_transform=blend;
				rGridWrite.m_weight=1.0;

#if FE_GWO_DEBUG
				feLog("GridWrapOp::handle result\n%s\n",
						c_print(rGridWrite.m_transform));
#endif
			}
		}
	}

	writeIndex=!writeIndex;
	writeGrid=grid[writeIndex];
	readGrid=grid[!writeIndex];

	for(U32 index=0;index<count;index++)
	{
		const SpatialVector inputPoint=spInputPoint->spatialVector(index);
		const SpatialVector inputUV=spInputUV->spatialVector(index);

		const I32 gridU=gridWidth*inputUV[0]-1e-3;
		const I32 gridV=gridHeight*inputUV[1]-1e-3;

		if(gridU<0 || gridU>=gridWidth || gridV<0 || gridV>=gridHeight)
		{
			continue;
		}

		const I32 gridIndex=gridWidth*gridV+gridU;

		//* TODO blend
		const SpatialTransform& rConversion=readGrid[gridIndex].m_transform;

		SpatialVector transformed;
		transformVector(rConversion,inputPoint,transformed);

		spOutputPoint->set(index,transformed);

		if(spDrawDebug.isValid())
		{
			const Color green(0.0,1.0,0.0);

			SpatialVector line[2];

			line[0]=inputPoint;
			line[1]=transformed;

			spDrawDebug->drawLines(line,NULL,2,DrawI::e_strip,false,&green);
		}
	}

	delete[] grid[1];
	delete[] grid[0];

#if FE_GWO_DEBUG
	feLog("GridWrapOp::handle done\n");
#endif
}
