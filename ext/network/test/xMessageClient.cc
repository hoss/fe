/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "network/network.pmh"
#include "math/math.h"

#include <thread>
#include <chrono>

using namespace fe;
using namespace fe::ext;

int main(int argc,char** argv)
{
	UNIT_START();

	BWORD complete=FALSE;

	milliSleep(100);

	try
	{
		{
			sp<SingleMaster> spSingleMaster=SingleMaster::create();
			sp<Master> spMaster=spSingleMaster->master();
			sp<Registry> spRegistry=spMaster->registry();

			Result result=spRegistry->manage("feAutoLoadDL");
			UNIT_TEST(successful(result));

			String implementation="ConnectedCatalog";
			if(argc>1)
			{
				implementation=argv[1];
			}
			feLog("implementation \"%s\"\n",implementation.c_str());

			const BWORD nosend=(argc>2 && argv[2]==String("nosend"));
			I32 messageCount=nosend? 0: 2;

			sp<StateCatalog> spStateCatalog=
					spMaster->registry()->create(implementation);
			UNIT_TEST(spStateCatalog.isValid());
			if(!spStateCatalog.isValid())
			{
				feX(argv[0],"spStateCatalog invalid");
			}

			result=spStateCatalog->configure("localhost:7890"
					" role=client");

			result=spStateCatalog->start();
			UNIT_TEST(successful(result));

			feLog("StateCatalog started\n");

			if(successful(result))
			{
				spStateCatalog->waitForConnection();

				String netID;
				spStateCatalog->getState<String>("net:id",netID);
				feLog("net:id \"%s\"\n",netID.c_str());

				I32 frame=0;
				while(frame++ < 1000)
				{
					sp<StateCatalog::Snapshot> spSnapshot;
					result=spStateCatalog->snapshot(spSnapshot);

					if(messageCount && frame%100 == 50)
					{
						messageCount--;

						String message;
						message.sPrintf("client %s is on frame %d",
								netID.c_str(),frame);
						spStateCatalog->sendMessage<String>(
								"request",message);

						spStateCatalog->flush();
					}

					std::this_thread::sleep_for(std::chrono::milliseconds(1));
				}

				spStateCatalog->stop();

				feLog("End State:\n");
				spStateCatalog->catalogDump();
			}
		}

		complete=TRUE;
	}
	catch(Exception &e) { e.log(); }
	catch(...)			{ feLog("uncaught exception\n"); }

	UNIT_TEST(complete);
	UNIT_TEST(!Counted::trackerCount());
	UNIT_TRACK(5);
	UNIT_RETURN();
}
