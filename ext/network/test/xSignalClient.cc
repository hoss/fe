/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

/*	for latency testing (on/off):
	# tc qdisc add dev lo root netem delay 100ms
	# tc qdisc del dev lo root
*/

#include "network/network.pmh"
#include "math/math.h"

#include <thread>
#include <chrono>

#define	TEST_ATOMIC				FALSE
#define	TEST_CLIENT_SIGNALS		TRUE
#define	TEST_CLIENT_RECORDS		FALSE

using namespace fe;
using namespace fe::ext;

#if TEST_ATOMIC
volatile BWORD gs_keepWaiting=TRUE;
#endif

class MyHandler:
	virtual public HandlerI,
	CastableAs<MyHandler>
{
	public:
				MyHandler(void):
					m_signalCount(0)
				{	setName("MyHandler"); }
virtual	void	handle(Record &a_signal)
				{
					sp<Scope> spScope=a_signal.layout()->scope();
					if(m_aFrameIndex.scope()!=spScope)
					{
						m_aFrameIndex.initialize(spScope,"frameIndex");
						m_aPayload.initialize(spScope,"payload");
					}

//					feLog("Signal: Scope %p frameIndex %d payload \"%s\"\n",
//							spScope.raw(),m_aFrameIndex(a_signal),
//							m_aPayload(a_signal).c_str());

					//* NOTE just count up the signals
					m_signalCount++;

#if TEST_ATOMIC
					if(!(m_aFrameIndex(a_signal)%10))
					{
						gs_keepWaiting=FALSE;
					}
#endif
				}
virtual	void	handleBind(sp<SignalerI> spSignalerI, sp<Layout> spLayout)
				{
//					m_aFrameIndex.initialize(spLayout->scope(), "frameIndex");
				}
		I32		signalCount(void) const		{ return m_signalCount; }
	private:
		Accessor<int>		m_aFrameIndex;
		Accessor<String>	m_aPayload;
		I32					m_signalCount;
};

#if TEST_CLIENT_SIGNALS
class IdHandler:
	virtual public HandlerI,
	CastableAs<IdHandler>
{
	public:
				IdHandler(void):
					m_idCount(0)
				{	setName("IdHandler"); }
		void	bind(sp<StateCatalog> a_spStateCatalog)
				{	m_spStateCatalog=a_spStateCatalog; }
virtual	void	handle(Record &a_signal)
				{
					sp<Scope> spScope=a_signal.layout()->scope();
					if(m_aId.scope()!=spScope)
					{
						m_aId.initialize(spScope,"node:id");
					}

					I32 frame(0);
					m_spStateCatalog->getState<I32>("frame",frame);

					String netID;
					m_spStateCatalog->getState<String>("net:id",netID);
					m_spStateCatalog->setState<I32>(netID+":frameplus",frame+1);

					feLog("Signal: Scope %p Id \"%s\" on frame %d\n",
							spScope.raw(),m_aId(a_signal).c_str(),frame);

					m_idCount++;
				}
		I32		idCount(void) const		{ return m_idCount; }
	private:
		Accessor<String>	m_aId;
		I32					m_idCount;
		sp<StateCatalog>	m_spStateCatalog;
};
class TimeHandler:
	virtual public HandlerI,
	CastableAs<TimeHandler>
{
	public:
				TimeHandler(void):
					m_timeCount(0)
				{	setName("TimeHandler"); }
		void	bind(sp<StateCatalog> a_spStateCatalog)
				{	m_spStateCatalog=a_spStateCatalog; }
virtual	void	handle(Record &a_signal)
				{
					sp<Scope> spScope=a_signal.layout()->scope();
					if(m_aTime.scope()!=spScope)
					{
						m_aTime.initialize(spScope,"clock:time");
					}

					I32 frame(0);
					m_spStateCatalog->getState<I32>("frame",frame);

					feLog("Signal: Scope %p Time \"%s\" on frame %d\n",
							spScope.raw(),m_aTime(a_signal).c_str(),frame);

					m_timeCount++;
				}
		I32		timeCount(void) const		{ return m_timeCount; }
	private:
		Accessor<String>	m_aTime;
		I32					m_timeCount;
		sp<StateCatalog>	m_spStateCatalog;
};
#endif

int main(int argc,char** argv)
{
	UNIT_START();

	BWORD complete=FALSE;

	milliSleep(100);

	try
	{
		{
			sp<SingleMaster> spSingleMaster=SingleMaster::create();
			sp<Master> spMaster=spSingleMaster->master();
			sp<Registry> spRegistry=spMaster->registry();

//			assertMath(spMaster->typeMaster());

			Result result=spRegistry->manage("feAutoLoadDL");
			UNIT_TEST(successful(result));

			String implementation="ConnectedCatalog";
			if(argc>1)
			{
				implementation=argv[1];
			}
			feLog("implementation \"%s\"\n",implementation.c_str());

			String transport="tcp";
			if(argc>2)
			{
				transport=argv[2];
			}
			feLog("transport \"%s\"\n",transport.c_str());

			U16 port=7890;
			if(argc>3)
			{
				port=atoi(argv[3]);
			}
			feLog("port %d\n",port);

			sp<StateCatalog> spStateCatalog=
					spMaster->registry()->create(implementation);
			UNIT_TEST(spStateCatalog.isValid());
			if(!spStateCatalog.isValid())
			{
				feX(argv[0],"spStateCatalog invalid");
			}

			String configuration;
			if(transport=="shm")
			{
				configuration.sPrintf("/xSignalServer");
			}
			else if(transport=="ipc")
			{
				configuration.sPrintf("/tmp/feeds/0");
			}
			else
			{
				configuration.sPrintf("localhost:%d",port);
			}
			configuration.catf(" role=client transport=%s",transport.c_str());
			result=spStateCatalog->configure(configuration);

			sp<SignalerI> spMessageSignalerFrame=
					spRegistry->create("*.MessageSignaler");
			spStateCatalog->addListener(spMessageSignalerFrame,"test_signal");
			sp<MyHandler> spMyHandler(new MyHandler);
			spMessageSignalerFrame->insert(spMyHandler,sp<Layout>(NULL));

			sp<Scope> spScope=spRegistry->create("Scope");

#if TEST_CLIENT_SIGNALS
			sp<SignalerI> spSignalerI=spRegistry->create("SignalerI");

			//* Id Signal
			sp<Layout> spLayoutId = spScope->declare("id_update");

			Accessor<String> aNodeId(spScope,"node:id");
			spLayoutId->populate(aNodeId);

			sp<HandlerI> spSignalMessengerId=
					spRegistry->create("*.SignalMessenger");
			sp<StateBindI> spStateBindId(spSignalMessengerId);
			if(spStateBindId.isValid())
			{
				spStateBindId->bind(spStateCatalog);
				spStateBindId->setKey("id_signal");
			}

			spSignalerI->insert(spSignalMessengerId,spLayoutId);

			Record signalId=spScope->createRecord(spLayoutId);

			sp<SignalerI> spMessageSignalerId=
					spRegistry->create("*.MessageSignaler");
			spStateCatalog->addListener(spMessageSignalerId,"id_signal");
			sp<IdHandler> spIdHandler(new IdHandler);
			spIdHandler->bind(spStateCatalog);
			spMessageSignalerId->insert(spIdHandler,sp<Layout>(NULL));

			//* Time Signal
			sp<Layout> spLayoutTime = spScope->declare("time");

			Accessor<String> aTime(spScope,"clock:time");
			spLayoutTime->populate(aTime);

			sp<HandlerI> spSignalMessengerTime=
					spRegistry->create("*.SignalMessenger");
			sp<StateBindI> spStateBindTime(spSignalMessengerTime);
			if(spStateBindTime.isValid())
			{
				spStateBindTime->bind(spStateCatalog);
				spStateBindTime->setKey("time_signal");
			}

			spSignalerI->insert(spSignalMessengerTime,spLayoutTime);

			Record signalTime=spScope->createRecord(spLayoutTime);

			sp<SignalerI> spMessageSignalerTime=
					spRegistry->create("*.MessageSignaler");
			spStateCatalog->addListener(spMessageSignalerTime,"time_signal");
			sp<TimeHandler> spTimeHandler(new TimeHandler);
			spTimeHandler->bind(spStateCatalog);
			spMessageSignalerTime->insert(spTimeHandler,sp<Layout>(NULL));
#endif

#if TEST_CLIENT_RECORDS
			sp<Layout> spManyInts = spScope->declare("many_ints");

			const I32 accessCount(10);
			Accessor<I32> aInt[accessCount];
			for(I32 accessIndex=0;accessIndex<accessCount;accessIndex++)
			{
				String attrName;
				attrName.sPrintf("int[%d]",accessIndex);
				aInt[accessIndex].setup(spScope,attrName);
				spManyInts->populate(aInt[accessIndex]);
			}

			Record manyInts=spScope->createRecord(spManyInts);
#endif

			I32 callbackCount(0);
			StateCatalog::Callback lambda=
					[&callbackCount](String a_name,String a_property)
			{
//				feLog("heard \"%s\" \"%s\"\n",
//						a_name.c_str(),a_property.c_str());
				callbackCount++;
			};
			spStateCatalog->addCallback(lambda,"test_signal");

			I32 changeCount(0);
			StateCatalog::Callback lambda2=
					[&spStateCatalog,&changeCount](
					String a_name,String a_property)
			{
				const String value=
						spStateCatalog->catalogValue(a_name,a_property);

				feLog("change in \"%s\" \"%s\" \"%s\"\n",
						a_name.c_str(),a_property.c_str(),value.c_str());
				changeCount++;
			};
			spStateCatalog->addCallback(lambda2,"running");

			result=spStateCatalog->start();
			UNIT_TEST(successful(result));

			feLog("StateCatalog started\n");

			sp<Layout> spLayoutFrame = spScope->declare("frame_update");

			Accessor<int> frameIndex(spScope,"frameIndex");
			spLayoutFrame->populate(frameIndex);

			if(successful(result))
			{
				spStateCatalog->waitForConnection();

				String netID;
				spStateCatalog->getState<String>("net:id",netID);
				feLog("net:id \"%s\"\n",netID.c_str());

				const I32 sampleStart=100;
				const I32 sampleCount=10;
				I32 sampleIndex=0;
				U32 lastTimeMS=0;
				U32* timeFrame=new U32[sampleCount];
				U32* timeSample=new U32[sampleCount];

				SystemTicker ticker;

				const I32 maxPolls=1200;
				I32 polls=0;
				I32 flushCount=0;
				I32 frame= -1;
				I32 unique=0;
				I32 recordMatches=0;
				bool running=true;
				I32 firstFrame= -1;
				while(running)
				{
//					feLog("FRAME %d connected %d\n",
//							frame,spStateCatalog->connected());
					if(!spStateCatalog->connected())
					{
						feLog("RESTART\n");
						result=spStateCatalog->stop();
						result=spStateCatalog->start();
						UNIT_TEST(successful(result));

						feLog("RECONNECT\n");
						spStateCatalog->waitForConnection();
						feLog("RECONNECTED\n");
					}

					const I32 lastFrame=frame;

					sp<RecordGroup> spRG;

#if TEST_ATOMIC
					{
						gs_keepWaiting=TRUE;
						StateCatalog::Atomic atomic(spStateCatalog,
								flushCount,0,gs_keepWaiting);
						feLog("flushCount %d spins %d"
								" keepWaiting %d locked %d\n",
								flushCount,atomic.spinCount(),
								gs_keepWaiting,atomic.locked());
						result=atomic.getState<bool>("running",running);
						result=atomic.getState<I32>("frame",frame);
						result=atomic.getState< sp<RecordGroup> >("rg",spRG);
					}
#else
					const I32 lastFlushCount=flushCount;
					sp<StateCatalog::Snapshot> spSnapshot;
					while(flushCount==lastFlushCount)
					{
						result=spStateCatalog->snapshot(spSnapshot);
						flushCount=spSnapshot->flushCount();
						if(flushCount==lastFlushCount)
						{
							std::this_thread::sleep_for(
									std::chrono::microseconds(10));

							if(!spStateCatalog->connected())
							{
								break;
							}
						}
					}

					spSnapshot->getState<bool>("running",running);
					spSnapshot->getState<I32>("frame",frame);
					spSnapshot->getState< sp<RecordGroup> >("rg",spRG);
#endif

					if(!(frame%100))
					{
						feLog("frame %d running %d \n",frame,running);
					}

					if(spRG.isValid())
					{
						sp<Scope> spScopeRG=spRG->scope();
						if(spScopeRG.isNull())
						{
							feLog("valid RecordGroup with invalid Scope\n");
						}
						else
						{
							Accessor<int> aCount;
							aCount.initialize(spScopeRG,"count");

							for(RecordGroup::iterator it=spRG->begin();
									it!=spRG->end();it++)
							{
								sp<RecordArray> spRA= *it;
								const I32 recordCount=spRA->length();
								for(I32 recordIndex=0;recordIndex<recordCount;
										recordIndex++)
								{
									const Record record=
											spRA->getRecord(recordIndex);
									const I32 count=aCount(record);
//									feLog("record %d/%d count %d\n",
//											recordIndex,recordCount,
//											aCount(record));
									if(count==frame && frame!=lastFrame)
									{
										recordMatches++;
									}
								}
							}
						}
					}

					if(firstFrame<0)
					{
						firstFrame=frame;

						spStateCatalog->setState<I32>("net:junk",13);

						spStateCatalog->setState<I32>(
								netID+":firstFrame",firstFrame);
						spStateCatalog->flush();
					}

					if(frame!=lastFrame)
					{
#if TEST_CLIENT_SIGNALS
						if(frame%100 == 25)
						{
							String text;
							text.sPrintf("%s:%d",netID.c_str(),frame);

							feLog("SIGNAL ID \"%s\"\n",text.c_str());

							aNodeId(signalId)=text;
							spSignalerI->signal(signalId);

							text=netID+" "+
									System::localTime(
									"%H:%M:%S.${MILLISECONDS}");

							feLog("SIGNAL TIME \"%s\"\n",text.c_str());

							aTime(signalTime)=text;
							spSignalerI->signal(signalTime);

//							spStateCatalog->flush();
						}
#endif

						if(frame%100 == 50)
						{
							spStateCatalog->sendMessage<String>(
									"request","Hello server!");

							String message;
							message.sPrintf("client %s is on frame %d",
									netID.c_str(),frame);
							spStateCatalog->sendMessage<String>(
									"request",message);

							spStateCatalog->flush();
						}

#if TEST_CLIENT_RECORDS
						if(frame%5 == 3)
						{
							static int intCounter(0);

							for(I32 accessIndex=0;accessIndex<accessCount;
									accessIndex++)
							{
								aInt[accessIndex](manyInts)=intCounter++;
							}

							String key;
							key.sPrintf("rec[%s]",netID.c_str());

							spStateCatalog->setState<Record>(key,manyInts);

							spStateCatalog->flush();
						}
#endif
					}

					const U32 timeMS=ticker.timeMS();
					const U32 deltaTimeMS=timeMS-lastTimeMS;
					lastTimeMS=timeMS;
					if(frame>=firstFrame+sampleStart &&
							sampleIndex<sampleCount)
					{
						timeFrame[sampleIndex]=frame;
						timeSample[sampleIndex]=deltaTimeMS;
						sampleIndex++;
					}

					if(successful(result))
					{
						if(frame!=lastFrame)
						{
							unique++;
						}
					}

					if(++polls > maxPolls)
					{
						feLog("reached max polls %d\n",maxPolls);
						break;
					}

//					std::this_thread::sleep_for(
//							std::chrono::milliseconds(100));
				}

				spStateCatalog->stop();

				const I32 signalCount=spMyHandler->signalCount();

				//* NOTE additional client may cause extra polls

				feLog("firstFrame %d\n",firstFrame);
				feLog("final %d polls %d unique %d signals %d"
						" callbacks %d rgs %d changeCount %d\n",
						frame,polls,unique,signalCount,
						callbackCount,recordMatches,changeCount);

				UNIT_TEST(!running);
				UNIT_TEST(frame==999);
				UNIT_TEST(firstFrame+unique>700 && firstFrame+unique<=1000);
				UNIT_TEST(firstFrame+signalCount>990 && signalCount<=1000);
				UNIT_TEST(firstFrame+callbackCount>990 && callbackCount<=1000);
				UNIT_TEST(firstFrame+recordMatches>900 && recordMatches<=1001);
				UNIT_TEST(changeCount==2);

#if	TEST_CLIENT_RECORDS
				UNIT_TEST(firstFrame+polls>700 && firstFrame+polls<=1030);
#else
				UNIT_TEST(firstFrame+polls>700 && firstFrame+polls<=1200);
#endif

				UNIT_TEST(!spStateCatalog->cataloged("disposable0"));
				UNIT_TEST(spStateCatalog->cataloged("disposable0","id"));
				UNIT_TEST(spStateCatalog->catalog<String>(
						"disposable0","id")=="0");

				UNIT_TEST(spStateCatalog->cataloged("disposable1"));
				UNIT_TEST(spStateCatalog->catalog<I32>("disposable1")==13);
				UNIT_TEST(spStateCatalog->cataloged("disposable1","id"));
				UNIT_TEST(spStateCatalog->catalog<String>(
						"disposable1","id")=="1");

				UNIT_TEST(!spStateCatalog->cataloged("disposable2"));
				UNIT_TEST(!spStateCatalog->cataloged("disposable2","id"));

				for(I32 index=0;index<sampleIndex;index++)
				{
					if(index && timeFrame[index]!=timeFrame[index-1]+1)
					{
						feLog("--missing frames--\n");
					}
					feLog("sample %d frame %d %dms\n",
							index,timeFrame[index],timeSample[index]);
				}

				delete[] timeSample;
				delete[] timeFrame;

				feLog("End State:\n");
				spStateCatalog->catalogDump();
			}
		}

		complete=TRUE;
	}
	catch(Exception &e) { e.log(); }
	catch(...)			{ feLog("uncaught exception\n"); }

	UNIT_TEST(complete);
	UNIT_TEST(!Counted::trackerCount());
	UNIT_TRACK(22);
	UNIT_RETURN();
}
