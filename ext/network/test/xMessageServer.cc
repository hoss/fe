/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "network/network.pmh"
#include "math/math.h"

#include <thread>
#include <chrono>

using namespace fe;
using namespace fe::ext;

int main(int argc,char** argv)
{
	String envCiBuild(FALSE);
	System::getEnvironmentVariable("FE_CI_BUILD", envCiBuild);
	const I32 ciBuild=envCiBuild.integer();
	feLog("FE_CI_BUILD %d\n",ciBuild);

	UNIT_START();

	BWORD complete=FALSE;

	try
	{
		{
			sp<SingleMaster> spSingleMaster=SingleMaster::create();
			sp<Master> spMaster=spSingleMaster->master();
			sp<Registry> spRegistry=spMaster->registry();

			Result result=spRegistry->manage("feAutoLoadDL");
			UNIT_TEST(successful(result));

			String implementation="ConnectedCatalog";
			if(argc>1)
			{
				implementation=argv[1];
			}
			feLog("implementation \"%s\"\n",implementation.c_str());

			String transport="tcp";
			if(argc>2)
			{
				transport=argv[2];
			}
			feLog("transport \"%s\"\n",transport.c_str());

			sp<StateCatalog> spStateCatalog=
					spMaster->registry()->create(implementation);
			UNIT_TEST(spStateCatalog.isValid());
			if(!spStateCatalog.isValid())
			{
				feX(argv[0],"spStateCatalog invalid");
			}

			if(transport=="ipc")
			{
				result=spStateCatalog->configure("/tmp/feeds/0"
						" role=server ioPriority=high transport=ipc");
			}
			else
			{
				result=spStateCatalog->configure("*:7890"
						" role=server ioPriority=high");
			}

			result=spStateCatalog->start();
			UNIT_TEST(successful(result));

			I32 messageCount(0);

			if(successful(result))
			{
				spStateCatalog->waitForConnection();

				const I32 frameCount=1000;

				for(I32 frame=0;frame<frameCount;frame++)
				{
					I32 messageIndex=0;
					while(TRUE)
					{
						String message;
						result=spStateCatalog->nextMessage("request",message);
						if(failure(result))
						{
							break;
						}
						feLog("message %d: \"%s\"\n",
								messageIndex++,message.c_str());
						messageCount++;
					}

					std::this_thread::sleep_for(std::chrono::milliseconds(2));
				}

				spStateCatalog->stop();

				feLog("End State:\n");
				spStateCatalog->catalogDump();
			}

			UNIT_TEST(messageCount==2);
		}

		complete=TRUE;
	}
	catch(Exception &e) { e.log(); }
	catch(...)			{ feLog("uncaught exception\n"); }

	UNIT_TEST(complete);
	UNIT_TEST(!Counted::trackerCount());
	UNIT_TRACK(6);
	UNIT_RETURN();
}
