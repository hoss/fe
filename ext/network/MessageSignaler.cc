/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <network/network.pmh>

namespace fe
{
namespace ext
{

void MessageSignaler::initialize(void)
{
	m_spRecordType=registry()->master()->typeMaster()->lookupType<Record>();
}

void MessageSignaler::notify(String a_name,String a_property)
{
#if FALSE
	const String typeName=m_spStateCatalog->catalogTypeName(a_name,a_property);
	feLog("MessageSignaler::notify \"%s\" \"%s\" \"%s\"\n",
			a_name.c_str(),a_property.c_str(),typeName.c_str());
#endif

	//* incoming, serialized over the network
	if(m_spStateCatalog->catalogTypeIs< std::deque<String> >(a_name))
	{
		//* try to pop String from queue
		std::deque<String>& rStringQueue=
					m_spStateCatalog->catalog< std::deque<String> >(a_name);

		//* if no serialized messages from network, check for local signal
		if(!rStringQueue.empty())
		{
			String text=rStringQueue.front();
			rStringQueue.pop_front();

			if(text.empty())
			{
				feLog("MessageSignaler::notify message has no text\n");
				return;
			}

//			feLog("  text:\n%s\n",text.c_str());

			if(m_spRecordType.isNull())
			{
				feLog("MessageSignaler::notify Record Type not known\n");
				return;
			}

			//* convert String to Record
			Record record;
			std::istringstream istrm(text.c_str());
			m_spRecordType->getInfo()->input(istrm,&record,
					BaseType::Info::e_ascii);

//			feLog("  signal interpreted\n");

			//* send Record to Handlers
			signal(record);

			return;
		}
	}

	//* NOTE Record only set during the StateCatalog::sendMessage
	if(m_spStateCatalog->catalogTypeIs<Record>(a_name,a_property))
	{
		Record& rRecord=
				m_spStateCatalog->catalog<Record>(a_name,a_property);

//		feLog("  signal direct %d\n",rRecord.isValid());

		if(rRecord.isValid())
		{
			//* relay transient local signal
			signal(rRecord);
		}
	}
}

} /* namespace ext */
} /* namespace fe */
