/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __network_StateBindI_h__
#define __network_StateBindI_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Specify a StateCatalog and key

	@ingroup network
*//***************************************************************************/
class FE_DL_EXPORT StateBindI:
		virtual public Component,
		public CastableAs<StateBindI>
{
	public:
				/// @brief specify a StateCatalog of interest
virtual void	bind(sp<StateCatalog> a_spStateCatalog)						=0;

				/// @brief target a particular key in the StateCatalog
virtual void	setKey(String a_name)										=0;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __network_StateBindI_h__ */
