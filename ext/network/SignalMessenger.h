/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __network_SignalMessenger_h__
#define __network_SignalMessenger_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Convert signals to network messages

	The SignalMessenger should be bound to a particular key on
	a specific StateCatalog.

	As a HandlerI, each incoming signal will be placed in the StateCatalog
	using its sendMessage method.

	@ingroup network
*//***************************************************************************/
class FE_DL_EXPORT SignalMessenger:
		virtual public StateBindI,
		virtual public HandlerI,
		public CastableAs<SignalMessenger>
{
	public:
				SignalMessenger(void)										{}

				//* as StateBindI
virtual void	bind(sp<StateCatalog> a_spStateCatalog)
				{	m_spStateCatalog=a_spStateCatalog; }
virtual void	setKey(String a_name)
				{	m_key=a_name; }

				//* as HandlerI
virtual void	handle(Record& a_signal);

	private:
		sp<StateCatalog>	m_spStateCatalog;
		String				m_key;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __network_SignalMessenger_h__ */

