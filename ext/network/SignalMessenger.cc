/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <network/network.pmh>

namespace fe
{
namespace ext
{

void SignalMessenger::handle(Record& a_signal)
{
//	feLog("SignalMessenger::handle \"%s\"\n",m_key.c_str());

	m_spStateCatalog->safeLock();
	m_spStateCatalog->catalog<bool>(m_key,"signal")=true;
	m_spStateCatalog->safeUnlock();

	m_spStateCatalog->sendMessage(m_key,a_signal);
}

} /* namespace ext */
} /* namespace fe */
