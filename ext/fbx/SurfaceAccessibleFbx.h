/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __fbx_SurfaceAccessibleFbx_h__
#define __fbx_SurfaceAccessibleFbx_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
    @brief FBX Surface Binding

	@ingroup fbx
*//***************************************************************************/
class FE_DL_EXPORT SurfaceAccessibleFbx:
	public SurfaceAccessibleJoint
{
	public:
				SurfaceAccessibleFbx(void);
virtual			~SurfaceAccessibleFbx(void);

				//* as SurfaceAccessibleI

				using SurfaceAccessibleJoint::load;

virtual	BWORD	load(String a_filename,sp<Catalog> a_spSettings);

	private:

	class FileContext: public Component
	{
		public:
						FileContext(void);
	virtual				~FileContext(void);

			FbxManager*	m_pFbxManager;
			FbxScene*	m_pFbxScene;
	};

		BWORD			setContext(String a_filename);
		BWORD			loadScene(const char* a_pFilename);
		void			readTransformations(FbxNode* a_pFbxNode);
		void			addMesh(FbxMesh* a_pFbxMesh,
							SpatialTransform& a_rTransform);

		String			m_filename;
		Real			m_fps;
		String			m_subset;
		BWORD			m_ytoz;
		BWORD			m_no_uvs;

		sp<FileContext>	m_spFileContext;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __fbx_SurfaceAccessibleFbx_h__ */
