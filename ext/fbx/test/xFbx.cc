/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "surface/surface.h"

using namespace fe;
using namespace fe::ext;

int main(int argc,char** argv)
{
	UNIT_START();

	BWORD complete=FALSE;

	if(argc<2)
	{
		feLog("Usage: %s <fbx file> [options]\n",argv[0]);
		return -1;
	}
	String filename=argv[1];
	String options;
	if(argc>2)
	{
		options=argv[2];
	}

	try
	{
		sp<Master> spMaster(new Master);
		sp<Registry> spRegistry=spMaster->registry();

		Result result=spRegistry->manage("feAutoLoadDL");
		UNIT_TEST(successful(result));

		sp<SurfaceAccessibleI> spSurfaceAccessibleI=
				spRegistry->create("SurfaceAccessibleI.*.*.fbx");
		UNIT_TEST(spSurfaceAccessibleI.isValid());

		sp<Scope> spScope=spRegistry->create("Scope");
		UNIT_TEST(spScope.isValid());

//		const String longname=
//				spRegistry->master()->catalog()->catalog<String>(
//				"path:media")+"/model/"+filename;

		if(spSurfaceAccessibleI.isValid())
		{
			sp<Catalog> spSettings=spMaster->createCatalog("settings");
			spSettings->catalog<String>("options")=options;

			spSurfaceAccessibleI->bind(spScope);
			spSurfaceAccessibleI->load(filename,spSettings);

			sp<SurfaceAccessorI> spName=spSurfaceAccessibleI->accessor(
					SurfaceAccessibleI::e_primitive,"name");
			sp<SurfaceAccessorI> spParent=spSurfaceAccessibleI->accessor(
					SurfaceAccessibleI::e_primitive,"parent");
			sp<SurfaceAccessorI> spAnimX=spSurfaceAccessibleI->accessor(
					SurfaceAccessibleI::e_primitive,"animX");
			sp<SurfaceAccessorI> spAnimY=spSurfaceAccessibleI->accessor(
					SurfaceAccessibleI::e_primitive,"animY");
			sp<SurfaceAccessorI> spAnimZ=spSurfaceAccessibleI->accessor(
					SurfaceAccessibleI::e_primitive,"animZ");
			sp<SurfaceAccessorI> spAnimT=spSurfaceAccessibleI->accessor(
					SurfaceAccessibleI::e_primitive,"animT");

			const I32 primitiveCount=spName->count();
			for(I32 primitiveIndex=0;primitiveIndex<primitiveCount;
					primitiveIndex++)
			{
				const String name=spName->string(primitiveIndex);
				const String parent=spParent->string(primitiveIndex);

				const SpatialVector animX=
						spAnimX->spatialVector(primitiveIndex);
				const SpatialVector animY=
						spAnimY->spatialVector(primitiveIndex);
				const SpatialVector animZ=
						spAnimZ->spatialVector(primitiveIndex);
				const SpatialVector animT=
						spAnimT->spatialVector(primitiveIndex);

continue;

				feLog("prim %d/%d \"%s\" parent \"%s\"\n",
						primitiveIndex,primitiveCount,
						name.c_str(),parent.c_str());
//				feLog("  animX %s\n",c_print(animX));
//				feLog("  animY %s\n",c_print(animY));
//				feLog("  animZ %s\n",c_print(animZ));
//				feLog("  animT %s\n",c_print(animT));
				feLog("  animX %12.6f %12.6f %12.6f\n",
						animX[0],animX[1],animX[2]);
				feLog("  animY %12.6f %12.6f %12.6f\n",
						animY[0],animY[1],animY[2]);
				feLog("  animZ %12.6f %12.6f %12.6f\n",
						animZ[0],animZ[1],animZ[2]);
				feLog("  animT %12.6f %12.6f %12.6f\n",
						animT[0],animT[1],animT[2]);
			}
		}

		complete=TRUE;
	}
	catch(Exception &e) { e.log(); }
	catch(...)			{ feLog("uncaught exception\n"); }

	UNIT_TEST(complete);
	UNIT_TEST(!Counted::trackerCount());
	UNIT_TRACK(5);
	UNIT_RETURN();
}
