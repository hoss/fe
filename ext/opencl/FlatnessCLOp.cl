/*	Copyright (C) 2003-2015 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

__kernel void deform(__global float4* a_position,
	__global int* a_vertStart,__global int* a_vertCount,
	float a_rotateRoot,float a_rotateTip,float a_rotateGamma,
	__global float4* a_debug)
{
	uint primitiveIndex=get_global_id(0);
	int subStart=a_vertStart[primitiveIndex];
	int subCount=a_vertCount[primitiveIndex];

//	if(primitiveIndex<16)
//	{
//		a_debug[primitiveIndex]=(float4)(primitiveIndex,subStart,subCount,0);
//	}

	if(subCount<2)
	{
		return;
	}

	float rotateStart=a_rotateRoot;
	float rotateAdd=a_rotateTip-a_rotateRoot;

	float4 rootPoint=a_position[subStart];
	float4 lastPoint=a_position[subStart+subCount-1];

	float4 tangent=normalize(lastPoint-rootPoint);

	float4 facing=(float4)(0,-1,0,0);

	//* tangentX, facingY (curve normal)

	float16 xformRoot=makeFrameNormalY(rootPoint,tangent,facing);

	float16 invRoot=invertMatrix(xformRoot);

	float16 identity=makeIdentity();

	for(int subIndex=0;subIndex<subCount;subIndex++)
	{
		float along=subIndex/(float)(subCount-1);
		float rotateAlong=rotateStart+rotateAdd*powr(along,a_rotateGamma);

		float16 rotation=rotateMatrix(identity,rotateAlong,2);

		float4 point=a_position[subStart+subIndex];

		float4 relative=transformVector(invRoot,point);

		float4 depressed=transformVector(rotation,relative);

		float4 rotated=transformVector(xformRoot,depressed);

		a_position[subStart+subIndex]=rotated;

		if(primitiveIndex==0 && subIndex==10)
		{
			a_debug[0]=rotation.s0123;
			a_debug[1]=rotation.s4567;
			a_debug[2]=rotation.s89ab;
			a_debug[3]=rotation.scdef;

			a_debug[5]=point;
			a_debug[6]=rotated;
		}
	}
}
