/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <opencl/opencl.pmh>

namespace fe
{
namespace ext
{

void CurlCLOp::initialize(void)
{
	catalog<Real>("amplitude")=1.0;
	catalog<String>("amplitude","page")="Config";

	catalog<Real>("spin")=360.0;
	catalog<String>("spin","page")="Config";

	catalog<String>("curlAttr")="curl";
	catalog<String>("curlAttr","label")="Curl Attribute";
	catalog<String>("curlAttr","page")="Config";

	catalog<String>("arguments")=
			"output:pt:P:rw "
			"output:vertStart "
			"output:vertCount "
			"output:rate:@curlAttr "
			"output:auto:@curlAttr "
			"param:amplitude "
			"param:spin";

	catalog<String>("domain")="primitive";

	catalog<String>("source")=
			#include "matrix_math.cl.c"
			#include "CurlCLOp.cl.c"
			;
}

} /* namespace ext */
} /* namespace fe */
