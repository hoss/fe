/*	Copyright (C) 2003-2015 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

__kernel void deform(__global float4* position,float4 rotateX,
	float4 rotateY,float4 rotateZ,float4 translation)
{
	uint global_addr=get_global_id(0);
	float4 input=position[global_addr];
	float4 output;
	output.x=input.x*rotateX.x+input.y*rotateY.x+
			input.z*rotateZ.x+translation.x;
	output.y=input.x*rotateX.y+input.y*rotateY.y+
			input.z*rotateZ.y+translation.y;
	output.z=input.x*rotateX.z+input.y*rotateY.z+
			input.z*rotateZ.z+translation.z;
	position[global_addr]=output;
}
