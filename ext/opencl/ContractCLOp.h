/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __opencl_ContractCLOp_h__
#define __opencl_ContractCLOp_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
    @brief OpenCL operator to receed points back along on their original curves

	@ingroup opencl
*//***************************************************************************/
class FE_DL_EXPORT ContractCLOp:
	public OpenCLOp,
	public Initialize<ContractCLOp>
{
	public:
				ContractCLOp(void)												{}
virtual			~ContractCLOp(void)												{}

		void	initialize(void);
};

} /* namespace ext */
} /* namespace fe */

#endif /* __opencl_ContractCLOp_h__ */
