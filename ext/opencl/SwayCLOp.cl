/*	Copyright (C) 2003-2015 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

__kernel void deform(__global float4* a_position,__global float4* a_normal,
	__global int* a_vertStart,__global int* a_vertCount,__global int* a_perm,
	float a_time,__global float4* a_debug)
{
	uint primitiveIndex=get_global_id(0);
	int subStart=a_vertStart[primitiveIndex];
	int subCount=a_vertCount[primitiveIndex];
	if(subCount<1)
	{
		return;
	}

	float4 rootPoint=a_position[subStart];
	float4 previousPoint=rootPoint;

	float amplitude=0.5*FastNoise_GetPerlin3D(
			rootPoint.x,rootPoint.y+0.4*a_time,rootPoint.z,a_perm);

	float perlinScalar=sqrt(2.0);

	float4 facing=(float4)(1,0,0,0);

	float4 lastNormal=a_normal[subStart];
	float4 lastPoint=rootPoint;

	for(int subIndex=1;subIndex<subCount;subIndex++)
	{
		int pointIndex=subStart+subIndex;
		float4 input=a_position[pointIndex];

		float along=subIndex/(float)(subCount-1);

		float4 output=input;
		output.x = output.x + amplitude * powr(along,2.0f);

		a_position[pointIndex]=output;

		float4 tangent=normalize(output-lastPoint);
		float4 side=cross(tangent,lastNormal);
		float4 newNormal=normalize(cross(side,tangent));
		lastPoint=output;

		a_normal[pointIndex]=newNormal;
	}
}
