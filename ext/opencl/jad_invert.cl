/*	Copyright (C) 2003-2015 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

inline float16 transpose(float16 m)
{
	float16 transposed;

	transposed.s0123456789abcdef=m.s048c159d26ae37bf;

	return transposed;
}

inline float determinant3x3(
	float a1,float a2,float a3,
	float b1,float b2,float b3,
	float c1,float c2,float c3)
{
	return a1*(b2*c3-b3*c2) - b1*(a2*c3-a3*c2) + c1*(a2*b3-a3*b2);
}

inline float determinant(float16 m)
{
	return
	m.s0*determinant3x3(m.s5,m.s6,m.s7,m.s9,m.sa,m.sb,m.sd,m.se,m.sf)
	-m.s4*determinant3x3(m.s1,m.s2,m.s3,m.s9,m.sa,m.sb,m.sd,m.se,m.sf)
	+m.s8*determinant3x3(m.s1,m.s2,m.s3,m.s5,m.s6,m.s7,m.sd,m.se,m.sf)
	-m.sc*determinant3x3(m.s1,m.s2,m.s3,m.s5,m.s6,m.s7,m.s9,m.sa,m.sb);
}

inline float16 jad_invert(float16 a_frame)
{
	float16 m=transpose(a_frame);
	float det=determinant(m);

	//* If determinant not valid, can't compute inverse (return identity)
	if(fabs(det)<1e-8)
	{
		float16 identity;
		identity.s0=1.0;
		identity.s1=0.0;
		identity.s2=0.0;
		identity.s3=0.0;
		identity.s4=0.0;
		identity.s5=1.0;
		identity.s6=0.0;
		identity.s7=0.0;
		identity.s8=0.0;
		identity.s9=0.0;
		identity.sa=1.0;
		identity.sb=0.0;
		identity.sc=0.0;
		identity.sd=0.0;
		identity.se=0.0;
		identity.sf=1.0;
		return identity;
	}

	float inv=1.0/det;
	float16 inverted;

	inverted.s0=  inv*determinant3x3(
			m.s5,m.s6,m.s7,m.s9,m.sa,m.sb,m.sd,m.se,m.sf);
	inverted.s4= -inv*determinant3x3(
			m.s1,m.s2,m.s3,m.s9,m.sa,m.sb,m.sd,m.se,m.sf);
	inverted.s8=  inv*determinant3x3(
			m.s1,m.s2,m.s3,m.s5,m.s6,m.s7,m.sd,m.se,m.sf);
	inverted.sc= -inv*determinant3x3(
			m.s1,m.s2,m.s3,m.s5,m.s6,m.s7,m.s9,m.sa,m.sb);

	inverted.s1= -inv*determinant3x3(
			m.s4,m.s6,m.s7,m.s8,m.sa,m.sb,m.sc,m.se,m.sf);
	inverted.s5=  inv*determinant3x3(
			m.s0,m.s2,m.s3,m.s8,m.sa,m.sb,m.sc,m.se,m.sf);
	inverted.s9= -inv*determinant3x3(
			m.s0,m.s2,m.s3,m.s4,m.s6,m.s7,m.sc,m.se,m.sf);
	inverted.sd=  inv*determinant3x3(
			m.s0,m.s2,m.s3,m.s4,m.s6,m.s7,m.s8,m.sa,m.sb);

	inverted.s2=  inv*determinant3x3(
			m.s4,m.s5,m.s7,m.s8,m.s9,m.sb,m.sc,m.sd,m.sf);
	inverted.s6= -inv*determinant3x3(
			m.s0,m.s1,m.s3,m.s8,m.s9,m.sb,m.sc,m.sd,m.sf);
	inverted.sa=  inv*determinant3x3(
			m.s0,m.s1,m.s3,m.s4,m.s5,m.s7,m.sc,m.sd,m.sf);
	inverted.se= -inv*determinant3x3(
			m.s0,m.s1,m.s3,m.s4,m.s5,m.s7,m.s8,m.s9,m.sb);

	inverted.s3= -inv*determinant3x3(
			m.s4,m.s5,m.s6,m.s8,m.s9,m.sa,m.sc,m.sd,m.se);
	inverted.s7=  inv*determinant3x3(
			m.s0,m.s1,m.s2,m.s8,m.s9,m.sa,m.sc,m.sd,m.se);
	inverted.sb= -inv*determinant3x3(
			m.s0,m.s1,m.s2,m.s4,m.s5,m.s6,m.sc,m.sd,m.se);
	inverted.sf=  inv*determinant3x3(
			m.s0,m.s1,m.s2,m.s4,m.s5,m.s6,m.s8,m.s9,m.sa);

	return inverted;
}
