/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <opencl/opencl.pmh>

namespace fe
{
namespace ext
{

void FlatnessCLOp::initialize(void)
{
	catalog<Real>("rotateRoot")=0.0;
	catalog<Real>("rotateRoot","high")=3.0;
	catalog<Real>("rotateRoot","max")=1e6;
	catalog<String>("rotateRoot","label")="Rotate Root";
	catalog<String>("rotateRoot","page")="Config";
	catalog<bool>("rotateRoot","joined")=true;
	catalog<String>("rotateRoot","hint")=
			"Peak of positional change at the start of each curve.";

	catalog<Real>("rotateTip")=1.0;
	catalog<Real>("rotateTip","high")=3.0;
	catalog<Real>("rotateTip","max")=1e6;
	catalog<String>("rotateTip","label")="Tip";
	catalog<String>("rotateTip","page")="Config";
	catalog<bool>("rotateTip","joined")=true;
	catalog<String>("rotateTip","hint")=
			"Peak of positional change at the end of each curve.";

	catalog<Real>("rotateGamma")=1.0;
	catalog<Real>("rotateGamma","high")=2.0;
	catalog<Real>("rotateGamma","max")=1e3;
	catalog<String>("rotateGamma","label")="Gamma";
	catalog<String>("rotateGamma","page")="Config";
	catalog<String>("rotateGamma","hint")=
			"Adjust how much influence the Rotate Root has on"
			" the overall rotate distribution along the curve.";

	catalog<String>("arguments")=
			"output:pt:P:rw "
			"output:vertStart "
			"output:vertCount "
			"param:rotateRoot "
			"param:rotateTip "
			"param:rotateGamma";

	catalog<String>("domain")="primitive";

	catalog<String>("source")=
			#include "matrix_math.cl.c"
			#include "FlatnessCLOp.cl.c"
			;
}

} /* namespace ext */
} /* namespace fe */
