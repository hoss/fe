/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <opencl/opencl.pmh>

#include <random>

namespace fe
{
namespace ext
{

void ClumpCLOp::initialize(void)
{
	catalog<Real>("convergenceRoot")=0.0;
	catalog<Real>("convergenceRoot","max")=1.0;
    catalog<String>("convergenceRoot","label")="Convergence Root";
	catalog<String>("convergenceRoot","page")="Config";
    catalog<bool>("convergenceRoot","joined")=true;
    catalog<String>("convergenceRoot","hint")=
            "Peak of positional change at the start of each curve.";

	catalog<Real>("convergenceTip")=1.0;
	catalog<Real>("convergenceTip","max")=1.0;
    catalog<String>("convergenceTip","label")="Tip";
	catalog<String>("convergenceTip","page")="Config";
    catalog<bool>("convergenceTip","joined")=true;
    catalog<String>("convergenceTip","hint")=
            "Peak of positional change at the end of each curve.";

	catalog<Real>("convergenceGamma")=1.0;
	catalog<Real>("convergenceGamma","high")=2.0;
	catalog<Real>("convergenceGamma","max")=1e3;
    catalog<String>("convergenceGamma","label")="Gamma";
	catalog<String>("convergenceGamma","page")="Config";
    catalog<String>("convergenceGamma","hint")=
            "Adjust how much influence the Convergence Root has on"
			" the overall convergence distribution along the curve.";

	catalog< sp<Component> >("Guide Curves");

	catalog< sp<Component> >("Reference Curves");
	catalog<bool>("Reference Curves","optional")=TRUE;

	catalog<String>("arguments")=
			"output:pt:P:rw "
			"output:vertStart "
			"output:vertCount "
			"2:pt:P "
			"2:vertStart "
			"2:vertCount "
			"param:guideIndex "
			"param:convergenceRoot "
			"param:convergenceTip "
			"param:convergenceGamma";

	catalog<String>("domain")="primitive";

	catalog<String>("source")=
			#include "matrix_math.cl.c"
			#include "ClumpCLOp.cl.c"
			;
}

void ClumpCLOp::handle(Record& a_rSignal)
{
//	feLog("ClumpCLOp::handle\n");

	//* TEMP
	catalog<String>("input:1")="Guide Curves";

	//* TODO assign cores

	startup();
	if(!m_started)
	{
		return;
	}

	sp<Counted>& rspCounted=catalog< sp<Counted> >("guideIndex");

	if(!paramContextIsCurrent("guideIndex"))
	{
		rspCounted=NULL;
	}

	if(rspCounted.isNull())
	{
		sp<SurfaceAccessibleI> spGuideAccessible=accessOpenCL("Guide Curves");
		if(spGuideAccessible.isNull()) return;

		sp<SurfaceAccessibleI> spReferenceAccessible=
				accessOpenCL("Reference Curves");
		if(spReferenceAccessible.isNull())
		{
			spReferenceAccessible=
					accessOpenCL("Input Surface");
		}
		if(spReferenceAccessible.isNull()) return;

		sp<SurfaceI> spGuides;
		if(!access(spGuides,spGuideAccessible)) return;

		sp<SurfaceAccessorI> spGuideVertices;
		if(!access(spGuideVertices,spGuideAccessible,
				e_primitive,e_vertices)) return;

		sp<SurfaceAccessorI> spReferenceVertices;
		if(!access(spReferenceVertices,spReferenceAccessible,
				e_primitive,e_vertices)) return;

		const I32 primitiveCount=spReferenceVertices->count();

//		feLog("ClumpCLOp::handle primitiveCount %d guideCount %d\n",
//				primitiveCount,spGuideVertices->count());

		I32* guideArray=new I32[primitiveCount];

		for(I32 primitiveIndex=0;primitiveIndex<primitiveCount;primitiveIndex++)
		{
			const I32 subCount=spReferenceVertices->subCount(primitiveIndex);
			if(subCount<2)
			{
				guideArray[primitiveIndex]= -1;
				continue;
			}

			const SpatialVector rootPoint=
					spReferenceVertices->spatialVector(primitiveIndex,0);

			const Real maxDistance(1.0);	//* TODO
			sp<SurfaceI::ImpactI> spRootImpact=
					spGuides->nearestPoint(rootPoint,maxDistance);
			if(spRootImpact.isNull())
			{
				guideArray[primitiveIndex]= -1;
				continue;
			}

			guideArray[primitiveIndex]=spRootImpact->primitiveIndex();
//			feLog("ClumpCLOp::handle prim %d guide %d\n",
//					primitiveIndex,guideArray[primitiveIndex]);
		}

		sp<SurfaceAccessibleOpenCL::ClMem> spClMem(
				new SurfaceAccessibleOpenCL::ClMem());
		rspCounted=spClMem;

		cl_int clError=0;
		spClMem->m_clMem=clCreateBuffer(m_clContext,
				CL_MEM_READ_ONLY|CL_MEM_COPY_HOST_PTR,
				primitiveCount*sizeof(I32),guideArray,&clError);
		if(clError)
		{
			feLog("ClumpCLOp::handle clCreateBuffer clError %d\n",clError);
		}

		delete[] guideArray;
	}

//	feLog("ClumpCLOp::handle as OpenCLOp\n");

	OpenCLOp::handle(a_rSignal);

//	feLog("ClumpCLOp::handle done\n");
}

} /* namespace ext */
} /* namespace fe */
