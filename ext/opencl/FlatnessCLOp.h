/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __opencl_FlatnessCLOp_h__
#define __opencl_FlatnessCLOp_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
    @brief OpenCL operator to lay curves down towards a direction

	@ingroup opencl
*//***************************************************************************/
class FE_DL_EXPORT FlatnessCLOp:
	public OpenCLOp,
	public Initialize<FlatnessCLOp>
{
	public:
				FlatnessCLOp(void)												{}
virtual			~FlatnessCLOp(void)												{}

		void	initialize(void);
};

} /* namespace ext */
} /* namespace fe */

#endif /* __opencl_FlatnessCLOp_h__ */
