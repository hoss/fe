/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <opencl/opencl.pmh>

#define FE_OCL_DEBUG			FALSE

#define FE_OCL_PROFILE			(FE_CODEGEN==FE_PROFILE)

#define FE_OCL_DEBUG_VECTORS	16
#define FE_OCL_PRINT_VECTORS	FALSE

//* see anteru.net/blog/2012/11/03/2009/index.html
//* see www.drdobbs.com/parallel/a-gentle-introduction-to-opencl/231002854

namespace fe
{
namespace ext
{

OpenCLOp::OpenCLOp(void):
	m_started(FALSE),
	m_deviceId(nullptr),
	m_clContext(nullptr),
	m_clProgram(nullptr),
	m_clCommandQueue(nullptr),
	m_clKernel(nullptr)
{
#if FE_OCL_DEBUG
	feLog("OpenCLOp::OpenCLOp\n");
#endif
}

OpenCLOp::~OpenCLOp(void)
{
#if FE_OCL_DEBUG
	feLog("OpenCLOp::~OpenCLOp\n");
#endif

	if(m_clKernel)
	{
		clReleaseKernel(m_clKernel);
	}

	if(m_clProgram)
	{
		clReleaseProgram(m_clProgram);
	}

//	clReleaseCommandQueue(m_clCommandQueue);
//	clReleaseContext(m_clContext);
}

void OpenCLOp::initialize(void)
{
#if FE_OCL_DEBUG
	feLog("OpenCLOp::initialize\n");
#endif

	catalog<String>("icon")="FE_alpha";

	catalog<String>("domain")="point";
	catalog<String>("domain","label")="Domain";
	catalog<String>("domain","choice:0")="point";
	catalog<String>("domain","label:0")="Points";
	catalog<String>("domain","choice:1")="primitive";
	catalog<String>("domain","label:1")="Primitives";
	catalog<String>("domain","IO")="output";
	catalog<String>("domain","page")="Config";

	catalog<String>("arguments");
	catalog<String>("arguments","label")="Arguments";
	catalog<String>("arguments","IO")="output";
	catalog<String>("arguments","page")="Internal";

	catalog<String>("source");
	catalog<String>("source","label")="Source";
	catalog<String>("source","suggest")="multiline";
	catalog<String>("source","IO")="output";
	catalog<String>("source","page")="Internal";

	catalog< sp<Component> >("Input Surface");

	catalog< sp<Component> >("Output Surface");
	catalog<String>("Output Surface","IO")="output";
	catalog<String>("Output Surface","copy")="Input Surface";
	catalog<bool>("Output Surface","clobber")=true;
}

BWORD OpenCLOp::paramContextIsCurrent(String a_key)
{
	sp<Counted>& rspCounted=catalog< sp<Counted> >(a_key);

	if(rspCounted.isNull())
	{
		return FALSE;
	}

	sp<SurfaceAccessibleOpenCL::ClMem> spClMem=rspCounted;

	cl_context context;

	cl_int clError=clGetMemObjectInfo(spClMem->m_clMem,CL_MEM_CONTEXT,
			sizeof(cl_context),&context,NULL);

	if(clError)
	{
		feLog("OpenCLOp::paramContextIsCurrent \"%s\""
				" clGetMemObjectInfo clError %d (%s)\n",
				a_key.c_str(),clError,clErrorString(clError));
	}

	return (context==m_clContext);
}

BWORD OpenCLOp::startup(void)
{
	sp<Master> spMaster=registry()->master();
	sp<Catalog> spMasterCatalog=spMaster->catalog();
	if(m_started && m_clContext!=
			spMasterCatalog->catalogOrDefault<void*>("clContext",nullptr))
	{
		m_started=FALSE;

#if FE_OCL_DEBUG
		feLog("OpenCLOp::startup \"%s\" restart\n",name().c_str());
#endif
	}

	if(m_started)
	{
		return FALSE;
	}

	if(m_clKernel)
	{
		clReleaseKernel(m_clKernel);
	}

	if(m_clProgram)
	{
		clReleaseProgram(m_clProgram);
	}

#if FE_OCL_PROFILE
	sp<Profiler> spProfiler(new Profiler("OpenCLOp startup"));
	spProfiler->begin();

	sp<Profiler::Profile> spProfileContext(
			new Profiler::Profile(spProfiler,"Get Context"));
	spProfileContext->start();
#endif

	const BWORD allowGLX=FALSE;
	OpenCLContext::confirm(spMaster,allowGLX);

	m_deviceId=(cl_device_id)
			spMasterCatalog->catalogOrDefault<void*>("clDeviceId",nullptr);
	if(m_deviceId==nullptr)
	{
		feLog("OpenCLOp::handle no deviceId\n");
		return FALSE;
	}

	m_clContext=(cl_context)
			spMasterCatalog->catalogOrDefault<void*>("clContext",nullptr);
	if(m_clContext==nullptr)
	{
		feLog("OpenCLOp::handle no clContext\n");
		return FALSE;
	}

	m_clCommandQueue=(cl_command_queue)
			spMasterCatalog->catalogOrDefault<void*>("clCommandQueue",nullptr);
	if(m_clCommandQueue==nullptr)
	{
		feLog("OpenCLOp::handle no clCommandQueue\n");
		return FALSE;
	}

#if FE_OCL_DEBUG
	feLog("OpenCLOp::startup clContext %p deviceId %p\n",
			m_clContext,m_deviceId);

	cl_ulong max_alloc=0;
	clGetDeviceInfo(m_deviceId,CL_DEVICE_MAX_MEM_ALLOC_SIZE,
			sizeof(cl_ulong),&max_alloc,nullptr);
	const I32 maxMb=max_alloc>>20;
	feLog("CL_DEVICE_MAX_MEM_ALLOC_SIZE %d Mb\n",maxMb);

	size_t max_device_group_size=0;
	clGetDeviceInfo(m_deviceId,CL_DEVICE_MAX_WORK_GROUP_SIZE,
			sizeof(size_t),&max_device_group_size,nullptr);
	feLog("CL_DEVICE_MAX_WORK_GROUP_SIZE %d\n",max_device_group_size);

	cl_uint max_item_dimensions=0;
	clGetDeviceInfo(m_deviceId,CL_DEVICE_MAX_WORK_ITEM_DIMENSIONS,
			sizeof(cl_uint),&max_item_dimensions,nullptr);
	feLog("CL_DEVICE_MAX_WORK_ITEM_DIMENSIONS %d\n",max_item_dimensions);

	size_t max_item_sizes[max_item_dimensions];
	clGetDeviceInfo(m_deviceId,CL_DEVICE_MAX_WORK_ITEM_SIZES,
			sizeof(max_item_sizes),max_item_sizes,nullptr);
	feLog("CL_DEVICE_MAX_WORK_ITEM_SIZES %d %d %d\n",
			max_item_sizes[0],max_item_sizes[1],max_item_sizes[2]);
#endif

#if FE_OCL_PROFILE
	sp<Profiler::Profile> spProfileBuild(
			new Profiler::Profile(spProfiler,"Build Program"));
	spProfileBuild->replace(spProfileContext);
#endif

	const char* source=catalog<String>("source").c_str();
	const size_t sourceLength=strlen(source);
	if(!sourceLength)
	{
		return FALSE;
	}

//	feLog("SOURCE:\n%s\n",source);

	cl_int clError=0;

	m_clProgram=clCreateProgramWithSource(m_clContext,
			1,&source,&sourceLength,&clError);
#if FE_OCL_DEBUG
	feLog("clCreateProgramWithSource clError %d (%s)\n",
			clError,clErrorString(clError));
#endif

	clError=clBuildProgram(m_clProgram,0,nullptr,nullptr,nullptr,nullptr);
#if FE_OCL_DEBUG
	feLog("clBuildProgram clError %d (%s)\n",
			clError,clErrorString(clError));
#endif

	if(clError<0)
	{
		size_t log_size=0;
		clGetProgramBuildInfo(m_clProgram,m_deviceId,CL_PROGRAM_BUILD_LOG,
				0,nullptr,&log_size);

		char* program_log=new char[log_size+1];
		clGetProgramBuildInfo(m_clProgram,m_deviceId,CL_PROGRAM_BUILD_LOG,
				log_size+1,program_log,nullptr);

		feLog("clGetProgramBuildInfo CL_PROGRAM_BUILD_LOG:\n%s\n",program_log);

		delete[] program_log;
	}

#if FE_OCL_PROFILE
	sp<Profiler::Profile> spProfileKernel(
			new Profiler::Profile(spProfiler,"Create Kernel"));
	spProfileKernel->replace(spProfileBuild);
#endif

	m_clKernel=clCreateKernel(m_clProgram,"deform",&clError);
#if FE_OCL_DEBUG
	feLog("clCreateKernel clError %d (%s)\n",clError,clErrorString(clError));
#endif

//~	m_clCommandQueue=clCreateCommandQueue(m_clContext,
//~			m_deviceId,0,&clError);
//~#if FE_OCL_DEBUG
//~	feLog("clCreateCommandQueue %p clError %d\n",m_clCommandQueue,clError);
//~#endif
//~
//~	spMasterCatalog->catalog<void*>("clCommandQueue")=m_clCommandQueue;

#if FE_OCL_PROFILE
	spProfileKernel->finish();
	spProfiler->end();

	feLog("%s:\n%s\n",spProfiler->name().c_str(),
			spProfiler->report().c_str());
#endif

	m_started=TRUE;
	return TRUE;
}

void OpenCLOp::handle(Record& a_rSignal)
{
	sp<SurfaceAccessibleI> spOutputAccessible;
	if(!accessOutput(spOutputAccessible,a_rSignal)) return;

	I32 pointCount=
			spOutputAccessible->count(SurfaceAccessibleI::e_point);
	I32 primitiveCount=
			spOutputAccessible->count(SurfaceAccessibleI::e_primitive);

#if FE_OCL_DEBUG
	feLog("OpenCLOp::handle \"%s\" output %p pointCount %d primitiveCount %d\n",
			name().c_str(),
			sp<SurfaceAccessibleOpenCL>(spOutputAccessible).raw(),
			pointCount,primitiveCount);
#endif

	startup();
	if(!m_started)
	{
		return;
	}

#if FE_OCL_PROFILE
	sp<Profiler> spProfiler(new Profiler("OpenCLOp handle"));
	spProfiler->begin();

	sp<Profiler::Profile> spProfileConvert(
			new Profiler::Profile(spProfiler,"Convert Input"));
	spProfileConvert->start();
#endif

	cl_mem pointMem;
	float* floatArray=nullptr;
	BWORD useTemporaryBuffer=FALSE;

	sp<SurfaceAccessibleOpenCL> spOutputAccessibleOpenCL=
			accessOpenCL("Output Surface");

	if(spOutputAccessibleOpenCL.isValid())
	{
		pointCount=spOutputAccessibleOpenCL->count(
				SurfaceAccessibleI::e_point);
		primitiveCount=spOutputAccessibleOpenCL->count(
				SurfaceAccessibleI::e_primitive);

#if FE_OCL_DEBUG
		feLog("  pointCount %d primitiveCount %d\n",
				pointCount,primitiveCount);
#endif
	}

	if(spOutputAccessibleOpenCL.isValid())
	{
		cp<SurfaceAccessibleOpenCL::Bridge>& rcpPositionBridge=
				spOutputAccessibleOpenCL->lookupBridge("pt:P",TRUE);
		if(rcpPositionBridge.isValid())
		{
			pointMem=rcpPositionBridge.writable()->getClMem()->m_clMem;
		}
	}

	sp<SurfaceAccessorI> spPointPosition;
	if(!pointMem)
	{
		if(!access(spPointPosition,spOutputAccessible,
				e_point,e_position)) return;

		floatArray=new float[pointCount*4];
		useTemporaryBuffer=TRUE;

		for(I32 pointIndex=0;pointIndex<pointCount;pointIndex++)
		{
			const SpatialVector point=
					spPointPosition->spatialVector(pointIndex);

			float* pointData=&floatArray[pointIndex*4];
			pointData[0]=point[0];
			pointData[1]=point[1];
			pointData[2]=point[2];
			pointData[3]=float(0);
		}
	}

	cl_int bufferSize=pointCount*4*sizeof(float);
#if FE_OCL_DEBUG
	feLog("bufferSize %d\n",bufferSize);
#endif

#if FE_OCL_PROFILE
	sp<Profiler::Profile> spProfileBuffer(
			new Profiler::Profile(spProfiler,"Create Buffer"));
	spProfileBuffer->replace(spProfileConvert);
#endif

	cl_int clError=0;

	if(!pointMem)
	{
		pointMem=clCreateBuffer(m_clContext,
				CL_MEM_READ_WRITE|CL_MEM_COPY_HOST_PTR,
				bufferSize,floatArray,&clError);

#if FE_OCL_DEBUG
		feLog("clCreateBuffer clError %d (%s)\n",
				clError,clErrorString(clError));
#endif
	}

#if FE_OCL_DEBUG
	size_t max_kernel_group_size=0;
	clGetKernelWorkGroupInfo(m_clKernel,m_deviceId,
			CL_KERNEL_WORK_GROUP_SIZE,
			sizeof(size_t),&max_kernel_group_size,nullptr);
	feLog("CL_KERNEL_WORK_GROUP_SIZE %d\n",max_kernel_group_size);
#endif

	I32 argIndex=0;

	String arguments=catalog<String>("arguments");
#if FE_OCL_DEBUG
	feLog("arguments: \"%s\"\n",arguments.c_str());
#endif

	String arg;
	while(!(arg=arguments.parse()).empty())
	{
		const String place=arg.parse("\"",":");
		if(place=="param")
		{
			const String key=arg.parse("\"",":");

			const fe_type_info& typeInfo=catalogTypeInfo(key);

#if FE_OCL_DEBUG
			feLog("  PARAM %d \"%s\" typeid \"%s\"\n",
					argIndex,key.c_str(),typeInfo.name());
#endif

			if(typeInfo==getTypeId<String>())
			{
				//* TODO does OpenCL even have strings?
			}
			else if(typeInfo==getTypeId<I32>())
			{
				const I32 integer=catalog<I32>(key);
				clError=clSetKernelArg(m_clKernel,argIndex++,
						sizeof(integer),&integer);
			}
			else if(typeInfo==getTypeId<Real>())
			{
				const Real real=catalog<Real>(key);
				clError=clSetKernelArg(m_clKernel,argIndex++,
						sizeof(real),&real);
			}
			else if(typeInfo==getTypeId<SpatialVector>())
			{
				const Vector4& rVector4=catalog<SpatialVector>(key);
				clError=clSetKernelArg(m_clKernel,argIndex++,
						sizeof(rVector4),&rVector4);
			}
			else if(typeInfo==getTypeId< sp<Counted> >())
			{
				sp<SurfaceAccessibleOpenCL::ClMem> spClMem=
						catalog< sp<Counted> >(key);
				if(spClMem.isValid() && spClMem->m_clMem)
				{
					clError=clSetKernelArg(m_clKernel,argIndex++,
							sizeof(cl_mem),&spClMem->m_clMem);
				}
			}
			else
			{
				feLog("OpenCLOp::handle"
						" unrecognized typeid \"%s\" for \"%s\" \n",
						key.c_str(),typeInfo.name());
			}

#if FE_OCL_DEBUG
			feLog("    clSetKernelArg param \"%s\" clError %d (%s)\n",
					key.c_str(),clError,clErrorString(clError));
#endif
		}
		else
		{
			String rate=arg.parse("\"",":");
			String attribute=arg.parse("\"",":");
			String mode=arg.parse("\"",":");

			//* indirect attribute name
			String key=attribute.prechop("@");
			if(attribute!=key)
			{
				attribute=catalog<String>(key);
			}

			if(!rate.empty() && attribute.empty())
			{
				attribute=rate;
				rate="";
			}

			if(mode.empty())
			{
				mode="ro";
			}

			const BWORD readWrite=(mode=="rw");

#if FE_OCL_DEBUG
			feLog("  ATTR %d \"%s\" \"%s\" \"%s\" \"%s\"\n",
					argIndex,place.c_str(),rate.c_str(),
					attribute.c_str(),mode.c_str());
#endif

			sp<SurfaceAccessibleOpenCL> spSurfaceAccessibleOpenCL;

			if(place=="output")
			{
				spSurfaceAccessibleOpenCL=spOutputAccessibleOpenCL;
			}
			else
			{
				const I32 channel=place.integer()-1;

				String lookup;
				lookup.sPrintf("input:%d",channel);

				const String inputKey=
						catalogOrDefault<String>(lookup,"");

#if FE_OCL_DEBUG
				feLog("    inputKey \"%s\" \"%s\"\n",
						lookup.c_str(),inputKey.c_str());
#endif

				if(!inputKey.empty())
				{
					spSurfaceAccessibleOpenCL=accessOpenCL(inputKey);
				}
			}

			if(spSurfaceAccessibleOpenCL.isNull())
			{
				feLog("OpenCLOp::handle no surface for \"%s\"\n",
						attribute.c_str());
				continue;
			}

			String bridgeName;
			SurfaceAccessibleOpenCL::Bridge* pAttrBridge(NULL);

			const BWORD asRate=(rate=="rate");
			if(asRate)
			{
				rate="auto";
			}

			if(rate=="auto")
			{
				for(I32 rateIndex=0;rateIndex<4;rateIndex++)
				{
					switch(rateIndex)
					{
						case 0:
							rate=SurfaceAccessibleBase::elementLayout(
									SurfaceAccessibleI::e_point);
							break;
						case 1:
							rate=SurfaceAccessibleBase::elementLayout(
									SurfaceAccessibleI::e_vertex);
							break;
						case 2:
							rate=SurfaceAccessibleBase::elementLayout(
									SurfaceAccessibleI::e_primitive);
							break;
						case 3:
							rate=SurfaceAccessibleBase::elementLayout(
									SurfaceAccessibleI::e_detail);
							break;
						default:
							;
					}

					bridgeName=rate+":"+attribute;

					cp<SurfaceAccessibleOpenCL::Bridge>& rcpAttrBridge=
							spSurfaceAccessibleOpenCL->lookupBridge(
							bridgeName,FALSE);
					if(rcpAttrBridge.isValid())
					{
						pAttrBridge=rcpAttrBridge.writable();
						break;
					}
				}
				if(!pAttrBridge)
				{
					rate="auto";
					bridgeName="";
				}
			}
			else
			{
				bridgeName=rate+":"+attribute;

				cp<SurfaceAccessibleOpenCL::Bridge>& rcpAttrBridge=
						spSurfaceAccessibleOpenCL->lookupBridge(
						bridgeName,FALSE);
				if(rcpAttrBridge.isValid())
				{
					pAttrBridge=rcpAttrBridge.writable();
				}
			}

			if(asRate)
			{
				I32 rateIndex= -1;
				if(rate==SurfaceAccessibleBase::elementLayout(
						SurfaceAccessibleI::e_point))
				{
					rateIndex=0;
				}
				else if(rate==SurfaceAccessibleBase::elementLayout(
						SurfaceAccessibleI::e_vertex))
				{
					rateIndex=1;
				}
				else if(rate==SurfaceAccessibleBase::elementLayout(
						SurfaceAccessibleI::e_primitive))
				{
					rateIndex=2;
				}
				else if(rate==SurfaceAccessibleBase::elementLayout(
						SurfaceAccessibleI::e_detail))
				{
					rateIndex=3;
				}

				clError=clSetKernelArg(m_clKernel,argIndex,
						sizeof(rateIndex),&rateIndex);

#if FE_OCL_DEBUG
				feLog("    clSetKernelArg"
						" %d output %p \"%s\" rate %d clError %d (%s)\n",
						argIndex,spOutputAccessible.raw(),bridgeName.c_str(),
						rateIndex,clError,clErrorString(clError));
#endif

				argIndex++;
				continue;
			}

			//* TODO use general version
			if(place=="output" && rate=="pt" && attribute=="P")
			{
				if(pAttrBridge && readWrite)
				{
					pAttrBridge->m_cpuCurrent=FALSE;
				}

				clError=clSetKernelArg(m_clKernel,argIndex,
						sizeof(cl_mem),&pointMem);
#if FE_OCL_DEBUG
				feLog("    clSetKernelArg"
						" %d output %p pointMem %p clError %d (%s)\n",
						argIndex,spOutputAccessible.raw(),pointMem,
						clError,clErrorString(clError));
#endif

				argIndex++;
				continue;
			}

			if(spSurfaceAccessibleOpenCL.isNull())
			{
#if FE_OCL_DEBUG
				feLog("  spSurfaceAccessibleOpenCL is NULL\n");
#endif
				continue;
			}

			if(!pAttrBridge)
			{
#if FE_OCL_DEBUG
				feLog("    pAttrBridge is NULL\n");
#endif
//				continue;
			}

			cl_mem attrMem(nullptr);

			if(pAttrBridge)
			{
				sp<SurfaceAccessibleOpenCL::ClMem> spClMem=
						pAttrBridge->getClMem();

				attrMem=spClMem->m_clMem;
				if(!attrMem)
				{
#if FE_OCL_DEBUG
					feLog("    attrMem is NULL\n");
#endif
//					continue;
				}

				if(readWrite)
				{
					pAttrBridge->m_cpuCurrent=FALSE;
				}
			}

			clError=clSetKernelArg(m_clKernel,argIndex,sizeof(cl_mem),&attrMem);
#if FE_OCL_DEBUG
			feLog("    clSetKernelArg"
					" %d output %p \"%s\" attrMem %p clError %d (%s)\n",
					argIndex,spOutputAccessible.raw(),bridgeName.c_str(),
					attrMem,clError,clErrorString(clError));
#endif
			argIndex++;
		}
	}

#if FE_OCL_DEBUG_VECTORS>0
	const I32 debugSize=FE_OCL_DEBUG_VECTORS*sizeof(Vector4);
	const I32 debugCount=FE_OCL_DEBUG_VECTORS*4;
	float debugArray[debugCount];
	for(I32 m=0;m<debugCount;m++)
	{
		debugArray[m]=float(0);
	}
	cl_mem debugBuffer=clCreateBuffer(m_clContext,
				CL_MEM_READ_WRITE|CL_MEM_COPY_HOST_PTR,
				debugSize,debugArray,&clError);

	clError=clSetKernelArg(m_clKernel,argIndex++,
			sizeof(cl_mem),&debugBuffer);
#endif

#if FE_OCL_PROFILE
	sp<Profiler::Profile> spProfileRun(
			new Profiler::Profile(spProfiler,"Run Kernel"));
	spProfileRun->replace(spProfileBuffer);
#endif

	const String domain=catalog<String>("domain");
	const size_t global_work_size=
			(domain=="point")? pointCount: primitiveCount;

	clError=clEnqueueNDRangeKernel(m_clCommandQueue,m_clKernel,1,nullptr,
			&global_work_size,nullptr,0,nullptr,nullptr);
	if(clError)
	{
		feLog("OpenCLOp::handle \"%s\" program failed:"
				" clEnqueueNDRangeKernel clError %d (%s)\n",
				name().c_str(),clError,clErrorString(clError));
	}

#if FE_OCL_DEBUG_VECTORS>0 && FE_OCL_PRINT_VECTORS
	clError=clEnqueueReadBuffer(m_clCommandQueue,debugBuffer,CL_TRUE,0,
			debugSize,debugArray,0,nullptr,nullptr);

	feLog("DEBUG\n");
	for(I32 row=0;row<FE_OCL_DEBUG_VECTORS;row++)
	{
		const I32 row4=row*4;
		const Vector4 vector4(debugArray[row4],debugArray[row4+1],
				debugArray[row4+2],debugArray[row4+3]);

		feLog("  %d: %s\n",row,c_print(vector4));
	}
#endif

#if FE_OCL_PROFILE
	if(useTemporaryBuffer)
	{
		//* NOTE block until kernel is finished (flush doesn't do it)
		clError=clEnqueueReadBuffer(m_clCommandQueue,pointMem,CL_TRUE,0,
				1,floatArray,0,nullptr,nullptr);
	}

	sp<Profiler::Profile> spProfileRead(
			new Profiler::Profile(spProfiler,"Read Back"));
	spProfileRead->replace(spProfileRun);
#endif

	if(useTemporaryBuffer)
	{
		clError=clEnqueueReadBuffer(m_clCommandQueue,pointMem,CL_TRUE,0,
				bufferSize,floatArray,0,nullptr,nullptr);
#if FE_OCL_DEBUG
		feLog("  clEnqueueReadBuffer clError %d (%s)\n",
				clError,clErrorString(clError));
#endif
	}

#if FE_OCL_PROFILE
	sp<Profiler::Profile> spProfileOutput(
			new Profiler::Profile(spProfiler,"Write Output"));
	spProfileOutput->replace(spProfileRead);
#endif

	if(useTemporaryBuffer)
	{
		for(I32 pointIndex=0;pointIndex<pointCount;pointIndex++)
		{
			float* pointData=&floatArray[pointIndex*4];
			const SpatialVector point(pointData[0],pointData[1],pointData[2]);

			spPointPosition->set(pointIndex,point);
		}
	}

#if FE_OCL_PROFILE
	sp<Profiler::Profile> spProfileCleanup(
			new Profiler::Profile(spProfiler,"Cleanup"));
	spProfileCleanup->replace(spProfileOutput);
#endif

	if(useTemporaryBuffer)
	{
		delete[] floatArray;
	}

#if FE_OCL_PROFILE
	spProfileCleanup->finish();
	spProfiler->end();

	feLog("%s:\n%s\n",spProfiler->name().c_str(),
			spProfiler->report().c_str());
#endif

#if FE_OCL_DEBUG
	feLog("OpenCLOp::handle done\n");
#endif
}

sp<SurfaceAccessibleI> OpenCLOp::accessOpenCL(String a_key)
{
	sp<SurfaceAccessibleI> spAccessible;
	access(spAccessible,a_key,e_quiet);

	sp<SurfaceAccessibleOpenCL> spSurfaceAccessibleOpenCL=spAccessible;

	if(spSurfaceAccessibleOpenCL.isValid())
	{
		return spSurfaceAccessibleOpenCL;
	}

	Array< sp<Component> >& rPayload=
			catalog< Array< sp<Component> > >(a_key,"payload");
	const I32 payloadCount=rPayload.size();

#if FE_OCL_DEBUG
	feLog("    input payload size %d\n",payloadCount);
#endif

	if(payloadCount && rPayload[0].isValid())
	{
		//* TODO more than one?
		sp<Component> spPayLoad=rPayload[0];

#if FE_OCL_DEBUG
		feLog("    input payload %p \"%s\"\n",
				spPayLoad.raw(),
				spPayLoad->name().c_str());
#endif

		spSurfaceAccessibleOpenCL=spPayLoad;

#if FE_OCL_DEBUG
		feLog("    as opencl %p\n",
				spSurfaceAccessibleOpenCL.raw());
#endif
	}

	return spSurfaceAccessibleOpenCL;
}

//* https://stackoverflow.com/questions/24326432/convenient-way-to-show-opencl-error-codes
//* static
const char* OpenCLOp::clErrorString(cl_int a_error)
{
	switch(a_error)
	{
		// run-time and JIT compiler errors
		case 0: return "CL_SUCCESS";
		case -1: return "CL_DEVICE_NOT_FOUND";
		case -2: return "CL_DEVICE_NOT_AVAILABLE";
		case -3: return "CL_COMPILER_NOT_AVAILABLE";
		case -4: return "CL_MEM_OBJECT_ALLOCATION_FAILURE";
		case -5: return "CL_OUT_OF_RESOURCES";
		case -6: return "CL_OUT_OF_HOST_MEMORY";
		case -7: return "CL_PROFILING_INFO_NOT_AVAILABLE";
		case -8: return "CL_MEM_COPY_OVERLAP";
		case -9: return "CL_IMAGE_FORMAT_MISMATCH";
		case -10: return "CL_IMAGE_FORMAT_NOT_SUPPORTED";
		case -11: return "CL_BUILD_PROGRAM_FAILURE";
		case -12: return "CL_MAP_FAILURE";
		case -13: return "CL_MISALIGNED_SUB_BUFFER_OFFSET";
		case -14: return "CL_EXEC_STATUS_ERROR_FOR_EVENTS_IN_WAIT_LIST";
		case -15: return "CL_COMPILE_PROGRAM_FAILURE";
		case -16: return "CL_LINKER_NOT_AVAILABLE";
		case -17: return "CL_LINK_PROGRAM_FAILURE";
		case -18: return "CL_DEVICE_PARTITION_FAILED";
		case -19: return "CL_KERNEL_ARG_INFO_NOT_AVAILABLE";

		// compile-time errors
		case -30: return "CL_INVALID_VALUE";
		case -31: return "CL_INVALID_DEVICE_TYPE";
		case -32: return "CL_INVALID_PLATFORM";
		case -33: return "CL_INVALID_DEVICE";
		case -34: return "CL_INVALID_CONTEXT";
		case -35: return "CL_INVALID_QUEUE_PROPERTIES";
		case -36: return "CL_INVALID_COMMAND_QUEUE";
		case -37: return "CL_INVALID_HOST_PTR";
		case -38: return "CL_INVALID_MEM_OBJECT";
		case -39: return "CL_INVALID_IMAGE_FORMAT_DESCRIPTOR";
		case -40: return "CL_INVALID_IMAGE_SIZE";
		case -41: return "CL_INVALID_SAMPLER";
		case -42: return "CL_INVALID_BINARY";
		case -43: return "CL_INVALID_BUILD_OPTIONS";
		case -44: return "CL_INVALID_PROGRAM";
		case -45: return "CL_INVALID_PROGRAM_EXECUTABLE";
		case -46: return "CL_INVALID_KERNEL_NAME";
		case -47: return "CL_INVALID_KERNEL_DEFINITION";
		case -48: return "CL_INVALID_KERNEL";
		case -49: return "CL_INVALID_ARG_INDEX";
		case -50: return "CL_INVALID_ARG_VALUE";
		case -51: return "CL_INVALID_ARG_SIZE";
		case -52: return "CL_INVALID_KERNEL_ARGS";
		case -53: return "CL_INVALID_WORK_DIMENSION";
		case -54: return "CL_INVALID_WORK_GROUP_SIZE";
		case -55: return "CL_INVALID_WORK_ITEM_SIZE";
		case -56: return "CL_INVALID_GLOBAL_OFFSET";
		case -57: return "CL_INVALID_EVENT_WAIT_LIST";
		case -58: return "CL_INVALID_EVENT";
		case -59: return "CL_INVALID_OPERATION";
		case -60: return "CL_INVALID_GL_OBJECT";
		case -61: return "CL_INVALID_BUFFER_SIZE";
		case -62: return "CL_INVALID_MIP_LEVEL";
		case -63: return "CL_INVALID_GLOBAL_WORK_SIZE";
		case -64: return "CL_INVALID_PROPERTY";
		case -65: return "CL_INVALID_IMAGE_DESCRIPTOR";
		case -66: return "CL_INVALID_COMPILER_OPTIONS";
		case -67: return "CL_INVALID_LINKER_OPTIONS";
		case -68: return "CL_INVALID_DEVICE_PARTITION_COUNT";

		// extension errors
		case -1000: return "CL_INVALID_GL_SHAREGROUP_REFERENCE_KHR";
		case -1001: return "CL_PLATFORM_NOT_FOUND_KHR";
		case -1002: return "CL_INVALID_D3D10_DEVICE_KHR";
		case -1003: return "CL_INVALID_D3D10_RESOURCE_KHR";
		case -1004: return "CL_D3D10_RESOURCE_ALREADY_ACQUIRED_KHR";
		case -1005: return "CL_D3D10_RESOURCE_NOT_ACQUIRED_KHR";
        case -1006: return "CL_INVALID_D3D11_DEVICE_KHR";
        case -1007: return "CL_INVALID_D3D11_RESOURCE_KHR";
        case -1008: return "CL_D3D11_RESOURCE_ALREADY_ACQUIRED_KHR";
        case -1009: return "CL_D3D11_RESOURCE_NOT_ACQUIRED_KHR";
        case -1010: return "CL_INVALID_DX9_MEDIA_ADAPTER_KHR";
        case -1011: return "CL_INVALID_DX9_MEDIA_SURFACE_KHR";
        case -1012: return "CL_DX9_MEDIA_SURFACE_ALREADY_ACQUIRED_KHR";
        case -1013: return "CL_DX9_MEDIA_SURFACE_NOT_ACQUIRED_KHR";
        case -1057: return "CL_DEVICE_PARTITION_FAILED_EXT";
        case -1058: return "CL_INVALID_PARTITION_COUNT_EXT";
        case -1059: return "CL_INVALID_PARTITION_NAME_EXT";
        case -1093: return "CL_INVALID_EGL_OBJECT_KHR";
        case -1092: return "CL_EGL_RESOURCE_NOT_ACQUIRED_KHR";
        case -1094: return "CL_INVALID_ACCELERATOR_INTEL";
        case -1095: return "CL_INVALID_ACCELERATOR_TYPE_INTEL";
        case -1096: return "CL_INVALID_ACCELERATOR_DESCRIPTOR_INTEL";
        case -1097: return "CL_ACCELERATOR_TYPE_NOT_SUPPORTED_INTEL";
        case -1098: return "CL_INVALID_VA_API_MEDIA_ADAPTER_INTEL";
        case -1099: return "CL_INVALID_VA_API_MEDIA_SURFACE_INTEL";
        case -1100: return "CL_VA_API_MEDIA_SURFACE_ALREADY_ACQUIRED_INTEL";
        case -1101: return "CL_VA_API_MEDIA_SURFACE_NOT_ACQUIRED_INTEL";
		default: return "Unknown OpenCL error";
    }
}

} /* namespace ext */
} /* namespace fe */
