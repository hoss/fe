/*	Copyright (C) 2003-2015 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

// based on FastNoise by Jordan Peck
// see ext/geometry/FastNoise/ for original code (including the MIT license)

inline int FastNoise_FastFloor(float f)
{ return (f>=0)? (int)(f): (int)(f-1); }

inline float FastNoise_Lerp(float a,float b,float t)
{ return a+t*(b-a); }

inline float FastNoise_InterpQuinticFunc(float t)
{ return t*t*t*(t*(t*6-15)+10); }

inline uchar FastNoise_Index2D_12(uchar offset,int x,int y,__global int* perm)
{
	return perm[(x&0xff)+perm[(y&0xff)+offset]+512];
}

inline uchar FastNoise_Index3D_12(uchar offset,int x,int y,int z,
	__global int* perm)
{
	return perm[(x&0xff)+perm[(y&0xff)+perm[(z&0xff)+offset]]+512];
}

__constant float GRAD_X[12]={ 1,-1, 1,-1,  1,-1, 1,-1,  0, 0, 0, 0 };
__constant float GRAD_Y[12]={ 1, 1,-1,-1,  0, 0, 0, 0,  1,-1, 1,-1 };
__constant float GRAD_Z[12]={ 0, 0, 0, 0,  1, 1,-1,-1,  1, 1,-1,-1 };

inline float FastNoise_GradCoord2D(uchar offset,int x,int y,
	float xd,float yd,__global int* perm)
{
	uchar lutPos=FastNoise_Index2D_12(offset,x,y,perm);

	return xd*GRAD_X[lutPos]+yd*GRAD_Y[lutPos];
}

inline float FastNoise_GradCoord3D(uchar offset,int x,int y,int z,
	float xd,float yd,float zd,__global int* perm)
{
	uchar lutPos=FastNoise_Index3D_12(offset,x,y,z,perm);

	return xd*GRAD_X[lutPos]+yd*GRAD_Y[lutPos]+zd*GRAD_Z[lutPos];
}

inline float FastNoise_SinglePerlin2D(uchar offset,float x,float y,
	__global int* perm)
{
	int x0=FastNoise_FastFloor(x);
	int y0=FastNoise_FastFloor(y);
	int x1=x0+1;
	int y1=y0+1;

	float xs,ys;

	xs=FastNoise_InterpQuinticFunc(x-(float)x0);
	ys=FastNoise_InterpQuinticFunc(y-(float)y0);

	float xd0=x-(float)x0;
	float yd0=y-(float)y0;
	float xd1=xd0-1;
	float yd1=yd0-1;

	float xf0=FastNoise_Lerp(
			FastNoise_GradCoord2D(offset,x0,y0,xd0,yd0,perm),
			FastNoise_GradCoord2D(offset,x1,y0,xd1,yd0,perm),xs);
	float xf1=FastNoise_Lerp(
			FastNoise_GradCoord2D(offset,x0,y1,xd0,yd1,perm),
			FastNoise_GradCoord2D(offset,x1,y1,xd1,yd1,perm),xs);

	return FastNoise_Lerp(xf0,xf1,ys);
}

inline float FastNoise_SinglePerlin3D(uchar offset,float x,float y,float z,
	__global int* perm)
{
	int x0=FastNoise_FastFloor(x);
	int y0=FastNoise_FastFloor(y);
	int z0=FastNoise_FastFloor(z);
	int x1=x0+1;
	int y1=y0+1;
	int z1=z0+1;

	float xs=FastNoise_InterpQuinticFunc(x-(float)x0);
	float ys=FastNoise_InterpQuinticFunc(y-(float)y0);
	float zs=FastNoise_InterpQuinticFunc(z-(float)z0);

	float xd0=x-(float)x0;
	float yd0=y-(float)y0;
	float zd0=z-(float)z0;
	float xd1=xd0-1;
	float yd1=yd0-1;
	float zd1=zd0-1;

	float xf00=FastNoise_Lerp(
			FastNoise_GradCoord3D(offset,x0,y0,z0,xd0,yd0,zd0,perm),
			FastNoise_GradCoord3D(offset,x1,y0,z0,xd1,yd0,zd0,perm),xs);
	float xf10=FastNoise_Lerp(
			FastNoise_GradCoord3D(offset,x0,y1,z0,xd0,yd1,zd0,perm),
			FastNoise_GradCoord3D(offset,x1,y1,z0,xd1,yd1,zd0,perm),xs);
	float xf01=FastNoise_Lerp(
			FastNoise_GradCoord3D(offset,x0,y0,z1,xd0,yd0,zd1,perm),
			FastNoise_GradCoord3D(offset,x1,y0,z1,xd1,yd0,zd1,perm),xs);
	float xf11=FastNoise_Lerp(
			FastNoise_GradCoord3D(offset,x0,y1,z1,xd0,yd1,zd1,perm),
			FastNoise_GradCoord3D(offset,x1,y1,z1,xd1,yd1,zd1,perm),xs);

	float yf0=FastNoise_Lerp(xf00,xf10,ys);
	float yf1=FastNoise_Lerp(xf01,xf11,ys);

	return FastNoise_Lerp(yf0,yf1,zs);
}

inline float FastNoise_GetPerlin2D(float x,float y,__global int* perm)
{
	return FastNoise_SinglePerlin2D(0,x*0.666,y*0.666,perm);
}

inline float FastNoise_GetPerlin3D(float x,float y,float z,__global int* perm)
{
	return FastNoise_SinglePerlin3D(0,x*0.666,y*0.666,z*0.666,perm);
}
