/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <opencl/opencl.pmh>

#include <random>

#define FE_PGCL_DEBUG		FALSE

namespace fe
{
namespace ext
{

void PostGuideCLOp::initialize(void)
{
	catalog< sp<Component> >("Guide Curves");

	catalog<String>("arguments")=
			"output:pt:P:rw "
			"output:vertStart "
			"output:vertCount "
			"2:pt:P "
			"2:pt:N "
			"2:vertStart "
			"2:vertCount "
			"2:pt:fromP "
			"2:pt:fromN "
			"param:guideIndices";

	catalog<String>("domain")="primitive";

	catalog<String>("source")=
			#include "matrix_math.cl.c"
			#include "PostGuideCLOp.cl.c"
			;
}

void PostGuideCLOp::handle(Record& a_rSignal)
{
#if FE_PGCL_DEBUG
	feLog("PostGuideCLOp::handle\n");
#endif

	//* TEMP
	catalog<String>("input:1")="Guide Curves";

	//* TODO assign cores

	startup();
	if(!m_started)
	{
		return;
	}

	sp<Counted>& rspCounted=catalog< sp<Counted> >("guideIndices");

	if(!paramContextIsCurrent("guideIndices"))
	{
		rspCounted=NULL;
	}

	if(rspCounted.isNull())
	{
#if FE_PGCL_DEBUG
		feLog("PostGuideCLOp::handle access guides\n");
#endif
		sp<SurfaceAccessibleI> spGuideAccessible=accessOpenCL("Guide Curves");
		if(spGuideAccessible.isNull()) return;

#if FE_PGCL_DEBUG
		feLog("PostGuideCLOp::handle access input\n");
#endif
		sp<SurfaceAccessibleI> spInputAccessible=accessOpenCL("Input Surface");
		if(spInputAccessible.isNull()) return;

#if FE_PGCL_DEBUG
		feLog("PostGuideCLOp::handle access input vertices\n");
#endif
		sp<SurfaceAccessorI> spInputVertices;
		if(!access(spInputVertices,spInputAccessible,
				e_primitive,e_vertices)) return;

#if FE_PGCL_DEBUG
		feLog("PostGuideCLOp::handle access guide surface\n");
#endif
		sp<SurfaceI> spGuides;
		if(!access(spGuides,spGuideAccessible)) return;

#if FE_PGCL_DEBUG
		feLog("PostGuideCLOp::handle access guide vertices\n");
#endif
		sp<SurfaceAccessorI> spGuideVertices;
		if(!access(spGuideVertices,spGuideAccessible,
				e_primitive,e_vertices)) return;

		const I32 primitiveCount=spInputVertices->count();

#if FE_PGCL_DEBUG
		feLog("PostGuideCLOp::handle primitiveCount %d guideCount %d\n",
				primitiveCount,spGuideVertices->count());
#endif

		Vector4i* guideIndicesArray=new Vector4i[primitiveCount];

		for(I32 primitiveIndex=0;primitiveIndex<primitiveCount;primitiveIndex++)
		{
			set(guideIndicesArray[primitiveIndex],-1,-1,-1,-1);

			const I32 subCount=spInputVertices->subCount(primitiveIndex);
			if(subCount<2)
			{
				continue;
			}

			const SpatialVector rootPoint=
					spInputVertices->spatialVector(primitiveIndex,0);

			const Real maxDistance(8.0);	//* TODO
			sp<SurfaceI::ImpactI> spRootImpact=
					spGuides->nearestPoint(rootPoint,maxDistance);
			if(spRootImpact.isNull())
			{
				continue;
			}

			//* TODO three guides
			guideIndicesArray[primitiveIndex][0]=
					spRootImpact->primitiveIndex();

//			feLog("prim %d guides %s\n",primitiveIndex,
//					c_print(guideIndicesArray[primitiveIndex]));
		}

		sp<SurfaceAccessibleOpenCL::ClMem> spClMem(
				new SurfaceAccessibleOpenCL::ClMem());
		rspCounted=spClMem;

		cl_int clError=0;

		spClMem->m_clMem=clCreateBuffer(m_clContext,
				CL_MEM_READ_ONLY|CL_MEM_COPY_HOST_PTR,
				primitiveCount*sizeof(Vector4i),guideIndicesArray,&clError);
		if(clError)
		{
			feLog("PostGuideCLOp::handle clCreateBuffer clError %d\n",clError);
		}

		delete[] guideIndicesArray;
	}

#if FE_PGCL_DEBUG
	feLog("PostGuideCLOp::handle as OpenCLOp\n");
#endif

	OpenCLOp::handle(a_rSignal);

#if FE_PGCL_DEBUG
	feLog("PostGuideCLOp::handle done\n");
#endif
}

} /* namespace ext */
} /* namespace fe */
