/*	Copyright (C) 2003-2015 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

__kernel void deform(__global float4* a_position,
	__global int* a_vertStart,__global int* a_vertCount,
	float a_contraction,__global float4* a_debug)
{
	uint primitiveIndex=get_global_id(0);
	int subStart=a_vertStart[primitiveIndex];
	int subCount=a_vertCount[primitiveIndex];

	if(subCount<2)
	{
		return;
	}

	//* HACK
	const float fakeRandom=(primitiveIndex%101)/100.0;

	const float shrink=a_contraction*fakeRandom;

	float curveLength=0;

	int segmentCount=subCount-1;

//	float segmentLength[segmentCount];
	float segmentLength[128];

//	float4 pointArray[subCount];
	float4 pointArray[128];

	float4 lastPoint=a_position[subStart];
	pointArray[0]=lastPoint;

	for(int subIndex=1;subIndex<subCount;subIndex++)
	{
		float4 point=a_position[subStart+subIndex];

		float len=length(point-lastPoint);

		curveLength+=len;
		lastPoint=point;

		segmentLength[subIndex-1]=len;
		pointArray[subIndex]=point;
	}

	if(curveLength<=0)
	{
		return;
	}

	float reducedLength=(1.0-shrink)*curveLength;

	//* NOTE crude linear interpolation

	int subIndex;
	for(subIndex=1;subIndex<subCount;subIndex++)
	{
		float unitDistance=subIndex/(float)(subCount-1);

		float remainingLength=unitDistance*reducedLength;

		int segmentIndex;
		for(segmentIndex=0;segmentIndex<segmentCount;segmentIndex++)
		{
			float nextLength=segmentLength[segmentIndex];

			if(remainingLength<nextLength)
			{
				break;
			}

			remainingLength-=nextLength;
		}

		float4 contracted;
		if(segmentIndex>segmentCount-1)
		{
			contracted=pointArray[subCount-1];
		}
		else
		{
			float fraction=remainingLength/segmentLength[segmentIndex];
			contracted=(1-fraction)*pointArray[segmentIndex]+
					fraction*pointArray[segmentIndex+1];
		}

		a_position[subStart+subIndex]=contracted;

		int debugIndex=subStart+subIndex;
		if(debugIndex<16)
		{
			a_debug[debugIndex]=(float4)(primitiveIndex,subIndex,
					curveLength,reducedLength);
		}
	}
}
