/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __opencl_SurfaceWrapCLOp_h__
#define __opencl_SurfaceWrapCLOp_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
    @brief OpenCL operator to wrap points to input curves

	@ingroup opencl
*//***************************************************************************/
class FE_DL_EXPORT SurfaceWrapCLOp:
	public OpenCLOp,
	public Initialize<SurfaceWrapCLOp>
{
	public:
				SurfaceWrapCLOp(void)										{}
virtual			~SurfaceWrapCLOp(void)										{}

		void	initialize(void);

				//* As HandlerI
virtual void	handle(Record& a_rSignal);
};

} /* namespace ext */
} /* namespace fe */

#endif /* __opencl_SurfaceWrapCLOp_h__ */
