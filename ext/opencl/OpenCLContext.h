/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __opencl_OpenCLContext_h__
#define __opencl_OpenCLContext_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
    @brief shared FE context for OpenCL usage

	@ingroup opencl

*//***************************************************************************/
class FE_DL_EXPORT OpenCLContext
{
	public:

static	void				confirm(sp<Master> a_spMaster,BWORD a_allowGLX);

	private:

static	RecursiveMutex		ms_mutex;
static	BWORD				ms_usingGLX;
static	cl_device_id		ms_clDeviceId;
static	cl_context			ms_clContext;
static	cl_command_queue	ms_clCommandQueue;
static	cl_command_queue	ms_clCommandQueue2;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __opencl_OpenCLContext_h__ */
