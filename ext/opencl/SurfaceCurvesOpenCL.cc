/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <opencl/opencl.pmh>

namespace fe
{
namespace ext
{

void SurfaceCurvesOpenCL::drawInternal(BWORD a_transformed,
	const SpatialTransform* a_pTransform,sp<DrawI> a_spDrawI,
	const fe::Color* a_pColor,sp<DrawBufferI> a_spDrawBuffer,
	sp<PartitionI> a_spPartition) const
{
//	feLog("SurfaceCurvesOpenCL::drawInternal %p\n",
//			fe_cast<Component>(this));

	SurfaceAccessibleOpenCL::updateDrawBuffer(a_spDrawBuffer,
			registry()->master(),m_spSurfaceAccessibleI,m_vertices);

	SurfaceCurvesAccessible::drawInternal(a_transformed,a_pTransform,
			a_spDrawI,a_pColor,a_spDrawBuffer,a_spPartition);
}

} /* namespace ext */
} /* namespace fe */
