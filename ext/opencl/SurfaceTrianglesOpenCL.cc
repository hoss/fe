/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <opencl/opencl.pmh>

#define	FE_STOC_TICKER				FALSE

namespace fe
{
namespace ext
{

void SurfaceTrianglesOpenCL::prepareForSearch(void)
{
	//* NOTE non-replicated points would have conflicting impact data
	//* from multiple primitives
	if(!m_replicated)
	{
		feLog("SurfaceTrianglesOpenCL::prepareForSearch"
				" non-replicated points not searchable\n");
		return;
	}

	SurfaceTrianglesAccessible::prepareForSearch();
}

void SurfaceTrianglesOpenCL::drawInternal(BWORD a_transformed,
	const SpatialTransform* a_pTransform,sp<DrawI> a_spDrawI,
	const fe::Color* a_pColor,sp<DrawBufferI> a_spDrawBuffer,
	sp<PartitionI> a_spPartition) const
{
//	feLog("SurfaceTrianglesOpenCL::drawInternal vertices %d\n",
//			m_vertices);

	SurfaceAccessibleOpenCL::updateDrawBuffer(a_spDrawBuffer,
			registry()->master(),m_spSurfaceAccessibleI,m_vertices);

	SurfaceTrianglesAccessible::drawInternal(a_transformed,a_pTransform,
			a_spDrawI,a_pColor,a_spDrawBuffer,a_spPartition);
}

void SurfaceTrianglesOpenCL::cache(void)
{
//	feLog("SurfaceTrianglesOpenCL::cache\n");

#if FE_STOC_TICKER
	const U32 tick0=fe::systemTick();
#endif

	//* NOTE minimalized clone of SurfaceTrianglesAccessible::cache()

	sp<SurfaceAccessorI> spVertices=m_spSurfaceAccessibleI->accessor(
			nodeName(),
			SurfaceAccessibleI::e_primitive,SurfaceAccessibleI::e_vertices,
			SurfaceAccessibleI::e_refuseMissing);
	if(spVertices.isNull())
	{
		feLog("SurfaceTrianglesOpenCL::cache no vertices\n");
		return;
	}

	sp<SurfaceAccessorI> spPointPosition=m_spSurfaceAccessibleI->accessor(
			nodeName(),
			SurfaceAccessibleI::e_point,SurfaceAccessibleI::e_position,
			SurfaceAccessibleI::e_refuseMissing);
	if(spPointPosition.isNull())
	{
		feLog("SurfaceTrianglesOpenCL::cache no points\n");
		return;
	}

	sp<SurfaceAccessorI> spPointNormal=m_spSurfaceAccessibleI->accessor(
			nodeName(),
			SurfaceAccessibleI::e_point,SurfaceAccessibleI::e_normal,
			SurfaceAccessibleI::e_refuseMissing);

	sp<SurfaceAccessorI> spVertexNormal;
	sp<SurfaceAccessorI> spPrimitiveNormal;
	if(spPointNormal.isNull())
	{
		spVertexNormal=m_spSurfaceAccessibleI->accessor(
				nodeName(),
				SurfaceAccessibleI::e_vertex,SurfaceAccessibleI::e_normal,
				SurfaceAccessibleI::e_refuseMissing);
		if(spVertexNormal.isNull())
		{
			spPrimitiveNormal=m_spSurfaceAccessibleI->accessor(
					nodeName(),
					SurfaceAccessibleI::e_primitive,
					SurfaceAccessibleI::e_normal,
					SurfaceAccessibleI::e_refuseMissing);
		}
	}

	sp<SurfaceAccessorI> spPointColor=m_spSurfaceAccessibleI->accessor(
			nodeName(),
			SurfaceAccessibleI::e_point,SurfaceAccessibleI::e_color,
			SurfaceAccessibleI::e_refuseMissing);

	sp<SurfaceAccessorI> spVertexColor;
	sp<SurfaceAccessorI> spPrimitiveColor;
	if(spPointColor.isNull())
	{
		spVertexColor=m_spSurfaceAccessibleI->accessor(
				nodeName(),
				SurfaceAccessibleI::e_vertex,SurfaceAccessibleI::e_color,
				SurfaceAccessibleI::e_refuseMissing);
		if(spVertexColor.isNull())
		{
			spPrimitiveColor=m_spSurfaceAccessibleI->accessor(
					nodeName(),
					SurfaceAccessibleI::e_primitive,
					SurfaceAccessibleI::e_color,
					SurfaceAccessibleI::e_refuseMissing);
		}
	}

	sp<SurfaceAccessorI> spPointUV=m_spSurfaceAccessibleI->accessor(
			nodeName(),
			SurfaceAccessibleI::e_point,SurfaceAccessibleI::e_uv,
			SurfaceAccessibleI::e_refuseMissing);

	sp<SurfaceAccessorI> spVertexUV;
	sp<SurfaceAccessorI> spPrimitiveUV;
	if(spPointUV.isNull())
	{
		spVertexUV=m_spSurfaceAccessibleI->accessor(
				nodeName(),
				SurfaceAccessibleI::e_vertex,SurfaceAccessibleI::e_uv,
				SurfaceAccessibleI::e_refuseMissing);
		if(spVertexUV.isNull())
		{
			spPrimitiveUV=m_spSurfaceAccessibleI->accessor(
					nodeName(),
					SurfaceAccessibleI::e_primitive,
					SurfaceAccessibleI::e_uv,
					SurfaceAccessibleI::e_refuseMissing);
		}
	}

	m_replicated=(m_searchable ||
			spVertexNormal.isValid() || spPrimitiveNormal.isValid() ||
			spVertexColor.isValid() || spPrimitiveColor.isValid() ||
			spVertexUV.isValid() || spPrimitiveUV.isValid());

	const BWORD hasColor=(spPointColor.isValid() ||
			spVertexColor.isValid() || spPrimitiveColor.isValid());

	const BWORD hasUV=(spPointUV.isValid() ||
			spVertexUV.isValid() || spPrimitiveUV.isValid());

	const Arrays optionalArrays=Arrays(
			(hasColor? e_arrayColor: e_arrayNull) ^
			(hasUV? e_arrayUV: e_arrayNull));
	setOptionalArrays(optionalArrays);

#if FALSE
	feLog("ptN %d vtN %d prN %d ptC %d vtC %d prC %d"
			" ptU %d vtU %d prU %d rp %d\n",
			spPointNormal.isValid(),spVertexNormal.isValid(),
			spPrimitiveNormal.isValid(),
			spPointColor.isValid(),spVertexColor.isValid(),
			spPrimitiveColor.isValid(),
			spPointUV.isValid(),spVertexUV.isValid(),
			spPrimitiveUV.isValid(),
			m_replicated);
#endif

	const I32 pointCount=spPointPosition->count();
	const U32 primitiveCount=spVertices->count();

	feLog("SurfaceTrianglesOpenCL::cache pointCount %d primitiveCount %d\n",
			pointCount,primitiveCount);

#if FE_STOC_TICKER
	const U32 tick1=fe::systemTick();
#endif

	m_vertices=m_replicated? primitiveCount*6: pointCount;
	m_elements=primitiveCount*2;
	U32 elementAllocated=0;
	U32 vertexAllocated=0;

	//* pre-allocate estimate
	resizeFor(m_elements,m_vertices,elementAllocated,vertexAllocated);

	m_hullPointMap.resize(0);
	m_hullFacePointArray.resize(0);

#if FE_STOC_TICKER
	const U32 tick2=fe::systemTick();

	U32 tick3(0);
#endif

	const SpatialVector yAxis(0.0,1.0,0.0);
	const Color white(1.0,1.0,1.0);

	I32 partitionIndex= -1;

	U32 triangleIndex(0);
	U32 total(0);

	sp<SurfaceAccessibleOpenCL> spSurfaceAccessibleOpenCL(
			m_spSurfaceAccessibleI);
	if(spSurfaceAccessibleOpenCL.isNull())
	{
		return;
	}

	const cp<SurfaceAccessibleOpenCL::Bridge>& rcpVertStartBridge=
			spSurfaceAccessibleOpenCL->lookupBridge(
			":vertStart",FALSE);
	if(rcpVertStartBridge.isNull())
	{
		return;
	}

	const cp<SurfaceAccessibleOpenCL::Bridge>& rcpVertCountBridge=
			spSurfaceAccessibleOpenCL->lookupBridge(
			":vertCount",TRUE);
	if(rcpVertCountBridge.isNull())
	{
		return;
	}

	const cp<SurfaceAccessibleOpenCL::Bridge>& rcpVertPointBridge=
			spSurfaceAccessibleOpenCL->lookupBridge(
			":vertPoint",FALSE);
	if(rcpVertPointBridge.isNull())
	{
		return;
	}

	const cp<SurfaceAccessibleOpenCL::Bridge>& rcpPositionBridge=
			spSurfaceAccessibleOpenCL->lookupBridge(
			"pt:P",FALSE);

	const cp<SurfaceAccessibleOpenCL::Bridge>& rcpNormalBridge=
			spSurfaceAccessibleOpenCL->lookupBridge(
					spPointNormal.isValid()? "pt:N":
					(spVertexNormal.isValid()? "vtx:N": "prim:N"),
					FALSE);

	const cp<SurfaceAccessibleOpenCL::Bridge>& rcpColorBridge=
			spSurfaceAccessibleOpenCL->lookupBridge(
					spPointColor.isValid()? "pt:Cd":
					(spVertexColor.isValid()? "vtx:Cd": "prim:Cd"),
					FALSE);

	const cp<SurfaceAccessibleOpenCL::Bridge>& rcpUVBridge=
			spSurfaceAccessibleOpenCL->lookupBridge(
					spPointUV.isValid()? "pt:uv":
					(spVertexUV.isValid()? "vtx:uv": "prim:uv"),
					FALSE);

	const I32* subStartArray=(I32*)(rcpVertStartBridge->m_bufferData);
	const I32* subCountArray=(I32*)(rcpVertCountBridge->m_bufferData);
	const I32* vertexPointArray=
			(I32*)(rcpVertPointBridge->m_bufferData);
	const Vector4* positionArray=(Vector4*)
			(rcpPositionBridge.isValid()?
			rcpPositionBridge->m_bufferData: NULL);
	const Vector4* normalArray=(Vector4*)
			(rcpNormalBridge.isValid()?
			rcpNormalBridge->m_bufferData: NULL);
	const Vector4* colorArray=(Vector4*)
			(rcpColorBridge.isValid()?
			rcpColorBridge->m_bufferData: NULL);
	const Vector4* uvArray=(Vector4*)
			(rcpUVBridge.isValid()?
			rcpUVBridge->m_bufferData: NULL);

	if(m_replicated)
	{
#if FE_STOC_TICKER
		tick3=fe::systemTick();
#endif

		for(U32 primitiveIndex=0;primitiveIndex<primitiveCount;primitiveIndex++)
		{
//~			const U32 subCount=spVertices->subCount(primitiveIndex);
			const U32 subCount=subCountArray[primitiveIndex];
			const U32 primStart=subStartArray[primitiveIndex];
			const U32 vertOffset=primitiveIndex*FE_OPENCL_BUFFER_MAX_VERTS;

			//* TODO triangulate any subCount
			if(subCount!=3 && subCount!=4)
			{
				continue;
			}

			m_pElementArray[triangleIndex]=Vector3i(total,3,FALSE);

			const U32 addCount=(subCount==4)? 6: 3;

			I32 pointIndex[4];

			for(U32 subIndex=0;subIndex<subCount;subIndex++)
			{
//~				pointIndex[subIndex]=
//~						spVertices->integer(primitiveIndex,subIndex);
				const I32 vertexIndex=primStart+subIndex;
				pointIndex[subIndex]=vertexPointArray[vertexIndex];

				const I32 arrayIndex=total+subIndex;
				m_pVertexArray[arrayIndex]=
//~						spPointPosition->spatialVector(pointIndex[subIndex]);
						positionArray[pointIndex[subIndex]];
				m_pTriangleIndexArray[arrayIndex]=triangleIndex+(subIndex==3);
				m_pPointIndexArray[arrayIndex]=pointIndex[subIndex];
				m_pPrimitiveIndexArray[arrayIndex]=primitiveIndex;
				m_pPartitionIndexArray[arrayIndex]=partitionIndex;
			}
			triangleIndex++;

			if(subCount==4)
			{
				m_pElementArray[triangleIndex]=Vector3i(total+3,3,FALSE);

				I32 arrayIndex=total+4;
				m_pVertexArray[arrayIndex]=m_pVertexArray[total];
				m_pTriangleIndexArray[arrayIndex]=triangleIndex;
				m_pPointIndexArray[arrayIndex]=pointIndex[0];
				m_pPrimitiveIndexArray[arrayIndex]=primitiveIndex;
				m_pPartitionIndexArray[arrayIndex]=partitionIndex;

				arrayIndex=total+5;
				m_pVertexArray[arrayIndex]=m_pVertexArray[total+2];
				m_pTriangleIndexArray[arrayIndex]=triangleIndex;
				m_pPointIndexArray[arrayIndex]=pointIndex[2];
				m_pPrimitiveIndexArray[arrayIndex]=primitiveIndex;
				m_pPartitionIndexArray[arrayIndex]=partitionIndex;

				triangleIndex++;
			}

			if(spPointNormal.isValid())
			{
				for(U32 subIndex=0;subIndex<subCount;subIndex++)
				{
					m_pNormalArray[total+subIndex]=
//~							spPointNormal->spatialVector(
//~							pointIndex[subIndex]);
							normalArray[pointIndex[subIndex]];
				}
				if(subCount==4)
				{
					m_pNormalArray[total+4]=m_pNormalArray[total];
					m_pNormalArray[total+5]=m_pNormalArray[total+2];
				}
			}
			else if(spVertexNormal.isValid())
			{
				for(U32 subIndex=0;subIndex<subCount;subIndex++)
				{
					m_pNormalArray[total+subIndex]=
//~							spVertexNormal->spatialVector(
//~							primitiveIndex,subIndex);
							normalArray[vertOffset+subIndex];
				}
				if(subCount==4)
				{
					m_pNormalArray[total+4]=m_pNormalArray[total];
					m_pNormalArray[total+5]=m_pNormalArray[total+2];
				}
			}
			else if(spPrimitiveNormal.isValid())
			{
				const SpatialVector norm=
//~						spPrimitiveNormal->spatialVector(primitiveIndex);
						normalArray[primitiveIndex];
				for(U32 subIndex=0;subIndex<addCount;subIndex++)
				{
					m_pNormalArray[total+subIndex]=norm;
				}
			}
			else
			{
				for(U32 subIndex=0;subIndex<addCount;subIndex++)
				{
					m_pNormalArray[total+subIndex]=yAxis;
				}
			}

			//* HACK converting to SpatialVector to force alpha to 1
			if(spPointColor.isValid())
			{
				for(U32 subIndex=0;subIndex<subCount;subIndex++)
				{
					m_pColorArray[total+subIndex]=
//~							spPointColor->spatialVector(
//~							pointIndex[subIndex]);
							SpatialVector(colorArray[pointIndex[subIndex]]);
				}
				if(subCount==4)
				{
					m_pColorArray[total+4]=m_pColorArray[total];
					m_pColorArray[total+5]=m_pColorArray[total+2];
				}
			}
			else if(spVertexColor.isValid())
			{
				for(U32 subIndex=0;subIndex<subCount;subIndex++)
				{
					m_pColorArray[total+subIndex]=
//~							spVertexColor->spatialVector(
//~							primitiveIndex,subIndex);
							SpatialVector(colorArray[vertOffset+subIndex]);
				}
				if(subCount==4)
				{
					m_pColorArray[total+4]=m_pColorArray[total];
					m_pColorArray[total+5]=m_pColorArray[total+2];
				}
			}
			else if(spPrimitiveColor.isValid())
			{
				const Color color=
//~						spPrimitiveColor->spatialVector(primitiveIndex);
						SpatialVector(colorArray[primitiveIndex]);
				for(U32 subIndex=0;subIndex<addCount;subIndex++)
				{
					m_pColorArray[total+subIndex]=color;
				}
			}

			if(spPointUV.isValid())
			{
				for(U32 subIndex=0;subIndex<subCount;subIndex++)
				{
					m_pUVArray[total+subIndex]=
//~							spPointUV->spatialVector(
//~							pointIndex[subIndex]);
							uvArray[pointIndex[subIndex]];
				}
				if(subCount==4)
				{
					m_pUVArray[total+4]=m_pUVArray[total];
					m_pUVArray[total+5]=m_pUVArray[total+2];
				}
			}
			else if(spVertexUV.isValid())
			{
				for(U32 subIndex=0;subIndex<subCount;subIndex++)
				{
					m_pUVArray[total+subIndex]=
//~							spVertexUV->spatialVector(
//~							primitiveIndex,subIndex);
							uvArray[vertOffset+subIndex];
				}
				if(subCount==4)
				{
					m_pUVArray[total+4]=m_pUVArray[total];
					m_pUVArray[total+5]=m_pUVArray[total+2];
				}
			}
			else if(spPrimitiveUV.isValid())
			{
				const SpatialVector uv=
//~						spPrimitiveUV->spatialVector(primitiveIndex);
						uvArray[primitiveIndex];
				for(U32 subIndex=0;subIndex<addCount;subIndex++)
				{
					m_pUVArray[total+subIndex]=uv;
				}
			}

			total+=addCount;
		}

		m_vertices=total;
	}
	else
	{
		m_vertexMap.resize(primitiveCount*6);

		for(I32 pointIndex=0;pointIndex<pointCount;pointIndex++)
		{
			m_pVertexArray[pointIndex]=
//~					spPointPosition->spatialVector(pointIndex);
					positionArray[pointIndex];
			m_pTriangleIndexArray[pointIndex]= -1;	//* possibly multiple
			m_pPointIndexArray[pointIndex]=pointIndex;
			m_pPrimitiveIndexArray[pointIndex]= -1;	//* possibly multiple
			m_pPartitionIndexArray[pointIndex]=partitionIndex;
		}

		if(spPointNormal.isValid())
		{
			for(I32 pointIndex=0;pointIndex<pointCount;pointIndex++)
			{
				m_pNormalArray[pointIndex]=
//~						spPointNormal->spatialVector(pointIndex);
						normalArray[pointIndex];
			}
		}
		else
		{
			for(I32 pointIndex=0;pointIndex<pointCount;pointIndex++)
			{
				m_pNormalArray[pointIndex]=yAxis;
			}
		}

		if(spPointColor.isValid())
		{
			for(I32 pointIndex=0;pointIndex<pointCount;pointIndex++)
			{
				m_pColorArray[pointIndex]=
//~						spPointColor->spatialVector(pointIndex);
						colorArray[pointIndex];
			}
		}

		if(spPointUV.isValid())
		{
			for(I32 pointIndex=0;pointIndex<pointCount;pointIndex++)
			{
				m_pUVArray[pointIndex]=
//~						spPointUV->spatialVector(pointIndex);
						uvArray[pointIndex];
			}
		}

#if FE_STOC_TICKER
		tick3=fe::systemTick();
#endif

		for(U32 primitiveIndex=0;primitiveIndex<primitiveCount;primitiveIndex++)
		{
//~			const U32 subCount=spVertices->subCount(primitiveIndex);
			const U32 subCount=subCountArray[primitiveIndex];
			const U32 primStart=subStartArray[primitiveIndex];

			//* TODO triangulate any subCount
			if(subCount!=3 && subCount!=4)
			{
				continue;
			}

			m_pElementArray[triangleIndex]=Vector3i(total,3,FALSE);

			const I32 addCount=(subCount==4)? 6: 3;

			for(U32 subIndex=0;subIndex<subCount;subIndex++)
			{
//~				const I32 pointIndex=
//~						spVertices->integer(primitiveIndex,subIndex);
				const I32 vertexIndex=primStart+subIndex;
				const I32 pointIndex=vertexPointArray[vertexIndex];

				//* NOTE this is the reverse of mapping used for Hydra
				m_vertexMap[total+subIndex]=pointIndex;
			}
			triangleIndex++;

			if(subCount==4)
			{
				m_pElementArray[triangleIndex]=Vector3i(total+3,3,FALSE);

				m_vertexMap[total+4]=m_vertexMap[total];
				m_vertexMap[total+5]=m_vertexMap[total+2];

				triangleIndex++;
			}

			total+=addCount;
		}

		m_vertexMap.resize(total);
	}

	m_elements=triangleIndex;
	resizeFor(m_elements,m_vertices,elementAllocated,vertexAllocated);

#if FE_STOC_TICKER
	const U32 tick4=fe::systemTick();
#endif

	calcBoundingSphere();

#if FE_STOC_TICKER
	const U32 tick5=fe::systemTick();

	const U32 tickDiff01=fe::SystemTicker::tickDifference(tick0,tick1);
	const U32 tickDiff12=fe::SystemTicker::tickDifference(tick1,tick2);
	const U32 tickDiff23=fe::SystemTicker::tickDifference(tick2,tick3);
	const U32 tickDiff34=fe::SystemTicker::tickDifference(tick3,tick4);
	const U32 tickDiff45=fe::SystemTicker::tickDifference(tick4,tick5);
	const U32 tickDiff05=fe::SystemTicker::tickDifference(tick0,tick5);

	const fe::Real msPerTick=1e-3*fe::SystemTicker::microsecondsPerTick();
	const fe::Real ms01=tickDiff01*msPerTick;
	const fe::Real ms12=tickDiff12*msPerTick;
	const fe::Real ms23=tickDiff23*msPerTick;
	const fe::Real ms34=tickDiff34*msPerTick;
	const fe::Real ms45=tickDiff45*msPerTick;
	const fe::Real ms05=tickDiff05*msPerTick;

	feLog("SurfaceTrianglesOpenCL::cache access    %.2f ms\n",ms01);
	feLog("SurfaceTrianglesOpenCL::cache alloc     %.2f ms\n",ms12);
	feLog("SurfaceTrianglesOpenCL::cache arrays    %.2f ms\n",ms23);
	feLog("SurfaceTrianglesOpenCL::cache vertexMap %.2f ms\n",ms34);
	feLog("SurfaceTrianglesOpenCL::cache calc      %.2f ms\n",ms45);
	feLog("SurfaceTrianglesOpenCL::cache TOTAL     %.2f ms\n",ms05);
#endif
}

} /* namespace ext */
} /* namespace fe */
