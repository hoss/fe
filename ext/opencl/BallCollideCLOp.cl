/*	Copyright (C) 2003-2015 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

__kernel void deform(__global float4* a_position,__global float4* a_normal,
	__global int* a_vertStart,__global int* a_vertCount,
	float3 a_center,float a_radius,__global float4* a_displacement,
	__global float4* a_debug)
{
	uint primitiveIndex=get_global_id(0);
	int subStart=a_vertStart[primitiveIndex];
	int subCount=a_vertCount[primitiveIndex];

	if(subCount<2)
	{
		return;
	}

	float4 displace=0.8f*a_displacement[primitiveIndex];
	float bend=length(displace);

	float4 tip=a_position[subStart+subCount-1];

	float4 center4;
	center4.xyz=a_center.xyz;
	center4.w=0.0f;

	float4 away=tip-center4;
	float distance=length(away);
	float intrusion=a_radius-distance;
	if(intrusion>bend)
	{
		float4 direction=away/distance;
		displace=direction*intrusion;
	}
	else if(bend<1e-3f)
	{
		return;
	}

	a_displacement[primitiveIndex]=displace;

	float4 lastNormal=a_normal[subStart];
	float4 lastPoint=a_position[subStart];

	for(int subIndex=1;subIndex<subCount;subIndex++)
	{
		float along=subIndex/(float)(subCount-1);

		int pointIndex=subStart+subIndex;
		float4 input=a_position[pointIndex];

		float4 output = input + displace * powr(along,2.0f);

		a_position[pointIndex]=output;

		float4 tangent=normalize(output-lastPoint);
		float4 side=cross(tangent,lastNormal);
		float4 newNormal=normalize(cross(side,tangent));
		lastPoint=output;

		a_normal[pointIndex]=newNormal;
	}
}
