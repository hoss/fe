/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <opencl/opencl.pmh>

#include <random>

#define FE_KCL_DEBUG			FALSE

namespace fe
{
namespace ext
{

void KinkCLOp::initialize(void)
{
	catalog<Real>("amplitudeRoot")=0.0;
	catalog<Real>("amplitudeRoot","high")=1.0;
	catalog<Real>("amplitudeRoot","max")=1e6;
    catalog<String>("amplitudeRoot","label")="Amplitude Root";
	catalog<String>("amplitudeRoot","page")="Config";
    catalog<bool>("amplitudeRoot","joined")=true;
    catalog<String>("amplitudeRoot","hint")=
            "Peak of positional change at the start of each curve.";

	catalog<Real>("amplitudeTip")=0.1;
	catalog<Real>("amplitudeTip","high")=1.0;
	catalog<Real>("amplitudeTip","max")=1e6;
    catalog<String>("amplitudeTip","label")="Tip";
	catalog<String>("amplitudeTip","page")="Config";
    catalog<bool>("amplitudeTip","joined")=true;
    catalog<String>("amplitudeTip","hint")=
            "Peak of positional change at the end of each curve.";

	catalog<Real>("amplitudeGamma")=1.0;
	catalog<Real>("amplitudeGamma","high")=2.0;
	catalog<Real>("amplitudeGamma","max")=1e3;
    catalog<String>("amplitudeGamma","label")="Gamma";
	catalog<String>("amplitudeGamma","page")="Config";
    catalog<bool>("amplitudeGamma","joined")=true;
    catalog<String>("amplitudeGamma","hint")=
            "Adjust how much influence the Amplitude Root has on"
			" the overall amplitude distribution along the curve.";

	catalog<Real>("amplitudeRandom")=0.0;
	catalog<Real>("amplitudeRandom","high")=1.0;
	catalog<Real>("amplitudeRandom","max")=1e3;
    catalog<String>("amplitudeRandom","label")="Random";
	catalog<String>("amplitudeRandom","page")="Config";
    catalog<String>("amplitudeRandom","hint")=
            "Variation of positional change at the end of each curve.";

	catalog<SpatialVector>("amplitudeScale")=SpatialVector(0,1,1);
    catalog<String>("amplitudeScale","label")="Amplitude Scale";
	catalog<String>("amplitudeScale","page")="Config";
    catalog<String>("amplitudeScale","hint")=
            "Tweak amplitude on axes of curve tangent,"
            " facing direction, and sideways to facing direction.";

	catalog<Real>("frequencyRoot")=100.0;
	catalog<Real>("frequencyRoot","high")=1e4;
	catalog<Real>("frequencyRoot","max")=1e9;
    catalog<String>("frequencyRoot","label")="Frequency Root";
	catalog<String>("frequencyRoot","page")="Config";
    catalog<bool>("frequencyRoot","joined")=true;
    catalog<String>("frequencyRoot","hint")=
            "Volatility of positional change at the start of each curve.";

	catalog<Real>("frequencyTip")=100.0;
	catalog<Real>("frequencyTip","high")=1e4;
	catalog<Real>("frequencyTip","max")=1e9;
    catalog<String>("frequencyTip","label")="Tip";
	catalog<String>("frequencyTip","page")="Config";
    catalog<bool>("frequencyTip","joined")=true;
    catalog<String>("frequencyTip","hint")=
            "Volatility of positional change at the end of each curve.";

	catalog<Real>("frequencyGamma")=1.0;
	catalog<Real>("frequencyGamma","high")=2.0;
	catalog<Real>("frequencyGamma","max")=1e3;
    catalog<String>("frequencyGamma","label")="Gamma";
	catalog<String>("frequencyGamma","page")="Config";
    catalog<String>("frequencyGamma","hint")=
            "Adjust how much influence the Frequency Root has on"
			" the overall frequency distribution along the curve.";

	catalog<SpatialVector>("frequencyScale")=SpatialVector(1,1,1);
    catalog<String>("frequencyScale","label")="Frequency Scale";
	catalog<String>("frequencyScale","page")="Config";
    catalog<String>("frequencyScale","hint")=
            "Tweak frequency on axes of curve tangent,"
            " facing direction, and sideways to facing direction.";

	catalog<String>("domain")="primitive";

	catalog<String>("arguments")=
			"output:pt:P:rw "
			"output:vertStart "
			"output:vertCount "
			"param:perm "
			"param:amplitudeRoot "
			"param:amplitudeTip "
			"param:amplitudeGamma "
			"param:amplitudeRandom "
			"param:amplitudeScale "
			"param:frequencyRoot "
			"param:frequencyTip "
			"param:frequencyGamma "
			"param:frequencyScale";

	catalog<String>("source")=
			#include "matrix_math.cl.c"
			#include "perlin.cl.c"
			#include "KinkCLOp.cl.c"
			;
}

void KinkCLOp::handle(Record& a_rSignal)
{
#if FE_KCL_DEBUG
	feLog("KinkCLOp::handle\n");
#endif

	const BWORD restart=startup();
	if(!m_started)
	{
		return;
	}

	if(restart)
	{
		//* NOTE existing cl_mem may be from an old cl_context
		catalogRemove("perm");
	}

	sp<Counted>& rspCounted=catalog< sp<Counted> >("perm");

	if(!paramContextIsCurrent("perm"))
	{
		rspCounted=NULL;
	}

	if(rspCounted.isNull())
	{
		I32* perm=new I32[1024];

		const I32 seed=1337;

		std::mt19937 gen(seed);

		for(I32 i=0;i<256;i++)
		{
			perm[i]=i;
		}

		for(I32 j=0;j<256;j++)
		{
			std::uniform_int_distribution<> dis(0,256-j);
			I32 k=dis(gen)+j;
			I32 l=perm[j];
			perm[j]=perm[j+256]=perm[k];
			perm[k]=l;
			perm[j+512]=perm[j+768]=perm[j]%12;
		}

		sp<SurfaceAccessibleOpenCL::ClMem> spClMem(
				new SurfaceAccessibleOpenCL::ClMem());
		rspCounted=spClMem;

		cl_int clError=0;
		spClMem->m_clMem=clCreateBuffer(m_clContext,
				CL_MEM_READ_ONLY|CL_MEM_COPY_HOST_PTR,
				1024*sizeof(I32),perm,&clError);
		if(clError)
		{
			feLog("KinkCLOp::handle clCreateBuffer clError %d\n",clError);
		}

		delete[] perm;
	}

#if FE_KCL_DEBUG
	feLog("KinkCLOp::handle as OpenCLOp\n");
#endif

	OpenCLOp::handle(a_rSignal);

#if FE_KCL_DEBUG
	feLog("KinkCLOp::handle done\n");
#endif
}

} /* namespace ext */
} /* namespace fe */
