/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <opencl/opencl.pmh>

#define FE_CCL_DEBUG	FALSE

namespace fe
{
namespace ext
{

CacheCLOp::CacheCLOp(void)
{
#if FE_CCL_DEBUG
	feLog("CacheCLOp::CacheCLOp\n");
#endif
}

CacheCLOp::~CacheCLOp(void)
{
#if FE_CCL_DEBUG
	feLog("CacheCLOp::~CacheCLOp\n");
#endif
}

void CacheCLOp::initialize(void)
{
#if FE_CCL_DEBUG
	feLog("CacheCLOp::initialize\n");
#endif

	catalog<String>("icon")="FE_alpha";

	catalog< sp<Component> >("Input Surface");

	catalog< sp<Component> >("Output Surface");
	catalog<String>("Output Surface","IO")="output";
	catalog<String>("Output Surface","copy")="Input Surface";
	catalog<bool>("Output Surface","temporal")=true;
	catalog<bool>("Output Surface","clobber")=true;

	//* TEMP until we have auto-dirty logic in HoudiniSOP::cookMySop
	catalog<bool>("Output Surface","continuous")=true;
}

void CacheCLOp::handle(Record& a_rSignal)
{
#if FE_CCL_DEBUG
	feLog("CacheCLOp::handle\n");
#endif

	sp<SurfaceAccessibleI> spOutputAccessible;
	if(!accessOutput(spOutputAccessible,a_rSignal)) return;

#if FE_CCL_DEBUG
	feLog("CacheCLOp::handle output %p cache %p\n",
			sp<SurfaceAccessibleOpenCL>(spOutputAccessible).raw(),
			sp<SurfaceAccessibleOpenCL>(m_spSurfaceAccessibleI).raw());

	const I32 pointCount=
			spOutputAccessible->count(SurfaceAccessibleI::e_point);
	const I32 primitiveCount=
			spOutputAccessible->count(SurfaceAccessibleI::e_primitive);
	feLog("  pointCount %d primitiveCount %d\n",
			pointCount,primitiveCount);
#endif

	BWORD hasPayload=FALSE;

	sp<SurfaceAccessibleOpenCL> spOutputAccessibleOpenCL(spOutputAccessible);
	if(spOutputAccessibleOpenCL.isNull())
	{
		//* NOTE see similar in OpenCLOp

		Array< sp<Component> >& rPayload=
				catalog< Array< sp<Component> > >("Output Surface","payload");
		const I32 payloadCount=rPayload.size();

		if(payloadCount)
		{
			//* TODO more than one?
			sp<Component> spPayload=rPayload[0];

			if(spPayload.isNull())
			{
				feLog("CacheCLOp::handle null payload\n");
				return;
			}

#if FE_CCL_DEBUG
			feLog("  payload \"%s\"\n",spPayload->name().c_str());
#endif

			spOutputAccessibleOpenCL=spPayload;
			hasPayload=TRUE;
		}
	}
	if(spOutputAccessibleOpenCL.isNull())
	{
		feLog("CacheCLOp::handle not OpenCL input\n");
		return;
	}

	if(hasPayload)
	{
		//* NOTE output will have different spPayload

		if(m_spSurfaceAccessibleI.isNull())
		{
#if FE_CCL_DEBUG
			feLog("CREATE CACHE\n");
#endif

			m_spSurfaceAccessibleI=sp<Protectable>(
					spOutputAccessibleOpenCL->clone());
		}
		else
		{
#if FE_CCL_DEBUG
			feLog("COPY CACHE\n");
#endif

			spOutputAccessibleOpenCL->clone(
					sp<SurfaceAccessibleOpenCL>(m_spSurfaceAccessibleI).raw());
		}

		Array< sp<Component> >& rPayload=
				catalog< Array< sp<Component> > >("Output Surface","payload");
		rPayload.resize(1);
		rPayload[0]=m_spSurfaceAccessibleI;
	}
	else
	{
		if(m_spSurfaceAccessibleI.isNull())
		{
#if FE_CCL_DEBUG
			feLog("CREATE CACHE\n");
#endif

			m_spSurfaceAccessibleI=sp<Protectable>(
					spOutputAccessibleOpenCL->clone());
		}
		else
		{
#if FE_CCL_DEBUG
			feLog("COPY CACHE\n");
#endif

			sp<SurfaceAccessibleOpenCL> spCacheOpenCL(m_spSurfaceAccessibleI);
			if(spCacheOpenCL.isNull())
			{
				feLog("CacheCLOp::handle not OpenCL cache\n");
				return;
			}

			spCacheOpenCL->clone(spOutputAccessibleOpenCL.raw());
		}
	}

#if FE_CCL_DEBUG
	feLog("CacheCLOp::handle done\n");
#endif
}

} /* namespace ext */
} /* namespace fe */
