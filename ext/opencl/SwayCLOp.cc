/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <opencl/opencl.pmh>

#include <random>

#define FE_SWCL_DEBUG			FALSE

namespace fe
{
namespace ext
{

void SwayCLOp::initialize(void)
{
	catalog<Real>("time")=0.0;
    catalog<String>("time","label")="Time";
	catalog<String>("time","page")="Config";

	catalog<String>("domain")="primitive";

	catalog<String>("arguments")=
			"output:pt:P:rw "
			"output:pt:N:rw "
			"output:vertStart "
			"output:vertCount "
			"param:perm "
			"param:time";

	catalog<String>("source")=
			#include "matrix_math.cl.c"
			#include "perlin.cl.c"
			#include "SwayCLOp.cl.c"
			;
}

void SwayCLOp::handle(Record& a_rSignal)
{
#if FE_SWCL_DEBUG
	feLog("SwayCLOp::handle\n");
#endif

	catalog<Real>("time")=currentFrame(a_rSignal);

	const BWORD restart=startup();
	if(!m_started)
	{
		return;
	}

	if(restart)
	{
		//* NOTE existing cl_mem may be from an old cl_context
		catalogRemove("perm");
	}

	sp<Counted>& rspCounted=catalog< sp<Counted> >("perm");

	if(!paramContextIsCurrent("perm"))
	{
		rspCounted=NULL;
	}

	if(rspCounted.isNull())
	{
		I32* perm=new I32[1024];

		const I32 seed=1337;

		std::mt19937 gen(seed);

		for(I32 i=0;i<256;i++)
		{
			perm[i]=i;
		}

		for(I32 j=0;j<256;j++)
		{
			std::uniform_int_distribution<> dis(0,256-j);
			I32 k=dis(gen)+j;
			I32 l=perm[j];
			perm[j]=perm[j+256]=perm[k];
			perm[k]=l;
			perm[j+512]=perm[j+768]=perm[j]%12;
		}

		sp<SurfaceAccessibleOpenCL::ClMem> spClMem(
				new SurfaceAccessibleOpenCL::ClMem());
		rspCounted=spClMem;

		cl_int clError=0;
		spClMem->m_clMem=clCreateBuffer(m_clContext,
				CL_MEM_READ_ONLY|CL_MEM_COPY_HOST_PTR,
				1024*sizeof(I32),perm,&clError);
		if(clError)
		{
			feLog("SwayCLOp::handle clCreateBuffer clError %d\n",clError);
		}

		delete[] perm;
	}

#if FE_SWCL_DEBUG
	feLog("SwayCLOp::handle as OpenCLOp\n");
#endif

	OpenCLOp::handle(a_rSignal);

#if FE_SWCL_DEBUG
	feLog("SwayCLOp::handle done\n");
#endif
}

} /* namespace ext */
} /* namespace fe */
