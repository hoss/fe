/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __opencl_OpenCLOp_h__
#define __opencl_OpenCLOp_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
    @brief test OpenCL

	@ingroup opencl
*//***************************************************************************/
class FE_DL_EXPORT OpenCLOp:
	public OperatorSurfaceCommon,
	public Initialize<OpenCLOp>
{
	public:
				OpenCLOp(void);
virtual			~OpenCLOp(void);

		void	initialize(void);

				//* As HandlerI
virtual void	handle(Record& a_rSignal);

static
const	char*	clErrorString(cl_int a_error);

	protected:

		BWORD	startup(void);

		BWORD	paramContextIsCurrent(String a_key);

		sp<SurfaceAccessibleI> accessOpenCL(String a_key);

		BWORD				m_started;
		cl_device_id		m_deviceId;
		cl_context			m_clContext;
		cl_program			m_clProgram;
		cl_command_queue	m_clCommandQueue;
		cl_kernel			m_clKernel;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __opencl_OpenCLOp_h__ */
