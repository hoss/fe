/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __opencl_KinkCLOp_h__
#define __opencl_KinkCLOp_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
    @brief OpenCL operator to wander output curves around input curves

	@ingroup opencl
*//***************************************************************************/
class FE_DL_EXPORT KinkCLOp:
	public OpenCLOp,
	public Initialize<KinkCLOp>
{
	public:
				KinkCLOp(void)												{}
virtual			~KinkCLOp(void)												{}

		void	initialize(void);

				//* As HandlerI
virtual void	handle(Record& a_rSignal);
};

} /* namespace ext */
} /* namespace fe */

#endif /* __opencl_KinkCLOp_h__ */
