/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <opencl/opencl.pmh>

namespace fe
{
namespace ext
{

void BallCollideCLOp::initialize(void)
{
	catalog<SpatialVector>("center")=SpatialVector(0.0,0.0,0.0);
	catalog<String>("center","label")="Center";
	catalog<String>("center","page")="Config";
	catalog<String>("center","hint")=
			"Midpoint of the collider.";

	catalog<Real>("radius")=1.0;
	catalog<Real>("radius","high")=10.0;
	catalog<Real>("radius","max")=1e6;
	catalog<String>("radius","label")="Radius";
	catalog<String>("radius","page")="Config";
	catalog<String>("radius","hint")=
			"Collision distance from center.";

	catalog<String>("arguments")=
			"output:pt:P:rw "
			"output:pt:N:rw "
			"output:vertStart "
			"output:vertCount "
			"param:center "
			"param:radius "
			"param:displacement";

	catalog<String>("domain")="primitive";

	catalog<String>("source")=
			#include "matrix_math.cl.c"
			#include "BallCollideCLOp.cl.c"
			;
}

void BallCollideCLOp::handle(Record& a_rSignal)
{
#if FE_PGCL_DEBUG
	feLog("BallCollideCLOp::handle\n");
#endif

	startup();
	if(!m_started)
	{
		return;
	}

	sp<Counted>& rspCounted=catalog< sp<Counted> >("displacement");

	if(!paramContextIsCurrent("displacement"))
	{
		rspCounted=NULL;
	}

	if(rspCounted.isNull())
	{
		sp<SurfaceAccessibleI> spInputAccessible=accessOpenCL("Input Surface");
		if(spInputAccessible.isNull()) return;

		sp<SurfaceAccessorI> spInputVertices;
		if(!access(spInputVertices,spInputAccessible,
				e_primitive,e_vertices)) return;

		const I32 primitiveCount=spInputVertices->count();

		Vector4* displacementArray=new Vector4[primitiveCount];

		for(I32 primitiveIndex=0;primitiveIndex<primitiveCount;primitiveIndex++)
		{
			set(displacementArray[primitiveIndex]);
		}

		sp<SurfaceAccessibleOpenCL::ClMem> spClMem(
				new SurfaceAccessibleOpenCL::ClMem());
		rspCounted=spClMem;

		cl_int clError=0;

		spClMem->m_clMem=clCreateBuffer(m_clContext,
				CL_MEM_READ_ONLY|CL_MEM_COPY_HOST_PTR,
				primitiveCount*sizeof(Vector4),displacementArray,&clError);
		if(clError)
		{
			feLog("BallCollideCLOp::handle clCreateBuffer clError %d\n",clError);
		}

		delete[] displacementArray;
	}

#if FE_PGCL_DEBUG
	feLog("BallCollideCLOp::handle as OpenCLOp\n");
#endif

	OpenCLOp::handle(a_rSignal);

#if FE_PGCL_DEBUG
	feLog("BallCollideCLOp::handle done\n");
#endif
}

} /* namespace ext */
} /* namespace fe */
