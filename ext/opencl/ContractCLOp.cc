/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <opencl/opencl.pmh>

namespace fe
{
namespace ext
{

void ContractCLOp::initialize(void)
{
	catalog<Real>("contraction")=0.0;
	catalog<Real>("contraction","high")=1.0;
	catalog<String>("contraction","label")="Contraction";
	catalog<String>("contraction","page")="Config";
	catalog<String>("contraction","hint")=
			"Peak of curve length reduction.";

	catalog<String>("arguments")=
			"output:pt:P:rw "
			"output:vertStart "
			"output:vertCount "
			"param:contraction";

	catalog<String>("domain")="primitive";

	catalog<String>("source")=
			#include "matrix_math.cl.c"
			#include "ContractCLOp.cl.c"
			;
}

} /* namespace ext */
} /* namespace fe */
