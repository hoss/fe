/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <opencl/opencl.pmh>

namespace fe
{
namespace ext
{

void TransformCLOp::initialize(void)
{
	catalog<SpatialVector>("rotateX")=SpatialVector(1.0,0.0,0.0);
	catalog<String>("rotateX","label")="Rotate X";
	catalog<String>("rotateX","page")="Config";

	catalog<SpatialVector>("rotateY")=SpatialVector(0.0,1.0,0.0);
	catalog<String>("rotateY","label")="Rotate Y";
	catalog<String>("rotateY","page")="Config";

	catalog<SpatialVector>("rotateZ")=SpatialVector(0.0,0.0,1.0);
	catalog<String>("rotateZ","label")="Rotate Z";
	catalog<String>("rotateZ","page")="Config";

	catalog<SpatialVector>("translation")=SpatialVector(0.0,0.0,0.0);
	catalog<String>("translation","label")="Translation";
	catalog<String>("translation","page")="Config";

	catalog<String>("arguments")=
			"output:pt:P:rw "
			"param:rotateX "
			"param:rotateY "
			"param:rotateZ "
			"param:translation";

	catalog<String>("source")=
			#include "TransformCLOp.cl.c"
			;
}

} /* namespace ext */
} /* namespace fe */
