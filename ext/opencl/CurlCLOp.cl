/*	Copyright (C) 2003-2015 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

__kernel void deform(__global float4* a_position,
	__global int* a_vertStart,__global int* a_vertCount,
	int a_rateCurl,__global float* a_curl,
	float a_amplitude,float a_spin,__global float4* a_debug)
{
	uint primitiveIndex=get_global_id(0);
	int subStart=a_vertStart[primitiveIndex];
	int subCount=a_vertCount[primitiveIndex];

	if(primitiveIndex<5)
	{
		a_debug[11+primitiveIndex]=
				(float4)(primitiveIndex,subStart,subCount,0);
	}

	float curlAmplitude=a_amplitude;
	if(a_rateCurl==2)
	{
		curlAmplitude*=a_curl[primitiveIndex];
	}
	else if(a_rateCurl==3)
	{
		curlAmplitude*=a_curl[0];
	}

	for(int subIndex=1;subIndex<subCount;subIndex++)
	{
		int pointIndex=subStart+subIndex;
		float4 input=a_position[pointIndex];

		if(a_rateCurl==0)
		{
			curlAmplitude=a_curl[pointIndex]*a_amplitude;
		}

		float curlSpin=a_spin*subIndex/subCount;

//		float4 prior=a_position[pointIndex-1];
		float4 base=a_position[subStart];
		float4 tangent=normalize(input-base);

		float4 facing=(float4)(1,0,0,0);

		float16 xformSelf=makeFrameNormalY(input,facing,tangent);

		float16 invSelf=invertMatrix(xformSelf);

		float4 relative=transformVector(invSelf,input);

		float angle=(2.0*3.14159)*curlSpin;

		relative.x+=curlAmplitude*sin(angle);
		relative.z+=curlAmplitude*cos(angle);

		float4 output=transformVector(xformSelf,relative);

		if(pointIndex==1)
		{
			a_debug[0]=xformSelf.s0123;
			a_debug[1]=xformSelf.s4567;
			a_debug[2]=xformSelf.s89ab;
			a_debug[3]=xformSelf.scdef;
			a_debug[4]=invSelf.s0123;
			a_debug[5]=invSelf.s4567;
			a_debug[6]=invSelf.s89ab;
			a_debug[7]=invSelf.scdef;
			a_debug[8]=input;
			a_debug[9]=relative;
			a_debug[10]=output;
		}

		a_position[pointIndex]=output;
	}
}
