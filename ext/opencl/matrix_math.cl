/*	Copyright (C) 2003-2015 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

inline float16 makeIdentity()
{
	return (float16)(1,0,0,0, 0,1,0,0, 0,0,1,0, 0,0,0,1);
}

inline float16 makeFrameTangentX(float4 a_location,
	float4 a_tangentX,float4 a_normalY)
{
	float4 sideZ=cross(a_tangentX,a_normalY);
	float4 normalY=cross(sideZ,a_tangentX);

	float16 frame;

	frame.s0123 = a_tangentX;
	frame.s4567 = normalize(normalY);
	frame.s89ab = normalize(sideZ);
	frame.scdef = a_location;

	frame.sf = 1;

	return frame;
}

inline float16 makeFrameNormalY(float4 a_location,
	float4 a_tangentX,float4 a_normalY)
{
	float4 sideZ=cross(a_tangentX,a_normalY);
	float4 tangentX=cross(a_normalY,sideZ);

	float16 frame;

	frame.s0123 = normalize(tangentX);
	frame.s4567 = a_normalY;
	frame.s89ab = normalize(sideZ);
	frame.scdef = a_location;

	frame.sf = 1;

	return frame;
}

inline float4 rotateVector(float16 a_frame,float4 a_in)
{
	float4 out;

	out.xyz=a_frame.s012*a_in.x+
			a_frame.s456*a_in.y+
			a_frame.s89a*a_in.z;
	out.w=0;

	return out;
}

inline float4 transformVector(float16 a_frame,float4 a_in)
{
	float4 out;

	out.xyz=a_frame.s012*a_in.x+
			a_frame.s456*a_in.y+
			a_frame.s89a*a_in.z+
			a_frame.scde;
	out.w=0;

	return out;
}

inline float16 rotateMatrix(float16 a_matrix,float a_radians,int a_axis)
{
	//* NOTE can't use [] on float16

	float buffer[9];
	buffer[0]=a_matrix.s0;
	buffer[1]=a_matrix.s1;
	buffer[2]=a_matrix.s2;
	buffer[3]=a_matrix.s4;
	buffer[4]=a_matrix.s5;
	buffer[5]=a_matrix.s6;
	buffer[6]=a_matrix.s8;
	buffer[7]=a_matrix.s9;
	buffer[8]=a_matrix.sa;
//	buffer[0]=a_matrix.s0;
//	buffer[3]=a_matrix.s1;
//	buffer[6]=a_matrix.s2;
//	buffer[1]=a_matrix.s4;
//	buffer[4]=a_matrix.s5;
//	buffer[7]=a_matrix.s6;
//	buffer[2]=a_matrix.s8;
//	buffer[5]=a_matrix.s9;
//	buffer[8]=a_matrix.sa;

	float sina=sin(a_radians);
	float cosa=cos(a_radians);

	int x1=3*((a_axis+1)%3);
	int x2=3*((a_axis+2)%3);

	float b=buffer[x1];
	float c=buffer[x2];
	buffer[x1]=c*sina+b*cosa;
	buffer[x2]=c*cosa-b*sina;

	x1++;
	x2++;

	b=buffer[x1];
	c=buffer[x2];
	buffer[x1]=c*sina+b*cosa;
	buffer[x2]=c*cosa-b*sina;

	x1++;
	x2++;

	b=buffer[x1];
	c=buffer[x2];
	buffer[x1]=c*sina+b*cosa;
	buffer[x2]=c*cosa-b*sina;

	float16 frame;
	frame.s0123 = (float4)(buffer[0],buffer[1],buffer[2],0);
	frame.s4567 = (float4)(buffer[3],buffer[4],buffer[5],0);
	frame.s89ab = (float4)(buffer[6],buffer[7],buffer[8],0);
//	frame.s0123 = (float4)(buffer[0],buffer[3],buffer[6],0);
//	frame.s4567 = (float4)(buffer[1],buffer[4],buffer[7],0);
//	frame.s89ab = (float4)(buffer[2],buffer[5],buffer[8],0);
	frame.scdef = a_matrix.scdef;

	return frame;
}

inline float16 invertMatrix(float16 a_matrix)
{
	/* Copied from OpenSceneGraph.org (transposed for column major) */

	float r00 = a_matrix.s0;
	float r01 = a_matrix.s4;
	float r02 = a_matrix.s8;
	float r10 = a_matrix.s1;
	float r11 = a_matrix.s5;
	float r12 = a_matrix.s9;
	float r20 = a_matrix.s2;
	float r21 = a_matrix.s6;
	float r22 = a_matrix.sa;

	float16 inverted;

	// Partially compute inverse of rot
	inverted.s0 = r11*r22 - r21*r12;
	inverted.s1 = r20*r12 - r10*r22;
	inverted.s2 = r10*r21 - r20*r11;

	// Compute determinant of rot from 3 elements just computed
	float one_over_det =
			1.0/(r00*inverted.s0 + r01*inverted.s1 + r02*inverted.s2);
	r00 *= one_over_det;
	r01 *= one_over_det;
	r02 *= one_over_det;

	// Finish computing inverse of rot
	inverted.s0 *= one_over_det;
	inverted.s1 *= one_over_det;
	inverted.s2 *= one_over_det;
	inverted.s3 = 0.0;
	inverted.s4 = r21*r02 - r01*r22; // Have already been divided by det
	inverted.s5 = r00*r22 - r20*r02; // same
	inverted.s6 = r20*r01 - r00*r21; // same
	inverted.s7 = 0.0;
	inverted.s8 = r01*r12 - r11*r02; // Have already been divided by det
	inverted.s9 = r10*r02 - r00*r12; // same
	inverted.sa = r00*r11 - r10*r01; // same
	inverted.sb = 0.0;
	inverted.sf = 1.0;

	// We no longer need the rxx or det variables anymore, so we can reuse
	// them for whatever we want.  But we will still rename them for the
	// sake of clarity.

	r22 = a_matrix.sf;

	float s = r22-1.0;
	if(s*s > 1e-6 ) // Involves perspective, so we compute the full inverse
	{
		float16 TPinv;
		inverted.sc = 0.0;
		inverted.sd = 0.0;
		inverted.se = 0.0;

		r01 = a_matrix.s3;
		r11 = a_matrix.s7;
		r21 = a_matrix.sb;
		r00 = inverted.s0*r01 + inverted.s1*r11 + inverted.s2*r21;
		r10 = inverted.s4*r01 + inverted.s5*r11 + inverted.s6*r21;
		r20 = inverted.s8*r01 + inverted.s9*r11 + inverted.sa*r21;

		r01 = a_matrix.sc;
		r11 = a_matrix.sd;
		r21 = a_matrix.se;
		one_over_det = 1.0/(r22 - (r01*r00 + r11*r10 + r21*r20));

		r01 *= one_over_det;
		r11 *= one_over_det;
		r21 *= one_over_det;

		// Compute inverse of trans*corr
		TPinv.s0 = r01*r00 + 1.0;
		TPinv.s1 = r11*r00;
		TPinv.s2 = r21*r00;
		TPinv.s3 = -r00 * one_over_det;
		TPinv.s4 = r01*r10;
		TPinv.s5 = r11*r10 + 1.0;
		TPinv.s6 = r21*r10;
		TPinv.s7 = -r10 * one_over_det;
		TPinv.s8 = r01*r20;
		TPinv.s9 = r11*r20;
		TPinv.sa = r21*r20 + 1.0;
		TPinv.sb = -r20 * one_over_det;
		TPinv.sc = -r01;
		TPinv.sd = -r11;
		TPinv.se = -r21;
		TPinv.sf = one_over_det;

		float16 preMult=inverted;

		//* TODO
//		inverted=multiply(preMult, TPinv);
	}
	else // bottommost row is [0; 0; 0; 1] so it can be ignored
	{
		r01 = a_matrix.sc;
		r11 = a_matrix.sd;
		r21 = a_matrix.se;

		// Compute translation components of mat'
		inverted.sc = -(r01*inverted.s0 + r11*inverted.s4 + r21*inverted.s8);
		inverted.sd = -(r01*inverted.s1 + r11*inverted.s5 + r21*inverted.s9);
		inverted.se = -(r01*inverted.s2 + r11*inverted.s6 + r21*inverted.sa);
	}

	return inverted;
}
