/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __opencl_ClumpCLOp_h__
#define __opencl_ClumpCLOp_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
    @brief OpenCL operator to pull output curves towards core curves

	@ingroup opencl
*//***************************************************************************/
class FE_DL_EXPORT ClumpCLOp:
	public OpenCLOp,
	public Initialize<ClumpCLOp>
{
	public:
				ClumpCLOp(void)											{}
virtual			~ClumpCLOp(void)												{}

		void	initialize(void);

				//* As HandlerI
virtual void	handle(Record& a_rSignal);
};

} /* namespace ext */
} /* namespace fe */

#endif /* __opencl_ClumpCLOp_h__ */
