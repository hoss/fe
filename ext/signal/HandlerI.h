/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __signal_HandlerI_h__
#define __signal_HandlerI_h__

namespace fe
{
namespace ext
{

class SignalerI;

/**************************************************************************//**
	@brief Interface to handle signals from an SignalerI

	@ingroup signal
*//***************************************************************************/
class FE_DL_EXPORT HandlerI:
	virtual public Component,
	public CastableAs<HandlerI>
{
	public:
virtual				~HandlerI(void)											{}

virtual	void		handleBind(sp<SignalerI> spSignalerI,
						sp<Layout> spLayout)								{}
virtual	void		handle(Record &signal)									{}

virtual	void		handleSignal(Record &signal, sp<SignalerI> spSignalerI)
					{	handle(signal); }
};

} /* namespace ext */
} /* namespace fe */

#endif /* __signal_HandlerI_h__ */
