/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __signal_SignalerI_h__
#define __signal_SignalerI_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Selective record broadcaster

	@ingroup signal
*//***************************************************************************/
class FE_DL_EXPORT SignalerI:
	virtual public Component,
	public CastableAs<SignalerI>
{
	public:

virtual	void		signal(Record &signal)									= 0;
virtual	void		insert(sp<HandlerI> spHandlerI, sp<Layout> spLayout,
						IWORD clue = -1)									= 0;
virtual	void		remove(sp<HandlerI> spHandlerI, sp<Layout> spLayout)	= 0;
virtual	bool		contains(sp<HandlerI> spHandlerI, sp<Layout> spLayout)	= 0;
virtual	void		peek(Peeker &peeker);
#ifdef MODAL_SIGNALER
virtual	void		setMode(const String &aMode, bool aIsActive)			= 0;
virtual	bool		getMode(const String &aMode)							= 0;
virtual	void		insert(sp<HandlerI> spHandlerI, sp<Layout> spLayout,
						const String &aMode, IWORD clue = -1)				= 0;
#endif
};

inline void SignalerI::peek(Peeker &peeker)
{
	/* NOOP */
}

} /* namespace ext */
} /* namespace fe */

#endif /* __signal_SignalerI_h__ */

