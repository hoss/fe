/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __signal_ReactorI_h__
#define __signal_ReactorI_h__

namespace fe
{
namespace ext
{

/**	@brief Enable specification of named 'reactions', presumably to anticipated
	conditions

	@ingroup signal

	For example, it may be useful for a GUI selection component to fire
	off an application specific signal when it actually starts and ends a
	selection.  These could be well-known (documented by the selection
	component) named conditions of 'on_select' and 'off_select'.

	The application could then specify records to fire on such conditions:

	@code

	sp<ReactorI> spSelectionComponent;
	Record r_on_select;
	Record r_off_select;

	...
	// create component and records, etc
	...

	spSelectionComponent->add("on_select", spSignaler, r_on_select);
	spSelectionComponent->add("off_select", spSignaler, r_off_select);

	@endcode


	Within the component itself the signals are fired like this:

	@code
	// near code of selection starting
	react("on_select");

	...

	// near code of selection ending
	react("off_select");
	@endcode

	*/
class FE_DL_EXPORT ReactorI:
	virtual public Component,
	public CastableAs<ReactorI>
{
	public:
virtual	void	add(const String &a_name,
						sp<SignalerI> a_signaler, Record r_sig)				= 0;
virtual	void	react(const String &a_name)									= 0;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __signal_ReactorI_h__ */

