/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __signal_LogHandler_h__
#define __signal_LogHandler_h__

namespace fe
{
namespace ext
{

/**	@brief Print signal layout name

	@ingroup signal

	@copydoc LogHandler_info
	*/
class FE_DL_EXPORT LogHandler : public HandlerI
{
	public:
				LogHandler(void);
virtual			~LogHandler(void);

virtual	void	handle(Record &signal);
};

} /* namespace ext */
} /* namespace fe */

#endif /* __signal_LogHandler_h__ */

