/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __signal_PushSequencer_h__
#define __signal_PushSequencer_h__

#define FE_SEQUENCER_ROLLOVER_WINDOW (U32)(1 << 31)


namespace fe
{
namespace ext
{

/**	@brief Mechanism to fire scheduled signals

	@ingroup signal

	Entries for the sequencer are represented by pulse records which have
	these four attributes:

	- time --
		The time at which to fire the signal.

	- interval --
		Interval in which to repeat the signal.

	- count --
		Number of times to repeatedly fire the signal.  Setting this to
		zero will repeat an unlimited number of times.  A non-zero count
		is decremented every time the signal is fired, and when decremented
		form 1 to 0 the signal is removed from the sequencer.
		A negative count will halt repeating without removing the signal,
		which is a way to pause repeating.

	- periodic --
		The mode to repeat signals.
		- 0 -- the repeat signal is set to the sequencer handle time plus the
			interval.
			This means that the true interval between firings in likely longer
			than the interval setting, but such signals will never
			'fall behind'.
		- 1	--  The time for a repeat signal is set to the previous time plus
			the interval.  Note that this could lead to the system getting
			swamped by periodic signals.  However, if the computation load
			varies a lot, this setting could allow repeats to 'catch up'.
		- 2 -- The time for a repeat signal is set to the previous time plus
			the interval unless such time has already passed in which case the
			time is set to the sequencer handle time.  This is like setting 1
			that will
			not fall behind.
		- 3 -- the repeat signal is set to the current time (after the immediate
			handling of the signal is processed) plus the interval.  This
			is a sort of 'forced yield for a time' mode.

	@copydoc PushSequencer_info
	*/
class FE_DL_EXPORT PushSequencer :
	virtual public SequencerI,
	virtual public HandlerI
{
	public:
		PushSequencer(void);
virtual	~PushSequencer(void);
		// AS SequencerI
virtual	U32		getCurrentTime(	void);
virtual	void	setTime(		U32 counter);
virtual	Record	add(			sp<Layout> spLayout,
								U32 startTime,
								U32 interval);
virtual	Record	lookup(			sp<Layout> spLayout,
								U32 interval);
virtual	bool	remove(			Record pulse);

		// AS HandlerI
virtual	void	handle(			Record &signal);
virtual	void	handleBind(		sp<SignalerI> spSignalerI,
								sp<Layout> spLayout);

	private:

	class t_pulse : public Counted
	{
		public:
			Record				m_record;
			AsSequenceSignal	m_asSeqSignal;
			int	time(void)
			{
				return m_asSeqSignal.time(m_record);
			}
			int	interval(void)
			{
				return m_asSeqSignal.interval(m_record);
			}
	};

		void	insert(			sp<t_pulse> &a_pulse);

	private:
		U32						m_timeOffset;
		SignalerI				*m_pSignaler;
		std::list<sp<t_pulse> >	m_pulseList;
		SystemTicker			m_ticker;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __signal_PushSequencer_h__ */

