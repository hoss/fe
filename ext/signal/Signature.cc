/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <signal/signal.pmh>

namespace fe
{
namespace ext
{

Signature::Signature(void)
{
}

Signature::Signature(const Signature &other)
{
	m_argSpecs = other.m_argSpecs;
}

Signature::~Signature(void)
{
}

Array<Signature::ArgSpec> &Signature::argspecs(void)
{
	return m_argSpecs;
}

Signature &Signature::operator=(const Signature &other)
{
	m_argSpecs = other.m_argSpecs;
	return *this;
}

bool Signature::match(t_arglist a_argv)
{
	unsigned int sz = a_argv.size();
	if(sz != m_argSpecs.size())
	{
		return false;
	}

	for(unsigned int i = 0; i < sz; i++)
	{
		if(m_argSpecs[i].m_spType != a_argv[i].type())
		{
			return false;
		}
		if(m_argSpecs[i].m_instance.type().isValid())
		{
			if(!m_argSpecs[i].m_instance.type()->equiv(
				m_argSpecs[i].m_instance.data(), a_argv[i].data()))
			{
				return false;
			}
		}
	}

	return true;
}

void Signature::append(sp<BaseType> a_type)
{
	ArgSpec argspec;
	argspec.m_spType = a_type;
	m_argSpecs.push_back(argspec);
}

void Signature::append(sp<BaseType> a_type, Instance a_value)
{
	ArgSpec argspec;
	argspec.m_spType = a_type;
	argspec.m_instance = a_value;
	m_argSpecs.push_back(argspec);
}

} /* namespace ext */
} /* namespace fe */

