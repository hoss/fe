/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <signal/signal.pmh>

namespace fe
{
namespace ext
{

PushSequencer::PushSequencer(void)
{
	m_pSignaler = NULL;
	m_timeOffset = 0;
}

PushSequencer::~PushSequencer(void)
{
}

U32 PushSequencer::getCurrentTime(void)
{
	return m_ticker.timeMS() + m_timeOffset;
}

void PushSequencer::setTime(U32 counter)
{
	m_timeOffset = counter - m_ticker.timeMS();
}

Record PushSequencer::add(sp<Layout> spLayout, U32 startTime, U32 interval)
{
	if(!m_pSignaler)
	{
		feX("PushSequencer::add",
			"pulseName: %s: Signaler not set; sequencer not registered?",
			spLayout->name().c_str());
	}

	sp<t_pulse> spPulse(new t_pulse());
	spPulse->m_asSeqSignal.bind(spLayout->scope());

	spPulse->m_asSeqSignal.populate(spLayout);

	spPulse->m_record = spLayout->scope()->createRecord(spLayout);

	spPulse->m_asSeqSignal.time(spPulse->m_record) = startTime;
	spPulse->m_asSeqSignal.interval(spPulse->m_record) = interval;
	spPulse->m_asSeqSignal.count(spPulse->m_record) = 0;
	spPulse->m_asSeqSignal.periodic(spPulse->m_record) = 0;

	insert(spPulse);

	return spPulse->m_record;
}

void PushSequencer::insert(sp<t_pulse> &a_pulse)
{
	std::list<sp<t_pulse> >::iterator it = m_pulseList.begin();
	for(; it != m_pulseList.end(); it++)
	{
		if((*it)->time() > a_pulse->time())
		{
			break;
		}
	}
	m_pulseList.insert(it,a_pulse);
}

Record PushSequencer::lookup(sp<Layout> spLayout, U32 interval)
{
	std::list<sp<t_pulse> >::iterator it = m_pulseList.begin();
	for(; it != m_pulseList.end(); it++)
	{
		if(interval != (U32)(*it)->interval()) { continue; }
		return (*it)->m_record;
	}
	Record record;
	return record;
}

bool PushSequencer::remove(Record pulse)
{
	std::list<sp<t_pulse> >::iterator it = m_pulseList.begin();
	for(; it != m_pulseList.end(); it++)
	{
		if((*it)->m_record.idr() == pulse.idr())
		{
			m_pulseList.erase(it);
			return true;
		}
	}
	return false;
}

void PushSequencer::handle(Record &signal)
{
	U32 current = getCurrentTime();
	//feLog("PushSequencer HANDLE %d\n", current);
	std::list<sp<t_pulse> >::iterator it = m_pulseList.begin();
	while(it != m_pulseList.end())
	{
		std::list<sp<t_pulse> >::iterator it_del = it;
		sp<t_pulse> spPulse = (*it);
		Record pulse = spPulse->m_record;
		AsSequenceSignal &asSeqSignal = spPulse->m_asSeqSignal;
		it++;
		if(asSeqSignal.time(pulse)-current-1 > FE_SEQUENCER_ROLLOVER_WINDOW)
		{
			m_pulseList.erase(it_del);
			if(asSeqSignal.count(pulse) >= 0)
			{
				m_pSignaler->signal(pulse);
			}
			bool repeat = true;
			if(asSeqSignal.count(pulse) > 0)
			{
				asSeqSignal.count(pulse) = asSeqSignal.count(pulse) - 1;
				if(asSeqSignal.count(pulse) == 0)
				{
					repeat = false;
				}
			}
			if(repeat)
			{
				if(asSeqSignal.periodic(pulse) == 1)
				{
					asSeqSignal.time(pulse) = asSeqSignal.time(pulse)
						+ asSeqSignal.interval(pulse);
					insert(spPulse);
					it = m_pulseList.begin();
				}
				else if(asSeqSignal.periodic(pulse) == 2)
				{
					asSeqSignal.time(pulse) = asSeqSignal.time(pulse)
						+ asSeqSignal.interval(pulse);
					if(asSeqSignal.time(pulse) < (I32)current)
					{
						//feLog("PushSequencer lost %d ms\n", current - asSeqSignal.time(pulse));
						asSeqSignal.time(pulse) = current;
						insert(spPulse);
					}
					else
					{
						//feLog("PushSequencer caught up %d %d\n", asSeqSignal.time(pulse), current);
						insert(spPulse);
						it = m_pulseList.begin();
					}
				}
				else if(asSeqSignal.periodic(pulse) == 3)
				{
					asSeqSignal.time(pulse) = getCurrentTime()
						+ asSeqSignal.interval(pulse);
					insert(spPulse);
				}
				else
				{
					asSeqSignal.time(pulse) = current
						+ asSeqSignal.interval(pulse);
					insert(spPulse);
				}
			}
		}
	}
}

void PushSequencer::handleBind(sp<SignalerI> spSignalerI, sp<Layout> spLayout)
{
	m_pSignaler = spSignalerI.raw();
}

} /* namespace ext */
} /* namespace fe */
