/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <signal/signal.pmh>

namespace fe
{
namespace ext
{

LogHandler::LogHandler(void)
{
}

LogHandler::~LogHandler(void)
{
}

void LogHandler::handle(Record &signal)
{
	feLog("LogHandler::handle %s\n", signal.layout()->name().c_str());
}


} /* namespace ext */
} /* namespace fe */


