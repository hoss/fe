/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __signal_dispatchi_h__
#define __signal_dispatchi_h__

namespace fe
{
namespace ext
{

/** Signature map */
typedef std::map<String, Signature> SignatureMap;

/**	@brief General call mechanism to enable exposure to scripting without
	requiring any binding work

	@ingroup signal

	This is similar in purpose to ParserI but but can handle multiple types.

	This (and ParserI) should probably reside in some other module than signal

	see @ref component_binding
	*/
class FE_DL_EXPORT DispatchI:
	public virtual Component,
	public CastableAs<DispatchI>
{
	public:
					/**	Execute the call.  See Signature for matching support.
						*/
virtual	bool				call(	const String &a_name,
									Array<Instance>& a_argv)				= 0;

virtual	bool				call(	const String &a_name,
									InstanceMap& a_argv)					= 0;

					/** Return a the signature supported by call() */
virtual	SignatureMap		&signatures(void)								= 0;

		template<class T>
		bool				call(	const String &a_name,
									T &a_arg1);
		template<class T>
		bool				call(	const String &a_name,
									const T &a_arg1);
		template<class T, class U>
		bool				call(	const String &a_name,
									T &a_arg1,
									U &a_arg2);
		template<class T, class U>
		bool				call(	const String &a_name,
									const T &a_arg1,
									const U &a_arg2);

#if 0
		template <class T>
		void				dispatch(const String &a_name);
#endif

virtual	bool				checkedCall(const String &a_name,
									Array<Instance> a_argv)			= 0;

};

template<class T>
bool DispatchI::call(const String &a_name, T &a_arg1)
{
	Array<Instance> args;
	Instance instance;
	instance.set<T>(registry()->master()->typeMaster(), a_arg1);
	args.push_back(instance);
	return call(a_name, args);
}

template<class T>
bool DispatchI::call(const String &a_name, const T &a_arg1)
{
	T arg1;
	arg1 = a_arg1;
	Array<Instance> args;
	Instance instance;
	instance.set<T>(registry()->master()->typeMaster(), arg1);
	args.push_back(instance);
	return call(a_name, args);
}

template<class T, class U>
bool DispatchI::call(const String &a_name, T &a_arg1, U &a_arg2)
{
	Array<Instance> args;
	Instance instance;
	instance.set<T>(registry()->master()->typeMaster(), a_arg1);
	args.push_back(instance);
	instance.set<U>(registry()->master()->typeMaster(), a_arg2);
	args.push_back(instance);
	return call(a_name, args);
}

template<class T, class U>
bool DispatchI::call(const String &a_name, const T &a_arg1, const U &a_arg2)
{
	T arg1;
	U arg2;
	arg1 = a_arg1;
	arg2 = a_arg2;
	Array<Instance> args;
	Instance instance;
	instance.set<T>(registry()->master()->typeMaster(), arg1);
	args.push_back(instance);
	instance.set<U>(registry()->master()->typeMaster(), arg2);
	args.push_back(instance);
	return call(a_name, args);
}

#if 0
template<class T>
void DispatchI::dispatch(const String &a_name)
{
	m_dispatchSignatures[a_name].append(
		registry()->master()->typeMaster()->lookupType(TypeInfo(typeid(T))));
}
#endif

} /* namespace ext */
} /* namespace fe */

#endif /* __signal_dispatchi_h__ */

