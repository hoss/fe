/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <signal/signal.pmh>

using namespace fe;
using namespace fe::ext;

class PreHandler:
	virtual public HandlerI
{
	public:
				PreHandler(void)		{ setName("MyHandler"); }
virtual	void	handle(Record &signal)	{ feLog("pre\n"); }
};

class MyHandler:
	virtual public HandlerI,
	virtual public ParserI
{
	public:
				MyHandler(void)			{ setName("MyHandler"); }
virtual	void	handle(Record &signal)	{ feLog("%d\n", m_aA(signal)); }
virtual	void	handleBind(sp<SignalerI> spSignalerI, sp<Layout> spLayout)
				{
					m_aA.initialize(spLayout->scope(), "a");
				}
virtual	void	parse(Array<String> &tokens)
				{
				}
	private:
		Accessor<int>	m_aA;
};

class AnotherHandler:
	virtual public HandlerI
{
	public:
				AnotherHandler(void)	{ setName("AnotherHandler"); }
virtual	void	handle(Record &signal)	{ feLog("%d\n", m_aA(signal)); }
virtual	void	handleBind(sp<SignalerI> spSignalerI, sp<Layout> spLayout)
				{
					m_aA.initialize(spLayout->scope(), "a");
				}
	private:
		Accessor<int>	m_aA;
};


int main(void)
{
	UnitTest unitTest;

	try
	{
		sp<Master> spMaster(new Master);
		sp<Scope> spScope(new Scope(*spMaster.raw()));

		sp<SignalerI> spSignalerI(new ChainSignaler());
		spSignalerI->setName("ChainSignaler");

		Accessor<int>	a(spScope,"a");

		sp<Layout>	spSignal = spScope->declare("SIGNAL");
		sp<Layout>	spPreLayout;

		spSignal->populate(a);

		sp<HandlerI> spMyHandler(new MyHandler);
		sp<HandlerI> spPreHandler(new PreHandler);
		spSignalerI->insert(spMyHandler, spSignal);
		spSignalerI->insert(spPreHandler, spPreLayout);
		spSignalerI->insert(spMyHandler, spSignal);

		Array<String> v;
		v.push_back("one");
		v.push_back("two");
		sp<ParserI>(spMyHandler)->parse(v);

		Record signal = spScope->createRecord(spSignal);
		a(signal) = 42;

		spSignalerI->signal(signal);

//		spScope->shutdown();
	}
	catch(Exception &e)
	{
		e.log();
	}
}

