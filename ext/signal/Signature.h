/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __signal_Signature_h__
#define __signal_Signature_h__

namespace fe
{
namespace ext
{

typedef Array<Instance> t_arglist;

/**	@ brief Defines a call argument signature for DispatchI

	@ingroup signal
	*/
class FE_DL_EXPORT Signature
{
	public:
		class ArgSpec
		{
			public:
				sp<BaseType>		m_spType;
				Instance			m_instance; // for exact match
		};

					Signature(void);
					Signature(const Signature &other);
		Signature	&operator=(const Signature &other);
virtual				~Signature(void);
virtual	bool		match(t_arglist a_argv);
virtual	void		append(sp<BaseType> a_type);
virtual	void		append(sp<BaseType> a_type, Instance a_value);
		template <class T>
		void		append(sp<TypeMaster> a_spTM, T *a_value);

		Array<ArgSpec>
					&argspecs(void);

	private:
		Array<ArgSpec>		m_argSpecs;
};

template<class T>
void Signature::append(sp<TypeMaster> a_spTM, T *a_value)
{
	sp<BaseType> spBT;
	spBT = a_spTM->lookupType(TypeInfo(getTypeId<T>()));
	Instance instance(reinterpret_cast<void *>(a_value), spBT, NULL);
	append(spBT, instance);
}

} /* namespace ext */
} /* namespace fe */

#endif /* __signal_Signature_h__ */

