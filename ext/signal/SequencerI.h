/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __signal_SequencerI_h__
#define __signal_SequencerI_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Timed record broadcaster

	@ingroup signal
*//***************************************************************************/
class FE_DL_EXPORT SequencerI:
	virtual public Component,
	public CastableAs<SequencerI>
{
	public:
virtual	U32		getCurrentTime(void)										= 0;
virtual	void	setTime(		U32 counter)								= 0;
virtual	Record	add(			sp<Layout> spLayout,
								U32 startTime,
								U32 interval)								= 0;
virtual	Record	lookup(			sp<Layout> spLayout,
								U32 interval)								= 0;
virtual	bool	remove(			Record pulse)								= 0;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __signal_SequencerI_h__ */

