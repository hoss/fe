
/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <signal/signal.pmh>

namespace fe
{
namespace ext
{

Dispatch::Dispatch(void)
{
}

Dispatch::~Dispatch(void)
{
}

SignatureMap &Dispatch::signatures(void)
{
	return m_dispatchSignatures;
}

bool Dispatch::checkedCall(const String &a_name, Array<Instance> a_argv)
{
	if(!signatures()[a_name].match(a_argv))
	{
		return false;
	}

	return call(a_name, a_argv);
}

} /* namespace ext */
} /* namespace fe */

