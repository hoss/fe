/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __networkhost_NetHost_h__
#define __networkhost_NetHost_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
    @brief Point of Entry for Net Development

	@ingroup networkhost
*//***************************************************************************/
class FE_DL_PUBLIC FE_NETWORKHOST_PORT NetHost:
	public Handled<NetHost>,
	public CastableAs<NetHost>
{
	public:
								NetHost(void);
virtual							~NetHost(void);

								/** @brief Return a NetHost singleton

									If the HostHost does not yet exist,
									it is created.
								*/
static	sp<NetHost> FE_CDECL	create(void);

								/** @brief Internal bookkeeping function

									@internal

									Do not call this method.
								*/
virtual	void					release(void);

								/** @brief Return the Master singleton

									A Master will have been created before
									you get the reference to a NetHost.
								*/
		sp<Master>				master(void)
									{	return ms_spSingleMaster->master(); }

								/// @brief Return TRUE if the space exists
		BWORD					hasSpace(String a_space);

								/** @brief Return a named space

									As a StateCatalog, a space is a
									mutex-protected double-key dictionary
									with freely extensible value types.

									If the space does not exist,
									it will be created.

									An implementation string can be
									specified to narrow what is used
									to provide the space,
									such as a particular network framework.
								*/
		sp<StateCatalog>		accessSpace(String a_space,
										String a_implementation=
										"ConnectedCatalog");
//										"*.ZeroCatalog");

protected:

static	sp<SingleMaster>		singleMaster(void);

static	void					lock(void)
								{	ms_recursiveMutex.lock(); }
static	void					unlock(void)
								{	ms_recursiveMutex.lock(); }

	private:
static	FE_DL_PUBLIC	sp<SingleMaster>	ms_spSingleMaster;
static	FE_DL_PUBLIC	RecursiveMutex		ms_recursiveMutex;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __networkhost_NetHost_h__ */
