/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "networkhost/networkhost.h"
#include "math/math.h"

using namespace fe;
using namespace fe::ext;

int main(int argc,char** argv)
{
	UNIT_START();

	BWORD complete=FALSE;

	try
	{
		{
			sp<NetHost> spNetHost(NetHost::create());
			const void* raw1=spNetHost.raw();
			feLog("spNetHost %p\n",raw1);

			sp<NetHost> spNetHost2(NetHost::create());
			const void* raw2=spNetHost2.raw();
			feLog("spNetHost2 %p\n",raw2);

			UNIT_TEST(raw1==raw2);

			sp<Master> spMaster=spNetHost->master();
			sp<Registry> spRegistry=spMaster->registry();

			Result result=spRegistry->manage("feAutoLoadDL");
			UNIT_TEST(successful(result));

			result=spRegistry->manage("fexNetworkHostDL");
			UNIT_TEST(successful(result));
		}

		complete=TRUE;
	}
	catch(Exception &e) { e.log(); }
	catch(...)			{ feLog("uncaught exception\n"); }

	UNIT_TEST(complete);
	UNIT_TEST(!Counted::trackerCount());
	UNIT_TRACK(5);
	UNIT_RETURN();
}
