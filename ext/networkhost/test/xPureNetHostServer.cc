/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "networkhost/networkhost.h"

#include <thread>

using namespace fe;
using namespace fe::ext;

int main(int argc,char** argv)
{
	UNIT_START();

	BWORD complete=FALSE;

	try
	{
		{
			sp<NetHost> spNetHost(NetHost::create());
			FEASSERT(spNetHost.isValid());

			sp<Master> spMaster=spNetHost->master();
			sp<Registry> spRegistry=spMaster->registry();

			Result result=spRegistry->manage("feAutoLoadDL");
			UNIT_TEST(successful(result));

			String implementation="ConnectedCatalog";
			if(argc>1)
			{
				implementation=argv[1];
			}
			feLog("implementation \"%s\"\n",implementation.c_str());

			sp<StateCatalog> spStateCatalog=
					spNetHost->accessSpace("world",implementation);
			FEASSERT(spStateCatalog.isValid());

			if(implementation=="*.SharedMemCatalog")
			{
				result=spStateCatalog->configure(
						"/xPureNetHostServer role=server");
			}
			else
			{
				result=spStateCatalog->configure(
						"*:9002 role=server ioPriority=high");
			}

			result=spStateCatalog->start();
			UNIT_TEST(successful(result));

			if(successful(result))
			{
				spStateCatalog->waitForConnection();

				spStateCatalog->setState<bool>("running",true);
				spStateCatalog->flush();

				I32 frame(0);

				while(TRUE)
				{
					spStateCatalog->setState<I32>("frame",frame++);
					spStateCatalog->flush();

					std::this_thread::sleep_for(std::chrono::milliseconds(1));

					//* TODO break out
				}

				spStateCatalog->setState<bool>("running",false);
				spStateCatalog->flush();

				spStateCatalog->stop();

				feLog("End State:\n");
				spStateCatalog->catalogDump();
			}

			spNetHost=NULL;
		}

		complete=TRUE;
	}
	catch(Exception &e) { e.log(); }
	catch(...)			{ feLog("uncaught exception\n"); }

	UNIT_TEST(complete);
	UNIT_TEST(!Counted::trackerCount());
	UNIT_TRACK(4);
	UNIT_RETURN();
}
