/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#define SIDE_TASKS	2
#define POD_TEST	TRUE

#include "networkhost/networkhost.h"
#include "math/math.h"

using namespace fe;
using namespace fe::ext;

class SideTask: public Thread::Functor
{
	public:
				SideTask(void):
					m_pings(0)
				{	feLog("SideTask::SideTask()\n"); }
virtual			~SideTask(void)
				{	feLog("SideTask::~SideTask()\n"); }
virtual	void	operate(void)
				{
					I32 frame(0);
					while(TRUE)
					{
						m_hpStateCatalog->getState<I32>("frame",frame);
						if(frame>=3000)
						{
							break;
						}

						SpatialTransform xform;
						setIdentity(xform);

						const I32 transformCount=4;
						for(I32 transformIndex=0;transformIndex<transformCount;
								transformIndex++)
						{
							String xformKey;
							xformKey.sPrintf("%s.xform%02d",
									m_key.c_str(),transformIndex);
							m_hpStateCatalog->setState<SpatialTransform>(
									xformKey,xform);
						}

//						feLog("%s %d\n",m_key.c_str(),m_pings);
						m_hpStateCatalog->setState<I32>(m_key,m_pings++);
						milliSleep(1);
					}
				}

		hp<StateCatalog>	m_hpStateCatalog;
		String				m_key;
		I32					m_pings;
};

int main(int argc,char** argv)
{
	UNIT_START();

	BWORD complete=FALSE;

	try
	{
		{
			sp<NetHost> spNetHost(NetHost::create());
			FEASSERT(spNetHost.isValid());

			sp<Master> spMaster=spNetHost->master();
			sp<Registry> spRegistry=spMaster->registry();

			Result result=spRegistry->manage("feAutoLoadDL");
			UNIT_TEST(successful(result));

			String implementation="ConnectedCatalog";
			if(argc>1)
			{
				implementation=argv[1];
			}
			feLog("implementation \"%s\"\n",implementation.c_str());

			sp<StateCatalog> spStateCatalog=
					spNetHost->accessSpace("world",implementation);
			UNIT_TEST(spNetHost->hasSpace("world"));

			FEASSERT(spStateCatalog.isValid());

			if(implementation=="*.SharedMemCatalog")
			{
				result=spStateCatalog->configure(
						"/xNetHostServer role=server");
			}
			else
			{
				result=spStateCatalog->configure(
						"*:7890 role=server ioPriority=high");
			}

			result=spStateCatalog->start();
			UNIT_TEST(successful(result));

#if TRUE
			Array<U8> byteBlock(5);
			strcpy((char*)byteBlock.data(),"3.14");
			spStateCatalog->setState< Array<U8> >("byteBlock",byteBlock);
#endif

#if POD_TEST
			struct Pod
			{
				int		intValue;
				float	floatValue;
				char	stringValue[16];
			} pod;

			pod=Pod{42, 1.23, "hello"};

			I32 structBytes=sizeof(pod);
			Array<U8> structBlock(structBytes);
			memcpy((char*)structBlock.data(),&pod,structBytes);

			spStateCatalog->setState< Array<U8> >("structBlock",structBlock);
#endif

			if(successful(result))
			{
				spStateCatalog->waitForConnection();

				const I32 frameCount=4000;

				SpatialTransform xform;
				setIdentity(xform);

				spStateCatalog->setState<bool>("running",true);
				spStateCatalog->flush();

#if SIDE_TASKS>0
				Array<Thread*> sideThread(SIDE_TASKS);
				Array<SideTask> sideTask(SIDE_TASKS);
				for(I32 m=0;m<SIDE_TASKS;m++)
				{
					sideTask[m].m_key.sPrintf("key%d",m);
					sideTask[m].m_hpStateCatalog=spStateCatalog;
					sideThread[m]=new Thread(&sideTask[m]);
				}
#endif

				for(I32 frame=0;frame<frameCount;frame++)
				{
					rotate(xform,0.1,e_zAxis);
					xform.translation()[0]=frame;
//					feLog("transform:\n%s\n",c_print(xform));
//
					spStateCatalog->setState<SpatialTransform>(
							"instances[0].transform",xform);
					spStateCatalog->setState<I32>("frame",frame);
					spStateCatalog->setState<String>("status","hint",
							"how we're doing");
					spStateCatalog->flush();

					Real nanoDelay(0);
					result=spStateCatalog->getState<Real>(
							"nanoDelay",nanoDelay);
					if(result==e_cannotFind)
					{
						nanoDelay=1e6;
					}
					nanoSpin(I32(nanoDelay));

					I32 messageIndex=0;
					while(TRUE)
					{
						String message;
						result=spStateCatalog->nextMessage("request",message);
						if(failure(result))
						{
							break;
						}
						feLog("message %d: \"%s\"\n",
								messageIndex++,message.c_str());
					}

					if((frame%150) == 75)
					{
						spStateCatalog->sendMessage<String>(
								"alert","Keep Going!");
					}
				}

#if POD_TEST
				Array<U8> structBlock2(structBytes);

				result=spStateCatalog->getState< Array<U8> >(
						"structBlock2",structBlock2);
				if(successful(result))
				{
					memcpy(&pod,(char*)structBlock2.data(),structBytes);
					feLog("pod: %d %.3f \"%s\"\n",
							pod.intValue,pod.floatValue,pod.stringValue);
				}
#endif

				spStateCatalog->setState<bool>("running",false);
				spStateCatalog->flush();

				feLog("last transform:\n%s\n",c_print(xform));

				spStateCatalog->stop();

				feLog("End State:\n");
				spStateCatalog->catalogDump();

#if SIDE_TASKS>0
				for(I32 m=0;m<SIDE_TASKS;m++)
				{
					sideThread[m]->join();
				}
#endif
			}

			spNetHost=NULL;
		}

		complete=TRUE;
	}
	catch(Exception &e) { e.log(); }
	catch(...)			{ feLog("uncaught exception\n"); }

	UNIT_TEST(complete);
	UNIT_TEST(!Counted::trackerCount());
	UNIT_TRACK(5);
	UNIT_RETURN();
}
