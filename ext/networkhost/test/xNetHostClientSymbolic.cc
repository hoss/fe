/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <networkhost/nethost_loader.h>	//* just basic OS calls
#include <thread>

#define TEST_BYTE_BLOCK		1

int main(int argc,char** argv)
{
	const char* implementation="ConnectedCatalog";
	if(argc>1)
	{
		implementation=argv[1];
	}

	std::this_thread::sleep_for(std::chrono::milliseconds(1000));

	feSym::Space space=feSym::accessSpace("world",implementation);

	if(!strcmp(implementation,"*.SharedMemCatalog"))
	{
		space.setState("net:address","/xNetHostServer");
	}
	else
	{
		space.setState("net:address","127.0.0.1");
		space.setState("net:port",7890);
	}

	space.setState("net:role","client");
	space.start();

	const int maxPolls=4000;
	int polls=0;
	int flushCount=0;
	int frame= -1;
	int unique=0;
	bool running=true;
	float xform[16];

	//* wait for the data to start
	space.waitForUpdate(flushCount);
	while(!space.getState("frame",&frame,1))
	{
		//* TODO yield
	}

#if TEST_BYTE_BLOCK
	{
		feSym::Space::ScopedBuffer<char*> scopedText=
				space.getStateScoped<char*>("net:role");
		printf("net:role is '%s'\n",(char*)scopedText.data());
	}

	const int byteCount(8);
	unsigned char byteBuffer[byteCount];
	for(int m=0;m<byteCount;m++)
	{
		byteBuffer[m]=100+m;
	}
	space.setState("test.block",byteBuffer,byteCount);

	{
		feSym::Space::ScopedBuffer<unsigned char*> scopedBytes=
				space.getStateScoped<unsigned char*>("test.block");
		printf("test.block is %d bytes\n",scopedBytes.size());
	}

	fflush(stdout);
#endif

	while(running)
	{
		const int lastFrame=frame;

		{
			feSym::Space::Atomic atomic(space,flushCount);
			if(!atomic.getState("running",&running,1))
			{
				printf("failed to get 'running'\n");
			}
			if(!atomic.getState("frame",&frame,1))
			{
				printf("failed to get 'frame'\n");
			}
			if(!atomic.getState("instances[0].transform",xform,16))
			{
				printf("failed to get transform\n");
			}
		}

		if(frame%100 == 50)
		{
			space.sendMessage("request","Hello server!");

			char message[1024];
			sprintf(message,"client is on frame %d",frame);
			space.sendMessage("request",message);

			space.flush();
		}

		if(frame!=lastFrame)
		{
			unique++;
		}

		const float tx=xform[12];
		if(frame!=int(tx))
		{
			printf("frame %d tx %.6G\n",frame,tx);
		}

		if(++polls > maxPolls)
		{
			printf("reached max polls %d\n",maxPolls);
			break;
		}
	}

	printf("final %d polls %d unique %d\n",frame,polls,unique);

	space.getState("instances[0].transform",xform,16);
	for(int index=0;index<16;index++)
	{
		printf("%8.3f ",xform[index]);
		if((index%4) == 3)
		{
			printf("\n");
		}
	}

	space.stop();
}
