/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "networkhost/networkhost.h"
#include "math/math.h"

using namespace fe;
using namespace fe::ext;

int main(int argc,char** argv)
{
	UNIT_START();

	BWORD complete=FALSE;

	try
	{
		{
			sp<NetHost> spNetHost(NetHost::create());
			FEASSERT(spNetHost.isValid());

			sp<Master> spMaster=spNetHost->master();
			sp<Registry> spRegistry=spMaster->registry();

			Result result=spRegistry->manage("feAutoLoadDL");
			UNIT_TEST(successful(result));

			String idString="0";
			if(argc>1)
			{
				idString=argv[1];
			}
			feLog("idString \"%s\"\n",idString.c_str());

			String implementation="ConnectedCatalog";
			if(argc>2)
			{
				implementation=argv[2];
			}
			feLog("implementation \"%s\"\n",implementation.c_str());

			sp<StateCatalog> spStateCatalog=
					spNetHost->accessSpace("world",implementation);
			FEASSERT(spStateCatalog.isValid());

			if(implementation=="*.SharedMemCatalog")
			{
				result=spStateCatalog->configure(
						"/xNetPureServer role=client");
			}
			else
			{
				result=spStateCatalog->configure("localhost:9002 role=client");
			}

			result=spStateCatalog->start();
			UNIT_TEST(successful(result));

			feLog("StateCatalog started\n");

			const I32 valueCount(10);
			sp<Scope> spScope=spRegistry->create("Scope");
			spScope->setLocking(FALSE);

			sp<Layout> spLayout = spScope->declare("values");

			Array< Accessor<Vector3d> > accessorArray(valueCount);
			for(I32 valueIndex=0;valueIndex<valueCount;valueIndex++)
			{
				String attrName;
				attrName.sPrintf("value[%d]",valueIndex);

				Accessor<Vector3d>& raValue(accessorArray[valueIndex]);
				raValue.setup(spScope,attrName);

				spLayout->populate(raValue);
			}

			Accessor<I32> aFrame(spScope,"frame");
			spLayout->populate(aFrame);

			Record record=spScope->createRecord(spLayout);
			for(I32 valueIndex=0;valueIndex<valueCount;valueIndex++)
			{
				String attrName;
				attrName.sPrintf("value[%d]",valueIndex);

				Accessor<Vector3d>& raValue(accessorArray[valueIndex]);
				raValue(record)=
						Vector3d(valueIndex+0.1,valueIndex+0.2,valueIndex+0.3);
			}

			sp<HandlerI> spSignalMessenger=
					spRegistry->create("*.SignalMessenger");
			sp<StateBindI> spStateBindI(spSignalMessenger);
			if(spStateBindI.isValid())
			{
				spStateBindI->bind(spStateCatalog);
				spStateBindI->setKey("test_signal");
			}

			sp<SignalerI> spSignalerI=spRegistry->create("SignalerI");
			spSignalerI->insert(spSignalMessenger,spLayout);

			String prefix;
			prefix.sPrintf("values[%s]",idString.c_str());

			String frameKey;
			frameKey.sPrintf("frame[%s]",idString.c_str());

			I32 step(0);

			if(successful(result))
			{
				spStateCatalog->waitForConnection();

				bool running=true;
				while(running)
				{
					I32 frame(-1);
					spStateCatalog->getState<I32>("frame",frame);

					I32 otherFrame(-1);
					Record otherRecord;
					spStateCatalog->getState<Record>("values[0].record[0]",
							otherRecord);
					if(otherRecord.isValid())
					{
						Accessor<I32> aOtherFrame(
								otherRecord.layout()->scope(),"frame");
						otherFrame=aOtherFrame(otherRecord);
					}

					feLog("frame %d other %d\n",frame,otherFrame);

					aFrame(record)=frame;

					spStateCatalog->setState<I32>(frameKey,frame);

					const I32 recordCount(50);
					for(I32 recordIndex=0;recordIndex<recordCount;recordIndex++)
					{
						String stateKey;
						stateKey.sPrintf("%s.record[%d]",
								prefix.c_str(),recordIndex);

						spStateCatalog->setState<Record>(stateKey,record);
					}
					spStateCatalog->flush();

					if(!(step%25))
					{
						spSignalerI->signal(record);
					}

					if(!(step%100))
					{
						spStateCatalog->catalogDump();
					}

					step++;

					milliSleep(20);
				}

				spStateCatalog->stop();
			}

			spNetHost=NULL;
		}

		complete=TRUE;
	}
	catch(Exception &e) { e.log(); }
	catch(...)			{ feLog("uncaught exception\n"); }

	UNIT_TEST(complete);
	UNIT_TEST(!Counted::trackerCount());
	UNIT_TRACK(9);
	UNIT_RETURN();
}
