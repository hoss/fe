/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <networkhost/networkhost.pmh>

#define	FE_NTD_DEBUG	FALSE

namespace fe
{
namespace ext
{

NetDispatch::NetDispatch(void)
{
#if FE_NTD_DEBUG
	feLog("NetDispatch::NetDispatch\n");
#endif
}

NetDispatch::~NetDispatch(void)
{
#if FE_NTD_DEBUG
	feLog("NetDispatch::~NetDispatch\n");
#endif
}

void NetDispatch::initialize(void)
{
	m_spNetHost=NetHost::create();
	FEASSERT(m_spNetHost.isValid());

	dispatch< sp<StateCatalog> >("accessSpace");	//* return value
	dispatch<String>("accessSpace");				//* first arg
}

bool NetDispatch::call(const String &a_name,Array<Instance>& a_argv)
{
#if FE_NTD_DEBUG
	feLog("NetDispatch::call ordered \"%s\" %d\n",
			a_name.c_str(),a_argv.size());
#endif

	//* NOTE a_argv[0] is for output

	const I32 argc=a_argv.size();

#if FE_NTD_DEBUG
	for(I32 index=0;index<argc;index++)
	{
		feLog("  %d/%d \"%s\"\n",index,argc,
				a_argv[index].data()? c_print(a_argv[index]): "NULL");
	}
#endif

	if(a_name == "accessSpace" && argc==2)
	{
		const String& key=a_argv[1].cast<String>();
		feLog("NetDispatch::call accessSpace(\"%s\")\n", key.c_str());

		a_argv[0].cast< sp<StateCatalog> >()=m_spNetHost->accessSpace(key);

		return true;
	}

	return false;
}

bool NetDispatch::call(const String &a_name,InstanceMap& a_argv)
{
#if FE_NTD_DEBUG
	feLog("NetDispatch::call map \"%s\" %d\n",
			a_name.c_str(),a_argv.size());
#endif

	feLog("NetDispatch::call map arg support not implemented\n");

	return false;
}

} /* namespace ext */
} /* namespace fe */
