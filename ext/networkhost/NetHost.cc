/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <networkhost/networkhost.pmh>

#define	FE_NETHOST_DEBUG	TRUE
#define	FE_NETHOST_SPACES	"network:spaces"

using namespace fe;
using namespace fe::ext;

namespace fe
{
namespace ext
{

FE_DL_PUBLIC FE_NETWORKHOST_PORT sp<SingleMaster> NetHost::ms_spSingleMaster;
FE_DL_PUBLIC FE_NETWORKHOST_PORT RecursiveMutex NetHost::ms_recursiveMutex;

NetHost::NetHost(void)
{
//	SingleMaster::usePrefix("fe  ");

#if FE_NETHOST_DEBUG
	feLog("NetHost::NetHost\n");
#endif
}

NetHost::~NetHost(void)
{
#if FE_NETHOST_DEBUG
	feLog("NetHost::~NetHost\n");
#endif
}

//* static
sp<SingleMaster> NetHost::singleMaster(void)
{
	ms_spSingleMaster=SingleMaster::create();
	return ms_spSingleMaster;
}

//* static
sp<NetHost> NetHost::create(void)
{
	//* NOTE does this need a mutex?

#if FE_NETHOST_DEBUG
	feLog("NetHost::create\n");
#endif

	ClassSafe<NetHost>::SafeGuard safeguard(&ms_recursiveMutex);

	ms_spSingleMaster=SingleMaster::create();
	if(ms_spSingleMaster.isNull())
	{
		return sp<NetHost>(NULL);
	}

	sp<Master> spMaster=ms_spSingleMaster->master();
	if(spMaster.isNull())
	{
		return sp<NetHost>(NULL);
	}

//	assertMath(spMaster->typeMaster());

//	sp<Registry> spRegistry=spMaster->registry();
//	Result result=spRegistry->manage("feAutoLoadDL");
//	FEASSERT(successful(result));

	sp<Catalog> spMasterCatalog=spMaster->catalog();

	sp<NetHost> spNetHost=
			spMasterCatalog->catalog< sp<Counted> >("NetHost");
	if(spNetHost.isNull())
	{
		spNetHost=new NetHost();
		spMasterCatalog->catalog< sp<Counted> >("NetHost")=spNetHost;

	}

	return spNetHost;
}

void NetHost::release(void)
{
#if FE_NETHOST_DEBUG
//	feLog("NetHost::release %p count %d\n",this,count());
#endif

	ClassSafe<NetHost>::SafeGuard safeguard(&ms_recursiveMutex);

	const I32 newCount=count()-1;

	Counted::release();

	if(newCount==1)
	{
#if FE_NETHOST_DEBUG
		feLog("NetHost::release drop SingleMaster\n");
#endif

		if(ms_spSingleMaster.isValid())
		{
			sp<Master> spMaster=ms_spSingleMaster->master();
			if(spMaster.isValid())
			{
				sp<Catalog> spMasterCatalog=spMaster->catalog();
				spMasterCatalog->catalog< sp<Catalog> >(FE_NETHOST_SPACES)=NULL;
			}

			ms_spSingleMaster=NULL;
		}
		else
		{
			feLog("NetHost::release SingleMaster was already invalid\n");
		}
	}
}

BWORD NetHost::hasSpace(fe::String a_space)
{
	ClassSafe<NetHost>::SafeGuard safeguard(&ms_recursiveMutex);

	sp<Master> spMaster=ms_spSingleMaster->master();
	sp<Catalog> spMasterCatalog=spMaster->catalog();
	sp<Catalog> spSpacesCatalog=
			spMasterCatalog->catalog< sp<Catalog> >(FE_NETHOST_SPACES);
	if(spSpacesCatalog.isNull())
	{
		return FALSE;
	}

	return spSpacesCatalog->cataloged(a_space);
}

sp<StateCatalog> NetHost::accessSpace(fe::String a_space,
	fe::String a_implementation)
{
	ClassSafe<NetHost>::SafeGuard safeguard(&ms_recursiveMutex);

	sp<Master> spMaster=ms_spSingleMaster->master();
	sp<Catalog> spMasterCatalog=spMaster->catalog();
	sp<Catalog> spSpacesCatalog=
			spMasterCatalog->catalog< sp<Catalog> >(FE_NETHOST_SPACES);
	if(spSpacesCatalog.isNull())
	{
		spSpacesCatalog=master()->createCatalog(FE_NETHOST_SPACES);
		spMasterCatalog->catalog< sp<Catalog> >(FE_NETHOST_SPACES)=
				spSpacesCatalog;
	}

	sp<StateCatalog> spStateCatalog;
	if(spSpacesCatalog->cataloged(a_space))
	{
		spStateCatalog=spSpacesCatalog->catalog< sp<Catalog> >(a_space);
	}
	if(spStateCatalog.isNull())
	{
		spStateCatalog=master()->registry()->create(a_implementation);
		if(spStateCatalog.isValid())
		{
			spSpacesCatalog->catalog< sp<Catalog> >(a_space)=spStateCatalog;
		}
	}

	return spStateCatalog;
}

} /* namespace ext */
} /* namespace fe */
