#! /usr/bin/python3

import os
scriptDir = os.path.dirname(os.path.realpath(__file__))

import sys
sys.path.append(scriptDir)

print(">> import pyfe.context")
import pyfe.context

print(">> create context")
fe = pyfe.context.Context()

print(">> get master")
master = fe.master()

print(">> get paths")
print(master["path:media"])
print(master["path:plugin"])

print(">> import pyfe.nethost")
import pyfe.nethost
import numpy

netHost = pyfe.nethost.NetHost()

print(">> get space")
space = netHost.accessSpace("world")

print(">> set space state")
space.setState("net:role","server")
space.setState("net:address","127.0.0.1")
space.setState("net:port",7890)

print(">> start space")
space.start()

print(">> wait for connection")
space.waitForConnection()

print(">> get catalog spaces")
spaces = master["network:spaces"]
print(spaces)

print(">> stop space")
space.stop()

print(">> script complete")
