/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __solve_RayPlaneIntersect_h__
#define __solve_RayPlaneIntersect_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Find intersection between ray and plane

	@ingroup solve
*//***************************************************************************/
template <typename T>
class RayPlaneIntersect: public RayDiskIntersect<T>
{
	public:
static	T		solve(const Vector<3,T>& center,const Vector<3,T>& facing,
						const T radius,
						const Vector<3,T>& origin,
						const Vector<3,T>& direction);

static	void	resolveContact(const Vector<3,T>& center,
						const Vector<3,T>& facing,
						const T radius,
						const Vector<3,T>& origin,
						const Vector<3,T>& direction,
						const T range,
						Vector<3,T>& intersection,
						Vector<3,T>& normal);
};

// direction must be normalized
template <typename T>
inline T RayPlaneIntersect<T>::solve(const Vector<3,T>& center,
		const Vector<3,T>& facing,T radius,
		const Vector<3,T>& origin, const Vector<3,T>& direction)
{
	const T incident=dot(facing,direction);
	if(incident==0.0f)
	{
		return T(-1);
	}
//	return (dot(facing,center)-dot(facing,origin))/incident;
	return dot(facing,center-origin)/incident;
}

// direction must be normalized
template <typename T>
inline void RayPlaneIntersect<T>::resolveContact(
		const Vector<3,T>& center,
		const Vector<3,T>& facing,
		const T radius,
		const Vector<3,T>& origin,
		const Vector<3,T>& direction,
		const T range,
		Vector<3,T>& intersection,
		Vector<3,T>& normal)
{
	normal=facing;
	intersection=origin+direction*range;
}

} /* namespace ext */
} /* namespace fe */

#endif /* __solve_RayPlaneIntersect_h__ */


