import sys
import os
forge = sys.modules["forge"]

def prerequisites():
	return [ "datatool" ]

def setup(module):

	srcList = [	"BasisSpline",
				"ConvergentSpline",
				"Geodesic",
				"InvKineCCD",
				"Noise",
				"PointConnect",
				"geometry.pmh",
				"geometryDL"
				]

	dll = module.DLL( "fexGeometryDL", srcList )

	for src in [ "Noise" ]:
		srcTarget = module.FindObjTargetForSrc(src)
		srcTarget.cppmap = {}
		srcTarget.cppmap['std'] = forge.use_std("c++11")

	deplibs = forge.corelibs+ [
				"fexSignalLib",
				"fexDataToolLib" ]

	forge.deps( ["fexGeometryDLLib"], deplibs )

#	forge.tests += [
#		("inspect.exe",		"fexGeometryDL",				None,		None) ]

	module.Module( 'test' )
