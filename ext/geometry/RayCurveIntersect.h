/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __solve_RayCurveIntersect_h__
#define __solve_RayCurveIntersect_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Find intersection between ray and curve with radius

	The radius tolerance can be adjusted by distance, useful for picking.
	The radius0 is at distance 0.  The radius0 is at distance 1.

	@ingroup solve

*//***************************************************************************/
template <typename T>
class RayCurveIntersect
{
	public:
static	T		solve(const Vector<3,T>* a_pVertex,const U32 a_vertCount,
						const T a_radiusAt0,const T a_radiusAt1,
						const Vector<3,T>& a_origin,
						const Vector<3,T>& a_direction,
						T& a_along,Vector<3,T>& a_intersection);
};

// direction must be normalized
template <typename T>
inline T RayCurveIntersect<T>::solve(
		const Vector<3,T>* a_pVertex,const U32 a_vertCount,
		const T a_radiusAt0,const T a_radiusAt1,
		const Vector<3,T>& a_origin, const Vector<3,T>& a_direction,
		T& a_along,Vector<3,T>& a_intersection)
{
	T bestMag= -1.0;
	T realIndex=0.0;
	for(U32 m=0;m<a_vertCount-1;m++)
	{
		const SpatialVector center=0.5*(a_pVertex[m]+a_pVertex[m+1]);
		const Real distance=magnitude(center-a_origin);
		const Real radius=a_radiusAt0+(a_radiusAt1-a_radiusAt0)*distance;

		SpatialVector oneIntersection;
		T oneAlong;
		T oneMag=RayCylinderIntersect<T>::solve(a_pVertex[m],
				a_pVertex[m+1]-a_pVertex[m],radius,
				a_origin,a_direction,oneAlong,oneIntersection);
		if(oneMag>=0.0 && (bestMag<0.0 || oneMag<bestMag))
		{
			a_intersection=oneIntersection;
			realIndex=m+oneAlong;
			bestMag=oneMag;
		}
	}

	a_along=(bestMag<0.0)? -1.0: (realIndex)/T(a_vertCount-1);

	return bestMag;
}

} /* namespace ext */
} /* namespace fe */

#endif /* __solve_RayCurveIntersect_h__ */
