/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __geometry_ConvergentSpline_h__
#define __geometry_ConvergentSpline_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Evaluate a stack of basis splines to approach the control points

	@ingroup geometry
*//***************************************************************************/
class FE_DL_EXPORT ConvergentSpline
{
	public:
						ConvergentSpline(void):
							m_iterations(100),
							m_threshold(1e-3),
							m_count(0),
							m_pKnots(NULL)
						{
							m_pControl[0]=NULL;
							m_pControl[1]=NULL;
							m_pControl[2]=NULL;
						}

						~ConvergentSpline(void)
						{	reset(); }

		void			setIterations(I32 a_iterations)
						{	m_iterations=a_iterations; }
		void			setThreshold(Real a_threshold)
						{	m_threshold=a_threshold; }

		Real			configure(I32 a_count,SpatialVector* a_pPoint,
							SpatialVector* a_pNormal);

		SpatialVector	solve(Real a_t);

	private:

		void			reset(void)
						{
							m_count=0;
							if(m_count)
							{
								delete m_pKnots;
								delete m_pControl[0];
								delete m_pControl[1];
								delete m_pControl[2];

								m_pKnots=NULL;
								m_pControl[0]=NULL;
								m_pControl[1]=NULL;
								m_pControl[2]=NULL;
							}
						}

		I32		m_iterations;
		Real	m_threshold;

		I32		m_count;
		F64*	m_pKnots;
		F64*	m_pControl[3];
};


} /* namespace ext */
} /* namespace fe */

#endif /* __geometry_ConvergentSpline_h__ */
