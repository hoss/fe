/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __solve_PointSphereNearest_h__
#define __solve_PointSphereNearest_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Find nearest point on sphere

	@ingroup solve
*//***************************************************************************/
template <typename T>
class PointSphereNearest
{
	public:
static	T		solve(const Vector<3,T>& center,const T radius,
						const Vector<3,T>& origin,
						Vector<3,T>& direction);

static	BWORD	within(const Vector<3,T>& center,const T radius,
						const Vector<3,T>& origin,const T distance);
};

template <typename T>
inline T PointSphereNearest<T>::solve(const Vector<3,T>& center,T radius,
		const Vector<3,T>& origin,Vector<3,T>& direction)
{
	const Vector<3,T> to_origin=origin-center;
	const T mag=magnitude(to_origin);
	if(mag>T(0))
	{
		direction= -T(1)/mag*to_origin;
	}
	else
	{
		set(direction);
	}
	return mag-radius;
}

template <typename T>
inline BWORD PointSphereNearest<T>::within(const Vector<3,T>& center,T radius,
		const Vector<3,T>& origin,const T distance)
{
	const Vector<3,T> to_origin=origin-center;
	const T mag2=magnitudeSquared(to_origin);
	const T sum=distance+radius;

	return (mag2<=(sum*sum));
}

} /* namespace ext */
} /* namespace fe */

#endif /* __solve_PointSphereNearest_h__ */

