/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __solve_RayDiskIntersect_h__
#define __solve_RayDiskIntersect_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Find intersection between ray and circular solid

	@ingroup solve
*//***************************************************************************/
template <typename T>
class RayDiskIntersect
{
	public:
static	T		solve(const Vector<3,T>& center,const Vector<3,T>& facing,
						const T radius,
						const Vector<3,T>& origin,
						const Vector<3,T>& direction,
						Vector<3,T>& intersection);
static	void	resolveContact(const Vector<3,T>& center,
						const Vector<3,T>& facing,
						const T radius,
						const Vector<3,T>& origin,
						const Vector<3,T>& direction,
						const T range,
						const Vector<3,T>& intersection,
						Vector<3,T>& normal);
};

// direction must be normalized
template <typename T>
inline T RayDiskIntersect<T>::solve(const Vector<3,T>& center,
		const Vector<3,T>& facing,T radius,
		const Vector<3,T>& origin, const Vector<3,T>& direction,
		Vector<3,T>& intersection)
{
//	feLog("center %s facing %s radius %.6G\n",c_print(center),radius);
//	feLog("origin: %s\n",c_print(origin));
//	feLog("direction: %s\n",c_print(direction));

	const T incident=dot(facing,direction);
	if(incident==0.0f)
	{
		return T(-1);
	}
	const T range=(dot(facing,center)-dot(facing,origin))/incident;

	intersection=origin+direction*range;
//	feLog("intersection: %s sq %.6G vs %.6G\n",c_print(intersection),
//			magnitudeSquared(intersection-center),radius*radius);

	if(magnitudeSquared(intersection-center)>radius*radius)
	{
		return T(-1);
	}

	return range;
}

// direction must be normalized
template <typename T>
inline void RayDiskIntersect<T>::resolveContact(
		const Vector<3,T>& center,
		const Vector<3,T>& facing,
		const T radius,
		const Vector<3,T>& origin,
		const Vector<3,T>& direction,
		const T range,
		const Vector<3,T>& intersection,
		Vector<3,T>& normal)
{
	normal=facing;
}

} /* namespace ext */
} /* namespace fe */

#endif /* __solve_RayDiskIntersect_h__ */

