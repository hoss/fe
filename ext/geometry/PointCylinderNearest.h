/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __geometry_PointCylinderNearest_h__
#define __geometry_PointCylinderNearest_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Find point nearest to a cylindrical solid

	@ingroup geometry
*//***************************************************************************/
template <typename T>
class PointCylinderNearest
{
	public:
static	T	solve(const Vector<3,T>& base,const Vector<3,T>& axis,
					const T radius,const Vector<3,T>& origin,
					Vector<3,T>& direction,T& along,Vector<3,T>& intersection);
};

template <typename T>
inline T PointCylinderNearest<T>::solve(
		const Vector<3,T>& base,const Vector<3,T>& axis,const T radius,
		const Vector<3,T>& origin, Vector<3,T>& direction,
		T& along,Vector<3,T>& intersection)
{
	const T length=magnitude(axis);
	const Vector<3,T> unitAxis=axis*(length>T(0)? T(1)/length: T(1));
	const Vector<3,T> towards=origin-base;
	along=dot(towards,unitAxis);

#if FALSE
	feLog("\nbase %s axis %s radius %.6G\n",
			c_print(base),c_print(axis),radius);
	feLog("origin: %s\n",c_print(origin));

	feLog("along: %.6G/%.6G\n",along,length);
#endif

	Vector<3,T> center=base+along*unitAxis;
	Vector<3,T> radial=origin-center;
	const T radial_mag=magnitude(radial);
	if(radial_mag>T(0))
	{
		radial*=radius/radial_mag;
	}
	if(along<T(0))
	{
		along=T(0);
		center=base;
	}
	else if(along>length)
	{
		along=length;
		center=base+axis;
	}

	intersection=center+radial;
	direction=intersection-origin;
	T mag=magnitude(direction);
	if(mag>T(0))
	{
		direction*=T(1)/mag;
	}

#if FALSE
	feLog("intersection: %s\n",c_print(intersection));
	feLog("direction: %s\n",c_print(direction));
#endif

	return mag;
}

} /* namespace ext */
} /* namespace fe */

#endif /* __geometry_PointCylinderNearest_h__ */
