/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "geometry/geometry.h"
#include "viewer/viewer.h"

#define	POWER_INCREMENT		0.04

using namespace fe;
using namespace fe::ext;

class MyDraw: public HandlerI
{
	public:
				MyDraw(sp<DrawI> spDrawI):
					m_spDrawI(spDrawI),
					m_power(0.0f)
				{
					setIdentity(m_delta);
					translate(m_delta,SpatialVector(0.0f,5.0f,5.0f));
					rotate(m_delta,90.0f*degToRad,e_xAxis);
				}

virtual void	handle(Record& render)
				{
					if(!m_asViewer.scope().isValid())
					{
						m_asViewer.bind(render.layout()->scope());
					}

					const I32& rLayer=m_asViewer.viewer_layer(render);
					if(rLayer==1)
					{
						m_spDrawI->drawAxes(1.0f);

						SpatialTransform transform;
						m_matrixPower.solve(transform,m_delta,m_power);

						const Color red(1.0f,0.0f,0.0f,1.0f);
						const F32 radius=1.0f;
						m_spDrawI->drawTransformedMarker(transform,radius,red);

						m_power+=POWER_INCREMENT;
					}
				}

	private:
		sp<DrawI>						m_spDrawI;
		AsViewer						m_asViewer;

		MatrixPower<SpatialTransform>	m_matrixPower;
		SpatialTransform				m_delta;
		F32								m_power;
};

int main(int argc,char** argv)
{
	UNIT_START();

	BWORD complete=FALSE;
	U32 frames=0;
	if(argc>1)
	{
		frames=atoi(argv[1])+1;
	}

	try
	{
		sp<Master> spMaster(new Master);
		sp<Registry> spRegistry=spMaster->registry();

		Result result=spRegistry->manage("fexViewerDL");
		UNIT_TEST(successful(result));

		{
			sp<QuickViewerI> spQuickViewerI(
					spRegistry->create("QuickViewerI"));
			if(!spQuickViewerI.isValid())
			{
				feX(argv[0], "couldn't create components");
			}

			spQuickViewerI->open();

			sp<DrawI> spDrawI=spQuickViewerI->getDrawI();
			if(spDrawI.isNull())
			{
				feX(argv[0], "couldn't get draw interface");
			}

			sp<MyDraw> spMyDraw(new MyDraw(spDrawI));

			spQuickViewerI->insertDrawHandler(spMyDraw);
			spQuickViewerI->run(frames);
		}

		complete=TRUE;
	}
	catch(Exception &e) { e.log(); }
	catch(...)			{ feLog("uncaught exception\n"); }

	UNIT_TEST(complete);
	UNIT_TEST(!Counted::trackerCount());
	UNIT_TRACK(3);
	UNIT_RETURN();
}
