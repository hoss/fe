/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "geometry/geometry.h"

using namespace fe;
using namespace fe::ext;

#define USE_4x4		FALSE

#if USE_4x4
typedef Matrix<4,4,F64> Transform;
#else
typedef SpatialTransform Transform;
#endif

int main(void)
{
	UNIT_START();
	BWORD completed=FALSE;

	try
	{
		Transform A;
		Transform B;

		for(U32 pass=0;pass<4;pass++)
		{
			SpatialTransform xform;

			setIdentity(xform);

			switch(pass)
			{
				case 0:
					rotate(xform,30.0f*degToRad,e_xAxis);
					translate(xform,SpatialVector(100.0f,200.0f,300.0f));
					rotate(xform,60.0f*degToRad,e_yAxis);
					break;
				case 1:
					rotate(xform,179.9f*degToRad,e_zAxis);
					break;
				case 2:
					rotate(xform,180.0f*degToRad,e_zAxis);
					break;
				case 3:
					xform.column(0)=unitSafe(SpatialVector(0.879,0.476,0.0));
					xform.column(1)=unitSafe(SpatialVector(0.476,-0.879,0.0));
					xform.column(2)=unitSafe(SpatialVector(0.0,0.0,-1.0));
					xform.column(3)=SpatialVector(0.862,2.377,0.0);
					break;
			}

			A=xform;

#if USE_4x4
			A(3,3)=1.0;
#endif

			feLog("\n****************\n");
			feLog("\nA\n%s\n",print(A).c_str());
			feLog("matrix B = sqrt A\n");

			MatrixSqrt<Transform> matrixSqrt;
			matrixSqrt.solve(B,A);

			Transform C=A-B*B;

			F32 sum=0.0f;
			for(U32 m=0;m<width(C);m++)
			{
				for(U32 n=0;n<height(C);n++)
				{
					sum+=fabs(C(m,n));
				}
			}

			feLog("\nB\n%s\n",print(B).c_str());
			feLog("\nB*B\n%s\n",print(B*B).c_str());
			feLog("\nC=A-B*B\n%s\n",print(C).c_str());

			feLog("\nC component sum = %.6G\n",sum);
			UNIT_TEST(sum < (pass==3? 0.1: 2e-3));
		}

		completed=TRUE;
	}
	catch(Exception &e)	{ e.log(); }
	catch(...)			{ feLog("uncaught exception\n"); }

	UNIT_TEST(completed);
	UNIT_TRACK(5);
	UNIT_RETURN();
}
