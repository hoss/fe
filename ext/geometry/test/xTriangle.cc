/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "geometry/geometry.h"
#include "viewer/viewer.h"

#define	POWER_INCREMENT		0.04

using namespace fe;
using namespace fe::ext;

class MyDraw: public HandlerI
{
	public:
				MyDraw(sp<DrawI> spDrawI):
					m_spDrawI(spDrawI)
				{
					const SpatialVector v1(1.0f,0.0f,0.0f);
					const SpatialVector v2(0.0f,1.0f,0.0f);
					const SpatialVector v3(0.0f,0.0f,1.0f);

					const SpatialVector n1(1.0f,0.0f,0.0f);
					const SpatialVector n2(0.0f,1.0f,0.0f);
					const SpatialVector n3(0.0f,0.0f,1.0f);

					m_trianglePN.configure(v1,v2,v3,n1,n2,n3);
					m_trianglePower.configure(-v1,-v2,v3,-n1,-n2,n3);
				}

virtual void	handle(Record& render)
				{
					if(!m_asViewer.scope().isValid())
					{
						m_asViewer.bind(render.layout()->scope());
					}

					const I32& rLayer=m_asViewer.viewer_layer(render);
					if(rLayer==1)
					{
						const Color red(1.0f,0.0f,0.0f,1.0f);
						const Color yellow(1.0f,1.0f,0.0f,1.0f);
						const Color green(0.0f,1.0f,0.0f,1.0f);
						const Color white(1.0f,1.0f,1.0f,1.0f);

						SpatialVector vertex;
						SpatialVector normal;

						m_spDrawI->drawAxes(1.0f);

//						m_spDrawI->drawAlignedText(v1," A",green);
//						m_spDrawI->drawAlignedText(v2," B",green);
//						m_spDrawI->drawAlignedText(v3," C",green);

						for(U32 pass=0;pass<2;pass++)
						{
							const Real inc=0.5;
							for(Real u=0.0;u<=1.01;u+=inc)
							{
								for(Real v=0.0;v<=1.01-u;v+=inc)
								{
									Barycenter<Real> barycenter(u,v);
									if(pass)
									{
										m_trianglePower.solve(
												barycenter,vertex,normal);
									}
									else
									{
										m_trianglePN.solve(
												barycenter,vertex,normal);
									}
									SpatialVector pLines[2];
									pLines[0]=vertex;
									pLines[1]=vertex+normal;

									const SpatialVector* pNormals=NULL;
									const BWORD multicolor=FALSE;
									m_spDrawI->drawLines(pLines,pNormals,2,
											DrawI::e_discrete,multicolor,&red);

									m_spDrawI->drawPoints(&vertex,NULL,1,
											FALSE,&yellow);

									char text[32];
									sprintf(text,"  %.2f %.2f",u,v);
									m_spDrawI->drawAlignedText(vertex,
											text,white);
								}
							}
						}
					}
				}

	private:
		sp<DrawI>			m_spDrawI;
		AsViewer			m_asViewer;

		TrianglePN<Real>	m_trianglePN;
		TrianglePower<Real>	m_trianglePower;
};

int main(int argc,char** argv)
{
	UNIT_START();

	BWORD complete=FALSE;
	U32 frames=0;
	if(argc>1)
	{
		frames=atoi(argv[1])+1;
	}

	try
	{
		sp<Master> spMaster(new Master);
		sp<Registry> spRegistry=spMaster->registry();

		Result result=spRegistry->manage("fexViewerDL");
		UNIT_TEST(successful(result));

		{
			sp<QuickViewerI> spQuickViewerI(
					spRegistry->create("QuickViewerI"));
			if(!spQuickViewerI.isValid())
			{
				feX(argv[0], "couldn't create components");
			}

			spQuickViewerI->open();

			sp<DrawI> spDrawI=spQuickViewerI->getDrawI();
			if(spDrawI.isNull())
			{
				feX(argv[0], "couldn't get draw interface");
			}

			sp<DrawMode> spDrawMode=spDrawI->drawMode();
			if(spDrawMode.isValid())
			{
				spDrawMode->setLineWidth(1.5f);
				spDrawMode->setPointSize(8.0f);
			}

			sp<MyDraw> spMyDraw(new MyDraw(spDrawI));

			spQuickViewerI->insertDrawHandler(spMyDraw);

			feLog("run %d frames\n",frames);
			spQuickViewerI->run(frames);
			feLog("run complete\n",frames);
		}

		complete=TRUE;
	}
	catch(Exception &e) { e.log(); }
	catch(...)			{ feLog("uncaught exception\n"); }

	UNIT_TEST(complete);
	UNIT_TEST(!Counted::trackerCount());
	UNIT_TRACK(3);
	UNIT_RETURN();
}
