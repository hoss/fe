/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __geometry_h__
#define __geometry_h__

#include "fe/data.h"
#include "math/math.h"

#ifdef MODULE_geometry
#define FE_GEOMETRY_PORT FE_DL_EXPORT
#else
#define FE_GEOMETRY_PORT FE_DL_IMPORT
#endif

#include "geometry/Geodesic.h"

#include "geometry/Spline.h"
#include "geometry/ConvergentSpline.h"

#include "geometry/MatrixBezier.h"
#include "geometry/MatrixSqrt.h"
#include "geometry/MatrixPower.h"
#include "geometry/TrianglePower.h"
#include "geometry/TrianglePN.h"

#include "geometry/BoundingSphere.h"

#include "geometry/RaySphereIntersect.h"
#include "geometry/RayCylinderIntersect.h"
#include "geometry/RayCurveIntersect.h"
#include "geometry/RayDiskIntersect.h"
#include "geometry/RayPlaneIntersect.h"
#include "geometry/RayTriangleIntersect.h"

#include "geometry/Noise.h"
#include "geometry/PointConnect.h"
#include "geometry/PointSphereNearest.h"
#include "geometry/PointDiskNearest.h"
#include "geometry/PointPlaneNearest.h"
#include "geometry/PointCylinderNearest.h"
#include "geometry/PointCurveNearest.h"
#include "geometry/PointTriangleNearest.h"

#include "geometry/InvKineCCD.h"

#endif // __geometry_h__
