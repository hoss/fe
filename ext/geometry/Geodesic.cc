/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <geometry/geometry.pmh>

#include <memory>
#include <algorithm>
#include <string.h>

#include "geodesic_cpp_mod/geodesic_algorithm_exact.h"

namespace fe
{
namespace ext
{

Geodesic::Mesh::Mesh(void):
	m_pMesh(NULL)
{
}

Geodesic::Mesh::~Mesh(void)
{
	clear();
}

void Geodesic::Mesh::clear(void)
{
	if(m_pMesh)
	{
		geodesic::Mesh* pGeodesicMesh=
				reinterpret_cast<geodesic::Mesh*>(m_pMesh);
		delete pGeodesicMesh;
		m_pMesh=NULL;
	}
}

void* Geodesic::Mesh::rawMesh(void)
{
	return m_pMesh;
}

void Geodesic::Mesh::populate(Array<F64>& a_rPoints,Array<U32>& a_rFaces)
{
	clear();

#if FALSE
	const U32 valueCount=a_rPoints.size();
	const U32 pointCount=valueCount/3;
	for(U32 valueIndex=0;valueIndex<valueCount;valueIndex++)
	{
		const F64 value=a_rPoints[valueIndex];
		if(FE_INVALID_SCALAR(value) || fabs(value)>16.0)
		{
			feLog("Geodesic::Mesh::populate value %d/%d %.6G\n",
					valueIndex,valueCount,value);
		}
	}

	const U32 vertexCount=a_rFaces.size();
	const U32 faceCount=vertexCount/3;
	for(U32 faceIndex=0;faceIndex<faceCount;faceIndex++)
	{
		const U32 vertexIndex=faceIndex*3;
		const U32 indexA=a_rFaces[vertexIndex];
		const U32 indexB=a_rFaces[vertexIndex+1];
		const U32 indexC=a_rFaces[vertexIndex+2];

		if(indexA>pointCount || indexB>pointCount || indexC>pointCount ||
				indexA==indexB || indexB==indexC || indexC==indexA)
		{
			feLog("Geodesic::Mesh::populate face %d/%d verts %d %d %d\n",
					faceIndex,faceCount,indexA,indexB,indexC);
		}
	}
#endif

	if(a_rPoints.size() && a_rFaces.size())
	{
		geodesic::Mesh* pGeodesicMesh=new geodesic::Mesh();
		pGeodesicMesh->initialize_mesh_data(a_rPoints,a_rFaces);

		m_pMesh=pGeodesicMesh;
	}
}

Geodesic::Geodesic(void):
	m_pAlgorithm(NULL)
{
}

Geodesic::~Geodesic(void)
{
	clear();
}

void Geodesic::clear(void)
{
	if(m_pAlgorithm)
	{
		geodesic::GeodesicAlgorithmExact* pGeodesicAlgorithm=
				reinterpret_cast<geodesic::GeodesicAlgorithmExact*>(
				m_pAlgorithm);
		delete pGeodesicAlgorithm;

		m_pAlgorithm=NULL;
	}
}

sp<Geodesic::Mesh> Geodesic::createMesh(
		Array<F64>& a_rPoints,Array<U32>& a_rFaces)
{
	clear();

	m_spMesh=sp<Geodesic::Mesh>(new Geodesic::Mesh());
	m_spMesh->populate(a_rPoints,a_rFaces);

	return m_spMesh;
}

void Geodesic::setMesh(sp<Geodesic::Mesh> a_spMesh)
{
	m_spMesh=a_spMesh;
}

sp<Geodesic::Mesh> Geodesic::mesh(void)
{
	return m_spMesh;
}

void Geodesic::setSource(I32 a_sourceFaceIndex,SpatialVector a_sourcePosition)
{
	if(m_pAlgorithm)
	{
		geodesic::GeodesicAlgorithmExact* pGeodesicAlgorithm=
				reinterpret_cast<geodesic::GeodesicAlgorithmExact*>(
				m_pAlgorithm);
		delete pGeodesicAlgorithm;

		m_pAlgorithm=NULL;
	}

	m_sourceFaceIndex=a_sourceFaceIndex;
	m_sourcePosition=a_sourcePosition;
}

void Geodesic::setMaxDistance(Real a_maxDistance)
{
	if(m_pAlgorithm)
	{
		geodesic::GeodesicAlgorithmExact* pGeodesicAlgorithm=
				reinterpret_cast<geodesic::GeodesicAlgorithmExact*>(
				m_pAlgorithm);
		delete pGeodesicAlgorithm;

		m_pAlgorithm=NULL;
	}

	m_maxDistance=a_maxDistance;
}

Real Geodesic::distanceTo(I32 a_targetFaceIndex,SpatialVector a_targetPosition)
{
	geodesic::Mesh* pGeodesicMesh=
			reinterpret_cast<geodesic::Mesh*>(m_spMesh->rawMesh());

	if(!pGeodesicMesh)
	{
		return -1.0;
	}

	geodesic::GeodesicAlgorithmExact* pGeodesicAlgorithm=
			reinterpret_cast<geodesic::GeodesicAlgorithmExact*>(m_pAlgorithm);

	if(!pGeodesicAlgorithm)
	{
		pGeodesicAlgorithm=
				new geodesic::GeodesicAlgorithmExact(pGeodesicMesh);
		m_pAlgorithm=pGeodesicAlgorithm;

		geodesic::SurfacePoint geoSource(
				&pGeodesicMesh->faces()[m_sourceFaceIndex],
				m_sourcePosition[0],m_sourcePosition[1],
				m_sourcePosition[2],geodesic::FACE);

		Array<geodesic::SurfacePoint> geoSources(1,geoSource);
		pGeodesicAlgorithm->propagate(geoSources,m_maxDistance);
	}

	geodesic::SurfacePoint geoTarget(&pGeodesicMesh->faces()[a_targetFaceIndex],
			a_targetPosition[0],a_targetPosition[1],a_targetPosition[2],
			geodesic::FACE);

	F64 distance;
//	U32 best=
			pGeodesicAlgorithm->best_source(geoTarget,distance);

	return distance;
}

} /* namespace ext */
} /* namespace fe */