/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __geometry_PointConnect_h__
#define __geometry_PointConnect_h__

namespace fe
{
namespace ext
{
/**************************************************************************//**
	@brief Delaunay Triangulation

	@ingroup geometry

	www.s-hull.org

*//***************************************************************************/
class FE_DL_EXPORT PointConnect
{
	public:

static	void	solve(Array<Vector2>& a_location,
						Array<Vector3i>& a_triangle);
};

} /* namespace ext */
} /* namespace fe */

#endif /* __geometry_PointConnect_h__ */
