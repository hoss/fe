#ifndef __Enet_EnetConnection_h__
#define __Enet_EnetConnection_h__

namespace fe
{
namespace ext
{

class FE_DL_EXPORT EnetConnection : virtual public ConnectionI
{
	public:
				EnetConnection(void);
virtual			~EnetConnection(void);

virtual	bool	connect(const bool asServer, const std::string &address,
					const short port) override;

virtual	bool	sendReliable(const std::vector<char> &message) override;
virtual	bool	sendReliable(const char *message, const int size) override;

virtual	bool	sendUnreliable(const std::vector<char> &message) override;
virtual	bool	sendUnreliable(const char *message, const int size) override;

virtual	bool	receiveBlocking(std::vector<char> &message) override;
virtual	bool	receiveNonblocking(std::vector<char> &message,
					const std::uint_fast32_t msWait) override;

virtual	bool	connected(void) override { return (m_numConnections > 0); }
virtual	bool	disconnect(void) override;

	private:
		bool	tryToConnectToServer(void);

		bool		m_initialized;
		int			m_numConnections;
		bool		m_isServer;
		ENetAddress	m_address;
		ENetHost*	m_host;
		ENetPeer*	m_peer;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __Enet_EnetConnection_h__ */
