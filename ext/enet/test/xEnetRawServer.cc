/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "enet/enetComs.pmh"

using namespace fe;
using namespace fe::ext;

int main(int argc,char** argv)
{
	UNIT_START();

	BWORD complete=FALSE;

	try
	{
		sp<Master> spMaster(new Master);
		sp<Registry> spRegistry=spMaster->registry();

		Result result=spRegistry->manage("fexEnetDL");
		UNIT_TEST(successful(result));

		{
			sp<ConnectionI> spConnectionI(spRegistry->create("ConnectionI"));
			if(spConnectionI.isNull())
			{
				feX(argv[0], "couldn't create components");
			}

			bool asServer = true;
			spConnectionI->connect(asServer,"",7890);
			String message(
					"Hello world! Goodbye World!"
					" Ok, I'm back again. That is now 64.");

			const I32 count=1000;
			for(I32 index=0;index<count;index++)
			{
				spConnectionI->sendReliable(message.c_str(), message.length()+1);
				nanoSpin(1e4);
			}

			//* wait for "done"
			std::vector<char> response;
			spConnectionI->receiveBlocking(response);

			String resolution = (const FESTRING_I8*)response.data();
			feLog("received \"%s\"\n",resolution.c_str());
			UNIT_TEST(resolution=="done");
		}

		complete=TRUE;
	}
	catch(Exception &e) { e.log(); }
	catch(...)			{ feLog("uncaught exception\n"); }

	UNIT_TEST(complete);
	UNIT_TEST(!Counted::trackerCount());
	UNIT_TRACK(4);
	UNIT_RETURN();
}
