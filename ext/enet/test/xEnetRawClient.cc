/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "enet/enetComs.pmh"

using namespace fe;
using namespace fe::ext;

int main(int argc,char** argv)
{
	UNIT_START();

	BWORD complete=FALSE;

	try
	{
		sp<Master> spMaster(new Master);
		sp<Registry> spRegistry=spMaster->registry();

		Result result=spRegistry->manage("fexEnetDL");
		UNIT_TEST(successful(result));

		{
			sp<ConnectionI> spConnectionI(spRegistry->create("ConnectionI"));
			if(spConnectionI.isNull())
			{
				feX(argv[0], "couldn't create components");
			}

			bool asServer = false;
			spConnectionI->connect(asServer,"127.0.0.1",7890);

			std::vector<char> message;

			I32 count=0;
			while(count<1000)
			{
				spConnectionI->receiveBlocking(message);

				if(!(++count%100))
				{
					feLog("count %d\n",count);
				}
			}

			String done = "done";
			spConnectionI->sendReliable(done.c_str(), done.length()+1);
		}

		complete=TRUE;
	}
	catch(Exception &e) { e.log(); }
	catch(...)			{ feLog("uncaught exception\n"); }

	UNIT_TEST(complete);
	UNIT_TEST(!Counted::trackerCount());
	UNIT_TRACK(3);
	UNIT_RETURN();
}
