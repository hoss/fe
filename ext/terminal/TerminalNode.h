/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __terminal_TerminalNode_h__
#define __terminal_TerminalNode_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Terminal control of an OperatorSurfaceI

	@ingroup terminal
*//***************************************************************************/
class FE_DL_EXPORT TerminalNode:
	public MetaPlugin
{
	public:

					TerminalNode(void);
virtual				~TerminalNode(void);

		void		setupOperator(sp<Scope> a_spScope,
							String a_implementation,String a_name);

		void		setDrawOutput(sp<DrawI> a_spDrawI)
					{
						m_spDrawOutput=a_spDrawI;
						m_aDrawOutput(m_cookSignal)=m_spDrawOutput;
					}

		void		clear(void);

		BWORD		cook(Real a_frame);

		sp<Catalog>	catalog(void)	{ return m_spOperatorSurfaceI; }

	private:

		void		updateCatalog();

		sp<Scope>	m_spScope;

};

} /* namespace ext */
} /* namespace fe */

#endif /* __terminal_TerminalNode_h__ */
