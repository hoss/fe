/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <terminal/terminal.pmh>

#define	FE_TND_COOK_DEBUG		FALSE
#define	FE_TND_COOK_TICKER		FALSE
#define	FE_TND_PRINT_MESSAGES	TRUE

namespace fe
{
namespace ext
{

TerminalNode::TerminalNode(void)
{
#if	FE_TND_COOK_DEBUG
	feLog("TerminalNode::TerminalNode %p\n",this);
#endif

#if FE_COUNTED_TRACK
	Counted::registerRegion(this,sizeof(TerminalNode),"TerminalNode");
#endif
}

TerminalNode::~TerminalNode(void)
{
#if	FE_TND_COOK_DEBUG
	feLog("TerminalNode::~TerminalNode %p\n",this);
#endif

	clear();

#if FE_COUNTED_TRACK
	Counted::deregisterRegion(this);
#endif
}

void TerminalNode::clear(void)
{
	m_spScope=NULL;

	clearReferences();
}

void TerminalNode::setupOperator(sp<Scope> a_spScope,
	String a_implementation,String a_name)
{
//	feLog("TerminalNode::setupOperator spScope %p \"%s\" \"%s\"\n",
//			m_spScope.raw(),a_implementation.c_str(),a_name.c_str());

	m_spScope=a_spScope;

	if(a_spScope.isNull())
	{
		feLog("TerminalNode::setupOperator NULL Scope\n",this);
		return;
	}

	sp<Registry> spRegistry=m_spScope->registry();

	m_spDrawOutput=spRegistry->create("DrawI.TerminalDraw");
	if(m_spDrawOutput.isValid())
	{
		m_spDrawOutput->setName("DrawOutput");
	}

	setupScope(m_spScope);

	m_spOperatorSurfaceI=spRegistry->create(
			"OperatorSurfaceI."+a_implementation);

	if(m_spOperatorSurfaceI.isValid())
	{
		m_spOperatorSurfaceI->setName(a_name);
		insertHandler(m_spOperatorSurfaceI);

		updateCatalog();
	}
}

void TerminalNode::updateCatalog(void)
{
	m_surfaceInputArray.clear();

	sp<Catalog> spCatalog=m_spOperatorSurfaceI;
	if(spCatalog.isNull())
	{
		return;
	}

	//* scan for inputs
	Array<String> keys;
	spCatalog->catalogKeys(keys);

	String firstInput;

	const U32 number=keys.size();
	for(U32 m=0;m<number;m++)
	{
		const String keyname=keys[m];
		if(keyname.empty())
		{
			continue;
		}

		Instance instance;
		bool success=spCatalog->catalogLookup(keyname,instance);
		if(!success)
		{
			continue;
		}

		if(instance.is< sp<Component> >())
		{
#if	FE_TND_COOK_DEBUG
			feLog("TerminalNode::updateCatalog \"%s\" \"%s\"\n",
					m_spOperatorSurfaceI->name().c_str(),
					keyname.c_str());
#endif

			const String implementation=
					spCatalog->catalogOrDefault<String>(keyname,
					"implementation","SurfaceAccessibleI");

			if(implementation.dotMatch("SurfaceAccessibleI"))
			{
				const String io=spCatalog->catalogOrDefault<String>(
						keyname,"IO","input");

				sp<Component>& rspComponent=instance.cast< sp<Component> >();
				sp<SurfaceAccessibleI> spSurfaceAccessibleI=rspComponent;

				if(io=="input")
				{
					//* WARNING expanding catalog can trash other threads
					I32 channel=m_surfaceInputArray.size();
					if(channel<4 && spCatalog->catalogOrDefault<I32>(
									keyname,"channel",-1)!=channel)
					{
						spCatalog->catalog<I32>(keyname,"channel")=channel;

						String lookup;
						lookup.sPrintf("input:%d",channel);
						spCatalog->catalog<String>(lookup)=keyname;
					}

					m_surfaceInputArray.push_back(spSurfaceAccessibleI);

					if(firstInput.empty())
					{
						firstInput=keyname;
					}
				}
				else
				{
					if(spCatalog->catalog<String>("output:0")!=keyname)
					{
						spCatalog->catalog<String>("output:0")=keyname;
					}
				}
			}
		}
	}

	FEASSERT(!firstInput.empty());

	if(spCatalog->catalog<String>("output:0")=="")
	{
		spCatalog->catalog<String>("output:0")="Output Surface";
		spCatalog->catalog<String>("Output Surface","copy")=firstInput;
		spCatalog->catalog<String>("Output Surface","IO")="output";
	}

	const String lookup=spCatalog->catalog<String>("output:0");
	m_spSurfaceOutput=spCatalog->catalog< sp<Component> >(lookup);

	sp<TerminalDraw> spTerminalDraw=m_spDrawOutput;
	if(spTerminalDraw.isValid())
	{
		spTerminalDraw->setSurfaceAccessible(m_spSurfaceOutput);
		spCatalog->catalog< sp<Component> >("")=spTerminalDraw;
	}
}

BWORD TerminalNode::cook(Real a_frame)
{
#if	FE_TND_COOK_DEBUG
	feLog("TerminalNode::cook %.6G\n",a_frame);
#endif

#if FE_TND_COOK_TICKER
	const U32 tick0=fe::systemTick();
	U32 tick1=tick0;
#endif

	BWORD success=TRUE;

	const double time=0.0;

	const Real startFrame=0.0;
	const Real endFrame=0.0;

	if(m_spOperatorSurfaceI.isValid())
	{
		updateCatalog();

		FEASSERT(m_spSurfaceOutput.isValid());

		sp<Catalog> spCatalog=m_spOperatorSurfaceI;
		if(spCatalog.isNull())
		{
			return FALSE;
		}

		m_aFrame(m_cookSignal)=a_frame;
		m_aStartFrame(m_cookSignal)=startFrame;
		m_aEndFrame(m_cookSignal)=endFrame;
		m_aTime(m_cookSignal)=time;
		m_aSurfaceOutput(m_cookSignal)=m_spSurfaceOutput;

		if(m_spDrawGuideCached.isValid())
		{
			m_spDrawGuideCached->clearInput();
		}

#if FE_TND_COOK_TICKER
		tick1=fe::systemTick();
#endif

		try
		{
			m_spSignalerI->signal(m_cookSignal);
		}
		catch(const Exception& e)
		{
			spCatalog->catalog<String>("error")+=
					"FE EXCEPTION: "+print(e);
			success=FALSE;
		}
		catch(const std::exception& std_e)
		{
			spCatalog->catalog<String>("error")+=
					"stdlib EXCEPTION: "+String(std_e.what());
			success=FALSE;
		}
#ifdef FE_BOOST_EXCEPTIONS
		catch(const boost::exception& boost_e)
		{
			spCatalog->catalog<String>("error")+=
					"boost EXCEPTION: "+
					String(boost::diagnostic_information(boost_e).c_str());
			success=FALSE;
		}
#endif
		catch(...)
		{
			spCatalog->catalog<String>("error")+=
					"UNRECOGNIZED EXCEPTION";
			success=FALSE;
		}

		String messageString=spCatalog->catalogOrDefault<String>("message","");
		if(!messageString.empty())
		{
			spCatalog->catalog<String>("message")="";
#if	FE_TND_PRINT_MESSAGES
			feLog("TerminalNode::cook MESSAGE '%s'\n",
					messageString.c_str());
#endif
		}

		String warningString=spCatalog->catalogOrDefault<String>("warning","");
		if(!warningString.empty())
		{
			spCatalog->catalog<String>("warning")="";
#if	FE_TND_PRINT_MESSAGES
			feLog("[33mTerminalNode::cook WARNING '%s'[0m\n",
					warningString.c_str());
#endif
		}

		String errorString=spCatalog->catalogOrDefault<String>("error","");
		if(!errorString.empty())
		{
			spCatalog->catalog<String>("error")="";
#if	FE_TND_PRINT_MESSAGES
			feLog("[31mTerminalNode::cook ERROR '%s'[0m\n",
					errorString.c_str());
#endif
			success=FALSE;
		}

//		sp<SurfaceAccessibleI>(m_spSurfaceOutput)->save(
//				spCatalog->name()+".rg");
	}

	m_lastTime=time;

#if	FE_TND_COOK_DEBUG
	feLog("TerminalNode::cook done\n");
#endif

#if FE_TND_COOK_TICKER
	const U32 tick2=fe::systemTick();

	const U32 tickDiff01=fe::SystemTicker::tickDifference(tick0,tick1);
	const U32 tickDiff12=fe::SystemTicker::tickDifference(tick1,tick2);

	const fe::Real msPerTick=1e-3*fe::SystemTicker::microsecondsPerTick();
	const fe::Real ms01=tickDiff01*msPerTick;
	const fe::Real ms12=tickDiff12*msPerTick;

	feLog("TerminalNode::cook map catalog %.2f ms\n",ms01);
	feLog("TerminalNode::cook map signal  %.2f ms\n",ms12);
#endif

	return success;
}

} /* namespace ext */
} /* namespace fe */
