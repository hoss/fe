/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <terminal/terminal.pmh>

namespace fe
{
namespace ext
{

void TerminalDraw::drawPoints(const SpatialVector *vertex,
	const SpatialVector *normal,U32 vertices,
	BWORD multicolor,const Color *color,
	sp<DrawBufferI> spDrawBuffer)
{
	if(m_spSurfaceAccessibleI.isNull())
	{
		feLog("TerminalDraw::drawPoints SurfaceAccessibleI is NULL\n");
		return;
	}

	//* HACK
//	return;

	//* TODO add to group
	sp<DrawMode> spDrawMode=drawMode();
	String group;
	if(spDrawMode.isValid())
	{
		group=spDrawMode->group();
	}

	if(m_spOutputPoint.isNull())
	{
		m_spOutputPoint=m_spSurfaceAccessibleI->accessor(
				SurfaceAccessibleI::e_point,SurfaceAccessibleI::e_position);
		if(m_spOutputPoint.isNull())
		{
			feLog("TerminalDraw::drawPoints points not accessible\n");
			return;
		}
	}

	if(normal && m_spOutputNormal.isNull())
	{
		m_spOutputNormal=m_spSurfaceAccessibleI->accessor(
				SurfaceAccessibleI::e_point,SurfaceAccessibleI::e_normal);
		if(m_spOutputNormal.isNull())
		{
			feLog("TerminalDraw::drawPoints normals not accessible\n");
			return;
		}
	}

	const I32 existingPointCount=m_spOutputPoint->count();

	m_spOutputPoint->append(vertices);

	for(U32 index=0;index<vertices;index++)
	{
		m_spOutputPoint->set(existingPointCount+index,vertex[index]);
	}

	if(normal && m_spOutputNormal.isValid())
	{
		for(U32 index=0;index<vertices;index++)
		{
			m_spOutputNormal->set(existingPointCount+index,normal[index]);
		}
	}
}

void TerminalDraw::drawLines(const SpatialVector *vertex,
	const SpatialVector *normal,U32 vertices,
	StripMode strip,BWORD multicolor,const Color *color,
	BWORD multiradius,const Real *radius,
	const Vector3i *element,U32 elementCount,
	sp<DrawBufferI> spDrawBuffer)
{
	drawPolygons(vertex,normal,vertices,strip,multicolor,color,TRUE);
}

void TerminalDraw::drawCurve(const SpatialVector *vertex,
	const SpatialVector *normal,const Real *radius,U32 vertices,
	BWORD multicolor,const Color *color)
{
	if(radius==NULL)
	{
		drawLines(vertex,normal,vertices,DrawI::e_strip,
				multicolor,color,FALSE,NULL,NULL,0,sp<DrawBufferI>(NULL));
		return;
	}

	//* append line segments with radius attribute

	if(m_spSurfaceAccessibleI.isNull())
	{
		feLog("TerminalDraw::drawCurve SurfaceAccessibleI is NULL\n");
		return;
	}

	//* TODO add to group
	sp<DrawMode> spDrawMode=drawMode();
	String group;
	if(spDrawMode.isValid())
	{
		group=spDrawMode->group();
	}

	if(m_spOutputPoint.isNull())
	{
		m_spOutputPoint=m_spSurfaceAccessibleI->accessor(
				SurfaceAccessibleI::e_point,SurfaceAccessibleI::e_position);
		if(m_spOutputPoint.isNull())
		{
			feLog("TerminalDraw::drawCurve points not accessible\n");
			return;
		}
	}

	if(normal && m_spOutputNormal.isNull())
	{
		m_spOutputNormal=m_spSurfaceAccessibleI->accessor(
				SurfaceAccessibleI::e_point,SurfaceAccessibleI::e_normal);
		if(m_spOutputNormal.isNull())
		{
			feLog("TerminalDraw::drawCurve normals not accessible\n");
			return;
		}
	}

	if(radius && m_spOutputRadius.isNull())
	{
		m_spOutputRadius=m_spSurfaceAccessibleI->accessor(
				SurfaceAccessibleI::e_point,"radius");
		if(m_spOutputRadius.isNull())
		{
			feLog("TerminalDraw::drawCurve radius not accessible\n");
			return;
		}
	}

	if(m_spOutputVertices.isNull())
	{
		m_spOutputVertices=m_spSurfaceAccessibleI->accessor(
				SurfaceAccessibleI::e_primitive,SurfaceAccessibleI::e_vertices);
		if(m_spOutputVertices.isNull())
		{
			feLog("TerminalDraw::drawCurve vertices not accessible\n");
			return;
		}
	}

	const I32 existingPointCount=m_spOutputPoint->count();
	const I32 primitiveIndex=m_spOutputVertices->count();

	//* TODO colors

	Array< Array<I32> > primVerts(1);

	Array<I32>& rVerts=primVerts[0];
	rVerts.resize(vertices);

	for(U32 index=0;index<vertices;index++)
	{
		rVerts[index]=existingPointCount+index;
	}

	m_spOutputVertices->append(primVerts);
	m_spOutputPoint->append(vertices);

	for(U32 index=0;index<vertices;index++)
	{
		m_spOutputPoint->set(existingPointCount+index,vertex[index]);
	}

	if(normal && m_spOutputNormal.isValid())
	{
		for(U32 index=0;index<vertices;index++)
		{
			m_spOutputNormal->set(existingPointCount+index,normal[index]);
		}
	}

	if(radius && m_spOutputRadius.isValid())
	{
		for(U32 index=0;index<vertices;index++)
		{
			m_spOutputRadius->set(existingPointCount+index,radius[index]);
		}
	}

	if(m_spOutputProperties.isNull())
	{
		m_spOutputProperties=m_spSurfaceAccessibleI->accessor(
				SurfaceAccessibleI::e_primitive,
				SurfaceAccessibleI::e_properties);
	}
	if(m_spOutputProperties.isValid())
	{
		m_spOutputProperties->set(primitiveIndex,
				SurfaceAccessibleI::e_openCurve,I32(1));
	}
}

void TerminalDraw::drawTriangles(const SpatialVector *vertex,
	const SpatialVector *normal,const Vector2 *texture,U32 vertices,
	StripMode strip,BWORD multicolor,const Color* color,
	const Array<I32>* vertexMap,
	const Array<I32>* hullPointMap,const Array<Vector4i>* hullFacePoint,
	sp<DrawBufferI> spDrawBuffer)
{
	//* TODO other strip modes

	if(strip==DrawI::e_discrete)
	{
		for(U32 m=0;m<vertices;m+=3)
		{
			if(color && multicolor)
			{
				drawPolygons(&vertex[m],&normal[m],3,DrawI::e_discrete,
						multicolor,&color[m],FALSE);
			}
			else
			{
				drawPolygons(&vertex[m],&normal[m],3,DrawI::e_discrete,
						multicolor,color,FALSE);
			}
		}
	}
	else if(strip==DrawI::e_strip)
	{
#if FALSE
		for(U32 m=0;m<vertices-2;m++)
		{
			if(color && multicolor)
			{
				drawPolygons(&vertex[m],&normal[m],3,DrawI::e_discrete,
						multicolor,&color[m],FALSE);
			}
			else
			{
				drawPolygons(&vertex[m],&normal[m],3,DrawI::e_discrete,
						multicolor,color,FALSE);
			}
		}
#else
		drawPolygons(vertex,normal,vertices,DrawI::e_strip,
				multicolor,color,FALSE);
#endif
	}
}

void TerminalDraw::drawPolygons(const SpatialVector *vertex,
	const SpatialVector *normal,U32 vertices,
	StripMode strip,BWORD multicolor,const Color *color,BWORD openPoly)
{
	if(m_spSurfaceAccessibleI.isNull())
	{
		feLog("TerminalDraw::drawPolygons SurfaceAccessibleI is NULL\n");
		return;
	}

//	feLog("TerminalDraw::drawTriangles"
//			" vertices %d strip %d multicolor %d color %p openPoly %d\n",
//			vertices, strip, multicolor, color, openPoly);

	//* TODO add to group
	sp<DrawMode> spDrawMode=drawMode();
	String group;
	if(spDrawMode.isValid())
	{
		group=spDrawMode->group();
	}

	if(m_spOutputPoint.isNull())
	{
		m_spOutputPoint=m_spSurfaceAccessibleI->accessor(
				SurfaceAccessibleI::e_point,SurfaceAccessibleI::e_position);
		if(m_spOutputPoint.isNull())
		{
			feLog("TerminalDraw::drawPolygons points not accessible\n");
			return;
		}
	}

	if(normal && m_spOutputNormal.isNull())
	{
		m_spOutputNormal=m_spSurfaceAccessibleI->accessor(
				SurfaceAccessibleI::e_point,SurfaceAccessibleI::e_normal);
		if(m_spOutputNormal.isNull())
		{
			feLog("TerminalDraw::drawPolygons normals not accessible\n");
			return;
		}
	}

	if(color && m_spOutputColor.isNull())
	{
		m_spOutputColor=m_spSurfaceAccessibleI->accessor(
				SurfaceAccessibleI::e_point,SurfaceAccessibleI::e_color);
		if(m_spOutputColor.isNull())
		{
			feLog("TerminalDraw::drawPolygons colors not accessible\n");
			return;
		}
	}

	if(m_spOutputVertices.isNull())
	{
		m_spOutputVertices=m_spSurfaceAccessibleI->accessor(
				SurfaceAccessibleI::e_primitive,SurfaceAccessibleI::e_vertices);
		if(m_spOutputVertices.isNull())
		{
			feLog("TerminalDraw::drawPolygons vertices not accessible\n");
			return;
		}
	}

	const I32 existingPointCount=m_spOutputPoint->count();
	const I32 primitiveIndex=m_spOutputVertices->count();

	Array< Array<I32> > primVerts(1);

	if(!openPoly && strip==DrawI::e_strip)
	{
		//* triangle strip (converted to discrete triangles)

		const I32 addCount=vertices-2;
		I32 startIndex=existingPointCount;

		primVerts.resize(addCount);

		for(I32 addIndex=0;addIndex<addCount;addIndex++)
		{
			Array<I32>& rVerts=primVerts[addIndex];
			rVerts.resize(3);

			const I32 swap=(addIndex%2);

			rVerts[0]=startIndex;
			rVerts[1]=startIndex+1+swap;
			rVerts[2]=startIndex+2-swap;

			startIndex++;
		}
	}
	else if(openPoly && strip==DrawI::e_discrete)
	{
		//* discrete line segments

		const I32 addCount=vertices/2;
		I32 startIndex=existingPointCount;

		primVerts.resize(addCount);

		for(I32 addIndex=0;addIndex<addCount;addIndex++)
		{
			Array<I32>& rVerts=primVerts[addIndex];
			rVerts.resize(2);

			rVerts[0]=startIndex++;
			rVerts[1]=startIndex++;
		}
	}
	else
	{
		Array<I32>& rVerts=primVerts[0];
		rVerts.resize(vertices);

		for(U32 index=0;index<vertices;index++)
		{
			rVerts[index]=existingPointCount+index;
		}
	}

	m_spOutputPoint->append(vertices);
	m_spOutputVertices->append(primVerts);

	for(U32 index=0;index<vertices;index++)
	{
		m_spOutputPoint->set(existingPointCount+index,vertex[index]);
	}

	if(normal && m_spOutputNormal.isValid())
	{
		for(U32 index=0;index<vertices;index++)
		{
			m_spOutputNormal->set(existingPointCount+index,normal[index]);
		}
	}

	if(color && m_spOutputColor.isValid())
	{
		for(U32 index=0;index<vertices;index++)
		{
			m_spOutputColor->set(existingPointCount+index,
					color[multicolor? index: 0]);
		}
	}

	if(openPoly)
	{
		if(m_spOutputProperties.isNull())
		{
			m_spOutputProperties=m_spSurfaceAccessibleI->accessor(
					SurfaceAccessibleI::e_primitive,
					SurfaceAccessibleI::e_properties);
		}
		if(m_spOutputProperties.isValid())
		{
			m_spOutputProperties->set(primitiveIndex,
					SurfaceAccessibleI::e_openCurve,I32(1));
		}
	}
}

} /* namespace ext */
} /* namespace fe */
