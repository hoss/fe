#! /usr/bin/python3

import sys
sys.path.append("ext/terminal")

import pyfe.context

fe = pyfe.context.Context()

bindOp = fe.create("SurfaceBindOp")
bindOp.["Bindings"] = 8

wrapOp = fe.create("SurfaceWrapOp")
wrapOp.["Distance Bias"] = 0.0

inputSurface = fe.load("../media/model/sleeve.geo", name = "sleeve")
driverSurface0 = fe.load("../media/model/arm0.geo", name = "arm0" )
driverSurface1 = fe.load("../media/model/arm1.geo", name = "arm1" )

boundSurface = bindOp.operate(inputSurface, driverSurface0, name = "bound")
wrappedSurface = wrapOp.operate(boundSurface, driverSurface0, driverSurface1, name = "wrapped")

wrappedSurface.save("test/wrapped.geo")
