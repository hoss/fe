/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdarg.h>
#include <dlfcn.h>

#include <string>
#include <fstream>

extern "C"
{

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wreturn-type-c-linkage"

std::string request(std::string a_request)
{
	printf("[35m\"%s\"[0m\n",a_request.c_str());

	return "unknown request";
}

#pragma GCC diagnostic pop

}

void terminal_send(int (*a_fpTerminal)(int,char*,const char*),
	std::string a_commandline)
{
	printf("\n[36m%% %s[0m\n",a_commandline.c_str());

	const int bytes=256;
	char output[bytes]="";
//	const int integer=
			a_fpTerminal(bytes,output,a_commandline.c_str());

	printf("[34m\"%s\"[0m\n",output);
}

int main(int argc,char** argv)
{
	if(argc<2)
	{
		fprintf(stderr,"Usage: %s <dso> <script>\n", argv[0]);
		return 1;
	}

	const std::string filename=argv[1];
	const std::string script=argc>2? argv[2]: "";

	fprintf(stderr,"dso: \"%s\"\n",filename.c_str());
	fprintf(stderr,"script: \"%s\"\n",script.c_str());

	void* pHandle=dlopen(filename.c_str(),RTLD_LAZY);
	if(!pHandle)
	{
		fprintf(stderr,"failed to open \"%s\"\n  %s\n",
				filename.c_str(),dlerror());
		return 1;
	}

	const std::string symbol="fe_terminal_execute";
	const void* pAddress=dlsym(pHandle,symbol.c_str());
	if(!pAddress)
	{
		fprintf(stderr,"failed to find \"%s\" in \"%s\"\n  %s\n",
				symbol.c_str(),filename.c_str(),dlerror());
		return 1;
	}

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wconditionally-supported"

	int (*fpTerminal)(int,char*,const char*);
	fpTerminal=(int(*)(int,char*,const char*))pAddress;

#pragma GCC diagnostic pop

	char commandline[128];

	sprintf(commandline,"set callback %p",request);
	terminal_send(fpTerminal,commandline);

	std::ifstream in(script.c_str());
	std::string line;
	while(std::getline(in, line))
	{
		if(line[0] && line[0]!='#')
		{
			terminal_send(fpTerminal,line);
		}
	}

	printf("\n");

	if(dlclose(pHandle))
	{
		fprintf(stderr,"failed to close \"%s\"\n  %s\n",
				filename.c_str(),dlerror());
		return 1;
	}
}
