#! /usr/bin/python3

import sys
sys.path.append("ext/terminal")

import pyfe.context

fe = pyfe.context.Context()
fe.manage("fexOperatorDL")

source = fe.load("../media/model/torus.geo")

normalOp = fe.create("SurfaceNormalOp")
normaled = normalOp.operate(source)
normalOp.release()
source.release()

bloatOp = fe.create("BloatOp")
bloatOp.["Bloat"] = 0.5
bloated = bloatOp.operate(normaled)
bloatOp.release()
normaled.release()

bloated.save("test/bloated.geo")
bloated.release()

print "<end of script>"
