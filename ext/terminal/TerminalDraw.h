/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __terminal_TerminalDraw_h__
#define __terminal_TerminalDraw_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
    @brief Draw to terminal buffer

	@ingroup terminal
*//***************************************************************************/
class FE_DL_EXPORT TerminalDraw:
	public DrawCommon,
	public CastableAs<TerminalDraw>
{
	public:
					TerminalDraw(void)										{}
virtual				~TerminalDraw(void)										{}

					//* as DrawI

					using DrawCommon::drawPoints;

virtual void		drawPoints(const SpatialVector *vertex,
							const SpatialVector *normal,U32 vertices,
							BWORD multicolor,const Color *color,
							sp<DrawBufferI> spDrawBuffer);

					using DrawCommon::drawLines;

virtual	void		drawLines(const SpatialVector *vertex,
							const SpatialVector *normal,U32 vertices,
							StripMode strip,
							BWORD multicolor,const Color *color,
							BWORD multiradius,const Real *radius,
							const Vector3i *element,U32 elementCount,
							sp<DrawBufferI> spDrawBuffer);

virtual	void		drawCurve(const SpatialVector *vertex,
							const SpatialVector *normal,
							const Real *radius,U32 vertices,
							BWORD multicolor,const Color *color);

					using DrawCommon::drawTriangles;

virtual void		drawTriangles(const SpatialVector *vertex,
							const SpatialVector *normal,
							const Vector2 *texture,U32 vertices,
							StripMode strip,BWORD multicolor,
							const Color* color,
							const Array<I32>* vertexMap,
							const Array<I32>* hullPointMap,
							const Array<Vector4i>* hullFacePoint,
							sp<DrawBufferI> spDrawBuffer);

					//* Terminal specific

		void		setSurfaceAccessible(
							sp<SurfaceAccessibleI> a_spSurfaceAccessibleI)
					{
						m_spSurfaceAccessibleI=a_spSurfaceAccessibleI;
						m_spOutputPoint=NULL;
						m_spOutputNormal=NULL;
						m_spOutputColor=NULL;
						m_spOutputRadius=NULL;
						m_spOutputVertices=NULL;
						m_spOutputProperties=NULL;
					}

		sp<SurfaceAccessibleI>	surfaceAccessible(void)
					{	return m_spSurfaceAccessibleI; }

	private:
		void		drawPolygons(const SpatialVector *vertex,
							const SpatialVector *normal,U32 vertices,
							StripMode strip,BWORD multicolor,
							const Color *color,BWORD openPoly);

		sp<SurfaceAccessibleI>	m_spSurfaceAccessibleI;

		sp<SurfaceAccessorI>	m_spOutputPoint;
		sp<SurfaceAccessorI>	m_spOutputNormal;
		sp<SurfaceAccessorI>	m_spOutputColor;
		sp<SurfaceAccessorI>	m_spOutputRadius;
		sp<SurfaceAccessorI>	m_spOutputVertices;
		sp<SurfaceAccessorI>	m_spOutputProperties;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __terminal_TerminalDraw_h__ */
