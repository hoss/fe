/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __moa_FrequencyResponseSystem_h__
#define __moa_FrequencyResponseSystem_h__

namespace fe
{
namespace ext
{

/// @brief Sweep through a range multiple times.
class FrequencyResponseSystem :
	virtual public SystemI,
	public Initialize<FrequencyResponseSystem>
{
	public:
				FrequencyResponseSystem(void);
virtual			~FrequencyResponseSystem(void);
		void	initialize(void);

virtual	void	start(const t_note_id &a_note_id);

virtual	void	connectOrchestrator(sp<OrchestratorI> a_spOrchestrator);


	private:
		sp<OrchestratorI>		m_spOrchestrator;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __moa_FrequencyResponseSystem_h__ */


