# MOA Canonical Addressing {#CanonicalGuide}

## Introduction

The purpose of MOA Canonical Addressing is to locate state within a MOA data
using a string.  A common use case is to get and set values within a dataset
from a scripting language.

## Example (locating a value on a tire)

This example is taken from a unit test (xTireRig.exe) that simulates tires on a
simulated "tire test rig".

    "base_rig.mnt[0].tire:force[1]"
    
  * Fields are delimited by "." (period)
  * First field is often input the entire dataset of Records
  * Final field is accessed as an attribute
  * The remaining fields are processed by a Canonical instance

This example string breaks up as follows:

NameFilter  | BondOp and IndexFilter | attribute (with index lookup)
------------|------------------------|-----------------------------
base_rig    | mnt[0]                 | tire:force[1]

  
Put simply, this address returns lateral force (Fy) for the first tire mounted
to the tire test rig.

In more detail, per field:

### base_rig

  * base\_rig matches a Record of that name (per AsNamed)
  * processed by Canonical via NameFilter
  * input: all records in dataset
  * output: Record of name "base_rig" (the tire test rig)
  
In RG file representation, the output Record might be something like
    
    RECORD * rig
        name "base_rig"


### mnt[0]
  * mnt matches Record which:
    * has a "mount:rig" attribute with a value of "base_rig"
    * has a "mount:tire" attribute
  * input: Record for the tire test rig
  * output: Record for the mounted tire
  * processed by Canonical via BondOp
  * [0] returns the first of matching Records

In RG file representation, the mount Record might be something like

    RECORD * mount
        mount:rig "base_rig"
        mount:tire "BT tire"

In RG file representation, the tire Record might be something like

    RECORD * tire
        name "BT tire"
        tire:radius 0.21
        tire:width 0.25
        tire:model "Brush Default"

### tire:force[1]
  * input: first Record from previous Filter, in this case, the mounted tire
  * output: a Real, in this case Fy
  * processed by RealAccessor via CanonicalRealSet and CanonicalRealGet
  * tire:force is a vector3 attribute, return the real in index 1, whihc is Fy

In RG file representation, the tire Record, live, might be something like

    RECORD tire9 tire
        name "BT tire"
        tire:model "Brush Default"
        tire:radius 0.21
        tire:width 0.25
        tire:force "0.00102577 1411.96 780.692"
        tire:moment "292.979 -0.000212844 0"
        spc:transform "1 0 0 0 0 1 0 0.000155239 0 0 1 -0.0125022"
        tire:velocity "-116.943 -6.70607 9.70792e-06"
        tire:angular_velocity -556.873
        tire:contact_radius 0.207498
        tire:inclination 0

In which case the returned value would be: ``1411.96``


## Reference

  * A period, ".", is used as a field separator.
  * The final field is considered an attibute.  All other fields are considered (specifying) operators.
  * State (often Real based) accessing of Record attributes, on that final field.
  * All other fields are for processing a Record Vector in steps (via fe::ext::Canonical operators) 
  * There are two types of operators.  Both have a Record vector as input and output.
    * Filters: have an optional "argument" (within brackets), and usually "filter" the vector (reduce Records).
    * Ops: have a "name" themselves, and often change, or even replace, the vector.


### Vector of Records filtering/processing

fe::ext::Canonical processes a vector of Records at each steps

Takes as input a vector of Records and outputs a vector of Records, with arguments.

#### BondOp

Connect (bond) Records, such that if a Record has a given specific name, and
another Record has a given specific name, a keyword can be used in the address
to specify this relation.

The names are themselves specified in a Record as values of attributes.  These
attribute names are themselves specified in the BondOp creation.

Here is an example of adding the BondOp to a Canonical processor:

    spCanonical->addBond("mnt", "mount:rig", "mount:tire");

The above addBond() call would configure the Canonical (in the above tire
example) to be able to correctly process the "mnt" in the example address:

    "base_rig.mnt[0].tire:force[1]"


#### NameFilter

Filters a Record vector to only Records with the name specified by the field.

A Record name is per AsNamed, meaning a string attribute "name".

#### IndexFilter

Filters a Record vector to a single Record, based on index.
 
Note that the "[0]" part of "mnt[0]" is actually processed by IndexFilter.
Note too that the following address is also valid, and equivalent:

    "base_rig.mnt.tire:force[1]"

However, if one wanted to get to a second tire mounted to the rig, in the case
where more than one tire is mounted, using IndexFilter is necessary:

    "base_rig.mnt[1].tire:force[1]"

#### RecordGroupOp

Add all records in a given RecordGroup to the Record vector.

#### AccessorSetOp

Filters a Record vector to only Records that match a given AccessorSet.

 
### State accessing of Record attributes

The final field in an address is typically an attribute name, possibly with an
index.

This is provided by RealAccessor, which is specialized to return a Real from
Real or Vector (of Real) attributes.


