/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __moa_moaAS_h__
#define __moa_moaAS_h__

namespace fe
{
namespace ext
{

/// @brief
class AsComposer
	: public AsNamed, public Initialize<AsComposer>
{
	public:
		AsConstruct(AsComposer);
		void initialize(void)
		{
			add(frequency,	FE_USE("composer:frequency"));
		}
		Accessor<t_moa_real>				frequency;
};

/// @brief
class AsMultiSweep
	: public AsComposer, public Initialize<AsMultiSweep>
{
	public:
		AsConstruct(AsMultiSweep);
		void initialize(void)
		{
			add(step,				FE_USE("composer:step"));
			add(outer,				FE_USE("composer:outer"));
			add(inner,				FE_USE("composer:inner"));
			add(linger,				FE_USE("composer:linger"));
			add(runup,				FE_USE("composer:runup"));
		}

		Accessor<String>	step;
		Accessor< Array<String> >		outer;
		Accessor< Array<String> >		inner;
		Accessor<t_moa_real>	linger;
		Accessor<t_moa_real>	runup;
};

class AsSimulatorLoop
	: public AsComposer, public Initialize<AsSimulatorLoop>
{
	public:
		AsConstruct(AsSimulatorLoop);
		void initialize(void)
		{
			add(linger,				FE_USE("composer:linger"));
			add(duration,			FE_USE("loop:duration"));
		}

		Accessor<t_moa_real>	linger;
		Accessor<t_moa_real>	duration;
};


class AsEvaluateAction
	: public AccessorSet, public Initialize<AsEvaluateAction>
{
	public:
		AsConstruct(AsEvaluateAction);
		void initialize(void)
		{
			add(time,				FE_USE("time:time"));
			add(action,				FE_USE("event:action"));
		}
		Accessor<t_moa_real>		time;
		Accessor<String>		action;
};

/// @brief
class AsFrequencyResponse
	: public AsComposer, public Initialize<AsFrequencyResponse>
{
	public:
		AsConstruct(AsFrequencyResponse);
		void initialize(void)
		{
			add(step,				FE_USE("composer:step"));
			add(home,				FE_USE("composer:home"));
			add(outer,				FE_USE("composer:outer"));
			add(inner,				FE_USE("composer:inner"));
			add(samples,			FE_USE("composer:samples"));
		}

		Accessor<String>					step;
		Accessor<String>					home;
		Accessor< Array<String> >		outer;
		Accessor< Array<String> >		inner;
		Accessor<int>							samples;
};


/// @brief Clock
class AsTime
	: public AsNamed, public Initialize<AsTime>
{
	public:
		AsConstruct(AsTime);
		void initialize(void)
		{
			add(time,				FE_USE("time:time"));
			add(dt,					FE_USE("time:dt"));
		}
		Record simClock(sp<RecordGroup> &a_rg)
		{
			bind(a_rg->scope());
			Record r_time = recordFind<AsTime>(a_rg, FE_SIM_CLOCK);
			if(!r_time.isValid())
			{
				sp<Layout> l_time = a_rg->scope()->declare(FE_L_TIME);
				populate(l_time);
				r_time = a_rg->scope()->createRecord(l_time);
				a_rg->add(r_time);
				name(r_time) = FE_SIM_CLOCK;
				time(r_time) = 0.0;
				dt(r_time) = 1.0;
			}
			return r_time;
		}

		Accessor<t_moa_real>		time;
		Accessor<t_moa_real>		dt;
};


/// @brief
class AsOverlay
	: public AsNamed, public Initialize<AsOverlay>
{
	public:
		AsConstruct(AsOverlay);
		void initialize(void)
		{
			add(label,					FE_USE("label"));
			add(dataset,				FE_USE("dataset:group"));
		}

		Accessor<String>					label;
		Accessor< sp<RecordGroup> >		dataset;
};



/// @brief Time/Simulation Tick Signal
class AsTick
	: public AccessorSet, public Initialize<AsTick>
{
	public:
		AsConstruct(AsTick);
		void initialize(void)
		{
			add(id,				FE_USE("note:id"));
		}
		Accessor<int>		id;
};


/// @brief Splat Configuration
class AsSplat
	: public AccessorSet, public Initialize<AsSplat>
{
	public:
		AsConstruct(AsSplat);
		void initialize(void)
		{
			add(x_range,		FE_USE("splat:x_range"));
			add(y_range,		FE_USE("splat:y_range"));
			add(expression,		FE_USE("splat:expression"));
			add(name,			FE_USE("name"));
			add(splat,			FE_USE("splat:splat"));
			add(color,			FE_USE("splat:color"));
		}
		AccessorR<t_moa_v2>	x_range;
		AccessorR<t_moa_v2>	y_range;
		AccessorR<String>	expression;
		AccessorR<String>	name;
		AccessorR<String>	splat;
		AccessorR<t_moa_v3>	color;
};

class AsSplatLine
	: public AsSplat, public Initialize<AsSplatLine>
{
	public:
		AsConstruct(AsSplatLine);
		void initialize(void)
		{
			add(is,				FE_USE("splatline:is"));
		}
		Accessor<void>		is;
};

class AsNyquistLine
	: public AsSplat, public Initialize<AsNyquistLine>
{
	public:
		AsConstruct(AsNyquistLine);
		void initialize(void)
		{
			add(scale,				FE_USE("nyquist:scale"));
		}
		Accessor<t_moa_real>		scale;
};

/// Something with a location in simulation
class AsLocator
	: public AccessorSet, public Initialize<AsLocator>
{
	public:
		AsConstruct(AsLocator);
		void initialize(void)
		{
			add(transform,				FE_USE("spc:transform"));
			//add(transform_inv,		FE_USE("spc:transform_inv"));
			//add(velocity,				FE_USE("spc:velocity"));
			//add(angular_velocity,		FE_USE("spc:angular_velocity"));
			//add(force,				FE_USE("sim:force"));
			//add(moment,				FE_USE("sim:moment"));
		}
		Accessor<t_moa_xform>		transform;
		//Accessor<t_moa_xform>		transform_inv;
		//Accessor<t_moa_v3>		velocity;
		//Accessor<t_moa_v3>		angular_velocity;
		//Accessor<t_moa_v3>		force;
		//Accessor<t_moa_v3>		moment;
};



#if 0

///
class AsPointsLocator
	: public AsLocator, public Initialize<AsPointsLocator>
{
	public:
		AsConstruct(AsPointsLocator);
		void initialize(void)
		{
			add(p0,						FE_USE("spc:p0"));
			add(p1,						FE_USE("spc:p1"));
			add(p1,						FE_USE("spc:p1"));
		}
};

///
class AsLocatorMount
	: public AccessorSet, public Initialize<AsLocatorMount>
{
	public:
		AsConstruct(AsLocatorMount);
		void initialize(void)
		{
			add(tire,					FE_USE("spc:tire"));
			add(locator,				FE_USE("spc:locator"));
		}
};
#endif

} /* namespace ext */
} /* namespace fe */

#endif /* __moa_moaAS_h__ */
