/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "moa/moa.pmh"

#include <regex>

namespace fe
{
namespace ext
{

Canonical::~Canonical(void)
{
}

Canonical::AccessorSetOp::AccessorSetOp(fe::sp<fe::AccessorSet> a_as)
{
	m_as = a_as;
}

Canonical::AccessorSetOp::~AccessorSetOp(void)
{
}

// Simply put all Records in a dataset into a t_r_vector
void loadVector(t_r_vector &a_r_vector, sp<RecordGroup> a_rg_dataset)
{
	for(fe::RecordGroup::iterator i_rg = a_rg_dataset->begin();
		i_rg != a_rg_dataset->end(); i_rg++)
	{
		fe::sp<fe::RecordArray> spRA = *i_rg;
		for(int i = 0; i < spRA->length(); i++)
		{
			a_r_vector.push_back(spRA->getRecord(i));
		}
	}
}

void Canonical::AccessorSetOp::compile(fe::sp<fe::RecordGroup> a_rg_dataset)
{
	m_as->bind(a_rg_dataset->scope());
	m_rg_dataset = a_rg_dataset;
	loadVector(m_r_vector, m_rg_dataset);
}

void Canonical::AccessorSetOp::process(const t_r_vector &a_input,
	t_r_vector &a_output)
{
	for(unsigned int i_r = 0; i_r < a_input.size(); i_r++)
	{
		if(m_as->check(a_input[i_r]))
		{
			a_output.push_back(a_input[i_r]);
		}
	}
}

void Canonical::addBond(const fe::String &a_key, const fe::String &a_left,
	const fe::String &a_right)
{
	fe::sp<BondOp> spBond(new BondOp(a_left, a_right));
	spBond->compile(m_rg_dataset);
	m_op_map[a_key] = spBond;
}

Canonical::BondOp::BondOp(const fe::String &a_left, const fe::String &a_right)
{
	m_left = a_left;
	m_right = a_right;
}

Canonical::BondOp::~BondOp(void)
{
}

void Canonical::BondOp::compile(fe::sp<fe::RecordGroup> a_rg_dataset)
{
	m_rg_dataset = a_rg_dataset;
	loadVector(m_r_vector, m_rg_dataset);
}


void Canonical::BondOp::process(const t_r_vector &a_input,t_r_vector &a_output)
{
	//TODO:X  ultra naive POC impl:  sloooooow.
	//TODO: probably have this only trigger on a dataset reconfigure
	fe::ext::AsNamed asNamed;
	asNamed.bind(m_rg_dataset->scope());
	fe::Accessor<fe::String> left(m_rg_dataset->scope(), m_left);
	fe::Accessor<fe::String> right(m_rg_dataset->scope(), m_right);

	// whole dataset
	for(unsigned int i_r_d = 0; i_r_d < m_r_vector.size(); i_r_d++)
	{
		if(left.check(m_r_vector[i_r_d]) && right.check(m_r_vector[i_r_d]))
		{
			for(unsigned int i_r = 0; i_r < a_input.size(); i_r++)
			{
				if(asNamed.check(a_input[i_r]) &&
					left(m_r_vector[i_r_d]) == asNamed.name(a_input[i_r]))
				{
					for(unsigned int j_r = 0; j_r < m_r_vector.size(); j_r++)
					{
						if(asNamed.check(m_r_vector[j_r]) &&
							right(m_r_vector[i_r_d]) ==
								asNamed.name(m_r_vector[j_r]))
						{
							a_output.push_back(m_r_vector[j_r]);
						}
					}
				}
			}
		}
	}
}

Canonical::RecordGroupOp::RecordGroupOp(fe::sp<fe::RecordGroup> a_rg_inject)
{
	m_rg_inject = a_rg_inject;
}

Canonical::RecordGroupOp::~RecordGroupOp(void)
{
}

void Canonical::RecordGroupOp::compile(fe::sp<fe::RecordGroup> a_rg_dataset)
{
	loadVector(m_r_vector, m_rg_inject);
}

void Canonical::RecordGroupOp::process(const t_r_vector &a_input,
	t_r_vector &a_output)
{
	a_output = m_r_vector;
}

void Canonical::addRecordGroup(const fe::String &a_key,
	fe::sp<fe::RecordGroup> a_rg_inject)
{
	fe::sp<RecordGroupOp> spOp(new RecordGroupOp(a_rg_inject));
	spOp->compile(m_rg_dataset);
	m_op_map[a_key] = spOp;
}

Canonical::NameFilter::NameFilter(void)
{
}

Canonical::NameFilter::~NameFilter(void)
{
}

void Canonical::NameFilter::compile(fe::sp<fe::RecordGroup> a_rg_dataset)
{
	m_asNamed.bind(a_rg_dataset->scope());
}

bool Canonical::NameFilter::filter(const fe::String &a_arg,
	const t_r_vector &a_in, t_r_vector &a_out)
{
	bool rv = false;
	const fe::String &label = a_arg;
	for(unsigned int i_r = 0; i_r < a_in.size(); i_r++)
	{
		if(m_asNamed.check(a_in[i_r]))
		{
			if(m_asNamed.name(a_in[i_r]) == label)
			{
				a_out.push_back(a_in[i_r]);
				rv = true;
			}
		}
	}
	return rv;
}


Canonical::IndexFilter::IndexFilter(void)
{
}

Canonical::IndexFilter::~IndexFilter(void)
{
}

void Canonical::IndexFilter::compile(fe::sp<fe::RecordGroup> a_rg_dataset)
{
}

bool Canonical::IndexFilter::filter(const fe::String &a_arg,
	const t_r_vector &a_in, t_r_vector &a_out)
{
	bool rv = false;
	const fe::String &label = a_arg;
	fe::Regex re_real("[-+]?[0-9]*\\.?[0-9]+");
	if(re_real.match(label.c_str()))
	{
		rv = true;
		int index = atoi(label.c_str());
		if(index >= 0 && index < (int)a_in.size())
		{
			a_out.push_back(a_in[index]);
		}
	}
	return rv;
}

void Canonical::baseAddress(const fe::String &a_address,
	fe::String &a_base, fe::String &a_attr)
{
	std::regex re_base_address("(.*)\\.([^\\.]+)$");
	std::string s_address(a_address.c_str());
	std::smatch smtch;
	if(std::regex_match(s_address, smtch, re_base_address))
	{
		a_base = (smtch[1]).str().c_str();
		a_attr = (smtch[2]).str().c_str();
	}
}

bool Canonical::arrayIndex(const fe::String &a_input,
	fe::String &a_trimmed, unsigned int &a_index)
{
	std::regex re_is_array("^([a-zA-Z][a-zA-Z_0-9\\s\\:]*)\\[([0-9]*)\\]");
	std::string s_input(a_input.c_str());
	std::smatch smtch;
	if(std::regex_match(s_input, smtch, re_is_array))
	{
		fe::String s_number = (smtch[2]).str().c_str();
		a_trimmed = (smtch[1]).str().c_str();
		a_index = atoi(s_number.c_str());
		return true;
	}
	a_trimmed = a_input;
	return false;
}

void Canonical::locate(t_r_vector &a_records, const fe::String &a_address)
{
	std::vector<fe::String> tokens;
	split(tokens, a_address, ".");

	if(tokens.size() == 0) { return; }

	t_r_vector dataset_records;
	for(fe::RecordGroup::iterator i_rg = m_rg_dataset->begin();
		i_rg != m_rg_dataset->end(); i_rg++)
	{
		fe::sp<fe::RecordArray> spRA = *i_rg;
		for(int i = 0; i < spRA->length(); i++)
		{
			a_records.push_back(spRA->getRecord(i));
		}
	}

	fe::ext::AsNamed asNamed;
	asNamed.bind(m_rg_dataset->scope());
	std::regex re_is_filter("^([a-zA-Z][a-zA-Z_0-9\\s\\:]*)\\[([^\\]]*)\\]");

	for(unsigned int i_token = 0; i_token < tokens.size(); i_token++)
	{
		fe::String key;

		bool do_filter = false;
		fe::String filter_arg;
		std::string s_input(tokens[i_token].c_str());
		std::smatch smtch;
		if(std::regex_match(s_input, smtch, re_is_filter))
		{
			do_filter = true;
			filter_arg = (smtch[2]).str().c_str();
			key = (smtch[1]).str().c_str();
		}
		else
		{
			key = tokens[i_token].c_str();
		}

		t_r_vector next_records;

		t_op_map::iterator i_ops = m_op_map.find(key);
		if(i_ops != m_op_map.end())
		{
			i_ops->second->process(a_records, next_records);
		}
		else if(i_token == 0) // if whiff first run, groom for filter
		{
			// pass through all records to filter
			next_records = a_records;
			do_filter = true;
			filter_arg = tokens[i_token].c_str();
		}
		else
		{
			if(a_records.size() > 0)
			{
				fe::Record r_first = a_records[0];

				fe::sp<fe::Scope> spScope = m_rg_dataset->scope();
				FE_UWORD i_attr;
				if(spScope->findAttribute(tokens[i_token],i_attr).isValid())
				{

					fe::sp<fe::TypeMaster> spTM = spScope->typeMaster();
					fe::sp<fe::BaseType> spTypeRecord =
						spTM->lookupType(fe::TypeInfo(
							fe::getTypeId<fe::Record>()));

					fe::sp<fe::BaseType> spTypeIs =
						spScope->attribute(i_attr)->type();

					if(spTypeRecord == spTypeIs)
					{
						fe::Accessor<fe::Record> as_record;
						as_record.setup(spScope, tokens[i_token]);
						if(r_first.isValid())
						{
							fe::Record r_referenced = as_record(r_first);
							if(r_referenced.isValid())
							{
								next_records.push_back(r_referenced);
							}
						}
					}
				}
			}
		}
		a_records = next_records;

		if(do_filter)
		{
			next_records.clear();
			for(unsigned int i = 0; i < m_op_vector.size(); i++)
			{
				if(m_op_vector[i]->filter(filter_arg,a_records,next_records))
				{
					a_records = next_records;
					break;
				}
			}
		}
	}
}


void RealAccessor::setup(fe::sp<fe::Scope> a_scope, const fe::String &a_name)
{
	if(a_scope.isValid())
	{
		fe::sp<fe::Scope> spScope = a_scope;
		fe::sp<fe::TypeMaster> spTM = spScope->typeMaster();

		m_index = 0;
		fe::String trimmed;
		bool is_array = Canonical::arrayIndex(a_name, trimmed, m_index);

		FE_UWORD i_attr;
		if(!spScope->findAttribute(trimmed,i_attr).isValid())
		{
			return;
		}

		fe::sp<fe::BaseType> spType1 =
			spTM->lookupType(
				fe::TypeInfo(fe::getTypeId<t_moa_real>()));
		fe::sp<fe::BaseType> spType2 =
			spTM->lookupType(fe::TypeInfo(fe::getTypeId<t_moa_v2>()));
		fe::sp<fe::BaseType> spType3 =
			spTM->lookupType(fe::TypeInfo(fe::getTypeId<t_moa_v3>()));
		fe::sp<fe::BaseType> spType4 =
			spTM->lookupType(fe::TypeInfo(fe::getTypeId<t_moa_v4>()));
		fe::sp<fe::BaseType> spType5 =
			spTM->lookupType(fe::TypeInfo(fe::getTypeId<t_atomic_real>()));
		fe::sp<fe::BaseType> spType6 =
			spTM->lookupType(fe::TypeInfo(fe::getTypeId<int>()));
		fe::sp<fe::BaseType> spTypeIs =
			spScope->attribute(i_attr)->type();

		m_mode = 0;
		if(spType1 == spTypeIs)
		{
			FEASSERT(!is_array);
			m_mode = 1;
			m_a_real.setup(spScope, trimmed);
		}
		if(spType2 == spTypeIs)
		{
			FEASSERT(is_array);
			m_mode = 2;
			m_a_v2.setup(spScope, trimmed);
		}
		if(spType3 == spTypeIs)
		{
			FEASSERT(is_array);
			m_mode = 3;
			m_a_v3.setup(spScope, trimmed);
		}
		if(spType4 == spTypeIs)
		{
			FEASSERT(is_array);
			m_mode = 4;
			m_a_v4.setup(spScope, trimmed);
		}
		if(spType5 == spTypeIs)
		{
			FEASSERT(!is_array);
			m_mode = 5;
			m_a_atomic.setup(spScope, trimmed);
		}
		if(spType6 == spTypeIs)
		{
			FEASSERT(!is_array);
			m_mode = 6;
			m_a_int.setup(spScope, trimmed);
		}
	}
}

bool RealAccessor::check(const fe::Record& r_hasreal) const
{
	if(m_mode == 1)
	{
		return m_a_real.check(r_hasreal);
	}
	else if(m_mode == 2)
	{
		return m_a_v2.check(r_hasreal);
	}
	else if(m_mode == 3)
	{
		return m_a_v3.check(r_hasreal);
	}
	else if(m_mode == 4)
	{
		return m_a_v4.check(r_hasreal);
	}
	else if(m_mode == 5)
	{
		return m_a_atomic.check(r_hasreal);
	}
	else if(m_mode == 6)
	{
		return m_a_int.check(r_hasreal);
	}
	return false;
}

t_moa_real	RealAccessor::get(const fe::Record& r_hasreal)
{
	if(m_mode == 1)
	{
		return m_a_real(r_hasreal);
	}
	else if(m_mode == 2)
	{
		return m_a_v2(r_hasreal)[m_index];
	}
	else if(m_mode == 3)
	{
		return m_a_v3(r_hasreal)[m_index];
	}
	else if(m_mode == 4)
	{
		return m_a_v4(r_hasreal)[m_index];
	}
	else if(m_mode == 5)
	{
		return m_a_atomic(r_hasreal);
	}
	else if(m_mode == 6)
	{
		return (t_moa_real)m_a_int(r_hasreal);
	}
	return 0.0;
}

void RealAccessor::set(const fe::Record& r_hasreal, t_moa_real a_v)
{
	if(m_mode == 1)
	{
		m_a_real(r_hasreal) = a_v;
	}
	else if(m_mode == 2)
	{
		m_a_v2(r_hasreal)[m_index] = a_v;
	}
	else if(m_mode == 3)
	{
		m_a_v3(r_hasreal)[m_index] = a_v;
	}
	else if(m_mode == 4)
	{
		m_a_v4(r_hasreal)[m_index] = a_v;
	}
	else if(m_mode == 5)
	{
		m_a_atomic(r_hasreal) = a_v;
	}
	else if(m_mode == 6)
	{
		m_a_int(r_hasreal) = (int)a_v;
	}
}

void CanonicalStd::bind(fe::sp<fe::RecordGroup> a_rg_dataset)
{
	Canonical::bind(a_rg_dataset);
	append(sp<Canonical::Filter>(new Canonical::NameFilter()));
	append(sp<Canonical::Filter>(new Canonical::IndexFilter()));
//TODO: this is poorly located:
	addBond("mnt", "mount:rig", "mount:tire");
}

Overlayer::Overlayer(sp<Scope> a_spScope)
{
	m_spScope = a_spScope;
	m_rg_baseline = new RecordGroup();
	m_built = false;

	//TODO: rm this
	//This is just forcing a scope which deprecates when RG's have 1 and only 1 scope
	{
		sp<Layout> l_scope_dummy = m_spScope->declare("ScopeDummy");
		Record r_dummy = m_spScope->createRecord(l_scope_dummy);
		m_rg_baseline->add(r_dummy);
	}

}

void Overlayer::build(void)
{
	m_datasets.build(m_rg_baseline);
	manyToMany();
	mixdown();
	bakeControls();
	m_built = true;
}

void Overlayer::manyToMany(void)
{
	AsDataset &asDataset = m_datasets.as();
	AsDatasetControl asDatasetControl(m_rg_baseline->scope());

	// A Default dataset Layout
	sp<Layout> l_dataset;
//TODO: Producer ??
	l_dataset = m_rg_baseline->scope()->declare("Producer.run.default.dataset");
	asDataset.populate(l_dataset);
	asDatasetControl.populate(l_dataset);

	AsDatasetManyToMany asDatasetManyToMany(m_rg_baseline->scope());
	std::vector<Record> many_to_manys;
	asDatasetManyToMany.filter(many_to_manys, m_rg_baseline);

	for(Record &r_many_to_many : many_to_manys)
	{
		if(!asDatasetManyToMany.active(r_many_to_many))
		{
			continue;
		}
		for(String &left : asDatasetManyToMany.left(r_many_to_many))
		{
			for(String &right : asDatasetManyToMany.right(r_many_to_many))
			{
				//TODO: is this filename ugly ok?
				String name = "M2M--" + left + "--" + right +"--M2M";

				Record r_output = m_datasets[name];

				if(!r_output.isValid())
				{
					r_output = m_rg_baseline->scope()->createRecord(l_dataset);
					asDataset.name(r_output) = name;
					asDatasetControl.active(r_output) = true;
					m_datasets[name] = r_output;
					m_rg_baseline->add(r_output);
				}

				if(r_output.isValid())
				{
					sp<RecordGroup> rg_output = asDataset.dataset(r_output);

					if(!rg_output.isValid())
					{
						rg_output = new RecordGroup();
						asDataset.dataset(r_output) = rg_output;
						rg_output->add(r_output);
					}

					fe::Array<fe::String> inputs;
					inputs.push_back(left);
					inputs.push_back(right);
					for(unsigned int i = 0; i < inputs.size(); i++)
					{
						Record r_input = m_datasets[inputs[i]];
						if(r_input.isValid())
						{
							sp<RecordGroup> rg_input=asDataset.dataset(r_input);
							rg_input = fe::ext::clone(rg_input);

							if(rg_input.isValid())
							{
								overlay(rg_output, rg_input);
							}
						}
					}
				}
			}
		}
	}
}

void Overlayer::mixdown(void)
{
	AsDataset &asDataset = m_datasets.as();
	AsDatasetControl asDatasetControl(m_rg_baseline->scope());

	// A Default dataset Layout
	sp<Layout> l_dataset;
	l_dataset = m_rg_baseline->scope()->declare("Producer.run.default.dataset");
	asDataset.populate(l_dataset);
	asDatasetControl.populate(l_dataset);

	AsDatasetMixer asDatasetMixer(m_rg_baseline->scope());
	std::vector<Record>	mixers;
	asDatasetMixer.filter(mixers, m_rg_baseline);

	for(unsigned int i_mixer = 0; i_mixer < mixers.size(); i_mixer++)
	{
		Record r_mixer = mixers[i_mixer];

		String name = "DSM--" + asDatasetMixer.name(r_mixer) + "--DSM";

		Record r_output = m_datasets[name];


		if(!r_output.isValid())
		{
			r_output = m_rg_baseline->scope()->createRecord(l_dataset);
			asDataset.name(r_output) = name;
			if(asDatasetControl.check(r_output) &&
				asDatasetControl.check(r_mixer))
			{
				asDatasetControl.active(r_output) =
					asDatasetControl.active(r_mixer);
			}
			m_datasets[name] = r_output;
			m_rg_baseline->add(r_output);
		}

		if(r_output.isValid())
		{
			sp<RecordGroup> rg_output = asDataset.dataset(r_output);

			if(!rg_output.isValid())
			{
				rg_output = new RecordGroup();
				asDataset.dataset(r_output) = rg_output;
				rg_output->add(r_output);
			}

			const fe::Array<fe::String> &inputs= asDatasetMixer.inputs(r_mixer);
			for(unsigned int i = 0; i < inputs.size(); i++)
			{
				Record r_input = m_datasets[inputs[i]];
				if(r_input.isValid())
				{
					sp<RecordGroup> rg_input = asDataset.dataset(r_input);
					// clone the input so each run is clean
					rg_input = fe::ext::clone(rg_input);

					if(rg_input.isValid())
					{
						overlay(rg_output, rg_input);
					}
				}
			}
		}
	}
}

void Overlayer::bakeControls(void)
{
	AsDatasetControl asDatasetControl(m_rg_baseline->scope());
	std::vector<Record>	controls;
	asDatasetControl.filter(controls, m_rg_baseline);

	AsDatasetMeta asDatasetMeta;
	asDatasetMeta.digest(m_rg_baseline);
	asDatasetMeta.set("splatpath", "data");

	AsDataset &asDataset = m_datasets.as();

	m_rgs_baked.clear();

	for(Record &r_control : controls)
	{
		if(!asDatasetControl.active(r_control)) { continue; }

		const String &datacode = asDatasetControl.name(r_control);
		Record r_dataset = m_datasets[datacode];

		if(!r_dataset.isValid()) { continue; }

		// >>>>>>>>> FINALIZE DATASET
		asDatasetMeta.set("datacode", datacode);
		sp<RecordGroup> rg_clone = fe::ext::clone(m_rg_baseline);
		overlay(rg_clone, asDataset.dataset(r_dataset));

		m_rgs_baked.push_back(rg_clone);
	}
}

void pre_dump(std::string &a_ctrl_code, std::string &a_ending, int a_flags)
{
	a_ctrl_code = "";
	a_ending = "\n";
	if(a_flags & e_unnl) { a_ending = ""; }
	if(a_flags & e_red) { a_ctrl_code += "[31m"; }
	if(a_flags & e_green) { a_ctrl_code += "[32m"; }
	if(a_flags & e_yellow) { a_ctrl_code += "[33m"; }
	if(a_flags & e_blue) { a_ctrl_code += "[34m"; }
	if(a_flags & e_magenta) { a_ctrl_code += "[35m"; }
	if(a_flags & e_cyan) { a_ctrl_code += "[36m"; }
	if(a_flags & e_white) { a_ctrl_code += "[37m"; }
}

void dump(const char *a_msg)
{
	dump(0, a_msg);
}

void dump(int a_flags, const char *a_msg)
{
	std::string ctrl_code;
	std::string ending;
	pre_dump(ctrl_code, ending, a_flags);
	fe_fprintf(stderr, "%s%-30s [0m%s", ctrl_code.c_str(), a_msg, ending.c_str());
}

void dump(const char *a_msg, const char *a_txt)
{
	dump(0, a_msg, a_txt);
}

void dump(int a_flags, const char *a_msg, const char *a_txt)
{
	std::string ctrl_code;
	std::string ending;
	pre_dump(ctrl_code, ending, a_flags);
	fe_fprintf(stderr, "%s%-30s [%s] [0m%s", ctrl_code.c_str(), a_msg, a_txt, ending.c_str());
}

#if 0
void dump(const char *a_msg, int a_i)
{
	dump(0, a_msg, a_i);
}

void dump(int a_flags, const char *a_msg, int a_i)
{
	std::string ctrl_code;
	std::string ending;
	pre_dump(ctrl_code, ending, a_flags);
	fe_fprintf(stderr, "%s%-30s %d [0m%s", ctrl_code.c_str(), a_msg, a_i, ending.c_str());
}
#endif

void dump(const char *a_msg, t_moa_real a_r)
{
	dump(0, a_msg, a_r);
}

void dump(int a_flags, const char *a_msg, t_moa_real a_r)
{
	std::string ctrl_code;
	std::string ending;
	pre_dump(ctrl_code, ending, a_flags);
	fe_fprintf(stderr, "%s%-30s %g [0m%s", ctrl_code.c_str(), a_msg, a_r, ending.c_str());
}

void dump(const char *a_msg, const fe::ext::t_moa_v3 &a_v3)
{
	dump(0, a_msg, a_v3);
}
void dump(int a_flags, const char *a_msg, const fe::ext::t_moa_v3 &a_v3)
{
	std::string ctrl_code;
	std::string ending;
	pre_dump(ctrl_code, ending, a_flags);
	//fe_fprintf(stderr, "%s%-30s % 6.2e % 6.2e % 6.2e [0m%s", ctrl_code.c_str(), a_msg, a_v3[0], a_v3[1], a_v3[2], ending.c_str());
	fe_fprintf(stderr, "%s%-30s % 5.4f % 5.4f % 5.4f [0m%s", ctrl_code.c_str(), a_msg, a_v3[0], a_v3[1], a_v3[2], ending.c_str());
}
void dump(const char *a_msg, const fe::ext::t_moa_xform &a_xform)
{
	dump(0, a_msg, a_xform);
}
void dump(int a_flags, const char *a_msg, const fe::ext::t_moa_xform &a_xform)
{
	std::string ctrl_code;
	std::string ending;
	pre_dump(ctrl_code, ending, a_flags);
#if 0
	fe_fprintf(stderr, "%s%-30s % 2.1e % 2.1e % 2.1e % 2.1e [0m%s", ctrl_code.c_str(), a_msg,	a_xform(0,0), a_xform(0,1), a_xform(0,2), a_xform(0,3), ending.c_str());
	fe_fprintf(stderr, "%s%-30s % 2.1e % 2.1e % 2.1e % 2.1e [0m%s", ctrl_code.c_str(), "|",		a_xform(1,0), a_xform(1,1), a_xform(1,2), a_xform(1,3), ending.c_str());
	fe_fprintf(stderr, "%s%-30s % 2.1e % 2.1e % 2.1e % 2.1e [0m%s", ctrl_code.c_str(), "|",		a_xform(2,0), a_xform(2,1), a_xform(2,2), a_xform(2,3), ending.c_str());
#endif

	t_moa_real max = 0.0;
	fe::ext::t_moa_xform xform = a_xform;
	for(unsigned int i = 0; i < 3; i++)
	{
		for(unsigned int j = 0; j < 4; j++)
		{
			if(fabs(xform(i,j)) > max) { max = fabs(xform(i,j)); }
		}
	}

	//if(max < 1.0e4 && max > 1.0e-4)
	//if(max < 1.0e4 && max > 1.0e-4)
	if(true)
	{
		fe_fprintf(stderr, "%s%-30s % 2.1f % 2.1f % 2.1f % 2.1f [0m%s", ctrl_code.c_str(), a_msg,	xform(0,0), xform(0,1), xform(0,2), xform(0,3), ending.c_str());
		fe_fprintf(stderr, "%s%-30s % 2.1f % 2.1f % 2.1f % 2.1f [0m%s", ctrl_code.c_str(), "|",		xform(1,0), xform(1,1), xform(1,2), xform(1,3), ending.c_str());
		fe_fprintf(stderr, "%s%-30s % 2.1f % 2.1f % 2.1f % 2.1f [0m%s", ctrl_code.c_str(), "|",		xform(2,0), xform(2,1), xform(2,2), xform(2,3), ending.c_str());
	}
	else
	{
		fe_fprintf(stderr, "%s%-30s % 2.1e % 2.1e % 2.1e % 2.1e [0m%s", ctrl_code.c_str(), a_msg,	a_xform(0,0), a_xform(0,1), a_xform(0,2), a_xform(0,3), ending.c_str());
		fe_fprintf(stderr, "%s%-30s % 2.1e % 2.1e % 2.1e % 2.1e [0m%s", ctrl_code.c_str(), "|",		a_xform(1,0), a_xform(1,1), a_xform(1,2), a_xform(1,3), ending.c_str());
		fe_fprintf(stderr, "%s%-30s % 2.1e % 2.1e % 2.1e % 2.1e [0m%s", ctrl_code.c_str(), "|",		a_xform(2,0), a_xform(2,1), a_xform(2,2), a_xform(2,3), ending.c_str());
	}
}

void dump(const char *a_msg, fe::Record a_r)
{
	dump(0, a_msg, a_r);
}

void dump(int a_flags, const char *a_msg, fe::Record a_r)
{
	std::string ctrl_code;
	std::string ending;
	pre_dump(ctrl_code, ending, a_flags);

	if(!a_r.isValid())
	{
		fe_fprintf(stderr, "%s%-30s %s [0m%s", ctrl_code.c_str(), a_msg, "INVALID", ending.c_str());
		return;
	}

	fe::ext::AsNamed asNamed;
	asNamed.bind(a_r);

	if(!asNamed.check(a_r))
	{
		fe_fprintf(stderr, "%s%-30s %s [0m%s", ctrl_code.c_str(), a_msg, "NOT NAMED", ending.c_str());
		return;
	}

	fe_fprintf(stderr, "%s%-30s %s [0m%s", ctrl_code.c_str(), a_msg, asNamed.name(a_r).c_str(), ending.c_str());

}

} /* namespace ext */
} /* namespace fe */
