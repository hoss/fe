/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __moa_SystemI_h__
#define __moa_SystemI_h__

namespace fe
{
namespace ext
{

/**
 * System Interface for MOA.
 */
class FE_DL_EXPORT SystemI :
	virtual public Component,
	public CastableAs<SystemI>
{
	public:
		/**
		 * Perform processing due to having been signaled.
		 *
		 * @param a_note_id The id of the tick type that we are reacting to.
		 *
		 * This is a convenience function, wrapping "note" connecting.
		 *
		 * orchestrator->connect(this, "a signal name"); // without functional
		 * connects to this perform(), such as:
		 * orchestrator->connect(this, &SystemI::perform, "a signal name");
		 *
		 */
		virtual	void perform(const t_note_id &a_note_id) {}

		/**
		 * Callback for the System to register itself to the orchestrator.
		 *
		 * This allows you to specify which ticks you'd like to react to.
		 *
		 * @param a_spOrchestrator Orchestrator for you to connect to.
		 */
		virtual	void connectOrchestrator(
			sp<OrchestratorI> a_spOrchestrator)								= 0;

};

} /* namespace ext*/
} /* namespace fe*/

#endif /* __moa_SystemI_h__ */
