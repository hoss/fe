/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "moa/moa.pmh"

namespace fe
{
namespace ext
{

MultiSweepSystem::MultiSweepSystem(void)
{
}

MultiSweepSystem::~MultiSweepSystem(void)
{
}

void MultiSweepSystem::initialize(void)
{
}

void MultiSweepSystem::connectOrchestrator(fe::sp<OrchestratorI> a_spOrchestrator)
{
	m_spOrchestrator = a_spOrchestrator;
	a_spOrchestrator->connect(this,&MultiSweepSystem::start,FE_NOTE_START);
}

void MultiSweepSystem::performDataset(sp<RecordGroup> a_rg_dataset)
{
	t_note_id m_note_run_start = m_spOrchestrator->note_id(FE_NOTE_RUN_STOP);
	t_note_id m_note_run_stop = m_spOrchestrator->note_id(FE_NOTE_RUN_START);
	t_note_id m_note_line_start = m_spOrchestrator->note_id(FE_NOTE_LINE_START);
	t_note_id m_note_line_stop = m_spOrchestrator->note_id(FE_NOTE_LINE_STOP);
	t_note_id m_note_datapoint = m_spOrchestrator->note_id(FE_NOTE_DATAPOINT);
	t_note_id m_note_image = m_spOrchestrator->note_id(FE_NOTE_IMAGE);

	//TODO: huh? -- why straight test
	t_note_id m_note_recompile = m_spOrchestrator->note_id("recompile");


	// filtering way up here per dataset is fine
	AsMultiSweep asMultiSweep(a_rg_dataset->scope());
	std::vector<Record>	multisweeps;
	asMultiSweep.filter(multisweeps, a_rg_dataset);

//TODO: we need a snapshot and re overlay so that tires dont wear out, etc
//or an init overlap concept?
//splat:splat -> splat:adjoin   , code -> name
	sp<RecordGroup> rg_dataset_sweep = a_rg_dataset;

	AsDatasetMeta asDatasetMeta;
	asDatasetMeta.digest(a_rg_dataset);
fe_fprintf(stderr, ">>>>>>>>>>>>>>>> A\n");

	for(unsigned int i_sweep = 0; i_sweep < multisweeps.size(); i_sweep++)
	{
		// r_sweep before rg_dataset_sweep, so should be consider RO from here
		Record r_sweep = multisweeps[i_sweep];

		//Record r_meta = recordFind<AsDatasetMeta>(rg_dataset_sweep, FE_HEADER);
		//asDatasetMeta.runcode(r_meta) = asMultiSweep.name(r_sweep);

		asDatasetMeta.set("runcode", asMultiSweep.name(r_sweep));

		m_spOrchestrator->perform(m_note_recompile);

		sp<ext::Evaluator> spStepEval;
		spStepEval = registry()->create("EvaluatorI.EvaluatorStd.fe");
		spStepEval->bind(rg_dataset_sweep);
		sp<AsVariable> asVariable = spStepEval->asVariable();
		Record r_outer = spStepEval->create("OUTER", asVariable);
		Record r_inner = spStepEval->create("INNER", asVariable);
		spStepEval->compile(asMultiSweep.step(r_sweep).c_str());

		EvalRange inner_range(asMultiSweep.inner(r_sweep));
		EvalRange outer_range(asMultiSweep.outer(r_sweep));
fe_fprintf(stderr, ">>>>>>>>>>>>>>>> B\n");

		// if increment is zero, then we basically have no outer loop at all
		bool do_outer = true;
		if(outer_range.m_incr == 0.0)
		{
			do_outer = false;
			outer_range = EvalRange();
		}
		EvalRange::iterator i_outer = outer_range.begin();
fe_fprintf(stderr, ">>>>>>>>>>>>>>>> C\n");

		m_spOrchestrator->perform(m_note_run_start);

unsigned int ii = 0;
fe_fprintf(stderr, ">>>>>>>>>>>>>>>> D\n");

		for(; i_outer != outer_range.end(); i_outer++)
		{
			EvalRange::iterator i_inner = inner_range.begin();
			if(do_outer) { asVariable->value(r_outer) = *i_outer; }
			asVariable->value(r_inner) = *i_inner;
			spStepEval->evaluate();
fe_fprintf(stderr, ">>>>>>>>>>>>>>>> E\n");

			// RUN-UP
			m_spOrchestrator->setDT(1.0/asMultiSweep.frequency(r_sweep));
fe_fprintf(stderr, ">>>>>>>>>>>>>>>> F\n");
			m_spOrchestrator->step(e_step_no_more_than, asMultiSweep.runup(r_sweep));
fe_fprintf(stderr, ">>>>>>>>>>>>>>>> G\n");

			m_spOrchestrator->perform(m_note_line_start);
fe_fprintf(stderr, ">>>>>>>>>>>>>>>> H\n");

			for(; i_inner != inner_range.end(); i_inner++)
			{
fe_fprintf(stderr, "setting inner %g\n", *i_inner);
				asVariable->value(r_inner) = *i_inner;
				spStepEval->evaluate();

				m_spOrchestrator->setDT(1.0/asMultiSweep.frequency(r_sweep));
				t_moa_real linger = asMultiSweep.linger(r_sweep);
				if(linger < m_spOrchestrator->getDT())
				{
					linger = m_spOrchestrator->getDT();
				}

ii++;
fe_fprintf(stderr, ">>>>>>>>>>>>>>>> %d\n", ii);
				m_spOrchestrator->step(e_step_no_more_than, linger);

				m_spOrchestrator->perform(m_note_datapoint);
			}

			m_spOrchestrator->perform(m_note_line_stop);
		}
		m_spOrchestrator->perform(m_note_run_stop);

		m_spOrchestrator->perform(m_note_image);
	}
}


void MultiSweepSystem::start(const t_note_id &a_note_id)
{
	sp<RecordGroup> rg_dataset = m_spOrchestrator->dataset();
	performDataset(rg_dataset);
}

} /* namespace ext */
} /* namespace fe */
