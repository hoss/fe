/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __moa_Function_h__
#define __moa_Function_h__

#include "solve/solve.h"
#include "evaluate/evaluate.h"
#include "evaluate/Function.h"

#define FE_GS_EVAL_GLOBAL_VAR	FALSE

namespace fe
{
namespace ext
{

#if FE_GS_EVAL_GLOBAL_VAR
extern std::map< std::string, t_moa_real > gs_eval_global_var;
#endif

class FE_DL_EXPORT GetGlobalVar :
	virtual public FunctionI,
	public Initialize<GetGlobalVar>
{
	public:
		GetGlobalVar(void) {}
		~GetGlobalVar(void);
		void initialize(void);
virtual	void eval(Record &a_return,
			t_stdvector<WeakRecord> &a_argv);
virtual	bool compile(sp<RecordGroup> a_rg, Record &a_return,
			t_stdvector<WeakRecord> &a_argv,
			t_stdstring &a_msg);
	private:
virtual	sp<Aggregate>	&returnType(void) { return m_aggregate; }
		sp<AsVariable>	m_asVariable;
		sp<Aggregate>	m_aggregate;
		bool						m_valid;
		t_moa_real					*m_p_real;
};


/// @brief FunctionI to access real value attributes in src/data
class FE_DL_EXPORT GetRealFromRecord :
	virtual public FunctionI,
	public Initialize<GetRealFromRecord>
{
	public:
		GetRealFromRecord(void) {}
		~GetRealFromRecord(void);
		void initialize(void);
virtual	void eval(Record &a_return,
			t_stdvector<WeakRecord> &a_argv);
virtual	bool compile(sp<RecordGroup> a_rg, Record &a_return,
			t_stdvector<WeakRecord> &a_argv,
			t_stdstring &a_msg);
	private:
virtual	sp<Aggregate>	&returnType(void) { return m_aggregate; }
		sp<AsVariable>	m_asVariable;
		sp<Aggregate>	m_aggregate;
		Accessor<t_moa_real>	m_a_real;
		Accessor<t_moa_v2>		m_a_v2;
		Accessor<t_moa_v3>		m_a_v3;
		Record					m_record;
		unsigned int				m_index;
		unsigned int				m_type;
		bool						m_valid;
		sp<RecordGroup>		m_rg;
		sp<Scope>			m_spScope;
};

/// @brief FunctionI to get real value attributes in src/data
class FE_DL_EXPORT CanonicalRealGet :
	virtual public FunctionI,
	public Initialize<CanonicalRealGet>
{
	public:
		CanonicalRealGet(void) {}
		~CanonicalRealGet(void);
		void initialize(void);
virtual	void eval(Record &a_return,
			t_stdvector<WeakRecord> &a_argv);
virtual	bool compile(sp<RecordGroup> a_rg, Record &a_return,
			t_stdvector<WeakRecord> &a_argv,
			t_stdstring &a_msg);
	private:
virtual	sp<Aggregate>	&returnType(void) { return m_aggregate; }
		sp<AsVariable>	m_asVariable;
		sp<Aggregate>	m_aggregate;
		RealAccessor				m_a_real;
		Record					m_record;
		sp<RecordGroup>		m_rg;
		sp<Scope>			m_spScope;
		CanonicalStd				m_canonical;
		unsigned int				m_mode;
		String					m_arg0;
		String					m_arg1;
		AsString			m_asString;
};

/// @brief FunctionI to set real value attributes in src/data
class FE_DL_EXPORT CanonicalRealSet :
	virtual public FunctionI,
	public Initialize<CanonicalRealSet>
{
	public:
		CanonicalRealSet(void) {}
		~CanonicalRealSet(void);
		void initialize(void);
virtual	void eval(Record &a_return,
			t_stdvector<WeakRecord> &a_argv);
virtual	bool compile(sp<RecordGroup> a_rg, Record &a_return,
			t_stdvector<WeakRecord> &a_argv,
			t_stdstring &a_msg);
	private:
virtual	sp<Aggregate>	&returnType(void) { return m_aggregate; }
		sp<AsVariable>	m_asVariable;
		sp<Aggregate>	m_aggregate;
		RealAccessor				m_a_real;
		Record					m_record;
		sp<RecordGroup>		m_rg;
		sp<Scope>			m_spScope;
		CanonicalStd				m_canonical;
		String					m_arg0;
		String					m_arg1;
		AsString			m_asString;
};

/// @brief Concatenate strings
class FE_DL_EXPORT Concatenate :
	virtual public FunctionI,
	public Initialize<Concatenate>
{
	public:
		Concatenate(void) {}
		~Concatenate(void);
		void initialize(void);
virtual	void eval(Record &a_return,
			t_stdvector<WeakRecord> &a_argv);
virtual	bool compile(sp<RecordGroup> a_rg, Record &a_return,
			t_stdvector<WeakRecord> &a_argv,
			t_stdstring &a_msg);
	private:
virtual	sp<Aggregate>	&returnType(void)
			{ return m_stringAggregate; }
		bool								m_valid;
		sp<RecordGroup>				m_rg;
		sp<Scope>					m_spScope;
		sp< AsString >			m_asString;
		sp< Aggregate >		m_stringAggregate;
};

/// @brief
class FE_DL_EXPORT EvaluatorStd :
	virtual public Evaluator,
	public Initialize<EvaluatorStd>
{
	public:
		EvaluatorStd(void) {}
virtual	~EvaluatorStd(void);
		void initialize(void);
virtual	void bind(sp<RecordGroup> a_rg);
};

} /* namespace ext */
} /* namespace fe */

#endif /* __moa_Function_h__ */
