/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#pragma once

namespace fe
{
namespace ext
{

/// @brief Time Stepping System
class FE_DL_EXPORT Stepper
	: virtual public SystemI,
	public CastableAs<Stepper>
{
	public:
					Stepper(void) { }
		void		compileStepper(const t_note_id &a_note_id);
virtual	void		perform(const t_note_id &a_note_id);

virtual	void		connectOrchestrator(sp<OrchestratorI> a_spOrchestrator);

					/** Compile internal structure for dataset */
virtual	void		compile(const t_note_id &a_note_id) {}

					/** Move system forward in time by a timestep */
virtual	void		step(t_moa_real a_dt)									= 0;

	protected:
		t_moa_real				m_time;
		AsTime					m_asTime;
		Record					r_time;
		t_note_id				m_note_step;
		sp<RecordGroup>			m_rg_dataset;
		sp<OrchestratorI>		m_spOrchestrator;

};

inline void Stepper::compileStepper(const t_note_id &a_note_id)
{
	m_time = 0.0;
	r_time = m_asTime.simClock(m_rg_dataset);
}

inline void Stepper::perform(const t_note_id &a_note_id)
{
	if(r_time.isValid())
	{
		t_moa_real dt = m_spOrchestrator->getTime() - m_time;
		step(dt);
		m_time += dt;
	}
}

inline void Stepper::connectOrchestrator(sp<OrchestratorI> a_spOrchestrator)
{
	m_note_step = a_spOrchestrator->connect(this, FE_NOTE_STEP);
	m_rg_dataset = a_spOrchestrator->dataset();
	a_spOrchestrator->connect(this, &Stepper::compileStepper,FE_NOTE_COMPILE);
	a_spOrchestrator->connect(this, &Stepper::compile, FE_NOTE_COMPILE);
	m_spOrchestrator = a_spOrchestrator;
};

} /* namespace ext */
} /* namespace fe */
