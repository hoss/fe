/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "moa/moa.pmh"

namespace fe
{
namespace ext
{

#if FE_GS_EVAL_GLOBAL_VAR
std::map< std::string, t_moa_real > gs_eval_global_var;
#endif

GetGlobalVar::~GetGlobalVar(void)
{
}

bool GetGlobalVar::compile(sp<RecordGroup> a_rg, Record &a_return,
	t_stdvector<WeakRecord> &a_argv, t_stdstring &a_msg)
{
	m_aggregate = new Aggregate();
	m_asVariable = new AsVariable();
	m_aggregate->bakeTypes(m_asVariable, a_rg->scope()->declare("l_eval_real"));
	a_return = a_rg->scope()->createRecord(m_aggregate->m_spLayout);

	AsString asString;
	asString.bind(a_rg->scope());

	m_valid = true;

	if( !m_asVariable->check(a_return) )
	{
		m_valid = false;
	}
	else if(a_argv.size() < 1)
	{
		a_msg = "requires at least 1 argument";
		m_valid = false;
	}
	else if( !asString.check(a_argv[0]) )
	{
		a_msg = "first arg must be string name";
		m_valid = false;
	}

#if FE_GS_EVAL_GLOBAL_VAR
	std::string key = asString.value(a_argv[0]).c_str();

	std::map< std::string, t_moa_real >::iterator i_s =
			gs_eval_global_var.find(key);
	if(i_s == gs_eval_global_var.end())
	{
		gs_eval_global_var[key] = 0.0;
	}

	m_p_real = &(gs_eval_global_var[key]);
#else
	m_valid = false;
#endif

	return m_valid;
}

void GetGlobalVar::eval(Record &a_return,
	ext::t_stdvector<WeakRecord> &a_argv)
{

	if(m_valid)
	{
		m_asVariable->value(a_return) = *m_p_real;
	}
}

void GetGlobalVar::initialize(void)
{
}

GetRealFromRecord::~GetRealFromRecord(void)
{
}

bool GetRealFromRecord::compile(sp<RecordGroup> a_rg, Record &a_return,
	t_stdvector<WeakRecord> &a_argv, t_stdstring &a_msg)
{
	m_rg = a_rg;
	m_spScope = a_rg->scope();

	m_aggregate = new Aggregate();
	m_asVariable = new AsVariable();
	m_aggregate->bakeTypes(m_asVariable, m_spScope->declare("l_eval_real"));
	a_return = m_spScope->createRecord(m_aggregate->m_spLayout);


	AsString asString;
	asString.bind(m_spScope);

	m_valid = true;
	m_type = 0;

	if( !m_asVariable->check(a_return) )
	{
		m_valid = false;
	}
	else if(a_argv.size() < 2)
	{
		a_msg = "requires at least 2 arguments";
		m_valid = false;
	}
	else if( !asString.check(a_argv[0]) )
	{
		a_msg = "first arg must be string name";
		m_valid = false;
	}
	else if( !asString.check(a_argv[1]) )
	{
		a_msg = "second arg must be string name";
		m_valid = false;
	}
	else if( a_argv.size() == 3 && !m_asVariable->check(a_argv[2]) )
	{
		a_msg = "third arg must be number (index)";
		m_valid = false;
	}

	// first try for compiling in location, but if the names come in via
	// variables, instead of literal strings, this will fall through
	// and we have to pay during eval to find things.
	if( a_argv.size() == 2)
	{
		m_a_real.setup(a_rg->scope(), asString.value(a_argv[1]).c_str());
		RecordDictionaryByAccessor<t_moa_real> records(a_rg, m_a_real);
		m_record = records[asString.value(a_argv[0])];
		if(m_record.isValid())
		{
			m_type = 1;
		}
	}
	else
	{
		m_index = (unsigned int)(m_asVariable->value(a_argv[2]));

		m_a_v3.setup(a_rg->scope(), asString.value(a_argv[1]).c_str());
		RecordDictionaryByAccessor<t_moa_v3> v3records(a_rg, m_a_v3);
		m_record = v3records[asString.value(a_argv[0])];
		if(m_record.isValid())
		{
			if(m_index > 2)
			{
				a_msg = "index out of range";
				m_valid = false;
			}
			else
			{
				m_type = 3;
			}
		}
		else
		{
			m_a_v2.setup(a_rg->scope(), asString.value(a_argv[1]).c_str());
			RecordDictionaryByAccessor<t_moa_v2> v2records(a_rg, m_a_v2);
			m_record = v2records[asString.value(a_argv[0])];
			if(m_record.isValid())
			{
				if(m_index > 1)
				{
					a_msg = "index out of range";
					m_valid = false;
				}
				else
				{
					m_type = 2;
				}
			}
		}

	}

	return m_valid;
}

void GetRealFromRecord::eval(Record &a_return,
	ext::t_stdvector<WeakRecord> &a_argv)
{

	switch(m_type)
	{
		case 1:
			m_asVariable->value(a_return) = m_a_real(m_record);
			break;
		case 2:
			m_asVariable->value(a_return) = m_a_v2(m_record)[m_index];
			break;
		case 3:
			m_asVariable->value(a_return) = m_a_v3(m_record)[m_index];
			break;
		default:
			AsString asString;
			asString.bind(a_return.layout()->scope());
			String record_name = asString.value(a_argv[0]);
			String attr_name = asString.value(a_argv[1]);
			if( a_argv.size() == 2)
			{
				m_a_real.setup(m_spScope, attr_name);
				RecordDictionaryByAccessor<t_moa_real> records(m_rg, m_a_real);
				m_record = records[record_name];
				if(m_record.isValid())
				{
					m_asVariable->value(a_return) = m_a_real(m_record);
				}
			}
			else
			{
				m_index = (unsigned int)(m_asVariable->value(a_argv[2]));

				m_a_v3.setup(m_spScope, attr_name);
				RecordDictionaryByAccessor<t_moa_v3> v3records(m_rg, m_a_v3);
				m_record = v3records[record_name];
				if(m_record.isValid())
				{
					if(m_index <= 2)
					{
						m_asVariable->value(a_return)=m_a_v3(m_record)[m_index];
					}
				}
				else
				{
					m_a_v2.setup(m_spScope, attr_name);
					RecordDictionaryByAccessor<t_moa_v2>
						v2records(m_rg, m_a_v2);
					m_record = v2records[record_name];
					if(m_record.isValid())
					{
						if(m_index <= 1)
						{
							m_asVariable->value(a_return) =
								m_a_v2(m_record)[m_index];
						}
					}
				}
			}
			break;
	}

}

void GetRealFromRecord::initialize(void)
{
}

CanonicalRealGet::~CanonicalRealGet(void)
{
}

bool CanonicalRealGet::compile(sp<RecordGroup> a_rg, Record &a_return,
	t_stdvector<WeakRecord> &a_argv, t_stdstring &a_msg)
{
	m_rg = a_rg;
	m_spScope = a_rg->scope();

	m_aggregate = new Aggregate();
	m_asVariable = new AsVariable();
	m_aggregate->bakeTypes(m_asVariable, m_spScope->declare("l_eval_real"));
	a_return = m_spScope->createRecord(m_aggregate->m_spLayout);

	m_asString.bind(m_spScope);

	m_mode = 0;

	if( !m_asVariable->check(a_return) )
	{
		return false;
	}
	else if(a_argv.size() < 1)
	{
		a_msg = "requires at least 1 argument";
		return false;
	}
	else if( !m_asString.check(a_argv[0]) )
	{
		a_msg = "arg must be string name";
		return false;
	}

	m_mode = 1;
	m_arg0 = m_asString.value(a_argv[0]);

	fe::String addr;
	fe::String attr;
	m_canonical.bind(a_rg);

	if(a_argv.size() >= 2 && m_asString.check(a_argv[1]))
	{
		m_arg1 = m_asString.value(a_argv[0]);
		m_mode = 2;
		addr = m_asString.value(a_argv[0]);
		attr = m_asString.value(a_argv[1]);
	}
	else
	{
		m_canonical.baseAddress(m_asString.value(a_argv[0]), addr, attr);
	}

	t_r_vector records;
	m_canonical.locate(records, addr);
	if(records.size() > 0)
	{
		m_record = records[0];
	}

	m_a_real.setup(m_spScope, attr);

	return true;
}

void CanonicalRealGet::eval(Record &a_return,
	ext::t_stdvector<WeakRecord> &a_argv)
{
	if(m_mode == 0) { return; }

	fe::String addr;
	fe::String attr;
	if(m_mode == 1)
	{
		if(m_asString.value(a_argv[0]) != m_arg0)
		{
			m_arg0 = m_asString.value(a_argv[0]);
			m_canonical.baseAddress(m_arg0, addr, attr);

			t_r_vector records;
			m_canonical.locate(records, addr);
			if(records.size() > 0) { m_record = records[0]; }

			m_a_real.setup(m_spScope, attr);
		}
	}
	else if(m_mode == 2)
	{
		if((m_asString.value(a_argv[0]) != m_arg0)
			|| (m_asString.value(a_argv[1]) != m_arg1))
		{
			m_arg0 = m_asString.value(a_argv[0]);
			m_arg1 = m_asString.value(a_argv[1]);
			addr = m_arg0;
			attr = m_arg1;

			t_r_vector records;
			m_canonical.locate(records, addr);
			if(records.size() > 0) { m_record = records[0]; }

			m_a_real.setup(m_spScope, attr);
		}
	}

	if(m_record.isValid() && m_a_real.check(m_record))
	{
		m_asVariable->value(a_return) = m_a_real.get(m_record);
	}
}


void CanonicalRealGet::initialize(void)
{
}


CanonicalRealSet::~CanonicalRealSet(void)
{
}

bool CanonicalRealSet::compile(sp<RecordGroup> a_rg, Record &a_return,
	t_stdvector<WeakRecord> &a_argv, t_stdstring &a_msg)
{
	m_rg = a_rg;
	m_spScope = a_rg->scope();

	m_aggregate = new Aggregate();
	m_asVariable = new AsVariable();
	m_aggregate->bakeTypes(m_asVariable, m_spScope->declare("l_eval_real"));
	a_return = m_spScope->createRecord(m_aggregate->m_spLayout);

	m_asString.bind(m_spScope);

	if( !m_asVariable->check(a_return) )
	{
		return false;
	}
	else if(a_argv.size() < 2)
	{
		a_msg = "requires at least 2 arguments";
		return false;
	}
	else if( !m_asString.check(a_argv[0]) )
	{
		a_msg = "arg must be string name";
		return false;
	}

	m_arg0 = m_asString.value(a_argv[0]);

	fe::String addr;
	fe::String attr;
	m_canonical.bind(a_rg);
	m_canonical.baseAddress(m_asString.value(a_argv[0]), addr, attr);

	t_r_vector records;
	m_canonical.locate(records, addr);
	if(records.size() > 0)
	{
		m_record = records[0];
	}

	m_a_real.setup(m_spScope, attr);

	return true;
}

void CanonicalRealSet::eval(Record &a_return,
	ext::t_stdvector<WeakRecord> &a_argv)
{
	fe::String addr;
	fe::String attr;

	if(m_asString.value(a_argv[0]) != m_arg0)
	{
		m_arg0 = m_asString.value(a_argv[0]);
		m_canonical.baseAddress(m_arg0, addr, attr);

		t_r_vector records;
		m_canonical.locate(records, addr);
		if(records.size() > 0) { m_record = records[0]; }

		m_a_real.setup(m_spScope, attr);
	}

	if(m_record.isValid() && m_a_real.check(m_record))
	{
		// set value in src/data
		m_a_real.set(m_record, m_asVariable->value(a_argv[1]));

		// same value as the return
		m_asVariable->value(a_return) = m_a_real.get(m_record);
	}
}


void CanonicalRealSet::initialize(void)
{
}

Concatenate::~Concatenate(void)
{
}

bool Concatenate::compile(sp<RecordGroup> a_rg, Record &a_return,
	t_stdvector<WeakRecord> &a_argv, t_stdstring &a_msg)
{
	m_rg = a_rg;
	m_spScope = a_rg->scope();

	m_stringAggregate = new Aggregate();
	m_asString = new AsString();
	m_stringAggregate->bakeTypes(m_asString,
		m_spScope->declare("l_eval_string"));

	a_return = m_spScope->createRecord(m_stringAggregate->m_spLayout);

	if( !m_asString->check(a_return) )
	{
		m_valid = false;
	}

	for(unsigned int i = 0; i < a_argv.size(); i++)
	{
		if( !m_asString->check(a_argv[i]) )
		{
			a_msg = "Concatenate argument not a string";
			m_valid = false;
			break;
		}
	}

	m_valid = true;

	return m_valid;
}

void Concatenate::eval(Record &a_return,
	ext::t_stdvector<WeakRecord> &a_argv)
{
	m_asString->value(a_return) = "";
	for(unsigned int i = 0; i < a_argv.size(); i++)
	{
		m_asString->value(a_return).cat(m_asString->value(a_argv[i]));
	}
}

void Concatenate::initialize(void)
{
}

EvaluatorStd::~EvaluatorStd(void)
{
}

void EvaluatorStd::initialize(void)
{
}

void EvaluatorStd::bind(sp<RecordGroup> a_rg)
{
	Evaluator::bind(a_rg);
	addFunction("smooth",	"FunctionI.Smooth");
	addFunction("compress",	"FunctionI.Compressor");
	addFunction("*",		"FunctionI.Multiply");
	addFunction("/",		"FunctionI.Divide");
	addFunction("+",		"FunctionI.Add");
	addFunction("-",		"FunctionI.Subtract");
	addFunction("peak",		"FunctionI.PeakValue");
	addFunction("abs",		"FunctionI.AbsoluteValue");
	addFunction("getreal",	"FunctionI.GetRealFromRecord.fe");
	addFunction("get",		"FunctionI.CanonicalRealGet.fe");
	addFunction("set",		"FunctionI.CanonicalRealSet.fe");
	addFunction("cat",		"FunctionI.Concatenate.fe");
	addFunction("global",	"FunctionI.GetGlobal.fe");
}

} /* namespace ext */
} /* namespace fe */
