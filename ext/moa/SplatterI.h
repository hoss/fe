/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __moa_SplatterI_h__
#define __moa_SplatterI_h__

namespace fe
{
namespace ext
{

/// @brief Interface to attach a 2D splat
class FE_DL_EXPORT SplatterI
	: virtual public Component,
	public CastableAs<SplatterI>
{
	public:
			SplatterI(void) { }
			/** Write an image file for the splat */
virtual		void							image(void)						= 0;
};


/// @brief Interface to attach a SurfaceI
class FE_DL_EXPORT GroundI
	: virtual public Component,
	public CastableAs<GroundI>
{
	public:
			GroundI(void) { }
virtual		void	setSurface(sp<SurfaceI> a_spSurface)					= 0;
virtual		void	setTransform(const SpatialTransform &a_xform)			= 0;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __moa_SplatterI_h__ */

