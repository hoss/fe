/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __moa_EvaluateSystem_h__
#define __moa_EvaluateSystem_h__

namespace fe
{
namespace ext
{

class FE_DL_EXPORT EvaluateSystem :
	virtual public SystemI,
	public Initialize<EvaluateSystem>
{
	public:
				EvaluateSystem(void);
virtual			~EvaluateSystem(void);
		void	initialize(void);

virtual	void	compile(const t_note_id &a_note_id);
virtual	void	perform(const t_note_id &a_note_id);
virtual	void	connectOrchestrator(sp<OrchestratorI> a_spOrchestrator);

		class EvaluateAction : public Counted
		{
			public:
				EvaluateAction(t_moa_real a_time,
					sp<Evaluator> a_evaluator)
				{
					m_time = a_time;
					m_spActionEval = a_evaluator;
				}
				sp<Evaluator> m_spActionEval;
				t_moa_real m_time;
		};


	private:

		AsEvaluateAction								m_asEvaluateAction;
		AsTime											m_asTime;
		WeakRecord									m_r_time;
		sp<RecordGroup>							m_rg_dataset;
		std::vector< sp<EvaluateAction> >			m_actions;
		t_moa_real										m_startTime;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __moa_EvaluateSystem_h__ */

