/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __moa_SimulatorLoopSystem_h__
#define __moa_SimulatorLoopSystem_h__

namespace fe
{
namespace ext
{

/**

*/
class SimulatorLoopSystem :
	virtual public SystemI,
	public Initialize<SimulatorLoopSystem>
{
	public:
				SimulatorLoopSystem(void);
virtual			~SimulatorLoopSystem(void);
		void	initialize(void);

virtual	void	start(const t_note_id &a_note_id);

virtual	void	connectOrchestrator(sp<OrchestratorI> a_spOrchestrator);

	private:
		void	performDataset(sp<RecordGroup> a_rg_dataset);

		sp<OrchestratorI>							m_spOrchestrator;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __moa_SimulatorLoopSystem_h__ */


