/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __moa_Orchestrator_h__
#define __moa_Orchestrator_h__

namespace fe
{
namespace ext
{

/// @brief Basic OrchestratorI implementation
class FE_DL_EXPORT Orchestrator :
	virtual public OrchestratorI,
	public Initialize<Orchestrator>
{
	public:
		Orchestrator(void);
virtual	~Orchestrator(void);
		void	initialize(void);

virtual	void		datasetInitialize(sp<RecordGroup> a_rg_dataset);
virtual	bool		compile(void);
virtual	void		perform(const char *a_label) { perform(note_id(a_label)); }
virtual	void		perform(const t_note_id &a_note_id);

virtual	t_note_id	note_id(const char *a_label);
virtual	t_note_id	connect(SystemI * a_system, const char *a_label);
virtual	t_note_id	connect(sp<SystemI> a_system,const char *a_label);

virtual	void		append(sp<SystemI> a_system);

virtual	void		setDT(t_moa_real a_dt);
virtual	t_moa_real	getDT(void);
virtual	t_moa_real	getTime(void);

virtual	void		step(const t_step_mode &a_step_mode, t_moa_real a_time);

virtual	sp<RecordGroup> dataset(void) { return rg_dataset; }

virtual	t_note_id	connect(t_note_perform a_perform, const char *a_label);

	private:
		sp<RecordGroup>		rg_dataset;

	private:
		typedef std::vector< sp<SystemI> >			t_system_array;

		typedef std::vector< sp<SystemI> >			t_systems;
		t_systems										m_systems;

		std::map<String, t_note_id>					m_note_map;

		t_note_id									m_note_step;
		t_note_id									m_note_compile;
		t_note_id									m_note_dt_change;

		typedef std::vector< t_note_perform >		t_perform_array;
		typedef std::vector< t_perform_array >		t_note_array;
		t_note_array								m_note_array;

		AsTime										m_asTime;
		Record										m_r_time;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __moa_Orchestrator_h__ */

