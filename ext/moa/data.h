/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __moa_data_h__
#define __moa_data_h__

namespace fe
{
namespace ext
{

typedef Real					t_moa_real;
typedef Vector<2, t_moa_real>	t_moa_v2;
typedef Vector<3, t_moa_real>	t_moa_v3;
typedef Vector<4, t_moa_real>	t_moa_v4;
typedef Matrix<3,3,t_moa_real>	t_moa_matrix;
typedef Matrix<3,4,t_moa_real>	t_moa_xform;

template<class AS>
class RecordDictionary : public std::map<String, Record>
{
	public:
		RecordDictionary(void)
		{
		}
		RecordDictionary(sp<RecordGroup> rg_dataset)
		{
			build(rg_dataset);
		}
		void build(sp<RecordGroup> rg_dataset)
		{
			asNamed.bind(rg_dataset->scope());

			//AS as;
			m_as.bind(rg_dataset->scope());

			std::vector<Record> records;
			asNamed.filter(records,rg_dataset);
			for(unsigned int i = 0; i < records.size(); i++)
			{
				if(m_as.check(records[i]))
				{
					if(this->find(asNamed.name(records[i])) != this->end())
					{
						fe_fprintf(stderr, "DUPLICATE [%s]\n",
							asNamed.name(records[i]).c_str());
					}
					(*this)[asNamed.name(records[i])] = records[i];
				}
			}
		}

		AS &as(void) { return m_as; }
		private:
			AsNamed asNamed;
			AS m_as;
};

template<typename T>
class RecordDictionaryByAccessor : public std::map<String, Record>
{
	public:
		RecordDictionaryByAccessor(void)
		{
		}
		RecordDictionaryByAccessor(sp<RecordGroup> rg_dataset,
			Accessor<T> &a_accessor)
		{
			load(rg_dataset, a_accessor);
		}
		void load(sp<RecordGroup> rg_dataset,
			Accessor<T> &a_accessor)
		{
			asNamed.bind(rg_dataset->scope());

			sp<TypeMaster> spTM = rg_dataset->scope()->typeMaster();
			sp<BaseType> spType =
				spTM->lookupType(TypeInfo(getTypeId<T>()));
			if(spType !=
				rg_dataset->scope()->attribute(a_accessor.index())->type())
			{
				fe_fprintf(stderr, "accessor type mismatch\n");
			}
			else
			{
				std::vector<Record> records;
				asNamed.filter(records,rg_dataset);
				for(unsigned int i = 0; i < records.size(); i++)
				{
					if(a_accessor.check(records[i]))
					{
						if(this->find(asNamed.name(records[i])) != this->end())
						{
							fe_fprintf(stderr, "DUPLICATE [%s]\n",
								asNamed.name(records[i]).c_str());
							assert(false);
						}
						(*this)[asNamed.name(records[i])] = records[i];
					}
				}
			}
		}
		private:
			AsNamed asNamed;
};


template <typename A, typename B>
void enforce(sp<RecordGroup> a_dataset)
{
	A asA;
	asA.bind(a_dataset->scope());
	B asB;
	asB.bind(a_dataset->scope());
	asA.enforceHaving(asB);
}


typedef std::atomic<t_moa_real>	t_atomic_real;


class FE_DL_EXPORT InfoAtomicReal : public BaseType::Info
{
	public:
virtual	IWORD	output(std::ostream &ostrm, void *instance, t_serialMode mode)
{
	t_moa_real moa_real = *(t_atomic_real *)instance;
	if(mode == e_binary)
	{
		ostrm.write((char *)&moa_real, sizeof(t_moa_real));
		return sizeof(t_moa_real);
	}
	else
	{
		ostrm << moa_real;
		return c_ascii;
	}
}

virtual	void	input(std::istream &istrm, void *instance, t_serialMode mode)
{
	t_moa_real moa_real=0.0f;
	if(mode == e_binary)
	{
		istrm.read((char *)&moa_real, sizeof(t_moa_real));
	}
	else
	{
		istrm >> moa_real;
	}
	*(t_atomic_real *)instance = moa_real;
}

virtual	String	print(void *instance)
{
	t_moa_real moa_real= *(t_moa_real *)instance;
	String string;
	string.sPrintf("%.6G",moa_real);
	return string;
}

virtual	void	construct(void *instance)
{
	*(t_atomic_real *)instance=0.0;
}

virtual	IWORD	iosize(void)
{
	return sizeof(t_atomic_real);
}


};

// prevent common bug of assigning when should be accumulating
class t_accum_v3
{
	public:
		t_accum_v3(void) { clear(); }
		~t_accum_v3(void) { }
		void	clear(void)
		{
			m_mutex.lock();
			m_data = t_moa_v3(0.0,0.0,0.0);
			m_mutex.unlock();
		}
		void	add(const t_moa_v3 &a_add)
		{
			m_mutex.lock();
			m_data = m_data + a_add;
			m_mutex.unlock();
		}

		bool operator == (t_accum_v3 &a_other)
		{
			bool rv;
			m_mutex.lock();
			a_other.m_mutex.lock();
			rv = (m_data == a_other.m_data);
			a_other.m_mutex.unlock();
			m_mutex.unlock();
			return rv;
		}
		bool operator == (t_moa_v3 &a_other)
		{
			bool rv;
			m_mutex.lock();
			rv = (m_data == a_other.m_data);
			m_mutex.unlock();
			return rv;
		}
		void read(t_moa_v3 &a_v3)
		{
			m_mutex.lock();
			a_v3 = m_data;
			m_mutex.unlock();
		}

		t_moa_v3			m_data;
		RecursiveMutex	m_mutex;
};


class InfoVectorAccum : public BaseType::Info
{
virtual	String	print(void *instance)
				{
					const t_accum_v3 *pV = (t_accum_v3 *)instance;
					String string;
					for(U32 n=0;n<3;n++)
						string.catf("%.6G%s",(*pV).m_data[n],(n==3-1)? "": " ");
					return string;
				}

virtual	IWORD	output(std::ostream &ostrm, void *instance, t_serialMode mode)
		{
			if(mode == e_ascii)
			{
				ostrm << '"';
			}
			IWORD n = 0;
			t_accum_v3 *pV = (t_accum_v3 *)instance;
			for(int i = 0; i < 3; i++)
			{
				t_moa_real t=(*pV).m_data[i];
				if(i && mode == e_ascii)
				{
					ostrm << ' ';
				}
				n += helpF.output(ostrm, (void *)&t, mode);
			}
			if(mode == e_ascii)
			{
				ostrm << '"';
			}
			return (mode == e_ascii)? c_ascii: n;
		}
virtual	void	input(std::istream &istrm, void *instance, t_serialMode mode)
		{
			t_accum_v3 *pV = (t_accum_v3 *)instance;
			for(int i = 0; i < 3; i++)
			{
				t_moa_real t=0.0f;
				helpF.input(istrm, (void *)&t, mode);
				setAt(pV->m_data,i,t);
			}
		}
virtual	IWORD	iosize(void) { return sizeof(t_accum_v3); }
virtual	FE_UWORD	alignment(void)
				{
#if FE_MEM_ALIGNMENT
					return (3*sizeof(t_moa_real)==FE_MEM_ALIGNMENT)?
							FE_MEM_ALIGNMENT: 0;
#else
					return 0;
#endif
				}
virtual	bool	getConstruct(void) { return true; }
virtual	void	construct(void *instance)
				{	new(instance)t_accum_v3; }
virtual	void	destruct(void *instance)
				{	((t_accum_v3 *)instance)->~t_accum_v3();}
	private:
#if FE_DOUBLE_REAL
		InfoF64 helpF;
#else
		InfoF32 helpF;
#endif
};

typedef std::function<void(const int &)> t_note_perform_tmp;


class InfoNotePerform : public BaseType::Info
{
virtual	String	print(void *instance)
				{
					return "--";
				}

virtual	IWORD	output(std::ostream &ostrm, void *instance, t_serialMode mode)
		{
			if(mode == e_ascii) { return c_noascii; }
			return 0;
		}
virtual	void	input(std::istream &istrm, void *instance, t_serialMode mode)
		{
		}
virtual	IWORD	iosize(void) { return c_implicit; }
virtual	bool	getConstruct(void) { return true; }
virtual	void	construct(void *instance)
				{	new(instance)t_note_perform_tmp; }
virtual	void	destruct(void *instance)
				{	((t_note_perform_tmp *)instance)->~t_note_perform_tmp();}
	private:
};

template<typename T>
Record recordFind(sp<RecordGroup> rg_dataset,
	const String &a_name)
{
	RecordDictionary<T> dictionary(rg_dataset);
	return dictionary[a_name];
}

template<typename AS>
Record createSignal(sp<RecordGroup> a_rg_dataset,
	const String &a_name)
{
	AS asAS;
	asAS.bind(a_rg_dataset->scope());
	sp<Layout> l_new = a_rg_dataset->scope()->declare(a_name);
	asAS.populate(l_new);
	Record r_new = a_rg_dataset->scope()->createRecord(l_new);
	return r_new;
}

#if 0
inline bool attachAccessorSet(AccessorSet &a_as,
	sp<RecordGroup> a_rg_dataset, const String &a_name)
{
	AsNamed asNamed;
	std::vector<Record> records;
	asNamed.filter(records,a_rg_dataset);
	for(unsigned int i = 0; i < records.size(); i++)
	{
		if(asNamed.name(records[i]) == a_name)
		{
			if(a_as.check(records[i]))
			{
				a_as.attach(records[i]);
				return true;
			}
		}
	}
	return false;
}
#endif

inline void split(std::vector<String> &a_tokens, String a_input,
	String a_delim)
{
	String buffer=a_input.c_str();
	String token;
	while(!(token=buffer.parse("",a_delim)).empty())
	{
		a_tokens.push_back(token);
	}
}

inline void splitAll(std::vector<String> &a_tokens, String a_input,
	String a_delim)
{
	String buffer=a_input.c_str();
	String token;
	while(!(token=buffer.parse("",a_delim,TRUE)).empty())
	{
		a_tokens.push_back(token);
	}
}

inline void split(std::list<String> &a_tokens, String a_input,
	String a_delim)
{
	String buffer=a_input.c_str();
	String token;
	while(!(token=buffer.parse("",a_delim)).empty())
	{
		a_tokens.push_back(token);
	}
}


inline void overlay(Record r_dst, Record r_src)
{
	const sp<Scope> &spScope = r_src.layout()->scope();

	t_bitset a_bitset =	r_src.layout()->bitset() &
						r_dst.layout()->bitset();
	for(unsigned int i_a = 0; i_a < a_bitset.size(); i_a++)
	{
		if(a_bitset[i_a] &&
			spScope->attribute(i_a)->isCloneable())
		{
			// make sure :CT and :SN don't hit
			FEASSERT(i_a != 0);
			FEASSERT(i_a != 1);

			void *dst = r_dst.rawAttribute(i_a);
			void *src = r_src.rawAttribute(i_a);
			spScope->attribute(i_a)->type()->assign(dst, src);
		}
	}
}



/** @brief merge a RecordGroup over another RecordGroup.

*/
inline void overlay(sp<RecordGroup> a_rg_data,
	const sp<RecordGroup> &a_rg_overlay)
{
//TODO:X fix this painfully slow A-Z only implementation

	if(!a_rg_data.isValid()) { return; }
	if(!a_rg_overlay.isValid()) { return; }

	AsNamed asNamed;
	asNamed.bind(a_rg_data->scope());

	// load all named records from target to a map
	std::map< String, Record > named_dataset;
	for(RecordGroup::iterator i_rg = a_rg_data->begin();
		i_rg != a_rg_data->end(); i_rg++)
	{
		sp<RecordArray> spRA = *i_rg;
		if(asNamed.check(spRA))
		{
			for(int i = 0; i < spRA->length(); i++)
			{
				Record r_named = spRA->getRecord(i);
				named_dataset[asNamed.name(r_named)] = r_named;
			}
		}
	}

	// iterate through records in the overlay
	for(RecordGroup::iterator i_rg_overlay = a_rg_overlay->begin();
		i_rg_overlay != a_rg_overlay->end(); i_rg_overlay++)
	{
		sp<RecordArray> spRAoverlay = *i_rg_overlay;

		sp<Layout> spLayout=spRAoverlay->layout();
		sp<RecordArray> spRAdataset = a_rg_data->getArray(spLayout);

		// For named records
		if(asNamed.check(spRAoverlay))
		{
			for(int i = 0; i < spRAoverlay->length(); i++)
			{
				Record record=spRAoverlay->getRecord(i);

				std::map< String, Record >::iterator i_named =
					named_dataset.find(asNamed.name(record));

				// if both RGs have the same named Record, overlay per attribute
				if(i_named != named_dataset.end())
				{



					Record r_overlay = spRAoverlay->getRecord(i);
					Record r_dataset = i_named->second;

					overlay(r_dataset, r_overlay);
				}
				// if the target RG doesn't have a match, just add the new one
				else
				{
					spRAdataset->addCovert(record);
					a_rg_data->watcherAdd(record);
				}
			}
		}
		// For unnamed Records, just add
		else
		{
			for(int i = 0; i < spRAoverlay->length(); i++)
			{
				Record record=spRAoverlay->getRecord(i);
				spRAdataset->addCovert(record);
				a_rg_data->watcherAdd(record);
			}
		}
	}
}


typedef std::vector<Record> t_r_vector;

/** @brief Get a vector of Records using a string "address"
*/
class FE_DL_EXPORT Canonical
{
	public:
		Canonical(void) {}
virtual	~Canonical(void);
virtual	void	bind(sp<RecordGroup> a_rg_dataset)
		{
			m_rg_dataset = a_rg_dataset;
		}

		template<typename AS>
		void	addX(const String &a_key)
		{
			sp<AS> spAS(new AS());
			spAS->bind(m_rg_dataset->scope());
			m_accessorSets[a_key] = spAS;
		}

		class Op : virtual public Counted, public CastableAs<Op>
		{
			public:
				Op(void) {}
virtual			~Op(void) {}
virtual			void compile(sp<RecordGroup> a_rg_dataset)			= 0;
virtual			void process(const t_r_vector &a_in, t_r_vector &a_out)		= 0;
		};

		class Filter : virtual public Counted, public CastableAs<Filter>
		{
			public:
				Filter(void) {}
virtual			~Filter(void) {}
virtual			void compile(sp<RecordGroup> a_rg_dataset)			= 0;
virtual			bool filter(const String &a_arg,
					const t_r_vector &a_in, t_r_vector &a_out)				= 0;
		};

		class AccessorSetOp :
			virtual public Op, public CastableAs<AccessorSetOp>
		{
			public:
				AccessorSetOp(sp<AccessorSet> a_as);
virtual			~AccessorSetOp(void);
virtual			void compile(sp<RecordGroup> a_rg_dataset);
virtual			void process(const t_r_vector &a_in, t_r_vector &a_out);

			private:
				sp<RecordGroup> m_rg_dataset;
				t_r_vector				m_r_vector;
				sp<AccessorSet>	m_as;
		};


		template<typename AS>
		void	add(const String &a_key)
		{
			sp<AS> spAS(new AS());
			sp<AccessorSetOp> spASOP(new AccessorSetOp(spAS));
			spASOP->compile(m_rg_dataset);
			m_op_map[a_key] = spASOP;
		}

		class BondOp : virtual public Op, public CastableAs<BondOp>
		{
			public:
				BondOp(const String &a_left, const String &a_right);
virtual			~BondOp(void);
virtual			void compile(sp<RecordGroup> a_rg_dataset);
virtual			void process(const t_r_vector &a_in, t_r_vector &a_out);

			private:
				String				m_left;
				String				m_right;
				sp<RecordGroup> m_rg_dataset;
				t_r_vector				m_r_vector;
		};

		void addBond(const String &a_key, const String &a_left,
			const String &a_right);

		class RecordGroupOp :
			virtual public Op, public CastableAs<RecordGroupOp>
		{
			public:
				RecordGroupOp(sp<RecordGroup> a_rg_inject);
virtual			~RecordGroupOp(void);
virtual			void compile(sp<RecordGroup> a_rg_dataset);
virtual			void process(const t_r_vector &a_in, t_r_vector &a_out);

			private:
				sp<RecordGroup> m_rg_inject;
				t_r_vector				m_r_vector;
		};

		void addRecordGroup(const String &a_key,
			sp<RecordGroup> a_rg_inject);

		class NameFilter :
			virtual public Filter, public CastableAs<NameFilter>
		{
			public:
				NameFilter(void);
virtual			~NameFilter(void);
virtual			void compile(sp<RecordGroup> a_rg_dataset);
virtual			bool filter(const String &a_arg,
					const t_r_vector &a_in, t_r_vector &a_out);
			private:
				AsNamed	m_asNamed;
		};

		class IndexFilter :
			virtual public Filter, public CastableAs<IndexFilter>
		{
			public:
				IndexFilter(void);
virtual			~IndexFilter(void);
virtual			void compile(sp<RecordGroup> a_rg_dataset);
virtual			bool filter(const String &a_arg,
					const t_r_vector &a_in, t_r_vector &a_out);
			private:
		};
		void append(sp<Filter> a_filter)
		{
			m_op_vector.push_back(a_filter);
			a_filter->compile(m_rg_dataset);
		}

		void locate(t_r_vector &a_records, const String &a_address);

		void baseAddress(const String &a_address, String &a_base,
			String &a_attr);

		static bool arrayIndex(const String &a_input,
			String &a_trimmed, unsigned int &a_index);

		template<typename T>
		T *find(const String &a_address)
		{
			String base;
			String attr;
			baseAddress(a_address, base, attr);

			t_r_vector records;
			locate(records, base);
			if(records.size() == 0) { return NULL; }

			// final token is for bridging access to caller
			Accessor<T> accessor(m_rg_dataset->scope(), attr);
			for(unsigned int i = 0; i < records.size(); i++)
			{
				if(accessor.check(records[i]))
				{
					return &(accessor(records[i]));
				}
			}

			return NULL;
		}

		typedef std::map< String, sp<AccessorSet> >	t_asets;
		typedef std::map< String, sp<Op> >				t_op_map;
		typedef std::vector< sp<Filter> >					t_op_vector;
		sp<RecordGroup>									m_rg_dataset;
		t_asets													m_accessorSets;
		t_op_map												m_op_map;
		t_op_vector												m_op_vector;
};

class FE_DL_EXPORT CanonicalStd : public Canonical
{
	public:
		CanonicalStd(void) { }
virtual	~CanonicalStd(void) { }
virtual	void	bind(sp<RecordGroup> a_rg_dataset);
};

class FE_DL_EXPORT RealAccessor
{
	public:
		RealAccessor(void) {}

		void		setup(sp<Scope> a_scope, const String &a_name);
		bool		check(const Record& r_hasreal) const;

		void		set(const Record& r_hasreal, t_moa_real a_v);
		t_moa_real	get(const Record& r_hasreal);

	private:
		Accessor<t_moa_real>	m_a_real;
		Accessor<t_moa_v2>		m_a_v2;
		Accessor<t_moa_v3>		m_a_v3;
		Accessor<t_moa_v4>		m_a_v4;
		Accessor<t_atomic_real>	m_a_atomic;
		Accessor<int>			m_a_int;
		unsigned int				m_mode;
		unsigned int				m_index;
};

inline sp<RecordGroup> clone(sp<RecordGroup> a_rg)
{
	Cloner cloner(a_rg->scope());
	return cloner.clone(a_rg);
}

class FE_DL_EXPORT StreamableI
	: virtual public Component,
	public CastableAs<StreamableI>
{
	public:
virtual	void	output(std::ostream &a_ostrm)								= 0;
virtual	void	input(std::istream &a_istrm)								= 0;
};



template <typename T>
class InfoI : public BaseType::Info
{
	public:
		InfoI(sp<Registry> a_spRegistry)
		{
			m_spRegistry = a_spRegistry;
		}
		sp<Registry> m_spRegistry;
typedef sp<T>		t_interface;
typedef sp<StreamableI> t_streamable;
virtual	IWORD	output(std::ostream &ostrm, void *instance, t_serialMode mode)
		{
			IWORD n = 0;
			if(mode == e_ascii)
			{
				t_interface &spInterface = *(t_interface *)instance;
				t_streamable spStreamable = spInterface;
				if(spInterface.isValid() && spStreamable.isValid())
				{

					ostrm << "\"";
					ostrm << spInterface->name().c_str();
					ostrm << " \'";
					spStreamable->output(ostrm);
					ostrm << "\'\"";
				}
				else
				{
					ostrm << "\"invalid interface\"";
				}
			}
			return (mode == e_ascii)? c_ascii: n;
		}
virtual	void	input(std::istream &istrm, void *instance, t_serialMode mode)
		{
			std::string s;
			char c;
			while(istrm.get(c))
			{
				if(c == '\0')
				{
					break;
				}
				s.push_back(c);
			}
			String buffer = s.c_str();

			std::vector< std::string > words;

			while(!buffer.empty())
			{
				String word=buffer.parse("'");
				if(!word.empty())
				{
					words.push_back(word.c_str());
				}
			}

			if(words.size() >= 2)
			{
				t_interface &spInterface = *(t_interface *)instance;

				spInterface = m_spRegistry->create(words[0].c_str());

				t_streamable spStreamable = spInterface;
				if(spInterface.isValid() && spStreamable.isValid())
				{
					std::istringstream istrm(words[1]);
					spStreamable->input(istrm);
				}
			}
		}
virtual	IWORD	iosize(void) { return BaseType::Info::c_implicit; }
virtual	bool	getConstruct(void) { return true; }
virtual	void	construct(void *instance)
				{	new(instance)t_interface; }
virtual	void	destruct(void *instance)
				{	((t_interface *)instance)->~t_interface();}
};


template<typename T>
void assertInterface(sp<Master> a_spMaster, const char *a_name)
{
	sp<BaseType> spT;
	spT = a_spMaster->typeMaster()->assertType< sp<T> >(a_name);
	spT->setInfo(new InfoI<T>(a_spMaster->registry()));
}

#define AsConstruct(AS)														\
		AS(void) {}															\
		AS(Record &r){ if(r.isValid()) { bind(r.layout()->scope()); }}	\
		AS(sp<Scope> a_spScope){ bind(a_spScope); }

class AsDatasetMeta
	: public AccessorSet, public Initialize<AsDatasetMeta>
{
	public:
		AsConstruct(AsDatasetMeta);
		void initialize(void)
		{
			add(dictionary,		FE_USE("dataset:dictionary"));
		}
		Accessor< std::map<String,String> >	dictionary;

		void digest(sp<RecordGroup> a_dataset)
		{
			m_dataset = a_dataset;
			bind(a_dataset->scope());
			m_all_map.clear();
			m_r_first_meta = Record();
			std::vector<Record>	metas;
			filter(metas, a_dataset);
			for(Record &r_meta : metas)
			{
				if(!m_r_first_meta.isValid())
				{
					m_r_first_meta = r_meta;
				}
				for(std::map<String,String>::iterator i_map =
					dictionary(r_meta).begin();
					i_map != dictionary(r_meta).end(); i_map++)
				{
					m_all_map[i_map->first] = r_meta;
				}
			}
		}
		String get(const String &a_key)
		{
			std::map<String,Record>::iterator i_map =
				m_all_map.find(a_key);

			if(i_map == m_all_map.end())
			{
				digest(m_dataset);
				i_map = m_all_map.find(a_key);
				if(i_map == m_all_map.end())
				{
					return String("__unset__") + a_key;
				}
			}

			return dictionary(i_map->second)[a_key];
		}
		void set(const String &a_key, const String &a_value)
		{
			std::map<String,Record>::iterator i_map =
				m_all_map.find(a_key);
			if(i_map == m_all_map.end())
			{
				if(!m_r_first_meta.isValid())
				{
					std::vector<Record> metas;
					filter(metas, m_dataset);
					if(metas.size() > 0)
					{
						m_r_first_meta = metas[0];
					}
					else
					{
						sp<Layout> l_first =
							m_dataset->scope()->declare("AUTOMATIC_META");
						populate(l_first);
						m_r_first_meta =
							m_dataset->scope()->createRecord(l_first);
						m_dataset->add(m_r_first_meta);
					}
				}
				dictionary(m_r_first_meta)[a_key] = a_value;
				m_all_map[a_key] = m_r_first_meta;
			}

			dictionary(m_all_map[a_key])[a_key] = a_value;

		}
		Record						m_r_first_meta;
		std::map<String,Record>	m_all_map;
		sp<RecordGroup>			m_dataset;

};

/// Associates a name with a RecordGroup
class AsDataset
	: public AsNamed, public Initialize<AsDataset>
{
	public:
		AsConstruct(AsDataset);
		void initialize(void)
		{
			add(dataset,				FE_USE("dataset:group"));
		}
		Accessor< sp<RecordGroup> >		dataset;
};


class AsDatasetControl
	: public AsNamed, public Initialize<AsDatasetControl>
{
	public:
		AsConstruct(AsDatasetControl);
		void initialize(void)
		{
			add(active,				FE_USE("dataset:active"));
		}
		Accessor< bool >		active;
};

class AsDatasetMixer
	: public AsNamed, public Initialize<AsDatasetMixer>
{
	public:
		AsConstruct(AsDatasetMixer);
		void initialize(void)
		{
			add(inputs,				FE_USE("mixer:inputs"));
		}
		Accessor< Array<String> >		inputs;
};

class AsDatasetManyToMany
	: public AccessorSet, public Initialize<AsDatasetManyToMany>
{
	public:
		AsConstruct(AsDatasetManyToMany);
		void initialize(void)
		{
			add(left,				FE_USE("mixer:left"));
			add(right,				FE_USE("mixer:right"));
			add(active,				FE_USE("dataset:active"));
		}
		Accessor< Array<String> >		left;
		Accessor< Array<String> >		right;
		Accessor< bool >						active;
};

class AsAspect
	: public AccessorSet, public Initialize<AsAspect>
{
	public:
		AsConstruct(AsAspect);
		void initialize(void)
		{
			add(label,				FE_USE("aspect:label"));
		}
		Accessor<String>					label;
};

class FE_DL_EXPORT Overlayer
{
	public:
		Overlayer(sp<Scope> a_spScope);
virtual	~Overlayer(void) {}


		void load(const char *a_str)
		{
			sp<data::StreamI> spStreamDataset;
			std::ifstream strm(a_str);
			spStreamDataset=new data::AsciiStream(m_rg_baseline->scope());
			sp<RecordGroup> rg_file = spStreamDataset->input(strm);
			strm.close();
			m_rg_baseline->add(rg_file);
		}
		void load(sp<RecordGroup> a_rg_file)
		{
			m_rg_baseline->add(a_rg_file);
		}

		std::vector< sp<RecordGroup> > &bakedDatasets(void)
		{
			if(!m_built)
			{
				build();
			}
			return m_rgs_baked;
		}

		RecordDictionary<AsDataset>	&datasets(void) { return m_datasets; }

		sp<RecordGroup> baseline(void) { return m_rg_baseline; }

		void build(void);
	private:
		void manyToMany(void);
		void mixdown(void);
		void bakeControls(void);

	private:
		RecordDictionary<AsDataset>				m_datasets;
		sp<RecordGroup>					m_rg_baseline;
		std::vector< sp<RecordGroup> >	m_rgs_baked;
		sp<Scope>						m_spScope;
		bool									m_built;
};

inline void loadFileIntoRG(sp<RecordGroup> a_spRG, const char *a_str)
{
	sp<data::StreamI> spStreamDataset;
	std::ifstream strm(a_str);
	spStreamDataset=new data::AsciiStream(a_spRG->scope());
	sp<RecordGroup> rg_file = spStreamDataset->input(strm);
	strm.close();
	a_spRG->add(rg_file);
}

#define		e_unnl		(int)(1<<0)
#define		e_red		(int)(1<<1)
#define		e_green		(int)(1<<2)
#define		e_yellow	(int)(1<<3)
#define		e_blue		(int)(1<<4)
#define		e_magenta	(int)(1<<5)
#define		e_cyan		(int)(1<<6)
#define		e_white		(int)(1<<7)

void dump(const char *a_msg);
void dump(int a_flags, const char *a_msg);
//void dump(const char *a_msg, int a_i);
//void dump(int a_flags, const char *a_msg, int a_i);
void dump(const char *a_msg, t_moa_real a_r);
void dump(int a_flags, const char *a_msg, t_moa_real a_r);
void dump(const char *a_msg, const char *a_txt);
void dump(int a_flags, const char *a_msg, const char *a_txt);
void dump(const char *a_msg, const t_moa_v3 &a_v3);
void dump(int a_flags, const char *a_msg, const t_moa_v3 &a_v3);
void dump(const char *a_msg, const t_moa_xform &a_xform);
void dump(int a_flags, const char *a_msg, const t_moa_xform &a_xform);

void dump(const char *a_msg, Record a_r);
void dump(int a_flags, const char *a_msg, Record a_r);

} /* namespace ext */
} /* namespace fe */

// t_atomic_real needs some type system specialization to deal with restrictions
namespace fe
{

template <>
class FE_DL_EXPORT TypeVector<::fe::ext::t_atomic_real> :
	public BaseTypeVector
	//, public CastableAs< TypeVector<int> >
{
	public:
		TypeVector(void)
		{
			m_base = NULL;
			m_size = sizeof(::fe::ext::t_atomic_real);
			m_count_alloc = 0;
			m_count_used = 0;
			m_vector = NULL;
		}
virtual	~TypeVector(void)
		{
			if(m_vector) { delete [] m_vector; }
		}

virtual void resize(unsigned int a_size)
		{
			if(a_size == 0)
			{
				if(m_vector)
				{
					delete [] m_vector;
					m_vector = NULL;
					m_base = NULL;
					m_count_alloc = 0;
				}
			}
			else if(a_size > m_count_used)
			{
				if(a_size > m_count_alloc)
				{
					unsigned int new_alloc = 1;
					if(m_count_alloc > 0) { new_alloc = m_count_alloc * 2; }
					::fe::ext::t_atomic_real *from = m_vector;
					m_vector = new ::fe::ext::t_atomic_real[new_alloc];
					m_base = m_vector;
					for(unsigned int i = 0; i < m_count_used; i++)
					{
						::fe::ext::t_moa_real intermediary = from[i];
						m_vector[i] = intermediary;
					}
					delete [] from;
					m_count_alloc = new_alloc;
				}
				for(unsigned int i = m_count_used; i < a_size; i++)
				{
					m_vector[i] = 0.0;
				}
			}
			m_count_used = a_size;
		}

virtual void *raw_at(unsigned int a_index)
		{
			return (void *)&m_vector[a_index];
		}

		::fe::ext::t_atomic_real &at(unsigned int a_index)
		{
			return m_vector[a_index];
		}

	private:
		::fe::ext::t_atomic_real	*m_vector;
		unsigned int m_count_alloc;
		unsigned int m_count_used;
};


template <>
inline void Type<::fe::ext::t_atomic_real>::assign(void *lvalue, void *rvalue)
{
	::fe::ext::t_moa_real moa_real =
		*(reinterpret_cast<::fe::ext::t_atomic_real *>(rvalue));
	*(reinterpret_cast<::fe::ext::t_atomic_real *>(lvalue)) = moa_real;
}

template <>
inline sp<BaseTypeVector> Type<::fe::ext::t_atomic_real>::createVector(void)
{
	sp< BaseTypeVector > spBTV(new TypeVector<::fe::ext::t_atomic_real>);
	return spBTV;
}

class EvalRange
{
	public:
		EvalRange(const Array<String> &a_args)
		{
			assert(a_args.size() >= 3);

			m_mode = 0;
			m_start = a_args[0].real();
			m_end = a_args[1].real();
			if(a_args[2].c_str()[0] == '*')
			{
				String n = a_args[2].prechop("*");
				m_incr = n.real();
				m_mode = 1;
			}
			else
			{
				m_incr = a_args[2].real();
			}
		}
		EvalRange(void)
		{
			m_mode = 0;
			m_start = 0.0;
			m_end = 0.0;
			m_incr = 1.0;
		}

		class iterator
		{
			friend class EvalRange;
			public:
			iterator(void)
			{
				m_value = 0.0;
				m_end = true;
			}
			iterator(const iterator& it)
			{
				m_value = it.m_value;
				m_end = it.m_end;
			}
			::fe::ext::t_moa_real	&operator*(void)
			{
				return m_value;
			}
			::fe::ext::t_moa_real	*operator->(void)
			{
				return &m_value;
			}
			iterator	operator++()
			{
				increment();
				check_for_end();
				return *this;
			}
			iterator	operator++(int dummy)
			{
				increment();
				check_for_end();
				return *this;
			}
			bool		operator==(const iterator &other)
			{
				return (m_end == other.m_end);
			}
			bool		operator!=(const iterator &other)
			{
				return (m_end != other.m_end);
			}

			void	increment(void)
			{
				if(m_pRange->m_mode == 1)
				{
					m_value = m_value * m_pRange->m_incr;
				}
				else
				{
					m_value = m_value + m_pRange->m_incr;
				}
			}
			void	check_for_end(void)
			{
				if(m_pRange->m_end >= m_pRange->m_start)
				{
					if(m_value > m_pRange->m_end) { m_end = true; }
				}
				else
				{
					if(m_value < m_pRange->m_end) { m_end = true; }
				}
			}

			::fe::ext::t_moa_real	m_value;
			bool		m_end;
			EvalRange	*m_pRange;
		};

		iterator	begin(void)
		{
			iterator it;
			it.m_end = false;
			it.m_value = m_start;
			it.m_pRange = this;
			it.check_for_end();
			return it;
		}
		iterator	end(void)
		{
			iterator it;
			it.m_pRange = this;
			return it;
		}

		::fe::ext::t_moa_real operator[] (int i)
		{
			if(i == 0) { return m_start; }
			else if(i == 1) { return m_end; }
			else if(i == 2) { return m_incr; }
			return 0.0;
		}

		::fe::ext::t_moa_real	m_start;
		::fe::ext::t_moa_real	m_end;
		::fe::ext::t_moa_real	m_incr;
		int					m_mode;
};

template<>
inline bool Type<::fe::ext::t_note_perform_tmp>::equiv(void *a_a, void *a_b)
{
	return true;
}

inline void snapshot(sp<RecordGroup> a_spRG, const char *a_str)
{
	sp<data::StreamI> spStream;
	spStream=new data::AsciiStream(a_spRG->scope());
	std::ofstream asciifile(a_str);
	spStream->output(asciifile, a_spRG);
	asciifile.close();
}

} /* namespace fe */

#endif /* __moa_data_h__ */

