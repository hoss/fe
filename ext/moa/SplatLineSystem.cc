/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "moa/moa.pmh"

namespace fe
{
namespace ext
{

SplatterSystem::SplatterSystem(void)
{
}

SplatterSystem::~SplatterSystem(void)
{
}

void SplatterSystem::initialize(void)
{
}

void	SplatterSystem::connectOrchestrator(fe::sp<OrchestratorI> a_spOrchestrator)
{
	m_note_datapoint = a_spOrchestrator->connect(this,
		&SystemI::perform, FE_NOTE_DATAPOINT);
	m_note_run_stop = a_spOrchestrator->connect(this,
		&SystemI::perform, FE_NOTE_RUN_STOP);
	m_note_line_start = a_spOrchestrator->connect(this,
		&SystemI::perform, FE_NOTE_LINE_START);
	m_note_line_stop = a_spOrchestrator->connect(this,
		&SystemI::perform, FE_NOTE_LINE_STOP);
	m_note_loop_start = a_spOrchestrator->connect(this,
		&SystemI::perform, FE_NOTE_LOOP_START);
	m_note_loop_stop = a_spOrchestrator->connect(this,
		&SystemI::perform, FE_NOTE_LOOP_STOP);
	m_note_dt_change = a_spOrchestrator->connect(this,
		&SystemI::perform, FE_NOTE_DT_CHANGE);
	m_note_image = a_spOrchestrator->connect(this,
		&SystemI::perform, FE_NOTE_IMAGE);

	a_spOrchestrator->connect(this, &SplatterSystem::compile, FE_NOTE_COMPILE);
	a_spOrchestrator->connect(this, &SplatterSystem::compile, "recompile");

	rg_dataset = a_spOrchestrator->dataset();
}

SplatterSystem::Context::Context(void)
{
	m_y_max = t_moa_v3(-1.0e10,0.0,0.0);
	m_line_started = false;
}

void SplatterSystem::compile(const t_note_id &a_note_id)
{
	m_asDatasetMeta.digest(rg_dataset);

	m_asSplat.bind(rg_dataset->scope());
	std::vector<Record>	splats;
	m_asSplat.filter(splats, rg_dataset);

	m_contexts.clear();
	m_splatMap.clear();

	for(unsigned int i_splat = 0; i_splat < splats.size(); i_splat++)
	{
		Record &r_splat = splats[i_splat];

		sp<Context> spContext(new Context());

		if(!match(r_splat))
		{
			continue;
		}

		String key;
		if(m_asSplat.splat(r_splat) == "")
		{
			key = m_asSplat.name(r_splat);
		}
		else
		{
			key = m_asSplat.splat(r_splat);
		}

		t_splatmap::iterator i_c_splat = m_splatMap.find(key);
		if(i_c_splat == m_splatMap.end())
		{
			sp<t_image_splat> spSp(new t_image_splat());
			spSp = new t_image_splat();
			spSp->create(t_image_splat::t_index(512,512),
				t_moa_v3(0.0,0.0,0.0));
			m_splatMap[key].m_splat = spSp;
		}
		m_splatMap[key].m_contexts.push_back(spContext);
		spContext->m_splat = m_splatMap[key].m_splat;

		sp<ext::Evaluator> spMyEval;

		spMyEval = registry()->create("EvaluatorI.EvaluatorStd.fe");
		spMyEval->bind(rg_dataset);

		sp<AsVariable> asVariable = spMyEval->asVariable();

		spContext->m_spMyEval = spMyEval;
		spContext->r_x = spMyEval->create("X", asVariable);
		spContext->r_y = spMyEval->create("Y", asVariable);
		spContext->r_r = spMyEval->create("R", asVariable);
		spContext->r_g = spMyEval->create("G", asVariable);
		spContext->r_b = spMyEval->create("B", asVariable);
		spContext->m_parent = this;

		t_moa_v3 parent_color = m_asSplat.color(r_splat);
		spMyEval->asVariable()->value(spContext->r_r) = parent_color[0];
		spMyEval->asVariable()->value(spContext->r_g) = parent_color[1];
		spMyEval->asVariable()->value(spContext->r_b) = parent_color[2];

		spContext->r_splat = r_splat;
		spMyEval->compile(m_asSplat.expression(spContext->r_splat).c_str());
		spContext->m_splatname = m_asSplat.name(spContext->r_splat);

		m_contexts.push_back(spContext);
	}
}

void SplatterSystem::Context::newline(void)
{
	m_line_started = false;
}

void SplatterSystem::Context::mark(t_moa_real a_x, t_moa_real a_y)
{
	t_moa_v3 color(1.0,0.0,1.0);
	t_image_splat::t_span l_splat;
	l_splat[0] = a_x;
	l_splat[1] = 1.0 - a_y;
	m_splat->splat(1.0, l_splat, color);

	m_loc[0] = l_splat[0];
	m_loc[1] = l_splat[1];
}

void SplatterSystem::Context::line(t_moa_real a_x, t_moa_real a_y)
{
	if(!m_line_started)
	{
		m_line_started = true;
		mark(a_x, a_y);
	}
	t_moa_v3 color = m_color;
	t_image_splat::t_span l_splat;
	l_splat[0] = a_x;
	l_splat[1] = 1.0 - a_y;

	t_image_splat::t_span d;
	d[0] = m_loc[0] - l_splat[0];
	d[1] = m_loc[1] - l_splat[1];
	t_moa_real r = sqrt(d[0]*d[0] + d[1]*d[1]);
	t_moa_real l = 0.002;
	unsigned int a = r/l + 1;
	t_moa_real s = 1.0 / (t_moa_real)a;
	d[0] = s * d[0];
	d[1] = s * d[1];

	if(a > 1000) { a = 1000; }

	for(unsigned int i = 1; i <= a; i++)
	{
		m_loc[0] = l_splat[0] + d[0]*(t_moa_real)i;
		m_loc[1] = l_splat[1] + d[1]*(t_moa_real)i;
		m_splat->splat(1.0, m_loc, color);
	}

	m_loc[0] = l_splat[0];
	m_loc[1] = l_splat[1];
}

void SplatterSystem::Context::dot(t_moa_real a_x, t_moa_real a_y)
{
	for(t_moa_real r = 0; r < 2.0 * fe::pi; r += fe::pi / 12.0)
	{
		t_image_splat::t_span loc;
		loc[0] = a_x + 0.006*sin(r);
		loc[1] = (1.0-a_y) + 0.006*cos(r);
		m_splat->splat(10.0, loc, t_moa_v3(0.0,1.0,1.0));
	}
	for(t_moa_real r = 0; r < 2.0 * fe::pi; r += fe::pi / 12.0)
	{
		t_image_splat::t_span loc;
		loc[0] = a_x + 0.016*sin(r);
		loc[1] = (1.0-a_y) + 0.016*cos(r);
		m_splat->splat(10.0, loc, t_moa_v3(0.0,1.0,1.0));
	}
}

void SplatterSystem::Context::annotate(t_moa_real a_x, t_moa_real a_y,
	const String &a_text)
{
	m_annotations.push_back(Annotation(a_x, a_y, a_text));
}

void SplatterSystem::Context::annotate(t_moa_real a_x, t_moa_real a_y,
	t_moa_real a_value)
{
	String s; s.sPrintf("%2.1f", a_value);
	m_annotations.push_back(Annotation(a_x, a_y, s.c_str()));
}

void SplatterSystem::image(void)
{
	AsAspect asAspect(rg_dataset->scope());
	std::vector<Record>	aspects;
	asAspect.filter(aspects, rg_dataset);

	String dirname(m_asDatasetMeta.get("splatpath"));
	System::createDirectory(dirname);

	for(t_splatmap::iterator i_splatmap = m_splatMap.begin();
		i_splatmap != m_splatMap.end(); i_splatmap++)
	{
		String filename;
		filename.sPrintf("%s/%s_%s-%s.ppm", dirname.c_str(),
			m_asDatasetMeta.get("datacode").c_str(),
			m_asDatasetMeta.get("runcode").c_str(), i_splatmap->first.c_str());
		filename = filename.replace(" ", "-");
		FILE *fp = fopen(filename.c_str(), "wb");
		fe_fprintf(fp, "P6\n");
		fe_fprintf(fp, "# DATASET=%s\n", m_asDatasetMeta.get("datacode").c_str());
		for(unsigned int i_aspect = 0; i_aspect < aspects.size(); i_aspect++)
		{
			fe_fprintf(fp, "# ASPECT=%s\n", asAspect.label(aspects[i_aspect]).c_str());
		}
		fe_fprintf(fp, "# METHOD=%s\n", m_asDatasetMeta.get("runcode").c_str());
		for(unsigned int i_context = 0;
			i_context < i_splatmap->second.m_contexts.size(); i_context++)
		{
			sp<Context> context = i_splatmap->second.m_contexts[i_context];
			const t_moa_v2 &x_range = m_asSplat.x_range(context->r_splat);
			const t_moa_v2 &y_range = m_asSplat.y_range(context->r_splat);
			fe_fprintf(fp, "# SUBTITLE=SN %s\n", context->m_splatname.c_str());
			fe_fprintf(fp,
				"# CONTEXT=%s,%5.3f,%5.3f,%5.3f,%5.3f,%5.3f,%5.3f,%5.3f\n",
					context->m_splatname.c_str(),
					x_range[0], x_range[1], y_range[0], y_range[1],
					m_asSplat.color(context->r_splat)[0],
					m_asSplat.color(context->r_splat)[1],
					m_asSplat.color(context->r_splat)[2]);
			for(unsigned int i = 0; i < context->m_annotations.size(); i++)
			{
				fe_fprintf(fp, "# %8.8f %8.8f %s\n", context->m_annotations[i].m_x,
					context->m_annotations[i].m_y,
					context->m_annotations[i].m_text.c_str());
			}

		}
		fe_fprintf(fp, "%d %d\n", i_splatmap->second.m_splat->count()[0],
			i_splatmap->second.m_splat->count()[1]);
		fe_fprintf(fp, "255\n");
		Imager imager(fp);
		i_splatmap->second.m_splat->iterate(imager);
		fclose(fp);
	}
}

void SplatterSystem::updateContext(sp<SplatterSystem::Context> a_context)
{
	const t_moa_v2 &x_range = m_asSplat.x_range(a_context->r_splat);
	const t_moa_v2 &y_range = m_asSplat.y_range(a_context->r_splat);

	t_moa_real x_scale = x_range[1] - x_range[0];
	t_moa_real y_scale = y_range[1] - y_range[0];

	a_context->m_spMyEval->evaluate();

	a_context->m_x_n =
		(a_context->m_spMyEval->asVariable()->value(a_context->r_x) -
			x_range[0]) / x_scale;
	a_context->m_y_n =
		(a_context->m_spMyEval->asVariable()->value(a_context->r_y) -
			y_range[0]) / y_scale;

	a_context->m_color[0] =
		a_context->m_spMyEval->asVariable()->value(a_context->r_r);
	a_context->m_color[1] =
		a_context->m_spMyEval->asVariable()->value(a_context->r_g);
	a_context->m_color[2] =
		a_context->m_spMyEval->asVariable()->value(a_context->r_b);

	if(a_context->m_spMyEval->asVariable()->value(a_context->r_y) >
		a_context->m_y_max[0])
	{
		a_context->m_y_max[0] =
			a_context->m_spMyEval->asVariable()->value(a_context->r_y);
		a_context->m_y_max[1] = a_context->m_x_n;
		a_context->m_y_max[2] = a_context->m_y_n;
	}
}


SplatLineSystem::SplatLineSystem(void)
{
}

SplatLineSystem::~SplatLineSystem(void)
{
}

void SplatLineSystem::perform(const t_note_id &a_note_id)
{
//TODO: faster version --- too much lookup per perform
	Record r_time = recordFind<AsTime>(rg_dataset, FE_SIM_CLOCK);
	AsTime asTime(rg_dataset->scope());

	for(unsigned int i_context = 0; i_context < m_contexts.size(); i_context++)
	{
		sp<Context> context = m_contexts[i_context];
		if(!context->m_splat.isValid()) { continue; }

		updateContext(context);

		if(a_note_id == m_note_datapoint)
		{
			context->line(context->m_x_n, context->m_y_n);
		}
		else if(a_note_id == m_note_line_start)
		{
			context->m_y_max = t_moa_v3(-1.0e10,0.0,0.0);
			context->newline();
		}
		else if(a_note_id == m_note_loop_stop)
		{
			context->annotate(context->m_y_max[1],context->m_y_max[2], "");
		}
		else if(a_note_id == m_note_image)
		{
			image();
		}
	}
}

bool SplatLineSystem::match(const fe::Record &r_splat)
{
	AsSplatLine asSplatLine;
	asSplatLine.bind(r_splat);
	return asSplatLine.check(r_splat);
}

NyquistLineSystem::NyquistLineSystem(void)
{
}

NyquistLineSystem::~NyquistLineSystem(void)
{
}

void NyquistLineSystem::initialize(void)
{
}

void NyquistLineSystem::compile(const t_note_id &a_note_id)
{
	SplatterSystem::compile(a_note_id);

	a_r_time = recordFind<AsTime>(rg_dataset, FE_SIM_CLOCK);
	m_asTime.bind(rg_dataset->scope());
	m_asNyquistLine.bind(rg_dataset->scope());

	m_nyquistContexts.resize(m_contexts.size());
}

#if 1
//TODO:X API fix
//TODO:X compare with a better impl
// fft2 is ripped straight from wikipedia.
// https://en.wikipedia.org/wiki/Cooley%E2%80%93Tukey_FFT_algorithm
// this is for offline testing only, so I chose open/free/embeddable
// over complexity of licensing questions.	 This version is
// plenty good enough for Nyquist plots
void separate (std::complex<double>* a, int n) {
	std::complex<double>* b = new std::complex<double>[n/2];
	for(int i=0; i<n/2; i++)
		b[i] = a[i*2+1];
	for(int i=0; i<n/2; i++)
		a[i] = a[i*2];
	for(int i=0; i<n/2; i++)
		a[i+n/2] = b[i];
	delete[] b;
}

void fft2 (std::complex<double>* X, int N) {
	if(N < 2) {
	} else {
		separate(X,N);
		fft2(X,		N/2);
		fft2(X+N/2, N/2);
		for(int k=0; k<N/2; k++) {
			std::complex<double> e = X[k    ];
			std::complex<double> o = X[k+N/2];
			std::complex<double> w =exp(std::complex<double>(0,-2.*fe::pi*k/N));
			X[k    ] = e + w * o;
			X[k+N/2] = e - w * o;
		}
	}
}
#endif

int size_fft(int a_all)
{
	int i = (int)log2(a_all);
	return (int)pow(2, i);
}

bool NyquistLineSystem::match(const fe::Record &r_splat)
{
	AsNyquistLine asNyquistLine;
	asNyquistLine.bind(r_splat);
	return asNyquistLine.check(r_splat);
}

void NyquistLineSystem::perform(const t_note_id &a_note_id)
{
	for(unsigned int i_context = 0; i_context < m_contexts.size(); i_context++)
	{
		sp<Context> context = m_contexts[i_context];
		NyquistContext &nyquistContext = m_nyquistContexts[i_context];
		if(!context->m_splat.isValid()) { continue; }
		updateContext(context);

		if(a_note_id == m_note_dt_change)
		{
			if(context->m_x_run.size() > 0)
			{
				feX("NyquistLineSystem::perform", "dt changed within a run");
			}
		}
		else if(a_note_id == m_note_datapoint)
		{
			context->m_x_run.push_back(context->m_x_n);
			context->m_y_run.push_back(context->m_y_n);
		}
		else if(a_note_id == m_note_line_start)
		{
			context->m_y_max = t_moa_v3(-1.0e10,0.0,0.0);
			context->m_x_run.clear();
			context->m_y_run.clear();
			nyquistContext.m_start_time = m_asTime.time(a_r_time);
		}
		else if(a_note_id == m_note_line_stop)
		{
//TODO:X streamline using fft, even this simply
			int samples = size_fft(context->m_x_run.size());
			std::complex<double> *driver = new std::complex<double>[samples];
			std::complex<double> *response = new std::complex<double>[samples];
			for(unsigned int i = 0; i < (unsigned int)samples; i++)
			{
				driver[i] = std::complex<double>(context->m_x_run[i],0.0);
				response[i] = std::complex<double>(context->m_y_run[i],0.0);
			}

			fft2(driver,samples);
			fft2(response,samples);

			t_moa_real peak_driver = 0.0;
			t_moa_real peak_response = 0.0;
			unsigned int i_driver = 0;
			for(unsigned int i = 1; i < (unsigned int)samples / 2; i++)
			{
				t_moa_real mag_driver = sqrt(
					driver[i].real()*driver[i].real() +
					driver[i].imag()*driver[i].imag());
				if(mag_driver > peak_driver)
					{ i_driver = i; peak_driver = mag_driver; }
				t_moa_real mag_response = sqrt(
					response[i].real()*response[i].real() +
					response[i].imag()*response[i].imag());
				if(mag_response > peak_response)
					{ /*i_response = i;*/ peak_response = mag_response; }
			}

			const t_moa_real &scale = m_asNyquistLine.scale(context->r_splat);

			std::complex<double> n = (response[i_driver]) / driver[i_driver];
			context->line((1.0 + scale*n.real()) / 2.0,
				(1.0 + scale*n.imag()) / 2.0);
			context->dot((1.0 + scale*n.real()) / 2.0,
				(1.0 + scale*n.imag()) / 2.0);

#if 0
			if(f >= 30.0 && f <= 35.0)
			{
				context->dot( (1.0 + scale*n.real()) / 2.0, (1.0 + scale*n.imag()) / 2.0);
			}
#endif

			delete[]driver;
			delete[]response;
		}
		else if(a_note_id == m_note_loop_start)
		{
			context->newline();
		}
		else if(a_note_id == m_note_image)
		{
			image();
		}
	}
}

} /* namespace ext */
} /* namespace fe */
