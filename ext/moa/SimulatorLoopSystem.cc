/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "moa/moa.pmh"

namespace fe
{
namespace ext
{

SimulatorLoopSystem::SimulatorLoopSystem(void)
{
}

SimulatorLoopSystem::~SimulatorLoopSystem(void)
{
}

void SimulatorLoopSystem::initialize(void)
{
}

void SimulatorLoopSystem::connectOrchestrator(
	fe::sp<OrchestratorI> a_spOrchestrator)
{
	m_spOrchestrator = a_spOrchestrator;
	a_spOrchestrator->connect(this,&SimulatorLoopSystem::start,FE_NOTE_START);
}

void SimulatorLoopSystem::performDataset(sp<RecordGroup> a_rg_dataset)
{
	t_note_id m_note_run_start = m_spOrchestrator->note_id(FE_NOTE_RUN_STOP);
	t_note_id m_note_run_stop = m_spOrchestrator->note_id(FE_NOTE_RUN_START);
	t_note_id m_note_line_start = m_spOrchestrator->note_id(FE_NOTE_LINE_START);
	t_note_id m_note_line_stop = m_spOrchestrator->note_id(FE_NOTE_LINE_STOP);
	t_note_id m_note_datapoint = m_spOrchestrator->note_id(FE_NOTE_DATAPOINT);
	t_note_id m_note_image = m_spOrchestrator->note_id(FE_NOTE_IMAGE);

	//TODO: huh? -- why straight test
	t_note_id m_note_recompile = m_spOrchestrator->note_id("recompile");


	// filtering way up here per dataset is fine
	AsSimulatorLoop asSimulatorLoop(a_rg_dataset->scope());
	std::vector<Record>	simulator_loops;
	asSimulatorLoop.filter(simulator_loops, a_rg_dataset);

	sp<RecordGroup> rg_dataset_loop = a_rg_dataset;

	AsDatasetMeta asDatasetMeta;
	asDatasetMeta.digest(a_rg_dataset);


//TODO:  this is serial, which is likely useless.  parallel makes more sense
	for(unsigned int i_loop = 0; i_loop < simulator_loops.size(); i_loop++)
	{
		Record r_loop = simulator_loops[i_loop];

		asDatasetMeta.set("runcode", asSimulatorLoop.name(r_loop));

		m_spOrchestrator->perform(m_note_recompile);
		m_spOrchestrator->perform(m_note_run_start);
		m_spOrchestrator->setDT(1.0/asSimulatorLoop.frequency(r_loop));
		//m_spOrchestrator->setDT(0.01*1.0/asSimulatorLoop.frequency(r_loop));

		while(m_spOrchestrator->getTime() < asSimulatorLoop.duration(r_loop))
		{
//fe_fprintf(stderr, "L|");
			m_spOrchestrator->perform(m_note_line_start);
			m_spOrchestrator->step(e_step_no_more_than,
				asSimulatorLoop.linger(r_loop));
			m_spOrchestrator->perform(m_note_datapoint);
			m_spOrchestrator->perform(m_note_line_stop);
		}
		m_spOrchestrator->perform(m_note_run_stop);
		m_spOrchestrator->perform(m_note_image);
	}
}

void SimulatorLoopSystem::start(const t_note_id &a_note_id)
{
	sp<RecordGroup> rg_dataset = m_spOrchestrator->dataset();
	performDataset(rg_dataset);
}

} /* namespace ext */
} /* namespace fe */
