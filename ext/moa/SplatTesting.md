# MOA Splat Tests Guide

_The examples cited here are pulled from unit tests running implementations of ProducerI (20210310)._

## Introduction

[Splat Primer](@ref SplatUtility).

The most common use case for this is to plot values from a simulation run
as a visualization tool.  Some of the other use cases are mentioned in


## Use Case: Test Suite for MOA

This use case involves two separate concepts:
  * generating data as useful test cases
  * capturing data and presenting clear results

Available to this use case is an Orchestrator configured to run a MOA simulation
and an appropriate SplatterI System Component to view results.


### Generating data as useful test cases

#### 1. Hook up the runtime functionality

A common test case is to vary a value of state while measuring another value of
state, plotting the relationship as a 2D curve or plot.   Doing this "sweep"
for more than one other value of state results in multiple curves.   This is
what the MultiSweep System enables.   The following is an example of adding
this functionality to MOA:

    spOrchestrator->append(spRegistry->create("SystemI.MultiSweep.fe"));

#### 2. Configure some test runs

Tests for MultiSweep are configured by adding Records to the MOA dataset.  An
example in a TireRigProducer::prepare looks like:

    loadFileInto(a_rg_dataset, "splat.rg");

AsMultiSweep is the AccessorSet that MultiSweep uses for configuration.

| attribute        | type                | description                                 |
|------------------|---------------------|---------------------------------------------|
| composer:step    | string Eval Script  | eval script run every step                  |
| composer:outer   | string Eval Range   | outer loop range                            |
| composer:inner   | string Eval Range   | inner loop range                            |
| composer:linger  | real                | time (s)  to linger on each data point      |
| composer:runup   | real                | runup time (s) before each inner loop pass  |

Eval Range is expressed as three numbers, with the third number optionally
specifying a mode.

    <start value> <end value> [optional '*' for multiply step mode]<step value>
  
Examples of Eval Range:
  * `"-10 10 2"` results in the sequence: -10 -8 -6 -4 -2 0 2 4 6 8 10
  * `"2 512 \*2"` results in the sequence: 2 4 8 16 32 64 128 256 512


Example MultiSweep Record

    RECORD * multisweep
        name			"κα"
        composer:outer	"0.0 20.0 10.0"
        composer:inner	"-20.0 20.0 1.0"
        composer:step	"	(set	'base_rig.rig:alpha' INNER)
                            (set	'base_rig.rig:kappa' OUTER)		"

This Record specifies a test run named "κα" such that the rig (base\_rig)
rig:alpha attribute is swept from a value of -20 to 20 in steps of 1.
Further, this is repeated 3 times, with rig:kappa being set to 0, 10, then 20.

The addressing within composer:step is covered in:
[MOA Canonical Addressing](@ref CanonicalGuide)

The evaluation within composer:step is covered in:
[Eval Language](@ref EvaluateGuide)


### Capturing data and presenting clear results


#### 1. Hook up the runtime functionality

Example appending a SplatLineSystem to an Orchestrator (cited from TireRigProducer):

    spOrchestrator->append(spRegistry->create("SystemI.SplatLineSystem.fe"));


#### 2. Specify some Splats

A SplatLineSystem is configured with the following atributes:

attribute        | type                | description
splat:x\_range   | vector2             | minimum and maximum value of X
splat:y\_range   | vector2             | minimum and maximum value of Y
splat:expression | string              | eval script run every data point
splat:splat      | string              | name of splat image to splat to
splat:color      | vector3             | color of data applied


Example Record for an alpha (α) by Fy plot:

    RECORD * splat
      name             "αFy"
      splat:splat      "αFyFzMz"
      splat:x_range    "-30.0      30.0"
      splat:y_range    "-7000.0    7000.0"
      splat:expression "   (X (get   'base_rig.rig:alpha'))
                           (Y (get   'base_rig.mnt:0.tire:force[1]'))"
      splat:color       "1 0 0.6"

Example Record for an alpha (α) by Fz plot:

    RECORD * splat
        name			"αFz"
        splat:splat		"αFyFzMz"
        splat:x_range	"-20.1		20.1"
        splat:y_range	"-6001.0	6001.0"
        splat:expression "	(X (get		'base_rig.rig:alpha')		)
                            (Y (* -1.0 (get		'base_rig.mnt.tire:force[2]'))	)	 "
        splat:color		"1.0 0.8 1.0"

Example Record for an alpha (α) by Mz plot:

    RECORD * splat
        name			"αMz"
        splat:splat		"αFyFzMz"
        splat:x_range	"-30.0		30.0"
        splat:y_range	"-20.0	20.0"
        splat:expression "	(X (get		'base_rig.rig:alpha')		)
                            (Y (get		'base_rig.mnt.tire:moment[2]')	)	 "
        splat:color		"0.0 0.8 1.0"

Multiple splat specifications can apply to the same image.   In the above example, all three
SplatLine renders go to a shared splat named "αFyFzMz".

_TODO: fix how we do images in MD/dox_

![just the render image](/home/orang/buffer/fe/ext/moa/render.png)

#### 3. Converting raw results into a web page

After running the test program (typically a unit test), the results will be in
the form of internally annotated PPM files in a "data" directory.

_TODO: fix currently hard coded and located where the test was run; also, at the moment, wrong wrt directories_

Then from the directory where the test was run, run the web generation script:

    mini2svg.py

Then from a browser open:

    results/index.html

![just the render image](/home/orang/buffer/fe/ext/moa/svgrender.png)
  
  
### Extra Techniques

#### Using color via splat:expression

Instead of using the splat:color attribute, R, G, and B can be set directly in
the expression:

    RECORD * splat
        name			"αFy"
        splat:splat		"αFyFzMz"
        splat:x_range	"-30.0		30.0"
        splat:y_range	"-7000.0	7000.0"
        splat:expression "	(X (get		'base_rig.rig:self.rig:alpha')		)
                            (Y (get		'base_rig.mnt.tire:force[1]')	)
                            (R        (/  (get 'base_rig.rig:kappa')    20.0) )
                            (G (- 1.0 (/  (get 'base_rig.rig:kappa')    20.0)))
                            (B (0.0) ) "

In this example the color varies with the rig's kappa setting, with the following result:


![color expression](/home/orang/buffer/fe/ext/tire/test/colorrender.png)



#### Setting up a test suite

Setting up a test suite can make use of overlays, which helps setup a suite of
RecordGroups (as datasets), from which to run many tests.

[Overlay Guide](@ref OverlayGuide)


