/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "fe/data.h"
#include "moa/moa.h"

using namespace fe;
using namespace ext;

int main(int argc, char *argv[])
{
	UnitTest unitTest;
	try
	{
		feLogger()->clear(".*");
		feLogger()->clear("error");
		feLogger()->clear("info");
		feLogger()->clear("warning");
#if 0
		feLogger()->setLog("group", new AnnotatedLog());
		feLogger()->setLog("stdout", new GroupLog());
		feLogger()->setLog("evdb", new EvaluatorLog());
		feLogger()->bind("error", "evdb");
		feLogger()->bind("info", "evdb");
		feLogger()->bind("warning", "evdb");
#endif

		sp<Master> spMaster(new Master);
		assertMath(spMaster->typeMaster());
		sp<Registry> spRegistry = spMaster->registry();
		spRegistry->manage("fexMoaDL");
		spRegistry->manage("feDataDL");

		sp<Scope> spScope = spRegistry->create("Scope");
	}
	catch(Exception &e)
	{
		e.log();
	}
	return unitTest.failures();
}


