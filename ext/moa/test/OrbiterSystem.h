/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#pragma once

namespace fe
{
namespace ext
{

// !HACK
static bool locked = false;

/**
 * Test-only.
 *
 * Simple system that orbits objects around a point.
 */
class OrbiterSystem : virtual public Stepper
{
public:
    void orbitersAdded(const t_note_id &a_note_id)
    {
        if (locked)
            return;
        locked = true;

        orbiterAccessorSet.bind(m_rg_dataset->scope());

        // Find orbiter records.
        orbiterRecords.clear();
        orbiterAccessorSet.filter(orbiterRecords, m_rg_dataset);

        locked = false;
    };

	void step(t_moa_real dt) override
    {
        // !HACK
        if (locked)
            return;
        locked = true;

        // Loop over each box.
        for (fe::Record orbiterRecord : orbiterRecords)
        {
            fe::Real radius = orbiterAccessorSet.orbitRadius(orbiterRecord);
            fe::Real angularVelocity = 180.0 * M_PI/180.0; // 180 deg/s
            orbiterAccessorSet.orbitAngle(orbiterRecord) += angularVelocity * dt;

            fe::SpatialVector offsetFromCenter = orbiterAccessorSet.orbitPoint(orbiterRecord);
            offsetFromCenter[0] += radius * cos(orbiterAccessorSet.orbitAngle(orbiterRecord));
            offsetFromCenter[1] += radius * sin(orbiterAccessorSet.orbitAngle(orbiterRecord));

            orbiterAccessorSet.transform(orbiterRecord).translation() =
                offsetFromCenter + orbiterAccessorSet.orbitPoint(orbiterRecord);
        }

        locked = false;
        // feLog("orbit dt: %f\n", dt);
    };

    void connectOrchestrator(fe::sp<OrchestratorI> a_spOrchestrator) override
    {
        Stepper::connectOrchestrator(a_spOrchestrator);

        a_spOrchestrator->connect(this, &OrbiterSystem::orbitersAdded, "orbitersAdded");
        a_spOrchestrator->connect(this, &OrbiterSystem::orbitersAdded, FE_NOTE_COMPILE);
    }

    AsOrbiter orbiterAccessorSet;
    std::vector<fe::Record> orbiterRecords;
};

} /* namespace ext */
} /* namespace fe */
