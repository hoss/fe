/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

/**
 * Creates a "planet" and orbits it around a point to demonstrate MOA.
 *
 * Displays the changing data using an imgui record viewer.
 *
 * @see OrbiterSystem.h
 * @file
 */

#include "moa_test.h"
#include <chrono>
#include "imgui/imgui.pmh"

using namespace fe;
using namespace ext;
using namespace std::chrono;

sp<Master> master;
sp<Registry> registry;

AsOrbiter orbiterAccessorSet;

int main(int argc, char **argv)
{
    // Create registry.
    master = new Master;
    registry = master->registry();
    registry->manage("feAutoLoadDL");

    // Assert attribute types.
    sp<TypeMaster> typeMaster = master->typeMaster();
    assertData(typeMaster);
	assertMath(typeMaster);

    // Create the dataset.
    sp<Scope> scope = registry->create("Scope");
    sp<RecordGroup> records(new RecordGroup());

    // Create layout for our planet records.
    sp<Layout> planetLayout = scope->declare("planet");
    orbiterAccessorSet.populate(planetLayout);

    // Create the planet record.
    Record planetRecord = scope->createRecord(planetLayout);

    // Modify the data.
    orbiterAccessorSet.bind(planetRecord);
    SpatialTransform transform;
    setIdentity(transform);
    orbiterAccessorSet.transform = transform;
    orbiterAccessorSet.orbitRadius(planetRecord) = 1;
    orbiterAccessorSet.orbitPoint(planetRecord) = SpatialVector(0, 0, 0);

    // Add the record the dataset.
    records->add(planetRecord);

    // Create orchestrator.
    sp<OrchestratorI> orchestrator(registry->create("OrchestratorI.Orchestrator"));
    orchestrator->datasetInitialize(records);
    orchestrator->setDT(1.0/100); // 100 HZ

    // Add an Orbiter System to the Orchestrator.
    sp<SystemI> orbiterSystem = registry->create("Stepper.OrbiterSystem.fe_test");
    orchestrator->append(orbiterSystem);

    // Inform the systems of the new data.
    orchestrator->compile(); // get rid of this
    orchestrator->perform(orchestrator->note_id("orbitersAdded"));

    // Create an Imgui Record Viewer.
    // TODO: This should be threaded.
    sp<ext::QuickViewerI> spQuickViewerI(registry->create("QuickViewerI"));
    spQuickViewerI->open();
    sp<ext::DrawI> spDrawI = spQuickViewerI->getDrawI();
    sp<ext::ImguiHandlerRecord> spImguiHandlerRecord =
			registry->create("*.ImguiHandlerRecord");
    spImguiHandlerRecord->bind(spDrawI);
    spImguiHandlerRecord->bind(records);
    spQuickViewerI->insertDrawHandler(spImguiHandlerRecord);
    spQuickViewerI->insertEventHandler(spImguiHandlerRecord);

	// Time at which the last tick was run.
	steady_clock::time_point lastTimePoint = steady_clock::now();

    // Step the simulation.
    while (true)
    {
        // Calculate dt.
		steady_clock::time_point currentTimePoint = steady_clock::now();
		std::chrono::duration<Real> dt = currentTimePoint - lastTimePoint;

        // Step.
		orchestrator->step(e_step_at_least, dt.count());

        // Save new time.
		lastTimePoint = currentTimePoint;

        // Render a frame.
        spQuickViewerI->run(1);

        // It'll tick too fast for numerical precision otherwise.
        microSleep(100);
    }

    return 0;
}
