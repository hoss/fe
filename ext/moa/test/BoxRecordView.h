/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#pragma once

namespace fe
{
namespace ext
{

/**
 * Test-only.
 *
 * Record View for a simple box record.
 */
class BoxRecordView :
    public DevRecordView,
    public fe::Initialize<BoxRecordView>
{
public:
    /** Transform of box. */
    fe::Accessor<fe::SpatialTransform> transform;
    /** Size of box. */
    fe::Accessor<fe::SpatialVector> size;

    void initialize()
    {
        add(transform, "transform");
        add(size, "size");
    };
};

} /* namespace ext */
} /* namespace fe */
