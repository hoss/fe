/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "moa/moa.pmh"

namespace fe
{
namespace ext
{

EvaluateSystem::EvaluateSystem(void)
{
}

EvaluateSystem::~EvaluateSystem(void)
{
}

void EvaluateSystem::initialize(void)
{
}

class actionLT {
	public:
		actionLT() { }
		bool operator()(
			sp<EvaluateSystem::EvaluateAction> a,
			sp<EvaluateSystem::EvaluateAction> b) const
		{
			return a->m_time > b->m_time;
		}
};

void EvaluateSystem::connectOrchestrator(fe::sp<OrchestratorI> a_spOrchestrator)
{
	m_rg_dataset = a_spOrchestrator->dataset();
	a_spOrchestrator->connect(this,&EvaluateSystem::compile,FE_NOTE_COMPILE);
};

void EvaluateSystem::compile(const t_note_id &a_note_id)
{
	m_asEvaluateAction.bind(m_rg_dataset->scope());
	std::vector<Record>	actions;
	m_asEvaluateAction.filter(actions, m_rg_dataset);

	for(unsigned int i = 0; i < actions.size(); i++)
	{
		sp<Evaluator> spEvaluator =
			registry()->create("EvaluatorI.EvaluatorStd.fe");
		spEvaluator->bind(m_rg_dataset);
		spEvaluator->compile(m_asEvaluateAction.action(actions[i]).c_str());

		sp<EvaluateAction> spAction(new EvaluateAction(
			m_asEvaluateAction.time(actions[i]), spEvaluator));

		m_actions.push_back(spAction);
	}

	std::sort(m_actions.begin(),m_actions.end(),actionLT());

	m_r_time = recordFind<AsTime>(m_rg_dataset, FE_SIM_CLOCK);
	m_asTime.bind(m_rg_dataset->scope());

	m_startTime = -1.0;
}

void EvaluateSystem::perform(const t_note_id &a_note_id)
{
	t_moa_real tol = 1.0e-6;

	if(m_startTime < 0.0)
	{
		m_startTime = m_asTime.time(m_r_time);
	}

	t_moa_real time = m_asTime.time(m_r_time) - m_startTime;

	while(m_actions.size() > 0)
	{
		if(m_actions.back()->m_time - time > tol)
		{
			break;
		}
		m_actions.back()->m_spActionEval->evaluate();
		m_actions.pop_back();
	}
}

} /* namespace ext */
} /* namespace fe */
