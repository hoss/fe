/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#pragma once

namespace fe
{
namespace ext
{

typedef int t_note_id;

class SystemI;

typedef enum
{
	e_step_none =		0,
	e_step_dt =			1,	///<    take a single time step
	e_step_no_more_than,	///<	as close as possible but not over via DT's
	e_step_at_least,		///<	as close as possible but not under via DT's
	e_step_count,
} t_step_mode;

/// @brief Orchestrate signalling of SystemI components
class FE_DL_EXPORT OrchestratorI
	: virtual public Component,
	public CastableAs<OrchestratorI>
{
	public:
		/** Signal SystemI's appended on matching id */
virtual	void		perform(const char *a_label)							= 0;
virtual	void		perform(const t_note_id &a_note_id)						= 0;
virtual	t_note_id	note_id(const char *a_label)							= 0;
virtual	t_note_id	connect(SystemI *a_system, const char *a_label)			= 0;
virtual	t_note_id	connect(sp<SystemI> a_system,
								const char *a_label)						= 0;

		/** Append a SystemI to be signaled. */
virtual	void	append(sp<SystemI> a_system)								= 0;
		/** Initialize based on a dataset */
virtual	void	datasetInitialize(sp<RecordGroup> a_rg_dataset)				= 0;
		/**	Get the dataset */
virtual	sp<RecordGroup> dataset(void)										= 0;

		/** Set time step */
virtual	void		setDT(t_moa_real a_dt)									= 0;
		/** Get time step */
virtual	t_moa_real	getDT(void)												= 0;
		/** Step time. */
virtual	void		step(const t_step_mode &a_step_mode=e_step_dt,
							t_moa_real a_time=0.0)							= 0;
		/** Get time */
virtual	t_moa_real	getTime(void)											= 0;

typedef std::function<void(const t_note_id &)> t_note_perform;
virtual t_note_id connect(t_note_perform a_perform, const char *a_label) = 0;

		template <class F, class S>
		t_note_id connect(S *s, F&& f, const char *a_label)
		{
			t_note_perform fn = std::bind(f, s, std::placeholders::_1);
			return connect(fn, a_label);
		}

		/** Compile internal structures of systems.
		*/
virtual	bool compile(void)													= 0;
};

} /* namespace ext */
} /* namespace fe */
