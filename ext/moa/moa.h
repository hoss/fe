/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __moa_moa_h__
#define __moa_moa_h__

#include "fe/plugin.h"
#include "fe/data.h"
#include "math/math.h"
#include "mechanics/mechanics.h"
#include "surface/surface.h"

#include <atomic>
#include <functional>

#define FE_SIM_CLOCK			"sim_clock"
#define FE_L_TIME				"layout_time"

#define	FE_NOTE_START			"start"
#define	FE_NOTE_TERMINAL		"terminal"
#define	FE_NOTE_COMPILE			"compile"
#define	FE_NOTE_STEP			"step"
#define	FE_NOTE_DATAPOINT		"datapoint"
#define	FE_NOTE_RUN_START		"run_start"
#define	FE_NOTE_RUN_STOP		"run_stop"
#define	FE_NOTE_LINE_START		"line_start"
#define	FE_NOTE_LINE_STOP		"line_stop"
#define	FE_NOTE_LOOP_START		"loop_start"
#define	FE_NOTE_LOOP_STOP		"loop_stop"
#define	FE_NOTE_DT_CHANGE		"dt_change"
#define	FE_NOTE_IMAGE			"image"
#define	FE_NOTE_UNKNOWN			"unknown"

#include "moa/data.h"

#include "moa/moaAS.h"

#include "moa/OrchestratorI.h"
#include "moa/SystemI.h"
#include "moa/Stepper.h"
#include "moa/SplatterI.h"

#endif /* __moa_moa_h__ */
