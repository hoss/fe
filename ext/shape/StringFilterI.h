/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __shape_StringFilterI_h__
#define __shape_StringFilterI_h__

namespace fe
{
namespace ext
{

//* NOTE not really shape-related

/**************************************************************************//**
    @brief String check or modification

	For a simple boolean check,
	the result should be the same string or an empty string.

	For modification, there is no particular general behavior.

	@ingroup shape
*//***************************************************************************/
class FE_DL_EXPORT StringFilterI:
	public Component,
	public CastableAs<StringFilterI>
{
	public:
virtual	BWORD					load(String a_filename)						=0;

virtual	void					configure(String a_config)					=0;

virtual	String					configuration(void)							=0;

virtual	String					filter(String a_string)						=0;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __shape_StringFilterI_h__ */
