/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __shape_shapeAS_h__
#define __shape_shapeAS_h__

namespace fe
{
namespace ext
{

class AsPoint :
	virtual public AccessorSet,
	public Initialize<AsPoint>
{
	public:
		void initialize(void)
		{
			add(location,	FE_SPEC("spc:at",		"point location"));
		}
		/// location
		Accessor<SpatialVector>					location;
};

class AsVelocityPoint :
	virtual public AsPoint,
	public Initialize<AsVelocityPoint>
{
	public:
		void initialize(void)
		{
			add(velocity,			FE_USE("spc:velocity"));
		}
		/// spatial velocity
		Accessor<SpatialVector>		velocity;
};

/// force application point
class AsForcePoint :
	public AsVelocityPoint,
	public Initialize<AsForcePoint>
{
	public:
		void initialize(void)
		{
			add(force,				FE_USE("sim:force"));
		}

		/// spatial force
		Accessor<SpatialVector>		force;
};


/// particle in physical space
class AsParticle :
	public AsForcePoint,
	public Initialize<AsParticle>
{
	public:
		void initialize(void)
		{
			add(mass,				FE_USE("sim:mass"));
		}
		/// mass
		Accessor<Real>				mass;
};

class AsPoint1D :
	virtual public AccessorSet,
	public Initialize<AsPoint1D>
{
	public:
		void initialize(void)
		{
			add(location,	FE_SPEC("spc:location1d",		"point location 1D"));
		}
		/// location
		Accessor<Real>					location;
};

class AsVelocityPoint1D :
	virtual public AsPoint1D,
	public Initialize<AsVelocityPoint1D>
{
	public:
		void initialize(void)
		{
			add(velocity,			FE_USE("spc:velocity1d"));
		}
		/// spatial velocity
		Accessor<Real>		velocity;
};

/// force application point
class AsForcePoint1D :
	public AsVelocityPoint1D,
	public Initialize<AsForcePoint1D>
{
	public:
		void initialize(void)
		{
			add(force,				FE_USE("sim:force1d"));
		}

		/// spatial force
		Accessor<Real>		force;
};


/// particle in physical space
class AsParticle1D :
	public AsForcePoint1D,
	public Initialize<AsParticle1D>
{
	public:
		void initialize(void)
		{
			add(mass,				FE_USE("sim:mass"));
		}
		/// mass
		Accessor<Real>				mass;
};

/// quad in physical space
class AsQuad :
	virtual public AccessorSet,
	public Initialize<AsQuad>
{
	public:
		void initialize(void)
		{
			add(v0,			FE_USE("quad:v0"));
			add(v1,			FE_USE("quad:v1"));
			add(v2,			FE_USE("quad:v2"));
			add(v3,			FE_USE("quad:v3"));
		}
		Accessor<SpatialVector>				v0;
		Accessor<SpatialVector>				v1;
		Accessor<SpatialVector>				v2;
		Accessor<SpatialVector>				v3;
};


/// possible bounding area/volume attributes
class AsBounded :
	virtual public AccessorSet,
	public Initialize<AsBounded>
{
	public:
		void initialize(void)
		{
			add(radius,				FE_USE("bnd:radius"));
		}
		/// spatial radius
		Accessor<Real>				radius;
};

class AsTransform :
	virtual public AccessorSet,
	public Initialize<AsTransform>
{
	public:
		void initialize(void)
		{
			add(transform,			FE_USE("spc:transform"));
		}
		/// transform
		Accessor<SpatialTransform>	transform;
};

class FE_DL_EXPORT AsColored:
	public AccessorSet,
	public Initialize<AsColored>
{
	public:
		void initialize(void)
		{
			add(color,			FE_USE("sim:color"));
		}
		Accessor<Color>			color;
};

/// Labeled
class AsLabeled :
	virtual public AccessorSet,
	public Initialize<AsLabeled>
{
	public:
		void initialize(void)
		{
			add(label,				FE_USE("sim:label"));
		}
		/// label
		Accessor<String>			label;
};


} /* namespace ext */
} /* namespace fe */

#endif /* __shape_shapeAS_h__ */

