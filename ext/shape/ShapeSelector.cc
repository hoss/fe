/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <shape/shape.pmh>

#define FE_SS_BND_PICKED		FE_USE("bnd:picked")
#define FE_SS_BND_SHAPE			FE_USE("bnd:shape")
#define FE_SS_THRESHOLD			(0.999f)

namespace fe
{
namespace ext
{

void ShapeSelector::configure(const Record& rCriteria,
		const sp<FilterI>& rspFilter)
{
	m_criteria=rCriteria;
	m_spCriteriaRG=NULL;
	m_spFilter=rspFilter;
	m_cumulative=TRUE;
}

void ShapeSelector::configure(const sp<RecordGroup>& rspCriteria,
		const sp<FilterI>& rspFilter,BWORD cumulative)
{
	m_criteria=Record();
	m_spCriteriaRG=rspCriteria;
	m_spFilter=rspFilter;
	m_cumulative=cumulative;
}

void ShapeSelector::handle(Record &record)
{
	sp<Scope> spScope=record.layout()->scope();
	if(m_operatorView.scope()!=spScope)
	{
		m_operatorView.bind(spScope);
		m_sphereRAV.bind(spScope);
	}

	if(!m_operatorView.input.check(record))
	{
		return;
	}

	const BWORD choosing=(m_operatorView.choice.check(record));
	const sp<RecordGroup>& rspInput=m_operatorView.input(record);
	sp<RecordGroup>& rspOutput=m_operatorView.output(record);

	if(m_spCriteriaRG.isValid())
	{
		if(choosing)
		{
			m_operatorView.choice(record)=pickGroup(rspOutput,rspInput,
					m_spCriteriaRG,m_spFilter,m_cumulative);
		}
		else
		{
			pickGroup(rspOutput,rspInput,
					m_spCriteriaRG,m_spFilter,m_cumulative);
		}
	}
	else
	{
		if(choosing)
		{
			m_operatorView.choice(record)=pickSingle(rspOutput,rspInput,
					m_criteria,m_spFilter);
		}
		else
		{
			pickSingle(rspOutput,rspInput,m_criteria,m_spFilter);
		}
	}
}

void ShapeSelector::resetPicked(const sp<RecordGroup>& rspSource)
{
	for(RecordGroup::iterator it=rspSource->begin();it!=rspSource->end();++it)
	{
		sp<RecordArray> spRA= *it;
		if(!m_aPicked.check(spRA))
		{
			continue;
		}

		I32 length=spRA->length();
		for(I32 index=0;index<length;index++)
		{
			m_aPicked(spRA,index)=Real(0);
		}
	}
}

Record ShapeSelector::pickGroup(sp<RecordGroup>& rspSelection,
						const sp<RecordGroup>& rspSource,
						const sp<RecordGroup>& rspCriteria,
						const sp<FilterI>& rspFilter,
						BWORD cumulative)
{
	Record closest;

	BWORD first=TRUE;
	sp<RecordGroup> spSubset=rspSource;

	if(!m_aPicked.scope().isValid())
	{
		RecordGroup::iterator it=rspCriteria->begin();
		if(it!=rspCriteria->end())
		{
			sp<RecordArray> spRA= *it;
			if(spRA->length())
			{
				sp<Scope> spScope=spRA->getRecord(0).layout()->scope();
				spScope->support(FE_SS_BND_PICKED,"real");
				m_aPicked.initialize(spScope,FE_SS_BND_PICKED);
			}
		}
	}

	resetPicked(rspSource);
	for(RecordGroup::iterator it=rspCriteria->begin();
			it!=rspCriteria->end();it++)
	{
		sp<RecordArray> spRA= *it;
		for(I32 index=0;index<spRA->length();index++)
		{
			if(!first && !cumulative && rspSelection.isValid())
			{
				spSubset=rspSelection;
				rspSelection=new RecordGroup();
			}
			closest=pick(rspSelection,spSubset,spRA->getRecord(index),
					rspFilter,(first || cumulative));
			first=FALSE;
		}
	}
	return closest;
}

Record ShapeSelector::pickSingle(sp<RecordGroup>& rspSelection,
						const sp<RecordGroup>& rspSource,
						const Record& rCriteria,
						const sp<FilterI>& rspFilter)
{
	if(!m_aPicked.scope().isValid())
	{
		sp<Scope> spScope=rCriteria.layout()->scope();
		spScope->support(FE_SS_BND_PICKED,"real");
		m_aPicked.initialize(spScope,FE_SS_BND_PICKED);
	}

	resetPicked(rspSource);
	return pick(rspSelection,rspSource,rCriteria,rspFilter,TRUE);
}

Record ShapeSelector::pick(sp<RecordGroup>& rspSelection,
		const sp<RecordGroup>& rspSource,
		const Record& rCriteria,
		const sp<FilterI>& rspFilter,
		BWORD cumulative)
{
	Record closest;

	if(rspSelection.isValid())
	{
		rspSelection->clear();
	}

	if(!m_aShape.scope().isValid())
	{
		sp<Scope> spScope=rCriteria.layout()->scope();
		spScope->support(FE_SS_BND_SHAPE,"record");
		m_aShape.initialize(spScope,FE_SS_BND_SHAPE);
	}

	//* if criteria has bnd:shape, use it, else use criteria itself
	const Record& criteria=
			(m_aShape.check(rCriteria))? m_aShape(rCriteria): rCriteria;

	m_cylinderView.bind(criteria);

	if(m_cylinderView.span.check())
	{
		closest=pickInCylinder(rspSelection,rspSource,criteria,rspFilter,
				cumulative);
	}
	else if(m_cylinderView.radius.check())
	{
		closest=pickInSphere(rspSelection,rspSource,criteria,rspFilter,
				cumulative);
	}

	//* release
	m_cylinderView.unbind();
//~	m_sphereRAV.unbind();

	return closest;
}

Record ShapeSelector::pickInCylinder(sp<RecordGroup>& rspSelection,
		const sp<RecordGroup>& rspSource,
		const Record& rCriteria,
		const sp<FilterI>& rspFilter,
		BWORD cumulative)
{
	const SpatialVector& location=m_cylinderView.location();
	const SpatialVector& span=m_cylinderView.span();
	const F32 baseRadius=m_cylinderView.baseRadius();
	const F32 endRadius=m_cylinderView.endRadius();
	const F32 range=magnitude(span);
	const F32 invRange=1.0f/range;
	const SpatialVector unitSpan=span*invRange;
	const F32 deltaRadius=(endRadius-baseRadius)*invRange;
	const Box3 bounds=aabb(location,span,baseRadius,endRadius);
	const BWORD filtered=rspFilter.isValid();
	const BWORD selecting=rspSelection.isValid();
	Box3 box;

	//* Fast mode
	if(cumulative && !selecting && !filtered)
	{
		for(RecordGroup::iterator it=rspSource->begin();it!=rspSource->end();
				++it)
		{
			sp<RecordArray> spRA= *it;
			m_sphereRAV.bind(spRA);

			if(!m_sphereRAV.recordView().radius.check(spRA))
			{
				continue;
			}

			for(Sphere& sphereView: m_sphereRAV)
			{
				Real& rPicked=sphereView.picked();
				if(rPicked>FE_SS_THRESHOLD)
				{
					continue;
				}

				const SpatialVector& center=sphereView.location();
				const F32 radius=sphereView.radius();
				set(box,center,radius);
				if(!intersecting(bounds,box))
				{
					continue;
				}

				const SpatialVector to=center-location;
				const F32 along=dot(to,unitSpan);
				if(along<0.0f || along>range+radius)
				{
					continue;
				}

				const F32 radial=baseRadius+deltaRadius*along;
				const F32 max=radial+radius;
				const F32 aside2=magnitudeSquared(to-along*unitSpan);
				const F32 max2=max*max;
				if(aside2<max2)
				{
					Real scalar=(along>range)? 1.0-(along-range)/radius: 1.0;
					rPicked=fe::minimum(1.0,rPicked+scalar*
							fe::minimum(1.0,
							(1.0-(sqrt(aside2)-radial)/radius)));
				}
			}
		}
		return Record();
	}

	F32 minAlong=range;
	Record closest;

	for(RecordGroup::iterator it=rspSource->begin();it!=rspSource->end();++it)
	{
		I32 closest_index= -1;
		sp<RecordArray> spRA= *it;
		m_sphereRAV.bind(spRA);

		if(!m_sphereRAV.recordView().radius.check(spRA))
		{
			continue;
		}

		for(RecordArrayView<Sphere>::Iterator it=m_sphereRAV.begin();
				it!=m_sphereRAV.end();++it)
		{
			Sphere& sphereView=*it;
			const I32 index=it.index();

			Real& rPicked=sphereView.picked();
			if((rPicked>FE_SS_THRESHOLD) == cumulative)
			{
				continue;
			}

			const SpatialVector& center=sphereView.location();
			const F32 radius=sphereView.radius();
			set(box,center,radius);
			if(!intersecting(bounds,box) ||
					(filtered && !rspFilter->test(sphereView.record())))
			{
				if(!cumulative)
				{
					rPicked=0.0f;
				}
				continue;
			}

			const SpatialVector to=center-location;
			const F32 along=dot(to,unitSpan);
			if(along<0.0f || along>range+radius)
			{
				if(!cumulative)
				{
					rPicked=0.0f;
				}
				continue;
			}

			const F32 radial=baseRadius+deltaRadius*along;
			const F32 max=radial+radius;
			const F32 aside2=magnitudeSquared(to-along*unitSpan);
			const F32 max2=max*max;
			if(aside2<max2)
			{
				if(minAlong>along)
				{
					minAlong=along;
					closest_index=index;
				}

				rPicked=fe::minimum(1.0,rPicked+
						1.0-(sqrt(aside2)-radial)/radius);

				if(selecting)
				{
					const Record& rRecord=spRA->getRecord(index);
					if(!rspSelection->find(rRecord))
					{
						rspSelection->add(rRecord);
					}
				}
				continue;
			}
			if(!cumulative)
			{
				rPicked=0.0f;
			}
		}
		if(closest_index>=0)
		{
			closest=spRA->getRecord(closest_index);
		}
	}
	return closest;
}

Record ShapeSelector::pickInSphere(sp<RecordGroup>& rspSelection,
		const sp<RecordGroup>& rspSource,
		const Record& rCriteria,
		const sp<FilterI>& rspFilter,
		BWORD cumulative)
{
	//* Sphere
	const SpatialVector& location=m_cylinderView.location();
	const F32 range=m_cylinderView.radius();
	const Box3 bounds=aabb(location,range);
	const BWORD filtered=rspFilter.isValid();
	const BWORD selecting=rspSelection.isValid();
	Box3 box;

	//* Fast mode
	if(cumulative && !selecting && !filtered)
	{
		for(RecordGroup::iterator it=rspSource->begin();it!=rspSource->end();
				++it)
		{
			sp<RecordArray> spRA= *it;
			m_sphereRAV.bind(spRA);

			if(!m_sphereRAV.recordView().radius.check(spRA))
			{
				continue;
			}

			for(Sphere& sphereView: m_sphereRAV)
			{
				Real& rPicked=sphereView.picked();
				if(rPicked>FE_SS_THRESHOLD)
				{
					continue;
				}

				const SpatialVector& center=sphereView.location();
				const F32 radius=sphereView.radius();
				set(box,center,radius);
				if(!intersecting(bounds,box))
				{
					continue;
				}

				const F32 distance=magnitude(center-location);
				if(distance-radius<range)
				{
					rPicked=fe::minimum(1.0,rPicked+
							1.0-(distance-range)/radius);
				}
			}
		}
		return Record();
	}

	Record closest;
	F32 minDistance=range;

	for(RecordGroup::iterator it=rspSource->begin();it!=rspSource->end();++it)
	{
		sp<RecordArray> spRA= *it;
		m_sphereRAV.bind(spRA);

		if(!m_sphereRAV.recordView().radius.check(spRA))
		{
			continue;
		}

		I32 closest_index= -1;
		for(RecordArrayView<Sphere>::Iterator it=m_sphereRAV.begin();
				it!=m_sphereRAV.end();++it)
		{
			Sphere& sphereView=*it;
			const I32 index=it.index();

			Real& rPicked=sphereView.picked();
			if((rPicked>FE_SS_THRESHOLD) == cumulative)
			{
				continue;
			}

			const SpatialVector& center=sphereView.location();
			const F32 radius=sphereView.radius();
			set(box,center,radius);
			if(!intersecting(bounds,box) ||
					(filtered && !rspFilter->test(spRA->getRecord(index))))
			{
				if(!cumulative)
				{
					rPicked=0.0f;
				}
				continue;
			}

			const F32 distance=magnitude(center-location)-radius;
			if(distance-radius<range)
			{
				if(minDistance>distance-radius)
				{
					minDistance=distance-radius;
					closest_index=index;
				}

				rPicked=fe::minimum(1.0,rPicked+
						1.0-(distance-range)/radius);
				if(selecting)
				{
					const Record& rRecord=spRA->getRecord(index);
					if(!rspSelection->find(rRecord))
					{
						rspSelection->add(rRecord);
					}
				}
				continue;
			}
			if(!cumulative)
			{
				rPicked=0.0f;
			}
		}
		if(closest_index>=0)
		{
			closest=spRA->getRecord(closest_index);
		}
	}
	return closest;
}

} /* namespace ext */
} /* namespace fe */
