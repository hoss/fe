/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __shape_Cylinder_h__
#define __shape_Cylinder_h__

FE_ATTRIBUTE("bnd:span",		"Vector centerline of cylinder");
FE_ATTRIBUTE("bnd:baseRadius",	"Bottom cap of cylinder");
FE_ATTRIBUTE("bnd:endRadius",	"Top cap of cylinder");

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Cylinder RecordView

	@ingroup shape

	Since it can have a different base and end radius,
	this is technically a conical frustrum.
*//***************************************************************************/
class FE_DL_EXPORT Cylinder: public Disk
{
	public:
		Functor<F32>			baseRadius;
		Functor<F32>			endRadius;

				Cylinder(void)		{ setName("Cylinder"); }
virtual	void	addFunctors(void)
				{
					Disk::addFunctors();

					add(baseRadius,	FE_USE("bnd:baseRadius"));
					add(endRadius,	FE_USE("bnd:endRadius"));
				}
virtual	void	initializeRecord(void)
				{
					Disk::initializeRecord();

					baseRadius()=1.0f;
					endRadius()=1.0f;
				}
};

} /* namespace ext */
} /* namespace fe */

#endif /* __shape_Cylinder_h__ */
