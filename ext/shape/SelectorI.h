/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __shape_SelectorI_h__
#define __shape_SelectorI_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Determine the Records that match a critera, such as shapes

	@ingroup shape

	As a HandlerI, marks the subset of the input that meets the criteria.
	The action of "marking" is determined by the specific implementation.

	The handle function will access the Record argument with an
	Operator AccessorSet, specifically looking for @em input,
	@em output, and @em choice.

	If @em output field is valid, it is populated with the matched records
	as a subset of the input field.

	If @em choice field is valid, some distinguished "best matching" Record
	will be determined and set.

	A FilterI, if provided, only permits records that pass that
	given arbitrary test.
*//***************************************************************************/
class FE_DL_EXPORT SelectorI:
	virtual public HandlerI,
	public CastableAs<SelectorI>
{
	public:
					/// @brief Configure selection based on a single criteria
virtual	void		configure(const Record& rCriteria,
							const sp<FilterI>& rspFilter)					{}

					/** @brief Configure selection based on a group of criteria

						Each Record in the criteria RecordGroup is applied
						in the order of iteration.

						If cumulative, a test against multiple elements
						accumulate into a union instead of an intersection. */
virtual	void		configure(const sp<RecordGroup>& rspCriteria,
							const sp<FilterI>& rspFilter,
							BWORD cumulative)								{}
};

} /* namespace ext */
} /* namespace fe */

#endif /* __shape_SelectorI_h__ */
