/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __shape_Sphere_h__
#define __shape_Sphere_h__

FE_ATTRIBUTE("spc:at",	"Local origin");
FE_ATTRIBUTE("bnd:radius",		"Spherical shell");
FE_ATTRIBUTE("bnd:picked",		"Set 0 to 1 during a selection operation");
FE_ATTRIBUTE("bnd:shape",		"Record of spatial encapsulation");

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Sphere RecordView

	@ingroup shape

	This is the base for all shapes.
	All shapes are expected to maintain this as a proper bounding sphere.
*//***************************************************************************/
class FE_DL_EXPORT Sphere: virtual public RecordView
{
	public:
		Functor<Real>			radius;
		Functor<Real>			picked;
		Functor<SpatialVector>	location;

				Sphere(void)		{ setName("Sphere"); }
virtual	void	addFunctors(void)
				{
					add(radius,		FE_USE("bnd:radius"));
					add(picked,		FE_USE("bnd:picked"));
					add(location,	FE_USE("spc:at"));
				}
virtual	void	initializeRecord(void)
				{
					set(location());
					radius()=1.0f;
					picked()=0.0f;
				}
};

} /* namespace ext */
} /* namespace fe */

#endif /* __shape_Sphere_h__ */
