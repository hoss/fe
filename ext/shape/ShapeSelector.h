/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __shape_ShapeSelector_h__
#define __shape_ShapeSelector_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Intersect a shape or shapes with a RecordGroup

	@ingroup shape

	The configured criteria should be either a Record or RecordGroup of shapes.
	Each criteria Record is checked to contain a "bnd:shape" attribute or to be
	a shape itself.

	Currently, the supported shapes are sphere and cylinder.

	As a SelectorI, "marking" a shape is defined as setting any
	"bnd:picked" atrribute to 1.
*//***************************************************************************/
class FE_DL_EXPORT ShapeSelector: virtual public SelectorI
{
	public:
					//* as HandlerI
virtual	void		handle(Record &record);

					//* as SelectorI
virtual	void		configure(const Record& rCriteria,
							const sp<FilterI>& rspFilter);

virtual	void		configure(const sp<RecordGroup>& rspCriteria,
							const sp<FilterI>& rspFilter,
							BWORD cumulative);

	private:
virtual	Record		pickSingle(sp<RecordGroup>& rspSelection,
							const sp<RecordGroup>& rspSource,
							const Record& rCriteria,
							const sp<FilterI>& rspFilter);

virtual	Record		pickGroup(sp<RecordGroup>& rspSelection,
							const sp<RecordGroup>& rspSource,
							const sp<RecordGroup>& rspCriteria,
							const sp<FilterI>& rspFilter,
							BWORD cumulative);
virtual	Record		pick(sp<RecordGroup>& rspSelection,
							const sp<RecordGroup>& rspSource,
							const Record& rCriteria,
							const sp<FilterI>& rspFilter,
							BWORD cumulative);

		Record		pickInCylinder(sp<RecordGroup>& rspSelection,
							const sp<RecordGroup>& rspSource,
							const Record& rCriteria,
							const sp<FilterI>& rspFilter,
							BWORD cumulative);

		Record		pickInSphere(sp<RecordGroup>& rspSelection,
							const sp<RecordGroup>& rspSource,
							const Record& rCriteria,
							const sp<FilterI>& rspFilter,
							BWORD cumulative);

		void		resetPicked(const sp<RecordGroup>& rspSource);

		Record					m_criteria;
		sp<RecordGroup>			m_spCriteriaRG;
		sp<FilterI>				m_spFilter;
		BWORD					m_cumulative;

		Cylinder				m_cylinderView;
		RecordArrayView<Sphere>	m_sphereRAV;
		Operator				m_operatorView;
		Accessor<Record>		m_aShape;
		Accessor<Real>			m_aPicked;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __shape_ShapeSelector_h__ */
