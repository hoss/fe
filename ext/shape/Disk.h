/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __shape_Disk_h__
#define __shape_Disk_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Disk RecordView

	@ingroup shape

	Filled circle in 3D space.
*//***************************************************************************/
class FE_DL_EXPORT Disk: public Sphere
{
	public:
		Functor<SpatialVector>	span;

				Disk(void)		{ setName("Disk"); }
virtual	void	addFunctors(void)
				{
					Sphere::addFunctors();

					add(span,	FE_USE("bnd:span"));
				}
virtual	void	initializeRecord(void)
				{
					Sphere::initializeRecord();

					set(span(),0.0f,0.0f,1.0f);
				}
};

} /* namespace ext */
} /* namespace fe */

#endif /* __shape_Disk_h__ */
