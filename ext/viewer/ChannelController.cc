/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <viewer/viewer.pmh>

namespace fe
{
namespace ext
{

ChannelController::ChannelController(void)
{
}

ChannelController::~ChannelController(void)
{
}

void ChannelController::initialize(void)
{
	m_key_press = WindowEvent::Mask(	WindowEvent::e_sourceKeyboard,
										WindowEvent::e_itemAny,
										WindowEvent::e_statePress);
	m_key_release = WindowEvent::Mask(	WindowEvent::e_sourceKeyboard,
										WindowEvent::e_itemAny,
										WindowEvent::e_stateRelease);
}

void ChannelController::handleBind(sp<SignalerI> spSignalerI, sp<Layout> l_sig)
{
	sp<Scope> spScope = l_sig->scope();
	m_asSignal.bind(spScope);

}

void ChannelController::handle(Record &r_event)
{
	Record r_windata =m_asSignal.winData(r_event, "WindowController");

	WindowEvent wev;
	wev.bind(r_event);

	if(wev.is(m_key_press))
	{
		//fe_fprintf(stderr, "item %d\n", (int)wev.item());
		String channel_name;
		channel_name.sPrintf("key_channel_%d", (int)wev.item());
		bool &state = registry()->master()->catalog()->catalog<bool>(
				channel_name);
		state = true;
	}
	if(wev.is(m_key_release))
	{
		//fe_fprintf(stderr, "item %d\n", (int)wev.item());
		String channel_name;
		channel_name.sPrintf("key_channel_%d", (int)wev.item());
		bool &state = registry()->master()->catalog()->catalog<bool>(channel_name);
		state = false;
	}
}


} /* namespace ext */
} /* namespace fe */

