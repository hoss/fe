/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <viewer/viewer.pmh>

namespace fe
{
namespace ext
{

ChannelFilter::ChannelFilter(void)
{
}

ChannelFilter::~ChannelFilter(void)
{
}

void ChannelFilter::initialize(void)
{
}

void ChannelFilter::handleBind(sp<SignalerI> spSignalerI, sp<Layout> l_signal)
{
	m_asTemporal.bind(l_signal->scope());
}

void ChannelFilter::handle(Record &r_signal)
{
	sp<Master> master = registry()->master();
	Real timestep = m_asTemporal.timestep(r_signal);

	sp<RecordGroup> rg_input =
		cfg< sp<RecordGroup> >("input"); // input group

	for(RecordGroup::iterator i_rg = rg_input->begin();
		i_rg != rg_input->end(); i_rg++)
	{
		sp<RecordArray> spRA = *i_rg;

		m_asBooleanFilter.bind(spRA->layout()->scope());

		if(m_asBooleanFilter.check(spRA))
		{
			for(int i = 0; i < spRA->length(); i++)
			{
				const Real &rest = m_asBooleanFilter.rest(spRA,i);
				Real minimum = m_asBooleanFilter.minimum(spRA,i);
				Real maximum = m_asBooleanFilter.maximum(spRA,i);
				Real rate = m_asBooleanFilter.rate(spRA,i);
				bool incr, decr;
				if(minimum > maximum)
				{
					decr = master->catalog()->catalog<bool>(m_asBooleanFilter.incr(spRA,i),false);
					incr = master->catalog()->catalog<bool>(m_asBooleanFilter.decr(spRA,i),false);
					Real tmp = maximum;
					maximum = minimum;
					minimum = tmp;
				}
				else
				{
					incr = master->catalog()->catalog<bool>(m_asBooleanFilter.incr(spRA,i),false);
					decr = master->catalog()->catalog<bool>(m_asBooleanFilter.decr(spRA,i),false);
				}

				Real &output=
					master->catalog()->catalog<Real>(m_asBooleanFilter.output(spRA,i),rest);

				if(incr == true && decr == true)
				{
				}
				else if(incr == true)
				{
					output += rate * timestep;
					if(output > maximum) { output = maximum; }
				}
				else if(decr == true)
				{
					output -= rate * timestep;
					if(output < minimum) { output = minimum; }
				}
				else
				{
					if(output > rest)
					{
						output -= rate * timestep;
						if(output < rest) { output = rest; }
					}
					if(output < rest)
					{
						output += rate * timestep;
						if(output > rest) { output = rest; }
					}
				}
			}
		}
	}
}

} /* namespace ext */
} /* namespace fe */

