/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __viewer_viewerAS_h__
#define __viewer_viewerAS_h__

#include "math/math.h"
#include "shape/shape.h"

namespace fe
{
namespace ext
{

class FE_DL_EXPORT AsChannelBinding :
	public AccessorSet,
	public Initialize<AsChannelBinding>
{
	public:
		void initialize(void)
		{
			add(record,			FE_USE("bnd:record"));
			add(attribute,		FE_USE("bnd:attribute"));
			add(channel,		FE_USE("bnd:channel"));
			add(mode,			FE_USE("bnd:mode"));
		}
		Accessor<Record>		record;
		Accessor<String>		attribute;
		Accessor<String>		channel;
		Accessor<String>		mode;
};


class FE_DL_EXPORT AsBooleanFilter :
	public AccessorSet,
	public Initialize<AsBooleanFilter>
{
	public:
		void initialize(void)
		{
			add(incr,		FE_USE("channel:incr"));
			add(decr,		FE_USE("channel:decr"));
			add(output,		FE_USE("channel:output"));
			add(rate,		FE_USE("channel:rate"));
			add(rest,		FE_USE("channel:rest"));
			add(minimum,	FE_USE("channel:min"));
			add(maximum,	FE_USE("channel:max"));
		}
		Accessor<String>		incr;
		Accessor<String>		decr;
		Accessor<String>		output;
		Accessor<Real>			rate;
		Accessor<Real>			rest;
		Accessor<Real>			minimum;
		Accessor<Real>			maximum;
};

} /* namespace ext */
} /* namespace fe */


#endif /* __viewer_viewerAS_h__ */


