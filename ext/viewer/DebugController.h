/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __viewer_DebugController_h__
#define __viewer_DebugController_h__

namespace fe
{
namespace ext
{

/**	Controller that provides some debug logging functions

	@copydoc DebugController_info
	*/
class FE_DL_EXPORT DebugController :
	virtual public Mask,
	virtual public HandlerI,
	public Initialize<DebugController>
{
	public:
					DebugController(void);
virtual				~DebugController(void);
		void		initialize(void);

		// AS HandlerI
virtual void		handleBind(sp<SignalerI> spSignalerI, sp<Layout> spLayout);
virtual	void		handle(Record &record);

	private:
		sp<Scope>			m_spScope;
		bool				m_init;
		WindowEvent::Mask	m_dumpScope;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __viewer_DebugController_h__ */

