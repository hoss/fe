/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __viewer_h__
#define __viewer_h__

#include "window/window.h"
#include "window/WindowEvent.h"
#include "draw/draw.h"
#include "shape/shape.h"

#include "viewer/viewerAS.h"

#include "viewer/IntersectorI.h"
#include "viewer/PointI.h"
#include "viewer/CameraControllerI.h"
#include "viewer/ViewerI.h"
#include "viewer/SignalerViewerI.h"
#include "viewer/EventMapI.h"
#include "viewer/QuickViewerI.h"
#include "viewer/MaskMapI.h"
#include "viewer/MaskI.h"

namespace fe
{
namespace ext
{

Library* CreateViewerLibrary(sp<Master> spMaster);

} /* namespace ext */
} /* namespace fe */

#endif /* __viewer_h__ */
