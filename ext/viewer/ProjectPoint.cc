/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "ProjectPoint.h"

namespace fe
{
namespace ext
{

ProjectPoint::ProjectPoint(void)
{
}

ProjectPoint::~ProjectPoint(void)
{
}

void ProjectPoint::initialize(void)
{
}

void ProjectPoint::handle(Record &r_sig)
{
	if(!m_drawview.handle(r_sig)) { return; }

	if(!m_drawview.drawI().isValid()) { return; }

	sp<ViewI> spView(m_drawview.drawI()->view());

	if(!spView.isValid()) { return; }


	sp<RecordGroup> rg_input = cfg< sp<RecordGroup> >("input");

	for(RecordGroup::iterator it = rg_input->begin();
		it != rg_input->end(); it++)
	{
		sp<RecordArray> spRA(*it);
		m_asProjected.bind(spRA->layout()->scope());
		m_asPoint.bind(spRA->layout()->scope());
		if(m_asProjected.check(spRA) && m_asPoint.check(spRA))
		{
			for(int i = 0; i < spRA->length(); i++)
			{
				m_asProjected.location(spRA,i) =
					spView->project(m_asPoint.location(spRA,i));
			}
		}
	}
}

} /* namespace ext */
} /* namespace fe */

