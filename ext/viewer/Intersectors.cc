/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <viewer/viewer.pmh>

namespace fe
{
namespace ext
{

#if 0
WorldSphereIntersector::WorldSphereIntersector(void)
{
}

WorldSphereIntersector::~WorldSphereIntersector(void)
{
}

bool WorldSphereIntersector::intersect(Real &a_distance, Record r_object, const Vector2 &a_projected, const SpatialVector &a_root, const SpatialVector &a_direction)
{
	return intersectRaySphere(&a_distance,
		a_root,
		a_direction,
		m_asSelSphere.location(r_object),
		m_asSelSphere.radius(r_object));
}

AccessorSet &WorldSphereIntersector::filter(void)
{
	return m_asSelSphere;
}

WorldTriangleIntersector::WorldTriangleIntersector(void)
{
}

WorldTriangleIntersector::~WorldTriangleIntersector(void)
{
}

bool WorldTriangleIntersector::intersect(Real &a_distance, Record r_object, const Vector2 &a_projected, const SpatialVector &a_root, const SpatialVector &a_direction)
{
	SpatialVector intersection;
	if(intersectRayTriangle(intersection,
		a_root,
		a_root+a_direction,
		m_asSelTri.vertA(r_object),
		m_asSelTri.vertB(r_object),
		m_asSelTri.vertC(r_object)))
	{
		a_distance = magnitude(intersection - a_root);
		return true;
	}
	return false;
}

AccessorSet &WorldTriangleIntersector::filter(void)
{
	return m_asSelTri;
}

ScreenTriangleIntersector::ScreenTriangleIntersector(void)
{
}

ScreenTriangleIntersector::~ScreenTriangleIntersector(void)
{
}

bool ScreenTriangleIntersector::intersect(Real &a_distance, Record r_object, const Vector2 &a_projected, const SpatialVector &a_root, const SpatialVector &a_direction)
{
	if(pointInTriangle(	SpatialVector(a_projected[0],a_projected[1],0.0),
						m_asSelTri.vertA(r_object),
						m_asSelTri.vertB(r_object),
						m_asSelTri.vertC(r_object)))
	{
		a_distance = 0.0;
		return true;
	}
	return false;
}

AccessorSet &ScreenTriangleIntersector::filter(void)
{
	return m_asSelTri;
}
#endif

IntersectRaySphere::IntersectRaySphere(void)
{
}

IntersectRaySphere::~IntersectRaySphere(void)
{
}

Record IntersectRaySphere::intersect(Real &a_distance, sp<RecordGroup> rg_output, sp<RecordGroup> rg_input, const SpatialVector &a_root, const SpatialVector &a_direction)
{
	Record r_closest;

	if(!rg_input.isValid()) { return r_closest; }

	Real close = 1.0e30;
	for(RecordGroup::iterator i_rg = rg_input->begin();
		i_rg != rg_input->end(); i_rg++)
	{
		sp<RecordArray> spRA = *i_rg;
		m_asSelSphere.bind(spRA->layout()->scope());
		if(m_asSelSphere.check(spRA))
		{
			for(int i = 0; i < spRA->length(); i++)
			{
				Real distance;
				if(intersectRaySphere(&distance,
					a_root,
					a_direction,
					m_asSelSphere.location(spRA, i),
					m_asSelSphere.radius(spRA, i)))
				{
					if(distance < close)
					{
						a_distance = distance;
						close = distance;
						r_closest = spRA->getRecord(i);
					}
					if(rg_output.isValid())
					{
						rg_output->add(spRA->getRecord(i));
					}
				}
			}
		}
	}
	return r_closest;
}


IntersectPointTriangle::IntersectPointTriangle(void)
{
}

IntersectPointTriangle::~IntersectPointTriangle(void)
{
}

Record IntersectPointTriangle::intersect(Real &a_distance, sp<RecordGroup> rg_output, sp<RecordGroup> rg_input, const Vector2 &a_projected)
{
	Record r_closest;

	if(!rg_input.isValid()) { return r_closest; }

	for(RecordGroup::iterator i_rg = rg_input->begin();
		i_rg != rg_input->end(); i_rg++)
	{
		sp<RecordArray> spRA = *i_rg;
		m_asSelTri.bind(spRA->layout()->scope());
		if(m_asSelTri.check(spRA))
		{
			for(int i = 0; i < spRA->length(); i++)
			{
				if(pointInTriangle(
						SpatialVector(a_projected[0],a_projected[1],0.0),
						m_asSelTri.vertA(spRA,i),
						m_asSelTri.vertB(spRA,i),
						m_asSelTri.vertC(spRA,i)))
				{
					a_distance = 0.0;
					r_closest = spRA->getRecord(i);
					if(rg_output.isValid())
					{
						rg_output->add(spRA->getRecord(i));
					}
				}
			}
		}
	}
	return r_closest;
}

IntersectRectPoint::IntersectRectPoint(void)
{
}

IntersectRectPoint::~IntersectRectPoint(void)
{
}

void IntersectRectPoint::intersect(sp<RecordGroup> rg_output, sp<RecordGroup> rg_input, const Vector2 &a_box_lo, const Vector2 &a_box_hi, sp<ViewI> spView)
{
	if(!rg_input.isValid()) { return; }
	if(!rg_output.isValid()) { return; }

	Vector3 screen;

	for(RecordGroup::iterator i_rg = rg_input->begin();
		i_rg != rg_input->end(); i_rg++)
	{
		sp<RecordArray> spRA = *i_rg;
		m_asPoint.bind(spRA->layout()->scope());
		m_asProjected.bind(spRA->layout()->scope());
		if(m_asPoint.check(spRA))
		{
			bool has_project = m_asProjected.check(spRA);
			for(int i = 0; i < spRA->length(); i++)
			{
				Record r_item = spRA->getRecord(i);
				if(has_project)
				{
					screen = m_asProjected.location(r_item);
				}
				else
				{
					screen = spView->project(m_asPoint.location(r_item));
				}
				if(screen[0] < a_box_lo[0] || screen[0] > a_box_hi[0])
				{
					continue;
				}
				if(screen[1] < a_box_lo[1] || screen[1] > a_box_hi[1])
				{
					continue;
				}
				rg_output->add(r_item);
			}
		}
	}
}

} /* namespace ext */
} /* namespace fe */

