/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <viewer/viewer.pmh>

namespace fe
{
namespace ext
{

bool b_and(const WindowEvent &a_wev, const WindowEvent::Mask &a_mask)
{
	if(!(a_wev.source() & a_mask.source())) { return false; }
	if(!(a_wev.item() & a_mask.item())) { return false; }
	if(!(a_wev.state() & a_mask.state())) { return false; }
	return true;
}

SelectController::SelectController(void)
{
}

SelectController::~SelectController(void)
{
}

void SelectController::handleBind(sp<SignalerI> spSignaler, sp<Layout> l_sig)
{
	m_spScope = l_sig->scope();
	m_asSel.bind(m_spScope);
	m_asSignal.bind(m_spScope);
	m_asPick.bind(m_spScope);
	m_asWindata.bind(m_spScope);
}

void SelectController::initialize(void)
{
	WindowEvent::Item button = (WindowEvent::Item)(
		(unsigned int)WindowEvent::e_itemLeft |
		(unsigned int)WindowEvent::e_itemMiddle |
		(unsigned int)WindowEvent::e_itemRight);

	// default event mapping
	maskInit("fe_select");
	maskDefault("fe_select_start",
		WindowEvent::Mask(	WindowEvent::e_sourceMouseButton,
							button,
							WindowEvent::e_statePress));

	maskDefault("fe_select_end",
		WindowEvent::Mask(	WindowEvent::e_sourceMouseButton,
							button,
							WindowEvent::e_stateRelease));

	maskDefault("fe_select_drag",
		WindowEvent::Mask(	WindowEvent::e_sourceMousePosition,
							WindowEvent::e_itemDrag,
							WindowEvent::e_stateAny));

	maskDefault("fe_select_clear",
		WindowEvent::Mask(	WindowEvent::e_sourceKeyboard,
							(WindowEvent::Item)27 /* esc */,
							WindowEvent::e_statePress));

	sp<IntersectRayI> spWP;
	spWP = registry()->create("IntersectRayI.Sphere");
	if(spWP.isValid()) { m_worldPicks.push_back(spWP); }

	sp<IntersectPointI> spSP;
	spSP = registry()->create("IntersectPointI.Triangle");
	if(spSP.isValid()) { m_screenPicks.push_back(spSP); }

	sp<IntersectRectI> spSS;
	spSS = registry()->create("IntersectRectI.Point");
	if(spSS.isValid()) { m_screenSelects.push_back(spSS); }

	sp<RecordGroup> rg_invalid;
	cfg< sp<RecordGroup> >("selectfrom") = rg_invalid;
}


void SelectController::handleSignal(Record &r_sig, sp<SignalerI> spSignaler)
{
	Record r_event = r_sig;
	Record r_windata = m_asSignal.winData(r_event, "SelectController");
	if(!r_windata.isValid()) { feX("invalid windata"); }
	checkWindata(r_windata);
	m_asSel.check(r_windata, "SelectController");

	WindowEvent wev;
	wev.bind(r_event);

	sp<Layout> l_sig = r_sig.layout();


	m_start = maskGet("fe_select_start");
	m_end = maskGet("fe_select_end");
	m_drag = maskGet("fe_select_drag");
	m_clear = maskGet("fe_select_clear");

	bool was_enabled = enabled();

	maskEnabled(wev);

	if(was_enabled && !enabled() && (m_asSel.selecting(r_windata) & 4))
	{
		react("off_selecting");
		m_asSel.selecting(r_windata) &= ~4;
	}

	if(b_and(wev,m_start))
	{
		if(wev.item() & WindowEvent::e_keyShift)
		{
			m_asSel.selecting(r_windata) = 2;
		}
		else
		{
			m_asSel.selecting(r_windata) = 1;
		}
		if(enabled())
		{
			m_asSel.selecting(r_windata) |= 4;
		}
	}

	if(b_and(wev,m_start) || b_and(wev,m_drag) || b_and(wev,m_end))
	{
		handlePick(r_sig, spSignaler);
	}

	if(b_and(wev,m_end))
	{
		if(m_asSel.selecting(r_windata) & 4)
		{
			// move marked to selected
			if(m_asSel.selecting(r_windata) & 2)
			{
				for(RecordGroup::iterator i_marked =
					m_asSel.marked(r_windata)->begin();
					i_marked != m_asSel.marked(r_windata)->end(); i_marked++)
				{
					sp<RecordArray> spRA = *i_marked;
					for(int i = 0; i < spRA->length(); i++)
					{
						Record r_marked = spRA->getRecord(i);
						if(m_asSel.selected(r_windata)->find(r_marked))
						{
							m_asSel.selected(r_windata)->remove(r_marked);
						}
						else
						{
							m_asSel.selected(r_windata)->add(r_marked);
						}
					}
				}
			}
			else
			{
				m_asSel.selected(r_windata)->clear();
				m_asSel.selected(r_windata)->add(m_asSel.marked(r_windata));
			}
		}
	}

	if(wev.is(m_clear))
	{
		m_asSel.selected(r_windata)->clear();
		m_asSel.selecting(r_windata) = 0;
		m_asSel.marked(r_windata)->clear();
	}

	if(b_and(wev,m_end))
	{
		m_asSel.selecting(r_windata) = 0;
		m_asSel.marked(r_windata)->clear();
	}
}

void SelectController::handlePick(Record &r_sig, sp<SignalerI> spSignaler)
{
	Record r_event = r_sig;
	Record r_windata = m_asSignal.winData(r_sig, "SelectController");

	hp<DrawI> spDraw = m_asWindata.drawI(r_windata, "SelectController");


	bool has_pick = false;
	if(m_asPick.check(r_windata))
	{
		has_pick = true;
	}


	sp<RecordGroup> rg_input = cfg< sp<RecordGroup> >("selectfrom");


	sp<RecordGroup> rg_selectable(new RecordGroup());

	if(rg_input.isValid())
	{
		m_asSelectable.filter(rg_selectable, rg_input);
	}

	WindowEvent wev;
	wev.bind(r_event);

	Vector<4,float> worldPt, tmpPt, vecPt;

	I32 mx,my;
	wev.getMousePosition(&mx,&my);
	I32 winy=height(spDraw->view()->viewport())-1-my;
	worldPt = spDraw->view()->unproject(mx, winy, 0.0f);
	vecPt = spDraw->view()->unproject(mx, winy, 1.0f);
	vecPt -= worldPt;


	if(b_and(wev,m_start))
	{
		react("on_select");
		if(m_asSel.selecting(r_windata) & 4) { react("on_selecting"); }
		m_asSel.start(r_windata)[0] = wev.mouseX();
		m_asSel.start(r_windata)[1] = wev.mouseY();
		m_asSel.end(r_windata)[0] = wev.mouseX();
		m_asSel.end(r_windata)[1] = wev.mouseY();
		m_asSel.prev(r_windata)[0] = wev.mouseX();
		m_asSel.prev(r_windata)[1] = wev.mouseY();
		if(has_pick)
		{
			m_asPick.start(r_windata) = worldPt;
			m_asPick.end(r_windata) = worldPt;
			m_asPick.startv(r_windata) = vecPt;
			m_asPick.endv(r_windata) = vecPt;
		}
	}

	if(b_and(wev,m_end))
	{
		m_asSel.prev(r_windata)[0] = m_asSel.end(r_windata)[0];
		m_asSel.prev(r_windata)[1] = m_asSel.end(r_windata)[1];
		m_asSel.end(r_windata)[0] = wev.mouseX();
		m_asSel.end(r_windata)[1] = wev.mouseY();
		if(has_pick)
		{
			m_asPick.end(r_windata) = worldPt;
			m_asPick.endv(r_windata) = vecPt;
			// TODO: currently sets to z==0 plane.  Improve to something better
			tmpPt = worldPt + vecPt;
			vecPt[0] = worldPt[0] +
				(-worldPt[2])*(vecPt[0])/(vecPt[2]);
			vecPt[1] = worldPt[1] +
				(-worldPt[2])*(vecPt[1])/(vecPt[2]);
			vecPt[2] = 0.0;
			m_asPick.focus(r_windata) = vecPt;

			sp<RecordGroup> rg_dummy;

			if(rg_input.isValid() && (m_asSel.selecting(r_windata) & 4))
			{
				Vector2 project_sel = m_asSel.end(r_windata);
				project_sel[1] =
						height(spDraw->view()->viewport()) -
						project_sel[1];
				Real close = 1.0e30;
				Record closest;
				for(unsigned int i_x = 0;i_x < m_worldPicks.size();i_x++)
				{
					Real t;
					Record r_t = m_worldPicks[i_x]->intersect(t,
						rg_dummy,
						rg_input,
						m_asPick.end(r_windata),
						m_asPick.endv(r_windata));
					if(r_t.isValid())
					{
						if(t < close)
						{
							close = t;
							closest = r_t;
						}
					}
				}
				for(unsigned int i_x = 0;i_x < m_screenPicks.size();i_x++)
				{
					Real t;
					Record r_t = m_screenPicks[i_x]->intersect(t,
						rg_dummy,
						rg_input,
						project_sel);
					if(r_t.isValid())
					{
						if(t < close)
						{
							close = t;
							closest = r_t;
						}
					}
				}

				if(closest.isValid())
				{
					m_asSelParent.bind(closest.layout()->scope());
					if(m_asSelParent.check(closest))
					{
						if(m_asSelParent.group(closest).isValid())
						{
							m_asSel.marked(r_windata)->
								add(m_asSelParent.group(closest));
						}
					}
					else
					{
						m_asSel.marked(r_windata)->add(closest);
					}
				}
			}
		}
		react("off_select");
		if((m_asSel.selecting(r_windata) & 4)) { react("off_selecting"); }
	}

	if(b_and(wev,m_drag))
	{
		if(m_asSel.selecting(r_windata))
		{
			m_asSel.prev(r_windata)[0] = m_asSel.end(r_windata)[0];
			m_asSel.prev(r_windata)[1] = m_asSel.end(r_windata)[1];
			m_asSel.end(r_windata)[0] = wev.mouseX();
			m_asSel.end(r_windata)[1] = wev.mouseY();
			if(has_pick)
			{
				m_asPick.end(r_windata) = worldPt;
				m_asPick.endv(r_windata) = vecPt;
			}

			float h = height(spDraw->view()->viewport());
			Real lx, hx, ly, hy;
			if(m_asSel.start(r_windata)[0] < m_asSel.end(r_windata)[0])
			{
				lx = m_asSel.start(r_windata)[0];
				hx = m_asSel.end(r_windata)[0];
			}
			else
			{
				hx = m_asSel.start(r_windata)[0];
				lx = m_asSel.end(r_windata)[0];
			}
			if(m_asSel.start(r_windata)[1] < m_asSel.end(r_windata)[1])
			{
				ly = m_asSel.start(r_windata)[1];
				hy = m_asSel.end(r_windata)[1];
			}
			else
			{
				hy = m_asSel.start(r_windata)[1];
				ly = m_asSel.end(r_windata)[1];
			}

			Vector2 lo, hi;
			lo[0] = lx;
			lo[1] = h - hy;
			hi[0] = hx;
			hi[1] = h - ly;

			if((m_asSel.selecting(r_windata) & 4))
			{
				m_asSel.marked(r_windata)->clear();
				for(unsigned int i_x = 0;i_x < m_screenSelects.size();i_x++)
				{
					m_screenSelects[i_x]->intersect(
						m_asSel.marked(r_windata),
						rg_selectable,
						lo,
						hi,
						spDraw->view());
				}
			}
		}
	}
}


void SelectController::checkWindata(Record &r_windata)
{
	if(!m_asSel.marked(r_windata).isValid())
	{
		m_asSel.marked(r_windata) = new RecordGroup();
	}
	if(!m_asSel.selected(r_windata).isValid())
	{
		m_asSel.selected(r_windata) = new RecordGroup();
	}
}

} /* namespace ext */
} /* namespace fe */


