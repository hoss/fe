/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __viewer_ObjectViewer_h__
#define __viewer_ObjectViewer_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Viewer designed to inspect a single object

	@ingroup viewer
*//***************************************************************************/
class FE_DL_EXPORT ObjectViewer:
		public Initialize<ObjectViewer>,
		virtual public SignalerViewerI
{
	public:
						ObjectViewer(void);
virtual					~ObjectViewer(void);
		void			initialize(void);

						//* As ViewerI
virtual	void			bind(sp<WindowI> spWindowI);
virtual	void			bind(sp<DrawI> spDrawI);
virtual	sp<CameraControllerI>	getCameraControllerI(void) const
						{	return m_spCameraControllerI; }
virtual void			setCameraControllerI(
							sp<CameraControllerI> a_spCameraControllerI)
						{	m_spCameraControllerI=a_spCameraControllerI; }
virtual	sp<DrawI>		getDrawI(void) const		{ return m_spDrawI; }
virtual	void			insertBeatHandler(sp<HandlerI> spHandlerI);
virtual	void			insertEventHandler(sp<HandlerI> spHandlerI);
virtual void			insertDrawHandler(sp<HandlerI> spHandlerI);
virtual void			render(Real a_time,Real a_timestep);
virtual Real			frameRate(void) const		{ return m_frameRate; }

	private:

		void			start(void);

		sp<SignalerI>			m_spEventSignaler;
		sp<SignalerI>			m_spDrawSignaler;
		sp<Scope>				m_spScope;
		sp<Layout>				m_spBeatLayout;
		sp<Layout>				m_spRenderLayout;

		Record					m_beat;
		Record					m_underlay;
		Record					m_render;
		Record					m_overlay;

		sp<DrawI>				m_spDrawI;
		sp<CameraControllerI>	m_spCameraControllerI;
		sp<CameraI>				m_spCameraPerspective;
		sp<CameraEditable>		m_spCameraOverlay;

		AsViewer				m_asViewer;

		BWORD					m_started;
		Real					m_frameMS;
		Real					m_frameRate;
		Real					m_frameTotal;
		U32						m_frameCount;
		SystemTicker			m_ticker;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __viewer_ObjectViewer_h__ */
