/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <viewer/viewer.pmh>

#define FRAMECOUNT_PERIOD	500.0f	//* ms

namespace fe
{
namespace ext
{

PerspectiveViewer::PerspectiveViewer(void)
{
	m_frameMS=0.0f;
	m_frameRate=0.0f;
	m_frameCount=0;
	m_frameTotal=0.0f;
}

PerspectiveViewer::~PerspectiveViewer(void)
{
}

void PerspectiveViewer::initialize(void)
{
	m_view = registry()->create("ViewI");
}

Real PerspectiveViewer::frameRate(void) const
{
	return m_frameRate;
}

void PerspectiveViewer::bind(fe::sp<WindowI> spWindowI)
{
	m_spWindowI=spWindowI;
}

void PerspectiveViewer::handle(fe::Record& r_sig)
{
	if(!m_spWindowI.isValid())
	{
		feX("PerspectiveViewer::handle",
			"not bound to a valid window");
	}

	const Real ms=m_ticker.timeMS();
	m_frameTotal+=ms-m_frameMS;
	m_frameCount++;
	if(m_frameTotal>FRAMECOUNT_PERIOD)
	{
		m_frameRate=m_frameCount*1e3f/m_frameTotal;
		m_frameTotal=0.0f;
		m_frameCount=0;
	}
	m_frameMS=ms;

	Real nearplane=0.1f;
	Real farplane=10e3;
	Real fovy=30.0f;

	fe::Box2i vp(Vector2i(0, 0));
	resize(vp, m_spWindowI->geometry().size());

	sp<DrawI> spDraw = m_asWindata.drawI(m_asSignal.winData(r_sig));
	spDraw->setView(m_view);
	m_view->setViewport(vp);

	sp<CameraI> spCamera=m_view->camera();
	spCamera->setFov(Vector2(0.0,fovy));
	spCamera->setPlanes(nearplane,farplane);

	SpatialTransform *pCameraMatrix = m_paPerspMatrix(r_sig);
	if(pCameraMatrix)
	{
		spCamera->setCameraMatrix(*(pCameraMatrix));
	}
	m_view->use(ViewI::e_perspective);
}

void PerspectiveViewer::handleBind(fe::sp<SignalerI> spSignalerI,
		fe::sp<Layout> spLayout)
{
	m_paPerspMatrix.setup(	String::join(".",	FE_USE("win:data"),
												FE_USE("persp:matrix")));
	m_asSignal.bind(spLayout->scope());
	m_asWindata.bind(spLayout->scope());
}



} /* namespace ext */
} /* namespace fe */

