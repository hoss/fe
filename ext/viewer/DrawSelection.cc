/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <viewer/viewer.pmh>

namespace fe
{
namespace ext
{

DrawSelection::DrawSelection(void)
{
}

DrawSelection::~DrawSelection(void)
{
}

void DrawSelection::initialize(void)
{
	maskInit("fe_drawselection");
	enabled() = false;
}


void DrawSelection::handle(Record &r_sig)
{
	WindowEvent wev;
	wev.bind(r_sig);
	if(!maskEnabled(wev)) { return; }
	if(wev.check(r_sig)) { return; }



	sp<Scope> spScope = r_sig.layout()->scope();
	m_asSignal.bind(spScope);
	m_asSel.bind(spScope);
	m_asWindata.bind(spScope);
	if(!m_asSignal.winData.check(r_sig)) { return; }
	Record r_windata = m_asSignal.winData(r_sig);

	if(!m_asWindata.check(r_windata)) { return; }
	if(!m_asSel.check(r_windata)) { return; }

	sp<DrawI> spDraw = m_asWindata.drawI(r_windata);

	if(m_asWindata.windowI(r_windata).isValid())
	{
		Real x1,y1,x2,y2;
		x1 = m_asSel.start(r_windata)[0];
		y1 = m_asSel.start(r_windata)[1];
		x2 = m_asSel.end(r_windata)[0];
		y2 = m_asSel.end(r_windata)[1];

		sp<WindowI> spWindow(m_asWindata.windowI(r_windata));
		Real h = height(spWindow->geometry());
		if(m_asSel.selecting(r_windata))
		{
			drawBox(spDraw, x1,h-y1,x2,h-y2);
		}
		drawMarker(spDraw, x2,h-y2);
	}
}

void DrawSelection::drawBox(sp<DrawI> spDraw, Real x1, Real y1,
		Real x2, Real y2)
{
	Box3 box;
	set(box, x1, y1, Real(0), x2-x1, y2-y1, Real(0));
	const Color yellow(1.0f,1.0f,0.0f);
	const Color green(0.0f,1.0f,0.0f, 0.2f);
	spDraw->drawBox(box, yellow);
	SpatialVector r[2];
	r[0]=SpatialVector(x1,y1,0.0);
	r[1]=SpatialVector(x2,y2,0.0);
	spDraw->drawRectangles(r,2,false,&green);
}

void DrawSelection::drawMarker(sp<DrawI> spDraw, Real x, Real y)
{
	SpatialVector v[2];
	v[0]=SpatialVector(x-10.5, y, 0.0);
	v[1]=SpatialVector(x+10.5, y, 0.0);
	const Color red(1.0f,0.0f,0.0f);
	spDraw->drawLines(v, NULL, 2, DrawI::e_strip, false, &red);
	v[0]=SpatialVector(x, y-10.5, 0.0);
	v[1]=SpatialVector(x, y+10.5, 0.0);
	const Color green(0.0f,1.0f,0.0f);
	spDraw->drawLines(v, NULL, 2, DrawI::e_strip, false, &green);
}

} /* namespace ext */
} /* namespace fe */

