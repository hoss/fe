/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __viewer_SelectController_h__
#define __viewer_SelectController_h__

//#include "dataui/datauiAS.h"

namespace fe
{
namespace ext
{

/**	Basic screen space selection controller.

	This controller simply maintains the start and end point of selections
	in screen space.

	This controller handles event mapped signals (EventMap) and sets
	AsSelection attributes in the window data record.

	Use the slot() method to set the layouts driving the controller.

	@copydoc SelectController_info
	*/
class FE_DL_EXPORT SelectController :
	virtual public HandlerI,
	virtual public Config,
	virtual public Mask,
	virtual	public Reactor,
	Initialize<SelectController>
{
	public:
		SelectController(void);
virtual	~SelectController(void);

		void			initialize(void);

		// AS HandlerI
virtual void			handleBind(sp<SignalerI> spSignaler, sp<Layout> l_sig);
virtual	void			handleSignal(Record &record, sp<SignalerI> spSignaler);

	private:
//virtual	void			handleMark(Record &r_sig);
virtual	void			handlePick(Record &r_sig, sp<SignalerI> spSignaler);

		void			checkWindata(Record &r_windata);

		sp<Scope>			m_spScope;
		AsSelectable		m_asSelectable;
		AsSelection			m_asSel;
		AsSelParent			m_asSelParent;
		AsSignal			m_asSignal;
		AsPoint				m_asPoint;
		AsBounded			m_asBounded;
		AsPick				m_asPick;
		AsWindata			m_asWindata;
		AsParticle			m_asParticle;
		AsProjected			m_asProjected;
		WindowEvent::Mask	m_start;
		WindowEvent::Mask	m_end;
		WindowEvent::Mask	m_drag;
		WindowEvent::Mask	m_clear;
		std::vector< sp<IntersectRectI> >		m_screenSelects;
		std::vector< sp<IntersectPointI> >		m_screenPicks;
		std::vector< sp<IntersectRayI> >		m_worldPicks;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __viewer_SelectController_h__ */

