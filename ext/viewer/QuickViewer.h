/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __viewer_QuickViewer_h__
#define __viewer_QuickViewer_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Self-configuring delegate for ObjectViewer

	@ingroup viewer
*//***************************************************************************/
class FE_DL_EXPORT QuickViewer:
		public Initialize<QuickViewer>,
		virtual public QuickViewerI
{
	public:
						QuickViewer(void);
virtual					~QuickViewer(void);
		void			initialize(void);

						//* As QuickViewerI
virtual	Real			frameRate(void) const
						{	return m_spObjectViewer->frameRate(); }
virtual void			bind(sp<WindowI> spWindowI)
						{	m_spWindowI=spWindowI; }
virtual	void			bind(sp<DrawI> spDrawI);
virtual	sp<CameraControllerI>	getCameraControllerI(void) const
						{	return m_spObjectViewer->getCameraControllerI(); }
virtual	sp<DrawI>		getDrawI(void) const
						{	return m_spObjectViewer->getDrawI(); }
virtual	sp<WindowI>		getWindowI(void) const
						{	return m_spWindowI; }
virtual	SpatialVector	interestPoint(void) const
						{	return m_spInterestHandler->m_spCameraControllerI
								->interestMatrix().translation(); }
virtual	SpatialVector	auxilliaryPoint(void) const
						{	return m_spInterestHandler->m_spCameraControllerI
								->auxillaryMatrix().translation(); }
virtual	void			insertBeatHandler(sp<HandlerI> spHandlerI);
virtual	void			insertEventHandler(sp<HandlerI> spHandlerI);
virtual void			insertDrawHandler(sp<HandlerI> spHandlerI);
virtual	void			open(void);
virtual	void			reopen(void);
virtual	void			run(U32 frames);

	private:

	class InterestHandler:
		public HandlerI,
		public CastableAs<InterestHandler>
	{
		public:
						InterestHandler(void)								{}

	virtual	void		bind(sp<DrawI> spDrawI)
						{	m_spDrawI=spDrawI; }
	virtual	void		bind(sp<CameraControllerI> spCameraControllerI)
						{	m_spCameraControllerI=spCameraControllerI; }

						//* As HandlerI
	virtual	void		handleBind(sp<SignalerI> spSignalerI,
							sp<Layout> spLayout);
	virtual void		handle(Record& render);

			sp<DrawI>				m_spDrawI;
			sp<CameraControllerI>	m_spCameraControllerI;
			AsViewer				m_asViewer;
	};

		BWORD					m_opened;
		sp<WindowI>				m_spWindowI;
		sp<SignalerViewerI>		m_spObjectViewer;
		sp<InterestHandler>		m_spInterestHandler;
		sp<HandlerI>			m_spJoyHandler;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __viewer_QuickViewer_h__ */
