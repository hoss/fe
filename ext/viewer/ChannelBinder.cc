/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <viewer/viewer.pmh>

namespace fe
{
namespace ext
{

ChannelBinder::ChannelBinder(void)
{
}

ChannelBinder::~ChannelBinder(void)
{
}

void ChannelBinder::initialize(void)
{
	m_vectorType = registry()->master()->typeMaster()->lookupType< SpatialVector >();
	m_realType = registry()->master()->typeMaster()->lookupType< Real >();
}

void ChannelBinder::handleBind(sp<SignalerI> spSignalerI, sp<Layout> spLayout)
{
	m_asChannelBinding.bind(spLayout->scope());
	m_asInit.bind(spLayout->scope());
}

void ChannelBinder::handle(Record &r_signal)
{
	sp<RecordGroup> rg_input =
		cfg< sp<RecordGroup> >("input"); // input group

	if(m_asInit.check(r_signal))
	{
		for(RecordGroup::iterator i_rg = rg_input->begin();
			i_rg != rg_input->end(); i_rg++)
		{
			sp<RecordArray> spRA = *i_rg;
			if(m_asChannelBinding.check(spRA))
			{
				for(int i = 0; i < spRA->length(); i++)
				{
					Record r_target = m_asChannelBinding.record(spRA, i);
					String &attr_name = m_asChannelBinding.attribute(spRA, i);
					String &channel_name = m_asChannelBinding.channel(spRA, i);
					String &mode = m_asChannelBinding.mode(spRA, i);

					if(mode == "export")
					{
						r_target.extractInstance(
							registry()->master()->catalog()->catalogInstance(channel_name),
							attr_name);
					}
				}
			}
		}
	}
	else
	{
#ifdef FE_BOOST_TOKENIZER
		boost::char_separator<char> sep(" ");
		typedef boost::tokenizer<boost::char_separator<char> > t_tokenizer;
		//typedef boost::tokenizer< > t_tokenizer;
#endif
		for(RecordGroup::iterator i_rg = rg_input->begin();
			i_rg != rg_input->end(); i_rg++)
		{
			sp<RecordArray> spRA = *i_rg;
			if(m_asChannelBinding.check(spRA))
			{
				for(int i = 0; i < spRA->length(); i++)
				{
					Record r_target = m_asChannelBinding.record(spRA, i);
					String &attr_name = m_asChannelBinding.attribute(spRA, i);
					String &channel_name = m_asChannelBinding.channel(spRA, i);
					String &mode = m_asChannelBinding.mode(spRA, i);

					std::vector<std::string> tokens;
					std::string s(mode.c_str());
#ifdef FE_BOOST_TOKENIZER
					t_tokenizer tkns(s, sep);
					for(t_tokenizer::iterator i_t = tkns.begin();
							i_t != tkns.end(); ++i_t)
					{
						tokens.push_back(std::string(*i_t).c_str());
					}
#else
					String buffer=mode;
					String token;
					while(!(token=buffer.parse(""," ")).empty())
					{
						tokens.push_back(token.c_str());
					}
#endif

					if(mode == "push")
					{
						Instance instance_record;
						r_target.extractInstance(instance_record, attr_name);
						Instance &instance_catalog =
							registry()->master()->catalog()->catalogInstance(channel_name);

						if(instance_catalog.data() == NULL)
						{
							instance_catalog.create(instance_record.type());
						}

						instance_catalog.assign(instance_record);
					}
#if 0
					else if(mode == "pull")
					{
						Instance instance_record;
						r_target.extractInstance(instance_record, attr_name);
						Instance &instance_catalog =
							registry()->master()->catalog()->catalogInstance(channel_name);

						if(instance_catalog.data() == NULL)
						{
							instance_catalog.create(instance_record.type());
							instance_catalog.assign(instance_record);
						}

						instance_record.assign(instance_catalog);
					}
#endif
					else if(tokens[0] == "pull")
					{
						Instance instance_record;
						r_target.extractInstance(instance_record, attr_name);
						Instance &instance_catalog =
							registry()->master()->catalog()->catalogInstance(channel_name);

						if(tokens.size() == 1)
						{
							if(instance_catalog.data() == NULL)
							{
								instance_catalog.create(instance_record.type());
								instance_catalog.assign(instance_record);
							}

							instance_record.assign(instance_catalog);
						}
						else if(tokens[1] == "vector3" || tokens[1] == "-vector3")
						{
							if (instance_record.type() != m_vectorType)
							{
								feX("channel type mismatch");
							}

							Real sign = 1.0;
							int index = abs(atoi(tokens[2].c_str()));
							if(tokens[1].c_str()[0] == '-')
							{
								sign = -1.0;
							}

							SpatialVector &dst_vec = instance_record.cast<SpatialVector>();

							if(instance_catalog.data() == NULL)
							{
								instance_catalog.create(m_realType);
								instance_catalog.cast<Real>() = sign * dst_vec[index];
							}

							if (instance_catalog.type() != m_realType)
							{
								feX("channel type mismatch");
							}

							Real &src_real = instance_catalog.cast<Real>();

							dst_vec[index] = sign * src_real;
						}
						else if(tokens[1] == "real" || tokens[1] == "-real")
						{
							if (instance_record.type() != m_realType)
							{
								feX("real channel type mismatch");
							}

							Real sign = 1.0;
							if(tokens[1].c_str()[0] == '-')
							{
								sign = -1.0;
							}

							Real &dst_real = instance_record.cast<Real>();

							if(instance_catalog.data() == NULL)
							{
								instance_catalog.create(m_realType);
								instance_catalog.cast<Real>() = sign * dst_real;
							}

							if (instance_catalog.type() != m_realType)
							{
								feX("real channel type mismatch");
							}

							Real &src_real = instance_catalog.cast<Real>();

							dst_real = sign * src_real;
						}
						else
						{
							feX("ChannelBinder",
								"unknown channel type specifier: %s", tokens[1].c_str());
						}
					}
				}
			}
		}
	}
}

} /* namespace ext */
} /* namespace fe */

