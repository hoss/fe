/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <viewer/viewer.pmh>

#define INSPECT_DEBUG		FALSE
#define INSPECT_TRACK		FALSE
#define INSPECT_ZOOM_IN_UP	TRUE	// mouse up zooms in (Houdini vs Maya)

namespace fe
{
namespace ext
{

namespace
{
typedef enum
{
	e_azimuth=0,
	e_inclination,
	e_translationX,	//* screen/world lateral
	e_translationY, //* world forward
	e_translationZ,	//* world Z
	e_translationV,	//* screen vertical
	e_aux0,
	e_aux1,
	e_zoom			//* pan-out distance, not FOV like with optics
} MouseMode;
}

#define FE_CAMERA_DISTANCE	1.0f	// in units of model height
#define FE_CAMERA_Z			0.0f	// in units of model height
#define FE_CAMERA_PHI		30.0f
#define FE_CAMERA_AZIMUTH	0.0f

InspectController::InspectController(void)
{
	m_rotationLock=FALSE;
	m_closeEvent=FALSE;
	m_leftRight=FALSE;
	m_alted=FALSE;
	m_ctrled=FALSE;
	m_shifted=FALSE;
	m_indexX=e_azimuth;
	m_indexY=e_inclination;

	static const Real defaultMouseScale[MOUSEMODES]=
	{	0.5f,0.5f,		// azimuth,inclination
		0.004f,0.004f,	// translationX,translationY
		0.004f,0.004f,	// translationZ,e_translationV
		0.001f,0.001f,	// aux0,aux1
		0.02f			// zoom
		};

	m_modelScale=4.0f;	// TODO

	U32 m;
	for(m=0;m<WindowEvent::e_keyUsed;m++)
	{
		m_keyCount[m]=0;
		m_keyMax[m]=1;
	}
	for(m=0;m<MOUSEMODES;m++)
	{
		m_fMouse[m]=0.0f;
		m_dfMouse[m]=0.0f;
		m_dfJoy[m]=0.0f;
		m_mouseScale[m]=defaultMouseScale[m];
		m_mouseState[m]=0;
	}

	m_dfMouse[e_azimuth]=1.0f;

	m_mouseScale[e_translationX]*=m_modelScale;
	m_mouseScale[e_translationY]*=m_modelScale;
	m_mouseScale[e_translationZ]*=m_modelScale;
	m_mouseScale[e_zoom]*= -m_modelScale;

	m_fMouse[e_azimuth]=FE_CAMERA_AZIMUTH;
	m_fMouse[e_inclination]=FE_CAMERA_PHI;
	m_fMouse[e_translationZ]=FE_CAMERA_Z*m_modelScale;
	m_fMouse[e_zoom]=FE_CAMERA_DISTANCE*m_modelScale;
	m_fMouse[e_aux0]=1.0f;

	m_spCameraCore=new CameraEditable();
	m_spCameraCore->setName("InspectController.CameraEditable");
//~	m_spCameraCore->setProjection(CameraI::e_perspective);

	updateMatrix();

	m_paPerspMatrix.setup(String::join(".",	FE_USE("win:data"),
											FE_USE("persp:matrix")));

	set(m_vertical,0,0,1);
}

InspectController::~InspectController(void)
{
}

Real InspectController::mouseDial(U32 mode) const
{
	if(mode<MOUSEMODES)
		return m_fMouse[mode];
	return 0.0f;
}

U32 InspectController::keyCount(U32 key) const
{
	if(key<WindowEvent::e_keyUsed)
		return m_keyCount[key];
	return 0;
}

U32 InspectController::setKeyCount(U32 key,U32 count)
{
	if(key<WindowEvent::e_keyUsed)
	{
		if(count>m_keyMax[key])
			count=m_keyMax[key];
		m_keyCount[key]=count;
		return count;
	}
	return 0;
}

void InspectController::setKeyMax(U32 key,U32 max)
{
	if(key<WindowEvent::e_keyUsed)
		m_keyMax[key]=max;
}

SpatialVector InspectController::lookPoint(void) const
{
	SAFEGUARD;

	return SpatialVector(m_fMouse[e_translationX],
			m_fMouse[e_translationY],m_fMouse[e_translationZ]);
}

Real InspectController::azimuth(void) const
{
	return m_fMouse[e_azimuth];
}

Real InspectController::downangle(void) const
{
	return m_fMouse[e_inclination];
}

Real InspectController::distance(void) const
{
	return m_fMouse[e_zoom];
}

void InspectController::setVertical(SpatialVector a_vertical)
{
	m_vertical=a_vertical;
}

void InspectController::setLookPoint(SpatialVector a_point)
{
	SAFEGUARD;

	m_fMouse[e_translationX]=a_point[0];
	m_fMouse[e_translationY]=a_point[1];
	m_fMouse[e_translationZ]=a_point[2];
	updateMatrix();
}

void InspectController::setLookAngle(Real azimuth,Real downangle,Real distance)
{
	SAFEGUARD;

	m_fMouse[e_azimuth]=azimuth;
	m_fMouse[e_inclination]=downangle;
	m_fMouse[e_zoom]=distance;
	updateMatrix();
}

void InspectController::useCustomCamera(sp<CameraI> a_spCamera)
{
	SAFEGUARD;

	m_spCameraCustom=a_spCamera;
}

sp<CameraI> InspectController::camera(void) const
{
	return m_spCameraCustom.isValid()?
			m_spCameraCustom: sp<CameraI>(m_spCameraCore);
}

void InspectController::handle(Record &record)
{
	SAFEGUARD;

	m_event.bind(record);

#if FALSE
	if(!m_event.isIdleMouse() && !m_event.isPoll())
		feLog("InspectController::handle %s\n",print(m_event).c_str());
#endif

	react(m_event);
	updateMatrix();

	SpatialTransform *pMatrix = m_paPerspMatrix(record);
	if(pMatrix)
	{
		*pMatrix = m_spCameraCustom.isValid()?
				m_spCameraCustom->cameraMatrix():
				m_spCameraCore->cameraMatrix();
	}
}

void InspectController::updateMatrix(void)
{
//	feLog("phi %.2f theta %.2f\n",
//			m_fMouse[e_inclination],m_fMouse[e_azimuth]);

	sp<CameraI> spCamera=camera();
	if(spCamera.isValid())
	{
		//* NOTE non-positive zoom can be bad
		if(m_fMouse[e_zoom]<1e-3)
		{
			m_fMouse[e_zoom]=1e-3;
		}

		//* NOTE set valid ortho even if camera will used as perspective
		Real zoom;
		Vector2 center;
		spCamera->getOrtho(zoom,center);
		zoom=1e3/m_fMouse[e_zoom];	//* should account for viewport
		spCamera->setOrtho(zoom,center);

		SpatialTransform cameraMatrix;

		if(m_rotationLock)
		{
			//* leave rotation as is
			cameraMatrix=spCamera->cameraMatrix();
			setTranslation(cameraMatrix,SpatialVector(0.0,0.0,0.0));
		}
		else
		{
			setIdentity(cameraMatrix);

			//* TODO any vertical (not just Y or Z)
			if(m_vertical[1]>0.9)
			{
				//* Y-up

				//* camera-space pan (zoom)
				translate(cameraMatrix,
						SpatialVector(0.0f,0.0,-m_fMouse[e_zoom]));
				//* phi
				rotate(cameraMatrix,m_fMouse[e_inclination]*degToRad,e_xAxis);
				//* theta
				rotate(cameraMatrix,m_fMouse[e_azimuth]*degToRad,e_yAxis);

				rotate(cameraMatrix,90.0f*degToRad,e_yAxis);
			}
			else
			{
				//* Z-up

				//* flat and level
				rotate(cameraMatrix,-90.0f*degToRad,e_xAxis);
				//* camera-space pan (zoom)
				translate(cameraMatrix,
						SpatialVector(0.0f,m_fMouse[e_zoom],0.0f));
				//* phi
				rotate(cameraMatrix,m_fMouse[e_inclination]*degToRad,e_xAxis);
				//* theta
				rotate(cameraMatrix,m_fMouse[e_azimuth]*degToRad,e_zAxis);

				rotate(cameraMatrix,90.0f*degToRad,e_zAxis);
			}
		}

		//* world-space translation
		translate(cameraMatrix,SpatialVector(-m_fMouse[e_translationX],
				-m_fMouse[e_translationY],-m_fMouse[e_translationZ]));

		spCamera->setCameraMatrix(cameraMatrix);
	}

	setIdentity(m_interestMatrix);
	translate(m_interestMatrix,SpatialVector(m_fMouse[e_translationX],
			m_fMouse[e_translationY],m_fMouse[e_translationZ]));

	m_auxillaryMatrix=m_interestMatrix;
	translate(m_auxillaryMatrix,SpatialVector(0.0f,m_fMouse[e_aux0],0.0f));
}

void InspectController::react(const WindowEvent &windowEvent)
{
#if INSPECT_DEBUG
	if(!m_event.isIdleMouse() && !m_event.isPoll())
	{
		feLog("%p InspectController::react %s\n",this,
				print(windowEvent).c_str());
	}
#endif

	I32 mx,my;
	windowEvent.getMousePosition(&mx,&my);
	I32 buttons=windowEvent.mouseButtons();

	m_leftRight=(buttons==(WindowEvent::e_itemLeft|WindowEvent::e_itemRight));
	m_alted=false;
	m_ctrled=false;
	m_shifted=false;
	WindowEvent::Item item=windowEvent.item();

	if(windowEvent.usesModifiers())
	{
		if(item&WindowEvent::e_keyAlt)
		{
			item=WindowEvent::Item(item^WindowEvent::e_keyAlt);
			m_alted=true;
		}
		if(item&WindowEvent::e_keyControl)
		{
			item=WindowEvent::Item(item^WindowEvent::e_keyControl);
			m_ctrled=true;
		}
		if(item&WindowEvent::e_keyShift)
		{
			item=WindowEvent::Item(item^WindowEvent::e_keyShift);
			m_shifted=true;
		}
	}

	if(windowEvent.isCloseWindow())
	{
//		feLog("CloseWindow windowEvent\n");
		m_closeEvent=TRUE;
	}
	else if(windowEvent.isKeyPress(WindowEvent::e_itemAny))
	{
//		I32 index= (item&255);
		I32 index=item;

		// convert to lower case
		if(index>='A' && index<='Z')
			index+='a'-'A';

		if(index<WindowEvent::e_keyUsed)
		{
			if(m_shifted)
			{
				if(m_keyCount[index]==0)
				{
					m_keyCount[index]=m_keyMax[index];
				}
				else
				{
					m_keyCount[index]--;
				}
			}
			else
			{
				m_keyCount[index]++;
				if(m_keyCount[index]>m_keyMax[index])
				{
					m_keyCount[index]=0;
				}
			}
		}
	}
	else if(m_alted || m_ctrled)
	{
//		feLog("ctrled %s\n",print(windowEvent).c_str());
		if(windowEvent.isMouseWheel())
		{
			const Real wheelClickScale=0.5f;
			const Real zoomDelta=wheelClickScale*
					windowEvent.state()*m_mouseScale[e_zoom]*zoomscalar();

//			feLog("%d %.6G    \n",windowEvent.state(),m_mouseScale[e_zoom]);
#if INSPECT_ZOOM_IN_UP
			m_fMouse[e_zoom]+=zoomDelta;
#else
			m_fMouse[e_zoom]-=zoomDelta;
#endif
		}
		else if(windowEvent.isMousePress())
		{
			m_indexX=e_azimuth;				//* spin
			m_indexY=e_inclination;

			if(item==WindowEvent::e_itemMiddle)
			{
				m_indexX=e_translationX;	//* pan in screen plane
				m_indexY=e_translationV;
			}
			else if(item==WindowEvent::e_itemRight)
			{
				m_indexX=e_aux1;
				m_indexY=e_zoom;
			}
			else if(m_leftRight)
			{
				m_indexX=e_translationX;	//* pan on ground plane
				m_indexX=e_translationY;
			}

			windowEvent.getMousePosition(&m_mouseState[m_indexX],
					&m_mouseState[m_indexY]);

			Reactor::react("on_inspect");
		}
		else if(windowEvent.isMouseDrag())
		{
//			feLog("\n%d,%d  %d,%d     \n",mx,my,
//					m_mouseState[m_indexX],m_mouseState[m_indexY]);
			m_dfMouse[m_indexX]=(Real)mx-m_mouseState[m_indexX];
			m_dfMouse[m_indexY]=(Real)my-m_mouseState[m_indexY];
			m_mouseState[m_indexX]=mx;
			m_mouseState[m_indexY]=my;
		}
		else if(windowEvent.isMouseRelease())
		{
			Reactor::react("off_inspect");
		}
	}
	else if(windowEvent.isMouseButton())
	{
		if(windowEvent.isMouseWheel())
		{
			m_fMouse[e_aux0]+=windowEvent.state()*m_mouseScale[e_aux0];
		}
	}
	else if(windowEvent.isIdleMouse() && buttons)
	{
		m_dfMouse[m_indexX]=0.0f;
		m_dfMouse[m_indexY]=0.0f;
	}
	else if(windowEvent.isJoy())
	{
		if(windowEvent.isJoyAxis())
		{
			const Real transScale=5.0f;

			Real value=windowEvent.state();

			//* dead zone
			value= fabs(value)>2500? value*1e-4f: 0.0f;

			if(value)
			{
				m_dfMouse[e_azimuth]=0;
				m_dfMouse[e_inclination]=0;
			}

			switch(item)
			{
				//* slide
				case WindowEvent::e_joyLeftStickX:
					m_dfJoy[e_translationX]= -transScale*value;
					break;
				case WindowEvent::e_joyLeftStickY:
					m_dfJoy[e_translationY]=transScale*value;
					break;
				case WindowEvent::e_joyLeftPadY:
					m_dfJoy[e_translationZ]=transScale*value;
					break;

				//* spin
				case WindowEvent::e_joyRightStickX:
					m_dfJoy[e_azimuth]=value;
					break;
				case WindowEvent::e_joyRightStickY:
					m_dfJoy[e_inclination]= -value;
					break;

				default:
					break;
			}
		}
		else
		{
			Real value=10.0f * windowEvent.state();

			switch(item)
			{
				//* zoom
				case WindowEvent::e_joyRightUp:
					m_dfJoy[e_zoom]= value;
					break;
				case WindowEvent::e_joyRightDown:
					m_dfJoy[e_zoom]= -value;
					break;

				default:
					break;
			}
		}
	}

	if(m_rotationLock)
	{
		m_dfMouse[e_azimuth]=0;
		m_dfMouse[e_inclination]=0;
	}

	idleFunction();
}

Real InspectController::zoomscalar(void)
{
	return m_fMouse[e_zoom]/(10.0f*m_modelScale);
}

void InspectController::idleFunction(void)
{
	BWORD moving=FALSE;
	for(I32 m=e_translationX;m<=e_translationV;m++)
	{
		if(m_dfMouse[m]!=0.0f || m_dfJoy[m]!=0.0f)
		{
			moving=TRUE;
		}
	}
	if(moving)
	{
#if FALSE
		const Real heading=m_fMouse[e_azimuth]*degToRad;
		const Real cosh=cosf(heading);
		const Real sinh=sinf(heading);

		const Real pitch=m_fMouse[e_inclination]*degToRad;
		const Real cosp=cosf(pitch);
		const Real sinp=sinf(pitch);
#endif

		sp<CameraI> spCamera=camera();
		SpatialTransform cameraMatrix=spCamera->cameraMatrix();


		const Real dfx=m_dfMouse[e_translationX]+m_dfJoy[e_translationX];
		const Real dfy=m_dfMouse[e_translationY]+m_dfJoy[e_translationY];
		const Real dfz=m_dfMouse[e_translationZ]+m_dfJoy[e_translationZ];
		const Real dfv=m_dfMouse[e_translationV]+m_dfJoy[e_translationV];

		//* TODO any vertical (not just Y or Z)
		if(m_vertical[1]>0.9)
		{
			const Real cosh=cameraMatrix(0,2);
			const Real sinh= -cameraMatrix(0,0);
			const Real cosp=cameraMatrix(1,1);
			const Real sinp=cameraMatrix(2,1);

			const Real up=dfv*sinp+dfy;

			m_dfMouse[e_translationX]=up*cosh+dfx*sinh;
			m_dfMouse[e_translationY]=dfv*cosp+dfz;
			m_dfMouse[e_translationZ]=up*sinh-dfx*cosh;
		}
		else
		{
			const Real cosh= -cameraMatrix(0,1);
			const Real sinh= -cameraMatrix(0,0);
			const Real cosp=cameraMatrix(1,2);
			const Real sinp=cameraMatrix(2,2);

			const Real up=dfv*sinp+dfz;

			m_dfMouse[e_translationX]=up*cosh+dfx*sinh;
			m_dfMouse[e_translationY]= -up*sinh+dfx*cosh;
			m_dfMouse[e_translationZ]=dfv*cosp+dfy;
		}
	}

	for(I32 m=0;m<MOUSEMODES;m++)
	{
		if(m>=e_translationX && m<=e_translationV)
		{
			m_fMouse[m]+=m_dfMouse[m]*m_mouseScale[m]*zoomscalar();
		}
		else if(m==e_zoom)
		{
			//* NOTE zoom on both axes
#if INSPECT_ZOOM_IN_UP
			m_fMouse[m]-=(m_dfMouse[m]-m_dfMouse[e_aux1]+m_dfJoy[m])*
					m_mouseScale[m]*zoomscalar();
#else
			m_fMouse[m]+=(m_dfMouse[m]+m_dfMouse[e_aux1]+m_dfJoy[m])*
					m_mouseScale[m]*zoomscalar();
#endif
		}
		else
		{
			m_fMouse[m]+=(m_dfMouse[m]+m_dfJoy[m])*m_mouseScale[m];
		}

		if(m<2)
		{
			if(m_fMouse[m]<0.0f)
				m_fMouse[m]+=360.0f;
			if(m_fMouse[m]>360.0f)
				m_fMouse[m]-=360.0f;
		}
	}

#if INSPECT_TRACK
	feLog("[H");
	feLog("\n\n\nindex=%d %d\n",m_indexX,m_indexY);
	feLog("azimuth=%.6G inclination=%.6G                       \n",
			e_azimuth,e_inclination);
	for(I32 m=0;m<MOUSEMODES;m++)
	{
		feLog("%d %.6G %.6G %.6G         \n",
				m,m_fMouse[m],m_dfMouse[m],m_dfJoy[m]);
	}
#endif

	// can't "throw" a pan
	m_dfMouse[e_translationX]=0;
	m_dfMouse[e_translationY]=0;
	m_dfMouse[e_translationZ]=0;
	m_dfMouse[e_translationV]=0;
	m_dfMouse[e_aux0]=0;
	m_dfMouse[e_aux1]=0;
	m_dfMouse[e_zoom]=0;

	if(!m_keyCount[U32('T')])
	{
		m_dfMouse[e_azimuth]=0;
		m_dfMouse[e_inclination]=0;
	}
	else
	{
		feLog("throwing\n");
	}
}

} /* namespace ext */
} /* namespace fe */
