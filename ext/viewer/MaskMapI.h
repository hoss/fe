/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __viewer_MaskMapI_h__
#define __viewer_MaskMapI_h__

#include "window/WindowEvent.h"

namespace fe
{
namespace ext
{

class FE_DL_EXPORT MaskMapI:
	virtual public Component,
	public CastableAs<MaskMapI>
{
	public:
virtual	bool	get(WindowEvent::Mask &a_mask, const String &a_name)		= 0;
virtual	void	set(const WindowEvent::Mask &a_mask, const String &a_name)	= 0;
virtual	void	all(std::vector< std::pair<String, WindowEvent::Mask> >
						&a_entries)											= 0;
};

} /* namespace ext */
} /* namespace fe */


#endif /* __viewer_MaskMapI_h__ */

