/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __viewer_ProjectPoint_h__
#define __viewer_ProjectPoint_h__

#include "signal/signal.h"
#include "draw/draw.h"
#include "shape/shape.h"
#include "window/window.h"
#include "viewer/DrawView.h"

namespace fe
{
namespace ext
{

class FE_DL_EXPORT ProjectPoint :
	public Initialize<ProjectPoint>,
	virtual public Config,
	virtual public HandlerI
{
	public:
				ProjectPoint(void);
virtual			~ProjectPoint(void);
		void	initialize(void);

virtual void	handle(Record &r_sig);

	private:
		DrawView						m_drawview;
		AsProjected						m_asProjected;
		AsPoint							m_asPoint;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __viewer_ProjectPoint_h__ */

