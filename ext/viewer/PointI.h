/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __viewer_PointI_h__
#define __viewer_PointI_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief A location in 3D space

	@ingroup viewer
*//***************************************************************************/
class FE_DL_EXPORT PointI:
	virtual public Component,
	public CastableAs<PointI>
{
	public:
		typedef enum
		{
			e_flagNull=			0x00,
			e_flagHold=			0x01,
			e_flagHold2=		0x02,
			e_flagHold3=		0x04
		} Flags;

						/** @brief Get the point matching a window mask

							The implementation may distinguish different
							points based on the mask with its own criteria.
							The only restriction is that every mask is
							associated for some point.

							An example would be six available points:
							three mouse buttons,
							each with a press and release point.
						*/
virtual SpatialVector&	point(const WindowEvent::Mask& mask)				=0;
virtual
const	SpatialVector&	point(const WindowEvent::Mask& mask) const			=0;

virtual	U32				flags(void) const									=0;

						/** @brief Associate with a ViewI for anything
							screen-based

							Note that there is no assumption related to the
							ViewI used by the DrawI in draw(),
							which can be different.
						*/
virtual	void			setView(sp<ViewI> spViewI)							=0;

						/// @brief Render the point in some fashion
virtual	void			drawFeedback(sp<DrawI> spDrawI) const				=0;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __viewer_PointI_h__ */
