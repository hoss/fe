/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __viewer_SignalerViewerI_h__
#define __viewer_SignalerViewerI_h__

namespace fe
{
namespace ext
{

//* TODO move token method implementations to a ViewerBase

/**************************************************************************//**
	@brief Simple viewer interface

	@ingroup viewer
*//***************************************************************************/
class FE_DL_EXPORT SignalerViewerI:
	virtual public ViewerI,
	public CastableAs<SignalerViewerI>
{
	public:
virtual	void			insertBeatHandler(sp<HandlerI> spHandlerI)			{}
virtual	void			insertEventHandler(sp<HandlerI> spHandlerI)			{}
virtual void			insertDrawHandler(sp<HandlerI> spHandlerI)			{}

						/// Send signals for one frame
virtual void			render(Real a_time,Real a_timestep)					=0;
};

/**	SignalerViewerI attributes
	*/
class FE_DL_EXPORT AsViewer:
	public AsTemporal,
	public Initialize<AsViewer>
{
	public:
		void initialize(void)
		{
			add(viewer_layer,		FE_USE("viewer:layer"));
			add(viewer_spatial,		FE_USE("viewer:spatial"));
			add(render_draw,		FE_USE("ren:DrawI"));
		}
		/// rendering layer
		Accessor<I32>				viewer_layer;
		/// 1 if rendering in 3D (vs overlay/underlay)
		Accessor<I32>				viewer_spatial;
		Accessor< sp<Component> >	render_draw;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __viewer_SignalerViewerI_h__ */
