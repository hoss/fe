/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <viewer/viewer.pmh>

namespace fe
{
namespace ext
{

EventMap::EventMap(void)
{
}

EventMap::~EventMap(void)
{
}

void EventMap::initialize(void)
{
}

void EventMap::handleBind(sp<SignalerI> spSignalerI, sp<Layout> spLayout)
{
	m_spScope = spLayout->scope();
	m_spSignaler = spSignalerI;
	m_asSignal.bind(m_spScope);
}

void EventMap::bind(WindowEvent::Source source, WindowEvent::Item item, WindowEvent::State state, sp<Layout> resultSignalLayout)
{
	resultSignalLayout->populate(m_asSignal.event);
	resultSignalLayout->populate(m_asSignal.winData);
	m_sourceMap[source][item][state].push_back(resultSignalLayout);
}

void EventMap::handle(Record &record)
{
	WindowEvent wev;
	wev.bind(record);

	handleSource(wev, m_sourceMap);

}

void EventMap::handleSource(WindowEvent &wev, t_source_item &sourceMap)
{
	if(wev.source() == WindowEvent::e_sourceAny)
	{
		for(t_source_item::iterator i_src = sourceMap.begin();
			i_src != sourceMap.end(); i_src++)
		{
			handleItem(wev, i_src->second);
		}
	}
	else
	{
		t_source_item::iterator i_src;
		i_src = sourceMap.find(wev.source());
		if(i_src != sourceMap.end())
		{
			handleItem(wev, i_src->second);
		}
		handleItem(wev, sourceMap[WindowEvent::e_sourceAny]);
	}
}

void EventMap::handleItem(WindowEvent &wev, t_item_state &itemMap)
{
	if(wev.item() == WindowEvent::e_itemAny)
	{
		for(t_item_state::iterator i_item = itemMap.begin();
			i_item != itemMap.end(); i_item++)
		{
			handleState(wev, i_item->second);
		}
	}
	else
	{
		t_item_state::iterator i_item;
		i_item = itemMap.find(wev.item());
		if(i_item != itemMap.end())
		{
			handleState(wev, i_item->second);
		}
		handleState(wev, itemMap[WindowEvent::e_itemAny]);
	}
}

void EventMap::handleState(WindowEvent &wev, t_state_layout &stateMap)
{
	if(wev.state() == WindowEvent::e_stateAny)
	{
		for(t_state_layout::iterator i_state = stateMap.begin();
			i_state != stateMap.end(); i_state++)
		{
			fireSignal(wev, i_state->second);
		}
	}
	else
	{
		t_state_layout::iterator i_state;
		i_state = stateMap.find(wev.state());
		if(i_state != stateMap.end())
		{
			fireSignal(wev, i_state->second);
		}
		fireSignal(wev, stateMap[WindowEvent::e_stateAny]);
	}
}

void EventMap::fireSignal(WindowEvent &wev, t_layout_list &layouts)
{
	for(t_layout_list::iterator i_layout = layouts.begin();
		i_layout != layouts.end(); i_layout++)
	{
		Record r_sig = m_spScope->createRecord(*i_layout);
		if(m_asSignal.event.check(r_sig))
		{
			m_asSignal.event(r_sig) = wev.record();
		}
		if(m_asSignal.winData.check(r_sig))
		{
			if(m_asSignal.winData.check(wev.record()))
			{
				m_asSignal.winData(r_sig) = m_asSignal.winData(wev.record());
			}
		}
		signal(r_sig);
	}
}

} /* namespace ext */
} /* namespace fe */

