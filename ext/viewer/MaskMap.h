/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __viewer_MaskMap_h__
#define __viewer_MaskMap_h__

#include "viewer/MaskMapI.h"

namespace fe
{
namespace ext
{

class FE_DL_EXPORT MaskMap : virtual public MaskMapI
{
	public:
				MaskMap(void);
virtual			~MaskMap(void);
virtual	bool	get(WindowEvent::Mask &a_mask, const String &a_name);
virtual	void	set(const WindowEvent::Mask &a_mask, const String &a_name);
virtual	void	all(std::vector< std::pair<String, WindowEvent::Mask> >
						&a_entries);

	private:

		std::map<String, WindowEvent::Mask>		m_map;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __viewer_MaskMap_h__ */

