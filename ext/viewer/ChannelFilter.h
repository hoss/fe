/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __viewer__ChannelFilter_h__
#define __viewer__ChannelFilter_h__

namespace fe
{
namespace ext
{

/**	Channel filter

	@copydoc ChannelFilter_info
	*/
class FE_DL_EXPORT ChannelFilter :
	virtual public HandlerI,
	virtual public Config,
	public Initialize<ChannelFilter>
{
	public:
				ChannelFilter(void);
virtual			~ChannelFilter(void);

		void	initialize(void);

		// AS HandlerI
virtual void	handleBind(sp<SignalerI> spSignalerI, sp<Layout> l_signal);
virtual	void	handle(Record &r_signal);

	private:
		AsTemporal			m_asTemporal;
		AsBooleanFilter		m_asBooleanFilter;
};


} /* namespace ext */
} /* namespace fe */

#endif
