/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __viewer_DrawSelection_h__
#define __viewer_DrawSelection_h__

#include "window/WindowEvent.h"
#include "viewer/Mask.h"

namespace fe
{
namespace ext
{

/**	Draw a box indicating the selected region

	@copydoc DrawSelection_info
	*/
class FE_DL_EXPORT DrawSelection:
	public Initialize<DrawSelection>,
	virtual public Mask,
	virtual public HandlerI
{
	public:
				DrawSelection(void);
virtual			~DrawSelection(void);

		void	initialize(void);

virtual void	handle(	fe::Record &record);

	private:
		void	drawBox(sp<DrawI> spDraw,Real x1,Real y1,Real x2,Real y2);
		void	drawMarker(sp<DrawI> spDraw, Real x, Real y);

		AsSelection		m_asSel;
		AsWindata		m_asWindata;
		AsSignal		m_asSignal;

		sp<DrawMode>	m_spBoxMode;
};


} /* namespace ext */
} /* namespace fe */


#endif /* __viewer_DrawSelection_h__ */
