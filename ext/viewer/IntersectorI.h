/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */


#ifndef __viewer_IntersectorI_h__
#define __viewer_IntersectorI_h__

namespace fe
{
namespace ext
{

#if 0
class FE_DL_EXPORT IntersectorI : virtual public Component
{
	public:
virtual	bool		intersect(	Real &a_distance,
								Record r_object,
								const Vector2 &a_projected,
								const SpatialVector &a_root,
								const SpatialVector &a_direction)			= 0;
virtual	AccessorSet	&filter(	void)										= 0;
};
#endif

/**	If rg_output is valid, populate
	with all intersecting records */
class FE_DL_EXPORT IntersectRectI : virtual public Component
{
	public:
virtual	void		intersect(	sp<RecordGroup> rg_output,
								sp<RecordGroup> rg_input,
								const Vector2 &a_rect_lo,
								const Vector2 &a_rect_hi,
								sp<ViewI> spView)							= 0;
};

/**	Return closest intersecting record.  If rg_output is valid, populate
	with all intersecting records */
class FE_DL_EXPORT IntersectPointI : virtual public Component
{
	public:
virtual	Record		intersect(	Real &a_distance,
								sp<RecordGroup> rg_output,
								sp<RecordGroup> rg_input,
								const Vector2 &a_projected)					= 0;
};

/**	Return closest intersecting record.  If rg_output is valid, populate
	with all intersecting records */
class FE_DL_EXPORT IntersectRayI : virtual public Component
{
	public:
virtual	Record		intersect(	Real &a_distance,
								sp<RecordGroup> rg_output,
								sp<RecordGroup> rg_input,
								const SpatialVector &a_root,
								const SpatialVector &a_direction)			= 0;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __viewer_IntersectorI_h__ */

