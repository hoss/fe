/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __viewer_Mask_h__
#define __viewer_Mask_h__

#include "viewer/MaskI.h"

namespace fe
{
namespace ext
{

class FE_DL_EXPORT Mask : virtual public MaskI
{
	public:
							Mask(void);
virtual						~Mask(void){}
virtual	void				maskSet(const String &key,
								const WindowEvent::Mask &value);
virtual	void				maskDefault(const String &key,
								const WindowEvent::Mask &value);
virtual	WindowEvent::Mask	maskGet(const String &key);

virtual	Record				maskCreate(const sp<Layout> l_mask,
								const String &key);
virtual	void				maskInit(const String &a_prefix);
virtual	bool				maskEnabled(const WindowEvent &wev);
virtual	void				maskEnable(bool a_setting);

	protected:
		bool				&enabled(void) { return m_enabled; }
		void				cookKey(String &a_cooked, const String &a_raw);

	private:
		bool				m_enabled;
		String				m_prefix;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __viewer_Mask_h__ */

