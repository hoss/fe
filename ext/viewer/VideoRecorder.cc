/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <viewer/viewer.pmh>

#include <math.h>

namespace fe
{
namespace ext
{

// png2yuv -Ip -L0 -f 60 -j video.%d.png | mpeg2enc -f0 -a1 -o xTree.m2v

// find -name "*.tga" -print0 | xargs -n1 -0 tgatoppm |
// ppmtoy4m -F 60:1 -S 420mpeg2 -v 1 |
// mpeg2enc -o xTree.m2v -v 1 -vbr -b 10000 -V 1000

void VideoRecorder::initialize(void)
{
	if(m_spImageI.isNull())
	{
		m_spImageI=registry()->create("ImageI");
		if(m_spImageI.isNull())
		{
			feLog("VideoRecorder::initialize failed to create ImageI\n");
			return;
		}

		m_spImageI->createSelect();
		m_spImageI->setFormat(ImageI::e_rgb);
	}
}

void VideoRecorder::handle(Record& record)
{
	if(!m_asViewer.scope().isValid())
	{
		m_asViewer.bind(record.layout()->scope());
	}
	if(!m_asViewer.viewer_spatial.check(record) ||
			!m_asViewer.viewer_layer.check(record) ||
			m_asViewer.viewer_layer(record)!=2)
	{
		return;
	}

	if(m_spImageI.isNull())
	{
		feLog("VideoRecorder::handle no image support loaded\n");
		return;
	}

	if(!m_asTemporal.scope().isValid())
	{
		m_asTemporal.bind(record.layout()->scope());
		m_aDrawI.initialize(record.layout()->scope(),FE_USE("ren:DrawI"));

		System::createDirectory(m_basename.c_str());
	}

	if(!m_asTemporal.timestep.check(record))
	{
		feLog("VideoRecorder::handle no timestep\n");
		return;
	}

	if(!m_aDrawI.check(record))
	{
		feLog("VideoRecorder::handle no draw\n");
		return;
	}

	sp<DrawI> spDrawI=m_aDrawI(record);
	sp<ViewI> spViewI=spDrawI->view();
	sp<WindowI> spWindowI=spViewI->window();
	Box2i geometry=spWindowI->geometry();

	const Real timestep=m_asTemporal.timestep(record);
	if(timestep==0.0f)
	{
		feLog("VideoRecorder::handle zero timestep\n");
		return;
	}

	if(m_index>=m_ringSize)
	{
		m_index=0;
	}

	String filename;
	filename.sPrintf("%s/video.%04d.tga",m_basename.c_str(),m_index);
	feLog("VideoRecorder::handle write %s\n",filename.c_str());

	const U32 x=0;
	const U32 y=0;
	const U32 z=0;
	const U32 depth=1;
	const GLenum format=GL_RGB;
	const GLenum type=GL_UNSIGNED_BYTE;

	U32 sizex=width(geometry);
	U32 sizey=height(geometry);
	U32 bytes=4*sizex*sizey;
//	unsigned char* pixeldata=new unsigned char[bytes];

	GLuint fbo=0;
	glGenBuffers(1,&fbo);
	glBindBuffer(GL_PIXEL_PACK_BUFFER,fbo);
	glBufferData(GL_PIXEL_PACK_BUFFER,bytes,NULL,GL_STATIC_READ);
	glReadBuffer(GL_FRONT);
	glReadPixels(x,y,sizex,sizey,format,type,0);
	unsigned char* pixeldata=(unsigned char*)glMapBuffer(
			GL_PIXEL_PACK_BUFFER,GL_READ_ONLY);

//	pixeldata[m_index*3]=0;
//	pixeldata[m_index*3+1]=0;
//	pixeldata[m_index*3+2]=0;

	m_spImageI->resize(sizex,sizey,1);
	m_spImageI->replaceRegion(x,y,z,sizex,sizey,depth,pixeldata);
//	delete[] pixeldata;

	glBindBuffer(GL_PIXEL_PACK_BUFFER,0);
	glDeleteBuffers(1,&fbo);

	BWORD success=m_spImageI->save(filename);
	if(!success)
	{
		feLog("VideoRecorder::handle could not open output file \"%s\"\n",
				filename.c_str());
	}
	else
	{
		const Real time=m_asTemporal.time.check(record)?
				m_asTemporal.time(record): 0.0f;
		m_timeStamp[m_index++]=time;

		feLog("VideoRecorder::handle time %.6G\n",time);
	}
}

String VideoRecorder::findNameFor(Real scalar)
{
	U32 index=0;
	Real best=fabsf(m_timeStamp[0]-scalar);
	for(U32 m=0;m<m_ringSize;m++)
	{
		Real error=fabsf(m_timeStamp[m]-scalar);
		if(error<best)
		{
			index=m;
			best=error;
		}
	}

	String filename;
	filename.sPrintf("%s/video.%04d.tga",m_basename.c_str(),index);
	return filename;
}

} /* namespace ext */
} /* namespace fe */