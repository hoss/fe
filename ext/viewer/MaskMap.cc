/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <viewer/viewer.pmh>

namespace fe
{
namespace ext
{

MaskMap::MaskMap(void)
{
}

MaskMap::~MaskMap(void)
{
#if FE_CODEGEN==FE_DEBUG
	feLog("MaskMap:\n");
	for(std::map<String, WindowEvent::Mask>::iterator i_map = m_map.begin();
		i_map != m_map.end(); i_map++)
	{
		feLog("  %s\n", i_map->first.c_str());
	}
	feLog("\n");
#endif
}

bool MaskMap::get(WindowEvent::Mask &a_mask, const String &a_name)
{
	std::map<String, WindowEvent::Mask>::iterator i_mask =
		m_map.find(a_name);

	if(i_mask != m_map.end())
	{
		a_mask = i_mask->second;
		return true;
	}
	return false;
}

void MaskMap::set(const WindowEvent::Mask &a_mask, const String &a_name)
{
	m_map[a_name] = a_mask;
}

void MaskMap::all(std::vector< std::pair<String, WindowEvent::Mask> > &a_entries)
{
	for(std::map<String, WindowEvent::Mask>::iterator i_m = m_map.begin();
		i_m != m_map.end(); i_m++)
	{
		a_entries.push_back(
			std::pair<String,WindowEvent::Mask>(i_m->first,i_m->second));
	}
}

} /* namespace ext */
} /* namespace fe */

