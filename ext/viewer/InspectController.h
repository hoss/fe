/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __viewer_InspectController_h__
#define __viewer_InspectController_h__

#define MOUSEMODES	9

namespace fe
{
namespace ext
{

/**************************************************************************//**
    @brief Viewpoint controller for detailed inspection of a single object

	@ingroup viewer
*//***************************************************************************/
class FE_DL_EXPORT InspectController:
		virtual public CameraControllerI,
		virtual public HandlerI,
		virtual	public Reactor,
		public CastableAs<InspectController>
{
	public:
							InspectController(void);
virtual						~InspectController(void);

							//* as HandlerI
virtual	void				handle(Record &record);

							//* as CameraControllerI
virtual
const	SpatialTransform&	interestMatrix(void) const
							{	return m_interestMatrix;}
virtual
const	SpatialTransform&	auxillaryMatrix(void) const
							{	return m_auxillaryMatrix;}

virtual	BWORD				closeEvent(void) const
							{	return m_closeEvent; }

virtual	U32					keyCount(U32 key) const;
virtual	U32					setKeyCount(U32 key,U32 count);
virtual	void				setKeyMax(U32 key,U32 max);
virtual	Real				mouseDial(U32 mode) const;

virtual	SpatialVector		lookPoint(void) const;
virtual	Real				azimuth(void) const;
virtual	Real				downangle(void) const;
virtual	Real				distance(void) const;

virtual	void				setVertical(SpatialVector a_vertical);
virtual	void				setLookPoint(SpatialVector a_point);
virtual	void				setLookAngle(Real azimuth,Real downangle,
									Real distance);

virtual	void				useCustomCamera(sp<CameraI> a_spCamera);
virtual	sp<CameraI>			camera(void) const;

virtual	BWORD				rotationLock(void) const
							{	return m_rotationLock; }
virtual	void				setRotationLock(BWORD a_rotationLock)
							{	m_rotationLock=a_rotationLock; }

	private:
							using Reactor::react;

							//* Change interpreted state using events
		void				react(const WindowEvent &windowEvent);

							//* Update state while nothing is happening
		void				idleFunction(void);

							//* Change matrix using interpreted state
		void				updateMatrix(void);

							//* adjustment based on distance from object
		Real				zoomscalar(void);

		WindowEvent			m_event;
		U32					m_keyCount[WindowEvent::e_keyUsed];
		U32					m_keyMax[WindowEvent::e_keyUsed];
		I32					m_mouseState[MOUSEMODES];
		Real				m_fMouse[MOUSEMODES];
		Real				m_dfMouse[MOUSEMODES];
		Real				m_dfJoy[MOUSEMODES];
		Real				m_mouseScale[MOUSEMODES];
		Real				m_modelScale;
		BWORD				m_rotationLock;
		BWORD				m_closeEvent;
		BWORD				m_leftRight;
		BWORD				m_alted;
		BWORD				m_ctrled;
		BWORD				m_shifted;
		U32					m_indexX;
		U32					m_indexY;

		sp<CameraEditable>	m_spCameraCore;
		sp<CameraI>			m_spCameraCustom;
		SpatialTransform	m_cameraMatrix;
		SpatialTransform	m_interestMatrix;
		SpatialTransform	m_auxillaryMatrix;

		SpatialVector		m_vertical;

		PathAccessor<SpatialTransform>	m_paPerspMatrix;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __viewer_InspectController_h__ */
