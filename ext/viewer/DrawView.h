/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __viewer_DrawView_h__
#define __viewer_DrawView_h__

#include "signal/signal.h"
#include "draw/draw.h"
#include "window/window.h"

namespace fe
{
namespace ext
{

/** Helper class for draw operators (handlers)

	*/
class FE_DL_EXPORT DrawView
{
	public:
				DrawView(void);
virtual			~DrawView(void);

		bool				handle(	Record &record);

		sp<DrawI>			drawI(void) { return m_spDraw; }
		sp<WindowI>			windowI(void) { return m_spWindow; }
		sp<RecordGroup>		group(void) { return m_spRG; }
		AsSignal			&asSignal(void) { return m_asSignal; }
		AsWindata			&asWindata(void) { return m_asWindata; }

		const Color			&getColor(const String &a_name,
								const Color &a_default);
	private:
		AsSignal			m_asSignal;
		AsWindata			m_asWindata;
		AsColor				m_asColor;
		sp<DrawI>			m_spDraw;
		sp<WindowI>			m_spWindow;
		sp<RecordGroup>		m_spRG;
};
} /* namespace ext */
} /* namespace fe */

#endif /* __viewer_DrawView_h__ */

