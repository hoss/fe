/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __viewer_ViewerI_h__
#define __viewer_ViewerI_h__

namespace fe
{
namespace ext
{

//* TODO move token method implementations to a ViewerBase

/**************************************************************************//**
	@brief Simple viewer interface

	@ingroup viewer
*//***************************************************************************/
class FE_DL_EXPORT ViewerI:
	virtual public Component,
	public CastableAs<ViewerI>
{
	public:
						/// Adopts a WindowI
virtual void			bind(sp<WindowI> spWindowI)							{}

						/// Adopts a DrawI
virtual	void			bind(sp<DrawI> spDrawI)								{}

						/// Returns the current DrawI
virtual	sp<DrawI>		getDrawI(void) const
						{	return sp<DrawI>(NULL); }

						/// Returns the current CameraControllerI
virtual	sp<CameraControllerI>	getCameraControllerI(void) const
						{	return sp<CameraControllerI>(NULL); }

						/// Replaces the current CameraControllerI
virtual void			setCameraControllerI(
							sp<CameraControllerI> spCameraControllerI)		{}

						/// Returns frames per seconds
virtual Real			frameRate(void) const
						{	return 0.0; }

};

} /* namespace ext */
} /* namespace fe */

#endif /* __viewer_ViewerI_h__ */
