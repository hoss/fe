/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __grass_DodgeOp_h__
#define __grass_DodgeOp_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Operator to move curves away from a collider

	@ingroup grass
*//***************************************************************************/
class FE_DL_EXPORT DodgeOp:
	public OperatorThreaded,
	public Initialize<DodgeOp>,
	public ObjectSafe<DodgeOp>
{
	public:

								DodgeOp(void):
									m_frame(-1)								{}

virtual							~DodgeOp(void)								{}

		void					initialize(void);

								//* As HandlerI
virtual	void					handle(Record& a_rSignal);

								using OperatorThreaded::run;

virtual	void					run(I32 a_id,sp<SpannedRange> a_spRange);

	private:
		sp<SurfaceI::ImpactI>	hitCheck(SpatialVector a_origin,
									SpatialVector a_direction,
									Real a_distance,Real a_radius,
									BWORD a_anyHit,sp<DrawI> a_spDebug,
									BWORD a_guard,
									sp<SurfaceI::ImpactI> a_spLastImpact);

		sp<SurfaceAccessibleI>		m_spInputAccessible;
		sp<SurfaceI>				m_spCollider;
		sp<SurfaceI>				m_spSkin;
		sp<DrawMode>				m_spDebugGroup;
		sp<DrawI>					m_spDrawDebug;

		I32							m_frame;
		Real						m_repulsionScale;

		Array<U32>					m_passLimitCount;
		Array<SpatialVector>		m_pointCache;
		Array<SpatialVector>		m_lastCache;
		Array<I32>					m_changedCache;
		I32							m_currentHistory;

		sp<Profiler>				m_spProfiler;
		sp<Profiler::Profile>		m_spProfileAccess;
		sp<Profiler::Profile>		m_spProfilePrepare;
		sp<Profiler::Profile>		m_spProfileCache;
		sp<Profiler::Profile>		m_spProfileRun;
		sp<Profiler::Profile>		m_spProfileFlush;
		sp<Profiler::Profile>		m_spProfileNearest;
		sp<Profiler::Profile>		m_spProfileRepulsion;
		sp<Profiler::Profile>		m_spProfileFirstRay;
		sp<Profiler::Profile>		m_spProfileStepRay;
		sp<Profiler::Profile>		m_spProfileSubRay;
		sp<Profiler::Profile>		m_spProfileLength;
		sp<Profiler::Profile>		m_spProfileLead;
		sp<Profiler::Profile>		m_spProfileProjection;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __grass_DodgeOp_h__ */

