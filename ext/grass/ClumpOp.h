/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __grass_ClumpOp_h__
#define __grass_ClumpOp_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
    @brief pull input curves towards guide curves

	@ingroup grass
*//***************************************************************************/
class FE_DL_EXPORT ClumpOp:
	public OperatorSurfaceCommon,
	public Initialize<ClumpOp>
{
	public:
				ClumpOp(void)												{}
virtual			~ClumpOp(void)												{}

		void	initialize(void);

				//* As HandlerI
virtual void	handle(Record& a_rSignal);
};

} /* namespace ext */
} /* namespace fe */

#endif /* __grass_ClumpOp_h__ */
