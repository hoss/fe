/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __grass_WavyOp_h__
#define __grass_WavyOp_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
    @brief bend and twist output curves from input curves

	@ingroup grass
*//***************************************************************************/
class FE_DL_EXPORT WavyOp:
	public OperatorSurfaceCommon,
	public Initialize<WavyOp>
{
	public:
				WavyOp(void)												{}
virtual			~WavyOp(void)												{}

		void	initialize(void);

				//* As HandlerI
virtual void	handle(Record& a_rSignal);
};

} /* namespace ext */
} /* namespace fe */

#endif /* __grass_WavyOp_h__ */
