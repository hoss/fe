/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __grass_ScatterOp_h__
#define __grass_ScatterOp_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
    @brief create points on the input surface

	@ingroup grass
*//***************************************************************************/
class FE_DL_EXPORT ScatterOp:
	public MapOp,
	public Initialize<ScatterOp>
{
	public:
				ScatterOp(void)												{}
virtual			~ScatterOp(void)											{}

		void	initialize(void);

				//* As HandlerI
virtual void	handle(Record& a_rSignal);

	private:
		Noise	m_noise;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __grass_ScatterOp_h__ */
