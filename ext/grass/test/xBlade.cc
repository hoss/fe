/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <grass/grass.pmh>

using namespace fe;
using namespace fe::ext;

int main(void)
{
	UNIT_START();
	BWORD completed=FALSE;

	try
	{
		Blade blade;
		Array<SpatialVector>& rTangent=blade.tangent();
		Array<SpatialVector>& rLocationRef=blade.locationRef();
		Array<SpatialVector>& rFacing=blade.facing();
		Array<SpatialVector>& rForce=blade.forceIn();
		Array<Real>& rVelocity=blade.velocity();

		rTangent.resize(1);
		set(rTangent[0],1.0,0.0,0.0);

		rLocationRef.resize(5);
		for(U32 m=0;m<5;m++)
		{
			set(rLocationRef[m],0.0,m,0.0);
		}

		rFacing.resize(5);
		for(U32 m=0;m<5;m++)
		{
			set(rFacing[m],1.0,0.0,0.0);
		}

		rForce.resize(5);
		for(U32 m=0;m<5;m++)
		{
			set(rForce[m]);
		}

		rVelocity.resize(4);
		for(U32 m=0;m<4;m++)
		{
			rVelocity[m]=0.0;
		}

		blade.storeAsRest(0);

//		Array<SpatialVector>& rLocationDef=blade.locationDef();

		for(U32 n=0;n<1e6;n++)
		{
//			feLog("\n");

			blade.step(0);
#if FALSE
			for(U32 m=0;m<5;m++)
			{
				feLog("location %s  velocity %.6G\n",
						c_print(rLocationRef[m]),m? rVelocity[m-1]: 0.0);
			}

			if(true)
			{
				for(I32 m=3;m>=0;m--)
				{
					for(I32 n=3;n>=0;n--)
					{
						Real along=n/4.0;
						Real between=along*rLocationDef[m+1][0]+
								(1.0-along)*rLocationDef[m][0];
						feLog("%*s#\n",int((between+4.0)*10)," ");
					}
				}
			}
#else
//			if(!(I32(n)%10000))
//			{
//				feLog("%d\n",n);
//			}
#endif
		}

		completed=TRUE;
	}
	catch(Exception &e)	{ e.log(); }
	catch(...)			{ feLog("uncaught exception\n"); }

	UNIT_TEST(completed);
	UNIT_TRACK(1);
	UNIT_RETURN();
}
