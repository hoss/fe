/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <grass/grass.pmh>

using namespace fe;
using namespace fe::ext;

void TwistBindOp::initialize(void)
{
	catalog<String>("Attach Attribute")="twistAttach";
	catalog<String>("Attach Attribute","hint")=
			"Name of float attribute added to output surface"
			" describing the connection of each point along"
			" its associated curve.";

	catalog<String>("Method")="Dot";
	catalog<String>("Method","choice:0")="Dot";
	catalog<String>("Method","choice:1")="Ratio";
	catalog<String>("Method","hint")=
			"Attachment algorithm.";

	catalog< sp<Component> >("Input Surface");
	catalog<String>("Input Surface","label")="Reference Input Surface";

	catalog< sp<Component> >("Driver Surface");
	catalog<String>("Driver Surface","label")="Reference Driver Curves";
}

void TwistBindOp::handle(Record& a_rSignal)
{
//	feLog("TwistBindOp::handle\n");

	String attachName=catalog<String>("Attach Attribute");
	const BWORD byRatio=(catalog<String>("Method")=="Ratio");

	//* output surface is copy of input surface
	sp<SurfaceAccessibleI> spOutputAccessible;
	if(!accessOutput(spOutputAccessible,a_rSignal)) return;

	sp<SurfaceAccessorI> spOutputVertices;
	if(!access(spOutputVertices,spOutputAccessible,
			e_primitive,e_vertices)) return;

	sp<SurfaceAccessorI> spOutputAttach;
	if(!access(spOutputAttach,spOutputAccessible,e_point,attachName)) return;

	sp<SurfaceAccessorI> spRefVertices;
	if(!access(spRefVertices,"Driver Surface",e_primitive,e_vertices)) return;

	const U32 count=spOutputVertices->count();
	const U32 countRef=spRefVertices->count();
	const U32 countMin=count<countRef? count: countRef;
	for(U32 index=0;index<countMin;index++)
	{
		const U32 subCount=spOutputVertices->subCount(index);
		for(U32 subIndex=0;subIndex<subCount;subIndex++)
		{
			const U32 vertexIndex=spOutputVertices->integer(index,subIndex);
			const SpatialVector point=
					spOutputVertices->spatialVector(index,subIndex);

			const Real attach=byRatio?
					nearestPointAlongCurveByRatio(point,spRefVertices,index):
					nearestPointAlongCurve(point,spRefVertices,index);

			spOutputAttach->set(vertexIndex,attach);
		}
	}

//	feLog("TwistBindOp::handle done\n");
}

Real TwistBindOp::nearestPointAlongCurveByRatio(const SpatialVector a_point,
	const sp<SurfaceAccessorI> a_spCurves,const I32 a_index)
{
	I32 best=0;
	Real distPre2= -1.0;
	Real distBest2= -1.0;
	Real distPost2= -1.0;
	Real distLast2= -1.0;

	const I32 subCount=a_spCurves->subCount(a_index);
	for(I32 subIndex=0;subIndex<subCount;subIndex++)
	{
		const SpatialVector cv=a_spCurves->spatialVector(a_index,subIndex);
		const SpatialVector diff=cv-a_point;
		const Real dist2=magnitudeSquared(diff);
		if(!subIndex || dist2<distBest2)
		{
			best=subIndex;
			distPre2=distLast2;
			distBest2=dist2;
			distPost2= -1.0;
		}
		else if(subIndex==best+1)
		{
			distPost2=dist2;
		}

		distLast2=dist2;
	}

	Real nearest=best;
	if(best==0 || (distPost2>=0.0 && distPost2<distPre2))
	{
		if(distPost2>=0.0)
		{
			const Real distBest=sqrt(distBest2);
			const Real distPost=sqrt(distPost2);
			nearest+=distBest/(distBest+distPost);
		}
	}
	else if(distPre2>=0.0)
	{
		const Real distBest=sqrt(distBest2);
		const Real distPre=sqrt(distPre2);
		nearest-=distBest/(distBest+distPre);
	}

	return nearest;
}
