/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <grass/grass.pmh>

namespace fe
{
namespace ext
{

void KinkOp::initialize(void)
{
	catalog<String>("icon")="FE_alpha";

	//* TODO noise seed, amplitude random, frequency random

	catalog<Real>("amplitudeRoot")=0.0;
	catalog<Real>("amplitudeRoot","high")=1.0;
	catalog<Real>("amplitudeRoot","max")=1e6;
    catalog<String>("amplitudeRoot","label")="Amplitude Root";
    catalog<bool>("amplitudeRoot","joined")=true;
    catalog<String>("amplitudeRoot","hint")=
            "Peak of positional change at the start of each curve.";

	catalog<Real>("amplitudeTip")=0.1;
	catalog<Real>("amplitudeTip","high")=1.0;
	catalog<Real>("amplitudeTip","max")=1e6;
    catalog<String>("amplitudeTip","label")="Tip";
    catalog<bool>("amplitudeTip","joined")=true;
    catalog<String>("amplitudeTip","hint")=
            "Peak of positional change at the end of each curve.";

	catalog<Real>("amplitudeGamma")=1.0;
	catalog<Real>("amplitudeGamma","high")=2.0;
	catalog<Real>("amplitudeGamma","max")=1e3;
    catalog<String>("amplitudeGamma","label")="Gamma";
    catalog<bool>("amplitudeGamma","joined")=true;
    catalog<String>("amplitudeGamma","hint")=
            "Adjust how much influence the Amplitude Root has on"
			" the overall amplitude distribution along the curve.";

	catalog<Real>("amplitudeRandom")=0.0;
	catalog<Real>("amplitudeRandom","high")=1.0;
	catalog<Real>("amplitudeRandom","max")=1e3;
    catalog<String>("amplitudeRandom","label")="Random";
    catalog<String>("amplitudeRandom","hint")=
            "Variation of positional change at the end of each curve.";

	catalog<SpatialVector>("amplitudeScale")=SpatialVector(0,1,1);
    catalog<String>("amplitudeScale","label")="Amplitude Scale";
    catalog<String>("amplitudeScale","hint")=
            "Tweak amplitude on axes of curve tangent,"
            " facing direction, and sideways to facing direction.";

	catalog<Real>("frequencyRoot")=100.0;
	catalog<Real>("frequencyRoot","high")=1e4;
	catalog<Real>("frequencyRoot","max")=1e9;
    catalog<String>("frequencyRoot","label")="Frequency Root";
    catalog<bool>("frequencyRoot","joined")=true;
    catalog<String>("frequencyRoot","hint")=
            "Volatility of positional change at the start of each curve.";

	catalog<Real>("frequencyTip")=100.0;
	catalog<Real>("frequencyTip","high")=1e4;
	catalog<Real>("frequencyTip","max")=1e9;
    catalog<String>("frequencyTip","label")="Tip";
    catalog<bool>("frequencyTip","joined")=true;
    catalog<String>("frequencyTip","hint")=
            "Volatility of positional change at the end of each curve.";

	catalog<Real>("frequencyGamma")=1.0;
	catalog<Real>("frequencyGamma","high")=2.0;
	catalog<Real>("frequencyGamma","max")=1e3;
    catalog<String>("frequencyGamma","label")="Gamma";
    catalog<String>("frequencyGamma","hint")=
            "Adjust how much influence the Frequency Root has on"
			" the overall frequency distribution along the curve.";

	catalog<SpatialVector>("frequencyScale")=SpatialVector(1,1,1);
    catalog<String>("frequencyScale","label")="Frequency Scale";
    catalog<String>("frequencyScale","hint")=
            "Tweak frequency on axes of curve tangent,"
            " facing direction, and sideways to facing direction.";

    catalog<String>("facingAttr")="facing";
    catalog<String>("facingAttr","label")="Facing Attribute";
    catalog<String>("facingAttr","hint")=
            "Vector attribute on input curve primitives"
            " describing a direction roughly perpendicular to the curve.";

	catalog< sp<Component> >("Input Curves");

	catalog< sp<Component> >("Output Curves");
	catalog<String>("Output Curves","IO")="output";
	catalog<String>("Output Curves","copy")="Input Curves";
	catalog<bool>("Output Curves","clobber")=true;
}

void KinkOp::handle(Record& a_rSignal)
{
	const Real amplitudeRoot=catalog<Real>("amplitudeRoot");
	const Real amplitudeTip=catalog<Real>("amplitudeTip");
	const Real amplitudeGamma=catalog<Real>("amplitudeGamma");
	const Real amplitudeRandom=catalog<Real>("amplitudeRandom");
	const SpatialVector amplitudeScale=
			catalog<SpatialVector>("amplitudeScale");

	const Real frequencyRoot=catalog<Real>("frequencyRoot");
	const Real frequencyTip=catalog<Real>("frequencyTip");
	const Real frequencyGamma=catalog<Real>("frequencyGamma");
	const SpatialVector frequencyScale=
			catalog<SpatialVector>("frequencyScale");

    const String facingAttr=catalog<String>("facingAttr");

//	sp<SurfaceAccessibleI> spInputAccessible;
//	if(!access(spInputAccessible,"Input Curves")) return;

	sp<SurfaceAccessibleI> spOutputAccessible;
	if(!accessOutput(spOutputAccessible,a_rSignal)) return;

//	spOutputAccessible->copy(spInputAccessible);

	sp<SurfaceAccessorI> spOutputVertices;
	if(!access(spOutputVertices,spOutputAccessible,
			e_primitive,e_vertices)) return;

    sp<SurfaceAccessorI> spPrimitiveFacing;
    access(spPrimitiveFacing,"Input Curves",
			e_primitive,facingAttr,e_warning);

	I32 curveCount=0;

	const I32 primitiveCount=spOutputVertices->count();
	for(I32 primitiveIndex=0;primitiveIndex<primitiveCount;primitiveIndex++)
	{
		const I32 subCount=spOutputVertices->subCount(primitiveIndex);
		if(subCount<2)
		{
			continue;
		}

		Real amplitudeStart=amplitudeRoot;
		if(amplitudeRandom>Real(0))
		{
			amplitudeStart+=amplitudeRandom*m_noise.whiteNoise2d(
					Vector2f(13*primitiveIndex,0.0));
		}
		const Real amplitudeAdd=amplitudeTip-amplitudeRoot;

		SpatialVector facing(0,1,0);
		if(spPrimitiveFacing.isValid())
		{
			facing=spPrimitiveFacing->spatialVector(primitiveIndex);
		}

        const SpatialVector rootPoint=
                spOutputVertices->spatialVector(primitiveIndex,0);
		SpatialVector previousPoint=rootPoint;

        const Real disruption=1e6;	//* TODO param

        const Real perlinRoot=disruption*m_noise.perlin3d(rootPoint);

		const Real perlinScalar=sqrt(2.0);

        for(I32 subIndex=1;subIndex<subCount;subIndex++)
		{
            const Real along=subIndex/Real(subCount-1);

            const Real amplitudeAlong=amplitudeStart+
					amplitudeAdd*pow(along,amplitudeGamma);

			const Real rescale=perlinScalar*amplitudeAlong;

            const Real frequencyAlong=frequencyRoot+
					(frequencyTip-frequencyRoot)*pow(along,frequencyGamma);

			//* TODO shouldn't this be cumulative?
            const Real offsetAlong=along*frequencyAlong;

            SpatialVector perlinOutput;

            Vector2d perlinInput;
            perlinInput[0]=perlinRoot;

            perlinInput[1]=offsetAlong*frequencyScale[0];
            perlinOutput[0]=m_noise.perlin2d(perlinInput)*
					rescale*amplitudeScale[0];

            perlinInput[1]=offsetAlong*frequencyScale[1]+1e3;
            perlinOutput[1]=m_noise.perlin2d(perlinInput)*
					rescale*amplitudeScale[1];

            perlinInput[1]=offsetAlong*frequencyScale[2]+2e3;
            perlinOutput[2]=m_noise.perlin2d(perlinInput)*
					rescale*amplitudeScale[2];

//			if(primitiveIndex==0)
//			{
//				feLog("%d/%d along %.6G amp %.6G freq %.6G off %.6G\n",
//						subIndex,subCount,along,amplitudeAlong,
//						frequencyAlong,offsetAlong);
//				feLog("  in %s out %s\n",
//						c_print(perlinInput),c_print(perlinOutput));
//			}

            SpatialVector currentPoint=
                    spOutputVertices->spatialVector(primitiveIndex,subIndex);
            const SpatialVector tangent=unitSafe(currentPoint-previousPoint);
			previousPoint=currentPoint;

			//* tangentX, facingY (curve normal)

            SpatialTransform xformSelf;
            makeFrameNormalY(xformSelf,currentPoint,tangent,facing);

			SpatialVector delta;
            rotateVector(xformSelf,perlinOutput,delta);

			currentPoint+=delta;

            spOutputVertices->set(primitiveIndex,subIndex,currentPoint);
		}

		curveCount++;
	}

	String summary;
	summary.sPrintf("%d curve%s\n",curveCount,curveCount==1? "": "s");
	catalog<String>("summary")=summary;
}

} /* namespace ext */
} /* namespace fe */
