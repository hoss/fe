/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <grass/grass.pmh>

namespace fe
{
namespace ext
{

void ClumpOp::initialize(void)
{
	catalog<String>("icon")="FE_alpha";

	catalog<Real>("convergenceRoot")=0.0;
	catalog<Real>("convergenceRoot","max")=1.0;
    catalog<String>("convergenceRoot","label")="Convergence Root";
    catalog<bool>("convergenceRoot","joined")=true;
    catalog<String>("convergenceRoot","hint")=
            "Peak of positional change at the start of each curve.";

	catalog<Real>("convergenceTip")=1.0;
	catalog<Real>("convergenceTip","max")=1.0;
    catalog<String>("convergenceTip","label")="Tip";
    catalog<bool>("convergenceTip","joined")=true;
    catalog<String>("convergenceTip","hint")=
            "Peak of positional change at the end of each curve.";

	catalog<Real>("convergenceGamma")=1.0;
	catalog<Real>("convergenceGamma","high")=2.0;
	catalog<Real>("convergenceGamma","max")=1e3;
    catalog<String>("convergenceGamma","label")="Gamma";
    catalog<String>("convergenceGamma","hint")=
            "Adjust how much influence the Convergence Root has on"
			" the overall convergence distribution along the curve.";

	catalog< sp<Component> >("Input Curves");

	catalog< sp<Component> >("Guide Curves");

	catalog< sp<Component> >("Output Curves");
	catalog<String>("Output Curves","IO")="output";
	catalog<String>("Output Curves","copy")="Input Curves";
	catalog<bool>("Output Curves","clobber")=true;
}

void ClumpOp::handle(Record& a_rSignal)
{
	const Real convergenceRoot=catalog<Real>("convergenceRoot");
	const Real convergenceTip=catalog<Real>("convergenceTip");
	const Real convergenceGamma=catalog<Real>("convergenceGamma");

//	sp<SurfaceAccessibleI> spInputAccessible;
//	if(!access(spInputAccessible,"Input Curves")) return;

	sp<SurfaceAccessibleI> spGuideAccessible;
	if(!access(spGuideAccessible,"Guide Curves")) return;

	sp<SurfaceAccessibleI> spOutputAccessible;
	if(!accessOutput(spOutputAccessible,a_rSignal)) return;

	sp<SurfaceI> spGuides;
	if(!access(spGuides,spGuideAccessible)) return;

//	spOutputAccessible->copy(spInputAccessible);

	sp<SurfaceAccessorI> spGuideVertices;
	if(!access(spGuideVertices,spGuideAccessible,
			e_primitive,e_vertices)) return;

	sp<SurfaceAccessorI> spOutputVertices;
	if(!access(spOutputVertices,spOutputAccessible,
			e_primitive,e_vertices)) return;

	const I32 guideCount=spGuideVertices->count();
	I32 curveCount=0;

	const I32 primitiveCount=spOutputVertices->count();
	for(I32 primitiveIndex=0;primitiveIndex<primitiveCount;primitiveIndex++)
	{
		const I32 subCount=spOutputVertices->subCount(primitiveIndex);
		if(subCount<2)
		{
			continue;
		}

		const SpatialVector rootPoint=
				spOutputVertices->spatialVector(primitiveIndex,0);

		const Real maxDistance(1.0);	//* TODO
		sp<SurfaceI::ImpactI> spRootImpact=
				spGuides->nearestPoint(rootPoint,maxDistance);
		if(spRootImpact.isNull())
		{
			continue;
		}

		const I32 primitiveIndexGuide=spRootImpact->primitiveIndex();

//		feLog("clump %d/%d to %d\n",
//				primitiveIndex,primitiveCount,primitiveIndexGuide);

		for(I32 subIndex=0;subIndex<subCount;subIndex++)
		{
//			const Real clumping=(subIndex+1)/Real(subCount);

            const Real along=subIndex/Real(subCount-1);
            const Real convergenceAlong=convergenceRoot+
					(convergenceTip-convergenceRoot)*
					pow(along,convergenceGamma);

			const SpatialVector inputPoint=spOutputVertices->spatialVector(
					primitiveIndex,subIndex);
			const SpatialVector clumpPoint=spGuideVertices->spatialVector(
					primitiveIndexGuide,subIndex);

			const SpatialVector clumpedPoint=
					inputPoint*(Real(1)-convergenceAlong)+
					clumpPoint*convergenceAlong;

			spOutputVertices->set(primitiveIndex,subIndex,clumpedPoint);
		}

		curveCount++;
	}

	String summary;
	summary.sPrintf("%d curve%s %d guide%s\n",
			curveCount,curveCount==1? "": "s",
			guideCount,guideCount==1? "": "s");
	catalog<String>("summary")=summary;
}

} /* namespace ext */
} /* namespace fe */
