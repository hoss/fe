/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __grass_h__
#define __grass_h__

#include "datatool/datatool.h"
#include "operator/operator.h"
#include "geometry/geometry.h"

#ifdef MODULE_grass
#define FE_GRASS_PORT FE_DL_EXPORT
#else
#define FE_GRASS_PORT FE_DL_IMPORT
#endif

#endif // __grass_h__

