/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __grass_BenderOp_h__
#define __grass_BenderOp_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Operator to bend primitives

	@ingroup grass
*//***************************************************************************/
class FE_DL_EXPORT BenderOp:
	public OperatorState,
	public Initialize<BenderOp>
{
	public:

						BenderOp(void):
							m_synced(FALSE),
							m_brushed(FALSE),
							m_lastFrame(0.0),
							m_keyframe(0.0),
							m_isolate(FALSE),
							m_isolating(-1),
							m_dragging(FALSE),
							m_transient(FALSE),
							m_pickId(-1)									{}

virtual					~BenderOp(void)										{}

		void			initialize(void);

						//* As HandlerI
virtual	void			handle(Record& a_rSignal);

	protected:

virtual	void			setupState(void);
virtual	BWORD			loadState(const String& rBuffer);

virtual	BWORD			undo(String a_label,sp<Counted> a_spCounted);
virtual	BWORD			redo(String a_label,sp<Counted> a_spCounted);

	private:
		void			syncIds(void);
		void			drawOverlayKey(sp<DrawI>& r_spDrawI,I32 a_px,I32 a_py,
								Real a_frame,const Color& a_rColor);
		void			initParam(String a_prefix,String a_label,
								BWORD a_joined);
		Record			removeChannel(I32 a_channelIndex);
		Record			removeChannel(Record a_channel);
		I32				keyCount(I32 a_channelIndex);
		I32				keyCount(Record a_channel);
		Record			indexedKey(I32 a_channelIndex,I32 a_keyIndex);
		Record			indexedKey(Record a_channel,I32 a_keyIndex);
		Record			keyAt(I32 a_channelIndex,Real a_frame);
		Record			keyAt(Record a_channel,Real a_frame);
		Record			removeKey(I32 a_channelIndex,Real a_frame);
		Record			removeKey(Record a_channel,Real a_frame);
		void			findKeys(I32 a_channelIndex,Real a_frame,
								Record& a_rBefore,Record& a_rAfter);
		void			findKeys(Record a_channel,Real a_frame,
								Record& a_rBefore,Record& a_rAfter);
		void			findKeys(I32 a_channelIndex,Real a_frame,
								Record& a_rBefore,Record& a_rAfter,
								Record& a_rTwoBefore,Record& a_rTwoAfter);
		void			findKeys(Record a_channel,Real a_frame,
								Record& a_rBefore,Record& a_rAfter,
								Record& a_rTwoBefore,Record& a_rTwoAfter);
		Record			evalAt(I32 a_channelIndex,Real a_frame);
		Record			evalAt(Record a_channel,Real a_frame);

		BWORD			calculateExtent(BWORD a_input,
								U32 a_primIndex,
								SpatialTransform& a_rParent,
								SpatialTransform& a_rRoot,
								SpatialTransform& a_rEnd);

		void			calculateTransforms(Record key,
								const SpatialTransform& a_rRoot,
								const SpatialTransform& a_rEnd,
								SpatialTransform& a_rBase,
								SpatialTransform& a_rDiff);

		void			interpolate(SpatialTransform& a_rPartial,
								const SpatialTransform& a_rBase,
								const SpatialTransform& a_rDiff,Real a_power);

		MatrixPower<SpatialTransform> m_matrixPower;

		sp<SurfaceI>				m_spDriver;
		sp<SurfaceI>				m_spOutput;
		sp<SurfaceAccessorI>		m_spInputPoint;
		sp<SurfaceAccessorI>		m_spOutputPoint;
		sp<SurfaceAccessorI>		m_spInputVertices;
		sp<SurfaceAccessorI>		m_spOutputVertices;
		sp<SurfaceAccessorI>		m_spInputFragment;
		sp<SurfaceAccessorI>		m_spChangeGroup;
		WindowEvent					m_event;

		sp<DrawMode>				m_spWireframe;
		sp<DrawMode>				m_spSolid;
		sp<DrawMode>				m_spChart;

		std::map<String,I32>		m_indexOfFragment;
		BWORD						m_fragmented;
		Real						m_linearity;
		BWORD						m_fastPower;
		BWORD						m_synced;
		BWORD						m_brushed;

		Real						m_frame;
		Real						m_startFrame;
		Real						m_endFrame;
		Real						m_lastFrame;
		Real						m_keyframe;
		sp< RecordMap<I32> >		m_spChannelMap;
		Record						m_key;
		BWORD						m_isolate;
		I32							m_isolating;
		BWORD						m_dragging;
		BWORD						m_transient;
		I32							m_pickId;
		I32							m_lastX;
		I32							m_lastY;
		String						m_changing;

		Accessor<I32>				m_aIndex;
		Accessor<String>			m_aName;
		Accessor< sp<RecordArray> >	m_aKeys;
		Accessor<Real>				m_aFrame;
		Accessor<Real>				m_aLeanX;
		Accessor<Real>				m_aLeanZ;
		Accessor<Real>				m_aArcX;
		Accessor<Real>				m_aArcZ;
		Accessor<Real>				m_aSpinY;
		Accessor<Real>				m_aTwistY;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __grass_BenderOp_h__ */
