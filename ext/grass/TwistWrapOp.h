/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __grass_TwistWrapOp_h__
#define __grass_TwistWrapOp_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Handler to wrap and twist geometry to follow bends in a curve

	@ingroup grass
*//***************************************************************************/
class FE_DL_EXPORT TwistWrapOp:
	public OperatorThreaded,
	public Initialize<TwistWrapOp>,
	public ObjectSafe<TwistWrapOp>
{
	public:

					TwistWrapOp(void)										{}
virtual				~TwistWrapOp(void)										{}

		void		initialize(void);

					//* As HandlerI
virtual	void		handle(Record& a_rSignal);

					using OperatorThreaded::run;

virtual	void		run(I32 a_id,sp<SpannedRange> a_spRange);

	private:

		sp<SurfaceAccessibleI>		m_spInputAccessible;
		sp<SurfaceAccessibleI>		m_spOutputAccessible;
		sp<SurfaceAccessibleI>		m_spDriverCurves;
		sp<SurfaceAccessibleI>		m_spDeformedCurves;
		String						m_facingName;
		String						m_attachName;
		BWORD						m_badFacing;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __grass_TwistWrapOp_h__ */
