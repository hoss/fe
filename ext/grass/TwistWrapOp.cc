/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <grass/grass.pmh>

#define FE_OTW_DEBUG	FALSE

using namespace fe;
using namespace fe::ext;

void TwistWrapOp::initialize(void)
{
	catalog<String>("Attach Attribute")="twistAttach";
	catalog<String>("Attach Attribute","hint")=
			"Name of float attribute on the input surface"
			" describing the connection of each point along"
			" its associated curve.";

	catalog<String>("Facing Attribute")="N";
	catalog<String>("Facing Attribute","hint")=
			"Name of vector attribute on input curves"
			" describing the twist orientation,"
			" preferably a normal presenting the widest exposure.";

	catalog<Real>("Facing Twist")=1.0;
	catalog<Real>("Facing Twist","max")=10.0;
	catalog<String>("Facing Twist","hint")=
			"Scaling of effect that the alignment of the 'facing'"
			" with the deflection causes twisting."
			"  A value of 1.0 represents a notion of \"full\" effect.";

	catalog< sp<Component> >("Input Surface");
	catalog<String>("Input Surface","label")="Bound Input Surface";

	catalog< sp<Component> >("Driver Surface");
	catalog<String>("Driver Surface","label")="Reference Driver Curves";

	catalog< sp<Component> >("Deformed Surface");
	catalog<String>("Deformed Surface","label")="Deformed Driver Curves";

	catalog< sp<Component> >("Output Surface");
	catalog<String>("Output Surface","IO")="output";
	catalog<String>("Output Surface","copy")="Input Surface";
	catalog<bool>("Output Surface","recycle")=true;
}

void TwistWrapOp::handle(Record& a_rSignal)
{
#if FE_OTW_DEBUG
	feLog("TwistWrapOp::handle\n");
#endif

	m_facingName=catalog<String>("Facing Attribute");
	m_attachName=catalog<String>("Attach Attribute");

	//* output curves is copy of input surface
	if(!accessOutput(m_spOutputAccessible,a_rSignal)) return;

	if(!access(m_spInputAccessible,"Input Surface")) return;

	if(!access(m_spDriverCurves,"Driver Surface")) return;

	if(!access(m_spDeformedCurves,"Deformed Surface")) return;

	sp<SurfaceAccessorI> spOutputVertices;
	if(!access(spOutputVertices,m_spOutputAccessible,
			e_primitive,e_vertices)) return;

	sp<SurfaceAccessorI> spDriverVertices;
	if(!access(spDriverVertices,m_spDriverCurves,
			e_primitive,e_vertices)) return;

	sp<SurfaceAccessorI> spDeformedVertices;
	if(!access(spDeformedVertices,m_spDeformedCurves,
			e_primitive,e_vertices)) return;

	const U32 primitiveCount = spOutputVertices->count();
	const U32 primitiveCountRef = spDriverVertices->count();
	const U32 primitiveCountDef = spDeformedVertices->count();
	U32 primitiveCountMin = primitiveCountRef<primitiveCountDef?
			primitiveCountRef: primitiveCountDef;
	primitiveCountMin = primitiveCountMin<primitiveCount?
			primitiveCountMin: primitiveCount;

	m_badFacing=FALSE;

	setAtomicRange(m_spOutputAccessible,e_pointsOfPrimitives);
	OperatorThreaded::handle(a_rSignal);

	if(m_badFacing)
	{
		catalog<String>("warning")+="null facing vector(s) detected;";
	}

#if FE_OTW_DEBUG
	feLog("TwistWrapOp::handle done\n");
#endif
}

void TwistWrapOp::run(I32 a_id,sp<SpannedRange> a_spRange)
{
	Real facingTwist=catalog<Real>("Facing Twist");

#if FE_OTW_DEBUG
	feLog("TwistWrapOp::run running thread %d %s\n",a_id,c_print(a_spRange));
#endif

	sp<SurfaceAccessorI> spInputVertices;
	if(!access(spInputVertices,m_spInputAccessible,
			e_primitive,e_vertices)) return;

	sp<SurfaceAccessorI> spOutputVertices;
	if(!access(spOutputVertices,m_spOutputAccessible,
			e_primitive,e_vertices)) return;

	sp<SurfaceAccessorI> spOutputAttach;
	if(!access(spOutputAttach,m_spOutputAccessible,e_point,m_attachName) ||
			!spOutputAttach->count()) return;

	sp<SurfaceAccessorI> spDriverVertices;
	if(!access(spDriverVertices,m_spDriverCurves,e_primitive,e_vertices) ||
			!spDriverVertices->count()) return;

	sp<SurfaceAccessorI> spDeformedVertices;
	if(!access(spDeformedVertices,m_spDeformedCurves,e_primitive,e_vertices) ||
			!spDeformedVertices->count()) return;

	sp<SurfaceAccessorI> spDriverFacing;
	if(!access(spDriverFacing,m_spDriverCurves,e_point,m_facingName) ||
			!spDriverFacing->count()) return;

	sp<SurfaceAccessorI> spDeformedFacing;
	if(!access(spDeformedFacing,m_spDeformedCurves,e_point,m_facingName) ||
			!spDeformedFacing->count()) return;

	I32 maxCount=0;
	for(SpannedRange::Iterator it=a_spRange->begin();!it.atEnd();it.step())
	{
		const U32 primitiveIndex=it.value();
		const I32 subCountRef = spDriverVertices->subCount(primitiveIndex);
		if(maxCount<subCountRef)
		{
			maxCount=subCountRef;
		}
	}

	SpatialTransform* transformArray=new SpatialTransform[maxCount];

	for(SpannedRange::Iterator it=a_spRange->begin();!it.atEnd();it.step())
	{
		const I32 index=it.value();
		const I32 subCountRef = spDriverVertices->subCount(index);
		const I32 subCountDef = spDeformedVertices->subCount(index);
		if(!subCountRef || subCountRef!=subCountDef)
		{
			feLog("TwistWrapOp::handle"
					" primitive %d mismatched subCount: %d ref %d def\n",
					index,subCountRef,subCountDef);
			continue;
		}

		SpatialVector pointRef(0.0,0.0,0.0);
		SpatialVector pointDef(0.0,0.0,0.0);
		SpatialVector pointRefPost=spDriverVertices->spatialVector(index,0);
		SpatialVector pointDefPost=spDeformedVertices->spatialVector(index,0);
		for(I32 subIndex=0;subIndex<subCountRef;subIndex++)
		{
			const I32 vertexIndexRef=spDriverVertices->integer(index,subIndex);
			const I32 vertexIndexDef=
					spDeformedVertices->integer(index,subIndex);

			const SpatialVector pointRefPre=pointRef;
			const SpatialVector pointDefPre=pointDef;
			pointRef=pointRefPost;
			pointDef=pointDefPost;

			if(subIndex<subCountRef-1)
			{
				pointRefPost=spDriverVertices->spatialVector(index,subIndex+1);
				pointDefPost=
					spDeformedVertices->spatialVector(index,subIndex+1);
			}

			const SpatialVector facingRef=
					spDriverFacing->spatialVector(vertexIndexRef);
			SpatialVector facingDef=
					spDeformedFacing->spatialVector(vertexIndexDef);

			SpatialVector dirRef;
			SpatialVector dirDef;
			if(subIndex==0)
			{
				dirRef=unit(pointRefPost-pointRef);
				dirDef=unit(pointDefPost-pointDef);
			}
			else if(subIndex==subCountRef-1)
			{
				dirRef=unit(pointRef-pointRefPre);
				dirDef=unit(pointDef-pointDefPre);
			}
			else
			{
				dirRef=unit(pointRefPost-pointRefPre);
				dirDef=unit(pointDefPost-pointDefPre);
			}

			//* HACK move facing in direction of change
			SpatialVector change=pointDef-pointRef;
			facingDef+=facingTwist*change*SpatialVector(1.0,0.0,1.0);
			facingDef[1]+=facingTwist*(fabs(change[0])+fabs(change[2]));

			const Real length=magnitude(facingDef);
			if(length>0.0 && magnitudeSquared(facingRef)>0.0)
			{
				facingDef*=1.0/length;

				SpatialTransform frameRef;
#if FE_CODEGEN<=FE_DEBUG
				BWORD success=
#endif
						makeFrame(frameRef,pointRef,dirRef,facingRef);
				FEASSERT(success);

				SpatialTransform frameDef;
#if FE_CODEGEN<=FE_DEBUG
				success=
#endif
						makeFrame(frameDef,pointDef,dirDef,facingDef);
				FEASSERT(success);

				SpatialTransform frameRefInv;
				invert(frameRefInv,frameRef);

				transformArray[subIndex]=frameRefInv*frameDef;
#if FALSE
				if(!subIndex)
				{
					feLog("\nprimitive %d\n",index);
				}
				feLog("transform %d\n%s\n",
						subIndex,c_print(transformArray[subIndex]));
				feLog("pointRef  %s\n",c_print(pointRef));
				feLog("dirRef    %s\n",c_print(dirRef));
				feLog("facingRef %s\n",c_print(facingRef));
				feLog("frameRef\n%s\n",c_print(frameRef));
				feLog("pointDef  %s\n",c_print(pointDef));
				feLog("dirDef    %s\n",c_print(dirDef));
				feLog("facingDef %s\n",c_print(facingDef));
				feLog("frameDef\n%s\n",c_print(frameDef));
#endif
			}
			else
			{
				setIdentity(transformArray[subIndex]);

				SAFEGUARD;
				m_badFacing=TRUE;
			}
		}

		const I32 subCount=spOutputVertices->subCount(index);
		for(I32 subIndex=0;subIndex<subCount;subIndex++)
		{
			const I32 vertexIndex=spOutputVertices->integer(index,subIndex);
			const Real attach=spOutputAttach->real(vertexIndex);

			const SpatialVector point=
					spInputVertices->spatialVector(index,subIndex);

			I32 first=I32(attach);
			if(first<0)
			{
				first=0;
			}
			if(first>subCountRef-2)
			{
				first=subCountRef-2;
			}
			const Real between=attach-Real(first);

			const SpatialVector point0=
					transformVector(transformArray[first],point);
			if(between<1e-6)
			{
				spOutputVertices->set(index,subIndex,point0);
				continue;
			}

			const SpatialVector point1=
					transformVector(transformArray[first+1],point);
			const SpatialVector blend=(1.0-between)*point0+between*point1;

			spOutputVertices->set(index,subIndex,blend);
		}
	}

	delete[] transformArray;

#if FE_OTW_DEBUG
	feLog("TwistWrapOp::run done running thread %d\n",a_id);
#endif
}
