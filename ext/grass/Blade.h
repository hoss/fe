/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __grass_Blade__
#define __grass_Blade__

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief minimal semi-implicit curve sim

	@ingroup grass
*//***************************************************************************/
class FE_DL_EXPORT Blade
{
	public:
		typedef enum
		{
			e_boundingSphere=0x00,
			e_nearestPoint=0x01,
			e_rayCastOneStep=0x02,
			e_rayCastHalvsies=0x03
		} CollisionMethod;

						Blade(void);
virtual					~Blade(void)										{}

		void			setTime(Real a_t)			{ m_t=a_t; }
		Real			time(void) const			{ return m_t; }

		void			setTimeStep(Real a_h)		{ m_h=a_h; }
		Real			timeStep(void) const		{ return m_h; }

		void			setTension(Real a_tension)		{ m_tension=a_tension; }
		Real			tension(void) const			{ return m_tension; }

		void			setTensionRandom(Real a_tensionRandom)
						{	m_tensionRandom=a_tensionRandom; }
		Real			tensionRandom(void) const
						{	return m_tensionRandom; }

		void			setRestoration(Real a_restoration)
						{	m_restoration=a_restoration; }
		Real			restoration(void) const
						{	return m_restoration; }

		void			setRestorationRandom(Real a_restorationRandom)
						{	m_restorationRandom=a_restorationRandom; }
		Real			restorationRandom(void) const
						{	return m_restorationRandom; }

		void			setDrag(Real a_drag)			{ m_drag=a_drag; }
		Real			drag(void) const				{ return m_drag; }

		void			setDragRandom(Real a_dragRandom)
						{	m_dragRandom=a_dragRandom; }
		Real			dragRandom(void) const
						{	return m_dragRandom; }

		void			setBend(Real a_bend)			{ m_bend=a_bend; }
		Real			bend(void) const				{ return m_bend; }

		void			setGrabbing(BWORD a_grabbing)
						{	m_grabbing=a_grabbing; }
		BWORD			grabbing(void) const
						{	return m_grabbing; }

		void			setGrabBias(Real a_grabBias){ m_grabBias=a_grabBias; }
		Real			grabBias(void) const		{ return m_grabBias; }

		void			setGrabBiasRandom(Real a_grabBiasRandom)
						{	m_grabBiasRandom=a_grabBiasRandom; }
		Real			grabBiasRandom(void) const
						{	return m_grabBiasRandom; }

		void			setCollisionGap(Real a_collisionGap)
						{	m_collisionGap=a_collisionGap; }
		Real			collisionGap(void) const
						{	return m_collisionGap; }

		void			setMass(Real a_mass)		{ m_mass=a_mass; }
		Real			mass(void) const			{ return m_mass; }

		void			setWind(Real a_wind)
						{	m_wind=a_wind; }
		Real			wind(void) const
						{	return m_wind; }

		void			setTurbulence(Real a_turbulence)
						{	m_turbulence=a_turbulence; }
		Real			turbulence(void) const
						{	return m_turbulence; }

		void			setBuckling(BWORD a_buckling)
						{	m_buckling=a_buckling; }
		BWORD			buckling(void) const
						{	return m_buckling; }

		void			setBreakAngle(Real a_breakAngle)
						{	m_breakAngle=a_breakAngle; }
		Real			breakAngle(void) const
						{	return m_breakAngle; }

		void			setBreakAngleRandom(Real a_breakAngleRandom)
						{	m_breakAngleRandom=a_breakAngleRandom; }
		Real			breakAngleRandom(void) const
						{	return m_breakAngleRandom; }

		void			setElasticAngle(Real a_elasticAngle)
						{	m_elasticAngle=a_elasticAngle; }
		Real			elasticAngle(void) const
						{	return m_elasticAngle; }

		void			setElasticAngleRandom(Real a_elasticAngleRandom)
						{	m_elasticAngleRandom=a_elasticAngleRandom; }
		Real			elasticAngleRandom(void) const
						{	return m_elasticAngleRandom; }

		void			setPopAngle(Real a_popAngle)
						{	m_popAngle=a_popAngle; }
		Real			popAngle(void) const
						{	return m_popAngle; }

		void			setPopAngleRandom(Real a_popAngleRandom)
						{	m_popAngleRandom=a_popAngleRandom; }
		Real			popAngleRandom(void) const
						{	return m_popAngleRandom; }

		void			setRecoveryAngle(Real a_recoveryAngle)
						{	m_recoveryAngle=a_recoveryAngle; }
		Real			recoveryAngle(void) const
						{	return m_recoveryAngle; }

		void			setRecoveryAngleRandom(Real a_recoveryAngleRandom)
						{	m_recoveryAngleRandom=a_recoveryAngleRandom; }
		Real			recoveryAngleRandom(void) const
						{	return m_recoveryAngleRandom; }

		void			setHalfingSteps(I32 a_halfingSteps)
						{	m_halfingSteps=a_halfingSteps; }
		I32				halfingSteps(void)
						{	return m_halfingSteps; }

		void			setCollisionMethod(CollisionMethod a_method)
						{	m_method=a_method; }
		CollisionMethod	collisionMethod(void)
						{	return m_method; }

		void			setCollider(sp<SurfaceI> a_spSurfaceI)
						{	m_spCollider=a_spSurfaceI; }
		sp<SurfaceI>	collider(void)
						{	return m_spCollider; }

		void			setDriver(sp<SurfaceI> a_spSurfaceI)
						{	m_spDriver=a_spSurfaceI; }
		sp<SurfaceI>	driver(void)
						{	return m_spDriver; }

		void			setDeformed(sp<SurfaceI> a_spSurfaceI)
						{	m_spDeformed=a_spSurfaceI; }
		sp<SurfaceI>	deformed(void)
						{	return m_spDeformed; }

		void			setDrawDebug(sp<DrawI> a_spDrawDebug)
						{	m_spDrawDebug=a_spDrawDebug; }

		void			storeAsRest(U32 a_index);

		void			populate(void);
		void			step(U32 a_index);

		Array<SpatialVector>&		tangent(void)	{ return m_tangent; }
		Array<SpatialVector>&		facing(void)	{ return m_facing; }
		Array<SpatialVector>&		forceIn(void)	{ return m_forceIn; }
		Array<SpatialVector>&		velocityIn(void){ return m_velocityIn; }
		Array<Real>&				velocity(void)	{ return m_velocity; }
		Array<I32>&					bindFace(void)	{ return m_bindFace; }
		Array<SpatialBary>&			bindBary(void)	{ return m_bindBary; }
		Array<SpatialVector>&		location(void)	{ return m_location; }
		Array<SpatialVector>&		locationRef(void)
										{	return m_locationRef; }
		Array<SpatialVector>&		locationDef(void)
										{	return m_locationDef; }

	private:
	class Contact
	{
		public:
							Contact(void)	{ clear(); }
							~Contact(void)									{}

			void			clear(void)
							{
								set(m_offset);
								set(m_normal);
								m_active=FALSE;
							}
			void			setOffset(SpatialVector a_offset)
							{
								m_offset=a_offset;
								m_active=TRUE;
							}
			void			setNormal(SpatialVector a_normal)
							{
								m_normal=a_normal;
							}

			BWORD			active(void)	{ return m_active; }
			SpatialVector	offset(void)	{ return m_offset; }
			SpatialVector	normal(void)	{ return m_normal; }

		private:
			SpatialVector	m_offset;
			SpatialVector	m_normal;
			BWORD			m_active;
	};

		void			computeAngles(U32 a_index, Vector4& a_rAngle,
								const SpatialVector* a_pDelta);

		Array<Contact>			m_contact;
		Array<SpatialVector>	m_tangent;		// root direction
		Array<SpatialVector>	m_facing;		// unit facing
		Array<SpatialVector>	m_forceIn;		// external force
		Array<SpatialVector>	m_velocityIn;	// external velocity
		Array<SpatialVector>	m_restFacing;	// original facing
		Array<SpatialVector>	m_location;		// local points
		Array<SpatialVector>	m_locationRef;	// in driver space
		Array<SpatialVector>	m_locationDef;	// in deformed space
		Array<Real>				m_velocity;		// angular velocities
		Array<I32>				m_bindFace;		// attach face index
		Array<SpatialBary>		m_bindBary;		// attach coordinate
		Array<Real>				m_restLength;	// full base to tip
		Array<Real>				m_tangentBias;	// tend to keep same dir
		Array<I32>				m_kink;			// buckle index
		Array<I32>				m_kinkForward;	// buckle dir (boolean)
		Array< Vector4 >		m_y;			// last correction
		Array< Vector4 >		m_length;		// segment length
		Array< Vector4 >		m_restAngle;	// original rest angle
		Array< Vector4 >		m_plasticAngle;	// dynamic rest angle

		Real				m_t;				// time
		Real				m_tension;			// spring
		Real				m_tensionRandom;
		Real				m_restoration;
		Real				m_restorationRandom;
		Real				m_drag;				// angular drag
		Real				m_dragRandom;
		Real				m_bend;				// added bend to rest angle
		BWORD				m_grabbing;
		Real				m_grabBias;
		Real				m_grabBiasRandom;
		Real				m_collisionGap;
		Real				m_mass;
		Real				m_h;				// time step
		Real				m_wind;
		Real				m_turbulence;
		BWORD				m_buckling;
		Real				m_breakAngle;		// buckle above this
		Real				m_breakAngleRandom;
		Real				m_elasticAngle;		// spring back a little
		Real				m_elasticAngleRandom;
		Real				m_popAngle;			// unbuckle below this
		Real				m_popAngleRandom;
		Real				m_recoveryAngle;	// inc towards original rest
		Real				m_recoveryAngleRandom;
		CollisionMethod		m_method;
		I32					m_halfingSteps;

		sp<SurfaceI>		m_spCollider;
		sp<SurfaceI>		m_spDriver;
		sp<SurfaceI>		m_spDeformed;
		sp<DrawI>			m_spDrawDebug;

		Matrix<4,4,Real>	m_invA;
		Matrix<4,4,Real>	m_dfdx;
		Matrix<4,4,Real>	m_hdfdx;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __grass_Blade__ */
