/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <grass/grass.pmh>

namespace fe
{
namespace ext
{

void FlatnessOp::initialize(void)
{
	catalog<String>("icon")="FE_alpha";

	catalog<Real>("rotateRoot")=0.0;
	catalog<Real>("rotateRoot","high")=3.0;
	catalog<Real>("rotateRoot","max")=1e6;
	catalog<String>("rotateRoot","label")="Rotate Root";
	catalog<bool>("rotateRoot","joined")=true;
	catalog<String>("rotateRoot","hint")=
			"Peak of positional change at the start of each curve.";

	catalog<Real>("rotateTip")=1.0;
	catalog<Real>("rotateTip","high")=3.0;
	catalog<Real>("rotateTip","max")=1e6;
	catalog<String>("rotateTip","label")="Tip";
	catalog<bool>("rotateTip","joined")=true;
	catalog<String>("rotateTip","hint")=
			"Peak of positional change at the end of each curve.";

	catalog<Real>("rotateGamma")=1.0;
	catalog<Real>("rotateGamma","high")=2.0;
	catalog<Real>("rotateGamma","max")=1e3;
	catalog<String>("rotateGamma","label")="Gamma";
	catalog<String>("rotateGamma","hint")=
			"Adjust how much influence the Rotate Root has on"
			" the overall rotate distribution along the curve.";

	catalog<Real>("random")=0.0;
	catalog<Real>("random","high")=1.0;
	catalog<Real>("random","max")=1e6;
	catalog<String>("random","label")="Random";
	catalog<String>("random","hint")=
			"Variation of positional change.";

	catalog<String>("facingAttr")="facing";
	catalog<String>("facingAttr","label")="Facing Attribute";
	catalog<String>("facingAttr","hint")=
			"Vector attribute on input curve primitives"
			" describing a direction roughly perpendicular to the curve.";

	catalog< sp<Component> >("Input Curves");

	catalog< sp<Component> >("Output Curves");
	catalog<String>("Output Curves","IO")="output";
	catalog<String>("Output Curves","copy")="Input Curves";
	catalog<bool>("Output Curves","clobber")=true;
}

void FlatnessOp::handle(Record& a_rSignal)
{
	const Real rotateRoot=catalog<Real>("rotateRoot");
	const Real rotateTip=catalog<Real>("rotateTip");
	const Real rotateGamma=catalog<Real>("rotateGamma");
	const Real random=catalog<Real>("random");

	const String facingAttr=catalog<String>("facingAttr");

	sp<SurfaceAccessibleI> spOutputAccessible;
	if(!accessOutput(spOutputAccessible,a_rSignal)) return;

	sp<SurfaceAccessorI> spOutputVertices;
	if(!access(spOutputVertices,spOutputAccessible,
			e_primitive,e_vertices)) return;

	sp<SurfaceAccessorI> spPrimitiveFacing;
	access(spPrimitiveFacing,spOutputAccessible,
			e_primitive,facingAttr,e_warning,e_refuseMissing);

	I32 curveCount=0;

	const I32 primitiveCount=spOutputVertices->count();
	for(I32 primitiveIndex=0;primitiveIndex<primitiveCount;primitiveIndex++)
	{
		const I32 subCount=spOutputVertices->subCount(primitiveIndex);
		if(subCount<2)
		{
			continue;
		}

		Real rotateStart=rotateRoot;
		if(random>Real(0))
		{
			rotateStart+=random*m_noise.whiteNoise2d(
					Vector2f(7*primitiveIndex,0.0));
		}
		const Real rotateAdd=rotateTip-rotateRoot;

		const SpatialVector rootPoint=
				spOutputVertices->spatialVector(primitiveIndex,0);
		const SpatialVector lastPoint=
				spOutputVertices->spatialVector(primitiveIndex,subCount-1);

		const SpatialVector tangent=unitSafe(lastPoint-rootPoint);

		const SpatialVector facing=spPrimitiveFacing.isValid()?
				unitSafe(spPrimitiveFacing->spatialVector(primitiveIndex)):
				SpatialVector(0,-1,0);

		//* tangentX, facingY (curve normal)

		SpatialTransform xformRoot;
		makeFrameNormalY(xformRoot,rootPoint,tangent,facing);

		SpatialTransform invRoot;
		invert(invRoot,xformRoot);

		for(I32 subIndex=0;subIndex<subCount;subIndex++)
		{
			const Real along=subIndex/Real(subCount-1);
			const Real rotateAlong=rotateStart+
					rotateAdd*pow(along,rotateGamma);

			SpatialTransform rotation;
			setIdentity(rotation);
			rotate(rotation,rotateAlong,e_zAxis);

			const SpatialTransform conversion=invRoot*rotation*xformRoot;

			const SpatialVector point=
					spOutputVertices->spatialVector(primitiveIndex,subIndex);

			SpatialVector rotated;
			transformVector(conversion,point,rotated);

			spOutputVertices->set(primitiveIndex,subIndex,rotated);
		}

		curveCount++;
	}

	String summary;
	summary.sPrintf("%d curve%s\n",curveCount,curveCount==1? "": "s");
	catalog<String>("summary")=summary;
}

} /* namespace ext */
} /* namespace fe */
