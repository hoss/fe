/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __grass_CurlOp_h__
#define __grass_CurlOp_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
    @brief spiral output curves around input curves

	@ingroup grass
*//***************************************************************************/
class FE_DL_EXPORT CurlOp:
	public OperatorSurfaceCommon,
	public Initialize<CurlOp>
{
	public:
				CurlOp(void)												{}
virtual			~CurlOp(void)												{}

		void	initialize(void);

				//* As HandlerI
virtual void	handle(Record& a_rSignal);
};

} /* namespace ext */
} /* namespace fe */

#endif /* __grass_CurlOp_h__ */
