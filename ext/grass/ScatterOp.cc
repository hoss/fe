/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <grass/grass.pmh>

namespace fe
{
namespace ext
{

void ScatterOp::initialize(void)
{
	//* NOTE inherits from MapOp

	catalog<String>("icon")="FE_alpha";

	catalog<String>("template")="";
	catalog<String>("template","label")="Map Template File";
	catalog<String>("template","hint")="Full pathname of density maps.";

	catalog<bool>("attribute","hidden")=true;
	catalog<bool>("class","hidden")=true;
	catalog<bool>("type","hidden")=true;
	catalog<bool>("toMin","hidden")=true;
	catalog<bool>("toMax","hidden")=true;

	catalog<Real>("density")=1e3;
	catalog<Real>("density","min")=0.0;
	catalog<Real>("density","high")=1e4;
	catalog<Real>("density","max")=1e6;
	catalog<String>("density","label")="Density.";

	catalog< sp<Component> >("Input Surface");

	catalogRemove("Output Surface","copy");
	catalogRemove("Output Surface","clobber");
}

void ScatterOp::handle(Record& a_rSignal)
{
	const Real density=catalog<Real>("density");
	const String templateFile=catalog<String>("template");

	sp<SurfaceAccessibleI> spInputAccessible;
	if(!access(spInputAccessible,"Input Surface")) return;

	sp<SurfaceAccessibleI> spOutputAccessible;
	if(!accessOutput(spOutputAccessible,a_rSignal)) return;

	sp<SurfaceAccessorI> spInputVertices;
	if(!access(spInputVertices,spInputAccessible,
			e_primitive,e_vertices)) return;

	sp<SurfaceI> spInput;
	if(!access(spInput,spInputAccessible)) return;

	sp<SurfaceAccessorI> spOutputNormal;
	if(!access(spOutputNormal,spOutputAccessible,
			e_point,e_normal)) return;

	sp<SurfaceAccessorI> spOutputUV;
	if(!access(spOutputUV,spOutputAccessible,
			e_point,e_uv)) return;

	sp<DrawI> spDrawI;
	if(!accessDraw(spDrawI,a_rSignal)) return;

	const BWORD useMap=(!templateFile.empty());

	Raster raster(registry());
	raster.setTemplate(templateFile);
	raster.setMinMax(0.0,1.0,0.0);

	I32 triangleIndex=0;
	I32 outputCount=0;

	Real carry(0);

	const I32 primitiveCount=spInputVertices->count();
	for(I32 primitiveIndex=0;primitiveIndex<primitiveCount;primitiveIndex++)
	{
		const I32 subCount=spInputVertices->subCount(primitiveIndex);
		if(subCount<3)
		{
			continue;
		}

		//* TODO other than triangles and quads
		const I32 triCount=subCount>3? 2: 1;

		for(I32 triIndex=0;triIndex<triCount;triIndex++)
		{

			const SpatialVector v0=
					spInputVertices->spatialVector(primitiveIndex,0);
			const SpatialVector v1=
					spInputVertices->spatialVector(primitiveIndex,
					triIndex? 2: 1);
			const SpatialVector v2=
					spInputVertices->spatialVector(primitiveIndex,
					triIndex? 3: 2);

			const SpatialVector v01=v1-v0;
			const Real base=magnitude(v01);

			const SpatialVector unit01=v01*(1/base);
			const SpatialVector v02=v2-v0;
			const Real along=dot(v02,unit01);
			const SpatialVector perpendicular=v02-along*unit01;
			const Real height=magnitude(perpendicular);

			const Real area=0.5*base*height+carry;

			const I32 sampleCount=area*density;

			carry=area-sampleCount/density;

//			feLog("ScatterOp::handle prim %d/%d tri %d area %.6G samples %d\n",
//					primitiveIndex,primitiveCount,triIndex,area,sampleCount);
//			feLog("  v0 %s v1 %s v2 %s base %.6G height %.6G\n",
//					c_print(v0),c_print(v1),c_print(v2),base,height);

			for(I32 sampleIndex=0;sampleIndex<sampleCount;sampleIndex++)
			{
#if FALSE
				//* no consistantancy when changing density param
				const Real r1=randomReal(0.0,1.0);
				const Real r2=randomReal(0.0,1.0);
				const Real r3=randomReal(1e-6,1.0);
#else
				//* higher density is a superset of lower density
				Vector2d noiseInput(1e2*triangleIndex,1e3*sampleIndex);
				const Real r1=clamp(
						Real(fabs(m_noise.whiteNoise2d(noiseInput))),
						Real(0),Real(1));

				noiseInput[1]+=3e2;
				const Real r2=clamp(
						Real(fabs(m_noise.whiteNoise2d(noiseInput))),
						Real(0),Real(1));

				noiseInput[1]+=3e2;
				const Real r3=clamp(
						Real(fabs(m_noise.whiteNoise2d(noiseInput))),
						Real(1e-6),Real(1));
#endif

				const Real sqrt_r1=sqrt(r1);

				//* "uniform random point in triangle"
				//* https://math.stackexchange.com/questions/18686
				const SpatialBary bary(
						1.0-sqrt_r1,
						sqrt_r1*(1.0-r2),
						sqrt_r1*r2);

#if FALSE
				const SpatialVector sample=location(bary,v0,v1,v2);
				sp<SurfaceI::ImpactI> spImpact=spInput->nearestPoint(sample);
#else
				sp<SurfaceI::ImpactI> spImpact=
						spInput->sampleImpact(triangleIndex,bary);
#endif

				if(!spImpact.isValid())
				{
					continue;
				}

				const SpatialVector intersection=spImpact->intersection();
				const SpatialVector normal=spImpact->normal();
				const Vector2 uv=spImpact->uv();

//				feLog("ScatterOp::handle triangleIndex %d uv %s\n",
//						triangleIndex,c_print(uv));

				if(useMap)
				{
					const Real densityScalar=raster.real(uv);

					if(densityScalar<r3)
					{
						continue;
					}
				}

				//* TODO accumulate points and then add in bundles

				spDrawI->drawPoints(&intersection,&normal,1,false,NULL);

				spOutputNormal->set(outputCount,normal);
				spOutputUV->set(outputCount,uv);

				outputCount++;
			}

			triangleIndex++;
		}
	}

//	spOutputAccessible->save("scatter.rg");

	String summary;
	summary.sPrintf("%d point%s\n",outputCount,outputCount==1? "": "s");
	catalog<String>("summary")=summary;
}

} /* namespace ext */
} /* namespace fe */
