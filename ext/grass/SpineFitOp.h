/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __grass_SpineFitOp_h__
#define __grass_SpineFitOp_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Handler to wrap and twist geometry to follow bends in a curve

	@ingroup grass
*//***************************************************************************/
class FE_DL_EXPORT SpineFitOp:
	public OperatorSurfaceCommon,
	public Initialize<SpineFitOp>
{
	public:

					SpineFitOp(void)										{}
virtual				~SpineFitOp(void)										{}

		void		initialize(void);

					//* As HandlerI
virtual	void		handle(Record& a_rSignal);

static	Real		nearestPointOnCurve(const SpatialVector a_point,
							const sp<SurfaceAccessorI> a_spCurves,
							const I32 a_index);

	private:
		sp<DrawMode>	m_spDebugGroup;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __grass_SpineFitOp_h__ */
