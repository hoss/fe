/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __ode_World_h__
#define __ode_World_h__
namespace fe
{
namespace ext
{

class FE_DL_EXPORT World :
	virtual public MechanicsSimI,
	public Initialize<World>
{
	public:
			World(void);
virtual		~World(void);

			void		initialize(void);
virtual		void		step(Real a_dt);

virtual		sp<BodyI>	createBody(void);
virtual		sp<JointI>	createJoint(JointType a_type,
				sp<BodyI> a_A, sp<BodyI> a_B);
virtual		sp<JointI>	createContact(sp<BodyI> a_A,
				const SpatialVector &a_location,
				const SpatialVector &a_normal,
				Real a_depth, Real a_stiffness,
				Real a_damping, Real a_timestep);

virtual		void		setGravity(const Vector3 &a_gravity);

			dWorldID id(void) { return m_world; }

	private:
		dWorldID		m_world;
		sp<Component>	m_ode;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __ode_World_h__ */


