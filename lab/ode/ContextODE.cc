/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <ode/ode.pmh>

#include "ode/collision.h"
#include "ContextODE.h"

namespace fe
{
namespace ext
{

ContextODE::ContextODE(void)
{
	dInitODE();
}

ContextODE::~ContextODE(void)
{

	dCloseODE();
}

} /* namespace ext */
} /* namespace fe */
