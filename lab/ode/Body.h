/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __ode_Body_h__
#define __ode_Body_h__
namespace fe
{
namespace ext
{


class FE_DL_EXPORT Body : public BodyI
{
	public:
				Body(dWorldID a_world_id);
virtual			~Body(void);

virtual	void	setMass(const Mass &a_mass);
virtual	void	getMass(Mass &a_mass);
virtual	void	setPosition(const Vector3 &a_pos);
virtual	void	setRotation(const SpatialTransform &a_rotation);
virtual	void	getPosition(Vector3 &a_pos);
virtual	void	getRotation(SpatialTransform &a_rotation);
virtual	void	applyForce(const SpatialVector &a_force,
					const SpatialVector &a_pos);
virtual	void	applyTorque(const SpatialVector &a_torque);
virtual void	getVelocity(SpatialVector &a_velocity,
					const SpatialVector &a_pos);
virtual void	getAngularVelocity(SpatialVector &a_velocity);

virtual void	translate(const SpatialVector &a_displ);

virtual	hp<MechanicsSimI> &sim(void) { return m_spSim; }

		dBodyID	id(void) { return m_id; }


	private:
		dBodyID				m_id;
		SpatialVector		m_offset;
		hp<MechanicsSimI>	m_spSim;

};

} /* namespace ext */
} /* namespace fe */

#endif /* __ode_Body_h__ */


