/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __ode_Joint_h__
#define __ode_Joint_h__
namespace fe
{
namespace ext
{

class FE_DL_EXPORT Joint : public JointI
{
	public:
				Joint(dWorldID a_world_id, JointType a_type,
					sp<BodyI> a_A, sp<BodyI> a_B);
				Joint(dWorldID, sp<BodyI> a_A,
					const SpatialVector &a_location,
					const SpatialVector &a_normal,
					Real a_depth,Real a_stiffness,
					Real a_damping, Real a_timestep);
virtual			~Joint(void);

virtual	void	setAnchor(const Vector3 &a_anchor);
virtual	void	setAxis(const Vector3 &a_axis);
virtual	void	limitRange(Real a_angleLo, Real a_angleHi);
virtual	void	setSpring(Real a_timestep, Real a_stiffness, Real a_damping);
virtual	hp<MechanicsSimI> &sim(void) { return m_spSim; }



	private:
		dJointID			m_id;
		JointType			m_type;
		hp<MechanicsSimI>	m_spSim;
};

class FE_DL_EXPORT DistanceJoint : public JointI
{
};

} /* namespace ext */
} /* namespace fe */

#endif /* __ode_Joint_h__ */

