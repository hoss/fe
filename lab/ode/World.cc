/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <ode/ode.pmh>

namespace fe
{
namespace ext
{

World::World(void)
{
}

World::~World(void)
{
}

void World::initialize(void)
{
	m_ode = registry()->create("Component.ContextODE.fe");
	m_world = dWorldCreate();
	dWorldSetERP(m_world, 0.9);
	//dWorldSetCFM(m_world, 1.0e-7);
	dWorldSetCFM(m_world, 0.00001);
}

void World::step(Real a_dt)
{
	//dWorldStepFast1(m_world, a_dt, 100);
	dWorldStep(m_world, a_dt);
}

sp<BodyI> World::createBody(void)
{
	sp<Body> spBody = sp<Body>(new Body(m_world));
	spBody->sim() = this;
	return spBody;
}

sp<JointI> World::createJoint(JointType a_type, sp<BodyI> a_A, sp<BodyI> a_B)
{
	sp<Joint> spJoint = sp<Joint>(new Joint(m_world, a_type, a_A, a_B));
	spJoint->sim() = this;
	return spJoint;
}

sp<JointI> World::createContact(sp<BodyI> a_A, const SpatialVector &a_location, const SpatialVector &a_normal, Real a_depth, Real a_stiffness, Real a_damping, Real a_timestep)
{
	sp<Joint> spJoint = sp<Joint>(new Joint(m_world, a_A, a_location,
			a_normal, a_depth, a_stiffness, a_damping, a_timestep));
	spJoint->sim() = this;
	return spJoint;
}

void World::setGravity(const Vector3 &a_gravity)
{
	dWorldSetGravity(m_world, a_gravity[0], a_gravity[1], a_gravity[2]);
}


} /* namespace ext */
} /* namespace fe */

