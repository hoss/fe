/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <ode/ode.pmh>

namespace fe
{
namespace ext
{

Joint::Joint(dWorldID a_world_id, JointType a_type, sp<BodyI> a_A, sp<BodyI> a_B)
{
	m_type = a_type;

	switch(a_type)
	{
		case e_hinge:
			m_id = dJointCreateHinge(a_world_id, 0);
			break;
		case e_ball:
			m_id = dJointCreateBall(a_world_id, 0);
			break;
		case e_slider:
			m_id = dJointCreateSlider(a_world_id, 0);
			break;
		default:
			feX("Joint::Joint", "unknown type");
	}

	sp<Body> body_a(a_A);
	dBodyID id_a = 0;
	if(body_a.isValid())
	{
		id_a = body_a->id();
	}

	sp<Body> body_b(a_B);
	dBodyID id_b = 0;
	if(body_b.isValid())
	{
		id_b = body_b->id();
	}

	dJointAttach(m_id, id_a, id_b);

}

Joint::Joint(dWorldID a_world_id, sp<BodyI> a_A, const SpatialVector &a_location, const SpatialVector &a_normal, Real a_depth, Real a_stiffness, Real a_damping, Real a_timestep)
{
	m_type = e_contact;

	Real erp = (a_timestep * a_stiffness) /
		(a_timestep * a_stiffness + a_damping);
	Real cfm = 1.0 / (a_timestep * a_stiffness + a_damping);

	dContact contactInfo;
	//contactInfo.surface.mode =  dContactBounce |  dContactSoftERP |  dContactSoftCFM;
	contactInfo.surface.mode =  dContactSoftERP |  dContactSoftCFM;
	//contactInfo.surface.mode =  dContactBounce;
	contactInfo.surface.soft_erp = erp;
	contactInfo.surface.soft_cfm = cfm;
	contactInfo.surface.bounce = 1.0;
	contactInfo.geom.pos[0] = a_location[0];
	contactInfo.geom.pos[1] = a_location[1];
	contactInfo.geom.pos[2] = a_location[2];
	contactInfo.geom.normal[0] = a_normal[0];
	contactInfo.geom.normal[1] = a_normal[1];
	contactInfo.geom.normal[2] = a_normal[2];
	contactInfo.geom.depth = a_depth;
	contactInfo.geom.g1 = 0;
	contactInfo.geom.g2 = 0;

fe_fprintf(stderr, "created contact joint\n");
	m_id = dJointCreateContact(a_world_id, 0, &contactInfo);

	sp<Body> body_a(a_A);
	dBodyID id_a = 0;
	if(body_a.isValid())
	{
		id_a = body_a->id();
	}
	dBodyID id_b = 0;
	dJointAttach(m_id, id_a, id_b);
}

Joint::~Joint(void)
{
fe_fprintf(stderr, "destroyed joint\n");
	dJointDestroy(m_id);
}

void Joint::setAnchor(const Vector3 &a_anchor)
{
	switch(m_type)
	{
		case e_hinge:
			dJointSetHingeAnchor(m_id, a_anchor[0], a_anchor[1], a_anchor[2]);
			break;
		case e_ball:
			dJointSetBallAnchor(m_id, a_anchor[0], a_anchor[1], a_anchor[2]);
			break;
		default:
			feX("Joint::setAnchor", "unknown type");
	}
}

void Joint::setAxis(const Vector3 &a_axis)
{
	switch(m_type)
	{
		case e_hinge:
			dJointSetHingeAxis(m_id, a_axis[0], a_axis[1], a_axis[2]);
			break;
		case e_slider:
			dJointSetSliderAxis(m_id, a_axis[0], a_axis[1], a_axis[2]);
			break;
		default:
			feX("Joint::setAnchor", "unknown type");
	}
}

void Joint::limitRange(Real a_angleLo, Real a_angleHi)
{
	switch(m_type)
	{
		case e_hinge:
			dJointSetHingeParam(m_id, dParamLoStop, a_angleLo);
			dJointSetHingeParam(m_id, dParamHiStop, a_angleHi);
			break;
		case e_slider:
			dJointSetSliderParam(m_id, dParamLoStop, a_angleLo);
			dJointSetSliderParam(m_id, dParamHiStop, a_angleHi);
			break;
		default:
			feX("Joint::limit angle", "unknown type");
	}
}

void Joint::setSpring(Real a_timestep, Real a_stiffness, Real a_damping)
{
	if(a_stiffness == 0.0 && a_damping == 0.0) { return; }
	Real erp = (a_timestep * a_stiffness) /
		(a_timestep * a_stiffness + a_damping);
	Real cfm = 1.0 / (a_timestep * a_stiffness + a_damping);
	switch(m_type)
	{
		case e_hinge:
			dJointSetHingeParam(m_id, dParamStopERP, erp);
			dJointSetHingeParam(m_id, dParamStopCFM, cfm);
			break;
		case e_slider:
			dJointSetSliderParam(m_id, dParamStopERP, erp);
			dJointSetSliderParam(m_id, dParamStopCFM, cfm);
			break;
		default:
			feX("Joint::setSpring", "unknown type");
	}
}

} /* namespace ext */
} /* namespace fe */

