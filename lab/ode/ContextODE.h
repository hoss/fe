/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __ode_ContextODE_h__
#define __ode_ContextODE_h__

#include "fe/data.h"
namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Encapsulate start/shutdown of ODE

	@ingroup ext_ode
*//***************************************************************************/
class FE_DL_EXPORT ContextODE: virtual public Component
{
	public:
					ContextODE(void);
virtual				~ContextODE(void);
};

} /* namespace ext */
} /* namespace fe */

#endif /* __ode_ContextODE_h__ */
