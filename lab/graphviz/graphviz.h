/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __graphviz_graphviz_h__
#define __graphviz_graphviz_h__

#include "fe/plugin.h"

#endif /* __graphviz_graphviz_h__ */
