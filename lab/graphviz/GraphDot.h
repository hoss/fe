/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __graphviz_GraphDot_h__
#define __graphviz_GraphDot_h__
namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Converter from DAGNode to dot format

	@ingroup graphviz
*//***************************************************************************/
class FE_DL_EXPORT GraphDot: public ImageCommon,
		public Initialize<GraphDot>
{
	public:
						GraphDot();
virtual					~GraphDot();

		void			initialize(void);

						//* As ImageI
virtual I32				interpretSelect(String a_source);

virtual	BWORD			save(String filename)				{ return FALSE; }
virtual void			select(I32 id)										{}
virtual I32				selected(void) const				{ return 0; }
virtual	void			unload(I32 id)										{}

virtual	void			setFormat(ImageI::Format format)
						{
							if(m_spDelegate.isValid())
							{
								m_spDelegate->setFormat(format);
								incrementSerial();
							}
						}
virtual	ImageI::Format	format(void) const
						{	return m_spDelegate.isValid()?
									m_spDelegate->format(): ImageI::e_none; }

virtual	void			resize(U32 width,U32 height,U32 depth)
						{
							if(m_spDelegate.isValid())
							{
								m_spDelegate->resize(width,height,depth);
							}
							incrementSerial();
						}
virtual	void			replaceRegion(U32 x,U32 y,U32 z,
								U32 width,U32 height,U32 depth,void* data)
						{
							if(m_spDelegate.isValid())
							{
								m_spDelegate->replaceRegion(x,y,z,
										width,height,depth,data);
							}
							incrementSerial();
						}

virtual	U32				width(void) const
						{	return m_spDelegate.isValid()?
									m_spDelegate->width(): 0; }
virtual	U32				height(void) const
						{	return m_spDelegate.isValid()?
									m_spDelegate->height(): 0; }
virtual	U32				depth(void) const
						{	return m_spDelegate.isValid()?
									m_spDelegate->depth(): 0; }
virtual	void*			raw(void) const
						{	return m_spDelegate.isValid()?
									m_spDelegate->raw(): NULL; }

virtual	U32				regionCount(void) const;
virtual	String			regionName(U32 a_regionIndex) const;
virtual	Box2i			regionBox(String a_regionName) const;
virtual	String			pickRegion(I32 a_x,I32 a_y) const;

	private:

virtual	void			render(String a_format);

		void			reset(void);

		GVC_t*			m_pGvc;
		Agraph_t*		m_pGraph;

		sp<ImageI>		m_spDelegate;

		I32				m_offsetX;
		I32				m_offsetY;
		Real			m_rescaleX;
		Real			m_rescaleY;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __graphviz_GraphDot_h__ */
