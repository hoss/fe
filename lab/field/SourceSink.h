/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __field_SourceSink_h__
#define __field_SourceSink_h__

#include "signal/signal.h"
#include "datatool/datatool.h"
#include "math/math.h"
#include "shape/shape.h"
namespace fe
{
namespace ext
{

/**	Changes local value in scalar field
	*/
class FE_DL_EXPORT BasicSourceSink :
	virtual public HandlerI,
	virtual public Config
{
	public:
				BasicSourceSink(void);
virtual			~BasicSourceSink(void);

				// AS HandlerI
virtual void	handle(	Record &r_sig);

	private:
		AsParticle					m_asParticle;
		Accessor<Real>				m_aConsumptionRate;

};


} /* namespace ext */
} /* namespace fe */

#endif /* __field_SourceSink_h__ */


