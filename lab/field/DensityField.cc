/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <field/field.pmh>

namespace fe
{
namespace ext
{

DensityField::DensityField(void)
{
}

DensityField::~DensityField(void)
{
}

void DensityField::inject(const Real &a_amount, const SpatialVector &a_loc)
{
	Real v = volume();
	add(a_loc, a_amount / v);
}

} /* namespace ext */
} /* namespace fe */

