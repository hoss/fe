/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __field_DensityField_h__
#define __field_DensityField_h__
namespace fe
{
namespace ext
{

class FE_DL_EXPORT DensityFieldI : virtual public Component
{
	public:
virtual	void	inject(const Real &a_amount, const SpatialVector &a_loc)	= 0;
};

/**	Interprets scalar field values as density
	*/
class FE_DL_EXPORT DensityField :
	virtual public GridScalarField,
	virtual	public DensityFieldI
{
	public:
				DensityField(void);
virtual			~DensityField(void);

				/// inject amount
virtual	void	inject(const Real &a_amount, const SpatialVector &a_loc);

	private:

};

} /* namespace ext */
} /* namespace fe */


#endif /* __field_DensityField_h__ */

