/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __field_fieldAS_h__
#define __field_fieldAS_h__
namespace fe
{
namespace ext
{

class AsScalarField :
	virtual public AccessorSet,
	public Initialize<AsScalarField>
{
	public:
		void initialize(void)
		{
			add(field,	FE_SPEC("field:scalar",		"scalar field component"));
		}
		Accessor< sp<Component> >					field;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __field_fieldAS_h__ */

