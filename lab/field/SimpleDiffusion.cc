/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "SimpleDiffusion.h"

#include <field/field.pmh>

namespace fe
{
namespace ext
{

SimpleDiffusion::SimpleDiffusion(void)
{
}

SimpleDiffusion::~SimpleDiffusion(void)
{
}

void SimpleDiffusion::initialize(void)
{
	cfg<Real>("rate") = 1.0f;
	cfg<int>("substeps") = 1;
}


void SimpleDiffusion::handle(Record &r_sig)
{
	Real timestep = cfg<Real>("timestep");

	sp<DensityField> spField = cfg< sp<Component> >("field"); // field to diffuse
	if(!spField.isValid()) { return; }

	Real rate = cfg<Real>("rate") ; // diffusion rate
	int substeps = cfg<int>("substeps"); // diffusion substeps

	Continuum<Real> accum;

	Vector3i count;
	count = spField->continuum().unit().count();

	SpatialVector cellsize;
	spField->continuum().cellsize(cellsize);

	SpatialVector area;
	area[0] = cellsize[1] * cellsize[2];
	area[1] = cellsize[0] * cellsize[2];
	area[2] = cellsize[0] * cellsize[1];

	bool next[3];
	Vector3i i;

	timestep /= (Real)substeps;
	for(int c = 0; c < substeps; c++)
	{
		for(i[0] = 0; i[0] < count[0]; i[0]++)
		{
			if(i[0] < count[0] - 1) { next[0] = true; }
			else { next[0] = false; }

			for(i[1] = 0; i[1] < count[1]; i[1]++)
			{
				if(i[1] < count[1] - 1) { next[1] = true; }
				else { next[1] = false; }

				for(i[2] = 0; i[2] < count[2]; i[2]++)
				{
					if(i[2] < count[2] - 1) { next[2] = true; }
					else { next[2] = false; }

					Real &a = spField->continuum().accessUnit().access(i);
					for(int d = 0; d < 2; d++)
					{
						if(next[d])
						{
							Vector3i j(i);
							j[d] += 1;
							Real &b =
								spField->continuum().accessUnit().access(j);
							Real diff = (a-b)/(cellsize[d]);
							diff *= rate*timestep;
							b += diff;
							a -= diff;
						}
					}
				}
			}
		}

	}

}

} /* namespace ext */
} /* namespace fe */


