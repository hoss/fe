/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <field/field.pmh>

#include "DensitySink.h"

namespace fe
{
namespace ext
{

DensitySink::DensitySink(void)
{
}

DensitySink::~DensitySink(void)
{
}

void DensitySink::initialize(void)
{
}

void DensitySink::handle(Record &r_sig)
{
	Real timestep = cfg<Real>("timestep");

	sp<RecordGroup> rg_input = cfg< sp<RecordGroup> >("input"); // input group

	sp<DensityField> spField = cfg< sp<Component> >("field"); // target field
	if(!spField.isValid()) { return; }
	Real volume = spField->volume();


	for(RecordGroup::iterator i_rg = rg_input->begin();
		i_rg != rg_input->end(); i_rg++)
	{
		sp<RecordArray> spRA(*i_rg);
		hp<Scope> spScope = spRA->layout()->scope();
		m_asParticle.bind(spScope);
		ScopedPathAccessor<Real> aRate(spScope, cfg<String>("rate"));
		ScopedPathAccessor<Real> aAccum(spScope, cfg<String>("accumulator"));
		if(	m_asParticle.location.check(spRA))
		{
			for(int i = 0; i < spRA->length(); i++)
			{
				Record record = spRA->getRecord(i);
				SpatialVector &location = m_asParticle.location(record);
				Real density = spField->sample(location);
				Real amount_available = density*volume;
				Real *pRate = aRate(record);
				Real *pAccum = aAccum(record);
				if(!pRate || !pAccum) { continue; }
				Real amount = *pRate * density * timestep;
				if(amount > amount_available) { amount = amount_available; }

				spField->inject(-amount, location);

				*pAccum += amount;
			}
		}
	}
}

} /* namespace ext */
} /* namespace fe */

