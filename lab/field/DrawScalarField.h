/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __misc_DrawScalarField_h__
#define __misc_DrawScalarField_h__

#include "signal/signal.h"
#include "draw/draw.h"
#include "window/window.h"
#include "shape/shape.h"
#include "spatial/spatial.h"
#include "viewer/DrawView.h"
#include "field/fieldAS.h"
namespace fe
{
namespace ext
{

/** Draw a scalar field

	@copydoc DrawScalarField_info
	*/
class FE_DL_EXPORT DrawScalarField :
		public Initialize<DrawScalarField>,
		public Config,
		virtual public HandlerI
{
	public:
				DrawScalarField(void);
virtual			~DrawScalarField(void);

		void	initialize(void);

				// AS HandlerI
virtual void	handleBind(sp<SignalerI> spSignalerI,
						sp<Layout> l_sig);
virtual void	handle(Record &r_sig);

	private:
		void	draw(sp<DrawI> spDraw, sp<ScalarFieldI> spField,
						sp<SpaceI> spSpace, Real a_zdepth, Real a_alpha);
		void	clean(void);
		void	resize(int a_size);

		DrawView							m_drawview;
		AsColor								m_asColor;
		sp<Layout>							m_l_sample;
		sp<Layout>							m_l_draw;
		Vector3f							m_cellCount;
		AsScalarField						m_asScalarField;

		Color								m_minSat;
		Color								m_maxSat;
		Color								m_minColor;
		Color								m_maxColor;

		sp<DrawMode>						m_spDrawmode;

		int									m_bufferCnt;
		SpatialVector						*m_va;
		SpatialVector						*m_na;
		Color								*m_ca;

};

} /* namespace ext */
} /* namespace fe */


#endif /* __misc_DrawScalarField_h__ */

