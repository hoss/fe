/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "SourceSink.h"

#include <field/field.pmh>

namespace fe
{
namespace ext
{

BasicSourceSink::BasicSourceSink(void)
{
}

BasicSourceSink::~BasicSourceSink(void)
{
}

void BasicSourceSink::handle(Record &r_sig)
{
	m_asParticle.bind(r_sig.layout()->scope());
	m_aConsumptionRate.setup(r_sig.layout()->scope(), FE_USE("--basic-consumption-rate--"));

	Real timestep = cfg<Real>("timestep");

	sp<RecordGroup> rg_input = cfg< sp<RecordGroup> >("input"); // input group

	sp<DensityField> spField = cfg< sp<Component> >("field"); // target field
	if(!spField.isValid()) { return; }


	for(RecordGroup::iterator i_rg = rg_input->begin();
		i_rg != rg_input->end(); i_rg++)
	{
		sp<RecordArray> spRA(*i_rg);
		if(	m_asParticle.location.check(spRA)	&&
			m_aConsumptionRate.check(spRA))
		{
			for(int i = 0; i < spRA->length(); i++)
			{
				spField->inject(timestep*m_aConsumptionRate(spRA, i),
					m_asParticle.location(spRA, i));
			}
		}
	}

}

} /* namespace ext */
} /* namespace fe */

