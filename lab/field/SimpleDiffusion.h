/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __field_SimpleDiffusion_h__
#define __field_SimpleDiffusion_h__

#include "fe/plugin.h"
#include "shape/shape.h"
#include "spatial/spatial.h"
namespace fe
{
namespace ext
{

/** Very simple diffusion handler.
	Single buffer (just the continuum itself) fixed step count iterations.

	@copydoc SimpleDiffusion_info

	*/
class FE_DL_EXPORT SimpleDiffusion :
	public Initialize<SimpleDiffusion>,
	virtual public HandlerI,
	virtual public Config
{
	public:
				SimpleDiffusion(void);
virtual			~SimpleDiffusion(void);

		void	initialize(void);

				// AS HandlerI
virtual void	handle(	Record &r_sig);

	private:
};

} /* namespace ext */
} /* namespace fe */

#endif /* __field_SimpleDiffusion_h__ */


