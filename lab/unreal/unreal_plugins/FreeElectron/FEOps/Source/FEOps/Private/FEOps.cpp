#include "IFEOps.h"

#if PLATFORM_LINUX
const char* ue4_module_options = "linux_global_symbols";
#endif

class FFEOps : public IFEOps
{
	/** IModuleInterface implementation */
	virtual void StartupModule() override;
	virtual void ShutdownModule() override;

		fe::sp<fe::SingleMaster>			m_spSingleMaster;
};

IMPLEMENT_MODULE( FFEOps, FEOps )

void FFEOps::StartupModule()
{
	// This code will execute after your module is loaded into memory
	// (but after global variables are initialized, of course.)

	m_spSingleMaster=UnrealGetSingleMaster();

#if FE_COMPILER==FE_GNU && !defined(__clang__)
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wconditionally-supported"
#endif
	const fe::String loadPath=fe::System::getLoadPath(
			(void*)FFEOps::IsAvailable,TRUE).pathname();
#if FE_COMPILER==FE_GNU && !defined(__clang__)
#pragma GCC diagnostic pop
#endif

	fe::DL_Loader::addPath(loadPath);

	feLog("FFEOps::StartupModule loadPath \"%s\"\n",loadPath.c_str());

	fe::sp<fe::Registry> spRegistry=m_spSingleMaster->master()->registry();

//	spRegistry->master()->catalog()->catalog<String>("path:media")=
//			"/home/symhost/sym/proj/fe/main/media";

	spRegistry->manage("fexOperatorDL");
	spRegistry->manage("fexVegetationDL");

//	spRegistry->manage("fexIronWorksDL");
//	spRegistry->manage("fexGrassDL");
}

void FFEOps::ShutdownModule()
{
	// This function may be called during shutdown to clean up your module.
	// For modules that support dynamic reloading,
	// we call this function before unloading the module.

	feLog("FFEOps::ShutdownModule\n");

	m_spSingleMaster=NULL;
}
