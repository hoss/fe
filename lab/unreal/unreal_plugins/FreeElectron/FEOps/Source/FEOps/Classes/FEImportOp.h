#pragma once

#include "FEOperator.h"

//* must be last include
#include "FEImportOp.generated.h"

//UCLASS(hidecategories = (Object, LOD),
//	meta = (BlueprintSpawnableComponent),
//	ClassGroup = Rendering)
//UCLASS(Blueprintable,BlueprintType)
UCLASS()
class AFEImportOp: public AFEOperator
{
        GENERATED_UCLASS_BODY()

	protected:

virtual	void	OperatorInit(void);
virtual	void	OperatorUpdate(void);

        UPROPERTY(EditAnywhere, Category="ImportOp",
				DisplayName="Import Filename")
		FString		m_fsFilename;

        UPROPERTY(EditAnywhere, Category="ImportOp",
				DisplayName="Model Filename")
		FString		m_fsModelfile;

        UPROPERTY(EditAnywhere, Category="ImportOp",
				DisplayName="Model Node")
		FString		m_fsModelnode;

		UFUNCTION(BlueprintCallable, Category="ImportOp")
		bool	Configure(FString configuration);

		fe::String	m_configuration;
};
