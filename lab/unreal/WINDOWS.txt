Setup Note
Setup.bat and GenerateProjectFiles.bat
use File Explorer or generic command line, not a Visual Studio shell
the 'Platform' variable is poison

Build Engine On Command Line
msbuild UE5.sln /p:Configuration="Development Editor" /p:Platform=Win64 -m:64

Rebuild Engine
'Clean' and 'Rebuild' in Visual Studio don't seem to do much.

can remove all ignored files with 'git clean -fx'
will need to run Setup.bat and GenerateProjectFiles.bat again

Build FE
currently, must build with VS 2019, even if UE5.sln builds in VS 2022 gui

Dependencies
Currently we get a crash on Windows unless USDImporter is loaded.
The reason is known, but as a temporary fix, USDImporter is listed as a dependency.

Alignment
In UnrealEngine/Engine/Source/Programs/UnrealBuildTool/Platform/Windows/VCToolChain.cs:
		// Pack struct members on 8-byte boundaries.
		Arguments.Add("/Zp8");
This can make structures incompatible with all libraries built at default (generally 16).

USD
UnrealEngine/Engine/Binaries/Win64

UnrealEngine/Engine/Plugins/Importers/USDImporter/Source/ThirdParty/USD/include
UnrealEngine/Engine/Plugins/Importers/USDImporter/Source/ThirdParty/USD/lib

UnrealEngine/Engine/Source/ThirdParty/Boost/boost-1_80_0/include
UnrealEngine/Engine/Source/ThirdParty/Boost/boost-1_80_0/lib/Win64

UnrealEngine/Engine/Source/ThirdParty/Python3/Win64/include
UnrealEngine/Engine/Source/ThirdParty/Python3/Win64/libs

UnrealEngine/Engine/Source/ThirdParty/Intel/TBB/IntelTBB-2019u8/include

Packaging
Until we find a way for Unreal to copy these during packaging,
	copy dlls in lib/x86_win64_optimize to
	<Project>/Windows/<Project>/Binaries/Win64
