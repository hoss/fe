import string
import os
import subprocess
import re
import sys

import utility
forge = sys.modules["forge"]

def prerequisites():
	return ["surface"]

def locate_unreal():
	confirmed_build = None

	unreal_build =os.environ["FE_UNREAL_BUILD"]
	if os.path.exists(unreal_build):
		confirmed_build = unreal_build

	if forge.fe_os == 'FE_WIN32' or forge.fe_os == 'FE_WIN64':
		if not confirmed_build:
			local_build = os.environ["FE_WINDOWS_LOCAL"] + "/unreal"
			if os.path.exists(local_build):
				confirmed_build = local_build

		if confirmed_build:
			confirmed_build = confirmed_build.replace("/","\\")

	return confirmed_build

def setup(module):

	confirmed_build = locate_unreal()

	version_h = confirmed_build + "/Engine/Source/Runtime/Launch/Resources/Version.h"
	if os.path.exists(version_h):
		unrealMajor = ""
		unrealMinor = ""
		unrealPatch = ""

		for line in open(version_h).readlines():
			if re.match("#define ENGINE_MAJOR_VERSION.*", line):
				unrealMajor = line.split()[2].rstrip()
			if re.match("#define ENGINE_MINOR_VERSION.*", line):
				unrealMinor = line.split()[2].rstrip()
			if re.match("#define ENGINE_PATCH_VERSION.*", line):
				unrealPatch = line.split()[2].rstrip()

		version = unrealMajor + "." + unrealMinor + "." + unrealPatch
		if version != ".":
			sys.stdout.write(" " + version)

	# unreal_modules/*

	if forge.fe_os == 'FE_WIN32' or forge.fe_os == 'FE_WIN64':
		platform = "Win64"
		prefix = ""
		suffix = ".dll"
	else:
		platform = "Linux"
		prefix = "lib"
		suffix = ".so"

	forge.unreal_plugin_targets = {}

	modset_root_list = forge.modset_roots.split(':')
	for modset_root in modset_root_list:
		modset_root_path = os.path.join(forge.rootPath, modset_root)
		if os.path.isdir(modset_root_path):
			dir_list = os.listdir(modset_root_path)
			for modset in dir_list:
				modset_path = os.path.join(modset_root_path, modset)
				if os.path.isdir(modset_path):
					for moduleName in forge.modules_found:
						module_plugins = os.path.join(modset_path, moduleName, "unreal_plugins")
						if os.path.isdir(module_plugins):
							groups_list = os.listdir(module_plugins)
							for group in groups_list:
								module_group = os.path.join(module_plugins, group)
								plugin_list = os.listdir(module_group)
								for plugin in plugin_list:
									src_plugin = os.path.join(module_group, plugin)
									if os.path.isdir(src_plugin):
										src_plugin = os.path.join(module_plugins, src_plugin)
										if not os.path.isdir(src_plugin):
											continue

										build_cs = os.path.join(src_plugin, "Source",plugin, plugin + ".Build.cs")
										build_template = build_cs + ".template"
										if os.path.exists(build_template):
											f = open(build_template,'r')
											template_data = f.read()
											f.close()

											fe_root = forge.rootPath
											fe_lib_platform = forge.libPath
											if forge.fe_os == 'FE_WIN32' or forge.fe_os == 'FE_WIN64':
												fe_root = fe_root.replace("\\","/")
												fe_lib_platform = fe_lib_platform.replace("\\","/")
											template_data = template_data.replace("${FE_ROOT}",fe_root)
											template_data = template_data.replace("${FE_LIB_PLATFORM}",fe_lib_platform)

											py_auto_file = open(build_cs,"w")
											py_auto_file.write(template_data)

										src_package = os.path.join(src_plugin, "Package")

										group_plugin = os.path.join(group, plugin)

										engine_group = os.path.join(confirmed_build, "Engine", "Plugins", group)
										engine_plugin = os.path.join(engine_group, plugin)

										soName = prefix + "UnrealEditor-" + plugin + suffix
#										soName += ".junk"
										src_so = os.path.join(src_package, "Binaries", platform, soName)

										if not os.path.exists(src_package):
											os.makedirs(src_package)

										if not os.path.exists(engine_group):
											os.makedirs(engine_group)

										if os.path.lexists(engine_plugin):
											if os.path.islink(engine_plugin):
												os.remove(engine_plugin)
											else:
												os.rmdir(engine_plugin)
										if not os.path.exists(engine_plugin):
											os.symlink(src_package, engine_plugin)

										if not os.path.isdir(engine_plugin):
											forge.color_on(0, forge.BLUE)
											sys.stdout.write(" -" + plugin)
											forge.color_off()
										else:
											sys.stdout.write(" " + plugin)

											target_exists = False

											soTarget = forge.LookupTarget(src_so)
											if soTarget:
												target_exists = True
											else:
												soTarget = forge.FileTarget(src_so)

											forge.unreal_plugin_targets[plugin] = (soTarget, src_plugin, engine_plugin)

											# trigger on any plugin source change
											src_source = os.path.join(src_plugin, "Source")
											src_pattern = re.compile(r'.*\.(h|cpp|template)$')
											for (dirpath, dirnames, filenames) in os.walk(src_source):
												for filename in filenames:
													fullname = os.path.join(dirpath, filename)
													if src_pattern.match(fullname):
														soTarget.AddDep(forge.FileTarget(fullname))
											# also trigger on the uplugin file peer to Source/
											src_pattern = re.compile(r'.*\.(uplugin)$')
											filenames = os.listdir(src_plugin)
											for filename in filenames:
												fullname = os.path.join(src_plugin, filename)
												if src_pattern.match(fullname):
													soTarget.AddDep(forge.FileTarget(fullname))

											if target_exists:
												continue

											soTarget.AddDep(forge.targetRegistry["lab.unreal.buildfile"])

											soTarget.AddDep(forge.targetRegistry["fexTerminalDLLib"])

											soTarget.AppendOverrideRule(".*", generate_module)

											module.AddDep(soTarget)

def generate_module(target):
	confirmed_build = locate_unreal()

	forge.cprint(forge.WHITE,0,'generating Unreal plugins')

	for plugin in forge.unreal_plugin_targets:
		(soTarget, src_plugin, engine_plugin) = forge.unreal_plugin_targets[plugin]

		forge.color_on(0, forge.BLUE)
		sys.stdout.write("REMOVE " + engine_plugin + "\n")
		forge.color_off()

		if os.path.lexists(engine_plugin):
			if os.path.islink(engine_plugin):
				os.remove(engine_plugin)
			else:
				os.rmdir(engine_plugin)

	sys.stdout.write("\n")

	if forge.fe_os == 'FE_WIN32' or forge.fe_os == 'FE_WIN64':
		platform = "Win64"
	else:
		platform = "Linux"

	for plugin in forge.unreal_plugin_targets:
		(soTarget, src_plugin, engine_plugin) = forge.unreal_plugin_targets[plugin]
		src_package = os.path.join(src_plugin, "Package")

		# NOTE remove symlink to 'pxr' or UAT will wipe the target contents
		src_pxr = confirmed_build + "/Engine/Plugins/Importers/USDImporter/Content/Python/Lib/" + platform + "/site-packages/pxr"
		package_python = src_package + "/Content/Python"
		package_pxr = package_python + "/pxr"

		forge.color_on(0, forge.BLUE)
		sys.stdout.write("UNLINK " + package_pxr + "\n")
		forge.color_off()

		if os.path.lexists(package_pxr):
			os.remove(package_pxr)

		command = confirmed_build + "/Engine/Build/BatchFiles/"
		if forge.fe_os == 'FE_WIN32' or forge.fe_os == 'FE_WIN64':
			command += "RunUAT.bat"
		else:
			command += "RunUAT.sh"
		command += " BuildPlugin"
		command += " -Plugin=" + src_plugin + "/" + plugin + ".uplugin"
		command += " -Package=" + src_package
		command += " -TargetPlatforms=" + platform
		command += " -Verbose"
		command += " -VeryVerbose"
#		command += " -rocket"

		forge.color_on(0, forge.BLUE)
		sys.stdout.write(command + "\n")
		forge.color_off()

		if os.system(command) != 0:
			raise Exception('Unreal BuildPlugin failed on "%s" ' % plugin)
			return

		sys.stdout.write("\n")

	for plugin in forge.unreal_plugin_targets:
		(soTarget, src_plugin, engine_plugin) = forge.unreal_plugin_targets[plugin]
		src_package = os.path.join(src_plugin, "Package")

		forge.color_on(0, forge.BLUE)
		sys.stdout.write("SYMLINK " + src_package + " " + engine_plugin + "\n")
		forge.color_off()

		if not os.path.lexists(engine_plugin):
			os.symlink(src_package, engine_plugin)

		src_pxr = confirmed_build + "/Engine/Plugins/Importers/USDImporter/Content/Python/Lib/" + platform + "/site-packages/pxr"
		package_python = src_package + "/Content/Python"
		package_pxr = package_python + "/pxr"

		forge.color_on(0, forge.BLUE)
		sys.stdout.write("SYMLINK " + src_pxr + " " + package_pxr + "\n")
		forge.color_off()

		os.makedirs(package_python)
		os.symlink(src_pxr, package_pxr)

		if forge.fe_os == 'FE_WIN32' or forge.fe_os == 'FE_WIN64':
			lib_dlls = forge.libPath + "/*.dll"
			lib_yamls = forge.libPath + "/*.yaml"
			package_binaries = src_package + "/Binaries/Win64"

			forge.color_on(0, forge.BLUE)
			sys.stdout.write("COPY " + lib_dlls + " " + package_binaries + "\n")
			forge.color_off()

			utility.copy_files(lib_dlls, package_binaries)

			forge.color_on(0, forge.BLUE)
			sys.stdout.write("COPY " + lib_yamls + " " + package_binaries + "\n")
			forge.color_off()

			utility.copy_files(lib_yamls, package_binaries)

def auto(module):
	confirmed_build = locate_unreal()

	if not confirmed_build:
		return 'not found'

	version_h = confirmed_build + "/Engine/Source/Runtime/Launch/Resources/Version.h"
	if not os.path.exists(version_h):
		return 'missing'

	# NOTE no build test
	return None
