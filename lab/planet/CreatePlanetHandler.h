/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __planet_CreatePlanetHandler_h__
#define __planet_CreatePlanetHandler_h__

#include "field/fieldAS.h"
namespace fe
{
namespace ext
{

class FE_DL_EXPORT CreatePlanetHandler :
	virtual public HandlerI,
	virtual public Config,
	public Initialize<CreatePlanetHandler>
{
	public:
				CreatePlanetHandler(void);
virtual			~CreatePlanetHandler(void);
		void	initialize(void);

				// AS HandlerI
virtual void	handle(Record &r_sig);

	private:

		void	createPlanet(Record r_planet);

		AsPlanet		m_asPlanet;
		AsScalarField	m_asScalarField;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __planet_CreatePlanetHandler_h__ */

