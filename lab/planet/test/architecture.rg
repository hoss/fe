INFO 5
ATTRIBUTE name string
ATTRIBUTE group group

LAYOUT "pipeline"
	name
	group

LAYOUT "component"
	name
	group

RECORD "window_pipeline" "pipeline"
	name "PipelineI.Window"
	group "win_components"

RECORD "debugwindow" "component"
	name "ApplicationWindowI.debug"
	group "windatagroup"

RECORD "appwindow" "component"
	name "ApplicationWindowI.World"
	group "appgroup"

RECORDGROUP "win_components"
	debugwindow
	appwindow

RECORDGROUP 1
	"window_pipeline"

RECORDGROUP "appgroup"

RECORDGROUP "windatagroup"

END


