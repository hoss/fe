options
{
 AA_samples 8
 outputs "RGBA RGBA myfilter mydriver" 
 xres 800
 yres 800
 GI_diffuse_depth 1
}

driver_png
{
 name mydriver
 filename "fur.png"
 gamma 1.5
}

gaussian_filter
{
 name myfilter
}

plane
{
 name myplane
 point 0 0 0
 normal 0 1 0
 shader planeshader
}

procedural
{
 name strand
 dso "libfexArnoldDL.so"
 shader curveshader
 visibility ""

 declare fe_component constant STRING
 fe_component "CurveCreateOp"

 declare length constant FLOAT
 length 0.06

 declare segments constant INT
 segments 8
}

procedural
{
 name torus
 dso "libfexArnoldDL.so"
 shader wood
 visibility ""

 declare fe_component constant STRING
 fe_component "ImportOp"

 declare loadfile constant STRING
 loadfile "torus.geo"
}

procedural
{
 name sphere
 dso "libfexArnoldDL.so"
 visibility ""

 declare fe_component constant STRING
 fe_component "ImportOp"

 declare loadfile constant STRING
 loadfile "sphere.geo"
}

procedural
{
 name collider
 dso "libfexArnoldDL.so"
 shader glass
 opaque 0

 declare fe_component constant STRING
 fe_component "SurfaceNormalOp"

 declare Input_Surface constant NODE
 Input_Surface sphere
}

procedural
{
 name subdivide
 dso "libfexArnoldDL.so"
 shader wood
 visibility ""

 declare fe_component constant STRING
 fe_component "SubdivideOp"

 declare Input_Surface constant NODE
 Input_Surface torus

 declare depth constant INT
 depth 2
}

procedural
{
 name skin
 dso "libfexArnoldDL.so"
 shader wood

 declare fe_component constant STRING
 fe_component "SurfaceNormalOp"

 declare Input_Surface constant NODE
 Input_Surface subdivide
}

procedural
{
 name fur
 dso "libfexArnoldDL.so"
 shader curveshader
 visibility ""

 declare fe_component constant STRING
 fe_component "SurfaceCopyOp"

 declare Input_Surface constant NODE
 Input_Surface strand

 declare Template_Points constant NODE
 Template_Points skin
}

procedural
{
 name dodged
 dso "libfexArnoldDL.so"
 shader curveshader

 declare fe_component constant STRING
 fe_component "DodgeOp"

 declare Projection constant FLOAT
 Projection 0.5

 declare Input_Surface constant NODE
 Input_Surface fur

 declare Collider_Surface constant NODE
 Collider_Surface collider
}

lambert
{
 name curveshader
 Kd_color 1.0 0.0 0.0
}

lambert
{
 name wood
 Kd_color 1.0 0.7 0.0
}

standard
{
 name steel
 Kd_color 0.8 0.9 1.0
 Kd 0.5
 Ks 0.5
}

standard
{
 name mirror
 Kd_color 1 1 1
 Kd 0.3
 Kr 1.0
}

standard
{
 name glass
 Kd 0.1
 Ks 0.2
 Kt 1.0
 Kt_color 0.6 0.8 1.0
#Ksn 1
#IOR 1.5
}

lambert
{
 name planeshader
 Kd_color 0.0 0.5 0.0
}

persp_camera
{
 name mycamera
 focus_distance 1.1
 aperture_size .03
 position 1 1 1
 look_at 0 0 0
}

distant_light
{
 name key
 direction -1 -1 -1
 color 1 1 1
 intensity 3
}
