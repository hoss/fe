/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <arnold/arnold.pmh>

namespace fe
{
namespace ext
{

ArnoldShader::ArnoldShader(void)
{
}

ArnoldShader::~ArnoldShader(void)
{
}

} /* namespace ext */
} /* namespace fe */
