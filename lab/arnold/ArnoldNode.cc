/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <arnold/arnold.pmh>

#define FE_ARN_DEBUG		FALSE
#define FE_ARN_PARAM_DEBUG	FALSE

namespace fe
{
namespace ext
{

ArnoldNode::ArnoldNode(void):
	m_pAtNodeProcedural(NULL),
	m_pAtNode(NULL)
{
	sp<Registry> spRegistry=m_arnoldContext.master()->registry();
	m_arnoldContext.loadLibraries();

	//* TODO put in FE_ARNOLD_LIBS
	spRegistry->manage("fexOperatorDL");
	spRegistry->manage("fexIronWorksDL");
	spRegistry->manage("fexGrassDL");
	spRegistry->manage("fexVegetationDL");
}

ArnoldNode::~ArnoldNode(void)
{
	if(m_pAtNodeProcedural)
	{
		AiNodeSetPtr(m_pAtNodeProcedural,"ArnoldNode",NULL);
	}

	//* TODO delete AiNode??

	clear();
}

void ArnoldNode::initOperator(void)
{
	const fe::String componentName=
			AiNodeGetStr(m_pAtNodeProcedural,"fe_component");
	const fe::String nodeName=AiNodeGetName(m_pAtNodeProcedural);

#if FE_ARN_DEBUG
	feLog(">>>> ArnoldNode::initOperator \"%s\" \"%s\"\n",
			componentName.c_str(),nodeName.c_str());
#endif

	if(AiNodeDeclare(m_pAtNodeProcedural,"ArnoldNode","constant POINTER"))
	{
		AiNodeSetPtr(m_pAtNodeProcedural,"ArnoldNode",this);
	}
}

void ArnoldNode::runOperator(void)
{
	const fe::String componentName=
			AiNodeGetStr(m_pAtNodeProcedural,"fe_component");
	const fe::String nodeName=AiNodeGetName(m_pAtNodeProcedural);

#if FE_ARN_DEBUG
	feLog(">>>> ArnoldNode::runOperator \"%s\" \"%s\"\n",
			componentName.c_str(),nodeName.c_str());
#endif

	sp<Master> spMaster=m_arnoldContext.master();

	m_spOutputAccessible=
			spMaster->registry()->create("*.SurfaceAccessibleCatalog");

	//* TODO only put in master catalog if used by another node

	sp<SurfaceAccessibleCatalog> spSurfaceAccessibleCatalog=
			m_spOutputAccessible;
	if(spSurfaceAccessibleCatalog.isNull())
	{
		return;
	}

	spSurfaceAccessibleCatalog->setCatalog(spMaster->catalog());
	spSurfaceAccessibleCatalog->setKey(nodeName);
	spSurfaceAccessibleCatalog->setPersistent(TRUE);

	sp<Scope> spScope=spMaster->catalog()->catalogComponent(
			"Scope","OperatorScope");

	setupOperator(spScope,componentName,componentName+".arnold");

//	declareParameters();

	sp<Catalog> spCatalog=catalog();

	spCatalog->catalog< sp<Component> >("Output Surface")=m_spOutputAccessible;

	readParameters();

	//* HACK
	const BWORD copyInput=(spCatalog->catalogOrDefault<String>(
			"Output Surface","IO","").empty() ||
			!spCatalog->catalogOrDefault<String>(
			"Output Surface","copy","").empty());

	const I32 frameCount=1;
	for(I32 frameIndex=0;frameIndex<frameCount;frameIndex++)
	{
		if(copyInput)
		{
			sp<SurfaceAccessibleI> spInputAccessible=
					spCatalog->catalog< sp<Component> >("Input Surface");

			if(spInputAccessible.isValid())
			{
				m_spOutputAccessible->copy(spInputAccessible);
			}
		}
		else
		{
			m_spOutputAccessible->clear();
		}

		cook(Real(frameIndex));
	}

	if(AiNodeGetByte(m_pAtNodeProcedural,"visibility"))
	{
		convertOutput();
	}

#if FE_ARN_DEBUG
	feLog(">>>> ArnoldNode::runOperator \"%s\" \"%s\" done\n",
			componentName.c_str(),nodeName.c_str());
#endif
}

void ArnoldNode::convertOutput(void)
{
	const fe::String componentName=
			AiNodeGetStr(m_pAtNodeProcedural,"fe_component");
	const fe::String nodeName=AiNodeGetName(m_pAtNodeProcedural);

#if FE_ARN_DEBUG
	feLog(">>>> ArnoldNode::convertOutput \"%s\" \"%s\"\n",
			componentName.c_str(),nodeName.c_str());
#endif

	sp<SurfaceAccessorI> spVertexAccessor=m_spOutputAccessible->accessor(
			SurfaceAccessibleI::e_primitive,SurfaceAccessibleI::e_vertices,
			SurfaceAccessibleI::e_refuseMissing);
	if(spVertexAccessor.isNull())
	{
		feLog("ArnoldNode::convertOutput"
				" failed to access primitive vertices\n");
		return;
	}

	const U32 primitiveCount=spVertexAccessor->count();
	if(!primitiveCount)
	{
		feLog("ArnoldNode::convertOutput no primitives\n");
		return;
	}

	sp<SurfaceAccessorI> spPropertyAccessor=m_spOutputAccessible->accessor(
			SurfaceAccessibleI::e_primitive,SurfaceAccessibleI::e_properties,
			SurfaceAccessibleI::e_refuseMissing);

	const BWORD isCurves=spPropertyAccessor.isValid()?
			spPropertyAccessor->integer(0,SurfaceAccessibleI::e_openCurve):
			FALSE;

	sp<SurfaceAccessorI> spPointAccessor=m_spOutputAccessible->accessor(
			SurfaceAccessibleI::e_point,SurfaceAccessibleI::e_position,
			SurfaceAccessibleI::e_refuseMissing);
	if(spPointAccessor.isNull())
	{
		feLog("ArnoldNode::convertOutput failed to access point position\n");
		return;
	}

	sp<SurfaceAccessorI> spRadiusAccessor=m_spOutputAccessible->accessor(
			SurfaceAccessibleI::e_point,"radius",
			SurfaceAccessibleI::e_refuseMissing);

	const U32 pointCount=spPointAccessor->count();

	if(isCurves)
	{
#if FE_ARN_DEBUG
		feLog("  creating curves node\n");
#endif

		m_pAtNode=AiNode("curves");

		AtArray* pCountArray=AiArrayAllocate(primitiveCount,1,AI_TYPE_INT);
		AtArray* pPointArray=AiArrayAllocate(pointCount,1,AI_TYPE_POINT);
		AtArray* pRadiusArray=AiArrayAllocate(pointCount,1,AI_TYPE_FLOAT);

		//* TODO one curve per branch

		for(U32 primitiveIndex=0;primitiveIndex<primitiveCount;primitiveIndex++)
		{
			const I32 subCount=spVertexAccessor->subCount(primitiveIndex);
			AiArraySetInt(pCountArray,primitiveIndex,subCount);
		}

		for(U32 pointIndex=0;pointIndex<pointCount;pointIndex++)
		{
			const SpatialVector point=
					spPointAccessor->spatialVector(pointIndex);
			const Real radius=spRadiusAccessor.isValid()?
					spRadiusAccessor->real(pointIndex): 0.005;

			const AtVector atVector={point[0],point[1],point[2]};

			AiArraySetPnt(pPointArray,pointIndex,atVector);
			AiArraySetFlt(pRadiusArray,pointIndex,radius);
		}

		AiNodeSetArray(m_pAtNode,"num_points",pCountArray);
		AiNodeSetArray(m_pAtNode,"points",pPointArray);
		AiNodeSetArray(m_pAtNode,"radius",pRadiusArray);
		AiNodeSetStr(m_pAtNode,"basis","linear");
		AiNodeSetInt(m_pAtNode,"mode",1);	//* 0=ribbon 1=tube
	}
	else
	{
#if FE_ARN_DEBUG
		feLog("  creating polymesh node\n");
#endif

		m_pAtNode=AiNode("polymesh");

		AtArray* pCountArray=AiArrayAllocate(primitiveCount,1,AI_TYPE_INT);
		AtArray* pPointArray=AiArrayAllocate(pointCount,1,AI_TYPE_POINT);

		U32 vertexCount=0;
		for(U32 primitiveIndex=0;primitiveIndex<primitiveCount;primitiveIndex++)
		{
			const I32 subCount=spVertexAccessor->subCount(primitiveIndex);
			AiArraySetInt(pCountArray,primitiveIndex,subCount);

			vertexCount+=subCount;
		}

		AtArray* pVertexArray=AiArrayAllocate(vertexCount,1,AI_TYPE_INT);

		U32 vertexIndex=0;
		for(U32 primitiveIndex=0;primitiveIndex<primitiveCount;primitiveIndex++)
		{
			const U32 subCount=spVertexAccessor->subCount(primitiveIndex);
			AiArraySetInt(pCountArray,primitiveIndex,subCount);

			for(U32 subIndex=0;subIndex<subCount;subIndex++)
			{
				const I32 pointIndex=
						spVertexAccessor->integer(primitiveIndex,subIndex);
				AiArraySetInt(pVertexArray,vertexIndex++,pointIndex);
			}
		}

		for(U32 pointIndex=0;pointIndex<pointCount;pointIndex++)
		{
			const SpatialVector point=
					spPointAccessor->spatialVector(pointIndex);
			const AtVector atVector={point[0],point[1],point[2]};

			AiArraySetPnt(pPointArray,pointIndex,atVector);
		}

		AiNodeSetArray(m_pAtNode,"nsides",pCountArray);
		AiNodeSetArray(m_pAtNode,"vlist",pPointArray);
		AiNodeSetArray(m_pAtNode,"vidxs",pVertexArray);
		AiNodeSetBool(m_pAtNode,"smoothing",true);
	}

#if FE_ARN_DEBUG
	feLog("  pointCount %d primitiveCount %d\n",
			pointCount,primitiveCount);
	feLog(">>>> ArnoldNode::convertOutput \"%s\" \"%s\" done\n",
			componentName.c_str(),nodeName.c_str());
#endif
}

void ArnoldNode::declareParameters(void)
{
	sp<Catalog> spCatalog=catalog();

	Array<String> keys;
	spCatalog->catalogKeys(keys);

	const U32 keyCount=keys.size();

	for(U32 keyIndex=0;keyIndex<keyCount;keyIndex++)
	{
		const String keyname=keys[keyIndex];

		if(keyname=="label" || keyname=="icon" || keyname=="url" ||
				keyname=="precook" || keyname=="summary")
		{
			spCatalog->catalog<bool>(keyname,"internal")=true;
		}
		if(spCatalog->catalogOrDefault<bool>(keyname,"internal",false))
		{
			continue;
		}
		if(spCatalog->catalogOrDefault<bool>(keyname,"hidden",false))
		{
			continue;
		}
		Instance instance;
		if(!spCatalog->catalogLookup(keyname,instance))
		{
			continue;
		}

		if(instance.is< sp<Component> >() &&
				spCatalog->catalogOrDefault<String>(keyname,
				"implementation","")!="RampI")
		{
			continue;
		}

		if(instance.is< Array< sp<Component> > >())
		{
			continue;
		}

#if FE_ARN_PARAM_DEBUG
		feLog("ArnoldNode::declareParameters \"%s\"\n",keyname.c_str());
#endif

		String typeString;

		if(instance.is<String>())
		{
			typeString="STRING";
		}
		else if(instance.is<bool>())
		{
			typeString="BOOL";
		}
		else if(instance.is<Real>())
		{
			typeString="FLOAT";
		}
		else if(instance.is<I32>())
		{
			typeString="INT";
		}
		else if(instance.is<SpatialVector>())
		{
			typeString="POINT";
		}
		else if(instance.is<Vector4>())
		{
		}
		else if(instance.is< sp<Component> >())
		{
			const String implementation=
					spCatalog->catalogOrDefault<String>(keyname,
					"implementation",
					"SurfaceAccessibleI.SurfaceAccessibleCatalog");

			if(implementation=="SurfaceAccessibleI" ||
					implementation==
					"SurfaceAccessibleI.SurfaceAccessibleCatalog")
			{
//				const bool optional=spCatalog->catalogOrDefault<bool>(
//						keyname,"optional",false);
			}
			else if(implementation=="DrawI")
			{
			}
			else if(implementation=="RampI")
			{
			}
		}

		if(!typeString.empty() &&
				!AiNodeLookUpUserParameter(m_pAtNodeProcedural,keyname.c_str()))
		{
#if FE_ARN_PARAM_DEBUG
			const bool declared=
#endif
					AiNodeDeclare(m_pAtNodeProcedural,
					keyname.c_str(),("constant "+typeString).c_str());

#if FE_ARN_PARAM_DEBUG
			feLog("  declared %d \"%s\"\n",declared,typeString.c_str());
#endif
		}
	}
}

void ArnoldNode::readParameters(void)
{
	sp<Catalog> spCatalog=catalog();

	AtUserParamIterator* pParamIt=
			AiNodeGetUserParamIterator(m_pAtNodeProcedural);
	const AtUserParamEntry* pParamEntry;
	while((pParamEntry=AiUserParamIteratorGetNext(pParamIt)))
	{
		const char* paramName=AiUserParamGetName(pParamEntry);
		const int typeConstant=AiUserParamGetType(pParamEntry);

#if FE_ARN_PARAM_DEBUG
		feLog(" param \"%s\" %d\n",paramName,typeConstant);
#endif

		switch(typeConstant)
		{
			case AI_TYPE_STRING:
			{
				const fe::String value=
						AiNodeGetStr(m_pAtNodeProcedural,paramName);
#if FE_ARN_PARAM_DEBUG
				feLog("  string \"%s\"\n",value.c_str());
#endif
				spCatalog->catalog<String>(paramName)=value;
			}
				break;
			case AI_TYPE_BOOLEAN:
			{
				const BWORD value=
						AiNodeGetBool(m_pAtNodeProcedural,paramName);
#if FE_ARN_PARAM_DEBUG
				feLog("  bool %d\n",value);
#endif
				spCatalog->catalog<bool>(paramName)=value;
			}
				break;
			case AI_TYPE_INT:
			{
				const I32 value=
						AiNodeGetInt(m_pAtNodeProcedural,paramName);
#if FE_ARN_PARAM_DEBUG
				feLog("  int %d\n",value);
#endif
				spCatalog->catalog<I32>(paramName)=value;
			}
				break;
			case AI_TYPE_FLOAT:
			{
				const Real value=
						AiNodeGetFlt(m_pAtNodeProcedural,paramName);
#if FE_ARN_PARAM_DEBUG
				feLog("  real %.6G\n",value);
#endif
				spCatalog->catalog<Real>(paramName)=value;
			}
				break;
			case AI_TYPE_VECTOR:
			{
				const AtVector value=
						AiNodeGetVec(m_pAtNodeProcedural,paramName);
				const SpatialVector point(value[0],value[1],value[2]);
#if FE_ARN_PARAM_DEBUG
				feLog("  vector \"%s\"\n",c_print(point));
#endif
				spCatalog->catalog<SpatialVector>(paramName)=point;
			}
				break;
			case AI_TYPE_POINT:
			{
				const AtPoint value=
						AiNodeGetPnt(m_pAtNodeProcedural,paramName);
				const SpatialVector point(value[0],value[1],value[2]);
#if FE_ARN_PARAM_DEBUG
				feLog("  point \"%s\"\n",c_print(point));
#endif
				spCatalog->catalog<SpatialVector>(paramName)=point;
			}
				break;
			case AI_TYPE_RGB:
			{
				const AtRGB value=
						AiNodeGetRGB(m_pAtNodeProcedural,paramName);
				const SpatialVector point(value[0],value[1],value[2]);
#if FE_ARN_PARAM_DEBUG
				feLog("  rgb \"%s\"\n",c_print(point));
#endif
				spCatalog->catalog<SpatialVector>(paramName)=point;
			}
				break;
			case AI_TYPE_NODE:
			{
				AtNode *pAtNodeOther=
						(AtNode*)AiNodeGetPtr(m_pAtNodeProcedural,paramName);

				const String otherName=AiNodeGetName(pAtNodeOther);
#if FE_ARN_PARAM_DEBUG
				feLog("  node %p \"%s\"\n",pAtNodeOther,otherName.c_str());
#endif

				if(!otherName.empty() && strcmp(paramName,"ArnoldNode"))
				{
//					ArnoldNode* pArnoldNode=(ArnoldNode*)
//							AiProceduralGetPluginData(pAtNodeOther);
					ArnoldNode *pArnoldNode=(ArnoldNode*)AiNodeGetPtr(
							pAtNodeOther,"ArnoldNode");
#if FE_ARN_PARAM_DEBUG
					feLog("  ArnoldNode %p\n",pArnoldNode);
#endif

					if(pArnoldNode && pArnoldNode->atNode()==NULL)
					{
#if FE_ARN_PARAM_DEBUG
						feLog("  running input node\n");
#endif
						pArnoldNode->runOperator();
					}

					sp<Master> spMaster=m_arnoldContext.master();

					sp<SurfaceAccessibleCatalog> spSurfaceAccessibleCatalog=
							spMaster->registry()->create(
							"*.SurfaceAccessibleCatalog");
					if(spSurfaceAccessibleCatalog.isValid())
					{

						spSurfaceAccessibleCatalog->setCatalog(
								spMaster->catalog());
						spSurfaceAccessibleCatalog->setKey(otherName);
						spSurfaceAccessibleCatalog->setPersistent(TRUE);

						//* HACK
						const String keyname=
								String(paramName).replace("_"," ");

						spCatalog->catalog< sp<Component> >(keyname)=
								spSurfaceAccessibleCatalog;
					}
				}
			}
				break;
			default:
#if FE_ARN_PARAM_DEBUG
				feLog("  not supported\n");
#endif
				break;
		}
	}
}

} /* namespace ext */
} /* namespace fe */
