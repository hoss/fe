/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __arnold_ShaderVariablesArnold_h__
#define __arnold_ShaderVariablesArnold_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Shader Variable Access

	@ingroup arnold
*//***************************************************************************/
class FE_DL_EXPORT ShaderVariablesArnold:
	virtual public ShaderVariablesI
{
	public:
						ShaderVariablesArnold(void):
							m_pAtNode(NULL),
							m_pAtShaderGlobals(NULL)						{}
virtual					~ShaderVariablesArnold(void)						{}

						//* as ShaderVariablesI
virtual	String			string(I32 a_channel) const
						{
							if(a_channel<paramOffset())
							{
								return (char*)((char*)&m_pAtShaderGlobals->x+
										a_channel);
							}
							const String value=AiShaderEvalParamFuncStr(
									m_pAtShaderGlobals,m_pAtNode,
									a_channel-paramOffset());
							return value;
						}

virtual	I32				integer(I32 a_channel) const
						{
							if(a_channel<paramOffset())
							{
								return I32(*(I32*)
										((char*)&m_pAtShaderGlobals->x+
										a_channel));
							}
							const I32 value=AiShaderEvalParamFuncInt(
									m_pAtShaderGlobals,m_pAtNode,
									a_channel-paramOffset());
							return value;
						}

virtual	Real			real(I32 a_channel) const
						{
							if(a_channel<paramOffset())
							{
								return Real(*(float*)
										((char*)&m_pAtShaderGlobals->x+
										a_channel));
							}
							const Real value=AiShaderEvalParamFuncFlt(
									m_pAtShaderGlobals,m_pAtNode,
									a_channel-paramOffset());
							return value;
						}

virtual	SpatialVector	spatialVector(I32 a_channel) const
						{
							if(a_channel<paramOffset())
							{
								return SpatialVector(Vector3f((float*)
										((char*)&m_pAtShaderGlobals->x+
										a_channel)));
							}
							const AtVector atVector=AiShaderEvalParamFuncVec(
									m_pAtShaderGlobals,m_pAtNode,
									a_channel-paramOffset());
							return SpatialVector(atVector[0],
									atVector[1],atVector[2]);
						}

virtual	Color			color(I32 a_channel) const
						{
							if(a_channel<paramOffset())
							{
								return Color(Vector3f((float*)
										((char*)&m_pAtShaderGlobals->x+
										a_channel)));
							}
							const AtRGBA atRGBA=AiShaderEvalParamFuncRGBA(
									m_pAtShaderGlobals,m_pAtNode,
									a_channel-paramOffset());
							return Color(atRGBA[0],atRGBA[1],
									atRGBA[2],atRGBA[3]);
						}

virtual	void			set(String a_string)
						{
							m_pAtShaderGlobals->out.STR=a_string.c_str();
						}

virtual	void			set(I32 a_integer)
						{
							m_pAtShaderGlobals->out.INT=a_integer;
						}

virtual	void			set(Real a_real)
						{
							m_pAtShaderGlobals->out.FLT=a_real;
						}

virtual	void			set(SpatialVector a_spatialVector)
						{
							AtVector& rAtVector=m_pAtShaderGlobals->out.VEC;
							rAtVector.x=a_spatialVector[0];
							rAtVector.y=a_spatialVector[1];
							rAtVector.z=a_spatialVector[2];
						}

virtual	void			set(Color a_color)
						{
							AtRGBA& rAtRGBA=m_pAtShaderGlobals->out.RGBA;
							rAtRGBA.r=a_color[0];
							rAtRGBA.g=a_color[1];
							rAtRGBA.b=a_color[2];
							rAtRGBA.a=a_color[3];
						}

static	I32				variableOffset(String a_variable);
static	I32				paramOffset(void)	{ return 1000; }

						//* as ShaderVariablesArnold
		void			setAtNode(AtNode* a_pAtNode)
						{	m_pAtNode=a_pAtNode; }

		void			setAtShaderGlobals(AtShaderGlobals* a_pAtShaderGlobals)
						{	m_pAtShaderGlobals=a_pAtShaderGlobals; }

	private:

		AtNode*				m_pAtNode;
		AtShaderGlobals*	m_pAtShaderGlobals;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __arnold_ShaderVariablesArnold_h__ */
