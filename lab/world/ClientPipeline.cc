/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <world/world.pmh>

#include "spatial/spatial.h"
#include "surface/surface.h"
#include "network/network.h"
#include "ClientPipeline.h"

namespace fe
{
namespace ext
{

class SetDataset:
	virtual public HandlerI,
	virtual public Config
{
	public:
		SetDataset(sp<RecordGroup> &rg_dataset) : m_rg_dataset(rg_dataset){}
virtual	~SetDataset(void){}
virtual	void	handle(Record &r_sig)
		{
			if(!m_rg_dataset.isValid())
			{
				return;
			}

			m_rg_dataset->clear();
			if(m_aGroup(r_sig).isValid())
			{
				m_rg_dataset->add(m_aGroup(r_sig));
			}
		}
virtual	void	handleBind(sp<SignalerI> spSignalerI,
						sp<Layout> spLayout)
		{
			m_aGroup.setup(spLayout->scope(), "group");
		}
	private:
		Accessor< sp<RecordGroup> > m_aGroup;
		sp<RecordGroup>				&m_rg_dataset;
};

ClientPipeline::ClientPipeline(void)
{
}

ClientPipeline::~ClientPipeline(void)
{
}

void ClientPipeline::initialize(void)
{
}

void ClientPipeline::attach(	sp<ApplicationI> a_application, sp<SignalerI> a_signaler, sp<SequencerI> a_sequencer, sp<Layout> l_hb, sp<RecordGroup> a_rg_input)
{
	m_spSignaler = a_signaler;

	sp<Config> spConfig;

	sp<Scope> spSimScope(registry()->create("Scope"));
	spSimScope->setName("WorldPipelineScope");

	registry()->manage("fexSpatialDL");
	registry()->manage("fexSurfaceDL");
	registry()->manage("fexNetSignalDL");

	// Simulation Data Set
	sp<RecordGroup> &rg_dataset =
		registry()->master()->catalog()->catalog< sp<RecordGroup> >("WorldDataSet");
	if(!rg_dataset.isValid())
	{
		rg_dataset = new RecordGroup();
	}
	// Collisions
	sp<RecordGroup> &rg_collisions =
		registry()->master()->catalog()->catalog< sp<RecordGroup> >("Collisions");
	if(!rg_collisions.isValid())
	{
		rg_collisions = new RecordGroup();
	}

	// heartbeat
	l_hb = spSimScope->declare("l_hb");
#ifdef FE_LOCK_SUPPRESSION
	l_hb->suppressLock(true);
#endif

#if 0
	sp<Component> spWorldClientHandler = create("HandlerI.WorldClient");
	spWorldClientHandler->adjoin(spSimScope);
	a_signaler->insert(spWorldClientHandler, l_hb);
	spConfig = spWorldClientHandler;
	spConfig->cfg< sp<RecordGroup> >("input") = rg_dataset;
#endif

	l_data = spSimScope->declare("l_data");
	//Accessor<int> ttl(spSimScope, FE_USE(":TTL"));
	Accessor< sp<RecordGroup> > group(spSimScope, "group");
	//l_data->populate(ttl);
	l_data->populate(group);

	sp<ClientI>	spClient(create("ClientI"));
	spClient->adjoin(spSimScope);
	//spClient->addLayout(l_data);
	spClient->setScope(spSimScope);

	int port = atoi(cfg<String>(FE_CAT("port")).c_str());
	String host = cfg<String>(FE_CAT("host"));
	spClient->start(m_spSignaler, host, port, l_hb);

	sp<HandlerI> spSetDataset(new SetDataset(rg_dataset));
	spSetDataset->adjoin(spSimScope);
	spSetDataset->setName("set_dataset");
	m_spSignaler->insert(spSetDataset, l_data);


	AsSequenceSignal asSequenceSignal;
	asSequenceSignal.bind(spSimScope);

#ifdef FE_LOCK_SUPPRESSION
	l_hb->suppressLock(false);
#endif
	Record r_hb = a_sequencer->add(l_hb,
		a_sequencer->getCurrentTime(), 1);
	asSequenceSignal.count(r_hb) = 0;
	asSequenceSignal.periodic(r_hb) = 2;
	asSequenceSignal.interval(r_hb) = 10;
	if(asSequenceSignal.interval(r_hb) <= 0)
	{
		asSequenceSignal.interval(r_hb) = 1;
	}

}


} /* namespace ext */
} /* namespace fe */

