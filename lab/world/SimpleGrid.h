/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __world_SimpleGrid_h__
#define __world_SimpleGrid_h__

#include "signal/signal.h"
#include "draw/draw.h"
#include "datatool/datatool.h"
namespace fe
{
namespace ext
{

/**	draw a 20x20 grid

	@copydoc SimpleGrid_info
	*/
class SimpleGrid :
	virtual public Config,
	virtual public HandlerI
{
	public:
				SimpleGrid(void);
virtual			~SimpleGrid(void);
virtual	void	handle(fe::Record &r_sig);

	private:
		AsSignal		m_asSignal;
		AsWindata		m_asWindata;
		SpatialVector	m_thin[80];
		SpatialVector	m_thick[8];
		SpatialVector	m_x[2];
		SpatialVector	m_y[2];
		SpatialVector	m_z[2];
		SpatialVector	m_x_thin[80];
		SpatialVector	m_x_thick[8];
		SpatialVector	m_x_x[2];
		SpatialVector	m_x_y[2];
		SpatialVector	m_x_z[2];
		sp<DrawMode>	m_spThinMode;
		sp<DrawMode>	m_spThickMode;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __world_SimpleGrid_h__ */

