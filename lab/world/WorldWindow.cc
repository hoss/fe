/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <world/world.pmh>

#include <viewer/viewer.h>
#include <dataui/datauiAS.h>

namespace fe
{
namespace ext
{

WorldWindow::WorldWindow(void)
{
}

WorldWindow::~WorldWindow(void)
{
}

void WorldWindow::initialize(void)
{
	cfg<String>("title") = "untitled";
}

void WorldWindow::populateDataLayout(sp<Layout> l_win_data)
{
	m_asOrtho.populate(l_win_data);
	m_asPerspective.populate(l_win_data);
	m_asSelection.populate(l_win_data);
	m_asSelect.populate(l_win_data);
	m_asPick.populate(l_win_data);
}

void WorldWindow::makeWindow(sp<WindowI> a_window, Record r_win_data, sp<Layout> l_event, sp<Layout> l_draw_hb, sp<RecordGroup> rg_info)
{
	registry()->manage("fexDatauiDL");
	registry()->manage("fexViewerDL");
	registry()->manage("fexFieldDL");
	registry()->manage("fexMechanicsDL");
	registry()->manage("fexSurfaceDL");

	a_window->setTitle(cfg<String>("title"));

	// Simulation Data Set
	sp<RecordGroup> &rg_dataset =
			registry()->master()->catalog()->catalog< sp<RecordGroup> >(
			"WorldDataSet");
	if(!rg_dataset.isValid())
	{
		rg_dataset = new RecordGroup();
	}

	// Collisions
	sp<RecordGroup> &rg_collisions =
			registry()->master()->catalog()->catalog< sp<RecordGroup> >(
			"Collisions");
	if(!rg_collisions.isValid())
	{
		rg_collisions = new RecordGroup();
	}


	// selection set
	sp<RecordGroup> &rg_selected = m_asSelect.selected(r_win_data);
	if(!rg_selected.isValid())
	{
		rg_selected = new RecordGroup();
	}


	// current marked set (during selection itself)
	sp<RecordGroup> &rg_marked = m_asSelect.marked(r_win_data);
	if(!rg_marked.isValid())
	{
		rg_marked = new RecordGroup();
	}

	sp<Config> spConfig;

	sp<SignalerI> spSignaler = feCast(SignalerI, a_window);


	// ******************************************************************
	// Controllers
	sp<Component> spChannelController = create("HandlerI.ChannelController");
	spChannelController->adjoin(a_window);
	spSignaler->insert(spChannelController, l_event);

	sp<Component> spWinController = create("HandlerI.WindowController");
	spWinController->adjoin(a_window);
	spSignaler->insert(spWinController, l_event);

	sp<Component> spSelController = create("HandlerI.SelectController");
	spSelController->adjoin(a_window);
	spSignaler->insert(spSelController, l_event);
	spConfig = spSelController;
	spConfig->cfg< sp<RecordGroup> >("selectfrom") = rg_dataset;

	sp<Component> spController = create("CameraControllerI.InspectController");
	spController->adjoin(a_window);
	spSignaler->insert(spController, l_event);


	// ******************************************************************
	// ortho
	sp<Component> spOrtho = create("ViewerI.OrthoViewer");
	spOrtho->adjoin(a_window);
	spSignaler->insert(spOrtho, l_draw_hb);
	feCast(ViewerI, spOrtho)->bind(a_window);
	m_asOrtho.zoom(r_win_data) = 10.0;
	m_asOrtho.center(r_win_data) = Vector2(200.0,200.0);


	sp<Component> spGrid = create("HandlerI.SimpleGrid");
	spGrid->adjoin(a_window);
	//spSignaler->insert(spGrid, l_draw_hb);

	sp<Component> spDrawAtoms = create("HandlerI.DrawAtoms");
	spDrawAtoms->adjoin(a_window);
	//spSignaler->insert(spDrawAtoms, l_draw_hb);
	spConfig = spDrawAtoms;
	spConfig->cfg< sp<RecordGroup> >("input") = rg_dataset;
	spConfig->cfg< sp<RecordGroup> >("selected") = rg_selected;
	spConfig->cfg< sp<RecordGroup> >("marked") = rg_marked;
	spConfig->cfg< String >("mode") = "circle";


	// ******************************************************************
	// perspective
	sp<Component> spPersp = create("ViewerI.PerspectiveViewer");
	spPersp->adjoin(a_window);
	spSignaler->insert(spPersp, l_draw_hb);
	feCast(ViewerI, spPersp)->bind(a_window);

	sp<Component> spObjectToScreen = create("HandlerI.ProjectPoint");
	spObjectToScreen->adjoin(a_window);
	spSignaler->insert(spObjectToScreen, l_draw_hb);
	spConfig = spObjectToScreen;
	spConfig->cfg< sp<RecordGroup> >("input") = rg_dataset;

	sp<Component> spPGrid = create("HandlerI.SimpleGrid");
	spPGrid->adjoin(a_window);
	spSignaler->insert(spPGrid, l_draw_hb);
	spConfig = spPGrid;
	spConfig->cfg< SpatialVector >("offset") = SpatialVector(0.0,0.0,0.0);

	sp<Component> spDrawScape = create("HandlerI.DrawScalarField");
	spDrawScape->adjoin(a_window);
	spSignaler->insert(spDrawScape, l_draw_hb);
	spConfig = spDrawScape;
	spConfig->cfg< sp<RecordGroup> >("input") = rg_dataset;

	spDrawAtoms = create("HandlerI.DrawAtoms");
	spDrawAtoms->adjoin(a_window);
	//spSignaler->insert(spDrawAtoms, l_draw_hb);
	spConfig = spDrawAtoms;
	spConfig->cfg< sp<RecordGroup> >("input") = rg_dataset;
	spConfig->cfg< sp<RecordGroup> >("selected") = rg_selected;
	spConfig->cfg< sp<RecordGroup> >("marked") = rg_marked;
	spConfig->cfg< String >("mode") = "sphere";

	spDrawAtoms = create("HandlerI.DrawAtoms");
	spDrawAtoms->adjoin(a_window);
	spSignaler->insert(spDrawAtoms, l_draw_hb);
	spConfig = spDrawAtoms;
	spConfig->cfg< sp<RecordGroup> >("input") = rg_dataset;
	spConfig->cfg< sp<RecordGroup> >("selected") = rg_selected;
	spConfig->cfg< sp<RecordGroup> >("marked") = rg_marked;
	spConfig->cfg< String >("mode") = "sphere";

	sp<Component> spDrawPairs = create("HandlerI.DrawPairs");
	spDrawPairs->adjoin(a_window);
	spSignaler->insert(spDrawPairs, l_draw_hb);
	spConfig = spDrawPairs;
	//spConfig->cfg< sp<RecordGroup> >("input") = rg_collisions;
	spConfig->cfg< sp<RecordGroup> >("input") = rg_dataset;

	sp<Component> spDrawMechanics = create("HandlerI.DrawMechanics");
	spDrawMechanics->adjoin(a_window);
	spSignaler->insert(spDrawMechanics, l_draw_hb);
	spConfig = spDrawMechanics;
	spConfig->cfg< sp<RecordGroup> >("input") = rg_dataset;

	sp<Component> spDrawTransform = create("HandlerI.DrawTransform");
	spDrawTransform->adjoin(a_window);
	spSignaler->insert(spDrawTransform, l_draw_hb);
	spConfig = spDrawTransform;
	spConfig->cfg< sp<RecordGroup> >("input") = rg_dataset;

#if 0
	// draw perspective plugin point
	for(RecordGroup::iterator i_rg = rg_info->begin();
		i_rg != rg_info->end(); i_rg++)
	{
		sp<RecordArray> spRA(*i_rg);
		AsPlugin asPlugin;
		asPlugin.bind(spRA->layout()->scope());
		if(asPlugin.check(spRA))
		{
			for(int i = 0; i < spRA->length(); i++)
			{
				if(asPlugin.plug(spRA,i) == "perspective_draw")
				{
					sp<WindowHookI> spHook = feCast(WindowHookI,
						create(asPlugin.name(spRA,i)));
					spHook->hook(a_window, r_win_data, l_event,
						l_draw_hb);
				}
			}
		}
	}
#endif



	// ******************************************************************
	// UI overlay
	sp<Component> spUI = create("ViewerI.OrthoViewer");
	spUI->adjoin(a_window);
	spSignaler->insert(spUI, l_draw_hb);
	feCast(ViewerI, spUI)->bind(a_window);
	spConfig = spUI;
	spConfig->cfg<Real>("zoom") = 1.0;
	spConfig->cfg<Vector2>("center") = Vector2(0.0,0.0);

	sp<Component> spUGrid = create("HandlerI.SimpleGrid");
	spUGrid->adjoin(a_window);
	spSignaler->insert(spUGrid, l_draw_hb);

	sp<Component> spDrawSel = create("HandlerI.DrawSelection");
	spDrawSel->adjoin(a_window);
	spSignaler->insert(spDrawSel, l_draw_hb);
	spSignaler->insert(spDrawSel, l_event);

	// reinsert perspective so that select controller is in right space for picking
	spSignaler->insert(spPersp, l_draw_hb);


	// ******************************************************************
	// Masks


	// any MaskI will do
	sp<MaskI> spMask = feCast(MaskI, spSelController);


	for(RecordGroup::iterator i_rg = rg_info->begin();
		i_rg != rg_info->end(); i_rg++)
	{
		sp<RecordArray> spRA(*i_rg);
		AsBinding asBinding;
		asBinding.bind(spRA->layout()->scope());
		if(asBinding.check(spRA))
		{
			for(int i = 0; i < spRA->length(); i++)
			{
				spMask->maskSet(asBinding.name(spRA,i),
					WindowEvent::stringToMask(asBinding.event(spRA,i)));
			}
		}
	}



#define WIRE(reactor, reaction, signaler, maskname)							\
	{																		\
		Record r_mask = spMask->maskCreate(l_event, maskname);				\
		sp<ReactorI>(reactor)->add(reaction, signaler, r_mask);				\
	}

	WIRE(spController,		"on_inspect",		a_window,	"fe_select_disable");
	WIRE(spController,		"off_inspect",		a_window,	"fe_select_enable");
	WIRE(spSelController,	"on_selecting",		a_window,	"fe_drawselection_enable");
	WIRE(spSelController,	"off_selecting",	a_window,	"fe_drawselection_disable");


}

} /* namespace ext */
} /* namespace fe */

