/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __world_DrawAtoms_h__
#define __world_DrawAtoms_h__

#include "signal/signal.h"
#include "draw/draw.h"
#include "window/window.h"
#include "shape/shape.h"
#include "viewer/DrawView.h"
#include "mechanics/mechanicsAS.h"
namespace fe
{
namespace ext
{

/**	Draw atoms/particles

	@copydoc DrawAtoms_info__
	*/
class FE_DL_EXPORT DrawAtoms :
		public Initialize<DrawAtoms>,
		virtual public HandlerI,
		virtual public Dispatch,
		virtual public Config
{
	public:
				DrawAtoms(void);
virtual			~DrawAtoms(void);

		void	initialize(void);

				// AS HandlerI
virtual void	handle(	Record &record);

				// AS DispatchI
virtual	bool	call(const String &a_name, std::vector<Instance> a_argv);

		typedef enum
		{
			e_none			= 0,
			e_point			= 1,
			e_circle		= 2,
			e_sphere		= 3
		} t_mode;

		void	setMode(t_mode a_mode);

	private:
		AsParticle							m_asParticle;
		AsLabeled							m_asLabeled;
		AsBounded							m_asBounded;
		AsSelectable						m_asSelectable;
		AsColored							m_asColored;
		DrawView							m_drawview;
		t_mode								m_mode;

};

} /* namespace ext */
} /* namespace fe */

#endif /* __world_DrawAtoms_h__ */
