/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <world/world.pmh>

#include "solve/solve.h"
#include "spatial/spatial.h"
#include "mechanics/mechanics.h"
#include "surface/surface.h"
#include "network/network.h"
#include "lua/lua.h"

#include "spatial/Flatten.h"
#include "spatial/ProxMultiGrid.h"

namespace fe
{
namespace ext
{

class SendDataset:
	virtual public HandlerI,
	virtual public Config
{
	public:
		SendDataset(sp<RecordGroup> &rg_dataset, sp<Layout> a_l_data) : m_rg_dataset(rg_dataset)
		{
			l_data = a_l_data;
		}
virtual	~SendDataset(void){}
virtual	void	handle(Record &r_sig)
		{
			if(!m_rg_dataset.isValid())
			{
				return;
			}

			if(m_rg_dataset.isValid())
			{
				Record r_data = l_data->createRecord();
				m_aGroup(r_data) = new RecordGroup();
				m_aGroup(r_data)->add(m_rg_dataset);
				m_signaler->signal(r_data);
			}
		}
virtual	void	handleBind(sp<SignalerI> spSignalerI,
						sp<Layout> spLayout)
		{
			m_aGroup.setup(spLayout->scope(), "group");
			m_signaler = spSignalerI;
		}
	private:
		Accessor< sp<RecordGroup> > m_aGroup;
		sp<RecordGroup>				&m_rg_dataset;
		// TODO: AJW: de-SB this
		sp<LayoutDefault>			l_data;
		sp<SignalerI>				m_signaler;
};

class SimCountControl : virtual public HandlerI
{
	public:
		SimCountControl(Record &a_r_sim_hb, int &run_level)
			: m_run_level(run_level)
		{
			r_sim_hb = a_r_sim_hb;
		}
virtual	~SimCountControl(void) {}

virtual void	handle(Record &r_sig)
		{
			m_asSeqSig.bind(r_sim_hb.layout()->scope());
			if(m_run_level == 2)
			{
				m_asSeqSig.count(r_sim_hb) = -1;
			}
			else
			{
				m_asSeqSig.count(r_sim_hb) = 0;
			}
		}

	private:
		AsSequenceSignal	m_asSeqSig;
		Record				r_sim_hb;
		int					&m_run_level;
};

class ClearCollisions : virtual public HandlerI
{
	public:
		ClearCollisions(sp<RecordGroup> rg_collisions)
		{
			m_collisions = rg_collisions;
		}
virtual	~ClearCollisions(void)
		{
		}
virtual void	handle(Record &r_sig)
				{
					m_collisions->clear();
				}

	private:
		sp<RecordGroup>	m_collisions;
};

WorldPipeline::WorldPipeline(void)
{
}

WorldPipeline::~WorldPipeline(void)
{
}

void WorldPipeline::initialize(void)
{
}

void WorldPipeline::attach(	sp<ApplicationI> a_application, sp<SignalerI> a_signaler, sp<SequencerI> a_sequencer, sp<Layout> l_hb, sp<RecordGroup> a_rg_input)
{
	m_spSignaler = a_signaler;

	sp<Config> spConfig;

	sp<Scope> spSimScope(registry()->create("Scope"));
	spSimScope->setName("WorldPipelineScope");

	registry()->manage("fexSolveDL");
	registry()->manage("fexSpatialDL");
	registry()->manage("fexMechanicsDL");
	registry()->manage("fexODEDL");
	registry()->manage("fexSurfaceDL");
	registry()->manage("fexPlanetDL");
	registry()->manage("fexNetSignalDL");



	// Simulation Data Set
	sp<RecordGroup> &rg_dataset =
		registry()->master()->catalog()->catalog< sp<RecordGroup> >(FE_CAT_SPEC("WorldDataSet", "dataset RG for simulation"));
	if(!rg_dataset.isValid())
	{
		rg_dataset = new RecordGroup();
	}
	// Collisions
	sp<RecordGroup> &rg_collisions =
		registry()->master()->catalog()->catalog< sp<RecordGroup> >(FE_CAT_SPEC("Collisions", "collision RG"));
	if(!rg_collisions.isValid())
	{
		rg_collisions = new RecordGroup();
	}


	// simulation heartbeat
	l_sim_hb = spSimScope->declare("l_sim_hb");
#ifdef FE_LOCK_SUPPRESSION
	l_sim_hb->suppressLock(true);
#endif

	// add timestep to sim signal
	AsTemporal asTemporal;
	asTemporal.bind(spSimScope);
	asTemporal.populate(l_sim_hb);


#if 0
	// Testing the pipeline
	sp<LuaI> spLua = feCast(LuaI,  registry()->create("LuaI"));
	spLua->adjoin(a_signaler);
	spLua->loadString("io.write('World Simulation Pipeline\\n') ");
	a_signaler->insert(spLua, l_sim_hb);
#endif


	sp<Layout> l_prox = spSimScope->declare("l_prox");
	AsProximity asProx;
	asProx.bind(spSimScope);
	asProx.populate(l_prox);

	// init signal
	l_init = spSimScope->declare("l_init");
	AsInit asInit;
	asInit.bind(spSimScope);
	asInit.populate(l_init);
#ifdef FE_LOCK_SUPPRESSION
	l_init->suppressLock(true);
#endif

	sp<Component> spMultiWriter = create("HandlerI.MultiGroupWriter");
	spMultiWriter->adjoin(a_signaler);
	a_signaler->insert(spMultiWriter, l_sim_hb);
	spConfig = spMultiWriter;
	spConfig->cfg< sp<RecordGroup> >("input") = rg_dataset;
	spConfig->configParent(sp<ConfigI>(this));


	sp<Component> spChannelBinder = create("HandlerI.ChannelBinder");
	spChannelBinder->adjoin(a_signaler);
	spConfig = spChannelBinder;
	spConfig->cfg< sp<RecordGroup> >("input") = rg_dataset;
	a_signaler->insert(spChannelBinder, l_init);
	a_signaler->insert(spChannelBinder, l_sim_hb);

	// clear collisions
	sp<HandlerI> spClearCollisions(new ClearCollisions(rg_collisions));
	spClearCollisions->adjoin(a_signaler);
	a_signaler->insert(spClearCollisions, l_sim_hb);

	sp<Component> spProximity = create("HandlerI.Proximity");
	spProximity->adjoin(spSimScope);
	a_signaler->insert(spProximity, l_sim_hb);
	spConfig = spProximity;
	spConfig->cfg< sp<RecordGroup> >("input") = rg_dataset;
	spConfig->cfg< sp<RecordGroup> >("output") = rg_collisions;

	sp<ProxI> spSweep = create("ProxI.Sweep");
	spSweep->adjoin(spProximity);

	sp<ProxMultiGrid> spPrxMG = create("ProxI.MultiGrid");
	spPrxMG->addGrid(2.0f);
	spPrxMG->adjoin(spProximity);

	sp<ProxI> spHash = create("ProxI.Hash");
	spHash->adjoin(spProximity);

	spConfig->cfg< sp<Component> >("prox") = spPrxMG;
	spConfig->cfg< sp<Layout> >("proximitylayout") = l_prox;

	// TODO: maybe hang ChannelFilter off of window pipeline instead (hacked in here for now)
	sp<Component> spChannelFilter = create("HandlerI.ChannelFilter");
	spChannelFilter->adjoin(spSimScope);
	a_signaler->insert(spChannelFilter, l_sim_hb);
	spConfig = spChannelFilter;
	spConfig->cfg< sp<RecordGroup> >("input") = rg_dataset;

	sp<Component> spBary = create("HandlerI.BarycentricPoint");
	spBary->adjoin(spSimScope);
	a_signaler->insert(spBary, l_sim_hb);
	spConfig = spBary;
	spConfig->cfg< sp<RecordGroup> >("input") = rg_dataset;

	sp<Component> spLocatedFrame = create("HandlerI.LocatedFrame");
	spLocatedFrame->adjoin(spSimScope);
	a_signaler->insert(spLocatedFrame, l_init);
	a_signaler->insert(spLocatedFrame, l_sim_hb);
	spConfig = spLocatedFrame;
	spConfig->cfg< sp<RecordGroup> >("input") = rg_dataset;

	sp<Component> spFrameConstraint = create("HandlerI.FrameConstraint");
	spFrameConstraint->adjoin(spSimScope);
	a_signaler->insert(spFrameConstraint, l_sim_hb);
	spConfig = spFrameConstraint;
	spConfig->cfg< sp<RecordGroup> >("input") = rg_dataset;

	sp<Component> spEulerFrame = create("HandlerI.EulerFrame");
	spEulerFrame->adjoin(spSimScope);
	a_signaler->insert(spEulerFrame, l_init);
	spConfig = spEulerFrame;
	spConfig->cfg< sp<RecordGroup> >("input") = rg_dataset;


	sp<Layout> l_accumulate = spSimScope->declare("l_accumulate");
	AsAccumulate asAccumulate;
	asAccumulate.bind(spSimScope);
	asAccumulate.populate(l_accumulate);
	Record r_accumulate = spSimScope->createRecord(l_accumulate);
	asAccumulate.dataset(r_accumulate) = rg_dataset;

	sp<Layout> l_clear = spSimScope->declare("l_clear");
	AsClear asClear;
	asClear.bind(spSimScope);
	asClear.populate(l_clear);
	Record r_clear = spSimScope->createRecord(l_clear);
	asClear.dataset(r_clear) = rg_dataset;

	sp<Layout> l_validate = spSimScope->declare("l_validate");
	AsValidate asValidate;
	asValidate.bind(spSimScope);
	asValidate.populate(l_validate);
	Record r_validate = spSimScope->createRecord(l_validate);
	asValidate.dataset(r_validate) = rg_dataset;

	sp<Layout> l_update = spSimScope->declare("l_update");
	AsUpdate asUpdate;
	asUpdate.bind(spSimScope);
	asUpdate.populate(l_update);
	Record r_update = spSimScope->createRecord(l_update);
	asUpdate.dataset(r_update) = rg_dataset;


//#define LINEARSPRINGONLY

//#define SPH
#ifdef SPH
	sp<Component> spSolver = create("HandlerI.ExplicitInertial");
#else
	//sp<Component> spSolver = create("HandlerI.ExplicitInertial");
	sp<Component> spSolver = create("HandlerI.SemiImplicitInertial");
#endif
	spSolver->adjoin(a_signaler);
	spConfig = spSolver;
	spConfig->cfg< sp<RecordGroup> >("input") = rg_dataset;
	spConfig->cfg< Record >("update") = r_update;
	spConfig->cfg< Record >("clear") = r_clear;
	spConfig->cfg< Record >("accumulate") = r_accumulate;
	spConfig->cfg< Record >("validate") = r_validate;
	a_signaler->insert(spSolver, l_sim_hb);

#if 0
	sp<Component> spPlanet = create("HandlerI.CreatePlanet");
	spPlanet->adjoin(a_signaler);
	spConfig = spPlanet;
	spConfig->cfg< sp<RecordGroup> >("input") = rg_dataset;
	a_signaler->insert(spPlanet, l_sim_hb);

	sp<Component> spFlora = create("HandlerI.Flora");
	spFlora->adjoin(a_signaler);
	spConfig = spFlora;
	spConfig->cfg< sp<RecordGroup> >("input") = rg_dataset;
	spConfig->cfg< sp<RecordGroup> >("collisions") = rg_collisions;
	a_signaler->insert(spFlora, l_sim_hb);

	sp<Component> spFlatten = create("HandlerI.Flatten");
	spFlatten->adjoin(a_signaler);
	spConfig = spFlatten;
	spConfig->cfg< sp<RecordGroup> >("input") = rg_dataset;
	a_signaler->insert(spFlatten, l_sim_hb);
	sp<Flatten> spFL = spFlatten;
	spFL->addLimit(2, 0.0, Flatten::e_eq);
	spFL->addLimit(0, 10.0, Flatten::e_lt);
	spFL->addLimit(0, 0.0, Flatten::e_gt);
	spFL->addLimit(1, 10.0, Flatten::e_lt);
	spFL->addLimit(1, 0.0, Flatten::e_gt);
#endif
#ifdef SPH
	sp<Component> spFlatten = create("HandlerI.Flatten");
	spFlatten->adjoin(a_signaler);
	spConfig = spFlatten;
	spConfig->cfg< sp<RecordGroup> >("input") = rg_dataset;
	a_signaler->insert(spFlatten, l_sim_hb);
	sp<Flatten> spFL = spFlatten;
	spFL->addLimit(0, -0.0, Flatten::e_gt);
	spFL->addLimit(1, -0.0, Flatten::e_gt);
	spFL->addLimit(0, 3.0, Flatten::e_lt);
	spFL->addLimit(1, 20.0, Flatten::e_lt);
	spFL->addLimit(2, -0.0, Flatten::e_gt);
#endif

	// This needs to be after integration for proper display
	a_signaler->insert(spLocatedFrame, l_sim_hb);
	a_signaler->insert(spBary, l_sim_hb);
	a_signaler->insert(spEulerFrame, l_sim_hb);


#if 0
	sp<Component> spPointThrust = create("HandlerI.PointThrust");
	spPointThrust->adjoin(a_signaler);
	spConfig = spPointThrust;
	spConfig->cfg< sp<RecordGroup> >("input") = rg_dataset;
	a_signaler->insert(spPointThrust, l_accumulate);
#endif

	sp<Component> spLinearSpring = create("HandlerI.LinearSpring");
	spLinearSpring->adjoin(a_signaler);
	spConfig = spLinearSpring;
	spConfig->cfg< sp<RecordGroup> >("input") = rg_dataset;
	a_signaler->insert(spLinearSpring, l_accumulate);
	a_signaler->insert(spLinearSpring, l_init);
	//a_signaler->insert(spLinearSpring, l_validate);

#ifdef SPH
	sp<Component> spSPH = create("HandlerI.SPH");
	spSPH->adjoin(a_signaler);
	spConfig = spSPH;
	spConfig->cfg< sp<RecordGroup> >("collisions") = rg_collisions;
	a_signaler->insert(spSPH, l_accumulate);
	a_signaler->insert(spSPH, l_update);
#endif

	sp<Component> spGravity = create("HandlerI.Gravity");
	spGravity->adjoin(a_signaler);
	spConfig = spGravity;
	//spConfig->cfg<SpatialVector>("constant") = SpatialVector(0.0,1000.0,-1000.81);
	//spConfig->cfg<SpatialVector>("constant") = SpatialVector(0.0,-0.2,-9.81);
	spConfig->cfg<SpatialVector>("constant") = SpatialVector(0.0,0.0,-9.81);
	a_signaler->insert(spGravity, l_accumulate);

	sp<Component> spFormDrag = create("HandlerI.FormDrag");
	spFormDrag->adjoin(a_signaler);
	spConfig = spFormDrag;
	spConfig->cfg<String>("constant") = "1.1";
	a_signaler->insert(spFormDrag, l_accumulate);


#ifndef LINEARSPRINGONLY
	sp<Component> spLinearSpringDelta = create("HandlerI.LinearSpringDelta");
	spLinearSpringDelta->adjoin(a_signaler);
	spConfig = spLinearSpringDelta;
	spConfig->cfg< sp<RecordGroup> >("input") = rg_dataset;
	a_signaler->insert(spLinearSpringDelta, l_accumulate);
	a_signaler->insert(spLinearSpringDelta, l_clear);

	sp<Component> spLinearBend = create("HandlerI.LinearBend");
	spLinearBend->adjoin(a_signaler);
	spConfig = spLinearBend;
	a_signaler->insert(spLinearBend, l_accumulate);

	sp<Component> spLinearStrand = create("HandlerI.LinearStrand");
	spLinearStrand->adjoin(a_signaler);
	spConfig = spLinearStrand;
	a_signaler->insert(spLinearStrand, l_accumulate);

	sp<Component> spStrandTwist = create("HandlerI.StrandTwist");
	spStrandTwist->adjoin(a_signaler);
	spConfig = spStrandTwist;
	//a_signaler->insert(spStrandTwist, l_accumulate);


#ifndef SPH
#if 1
	sp<Component> spTire = create("HandlerI.Tire");
	spTire->adjoin(a_signaler);
	a_signaler->insert(spTire, l_accumulate);
	a_signaler->insert(spTire, l_clear);
	spConfig = spTire;
#endif
#endif

	sp<Component> spSSCol = create("HandlerI.SphereSphereCollision");
	spSSCol->adjoin(a_signaler);
	//a_signaler->insert(spSSCol, l_accumulate);
	spConfig = spSSCol;
	spConfig->cfg< sp<RecordGroup> >("input") = rg_collisions;

	sp<Component> spContact = create("HandlerI.Contact");
	spContact->adjoin(a_signaler);
	a_signaler->insert(spContact, l_clear);
	spConfig = spContact;

	a_signaler->insert(spEulerFrame, l_accumulate);

	sp<Component> spPhysicalFrame = create("HandlerI.PhysicalFrame");
	spPhysicalFrame->adjoin(a_signaler);
	a_signaler->insert(spPhysicalFrame, l_accumulate);
	a_signaler->insert(spPhysicalFrame, l_clear);
	spConfig = spPhysicalFrame;


	sp<Component> spMechanics = create("HandlerI.Mechanics");
	spMechanics->adjoin(a_signaler);
	spConfig = spMechanics;
	spConfig->cfg< sp<RecordGroup> >("input") = rg_dataset;
	//spConfig->cfg<SpatialVector>("gravity") = SpatialVector(0.0,5.0,-9.81);
	spConfig->cfg<SpatialVector>("gravity") = SpatialVector(0.0,0.0,-9.81);
	//spConfig->cfg<SpatialVector>("gravity") = SpatialVector(0.0,0.0,0.0);
	a_signaler->insert(spMechanics, l_sim_hb);
	a_signaler->insert(spMechanics, l_init);

	a_signaler->insert(spEulerFrame, l_sim_hb);
	a_signaler->insert(spFrameConstraint, l_sim_hb);
	a_signaler->insert(spChannelBinder, l_sim_hb);
#endif

#if 0
	l_data = spSimScope->declare("l_data");
	//Accessor<int> ttl(spSimScope, FE_USE(":TTL"));
	Accessor< sp<RecordGroup> > group(spSimScope, "group");
	//l_data->populate(ttl);
	l_data->populate(group);
	sp<ServerI>	spServer(create("ServerI"));
	if(!spServer.isValid())
	{
		feX("could not create Server component");
	}
	spServer->adjoin(spSimScope);
	spServer->addLayout(l_data);
	int port = atoi(cfg<String>(FE_CAT("port")).c_str());
	spServer->start(a_signaler, port, l_sim_hb);

#if 1
	sp<HandlerI> spSendDataset(new SendDataset(rg_dataset, l_data));
	spSendDataset->adjoin(spSimScope);
	m_spSignaler->insert(spSendDataset, l_sim_hb);
#endif
#endif


	AsSequenceSignal asSequenceSignal;
	asSequenceSignal.bind(spSimScope);

#ifdef FE_LOCK_SUPPRESSION
	l_init->suppressLock(false);
#endif
	Record r_init = a_sequencer->add(l_init,
		a_sequencer->getCurrentTime(), 1);
	asSequenceSignal.count(r_init) = 1;
	asSequenceSignal.periodic(r_init) = 0;

#ifdef FE_LOCK_SUPPRESSION
	l_sim_hb->suppressLock(false);
#endif
	Record r_sim_hb = a_sequencer->add(l_sim_hb,
		a_sequencer->getCurrentTime(), 1);
	asSequenceSignal.count(r_sim_hb) = 0;
	asSequenceSignal.periodic(r_sim_hb) = 2;
	asTemporal.timestep(r_sim_hb) = 1.0/100.0;
	asSequenceSignal.interval(r_sim_hb) =
		(int)(1000.0 * asTemporal.timestep(r_sim_hb));
	if(asSequenceSignal.interval(r_sim_hb) <= 0)
	{
		asSequenceSignal.interval(r_sim_hb) = 1;
	}


	sp<HandlerI> spH(new SimCountControl(r_sim_hb,
			registry()->master()->catalog()->catalog<int>("run:level")));
	spH->adjoin(a_signaler);
	a_signaler->insert(spH, l_hb);


	if(cfg<String>("dataset") != "")
	{
		sp<data::FileStreamI> spFileStream(
			registry()->create("FileStreamI.Ascii"));
		spFileStream->bind(spSimScope);
		sp<RecordGroup> rg_input =
			spFileStream->input(cfg<String>("dataset"));
		rg_dataset->add(rg_input);

		if(false)
		{
			SpatialTransform a_orientation;
			setIdentity(a_orientation);
			translate(a_orientation, SpatialVector(5.0,0.0,0.0));
			rotate(a_orientation, 1.0, e_yAxis);
			SpatialTransform yup;
			yup.column(0) = SpatialVector(1.0,0.0,0.0);
			yup.column(1) = SpatialVector(0.0,0.0,1.0);
			yup.column(2) = SpatialVector(0.0,-1.0,0.0);

			AsPoint m_asPoint;
			AsRelativeFrame m_asRelativeFrame;
			AsZYXTFrame m_asZYXTFrame;

			SpatialTransform orientation, inv_yup;
			invert(inv_yup, yup);
			//orientation = a_orientation * inv_yup;
			orientation = a_orientation;
			SpatialTransform rotation = orientation;
			rotation.translation() = SpatialVector(0.0,0.0,0.0);
			for(RecordGroup::iterator i_rg = rg_dataset->begin();
				i_rg != rg_dataset->end(); i_rg++)
			{
				sp<RecordArray> spRA = *i_rg;
				m_asPoint.bind(spRA->layout()->scope());
				m_asRelativeFrame.bind(spRA->layout()->scope());
				m_asZYXTFrame.bind(spRA->layout()->scope());

				if(m_asPoint.check(spRA))
				{
					for(unsigned int i = 0; i < spRA->length(); i++)
					{
						SpatialVector &v = m_asPoint.location(spRA, i);
						transformVector(orientation, v, v);
					}
				}
#if 0
				if(m_asRelativeFrame.check(spRA))
				{
					for(unsigned int i = 0; i < spRA->length(); i++)
					{
						//if(!m_asRelativeFrame.bound(spRA,i))
						{
							SpatialTransform &t =
								m_asRelativeFrame.transform(spRA,i);
							t *= orientation;
						}
					}
				}
#endif
				if(m_asZYXTFrame.check(spRA))
				{
					for(unsigned int i = 0; i < spRA->length(); i++)
					{
						if(!m_asZYXTFrame.parent(spRA,i).isValid())
						{
							SpatialVector &t = m_asZYXTFrame.t(spRA, i);

							SpatialTransform xform, xform_inv;
							setIdentity(xform);
							rotate(xform, m_asZYXTFrame.zyx(spRA, i)[0], e_zAxis);
							rotate(xform, m_asZYXTFrame.zyx(spRA, i)[1], e_yAxis);
							rotate(xform, m_asZYXTFrame.zyx(spRA, i)[2], e_xAxis);
							xform *= rotation;
							Euler<Real> euler;
							euler = xform;
							m_asZYXTFrame.zyx(spRA, i)[0] = euler[2];
							m_asZYXTFrame.zyx(spRA, i)[1] = euler[1];
							m_asZYXTFrame.zyx(spRA, i)[2] = euler[0];
							SpatialVector translation = orientation.translation();
							invert(xform_inv, rotation);
							transformVector(xform_inv, translation, translation);

							t += translation;
							//transformVector(orientation, t, t);
						}
					}
				}
			}
		}

#if 0
		spFileStream->output("xxx.ascii.rg", rg_dataset);

		spFileStream = registry()->create("FileStreamI.Binary");
		spFileStream->bind(spSimScope);

		spFileStream->output("xxx.binary.rg", rg_dataset);
#endif
	}

	// =====================================================================
	// ground surface
	if(cfg<String>("ground") != "")
	{
		registry()->manage("fexSurfaceDL");
		registry()->manage("fexTerrainDL");
		registry()->manage("fexOSGDL");
		sp<Scope> spAppScope =
			registry()->master()->catalog()->catalogComponent(
				"Scope", "ConfiguredApplicationScope");
		sp<RecordGroup> spSurfaceGroup=
			RecordView::loadRecordGroup(spSimScope,cfg<String>("ground"));
		// HACK assuming one surface in file (blindly use first)
		SurfaceModel surfaceModelRV;
		surfaceModelRV.bind(*spSurfaceGroup->begin());

		cp<SurfaceI> cpSurfaceI=surfaceModelRV.component();

		feLog("ConfiguredApplication::setup m_cpSurfaceI %p\n",
				cpSurfaceI.raw());

		registry()->master()->
			catalog()->catalog< cp<Component> >("GroundSurface") = cpSurfaceI;

		//* cpSurfaceI isn't enough, group needs to persist
		registry()->master()->
			catalog()->catalog< sp<RecordGroup> >("GroundSurfaceGroup") = spSurfaceGroup;
	}


}


} /* namespace ext */
} /* namespace fe */

