INFO 5
ATTRIBUTE spc:velocity spatial_vector
ATTRIBUTE spc:at spatial_vector
ATTRIBUTE sim:force spatial_vector
ATTRIBUTE sim:mass real
ATTRIBUTE bnd:radius real

LAYOUT "particle"
	spc:velocity
	spc:at
	sim:force
	sim:mass
	bnd:radius

RECORD 1 particle
	spc:velocity "0.1 0.0 0.0"
	sim:force "0.0 0.0 0.0"
	spc:at "2.0 3.0 -1.0"
	sim:mass 1.0
	bnd:radius 1.0

RECORD 2 particle
	spc:velocity "0.0 0.6 0.0"
	sim:force "0.0 0.0 0.0"
	spc:at "0.0 0.0 0.0"
	sim:mass 1.0
	bnd:radius 0.5

RECORDGROUP 1
	1
	2

END



