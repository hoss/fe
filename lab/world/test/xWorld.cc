/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "viewer/viewer.h"
#include "window/WindowEvent.h"
#include "architecture/architecture.h"
#include "world/world.h"
#include "math/math.h"

using namespace fe;
using namespace fe::ext;

int main(int argc,char** argv,char **env)
{
	//UNIT_START();
	bool completed=FALSE;

	try
	{
		feLogger()->clear(".*");
		feLogger()->setLog("stdout", new GroupLog());
		//feLogger()->bind("Naive.*", "stdout");

		sp<Master> spMaster(new Master);
		assertMath(spMaster->typeMaster());
		spMaster->registry()->manage("fexArchitectureDL");
		spMaster->registry()->manage("fexWorldDL");
		spMaster->catalog()->catalog<String>(FE_CAT_SPEC("log", ""));
		sp<ApplicationI> spApplication
			= spMaster->registry()->create("ApplicationI.Configured");

		spApplication->setup(argc, argv, env);

		std::vector<std::string> tokens;
		boost::char_separator<char> sep(":");
		typedef boost::tokenizer<boost::char_separator<char> > t_tokenizer;
		std::string s(spMaster->catalog()->catalog<String>(FE_USE("log")).c_str());
		t_tokenizer tkns(s, sep);
		for(t_tokenizer::iterator i_t = tkns.begin(); i_t != tkns.end(); ++i_t)
		{
			String lg = std::string(*i_t).c_str();
			if(lg != "")
			{
				feLogger()->bind(lg, "stdout");
			}
		}

//		int &run_level = spMaster->catalog()->catalog<int>("run:level");

		int rv;
		spApplication->loop(rv);

		completed=true;
	}
	catch(Exception &e)
	{
		e.log();
	}
#if 0
	catch(std::exception &e)
	{
		fprintf(stderr,"%s\n", e.what());
	}
	catch(...)
	{
		feLog("uncaught exception\n");
	}
#endif

	//UNIT_TEST(completed);
	//UNIT_RETURN();
}

