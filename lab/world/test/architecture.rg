INFO 5
ATTRIBUTE name string
ATTRIBUTE plug string
ATTRIBUTE group group
ATTRIBUTE col:name string
ATTRIBUTE col:rgba color
ATTRIBUTE bind:event string
 
LAYOUT "pipeline"
	name
	group

LAYOUT "component"
	name
	group

LAYOUT "plugin"
	name
	plug

LAYOUT "color"
	col:name
	col:rgba

LAYOUT "binding"
	name
	bind:event

LAYOUT "plugins"
	name
	group



RECORD "sim_pipeline" "pipeline"
	name "PipelineI.World"
	group "sim_components"

RECORD "window_pipeline" "pipeline"
	name "PipelineI.Window"
	group "win_components"




RECORD "debugwindow" "component"
	name "ApplicationWindowI.debug"
	group "windatagroup"

RECORD "appwindow" "component"
	name "ApplicationWindowI.World"
	group "appgroup"



RECORD "formdrag" "component"
	name "HandlerI.FormDrag"

RECORD "drawfield" "component"
	name "HandlerI.DrawScalarField"




RECORD "background" "color"
	col:name "background"
	col:rgba "0.0 0.1 0.0 1.0"

RECORD "foreground" "color"
	col:name "foreground"
	col:rgba "1.0 1.0 0.0 1.0"

RECORD "theme0" "color"
	col:name "theme0"
	col:rgba "1.0 0.0 0.5 1.0"



RECORD fe_window_orthoZoom binding
	name fe_window_orthoZoom
	bind:event mouseButton,wheel|shift,any

RECORD fe_window_orthoZoomUp binding
	name fe_window_orthoZoomUp
	bind:event i

RECORD fe_window_orthoZoomDown binding
	name fe_window_orthoZoomDown
	bind:event o

RECORD fe_window_orthoCenter binding
	name fe_window_orthoCenter
	bind:event mousePosition,drag|shift,any

RECORD fe_window_orthoRecenter binding
	name fe_window_orthoRecenter
	bind:event escape


RECORDGROUP 1
	"window_pipeline"
	"sim_pipeline"

RECORDGROUP "sim_components"

RECORDGROUP "win_components"
	debugwindow
	appwindow

RECORDGROUP "windatagroup"
	"background"
	"foreground"
	"theme0"

RECORDGROUP "appgroup"
	fe_window_orthoZoom
	fe_window_orthoZoomUp
	fe_window_orthoZoomDown
	fe_window_orthoCenter
	fe_window_orthoRecenter
	"background"
	"foreground"
	"theme0"
	"drawfield"


END

