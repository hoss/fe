/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __world_DrawTransform_h__
#define __world_DrawTransform_h__

#include "signal/signal.h"
#include "draw/draw.h"
#include "window/window.h"
#include "shape/shape.h"
#include "viewer/DrawView.h"
namespace fe
{
namespace ext
{

class FE_DL_EXPORT DrawTransform :
		public Initialize<DrawTransform>,
		virtual public HandlerI,
		virtual public Config
{

	public:
				DrawTransform(void);
virtual			~DrawTransform(void);

		void	initialize(void);

				// AS HandlerI
virtual void	handle(	Record &record);

	private:
		AsTransform			m_asTransform;
		AsColor				m_asColor;
		DrawView			m_drawview;
};


} /* namespace ext */
} /* namespace fe */

#endif /* __world_DrawTransform_h__ */
