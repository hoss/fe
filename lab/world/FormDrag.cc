/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "FormDrag.h"

namespace fe
{
namespace ext
{

FormDrag::FormDrag(void)
{
}

FormDrag::~FormDrag(void)
{
}

void FormDrag::initialize(void)
{
	cfg<String>("constant") = "1.0";
}

void FormDrag::handle(Record &r_sig)
{
	m_asParticle.bind(r_sig.layout()->scope());
	m_asBounded.bind(r_sig.layout()->scope());

	m_asAccumulate.bind(r_sig.layout()->scope());
	if(!m_asAccumulate.check(r_sig)) { return; }
	sp<RecordGroup> rg_input = m_asAccumulate.dataset(r_sig);

	Real Cdf = atof(cfg<String>("constant").c_str());

	SpatialVector zero(0.0f, 0.0f, 0.0f);

	for(RecordGroup::iterator i_rg = rg_input->begin();
		i_rg != rg_input->end(); i_rg++)
	{
		sp<RecordArray> spRA = *i_rg;
		if(m_asParticle.check(spRA) && m_asBounded.radius.check(spRA))
		{
			for(int i = 0; i < spRA->length(); i++)
			{
				SpatialVector force = -m_asParticle.velocity(spRA, i);
				force *= m_asBounded.radius(spRA, i) *
					m_asBounded.radius(spRA, i) * magnitude(force);
				m_asParticle.force(spRA, i) += Cdf * force;
			}
		}
	}
}

} /* namespace ext */
} /* namespace fe */

