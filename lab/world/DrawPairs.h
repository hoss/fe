/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __world_DrawPairs_h__
#define __world_DrawPairs_h__

#include "signal/signal.h"
#include "draw/draw.h"
#include "window/window.h"
#include "shape/shape.h"
#include "viewer/DrawView.h"
namespace fe
{
namespace ext
{

/** Draw record pairs.

	@copydoc DrawPairs_info
	*/
class FE_DL_EXPORT DrawPairs : public Initialize<DrawPairs>,
		virtual public HandlerI,
		virtual public Config
{
	public:
				DrawPairs(void);
virtual			~DrawPairs(void);

		void	initialize(void);

				// AS HandlerI
virtual void	handleBind(	sp<SignalerI> spSignalerI,
						sp<Layout> l_sig);
virtual void	handle(	Record &r_sig);

	private:


		AsColor							m_asColor;
		AsParticle						m_asParticle;
		AsPair							m_asPair;
		AsProximity						m_asProximity;
		DrawView						m_drawview;
		Color							m_color[1];
};

} /* namespace ext */
} /* namespace fe */

#endif /* __world_DrawPairs_h__ */

