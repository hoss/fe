/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "DrawPairs.h"

namespace fe
{
namespace ext
{

DrawPairs::DrawPairs(void)
{
}

DrawPairs::~DrawPairs(void)
{
}

void DrawPairs::initialize(void)
{
}

void DrawPairs::handleBind(fe::sp<fe::SignalerI> spSignalerI,
	fe::sp<fe::Layout> l_sig)
{
	m_asColor.bind(l_sig->scope());
}

void DrawPairs::handle(fe::Record &r_sig)
{
	if(!m_drawview.handle(r_sig))
	{
		return;
	}

	m_color[0] = m_drawview.getColor("foreground",		Color(1.0f,1.0f,1.0f));

	sp<RecordGroup> rg_input =
		cfg< sp<RecordGroup> >("input"); // input group

	for(RecordGroup::iterator it = rg_input->begin();
		it != rg_input->end(); it++)
	{
		sp<RecordArray> spRA(*it);
		m_asPair.bind(spRA->layout()->scope());
		m_asParticle.bind(spRA->layout()->scope());
		if(m_asPair.check(spRA))
		{
			for(int i = 0; i < spRA->length(); i++)
			{
				SpatialVector l[2];
				if(	m_asParticle.location.check(m_asPair.left(spRA, i)) &&
					m_asParticle.location.check(m_asPair.right(spRA, i)))
				{
					l[0] = m_asParticle.location(m_asPair.left(spRA, i));
					l[1] = m_asParticle.location(m_asPair.right(spRA, i));
					m_drawview.drawI()->drawLines(l, NULL, 2,
						DrawI::e_strip, false, &(m_color[0]));
				}
			}
		}
		m_asProximity.bind(spRA->layout()->scope());
		if(m_asProximity.check(spRA))
		{
			for(int i = 0; i < spRA->length(); i++)
			{
				SpatialVector l[2];
				if(	m_asParticle.location.check(m_asProximity.left(spRA, i)) &&
					m_asParticle.location.check(m_asProximity.right(spRA, i)))
				{
					l[0] = m_asParticle.location(m_asProximity.left(spRA, i));
					l[1] = m_asParticle.location(m_asProximity.right(spRA, i));
					m_drawview.drawI()->drawLines(l, NULL, 2,
						DrawI::e_strip, false, &(m_color[0]));
				}
			}
		}
	}
}


} /* namespace ext */
} /* namespace fe */

