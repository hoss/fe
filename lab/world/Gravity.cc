/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "Gravity.h"

namespace fe
{
namespace ext
{

Gravity::Gravity(void)
{
}

Gravity::~Gravity(void)
{
}

void Gravity::initialize(void)
{
	cfg<SpatialVector>("constant") = SpatialVector(0.0,0.0,-9.81);
}

void Gravity::handle(Record &r_sig)
{
	m_asParticle.bind(r_sig.layout()->scope());
	m_asSolverParticle.bind(r_sig.layout()->scope());

	m_asAccumulate.bind(r_sig.layout()->scope());
	if(!m_asAccumulate.check(r_sig)) { return; }
	sp<RecordGroup> rg_input = m_asAccumulate.dataset(r_sig);

	SpatialVector G = cfg<SpatialVector>("constant");

	SpatialVector zero(0.0f, 0.0f, 0.0f);

	for(RecordGroup::iterator i_rg = rg_input->begin();
		i_rg != rg_input->end(); i_rg++)
	{
		sp<RecordArray> spRA = *i_rg;
		if(m_asParticle.check(spRA))
		{
			bool external = false;
			if(m_asSolverParticle.check(spRA))
			{
				external = true;
			}
			for(int i = 0; i < spRA->length(); i++)
			{
				m_asParticle.force(spRA, i) +=
					G * m_asParticle.mass(spRA, i);
				if(external)
				{
					m_asSolverParticle.externalForce(spRA, i) +=
						G * m_asParticle.mass(spRA, i);
				}
			}
		}
	}
}


} /* namespace ext */
} /* namespace fe */

