/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __world_ClientPipeline_h__
#define __world_ClientPipeline_h__
namespace fe
{
namespace ext
{

class FE_DL_EXPORT ClientPipeline
	:	virtual public PipelineI,
		virtual public Config,
		public Initialize<ClientPipeline>
{
	public:
					ClientPipeline(void);
virtual				~ClientPipeline(void);
		void		initialize(void);

		// AS PipelineI
virtual	void		attach(	sp<ApplicationI> a_application,
							sp<SignalerI> a_signaler,
							sp<SequencerI> a_sequencer,
							sp<Layout> l_hb,
							sp<RecordGroup> rg_input);

	private:
		sp<SignalerI>				m_spSignaler;
		sp<Layout>					l_sim_hb;
		sp<Layout>					l_init;
		sp<Layout>					l_data;

};


} /* namespace ext */
} /* namespace fe */

#endif /* __world_ClientPipeline_h__ */

