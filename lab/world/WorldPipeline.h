/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __world_WorldPipeline_h__
#define __world_WorldPipeline_h__
namespace fe
{
namespace ext
{

/** This is a simulation pipeline, providing some useful assumptions.

	A Record of Layout "l_sim_hb" is used as a simulation heartbeat.

	Catalog item "WorldDataSet" is used as the rendevous for the main dataset
	RecordGroup.

	*/
class FE_DL_EXPORT WorldPipeline
	:	virtual public PipelineI,
		virtual public Config,
		public Initialize<WorldPipeline>
{
	public:
					WorldPipeline(void);
virtual				~WorldPipeline(void);
		void		initialize(void);

		// AS PipelineI
virtual	void		attach(	sp<ApplicationI> a_application,
							sp<SignalerI> a_signaler,
							sp<SequencerI> a_sequencer,
							sp<Layout> l_hb,
							sp<RecordGroup> rg_input);

	private:
		sp<SignalerI>				m_spSignaler;
		sp<LayoutDefault>			l_sim_hb;
		sp<LayoutDefault>			l_init;
		sp<LayoutDefault>			l_data;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __world_WorldPipeline_h__ */

