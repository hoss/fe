/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "DrawTransform.h"

namespace fe
{
namespace ext
{

DrawTransform::DrawTransform(void)
{
}

DrawTransform::~DrawTransform(void)
{
}

void DrawTransform::initialize(void)
{
}

void DrawTransform::handle(Record &r_sig)
{
	if(!m_drawview.handle(r_sig))
	{
		return;
	}

	sp<RecordGroup> rg_input =
		cfg< sp<RecordGroup> >("input"); // input group

	feAssert(rg_input.isValid(), "DrawTransform requires valid input group");

	hp<DrawI> spDraw(m_drawview.drawI());

	for(RecordGroup::iterator it = rg_input->begin();
		it != rg_input->end(); it++)
	{
		sp<RecordArray> spRA(*it);

		m_asTransform.bind(spRA->layout()->scope());
		m_asColor.bind(spRA->layout()->scope());
		if(m_asTransform.check(spRA))
		{
			for(int i = 0; i < spRA->length(); i++)
			{
				spDraw->drawTransformedAxes(
					m_asTransform.transform(spRA,i), 1.0);
			}

			if(m_asColor.check(spRA))
			{
				for(int i = 0; i < spRA->length(); i++)
				{
					String s;
					s.sPrintf("  %s", m_asColor.name(spRA,i).c_str());
					spDraw->drawAlignedText(
							m_asTransform.transform(spRA,i).translation(),
							s, m_asColor.rgba(spRA,i));
				}
			}
		}
	}

}


} /* namespace ext */
} /* namespace fe */

