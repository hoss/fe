/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __world_WorldWindow_h__
#define __world_WorldWindow_h__
namespace fe
{
namespace ext
{

/** world window.
	*/
class FE_DL_EXPORT WorldWindow :
	virtual public ApplicationWindowI,
	virtual public Config,
	public Initialize<WorldWindow>
{
	public:
				WorldWindow(void);
virtual			~WorldWindow(void);

		void	initialize(void);

				// AS ApplicationWindowI
virtual	void	populateDataLayout(sp<Layout> l_win_data);
virtual	void	makeWindow(sp<WindowI> a_window,
					Record r_win_data,
					sp<Layout> l_windowEvent,
					sp<Layout> l_draw_hb,
					sp<RecordGroup> rg_info);

	private:
		AsOrtho			m_asOrtho;
		AsPerspective	m_asPerspective;
		AsSelection		m_asSelection;
		AsSelect		m_asSelect;
		AsPick			m_asPick;
};


} /* namespace ext */
} /* namespace fe */

#endif /* __world_WorldWindow_h__ */

