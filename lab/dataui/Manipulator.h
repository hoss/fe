/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __dataui_Manipulator_h__
#define __dataui_Manipulator_h__

#include <dataui/dataui.pmh>

#include "viewer/DrawView.h"
namespace fe
{
namespace ext
{

/**
	*/
class FE_DL_EXPORT Manipulator :
	virtual public HandlerI,
	virtual public Config,
	virtual	public Reactor,
	virtual public Mask,
	public Initialize<Manipulator>
{
	public:
				Manipulator(void);
virtual			~Manipulator(void);
		void	initialize(void);

				// AS HandlerI
virtual void	handleBind(sp<SignalerI> spSignalerI, sp<Layout> spLayout);
virtual void	handle(Record &r_sig);

	private:
		void	draw(Record &r_sig);
		void	manipulate(Record &r_sig);
		void	create(Record &r_sig);
		void	createall(Record &r_sig);
		void	clear(Record &r_sig);

		WindowEvent::Mask	m_start;
		WindowEvent::Mask	m_drag;
		WindowEvent::Mask	m_end;
		WindowEvent::Mask	m_create;
		WindowEvent::Mask	m_createall;
		WindowEvent::Mask	m_clear;
		WindowEvent::Mask	m_draw;
		WindowEvent::Mask	m_start_resize;
		WindowEvent::Mask	m_end_resize;

		sp<Layout>			m_l_manipulator;
		sp<Layout>			m_l_draw;

		bool				m_resize;
		bool				m_hit;

		Real				m_aspect;
		Real				m_hide;

		sp<DrawMode>		m_spOutline;
		DrawView			m_drawview;

		AsSignal			m_asSignal;
		AsWindata			m_asWindata;
		AsSelection			m_asSel;
		AsManipulator		m_asManipulator;
		AsManipulatable		m_asManipulatable;
		AsPoint				m_asPoint;
		AsBounded			m_asBounded;
		AsPick				m_asPick;
		sp<Scope>			m_scope;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __dataui_Manipulator_h__ */
