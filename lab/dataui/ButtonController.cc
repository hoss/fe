/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "ButtonController.h"

namespace fe
{
namespace ext
{

ButtonController::ButtonController(void)
{
	m_selecting = false;

}

ButtonController::~ButtonController(void)
{
}

void ButtonController::initialize(void)
{
	// default event mapping
	maskDefault("fe_button_start",
		WindowEvent::Mask(	WindowEvent::e_sourceMouseButton,
							WindowEvent::e_itemLeft,
							WindowEvent::e_statePress));

	maskDefault("fe_button_end",
		WindowEvent::Mask(	WindowEvent::e_sourceMouseButton,
							WindowEvent::e_itemLeft,
							WindowEvent::e_stateRelease));

	maskDefault("fe_button_drag",
		WindowEvent::Mask(	WindowEvent::e_sourceMousePosition,
							WindowEvent::e_itemDrag,
							WindowEvent::e_stateAny));

	maskDefault("fe_button_motion",
		WindowEvent::Mask(	WindowEvent::e_sourceMousePosition,
							WindowEvent::e_itemMove,
							WindowEvent::e_stateAny));
}


void ButtonController::handleBind(sp<SignalerI> spSignalerI, sp<Layout> l_sig)
{
	m_asRectButton.bind(l_sig->scope());
	m_asValueButton.bind(l_sig->scope());
	m_asSignal.bind(l_sig->scope());
	m_asWindata.bind(l_sig->scope());
	m_asCallback.bind(l_sig->scope());
	m_asSel.bind(l_sig->scope());
	m_spSignaler = spSignalerI;
}

void ButtonController::handle_kb(Record &r_sig, WindowEvent &a_wev, Record &r_windata, sp<RecordGroup> spRG)
{
	for(RecordGroup::iterator it = spRG->begin(); it != spRG->end(); it++)
	{
		sp<RecordArray> spRA(*it);
		if(m_asRectButton.check(spRA))
		{
			for(int i = 0; i < spRA->length(); i++)
			{
				if(m_asRectButton.accelerator(spRA,i).isValid())
				{
					WindowEvent wev;
					wev.bind(m_asRectButton.accelerator(spRA,i));
					if(a_wev.is(wev))
					{
						if((m_asRectButton.state(spRA,i) & e_type) == e_toggle)
						{
							if(!(m_asRectButton.state(spRA,i) & e_set))
							{
								m_asRectButton.state(spRA,i) |= e_set;
								fireSignal(m_asRectButton.signal(spRA,i));
							}
						}
						if((m_asRectButton.state(spRA,i) & e_type) == e_simple)
						{
							fireSignal(m_asRectButton.signal(spRA,i));
						}
					}
				}
				if(m_asRectButton.unaccelerator(spRA,i).isValid())
				{
					WindowEvent wev;
					wev.bind(m_asRectButton.unaccelerator(spRA,i));
					if(a_wev.is(wev))
					{
						if((m_asRectButton.state(spRA,i) & e_type) == e_toggle)
						{
							if(m_asRectButton.state(spRA,i) & e_set)
							{
								m_asRectButton.state(spRA,i) &= ~e_set;
								fireSignal(m_asRectButton.unsignal(spRA,i));
							}
						}
					}
				}
			}
		}
	}
}

void ButtonController::handle(Record &r_sig)
{
	m_start = maskGet("fe_button_start");
	m_end = maskGet("fe_button_end");
	m_drag = maskGet("fe_button_drag");
	m_motion = maskGet("fe_button_motion");

	Record r_event = r_sig;
	Record r_windata = m_asSignal.winData(r_event);
	if(!r_windata.isValid()) { return; }

	sp<RecordGroup> spRG = m_asWindata.group(r_windata);

	if(!spRG.isValid())
	{
		feX("ButtonController::handle",
			"no valid record group");
		return;
	}

	if(!m_asWindata.windowI(r_windata).isValid())
	{
		feX("ButtonController::handle",
			"no valid WindowI");
		return;
	}

	WindowEvent wev;
	wev.bind(r_event);

	// if a KB event, divert to KB handler
	if(wev.matchesSource(WindowEvent::e_sourceKeyboard))
	{
		handle_kb(r_sig, wev, r_windata, spRG);
		return;
	}

	bool has_select = false;
	if(m_asSel.check(r_windata))
	{
		has_select = true;
	}

	sp<WindowI> spWindow(m_asWindata.windowI(r_windata));
	Real h = height(spWindow->geometry());


	for(RecordGroup::iterator it = spRG->begin(); it != spRG->end(); it++)
	{
		sp<RecordArray> spRA(*it);
		if(m_asRectButton.check(spRA))
		{
			bool valuator = false;
			if(m_asValueButton.check(spRA))
			{
				valuator = true;
			}
			for(int i = 0; i < spRA->length(); i++)
			{
				bool in_button = false;
				Vector4 &v = m_asRectButton.screen(spRA,i);
				if(		(wev.mouseX()		>=
						v[0])				&&
						(wev.mouseX()		<=
						v[2])				&&
						(h-wev.mouseY()		>=
						v[1])				&&
						(h-wev.mouseY()		<=
						v[3])

					)
				{
					in_button = true;
				}

				enum
				{
					e_n,
					e_si,
					e_so,
					e_ri,
					e_ro
				} transition;

				transition = e_n;


				if(wev.is(m_start))
				{
					if(in_button)
					{
						transition = e_si;
					}
					else
					{
						transition = e_so;
					}
				}
				if(wev.is(m_end))
				{
					if(in_button)
					{
						transition = e_ri;
					}
					else
					{
						transition = e_ro;
					}
				}


				if(in_button)
				{
					m_asRectButton.state(spRA,i) |= e_in;
				}
				else
				{
					m_asRectButton.state(spRA,i) &= ~e_in;
				}

				if((m_asRectButton.state(spRA,i) & e_type) == e_simple)
				{
					switch(transition)
					{
						case e_si:
							m_asRectButton.state(spRA,i) |= e_sel;
							break;
						case e_so:
						case e_ro:
							m_asRectButton.state(spRA,i) &= ~e_sel;
							break;
						case e_ri:
							if(m_asRectButton.state(spRA,i) & e_sel)
							{
								fireSignal(m_asRectButton.signal(spRA,i));
							}
							m_asRectButton.state(spRA,i) &= ~e_sel;
#if FE_POINTERMOTION==FALSE
							m_asRectButton.state(spRA,i) &= ~e_in;
#endif
							break;
						default:
							break;
					}
				}
				else if((m_asRectButton.state(spRA,i) & e_type) == e_toggle)
				{
					switch(transition)
					{
						case e_si:
							m_asRectButton.state(spRA,i) |= e_sel;
							break;
						case e_so:
						case e_ro:
							m_asRectButton.state(spRA,i) &= ~e_sel;
							break;
						case e_ri:
							if(m_asRectButton.state(spRA,i) & e_sel)
							{
								if(m_asRectButton.state(spRA,i) & e_set)
								{
									m_asRectButton.state(spRA,i) &= ~e_set;
									fireSignal(m_asRectButton.unsignal(spRA,i));
								}
								else
								{
									m_asRectButton.state(spRA,i) |= e_set;
									fireSignal(m_asRectButton.signal(spRA,i));
								}
							}
							m_asRectButton.state(spRA,i) &= ~e_sel;
#if FE_POINTERMOTION==FALSE
							m_asRectButton.state(spRA,i) &= ~e_in;
#endif
							break;
						default:
							break;
					}
				}
				else if((m_asRectButton.state(spRA,i) & e_type) == e_condition)
				{
					if(m_asRectButton.state(spRA,i) & e_set)
					{
						switch(transition)
						{
							case e_ri:
#if FE_POINTERMOTION==FALSE
								m_asRectButton.state(spRA,i) &= ~e_in;
#endif
								break;
							case e_so:
							case e_ro:
							case e_si:
							default:
								break;
						}
					}
					else
					{
						switch(transition)
						{
							case e_si:
								m_asRectButton.state(spRA,i) |= e_sel;
								break;
							case e_so:
							case e_ro:
								m_asRectButton.state(spRA,i) &= ~e_sel;
								break;
							case e_ri:
								if(m_asRectButton.state(spRA,i) & e_sel)
								{
									m_asRectButton.state(spRA,i) |= e_set;
									fireSignal(m_asRectButton.signal(spRA,i));
								}
								m_asRectButton.state(spRA,i) &= ~e_sel;
#if FE_POINTERMOTION==FALSE
								m_asRectButton.state(spRA,i) &= ~e_in;
#endif
								break;
							default:
								break;
						}
					}
				}

				if(wev.is(m_drag))
				{
					if(valuator && has_select)
					{
						if(m_asRectButton.state(spRA,i) & e_sel)
						{
							m_asValueButton.value(spRA,i)[0] +=
								m_asValueButton.scale(spRA,i)[0] * (
									m_asSel.end(r_windata)[0] -
									m_asSel.prev(r_windata)[0]);
							m_asValueButton.value(spRA,i)[1] -=
								m_asValueButton.scale(spRA,i)[1] * (
									m_asSel.end(r_windata)[1] -
									m_asSel.prev(r_windata)[1]);
							for(int d = 0; d < 2; d++)
							{
								if(m_asValueButton.maximum(spRA,i)[d] !=
									m_asValueButton.minimum(spRA,i)[d])
								{
									if(m_asValueButton.value(spRA,i)[d] >
										m_asValueButton.maximum(spRA,i)[d])
									{
										m_asValueButton.value(spRA,i)[d] =
											m_asValueButton.maximum(spRA,i)[d];
									}
									if(m_asValueButton.value(spRA,i)[d] <
										m_asValueButton.minimum(spRA,i)[d])
									{
										m_asValueButton.value(spRA,i)[d] =
											m_asValueButton.minimum(spRA,i)[d];
									}
								}
							}
							fireSignal(m_asRectButton.signal(spRA,i));
						}
					}
				}

			}
		}
	}
}

void ButtonController::fireSignal(Record &a_record)
{
	if(a_record.isValid())
	{
		if(m_asCallback.check(a_record))
		{
			if(m_asCallback.signal(a_record).isValid())
			{
				sp<SignalerI> spSignaler(m_asCallback.component(a_record));
				if(spSignaler.isValid())
				{
					spSignaler->signal(m_asCallback.signal(a_record));
				}
				else
				{
					sp<HandlerI> spHandler(m_asCallback.component(a_record));
					if(spHandler.isValid())
					{
						spHandler->handle(m_asCallback.signal(a_record));
					}
				}
			}
		}
		else
		{
			m_spSignaler->signal(a_record);
		}
	}
}

}
}
