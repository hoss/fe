/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __dataui_RootLocate_h__
#define __dataui_RootLocate_h__

#include <dataui/dataui.pmh>

#include "viewer/DrawView.h"
namespace fe
{
namespace ext
{

class FE_DL_EXPORT RootLocate :
		virtual public HandlerI
{
	public:
				RootLocate(void);
virtual			~RootLocate(void);

virtual void	handle(	Record &r_sig);

static	void	convert(Vector4 &a_v,
						sp<WindowI> a_spWindow,
						Vector2 &a_start,
						Vector2 &a_end);

	private:
		AsWidget				m_asWidget;
		DrawView				m_drawview;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __dataui_RootLocate_h__ */

