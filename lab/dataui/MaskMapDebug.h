/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __dataui_MaskMapDebug_h__
#define __dataui_MaskMapDebug_h__

#include <dataui/dataui.pmh>

#include "viewer/DrawView.h"
#include "viewer/MaskMapI.h"
#include "RegionalDebug.h"
namespace fe
{
namespace ext
{

class FE_DL_EXPORT MaskMapDebug :
	public Initialize<MaskMapDebug>,
	virtual	public RegionalDebug
{
	public:
					MaskMapDebug(void);
virtual				~MaskMapDebug(void);

		void		initialize(void);

	protected:
virtual	void	handleRegion(sp<Region> a_region);
virtual	void	handleDraw(Record &r_sig);
virtual	void	handleHome(void);

};

} /* namespace ext */
} /* namespace fe */

#endif /* __dataui_MaskMapDebug_h__ */

