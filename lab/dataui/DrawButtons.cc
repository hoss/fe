/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "DrawButtons.h"
#include "ButtonController.h"

namespace fe
{
namespace ext
{

DrawButtons::DrawButtons(void)
{
	m_spNarrow=new DrawMode();
	m_spWide=new DrawMode();

	m_spNarrow->setLineWidth(0.5f);
	m_spWide->setLineWidth(2.0f);
}

DrawButtons::~DrawButtons(void)
{
}

void DrawButtons::initialize(void)
{
}


void DrawButtons::handleBind(sp<SignalerI> spSignalerI,
	sp<Layout> l_sig)
{
	m_asRectButton.bind(l_sig->scope());
}

void DrawButtons::handle(Record &r_sig)
{
	if(!m_drawview.handle(r_sig)) { return; }

	m_color[0] = m_drawview.getColor("text",			Color(1.0f,0.5f,0.5f));
	m_color[1] = m_drawview.getColor("foreground",		Color(1.0f,1.0f,1.0f));
	m_color[2] = m_drawview.getColor("unselected_out",	Color(1.0f,1.0f,0.0f));
	m_color[3] = m_drawview.getColor("selected_in",		Color(0.0f,1.0f,0.0f));
	m_color[4] = m_drawview.getColor("selected_out",	Color(1.0f,0.5f,0.5f));
	m_color[5] = m_drawview.getColor("fall_thru",		Color(1.0f,0.0f,0.0f));

//	sp<EventContextI> spEC(m_drawview.windowI()->getEventContextI());


	for(RecordGroup::iterator it = m_drawview.group()->begin();
		it != m_drawview.group()->end(); it++)
	{
		sp<RecordArray> spRA(*it);
		if(m_asRectButton.check(spRA))
		{
			for(int i = 0; i < spRA->length(); i++)
			{
				Vector4 &v = m_asRectButton.screen(spRA,i);
				drawBox(m_drawview.drawI(),	v[0], v[1], v[2], v[3],
								m_asRectButton.depth(spRA,i),
								m_asRectButton.state(spRA,i));
				int w = m_drawview.drawI()->font()->pixelWidth(
						m_asRectButton.label(spRA,i));
				I32 a, d;
				m_drawview.drawI()->font()->fontHeight(&a, &d);
				int x_offset = (int)(((v[2]-v[0])-w) / 2);
				int y_offset = (int)(((v[3]-v[1])-(a+d)) / 2);
				Vector4 textLoc(v[0]+x_offset, v[1]+y_offset+d, 0.0f);
				m_drawview.drawI()->drawAlignedText(textLoc,
					m_asRectButton.label(spRA,i),m_color[0]);
			}
		}
	}
}

void DrawButtons::drawBox(sp<DrawI> spDraw, Real x1, Real y1,
		Real x2, Real y2, Real z, int state)
{
	Box3 box;
	set(box, x1, y1, z, x2-x1, y2-y1, z);
	Color c(m_color[5]);
	if(state & ButtonController::e_sel)
	{
		if(state & ButtonController::e_in) { c = m_color[3]; }
		else { c = m_color[4]; }
	}
	else
	{
		if(state & ButtonController::e_in) { c = m_color[2]; }
		else { c = m_color[1]; }
	}
	spDraw->pushDrawMode(
			(state & ButtonController::e_set)? m_spWide: m_spNarrow);
	spDraw->drawBox(box, c);
	spDraw->popDrawMode();
}


} /* namespace ext */
} /* namespace fe */
