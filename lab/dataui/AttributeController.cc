/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "AttributeController.h"

namespace fe
{
namespace ext
{

AttributeController::AttributeController(void)
{
	m_active = new RecordGroup();
	m_live = false;

}

AttributeController::~AttributeController(void)
{
}

void AttributeController::initialize(void)
{
	// default event mapping
	maskInit("fe_attribute");
	maskDefault("fe_attribute_start",
		WindowEvent::Mask(	WindowEvent::e_sourceKeyboard,
							(WindowEvent::Item)101 /* 'e' */,
							WindowEvent::e_statePress));

	maskDefault("fe_attribute_end",
		WindowEvent::Mask(	WindowEvent::e_sourceKeyboard,
							(WindowEvent::Item)101 /* 'e' */,
							WindowEvent::e_statePress));

	maskDefault("fe_attribute_update",
		WindowEvent::Mask(	WindowEvent::e_sourceKeyboard,
							(WindowEvent::Item)103 /* 'g' */,
							WindowEvent::e_statePress));
}

void AttributeController::handleBind(sp<SignalerI> spSignalerI,
	sp<Layout> l_sig)
{
	m_asSignal.bind(l_sig->scope());
	m_asWindata.bind(l_sig->scope());
	m_asStringEntry.bind(l_sig->scope());
	m_l_stringentry = l_sig->scope()->declare("l_fe_attr_ctrl_se");
	m_asStringEntry.populate(m_l_stringentry);

	m_asCallback.bind(l_sig->scope());
	m_l_callback = l_sig->scope()->declare("l_fe_attr_ctrl_cb");
	m_asCallback.populate(m_l_callback);

	m_asGeneric.bind(l_sig->scope());
	m_aSE.initialize(l_sig->scope(), m_asGeneric.r0.name());
	m_aTargetRecord.initialize(l_sig->scope(), m_asGeneric.r1.name());
	m_aTargetAttr.initialize(l_sig->scope(), m_asGeneric.s0.name());
	m_aTargetIndex.initialize(l_sig->scope(), m_asGeneric.i0.name());
	m_l_callback_sig = l_sig->scope()->declare("l_fe_attr_ctrl_cbsig");
	m_l_callback_sig->populate(m_aSE);
	m_l_callback_sig->populate(m_aTargetRecord);
	m_l_callback_sig->populate(m_aTargetAttr);
	m_l_callback_sig->populate(m_aTargetIndex);

	sp<TypeMaster> spTM = l_sig->scope()->typeMaster();
	m_spStringType = spTM->lookupType<String>();

	m_stringer.bind(l_sig->scope());
}


void AttributeController::handleCallback(Record &r_sig)
{
	if(m_stringer.setup(m_aTargetRecord(r_sig).layout(), m_aTargetAttr(r_sig)))
	{
		m_stringer.set(	m_aTargetRecord(r_sig),
						m_asStringEntry.text(m_aSE(r_sig)),
						m_aTargetIndex(r_sig));
	}
}

void AttributeController::handleUpdate(Record &r_update)
{
	for(unsigned int j = 0; j < m_updates.size(); j++)
	{
		for(RecordGroup::iterator it = m_updates[j]->begin();
			it != m_updates[j]->end(); it++)
		{
			sp<RecordArray> spRA(*it);
			if(m_asStringEntry.check(spRA))
			{
				for(int i = 0; i < spRA->length(); i++)
				{
					if(m_asStringEntry.state(spRA,i) == 0 &&
					m_asStringEntry.signal(spRA, i).isValid())
					{
						Record r_sig = m_asCallback.signal(
							m_asStringEntry.signal(spRA, i));
						if(m_stringer.setup(m_aTargetRecord(r_sig).layout(),
							m_aTargetAttr(r_sig)))
						{
							Array<String> values;
							m_stringer.get(m_aTargetRecord(r_sig), values);
							m_asStringEntry.text(spRA, i) =
								values[m_aTargetIndex(r_sig)];
						}
					}
				}
			}
		}
	}
}

void AttributeController::handle(Record &r_sig)
{
	if(r_sig.layout() == m_l_callback_sig)
	{
		handleCallback(r_sig);
		return;
	}

	WindowEvent wev;
	wev.bind(r_sig);

	m_start = maskGet("fe_attribute_start");
	m_end = maskGet("fe_attribute_end");
	m_update = maskGet("fe_attribute_update");

	if(wev.is(m_update))
	{
		handleUpdate(r_sig);
		return;
	}

	if(!maskEnabled(wev)) { return; }

	if(!m_asSignal.winData.check(r_sig))
	{
		feX("AttributeController");
	}
	Record r_windata = m_asSignal.winData(r_sig);

	sp<RecordGroup> rg_windata = m_asWindata.group(r_windata);
	if(!rg_windata.isValid()) { return; }

	sp<RecordGroup> rg_input = cfg< sp<RecordGroup> >("input");


	bool deactive = false;
	if(wev.is(m_end))
	{
		if(m_live) { deactive = true; m_live = false; }
		for(RecordGroup::iterator it = m_active->begin();
			it != m_active->end(); it++)
		{
			sp<RecordArray> spRA(*it);
			for(int i = 0; i < spRA->length(); i++)
			{
				rg_windata->remove(spRA->getRecord(i));
				for(int j = 0; j < int(m_outputs.size()); j++)
				{
					m_outputs[j]->remove(spRA->getRecord(i));
				}
			}
		}
		m_active->clear();
	}

	if(wev.is(m_start))
	{
		if(!m_live && !deactive)
		{
			m_live = true;
			int h = 0;
			Real w_part = (Real)1.0/(Real)m_attrs.size();
			for(RecordGroup::iterator it = rg_input->begin();
				it != rg_input->end(); it++)
			{
				sp<RecordArray> spRA(*it);
				for(int i = 0; i < spRA->length(); i++)
				{
					Real W = (Real)0.0;
					for(unsigned int j = 0; j < m_attrs.size(); j++)
					{
						if(m_stringer.setup(spRA->layout(),m_attrs[j].m_name)>0)
						{
							Array<String> values;
							m_stringer.get(spRA->getRecord(i), values);
							Real w = w_part/(Real)values.size();
							for(unsigned int k = 0; k < values.size(); k++)
							{
								Record r_se =
									r_windata.layout()->scope()->
									createRecord(m_l_stringentry);
								m_asStringEntry.state(r_se) = 0;
								m_asStringEntry.depth(r_se) = 0;
								m_asStringEntry.text(r_se) = values[k];
								set(m_asStringEntry.start(r_se),
									W+(Real)k*w, (Real)h);
								Real end = W+((Real)k+(Real)1.0)*w;
								if(end >= 1.0) { end = -1.0; }
								set(m_asStringEntry.end(r_se),
									end, (Real)(h+20));

								Record r_cb =
									r_windata.layout()->scope()->
									createRecord(m_l_callback);

								Record r_cbsig =
									r_windata.layout()->scope()->
									createRecord(m_l_callback_sig);
								m_aSE(r_cbsig) = r_se;
								m_aTargetRecord(r_cbsig) = spRA->getRecord(i);
								m_aTargetAttr(r_cbsig) = m_attrs[j].m_name;
								m_aTargetIndex(r_cbsig) = (int)k;

								m_asCallback.component(r_cb) = this;
								m_asCallback.signal(r_cb) = r_cbsig;

								m_asStringEntry.signal(r_se) = r_cb;

								rg_windata->add(r_se);
								for(int j = 0; j < int(m_outputs.size()); j++)
								{
									m_outputs[j]->add(r_se);
								}
								m_active->add(r_se);
							}
						}
						W += w_part;
					}
					h += 20;
				}
			}
		}
	}
}

void AttributeController::addAttribute(const String &a_name)
{
	Label label;
	label.m_name = a_name;
	m_attrs.push_back(label);
}

void AttributeController::addUpdate(sp<RecordGroup> spUpdate)
{
	m_updates.push_back(spUpdate);
}

void AttributeController::addOutput(sp<RecordGroup> spOutput)
{
	m_outputs.push_back(spOutput);
}

} /* namespace ext */
} /* namespace fe */

