/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __dataui_DrawAttributeLabels_h__
#define __dataui_DrawAttributeLabels_h__

#include <dataui/dataui.pmh>

#include "viewer/DrawView.h"
namespace fe
{
namespace ext
{

/**	draw names

	Use AsAttributeLabel records in the "input" RecordGroup to specify
	labels and attributes.

	@copydoc DrawNames_info
	*/
class FE_DL_EXPORT DrawAttributeLabels :
		public Initialize<DrawAttributeLabels>,
		virtual public Config,
		virtual public HandlerI
{
	public:
				DrawAttributeLabels(void);
virtual			~DrawAttributeLabels(void);
		void	initialize(void);

virtual void	handle(Record &r_sig);
virtual void	handleBind(	sp<SignalerI> spSignalerI,
						sp<Layout> l_sig);

	private:
		class Label
		{
			public:
				String		m_name;
				Vector2		m_offset;
		};
		class AnnoLabel
		{
			public:
				String		m_label;
				String		m_name;
				Vector2		m_offset;
		};
		DrawView						m_drawview;
		AsProjected						m_asProjected;
		std::vector<Label>				m_attrs;
		std::vector<AnnoLabel>			m_labels;
		StringAccessor					m_aString;
};

class AsAttributeLabel : public AccessorSet, public Initialize<AsAttributeLabel>
{
	public:
		AsAttributeLabel(void){}
		void initialize(void)
		{
			add(name,			FE_USE("dataui:name"));
			add(label,			FE_USE("dataui:label"));
			add(offset,			FE_USE("dataui:offset"));
		}

		Accessor<String>		name;
		Accessor<String>		label;
		Accessor<Vector2>		offset;
};


} /* namespace ext */
} /* namespace fe */

#endif /* __dataui_DrawAttributeLabels_h__ */

