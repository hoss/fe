/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __dataui_DrawStringEntry_h__
#define __dataui_DrawStringEntry_h__

#include <dataui/dataui.pmh>

#include "viewer/DrawView.h"
namespace fe
{
namespace ext
{

class FE_DL_EXPORT DrawStringEntry:
	public Initialize<DrawStringEntry>,
	virtual public HandlerI
{
	public:
				DrawStringEntry(void);
virtual			~DrawStringEntry(void);

		void	initialize(void);

virtual void	handleBind(	sp<SignalerI> spSignalerI,
						sp<Layout> l_sig);
virtual void	handle(	Record &r_sig);

	private:
		void	drawBox(sp<DrawI> spDraw,
						Real x1,Real y1,Real x2,Real y2,Real z,int state);

		sp<DrawMode>					m_spNarrow;
		sp<DrawMode>					m_spWide;

		AsStringEntry					m_asStringEntry;
		DrawView						m_drawview;

		Color							m_color[6];

};


} /* namespace ext */
} /* namespace fe */

#endif /* __dataui_DrawStringEntry_h__ */

