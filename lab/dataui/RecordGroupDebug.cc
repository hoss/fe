/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "RecordGroupDebug.h"
#include "window/WindowEvent.h"

namespace fe
{
namespace ext
{

RecordGroupDebug::RecordGroupDebug(void)
{
}

RecordGroupDebug::~RecordGroupDebug(void)
{
}


void RecordGroupDebug::initialize(void)
{
	cfg< sp<RecordGroup> >("group");
}

void RecordGroupDebug::handleDraw(Record &r_sig)
{
	sp<RecordGroup> m_spRG = cfg< sp<RecordGroup> >("group");

	if(!m_spRG.isValid()) { return; }

	Color red(1.0f,0.0f,0.0f,1.0f);
	Color blue(0.0f,0.0f,1.0f,1.0f);


	for(RecordGroup::iterator i_rg = m_spRG->begin();
		i_rg != m_spRG->end(); i_rg++)
	{
		sp<RecordArray> spRA = *i_rg;
		String line;
		line.sPrintf("%-30s  %4d", spRA->layout()->name().c_str(),
			spRA->length());
		drawText(line, red);
		m_cursor[1] -= m_fontHeight;
	}

}

void RecordGroupDebug::handleRegion(sp<Region> a_region)
{
}

void RecordGroupDebug::handleHome(void)
{
}

} /* namespace ext */
} /* namespace fe */

