/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __dataui_DataIOController_h__
#define __dataui_DataIOController_h__

#include <dataui/dataui.pmh>
namespace fe
{
namespace ext
{

class FE_DL_EXPORT DataIOController : public Initialize<DataIOController>,
		virtual public Config,
		virtual public Mask,
		virtual public HandlerI
{
	public:
				DataIOController(void);
virtual			~DataIOController(void);

		void	initialize(void);

				// AS HandlerI
virtual void	handle(	Record &r_sig);

	private:
		AsSignal			m_asSignal;
		sp<Scope>			m_spScope;
};

} /*namespace ext */
} /*namespace fe */

#endif /* __dataui_DataIOController_h__ */

