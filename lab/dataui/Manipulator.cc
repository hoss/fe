/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "Manipulator.h"

namespace fe
{
namespace ext
{

Manipulator::Manipulator(void)
{
	m_aspect = 2.0;
	m_hide = 0.95;

	m_spOutline=new DrawMode();
	m_spOutline->setDrawStyle(DrawMode::e_outline);
	m_spOutline->setLineWidth(1);

}

Manipulator::~Manipulator(void)
{
}

void Manipulator::initialize(void)
{
	// default event mapping
	maskInit("fe_manipulator");
	maskDefault("fe_manipulator_start",
		WindowEvent::Mask(	WindowEvent::e_sourceMouseButton,
							WindowEvent::e_itemLeft,
							WindowEvent::e_statePress));

	maskDefault("fe_manipulator_end",
		WindowEvent::Mask(	WindowEvent::e_sourceMouseButton,
							WindowEvent::e_itemLeft,
							WindowEvent::e_stateRelease));

	maskDefault("fe_manipulator_drag",
		WindowEvent::Mask(	WindowEvent::e_sourceMousePosition,
							WindowEvent::e_itemDrag,
							WindowEvent::e_stateAny));

	maskDefault("fe_manipulator_clear",
		WindowEvent::Mask(	WindowEvent::e_sourceKeyboard,
							(WindowEvent::Item)27 /* esc */,
							WindowEvent::e_statePress));

	maskDefault("fe_manipulator_createall",
		WindowEvent::Mask(	WindowEvent::e_sourceKeyboard,
							(WindowEvent::Item)99 /* 'c' */,
							WindowEvent::e_statePress));

	maskDefault("fe_manipulator_create",
		WindowEvent::Mask(	WindowEvent::e_sourceKeyboard,
							(WindowEvent::Item)114 /* 'r' */,
							WindowEvent::e_statePress));

	maskDefault("fe_manipulator_start_resize",
		WindowEvent::Mask(	WindowEvent::e_sourceMouseButton,
							WindowEvent::e_itemMiddle,
							WindowEvent::e_statePress));

	maskDefault("fe_manipulator_end_resize",
		WindowEvent::Mask(	WindowEvent::e_sourceMouseButton,
							WindowEvent::e_itemMiddle,
							WindowEvent::e_stateRelease));
}

void Manipulator::handleBind(sp<SignalerI> spSignalerI, sp<Layout> l_sig)
{
	m_scope = l_sig->scope();
	m_asSignal.bind(m_scope);
	m_asWindata.bind(m_scope);
	m_asSel.bind(m_scope);
	m_asPick.bind(m_scope);
	m_asManipulator.bind(m_scope);
}

void Manipulator::handle(Record &r_sig)
{
	hp<Layout> l_sig = r_sig.layout();
	if(l_sig->name() == cfg<String>("draw"))
	{
		draw(r_sig);
	}
	else
	{
		m_clear = maskGet("fe_manipulator_clear");
		m_create = maskGet("fe_manipulator_create");
		m_createall = maskGet("fe_manipulator_createall");
		m_start = maskGet("fe_manipulator_start");
		m_end = maskGet("fe_manipulator_end");
		m_drag = maskGet("fe_manipulator_drag");
		m_start_resize = maskGet("fe_manipulator_start_resize");
		m_end_resize = maskGet("fe_manipulator_end_resize");

		WindowEvent wev;
		wev.bind(r_sig);

		if(!maskEnabled(wev)) { return; }

		if(wev.is(m_clear)) { clear(r_sig); }
		else if(wev.is(m_create)) { create(r_sig); }
		else if(wev.is(m_createall)) { createall(r_sig); }
		else if(wev.is(m_start)) { manipulate(r_sig); }
		else if(wev.is(m_start_resize)) { manipulate(r_sig); }
		else if(wev.is(m_drag)) { manipulate(r_sig); }
		else if(wev.is(m_end)) { manipulate(r_sig); }
		else if(wev.is(m_end_resize)) { manipulate(r_sig); }
	}
}

void Manipulator::draw(Record &r_sig)
{
	if(!m_drawview.handle(r_sig))
	{
		return;
	}

	const Color white(1.0f,1.0f,1.0f,1.0f);
	const Color grey(0.3f,0.3f,0.3f,1.0f);
	const Color pink(1.0f,0.5f,0.5f,1.0f);
	const Color cyan(0.0f,1.0f,1.0f,1.0f);
	const Color yellow(1.0f,1.0f,0.0f,1.0f);
	const Color red(1.0f,0.0f,0.0f,1.0f);
	const Color green(0.0f,1.0f,0.0f,1.0f);
	const Color blue(0.0f,0.0f,1.0f,1.0f);

	Record r_windata = m_asSignal.winData(r_sig, "Manipulator");
	sp<RecordGroup> rg_input = m_asWindata.group(r_windata, "Manipulator");

	SpatialVector scale;
	SpatialTransform transform;

	hp<DrawI> spDraw = m_asWindata.drawI(r_windata, "Manipulator");

	spDraw->pushDrawMode(m_spOutline);

	SpatialTransform rotation;
	spDraw->view()->screenAlignment(rotation);
	SpatialVector planeNormal;
	getColumn(2,rotation,planeNormal);
	normalize(planeNormal);

	for(RecordGroup::iterator it = rg_input->begin();
		it != rg_input->end(); it++)
	{
		sp<RecordArray> spRA(*it);
		if(m_asManipulator.check(spRA))
		{
			for(int i = 0; i < spRA->length(); i++)
			{
				SpatialVector l = m_asManipulator.location(spRA,i);

				transform = SpatialTransform::identity();
				transform = rotation;
				setTranslation(transform, l);

				SpatialVector screenl;
				Real sc;
				spDraw->view()->screenInfo(sc, screenl, rotation, l);

				Real scr = sc * m_asManipulator.radius(spRA,i);
				Real r = scr;

				bool single_centered = false;

				if(m_asManipulator.points(spRA,i)->count() == 1)
				{
					for(RecordGroup::iterator i_p =
						m_asManipulator.points(spRA,i)->begin();
						i_p != m_asManipulator.points(spRA,i)->end(); i_p++)
					{
						sp<RecordArray> pointarray = *i_p;
						m_asBounded.bind(pointarray->layout()->scope());
						m_asPoint.bind(pointarray->layout()->scope());
						if(m_asBounded.check(pointarray))
						{
							for(int j = 0; j < pointarray->length(); j++)
							{
								if(m_asPoint.location(pointarray, j) == l)
								{
									single_centered = true;
									if((r < m_asBounded.radius(pointarray, j)))
									{
										r = m_asBounded.radius(pointarray, j);
									}
								}
							}
						}
					}
				}

				if(m_asManipulator.style(spRA,i) == 0)
				{
					r *= 1.2;
				}

				set(scale,r,r,r);


				// center
				if(m_asManipulator.style(spRA,i) == 0)
				{
					spDraw->drawCircle(transform,&scale,white);
				}
				else
				{
					if(!single_centered)
					{
						spDraw->drawCircle(transform,&scale,white);
					}
				}

				set(scale,scr,scr,scr);

				if(m_asManipulator.style(spRA,i) == 1)
				{
					SpatialVector line[3];
					if(fabs(dot(planeNormal,SpatialVector(1.0,0.0,0.0))) < m_hide)
					{
						// x+
						l[0] += r+m_aspect*scr;
						line[0] = l;
						setTranslation(transform, l);
						spDraw->drawCircle(transform,&scale,red);

						// x-
						l[0] -= 2.0*(r+m_aspect*scr);
						line[1] = l;
						setTranslation(transform, l);
						spDraw->drawCircle(transform,&scale,red);

						//spDraw->drawLines(line,NULL,2,DrawI::e_discrete,false,&red);

						l[0] = m_asManipulator.location(spRA,i)[0];
					}

					if(fabs(dot(planeNormal,SpatialVector(0.0,1.0,0.0))) < m_hide)
					{
						// y+
						l[1] += r+m_aspect*scr;
						line[0] = l;
						setTranslation(transform, l);
						spDraw->drawCircle(transform,&scale,green);

						// y-
						l[1] -= 2.0*(r+m_aspect*scr);
						line[1] = l;
						setTranslation(transform, l);
						spDraw->drawCircle(transform,&scale,green);

						//spDraw->drawLines(line,NULL,2,DrawI::e_discrete,false,&green);

						l[1] = m_asManipulator.location(spRA,i)[1];
					}

					if(fabs(dot(planeNormal,SpatialVector(0.0,0.0,1.0))) < m_hide)
					{
						// z+
						l[2] += r+m_aspect*scr;
						line[0] = l;
						setTranslation(transform, l);
						spDraw->drawCircle(transform,&scale,blue);

						// z-
						l[2] -= 2.0*(r+m_aspect*scr);
						line[1] = l;
						setTranslation(transform, l);
						spDraw->drawCircle(transform,&scale,blue);

						//spDraw->drawLines(line,NULL,2,DrawI::e_discrete,false,&blue);
					}

					line[0] = m_asManipulator.location(spRA,i);
					for(RecordGroup::iterator i_p =
						m_asManipulator.points(spRA,i)->begin();
						i_p != m_asManipulator.points(spRA,i)->end(); i_p++)
					{
						sp<RecordArray> pointarray = *i_p;
						m_asManipulatable.bind(pointarray->layout()->scope());
						for(int j = 0; j < pointarray->length(); j++)
						{
							line[1] = m_asManipulatable.location(pointarray, j);
							line[2] = m_asManipulatable.prev(pointarray, j);
							spDraw->drawLines(line,NULL,3,DrawI::e_strip,false,&grey);
						}
					}

				}
			}
		}
	}
	spDraw->popDrawMode();
}

void Manipulator::manipulate(Record &r_sig)
{
	Record r_windata = m_asSignal.winData(r_sig, "Manipulator");
	m_asPick.check(r_windata, "Manipulator");

	WindowEvent wev;
	wev.bind(r_sig);

	sp<RecordGroup> rg_win = m_asWindata.group(r_windata, "Manipulator");

	hp<DrawI> spDraw = m_asWindata.drawI(r_windata, "Manipulator");
	SpatialTransform rotation;
	spDraw->view()->screenAlignment(rotation);
	SpatialVector planeNormal;
	getColumn(2,rotation,planeNormal);

	if(wev.is(m_start) || wev.is(m_start_resize))
	{
		if(wev.is(m_start_resize))
		{
			m_resize = true;
		}
		else
		{
			m_resize = false;
		}
		m_hit = false;
		Real farthest = 1.0e30;
		Real closest = farthest;
		Real collisionDistance = 0.0;
		Record r_closest;
		int close_state;
		for(RecordGroup::iterator i_rg = rg_win->begin();
			i_rg != rg_win->end(); i_rg++)
		{
			sp<RecordArray> spRA = *i_rg;
			if(m_asManipulator.check(spRA))
			{
				for(int i = 0; i < spRA->length(); i++)
				{
					SpatialVector l = m_asManipulator.location(spRA,i);
					SpatialVector screenl;
					Real sc;
					spDraw->view()->screenInfo(sc, screenl, rotation, l);

					Real scr = sc * m_asManipulator.radius(spRA,i);
					Real r = scr;

					for(RecordGroup::iterator i_p =
						m_asManipulator.points(spRA,i)->begin();
						i_p != m_asManipulator.points(spRA,i)->end(); i_p++)
					{
						sp<RecordArray> pointarray = *i_p;
						m_asBounded.bind(pointarray->layout()->scope());
						m_asManipulatable.bind(pointarray->layout()->scope());
						if(m_asBounded.check(pointarray))
						{
							for(int j = 0; j < pointarray->length(); j++)
							{
								if(m_asManipulator.points(spRA,i)->count() == 1)
								{
									if((r<m_asBounded.radius(pointarray, j)) &&
										(m_asManipulatable.location(pointarray, j)==l))
									{
										r = m_asBounded.radius(pointarray, j);
									}
								}
							}
						}
						for(int j = 0; j < pointarray->length(); j++)
						{
							m_asManipulatable.prev(pointarray, j) =
								m_asManipulatable.location(pointarray, j);
						}
					}


					// center
					if(intersectRaySphere(&collisionDistance,
						m_asPick.start(r_windata),
						m_asPick.startv(r_windata),
						l, r))
					{
						if(collisionDistance < closest)
						{
							closest = collisionDistance;
							r_closest = spRA->getRecord(i);
							close_state = 1;
						}
					}

					if(m_asManipulator.style(spRA,i) == 1)
					{
						// x
						if(fabs(dot(planeNormal,SpatialVector(1.0,0.0,0.0)))
							< m_hide)
						{
							l[0] += r+m_aspect*scr;
							if(intersectRaySphere(&collisionDistance,
								m_asPick.start(r_windata),
								m_asPick.startv(r_windata),
								l, scr))
							{
								if(collisionDistance < closest)
								{
									closest = collisionDistance;
									r_closest = spRA->getRecord(i);
									close_state = 2;
								}
							}
							l[0] -= 2.0*(r+m_aspect*scr);
							if(intersectRaySphere(&collisionDistance,
								m_asPick.start(r_windata),
								m_asPick.startv(r_windata),
								l, scr))
							{
								if(collisionDistance < closest)
								{
									closest = collisionDistance;
									r_closest = spRA->getRecord(i);
									close_state = 2;
								}
							}
							l[0] = m_asManipulator.location(spRA,i)[0];
						}

						// y
						if(fabs(dot(planeNormal,SpatialVector(0.0,1.0,0.0)))
							< m_hide)
						{
							l[1] += r+m_aspect*scr;
							if(intersectRaySphere(&collisionDistance,
								m_asPick.start(r_windata),
								m_asPick.startv(r_windata),
								l, scr))
							{
								if(collisionDistance < closest)
								{
									closest = collisionDistance;
									r_closest = spRA->getRecord(i);
									close_state = 3;
								}
							}
							l[1] -= 2.0*(r+m_aspect*scr);
							if(intersectRaySphere(&collisionDistance,
								m_asPick.start(r_windata),
								m_asPick.startv(r_windata),
								l, scr))
							{
								if(collisionDistance < closest)
								{
									closest = collisionDistance;
									r_closest = spRA->getRecord(i);
									close_state = 3;
								}
							}
							l[1] = m_asManipulator.location(spRA,i)[1];
						}

						// z
						if(fabs(dot(planeNormal,SpatialVector(0.0,0.0,1.0)))
							< m_hide)
						{
							l[2] += r+m_aspect*scr;
							if(intersectRaySphere(&collisionDistance,
								m_asPick.start(r_windata),
								m_asPick.startv(r_windata),
								l, scr))
							{
								if(collisionDistance < closest)
								{
									closest = collisionDistance;
									r_closest = spRA->getRecord(i);
									close_state = 4;
								}
							}
							l[2] -= 2.0*(r+m_aspect*scr);
							if(intersectRaySphere(&collisionDistance,
								m_asPick.start(r_windata),
								m_asPick.startv(r_windata),
								l, scr))
							{
								if(collisionDistance < closest)
								{
									closest = collisionDistance;
									r_closest = spRA->getRecord(i);
									close_state = 4;
								}
							}
							l[2] = m_asManipulator.location(spRA,i)[2];
						}

					}
					if(closest < farthest)
					{
						m_hit = true;
					}
				}
			}
		}
		if(m_hit)
		{
			m_asManipulator.state(r_closest) = close_state;
			m_asManipulator.base(r_closest) =
				m_asManipulator.location(r_closest);
			react("on_manip");
		}
	}



	if(wev.is(m_drag))
	{
		for(RecordGroup::iterator i_rg = rg_win->begin();
			i_rg != rg_win->end(); i_rg++)
		{
			sp<RecordArray> spRA = *i_rg;
			if(m_asManipulator.check(spRA))
			{
				for(int i = 0; i < spRA->length(); i++)
				{
					if(!m_asManipulator.state(spRA,i))
					{
						continue;
					}
					SpatialVector start, end, diff;

					if(m_asManipulator.state(spRA,i) == 1)
					{
						if(!intersectRayPlane(start,
							m_asPick.start(r_windata),
							m_asPick.start(r_windata)
								+ m_asPick.startv(r_windata),
							m_asManipulator.base(spRA,i),
							planeNormal))
						{
							feX("Manipulator::manipulate", "impossibility");
						}
						if(!intersectRayPlane(end,
							m_asPick.end(r_windata),
							m_asPick.end(r_windata)
								+ m_asPick.endv(r_windata),
							m_asManipulator.base(spRA,i),
							planeNormal))
						{
							feX("Manipulator::manipulate", "impossibility");
						}

						diff = end - start;
					}

					if(m_asManipulator.style(spRA,i) == 1)
					{
						// x
						if(m_asManipulator.state(spRA,i) == 2)
						{
							SpatialVector v;
							SpatialVector shot = m_asPick.startv(r_windata);
							SpatialVector axis(1.0,0.0,0.0);
							SpatialVector planenormal;
							cross3(v, shot, axis);
							cross3(planenormal, v, axis);
							intersectRayPlane(start,
								m_asPick.start(r_windata),
								m_asPick.start(r_windata)
									+ m_asPick.startv(r_windata),
								m_asManipulator.base(spRA,i),
								planenormal);
							intersectRayPlane(end,
								m_asPick.end(r_windata),
								m_asPick.end(r_windata)+m_asPick.endv(r_windata),
								m_asManipulator.base(spRA,i),
								planenormal);
							diff = end - start;
							diff[1] = 0.0;
							diff[2] = 0.0;
						}

						// y
						if(m_asManipulator.state(spRA,i) == 3)
						{
							SpatialVector v;
							SpatialVector shot = m_asPick.startv(r_windata);
							SpatialVector axis(0.0,1.0,0.0);
							SpatialVector planenormal;
							cross3(v, shot, axis);
							cross3(planenormal, v, axis);
							intersectRayPlane(start,
								m_asPick.start(r_windata),
								m_asPick.start(r_windata)
									+ m_asPick.startv(r_windata),
								m_asManipulator.base(spRA,i),
								planenormal);
							intersectRayPlane(end,
								m_asPick.end(r_windata),
								m_asPick.end(r_windata)+m_asPick.endv(r_windata),
								m_asManipulator.base(spRA,i),
								planenormal);
							diff = end - start;
							diff[0] = 0.0;
							diff[2] = 0.0;
						}

						// z
						if(m_asManipulator.state(spRA,i) == 4)
						{
							SpatialVector v;
							SpatialVector shot = m_asPick.startv(r_windata);
							SpatialVector axis(0.0,0.0,1.0);
							SpatialVector planenormal;
							cross3(v, shot, axis);
							cross3(planenormal, v, axis);
							intersectRayPlane(start,
								m_asPick.start(r_windata),
								m_asPick.start(r_windata)
									+ m_asPick.startv(r_windata),
								m_asManipulator.base(spRA,i),
								planenormal);
							intersectRayPlane(end,
								m_asPick.end(r_windata),
								m_asPick.end(r_windata)+m_asPick.endv(r_windata),
								m_asManipulator.base(spRA,i),
								planenormal);
							diff = end - start;
							diff[0] = 0.0;
							diff[1] = 0.0;
						}
					}

					if(!m_resize)
					{
						SpatialVector l = m_asManipulator.location(spRA,i);

						m_asManipulator.location(spRA,i) =
							m_asManipulator.base(spRA,i) + diff;

						l = m_asManipulator.location(spRA,i) - l;

						for(RecordGroup::iterator i_p =
							m_asManipulator.points(spRA,i)->begin();
							i_p != m_asManipulator.points(spRA,i)->end(); i_p++)
						{
							sp<RecordArray> pointarray = *i_p;
							m_asPoint.bind(pointarray->layout()->scope());
							for(int j = 0; j < pointarray->length(); j++)
							{
								m_asPoint.location(pointarray, j) += l;
							}
						}
					}
					else
					{
						SpatialVector l = m_asManipulator.base(spRA,i);

						m_asManipulator.base(spRA,i) =
							m_asManipulator.location(spRA,i) + diff;

						l = m_asManipulator.base(spRA,i) - l;

						for(RecordGroup::iterator i_p =
							m_asManipulator.points(spRA,i)->begin();
							i_p != m_asManipulator.points(spRA,i)->end(); i_p++)
						{
							sp<RecordArray> pointarray = *i_p;
							m_asBounded.bind(pointarray->layout()->scope());
							for(int j = 0; j < pointarray->length(); j++)
							{
								m_asBounded.radius(pointarray, j) += l[0];
							}
						}
					}
				}
			}
		}
	}

	if(wev.is(m_end) || wev.is(m_end_resize))
	{
		if(m_hit)
		{
			react("off_manip");
			m_hit = false;
		}
		for(RecordGroup::iterator i_rg = rg_win->begin();
			i_rg != rg_win->end(); i_rg++)
		{
			sp<RecordArray> spRA = *i_rg;
			if(m_asManipulator.check(spRA))
			{
				for(int i = 0; i < spRA->length(); i++)
				{
					m_asManipulator.state(spRA,i) = 0;

					for(RecordGroup::iterator i_p =
						m_asManipulator.points(spRA,i)->begin();
						i_p != m_asManipulator.points(spRA,i)->end(); i_p++)
					{
						sp<RecordArray> pointarray = *i_p;
						m_asManipulatable.bind(pointarray->layout()->scope());
						for(int j = 0; j < pointarray->length(); j++)
						{
							m_asManipulatable.prev(pointarray, j) =
								m_asManipulatable.location(pointarray, j);
						}
					}
				}
			}
		}
	}

	// rcalc manipulator locations
	for(RecordGroup::iterator i_rg = rg_win->begin();
		i_rg != rg_win->end(); i_rg++)
	{
		sp<RecordArray> spRA = *i_rg;
		if(m_asManipulator.check(spRA))
		{
			for(int i = 0; i < spRA->length(); i++)
			{
				set(m_asManipulator.location(spRA,i));

				for(RecordGroup::iterator i_p =
					m_asManipulator.points(spRA,i)->begin();
					i_p != m_asManipulator.points(spRA,i)->end(); i_p++)
				{
					sp<RecordArray> pointarray = *i_p;
					m_asManipulatable.bind(pointarray->layout()->scope());
					for(int j = 0; j < pointarray->length(); j++)
					{
						m_asManipulator.location(spRA,i) +=
							m_asManipulatable.location(pointarray, j);
					}
				}

				m_asManipulator.location(spRA,i) *=
					(Real)1.0/((Real)m_asManipulator.points(spRA,i)->count());
			}
		}
	}
}

void Manipulator::create(Record &r_sig)
{
	Record r_windata = m_asSignal.winData(r_sig, "Manipulator");
	m_asSel.check(r_windata, "Manipulator");
	sp<RecordGroup> rg_output = m_asWindata.group(r_windata, "Manipulator");

	sp<RecordGroup> rg_selected = m_asSel.selected(r_windata, "Manipulator");

	if(!m_l_manipulator.isValid())
	{
		m_l_manipulator =
			m_scope->declare("l_fe_Manipulator");
		m_asManipulator.populate(m_l_manipulator);
	}

	Record r_manipulator = m_scope->createRecord(m_l_manipulator);
	m_asManipulator.points(r_manipulator) = new RecordGroup();
	m_asManipulator.state(r_manipulator) = 0;
	m_asManipulator.style(r_manipulator) = 1;

	SpatialVector center;
	int count = 0;

	if(!rg_selected.isValid())
	{
		return;
	}

	for(RecordGroup::iterator i_rg = rg_selected->begin();
		i_rg != rg_selected->end(); i_rg++)
	{
		sp<RecordArray> spRA = *i_rg;
		m_asManipulatable.bind(spRA->layout()->scope());
		if(m_asManipulatable.check(spRA))
		{
			count += spRA->length();
			for(int i = 0; i < spRA->length(); i++)
			{
				m_asManipulator.points(r_manipulator)->add(spRA->getRecord(i));
				center += m_asManipulatable.location(spRA, i);
				m_asManipulatable.prev(spRA, i) =
					m_asManipulatable.location(spRA, i);
			}
		}
	}

	if(count > 0)
	{
		center *= (Real)(1.0/(Real)count);

		m_asManipulator.location(r_manipulator) = center;
		m_asManipulator.radius(r_manipulator) = 10.0;

		rg_output->add(r_manipulator);
	}
}

void Manipulator::createall(Record &r_sig)
{
	Record r_windata = m_asSignal.winData(r_sig, "Manipulator");
	m_asSel.check(r_windata, "Manipulator");
	sp<RecordGroup> rg_output = m_asWindata.group(r_windata, "Manipulator");

	sp<RecordGroup> rg_input =
		cfg< sp<RecordGroup> >("input"); // input group

	if(!m_l_manipulator.isValid())
	{
		m_l_manipulator =
			m_scope->declare("l_fe_Manipulator");
		m_asManipulator.populate(m_l_manipulator);
	}

	if(!rg_input.isValid())
	{
		return;
	}

	for(RecordGroup::iterator i_rg = rg_input->begin();
		i_rg != rg_input->end(); i_rg++)
	{
		sp<RecordArray> spRA = *i_rg;
		m_asManipulatable.bind(spRA->layout()->scope());
		if(m_asManipulatable.check(spRA))
		{
			for(int i = 0; i < spRA->length(); i++)
			{
				Record r_manipulator = m_scope->createRecord(m_l_manipulator);
				m_asManipulator.points(r_manipulator) = new RecordGroup();
				m_asManipulator.state(r_manipulator) = 0;
				m_asManipulator.style(r_manipulator) = 0;

				m_asManipulator.points(r_manipulator)->add(spRA->getRecord(i));
				m_asManipulatable.prev(spRA, i) =
					m_asManipulatable.location(spRA, i);


				m_asManipulator.location(r_manipulator) =
					m_asManipulatable.location(spRA, i);
				m_asManipulator.radius(r_manipulator) = 10.0;

				rg_output->add(r_manipulator);
			}
		}
	}

}

void Manipulator::clear(Record &r_sig)
{
	Record r_windata = m_asSignal.winData(r_sig, "Manipulator");

	sp<RecordGroup> rg_win = m_asWindata.group(r_windata, "Manipulator");


	for(RecordGroup::iterator i_rg = rg_win->begin();
		i_rg != rg_win->end(); i_rg++)
	{
		sp<RecordArray> spRA = *i_rg;
		if(m_asManipulator.check(spRA))
		{
			spRA->clear();
		}
	}
}

} /* namespace ext */
} /* namespace fe */

