/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "dataui/dataui.h"

using namespace fe;
using namespace fe::ext;


int main(int argc,char** argv)
{
	UNIT_START();

	BWORD complete=FALSE;
	U32 limit=(argc>1)? atoi(argv[1])+1: 0;

	try
	{
		sp<Master> spMaster(new Master);
		sp<Registry> spRegistry=spMaster->registry();

		assertMath(spMaster->typeMaster());

		Result result=spRegistry->manage("fexDatauiDL");
		UNIT_TEST(successful(result));

		result=spRegistry->manage("fexNativeWindowDL");
		UNIT_TEST(successful(result));

		result=spRegistry->manage("fexOpenGLDL");
		UNIT_TEST(successful(result));

		{
			sp<HandlerI> spDebugWindow(spRegistry->create("*.DebugWindow"));
			sp<EventContextI> spEventContextI(
					spRegistry->create("EventContextI"));
			sp<SignalerI> spSignalerI(spRegistry->create("SignalerI"));
			sp<Scope> spWinScope(spMaster->catalog()->catalogComponent(
					"Scope","WinScope"));

			if(!spDebugWindow.isValid() || !spEventContextI.isValid() ||
					!spWinScope.isValid() || !spSignalerI.isValid())
			{
				feX(argv[0], "can't continue");
			}

			sp<Layout> spBeatLayout=spWinScope->declare("BEAT");
			Record beat=spWinScope->createRecord(spBeatLayout);

			spSignalerI->insert(spEventContextI,spBeatLayout);
			spSignalerI->insert(spDebugWindow,spBeatLayout);

			U32 count=0;
			while(++count!=limit)
			{
				spSignalerI->signal(beat);

				milliSleep(1);
			}
		}

		complete=TRUE;
	}
	catch(Exception &e)	{ e.log(); }
	catch(...)			{ feLog("uncaught exception\n"); }

	UNIT_TEST(complete);
	UNIT_TEST(!Counted::trackerCount());
	UNIT_TRACK(5);
	UNIT_RETURN();
}
