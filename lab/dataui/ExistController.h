/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __dataui_ExistController_h__
#define __dataui_ExistController_h__

#include <dataui/dataui.pmh>
namespace fe
{
namespace ext
{

class FE_DL_EXPORT ExistController :
	virtual public Config,
	virtual public Mask,
	virtual public HandlerI,
	public Initialize<ExistController>
{
	public:
				ExistController(void);
virtual			~ExistController(void);

				void initialize(void);

				// AS HandlerI
virtual void	handle(Record &r_sig);

	private:
		AsSignal			m_asSignal;
		AsPick				m_asPick;
		AsPoint				m_asPoint;
};


} /* namespace ext */
} /* namespace fe */


#endif /* __dataui_ExistController_h__ */

