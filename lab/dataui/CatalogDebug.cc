/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "CatalogDebug.h"
#include "window/WindowEvent.h"

namespace fe
{
namespace ext
{

CatalogDebug::CatalogDebug(void)
{
}

CatalogDebug::~CatalogDebug(void)
{
}

void CatalogDebug::initialize(void)
{
}

void CatalogDebug::handleRegion(sp<Region> a_region)
{
	sp<CountedRegion> spRegion(a_region);

	m_instance = spRegion->m_instance;
	m_name = spRegion->m_name;
}

void CatalogDebug::handleHome(void)
{
	m_instance.release();
}

void CatalogDebug::handleDraw(Record &r_sig)
{
	Array<String> keys;
	registry()->master()->catalog()->catalogKeys(keys);

	Color yellow(1.0f,1.0f,0.0f,1.0f);
	Color green(0.0f,1.0f,0.0f,1.0f);
	Color white(1.0f,1.0f,1.0f,1.0f);

	std::sort(keys.begin(), keys.end());

	m_regions.clear();
	for(unsigned int i = 0; i < keys.size(); i++)
	{
		Instance instance =
				registry()->master()->catalog()->catalogInstance(keys[i]);
		String line;
		line.sPrintf("0x%x  %s", instance.data(), keys[i].c_str());
		if(m_instance.data() == instance.data())
		{
			drawText(line, white);
		}
		else
		{
			drawText(line, green);
		}

		sp<CountedRegion> spRegion(new (CountedRegion));
		spRegion->m_instance = instance;
		spRegion->m_name = keys[i];
		m_regions.push_back(spRegion);
		spRegion->m_lo[0] = I32(m_cursor[0]);
		spRegion->m_hi[0] = I32(m_cursor[0] + textWidth(line));
		spRegion->m_lo[1] = I32(m_cursor[1]);
		spRegion->m_hi[1] = I32(m_cursor[1]+(m_fontHeight-1));

		m_cursor[1] -= m_fontHeight;
	}

	if(m_instance.data())
	{
		m_cursor[1] -= m_fontHeight;
		drawText(m_name, green);

		sp<Component> spComponent;
		if(m_instance.is< sp<Component> >())
		{
			spComponent = m_instance.cast< sp<Component> >();
		}

		if(spComponent.isValid())
		{
			m_cursor[1] -= m_fontHeight;
			drawText(spComponent->name(), green);

			sp<Config> spConfig(spComponent);
			if(spConfig.isValid())
			{
				m_cursor[1] -= m_fontHeight;
				m_cursor[0] += m_fontHeight * 1.0;
				drawText("Config", green);
				m_cursor[0] += m_fontHeight * 1.0;
				std::vector< std::pair<String,String> > configs;
// TODO reimplement this
#if 0
				spConfig->cfg(configs);
				for(unsigned int i_c = 0; i_c < configs.size(); i_c++)
				{
					String line;
					line.sPrintf("%-20s %-20s", configs[i_c].first.c_str(),
						configs[i_c].second.c_str());
					m_cursor[1] -= m_fontHeight;
					drawText(line, green);
				}
#endif
				m_cursor[0] = 0.0;
			}
		}

		if(m_instance.is< String >())
		{
			String &s = m_instance.cast<String>();
			m_cursor[1] -= m_fontHeight;
			m_cursor[0] += m_fontHeight * 1.0;
			String line;
			line.sPrintf("string: \"%s\"", s.c_str());
			drawText(line, yellow);
			m_cursor[0] = 0.0;
		}
		if(m_instance.is< int >())
		{
			int &i = m_instance.cast<int>();
			m_cursor[1] -= m_fontHeight;
			m_cursor[0] += m_fontHeight * 1.0;
			String line;
			line.sPrintf("integer: %d", i);
			drawText(line, yellow);
			m_cursor[0] = 0.0;
		}
		if(m_instance.is< Real >())
		{
			Real &r = m_instance.cast<Real>();
			m_cursor[1] -= m_fontHeight;
			m_cursor[0] += m_fontHeight * 1.0;
			String line;
			line.sPrintf("real: %f", r);
			drawText(line, yellow);
			m_cursor[0] = 0.0;
		}
		if(m_instance.is< bool >())
		{
			bool &b = m_instance.cast<bool>();
			m_cursor[1] -= m_fontHeight;
			m_cursor[0] += m_fontHeight * 1.0;
			String line;
			if(b)
			{
				line.sPrintf("bool: true");
			}
			else
			{
				line.sPrintf("bool: false");
			}
			drawText(line, yellow);
			m_cursor[0] = 0.0;
		}
	}
}




} /* namespace ext */
} /* namespace fe */

