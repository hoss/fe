/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <dataui/dataui.pmh>

#include "architecture/architecture.h"
#include "viewer/viewer.h"

namespace fe
{
namespace ext
{

DebugWindow::DebugWindow(void):
	m_bound(FALSE),
	m_drawCount(0)
{
}

DebugWindow::~DebugWindow(void)
{
	if(m_winData.isValid())
	{
		AsWindata asWindata;
		asWindata.bind(m_winData.layout()->scope());
		asWindata.drawI(m_winData) = NULL;
		asWindata.windowI(m_winData) = NULL;
		asWindata.group(m_winData) = NULL;
	}
}

void DebugWindow::initialize(void)
{
	m_spWindowI=registry()->create("WindowI");
	m_spSubSignalerI=registry()->create("SignalerI");
#if FE_OS == FE_OSX
	m_spDrawI=registry()->create("DrawI");
#else
	m_spDrawI=registry()->create("*.DrawThreaded");
#endif
}

void DebugWindow::handleSignal(Record &signal, sp<SignalerI> spSignalerI)
{
	if(!m_spDrawI.isValid() || !m_spWindowI.isValid())
	{
		feLog("invalid\n");
		return;
	}

	if(!m_bound)
	{
		sp<Layout> spBeatLayout=signal.layout();
		sp<Scope> spWinScope=spBeatLayout->scope();

		m_windowEvent.bind(spWinScope);

		sp<Layout> spEventLayout = spWinScope->declare("WindowEvent");

		sp<Layout> spWinDataLayout = spWinScope->declare("win_data");
		AsWindata asWindata;
		asWindata.populate(spWinDataLayout);
		AsSelection asSelection;
		asSelection.populate(spWinDataLayout);
		sp<Layout> spDrawLayout = spWinScope->declare("draw");
		spDrawLayout->populate(FE_USE("win:data"), "record");

		sp<AnnotateI> spAnnotateWinData(
				registry()->create("HandlerI.Annotate"));
		spAnnotateWinData->adjoin(m_spSubSignalerI);
		spSignalerI->insert(spAnnotateWinData, spEventLayout);
		m_spSubSignalerI->insert(spAnnotateWinData, spDrawLayout);

		m_winData = spWinScope->createRecord(spWinDataLayout);
		spAnnotateWinData->attach(FE_USE("win:data"), m_winData);
		asWindata.bind(spWinScope);
		asWindata.drawI(m_winData) = m_spDrawI;
		asWindata.windowI(m_winData) = m_spWindowI;
		asWindata.group(m_winData) = new RecordGroup();

		sp<Component> spOrtho(registry()->create("ViewerI.OrthoViewer"));
		spOrtho->adjoin(m_spSubSignalerI);
		m_spSubSignalerI->insert(spOrtho, spDrawLayout);
		sp<ViewerI>(spOrtho)->bind(m_spWindowI);

#if 0
		sp<Component> spCatalogDB(registry()->create("HandlerI.CatalogDebug"));
		spCatalogDB->adjoin(m_spSubSignalerI);
		spSignalerI->insert(spCatalogDB, spEventLayout);
		m_spSubSignalerI->insert(spCatalogDB, spDrawLayout);
		sp<Config>(spCatalogDB)->cfg< sp<Layout> >("draw") = spDrawLayout;
		sp<Config>(spCatalogDB)->cfg< Vector<2,Real> >("offset") =
				Vector<2,Real>((Real)500, (Real)10);
		sp<Config>(spCatalogDB)->cfg< String >("label") = "Catalog";
#endif

#if 0
		sp<Component> spWinScopeDB(registry()->create("HandlerI.ScopeDebug"));
		spWinScopeDB->adjoin(m_spSubSignalerI);
		spSignalerI->insert(spWinScopeDB, spEventLayout);
		m_spSubSignalerI->insert(spWinScopeDB, spDrawLayout);
		sp<Config>(spWinScopeDB)->cfg< sp<Component> >("scope") =
				sp<Component>(spWinScope);
		sp<Config>(spWinScopeDB)->cfg< sp<Layout> >("draw") = spDrawLayout;
		sp<Config>(spWinScopeDB)->cfg< Vector<2,Real> >("offset") =
				Vector<2,Real>((Real)500, (Real)100);
		//sp<Config>(spWinScopeDB)->cfg< String >("label") = "WinScope";
		sp<Config>(spWinScopeDB)->cfg< Real >("margin") = 10;
#endif


		sp<SignalerI>(m_spWindowI)->insert(sp<HandlerI>(this),spEventLayout);

		m_drawSignal=spWinScope->createRecord(spDrawLayout);

		m_spWindowI->open("DebugWindow");

		m_bound=TRUE;
	}

	// NOTE initialize above on beat; after that only look at window events

	BWORD draw=FALSE;
	if(m_windowEvent.eventSource.check(signal))
	{
		m_windowEvent.bind(signal);
		if(!m_windowEvent.isContinuous())
		{
//			feLog("%s\n",print(m_windowEvent).c_str());
			draw=TRUE;
			m_drawCount++;
		}
	}
	else if(m_drawCount)
	{
//		feLog("beat\n");
		draw=TRUE;
		m_drawCount--;
	}
	if(draw)
	{
		m_spSubSignalerI->signal(m_drawSignal);
		m_spDrawI->flush();
	}
}

} /* namespace ext */
} /* namespace fe */
