/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "RootLocate.h"

namespace fe
{
namespace ext
{

RootLocate::RootLocate(void)
{
}

RootLocate::~RootLocate(void)
{
}

void RootLocate::handle(Record &r_sig)
{
	if(!m_drawview.handle(r_sig)) { return; }
	for(RecordGroup::iterator it = m_drawview.group()->begin();
		it != m_drawview.group()->end(); it++)
	{
		sp<RecordArray> spRA(*it);
		m_asWidget.bind(spRA->layout()->scope());
		if(m_asWidget.check(spRA))
		{
			for(int i = 0; i < spRA->length(); i++)
			{
				convert(	m_asWidget.screen(spRA,i),
							m_drawview.windowI(),
							m_asWidget.start(spRA,i),
							m_asWidget.end(spRA,i));
			}
		}
	}
}

void RootLocate::convert(Vector4 &a_v, sp<WindowI> a_spWindow, Vector2 &a_start, Vector2 &a_end)
{
	Real h = height(a_spWindow->geometry());
	Real w = width(a_spWindow->geometry());

	if(a_start[0] < 0.0)
	{
		a_v[0] = w + a_start[0];
	}
	else if(a_start[0] < 1.0)
	{
		a_v[0] = a_start[0] * w;
	}
	else
	{
		a_v[0] = a_start[0];
	}

	if(a_start[1] < 0.0)
	{
		a_v[1] = h + a_start[1];
	}
	else if(a_start[1] < 1.0)
	{
		a_v[1] = a_start[1] * h;
	}
	else
	{
		a_v[1] = a_start[1];
	}

	if(a_end[0] < 0.0)
	{
		a_v[2] = w + a_end[0];
	}
	else if(a_end[0] < 1.0)
	{
		a_v[2] = a_end[0] * w;
	}
	else
	{
		a_v[2] = a_end[0];
	}

	if(a_end[1] < 0.0)
	{
		a_v[3] = h + a_end[1];
	}
	else if(a_end[1] < 1.0)
	{
		a_v[3] = a_end[1] * h;
	}
	else
	{
		a_v[3] = a_end[1];
	}

	Real t;
	if(a_v[2] < a_v[0])
	{
		t = a_v[0];
		a_v[0] = a_v[2];
		a_v[2] = t;
	}

	if(a_v[3] < a_v[1])
	{
		t = a_v[1];
		a_v[1] = a_v[3];
		a_v[3] = t;
	}

}

} /* namespace ext */
} /* namespace fe */

