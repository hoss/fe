/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __dataui_CatalogDebug_h__
#define __dataui_CatalogDebug_h__

#include <dataui/dataui.pmh>

#include "viewer/DrawView.h"
#include "RegionalDebug.h"
namespace fe
{
namespace ext
{

class FE_DL_EXPORT CatalogDebug :
	public Initialize<CatalogDebug>,
	virtual	public RegionalDebug
{
	public:
				CatalogDebug(void);
virtual			~CatalogDebug(void);

		void	initialize(void);

	protected:
		class CountedRegion : public Region
		{
			public:
				CountedRegion(void)	{ setName("CountedRegion"); }
virtual			~CountedRegion(void) {}
				Instance	m_instance;
				String		m_name;
		};

virtual	void	handleRegion(sp<Region> a_region);
virtual	void	handleDraw(Record &r_sig);
virtual	void	handleHome(void);

	private:
		Instance				m_instance;
		String					m_name;
};

} /* namespace ext */
} /* namespace fe */

#endif

