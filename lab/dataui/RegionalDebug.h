/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __dataui_RegionalDebug_h__
#define __dataui_RegionalDebug_h__

#include <dataui/dataui.pmh>

#include "viewer/DrawView.h"
namespace fe
{
namespace ext
{

class FE_DL_EXPORT RegionalDebug :
	public Initialize<RegionalDebug>,
	virtual public Mask,
	virtual public HandlerI,
	virtual public Config
{
	public:
				RegionalDebug(void);
virtual			~RegionalDebug(void);

		void	initialize(void);

				// AS HandlerI
virtual void	handleBind(sp<SignalerI> spSignalerI,
						sp<Layout> l_sig);
virtual void	handle(Record &record);

	private:
		void	handleEvent(Record &r_sig);
		void	applyOffset(void);

	protected:
		class Region : public Counted
		{
			public:
virtual						~Region(void){}
				Vector2i	m_lo;
				Vector2i	m_hi;
		};

virtual	void	handleRegion(sp<Region> a_region)							=0;
virtual	void	handleHome(void)											=0;
virtual	void	handleDraw(Record &r_sig)									=0;

		void	drawText(const String &a_text, const Color a_color);
		Real	textWidth(const String &a_text);

		WindowEvent::Mask					m_select;
		WindowEvent::Mask					m_home;
		sp<Scope>							m_spScope;
		AsSignal							m_asSignal;
		AsWindata							m_asWindata;
		AsOrtho								m_asOrtho;
		std::vector< sp<Region> >			m_regions;
		I32									m_fontHeight;
		Real								m_height;
		Real								m_width;
		Vector2f							m_cursor;
		DrawView							m_drawview;

	private:
		WindowEvent							m_wev;
		Vector2								m_lo;
		Vector2								m_hi;
		Vector2								m_offset;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __dataui_RegionalDebug_h__ */

