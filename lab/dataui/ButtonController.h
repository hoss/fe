/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __dataui_ButtonController_h__
#define __dataui_ButtonController_h__

#include <dataui/dataui.pmh>
namespace fe
{
namespace ext
{

/**	button controller
	@copydoc ButtonController_info
	*/
class FE_DL_EXPORT ButtonController:
	public Initialize<ButtonController>,
	virtual public HandlerI,
	virtual public Mask
{
	public:
				ButtonController(void);
virtual			~ButtonController(void);

		void	initialize(void);

virtual void	handleBind(sp<SignalerI> spSignalerI, sp<Layout> spLayout);
virtual void	handle(	Record &record);

		enum
		{
			e_base			=	0,		// 0000 0000
			e_in			=	16,		// 0001 0000
			e_sel			=	32,		// 0010 0000
			e_reserved		=	64,		// 0100 0000
			e_set			=	128,	// 1000 0000

			e_type			=	15,		// 0000 1111
			e_state			=	240,	// 1111 0000

			e_simple		=	0,		// 0000 0000
			e_toggle		=	1,		// 0000 0001
			e_condition		=	2		// 0000 0010

		};

	private:
		void handle_kb(Record &r_sig, WindowEvent &a_wev, Record &r_windata,
			sp<RecordGroup> spRG);
		void fireSignal(Record &a_record);
		sp<SignalerI>					m_spSignaler;
		AsRectButton					m_asRectButton;
		AsValueButton					m_asValueButton;
		AsWindata						m_asWindata;
		AsSignal						m_asSignal;
		AsCallback						m_asCallback;
		AsSelection						m_asSel;
		bool							m_selecting;
		WindowEvent::Mask				m_start;
		WindowEvent::Mask				m_drag;
		WindowEvent::Mask				m_end;
		WindowEvent::Mask				m_motion;
};

} /*namespace ext */
} /*namespace fe */

#endif /* __dataui_ButtonController_h__ */

