/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __dataui_StringEntryController_h__
#define __dataui_StringEntryController_h__

#include <dataui/dataui.pmh>
namespace fe
{
namespace ext
{

/**	string entry controller
	@copydoc StringEntryController_info
	*/
class FE_DL_EXPORT StringEntryController :
		public Initialize<StringEntryController>,
		virtual public Config,
		virtual public HandlerI,
		virtual	public Reactor,
		virtual	public Mask
{
	public:
				StringEntryController(void);
virtual			~StringEntryController(void);

		void	initialize(void);

virtual void	handleBind(sp<SignalerI> spSignalerI, sp<Layout> spLayout);
virtual void	handle(	Record &r_sig);

		enum
		{
			e_base			=	0,		// 0000 0000
			e_in			=	16,		// 0001 0000
			e_sel			=	32,		// 0010 0000
			e_reserved		=	64,		// 0100 0000
			e_set			=	128,	// 1000 0000

			e_type			=	15,		// 0000 1111
			e_state			=	240,	// 1111 0000

			e_basic			=	0		// 0000 0000

		};

	private:
		void fireSignal(Record &a_record);
		sp<SignalerI>					m_spSignaler;
		AsStringEntry					m_asStringEntry;
		AsWindata						m_asWindata;
		AsSignal						m_asSignal;
		AsCallback						m_asCallback;
		AsSelection						m_asSel;
		bool							m_selecting;
		WindowEvent::Mask				m_start;
		WindowEvent::Mask				m_end;
		WindowEvent::Mask				m_drag;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __dataui_StringEntryController_h__ */

