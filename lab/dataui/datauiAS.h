/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __dataui_dataiAS_h__
#define __dataui_dataiAS_h__

#include "shape/shapeAS.h"
namespace fe
{
namespace ext
{

/** manipulator.
	Typically populated and created on-demand by a Handler, so generic
	attributes are used.
	*/
class AsManipulator :
	virtual public AsPoint,
	virtual public AsBounded,
	public Initialize<AsManipulator>
{
	public:
		void initialize(void)
		{
			add(points,			FE_USE("generic:g_0"));
			add(state,			FE_USE("generic:i_0"));
			add(base,			FE_USE("manip:base"));
			add(style,			FE_USE("generic:i_1"));
		}
		/// manipulated points
		Accessor< sp<RecordGroup> >	points;

		/// actively manipulating
		Accessor<int>				state;

		/// base location used during manipulation
		Accessor<SpatialVector>		base;

		/// style 0: single center sphere   1: gnomon-like jack
		Accessor<int>				style;
};

/// manipulated
class AsManipulatable :
	virtual public AsPoint,
	public Initialize<AsManipulatable>
{
	public:
		void initialize(void)
		{
			add(prev,			FE_USE("manip:prev"));
		}
		/// base location used during manipulation
		Accessor<SpatialVector>		prev;
};

/// Rectangular widget
class AsWidget :
	public AccessorSet,
	public Initialize<AsWidget>
{
	public:
		void initialize(void)
		{
			add(start,			FE_USE("win:rb_start"));
			add(end,			FE_USE("win:rb_end"));
			add(depth,			FE_USE("win:rb_depth"));
			add(screen,			FE_USE("win:wd_screen"));
		}
		/// 2D start point of rectangle
		Accessor<Vector2>		start;
		/// 2D end point of rectangle
		Accessor<Vector2>		end;
		/// depth of button
		Accessor<Real>			depth;
		/// x1 y1 x2 y2; x1 < x2 and y1 < y2
		Accessor<Vector4>		screen;
};

/// Rectangular UI button
class AsRectButton :
	public AsWidget,
	public Initialize<AsRectButton>
{
	public:
		void initialize(void)
		{
			add(state,			FE_USE("win:rb_state"));
			add(signal,			FE_USE("win:rb_signal"));
			add(unsignal,		FE_USE("win:rb_unsignal"));
			add(label,			FE_USE("dataui:name"));
			add(accelerator,	FE_USE("win:accel"));
			add(unaccelerator,	FE_USE("win:unaccel"));
		}
		/// state of button
		Accessor<int>			state;
		/// signal callback
		Accessor<Record>		signal;
		/// unsignal callback
		Accessor<Record>		unsignal;
		/// label
		Accessor<String>		label;
		/// accelerator (as WindowEvent)
		Accessor<Record>		accelerator;
		/// unaccelerator (as WindowEvent)
		Accessor<Record>		unaccelerator;
};

///
class AsValueButton :
	public AccessorSet,
	public Initialize<AsValueButton>
{
	public:
		void initialize(void)
		{
			add(value,			FE_USE("win:vb_value"));
			add(minimum,		FE_USE("win:vb_min"));
			add(maximum,		FE_USE("win:vb_max"));
			add(scale,			FE_USE("win:vb_scale"));
		}
		///
		Accessor<Vector2>			value;
		Accessor<Vector2>			minimum;
		Accessor<Vector2>			maximum;
		Accessor<Vector2>			scale;
};

/// string entry
class AsStringEntry :
	public AsWidget,
	public Initialize<AsStringEntry>
{
	public:
		void initialize(void)
		{
			add(state,			FE_USE("win:se_state")); // differs from rb_
			add(text,			FE_USE("dataui:name"));
			add(cursor,			FE_USE("win:se_cursor"));
			add(signal,			FE_USE("win:rb_signal"));
		}
		/// state of button
		Accessor<int>			state;
		/// text
		Accessor<String>		text;
		/// cursor
		Accessor<int>			cursor;
		/// signal callback
		Accessor<Record>		signal;
};

class AsPane :
	public AsWidget,
	public Initialize<AsPane>
{
	public:
		void initialize(void)
		{
			add(group,			FE_USE("win:group"));
		}
		/// widgets in pane
		Accessor< sp<RecordGroup> >				group;
};

class AsBinding :
	public AccessorSet,
	public Initialize<AsBinding>
{
	public:
		void initialize(void)
		{
			add(name,			FE_USE("name"));
			add(event,			FE_USE("bind:event"));
		}
		/// name
		Accessor< String >				name;
		/// event
		Accessor< String >				event;
};


} /* namespace ext */
} /* namespace fe */

#endif /* __dataui_dataiAS_h__ */

