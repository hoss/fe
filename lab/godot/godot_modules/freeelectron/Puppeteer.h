/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __godot_Puppeteer_h__
#define __godot_Puppeteer_h__

/**************************************************************************//**
    @brief Skeleton Modifier

	@ingroup godot
*//***************************************************************************/
class FE_DL_EXPORT Puppeteer: public Node
{
    GDCLASS(Puppeteer, Node);

	public:

					Puppeteer(void);
					~Puppeteer(void);

virtual	void		_notification(int p_notification);

virtual	void		postinitialize(void);
virtual	void		process(float delta);

	protected:

static	void		_bind_methods(void);

	private:

		void		modify(void);

		void		set_skeleton(const NodePath &a_skeletonPath);
		NodePath	get_skeleton(void) const;

		fe::sp<fe::SingleMaster>	m_spSingleMaster;
		NodePath					m_skeletonPath;
};

#endif /* __godot_Puppeteer_h__ */
