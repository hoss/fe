/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#define FEN_NOTIFICATION_DEBUG	FALSE

#include <freeelectron.pmh>

FENode::FENode(void)
{
	feLog("fe::FENode::FENode\n");

    m_time_passed = 0.0;
    m_time_emit = 0.0;

	set_process(true);
	set_physics_process(true);
}

FENode::~FENode(void)
{
	feLog("fe::FENode::~FENode\n");
}

/***
		startup:
0		NOTIFICATION_POSTINITIALIZE
18		NOTIFICATION_PARENTED
41		NOTIFICATION_MOUSE_ENTER
10		NOTIFICATION_ENTER_TREE
27		NOTIFICATION_POST_ENTER_TREE
13		NOTIFICATION_READY
2000	NOTIFICATION_TRANSFORM_CHANGED

		per frame:
16		NOTIFICATION_PHYSICS_PROCESS
17		NOTIFICATION_PHYSICS_PROCESS

		UI:
1002	NOTIFICATION_WM_MOUSE_ENTER
1003	NOTIFICATION_WM_MOUSE_EXIT
1004	NOTIFICATION_WM_FOCUS_IN
1005	NOTIFICATION_WM_FOCUS_OUT
1006	NOTIFICATION_WM_QUIT_REQUEST
*/

void FENode::_notification(int p_notification)
{
#if FEN_NOTIFICATION_DEBUG
	feLog("FENode::_notification %d\n",p_notification);
#endif

	if(p_notification == NOTIFICATION_POSTINITIALIZE)
	{
#if FEN_NOTIFICATION_DEBUG
		feLog("NOTIFICATION_POSTINITIALIZE\n");
#endif

		postinitiialize();
	}
	if(p_notification == NOTIFICATION_PROCESS)
	{
#if FEN_NOTIFICATION_DEBUG
		feLog("NOTIFICATION_PROCESS %.6G\n",
				get_process_delta_time());
#endif

		process(get_process_delta_time());
	}
#if FEN_NOTIFICATION_DEBUG
	if(p_notification == NOTIFICATION_PHYSICS_PROCESS)
	{
		feLog("NOTIFICATION_PHYSICS_PROCESS %.6G\n",
				get_physics_process_delta_time());
	}
	if(p_notification == NOTIFICATION_READY)
	{
		feLog("NOTIFICATION_READY\n");
	}
	if(p_notification == NOTIFICATION_PAUSED)
	{
		feLog("NOTIFICATION_PAUSED\n");
	}
	if(p_notification == NOTIFICATION_UNPAUSED)
	{
		feLog("NOTIFICATION_UNPAUSED\n");
	}
#endif
}

void FENode::postinitiialize(void)
{
	feLog("fe::FENode::postinitiialize\n");

	m_spSingleMaster=fe::SingleMaster::create();
	FEASSERT(m_spSingleMaster.isValid());

	fe::sp<fe::Registry> spRegistry=m_spSingleMaster->master()->registry();
	spRegistry->manage("feAutoLoadDL");

	//* TODO surface module needs a manifest
	spRegistry->manage("fexSurfaceDL");
}

void FENode::process(float delta)
{
	const BWORD inEditor=Engine::get_singleton()->is_editor_hint();

#if FALSE
	feLog("fe::FENode::process delta=%.6G FPS=%.6G editor=%d paused=%d\n",
			delta,
			Engine::get_singleton()->get_frames_per_second(),
			inEditor,
			get_tree()->is_paused());
#endif

	//* only sim first frame in editor
	if(inEditor && m_spOutputAccessible.isValid())
	{
		return;
	}

    m_time_passed += delta;

    m_time_emit += delta;
    if (m_time_emit >= 1.0)
	{
		feLog("fe::FENode::process time_passed %.6G\n",m_time_passed);

        emit_signal("time_passed", this, m_time_passed);

        m_time_emit -= 1.0;
    }

	updateMesh();
}

void FENode::createOperator(void)
{
	const fe::String componentName="TreeOp";
	const fe::String nodeName="TreeActor";

	feLog("createOperator \"%s\" \"%s\"\n",
			componentName.c_str(),nodeName.c_str());

	FEASSERT(m_spSingleMaster.isValid());
	fe::sp<fe::Master> spMaster=m_spSingleMaster->master();
	fe::sp<fe::Registry> spRegistry=spMaster->registry();

	fe::sp<fe::Scope> spScope=spMaster->catalog()->catalogComponent(
			"Scope","OperatorScope");

	m_terminalNode.setupOperator(
			spScope,componentName,componentName+".godot");

#if FE_USE_SAGM
	m_spOutputAccessible=spRegistry->create("*.SurfaceAccessibleGodotMesh");

	fe::sp<fe::ext::SurfaceAccessibleGodotMesh> spSurfaceAccessibleGodotMesh=
			m_spOutputAccessible;

	if(spSurfaceAccessibleGodotMesh.isValid())
	{
		m_spGodotDraw=spRegistry->create("*.GodotDraw");
		m_spGodotDraw->setSurfaceAccessibleGodotMesh(
				spSurfaceAccessibleGodotMesh);

		feLog("FENode::createOperator setDrawOutput %d\n",
				m_spGodotDraw.isValid());
		m_terminalNode.setDrawOutput(m_spGodotDraw);
	}
#else
	m_spOutputAccessible=spRegistry->create("*.SurfaceAccessibleCatalog");

	//* TODO only need to put in master catalog if used by another node

	fe::sp<fe::ext::SurfaceAccessibleCatalog> spSurfaceAccessibleCatalog=
			m_spOutputAccessible;
	if(spSurfaceAccessibleCatalog.isNull())
	{
		return;
	}

	spSurfaceAccessibleCatalog->setCatalog(spMaster->catalog());
	spSurfaceAccessibleCatalog->setKey(nodeName);
	spSurfaceAccessibleCatalog->setPersistent(TRUE);
#endif

	fe::sp<fe::Catalog> spCatalog=m_terminalNode.catalog();

	spCatalog->catalog< fe::sp<fe::Component> >("Output Surface")=
			m_spOutputAccessible;

	spCatalog->catalog<fe::SpatialVector>("gravity")=
			fe::SpatialVector(0.0,0.0,0.0);
	spCatalog->catalog<bool>("YUp")=true;
	spCatalog->catalog<fe::String>("stickForm")="cylinder";
	spCatalog->catalog<fe::String>("leafForm")="point";

	feLog("CREATED \"%s\" \"%s\" done\n",
			componentName.c_str(),nodeName.c_str());
}

void FENode::updateMesh(void)
{
	static I32 s_updateCount=0;
	feLog("---- %d\n",s_updateCount++);

	feLog("FENode::updateMesh\n");

	const BWORD createMesh=m_spOutputAccessible.isNull();
	if(createMesh)
	{
		createOperator();
	}

	fe::sp<fe::Catalog> spCatalog=m_terminalNode.catalog();

#if TRUE
	spCatalog->catalog<fe::SpatialVector>("WindDirection")=fe::SpatialVector(
			0.5*sin(m_time_passed)+0.2*sin(7*m_time_passed),
			0.5*sin(13*m_time_passed),
			0.2*sin(11*m_time_passed));
#endif

	//* HACK
	const BWORD copyInput=(spCatalog->catalogOrDefault<fe::String>(
			"Output Surface","IO","").empty() ||
			!spCatalog->catalogOrDefault<fe::String>(
			"Output Surface","copy","").empty());

	const I32 frameCount=1;
	for(I32 frameIndex=0;frameIndex<frameCount;frameIndex++)
	{
		if(copyInput)
		{
			fe::sp<fe::ext::SurfaceAccessibleI> spInputAccessible=
					spCatalog->catalog< fe::sp<fe::Component> >(
					"Input Surface");

			if(spInputAccessible.isValid())
			{
				m_spOutputAccessible->copy(spInputAccessible);
			}
		}
		else
		{
			m_spOutputAccessible->clear();
		}

//		feLog("COOK %d/%d\n",frameIndex,frameCount);
		m_terminalNode.cook(fe::Real(frameIndex));
	}

#if FE_USE_SAGM
	if(m_spGodotDraw.isValid())
	{
		Ref<ArrayMesh> refMesh=
				m_spGodotDraw->surfaceAccessibleGodotMesh()->mesh();

		const I32 count=refMesh->get_surface_count();
		feLog("FENode::updateMesh surface count %d\n",count);

		set_mesh(refMesh);
	}

	return;
#else

	feLog("FENode::updateMesh output points %d prims %d\n",
		m_spOutputAccessible->count(fe::ext::SurfaceAccessibleI::e_point),
		m_spOutputAccessible->count(fe::ext::SurfaceAccessibleI::e_primitive));

	fe::sp<fe::ext::SurfaceAccessorI> spOutputPoint=
			m_spOutputAccessible->accessor(
			fe::ext::SurfaceAccessibleI::e_point,
			fe::ext::SurfaceAccessibleI::e_position);
	if(spOutputPoint.isNull())
	{
		feLog("no point access\n");
		return;
	}

	fe::sp<fe::ext::SurfaceAccessorI> spOutputNormal=
			m_spOutputAccessible->accessor(
			fe::ext::SurfaceAccessibleI::e_point,
			fe::ext::SurfaceAccessibleI::e_normal);
	if(spOutputNormal.isNull())
	{
		feLog("no normal access\n");
	}

	fe::sp<fe::ext::SurfaceAccessorI> spOutputVertices=
			m_spOutputAccessible->accessor(
			fe::ext::SurfaceAccessibleI::e_primitive,
			fe::ext::SurfaceAccessibleI::e_vertices);
	if(spOutputVertices.isNull())
	{
		feLog("no primitive access\n");
		return;
	}

	const fe::Real size=0.2;

	I32 pointStart=0;

	const I32 pointCount=spOutputPoint->count();
	const I32 primitiveCount=spOutputPoint->count();

	if(createMesh)
	{
		Ref<SpatialMaterial> refMaterial;
		refMaterial.instance();

		refMaterial->set_albedo(Color(0.5,0.4,0.3,1));

		Ref<SurfaceTool> refSurfaceTool;
		refSurfaceTool.instance();

		refSurfaceTool->begin(Mesh::PRIMITIVE_TRIANGLES);
		refSurfaceTool->set_material(refMaterial);

		for(I32 primitiveIndex=0;primitiveIndex<primitiveCount;primitiveIndex++)
		{
			const I32 subCount=spOutputVertices->subCount(primitiveIndex);
			if(subCount!=3)
			{
				continue;
			}

			const fe::SpatialVector pointA=
					spOutputVertices->spatialVector(primitiveIndex,0);
			const fe::SpatialVector pointB=
					spOutputVertices->spatialVector(primitiveIndex,1);
			const fe::SpatialVector pointC=
					spOutputVertices->spatialVector(primitiveIndex,2);

//			refSurfaceTool->add_uv(Vector2(0,0));
//			refSurfaceTool->add_normal(Vector3(1,0,0));
			refSurfaceTool->add_vertex(
					Vector3(pointA[0], pointA[1], pointA[2]));

//			refSurfaceTool->add_uv(Vector2(1,1));
//			refSurfaceTool->add_normal(Vector3(1,0,0));
			refSurfaceTool->add_vertex(
					Vector3(pointB[0], pointB[1], pointB[2]));

//			refSurfaceTool->add_uv(Vector2(0,1));
//			refSurfaceTool->add_normal(Vector3(1,0,0));
			refSurfaceTool->add_vertex(
					Vector3(pointC[0], pointC[1], pointC[2]));

			//* HACK
			pointStart=spOutputVertices->integer(primitiveIndex,2)+1;
		}

		for(I32 pointIndex=pointStart;pointIndex<pointCount;pointIndex++)
		{
			const fe::SpatialVector point=
					spOutputPoint->spatialVector(pointIndex);

//			refSurfaceTool->add_uv(Vector2(0,0));
//			refSurfaceTool->add_normal(Vector3(0,0,1));
			refSurfaceTool->add_vertex(
					Vector3(point[0], point[1], point[2]));

//			refSurfaceTool->add_uv(Vector2(1,1));
//			refSurfaceTool->add_normal(Vector3(0,0,1));
			refSurfaceTool->add_vertex(
					Vector3(point[0]-0.5*size, point[1]+size, point[2]));

//			refSurfaceTool->add_uv(Vector2(0,1));
//			refSurfaceTool->add_normal(Vector3(0,0,1));
			refSurfaceTool->add_vertex(
					Vector3(point[0]+0.5*size, point[1]+size, point[2]));
		}

		refSurfaceTool->generate_normals();

		//* this takes non-trivial time
//		refSurfaceTool->index();

		m_refMesh=refSurfaceTool->commit();

		m_refMeshDataTool.instance();
		m_refMeshDataTool->create_from_surface(m_refMesh,0);
	}
	else
	{
		I32 vertCount=0;
		for(I32 primitiveIndex=0;primitiveIndex<primitiveCount;primitiveIndex++)
		{
			const I32 subCount=spOutputVertices->subCount(primitiveIndex);
			if(subCount!=3)
			{
				continue;
			}

			const fe::SpatialVector pointA=
					spOutputVertices->spatialVector(primitiveIndex,0);
			const fe::SpatialVector pointB=
					spOutputVertices->spatialVector(primitiveIndex,1);
			const fe::SpatialVector pointC=
					spOutputVertices->spatialVector(primitiveIndex,2);

			m_refMeshDataTool->set_vertex(vertCount++,
					Vector3(pointA[0], pointA[1], pointA[2]));

			m_refMeshDataTool->set_vertex(vertCount++,
					Vector3(pointB[0], pointB[1], pointB[2]));

			m_refMeshDataTool->set_vertex(vertCount++,
					Vector3(pointC[0], pointC[1], pointC[2]));

			//* HACK
			pointStart=spOutputVertices->integer(primitiveIndex,2)+1;
		}

		for(I32 pointIndex=pointStart;pointIndex<pointCount;pointIndex++)
		{
			const fe::SpatialVector point=
					spOutputPoint->spatialVector(pointIndex);

			m_refMeshDataTool->set_vertex(vertCount++,
					Vector3(point[0], point[1], point[2]));

			m_refMeshDataTool->set_vertex(vertCount++,
					Vector3(point[0]-0.5*size, point[1]+size, point[2]));

			m_refMeshDataTool->set_vertex(vertCount++,
					Vector3(point[0]+0.5*size, point[1]+size, point[2]));
		}

		//* don't exist
//		m_refMeshDataTool->generate_normals();
//		m_refMeshDataTool->index();

		m_refMesh->surface_remove(0);
		m_refMeshDataTool->commit_to_surface(m_refMesh);
	}

	const I32 count=m_refMesh->get_surface_count();
	feLog("FENode::updateMesh surface count %d\n",count);

	set_mesh(m_refMesh);
#endif

	feLog("FENode::updateMesh done\n");
}
