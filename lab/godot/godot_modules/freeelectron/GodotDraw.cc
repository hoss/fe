/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <freeelectron.pmh>

namespace fe
{
namespace ext
{

GodotDraw::GodotDraw(void)
{
	m_refMaterial.instance();
	m_refMaterial->set_albedo(::Color(0.5,0.4,0.3,1));

	m_refSurfaceTool.instance();
}

void GodotDraw::drawPoints(const SpatialVector *vertex,
	const SpatialVector *normal,U32 vertices,
	BWORD multicolor,const Color *color,
	sp<DrawBufferI> spDrawBuffer)
{
	if(m_spGodotMesh.isNull() || !m_spGodotMesh->mesh().is_valid())
	{
		feLog("GodotDraw::drawPoints ArrayMesh is invalid\n");
		return;
	}

	//* TODO
	return;
}

void GodotDraw::drawLines(const SpatialVector *vertex,
	const SpatialVector *normal,U32 vertices,
	StripMode strip,BWORD multicolor,const Color *color,
	BWORD multiradius,const Real *radius,
	const Vector3i *element,U32 elementCount,
	sp<DrawBufferI> spDrawBuffer)
{
	drawPolygons(vertex,normal,vertices,strip,multicolor,color,TRUE);
}

void GodotDraw::drawCurve(const SpatialVector *vertex,
	const SpatialVector *normal,const Real *radius,U32 vertices,
	BWORD multicolor,const Color *color)
{
	if(radius==NULL)
	{
		drawLines(vertex,normal,vertices,DrawI::e_strip,
				multicolor,color,FALSE,NULL,NULL,0,sp<DrawBufferI>(NULL));
		return;
	}

	//* append line segments with radius attribute

	if(m_spGodotMesh.isNull() || !m_spGodotMesh->mesh().is_valid())
	{
		feLog("GodotDraw::drawCurve ArrayMesh is invalid\n");
		return;
	}

	//* TODO
	return;
}

void GodotDraw::drawTriangles(const SpatialVector *vertex,
	const SpatialVector *normal,const Vector2 *texture,U32 vertices,
	StripMode strip,BWORD multicolor,const Color* color,
	const Array<I32>* vertexMap,
	const Array<I32>* hullPointMap,const Array<Vector4i>* hullFacePoint,
	sp<DrawBufferI> spDrawBuffer)
{
	//* TODO other strip modes

	if(strip==DrawI::e_discrete)
	{
		for(U32 m=0;m<vertices;m+=3)
		{
			if(color && multicolor)
			{
				drawPolygons(&vertex[m],&normal[m],3,DrawI::e_discrete,
						multicolor,&color[m],FALSE);
			}
			else
			{
				drawPolygons(&vertex[m],&normal[m],3,DrawI::e_discrete,
						multicolor,color,FALSE);
			}
		}
	}
	else if(strip==DrawI::e_strip)
	{
#if FALSE
		for(U32 m=0;m<vertices-2;m++)
		{
			if(color && multicolor)
			{
				drawPolygons(&vertex[m],&normal[m],3,DrawI::e_discrete,
						multicolor,&color[m],FALSE);
			}
			else
			{
				drawPolygons(&vertex[m],&normal[m],3,DrawI::e_discrete,
						multicolor,color,FALSE);
			}
		}
#else
		drawPolygons(vertex,normal,vertices,DrawI::e_strip,
				multicolor,color,FALSE);
#endif
	}
}

void GodotDraw::drawPolygons(const SpatialVector *vertex,
	const SpatialVector *normal,U32 vertices,
	StripMode strip,BWORD multicolor,const Color *color,BWORD openPoly)
{
	if(m_spGodotMesh.isNull())
	{
		feLog("GodotDraw::drawPolygons"
				" SurfaceAccessibleGodotMesh is invalid\n");
		return;
	}

	Ref<ArrayMesh> refMesh=m_spGodotMesh->mesh();

	if(!m_spGodotMesh->mesh().is_valid())
	{
		feLog("GodotDraw::drawPolygons surface's ArrayMesh is invalid\n");
		return;
	}

//	feLog("GodotDraw::drawPolygons"
//			" vertices %d strip %d multicolor %d color %p openPoly %d\n",
//			vertices, strip, multicolor, color, openPoly);

	//* TODO others
	if(strip==DrawI::e_strip)
	{
#if TRUE
		m_refSurfaceTool->begin(Mesh::PRIMITIVE_TRIANGLE_STRIP);
		m_refSurfaceTool->set_material(m_refMaterial);

		const I32 addCount=vertices;

		for(I32 addIndex=0;addIndex<addCount;addIndex++)
		{
			const SpatialVector point=vertex[addIndex];

			m_refSurfaceTool->add_vertex(
					::Vector3(point[0], point[1], point[2]));
		}
#else
		m_refSurfaceTool->begin(Mesh::PRIMITIVE_TRIANGLES);
		m_refSurfaceTool->set_material(m_refMaterial);

		const I32 addCount=vertices-2;
		I32 startIndex=0;

		for(I32 addIndex=0;addIndex<addCount;addIndex++)
		{
			const I32 swap=(addIndex%2);

			const SpatialVector pointA=vertex[startIndex];
			const SpatialVector pointB=vertex[startIndex+1+swap];
			const SpatialVector pointC=vertex[startIndex+2-swap];

			startIndex++;

			m_refSurfaceTool->add_vertex(
					::Vector3(pointA[0], pointA[1], pointA[2]));

			m_refSurfaceTool->add_vertex(
					::Vector3(pointB[0], pointB[1], pointB[2]));

			m_refSurfaceTool->add_vertex(
					::Vector3(pointC[0], pointC[1], pointC[2]));
		}

		m_refSurfaceTool->generate_normals();
#endif

		m_spGodotMesh->setMesh(m_refSurfaceTool->commit(refMesh));
	}
}

} /* namespace ext */
} /* namespace fe */
