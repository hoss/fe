/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __godot_GodotDraw_h__
#define __godot_GodotDraw_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
    @brief Draw to Godot ArrayMesh

	@ingroup godot
*//***************************************************************************/
class FE_DL_EXPORT GodotDraw: public DrawCommon
{
	public:
						GodotDraw(void);
virtual					~GodotDraw(void)									{}

						//* as DrawI

						using DrawCommon::drawPoints;

virtual void			drawPoints(const SpatialVector *vertex,
								const SpatialVector *normal,U32 vertices,
								BWORD multicolor,const Color *color,
								sp<DrawBufferI> spDrawBuffer);

						using DrawCommon::drawLines;

virtual	void			drawLines(const SpatialVector *vertex,
								const SpatialVector *normal,U32 vertices,
								StripMode strip,
								BWORD multicolor,const Color *color,
								BWORD multiradius,const Real *radius,
								const Vector3i *element,U32 elementCount,
								sp<DrawBufferI> spDrawBuffer);

virtual	void			drawCurve(const SpatialVector *vertex,
								const SpatialVector *normal,
								const Real *radius,U32 vertices,
								BWORD multicolor,const Color *color);

						using DrawCommon::drawTriangles;

virtual void			drawTriangles(const SpatialVector *vertex,
								const SpatialVector *normal,
								const Vector2 *texture,U32 vertices,
								StripMode strip,BWORD multicolor,
								const Color* color,
								const Array<I32>* vertexMap,
								const Array<I32>* hullPointMap,
								const Array<Vector4i>* hullFacePoint,
								sp<DrawBufferI> spDrawBuffer);

						//* Godot specific
		sp<SurfaceAccessibleGodotMesh>	surfaceAccessibleGodotMesh(void)
						{	return m_spGodotMesh; }
		void			setSurfaceAccessibleGodotMesh(
								sp<SurfaceAccessibleGodotMesh> a_spGodotMesh)
						{	m_spGodotMesh=a_spGodotMesh; }

	private:
		void			drawPolygons(const SpatialVector *vertex,
								const SpatialVector *normal,U32 vertices,
								StripMode strip,BWORD multicolor,
								const Color *color,BWORD openPoly);

		sp<SurfaceAccessibleGodotMesh>	m_spGodotMesh;

		Ref<SpatialMaterial>			m_refMaterial;
		Ref<SurfaceTool>				m_refSurfaceTool;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __godot_GodotDraw_h__ */
