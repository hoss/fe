/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __godot_FENode_h__
#define __godot_FENode_h__

#define FE_USE_SAGM		FALSE

/**************************************************************************//**
    @brief Generic Godot Node

	@ingroup godot
*//***************************************************************************/
class FE_DL_EXPORT FENode: public MeshInstance
{
    GDCLASS(FENode, MeshInstance);

	public:

				FENode(void);
				~FENode(void);

virtual	void	_notification(int p_notification);

virtual	void	postinitiialize(void);
virtual	void	process(float delta);

	private:
		void	createOperator(void);
		void	updateMesh(void);

		float	m_time_passed;
		float	m_time_emit;

		fe::sp<fe::SingleMaster>			m_spSingleMaster;
		fe::ext::TerminalNode				m_terminalNode;
		fe::sp<fe::ext::SurfaceAccessibleI>	m_spOutputAccessible;

		Ref<MeshDataTool>			m_refMeshDataTool;
#if FE_USE_SAGM
		fe::sp<fe::ext::GodotDraw>	m_spGodotDraw;
#else
		Ref<ArrayMesh>				m_refMesh;
#endif
};

#endif /* __godot_FENode_h__ */
