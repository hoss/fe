#include "godot/godotSilence.h"

#include "register_types.h"

#include "core/class_db.h"

#include "freeelectron.pmh"

static fe::sp<fe::SingleMaster> gs_spSingleMaster;

void register_freeelectron_types()
{
	feLog("register_freeelectron_types()\n");

	ClassDB::register_class<FENode>();
	ClassDB::register_class<Puppeteer>();

	static fe::sp<fe::Library> s_spLibrary(new fe::Library());

	s_spLibrary->add<fe::ext::SurfaceAccessibleGodotMesh>(
			"SurfaceAccessibleI.SurfaceAccessibleGodotMesh.fe");

	s_spLibrary->add<fe::ext::GodotDraw>(
			"DrawI.GodotDraw.fe");

	gs_spSingleMaster=fe::SingleMaster::create();
	if(gs_spSingleMaster.isNull())
	{
		feLog("register_freeelectron_types() failed to create SingleMaster\n");
		return;
	}
	gs_spSingleMaster->master()->registry()->registerLibrary(s_spLibrary);
}

void unregister_freeelectron_types()
{
	feLog("unregister_freeelectron_types()\n");
	gs_spSingleMaster=NULL;
}
