/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <godot/native/godotNative.pmh>

using namespace fe;
using namespace fe::ext;

void GodotNode::_register_methods(void)
{
	feLog("fe::GodotNode::_register_methods\n");

	godot::register_method("_process", &GodotNode::_process);
	godot::register_signal<GodotNode>((char *)"time_passed",
			"node", GODOT_VARIANT_TYPE_OBJECT,
			"time", GODOT_VARIANT_TYPE_VECTOR2);
}

GodotNode::GodotNode(void)
{
	feLog("fe::GodotNode::GodotNode\n");

	m_time_passed = 0.0;
	m_time_emit = 0.0;
}

GodotNode::~GodotNode(void)
{
	feLog("fe::GodotNode::~GodotNode\n");
}

void GodotNode::_init(void)
{
	feLog("fe::GodotNode::_init\n");

	m_spSingleMaster=SingleMaster::create();
	FEASSERT(m_spSingleMaster.isValid());

	sp<Registry> spRegistry=m_spSingleMaster->master()->registry();
	spRegistry->manage("feAutoLoadDL");

	//* TODO surface module needs a manifest
	spRegistry->manage("fexSurfaceDL");

#if FALSE
	godot::Ref<godot::SpatialMaterial> refMaterial;
	refMaterial.instance();

	refMaterial->set_albedo(godot::Color(1,1,0,1));

	godot::Ref<godot::SurfaceTool> refSurfaceTool;
	refSurfaceTool.instance();

	refSurfaceTool->begin(godot::Mesh::PRIMITIVE_TRIANGLES);
	refSurfaceTool->set_material(refMaterial);

	refSurfaceTool->add_uv(godot::Vector2(0,0));
	refSurfaceTool->add_normal(godot::Vector3(0,0,1));
	refSurfaceTool->add_vertex(godot::Vector3(-4,0,0));

	refSurfaceTool->add_uv(godot::Vector2(0.5,1));
	refSurfaceTool->add_normal(godot::Vector3(0,0,1));
	refSurfaceTool->add_vertex(godot::Vector3(0,8,0));

	refSurfaceTool->add_uv(godot::Vector2(1,0));
	refSurfaceTool->add_normal(godot::Vector3(0,0,1));
	refSurfaceTool->add_vertex(godot::Vector3(4,0,0));

//	refSurfaceTool->generate_normals();
	refSurfaceTool->index();

	m_refMesh=refSurfaceTool->commit();

	const I32 count=m_refMesh->get_surface_count();
	feLog("fe::GodotNode::_init surface count %d\n",count);

	set_mesh(m_refMesh);
#endif

	feLog("fe::GodotNode::_init done\n");
}

void GodotNode::_process(float delta)
{
	feLog("fe::GodotNode::_process delta=%.6G\n",delta);

	m_time_passed += delta;

	m_time_emit += delta;
	if (m_time_emit >= 1.0)
	{
		feLog("fe::GodotNode::_process time_passed %.6G\n",m_time_passed);

		emit_signal("time_passed", this, m_time_passed);

		m_time_emit -= 1.0;
	}

	updateMesh();
}

void GodotNode::createOperator(void)
{
	feLog("GodotNode::createOperator\n");

	const fe::String componentName="TreeOp";
	const fe::String nodeName="TreeActor";

	feLog("cook \"%s\" \"%s\"\n",componentName.c_str(),nodeName.c_str());

	FEASSERT(m_spSingleMaster.isValid());
	sp<Master> spMaster=m_spSingleMaster->master();

	m_spOutputAccessible=
			spMaster->registry()->create("*.SurfaceAccessibleCatalog");

	//* TODO only need to put in master catalog if used by another node

	sp<SurfaceAccessibleCatalog> spSurfaceAccessibleCatalog=
			m_spOutputAccessible;
	if(spSurfaceAccessibleCatalog.isNull())
	{
		return;
	}

	spSurfaceAccessibleCatalog->setCatalog(spMaster->catalog());
	spSurfaceAccessibleCatalog->setKey(nodeName);
	spSurfaceAccessibleCatalog->setPersistent(TRUE);

	sp<Scope> spScope=spMaster->catalog()->catalogComponent(
			"Scope","OperatorScope");

	m_terminalNode.setupOperator(
			spScope,componentName,componentName+".godot");

	sp<Catalog> spCatalog=m_terminalNode.catalog();

	spCatalog->catalog< sp<Component> >("Output Surface")=m_spOutputAccessible;

	spCatalog->catalog<SpatialVector>("gravity")=SpatialVector(0.0,0.0,0.0);
	spCatalog->catalog<bool>("YUp")=true;
	spCatalog->catalog<String>("stickForm")="cylinder";
	spCatalog->catalog<String>("leafForm")="point";

	feLog("CREATED \"%s\" \"%s\" done\n",
			componentName.c_str(),nodeName.c_str());
}

void GodotNode::updateMesh(void)
{
	static I32 s_updateCount=0;
	feLog("---- %d\n",s_updateCount++);

	feLog("GodotNode::updateMesh\n");

	const BWORD createMesh=m_spOutputAccessible.isNull();
	if(createMesh)
	{
		createOperator();
	}

	sp<Catalog> spCatalog=m_terminalNode.catalog();

#if TRUE
	spCatalog->catalog<SpatialVector>("WindDirection")=SpatialVector(
			0.5*sin(m_time_passed)+0.2*sin(7*m_time_passed),
			0.5*sin(13*m_time_passed),
			0.2*sin(11*m_time_passed));
#endif

	//* HACK
	const BWORD copyInput=(spCatalog->catalogOrDefault<String>(
			"Output Surface","IO","").empty() ||
			!spCatalog->catalogOrDefault<String>(
			"Output Surface","copy","").empty());

	const I32 frameCount=1;
	for(I32 frameIndex=0;frameIndex<frameCount;frameIndex++)
	{
		if(copyInput)
		{
			sp<SurfaceAccessibleI> spInputAccessible=
					spCatalog->catalog< sp<Component> >("Input Surface");

			if(spInputAccessible.isValid())
			{
				m_spOutputAccessible->copy(spInputAccessible);
			}
		}
		else
		{
			m_spOutputAccessible->clear();
		}

		feLog("COOK %d/%d\n",frameIndex,frameCount);
		m_terminalNode.cook(Real(frameIndex));
	}

	feLog("  output points %d prims %d\n",
		m_spOutputAccessible->count(SurfaceAccessibleI::e_point),
		m_spOutputAccessible->count(SurfaceAccessibleI::e_primitive));

	sp<SurfaceAccessorI> spOutputPoint=m_spOutputAccessible->accessor(
			SurfaceAccessibleI::e_point,SurfaceAccessibleI::e_position);
	if(spOutputPoint.isNull())
	{
		feLog("no point access\n");
		return;
	}

	sp<SurfaceAccessorI> spOutputNormal=m_spOutputAccessible->accessor(
			SurfaceAccessibleI::e_point,SurfaceAccessibleI::e_normal);
	if(spOutputNormal.isNull())
	{
		feLog("no normal access\n");
	}

	sp<SurfaceAccessorI> spOutputVertices=m_spOutputAccessible->accessor(
			SurfaceAccessibleI::e_primitive,SurfaceAccessibleI::e_vertices);
	if(spOutputVertices.isNull())
	{
		feLog("no primitive access\n");
		return;
	}

	const Real size=0.2;

	I32 pointStart=0;

	const I32 pointCount=spOutputPoint->count();
	const I32 primitiveCount=spOutputPoint->count();

	godot::Ref<godot::SpatialMaterial> refMaterial;
	refMaterial.instance();

	refMaterial->set_albedo(godot::Color(0.5,0.4,0.3,1));

	if(createMesh)
	{
		godot::Ref<godot::SurfaceTool> refSurfaceTool;
		refSurfaceTool.instance();

		refSurfaceTool->begin(godot::Mesh::PRIMITIVE_TRIANGLES);
		refSurfaceTool->set_material(refMaterial);

		for(I32 primitiveIndex=0;primitiveIndex<primitiveCount;primitiveIndex++)
		{
			const I32 subCount=spOutputVertices->subCount(primitiveIndex);
			if(subCount!=3)
			{
				continue;
			}

			const SpatialVector pointA=
					spOutputVertices->spatialVector(primitiveIndex,0);
			const SpatialVector pointB=
					spOutputVertices->spatialVector(primitiveIndex,1);
			const SpatialVector pointC=
					spOutputVertices->spatialVector(primitiveIndex,2);

//			refSurfaceTool->add_uv(godot::Vector2(0,0));
//			refSurfaceTool->add_normal(godot::Vector3(1,0,0));
			refSurfaceTool->add_vertex(
					godot::Vector3(pointA[0], pointA[1], pointA[2]));

//			refSurfaceTool->add_uv(godot::Vector2(1,1));
//			refSurfaceTool->add_normal(godot::Vector3(1,0,0));
			refSurfaceTool->add_vertex(
					godot::Vector3(pointB[0], pointB[1], pointB[2]));

//			refSurfaceTool->add_uv(godot::Vector2(0,1));
//			refSurfaceTool->add_normal(godot::Vector3(1,0,0));
			refSurfaceTool->add_vertex(
					godot::Vector3(pointC[0], pointC[1], pointC[2]));

			//* HACK
			pointStart=spOutputVertices->integer(primitiveIndex,2)+1;
		}

		for(I32 pointIndex=pointStart;pointIndex<pointCount;pointIndex++)
		{
			const SpatialVector point=spOutputPoint->spatialVector(pointIndex);

//			refSurfaceTool->add_uv(godot::Vector2(0,0));
//			refSurfaceTool->add_normal(godot::Vector3(0,0,1));
			refSurfaceTool->add_vertex(
					godot::Vector3(point[0], point[1], point[2]));

//			refSurfaceTool->add_uv(godot::Vector2(1,1));
//			refSurfaceTool->add_normal(godot::Vector3(0,0,1));
			refSurfaceTool->add_vertex(
					godot::Vector3(point[0]-0.5*size, point[1]+size, point[2]));

//			refSurfaceTool->add_uv(godot::Vector2(0,1));
//			refSurfaceTool->add_normal(godot::Vector3(0,0,1));
			refSurfaceTool->add_vertex(
					godot::Vector3(point[0]+0.5*size, point[1]+size, point[2]));
		}

		refSurfaceTool->generate_normals();

		//* this takes non-trivial time
//		refSurfaceTool->index();

		m_refMesh=refSurfaceTool->commit();

		m_refMeshDataTool.instance();

		m_refMeshDataTool->create_from_surface(m_refMesh,0);
	}
	else
	{
		I32 vertCount=0;
		for(I32 primitiveIndex=0;primitiveIndex<primitiveCount;primitiveIndex++)
		{
			const I32 subCount=spOutputVertices->subCount(primitiveIndex);
			if(subCount!=3)
			{
				continue;
			}

			const SpatialVector pointA=
					spOutputVertices->spatialVector(primitiveIndex,0);
			const SpatialVector pointB=
					spOutputVertices->spatialVector(primitiveIndex,1);
			const SpatialVector pointC=
					spOutputVertices->spatialVector(primitiveIndex,2);

			m_refMeshDataTool->set_vertex(vertCount++,
					godot::Vector3(pointA[0], pointA[1], pointA[2]));

			m_refMeshDataTool->set_vertex(vertCount++,
					godot::Vector3(pointB[0], pointB[1], pointB[2]));

			m_refMeshDataTool->set_vertex(vertCount++,
					godot::Vector3(pointC[0], pointC[1], pointC[2]));

			//* HACK
			pointStart=spOutputVertices->integer(primitiveIndex,2)+1;
		}

		for(I32 pointIndex=pointStart;pointIndex<pointCount;pointIndex++)
		{
			const SpatialVector point=spOutputPoint->spatialVector(pointIndex);

			m_refMeshDataTool->set_vertex(vertCount++,
					godot::Vector3(point[0], point[1], point[2]));

			m_refMeshDataTool->set_vertex(vertCount++,
					godot::Vector3(point[0]-0.5*size, point[1]+size, point[2]));

			m_refMeshDataTool->set_vertex(vertCount++,
					godot::Vector3(point[0]+0.5*size, point[1]+size, point[2]));
		}

		//* don't exist
//		m_refMeshDataTool->generate_normals();
//		m_refMeshDataTool->index();

		m_refMesh->surface_remove(0);
		m_refMeshDataTool->commit_to_surface(m_refMesh);
	}


	const I32 count=m_refMesh->get_surface_count();
	feLog("GodotNode::updateMesh surface count %d\n",count);

	set_mesh(m_refMesh);

	feLog("GodotNode::updateMesh done\n");
}
