/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __godot_GodotNode_h__
#define __godot_GodotNode_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
    @brief Generic Godot Node

	@ingroup godot
*//***************************************************************************/
class FE_DL_EXPORT GodotNode: public godot::MeshInstance
{
    GODOT_CLASS(GodotNode, godot::MeshInstance)

	public:

				GodotNode(void);
				~GodotNode(void);

static	void	_register_methods(void);
		void	_init(void);
		void	_process(float delta);

	private:
		void	createOperator(void);
		void	updateMesh(void);

		float	m_time_passed;
		float	m_time_emit;

		fe::sp<SingleMaster>			m_spSingleMaster;
		TerminalNode					m_terminalNode;
		sp<SurfaceAccessibleI>			m_spOutputAccessible;

		godot::Ref<godot::MeshDataTool> m_refMeshDataTool;
		godot::Ref<godot::ArrayMesh>	m_refMesh;

};

} /* namespace ext */
} /* namespace fe */

#endif /* __godot_GodotNode_h__ */
