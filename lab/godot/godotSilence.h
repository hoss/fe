#ifndef __godot_godotSilence_h__
#define __godot_godotSilence_h__

#ifdef _WIN32
//* silence strcpy() warnings
#define _CRT_SECURE_NO_DEPRECATE
#define _CRT_SECURE_NO_WARNINGS
#endif

#endif /* __godot_godotSilence_h__ */
