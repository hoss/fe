/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __shade_ShaderVariablesI_h__
#define __shade_ShaderVariablesI_h__
namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Surface Variable Access

	@ingroup shade
*//***************************************************************************/
class FE_DL_EXPORT ShaderVariablesI:
	virtual public Component
{
	public:

						//* read from input
virtual	String			string(I32 a_channel) const							=0;
virtual	I32				integer(I32 a_channel) const						=0;
virtual	Real			real(I32 a_channel) const							=0;
virtual	SpatialVector	spatialVector(I32 a_channel) const					=0;
virtual	Color			color(I32 a_channel) const							=0;

						//* write to output
virtual	void			set(String a_string)								=0;
virtual	void			set(I32 a_integer)									=0;
virtual	void			set(Real a_real)									=0;
virtual	void			set(SpatialVector a_spatialVector)					=0;
virtual	void			set(Color a_color)									=0;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __shade_ShaderVariablesI_h__ */
