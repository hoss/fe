/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __shade_ShaderI_h__
#define __shade_ShaderI_h__
namespace fe
{
namespace ext
{

/**************************************************************************//**
    @brief General Surface Evaluation

	@ingroup shade
*//***************************************************************************/
class FE_DL_EXPORT ShaderI:
	virtual public Component
{
	public:

virtual	void	update(void)												=0;
virtual	void	evaluate(sp<ShaderVariablesI> a_spShaderVariablesI) const	=0;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __shade_ShaderI_h__ */
