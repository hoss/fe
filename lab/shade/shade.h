/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __shade_h__
#define __shade_h__

#include "surface/surface.h"

#include "shade/ShaderVariablesI.h"
#include "shade/ShaderI.h"

#endif /* __shade_h__ */
