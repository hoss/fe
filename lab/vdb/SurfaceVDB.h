/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __vdb_SurfaceVDB_h__
#define __vdb_SurfaceVDB_h__
namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief VDB Surface

	@ingroup vdb
*//***************************************************************************/
class FE_DL_EXPORT SurfaceVDB:
	public SurfaceSphere,
	public CastableAs<SurfaceVDB>
{
	public:
	class Impact:
		public SurfaceSphere::Impact,
		public CastableAs<Impact>
	{
		public:
							Impact(void)
							{
#if FE_COUNTED_STORE_TRACKER
								setName("SurfaceVDB::Impact");
#endif
							}
	virtual					~Impact(void)									{}

	virtual	I32				pointIndex0(void) const
							{	return m_pointIndex0; }
			void			setPointIndex0(I32 a_pointIndex0)
							{	m_pointIndex0=a_pointIndex0; }

	virtual	SpatialVector	vertex0(void) const
							{	return m_vertex0; }
			void			setVertex0(SpatialVector a_vertex0)
							{	m_vertex0=a_vertex0; }

	virtual	Vector2			uv(void)
							{	return m_uv; }
			void			setUV(Vector2 a_uv)
							{	m_uv=a_uv; }

							using SurfaceSphere::Impact::draw;

	virtual	void			draw(const SpatialTransform& a_rTransform,
									sp<DrawI> a_spDrawI,
									const fe::Color* a_pColor,
									sp<DrawBufferI> a_spDrawBuffer,
									sp<PartitionI> a_spPartition) const;

		protected:
			Vector2			m_uv;
			I32				m_pointIndex0;
			SpatialVector	m_vertex0;

	};
								SurfaceVDB(void);
virtual							~SurfaceVDB(void);

								//* As Protectable
virtual	Protectable*			clone(Protectable* pInstance=NULL);

								//* As SurfaceI

								using SurfaceSphere::bind;

virtual	void					bind(Instance a_instance);

								using SurfaceSphere::containment;

virtual Containment				containment(
										const SpatialVector& a_origin) const;

								using SurfaceSphere::nearestPoint;

virtual	sp<SurfaceI::ImpactI>	nearestPoint(
										const SpatialVector& a_origin) const;

								using SurfaceSphere::rayImpact;

virtual	sp<SurfaceI::ImpactI>	rayImpact(const SpatialVector& a_origin,
										const SpatialVector& a_direction,
										Real a_maxDistance,
										BWORD a_anyHit) const;

								using SurfaceSphere::draw;

virtual	void					draw(const SpatialTransform&,
										sp<DrawI> a_spDrawI,
										const fe::Color* a_color,
										sp<DrawBufferI> a_spDrawBuffer,
										sp<PartitionI> a_spPartition) const;

		I32						vertexCount(void) const
								{	return m_vertexCount; }

		BWORD					getVertexPoint(I32 a_index,
										SpatialVector& a_rPoint)
								{
									if(a_index<0 || a_index>=m_vertexCount)
									{
										return FALSE;
									}
									a_rPoint=m_pVertexArray[a_index];
									return TRUE;
								}
		BWORD					getVertexNormal(I32 a_index,
										SpatialVector& a_rNormal)
								{
									if(a_index<0 || a_index>=m_vertexCount)
									{
										return FALSE;
									}
									a_rNormal=m_pNormalArray[a_index];
									return TRUE;
								}
		BWORD					getVertexCpt(I32 a_index,
										SpatialVector& a_rCpt)
								{
									if(a_index<0 || a_index>=m_vertexCount)
									{
										return FALSE;
									}
									a_rCpt=m_pCptArray[a_index];
									return TRUE;
								}
		BWORD					getVertexValue(I32 a_index,
										Real& a_rValue)
								{
									if(a_index<0 || a_index>=m_vertexCount)
									{
										return FALSE;
									}
									a_rValue=m_pValueArray[a_index];
									return TRUE;
								}

	protected:
virtual	void					cache(void);

virtual	void					resolveImpact(sp<ImpactI> a_spImpactI) const {}

		sp<SurfaceI::ImpactI>	computeImpact(const SpatialVector& a_origin,
										const SpatialVector&
										a_intersection,
										I32 a_vertexIndex) const;

		openvdb::Vec3d			spatialToIndex(
										const SpatialVector& a_point) const;
		SpatialVector			indexToSpatial(
										const openvdb::Vec3d& a_coord) const;
	private:

	class PointList: public std::vector<openvdb::Vec3R>
	{
	public:
				using PosType = openvdb::Vec3R;

				PointList(void)												{}

		void	getPos(size_t n, openvdb::Vec3R& xyz) const
				{	xyz = operator[](n); }
	};

		I32		nearestIndex(const SpatialVector& a_origin,
						Real a_maxDistance) const;

		void	bindGridBase(openvdb::GridBase::ConstPtr& a_gridPtr);

		String							m_vdbClass;

		openvdb::FloatGrid::ConstPtr	m_floatGridPtr;
		openvdb::Vec3SGrid::ConstPtr	m_cptGridPtr;
		openvdb::Vec3SGrid::ConstPtr	m_normalGridPtr;
		openvdb::Vec3SGrid::ConstPtr	m_uvGridPtr;

		openvdb::tools::PointIndexGrid::Ptr	m_pointGridPtr;

		PointList						m_pointList;

		SpatialVector*					m_pVertexArray;
		SpatialVector*					m_pNormalArray;
		Color*							m_pColorArray;
		SpatialVector*					m_pCptArray;
		Real*							m_pValueArray;
		I32								m_vertexCount;

mutable	CountedPool<Impact>	m_vdbImpactPool;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __vdb_SurfaceVDB_h__ */
