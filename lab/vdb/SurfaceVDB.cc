/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <vdb/vdb.pmh>

#define FE_SVDB_DEBUG			FALSE
#define FE_SVDB_IMPACT_DEBUG	FALSE

#define FE_SVDB_SEARCH_SCALE	2.0

namespace fe
{
namespace ext
{

SurfaceVDB::SurfaceVDB(void):
	m_pVertexArray(NULL),
	m_pNormalArray(NULL),
	m_pColorArray(NULL),
	m_pCptArray(NULL),
	m_pValueArray(NULL),
	m_vertexCount(0)
{
#if	FE_SVDB_DEBUG
	feLog("SurfaceVDB::SurfaceVDB\n");
#endif
}

SurfaceVDB::~SurfaceVDB(void)
{
#if	FE_SVDB_DEBUG
	feLog("SurfaceVDB::~SurfaceVDB\n");
#endif
	deallocate(m_pValueArray);
	deallocate(m_pCptArray);
	deallocate(m_pNormalArray);
	deallocate(m_pColorArray);
	deallocate(m_pVertexArray);
}

Protectable* SurfaceVDB::clone(Protectable* pInstance)
{
	return NULL;
}

void SurfaceVDB::bind(Instance a_instance)
{
	void* pPtr=a_instance.cast<void*>();
	openvdb::GridBase::ConstPtr* pGridPtr=(openvdb::GridBase::ConstPtr*)pPtr;

	bindGridBase(*pGridPtr);
}

openvdb::Vec3d SurfaceVDB::spatialToIndex(const SpatialVector& a_point) const
{
#if FALSE
	openvdb::Vec3d voxel3d=m_cptGridPtr->voxelSize();
	return openvdb::Vec3d(a_point[0]/voxel3d[0],
			a_point[1]/voxel3d[1],a_point[2]/voxel3d[2]);
#else
	const openvdb::math::Transform& rTransform=m_floatGridPtr->transform();
	const openvdb::Vec3d worldPoint(a_point[0],a_point[1],a_point[2]);
	return rTransform.worldToIndex(worldPoint);
#endif
}

SpatialVector SurfaceVDB::indexToSpatial(const openvdb::Vec3d& a_coord) const
{
#if FALSE
	return SpatialVector(a_coord[0]*voxel3d[0],
			a_coord[1]*voxel3d[1],a_coord[2]*voxel3d[2]);
#else
	const openvdb::math::Transform& rTransform=m_floatGridPtr->transform();
	const openvdb::Vec3d worldCpt=rTransform.indexToWorld(a_coord);
	return SpatialVector(worldCpt[0],worldCpt[1],worldCpt[2]);
#endif
}

//* TODO don't generate CPT until needed
void SurfaceVDB::bindGridBase(openvdb::GridBase::ConstPtr& a_gridPtr)
{
	const openvdb::Vec3R origin(0.0,0.0,0.0);

	try
	{
		m_vdbClass=a_gridPtr->metaValue<std::string>("class").c_str();
	}
	catch(...)
	{
		m_vdbClass="";
	}

	feLog("vdbClass \"%s\"\n",m_vdbClass.c_str());

#if	FE_SVDB_DEBUG
	feLog("SurfaceVDB::bindGridBase \"%s\" type \"%s\" valueType \"%s\"\n",
			a_gridPtr->getName().c_str(),
			a_gridPtr->type().c_str(),
			a_gridPtr->valueType().c_str());
#endif

	if(a_gridPtr->getName()=="normal")
	{
		m_normalGridPtr=
				openvdb::gridConstPtrCast<openvdb::Vec3SGrid>(a_gridPtr);
	}
	else if(a_gridPtr->getName()=="uv")
	{
		m_uvGridPtr=
				openvdb::gridConstPtrCast<openvdb::Vec3SGrid>(a_gridPtr);
	}
//	else if(a_gridPtr->getName()=="surface")
	else if(a_gridPtr->valueType()=="float")
	{
		//* NOTE SDF = Signed Distance Function/Field (scalar)
		//* NOTE CPT = Closest Point Transform (vector)

		//* float value appears to be an SDF when VDB class is "level set"
		m_floatGridPtr=
				openvdb::gridConstPtrCast<openvdb::FloatGrid>(a_gridPtr);

		openvdb::CoordBBox bbox=m_floatGridPtr->evalActiveVoxelBoundingBox();
		openvdb::Vec3d indexCenter=bbox.getCenter();
		openvdb::Vec3d min=bbox.min().asVec3d();
		openvdb::Vec3d max=bbox.max().asVec3d();
		openvdb::Vec3d voxel3d=m_floatGridPtr->voxelSize();

		m_center=indexToSpatial(indexCenter);

		//* TODO check min and max
		const SpatialVector corner=indexToSpatial(max);
		m_radius=magnitude(corner-m_center);

		openvdb::tools::Cpt<openvdb::FloatGrid> cpt(*m_floatGridPtr);
		m_cptGridPtr=cpt.process(true,false);

#if	FE_SVDB_DEBUG
		openvdb::tools::BoxSampler sampler;

		const Real floatValue=sampler.sample(m_floatGridPtr->tree(),origin);

		openvdb::Vec3s v3sValue;
		sampler.sample(m_cptGridPtr->tree(),origin,v3sValue);

		feLog("SurfaceVDB::bindGridBase at origin,"
				" sdf %.6G cpt %.6G %.6G %.6G\n",
				floatValue,c_print(m_center),
				v3sValue[0],v3sValue[1],v3sValue[2]);

		bool uniform=m_floatGridPtr->hasUniformVoxels();

		feLog("  center %s radius %.6G\n",c_print(m_center),m_radius);
		feLog("  corner %s\n",c_print(corner));
		feLog("  voxelsize %.6G %.6G %.6G u %d\n",
				voxel3d[0],voxel3d[1],voxel3d[2],uniform);
		feLog("  min %.6G %.6G %.6G max %.6G %.6G %.6G\n",
				min[0],min[1],min[2],max[0],max[1],max[2]);
#endif
	}
}

SurfaceI::Containment SurfaceVDB::containment(
	const SpatialVector& a_origin) const
{
	const openvdb::Vec3d indexOrigin=spatialToIndex(a_origin);

	openvdb::tools::PointSampler sampler;

	openvdb::Vec3s indexCpt;
	const BWORD successCPT=sampler.sample(m_cptGridPtr->tree(),
			indexOrigin,indexCpt);

	//* NOTE exact zero
	if(indexCpt[0]==0.0 && indexCpt[1]==0.0 && indexCpt[2]==0.0)
	{
		return SurfaceI::e_unknown;
	}

	F32 sdfSample=0.0;
	const BWORD successSDF=sampler.sample(m_floatGridPtr->tree(),
			indexOrigin,sdfSample);

	return sdfSample>0.0? SurfaceI::e_outside: SurfaceI::e_inside;
}

sp<SurfaceI::ImpactI> SurfaceVDB::nearestPoint(
	const SpatialVector& a_origin) const
{
	const openvdb::Vec3d indexOrigin=spatialToIndex(a_origin);

	openvdb::Vec3d voxel3d=m_cptGridPtr->voxelSize();

	//* NOTE BoxSampler and QuadraticSampler can give lousy results
//	openvdb::tools::BoxSampler sampler;
//	openvdb::tools::QuadraticSampler sampler;
	openvdb::tools::PointSampler sampler;

	F32 sdfSample=0.0;
	const BWORD successSDF=sampler.sample(m_floatGridPtr->tree(),
			indexOrigin,sdfSample);

	openvdb::Vec3s indexCpt;
	const BWORD successCPT=sampler.sample(m_cptGridPtr->tree(),
			indexOrigin,indexCpt);

	//* NOTE exact zero
	if(indexCpt[0]==0.0 && indexCpt[1]==0.0 && indexCpt[2]==0.0)
	{
		return sp<ImpactI>(NULL);
	}

	const SpatialVector cptSample=indexToSpatial(indexCpt);

/*
	openvdb::Vec3s normalValue;
	const BWORD successNormal=sampler.sample(m_normalGridPtr->tree(),
			indexOrigin,normalValue);
*/

	const SpatialVector towards=unitSafe(cptSample-a_origin);
	const SpatialVector intersection=a_origin+fabs(sdfSample)*towards;

#if	FE_SVDB_IMPACT_DEBUG
	feLog("value %.6G %.6G %.6G success %d %d size %.6G %.6G %.6G\n",
			indexCpt[0],indexCpt[1],indexCpt[2],successSDF,successCPT,
			voxel3d[0],voxel3d[1],voxel3d[2]);
	feLog("near %s sdf %.6G cpt %s\n",c_print(a_origin),
			sdfSample,c_print(cptSample));
#endif

	const SpatialVector voxelSize(voxel3d[0],voxel3d[1],voxel3d[2]);
	const Real radius=FE_SVDB_SEARCH_SCALE*magnitude(voxelSize);
	const I32 nearest=nearestIndex(intersection,radius);
	return computeImpact(a_origin,intersection,nearest);
}

sp<SurfaceI::ImpactI> SurfaceVDB::computeImpact(const SpatialVector& a_origin,
	const SpatialVector& a_intersection,I32 a_vertexIndex) const
{
	const openvdb::Vec3d indexOrigin=spatialToIndex(a_origin);

	openvdb::tools::PointSampler sampler;

	//* NOTE default to lousy normal using spherical approximation
	SpatialVector normal=unitSafe(a_intersection-m_center);
	if(m_normalGridPtr!=NULL)
	{
		openvdb::Vec3s normalValue;
		const BWORD successNormal=sampler.sample(m_normalGridPtr->tree(),
				indexOrigin,normalValue);
		set(normal,normalValue[0],normalValue[1],normalValue[2]);
	}

	SpatialVector uv(0.0,0.0,0.0);
	if(m_uvGridPtr!=NULL)
	{
		openvdb::Vec3s uvValue;
		const BWORD successUV=sampler.sample(m_uvGridPtr->tree(),
				indexOrigin,uvValue);
		set(uv,uvValue[0],uvValue[1],uvValue[2]);
	}

	SpatialVector direction=a_intersection-a_origin;
	const Real distance=magnitude(direction);
	if(distance>0.0)
	{
		direction*=1.0/distance;
	}

#if	FE_SVDB_IMPACT_DEBUG
	feLog("SurfaceVDB::computeImpact vertexIndex %d\n",a_vertexIndex);
	feLog("SurfaceVDB::computeImpact intersect %s dist %.6G dir %s\n",
			c_print(a_intersection),distance,c_print(direction));
	feLog("SurfaceVDB::computeImpact normal %s uv %s\n",
			c_print(normal),c_print(uv));
#endif

	sp<SurfaceVDB::Impact> spImpact=m_vdbImpactPool.get();
	FEASSERT(spImpact.isValid());

	spImpact->setSurface(this);
	spImpact->setLocationLocal(m_center);
	spImpact->setIntersectionLocal(a_intersection);
	spImpact->setNormalLocal(normal);
	spImpact->setPointIndex0(a_vertexIndex);
	spImpact->setVertex0(m_pVertexArray[a_vertexIndex]);
	spImpact->setUV(uv);
	spImpact->setDiffuse(m_diffuse);
	spImpact->setRadius(m_radius);
	spImpact->setOrigin(a_origin);
	spImpact->setDirection(direction);
	spImpact->setDistance(distance);

	return spImpact;
}

I32 SurfaceVDB::nearestIndex(const SpatialVector& a_origin,
	Real a_maxDistance) const
{
	I32 nearest= -1;
	if(m_pointGridPtr==NULL)
	{
		return nearest;
	}

/*
	openvdb::PointIndex32 indexValue;
	const BWORD successIndex=sampler.sample(m_pointGridPtr->tree(),
			indexOrigin,indexValue);
	vertexIndex=indexValue;
	feLog("vertexIndex %d\n",vertexIndex);
*/

	const SpatialVector margin(a_maxDistance,a_maxDistance,a_maxDistance);
	const SpatialVector min=a_origin-margin;
	const SpatialVector max=a_origin+margin;

	const openvdb::Vec3d bmin(min[0],min[1],min[2]);
	const openvdb::Vec3d bmax(max[0],max[1],max[2]);

	openvdb::BBoxd wsBBox(bmin,bmax);

	openvdb::tools::PointIndexGrid::ConstAccessor acc =
			m_pointGridPtr->getConstAccessor();
	openvdb::tools::PointIndexIterator<openvdb::tools::PointIndexTree>
			pointIndexIter;
	pointIndexIter.worldSpaceSearchAndUpdate<PointList>(wsBBox, acc,
			m_pointList, m_pointGridPtr->transform());

	Real best= -1.0;
	for (; pointIndexIter; ++pointIndexIter)
	{
		const I32 vertexIndex=*pointIndexIter;
		const SpatialVector& rPoint=m_pVertexArray[vertexIndex];
		const Real distance=magnitude(rPoint-a_origin);

#if	FE_SVDB_IMPACT_DEBUG
		feLog("SurfaceVDB::nearestIndex vert %d  %s distance %.6G\n",
				vertexIndex,c_print(rPoint),distance);
#endif

		if(nearest<0 || distance<best)
		{
			nearest=vertexIndex;
			best=distance;
		}
	}

#if	FE_SVDB_IMPACT_DEBUG
	feLog("SurfaceVDB::nearestIndex from %s nearest %d\n",
			c_print(a_origin),nearest);
#endif

	return nearest;
}

sp<SurfaceI::ImpactI> SurfaceVDB::rayImpact(
		const SpatialVector& a_origin,const SpatialVector& a_direction,
		Real a_maxDistance,BWORD a_anyHit) const
{
#if	FE_SVDB_IMPACT_DEBUG
	feLog("--------\n");
	feLog("SurfaceVDB::rayImpact origin %s dir %s max %.6G any %d\n",
			c_print(a_origin),c_print(a_direction),a_maxDistance,a_anyHit);
#endif

	const openvdb::Vec3d voxel3d=m_cptGridPtr->voxelSize();
	const SpatialVector voxelSize(voxel3d[0],voxel3d[1],voxel3d[2]);
	const Real radius=FE_SVDB_SEARCH_SCALE*magnitude(voxelSize);
	const Real step=0.5*radius;				//* NOTE tweak

	const Real min=openvdb::math::Delta<openvdb::Real>::value();
			std::numeric_limits<openvdb::Real>::max();
	const Real max=a_maxDistance>Real(0)? a_maxDistance:
			std::numeric_limits<openvdb::Real>::max();
	Vector<3,openvdb::Real> doubleDir(a_direction);
	normalizeSafe(doubleDir);

	const openvdb::Vec3R v3rEye(a_origin[0],a_origin[1],a_origin[2]);
	const openvdb::Vec3R v3rDir(doubleDir[0],doubleDir[1],doubleDir[2]);
	const openvdb::math::Ray<openvdb::Real> ray(v3rEye,v3rDir,min,max);

	openvdb::tools::VolumeRayIntersector<openvdb::FloatGrid> intersector(
			*m_floatGridPtr);
	const bool hit=intersector.setWorldRay(ray);
	if(!hit)
	{
#if	FE_SVDB_IMPACT_DEBUG
		feLog("SurfaceVDB::rayImpact missed\n");
#endif

		return sp<ImpactI>(NULL);
	}

	SpatialVector intersection(0.0,0.0,0.0);
	I32 nearest= -1;
	Real best= -1.0;

	const I32 checkLimit=8;
	I32 checkCount=0;

	double t0=0;
	double t1=0;
	while(checkCount<checkLimit && intersector.march(t0,t1))
	{
		const openvdb::Vec3R v3rHit0=intersector.getWorldPos(t0);
		const openvdb::Vec3R v3rHit1=intersector.getWorldPos(t1);
		const SpatialVector hit0(v3rHit0[0],v3rHit0[1],v3rHit0[2]);
		const SpatialVector hit1(v3rHit1[0],v3rHit1[1],v3rHit1[2]);

#if	FE_SVDB_IMPACT_DEBUG
		feLog("SurfaceVDB::rayImpact hit from %s to %s (%.6G)\n",
				c_print(hit0),
				c_print(hit1),
				magnitude(hit1-hit0));
#endif

		for(double t=t0;t<=t1;t++)
		{
			const openvdb::Vec3R v3rProbe=intersector.getWorldPos(t);
			const SpatialVector probe(v3rProbe[0],v3rProbe[1],v3rProbe[2]);

			const I32 candidate=nearestIndex(probe,radius);
			if(candidate>=0)
			{
				const SpatialVector hitPoint=m_pVertexArray[candidate];
				const Real hitDistance=magnitude(hitPoint-probe);
				if(nearest<0.0 || hitDistance<best)
				{
					nearest=candidate;
					best=hitDistance;
					intersection=hitPoint;
				}
				checkCount++;
			}

			if(checkCount>=checkLimit)
			{
				break;
			}
		}

		//* TODO multiple hits (rayImpacts)
	}

	if(nearest>=0)
	{
		return computeImpact(a_origin,intersection,nearest);
	}

	return sp<ImpactI>(NULL);
}

void SurfaceVDB::cache(void)
{
	if(!m_floatGridPtr)
	{
		feLog("SurfaceVDB::cache no SDF\n");
		return;
	}

#if	FE_SVDB_DEBUG
	feLog("SurfaceVDB::cache\n");
#endif

	openvdb::FloatGrid::ConstAccessor floatAccessor=
			m_floatGridPtr->getConstAccessor();

	openvdb::Vec3SGrid::ConstAccessor* pV3sAccessor=NULL;
	if(m_normalGridPtr!=NULL)
	{
		pV3sAccessor=new openvdb::Vec3SGrid::ConstAccessor(
				m_normalGridPtr->getConstAccessor());
	}

	const openvdb::Vec3d voxel3d=m_cptGridPtr->voxelSize();
	const SpatialVector voxelSize(voxel3d[0],voxel3d[1],voxel3d[2]);

	const openvdb::Index64 activeCount=m_cptGridPtr->activeVoxelCount();

	m_pVertexArray=(SpatialVector*)reallocate(m_pVertexArray,
			activeCount*sizeof(SpatialVector));
	m_pNormalArray=(SpatialVector*)reallocate(m_pNormalArray,
			activeCount*sizeof(SpatialVector));
	m_pCptArray=(SpatialVector*)reallocate(m_pCptArray,
			activeCount*sizeof(SpatialVector));
	m_pValueArray=(Real*)reallocate(m_pValueArray,
			activeCount*sizeof(Real));
	m_vertexCount=0;

#if	FE_SVDB_DEBUG
	feLog("SurfaceVDB::cache"
			" activeCount %d voxelSize %s uniform %d\n",
			activeCount,c_print(voxelSize),m_cptGridPtr->hasUniformVoxels());
#endif

	const BWORD isLevelSet=(m_vdbClass=="level set");

	const openvdb::math::Transform& rTransform=m_cptGridPtr->transform();

	for(openvdb::Vec3SGrid::ValueOnCIter it=m_cptGridPtr->cbeginValueOn();
			it.test();++it)
	{
		if(it.isVoxelValue())
		{
			//* leaf (voxel)
			const openvdb::Coord coord=it.getCoord();

			float floatValue=0.0;
			const BWORD sdfActive=
					floatAccessor.probeValue(coord,floatValue);

			if(!sdfActive)
			{
				continue;
			}

			if(isLevelSet)
			{
				//* HACK thin surface only
				const Real margin=1.0*magnitude(voxelSize);
				if(floatValue<0.0 || floatValue>margin)
				{
					continue;
				}
			}

			const openvdb::Vec3s indexCenter=coord.asVec3s();
			const SpatialVector voxelCenter=indexToSpatial(indexCenter);

			const openvdb::Vec3s& indexCpt=it.getValue();
			const SpatialVector target=indexToSpatial(indexCpt);

			const SpatialVector towards=unitSafe(target-voxelCenter);

			m_pVertexArray[m_vertexCount]=voxelCenter;
			m_pCptArray[m_vertexCount]=target;
			m_pValueArray[m_vertexCount]=floatValue;

			if(pV3sAccessor)
			{
				openvdb::Vec3s normalValue;
				const BWORD normalActive=
						pV3sAccessor->probeValue(coord,normalValue);
				set(m_pNormalArray[m_vertexCount],
						normalValue[0],normalValue[1],normalValue[2]);
			}
			else
			{
				m_pNormalArray[m_vertexCount]= -towards;
			}

			m_vertexCount++;

#if FALSE
			const SpatialVector retarget=
					voxelCenter+fabs(floatValue)*towards;
			if(magnitude(retarget)>magnitude(voxelCenter))
			{
				const openvdb::tree::LeafNode<float,3>* pLeaf=
						floatAccessor.probeConstLeaf(coord);
//				const openvdb::tree::LeafNode<openvdb::Vec3s,3>* pLeaf=
//						v3sAccessor.probeConstLeaf(coord);

				feLog(" leaf %s ret %s val %.6G cpt %.6G %.6G %.6G\n",
						c_print(voxelCenter),c_print(retarget),
						floatValue,indexCpt[0],indexCpt[1],indexCpt[2]);
				feLog(" size %d values %d on %d onLeaf %d empty %d dense %d\n",
						pLeaf->size(),
						pLeaf->numValues(),
						pLeaf->onVoxelCount(),
						pLeaf->onLeafVoxelCount(),
						pLeaf->isEmpty(),
						pLeaf->isDense());
			}
#endif
		}

#if 0
		openvdb::CoordBBox bbox;
		it.getBoundingBox(bbox);
		openvdb::Vec3d v3d=bbox.getCenter();
		feLog(" bbox %.6G %.6G %.6G\n",v3d[0],v3d[1],v3d[2]);
#endif
	}

	m_pointList.resize(m_vertexCount);
	for(I32 index=0;index<m_vertexCount;index++)
	{
		SpatialVector& rVertex=m_pVertexArray[index];
		openvdb::Vec3R& rPoint=m_pointList[index];
		rPoint[0]=rVertex[0];
		rPoint[1]=rVertex[1];
		rPoint[2]=rVertex[2];
	}

	const openvdb::math::Transform::Ptr transform=
			openvdb::math::Transform::createLinearTransform(voxel3d[0]);
	m_pointGridPtr=openvdb::tools::createPointIndexGrid
			<openvdb::tools::PointIndexGrid>(m_pointList, *transform);

#if	FE_SVDB_DEBUG
	feLog("SurfaceVDB::cache using %d/%d voxels\n",m_vertexCount,activeCount);
#endif

	delete pV3sAccessor;

	m_pVertexArray=(SpatialVector*)reallocate(m_pVertexArray,
			m_vertexCount*sizeof(SpatialVector));
	m_pNormalArray=(SpatialVector*)reallocate(m_pNormalArray,
			m_vertexCount*sizeof(SpatialVector));
	m_pColorArray=(Color*)reallocate(m_pColorArray,
			m_vertexCount*sizeof(Color));
	m_pCptArray=(SpatialVector*)reallocate(m_pCptArray,
			m_vertexCount*sizeof(SpatialVector));
	m_pValueArray=(Real*)reallocate(m_pValueArray,
			m_vertexCount*sizeof(Real));

	Real minValue(0);
	Real maxValue(0);
	for(I32 vertexIndex=0;vertexIndex<m_vertexCount;vertexIndex++)
	{
		const Real floatValue=m_pValueArray[vertexIndex];

		if(!vertexIndex || minValue>floatValue)
		{
			minValue=floatValue;
		}
		if(!vertexIndex || maxValue<floatValue)
		{
			maxValue=floatValue;
		}
	}
	const Real scaleValue=
			(maxValue>minValue)? Real(1)/(maxValue-minValue): Real(1);
	for(I32 vertexIndex=0;vertexIndex<m_vertexCount;vertexIndex++)
	{
		const Real floatValue=scaleValue*(m_pValueArray[vertexIndex]-minValue);

		m_pColorArray[vertexIndex]=Color(1-floatValue,floatValue,0.0);
	}
}

void SurfaceVDB::Impact::draw(const SpatialTransform& a_rTransform,
	sp<DrawI> a_spDrawI,const fe::Color* a_pColor,
	sp<DrawBufferI> a_spDrawBuffer,sp<PartitionI> a_spPartition) const
{
	const Color red(1.0,0.0,0.0);
	const Color green(0.0,1.0,0.0);
	const Color darkgreen(0.0,0.7,0.0);
	const Color darkergreen(0.0,0.4,0.0);

	sp<SurfaceVDB> spSurfaceVDB=m_hpSurface;
	FEASSERT(spSurfaceVDB.isValid());

	const openvdb::Vec3d voxel3d=spSurfaceVDB->m_cptGridPtr->voxelSize();
	const SpatialVector voxelSize(voxel3d[0],voxel3d[1],voxel3d[2]);

	if(spSurfaceVDB->m_pointGridPtr)
	{
		const SpatialVector margin=1.5*voxelSize;
		const SpatialVector min=m_intersection-margin;
		const SpatialVector max=m_intersection+margin;

		const openvdb::Vec3d bmin(min[0],min[1],min[2]);
		const openvdb::Vec3d bmax(max[0],max[1],max[2]);

		openvdb::BBoxd wsBBox(bmin,bmax);

		openvdb::tools::PointIndexGrid::ConstAccessor acc =
				spSurfaceVDB->m_pointGridPtr->getConstAccessor();
		openvdb::tools::PointIndexIterator<openvdb::tools::PointIndexTree>
				pointIndexIter;
		pointIndexIter.worldSpaceSearchAndUpdate<PointList>(wsBBox, acc,
				spSurfaceVDB->m_pointList,
				spSurfaceVDB->m_pointGridPtr->transform());

		for(; pointIndexIter; ++pointIndexIter)
		{
			const I32 vertexIndex=*pointIndexIter;
			const SpatialVector& rPoint=
					spSurfaceVDB->m_pVertexArray[vertexIndex];

			const Box3 box(rPoint-0.5*voxelSize,voxelSize);
			a_spDrawI->drawBox(box,darkergreen);
		}
	}

	SpatialVector points[2];
	points[0]=m_intersection;
	points[1]=m_vertex0;

	Color colors[2];
	colors[0]=red;
	colors[1]=green;

	const Box3 box(m_vertex0-0.5*voxelSize,voxelSize);
	a_spDrawI->drawBox(box,darkgreen);

	a_spDrawI->drawTransformedLines(a_rTransform,NULL,
			points,NULL,2,DrawI::e_strip,FALSE,colors,
			FALSE,NULL,NULL,0,a_spDrawBuffer);

	a_spDrawI->drawTransformedPoints(a_rTransform,NULL,
			points,NULL,2,TRUE,colors,a_spDrawBuffer);
}

void SurfaceVDB::draw(const SpatialTransform& a_rTransform,
	sp<DrawI> a_spDrawI,const fe::Color* a_color,
	sp<DrawBufferI> a_spDrawBuffer,sp<PartitionI> a_spPartition) const
{
#if	FE_SVDB_DEBUG
//	feLog("SurfaceVDB::draw m_vertexCount %d\n",m_vertexCount);
#endif

	if(!m_vertexCount && !m_pVertexArray)
	{
		return;
	}

	feLog("STYLE %s\n",c_print(a_spDrawI->drawMode()));

//	const Color white(1.0,1.0,1.0);
	a_spDrawI->drawTransformedPoints(a_rTransform,NULL,
			m_pVertexArray,m_pNormalArray,m_vertexCount,
			a_color? FALSE: TRUE,
			a_color? a_color: m_pColorArray,
			a_spDrawBuffer);
}

} /* namespace ext */
} /* namespace fe */
