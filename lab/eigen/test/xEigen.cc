/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "eigen/eigen.pmh"

using namespace fe;

int main(int argc,char** argv)
{
	UNIT_START();

	BWORD complete=FALSE;

	try
	{
#if FALSE
		sp<Master> spMaster(new Master);
		sp<Registry> spRegistry=spMaster->registry();

		Result result=spRegistry->manage("fexEigenDL");
		UNIT_TEST(successful(result));
#endif

		{
			Eigen::MatrixXd m(2,2);
			m(0,0) = 3;
			m(1,0) = 2.5;
			m(0,1) = -1;
			m(1,1) = m(1,0) + m(0,1);
			std::cout << m << std::endl;
		}

		complete=TRUE;
	}
	catch(Exception &e) { e.log(); }
	catch(...)			{ feLog("uncaught exception\n"); }

	UNIT_TEST(complete);
	UNIT_TEST(!Counted::trackerCount());
	UNIT_TRACK(2);
	UNIT_RETURN();
}
