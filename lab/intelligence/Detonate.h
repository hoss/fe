/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __intelligence_Detonate_h__
#define __intelligence_Detonate_h__
namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Create explosions

	@ingroup intelligence
*//***************************************************************************/
class FE_DL_EXPORT Detonate: public HandlerI
{
	public:
				Detonate(void)												{}

				//* as HandlerI
virtual void	handle(Record &record);


	private:

		Theater					m_theaterRV;
		Periodic				m_periodicRV;
		Sphere					m_sphereRV;
		RecordArrayView<Mortal>	m_mortalRAV;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __intelligence_Detonate_h__ */
