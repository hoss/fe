/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __intelligence_FactionFilter_h__
#define __intelligence_FactionFilter_h__
namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Test a Record for appropriate Faction

	@ingroup intelligence

	Returns non-zero if the test Record has a faction matching the
	interest of the refernce Record.
*//***************************************************************************/
class FE_DL_EXPORT FactionFilter: virtual public FilterI
{
	public:

				//* as FilterI
virtual	void	configure(WeakRecord record);
virtual	I32		test(WeakRecord rRecord);

	private:
		Scanner		m_scannerRV;

		WeakRecord	m_controlRecord;
		I32			m_ignoreFriend;
		I32			m_ignoreNeutral;
		I32			m_ignoreEnemy;
		String		m_faction;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __intelligence_FactionFilter_h__ */
