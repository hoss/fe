/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __intelligence_Control_h__
#define __intelligence_Control_h__
namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Control other particles

	@ingroup intelligence
*//***************************************************************************/
class FE_DL_EXPORT Control: public HandlerI
{
	public:
				Control(void)												{}

				//* as HandlerI
virtual void	handle(Record &record);


	private:

		Theater							m_theaterRV;
		RecordArrayView<Behavior>		m_behaviorRAV;
		RecordArrayView<ControlCenter>	m_controlRAV;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __intelligence_Control_h__ */
