/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __intelligence_DrawParticle_h__
#define __intelligence_DrawParticle_h__
namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Draw each particle

	@ingroup intelligence
*//***************************************************************************/
class FE_DL_EXPORT DrawParticle: public DrawShape, public HandlerI
{
	public:
				DrawParticle(void)											{}

				//* as HandlerI
virtual void	handle(Record &record);

	private:
		sp<SelectorI>				m_spSelector;
		sp<DrawMode>				m_spLine;

		Operator					m_selectOp;
		Theater						m_theaterRV;
		Surveillance				m_surveillanceRV;
		RecordArrayView<Particle>	m_particleRAV;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __intelligence_DrawParticle_h__ */
