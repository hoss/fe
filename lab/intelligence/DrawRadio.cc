/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <intelligence/intelligence.pmh>

#define FE_DP_DRAW_RADIO	TRUE

namespace fe
{
namespace ext
{

void DrawRadio::handle(Record &record)
{
	if(!m_particleRV.scope().isValid())
	{
		sp<Scope> spScope=record.layout()->scope();

		m_particleRV.bind(spScope);

		m_spWireframe=new DrawMode;
		m_spWireframe->setDrawStyle(DrawMode::e_wireframe);
	}

	const Color white(1.0f,1.0f,1.0f,1.0f);
	const Color green(0.0f,1.0f,0.0f,1.0f);
	const Color darkgreen(0.0f,0.3f,0.0f,1.0f);
	const Color grey(0.5f,0.5f,0.5f,1.0f);
	const Color yellow(1.0f,1.0f,0.0f,1.0f);
	const Color red(1.0f,0.0f,0.0f,1.0f);
	const Color pink(1.0f,0.5f,1.0f,1.0f);
	const Color orange(1.0f,0.5f,0.0f,1.0f);
	const Color blue(0.5f,0.5f,1.0f,1.0f);
	const Color purple(0.5f,0.0f,1.0f,1.0f);
	const Color darkpurple(0.3f,0.0f,0.6f,0.5f);
	const Color cyan(0.0f,1.0f,1.0f,0.4f);

	m_theaterRV.bind(record);

	sp<DrawI> spDrawI=m_theaterRV.drawI();
	FEASSERT(spDrawI.isValid());

	sp<RecordGroup> spRG=m_theaterRV.particles();
	FEASSERT(spRG.isValid());

	for(RecordGroup::iterator it=spRG->begin();it!=spRG->end();it++)
	{
		sp<RecordArray> spRA= *it;
		m_radioRAV.bind(spRA);

		if(!m_radioRAV.recordView().neighborhood.check(spRA))
		{
			continue;
		}

		const SpatialVector offset(0.0f,0.4f,0.0f);

		spDrawI->setDrawMode(m_spWireframe);

		for(I32 index=0;index<spRA->length();index++)
		{
			Radio& radioRV=m_radioRAV[index];

#if FE_DP_DRAW_RADIO
			if(m_particleRV.location.check(radioRV.record()))
			{
				const SpatialVector& location=
						m_particleRV.location(radioRV.record());
				const F32 range=radioRV.range();

				drawCircleXY(spDrawI,location,range,darkgreen);
/*
				if(radioRV.neighborhood().isValid())
				{
					Crowd crowdRV;
					crowdRV.bind(radioRV.neighborhood());
					String text;
					text.sPrintf("  %p",crowdRV.viewpoint().data());
					spDrawI->drawAlignedText(
							location+SpatialVector(0.0f,1.0f,0.0f),
							text,yellow);
				}
*/
			}
#endif

			if(!radioRV.neighborhood().isValid())
			{
				continue;
			}

			// NOTE Neighborhood's Neighbors, not the Radio's Neighbors
			sp<RecordGroup> spRG2=
					radioRV.neighbors(radioRV.neighborhood());

			if(spRG2.isValid())
			{
				SpatialVector vertex[2];
				vertex[0]=offset+m_particleRV.location(spRA,index);

				for(RecordGroup::iterator it2=spRG2->begin();
						it2!=spRG2->end();it2++)
				{
					sp<RecordArray> spRA2= *it2;
					if(!m_particleRV.location.check(spRA2) ||
							spRA2->getWeakRecord(0)!=radioRV.record())
					{
						continue;
					}
					for(I32 index2=1;index2<spRA2->length();index2++)
					{
/*
						if(spRA2->getWeakRecord(index2).data()>
								radioRV.record().data())
						{
							continue;
						}
*/

						vertex[1]=offset+
								m_particleRV.location(spRA2,index2);

						//* draw line between radios
						spDrawI->drawLines(vertex,NULL,2,DrawI::e_strip,
								false,&green);
					}
				}
			}
		}

		spDrawI->setDrawMode(sp<DrawMode>(NULL));
	}
}

} /* namespace ext */
} /* namespace fe */
