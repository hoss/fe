/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <intelligence/intelligence.pmh>

namespace fe
{
namespace ext
{

//* NOTE TempI32 used to cache Subservience values

void Dominate::handle(Record& record)
{
//	feLog("\nFRAME\n\n");

	m_theaterRV.bind(record);
	sp<RecordGroup> spRG=m_theaterRV.particles();
	FEASSERT(spRG.isValid());

	//* Decrement all existing subservience
	for(RecordGroup::iterator it=spRG->begin();it!=spRG->end();it++)
	{
		sp<RecordArray> spRA= *it;
		m_controlRAV.bind(spRA);

		if(!m_controlRAV.recordView().subservience.check(spRA))
		{
			continue;
		}

		for(ControlCenter& controlRV: m_controlRAV)
		{
			const I32 subservience=controlRV.subservience();
			I32& rTempI32=controlRV.tempI32();
//			feLog("%d %d\n",subservience,rTempI32);
			rTempI32=(subservience>0)? subservience-1: 0;
		}
	}

	//* Propagate dominance using subservience, decrementing on each rod away
	for(RecordGroup::iterator it=spRG->begin();it!=spRG->end();it++)
	{
		sp<RecordArray> spRA= *it;
		m_rodRAV.bind(spRA);

		if(!m_rodRAV.recordView().length.check(spRA))
		{
			continue;
		}

		for(Rod& rodRV: m_rodRAV)
		{
			// TODO fix or replace binding arbitrary records to RecordView
			// which is inefficient without giving RecordArray

			const Record& particle1=rodRV.particle1();
			const Record& particle2=rodRV.particle2();
			m_controlRV1.bind(particle1);
			m_controlRV2.bind(particle2);

			if(!m_controlRV1.subservience.check() ||
					!m_controlRV2.subservience.check())
			{
				continue;
			}

			const I32 dominance1=m_controlRV1.dominance.safeGet();
			const I32 dominance2=m_controlRV2.dominance.safeGet();

			const I32 subservience1=m_controlRV1.subservience();
			const I32 subservience2=m_controlRV2.subservience();

			const I32 subserviencePlus1=m_controlRV1.tempI32();
			const I32 subserviencePlus2=m_controlRV2.tempI32();

#if FALSE
			feLog("\ndominance %d %d subservience %d %d subservience+ %d %d\n",
					dominance1,dominance2,
					subservience1,subservience2,
					subserviencePlus1,subserviencePlus2);
#endif

			if(dominance2<dominance1 && !subservience1 &&
					subserviencePlus2<dominance1)
			{
				wire(m_controlRV2,m_controlRV1,dominance1);
			}
			else if(dominance1<dominance2 && !subservience2 &&
					subserviencePlus1<dominance2)
			{
				wire(m_controlRV1,m_controlRV2,dominance2);
			}
			else if(dominance2<subservience1-1 &&
					subserviencePlus2<subservience1-1)
			{
				wireLike(m_controlRV2,m_controlRV1,subservience1-1);
			}
			else if(dominance1<subservience2-1 &&
					subserviencePlus1<subservience2-1)
			{
				wireLike(m_controlRV1,m_controlRV2,subservience2-1);
			}
		}
	}

	//* Excommunicate zero-strength subservients
	for(RecordGroup::iterator it=spRG->begin();it!=spRG->end();it++)
	{
		sp<RecordArray> spRA= *it;
		m_controlRAV.bind(spRA);

		if(!m_controlRAV.recordView().subservience.check(spRA))
		{
			continue;
		}

		for(ControlCenter& controlRV: m_controlRAV)
		{
			I32& rSubservience=controlRV.subservience();
			const I32 subserviencePlus=controlRV.tempI32();
			rSubservience=subserviencePlus;
//			feLog("%d %d\n",rSubservience,subserviencePlus);
			if(!rSubservience)
			{
				WeakRecord& rControlRecord=controlRV.controlRecord();

				if(rControlRecord.isValid())
				{
					unwire(controlRV,rControlRecord);
				}
			}
		}
	}
}

void Dominate::unwire(ControlCenter& controlled,WeakRecord& rOldControlRecord)
{
	ControlCenter oldControlCenter;
	oldControlCenter.bind(rOldControlRecord);

	sp<RecordGroup>& rspOldRG=oldControlCenter.controlled();
	FEASSERT(rspOldRG.isValid());
	rspOldRG->remove(controlled.record());
}

void Dominate::wireLike(ControlCenter& controlled,
		ControlCenter& controlCenter,I32 subservience)
{
	WeakRecord& rControlRecord=controlCenter.controlRecord();

	FEASSERT(rControlRecord.isValid());

	//* NOTE reusing RV
	controlCenter.bind(rControlRecord);
	wire(controlled,controlCenter,subservience);
}

void Dominate::wire(ControlCenter& controlled,
		ControlCenter& controlCenter,I32 subservience)
{
#if FALSE
	feLog("\nwire controlled %p:\n",controlled.record().data());
	controlled.dump();
	feLog("to controlCenter %p:\n",controlCenter.record().data());
	controlCenter.dump();
#endif

	WeakRecord controlRecord=controlCenter.record();
	WeakRecord& rOldControlRecord=controlled.controlRecord();

	//* update only if changed
	if(!rOldControlRecord.isValid() || rOldControlRecord!=controlRecord)
	{
		if(rOldControlRecord.isValid())
		{
			unwire(controlled,rOldControlRecord);
		}

		controlled.controlRecord()=controlRecord;

		sp<RecordGroup>& rspNewRG=controlCenter.controlled();
		FEASSERT(rspNewRG.isValid());
		rspNewRG->add(controlled.record());
	}

	controlled.tempI32()=subservience;

#if FALSE
	feLog("\n results controlled:\n");
	controlled.dump();
	feLog("to controlCenter:\n");
	controlCenter.dump();
#endif

}

} /* namespace ext */
} /* namespace fe */
