/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <intelligence/intelligence.pmh>

namespace fe
{
namespace ext
{

void Detonate::handle(Record &record)
{
	if(!m_sphereRV.scope().isValid())
	{
		sp<Scope> spScope=record.layout()->scope();

		m_sphereRV.bind(spScope);
	}

	m_theaterRV.bind(record);
	sp<RecordGroup> spRG=m_theaterRV.particles();
	FEASSERT(spRG.isValid());

	for(RecordGroup::iterator it=spRG->begin();it!=spRG->end();it++)
	{
		sp<RecordArray> spRA= *it;
		m_mortalRAV.bind(spRA);

		if(!m_mortalRAV.recordView().deathSpawn.check(spRA))
		{
			continue;
		}

		for(Mortal& mortalRV: m_mortalRAV)
		{
			m_periodicRV.bind(mortalRV.record());
			I32& backlog=m_periodicRV.backlog();
			if(backlog && !(mortalRV.flags()&Matter::e_discard))
			{
				backlog=0;

//				feLog("Detonate::handle spawn \"%s\"\n",
//						mortalRV.deathSpawn().c_str());

				Record spawn=mortalRV.scope()->produceRecord(
						mortalRV.deathSpawn());

				m_sphereRV.bind(spawn);
				m_sphereRV.location()=mortalRV.location();

				spRG->add(spawn);

				const String& deathSong=mortalRV.deathSong();
				if(!deathSong.empty())
				{
					mortalRV.song()="";
					mortalRV.nextSong()=deathSong;
					mortalRV.nextPlays()=1;
					mortalRV.nextGain()=3;

					sp<VoiceI> spVoiceI=mortalRV.voice();
					if(spVoiceI.isValid())
					{
						spVoiceI->persist(TRUE);
					}
				}

				mortalRV.flags()|=Matter::e_discard;
			}
		}
	}

	m_periodicRV.unbind();
}

} /* namespace ext */
} /* namespace fe */
