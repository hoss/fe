/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __intelligence_Periodic_h__
#define __intelligence_Periodic_h__
namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Periodic RecordView

	@ingroup intelligence
*//***************************************************************************/
class FE_DL_EXPORT Periodic: virtual public RecordView
{
	public:
		Functor<I32>				periodMS;
		Functor<I32>				readyMS;
		Functor<I32>				backlog;

				Periodic(void)			{ setName("Periodic"); }
virtual	void	addFunctors(void)
				{
					add(periodMS,	FE_SPEC("ai:periodMS",
							"How often we can act"));
					add(readyMS,	FE_SPEC("ai:readyMS",
							"When to act next"));
					add(backlog,	FE_SPEC("ai:backlog",
							"Count of unused triggers"));
				}
virtual	void	initializeRecord(void)
				{
					periodMS()=1000;
					readyMS()=0;
					backlog()=0;
				}
};

} /* namespace ext */
} /* namespace fe */

#endif /* __intelligence_Periodic_h__ */
