/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <intelligence/intelligence.pmh>

namespace fe
{
namespace ext
{

void Attract::handle(Record& record)
{
	if(!m_cylinderRV.scope().isValid())
	{
		sp<Scope> spScope=record.layout()->scope();

		m_cylinderRV.bind(spScope);
	}

	m_theaterRV.bind(record);
	sp<RecordGroup> spRG=m_theaterRV.particles();
	FEASSERT(spRG.isValid());

	for(RecordGroup::iterator it=spRG->begin();it!=spRG->end();it++)
	{
		sp<RecordArray>& rspRA= *it;
		m_attractorRAV.bind(rspRA);

		if(!m_attractorRAV.recordView().attraction.check(rspRA))
		{
			continue;
		}

		for(Attractor& attractorRV: m_attractorRAV)
		{
			m_observationRV.bind(attractorRV.purview());
			SpatialVector& span=m_cylinderRV.span(m_observationRV.shape());
			const F32 range=magnitude(span);
//			const SpatialVector unitSpan=span/range;

			const SpatialVector& location=attractorRV.location();
			const F32 attraction=attractorRV.attraction();
			const F32 focus=attractorRV.focus();
			const sp<RecordGroup>& rspSelection=attractorRV.selection();

			for(RecordGroup::iterator it2=rspSelection->begin();
					it2!=rspSelection->end();it2++)
			{
				sp<RecordArray>& rspRA2= *it2;
				if(!attractorRV.mass.check(rspRA2))
				{
					continue;
				}

				m_matterRAV.bind(rspRA2);
				for(Matter& matterRV: m_matterRAV)
				{
					const SpatialVector& location2=matterRV.location();
					const F32 radius2=matterRV.radius();
					const SpatialVector difference=location2-location;
					const F32 distance=magnitude(difference);
//					const F32 dotDistance=dot(difference,unitSpan);
					const SpatialVector force=difference*
							(fe::pi*radius2*radius2)*
							(attraction/distance*
							fe::minimum((distance-focus)/focus,1.0f)*
							fe::maximum(range-distance,0.0f)/(range*range));

#if FALSE
					feLog("attract %s force %s diff %s dist %.6G %.6G/%.6G\n",
							matterRV.identifier().c_str(),
							print(force).c_str(),print(difference).c_str(),
							distance,dotDistance,range);
#endif

					attractorRV.force()+=force;
					matterRV.force()-=force;
				}
			}
		}
	}
}

} /* namespace ext */
} /* namespace fe */
