/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <intelligence/intelligence.pmh>

namespace fe
{
namespace ext
{

void LuaBehavior::handle(Record &record)
{
	if(!m_controlRV.scope().isValid())
	{
		m_controlRV.bind(record.layout()->scope());
	}

	m_controlRV.bind(record);
//	m_controlRV.dump();

	if(!m_controlRV.behaviors.check())
	{
		feLog("LuaBehavior::handle not a ControlCenter\n");
		return;
	}

	m_behaviorRV.bind(m_recorded);
	if(!m_spLuaI.isValid())
	{
		const String& rScriptName=m_behaviorRV.behaviorName();

		if(!rScriptName.empty())
		{
			m_spLuaI=m_controlRV.scope()->registry()->create("LuaI");
			FEASSERT(m_spLuaI.isValid());
			m_spLuaI->setName("control LuaI");

//			feLog("LuaBehavior::handle load \"%s\"\n",rScriptName.c_str());
			m_spLuaI->loadFile(rScriptName);
		}
	}

	if(m_spLuaI.isValid())
	{
		I32 timeMS=I32(1000.0f*m_controlRV.time());

#if TRUE
		//* TODO Can this be handled generically somewhere else?
		if(m_behaviorRV.readyMS()<I32(timeMS))
		{
			m_behaviorRV.readyMS()+=m_behaviorRV.periodMS();
			m_behaviorRV.backlog()++;

/*
			feLog("LuaBehavior::handle"
					" %p period %d tick %d vs %d backlog=%d\n",
					this,m_behaviorRV.periodMS(),timeMS,
					m_behaviorRV.readyMS(),m_behaviorRV.backlog());
*/
		}
#endif

		I32& backlog=m_behaviorRV.backlog();
		if(backlog)
		{
//			feLog("LuaBehavior::handle %p backlog=%d\n",this,backlog);
			backlog=0;
			m_spLuaI->set("controlCenter",record);
			m_spLuaI->execute();
		}
	}
}

} /* namespace ext */
} /* namespace fe */