/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __intelligence_Construct_h__
#define __intelligence_Construct_h__
namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Operate building abilities

	@ingroup intelligence
*//***************************************************************************/
class FE_DL_EXPORT Construct: virtual public HandlerI
{
	public:
						Construct(void)										{}

						//* as HandlerI
virtual void			handle(Record &record);

	private:
		sp<RecordGroup>	generate(sp<RecordGroup> spPrototype);
		BWORD			build(sp<RecordGroup> spPrototype,
								sp<RecordGroup> spSelection,F32 deltaT);

		Theater						m_theaterRV;
		Matter						m_matterRV2;
		RecordArrayView<Foundry>	m_foundryRAV;
		RecordArrayView<Matter>		m_matterRAV;
		RecordArrayView<Matter>		m_matterRAV3;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __intelligence_Construct_h__ */
