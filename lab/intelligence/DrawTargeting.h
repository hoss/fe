/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __intelligence_DrawTargeting_h__
#define __intelligence_DrawTargeting_h__
namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Draw targeting info

	@ingroup intelligence
*//***************************************************************************/
class FE_DL_EXPORT DrawTargeting: public DrawShape, public HandlerI
{
	public:
				DrawTargeting(void)											{}

				//* as HandlerI
virtual void	handle(Record &record);

	private:
		Theater						m_theaterRV;
		RecordArrayView<Targeter>	m_targeterRAV;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __intelligence_DrawTargeting_h__ */
