/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __intelligence_h__
#define __intelligence_h__

#include "element/element.h"

#if FE_CODEGEN<=FE_DEBUG
#define FEX_BEH_DRAW_TEXT	TRUE
#else
#define FEX_BEH_DRAW_TEXT	FALSE
#endif

#include "intelligence/DirectorI.h"

#include "intelligence/Matter.h"
#include "intelligence/Audible.h"
#include "intelligence/Periodic.h"
#include "intelligence/Behavior.h"
#include "intelligence/Mortal.h"
#include "intelligence/Controlled.h"
#include "intelligence/Thruster.h"
#include "intelligence/Surveillance.h"
#include "intelligence/Observation.h"
#include "intelligence/Crowd.h"
#include "intelligence/Radio.h"
#include "intelligence/ControlCenter.h"
#include "intelligence/Targeter.h"
#include "intelligence/Cannon.h"
#include "intelligence/Scanner.h"
#include "intelligence/Attractor.h"
#include "intelligence/Foundry.h"

#include "intelligence/Theater.h"

#endif /* __intelligence_h__ */
