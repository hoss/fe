/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __intelligence_Attract_h__
#define __intelligence_Attract_h__
namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Employ attraction

	@ingroup intelligence
*//***************************************************************************/
class FE_DL_EXPORT Attract: virtual public HandlerI
{
	public:
				Attract(void)												{}

				//* as HandlerI
virtual void	handle(Record &record);

	private:
		Theater						m_theaterRV;
		Observation					m_observationRV;
		Cylinder					m_cylinderRV;
		RecordArrayView<Attractor>	m_attractorRAV;
		RecordArrayView<Matter>		m_matterRAV;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __intelligence_Attract_h__ */
