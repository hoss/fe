/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __intelligence_Thruster_h__
#define __intelligence_Thruster_h__
namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Thruster RecordView

	@ingroup intelligence
*//***************************************************************************/
class FE_DL_EXPORT Thruster: public Controlled
{
	public:
		Functor<SpatialVector>		thrust;
		Functor<F32>				thrustLimit;

				Thruster(void)			{ setName("Thruster"); }
virtual	void	addFunctors(void)
				{
					Controlled::addFunctors();

					add(thrust,			FE_SPEC("ai:thrust",
							"Current propulsion"));
					add(thrustLimit,	FE_SPEC("ai:thrustLimit",
							"Maximum propulsion"));
				}
virtual	void	initializeRecord(void)
				{
					Controlled::initializeRecord();

					identifier()="Thruster";

//					nextSong()="thrust";

					set(thrust());
					thrustLimit()=1.0f;
				}
};

} /* namespace ext */
} /* namespace fe */

#endif /* __intelligence_Thruster_h__ */
