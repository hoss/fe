/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __intelligence_Radio_h__
#define __intelligence_Radio_h__
namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Radio RecordView

	@ingroup intelligence
*//***************************************************************************/
class FE_DL_EXPORT Radio: virtual public RecordView
{
	public:
		Functor<F32>				range;
		Functor<Record>				neighborhood;
		Functor<Record>				newNeighborhood;
		Functor< sp<RecordGroup> >	neighbors;

				Radio(void)		{ setName("Radio"); }
virtual	void	addFunctors(void)
				{
					add(range,				FE_SPEC("comm:range",
							"Radius of effect"));
					add(neighborhood,		FE_SPEC("comm:crowd",
							"Cooperative network"));
					add(newNeighborhood,	FE_SPEC("comm:newcrowd",
							"Crowd update during partitioning"));
					add(neighbors,			FE_USE("comm:neighbors"));
				}
virtual	void	initializeRecord(void)
				{
					neighborhood.attribute()->setCloneable(FALSE);
					neighbors.attribute()->setCloneable(FALSE);

					Crowd crowdRV;
					crowdRV.bind(scope());
					neighborhood()=crowdRV.createRecord();
					crowdRV.neighbors()->add(record());

					neighbors.createAndSetRecordGroup();
					neighbors()->setWeak(TRUE);

					range()=1.0f;
				}
};

} /* namespace ext */
} /* namespace fe */

#endif /* __intelligence_Radio_h__ */
