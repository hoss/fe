/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <intelligence/intelligence.pmh>

namespace fe
{
namespace ext
{

void Discard::handle(Record& record)
{
	m_theaterRV.bind(record);
	sp<RecordGroup> spRG=m_theaterRV.particles();
	FEASSERT(spRG.isValid());

	for(RecordGroup::iterator it=spRG->begin();it!=spRG->end();it++)
	{
		sp<RecordArray> spRA= *it;
		m_matterRAV.bind(spRA);

		if(!m_matterRAV.recordView().flags.check(spRA))
		{
			continue;
		}

		for(I32 index=0;index<spRA->length();)
		{
			Matter& matterRV=m_matterRAV[index];

			const I32 flags=matterRV.flags();
			if(flags&Matter	::e_discard)
			{
//				feLog("Discard::handle remove %s\n",
//						matterRV.identifier().c_str());

				Record record=matterRV.record();
				spRG->remove(record);
			}
			else
			{
				index++;
			}
		}
	}
}

} /* namespace ext */
} /* namespace fe */
