/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __intelligence_DrawRadio_h__
#define __intelligence_DrawRadio_h__
namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Draw each communication span

	@ingroup intelligence
*//***************************************************************************/
class FE_DL_EXPORT DrawRadio: public DrawShape, public HandlerI
{
	public:
				DrawRadio(void)											{}

				//* as HandlerI
virtual void	handle(Record &record);

	private:
		sp<DrawMode>			m_spWireframe;

		Theater					m_theaterRV;
		Particle				m_particleRV;
		RecordArrayView<Radio>	m_radioRAV;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __intelligence_DrawRadio_h__ */
