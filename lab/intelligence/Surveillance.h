/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __intelligence_Surveillance_h__
#define __intelligence_Surveillance_h__
namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Surveillance RecordView

	@ingroup intelligence
*//***************************************************************************/
class FE_DL_EXPORT Surveillance: public RecordView
{
	public:
		Functor< sp<RecordGroup> >	observations;
		Functor< sp<RecordGroup> >	proxyGroup;
		Functor<I32>				serial;

				Surveillance(void)		{ setName("Surveillance"); }
virtual	void	addFunctors(void)
				{
					add(observations,	FE_SPEC("bnd:observeRG",
							"Shapes totaling what can be seen"));
					add(proxyGroup,		FE_SPEC("ai:proxyRG",
							"Set of proxies for what can be seen"));
					add(serial,			FE_USE(":SN"));
				}
virtual	void	initializeRecord(void)
				{
					observations.createAndSetRecordGroup();
					proxyGroup.createAndSetRecordGroup();
				}
};

} /* namespace ext */
} /* namespace fe */

#endif /* __intelligence_Surveillance_h__ */
