/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __intelligence_Targeter_h__
#define __intelligence_Targeter_h__
namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Targeter RecordView

	@ingroup intelligence
*//***************************************************************************/
class FE_DL_EXPORT Targeter: public Controlled
{
	public:
		Functor<SpatialVector>		target;
		Functor<I32>				requests;

				Targeter(void)		{ setName("Targeter"); }
virtual	void	addFunctors(void)
				{
					Controlled::addFunctors();

					add(target,		FE_SPEC("ai:target",
							"Location of intended impact"));
					add(requests,	FE_SPEC("ai:requests",
							"Number of actions queued"));
				}
virtual	void	initializeRecord(void)
				{
					Controlled::initializeRecord();

					identifier()="Targeter";
					requests()=0;
				}
};

} /* namespace ext */
} /* namespace fe */

#endif /* __intelligence_Targeter_h__ */
