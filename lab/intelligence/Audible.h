/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __intelligence_Audible_h__
#define __intelligence_Audible_h__
namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Audible RecordView

	@ingroup intelligence

	To queue a sound, set @em nextSong.
	To play a new sound immediately, clear @em song and set @em nextSong.
	To stop, clear both.

	To queue as looping, set @em nextPlays to 0.
	Otherwise, set a number of repititions.

	The @em nextPlays attribute is cleared once a new sound is started.
*//***************************************************************************/
class FE_DL_EXPORT Audible: public Matter
{
	public:
		Functor< sp<Component> >	voice;
		Functor<String>				song;
		Functor<String>				nextSong;
		Functor<I32>				nextPlays;
		Functor<F32>				gain;
		Functor<F32>				pitch;
		Functor<F32>				nextGain;
		Functor<F32>				nextPitch;

				Audible(void)			{ setName("Audible"); }
virtual	void	addFunctors(void)
				{
					Matter::addFunctors();

					add(voice,		FE_SPEC("snd:voice",
							"sp<VoiceI> created as needed"));
					add(song,		FE_SPEC("snd:song",
							"Name of sound emitted"));
					add(nextSong,	FE_SPEC("snd:nextSong",
							"Next sound to play"));
					add(nextPlays,	FE_SPEC("snd:nextPlays",
							"Number of times to play next sound (0 for loop)"));
					add(gain,		FE_SPEC("snd:gain",
							"Fractional volume of sound emitted"));
					add(pitch,		FE_SPEC("snd:pitch",
							"Relative pitch of sound emitted"));
					add(nextGain,	FE_SPEC("snd:nextGain",
							"Fractional volume of next sound"));
					add(nextPitch,	FE_SPEC("snd:nextPitch",
							"Relative pitch of next sound"));
				}
virtual	void	initializeRecord(void)
				{
					Matter::initializeRecord();

					voice.attribute()->setSerialize(FALSE);

					identifier()="Audible";

					voice()=NULL;
					song()="";
					nextSong()="";
					nextPlays()=0;
					gain()=1.0f;
					nextGain()=1.0f;
					pitch()=1.0f;
					nextPitch()=1.0f;
				}
};

} /* namespace ext */
} /* namespace fe */

#endif /* __intelligence_Audible_h__ */
