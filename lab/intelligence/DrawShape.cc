/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <intelligence/intelligence.pmh>

namespace fe
{
namespace ext
{

void DrawShape::draw(sp<DrawI> spDrawI,Record shape,const Color& color)
{
	if(!m_cylinderRV.scope().isValid())
	{
		sp<Scope> spScope=shape.layout()->scope();

		m_cylinderRV.bind(spScope);
	}

	const SpatialVector& location=m_cylinderRV.location(shape);

	if(m_cylinderRV.span.check(shape))
	{
		const F32 baseRadius=m_cylinderRV.baseRadius(shape);
		const F32 endRadius=m_cylinderRV.endRadius(shape);
		const SpatialVector& span=m_cylinderRV.span(shape);
		F32 range=magnitude(span);

		SpatialQuaternion rotation;
		set(rotation,SpatialVector(0.0f,0.0f,1.0f),span/range);

		m_transform=rotation;
		setTranslation(m_transform,location);

		SpatialVector scale(endRadius,endRadius,range);

		spDrawI->drawCylinder(m_transform,&scale,baseRadius/endRadius,color,0);
	}
	else
	{
		const F32 radius=m_cylinderRV.radius(shape);
		drawSphere(spDrawI,location,radius,color);
	}
}

void DrawShape::drawSphere(sp<DrawI> spDrawI,const SpatialVector& location,
		F32 radius,const Color& color)
{
	setIdentity(m_transform);
	translate(m_transform,location);
	rotate(m_transform,90.0f*degToRad,e_xAxis);
	SpatialVector scale(radius,radius,radius);
	spDrawI->drawSphere(m_transform,&scale,color);
}

void DrawShape::drawCircleXY(sp<DrawI> spDrawI,const SpatialVector& location,
		F32 radius,const Color& color)
{
	setIdentity(m_transform);
	translate(m_transform,location);
//	rotate(m_transform,90.0f*degToRad,e_xAxis);
	SpatialVector scale(radius,radius,radius);
	spDrawI->drawCircle(m_transform,&scale,color);
}

} /* namespace ext */
} /* namespace fe */