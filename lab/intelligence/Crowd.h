/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __intelligence_Crowd_h__
#define __intelligence_Crowd_h__
namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Crowd RecordView

	@ingroup intelligence
*//***************************************************************************/
class FE_DL_EXPORT Crowd: virtual public RecordView
{
	public:
		Functor<Record>				viewpoint;
		Functor< sp<RecordGroup> >	neighbors;
		Functor<I32>				serial;

				Crowd(void)		{ setName("Crowd"); }
virtual	void	addFunctors(void)
				{
					add(viewpoint,	FE_SPEC("ai:viewpoint",
							"Surveillance of what can be seen"));

					add(neighbors,	FE_SPEC("comm:neighbors",
							"Group of nearby Records with similar interests"));

					add(serial,		FE_USE(":SN"));
				}
virtual	void	initializeRecord(void)
				{
					Surveillance surveillanceRV;
					surveillanceRV.bind(scope());
					viewpoint()=surveillanceRV.createRecord();

					neighbors.createAndSetRecordGroup();
					neighbors()->setWeak(TRUE);
				}
};

} /* namespace ext */
} /* namespace fe */

#endif /* __intelligence_Crowd_h__ */
