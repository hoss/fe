/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __intelligence_Scanner_h__
#define __intelligence_Scanner_h__
namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Scanner RecordView

	@ingroup intelligence
*//***************************************************************************/
class FE_DL_EXPORT Scanner: public Controlled
{
	public:
		Functor<SpatialVector>		track;
		Functor<SpatialQuaternion>	autoRotate;
		Functor<SpatialQuaternion>	rotVelocity;
		Functor< sp<RecordGroup> >	selection;
		Functor<Record>				purview;
		Functor<Record>				nearest;
		Functor<F32>				maxRotVelocity;
		Functor<F32>				maxRotAcceleration;
		Functor<I32>				tracking;
		Functor<I32>				ignoreFriend;
		Functor<I32>				ignoreNeutral;
		Functor<I32>				ignoreEnemy;

				Scanner(void)			{ setName("Scanner"); }
virtual	void	addFunctors(void)
				{
					Controlled::addFunctors();

					add(track,				FE_SPEC("ai:track",
							"Location of attention"));
					add(autoRotate,			FE_SPEC("ai:autoRotate",
							"Default Rotational Velocity"));
					add(rotVelocity,		FE_SPEC("ai:rotVelocity",
							"Current Rotational Velocity"));
					add(selection,			FE_SPEC("ai:selection",
							"RG of interest"));
					add(purview,			FE_SPEC("ai:observation",
							"Record describing region of sight"));
					add(nearest,			FE_SPEC("ai:nearest",
							"The nearest Record in selection"));
					add(maxRotVelocity,		FE_SPEC("ai:maxRotVel",
							"Max angular velocity in degrees/second"));
					add(maxRotAcceleration,	FE_SPEC("ai:maxRotAccel",
							"Max angular acceleration in degrees/second^2"));
					add(tracking,			FE_SPEC("ai:tracking",
							"Flag to track a point"));
					add(ignoreFriend,		FE_SPEC("ai:ignFriend",
							"Flag to ignore friends"));
					add(ignoreNeutral,		FE_SPEC("ai:ignNeutral",
							"Flag to ignore those neither friend nor enemy"));
					add(ignoreEnemy,		FE_SPEC("ai:ignEnemy",
							"Flag to ignore enemies"));
				}
virtual	void	initializeRecord(void)
				{
					Controlled::initializeRecord();

					identifier()="Scanner";

					Observation observationRV;
					observationRV.bind(scope());
					purview()=observationRV.createRecord();
					observationRV.observer()=record();

					Cylinder cylinderRV;
					cylinderRV.bind(scope());
					observationRV.shape()=cylinderRV.createRecord();

					set(cylinderRV.span(),8.0f);
					cylinderRV.baseRadius()=0.4f;
					cylinderRV.endRadius()=4.0f;

					//* per ms
					autoRotate()=SpatialQuaternion(3.0f,e_zAxis);
					set(rotVelocity());

					maxRotVelocity()=1.0f;
					maxRotAcceleration()=1.0f;
					tracking()=false;
					ignoreFriend()=false;
					ignoreNeutral()=false;
					ignoreEnemy()=false;
				}
};

} /* namespace ext */
} /* namespace fe */

#endif /* __intelligence_Scanner_h__ */
