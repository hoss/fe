/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <intelligence/intelligence.pmh>

namespace fe
{
namespace ext
{

void FactionFilter::configure(WeakRecord record)
{
	m_scannerRV.bind(record);

	m_controlRecord=m_scannerRV.controlRecord();
	m_ignoreFriend=m_scannerRV.ignoreFriend();
	m_ignoreNeutral=m_scannerRV.ignoreNeutral();
	m_ignoreEnemy=m_scannerRV.ignoreEnemy();
	m_faction=m_scannerRV.faction();
}

I32	FactionFilter::test(WeakRecord record)
{
	//* presumed only up through Controlled
	if(!m_scannerRV.faction.check(record))
	{
		//* no faction attribute means neutral
		return !m_ignoreNeutral;
	}

	//* never include your own control center or its controlled records
	if(record==m_controlRecord || (m_scannerRV.controlRecord.check(record) &&
			m_scannerRV.controlRecord(record)==m_controlRecord))
	{
		return I32(FALSE);
	}

	const String& opposition=m_scannerRV.faction(record);

	bool neutral= m_faction.empty() || opposition.empty();
	bool friendly= !neutral && m_faction==opposition;

	return	!( (m_ignoreNeutral && neutral) ||

			(m_ignoreFriend && friendly) ||

			(m_ignoreEnemy && !friendly && !neutral) );
}

} /* namespace ext */
} /* namespace fe */