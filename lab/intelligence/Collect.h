/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __intelligence_Collect_h__
#define __intelligence_Collect_h__
namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Collect knowledge from controlled particles

	@ingroup intelligence
*//***************************************************************************/
class FE_DL_EXPORT Collect: public HandlerI
{
	public:
				Collect(void)												{}

				//* as HandlerI
virtual void	handle(Record &record);

	private:

		void	checkSurveillance(Record& rViewpoint,
						Record rControl,Record& rPurview);

		Theater							m_theaterRV;
		Surveillance					m_surveillanceRV;
		Controlled						m_controlledRV;
		Observation						m_observeRV;
		RecordArrayView<ControlCenter>	m_controlRAV;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __intelligence_Collect_h__ */
