/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __intelligence_Observation_h__
#define __intelligence_Observation_h__

FE_ATTRIBUTE("sim:spectrum",
		"Named domain of exposure (visible, infrared, radar)");
namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Observation RecordView

	@ingroup intelligence
*//***************************************************************************/
class FE_DL_EXPORT Observation: public RecordView
{
	public:
		Functor<Record>			shape;
		Functor<String>			spectrum;
		Functor<WeakRecord>		observer;
		Functor<WeakRecord>		viewpoint;

				Observation(void)		{ setName("Observation"); }
virtual	void	addFunctors(void)
				{
					add(shape,		FE_USE("bnd:shape"));
					add(spectrum,	FE_USE("sim:spectrum"));

					add(observer,	FE_SPEC("node:owner",
							"Link back to Record that owns this Record"));

					add(viewpoint,	FE_SPEC("node:group",
							"Link to a RecordGroup that is using this Record"));
				}
virtual	void	initializeRecord(void)
				{
					spectrum()="visible";
				}
};

} /* namespace ext */
} /* namespace fe */

#endif /* __intelligence_Observation_h__ */
