/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <intelligence/intelligence.pmh>

#define	FE_COMMUNICATE_DEBUG	FALSE

namespace fe
{
namespace ext
{

void Communicate::handle(Record &record)
{
	if(!m_radioRAV.scope().isValid())
	{
		sp<Scope> spScope=record.layout()->scope();

		m_radioRAV.bind(spScope);

		m_aLocation.initialize(spScope,FE_USE("spc:at"));
		m_aFaction.initialize(spScope,FE_USE("ai:faction"));
	}

	m_theaterRV.bind(record);

	sp<RecordGroup>& rspRG=m_theaterRV.particles();
	FEASSERT(rspRG.isValid());

	//*		`Update Radio Neighbors'
	for(RecordGroup::iterator it=rspRG->begin();it!=rspRG->end();it++)
	{
		sp<RecordArray> spRA= *it;
		m_radioRAV.bind(spRA);

		if(!m_radioRAV.recordView().neighborhood.check(spRA))
		{
			continue;
		}

		for(I32 index=0;index<spRA->length();index++)
		{
			Radio& radioRV=m_radioRAV[index];

			radioRV.neighbors()->setWeak(TRUE);
			WeakRecord rRadio1=radioRV.record();

//			radioRV.NewNeighborhood()=Record();

			const F32 range=radioRV.range();
			const SpatialVector& location=m_aLocation(rRadio1);
			const String& faction=m_aFaction(rRadio1);

			//* find other Radios
			I32 start=index+1;
			for(RecordGroup::iterator it2=it;it2!=rspRG->end();it2++)
			{
				sp<RecordArray> spRA2= *it2;
				if(m_radioRAV.recordView().neighborhood.check(spRA2))
				{
					m_radioRAV2.bind(spRA2);
					for(I32 index2=start;index2<spRA2->length();index2++)
					{
						Radio& radioRV2=m_radioRAV2[index2];

						WeakRecord rRadio2=radioRV2.record();

						//* TODO allied communication
						if(m_aFaction(rRadio2) != faction)
						{
							continue;
						}

						//* TODO less brute-force

						sp<RecordGroup>& rspNeighbors1=radioRV.neighbors();
						sp<RecordGroup>& rspNeighbors2=radioRV2.neighbors();
						Record& rNeighborhood=radioRV.neighborhood();
						Record& rNeighborhood2=radioRV2.neighborhood();
						F32 distance=magnitude(m_aLocation(rRadio2)-location);

						if(distance<range && distance<radioRV2.range())
						{
							if(!rspNeighbors1->find(rRadio2))
							{
								rspNeighbors1->add(rRadio2);
							}
							if(!rspNeighbors2->find(rRadio1))
							{
								rspNeighbors2->add(rRadio1);
							}

							if(rNeighborhood!=rNeighborhood2)
							{
								merge(rNeighborhood,rNeighborhood2);
							}
						}
						else if(rNeighborhood==rNeighborhood2)
						{
							rspNeighbors1->remove(rRadio2);
							rspNeighbors2->remove(rRadio1);
						}
					}
				}
				start=0;
			}
		}
	}

	//*		`Partition Crowds'
	for(RecordGroup::iterator it=rspRG->begin();it!=rspRG->end();it++)
	{
		sp<RecordArray> spRA= *it;
		m_radioRAV.bind(spRA);

		if(!m_radioRAV.recordView().neighborhood.check(spRA))
		{
			continue;
		}

		for(Radio& radioRV: m_radioRAV)
		{
			if(!radioRV.newNeighborhood().isValid())
			{
				fill(radioRV.record(),radioRV.neighborhood());
				partition(radioRV.neighborhood());
			}
		}
	}

	//*		`Transition Radios'
	for(RecordGroup::iterator it=rspRG->begin();it!=rspRG->end();it++)
	{
		sp<RecordArray> spRA= *it;
		m_radioRAV.bind(spRA);

		if(!m_radioRAV.recordView().neighborhood.check(spRA))
		{
			continue;
		}

		for(Radio& radioRV: m_radioRAV)
		{
			if(!radioRV.newNeighborhood().isValid())
			{
				feX(e_invalidHandle,"Communicate::handle",
						"inValid NewNeighborhood");
			}

			if(radioRV.neighborhood() != radioRV.newNeighborhood())
			{
				m_crowdRV1.bind(radioRV.newNeighborhood());
				m_crowdRV2.bind(radioRV.neighborhood());

#if	FE_COMMUNICATE_DEBUG
				feLog("Communicate::handle move Radio %p to NH %p from NH %p\n",
						radioRV.record().data(),
						m_crowdRV1.viewpoint().data(),
						m_crowdRV2.viewpoint().data());
#endif

				m_crowdRV1.neighbors()->add(radioRV.record());
				m_crowdRV2.neighbors()->remove(radioRV.record());
				radioRV.neighborhood()=radioRV.newNeighborhood();
			}

			radioRV.newNeighborhood()=Record();
		}
	}
}

void Communicate::merge(Record& rNeighborhood1,Record& rNeighborhood2)
{
	m_crowdRV1.bind(rNeighborhood1);
	m_crowdRV2.bind(rNeighborhood2);

#if	FE_COMMUNICATE_DEBUG
	feLog("Communicate::merge NH %p into NH %p\n",
			m_crowdRV2.viewpoint().data(),
			m_crowdRV1.viewpoint().data());
#endif

	sp<RecordGroup>& rspRG1=m_crowdRV1.neighbors();
	sp<RecordGroup>& rspRG2=m_crowdRV2.neighbors();

	//* move Neighbors from old Crowd to new Crowd
	for(RecordGroup::iterator it=rspRG2->begin();it!=rspRG2->end();it++)
	{
		sp<RecordArray> spRA= *it;
		m_radioRAV3.bind(spRA);
		for(Radio& radioRV3: m_radioRAV3)
		{
			rspRG1->add(radioRV3.record());
			radioRV3.neighborhood()=rNeighborhood1;
		}
	}

	//* should destroy itself very soon, regardless
	rspRG2->clear();
}

void Communicate::fill(WeakRecord rRadio,WeakRecord rNeighborhood)
{
	m_radioRAV2.recordView().bind(rRadio);
	m_radioRAV2.recordView().newNeighborhood()=rNeighborhood;

	m_crowdRV1.neighbors(rNeighborhood)->setWeak(TRUE);

/*
	m_crowdRV1.bind(rNeighborhood);
	feLog("Communicate::fill %p in %p\n",
			rRadio.data(),
			m_crowdRV1.Viewpoint().data());
*/

	sp<RecordGroup>& rspRG=m_radioRAV2.recordView().neighbors();
	for(RecordGroup::iterator it=rspRG->begin();it!=rspRG->end();it++)
	{
		sp<RecordArray> spRA= *it;
		for(I32 index=0;index<spRA->length();index++)
		{
			if(!m_radioRAV.recordView().newNeighborhood(spRA,index).isValid())
			{
				fill(spRA->getRecord(index),rNeighborhood);
			}
		}
	}
}

void Communicate::partition(Record& rNeighborhood)
{
	m_crowdRV1.bind(rNeighborhood);

	sp<RecordGroup>& rspRG=m_crowdRV1.neighbors();
	for(RecordGroup::iterator it=rspRG->begin();it!=rspRG->end();it++)
	{
		sp<RecordArray> spRA= *it;
		for(I32 index=0;index<spRA->length();index++)
		{
			if(!m_radioRAV.recordView().newNeighborhood(spRA,index).isValid())
			{
				m_crowdRV2.createRecord();
#if	FE_COMMUNICATE_DEBUG
				feLog("Communicate::partition Radio %p to NH %p from NH %p\n",
						spRA->getRecord(index).data(),
						m_crowdRV2.viewpoint().data(),
						m_crowdRV1.viewpoint().data());
#endif

				fill(spRA->getRecord(index),m_crowdRV2.record());
			}
		}
	}
}

} /* namespace ext */
} /* namespace fe */
