/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __intelligence_ControlCenter_h__
#define __intelligence_ControlCenter_h__
namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief ControlCenter RecordView

	@ingroup intelligence
*//***************************************************************************/
class FE_DL_EXPORT ControlCenter: public Controlled, public Radio
{
	public:
		Functor<Record>				viewpoint;
		Functor<Record>				purview;
		Functor< sp<RecordGroup> >	controlled;
		Functor< sp<RecordGroup> >	behaviors;
		Functor< sp<Component> >	selfSignaler;
		Functor<I32>				dominance;
		Functor<Real>				time;

				ControlCenter(void)		{ setName("ControlCenter"); }
virtual	void	addFunctors(void)
				{
					Controlled::addFunctors();
					Radio::addFunctors();

					add(viewpoint,		FE_USE("ai:viewpoint"));
					add(purview,		FE_USE("ai:observation"));

					add(controlled,		FE_SPEC("ai:controlled",
							"RG under our control"));
					add(behaviors,		FE_SPEC("ai:behaviors",
							"RG of behaviors"));
					add(selfSignaler,	FE_SPEC("ai:selfSignaler",
							"sp<Component> signaled for behaving"));
					add(dominance,		FE_SPEC("ai:dominance",
							"Magnitude of control"));
					add(time,			FE_USE("sim:time"));
				}
virtual	void	initializeRecord(void)
				{
					Controlled::initializeRecord();
					Radio::initializeRecord();

					selfSignaler.attribute()->setSerialize(FALSE);

					controlled.attribute()->setCloneable(FALSE);
					behaviors.attribute()->setCloneable(FALSE);
					selfSignaler.attribute()->setCloneable(FALSE);
					purview.attribute()->setCloneable(FALSE);

					identifier()="ControlCenter";

					controlled.createAndSetRecordGroup();
					controlled()->setWeak(TRUE);

					behaviors.createAndSetRecordGroup();

					selfSignaler.createAndSetComponent("SignalerI");

					Observation observationRV;
					observationRV.bind(scope());
					purview()=observationRV.createRecord();
					observationRV.observer()=record();

					Sphere sphereRV;
					sphereRV.bind(scope());
					observationRV.shape()=sphereRV.createRecord();

					sphereRV.radius()=2.0f;

					dominance()=0;
					time()=0.0f;
				}
};

} /* namespace ext */
} /* namespace fe */

#endif /* __intelligence_ControlCenter_h__ */
