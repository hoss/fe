/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __intelligence_Scan_h__
#define __intelligence_Scan_h__
namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief	Operate scanning abilities

	@ingroup intelligence
*//***************************************************************************/
class FE_DL_EXPORT Scan: virtual public HandlerI
{
	public:
					Scan(void)												{}

					//* as HandlerI
virtual void		handle(Record &record);

	private:

		sp<SelectorI>				m_spSelectorI;
		sp<FilterI>					m_spFilterI;

		Operator					m_selectOp;
		Theater						m_theaterRV;
		Proxy						m_proxyRV;
		Controlled					m_controlledRV;
		Observation					m_observationRV;
		Cylinder					m_cylinderRV;
		RecordArrayView<Scanner>	m_scannerRAV;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __intelligence_Scan_h__ */
