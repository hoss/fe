/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <intelligence/intelligence.pmh>

namespace fe
{
namespace ext
{

void Thrust::handle(Record& record)
{
	m_theaterRV.bind(record);
	sp<RecordGroup> spRG=m_theaterRV.particles();
	FEASSERT(spRG.isValid());

	for(RecordGroup::iterator it=spRG->begin();it!=spRG->end();it++)
	{
		sp<RecordArray> spRA= *it;
		m_thrusterRAV.bind(spRA);

		if(!m_thrusterRAV.recordView().thrust.check(spRA))
		{
			continue;
		}

		for(Thruster& thrusterRV: m_thrusterRAV)
		{
			SpatialVector& thrust=thrusterRV.thrust();
			F32 mag=magnitude(thrust);
			F32 limit=thrusterRV.thrustLimit();
			if(mag>limit)
			{
				thrust*=limit/mag;
				mag=limit;
			}
			thrusterRV.force()-=thrust;
			thrusterRV.gain()=0.1f*mag;
		}
	}
}

} /* namespace ext */
} /* namespace fe */
