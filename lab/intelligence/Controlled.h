/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __intelligence_Controlled_h__
#define __intelligence_Controlled_h__
namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Controlled RecordView

	@ingroup intelligence
*//***************************************************************************/
class FE_DL_EXPORT Controlled: public Audible
{
	public:
		Functor<WeakRecord>			controlRecord;
		Functor<String>				faction;
		Functor<I32>				subservience;

				Controlled(void)			{ setName("Controlled"); }
virtual	void	addFunctors(void)
				{
					Audible::addFunctors();

					add(controlRecord,	FE_SPEC("ai:controlCen",
							"Who controls us"));
					add(faction,		FE_SPEC("ai:faction","Allegiance"));
					add(subservience,	FE_SPEC("ai:subservience",
							"Magnitude of control over us"));
				}
virtual	void	initializeRecord(void)
				{
					Audible::initializeRecord();

					identifier()="Controlled";

					faction()="";
					subservience()=0;
				}
};

} /* namespace ext */
} /* namespace fe */

#endif /* __intelligence_Controlled_h__ */
