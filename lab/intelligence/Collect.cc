/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <intelligence/intelligence.pmh>

#define	FE_COLLECT_DEBUG	FALSE

namespace fe
{
namespace ext
{

void Collect::handle(Record &record)
{
	if(!m_observeRV.scope().isValid())
	{
		sp<Scope> spScope=record.layout()->scope();

		m_observeRV.bind(spScope);
		m_controlledRV.bind(spScope);
		m_surveillanceRV.bind(spScope);
	}

	m_theaterRV.bind(record);

	sp<RecordGroup> spRG=m_theaterRV.particles();
	FEASSERT(spRG.isValid());

	for(RecordGroup::iterator it=spRG->begin();it!=spRG->end();it++)
	{
		sp<RecordArray> spRA= *it;
		m_controlRAV.bind(spRA);

		if(!m_controlRAV.recordView().controlled.check(spRA))
			continue;

		for(I32 index=0;index<spRA->length();index++)
		{
			ControlCenter& controlRV=m_controlRAV[index];

			//* use self as Radio to choose a Surveillance
			controlRV.viewpoint()=
					controlRV.viewpoint(controlRV.neighborhood());

			//*		`Update Built-in Vision Sphere'

			m_controlledRV.location(m_observeRV.shape(controlRV.purview()))=
					controlRV.location();

			checkSurveillance(controlRV.viewpoint(),controlRV.record(),
					controlRV.purview());

			//*		`Collect Scanner Purviews'

			sp<RecordGroup> spControlled=controlRV.controlled();
			for(RecordGroup::iterator it2=spControlled->begin();
					it2!=spControlled->end();it2++)
			{
				sp<RecordArray> spRA2= *it2;
				if(!controlRV.purview.check(spRA2))
					continue;

				for(I32 index2=0;index2<spRA2->length();index2++)
				{
					checkSurveillance(controlRV.viewpoint(),
							controlRV.record(),
							controlRV.purview(spRA2,index2));
				}
			}
		}
	}

	//*		`Update Terrain Visibility'
	sp<StratumDrawI> spStratumDrawI=m_theaterRV.stratumDrawI();
	if(spStratumDrawI.isValid() && m_theaterRV.viewpoint().isValid())
	{
		spStratumDrawI->assignVisibility(
				m_surveillanceRV.observations(m_theaterRV.viewpoint()));
	}
}

void Collect::checkSurveillance(Record& rViewpoint,Record rControl,
	Record& rPurview)
{
	FEASSERT(rPurview.isValid());

	WeakRecord& rViewpoint2=m_observeRV.viewpoint(rPurview);

	//* add if new
	if(rViewpoint2!=rViewpoint)
	{
		if(rViewpoint2.isValid())
		{
#if	FE_COLLECT_DEBUG
			feLog("Collect::handle disconnect Observation"
					" %p from Surveillance %p %s\n",
					rPurview.idr(),
					rViewpoint2.idr(),
					m_controlRAV.recordView().faction(rControl).c_str());
#endif

			//* disconnect from old Viewpoint
			m_surveillanceRV.observations(rViewpoint2)->remove(rPurview);
		}

		if(rViewpoint.isValid())
		{
#if	FE_COLLECT_DEBUG
			feLog("Collect::handle connect Observation"
					" %p to Surveillance %p %s\n",
					rPurview.idr(),
					rViewpoint.idr(),
					m_controlRAV.recordView().faction(rControl).c_str());
#endif

			//* connect to new Viewpoint
			m_surveillanceRV.observations(rViewpoint)->add(rPurview);
			rViewpoint2=rViewpoint;
		}
		else
		{
			rViewpoint2=Record();
		}
	}
}

} /* namespace ext */
} /* namespace fe */
