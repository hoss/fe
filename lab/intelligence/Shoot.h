/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __intelligence_Shoot_h__
#define __intelligence_Shoot_h__
namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Operate launching abilities

	@ingroup intelligence
*//***************************************************************************/
class FE_DL_EXPORT Shoot: virtual public HandlerI
{
	public:
				Shoot(void)													{}

				//* as HandlerI
virtual void	handle(Record &record);

	private:
		Theater					m_theaterRV;
		Mortal					m_mortalRV;
		Periodic				m_periodicRV;
		RecordArrayView<Cannon>	m_cannonRAV;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __intelligence_Shoot_h__ */
