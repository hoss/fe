/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __intelligence_Sing_h__
#define __intelligence_Sing_h__
namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Operate audible abilities

	@ingroup intelligence
*//***************************************************************************/
class FE_DL_EXPORT Sing: virtual public HandlerI
{
	public:
				Sing(void)												{}

				//* as HandlerI
virtual void	handle(Record &record);

	private:
		Theater						m_theaterRV;
		RecordArrayView<Audible>	m_audibleRAV;

		sp<AudioI>		m_spAudioI;
		sp<ListenerI>	m_spListenerI;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __intelligence_Sing_h__ */
