/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __intelligence_Theater_h__
#define __intelligence_Theater_h__
namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Theater RecordView

	@ingroup intelligence
*//***************************************************************************/
class FE_DL_EXPORT Theater: public Arena
{
	public:
		Functor<SpatialVector>		cameraLoc;
		Functor<SpatialVector>		cameraAt;
		Functor<SpatialVector>		cameraUp;
		Functor<Record>				viewpoint;
		Functor< sp<Component> >	directorI;
		Functor< sp<Component> >	behaviorPalette;

				Theater(void)		{ setName("Theater"); }
virtual	void	addFunctors(void)
				{
					Arena::addFunctors();

					add(cameraLoc,			FE_SPEC("ai:cameraLoc",
							"Location of camera"));
					add(cameraAt,			FE_SPEC("ai:cameraAt",
							"Direction of camera"));
					add(cameraUp,			FE_SPEC("ai:cameraUp",
							"Up vector of camera"));
					add(viewpoint,			FE_USE("ai:viewpoint"));

					add(directorI,			FE_SPEC("ai:DirectorI",
							"DirectorI behavior manipulator"));
					add(behaviorPalette,	FE_SPEC("ai:behaviorPal",
							"Palette of behaviors"));
				}
virtual	void	initializeRecord(void)
				{
					Arena::initializeRecord();

					directorI.createAndSetComponent("DirectorI");
					behaviorPalette.createAndSetComponent("PaletteI");
				}
};

} /* namespace ext */
} /* namespace fe */

#endif /* __intelligence_Theater_h__ */
