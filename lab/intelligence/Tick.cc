/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <intelligence/intelligence.pmh>

namespace fe
{
namespace ext
{

void Tick::handle(Record& record)
{
	m_theaterRV.bind(record);
	sp<RecordGroup> spRG=m_theaterRV.particles();
	FEASSERT(spRG.isValid());

	I32 timeMS=I32(1000.0f*m_theaterRV.time());

	for(RecordGroup::iterator it=spRG->begin();it!=spRG->end();it++)
	{
		sp<RecordArray> spRA= *it;
		m_periodicRAV.bind(spRA);

		if(!m_periodicRAV.recordView().backlog.check(spRA))
		{
			continue;
		}

		for(Periodic& periodicRV: m_periodicRAV)
		{
			if(periodicRV.readyMS()<I32(timeMS))
			{
				periodicRV.readyMS()+=periodicRV.periodMS();
				periodicRV.backlog()++;

//				feLog("Tick::handle tick %d vs %d backlog=%d\n",
//						timeMS,periodicRV.readyMS(),periodicRV.backlog());
			}
		}
	}
}

} /* namespace ext */
} /* namespace fe */
