/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <intelligence/intelligence.pmh>

namespace fe
{
namespace ext
{

bool Director::addBehavior(Record& rControlRecord,String name) const
{
	if(!rControlRecord.isValid())
	{
		feX(e_invalidHandle,"Director::addBehavior invalid ControlCenter\n");
		return false;
	}

	ControlCenter controlCenterRV;
	controlCenterRV.bind(rControlRecord);

	sp<RecordGroup>& rspBehaviors=controlCenterRV.behaviors();
	if(!rspBehaviors.isValid())
	{
		feLog("Director::addBehavior invalid behavior group\n");
		return false;
	}

	Record behavior=controlCenterRV.scope()->produceRecord("Behavior");

	Behavior behaviorRV;
	behaviorRV.bind(behavior);
	behaviorRV.behaviorName()=name;

	rspBehaviors->add(behavior);
	return true;
}

void Director::moveRecordGroup(sp<RecordGroup> spRecordGroup,
		const SpatialVector displacement) const
{
	RecordArrayView<Sphere> sphereRAV;

	for(RecordGroup::iterator it=spRecordGroup->begin();
			it!=spRecordGroup->end();it++)
	{
		sp<RecordArray>& rspRA= *it;
		sphereRAV.bind(rspRA);

		if(!sphereRAV.recordView().location.check(rspRA))
		{
			continue;
		}

		for(Sphere& sphereRV: sphereRAV)
		{
			sphereRV.location()+=displacement;
		}
	}
}

void Director::adoptRecordGroup(Record arena,
		sp<RecordGroup> spRecordGroup) const
{
	if(!spRecordGroup.isValid())
	{
		feLog("Director::adoptRecordGroup invalid RecordGroup\n");
		return;
	}

	Arena arenaRV;
	arenaRV.bind(arena);
	arenaRV.particles()->add(spRecordGroup);
}

Record Director::searchRecordGroup(sp<RecordGroup> spRG,
		String particlename)
{
	Record result;

	for(RecordGroup::iterator it=spRG->begin();it!=spRG->end();it++)
	{
		sp<RecordArray>& rspRA= *it;
		m_matterRAV.bind(rspRA);

		if(!m_matterRAV.recordView().identifier.check(rspRA))
		{
			continue;
		}

		for(Matter& matterRV: m_matterRAV)
		{
//			feLog("compare \"%s\" vs \"%s\"\n",particlename.c_str(),
//					matterRV.identifier().c_str());

			if(matterRV.identifier()==particlename)
			{
//				feLog("found\n");

				result=matterRV.record();
				break;
			}
		}
	}

	return result;
}

} /* namespace ext */
} /* namespace fe */
