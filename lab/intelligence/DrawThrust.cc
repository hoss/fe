/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <intelligence/intelligence.pmh>

namespace fe
{
namespace ext
{

void DrawThrust::handle(Record &record)
{
	if(!m_thrusterRAV.scope().isValid())
	{
		m_thrusterRAV.bind(record.layout()->scope());

		m_spTwoSide=new DrawMode;
		m_spTwoSide->setTwoSidedLighting(TRUE);
		m_spTwoSide->setBackfaceCulling(FALSE);
	}

	const Color white(1.0f,1.0f,1.0f,1.0f);
	const Color green(0.0f,1.0f,0.0f,1.0f);
	const Color darkgreen(0.0f,0.3f,0.0f,1.0f);
	const Color grey(0.5f,0.5f,0.5f,1.0f);
	const Color yellow(1.0f,1.0f,0.0f,1.0f);
	const Color red(1.0f,0.0f,0.0f,1.0f);
	const Color pink(1.0f,0.5f,1.0f,1.0f);
	const Color orange(1.0f,0.5f,0.0f,1.0f);
	const Color blue(0.5f,0.5f,1.0f,1.0f);
	const Color purple(0.5f,0.0f,1.0f,1.0f);
	const Color darkpurple(0.3f,0.0f,0.6f,0.5f);
	const Color cyan(0.0f,1.0f,1.0f,0.4f);
	const F32 thrustAlpha=0.4f;

	m_theaterRV.bind(record);

	sp<DrawI> spDrawI=m_theaterRV.drawI();
	FEASSERT(spDrawI.isValid());

	sp<RecordGroup> spRG=m_theaterRV.particles();
	FEASSERT(spRG.isValid());

	spDrawI->pushDrawMode(m_spTwoSide);

	for(RecordGroup::iterator it=spRG->begin();it!=spRG->end();it++)
	{
		sp<RecordArray> spRA= *it;
		m_thrusterRAV.bind(spRA);

		if(!m_thrusterRAV.recordView().thrust.check(spRA))
		{
			continue;
		}

		for(Thruster& thrusterRV: m_thrusterRAV)
		{
			const SpatialVector& location=thrusterRV.location();
			const F32 radius=thrusterRV.radius();
			const SpatialVector& thrust=thrusterRV.thrust();
			F32 mag=magnitude(thrust);
			if(mag<1e-3f)
			{
				continue;
			}

			//* draw thrust shape
			const F32 baseRadius=0.1f;
			const F32 endRadius=0.2f;

			SpatialQuaternion rotation;
			const SpatialVector unitThrust=thrust/mag;
			set(rotation,SpatialVector(0.0f,0.0f,1.0f),unitThrust);

			SpatialTransform transform=rotation;
			setTranslation(transform,location+unitThrust*radius);

			SpatialVector scale(endRadius,endRadius,mag*0.1f);
			Color color=orange;
			color[3]=thrustAlpha;

			spDrawI->drawCylinder(transform,&scale,baseRadius/endRadius,
					color,0);
		}
	}

	spDrawI->popDrawMode();
}

} /* namespace ext */
} /* namespace fe */
