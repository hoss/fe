/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <intelligence/intelligence.pmh>

namespace fe
{
namespace ext
{

void Shoot::handle(Record &record)
{
	m_theaterRV.bind(record);

	const I32 ms=I32(m_theaterRV.time()*1e3f);
	sp<RecordGroup> spRG=m_theaterRV.particles();
	FEASSERT(spRG.isValid());

	for(RecordGroup::iterator it=spRG->begin();it!=spRG->end();it++)
	{
		sp<RecordArray> spRA= *it;
		m_cannonRAV.bind(spRA);

		if(!m_cannonRAV.recordView().target.check(spRA))
		{
			continue;
		}

		for(Cannon& cannonRV: m_cannonRAV)
		{
			//* TODO shouldn't be assuming shooter is a Cannon

			const SpatialVector& location=cannonRV.location();
			const SpatialVector& target=cannonRV.target();

			const F32 radius=cannonRV.radius();
			I32& requests=cannonRV.requests();
			I32& backlog=cannonRV.backlog();
			const F32 launchSpeed=cannonRV.launchSpeed();
			const String& projectile=cannonRV.projectile();

//			feLog("%d %d %.2f %.2f\n",ms,launchTime,since,launchPeriod);
			if(requests && backlog && !projectile.empty())
			{
//				feLog("SHOOT\n");
				backlog=0;

				if(requests>0)
					requests--;

				Record mortal=cannonRV.scope()->produceRecord(
						cannonRV.projectile());
				m_mortalRV.bind(mortal);
				m_periodicRV.bind(mortal);

				SpatialVector direction=unit(target-location);
				m_mortalRV.location()=location+radius*direction;
				m_mortalRV.velocity()=cannonRV.velocity()+
						launchSpeed*direction;

				if(m_periodicRV.periodMS.check())
				{
					m_periodicRV.readyMS()=ms+m_periodicRV.periodMS();
				}

				if(m_mortalRV.faction.check())
				{
					m_mortalRV.faction()=cannonRV.faction();
				}

				cannonRV.impulse()-=m_mortalRV.mass()*m_mortalRV.velocity();

				spRG->add(mortal);
				m_mortalRV.unbind();
				m_periodicRV.unbind();

				cannonRV.nextSong()=cannonRV.launchSong();
				cannonRV.nextPlays()=1;
			}
		}
	}
}

} /* namespace ext */
} /* namespace fe */
