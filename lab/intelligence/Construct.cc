/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <intelligence/intelligence.pmh>

#define FE_CONSTRUCT_DEBUG	FALSE

namespace fe
{
namespace ext
{

void Construct::handle(Record& record)
{
	m_theaterRV.bind(record);
	sp<RecordGroup> spRG=m_theaterRV.particles();
	FEASSERT(spRG.isValid());

	F32 deltaT=m_theaterRV.deltaT();

	sp<DirectorI> spDirectorI=m_theaterRV.directorI();

	for(RecordGroup::iterator it=spRG->begin();it!=spRG->end();it++)
	{
		sp<RecordArray> spRA= *it;
		m_foundryRAV.bind(spRA);

		if(!m_foundryRAV.recordView().prototype.check(spRA))
		{
			continue;
		}

		for(Foundry& foundryRV: m_foundryRAV)
		{
			if(foundryRV.design().empty())
			{
				foundryRV.prototype()=NULL;
			}

			//* load design
			if(!foundryRV.prototype().isValid())
			{
				foundryRV.design()=foundryRV.nextDesign();

				//* HACK no auto-repeat
//				foundryRV.nextDesign()="";

				if(!foundryRV.design().empty())
				{
					sp<Scope> spScope=foundryRV.scope();

					foundryRV.prototype()=RecordView::loadRecordGroup(
							spScope,foundryRV.design());
				}
			}

			if(foundryRV.prototype().isValid())
			{
				//* (re)start manufacturing
				sp<RecordGroup> spProduct=generate(foundryRV.prototype());

				if(spProduct.isValid())
				{
					spDirectorI->moveRecordGroup(spProduct,
							foundryRV.location()+
							SpatialVector(0.0f,2.0f,0.0f));

					spDirectorI->adoptRecordGroup(
							m_theaterRV.record(),spProduct);
				}

				//* continue manufacturing
				BWORD finished=build(foundryRV.prototype(),
						foundryRV.selection(),deltaT);
				if(finished)
				{
#if FE_CONSTRUCT_DEBUG
					feLog("Construct::handle finished \"%s\"\n",
							foundryRV.design().c_str());
#endif

					if(foundryRV.design()!=foundryRV.nextDesign())
					{
						foundryRV.design()="";
					}
				}
			}
		}
	}
}

sp<RecordGroup> Construct::generate(sp<RecordGroup> spPrototype)
{
	sp<RecordGroup> spProduct;

	for(RecordGroup::iterator it=spPrototype->begin();
			it!=spPrototype->end();it++)
	{
		sp<RecordArray> spRA= *it;
		m_matterRAV.bind(spRA);

		for(Matter& matterRV: m_matterRAV)
		{
			Record original=matterRV.record();
			Record copy=matterRV.pattern.safeGet();
			if(!copy.isValid())
			{
				copy=original.clone();
				if(!spProduct.isValid())
				{
					// don't create group unless we need it
					spProduct=new RecordGroup;
				}
				spProduct->add(copy);

#if FE_CONSTRUCT_DEBUG
				feLog("Construct::generate Layout %s %p\n",
						original.layout()->name().c_str(),
						copy.data());
#endif

				// back-wire for reference
				matterRV.pattern.safeSet(copy);

				m_matterRV2.bind(copy);
				m_matterRV2.pattern.safeSet(original);

				m_matterRV2.metal.safeSet(0);
				m_matterRV2.fuel.safeSet(0);
				m_matterRV2.gem.safeSet(0);

				//* HACK adjust thrust
				Thruster thrusterRV;
				thrusterRV.bind(copy);
				if(thrusterRV.thrust.check())
				{
					FE_STATIC_BAD I32 inc=1;
					const SpatialVector axis(0.0f,1.0f,0.0f);
					const F32 angle=77.0f*fe::degToRad*inc;
					SpatialQuaternion rotation(angle,axis);
					rotateVector(rotation,thrusterRV.thrust(),
							thrusterRV.thrust());
					inc++;
				}
			}
		}
	}

	if(!spProduct.isValid())
	{
		return spProduct;
	}

	//* reattach any contained records with a pattern
	for(RecordGroup::iterator it=spProduct->begin();it!=spProduct->end();it++)
	{
		sp<RecordArray> spRA= *it;
		m_matterRAV.bind(spRA);

		for(Matter& matterRV: m_matterRAV)
		{
			Record copy=matterRV.record();
			sp<Layout> spLayout=copy.layout();
			sp<Scope> spScope=spLayout->scope();
			FE_UWORD cnt = spScope->getAttributeCount();
			for(FE_UWORD i = 0; i < cnt; i++)
			{
				if(spLayout->checkAttribute(i))
				{
					sp<Attribute> spAttribute=spScope->attribute(i);
					sp<BaseType> spBT = spAttribute->type();
/*
					feLog("%d offset %d %s\n",i,offset,
							spAttribute->name().c_str());
*/
					if(spBT->typeinfo() == getTypeId<Record>())
					{
						Record& record = copy.accessAttribute<Record>(i);
						if(record.isValid())
						{
							m_matterRV2.bind(record);
							if(m_matterRV2.pattern.check())
							{
								Record pattern=m_matterRV2.pattern();
/*
								feLog("Record %s pattern %d\n",
										record.layout().isValid()?
										record.layout()->name().c_str():
										"invalid",
										pattern.isValid());
*/
								if(pattern.isValid())
								{
									record=pattern;
								}
							}
						}
					}
				}
			}
		}
	}

	return spProduct;
}

BWORD Construct::build(sp<RecordGroup> spPrototype,sp<RecordGroup> spSelection,
		F32 deltaT)
{
	const F32 zero_mass=1e-3f;

	BWORD finished=TRUE;

	//* tranfer mass
	for(RecordGroup::iterator it=spPrototype->begin();
			it!=spPrototype->end();it++)
	{
		sp<RecordArray> spRA= *it;
		m_matterRAV.bind(spRA);

		if(!m_matterRAV.recordView().pattern.check(spRA))
		{
			continue;
		}

		for(Matter& matterRV: m_matterRAV)
		{
			Record pattern=matterRV.pattern();
			if(!pattern.isValid())
			{
				continue;
			}
			m_matterRV2.bind(pattern);

			//* TODO reduction rate, as well as build rate
			const F32 rate=0.1f*deltaT;

			//* determine how much we can absorb
			const F32 min_request=0.01f;
			F32 metal_request=0.0f;
			if(m_matterRV2.metal.check())
			{
				F32& metal=m_matterRV2.metal();
				F32 desired=matterRV.metal.safeGet();
				if(metal<desired)
				{
					metal_request=desired-metal;
					if(metal_request<min_request)
					{
						metal_request=0.0f;
					}
					else
					{
						finished=FALSE;
					}
					metal_request=fe::minimum(metal_request,rate);
				}
			}
			F32 gem_request=0.0f;
			if(m_matterRV2.gem.check())
			{
				F32& gem=m_matterRV2.gem();
				F32 desired=matterRV.gem.safeGet();
				if(gem<desired)
				{
					gem_request=desired-gem;
					if(gem_request<min_request)
					{
						gem_request=0.0f;
					}
					else
					{
						finished=FALSE;
					}
					gem_request=fe::minimum(gem_request,rate);
				}
			}

			//* take from scanned items
			F32 metal_response=0.0f;
			F32 gem_response=0.0f;

			for(RecordGroup::iterator it2=spSelection->begin();
					it2!=spSelection->end();it2++)
			{
				sp<RecordArray> spRA= *it2;
				if(!matterRV.metal.check(spRA))
				{
					continue;
				}

				m_matterRAV3.bind(spRA);
				for(Matter& matterRV3: m_matterRAV3)
				{
					F32 metal=fe::minimum(matterRV3.metal(),
							metal_request-metal_response);
					F32 gem=fe::minimum(matterRV3.gem(),
							gem_request-gem_response);

					matterRV3.metal()-=metal;
					matterRV3.gem()-=gem;

					//* recalculate mass
					const F32 density=12.0f;	// HACK
					F32 mass=matterRV3.metal()+matterRV3.gem()+
							matterRV3.fuel()+1e-4f;
					F32 radius=powf((mass/density)*(0.75f/fe::pi),0.333f);
					matterRV3.mass()=mass;
					matterRV3.radius()=radius;
					if(mass<zero_mass)
					{
						matterRV3.flags()|=Matter::e_discard;
					}

//					feLog("Construct::build metal %.6G gem %.6G\n",metal,gem);

					metal_response+=metal;
					gem_response+=gem;
				}
			}

#if FALSE
			feLog("Construct::build %s finished %d"
					" metal %.6G/%.6G+%.6G gem %.6G/%.6G+%.6G\n",
					m_matterRV2.record().layout()->name().c_str(),
					finished,
					m_matterRV2.metal.safeGet(),
					matterRV.metal.safeGet(),metal_response,
					m_matterRV2.gem.safeGet(),
					matterRV.gem.safeGet(),gem_response);
#endif

			//* deliver
			if(metal_response>0.0f)
			{
				m_matterRV2.metal()+=metal_response;
			}
			if(gem_response>0.0f)
			{
				m_matterRV2.gem()+=gem_response;
			}
		}
	}

	//* set free
	if(finished)
	{
		for(RecordGroup::iterator it=spPrototype->begin();
				it!=spPrototype->end();it++)
		{
			sp<RecordArray> spRA= *it;
			m_matterRAV.bind(spRA);

			if(!m_matterRAV.recordView().pattern.check(spRA))
			{
				continue;
			}

			for(Matter& matterRV: m_matterRAV)
			{
				Record pattern=matterRV.pattern();
				if(!pattern.isValid())
				{
					continue;
				}

				m_matterRV2.bind(pattern);
				m_matterRV2.pattern.safeSet(Record());

				matterRV.pattern()=Record();
			}
		}
	}

	return finished;
}

} /* namespace ext */
} /* namespace fe */
