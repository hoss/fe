/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <intelligence/intelligence.pmh>

#define FE_DP_DRAW_SCAN		TRUE
#define FE_DP_DRAW_BBOX		FALSE

namespace fe
{
namespace ext
{

void DrawScan::handle(Record &record)
{
	if(!m_cylinderRV.scope().isValid())
	{
		sp<Scope> spScope=record.layout()->scope();

		m_cylinderRV.bind(spScope);
		m_observationRV.bind(spScope);

		m_spSelector=registry()->create("SelectorI");

		m_spWireframe=new DrawMode();
		m_spWireframe->setDrawStyle(DrawMode::e_wireframe);
	}

	const Color white(1.0f,1.0f,1.0f,1.0f);
	const Color green(0.0f,1.0f,0.0f,1.0f);
	const Color darkgreen(0.0f,0.3f,0.0f,1.0f);
	const Color grey(0.5f,0.5f,0.5f,1.0f);
	const Color yellow(1.0f,1.0f,0.0f,1.0f);
	const Color red(1.0f,0.0f,0.0f,1.0f);
	const Color pink(1.0f,0.5f,1.0f,1.0f);
	const Color orange(1.0f,0.5f,0.0f,1.0f);
	const Color blue(0.5f,0.5f,1.0f,1.0f);
	const Color purple(0.5f,0.0f,1.0f,1.0f);
	const Color darkpurple(0.3f,0.0f,0.6f,0.5f);
	const Color cyan(0.0f,1.0f,1.0f,0.4f);
	const F32 scanAlpha=0.2f;

	m_theaterRV.bind(record);

	const SpatialVector& camera=m_theaterRV.cameraLoc();

	sp<DrawI> spDrawI=m_theaterRV.drawI();
	FEASSERT(spDrawI.isValid());

	sp<RecordGroup> spRG=m_theaterRV.particles();
	FEASSERT(spRG.isValid());

	for(RecordGroup::iterator it=spRG->begin();it!=spRG->end();it++)
	{
		sp<RecordArray> spRA= *it;
		m_scannerRAV.bind(spRA);

		if(m_scannerRAV.recordView().nearest.check(spRA))
		{
			for(I32 index=0;index<spRA->length();index++)
			{
				Scanner& scannerRV=m_scannerRAV[index];

				const sp<RecordGroup>& rspSelection=scannerRV.selection();

				//* draw nearest
				const Record& nearest=scannerRV.nearest();
				if(nearest.isValid())
				{
					SpatialVector vertex[2];
					vertex[0]=scannerRV.location();
					vertex[1]=scannerRV.location(nearest);

					const F32 radius=0.5f;
					drawCircleXY(spDrawI,vertex[1],radius,red);

					spDrawI->drawLines(vertex,NULL,2,
							DrawI::e_strip,false,&red);
				}

				//* draw selection
				if(!rspSelection.isValid())
					continue;
				spDrawI->setDrawMode(m_spWireframe);
				for(RecordGroup::iterator it2=rspSelection->begin();
						it2!=rspSelection->end();it2++)
				{
					sp<RecordArray> spRA= *it2;
					for(I32 index=0;index<spRA->length();index++)
					{
						const SpatialVector& location2=
								scannerRV.location(spRA,index);
						const F32 radius=0.6f;
						drawCircleXY(spDrawI,location2,radius,white);
					}
				}
				spDrawI->setDrawMode(sp<DrawMode>(NULL));
			}
		}
#if FE_DP_DRAW_SCAN
		Color color=darkpurple;
		color[3]=scanAlpha;

		if(!m_scannerRAV.recordView().purview.check(spRA))
		{
			continue;
		}

		spDrawI->setDrawMode(m_spWireframe);

		for(Scanner& scannerRV: m_scannerRAV)
		{
			Record& observation=scannerRV.purview();
			if(!observation.isValid())
			{
				continue;
			}
			Record& shape=m_observationRV.shape(observation);
			if(!shape.isValid())
			{
				continue;
			}

			if(m_cylinderRV.span.check(shape))
			{
#if FE_DP_DRAW_SCAN
				//* draw scan shape
				draw(spDrawI,shape,darkpurple);
#endif

#if FE_DP_DRAW_SCAN && FE_DP_DRAW_BBOX
				const SpatialVector& location=scannerRV.location();
				const Box3f bounds=aabb(location,span,baseRadius,endRadius);
				spDrawI->drawBox(bounds,cyan);
#endif
			}
			else
			{
				const SpatialVector& center=m_cylinderRV.location(shape);

				//* TODO should be parallel to screen,
				//* not pointing at the camera (slight difference)
				SpatialVector direction=camera-center;

				SpatialTransform transform=billboardTransform(
					center,direction);
				const F32 radius=m_cylinderRV.radius(shape);
				SpatialVector scale(radius,radius,radius);

				spDrawI->drawCircle(transform,&scale,color);

//				drawSphere(spDrawI,m_cylinderRV.location(shape),
//						m_cylinderRV.radius(shape),color);

#if FE_DP_DRAW_SCAN && FE_DP_DRAW_BBOX
				const Box3f bounds=aabb(m_cylinderRV.location(shape),
						m_cylinderRV.radius(shape));
				spDrawI->drawBox(bounds,cyan);
#endif
			}
		}
		spDrawI->setDrawMode(sp<DrawMode>(NULL));
#endif
	}
}

} /* namespace ext */
} /* namespace fe */
