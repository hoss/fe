/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __intelligence_Tick_h__
#define __intelligence_Tick_h__
namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Operate periodic abilities

	@ingroup intelligence
*//***************************************************************************/
class FE_DL_EXPORT Tick: virtual public HandlerI
{
	public:
				Tick(void)												{}

				//* as HandlerI
virtual void	handle(Record &record);

	private:
		Theater						m_theaterRV;
		RecordArrayView<Periodic>	m_periodicRAV;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __intelligence_Tick_h__ */
