/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <intelligence/intelligence.pmh>

namespace fe
{
namespace ext
{

void DrawTargeting::handle(Record &record)
{
	const Color white(1.0f,1.0f,1.0f,1.0f);
	const Color green(0.0f,1.0f,0.0f,1.0f);
	const Color darkgreen(0.0f,0.3f,0.0f,1.0f);
	const Color grey(0.5f,0.5f,0.5f,1.0f);
	const Color yellow(1.0f,1.0f,0.0f,1.0f);
	const Color red(1.0f,0.0f,0.0f,1.0f);
	const Color pink(1.0f,0.5f,1.0f,1.0f);
	const Color orange(1.0f,0.5f,0.0f,1.0f);
	const Color blue(0.5f,0.5f,1.0f,1.0f);
	const Color purple(0.5f,0.0f,1.0f,1.0f);
	const Color darkpurple(0.3f,0.0f,0.6f,0.5f);
	const Color cyan(0.0f,1.0f,1.0f,0.4f);

	m_theaterRV.bind(record);

	sp<DrawI> spDrawI=m_theaterRV.drawI();
	FEASSERT(spDrawI.isValid());

	sp<RecordGroup> spRG=m_theaterRV.particles();
	FEASSERT(spRG.isValid());

	for(RecordGroup::iterator it=spRG->begin();it!=spRG->end();it++)
	{
		sp<RecordArray> spRA= *it;
		m_targeterRAV.bind(spRA);

		if(!m_targeterRAV.recordView().target.check(spRA))
		{
			continue;
		}

		for(Targeter& targeterRV: m_targeterRAV)
		{
			SpatialVector vertex[2];
			vertex[0]=targeterRV.location();
			vertex[1]=targeterRV.target();

			//* draw line to target
			spDrawI->drawLines(vertex,NULL,2,DrawI::e_strip,false,&blue);
		}
	}
}

} /* namespace ext */
} /* namespace fe */
