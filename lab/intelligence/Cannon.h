/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __intelligence_Cannon_h__
#define __intelligence_Cannon_h__
namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Cannon RecordView

	@ingroup intelligence
*//***************************************************************************/
class FE_DL_EXPORT Cannon: public Targeter, public Periodic
{
	public:
		Functor<F32>				launchSpeed;
		Functor<String>				launchSong;
		Functor<String>				projectile;

				Cannon(void)			{ setName("Cannon"); }
virtual	void	addFunctors(void)
				{
					Targeter::addFunctors();
					Periodic::addFunctors();

					add(launchSpeed,	FE_SPEC("ai:launchSpeed",
							"Muzzle velocity"));
					add(launchSong,		FE_SPEC("ai:launchSong",
							"sound for each shot"));
					add(projectile,		FE_SPEC("ai:projectile",
							"Layout name of bullet"));
				}
virtual	void	initializeRecord(void)
				{
					Targeter::initializeRecord();
					Periodic::initializeRecord();

					identifier()="cannon";
					periodMS()=1000;
					launchSpeed()=1.0f;
				}
};

} /* namespace ext */
} /* namespace fe */

#endif /* __intelligence_Cannon_h__ */
