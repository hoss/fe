/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "griddb/griddb.h"

using namespace fe;
using namespace fe::ext;

int main(int argc,char** argv,char **env)
{
	BWORD completed=FALSE;
	UNIT_START();
	try
	{
		completed=TRUE;
	}
	catch(Exception &e)
	{
		e.log();
	}
	catch(std::exception &e)
	{
		fprintf(stderr,"%s\n", e.what());
	}
	catch(...)
	{
		feLog("uncaught exception\n");
	}
	UNIT_TEST(completed);
	UNIT_TRACK(2);
	UNIT_RETURN();
}

