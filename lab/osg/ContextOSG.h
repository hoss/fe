/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __osg_ContextOSG_h__
#define __osg_ContextOSG_h__

#include "fe/data.h"
namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Encapsulate start/shutdown of OSG

	@ingroup osg
*//***************************************************************************/
class FE_DL_EXPORT ContextOSG: virtual public Component
{
	public:
					ContextOSG(void);
virtual				~ContextOSG(void);
};

} /* namespace ext */
} /* namespace fe */

#endif /* __osg_ContextOSG_h__ */

