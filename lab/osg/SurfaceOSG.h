/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __osg_SurfaceOSG_h__
#define __osg_SurfaceOSG_h__
namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Surface in OpenSceneGraph

	@ingroup osg
*//***************************************************************************/
class FE_DL_EXPORT SurfaceOSG: public SurfaceTriangles,
	public Initialize<SurfaceOSG>
{
	protected:
	class Impact:
		public SurfaceSphere::Impact,
		public CastableAs<Impact>
	{
		public:
							Impact(void)									{}
	virtual					~Impact(void)									{}

	virtual	SpatialVector	intersection(void)	{ return m_intersection; }
	virtual	SpatialVector	normal(void)		{ return m_normal; }

		protected:
	virtual	void			resolve(void)									{}
	};

	public:
							SurfaceOSG(void);
virtual						~SurfaceOSG(void)								{}

void						initialize(void);

							//* As Protectable
virtual	Protectable*		clone(Protectable* pInstance=NULL);

							//* As SurfaceI
virtual SpatialTransform	sample(Vector2 a_uv) const;

virtual	sp<ImpactI>			rayImpact(const SpatialVector& a_origin,
									const SpatialVector& a_direction,
									Real a_maxDistance) const;

							//* As DrawableI
virtual	void				draw(sp<DrawI> a_spDrawI,
									const Color* a_color) const;

	private:

	typedef	osg::ref_ptr<osg::DrawArrays> RefArray;
	typedef	osg::ref_ptr<osg::DrawElementsUShort> RefShort;

	class Array
	{
		public:
					Array(void): which(0)									{}
			void	bind(RefArray& a_refArray)
					{
						which=0;
						m_refArray=a_refArray;
					}
			void	bind(RefShort& a_refShort)
					{
						which=1;
						m_refShort=a_refShort;
					}
			U32		count(void)
					{
						return which? m_refShort->size():
								m_refArray->getCount();
					}
			U32		operator[](U32 a_index)
					{
						return which? m_refShort->at(a_index):
								m_refArray->index(a_index);
					}

		private:
			U32	which;
			RefArray	m_refArray;
			RefShort	m_refShort;
	};

virtual	void				cache(void);

		sp<Component>		m_spContextOSG;
		sp<SceneNodeI>		m_spSceneNodeI;

mutable	CountedPool<Impact>	m_osgImpactPool;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __osg_SurfaceOSG_h__ */

