/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <osg/osg.pmh>

namespace fe
{
namespace ext
{

SceneNodeOSG::SceneNodeOSG(void)
{
}

SceneNodeOSG::~SceneNodeOSG(void)
{
}

BWORD SceneNodeOSG::load(String a_filename)
{
	feLog("SceneNodeOSG::load \"%s\"\n",a_filename.c_str());

	try
	{
		m_refNode=osgDB::readNodeFile(a_filename.c_str());
	}
	catch(std::exception &e)
	{
		feLog("SceneNodeOSG::load exception \"%s\"\n",e.what());
	}
	catch(...)
	{
		feLog("SceneNodeOSG::load unrecognized exception\n");
	}

	feLog("SceneNodeOSG::load \"%s\" valid=%d\n",
			a_filename.c_str(),m_refNode.valid());

	return m_refNode.valid();
}

U32 SceneNodeOSG::childCount(void) const
{
	if(!m_refNode.valid())
	{
		return 0;
	}

	osg::ref_ptr<osg::Group> refGroup=m_refNode->asGroup();
	if(!refGroup.valid())
	{
		return 0;
	}

	return refGroup->getNumChildren();
}


sp<SceneNodeI> SceneNodeOSG::child(U32 a_index) const
{
	if(a_index>=childCount())
	{
		return sp<SceneNodeI>(NULL);
	}

	sp<SceneNodeOSG> spChild(Library::create<SceneNodeOSG>("SceneNodeOSG"));
	osg::ref_ptr<osg::Group> refGroup=m_refNode->asGroup();
	spChild->m_refNode=refGroup->getChild(a_index);
	return spChild;
}

sp<SceneNodeI> SceneNodeOSG::createChild(void)
{
	sp<SceneNodeOSG> spChild(Library::create<SceneNodeOSG>("SceneNodeOSG"));
	append(spChild);
	return spChild;
}

BWORD SceneNodeOSG::append(sp<SceneNodeI> a_spChild)
{
	if(!m_refNode.valid() || !m_refNode->asGroup())
	{
		m_refNode=new osg::Group();
	}

	sp<SceneNodeOSG> spChildOSG=a_spChild;
	if(!spChildOSG.isValid())
	{
		return FALSE;
	}

	osg::ref_ptr<osg::Group> refGroup=m_refNode->asGroup();
	FEASSERT(refGroup.valid());

	return refGroup->addChild(spChildOSG->m_refNode.get());
}

BWORD SceneNodeOSG::remove(sp<SceneNodeI> a_spChild)
{
	if(!m_refNode.valid() || !m_refNode->asGroup())
	{
		return FALSE;
	}

	sp<SceneNodeOSG> spChildOSG=a_spChild;
	if(!spChildOSG.isValid())
	{
		return FALSE;
	}

	osg::ref_ptr<osg::Group> refGroup=m_refNode->asGroup();
	FEASSERT(refGroup.valid());

	return refGroup->removeChild(spChildOSG->m_refNode.get());
}

} /* namespace ext */
} /* namespace fe */