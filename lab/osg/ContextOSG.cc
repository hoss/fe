/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <osg/osg.pmh>

namespace fe
{
namespace ext
{

ContextOSG::ContextOSG(void)
{
	feLog("ContextOSG::ContextOSG()\n");
}

ContextOSG::~ContextOSG(void)
{
	feLog("ContextOSG::~ContextOSG()\n");

	const bool erase=true;
	osgDB::Registry::instance(erase);
}

} /* namespace ext */
} /* namespace fe */

