/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <osg/osg.pmh>

#define FE_SOSG_CACHE_DEBUG		FALSE

namespace fe
{
namespace ext
{

SurfaceOSG::SurfaceOSG(void)
{
	feLog("SurfaceOSG()\n");
}

void SurfaceOSG::initialize(void)
{
	feLog("SurfaceOSG:initialize\n");

	m_spContextOSG=registry()->create("*.ContextOSG");
	if(!m_spContextOSG.isValid())
	{
		feX("SurfaceOSG::initialize","Unable to create ContextOSG\n");
	}

	m_spSceneNodeI=registry()->create("SceneNodeI.SceneNodeOSG");
	if(!m_spSceneNodeI.isValid())
	{
		feX("SurfaceOSG::initialize","Unable to create SceneNodeOSG\n");
	}
}

Protectable* SurfaceOSG::clone(Protectable* pInstance)
{
	feLog("SurfaceOSG::clone\n");

	SurfaceOSG* pSurfaceOSG= pInstance?
			fe_cast<SurfaceOSG>(pInstance): new SurfaceOSG();

	checkCache();

	SurfaceSphere::clone(pSurfaceOSG);

	//* copy attributes

	// TODO may change, should copy
	pSurfaceOSG->m_spSceneNodeI=m_spSceneNodeI;

	return pSurfaceOSG;
}

void SurfaceOSG::cache(void)
{
	feLog("SurfaceOSG::cache\n");

	if(!m_record.isValid())
	{
		feLog("SurfaceOSG::cache record invalid\n");
		return;
	}

	SurfaceTriangles::cache();

	SurfaceFile surfaceFileRV;
	surfaceFileRV.bind(m_record);

	if(m_spSceneNodeI.isValid())
	{
		String path=registry()->master()->catalog()->catalog<String>(
				"path:media")+"/"+surfaceFileRV.filename();

		m_spSceneNodeI->load(path);

		feLog("SurfaceOSG::cache childCount %d\n",m_spSceneNodeI->childCount());
		if(m_spSceneNodeI->childCount()!=1)
		{
			feLog("SurfaceOSG::cache expected one child surface -> abort\n");
			return;
		}

		FEASSERT(m_spSceneNodeI->childCount()==1);

		sp<SceneNodeOSG> spSceneNodeOSG=m_spSceneNodeI->child(0);
		FEASSERT(spSceneNodeOSG.isValid());

		osg::ref_ptr<osg::Node> refNode=spSceneNodeOSG->node();
		FEASSERT(refNode.valid());

		osg::ref_ptr<osg::Geode> refGeode=
				fe_cast<osg::Geode>(refNode.get());
		FEASSERT(refGeode.valid());

		U32 drawCount=refGeode->getNumDrawables();
		FEASSERT(drawCount==1);

		osg::ref_ptr<osg::Drawable> refDrawable=refGeode->getDrawable(0);
		FEASSERT(refDrawable.valid());

		osg::ref_ptr<osg::Geometry> refGeometry=refDrawable->asGeometry();
		FEASSERT(refGeometry.valid());

		const osg::ref_ptr<osg::Array> refVertexArray=
				refGeometry->getVertexArray();
		FEASSERT(refVertexArray.valid());

		const GLvoid* pVertexArray=refVertexArray->getDataPointer();
		const osg::Vec3* pVertex=(const osg::Vec3*)pVertexArray;

		const osg::ref_ptr<osg::Array> refNormalArray=
				refGeometry->getNormalArray();
		FEASSERT(refNormalArray.valid());

		const GLvoid* pNormalArray=refNormalArray->getDataPointer();
		const osg::Vec3* pNormal=(const osg::Vec3*)pNormalArray;

		U32 vertexCount=refVertexArray->getNumElements();
		U32 normalCount=refNormalArray->getNumElements();
		FEASSERT(vertexCount==normalCount);	//* always?

#if FE_SOSG_CACHE_DEBUG
		I32 grain=refVertexArray->getDataSize();
		GLenum dataType=refVertexArray->getDataType();

		feLog("SurfaceOSG::cache vertexCount %d\n",vertexCount);
		feLog("SurfaceOSG::cache grain %d\n",grain);
		feLog("SurfaceOSG::cache dataType %d\n",dataType);
		feLog("SurfaceOSG::cache pVertexArray %p\n",pVertexArray);

		for(U32 v=0;v<vertexCount;v++)
		{
			const osg::Vec3& vert3=pVertex[v];
			feLog("SurfaceOSG::cache vertex %d  %.6G %.6G %.6G\n",
					v,vert3[0],vert3[1],vert3[2]);
		}
		for(U32 v=0;v<normalCount;v++)
		{
			const osg::Vec3& norm3=pNormal[v];
			feLog("SurfaceOSG::cache normal %d  %.6G %.6G %.6G\n",
					v,norm3[0],norm3[1],norm3[2]);
		}
#endif

		//* first pass: just count
		vertexCount=0;
		U32 setCount=refGeometry->getNumPrimitiveSets();
		for(U32 m=0;m<setCount;m++)
		{
			const osg::ref_ptr<osg::PrimitiveSet> refPrimitiveSet=
					refGeometry->getPrimitiveSet(m);
			FEASSERT(refPrimitiveSet.valid());

			GLenum drawMode=refPrimitiveSet->getMode();
			GLenum indexCount=refPrimitiveSet->getNumIndices();
			switch(drawMode)
			{
				case osg::PrimitiveSet::TRIANGLES:
				{
					vertexCount+=indexCount;
				}
				break;

				case osg::PrimitiveSet::TRIANGLE_STRIP:
				{
					vertexCount+=(indexCount-2)*3;
				}
			}
		}

		m_pVertexArray=(SpatialVector*)reallocate(m_pVertexArray,
				(m_vertices+vertexCount)*sizeof(SpatialVector));
		m_pNormalArray=(SpatialVector*)reallocate(m_pNormalArray,
				(m_vertices+vertexCount)*sizeof(SpatialVector));

		//* second pass: convert
		for(U32 m=0;m<setCount;m++)
		{
			const osg::ref_ptr<osg::PrimitiveSet> refPrimitiveSet=
					refGeometry->getPrimitiveSet(m);
			FEASSERT(refPrimitiveSet.valid());

			GLenum drawMode=refPrimitiveSet->getMode();
			GLenum indexCount=refPrimitiveSet->getNumIndices();

#if FE_SOSG_CACHE_DEBUG
			GLenum drawType=refPrimitiveSet->getType();

			feLog("\nSurfaceOSG::cache %d/%d\n",m,setCount);
			feLog("SurfaceOSG::cache drawMode %d\n",drawMode);
			feLog("SurfaceOSG::cache drawType %d\n",drawType);
			feLog("SurfaceOSG::cache indexCount %d\n",indexCount);
			for(U32 n=0;n<indexCount;n++)
			{
				feLog("SurfaceOSG::cache %d/%d  %hd\n",
						n,indexCount,refPrimitiveSet->index(n));
			}

			switch(drawType)
			{
				case osg::PrimitiveSet::DrawArraysPrimitiveType:
				{
					feLog("SurfaceOSG::cache"
							" drawType is DrawArraysPrimitiveType\n");
				}
				break;

				case osg::PrimitiveSet::DrawElementsUShortPrimitiveType:
				{
					feLog("SurfaceOSG::cache"
							" drawType is DrawElementsUShortPrimitiveType\n");
				}
				break;
			}
#endif

			switch(drawMode)
			{
				case osg::PrimitiveSet::TRIANGLES:
				{
#if FE_SOSG_CACHE_DEBUG
					feLog("SurfaceOSG::cache"
							" drawMode is TRIANGLES\n");
#endif

					for(U32 n=0;n<indexCount;n++)
					{
						const U32 index=refPrimitiveSet->index(n);

						const osg::Vec3& vert3=pVertex[index];
						const osg::Vec3& norm3=pNormal[index];

						SpatialVector location(vert3[0],vert3[1],vert3[2]);
						SpatialVector up(norm3[0],norm3[1],norm3[2]);

						SpatialVector& rVertex=m_pVertexArray[m_vertices];
						SpatialVector& rNormal=m_pNormalArray[m_vertices];

						rVertex=location;
						rNormal=up;

#if FE_SOSG_CACHE_DEBUG
						feLog("SurfaceOSG::cache %d/%d  %hd\n",
								n,indexCount,index);
						feLog("  vertex %d  %s  %s\n",m_vertices,
								c_print(rVertex),c_print(rNormal));
#endif
						m_vertices++;
					}
				}
				break;

				case osg::PrimitiveSet::TRIANGLE_STRIP:
				{
#if FE_SOSG_CACHE_DEBUG
					feLog("SurfaceOSG::cache"
							" drawMode is TRIANGLE_STRIP\n");
#endif
					for(U32 n=2;n<indexCount;n++)
					{

						const I32 remap[3]={1,0,2};
						for(I32 v=0;v<3;v++)
						{
							const I32 vv=(n%2)? n+remap[v]-2: n+v-2;

							const U32 ii=refPrimitiveSet->index(vv);
							const osg::Vec3& vert3=pVertex[ii];
							const osg::Vec3& norm3=pNormal[ii];

							SpatialVector location(vert3[0],vert3[1],vert3[2]);
							SpatialVector up(norm3[0],norm3[1],norm3[2]);

							SpatialVector& rVertex=m_pVertexArray[m_vertices];
							SpatialVector& rNormal=m_pNormalArray[m_vertices];

							rVertex=location;
							rNormal=up;

#if FE_SOSG_CACHE_DEBUG
							const U32 index=refPrimitiveSet->index(n);
							feLog("SurfaceOSG::cache %d/%d +%d %hd\n",
									n,v,indexCount,index);
							feLog("  vertex %d  %s  %s\n",m_vertices,
									c_print(rVertex),c_print(rNormal));
#endif
							m_vertices++;
						}
					}
				}
				break;
			}
		}
	}

/*
	if(m_spSpatialTreeI.isValid())
	{
		m_spSpatialTreeI->populate(m_pVertexArray,m_pNormalArray,
				m_vertices,radius());
	}
*/
}

SpatialTransform SurfaceOSG::sample(Vector2 a_uv) const
{
	SpatialTransform result;

	return result;
}

sp<SurfaceSphere::ImpactI> SurfaceOSG::rayImpact(const SpatialVector& a_origin,
		const SpatialVector& a_direction, Real a_maxDistance) const
{
	feLog("SurfaceOSG::rayImpact\n");

	sp<SceneNodeOSG> spSceneNodeOSG=m_spSceneNodeI;
	if(!spSceneNodeOSG.isValid())
	{
		feLog("SurfaceOSG::rayImpact invalid OSG\n");
		return sp<Impact>(NULL);
	}

	const Real maxDistance=1e3;
	const SpatialVector endPoint=a_origin+a_direction*maxDistance;

	osg::LineSegment* lineSegment = new osg::LineSegment();
	lineSegment->set(
			osg::Vec3(a_origin[0],a_origin[1],a_origin[2]),
			osg::Vec3(endPoint[0],endPoint[1],endPoint[2]));

	osgUtil::IntersectVisitor intersectVisitor;
	intersectVisitor.addLineSegment(lineSegment);

	osg::ref_ptr<osg::Node>	refNode=spSceneNodeOSG->node();
	refNode->accept(intersectVisitor);

	osgUtil::IntersectVisitor::HitList hitList;
	hitList=intersectVisitor.getHitList(lineSegment);

	if(hitList.empty())
	{
		return sp<Impact>(NULL);
	}

	osgUtil::Hit frontHit;
	frontHit=hitList.front();
	osg::Vec3d intersectPoint=frontHit.getWorldIntersectPoint();

	const Vector3 intersection(intersectPoint[0],intersectPoint[1],
			intersectPoint[2]);
	const Real distance=magnitude(intersection-a_origin);

	if(a_maxDistance>0.0 && distance>a_maxDistance)
	{
		return sp<Impact>(NULL);
	}

	osg::Vec3d intersectNormal=frontHit.getWorldIntersectNormal();
	const Vector3 normal(intersectNormal[0],intersectNormal[1],
			intersectNormal[2]);

//	feLog("\nSurfaceOSG::rayImpact %s to %s\n",
//			c_print(a_origin),c_print(endPoint));
//	feLog("SurfaceOSG::rayImpact intersect %s n %s\n",
//			c_print(intersection),c_print(normal));

	sp<Impact> spImpact=m_osgImpactPool.get();

	spImpact->setSurface(this);
	spImpact->setLocationLocal(m_center);
	spImpact->setRadius(m_radius);
	spImpact->setOrigin(a_origin);
	spImpact->setDirection(a_direction);
	spImpact->setDistance(distance);
	spImpact->setIntersectionLocal(intersection);
	spImpact->setNormalLocal(normal);

	return spImpact;
}

void SurfaceOSG::draw(sp<DrawI> a_spDrawI,const Color* a_color) const
{
	SurfaceTriangles::draw(a_spDrawI,a_color);
}

} /* namespace ext */
} /* namespace fe */

