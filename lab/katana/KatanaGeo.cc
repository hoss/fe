/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <katana/katana.pmh>

#define FE_KGO_DEBUG	FALSE

//* TODO can store state in output attributes or AttributeKeyedCache

namespace fe
{
namespace ext
{

std::map<String, sp<KatanaGeo::KatanaState> > KatanaGeo::ms_stateMap;

KatanaGeo::KatanaState::KatanaState(void)
{
//	feLog("KatanaGeo::KatanaState::KatanaState\n");

	m_pKatanaContext=new KatanaContext();

	m_pKatanaContext->loadLibraries();

	m_pTerminalNode=new TerminalNode();
}

KatanaGeo::KatanaState::~KatanaState(void)
{
//	feLog("KatanaGeo::KatanaState::~KatanaState\n");

	delete m_pTerminalNode;
	m_pTerminalNode=NULL;

	//* TODO
//	delete m_pKatanaContext;
	m_pKatanaContext=NULL;
}

//* static
void KatanaGeo::setup(Foundry::Katana::GeolibSetupInterface &interface)
{
//	feLog("KatanaGeo::setup\n");

	interface.setThreading(
		Foundry::Katana::GeolibSetupInterface::ThreadModeConcurrent);

//	feLog("KatanaGeo::setup done\n");
}

//* static
void KatanaGeo::cook(Foundry::Katana::GeolibCookInterface &interface)
{
	const String outputPath=interface.getOutputLocationPath().c_str();
	const String opType=interface.getOpType().c_str();

#if FE_KGO_DEBUG
	feLog("KatanaGeo::cook \"%s\" type \"%s\"\n",
			outputPath.c_str(),opType.c_str());
#endif

	FnAttribute::StringAttribute feComponentAttr=
			interface.getOpArg("fe_component");
	if(!feComponentAttr.isValid())
	{
		feLog("KatanaGeo::cook no fe_component attr\n");
		return;
	}
	const String componentName=feComponentAttr.getValue().c_str();
	if(componentName.empty())
	{
		feLog("KatanaGeo::cook empty fe_component\n");
		return;
	}

	FnAttribute::StringAttribute locationAttr=
			interface.getOpArg("location");
	if(!locationAttr.isValid())
	{
		feLog("KatanaGeo::cook no location attr\n");
		return;
	}
	const String location=locationAttr.getValue().c_str();
	if(location.empty())
	{
		feLog("KatanaGeo::cook empty location\n");
		return;
	}

	if(strncmp(location.c_str(),outputPath.c_str(),outputPath.length()))
	{
		//* TODO remove message once confirmed that it works
		feLog("KatanaGeo::cook location \"%s\" not in path \"%s\"\n",
				location.c_str(),outputPath.c_str());
		return;
	}

	String buffer=location;
	String at;
	while(!buffer.empty())
	{
		const String token=buffer.parse("","/");

		if(at==outputPath)
		{
#if FE_KGO_DEBUG
			feLog("KatanaGeo::cook create \"%s\" under \"%s\"\n",
					token.c_str(),outputPath.c_str());
#endif
			interface.createChild(token.c_str(),"",interface.getOpArg());
			interface.stopChildTraversal();
			return;
		}

		at+="/"+token;
	}

	Real currentFrame(0);
	FnAttribute::FloatAttribute currentFrameAttr=
			interface.getOpArg("currentFrame");
	if(currentFrameAttr.isValid())
	{
		currentFrame=currentFrameAttr.getValue();
	}

	BWORD newState=FALSE;

	sp<KatanaState>& rspKatanaState=ms_stateMap[location];
	if(rspKatanaState.isNull())
	{
		feLog(">>>>>>>> CREATE NEW KatanaState <<<<<<<<\n");
		rspKatanaState=new KatanaState();
		newState=TRUE;
	}
	KatanaContext& katanaContext= *(rspKatanaState->m_pKatanaContext);
	TerminalNode& terminalNode= *(rspKatanaState->m_pTerminalNode);

	sp<Master> spMaster=katanaContext.master();

	if(newState)
	{
		sp<Scope> spScope=spMaster->catalog()->catalogComponent(
				"Scope","OperatorScope");

		terminalNode.setupOperator(spScope,
				componentName,componentName+".katana");
	}

	sp<Catalog> spCatalog=terminalNode.catalog();

	Array<String> keys;
	spCatalog->catalogKeys(keys);

	const I32 keyCount=keys.size();
	for(I32 keyIndex=0;keyIndex<keyCount;keyIndex++)
	{
		const String key=keys[keyIndex];

		const fe_type_info& typeInfo=spCatalog->catalogTypeInfo(key);

//		feLog("key \"%s\"\n",key.c_str());

		String keyType;
		if(typeInfo==getTypeId<bool>())
		{
			FnAttribute::IntAttribute intAttr=
					interface.getOpArg(key.c_str());
//			feLog("  bool %d\n",intAttr.isValid());
			if(intAttr.isValid())
			{
//				feLog("  is %d\n",intAttr.getValue());
				spCatalog->catalog<bool>(key)=intAttr.getValue();
			}
		}
		else if(typeInfo==getTypeId<String>())
		{
			FnAttribute::StringAttribute stringAttr=
					interface.getOpArg(key.c_str());
//			feLog("  string %d\n",stringAttr.isValid());
			if(stringAttr.isValid())
			{
//				feLog("  is \"%s\"\n",stringAttr.getValue().c_str());
				spCatalog->catalog<String>(key)=stringAttr.getValue().c_str();
			}
		}
		else if(typeInfo==getTypeId<I32>())
		{
			FnAttribute::IntAttribute intAttr=
					interface.getOpArg(key.c_str());
//			feLog("  int %d\n",intAttr.isValid());
			if(intAttr.isValid())
			{
//				feLog("  is %d\n",intAttr.getValue());
				spCatalog->catalog<I32>(key)=intAttr.getValue();
			}
		}
		else if(typeInfo==getTypeId<Real>())
		{
			FnAttribute::FloatAttribute floatAttr=
					interface.getOpArg(key.c_str());
//			feLog("  float %d\n",floatAttr.isValid());
			if(floatAttr.isValid())
			{
//				feLog("  is %.6G\n",floatAttr.getValue());
				spCatalog->catalog<Real>(key)=floatAttr.getValue();
			}
		}
		else if(typeInfo==getTypeId<SpatialVector>())
		{
			//* TODO
		}
	}

	//* TEMP for FurOp
	const String loadfile=spCatalog->catalog<String>("loadfile");
	String& rOptions=spCatalog->catalog<String>("options");
	const String originalOptions=rOptions;

	Real curl_amplitude(1);
	FnAttribute::FloatAttribute curlAmplitudeAttr=
			interface.getOpArg("curl.amplitude");
	if(curlAmplitudeAttr.isValid())
	{
		curl_amplitude=curlAmplitudeAttr.getValue();
	}

	Real curl_spin(1);
	FnAttribute::FloatAttribute curlSpinAttr=
			interface.getOpArg("curl.spin");
	if(curlSpinAttr.isValid())
	{
		curl_spin=curlSpinAttr.getValue();
	}

	String extraOptions;
	extraOptions.sPrintf("curl_amplitude=%.6G curl_spin=%.6G",
			curl_amplitude,curl_spin);

	rOptions+=(rOptions.empty()? " ": "")+extraOptions;

#if FE_KGO_DEBUG
	feLog("KatanaGeo::cook loadfile \"%s\" options \"%s\"\n",
			loadfile.c_str(),rOptions.c_str());
#endif

	const fe::String nodeName=outputPath;

	sp<SurfaceAccessibleI> spOutputAccessible=
			spMaster->registry()->create("*.SurfaceAccessibleCatalog");

	//* TODO only put in master catalog if used by another node

	sp<SurfaceAccessibleCatalog> spSurfaceAccessibleCatalog=
			spOutputAccessible;
	if(spSurfaceAccessibleCatalog.isNull())
	{
		return;
	}

	spSurfaceAccessibleCatalog->setCatalog(spMaster->catalog());
	spSurfaceAccessibleCatalog->setKey(nodeName);
	spSurfaceAccessibleCatalog->setPersistent(TRUE);

	spCatalog->catalog< sp<Component> >("Output Surface")=spOutputAccessible;

	//* HACK
	const BWORD copyInput=(spCatalog->catalogOrDefault<String>(
			"Output Surface","IO","").empty() ||
			!spCatalog->catalogOrDefault<String>(
			"Output Surface","copy","").empty());

	if(copyInput)
	{
		sp<SurfaceAccessibleI> spInputAccessible=
				spCatalog->catalog< sp<Component> >("Input Surface");

		if(spInputAccessible.isValid())
		{
			spOutputAccessible->copy(spInputAccessible);
		}
	}
	else
	{
		spOutputAccessible->clear();
	}

	const Real frame=currentFrame;

#if FE_KGO_DEBUG
#endif
	feLog("KatanaGeo::cook terminalNode cook %.6G\n",currentFrame);

	terminalNode.cook(frame);

#if FE_KGO_DEBUG
	feLog("KatanaGeo::cook terminalNode cook done\n");
#endif

	rOptions=originalOptions;

	//* convert to Katana geometry

	sp<SurfaceAccessorI> spOutputPosition=
			spOutputAccessible->accessor(
			SurfaceAccessibleI::e_point,
			SurfaceAccessibleI::e_position);
	if(spOutputPosition.isNull())
	{
		feLog("KatanaGeo::cook failed access to point positions\n");
		return;
	}

	sp<SurfaceAccessorI> spOutputVertices=
			spOutputAccessible->accessor(
			SurfaceAccessibleI::e_primitive,
			SurfaceAccessibleI::e_vertices);
	if(spOutputVertices.isNull())
	{
		feLog("KatanaGeo::cook failed access to primitive vertices\n");
		return;
	}

	const I32 pointCount=spOutputPosition->count();
	const I32 primitiveCount=spOutputVertices->count();

#if FE_KGO_DEBUG
	feLog("KatanaGeo::cook point %d primitives %d\n",pointCount,primitiveCount);
#endif

	I32 vertexCount=pointCount;	//* TODO

	float* pointArray=new float[pointCount*3];
	int* vertexIndexArray=new int[vertexCount];
	int* startIndexArray=new int[primitiveCount];
	int* subCountArray=new int[primitiveCount];

	I32 floatIndex=0;
	for(I32 pointIndex=0;pointIndex<pointCount;pointIndex++)
	{
		const SpatialVector point=spOutputPosition->spatialVector(pointIndex);
		pointArray[floatIndex++]=point[0];
		pointArray[floatIndex++]=point[1];
		pointArray[floatIndex++]=point[2];
	}

	I32 vertexIndex=0;
	for(I32 primitiveIndex=0;primitiveIndex<primitiveCount;primitiveIndex++)
	{
		const I32 subCount=spOutputVertices->subCount(primitiveIndex);
		for(I32 subIndex=0;subIndex<subCount;subIndex++)
		{
			const I32 pointIndex=
					spOutputVertices->integer(primitiveIndex,subIndex);
			if(!subIndex)
			{
				startIndexArray[primitiveIndex]=vertexIndex;
			}
			vertexIndexArray[vertexIndex++]=pointIndex;
		}
		subCountArray[primitiveIndex]=subCount;
	}

	FnAttribute::GroupBuilder gnPoints;
	gnPoints.set("P",
			FnAttribute::FloatAttribute(pointArray,pointCount*3,3));

	FnAttribute::GroupBuilder gbPrimitives;
	gbPrimitives.set("vertexList",
			FnAttribute::IntAttribute(vertexIndexArray,vertexCount,1));
	gbPrimitives.set("startIndex",
			FnAttribute::IntAttribute(startIndexArray,primitiveCount,1));

	FnAttribute::GroupBuilder gbCurves;
	gbCurves.set("numVertices",
			FnAttribute::IntAttribute(subCountArray,primitiveCount,1));
	gbCurves.set("point",gnPoints.build());
	gbCurves.set("poly",gbPrimitives.build());
	gbCurves.set("constantWidth",FnAttribute::FloatAttribute(0.1f));
	gbCurves.set("closed",FnAttribute::IntAttribute(0));
	gbCurves.set("degree",FnAttribute::IntAttribute(1));
	gbCurves.set("knots",FnAttribute::FloatAttribute(0.0f));

#if FALSE
	FnAttribute::DoubleBuilder doubleBuilder(16);
	std::vector<double>& matrix=doubleBuilder.get(0);
	matrix.resize(16);

	matrix[0]=1;
	matrix[1]=0;
	matrix[2]=0;
	matrix[3]=0;

	matrix[4]=0;
	matrix[5]=1;
	matrix[6]=0;
	matrix[7]=0;

	matrix[8]=0;
	matrix[9]=0;
	matrix[10]=1;
	matrix[11]=0;

	matrix[12]=0;
	matrix[13]=0;
	matrix[14]=0;
	matrix[15]=1;

	FnAttribute::GroupBuilder gbXform;
	gbXform.set("matrix",doubleBuilder.build());
#else
	FnKat::GroupBuilder gbXform;

	const double translate[]={ 0.0, 0.0, 0.0 };
	gbXform.set("translate", FnKat::DoubleAttribute(translate, 3, 3));

	const double rxValues[]={ 0.0, 1.0, 0.0, 0.0 };
	const double ryValues[]={ 0.0, 0.0, 1.0, 0.0 };
	const double rzValues[]={ 0.0, 0.0, 0.0, 1.0 };
	gbXform.set("rotateX", FnKat::DoubleAttribute(rxValues, 4, 4));
	gbXform.set("rotateY", FnKat::DoubleAttribute(ryValues, 4, 4));
	gbXform.set("rotateZ", FnKat::DoubleAttribute(rzValues, 4, 4));

	const double scaleValues[]={ 1.0, 1.0, 1.0 };
	gbXform.set("scale", FnKat::DoubleAttribute(scaleValues, 3, 3));

	gbXform.setGroupInherit(false);
#endif

	interface.setAttr("xform",gbXform.build());
	interface.setAttr("geometry",gbCurves.build());
	interface.setAttr("type",FnAttribute::StringAttribute("curves"));

	//* TODO can we delete?
//	delete[] startIndexArray;
//	delete[] vertexIndexArray;
//	delete[] pointArray;

	interface.stopChildTraversal();

#if FE_KGO_DEBUG
	feLog("KatanaGeo::cook done\n");
#endif
}

//* static
void KatanaGeo::flush(void)
{
	ms_stateMap.clear();
}

} /* namespace ext */
} /* namespace fe */
