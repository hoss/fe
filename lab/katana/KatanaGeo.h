/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __katana_KatanaGeo_h__
#define __katana_KatanaGeo_h__
namespace fe
{
namespace ext
{

/**************************************************************************//**
    @brief General Katana Geolib Operator

	@ingroup katana
*//***************************************************************************/

class KatanaGeo: public Foundry::Katana::GeolibOp
{
	public:

static	void	setup(Foundry::Katana::GeolibSetupInterface &interface);
static	void	cook(Foundry::Katana::GeolibCookInterface &interface);
static	void	flush(void);

	class KatanaState: public Counted
	{
		public:
					KatanaState(void);
	virtual			~KatanaState(void);

			KatanaContext*	m_pKatanaContext;
			TerminalNode*	m_pTerminalNode;
	};

static	std::map<String, sp<KatanaState> >	ms_stateMap;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __katana_KatanaGeo_h__ */
