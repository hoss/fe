/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __katana_KatanaContext_h__
#define __katana_KatanaContext_h__
namespace fe
{
namespace ext
{

/**************************************************************************//**
    @brief shared FE context for Katana plugins

	@ingroup katana
*//***************************************************************************/
class KatanaContext: public OperatorContext
{
	public:
				KatanaContext(void)
				{
					m_libEnvVar="FE_KATANA_LIBS";
					m_libDefault="fexKatanaDL";

				}
virtual			~KatanaContext(void)										{}
};

} /* namespace ext */
} /* namespace fe */

#endif /* __katana_KatanaContext_h__ */
