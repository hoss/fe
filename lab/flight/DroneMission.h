/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __flight_DroneMission_h__
#define __flight_DroneMission_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Plan and execute a drone flight

	@ingroup flight
*//***************************************************************************/
class FE_DL_EXPORT DroneMission:
	virtual public FlightMissionI,
	public Initialize<DroneMission>
{
	public:

							DroneMission(void);
virtual						~DroneMission(void);

		void				initialize(void);

							//* as FlightMissionI
virtual void				load(String a_filename);

virtual void				use(sp<StateCatalog> a_spStateCatalog)
							{	m_spStateCatalog=a_spStateCatalog; }

virtual void				start(void);

virtual void				stop(void);

virtual sp<StateCatalog>	state(void)		{ return m_spStateCatalog; }

virtual void				step(Real a_deltaTime);

	private:

		I32					m_frame;

		sp<CatalogReaderI>	m_spCatalogReader;
		sp<Catalog>			m_spPreCatalog;
		sp<StateCatalog>	m_spStateCatalog;

		sp<Scope>			m_spScope;
		sp<SignalerI>		m_spSignaler;

		sp<HandlerI>		m_spGameController;
		sp<HandlerI>		m_spDroneInput;
		sp<HandlerI>		m_spDroneControl;
		sp<HandlerI>		m_spDroneDynamics;

		sp<Layout>			m_spPollLayout;
		WindowEvent			m_pollEvent;
		Record				m_pollRecord;

		AsCatalog			m_asCatalog;

		sp<Layout>			m_spStepLayout;
		Record				m_stepRecord;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __flight_DroneMission_h__ */
