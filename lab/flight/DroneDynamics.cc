/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "flight/flight.pmh"

namespace fe
{
namespace ext
{

DroneDynamics::DroneDynamics(void):
	m_location(0,0,32),
	m_velocity(0,0,0),
	m_heading(0),
	m_pitch(0),
	m_bank(0)
{
};

DroneDynamics::~DroneDynamics(void)
{
};

void DroneDynamics::initialize(void)
{
}

void DroneDynamics::handleBind(sp<SignalerI> spSignalerI,sp<Layout> spLayout)
{
}

void DroneDynamics::handle(Record& a_signal)
{
//	feLog("DroneDynamics::handle layout \"%s\"\n",
//			a_signal.layout()->name().c_str());

	if(!m_asCatalog.bind(a_signal))
	{
		return;
	}

	sp<Catalog>& rspStepCatalog=m_asCatalog.catalog(a_signal);
	const Real deltaTime=rspStepCatalog->catalog<Real>("deltaTime");

	//* Mavic 2 Pro Specs (sport mode)
	//* weight				0.9 kg
	//* speed				20 m/s (72 kph)
	//* ascent				5 m/s
	//* descent				3 m/s
	//* tilt				35 deg
	//* ang vel				200 deg/s (yaw only?)
	//* gimbal tilt ang vel	120 deg/s (craft tilt as well?)
	//* Observed:
	//* tilt vel			0 to 35 deg in 0.2s? (hard to tell)
	//* Z vel				~120 deg/s

	//* TODO DroneControl should set separate engine speeds from inputs
	//* DroneDynamics should just accumulate and apply
	const Real inputThrottle=rspStepCatalog->catalog<Real>("input.throttle");
	const Real inputYaw=rspStepCatalog->catalog<Real>("input.yaw");
	const Real inputPitch=rspStepCatalog->catalog<Real>("input.pitch");
	const Real inputBank=rspStepCatalog->catalog<Real>("input.bank");

	const Real deadZone(0.1);
	const Real deadScale(1.0/(1.0-deadZone));
	const Real deadThrottle=copysign(
			fe::maximum(Real(0),(fabs(inputThrottle)-deadZone))*deadScale,
			inputThrottle);
	const Real deadYaw=copysign(
			fe::maximum(Real(0),(fabs(inputYaw)-deadZone))*deadScale,
			inputYaw);
	const Real deadPitch=copysign(
			fe::maximum(Real(0),(fabs(inputPitch)-deadZone))*deadScale,
			inputPitch);
	const Real deadBank=copysign(
			fe::maximum(Real(0),(fabs(inputBank)-deadZone))*deadScale,
			inputBank);

	const Real gamma(2);
	const Real gammaThrottle=
			copysign(pow(fabs(deadThrottle),gamma),deadThrottle);
	const Real gammaYaw=
			copysign(pow(fabs(deadYaw),gamma),deadYaw);
	const Real gammaPitch=
			copysign(pow(fabs(deadPitch),gamma),deadPitch);
	const Real gammaBank=
			copysign(pow(fabs(deadBank),gamma),deadBank);

	const Real maxVertical(3.0);
	const Real maxPitch(35.0*fe::degToRad);
	const Real maxBank(35.0*fe::degToRad);
	const Real gainVertical(10.0);
	const Real gainTilt(10.0);
	const Real maxAccVertical(10.0);
	const Real maxVelTilt(2);
	const Real scaleYaw(0.09);
	const Real scaleAdvance(50.0);

	const Real scaleVelocity(0.95);	//* HACK crude drag

	m_heading+=scaleYaw*gammaYaw;

	const Real desiredVertical=maxVertical*gammaThrottle;
	const Real desiredPitch=maxPitch*gammaPitch;
	const Real desiredBank=maxBank*gammaBank;

	const Real transitionVertical(0.1);
	const Real transitionTilt(0.1);

	Real adjustVertical=desiredVertical-m_velocity[2];
	adjustVertical*=gainVertical*fabs(tanh(adjustVertical/transitionVertical));
	adjustVertical=fe::maximum(-maxAccVertical,
			fe::minimum(maxAccVertical,adjustVertical));

	Real adjustPitch=desiredPitch-m_pitch;
	adjustPitch*=gainTilt*fabs(tanh(adjustPitch/transitionTilt));
	adjustPitch=
			fe::maximum(-maxVelTilt,fe::minimum(maxVelTilt,adjustPitch));

	Real adjustBank=desiredBank-m_bank;
	adjustBank*=gainTilt*fabs(tanh(adjustBank/transitionTilt));
	adjustBank=
			fe::maximum(-maxVelTilt,fe::minimum(maxVelTilt,adjustBank));

	m_pitch+=adjustPitch*deltaTime;
	m_bank+=adjustBank*deltaTime;

	SpatialTransform spin;
	setIdentity(spin);
	rotate(spin,m_heading,e_zAxis);

	SpatialVector advance(-m_pitch,-m_bank,0.0);
	m_velocity+=scaleAdvance*rotateVector(spin,advance)*deltaTime;
	m_velocity[2]+=adjustVertical*deltaTime;
	m_velocity[0]*=scaleVelocity;
	m_velocity[1]*=scaleVelocity;
	m_location+=m_velocity*deltaTime;

	feLog("drone heading %.3f vel %s\n",m_heading,c_print(m_velocity),
			adjustVertical);

	SpatialTransform xform;
	setIdentity(xform);
	translate(xform,m_location);
	rotate(xform,m_heading,e_zAxis);
	rotate(xform,-m_pitch,e_yAxis);
	rotate(xform,m_bank,e_xAxis);

	rspStepCatalog->catalog<SpatialTransform>("transform")=xform;
}

} /* namespace ext */
} /* namespace fe */
