/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __flight_SurfaceDrone_h__
#define __flight_SurfaceDrone_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Drawable suface and canera view of drone

	@ingroup flight
*//***************************************************************************/
class FE_DL_EXPORT SurfaceDrone:
	public SurfaceSphere,
	virtual public CameraEditable,
	public CastableAs<SurfaceDrone>
{
	public:

							SurfaceDrone(void);
virtual						~SurfaceDrone(void);

							//* As Protectable
virtual	Protectable*		clone(Protectable* pInstance=NULL);

							//* As SurfaceI

							using SurfaceSphere::draw;

virtual	void				draw(const SpatialTransform&,
								sp<DrawI> a_spDrawI,
								const Color* a_color,
								sp<DrawBufferI> a_spDrawBuffer,
								sp<PartitionI> a_spPartition)
								const;

							//* As CameraEditable
virtual
const	SpatialTransform&	cameraMatrix(void) const
								{	return m_xformCam; }

								/// @brief Field of View (degrees)
virtual	Vector2				fov(void) const
								{	return Vector2(90,90); }

	protected:

virtual	void				cache(void);

	private:

		void				updateState(void);

		sp<StateCatalog>	m_spStateCatalog;

		I32					m_frame;
		SpatialTransform	m_xformDrone;
		SpatialTransform	m_xformCam;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __flight_SurfaceDrone_h__ */
