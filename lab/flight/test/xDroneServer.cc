/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "flight/flight.h"

using namespace fe;
using namespace fe::ext;

int main(int argc,char** argv)
{
	UNIT_START();

	BWORD complete=FALSE;

	try
	{
		sp<SingleMaster> spSingleMaster=SingleMaster::create();
		sp<Master> spMaster=spSingleMaster->master();
		sp<Registry> spRegistry=spMaster->registry();

		Result result=spRegistry->manage("feAutoLoadDL");
		UNIT_TEST(successful(result));

		sp<FlightMissionI> spFlightMissionI=
				spRegistry->create("*.DroneMission");

		for(I32 arg=1;arg<argc;arg++)
		{
			spFlightMissionI->load(argv[arg]);
		}

		spFlightMissionI->start();

		sp<StateCatalog> spStateCatalog=spFlightMissionI->state();
		spStateCatalog->catalogDump();

		U32 frame(0);

		while(TRUE)
		{
			const Real deltaTime(1.0/30.0);

			spFlightMissionI->step(deltaTime);

			spStateCatalog->setState<I32>("frame",frame++);
			spStateCatalog->flush();

			milliSleep(1e3*deltaTime);
		}

		spFlightMissionI->stop();

		complete=TRUE;
	}
	catch(Exception &e) { e.log(); }
	catch(...)			{ feLog("uncaught exception\n"); }

	UNIT_TEST(complete);
	UNIT_TEST(!Counted::trackerCount());
	UNIT_TRACK(3);
	UNIT_RETURN();
}
