/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __flight_AsCatalog_h__
#define __flight_AsCatalog_h__

namespace fe
{
namespace ext
{

class FE_DL_EXPORT AsCatalog:
	public AccessorSet,
	public Initialize<AsCatalog>
{
	public:

		void initialize(void)
		{
			add(catalog,			FE_USE("sim:catalog"));
		}

		Accessor< sp<Catalog> >			catalog;
};

} /* namespace ext */
} /* namespace fe */
#endif
