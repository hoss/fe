/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __flight_DroneDynamics_h__
#define __flight_DroneDynamics_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Apply aircraft environment and manipulations into motion

	@ingroup flight
*//***************************************************************************/
class FE_DL_EXPORT DroneDynamics:
	virtual public HandlerI,
	public Initialize<DroneDynamics>
{
	public:

				DroneDynamics(void);
virtual			~DroneDynamics(void);

		void	initialize(void);

virtual	void	handleBind(sp<SignalerI> spSignalerI,sp<Layout> spLayout);
virtual	void	handle(Record& a_signal);

	private:

		AsCatalog		m_asCatalog;

		SpatialVector	m_location;
		SpatialVector	m_velocity;
		Real			m_heading;
		Real			m_pitch;
		Real			m_bank;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __flight_DroneDynamics_h__ */
