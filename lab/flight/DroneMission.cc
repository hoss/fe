/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "flight/flight.pmh"

namespace fe
{
namespace ext
{

DroneMission::DroneMission(void):
	m_frame(0)
{
}

DroneMission::~DroneMission(void)
{
}

void DroneMission::initialize(void)
{
	sp<Registry> spRegistry=registry();
	sp<Master> spMaster=spRegistry->master();

	m_spCatalogReader=spRegistry->create("CatalogReaderI.*.*.yaml");
	if(m_spCatalogReader.isNull())
	{
		feLog("DroneMission::initialize CatalogReader invalid");
	}

	m_spPreCatalog=spMaster->createCatalog("drone_config");
	if(m_spPreCatalog.isNull())
	{
		feX(e_invalidPointer,"DroneMission::initialize",
				"Catalog is invalid");
	}

	m_spScope=new Scope(spMaster);

	m_pollEvent.bind(m_spScope);
	m_pollRecord = m_pollEvent.createRecord();

	m_spPollLayout = m_pollRecord.layout();
	m_pollEvent.setSIS(
			WindowEvent::e_sourceSystem,
			WindowEvent::e_itemPoll,
			WindowEvent::e_stateNull);

	m_spStepLayout=m_spScope->declare("step");

	m_asCatalog.bind(m_spScope);
	m_asCatalog.populate(m_spStepLayout);

	m_stepRecord=m_spScope->createRecord(m_spStepLayout);
	m_asCatalog.catalog(m_stepRecord)=spMaster->createCatalog("drone_step");

	m_spSignaler=spRegistry->create("*.ChainSignaler");
	if(m_spSignaler.isNull())
	{
		return;
	}

	m_spGameController = registry()->create("JoyI");
	if(m_spGameController.isValid())
	{
		m_spSignaler->insert(m_spGameController,m_spPollLayout);
	}

	m_spDroneInput = registry()->create("*.DroneInput");
	if(m_spDroneInput.isValid())
	{
		m_spSignaler->insert(m_spDroneInput,sp<Layout>(NULL));
	}

	m_spDroneControl = registry()->create("*.DroneControl");
	if(m_spDroneControl.isValid())
	{
		m_spSignaler->insert(m_spDroneControl,m_spStepLayout);
	}

	m_spDroneDynamics = registry()->create("*.DroneDynamics");
	if(m_spDroneDynamics.isValid())
	{
		m_spSignaler->insert(m_spDroneDynamics,m_spStepLayout);
	}
}

void DroneMission::load(String a_filename)
{
	if(m_spCatalogReader.isNull())
	{
		return;
	}

	m_spCatalogReader->load(a_filename,m_spPreCatalog);
}

void DroneMission::start(void)
{
	if(m_spPreCatalog.isNull())
	{
		return;
	}

	const String implementation=
			m_spPreCatalog->catalogOrDefault<String>(
			"net:implementation","ConnectedCatalog");
	const String address=
			m_spPreCatalog->catalogOrDefault<String>(
			"net:address","127.0.0.1");
	const String transport=
			m_spPreCatalog->catalogOrDefault<String>(
			"net:transport","tcp");
	const I32 port=
			m_spPreCatalog->catalogOrDefault<I32>(
			"net:port",12345);

	//* make sure catalog is set, if defaulted
	m_spPreCatalog->catalog<String>("net:implementation")=implementation;
	m_spPreCatalog->catalog<String>("net:address")=address;
	m_spPreCatalog->catalog<String>("net:transport")=transport;
	m_spPreCatalog->catalog<I32>("net:port")=port;

	m_spPreCatalog->catalog<String>("net:role")="server";

	m_spStateCatalog=registry()->create(implementation);
	if(m_spStateCatalog.isNull())
	{
		feX(e_invalidPointer,"DroneMission::start",
				"StateCatalog is invalid");
	}

	m_spStateCatalog->overlayState(m_spPreCatalog);

	m_spStateCatalog->catalogDump();

	sp<Catalog> spMasterCatalog=registry()->master()->catalog();
	spMasterCatalog->catalog< sp<Catalog> >("world")=m_spStateCatalog;

	m_spStateCatalog->start();
}

void DroneMission::stop(void)
{
	if(m_spStateCatalog.isNull())
	{
		feX(e_invalidPointer,"DroneMission::stop",
				"StateCatalog is invalid");
	}

	m_spStateCatalog->stop();

	m_spStateCatalog=NULL;
}

void DroneMission::step(Real a_deltaTime)
{
	if(m_spSignaler.isNull())
	{
		feX(e_invalidPointer,"DroneMission::step",
				"Signaler is invalid");
	}

//	feLog("DroneMission::step %.4f\n",a_deltaTime);
	feLog("--------\n");

	sp<Catalog>& rspStepCatalog=m_asCatalog.catalog(m_stepRecord);
	rspStepCatalog->catalog<Real>("deltaTime")=a_deltaTime;

	m_spSignaler->signal(m_pollRecord);
	m_spSignaler->signal(m_stepRecord);

//	rspStepCatalog->catalogDump();

	const SpatialTransform xform=
			rspStepCatalog->catalog<SpatialTransform>("transform");
//	feLog("DroneMission::step transform\n%s\n",c_print(xform));

	m_spStateCatalog->setState<SpatialTransform>(
			"instance[0].root.transform",xform);
}

} /* namespace ext */
} /* namespace fe */
