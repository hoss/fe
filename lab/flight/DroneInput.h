/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __flight_DroneInput_h__
#define __flight_DroneInput_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Convert controller events into flight control inputs

	@ingroup flight
*//***************************************************************************/
class FE_DL_EXPORT DroneInput:
	virtual public HandlerI,
	public Initialize<DroneInput>
{
	public:

				DroneInput(void);
virtual			~DroneInput(void);

		void	initialize(void);

virtual	void	handleBind(sp<SignalerI> spSignalerI,sp<Layout> spLayout);
virtual	void	handle(Record& a_signal);

	private:

		void	handleEvent(Record& a_signal);

		WindowEvent			m_event;
		AsCatalog			m_asCatalog;

		Real				m_throttle;
		Real				m_yaw;
		Real				m_pitch;
		Real				m_bank;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __flight_DroneInput_h__ */
