/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __flight_FlightMissionI_h__
#define __flight_FlightMissionI_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Plan and execute a flight

	@ingroup flight
*//***************************************************************************/
class FE_DL_EXPORT FlightMissionI:
	virtual public Component,
	public CastableAs<FlightMissionI>
{
	public:
							/// @brief load values into the StateCatalog
virtual void				load(String a_filename)							=0;

							/// @brief use existing StateCatalog
virtual void				use(sp<StateCatalog> a_spStateCatalog)			=0;

							/// @brief get current StateCatalog
virtual sp<StateCatalog>	state(void)										=0;

							/// @brief create and connect StateCatalog
virtual void				start(void)										=0;

							/// @brief disconnect and release StateCatalog
virtual void				stop(void)										=0;

							/// @brief advance time
virtual void				step(Real a_deltaTime)							=0;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __flight_FlightMissionI_h__ */
