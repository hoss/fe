/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __flight_SurfaceAccessorDrone_h__
#define __flight_SurfaceAccessorDrone_h__

namespace fe
{
namespace ext
{

/**************************************************************************//**
    @brief Accessor for Drone meshes

	@ingroup flight
*//***************************************************************************/
class FE_DL_EXPORT SurfaceAccessorDrone:
	public SurfaceAccessorBase,
	public CastableAs<SurfaceAccessorDrone>
{
	public:
						SurfaceAccessorDrone(void);
virtual					~SurfaceAccessorDrone(void)							{}

						using SurfaceAccessorBase::set;
						using SurfaceAccessorBase::append;
						using SurfaceAccessorBase::spatialVector;

						//* as SurfaceAccessorI
		BWORD			bind(SurfaceAccessibleI::Element a_element,
							SurfaceAccessibleI::Attribute
							a_attribute);
		BWORD			bind(SurfaceAccessibleI::Element a_element,
							const String& a_name)
						{
							m_attribute=SurfaceAccessibleI::e_generic;
							return bindInternal(a_element,a_name);
						}
virtual	U32				count(void) const;
virtual	U32				subCount(U32 a_index) const;

virtual	void			set(U32 a_index,U32 a_subIndex,String a_string)		{}
virtual	String			string(U32 a_index,U32 a_subIndex=0);

virtual	void			set(U32 a_index,U32 a_subIndex,I32 a_integer)		{}
virtual	I32				integer(U32 a_index,U32 a_subIndex=0)	{ return 0; }

virtual	I32				append(SurfaceAccessibleI::Form a_form)
						{	return 0; }

virtual	void			append(U32 a_index,I32 a_integer)					{}

virtual	void			set(U32 a_index,U32 a_subIndex,Real a_real)			{}

virtual	Real			real(U32 a_index,U32 a_subIndex=0)	{ return 0.0; }

virtual	void			set(U32 a_index,U32 a_subIndex,
							const SpatialVector& a_vector)					{}
virtual	SpatialVector	spatialVector(U32 a_index,U32 a_subIndex=0)
						{	return SpatialVector(0,0,0); }

virtual	void			setSurface(sp<SurfaceI> a_spSurfaceI)
						{	m_spSurfaceI=a_spSurfaceI; }

	private:

virtual	BWORD			bindInternal(
							SurfaceAccessibleI::Element a_element,
							const String& a_name);

		BWORD			isBound(void) const
						{	return TRUE; }

		sp<SurfaceI>	m_spSurfaceI;
};

} /* namespace ext */
} /* namespace fe */


#endif /* __flight_SurfaceAccessorDrone_h__ */
