/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <shader/shader.pmh>

using namespace fe;
using namespace fe::ext;

void ColorShade::initialize(void)
{
	catalog<String>("output")="rgb";

	catalog<Color>("color")=Color(1.0f,1.0f,1.0f);
	catalog<I32>("color","channel")= -1;
}

void ColorShade::update(void)
{
	m_channelColor=catalog<I32>("color","channel");
}

void ColorShade::evaluate(sp<ShaderVariablesI> a_spShaderVariablesI) const
{
	const Color rgb=a_spShaderVariablesI->color(m_channelColor);

	a_spShaderVariablesI->set(rgb);
}
