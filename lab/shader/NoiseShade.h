/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __shader_NoiseShade_h__
#define __shader_NoiseShade_h__
namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Simple test shader to add noise

	@ingroup shader
*//***************************************************************************/
class FE_DL_EXPORT NoiseShade:
	public ShaderCommon,
	public Initialize<NoiseShade>
{
	public:

				NoiseShade(void)											{}
virtual			~NoiseShade(void)											{}

		void	initialize(void);

				//* As ShaderI
virtual	void	update(void);
virtual	void	evaluate(sp<ShaderVariablesI> a_spShaderVariablesI) const;

	private:
		I32		m_channelN;
		I32		m_channelAmplitude;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __shader_NoiseShade_h__ */
