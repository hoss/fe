/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <shader/shader.pmh>

using namespace fe;
using namespace fe::ext;

void NoiseShade::initialize(void)
{
	catalog<String>("output")="vector";

	catalog<I32>("N","channel")= -1;

	catalog<Real>("amplitude")=Real(1);
	catalog<I32>("amplitude","channel")= -1;
}

void NoiseShade::update(void)
{
	m_channelN=catalog<I32>("N","channel");
	m_channelAmplitude=catalog<I32>("amplitude","channel");
}

void NoiseShade::evaluate(sp<ShaderVariablesI> a_spShaderVariablesI) const
{
	const Real amplitude=a_spShaderVariablesI->real(m_channelAmplitude);
	const SpatialVector N=a_spShaderVariablesI->spatialVector(m_channelN);
	const SpatialVector displacement=randomReal(-amplitude,amplitude)*N;

	a_spShaderVariablesI->set(displacement);
}
