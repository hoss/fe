/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __element_Arena_h__
#define __element_Arena_h__
namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Arena RecordView

	@ingroup element
*//***************************************************************************/
class FE_DL_EXPORT Arena: public RecordView
{
	public:
		Functor<I32>				serial;
		Functor<Real>				time;
		Functor<Real>				deltaT;
		Functor< sp<RecordGroup> >	particles;
		Functor< sp<Component> >	strataI;
		Functor< sp<Component> >	drawI;
		Functor< sp<Component> >	stratumDrawI;
		Functor< sp<Component> >	recordRecorder;

				Arena(void)		{ setName("Arena"); }
virtual	void	addFunctors(void)
				{
					add(serial,			FE_USE(":SN"));
					add(time,			FE_SPEC("sim:time","Absolute time"));
					add(deltaT,			FE_SPEC("sim:timestep","Delta time"));

					add(particles,		FE_USE("op:input"));

					add(strataI,		FE_SPEC("sim:StrataI",
							"StrataI layered world"));
					add(drawI,			FE_SPEC("ren:DrawI",
							"DrawI drawing mechanism"));
					add(stratumDrawI,	FE_SPEC("ren:Stratum",
							"StratumDrawI drawing method"));
					add(recordRecorder,	FE_SPEC("op:recorder",
							"sp<RecordRecorder>"));

				}
virtual	void	initializeRecord(void)
				{
					particles.createAndSetRecordGroup();

					time()=0.0f;
					deltaT()=(1.0f/60.0f);
				}
};

} /* namespace ext */
} /* namespace fe */

#endif /* __element_Arena_h__ */
