/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <element/element.pmh>

namespace fe
{
namespace ext
{

void Gravity::handle(Record& record)
{
	if(!m_asOperator.scope().isValid())
	{
		sp<Scope> spScope=record.layout()->scope();

		m_asOperator.bind(spScope);
	}

	sp<RecordGroup>& rspRG= *(m_asOperator.input).queryAttribute(record);
	FEASSERT(rspRG.isValid());

	for(RecordGroup::iterator it=rspRG->begin();it!=rspRG->end();it++)
	{
		sp<RecordArray>& rspRA= *it;
		m_particleRAV.bind(rspRA);

		if(!m_particleRAV.recordView().mass.check(rspRA))
			continue;

		for(Particle& particleRV: m_particleRAV)
		{
			particleRV.force()[2]-=9.81f*particleRV.mass();
		}
	}
}

} /* namespace ext */
} /* namespace fe */
