/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <element/element.pmh>

namespace fe
{
namespace ext
{

void Terrain::handle(Record& record)
{
	m_arenaRV.bind(record);
	sp<StrataI> spStrataI=m_arenaRV.strataI();

	const F32 deltaT=m_arenaRV.deltaT();
	const F32 time=m_arenaRV.time();
	spStrataI->setTime(time);

	sp<RecordGroup> spRG=m_arenaRV.particles();
	FEASSERT(spRG.isValid());

	SpatialVector force;

	for(RecordGroup::iterator it=spRG->begin();it!=spRG->end();it++)
	{
		sp<RecordArray> spRA= *it;
		m_particleRAV.bind(spRA);

		if(!m_particleRAV.recordView().mass.check(spRA))
		{
			continue;
		}

		for(Particle& particleRV: m_particleRAV)
		for(I32 index=0;index<spRA->length();index++)
		{
			const F32 radius=particleRV.radius();
			const F32 mass=particleRV.mass();
			SpatialVector& location=particleRV.location();
			SpatialVector& velocity=particleRV.velocity();

			spStrataI->impact(location,velocity,mass,radius,deltaT);
		}
	}
}

} /* namespace ext */
} /* namespace fe */
