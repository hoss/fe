/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __element_Gravity_h__
#define __element_Gravity_h__
namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Contribute uniform gravity

	@ingroup element
*//***************************************************************************/
class FE_DL_EXPORT Gravity: virtual public HandlerI
{
	public:
				Gravity(void)												{}

				//* as HandlerI
virtual void	handle(Record &record);

	private:
		AsOperator					m_asOperator;
		RecordArrayView<Particle>	m_particleRAV;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __element_Gravity_h__ */
