/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <element/element.pmh>

namespace fe
{
namespace ext
{

void DrawTerrain::handle(Record &record)
{
	m_arenaRV.bind(record);

	sp<StrataI> spStrataI=m_arenaRV.strataI();
	FEASSERT(spStrataI.isValid());

	sp<StratumDrawI> spStratumDrawI=m_arenaRV.stratumDrawI();
	FEASSERT(spStratumDrawI.isValid());

	U32 numStratum=spStrataI->numStratum();
	for(U32 m=0;m<numStratum;m++)
	{
		spStratumDrawI->draw(spStrataI->stratum(m));
	}
}

} /* namespace ext */
} /* namespace fe */