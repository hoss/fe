/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <element/element.pmh>

namespace fe
{
namespace ext
{

void Provot::handle(Record& record)
{
	if(!m_particleRV.scope().isValid())
	{
		m_particleRV.bind(record.layout()->scope());
		m_rodRAV.bind(record.layout()->scope());
	}

	m_arenaRV.bind(record);
	sp<RecordGroup> spRG=m_arenaRV.particles();
	FEASSERT(spRG.isValid());

	// HACK no correction when paused
	if(m_arenaRV.deltaT()<1e-3f)
	{
		return;
	}

	// TODO param somewhere
	const U32 passes=4;

	for(U32 pass=0;pass<passes;pass++)
	{
		for(RecordGroup::iterator it=spRG->begin();it!=spRG->end();it++)
		{
			sp<RecordArray> spRA= *it;
			m_rodRAV.bind(spRA);

			if(!m_rodRAV.recordView().length.check(spRA))
			{
				continue;
			}

			for(Rod& rodRV: m_rodRAV)
			{
				const F32 length=rodRV.length();
				const Record& particle1=rodRV.particle1();
				const Record& particle2=rodRV.particle2();

				SpatialVector& location1=m_particleRV.location(particle1);
				SpatialVector& location2=m_particleRV.location(particle2);
				const SpatialVector difference=location2-location1;
				const F32 distance=magnitude(difference);
				if(distance>0.1f)
				{
					const SpatialVector change=
							(0.5f*(1.0f-length/distance))*difference;
					location1+=change;
					location2-=change;

					SpatialVector& velocity1=m_particleRV.velocity(particle1);
					SpatialVector& velocity2=m_particleRV.velocity(particle2);
					const SpatialVector diffVel=velocity2-velocity1;
					const F32 dotVel=dot(diffVel,difference)/distance;
					const SpatialVector changeVel=
							difference*(0.5f*dotVel/distance);
					velocity1+=changeVel;
					velocity2-=changeVel;
				}
				else
				{
					feLog("Provot::handle Warning short rod %.6G"
							" \"%s\" vs. \"%s\"\n"
							"\t\"%s\" vs. \"%s\"\n",distance,
							particle1.layout()->name().c_str(),
							particle2.layout()->name().c_str(),
							print(location1).c_str(),
							print(location2).c_str());

					feX(e_unsolvable,"Provot::handle","negligible length");
				}
			}
		}
	}
}

} /* namespace ext */
} /* namespace fe */
