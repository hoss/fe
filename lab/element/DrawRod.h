/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __element_DrawRod_h__
#define __element_DrawRod_h__
namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Draw each connecting cylinder

	@ingroup element
*//***************************************************************************/
class FE_DL_EXPORT DrawRod: public HandlerI
{
	public:
				DrawRod(void)												{}

				//* as HandlerI
virtual void	handle(Record &record);

	private:
		Arena					m_arenaRV;
		Particle				m_particleRV;
		RecordArrayView<Rod>	m_rodRAV;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __element_DrawRod_h__ */
