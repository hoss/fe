/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __particle_Drag_h__
#define __particle_Drag_h__
namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Contribute viscous drag

	@ingroup element
*//***************************************************************************/
class FE_DL_EXPORT Drag: public RecordOperation
{
	public:
				Drag(sp<Scope> spScope): RecordOperation(spScope)			{}
		void	bind(const Record &record)									{}
		void	operator()(const Record &record);

	private:
		Particle	m_particleRV;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __particle_Drag_h__ */
