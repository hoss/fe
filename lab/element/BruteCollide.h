/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __element_BruteCollide_h__
#define __element_BruteCollide_h__
namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief n-squared collision detection and resolution

	@ingroup element
*//***************************************************************************/
class FE_DL_EXPORT BruteCollide: virtual public HandlerI
{
	public:
				BruteCollide(void)											{}

				//* as HandlerI
virtual void	handle(Record &record);

	private:
		Accessor< sp<RecordGroup> >	m_aRecordGroup;

		Arena						m_arenaRV;
		RecordArrayView<Explosion>	m_explosionRAV;
		RecordArrayView<Particle>	m_particleRAV;
		RecordArrayView<Particle>	m_particleRAV2;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __element_BruteCollide_h__ */
