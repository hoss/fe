/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <element/element.pmh>

namespace fe
{
namespace ext
{

void DrawRod::handle(Record &record)
{
	if(!m_particleRV.scope().isValid())
	{
		m_particleRV.bind(record.layout()->scope());
	}

	const Color blue(0.2f,0.2f,1.0f,1.0f);
	const Color green(0.0f,0.5f,0.0f,0.9f);
	const Color yellow(1.0f,1.0f,0.0f,1.0f);

	m_arenaRV.bind(record);

	sp<DrawI> spDrawI=m_arenaRV.drawI();
	FEASSERT(spDrawI.isValid());

	sp<RecordGroup> spRG=m_arenaRV.particles();
	FEASSERT(spRG.isValid());

	for(RecordGroup::iterator it=spRG->begin();it!=spRG->end();it++)
	{
		sp<RecordArray> spRA= *it;
		m_rodRAV.bind(spRA);

		if(!m_rodRAV.recordView().length.check(spRA))
		{
			continue;
		}

		for(Rod& rodRV: m_rodRAV)
		{
			const F32 length=rodRV.length();
			const Record& particle1=rodRV.particle1();
			const Record& particle2=rodRV.particle2();

			SpatialVector vertex[2];
			vertex[0]=m_particleRV.location(particle1);
			vertex[1]=m_particleRV.location(particle2);
		//	spDrawI->drawLines(vertex,NULL,2,true,false,&blue);

			const F32 radius1=m_particleRV.radius(particle1);
			const F32 radius2=m_particleRV.radius(particle2);
			F32 separation=length-radius1-radius2;
			if(separation>0.0f)
			{
				SpatialVector direction=vertex[1]-vertex[0];
				F32 length=magnitude(direction);
				if(length>0.1f)
				{
					normalize(direction);
				}
				else
				{
					feLog("DrawRod::handle Warning short rod %.6G"
						" \"%s\" vs. \"%s\"\n"
						"\t\"%s\" radius %.2f vs. \"%s\" radius %.2f\n",
						length,
						particle1.layout()->name().c_str(),
						particle2.layout()->name().c_str(),
						print(vertex[0]).c_str(),radius1,
						print(vertex[1]).c_str(),radius2);

					feX(e_unsolvable,"DrawRod::handle","negligible length");
				}

				const F32 rodRadius=0.1f;
				spDrawI->drawCylinder(vertex[0]+direction*radius1,
						vertex[1]-direction*radius2,
						rodRadius,rodRadius,green,0);
			}

#if FEX_BEH_DRAW_TEXT
//			spDrawI->drawAlignedText(0.5f*(vertex[0]+vertex[1]),"rod",yellow);
#endif

		}
	}
}

} /* namespace ext */
} /* namespace fe */
