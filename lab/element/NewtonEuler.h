/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __element_NewtonEuler_h__
#define __element_NewtonEuler_h__
namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Convert force to motion

	@ingroup element
*//***************************************************************************/
class FE_DL_EXPORT NewtonEuler: virtual public HandlerI
{
	public:
				NewtonEuler(void)											{}

				//* as HandlerI
virtual void	handle(Record &record);

	private:
		Arena						m_arenaRV;
		RecordArrayView<Particle>	m_particleRAV;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __element_NewtonEuler_h__ */
