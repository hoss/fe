/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __element_Modifier_h__
#define __element_Modifier_h__
namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Stream particles through an operation

	NOTE This is a historical experiment and is not currently recommended
	for future work.
*//***************************************************************************/
template<typename GROUP,int SUBGROUP,typename RECORDOP>
class FE_DL_EXPORT Modifier: virtual public HandlerI
{
	public:
				//* As HandlerI
virtual	void	handle(Record& record)
				{
					m_groupRV.bind(record);
					sp<RecordGroup>& rspRecordGroup=m_groupRV.SUBGROUP();

					FEASSERT(rspRecordGroup.isValid());
					RECORDOP op(record.layout()->scope());
					op.bind(record);
					rspRecordGroup->all(op);
				}
	private:

		GROUP	m_groupRV;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __element_Modifier_h__ */
