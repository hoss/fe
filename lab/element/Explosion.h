/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __element_Explosion_h__
#define __element_Explosion_h__
namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Explosion RecordView

	@ingroup element
*//***************************************************************************/
class FE_DL_EXPORT Explosion: public Sphere
{
	public:
		Functor<F32>	radialForce;
		Functor<I32>	serial;

				Explosion(void)			{ setName("Explosion"); }
virtual	void	addFunctors(void)
				{
					Sphere::addFunctors();

					add(radialForce,	FE_SPEC("sim:radialForce",
							"Magnitude of explosive effect"));
					add(serial,			FE_USE(":SN"));
				}
virtual	void	initializeRecord(void)
				{
					Sphere::initializeRecord();

					radialForce()=1.0f;
				}
};

} /* namespace ext */
} /* namespace fe */

#endif /* __element_Explosion_h__ */
