/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __element_DrawTerrain_h__
#define __element_DrawTerrain_h__
namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Draw the surface layers

	@ingroup element
*//***************************************************************************/
class FE_DL_EXPORT DrawTerrain: virtual public HandlerI
{
	public:
				DrawTerrain(void)											{}

				//* as HandlerI
virtual	void	handle(Record &record);

	private:

		Arena	m_arenaRV;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __element_DrawTerrain_h__ */
