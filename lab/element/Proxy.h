/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __element_Proxy_h__
#define __element_Proxy_h__
namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Proxy RecordView

	@ingroup element
*//***************************************************************************/
class FE_DL_EXPORT Proxy: public Sphere
{
	public:
		Functor<I32>			inceptionMS;

				Proxy(void)				{ setName("Proxy"); }
virtual	void	addFunctors(void)
				{
					Sphere::addFunctors();

					add(inceptionMS,	FE_SPEC("ai:inceptionMS",
							"When we last saw a remembrance"));
				}
virtual	void	initializeRecord(void)
				{
					Sphere::initializeRecord();

					inceptionMS()=0;
				}
};

} /* namespace ext */
} /* namespace fe */

#endif /* __element_Proxy_h__ */
