/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __architecture_WindowPipeline_h__
#define __architecture_WindowPipeline_h__

#include <datatool/datatool.h>
#include <window/window.h>
#include <lua/lua.h>
namespace fe
{
namespace ext
{

/**	Window Pipeline
	*/
class FE_DL_EXPORT WindowPipeline
	:	virtual public PipelineI,
		virtual public Config,
		public Initialize<WindowPipeline>
{
	public:
				WindowPipeline(void);
virtual			~WindowPipeline(void);
		void	initialize(void);

		// AS PipelineI
virtual	void				attach(	sp<ApplicationI> a_application,
									sp<SignalerI> a_signaler,
									sp<SequencerI> a_sequencer,
									sp<Layout> l_hb,
									sp<RecordGroup> rg_input);
virtual	void				loadConfig(sp<RecordGroup> rg_input);
virtual	void				loadWindows(void);
virtual sp<WindowI>			createWindow(const String &a_title,
									Record &r_win_data,
									sp<RecordGroup> rg_appwin);

	private:
		sp<SignalerI>				m_spSignaler;
		sp<EventContextI>			m_spEventContext;
		sp<Layout>					l_draw_hb;
		sp<Layout>					l_winev_hb;
		sp<Layout>					l_win_data;
		sp<Layout>					l_event;
		std::list< sp<WindowI> >	m_windows;
		String						m_directory;
		sp<RecordGroup>				m_spWindatas;
		typedef std::pair< sp<ApplicationWindowI>, sp<RecordGroup> > t_appwin;
		std::vector< t_appwin >		m_appWindows;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __architecture_WindowPipeline_h__ */

