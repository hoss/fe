/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <architecture/architecture.pmh>

#include <viewer/viewer.h>

namespace fe
{
namespace ext
{

AppDebugWindow::AppDebugWindow(void)
{
}

AppDebugWindow::~AppDebugWindow(void)
{
}

void AppDebugWindow::initialize(void)
{
	cfg<String>("title") = "untitled";
}

void AppDebugWindow::populateDataLayout(sp<Layout> l_win_data)
{
	asOrtho.populate(l_win_data);
	asSelection.populate(l_win_data);
}

void AppDebugWindow::makeWindow(sp<WindowI> a_window, Record r_win_data, sp<Layout> l_event, sp<Layout> l_draw_hb, sp<RecordGroup> rg_info)
{
	registry()->manage("fexDatauiDL");

	a_window->setTitle(cfg<String>("title"));

	sp<SignalerI> spSignaler = feCast(SignalerI, a_window);

	// UI needs orthographic projection
	sp<Component> spOrtho = create("ViewerI.OrthoViewer");
	spOrtho->adjoin(a_window);
	spSignaler->insert(spOrtho, l_draw_hb);
	feCast(ViewerI, spOrtho)->bind(a_window);
	asOrtho.zoom(r_win_data) = 1.0;

	// Scope Debug
	sp<Component> spScopeDebug = create("HandlerI.ScopeDebug");
	spScopeDebug->adjoin(a_window);
	spSignaler->insert(spScopeDebug, l_draw_hb);
	spSignaler->insert(spScopeDebug, l_event);

	// Catalog Debug
	sp<Component> spCatalogDebug = create("HandlerI.CatalogDebug");
	spCatalogDebug->adjoin(a_window);
	spSignaler->insert(spCatalogDebug, l_draw_hb);
	spSignaler->insert(spCatalogDebug, l_event);
	sp<Config> spConfig = feCast(Config, spCatalogDebug);
	spConfig->cfg< sp<Layout> >("draw") = l_draw_hb;
	spConfig->cfg< Vector2 >("offset") = Vector2(500,10);
	spConfig->cfg< Real >("margin") = 20.0;
	spConfig->cfg< String >("label") = "CATALOG";

	// Catalog Debug
	sp<Component> spMaskDebug = create("HandlerI.MaskMapDebug");
	spMaskDebug->adjoin(a_window);
	spSignaler->insert(spMaskDebug, l_draw_hb);
	spSignaler->insert(spMaskDebug, l_event);
	spConfig = feCast(Config, spMaskDebug);
	spConfig->cfg< sp<Layout> >("draw") = l_draw_hb;
	spConfig->cfg< Vector2 >("offset") = Vector2(800,10);
	spConfig->cfg< Real >("margin") = 20.0;
	spConfig->cfg< String >("label") = "MASK MAP";

	// TODO: remove
	feCast(Config, spScopeDebug)->cfg< sp<Component> >("scope") = l_draw_hb->scope();



	feCast(Config, spScopeDebug)->cfg< sp<Layout> >("draw") = l_draw_hb;

	// Channel Controller
	sp<Component> spChannelController = create("HandlerI.ChannelController");
	spChannelController->adjoin(a_window);
	spSignaler->insert(spChannelController, l_event);

	// Window Controller
	sp<Component> spController = create("HandlerI.WindowController");
	spController->adjoin(a_window);
	spSignaler->insert(spController, l_event);
}


} /* namespace ext */
} /* namespace fe */

