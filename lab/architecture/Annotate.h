/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __architecture_Annotate_h__
#define __architecture_Annotate_h__
namespace fe
{
namespace ext
{

/** Set a record attribute to a given record

	@copydoc Annotate_info
	*/
class Annotate : public Initialize<Annotate>,
	virtual public AnnotateI
{
	public:
				Annotate(void);
virtual			~Annotate(void);
		void	initialize(void);

				// AS AnnotateI
virtual	void	attach(const String &attrname, Record &r_data);

				// AS HandlerI
virtual	void	handle(Record& r_sig);

	private:
		class Annotation
		{
			public:
				Record				m_record;
				String				m_attrname;
		};
		std::list<Annotation>		m_annotations;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __architecture_Annotate_h__ */
