/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __ext_architecture_h__
#define __ext_architecture_h__

#include "signal/signal.h"

#include "architecture/ForkI.h"
#include "architecture/AnnotateI.h"
#include "architecture/AssignI.h"
#include "architecture/PaletteI.h"
#include "architecture/ApplicationI.h"
#include "architecture/ApplicationWindowI.h"
#include "architecture/PipelineI.h"

#endif /* __ext_architecture_h__ */

