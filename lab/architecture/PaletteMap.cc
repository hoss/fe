/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <architecture/architecture.pmh>

namespace fe
{
namespace ext
{

PaletteMap::PaletteMap(void)
{
}

PaletteMap::~PaletteMap(void)
{
}

void PaletteMap::set(const String &a_name, sp<Component> spCmp)
{
	m_map[a_name] = spCmp;

#if FE_COUNTED_TRACK
	if(spCmp.isValid())
	{
		spCmp->trackReference(this,"PaletteMap");
	}
#endif
}

sp<Component> PaletteMap::get(const String &a_name)
{
	if(!m_map[a_name].isValid())
	{
//		feLog("PaletteMap::get invalid name \"%s\"\n",a_name.c_str());
	}

	return m_map[a_name];
}

void PaletteMap::remove(const String &a_name)
{
#if FE_COUNTED_TRACK
	sp<Component> spCmp=m_map[a_name];
	if(spCmp.isValid())
	{
		spCmp->untrackReference(this);
	}
#endif
	m_map.erase(a_name);
}

} /* namespace ext */
} /* namespace fe */

