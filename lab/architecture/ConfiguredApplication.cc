/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <architecture/architecture.pmh>

//#include "surface/surface.h"
#include <boost/filesystem/operations.hpp>

#include <boost/filesystem/operations.hpp>
#include <boost/filesystem/path.hpp>

using namespace boost::filesystem;

namespace fe
{
namespace ext
{

ConfiguredApplication::ConfiguredApplication(void)
	: m_ticker("ConfiguredApplication")
{
}

ConfiguredApplication::~ConfiguredApplication(void)
{
}

void ConfiguredApplication::initialize(void)
{
	registry()->master()->catalog()->catalog<String>(
			FE_CAT_SPEC("root", "root pathname of application")) = ".";
	registry()->master()->catalog()->catalog<String>(
			FE_CAT_SPEC("plugin", "plugin directory")) = "";
	registry()->master()->catalog()->catalog<String>(
			FE_CAT_SPEC("debug", "debug mode (0 for none)")) = "0";
	registry()->master()->catalog()->catalog<String>(
			FE_CAT_SPEC("architecture", "architecture rg file")) = "";
	registry()->master()->catalog()->catalog<String>(
			FE_CAT_SPEC("media", "media pathname")) = "";
	registry()->master()->catalog()->catalog<String>(
			FE_CAT_SPEC("pause", "0 or false to start paused")) = "0";
	registry()->master()->catalog()->catalog<String>(
			FE_CAT_SPEC("host", "server hostname")) = "127.0.0.1";
	registry()->master()->catalog()->catalog<String>(
			FE_CAT_SPEC("port", "server port")) = "7654";
	registry()->master()->catalog()->catalog<String>("ground") = "";
}

void ConfiguredApplication::setup(int a_argc, char **a_argv, char** a_env)
{
	// =====================================================================
	// setup configuration variables
	Regex re_assign("(.*)=(.*)");
	for(int i = 1; i < a_argc; i++)
	{
		char* arg=a_argv[i];
		if(re_assign.search(arg))
		{
			registry()->master()->catalog()->catalog<String>(
					re_assign.result(1)) = re_assign.result(2);
		}
		else
		{
			registry()->master()->catalog()->catalog<String>(arg) = "1";
			m_args.push_back(a_argv[i]);
		}
	}

	registry()->master()->catalog()->catalog<int>("answer") = 42;

	// =====================================================================
	// setup paths
	path root_path(cfg<String>("root").c_str());
	root_path = complete(root_path);
	path exe_path(a_argv[0]);
	path default_plugin_path(exe_path.branch_path() / "plugin");
	if(cfg<String>("plugin") == "")
	{
		registry()->master()->catalog()->catalog<String>("plugin") = default_plugin_path.string().c_str();
	}
	path plugin_path(cfg<String>("plugin").c_str());
	registry()->master()->catalog()->catalog<String>("plugin") = plugin_path.string().c_str();
	plugin_path = complete(plugin_path);

	m_rootPath = root_path.string().c_str();
	m_pluginPath = plugin_path.string().c_str();

	System::setCurrentWorkingDirectory(m_rootPath);

	if(cfg<String>("media") != "")
	{
		fe_fprintf(stderr, "Setting path:media to %s\n",
				cfg<String>("media").c_str());
		registry()->master()->catalog()->catalog<String>("path:media") =
				cfg<String>("media");
	}

	// =====================================================================
	// load plugin directory
	manageDirectory(m_pluginPath);


	// =====================================================================
	// setup main signaler, sequencer, scope, and heartbeat

	// application level signaler
	m_spAppSignaler = registry()->create("SignalerI");
	if(!m_spAppSignaler.isValid())
	{
		feX("ConfiguredApplication::setup",
			"possible invalid plugin path \'%s\'", m_pluginPath.c_str());
	}

	// application level scope
	//m_scope = registry()->create("Scope");
	m_scope =
		registry()->master()->catalog()->catalogComponent(
			"Scope", "ConfiguredApplicationScope");
	m_scope->setName("ConfiguredApplicationScope");

	// application data record layout
	sp<Layout> l_app_data = m_scope->declare("l_app_data");

	// create sequencer
	sp<SequencerI>	spAppSequencer(registry()->create(
			"SequencerI.PushSequencer"));

	// heartbeat signal to push sequencer
	sp<Layout> l_seq_hb = m_scope->declare("l_seq_hb");
	spAppSequencer->adjoin(m_spAppSignaler);
	m_spAppSignaler->insert(spAppSequencer, l_seq_hb);



	// =====================================================================
	// =====================================================================
	// pipelines plugin here
	// =====================================================================

	if(cfg<String>("architecture") == "")
	{
		feX("ConfiguredApplication::setup",
			"architecture file not specified");
	}
	else
	{
#if 1
		sp<Layout> l_pipeline = m_scope->declare("pipeline");
		AsNamed asNamed;
		asNamed.bind(m_scope);
		asNamed.populate(l_pipeline);
		AsGroup asGroup;
		asGroup.bind(m_scope);
		asGroup.populate(l_pipeline);
#endif

		sp<data::FileStreamI> spFileStream(
			registry()->create("FileStreamI.Ascii"));
		spFileStream->bind(m_scope);
		sp<RecordGroup> rg_architecture =
			spFileStream->input(cfg<String>("architecture"));

#if 0
		sp<Layout> l_pipeline = m_scope->lookupLayout("pipeline");
		AsNamed asNamed;
		asNamed.bind(m_scope);
		AsGroup asGroup;
		asGroup.bind(m_scope);
#endif

		for(RecordGroup::iterator i_rg = rg_architecture->begin();
			i_rg != rg_architecture->end(); i_rg++)
		{
			sp<RecordArray> spRA(*i_rg);
			if(l_pipeline == spRA->layout()
				&& asNamed.check(spRA) && asGroup.check(spRA))
			{
				for(int i = 0; i < spRA->length(); i++)
				{
					sp<PipelineI> spPipeline(
						registry()->create(asNamed.name(spRA, i)));
					sp<ConfigI>(spPipeline)->configParent(sp<ConfigI>(this));
					spPipeline->attach(sp<ApplicationI>(this),
						m_spAppSignaler, spAppSequencer, l_seq_hb,
						asGroup.group(spRA, i));
				}
			}
		}
	}


	// =====================================================================
	// Record Creation

	// application control
	// 0 == quit, 1 == run, 2 == pause
	if(	cfg<String>("pause") == "0" || cfg<String>("pause") == "false")
	{
		registry()->master()->catalog()->catalog<int>("run:level") = 1;
	}
	else
	{
		registry()->master()->catalog()->catalog<int>("run:level") = 2;
	}

	// sequencer heartbeat
	m_seq_hb = m_scope->createRecord(l_seq_hb);

	m_ticker.log();
}


bool ConfiguredApplication::step(int &a_returnCode)
{
	//feLog("--- step ---\n");
	if(registry()->master()->catalog()->catalog<int>("run:level") == 0)
	{
		return false;
	}
	m_spAppSignaler->signal(m_seq_hb);
	if(cfg<String>("debug").match("peek"))
	{
		Peeker peeker;
		m_spAppSignaler->peek(peeker);
		feLog(peeker.str().c_str());
	}
	if(cfg<String>("debug").match("time"))
	{
		m_ticker.log("step");
	}
	return true;
}

bool ConfiguredApplication::loop(int &a_returnCode)
{
	String countStr;
	Instance count_instance;
	Instance delay_instance;
	unsigned long delay = 0;
	if(configLookup("delay", delay_instance))
	{
		delay = atoi(delay_instance.cast<String>().c_str());
	}
	if(configLookup("count", count_instance))
	{
		countStr = count_instance.cast<String>();
		unsigned long count;
		count = atoi(countStr.c_str());
		while(count != 0 && step(a_returnCode))
		{
			count--;
			if(delay)
			{
				milliSleep(delay);
			}
		}
	}
	else
	{
		while( step(a_returnCode) )
		{
			milliSleep(delay);
		}
	}

	if(cfg<String>("debug").match("dump"))
	{
		Peeker peeker;
		peeker(*m_scope);
		peeker(*m_spAppSignaler);
		feLog(peeker.output().c_str());
	}

	return true;
}

namespace bfs = boost::filesystem;

void ConfiguredApplication::manageDirectory(const String &dirname)
{
//	SAFEGUARD;

	bfs::path dirpath(dirname.c_str());
	if(!bfs::exists(dirpath))
	{
		return;
	}

	Regex re("(lib)?(.*)\\.((d|D)(l|L)(l|L)|so)");

	std::vector<std::string> libs;
	std::vector<std::string> failed;

	bfs::directory_iterator i_end;
	for(bfs::directory_iterator i_dir(dirpath); i_dir != i_end; ++i_dir )
	{
		if(!bfs::is_directory(*i_dir))
		{
#if BOOST_VERSION < 103400
			std::string fn = i_dir->native_file_string().c_str();
#else
//			std::string fn = i_dir->path().native_file_string().c_str();
			std::string fn = i_dir->path().string().c_str();
#endif
			if(re.match(fn.c_str()))
			{
				failed.push_back(fn);
			}
		}
	}

	while(libs.size() != failed.size())
	{
		libs.clear();
		libs = failed;
		failed.clear();
		for(std::vector<std::string>::iterator i_lib = libs.begin();
			i_lib != libs.end(); i_lib++)
		{
			if(!successful(registry()->manage(i_lib->c_str(), false, true)))
			{
				failed.push_back(*i_lib);
			}
		}
	}

	if(failed.size() > 0)
	{
		feLog("WARNING: failed loading all libraries in directory \'%s\'\n",
				dirname.c_str());
	}
	for(std::vector<std::string>::iterator i_fail = failed.begin();
			i_fail != failed.end(); i_fail++)
	{
		feLog("         %s\n", i_fail->c_str());
	}

}

} /* namespace ext */
} /* namespace fe */

