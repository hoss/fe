/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __architecture_ConfiguredApplication_h__
#define __architecture_ConfiguredApplication_h__

#include <datatool/datatool.h>
namespace fe
{
namespace ext
{

/** Mix of ApplicationI and ConfigI
	*/
class FE_DL_EXPORT ConfiguredApplication
	:	virtual public ApplicationI,
		virtual	public Config,
		public Initialize<ConfiguredApplication>
{
	public:
				ConfiguredApplication(void);
virtual			~ConfiguredApplication(void);
		void	initialize(void);

		// AS ApplicationI
virtual	void	setup(int a_argc, char **a_argv, char** a_env);
virtual	bool	step(int &a_returnCode);
virtual	bool	loop(int &a_returnCode);

		void	manageDirectory(const String &dirname);

	protected:
		std::vector<String>		m_args;
		String					m_rootPath;
		String					m_pluginPath;
		Record					m_seq_hb;
		sp<SignalerI>			m_spAppSignaler;
		SystemTicker			m_ticker;
		sp<Scope>				m_scope;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __architecture_ConfiguredApplication_h__ */

