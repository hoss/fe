/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __architecture_ApplicationI_h__
#define __architecture_ApplicationI_h__
namespace fe
{
namespace ext
{

/**	application framework interface
	*/
class FE_DL_EXPORT ApplicationI:
	virtual public Component,
	public CastableAs<ApplicationI>
{
	public:
virtual	void	setup(int a_argc, char **a_argv, char** a_env)				= 0;
virtual	bool	step(int &a_returnCode)										= 0;
virtual	bool	loop(int &a_returnCode)										= 0;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __architecture_ApplicationI_h__ */

