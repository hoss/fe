/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <architecture/architecture.pmh>

#include <draw/draw.h>

namespace fe
{
namespace ext
{

class SimCountControl : virtual public HandlerI
{
	public:
		SimCountControl(Record &a_r_sim_hb, int &run_level)
			: m_run_level(run_level)
		{
			r_sim_hb = a_r_sim_hb;
		}
virtual	~SimCountControl(void) {}

virtual void	handle(Record &r_sig)
		{
			m_asSeqSig.bind(r_sim_hb.layout()->scope());
			if(m_run_level == 2)
			{
				m_asSeqSig.count(r_sim_hb) = -1;
			}
			else
			{
				m_asSeqSig.count(r_sim_hb) = 0;
			}
		}

	private:
		AsSequenceSignal	m_asSeqSig;
		Record				r_sim_hb;
		int					&m_run_level;
};

SimulationPipeline::SimulationPipeline(void)
{
}

SimulationPipeline::~SimulationPipeline(void)
{
}

void SimulationPipeline::initialize(void)
{
}

void SimulationPipeline::attach(sp<ApplicationI> a_application,
	sp<SignalerI> a_signaler, sp<SequencerI> a_sequencer,
	sp<Layout> l_hb, sp<RecordGroup> rg_input)
{
	m_spSignaler = a_signaler;

	sp<Scope> spSimScope(registry()->create("Scope"));
	spSimScope->setName("WindowPipelineScope");

	// simulation heartbeat
	l_sim_hb = spSimScope->declare("l_sim_hb");
#ifdef FE_LOCK_SUPPRESSION
	l_sim_hb->suppressLock(true);
#endif


	AsSequenceSignal asSequenceSignal;
	asSequenceSignal.bind(spSimScope);
#ifdef FE_LOCK_SUPPRESSION
	l_sim_hb->suppressLock(false);
#endif
	Record r_sim_hb = a_sequencer->add(l_sim_hb,
		a_sequencer->getCurrentTime(), 1);
	asSequenceSignal.count(r_sim_hb) = 0;
	asSequenceSignal.periodic(r_sim_hb) = 0;

	sp<HandlerI> spH(new SimCountControl(r_sim_hb,
			registry()->master()->catalog()->catalog<int>("run:level")));
	spH->adjoin(a_signaler);
	a_signaler->insert(spH, l_hb);
}

} /* namespace ext */
} /* namespace fe */
