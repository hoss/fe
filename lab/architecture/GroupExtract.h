/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __architecture_GroupExtract_h__
#define __architecture_GroupExtract_h__

#include "signal/signal.h"
#include <datatool/datatool.h>
namespace fe
{
namespace ext
{

class GroupExtract :
	virtual public HandlerI,
	virtual public Config,
	public Initialize<GroupExtract>
{
	public:
				GroupExtract(void);
virtual			~GroupExtract(void);

virtual	void	initialize(void);

		void	addRequiredAttribute(const String &a_attr);

				// AS HandlerI
virtual	void	handle(Record& r_sig);

	private:
		std::vector<String>		m_attrNames;

};

} /* namespace ext */
} /* namespace fe */

#endif /* __architecture_GroupExtract_h__ */

