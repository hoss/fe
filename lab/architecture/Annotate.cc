/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <architecture/architecture.pmh>

namespace fe
{
namespace ext
{

Annotate::Annotate(void)
{
}

Annotate::~Annotate(void)
{
}

void Annotate::initialize(void)
{
}

void Annotate::attach(const String &attrname, Record &r_data)
{
	Annotation annotation;
	annotation.m_record = r_data;
	annotation.m_attrname = attrname;

	m_annotations.push_back(annotation);
}

void Annotate::handle(Record& r_sig)
{
	for(std::list<Annotation>::iterator i_anno = m_annotations.begin();
		i_anno != m_annotations.end(); i_anno++)
	{
		PathAccessor<Record> pathaccessor(i_anno->m_attrname);
		Record *r_set = pathaccessor(r_sig);
		if(r_set)
		{
			*r_set = i_anno->m_record;
		}
#if 0
		if(i_anno->m_record.isValid() &&
			checkAttribute(i_anno->m_attrname, r_sig))
		{
			accessAttribute<Record>(i_anno->m_attrname, r_sig) =
				i_anno->m_record;
		}
#endif
	}
}

} /* namespace ext */
} /* namespace fe */

