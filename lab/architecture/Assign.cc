/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <architecture/architecture.pmh>

namespace fe
{
namespace ext
{

Assign::Assign(void)
{
}

Assign::~Assign(void)
{
}

void Assign::initialize(void)
{
}

void Assign::set(const String &a_lhs, const String &a_rhs)
{
	m_assignments.push_back(Assignment());
	Assignment &a = m_assignments.back();
	a.m_lhs = a_lhs;
	a.m_rhs = a_rhs;
}

void Assign::handle(Record& r_sig)
{
	for(std::list<Assignment>::iterator i_assign = m_assignments.begin();
		i_assign != m_assignments.end(); i_assign++)
	{
		sp<Attribute> attr_lhs;
		void *p_lhs = i_assign->m_lhs.access(r_sig, attr_lhs);

		sp<Attribute> attr_rhs;
		void *p_rhs = i_assign->m_rhs.access(r_sig, attr_rhs);

		if(!p_lhs)
		{
			String s;
			s = i_assign->m_lhs.path();
			feX("Assign::handle", "%s not found", s.c_str());
		}

		if(!p_rhs)
		{
			String s;
			s = i_assign->m_rhs.path();
			feX("Assign::handle", "%s not found", s.c_str());
		}

		if(p_lhs && p_rhs)
		{
			if(attr_lhs->type()->typeinfo() != attr_rhs->type()->typeinfo())
			{
				String s[2];
				s[0] = i_assign->m_lhs.path();
				s[1] = i_assign->m_rhs.path();
				feX("Assign::handle",
					"type mismatch %s %s", s[0].c_str(), s[1].c_str());
			}
			attr_lhs->type()->assign(p_lhs, p_rhs);
		}
	}
}


} /* namespace ext */
} /* namespace fe */

