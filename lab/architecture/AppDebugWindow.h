/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __architecture_AppDebugWindow_h__
#define __architecture_AppDebugWindow_h__
namespace fe
{
namespace ext
{

/** Data System debug window.
	*/
class AppDebugWindow :
	virtual public ApplicationWindowI,
	virtual public Config,
	public Initialize<AppDebugWindow>
{
	public:
				AppDebugWindow(void);
virtual			~AppDebugWindow(void);

		void	initialize(void);

				// AS ApplicationWindowI
virtual	void	populateDataLayout(sp<Layout> l_win_data);
virtual	void	makeWindow(sp<WindowI> a_window,
					Record r_win_data,
					sp<Layout> l_windowEvent,
					sp<Layout> l_draw_hb,
					sp<RecordGroup> rg_info);

	private:
		AsOrtho			asOrtho;
		AsSelection		asSelection;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __architecture_AppDebugWindow_h__ */

