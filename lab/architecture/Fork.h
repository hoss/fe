/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __architecture_Fork_h__
#define __architecture_Fork_h__

namespace fe
{
namespace ext
{

/**	Fork a signal
	*/
class Fork : public Initialize<Fork>,
	virtual public ForkI,
	virtual public HandlerI
{
	public:
				Fork(void);
virtual			~Fork(void);
		void	initialize(void);

				// AS ForkI
virtual	void	use(Record &r_fork);
virtual	void	spawn(sp<Layout> &l_fork);

				// AS HandlerI
virtual	void	handle(Record& r_sig);
virtual	void	handleBind(sp<SignalerI> spSignalerI,
						sp<Layout> l_sig);

	private:
		sp<Layout>		m_spSpawn;
		Record			m_fork;
		sp<SignalerI>	m_spSignaler;
		sp<Scope>		m_spScope;

};

} /* namespace ext */
} /* namespace fe */

#endif /* __architecture_Fork_h__ */

