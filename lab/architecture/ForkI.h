/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __architecture__ForkI_h__
#define __architecture__ForkI_h__

namespace fe
{
namespace ext
{

/** The purpose of ForkI is to fire off a signal when itself is signaled.
	Therefore, this interface is intended to be used in conjunction with
	HandlerI. */
class FE_DL_EXPORT ForkI : virtual public Component
{
	public:
		/** Signal using the provided record @em r_sig when signaling.  If
			no record has been specified with use(), then the record should be
			automatically created using Scope::createRecord(). */
virtual	void	use(Record &r_sig)											= 0;
virtual	void	spawn(sp<Layout> &l_sig)									= 0;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __architecture__ForkI_h__ */
