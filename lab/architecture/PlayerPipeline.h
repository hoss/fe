/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __world_PlayerPipeline_h__
#define __world_PlayerPipeline_h__

#include <datatool/datatool.h>
#include <window/window.h>
#include <lua/lua.h>
namespace fe
{
namespace ext
{

/** 

	*/
class FE_DL_EXPORT PlayerPipeline
	:	virtual public PipelineI,
		virtual public Config,
		public Initialize<PlayerPipeline>
{
	public:
					PlayerPipeline(void);
virtual				~PlayerPipeline(void);
		void		initialize(void);

		// AS PipelineI
virtual	void		attach(	sp<ApplicationI> a_application,
							sp<SignalerI> a_signaler,
							sp<SequencerI> a_sequencer,
							sp<Layout> l_hb,
							sp<RecordGroup> rg_input);

	private:
		sp<SignalerI>				m_spSignaler;
		sp<LayoutDefault>			l_sim_hb;
		sp<LayoutDefault>			l_init;
		sp<LayoutDefault>			l_data;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __world_PlayerPipeline_h__ */

