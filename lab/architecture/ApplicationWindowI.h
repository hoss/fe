/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __architecture_ApplicationWindowI_h__
#define __architecture_ApplicationWindowI_h__

#include <window/window.h>
namespace fe
{
namespace ext
{

class FE_DL_EXPORT ApplicationWindowI : virtual public Component
{
	public:
				/** Add attributes to the windata Layout if necessary. */
virtual	void	populateDataLayout(sp<Layout> l_win_data)					= 0;
				/** Setup the content of the window */
virtual	void	makeWindow(sp<WindowI> a_window,
					Record r_win_data, sp<Layout> l_windowEvent,
					sp<Layout> l_draw_hb, sp<RecordGroup> rg_info)			= 0;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __architecture_ApplicationWindowI_h__ */

