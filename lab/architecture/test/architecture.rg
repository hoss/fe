INFO 5
ATTRIBUTE name string
ATTRIBUTE group group
ATTRIBUTE col:name string
ATTRIBUTE col:rgba vector4

LAYOUT "pipeline"
	name
	group

LAYOUT "component"
	name
	group

LAYOUT "color"
	col:name
	col:rgba

RECORD "simulation_pipeline" "pipeline"
	name "PipelineI.Simulation"
	group "sim_components"

RECORD "window_pipeline" "pipeline"
	name "PipelineI.Window"
	group "win_components"

RECORD "debugwindow" "component"
	name "ApplicationWindowI.debug"
	group "windatagroup"

RECORD 4 "component"
	name "ApplicationWindowI.simtest"
	group "windatagroup"

RECORD "background" "color"
	col:name "background"
	col:rgba "0.0 0.2 0.0 1.0"

RECORD "foreground" "color"
	col:name "foreground"
	col:rgba "1.0 1.0 0.0 1.0"

RECORD "hilight" "color"
	col:name "hilight"
	col:rgba "1.0 0.0 0.5 1.0"

RECORDGROUP 1
	"window_pipeline"

RECORDGROUP "sim_components"

RECORDGROUP "win_components"
	"debugwindow"

RECORDGROUP "windatagroup"
	"background"
	"foreground"
	"hilight"

END

