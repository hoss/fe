/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "architecture/architecture.h"

using namespace fe;
using namespace fe::ext;

int main(int argc,char** argv,char **env)
{
	BWORD completed=FALSE;
	UNIT_START();

	if(argc<2)
	{
		feLog("Usage: %s [rg file]\n", argv[0]);
		exit(1);
	}

	const char* file=argv[1];

	try
	{
		sp<Master> spMaster(new Master);
		spMaster->registry()->manage("fexArchitectureDL");

		sp<Scope> spScope = spMaster->registry()->create("Scope");
		String mediaPath=spMaster->catalog()->catalog<String>("path:media");
		sp<RecordGroup> spAppGroup=RecordView::loadRecordGroup(spScope,
				mediaPath+"/architecture/"+file);

		U32 applications=0;
		Accessor< cp<Component> > component(spScope, "rec:component");
		for(RecordGroup::iterator it = spAppGroup->begin();
				it != spAppGroup->end(); it++)
		{
			sp<RecordArray> spRA = *it;
			if(component.check(spRA))
			{
				for(U32 i=0; i<spRA->length(); i++)
				{
					cp<Component>& rcpComponent=component(spRA,i);
					sp<ApplicationI> spApplicationI=rcpComponent;
					UNIT_TEST(spApplicationI.isValid());

					if(spApplicationI.isValid())
					{
						int rv;
						spApplicationI->setup(argc, argv, env);
						spApplicationI->loop(rv);

						applications++;
					}
				}
			}
		}

		RecordView::saveRecordGroup(spScope,spAppGroup,"application_out.rg");

		UNIT_TEST(applications==1);
		completed=TRUE;
	}
	catch(Exception &e)			{ e.log(); }
	catch(std::exception &e)	{ fprintf(stderr,"%s\n", e.what()); }
	catch(...)					{ feLog("uncaught exception\n"); }
	UNIT_TEST(completed);
	UNIT_TRACK(3);
	UNIT_RETURN();
}
