/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "architecture/architecture.h"

using namespace fe;
using namespace fe::ext;

int main(int argc,char** argv,char **env)
{
	BWORD completed=FALSE;
	UNIT_START();

	const char* application="ApplicationI.Configured";
	if(argc>1)
	{
		application=argv[1];
	}

	try
	{
		sp<Master> spMaster(new Master);
		spMaster->registry()->manage("fexArchitectureDL");
		sp<ApplicationI> spApplication=
				spMaster->registry()->create(application);

		UNIT_TEST(spApplication.isValid());
		if(spApplication.isValid())
		{
			spApplication->setup(argc, argv, env);

			// temporarily switched int to String
//			int &run_level =
					spMaster->catalog()->catalog<String>("run:level");

			int rv;
			UNIT_TEST(spApplication->loop(rv));
		}
		completed=TRUE;
	}
	catch(Exception &e)			{ e.log(); }
	catch(std::exception &e)	{ fprintf(stderr,"%s\n", e.what()); }
	catch(...)					{ feLog("uncaught exception\n"); }
	UNIT_TEST(completed);
	UNIT_TRACK(3);
	UNIT_RETURN();
}

