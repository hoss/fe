require("util")

function sim_stage0()
	l_sim_hb:populate('sim:data',	'record')
end

function sim_stage1()
	create_particles()

	install(c_signaler, "HandlerI.Log", {l_sim_hb})
end

function create_particles()
	manage("fexShapeDL")

	c_scope = l_sim_hb:scope()

	l_sim_data = c_scope:declare("l_sim_data")
	l_sim_data:populate('dataset', 'group')
	r_sim_data = c_scope:createRecord(l_sim_data)
	--catalogRecord("sim:data", r_sim_data)

	rg_dataset = createRecordGroup()
	r_sim_data['dataset'] = rg_dataset

	l_particle = c_scope:declare("l_particle")
	l_particle:populate(create("PopulateI.AsPoint"))

	r_particle = c_scope:createRecord(l_particle)

	rg_dataset:add(r_particle)
end

