require("util")

function window_populate()
	l_win_data:populate(create("PopulateI.AsPerspective"))
	l_win_data:populate(create("PopulateI.AsSelection"))
end

function window_create()
	manage("fexDatauiDL")
	manage("fexDrawToolDL")
	manage("fexViewerDL")

	c_window:setTitle("xArchitecture Test App -- Simulation Window")

	colormap = {
		{ 'background',		0.0, 0.0, 0.0, 0.0 },
		{ 'foreground',		0.5, 0.5, 0.5, 1.0 },
		{ 'hilight',		0.7, 0.6, 0.5, 1.0 }
	}

	installColormap(r_win_data["win:group"], l_draw_hb:scope(), colormap)

	c_pctrl = install(c_window, "ControllerI.InspectController",
		{l_event})

	c_viewer = install(c_window, "ViewerI.PerspectiveViewer",
		{l_draw_hb})
	c_viewer:bind(c_window)

	c_grid = install(c_window, "HandlerI.DrawGrid",
		{l_draw_hb})


	c_ortho = install(c_window, "ViewerI.OrthoViewer",
		{l_draw_hb})
	c_ortho:bind(c_window)
	r_win_data['ortho:zoom'] = 1.0

	c_maskMapDebug = install(c_window, "HandlerI.MaskMapDebug",
		{l_draw_hb,l_event})
	c_maskMapDebug:setDraw(l_draw_hb)
	c_maskMapDebug:setOffset(0,0)
	c_maskMapDebug:setLabel("mask map")
	c_maskMapDebug:maskEnable(0)
	c_maskMapDebug:maskInit("mmpopup")
	c_maskMapDebug:maskSet("mmpopup_enable",
		WindowEvent.sourceKeyboard,
		string.byte('\t'),
		WindowEvent.statePress)
	c_maskMapDebug:maskSet("mmpopup_disable",
		WindowEvent.sourceKeyboard,
		string.byte('\t'),
		WindowEvent.statePress)

		-- reinsert perspective
	c_window:insert(c_viewer, l_draw_hb)

end
