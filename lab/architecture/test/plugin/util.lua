function install(c_signaler, component_name, signals)
	c_component = create(component_name)
	c_component:adjoin(c_signaler)
	for i,l_signal in signals do
		c_signaler:insert(c_component, l_signal)
	end
	return c_component
end

--[[function catalogRecord(label, r_in)
	c_r = create("RecordI")
	c_r:setRecord(r_in)
	catalog(c_r, label)
end]]

function config(c_cfg, settings)
	for k,v in settings do
		c_cfg:cfg(k,v)
	end
end

function installColormap(rg_target, c_scope, colormap)
	l_color = c_scope:declare("color")
	l_color:populate(create("PopulateI.AsColor"))

	for i in colormap do
		col_name = colormap[i][1]

		r_color = rg_target:lookup(c_scope, 'col:name', col_name)
		if not r_color:isValid() then
			r_color = c_scope:createRecord(l_color)
			r_color["col:name"] = col_name
			rg_target:add(r_color)
		end
		r_color["col:rgba"][0] = colormap[i][2]
		r_color["col:rgba"][1] = colormap[i][3]
		r_color["col:rgba"][2] = colormap[i][4]
		r_color["col:rgba"][3] = colormap[i][5]
	end
end

