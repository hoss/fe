/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __architecture_AssignI_h__
#define __architecture_AssignI_h__
namespace fe
{
namespace ext
{

class AssignI : virtual public HandlerI
{
	public:
virtual	void	set(const String &a_lhs, const String &a_rhs)				= 0;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __architecture_AssignI_h__ */

