/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __ptex_SurfaceAccessiblePtex_h__
#define __ptex_SurfaceAccessiblePtex_h__
namespace fe
{
namespace ext
{

/**************************************************************************//**
    @brief Ptex Surface Binding

	@ingroup ptex
*//***************************************************************************/
class FE_DL_EXPORT SurfaceAccessiblePtex:
	public SurfaceAccessibleBase
{
	public:
								SurfaceAccessiblePtex(void)					{}
virtual							~SurfaceAccessiblePtex(void)				{}

								//* as SurfaceAccessibleI

virtual	BWORD					isBound(void)
								{	return (m_texelArray.size()>1); }

								using SurfaceAccessibleBase::accessor;

virtual sp<SurfaceAccessorI>	accessor(String a_node,Element a_element,
										String a_name,
										Creation a_create,Writable a_writable);

virtual sp<SurfaceAccessorI>	accessor(String a_node,Element a_element,
										Attribute a_attribute,
										Creation a_create,Writable a_writable);

								using SurfaceAccessibleBase::load;

virtual	BWORD					load(String a_filename,
										sp<Catalog> a_spSettings);

								using SurfaceAccessibleBase::attributeSpecs;

virtual	void					attributeSpecs(
										Array<SurfaceAccessibleI::Spec>&
										a_rSpecs,
										String a_node,
										SurfaceAccessibleI::Element
										a_element) const;

	class Texel
	{
		public:
			SpatialVector	m_location;
			Color			m_color;
	};

	protected:

virtual	void					reset(void);

	private:
		Array<Texel>			m_texelArray;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __ptex_SurfaceAccessiblePtex_h__ */
