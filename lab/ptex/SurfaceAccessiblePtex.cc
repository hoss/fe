/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <ptex/ptex.pmh>

#define FE_SAP_DEBUG		FALSE
#define FE_SAP_VERBOSE		FALSE
#define FE_SAP_PARTITION	FALSE

namespace fe
{
namespace ext
{

void SurfaceAccessiblePtex::reset(void)
{
	SurfaceAccessibleBase::reset();

	m_texelArray.clear();
}

sp<SurfaceAccessorI> SurfaceAccessiblePtex::accessor(
	String a_node,Element a_element,String a_name,
	Creation a_create,Writable a_writable)
{
	sp<SurfaceAccessorPtex> spAccessor(new SurfaceAccessorPtex);

	if(spAccessor.isValid())
	{
		spAccessor->setWritable(a_writable);
		spAccessor->setTexelArray(&m_texelArray);

		if(spAccessor->bind(a_element,a_name) ||
				a_create==SurfaceAccessibleI::e_createMissing)
		{
			spAccessor->setSurfaceAccessible(sp<SurfaceAccessibleI>(this));
			return spAccessor;
		}
	}

	return sp<SurfaceAccessorI>(NULL);
}

sp<SurfaceAccessorI> SurfaceAccessiblePtex::accessor(
	String a_node,Element a_element,Attribute a_attribute,
	Creation a_create,Writable a_writable)
{
	sp<SurfaceAccessorPtex> spAccessor(new SurfaceAccessorPtex);

	if(spAccessor.isValid())
	{
		spAccessor->setWritable(a_writable);
		spAccessor->setTexelArray(&m_texelArray);

		if(spAccessor->bind(a_element,a_attribute) ||
				a_create==SurfaceAccessibleI::e_createMissing)
		{
			spAccessor->setSurfaceAccessible(sp<SurfaceAccessibleI>(this));
			return spAccessor;
		}
	}

	return sp<SurfaceAccessorI>(NULL);
}

void SurfaceAccessiblePtex::attributeSpecs(
	Array<SurfaceAccessibleI::Spec>& a_rSpecs,
	String a_node,
	SurfaceAccessibleI::Element a_element) const
{
	a_rSpecs.clear();

	//* NOTE a_node ignored

	if(a_element==SurfaceAccessibleI::e_point)
	{
		SurfaceAccessibleI::Spec spec;

		spec.set("P","vector3");
		a_rSpecs.push_back(spec);

		spec.set("Cd","vector3");
		a_rSpecs.push_back(spec);
	}
}

BWORD SurfaceAccessiblePtex::load(String a_filename,sp<Catalog> a_spSettings)
{
#if FE_SAP_DEBUG
	feLog("SurfaceAccessiblePtex::load \"%s\"\n",a_filename.c_str());
#endif

	sp<SurfaceAccessibleI> spContext;

	String options;
	if(a_spSettings.isValid())
	{
		options=a_spSettings->catalog<String>("options");
		spContext=a_spSettings->catalogOrDefault< sp<Component> >(
				"context",sp<Component>(NULL));
	}

	I32 depth=1;
	Real resolution=1.0;

	String option;
	while(!(option=options.parse()).empty())
	{
		const String property=option.parse("\"","=");
		const String value=option.parse("\"","=");

		if(property=="depth")
		{
			depth=fe::maximum(1,value.integer());
		}
		if(property=="resolution")
		{
			resolution=value.real();
		}
	}

	Ptex::String ptexError;
	PtexPtr<PtexTexture> ptrTexture(
			PtexTexture::open(a_filename.c_str(),ptexError));

	if(!ptexError.empty())
	{
		feX(e_invalidPointer,"SurfaceAccessiblePtex::load",
				"open error \"%s\"\n",ptexError.c_str());
	}

	if(!ptrTexture)
	{
		feX(e_invalidPointer,"SurfaceAccessiblePtex::load","NULL texture\n");
	}

	Array<I32> faceVertexCountArray;
	Array<I32> faceVertexIndexArray;
	Array<SpatialVector> pointArray;

	PtexPtr<PtexMetaData> ptrMetaData(ptrTexture->getMetaData());

	const I32 metaDataCount=ptrMetaData->numKeys();
	if(!metaDataCount)
	{
		feLog("SurfaceAccessiblePtex::load found no metaData\n");
	}

	for(I32 metaDataIndex=0;metaDataIndex<metaDataCount;metaDataIndex++)
	{
		Ptex::MetaDataType dataType;
		const char* rawKey;

		ptrMetaData->getKey(metaDataIndex,rawKey,dataType);

#if FE_SAP_VERBOSE
		feLog("meta %d/%d \"%s\"\n",metaDataIndex,metaDataCount,rawKey);
#endif

		const String key(rawKey);

		switch(dataType)
		{
			case Ptex::mdt_string:
			{
				const char* value;
				ptrMetaData->getValue(rawKey,value);

#if FE_SAP_VERBOSE
				feLog("  string \"%s\"\n",value);
#endif

				//* TODO create detail string
				break;
			}
			case Ptex::mdt_int8:
			{
#if FE_SAP_VERBOSE
				const int8_t* valueBuffer;
				I32 valueCount=0;

				ptrMetaData->getValue(rawKey,valueBuffer,valueCount);

				for(I32 valueIndex=0;valueIndex<valueCount;valueIndex++)
				{
					feLog("  int8 %d/%d %d\n",
							valueIndex,valueCount,valueBuffer[valueIndex]);
				}
#endif
				break;
			}
			case Ptex::mdt_int16:
			{
#if FE_SAP_VERBOSE
				const int16_t* valueBuffer;
				I32 valueCount=0;

				ptrMetaData->getValue(rawKey,valueBuffer,valueCount);

				for(I32 valueIndex=0;valueIndex<valueCount;valueIndex++)
				{
					feLog("  int16 %d/%d %d\n",
							valueIndex,valueCount,valueBuffer[valueIndex]);
				}
#endif
				break;
			}
			case Ptex::mdt_int32:
			{
				const int32_t* valueBuffer;
				I32 valueCount=0;

				ptrMetaData->getValue(rawKey,valueBuffer,valueCount);

#if FE_SAP_VERBOSE
				for(I32 valueIndex=0;valueIndex<valueCount;valueIndex++)
				{
					feLog("  int32 %d/%d %d\n",
							valueIndex,valueCount,valueBuffer[valueIndex]);
				}
#endif

				if(key=="PtexFaceVertCounts")
				{
					faceVertexCountArray.resize(valueCount);
					for(I32 valueIndex=0;valueIndex<valueCount;valueIndex++)
					{
						faceVertexCountArray[valueIndex]=
								valueBuffer[valueIndex];
					}
				}

				if(key=="PtexFaceVertIndices")
				{
					faceVertexIndexArray.resize(valueCount);
					for(I32 valueIndex=0;valueIndex<valueCount;valueIndex++)
					{
						faceVertexIndexArray[valueIndex]=
								valueBuffer[valueIndex];
					}
				}

				break;
			}
			case Ptex::mdt_float:
			{
				const float* valueBuffer;
				I32 valueCount=0;

				ptrMetaData->getValue(rawKey,valueBuffer,valueCount);

#if FE_SAP_VERBOSE
				for(I32 valueIndex=0;valueIndex<valueCount;valueIndex++)
				{
					feLog("  float %d/%d %.6G\n",
							valueIndex,valueCount,valueBuffer[valueIndex]);
				}
#endif

				if(key=="PtexVertPositions")
				{
					//* NOTE each 3 floats is a point location

					const I32 pointCount=valueCount/3;

					pointArray.resize(pointCount);
					for(I32 pointIndex=0;pointIndex<pointCount;pointIndex++)
					{
						const I32 valueIndex=pointIndex*3;

						set(pointArray[pointIndex],
								valueBuffer[valueIndex],
								valueBuffer[valueIndex+1],
								valueBuffer[valueIndex+2]);
					}
				}

				break;
			}
			case Ptex::mdt_double:
			{
#if FE_SAP_VERBOSE
				const double* valueBuffer;
				I32 valueCount=0;

				ptrMetaData->getValue(rawKey,valueBuffer,valueCount);

				for(I32 valueIndex=0;valueIndex<valueCount;valueIndex++)
				{
					feLog("  double %d/%d %.12G\n",
							valueIndex,valueCount,valueBuffer[valueIndex]);
				}
#endif
				break;
			}
		}
	}

	const Ptex::DataType ptexDataType=ptrTexture->dataType();
	const Ptex::MeshType ptexMeshType=ptrTexture->meshType();

#if FE_SAP_DEBUG
	String ptexDataTypeName;
	switch(ptexDataType)
	{
		case Ptex::dt_uint8:
			ptexDataTypeName="dt_uint8";
			break;
		case Ptex::dt_uint16:
			ptexDataTypeName="dt_uint16";
			break;
		case Ptex::dt_half:
			ptexDataTypeName="dt_half";
			break;
		case Ptex::dt_float:
			ptexDataTypeName="dt_float";
			break;
		default:
			ptexDataTypeName="unknown";
			break;
	}

	String ptexMeshTypeName;
	switch(ptexMeshType)
	{
		case Ptex::mt_triangle:
			ptexMeshTypeName="mt_triangle";
			break;
		case Ptex::mt_quad:
			ptexMeshTypeName="mt_quad";
			break;
		default:
			ptexMeshTypeName="unknown";
			break;
	}

	feLog("SurfaceAccessiblePtex::load DataType \"%s\" MeshType \"%s\"\n",
			ptexDataTypeName.c_str(),ptexMeshTypeName.c_str());
#endif

	I32 lookupIndex=0;
	Array< Array<I32> > primVerts;

	sp<SurfaceAccessibleI> spHullAccessible;
	sp<SurfaceAccessorI> spHullVertices;

	sp<SurfaceAccessorI> spSubdivVertices;
	sp<SurfaceAccessorI> spSubdivSubuv;
	sp<SurfaceAccessorI> spSubdivPartition;

	I32 hullFaceCount=faceVertexCountArray.size();
	if(hullFaceCount)
	{
		primVerts.resize(hullFaceCount);

		for(I32 hullFaceIndex=0;hullFaceIndex<hullFaceCount;hullFaceIndex++)
		{
			const I32 subCount=faceVertexCountArray[hullFaceIndex];

			Array<I32>& rVerts=primVerts[hullFaceIndex];
			rVerts.resize(subCount);

			for(I32 subIndex=0;subIndex<subCount;subIndex++)
			{
				rVerts[subIndex]=faceVertexIndexArray[lookupIndex+subIndex];
			}

			lookupIndex+=subCount;
		}

		//* populate SurfaceAccessibleI with Ptex metadata
		spHullAccessible=
				registry()->create("*.SurfaceAccessibleCatalog");

		sp<SurfaceAccessorI> spHullPoint=
				spHullAccessible->accessor(e_point,e_position);

		const I32 pointCount=pointArray.size();
		spHullPoint->append(pointCount);

		for(I32 pointIndex=0;pointIndex<pointCount;pointIndex++)
		{
			spHullPoint->set(pointIndex,pointArray[pointIndex]);
		}

		spHullVertices=
				spHullAccessible->accessor(e_primitive,e_vertices);

		spHullVertices->append(primVerts);
	}
	else
	{
		spHullAccessible=spContext;

		if(spHullAccessible.isValid())
		{
			spHullVertices=
					spHullAccessible->accessor(e_primitive,e_vertices);

			hullFaceCount=spHullVertices->count();

			feLog("SurfaceAccessiblePtex::load context hullFaceCount %d\n",
					hullFaceCount);
		}
	}

	if(!hullFaceCount)
	{
		spHullAccessible=NULL;
	}

	if(spHullAccessible.isValid())
	{
		sp<SurfaceAccessibleI> spSubdivAccessible=
				registry()->create("*.SurfaceAccessibleCatalog");

		sp<Scope> spScope=registry()->master()->catalog()->catalogComponent(
				"Scope","OperatorScope");

		TerminalNode terminalNode;
		terminalNode.setupOperator(spScope,"SubdivideOp",
				"SurfaceAccessiblePtex.SubdivideOp");

		sp<Catalog> spCatalog=terminalNode.catalog();
		spCatalog->catalog<I32>("depth")=depth;
		spCatalog->catalog<bool>("writeHull")=true;
		spCatalog->catalog< sp<Component> >("Input Surface")=spHullAccessible;
		spCatalog->catalog< sp<Component> >("Output Surface")=
				spSubdivAccessible;

		terminalNode.cook(0.0);

		const String partitionAttr=spCatalog->catalog<String>("partitionAttr");

		spSubdivVertices=spSubdivAccessible->accessor(e_primitive,e_vertices);
		spSubdivSubuv=spSubdivAccessible->accessor(e_vertex,"subuv");

#if 0//FE_SAP_PARTITION
		sp<SurfaceAccessorI> spSubdivUV=
				spSubdivAccessible->accessor(e_vertex,e_uv);

		const U32 subdivPrimitiveCount=spSubdivVertices->count();
		for(U32 subdivPrimitiveIndex=0;
				subdivPrimitiveIndex<subdivPrimitiveCount;
				subdivPrimitiveIndex++)
		{
			const U32 subCount=spSubdivVertices->subCount(subdivPrimitiveIndex);
			for(U32 subIndex=0;subIndex<subCount;subIndex++)
			{
				const SpatialVector subuv=spSubdivSubuv->spatialVector(
						subdivPrimitiveIndex,subIndex);

				spSubdivUV->set(subdivPrimitiveIndex,subIndex,subuv);
			}
		}

		sp<SurfaceI> spLimitSurface=spSubdivAccessible->surface();
		spLimitSurface->partitionWith(partitionAttr);
#else
		spSubdivPartition=
				spSubdivAccessible->accessor(e_primitive,partitionAttr);
#endif
	}

	sp<SurfaceAccessibleI> spFragAccessible;
//	sp<SurfaceAccessorI> spFragPoint;
//	sp<SurfaceAccessorI> spFragUV;
//	sp<SurfaceAccessorI> spFragVertices;

	if(spSubdivVertices.isValid())
	{
		spFragAccessible=
				registry()->create("*.SurfaceAccessibleCatalog");

//		spFragPoint= spFragAccessible->accessor(e_point,e_position);
//		spFragUV= spFragAccessible->accessor(e_point,e_uv);
//		spFragVertices=spFragAccessible->accessor(e_primitive,e_vertices);
	}

	const U32 subdivPrimitiveCount=spSubdivVertices.isValid()?
			spSubdivVertices->count(): 0;
	Array<I32> faceOfSubPrim(subdivPrimitiveCount);
	for(U32 subdivPrimitiveIndex=0;
			subdivPrimitiveIndex<subdivPrimitiveCount;
			subdivPrimitiveIndex++)
	{
		const String onePartition=
				spSubdivPartition->string(subdivPrimitiveIndex);

		faceOfSubPrim[subdivPrimitiveIndex]=onePartition.integer();
	}

	//* NOTE one face per quad; three faces per triangle

	I32 faceIndex=0;
	I32 triSection=0;

	const I32 channelCount=ptrTexture->numChannels();
//	feLog("channelCount %d\n",channelCount);

	const I32 faceCount=ptrTexture->numFaces();
	for(I32 faceID=0;faceID<faceCount;faceID++)
	{
//~		String facePartition;
//~		facePartition.sPrintf("%d",faceIndex);

#if FE_SAP_VERBOSE
		feLog("SurfaceAccessiblePtex::load partition \"%s\"\n",
				facePartition.c_str());
#endif


		sp<SurfaceI> spFragSurface;
		Array<SpatialVector> uvArray;

#if 0//FE_SAP_PARTITION
		spLimitSurface->setPartitionFilter(facePartition);
#else
		if(spSubdivVertices.isValid())
		{
			const U32 fragEstimate=powl(4,depth);

			primVerts.resize(fragEstimate);
			pointArray.resize(fragEstimate*4);
			uvArray.resize(fragEstimate*4);

			U32 fragPrimitiveCount=0;
			U32 fragPointCount=0;

			for(U32 subdivPrimitiveIndex=0;
					subdivPrimitiveIndex<subdivPrimitiveCount;
					subdivPrimitiveIndex++)
			{
//~				const String onePartition=
//~						spSubdivPartition->string(subdivPrimitiveIndex);

//~				if(onePartition!=facePartition)
				if(faceOfSubPrim[subdivPrimitiveIndex]!=faceIndex)
				{
					continue;
				}

				const U32 subCount=
						spSubdivVertices->subCount(subdivPrimitiveIndex);

				if(primVerts.size()<fragPrimitiveCount+1)
				{
					primVerts.resize(fragPrimitiveCount+1);
				}

				if(pointArray.size()<fragPointCount+subCount)
				{
					pointArray.resize(fragPointCount+subCount);
					uvArray.resize(fragPointCount+subCount);
				}

				Array<I32>& rVerts=primVerts[fragPrimitiveCount];
				rVerts.resize(subCount);

				for(U32 subIndex=0;subIndex<subCount;subIndex++)
				{
					const U32 fragPointIndex=fragPointCount+subIndex;

					rVerts[subIndex]=fragPointIndex;

					pointArray[fragPointIndex]=spSubdivVertices->spatialVector(
							subdivPrimitiveIndex,subIndex);
					uvArray[fragPointIndex]=spSubdivSubuv->spatialVector(
							subdivPrimitiveIndex,subIndex);
				}

				fragPointCount+=subCount;
				fragPrimitiveCount++;
			}

			primVerts.resize(fragPrimitiveCount);

//~			sp<SurfaceAccessibleI> spFragAccessible=
//~					registry()->create("*.SurfaceAccessibleCatalog");

			spFragAccessible->clear();

			sp<SurfaceAccessorI> spFragPoint=
					spFragAccessible->accessor(e_point,e_position);
			spFragPoint->append(fragPointCount);

			sp<SurfaceAccessorI> spFragUV=
					spFragAccessible->accessor(e_point,e_uv);

			for(U32 fragPointIndex=0;fragPointIndex<fragPointCount;
					fragPointIndex++)
			{
				spFragPoint->set(fragPointIndex,pointArray[fragPointIndex]);
				spFragUV->set(fragPointIndex,uvArray[fragPointIndex]);
			}

			sp<SurfaceAccessorI> spFragVertices=
					spFragAccessible->accessor(e_primitive,e_vertices);
			spFragVertices->append(primVerts);

			spFragSurface=spFragAccessible->surface();
		}
#endif

//		const Ptex::FaceInfo& rFaceInfo=ptrTexture->getFaceInfo(faceID);

		PtexPtr<PtexFaceData> ptrFaceData(ptrTexture->getData(faceID));

		const Ptex::Res ptexRes=ptrFaceData->res();
		const I32 sizeU=ptexRes.u();
		const I32 sizeV=ptexRes.v();

		const I32 samplesU=fe::maximum(1,I32(sizeU*resolution));
		const I32 samplesV=fe::maximum(1,I32(sizeV*resolution));

		if(hullFaceCount && faceIndex>=hullFaceCount)
		{
			feLog("SurfaceAccessiblePtex::load"
					" face %d/%d bad lookup %d/%d\n",
					faceID,faceCount,faceIndex,hullFaceCount);
			continue;
		}

		const I32 subCount=spHullVertices.isValid()?
				spHullVertices->subCount(faceIndex): 4;
		const BWORD subTriangles=(subCount==3 && ptexMeshType==Ptex::mt_quad);

		const U32 texelCount=m_texelArray.size();
		const U32 addCount=samplesU*samplesV;

//		feLog("%d subCount %d size %d %d\n",
//				faceID,subCount,sizeU,sizeV);

		m_texelArray.resize(texelCount+addCount);

		Vector2 uv0(0.0,0.0);
		Vector2 uv1(1.0,0.0);
		Vector2 uv2(1.0,1.0);
		Vector2 uv3(0.0,1.0);

		if(subTriangles)
		{
#if 0//FE_SAP_PARTITION
			const Real third=(Real(1)/Real(3));

			set(uv2,third,third);

			switch(triSection)
			{
				case 0:
					set(uv1,0.5,0.0);
					set(uv3,0.0,0.5);
					break;

				case 1:
					set(uv0,1.0,0.0);
					set(uv1,0.5,0.5);
					set(uv3,0.5,0.0);
					break;

				case 2:
					set(uv0,0.0,1.0);
					set(uv1,0.0,0.5);
					set(uv3,0.5,0.5);
					break;
			}
#else
			if(spSubdivVertices.isValid())
			{
				const U32 pow4=pow(4,depth-1);
				const U32 tri4=triSection*pow4*4;
				uv0=uvArray[tri4];
				uv1=uvArray[tri4+pow4];
				uv2=uvArray[tri4+pow4*2];
				uv3=uvArray[tri4+pow4*3];
			}
#endif

//			feLog("uv0 %s\n",c_print(uv0));
//			feLog("uv1 %s\n",c_print(uv1));
//			feLog("uv2 %s\n",c_print(uv2));
//			feLog("uv3 %s\n",c_print(uv3));
		}

		Real startU=Real(1)/Real(2*samplesU);
		Real startV=Real(1)/Real(2*samplesV);

		Real scaleU=Real(1)-Real(2)*startU;
		Real scaleV=Real(1)-Real(2)*startV;

		if(ptexMeshType==Ptex::mt_triangle)
		{
			startU=Real(1)/Real(3*samplesU);
			startV=Real(1)/Real(3*samplesV);

			scaleU=Real(1)-Real(3)*startU;
			scaleV=Real(1)-Real(3)*startV;
		}

		const Real startU2=Real(2)/Real(3*samplesU);
		const Real startV2=Real(2)/Real(3*samplesV);

		const Real scaleU2=Real(1)-Real(3)*startU2;
		const Real scaleV2=Real(1)-Real(3)*startV2;

		for(I32 indexV=0;indexV<samplesV;indexV++)
		{
			for(I32 indexU=0;indexU<samplesU;indexU++)
			{
				const U32 addIndex=indexV*samplesU+indexU;

				FEASSERT(addIndex<addCount);
				FEASSERT(texelCount+addIndex<m_texelArray.size());

				Texel& rTexel=m_texelArray[texelCount+addIndex];

				Real ratioU;
				Real ratioV;

				U32 texelU;
				U32 texelV;

				if(samplesU>1)
				{
					ratioU=indexU/Real(samplesU-1);
					texelU=ratioU*(sizeU-1);
				}
				else
				{
					ratioU=0.5;
					texelU=0;
				}

				if(samplesV>1)
				{
					ratioV=indexV/Real(samplesV-1);
					texelV=ratioV*(sizeV-1);
				}
				else
				{
					ratioV=0.5;
					texelV=0;
				}

				if(ptexMeshType==Ptex::mt_triangle)
				{
					ratioU=indexV/Real(samplesV-1);
					ratioV=indexU/Real(samplesU-1);

					texelU=ratioV*(sizeV-1);
					texelV=ratioU*(sizeU-1);
				}

				const Real subU=startU+ratioU*scaleU;
				const Real subV=startV+ratioV*scaleV;

				const Vector2 subuv03=uv0+subV*(uv3-uv0);
				const Vector2 subuv12=uv1+subV*(uv2-uv1);
				Vector2 subuv=subuv03+subU*(subuv12-subuv03);

				if(ptexMeshType==Ptex::mt_triangle)
				{
					if(subuv[0]+subuv[1]>Real(1))
					{
						const Real ratioU2=Real(1)-(indexU-1)/Real(samplesU-2);
						const Real subU2=startU2+ratioU2*scaleU2;

						const Real ratioV2=Real(1)-(indexV-1)/Real(samplesV-2);
						const Real subV2=startV2+ratioV2*scaleV2;

						set(subuv,subU2,subV2);
					}
				}

				//* TODO should stop just short of edges
#if 0//FE_SAP_PARTITION
				if(spLimitSurface.isValid)
				rTexel.m_location=spLimitSurface.isValid()?
						spLimitSurface->samplePoint(subuv):
						SpatialVector(ratioU,ratioV,faceID);
#else
				rTexel.m_location=spFragSurface.isValid()?
						spFragSurface->samplePoint(subuv):
						SpatialVector(ratioU,ratioV,faceID);
#endif

				switch(ptexDataType)
				{
					case Ptex::dt_uint8:
					{
						uint8_t pPixelU8[3];
						ptrFaceData->getPixel(texelU,texelV,pPixelU8);

//						feLog("u8[%d] %d %d %d\n",channelCount,
//								pPixelU8[0],pPixelU8[1],pPixelU8[2]);

						if(channelCount==3)
						{
							set(rTexel.m_color,
									pPixelU8[0]/Real(255),
									pPixelU8[1]/Real(255),
									pPixelU8[2]/Real(255));
						}
						else
						{
							const Real shade=
									pPixelU8[0]/Real(255);
							set(rTexel.m_color,
									shade,shade,shade);
						}
					}
						break;

					case Ptex::dt_uint16:
					{
						uint16_t pPixelU16[3];
						ptrFaceData->getPixel(texelU,texelV,pPixelU16);

//						feLog("u16[%d] %d %d %d\n",channelCount,
//								pPixelU16[0],pPixelU16[1],pPixelU16[2]);

						if(channelCount==3)
						{
							set(rTexel.m_color,
									pPixelU16[0]/Real(65535),
									pPixelU16[1]/Real(65535),
									pPixelU16[2]/Real(65535));
						}
						else
						{
							const Real shade=
									pPixelU16[0]/Real(65535);
							set(rTexel.m_color,
									shade,shade,shade);
						}
					}
						break;

					case Ptex::dt_half:
					{
#ifdef _HALF_H_
						half pHalf[3];
						ptrFaceData->getPixel(texelU,texelV,pHalf);

						if(channelCount==3)
						{
							set(rTexel.m_color,
									pHalf[0],
									pHalf[1],
									pHalf[2]);
						}
						else
						{
							set(rTexel.m_color,
									pHalf[0],
									pHalf[0],
									pHalf[0]);
						}
#endif
					}
						break;

					case Ptex::dt_float:
					{
						float pFloat[3];
						ptrFaceData->getPixel(texelU,texelV,pFloat);

//						feLog("float[%d] %.6G %.6G %.6G\n",channelCount,
//								pFloat[0],pFloat[1],pFloat[2]);


						if(channelCount==3)
						{
							set(rTexel.m_color,
									pFloat[0],
									pFloat[1],
									pFloat[2]);
						}
						else
						{
							set(rTexel.m_color,
									pFloat[0],
									pFloat[0],
									pFloat[0]);
						}
					}
						break;
				}
			}
		}

		if(subTriangles)
		{
			triSection=(triSection+1)%subCount;
		}
		if(!triSection)
		{
			triSection=0;
			faceIndex++;
		}

		FEASSERT(!triSection || subTriangles);
	}

	const I32 texelCount=m_texelArray.size();

#if FE_SAP_VERBOSE
	const I32 printCount=fe::minimum(I32(m_texelArray.size()),16);
	for(I32 texelIndex=0;texelIndex<printCount;texelIndex++)
	{
		const Texel& rTexel=m_texelArray[texelIndex];

		feLog("%d/%d loc %s color %s\n",texelIndex,texelCount,
				c_print(rTexel.m_location),c_print(rTexel.m_color));
	}
#endif

#if FE_SAP_DEBUG
	feLog("SurfaceAccessiblePtex::load texelCount %d\n",texelCount);
#endif

	return (texelCount>0);
}

} /* namespace ext */
} /* namespace fe */
