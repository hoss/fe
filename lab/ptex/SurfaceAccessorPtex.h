/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __ptex_SurfaceAccessorPtex_h__
#define __ptex_SurfaceAccessorPtex_h__
namespace fe
{
namespace ext
{

/**************************************************************************//**
    @brief Accessor for Ptex Texels

	@ingroup ptex
*//***************************************************************************/
class FE_DL_EXPORT SurfaceAccessorPtex:
	public SurfaceAccessorBase
{
	public:
						SurfaceAccessorPtex(void):
							m_pTexelArray(NULL)
						{	setName("SurfaceAccessorPtex"); }
virtual					~SurfaceAccessorPtex(void)							{}

						using SurfaceAccessorBase::set;
						using SurfaceAccessorBase::spatialVector;

						//* as SurfaceAccessorI
		BWORD			bind(SurfaceAccessibleI::Element a_element,
								SurfaceAccessibleI::Attribute a_attribute)
						{
							m_attribute=a_attribute;

							String name;
							switch(a_attribute)
							{
								case SurfaceAccessibleI::e_generic:
								case SurfaceAccessibleI::e_position:
									name="P";
									break;
								case SurfaceAccessibleI::e_normal:
									name="N";
									break;
								case SurfaceAccessibleI::e_uv:
									name="uv";
									break;
								case SurfaceAccessibleI::e_color:
									name="Cd";
									break;
								case SurfaceAccessibleI::e_vertices:
									m_attrName="vertices";
									FEASSERT(a_element==
											SurfaceAccessibleI::e_primitive);
									m_element=a_element;
									return TRUE;
								case SurfaceAccessibleI::e_properties:
									m_attrName="properties";
									FEASSERT(a_element==
											SurfaceAccessibleI::e_primitive);
									m_element=a_element;
									return TRUE;
							}
							return bindInternal(a_element,name);
						}
		BWORD			bind(SurfaceAccessibleI::Element a_element,
								const String& a_name)
						{
							m_attribute=SurfaceAccessibleI::e_generic;

							if(a_name=="P")
							{
								m_attribute=SurfaceAccessibleI::e_position;
							}
							else if(a_name=="N")
							{
								m_attribute=SurfaceAccessibleI::e_normal;
							}
							else if(a_name=="uv")
							{
								m_attribute=SurfaceAccessibleI::e_uv;
							}
							else if(a_name=="Cd")
							{
								m_attribute=SurfaceAccessibleI::e_color;
							}

							return bindInternal(a_element,a_name);
						}
virtual	U32				count(void) const
						{
							if(!isBound() ||
									m_element!=SurfaceAccessibleI::e_point ||
									(m_attribute!=
									SurfaceAccessibleI::e_position &&
									m_attrName!="Cd"))
							{
								return 0;
							}

							return (*m_pTexelArray).size();
						}
virtual	U32				subCount(U32 a_index) const
						{	return 0; }

virtual	void			set(U32 a_index,U32 a_subIndex,String a_string)		{}
virtual	String			string(U32 a_index,U32 a_subIndex=0)
						{	return ""; }

virtual	void			set(U32 a_index,U32 a_subIndex,I32 a_integer)		{}
virtual	I32				integer(U32 a_index,U32 a_subIndex=0)
						{	return 0; }

virtual	void			set(U32 a_index,U32 a_subIndex,Real a_real)			{}
virtual	Real			real(U32 a_index,U32 a_subIndex=0)
						{	return Real(0); }

virtual	void			set(U32 a_index,U32 a_subIndex,
								const SpatialVector& a_vector)				{}
virtual	SpatialVector	spatialVector(U32 a_index,U32 a_subIndex=0)
						{
							if(!isBound())
							{
								return SpatialVector(0.0,0.0,0.0);
							}

							if(m_element!=SurfaceAccessibleI::e_point)
							{
								return SpatialVector(0.0,0.0,0.0);
							}

							if(m_attribute==SurfaceAccessibleI::e_position)
							{
								return (*m_pTexelArray)[a_index].m_location;
							}

							if(m_attrName=="Cd")
							{
								return (*m_pTexelArray)[a_index].m_color;
							}

							return SpatialVector(0.0,0.0,0.0);
						}

						//* Ptex specific
		void			setTexelArray(Array<SurfaceAccessiblePtex::Texel>*
								a_pTexelArray)
						{	m_pTexelArray=a_pTexelArray; }

	private:

virtual	BWORD			bindInternal(SurfaceAccessibleI::Element a_element,
								const String& a_name)
						{
							//* TODO can Ptex have custom attributes?
							if(m_attribute==SurfaceAccessibleI::e_generic)
							{
								return FALSE;
							}

							if(a_element<0 && a_element>5)
							{
								a_element=SurfaceAccessibleI::e_point;
							}

							m_element=a_element;
							m_attrName=a_name;

							return TRUE;
						}

		BWORD			isBound(void) const
						{	return (m_pTexelArray!=NULL); }

		Array<SurfaceAccessiblePtex::Texel>*	m_pTexelArray;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __ptex_SurfaceAccessorPtex_h__ */
