/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __terrain_h__
#define __terrain_h__

#include "fe/data.h"
#include "draw/draw.h"
#include "surface/surface.h"

#include "terrain/StratumI.h"
#include "terrain/StratumDrawI.h"
#include "terrain/StrataI.h"

#include "terrain/Stratum.h"
#include "terrain/Strata.h"

#endif /* __terrain_h__ */
