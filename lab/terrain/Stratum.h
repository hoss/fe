/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __terrain_Stratum_h__
#define __terrain_Stratum_h__
namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Stratum RecordView

	@ingroup terrain
*//***************************************************************************/
class FE_DL_EXPORT Stratum: public Recordable
{
	public:
		Functor<I32>				serial;
		Functor<I32>				phase;
		Functor<Real>				density;
		Functor<Real>				offset;
		Functor<Real>				scale;
		Functor< sp<RecordGroup> >	dataRG;

				Stratum(void)			{ setName("Stratum"); }
virtual	void	addFunctors(void)
				{
					Recordable::addFunctors();

					add(serial,		FE_USE(":SN"));
					add(phase,		FE_SPEC("surf:phase",
							"Bounded solid, liquid, or gas"));
					add(density,	FE_SPEC("surf:density",
							"Mass per volume"));
					add(offset,		FE_SPEC("surf:offset",
							"Vertical displacement"));
					add(scale,		FE_SPEC("surf:scale",
							"Vertical amplification"));
					add(dataRG,		FE_SPEC("surf:datagroup",
							"arbitrary surface elements"));
				}
virtual	void	initializeRecord(void)
				{
					Recordable::initializeRecord();

					phase()=StratumI::e_gas;
					density()=1.0f;
					offset()=0.0f;
					scale()=1.0f;

					// replaced if loaded
					dataRG.createAndSetRecordGroup();
				}
};

} /* namespace ext */
} /* namespace fe */

#endif /* __terrain_Stratum_h__ */
