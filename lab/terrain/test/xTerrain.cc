/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "terrain/terrain.h"
#include "viewer/viewer.h"
#include "draw/draw.h"

using namespace fe;
using namespace fe::ext;

class MyDraw: public HandlerI
{
	public:
				MyDraw(void):
						m_time(0.0f)										{}

		void	bind(sp<StratumDrawI> spStratumDrawI,sp<StratumI> spStratumI)
				{
					m_spStratumDrawI=spStratumDrawI;
					m_spStratumI=spStratumI;

					m_spStratumI->setPhase(StratumI::e_liquid);
				}

virtual void	handle(Record& render)
				{
fprintf(stderr, "DRAW HANDLE\n");
					if(!m_asViewer.scope().isValid())
					{
						sp<Scope> spScope=render.layout()->scope();

						m_asViewer.bind(spScope);

						m_spZoneGroup=new RecordGroup();

						m_cylinderRV.bind(spScope);
						m_zone[0]=m_cylinderRV.createRecord();
						m_cylinderRV.baseRadius()=1.0f;
						m_cylinderRV.endRadius()=3.0f;
						set(m_cylinderRV.span(),0.0f,0.0f,8.0f);

						m_sphereRV.bind(spScope);
						m_zone[1]=m_sphereRV.createRecord();
						set(m_sphereRV.location(),-5.0f,-4.0f,0.0f);
						m_sphereRV.radius()=4.0f;

						m_spZoneGroup->add(m_zone[0]);
						m_spZoneGroup->add(m_zone[1]);

						m_spStratumDrawI->assignVisibility(m_spZoneGroup);
					}

					if(m_asViewer.viewer_layer.check(render) &&
							m_asViewer.viewer_spatial.check(render))
					{
						const I32& rPerspective=
								m_asViewer.viewer_spatial(render);
						if(rPerspective==1)
						{
							static Real cycle=0.0f;
							cycle+=.1f;
							m_cylinderRV.bind(m_zone[0]);
							set(m_cylinderRV.span(),8.0f*cosf(cycle),
									8.0f*sinf(cycle),0.0f);

							m_sphereRV.bind(m_zone[1]);
							m_sphereRV.radius()=2.0f+1.0f*sin(2.0f*cycle);

							m_spStratumI->setTime(m_time+=0.03f);
							m_spStratumDrawI->draw(m_spStratumI);
						}
					}
				}

	private:
		sp<StratumDrawI>	m_spStratumDrawI;
		sp<StratumI>		m_spStratumI;
		Real				m_time;
		AsViewer			m_asViewer;

		Record				m_zone[2];
		sp<RecordGroup>		m_spZoneGroup;
		Sphere				m_sphereRV;
		Cylinder			m_cylinderRV;
};

int main(int argc,char** argv)
{
	UNIT_START();

	BWORD complete=FALSE;
	U32 frames=(argc>1)? atoi(argv[1])+1: 0;

	try
	{
		sp<Master> spMaster(new Master);
		sp<Registry> spRegistry=spMaster->registry();

		Result result=spRegistry->manage("fexTerrainDL");
		UNIT_TEST(successful(result));
		result=spRegistry->manage("fexViewerDL");
		UNIT_TEST(successful(result));

		{
			sp<Scope> spSimScope=
					spMaster->catalog()->catalogComponent("Scope","SimScope");
			Record stratum=spSimScope->produceRecord("Stratum");
			Stratum stratumRV;
			stratumRV.bind(stratum);
			stratumRV.recordableName()="*.StratumRaster";
			stratumRV.finalizeRecord();
			sp<StratumI> spStratumI=stratumRV.component();

			sp<QuickViewerI> spQuickViewerI(
					spRegistry->create("QuickViewerI"));
			sp<StratumDrawI> spStratumDrawI(
					spRegistry->create("StratumDrawI"));

			if(!spQuickViewerI.isValid() || !spStratumI.isValid() ||
					!spStratumDrawI.isValid())
			{
				feX(argv[0], "couldn't create components");
			}

			sp<DrawI> spDrawI=spQuickViewerI->getDrawI();
			spDrawI->drawMode()->setLineWidth(1.5f);

			spStratumDrawI->bind(spDrawI);

			sp<MyDraw> spMyDraw(Library::create<MyDraw>("MyDraw"));
			spMyDraw->bind(spStratumDrawI,spStratumI);

			spQuickViewerI->insertDrawHandler(spMyDraw);
			spQuickViewerI->run(frames);

			spStratumDrawI=spRegistry->create("*.StratumDrawWire");
			spStratumDrawI->bind(spDrawI);
			spMyDraw->bind(spStratumDrawI,spStratumI);
			spQuickViewerI->run(frames);
fprintf(stderr, "done running frames\n");
		}

fprintf(stderr, "out of scope\n");
		complete=TRUE;
	}
	catch(Exception &e) { e.log(); }
	catch(...)			{ feLog("uncaught exception\n"); }

	UNIT_TEST(complete);
	UNIT_TEST(!Counted::trackerCount());
	UNIT_TRACK(4);
	UNIT_RETURN();
fprintf(stderr, "totally done except C++ teardown\n");
}
