/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __terrain_Strata_h__
#define __terrain_Strata_h__

FE_ATTRIBUTE("surf:strataName",	"Name of StrataI Component");
FE_ATTRIBUTE("surf:strataI",	"Instance of StrataI Component");
namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Strata RecordView

	@ingroup terrain
*//***************************************************************************/
class FE_DL_EXPORT Strata: public RecordView
{
	public:
		Functor<String>				strataName;
		Functor< cp<Component> >	strataComponent;
		Functor< sp<RecordGroup> >	stratumGroup;

				Strata(void)			{ setName("Strata"); }
virtual	void	addFunctors(void)
				{
					add(strataName,			FE_USE("surf:strataName"));
					add(strataComponent,	FE_USE("surf:strataI"));
					add(stratumGroup,		FE_SPEC("surf:stratumGrp",
							"RecordGroup of Stratum"));
				}
virtual	void	initializeRecord(void)
				{
					RecordView::initializeRecord();

					strataComponent.attribute()->setSerialize(FALSE);

					feLog("Strata::initializeRecord %s\n",name().c_str());
				}
virtual	void	finalizeRecord(void)
				{
					RecordView::finalizeRecord();

					feLog("Strata::finalizeRecord %s %s\n",name().c_str(),
							strataName().c_str());

					if(!strataName().empty())
					{
						strataComponent.createAndSetComponent(strataName());
					}
				}
};

} /* namespace ext */
} /* namespace fe */

#endif /* __terrain_Strata_h__ */

