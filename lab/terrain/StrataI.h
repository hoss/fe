/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __terrain_StrataI_h__
#define __terrain_StrataI_h__
namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Layered environment delimited by surfaces

	@ingroup terrain

	TODO solid surface contacts
*//***************************************************************************/
class FE_DL_EXPORT StrataI: virtual public Component
{
	public:
virtual void			addStratum(sp<StratumI> spStratumI)					=0;
virtual void			removeStratum(sp<StratumI> spStratumI)				=0;
virtual U32				numStratum(void)									=0;
virtual sp<StratumI>	stratum(U32 index)									=0;

virtual void			setTime(Real time)									=0;

						/** @brief Discover the burdens of the world

							Drag and  buoyancys are likely candidates. */
virtual void			calcInfluence(SpatialVector& rForce,Real radius,
								SpatialVector center,
								SpatialVector velocity)						=0;

						/// @brief Adjust location and velocity if colliding
virtual	void			impact(SpatialVector& center,SpatialVector& velocity,
								Real mass,Real radius,Real deltaT)			=0;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __terrain_StrataI_h__ */
