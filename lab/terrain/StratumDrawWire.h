/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __terrain_StratumDrawWire_h__
#define __terrain_StratumDrawWire_h__
namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Draw a terrain using wireframe

	@ingroup terrain
*//***************************************************************************/
class FE_DL_EXPORT StratumDrawWire: virtual public StratumDrawPoly
{
	public:
					StratumDrawWire(void)									{}
virtual				~StratumDrawWire(void)									{}

	protected:
virtual	void		drawSurface(void);
};

} /* namespace ext */
} /* namespace fe */

#endif /* __terrain_StratumDrawWire_h__ */
