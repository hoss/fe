/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <terrain/terrain.pmh>

namespace fe
{
namespace ext
{

void StratumDrawWire::drawSurface(void)
{
	const Color red(1.0f,0.0f,0.0f);

	const BWORD multicolor=true;

	for(U32 pass=0;pass<2;pass++)
	{
		for(U32 y=0;y<m_vertices;y++)
		{
			for(U32 x=0;x<m_vertices;x++)
			{
				if(pass)
				{
					m_pColor[x]=m_pFieldColor[x*m_vertices+y];
					m_pVertex[x]=m_pFieldVert[x*m_vertices+y];
				}
				else
				{
					m_pColor[x]=m_pFieldColor[y*m_vertices+x];
					m_pVertex[x]=m_pFieldVert[y*m_vertices+x];
#if FALSE
					SpatialVector normal[2];
					normal[0]=m_pVertex[x];
					normal[1]=m_pVertex[x]+m_pFieldNorm[y*m_vertices+x];
					m_spDrawI->drawLines(normal,NULL,2,DrawI::e_strip,
							FALSE,&red);
#endif
				}
			}
			m_spDrawI->drawLines(m_pVertex,NULL,m_vertices,DrawI::e_strip,
					multicolor,m_pColor);
		}
	}
}

} /* namespace ext */
} /* namespace fe */