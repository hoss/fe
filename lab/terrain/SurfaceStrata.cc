/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <terrain/terrain.pmh>

namespace fe
{
namespace ext
{

SurfaceStrata::SurfaceStrata(void)
{
	feLog("SurfaceStrata()\n");
}

void SurfaceStrata::initialize(void)
{
	feLog("SurfaceStrata:initialize\n");

	m_spStratumDrawI=registry()->create("StratumDrawI");
}

Protectable* SurfaceStrata::clone(Protectable* pInstance)
{
	feLog("SurfaceStrata::clone\n");

	SurfaceStrata* pSurfaceStrata= pInstance?
			fe_cast<SurfaceStrata>(pInstance): new SurfaceStrata();

	checkCache();

	SurfaceTriangles::clone(pSurfaceStrata);

	//* copy attributes

	// TODO may change, should copy
	pSurfaceStrata->m_spStratumDrawI=m_spStratumDrawI;
	pSurfaceStrata->m_spStrataI=m_spStrataI;

	return pSurfaceStrata;
}

void SurfaceStrata::cache(void)
{
	feLog("SurfaceStrata::cache\n");

	if(!m_record.isValid())
	{
		feLog("SurfaceStrata::cache record invalid\n");
		return;
	}

	// TODO prevent SurfaceTriangles from reading records

	SurfaceTriangles::cache();

	Strata strataRV;
	strataRV.bind(m_record);
	m_spStrataI=strataRV.strataComponent();

	sp<RecordGroup> stratumRG=strataRV.stratumGroup();
	FEASSERT(stratumRG.isValid());

	U32 count=0;
	RecordArrayView<Stratum> stratumRAV;

	for(RecordGroup::iterator it=stratumRG->begin();it!=stratumRG->end();it++)
	{
		sp<RecordArray>& rspRA= *it;
		stratumRAV.bind(rspRA);

		if(!stratumRAV.recordView().phase.check(rspRA))
		{
			continue;
		}

		for(Stratum& stratumRV: stratumRAV)
		{
			sp<StratumI> spStratumI=stratumRV.component();
			feLog("SurfaceStrata::cache %d scale %.6G\n",count++,
					spStratumI->getScale());
			m_spStrataI->addStratum(spStratumI);

			// HACK hardcoded
			const Real widthx= 50.0;
			const Real widthy= 50.0;
			const U32 vertx= 7;//4 11 31;
			const U32 verty= 7;//4 11 31;
			const Real startx= -0.5*widthx;
			const Real starty= -0.5*widthy;
			const Real incx= widthx/F32(vertx-1);
			const Real incy= widthy/F32(verty-1);
			SpatialVector pVertex[vertx*verty];
			SpatialVector pNormal[vertx*verty];

			spStratumI->sample(startx,starty,incx,incy,vertx,verty,
					pVertex,pNormal);

			for(U32 vy=0;vy<verty-1;vy++)
			{
				const U32 vyy=vy*vertx;
				for(U32 vx=0;vx<vertx-1;vx++)
				{
//					feLog("SurfaceStrata::cache %p %d %d\n",
//							spStratumI.raw(),vx,vy);
					const U32 vyx=vyy+vx;
					m_pVertexArray=(SpatialVector*)reallocate(m_pVertexArray,
							(m_vertices+6)*sizeof(SpatialVector));
					m_pNormalArray=(SpatialVector*)reallocate(m_pNormalArray,
							(m_vertices+6)*sizeof(SpatialVector));

					m_pVertexArray[m_vertices]=pVertex[vyx];
					m_pNormalArray[m_vertices]=pNormal[vyx];
					m_pVertexArray[m_vertices+1]=pVertex[vyx+verty+1];
					m_pNormalArray[m_vertices+1]=pNormal[vyx+verty+1];
					m_pVertexArray[m_vertices+2]=pVertex[vyx+verty];
					m_pNormalArray[m_vertices+2]=pNormal[vyx+verty];
					m_pVertexArray[m_vertices+3]=pVertex[vyx];
					m_pNormalArray[m_vertices+3]=pNormal[vyx];
					m_pVertexArray[m_vertices+4]=pVertex[vyx+1];
					m_pNormalArray[m_vertices+4]=pNormal[vyx+1];
					m_pVertexArray[m_vertices+5]=pVertex[vyx+verty+1];
					m_pNormalArray[m_vertices+5]=pNormal[vyx+verty+1];

					m_vertices+=6;
				}
			}
		}
	}
}

SpatialTransform SurfaceStrata::sample(Vector2 a_uv) const
{
	Real u=a_uv[0];
	if(u<0.0f)
	{
		u=0.0f;
	}
	else if(u>1.0f)
	{
		u=1.0f;
	}

	SpatialTransform result;

	return result;
}

void SurfaceStrata::draw(sp<DrawI> a_spDrawI,const Color*) const
{
#if TRUE
	// bypass specialized draw
	SurfaceTriangles::draw(a_spDrawI,NULL);
	return;
#endif

	const U32 count=m_spStrataI->numStratum();
	if(!count)
	{
		return;
	}

	m_spStratumDrawI->bind(a_spDrawI);
	for(U32 m=0;m<count;m++)
	{
		m_spStratumDrawI->draw(m_spStrataI->stratum(m));
	}
}

} /* namespace ext */
} /* namespace fe */
