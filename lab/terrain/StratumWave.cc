/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <terrain/terrain.pmh>

namespace fe
{
namespace ext
{

Real StratumWave::height(Real x,Real y) const
{
	const Real currentOffset=offset();
	const Real currentScale=scale();
	const Real currentTime=m_time;

	return currentOffset+
			currentScale*(sinf(currentTime+x)+sinf(currentTime+y));
}

SpatialVector StratumWave::normal(Real x,Real y) const
{
	const Real currentScale=scale();
	const Real currentTime=m_time;

	return fe::unit(SpatialVector(-currentScale*cosf(currentTime+x),
			-currentScale*cosf(currentTime+y),1.0f));
}

void StratumWave::sample(Real startx,Real starty,Real incx,Real incy,
		U32 vertx,U32 verty,SpatialVector* pVertex,SpatialVector* pNormal)
{
	const Real currentOffset=offset();
	const Real currentScale=scale();
	const Real currentTime=m_time;

	Real* sinx=(vertx>WAVE_SAMPLES)? new Real[vertx]: m_sinxBuffer;
	Real* siny=(verty>WAVE_SAMPLES)? new Real[verty]: m_sinyBuffer;
	Real* cosx=(vertx>WAVE_SAMPLES)? new Real[vertx]: m_cosxBuffer;
	Real* cosy=(verty>WAVE_SAMPLES)? new Real[verty]: m_cosyBuffer;

	for(U32 x=0;x<vertx;x++)
	{
		const Real xx=startx+incx*x;
		const Real yy=starty+incy*x;

		sinx[x]=currentScale*sinf(currentTime+xx);
		siny[x]=currentScale*sinf(currentTime+yy);
		cosx[x]=currentScale*cosf(currentTime+xx);
		cosy[x]=currentScale*cosf(currentTime+yy);
	}

	for(U32 y=0;y<verty;y++)
	{
		const U32 yx=y*vertx;
		const Real yy=starty+incy*y;
		for(U32 x=0;x<vertx;x++)
		{
			const U32 yxx=yx+x;
			const Real xx=startx+incx*x;

			set(pVertex[yxx],xx,yy,currentOffset+sinx[x]+siny[y]);
			set(pNormal[yxx],-cosx[x],-cosy[y],1.0f);
			normalize(pNormal[yxx]);
		}
	}

	if(sinx!=m_sinxBuffer)
	{
		delete[] sinx;
		delete[] cosx;
	}
	if(siny!=m_sinyBuffer)
	{
		delete[] siny;
		delete[] cosy;
	}
}

} /* namespace ext */
} /* namespace fe */