/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __terrain_StratumWave_h__
#define __terrain_StratumWave_h__
namespace fe
{
namespace ext
{

#define WAVE_SAMPLES		100	//* reusable space vs new()

/**************************************************************************//**
	@brief Elevation using waves

	@ingroup terrain
*//***************************************************************************/
class FE_DL_EXPORT StratumWave: public StratumPlane
{
	public:
						StratumWave(void)									{}
virtual					~StratumWave(void)									{}

						//* As StratumI
virtual Real			height(Real x,Real y) const;
virtual SpatialVector	normal(Real x,Real y) const;

virtual	void			sample(Real startx,Real starty,Real incx,Real incy,
								U32 vertx,U32 verty,
								SpatialVector* pVertex,SpatialVector* pNormal);
	private:
		Real		m_sinxBuffer[WAVE_SAMPLES];
		Real		m_sinyBuffer[WAVE_SAMPLES];
		Real		m_cosxBuffer[WAVE_SAMPLES];
		Real		m_cosyBuffer[WAVE_SAMPLES];
};

} /* namespace ext */
} /* namespace fe */

#endif /* __terrain_StratumWave_h__ */
