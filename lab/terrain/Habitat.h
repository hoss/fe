/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __terrain_Habitat_h__
#define __terrain_Habitat_h__
namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief A place to make a home

	@ingroup terrain
*//***************************************************************************/
class FE_DL_EXPORT Habitat: virtual public StrataI
{
	public:
virtual void			addStratum(sp<StratumI> spStratumI)
						{	m_layerList.append(spStratumI); }
virtual void			removeStratum(sp<StratumI> spStratumI)
						{	m_layerList.remove(spStratumI); }
virtual	U32				numStratum(void)		{ return m_layerList.size(); }
virtual	sp<StratumI>	stratum(U32 index)		{ return m_layerList[index]; }

virtual void			setTime(Real time);

virtual void			calcInfluence(SpatialVector& rForce,
								Real radius,SpatialVector center,
								SpatialVector velocity);
virtual	void			impact(SpatialVector& center,SpatialVector& velocity,
								Real mass,Real radius,Real deltaT);

	private:
		List< sp<StratumI> >	m_layerList;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __terrain_Habitat_h__ */
