/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <terrain/terrain.pmh>

#define FE_SDP_DRAW_PROBES	FALSE

namespace fe
{
namespace ext
{

StratumDrawPoly::StratumDrawPoly(void):
		m_size(32.0f),
		m_vertices(32),
		m_probeStep(4)
{
	set(m_center);
	const U32 size=(m_vertices+1)*(m_vertices+1);
	m_pFieldVert=new SpatialVector[size];
	m_pFieldNorm=new SpatialVector[size];
	m_pFieldColor=new Color[size];

	m_pVertex=new SpatialVector[2*m_vertices];
	m_pNormal=new SpatialVector[2*m_vertices];
	m_pColor=new Color[2*m_vertices];

#if FE_SDP_DRAW_PROBES
	m_spWireframe=new DrawMode();
	m_spWireframe->setDrawStyle(DrawMode::e_wireframe);
#endif
}

StratumDrawPoly::~StratumDrawPoly(void)
{
	delete[] m_pFieldVert;
	delete[] m_pFieldNorm;
	delete[] m_pFieldColor;

	delete[] m_pVertex;
	delete[] m_pNormal;
	delete[] m_pColor;
}

void StratumDrawPoly::initialize(void)
{
	m_spSelector=registry()->create("SelectorI");
	m_spProbeGroup=new RecordGroup();
}

void StratumDrawPoly::assignVisibility(sp<RecordGroup>& rspRecordGroup)
{
	if(m_spVisibilityGroup==rspRecordGroup)
	{
		return;
	}

	m_spVisibilityGroup=rspRecordGroup;

	RecordGroup::iterator it=rspRecordGroup->begin();
	if(it!=rspRecordGroup->end())
	{
		sp<RecordArray> spRA= *it;
		sp<Scope> spScope=spRA->layout()->scope();

		if(m_spScope!=spScope)
		{
			m_spProbeGroup->clear();

			m_spScope=spScope;

			m_selectOp.bind(spScope);
			m_selectOp.createRecord();

			const Real inc=0.5*m_size*m_probeStep/Real(m_vertices);
			const U32 probewidth=m_vertices/m_probeStep+1;
			const U32 probes=probewidth*probewidth;

			m_sphereRAV.bind(spScope);
			sp<RecordArray> spRA=spScope->createRecordArray(
					m_sphereRAV.recordView().layout(),probes);

			for(U32 m=0;m<probes;m++)
			{
				Record probe=spRA->getRecord(m);
				m_sphereRAV.recordView().radius(probe)=inc;

				m_spProbeGroup->add(probe);
			}
		}
	}

	if(!m_spScope.isValid())
	{
		m_spVisibilityGroup=NULL;
	}

	//* should be exactly one RecordArray
	it=m_spProbeGroup->begin();
	if(it!=m_spProbeGroup->end())
	{
		sp<RecordArray> spRA= *it;
		m_sphereRAV.bind(spRA);
	}
	else
	{
		feX("StratumDrawPoly::assignVisibility",
					"failed to create proper m_spProbeGroup\n");
	}
}

void StratumDrawPoly::draw(const sp<StratumI>& rspStratumI)
{
	const Color darkgrey(0.3f,0.3f,0.3f,0.3f);
	const Color lightgrey(0.7f,0.7f,0.7f,0.1f);
	const Color white(1.0f,1.0f,1.0f,1.0f);
	const Color blue(0.0f,0.0f,1.0f);
	const Color brown(0.5f,0.3f,0.0f);

	const Real inc=m_size/Real(m_vertices);
	const I32 grain=I32(inc*m_probeStep);
	const Real xs=(m_center[0]+0.5f)/grain*grain-0.5f*m_size;
	const Real ys=(m_center[1]+0.5f)/grain*grain-0.5f*m_size;

	BWORD select=m_spVisibilityGroup.isValid();

	rspStratumI->sample(xs,ys,inc,inc,m_vertices+1,m_vertices+1,
			m_pFieldVert,m_pFieldNorm);

	Color color;
	Real alpha=1.0f;
	switch(rspStratumI->getPhase())
	{
		case StratumI::e_gas:
			color=lightgrey;
			alpha=0.1f;
			select=FALSE;
			break;
		case StratumI::e_liquid:
			color=blue;
			alpha=0.5f;
			break;
		case StratumI::e_solid:
			color=brown;
			break;
	}

	color[3]=alpha;

	const U32 count=m_vertices*m_vertices;
	const U32 probewidth=m_vertices/m_probeStep+1;
	const U32 probes=probewidth*probewidth;
	for(U32 m=0;m<count;m++)
	{
		m_pFieldColor[m]=color;
	}

	if(select)
	{
		for(U32 m=0;m<probes;m++)
		{
			const U32 gridx=(m%probewidth)*m_probeStep;
			const U32 gridy=(m/probewidth)*m_probeStep;
			m_sphereRAV[m].location()=m_pFieldVert[gridy*(m_vertices+1)+gridx];

		}

		const BWORD cumulative=TRUE;
		const sp<FilterI> spFilter;
		m_spSelector->configure(m_spVisibilityGroup,spFilter,cumulative);

		m_selectOp.input()=m_spProbeGroup;
		sp<HandlerI> spHandlerI=m_spSelector;
		FEASSERT(spHandlerI.isValid());

		Record record=m_selectOp.record();
		spHandlerI->handle(record);

#if FE_SDP_DRAW_PROBES
		for(U32 m=0;m<probes;m++)
		{
			Sphere& sphereRV=m_sphereRAV[m];
			Real radius=sphereRV.radius();
			SpatialVector scale(radius,radius,radius);
			SpatialTransform transform;
			setIdentity(transform);
			setTranslation(transform,sphereRV.location());
			m_spDrawI->setDrawMode(m_spWireframe);
			m_spDrawI->drawSphere(transform,&scale,lightgrey);
			char text[256];
			sprintf(text,"%.2G",sphereRV.picked());
			m_spDrawI->drawAlignedText(sphereRV.location(),text,white);
			m_spDrawI->setDrawMode(sp<DrawMode>(NULL));
		}
#endif

		for(U32 y=0;y<probewidth-1;y++)
		{
			U32 index1=y*probewidth;
			U32 index2=index1+probewidth;

			Real pick_1a=m_sphereRAV[index1++].picked();
			Real pick_2a=m_sphereRAV[index2++].picked();

			for(U32 x=0;x<probewidth-1;x++)
			{
				Real pick_1b=m_sphereRAV[index1++].picked();
				Real pick_2b=m_sphereRAV[index2++].picked();

				for(U32 yy=0;yy<m_probeStep;yy++)
				{
					U32 yxx=(y*m_probeStep+yy)*(m_vertices+1)+x*m_probeStep;

					const Real ratioy=yy/Real(m_probeStep);
					for(U32 xx=0;xx<m_probeStep;xx++)
					{
						const Real ratiox=xx/Real(m_probeStep);

						const Real sum1=pick_1a*(1.0f-ratiox)+pick_1b*ratiox;
						const Real sum2=pick_2a*(1.0f-ratiox)+pick_2b*ratiox;
						const Real scale=sum1*(1.0f-ratioy)+sum2*ratioy;

						Color& rColor=m_pFieldColor[yxx++];
						const Real alpha=rColor[3];
						rColor*=0.3f+0.7f*scale;
						rColor[3]=alpha;

//						feLog(" %d %s\n",yxx,c_print(rColor));
					}
				}
//				feLog("StratumDrawPoly %d %d  %d %d\n",x,y,index1,index2);
				pick_1a=pick_1b;
				pick_2a=pick_2b;
			}
		}
	}

	drawSurface();
}

void StratumDrawPoly::drawSurface(void)
{
	const BWORD multicolor=true;

	for(U32 y=0;y<m_vertices-1;y++)
	{
		const U32 yx=y*(m_vertices+1);
		for(U32 x=0;x<m_vertices;x++)
		{
			const U32 x2=x*2;
			const U32 x2p=x2+1;
			const U32 yxx=yx+x;
			const U32 yxxv=yxx+m_vertices+1;

			m_pColor[x2]=m_pFieldColor[yxxv];
			m_pVertex[x2]=m_pFieldVert[yxxv];
			m_pNormal[x2]=m_pFieldNorm[yxxv];

			m_pColor[x2p]=m_pFieldColor[yxx];
			m_pVertex[x2p]=m_pFieldVert[yxx];
			m_pNormal[x2p]=m_pFieldNorm[yxx];
		}
		m_spDrawI->drawTriangles(m_pVertex,m_pNormal,NULL,m_vertices*2,
				DrawI::e_strip,multicolor,m_pColor);
	}
}

} /* namespace ext */
} /* namespace fe */
