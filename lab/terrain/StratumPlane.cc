/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <terrain/terrain.pmh>

namespace fe
{
namespace ext
{

void StratumPlane::sample(Real startx,Real starty,Real incx,Real incy,
		U32 vertx,U32 verty,SpatialVector* pVertex,SpatialVector* pNormal)
{
	for(U32 y=0;y<verty;y++)
	{
		const U32 yx=y*vertx;
		const Real yy=starty+incy*y;
		for(U32 x=0;x<vertx;x++)
		{
			const U32 yxx=yx+x;
			const Real xx=startx+incx*x;

			set(pVertex[yxx],xx,yy,height(xx,yy));
			pNormal[yxx]=normal(xx,yy);
		}
	}
}

} /* namespace ext */
} /* namespace fe */