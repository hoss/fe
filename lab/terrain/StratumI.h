/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __terrain_StratumI_h__
#define __terrain_StratumI_h__
namespace fe
{
namespace ext
{

/**************************************************************************//**
	@brief Elevation at location

	@ingroup terrain

	TODO a deformable phase, like sand
*//***************************************************************************/
class FE_DL_EXPORT StratumI: virtual public Component
{
	public:
		enum Phase
		{
			e_solid,	// fixed collidable
			e_liquid,	// non-compressible
			e_gas		// compressible gradiant
		};

virtual Phase			getPhase(void) const								=0;
virtual void			setPhase(Phase phase)								=0;

virtual Real			getDensity(void) const								=0;
virtual void			setDensity(Real density)							=0;

virtual Real			getOffset(void) const								=0;
virtual void			setOffset(Real offset)								=0;

virtual Real			getScale(void) const								=0;
virtual void			setScale(Real scale)								=0;

virtual void			setTime(Real time)									=0;

virtual Real			height(Real x,Real y) const							=0;
virtual SpatialVector	normal(Real x,Real y) const							=0;
virtual	void			sample(Real startx,Real starty,Real incx,Real incy,
								U32 vertx,U32 verty,SpatialVector* pVertex,
								SpatialVector* pNormal)						=0;
};

} /* namespace ext */
} /* namespace fe */

#endif /* __terrain_StratumI_h__ */
