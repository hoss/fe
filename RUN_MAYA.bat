echo off
setlocal

set AUTODESK=c:\Program Files\Autodesk

:: auto-find the highest numbered version
for /f "usebackq delims=|" %%f in (`dir /b "%AUTODESK%\Maya*" ^| findstr "[0-9]*"`) do set MAYA_SUBDIR=%%f
set MAYA_VERSION=%MAYA_SUBDIR:~4%
echo found Maya version: %MAYA_VERSION%

set MAYA_BIN=%AUTODESK%\Maya%MAYA_VERSION%\bin
if exist "%MAYA_BIN%" goto found_maya
    echo could not verify install for Maya
	echo %MAYA_BIN%
    pause
    exit /b
:found_maya

set FE_ROOT=%cd%

if exist "%FE_ROOT%\lib\x86_win64_optimize\maya%MAYA_VERSION%\scripts" goto picked_root
	set FE_ROOT=%USERPROFILE%\scoop\apps\freeelectron\current\fe
if exist "%FE_ROOT%\lib\x86_win64_optimize\maya%MAYA_VERSION%\scripts" goto picked_root
    echo could not find valid install for freeelectron
    pause
    exit /b
:picked_root

echo using FE_ROOT as %FE_ROOT%

set MAYA_PLUG_IN_PATH=%MAYA_PLUG_IN_PATH%;%FE_ROOT%\lib\x86_win64_optimize\maya%MAYA_VERSION%
set MAYA_SCRIPT_PATH=%MAYA_PLUG_IN_PATH%\scripts
set XBMLANGPATH=%MAYA_PLUG_IN_PATH%\icons

set path=%PATH%;%AUTODESK%\Maya%MAYA_VERSION%\bin
set path=%PATH%;%FE_ROOT%\lib\x86_win64_optimize

maya

endlocal
