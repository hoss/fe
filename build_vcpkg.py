#!/usr/bin/python3

import os
import sys
import subprocess
import re

# NOTE: The utility module enforces a python version check at import time.  So
# we import it as soon as possible, which causes an immediate abort if the
# check fails.  This is more implicit than is desired, but we can't directly
# import the version check method, since doing so just causes the module to
# experience import processing in full, so there's no way to isolate the check
# ourselves -- we just defer to the module doing it for us.  This could/would
# change if forge becomes a proper package in the future.
import utility

import pathlib

SUCCESS = 0
FAIL = 1

DIRECTIVES = [
    'install',
    'remove',
    'upload',
    'download',
]

# NOTE to select Visual Studio version for install,
# you have to hide the more recent versions (can rename directories)
# We are currently building with VS 2019, as 2022 builds can fail on 2019.

# NOTE we can still need to do updates/upgrades by hand.
# Maybe we need another directive or have install do more.

TOOLNAME = pathlib.Path(__file__).stem

# -----------------------------------------------------------------------------
def Build(directive):

	sys.stdin = open(0)

	try:
		_build(directive)
		print("build vcpkg completed")
		return SUCCESS
	except Exception as e:
		print("build vcpkg exception")
		print(e)
		return FAIL

# Internal use-only function.  External consumers of this module should use
# Build().
def _build(directive):

	if directive not in DIRECTIVES:
		print("%s: invalid directive '%s'" % (TOOLNAME, directive))
		print("Must be one of: {}.".format(DIRECTIVES))
		return

	packages = []

	packages += [ "czmq:x86-windows-static-md" ]
	packages += [ "czmq:x64-windows-static-md" ]

	packages += [ "eigen3:x86-windows-static-md" ]
	packages += [ "eigen3:x64-windows-static-md" ]

	packages += [ "enet:x86-windows-static-md" ]
	packages += [ "enet:x64-windows-static-md" ]

	packages += [ "freetype-gl:x86-windows-static-md" ]
	packages += [ "freetype-gl:x64-windows-static-md" ]

	packages += [ "glew:x86-windows-static-md" ]
	packages += [ "glew:x64-windows-static-md" ]

	# now building directly in the module
#	packages += [ "jsoncpp:x86-windows-static-md" ]
#	packages += [ "jsoncpp:x64-windows-static-md" ]

	packages += [ "libwebsockets:x86-windows-static-md" ]
	packages += [ "libwebsockets:x64-windows-static-md" ]

	packages += [ "openal-soft:x86-windows-static-md" ]
	packages += [ "openal-soft:x64-windows-static-md" ]

	packages += [ "opencl:x86-windows-static-md" ]
	packages += [ "opencl:x64-windows-static-md" ]

	packages += [ "opencv:x86-windows-static-md" ]
	packages += [ "opencv:x64-windows-static-md" ]

	packages += [ "rapidjson:x86-windows-static-md" ]
	packages += [ "rapidjson:x64-windows-static-md" ]

	packages += [ "sdl2:x86-windows-static-md" ]
	packages += [ "sdl2:x64-windows-static-md" ]

	# now building directly in the module
#	packages += [ "yaml-cpp:x86-windows-static-md" ]
#	packages += [ "yaml-cpp:x64-windows-static-md" ]

	(env, modset_roots) = utility.applyLocalEnvFiles()

	fe_vcpkg_upload_path = os.environ["FE_VCPKG_UPLOAD_PATH"]
	fe_vcpkg_download_url = os.environ["FE_VCPKG_DOWNLOAD_URL"]
	fe_vcpkg_download_stamp = os.environ["FE_VCPKG_DOWNLOAD_STAMP"]
	fe_vcpkg_download_google_id = os.environ["FE_VCPKG_DOWNLOAD_GOOGLE_ID"]
	fe_vcpkg_download_authenticated = \
		bool(int(os.environ["FE_VCPKG_DOWNLOAD_AUTHENTICATED"]))
	fe_vcpkg_install_path = os.path.normpath(os.environ["FE_VCPKG_INSTALL_PATH"])
	fe_vcpkg_install = os.path.normpath(os.environ["FE_VCPKG_INSTALL"])

	local_vcpkg = os.path.join(fe_vcpkg_install_path, "vcpkg")

	(drive, local_path) = fe_vcpkg_install_path.split(":")

	cd_command = drive + ": &&"
	cd_command += " cd " + local_path

	if directive == "upload" and not os.path.exists(local_vcpkg):
		utility.cprint(utility.RED,0,"vcpkg not installed in %s" % fe_vcpkg_install_path)
		return

	if directive == "upload":

		creds = utility.getUserCredentials(utility.Credential(password=False))

		command = cd_command + "/vcpkg &&"
		command += " vcpkg.exe export"
		command += " --zip " + " ".join(packages)

		utility.cprint(utility.CYAN,0,"Export packages to zip file")
		print(command)
		os.system(command)

		# NOTE presume listdir sorting will see latest file last
		zipFile = ""
		expression = re.compile(r"^vcpkg-export-20.*\.zip$")
		entries = os.listdir(local_vcpkg)
		for entry in entries:
			if expression.match(entry):
				zipFile = entry

		if zipFile == "":
			utility.cprint(utility.RED,0,"no zip file found")
			return

		command = cd_command + "/vcpkg &&"
		command += " scp " + zipFile
		command += " " + creds.user + "@" + fe_vcpkg_upload_path + "/"

		utility.cprint(utility.CYAN,0,"Copying zip file to server")
		print(command)
		os.system(command)

		print()
		return

	if directive == "download":

		import urllib.request
		import shutil

		creds = None
		password_mgr = None

		targetdir = "vcpkg-export-" + fe_vcpkg_download_stamp
		zipFile = targetdir + ".zip"
		url = fe_vcpkg_download_url + "/" + zipFile
		pathsuffix = os.path.normpath(drive + ":" + local_path)
		dirname = os.path.join(pathsuffix, targetdir)
		filename = os.path.join(pathsuffix, zipFile)

		if fe_vcpkg_download_google_id != "":
			command = 'C:\Windows\System32\curl.exe -s -L "https://drive.google.com/uc?export=download&id=' + fe_vcpkg_download_google_id + '"'
			popen = subprocess.Popen(command,bufsize=-1, shell=True,
					stdin=subprocess.PIPE, stdout=subprocess.PIPE)
			(html, p_err)=popen.communicate()

			if html:
				confirm = re.search(r"(confirm=[a-zA-Z0-9\-_]+)",html.decode("utf-8"))[1]

				if confirm:
					url = 'https://drive.google.com/uc?export=download&' + confirm + '&id=' + fe_vcpkg_download_google_id

		# Avoid redundant download processing.
		if os.path.isdir(dirname):
			utility.cprint(
				utility.CYAN, 0,
				"Local vcpkg with matching ID already exists: {}".format(
					dirname)
			)
			return

		if fe_vcpkg_download_authenticated:
			creds = utility.getUserCredentials()
			password_mgr = urllib.request.HTTPPasswordMgrWithDefaultRealm()
			password_mgr.add_password(
				None, fe_vcpkg_download_url, creds.user, creds.password)

		handler = urllib.request.HTTPBasicAuthHandler(password_mgr)
		opener = urllib.request.build_opener(handler)

		with opener.open(url) as inputObj:
			os.makedirs(os.path.dirname(filename), exist_ok=True)
			with open(filename, "wb") as outputObj:
				print()
				utility.cprint(
					utility.CYAN, 0, "Downloading %s to %s" % (url, filename))
				shutil.copyfileobj(inputObj, outputObj)

		command = cd_command + " &&"
		command += " tar xf %s" % zipFile

		print()
		utility.cprint(utility.CYAN,0,"Extracting binary packages")
		print(command)
		os.system(command)

		print()
		utility.cprint(utility.CYAN,0,"Removing downloaded zip file")
		os.remove(filename)

		downloaded_as = filename[:-4]
		if downloaded_as != fe_vcpkg_install:
			utility.cprint(utility.YELLOW,0,"WARNING not configured to use this download")
			utility.cprint(utility.YELLOW,0,"  just downloaded to '%s'" % downloaded_as)
			utility.cprint(utility.YELLOW,0,"  FE_VCPKG_INSTALL is set to '%s'" % fe_vcpkg_install)

		print()
		return

	if directive == "install" or directive == "remove":

		command = cd_command + "/vcpkg &&"
		command += " vcpkg.exe " + directive
		if directive == "install":
			command += " --clean-after-build"
		command += " " + " ".join(packages)
#		command += " --keep-going"
#		command += " --dry-run"

		utility.cprint(utility.CYAN,0,"Building packages")
		print(command)
		os.system(command)

		if directive == "install":
			installed_as = fe_vcpkg_install_path + r"\vcpkg"
			if installed_as != fe_vcpkg_install:
				utility.cprint(utility.YELLOW,0,"WARNING not configured to use this install")
				utility.cprint(utility.YELLOW,0,"  just installed to '%s'" % installed_as)
				utility.cprint(utility.YELLOW,0,"  FE_VCPKG_INSTALL is set to '%s'" % fe_vcpkg_install)


		print()
		return

	utility.cprint(utility.RED,0,"unknown vcpkg argument")

# -----------------------------------------------------------------------------
if __name__ == "__main__" :

	directive = sys.argv[1]
	sys.exit(Build(directive))

