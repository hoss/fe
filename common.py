# this forge file is split out from root.py so that projects built with forge
# can use this configuration information
import os
import sys
import string
import re
import imp

import utility

forge = sys.modules["forge"]

#forge.cprint(forge.YELLOW,0,'Free Electron Common forge.py support')

def platform_setup():
	if forge.api == "x86_linux" or forge.api == "x86_64_linux":
		forge.fe_os = 'FE_LINUX'
		forge.fe_osver = '2'
		forge.fe_compiler = 'FE_GNU'
		forge.fe_gl2d = 'FE_2D_X_GFX'
		forge.fe_gl3d = 'FE_3D_OPENGL'
		forge.fe_hw = 'FE_X86'
		forge.includemap['include'] = '/usr/X11R6/include'
#		forge.linkmap['linux'] = '-lm -lc -ldl'
#		forge.linkmap['linux'] += ' -lcrypt'
		forge.winlibs = '-L/usr/X11/lib -L/usr/X11R6/lib -lX11 -lXext -lXrender -lXi'
		if forge.api == "x86_64_linux":
			forge.winlibs = '-L/usr/X11R6/lib64 ' + forge.winlibs
		forge.gllibs = '-lGL'
		forge.gllibs += ' -lGLU'
#		forge.cppmap['checks'] = '-ansi'
#		forge.linkmap['checks'] = '-ansi'
		forge.cppmap['efficiency'] = '-pipe'
		forge.linkmap['efficiency'] = '-pipe'
		forge.cppmap['multithread'] = '-D_REENTRANT -D_POSIX_REENTRANT_FUNCTIONS -DZT_POSIX'
#		forge.linkmap['multithread'] = '-lpthread -lrt'
		forge.linkmap['rpath'] = "-Wl,-rpath='$ORIGIN/'"
		forge.cppmap['template-depth'] = '-ftemplate-depth-60'
		forge.cppmap['std'] = '-std=c++11'
		forge.cpp_11 = 1
		forge.cppmap['pthread'] = '-pthread'
		forge.cppmap['woe_return'] = '-Werror=return-type'
		if forge.compiler_brand == 'gnu':
			forge.cppmap['woe_conditionally'] = '-Werror=conditionally-supported'
		# error on any unused variable
#		forge.cppmap['woe_unused'] = '-Werror=unused'
		forge.cppmap['unused'] = '-Wunused'
		if "_GLIBCXX_USE_CXX11_ABI" in os.environ and os.environ["_GLIBCXX_USE_CXX11_ABI"] == "0":
			forge.cppmap['abi'] = '-D_GLIBCXX_USE_CXX11_ABI=0'
		if not forge.rtti:
			forge.cppmap['rtti'] = '-fno-rtti'
	elif forge.api == "ppc_linux":
		forge.fe_os = 'FE_LINUX'
		forge.fe_osver = '2'
		forge.fe_compiler = 'FE_GNU'
		forge.fe_gl2d = 'FE_2D_X_GFX'
		forge.fe_gl3d = 'FE_3D_OPENGL'
		forge.fe_hw = 'FE_PPC'
		forge.includemap['include'] = '/usr/X11R6/include'
		forge.linkmap['linux'] = '-lm -lc -ldl'
#		forge.linkmap['linux'] += ' -lcrypt'
		forge.winlibs = '-L/usr/X11/lib -L/usr/X11R6/lib -lX11 -lXext'
		forge.gllibs = '-lGL'
		forge.gllibs += ' -lGLU'
#		forge.cppmap['checks'] = '-ansi'
#		forge.linkmap['checks'] = '-ansi'
		forge.cppmap['efficiency'] = '-pipe'
		forge.linkmap['efficiency'] = '-pipe'
		forge.cppmap['multithread'] = '-D_REENTRANT -D_POSIX_REENTRANT_FUNCTIONS -DZT_POSIX'
		forge.linkmap['multithread'] = '-lpthread -lrt'
		forge.cppmap['template-depth'] = '-ftemplate-depth-60'
	elif forge.api[-4:] == "_osx":
		forge.fe_os = 'FE_OSX'
		forge.fe_osver = '10'
		forge.fe_compiler = 'FE_GNU'
		forge.fe_gl2d = 'FE_2D_OSX'
#		forge.fe_gl2d = 'FE_2D_X_GFX'
		forge.fe_gl3d = 'FE_3D_OPENGL'
		if forge.api == "ppc_osx":
			forge.fe_hw = 'FE_PPC'
		elif forge.api == "x86_osx" or forge.api == "x86_64_osx":
			forge.fe_hw = 'FE_X86'
		elif forge.api == "arm64_osx":
			forge.fe_hw = 'FE_ARM'
		else:
			forge.fe_hw = '<unknown>'
#		forge.includemap['x11'] = '/usr/X11R6/include'
		forge.includemap['homebrew'] = '/opt/homebrew/include'
		forge.linkmap['homebrew'] = '-L/opt/homebrew/lib'
		forge.linkmap['osx'] = '-lm -lc -ldl'
#		forge.winlibs = '-L/usr/X11R6/lib -lX11 -lXext'
#		forge.gllibs = '-framework OpenGL -framework GLUT'
		forge.gllibs += ' -lGL -lGLU'
#		forge.cppmap['checks'] = '-ansi'
#		forge.linkmap['checks'] = '-ansi'
		forge.cppmap['efficiency'] = '-pipe'
		forge.linkmap['efficiency'] = '-pipe'
		forge.cppmap['multithread'] = '-D_REENTRANT -D_POSIX_REENTRANT_FUNCTIONS -DZT_POSIX'
		forge.linkmap['multithread'] = '-lpthread'
		forge.linkmap['undefined'] = '-undefined dynamic_lookup'
		forge.cppmap['template-depth'] = '-ftemplate-depth-60'
		forge.cppmap['std'] = '-std=c++11'
		if not forge.rtti:
			forge.cppmap['rtti'] = '-fno-rtti'
	elif forge.api == "x86_cygwin":
		forge.fe_os = 'FE_CYGWIN'
		forge.fe_osver = '2'
		forge.fe_compiler = 'FE_GNU'
		forge.fe_gl2d = 'FE_2D_X_GFX'
		forge.fe_gl3d = 'FE_3D_OPENGL'
		forge.fe_hw = 'FE_X86'
		forge.includemap['include'] = '/usr/X11R6/include'
		forge.linkmap['linux'] = '-lm -lc  -ldl'
#		forge.linkmap['linux'] += ' -lcrypt'
		forge.winlibs = '-L/usr/X11/lib -L/usr/X11R6/lib -lX11 -lXext'
		forge.gllibs = '-lGL -lGLU'
		forge.cppmap['multithread'] = ' -DREENTRANT -DZT_POSIX'
		forge.linkmap['multithread'] = '-lpthread -lrt'

	elif forge.api == "x86_win32" or forge.api == "x86_win64":
		if forge.api == "x86_win64":
			forge.fe_os = 'FE_WIN64'
		else:
			forge.fe_os = 'FE_WIN32'
		forge.fe_osver = '4'
		forge.fe_compiler = 'FE_MICROSOFT'
		forge.fe_gl2d = 'FE_2D_GDI'
		forge.fe_gl3d = 'FE_3D_OPENGL'
		forge.fe_hw = 'FE_X86'
		forge.linkmap['win32'] = 'kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib winmm.lib ws2_32.lib'
		forge.winlibs = ''
		forge.gllibs = 'opengl32.lib'
		forge.gllibs += ' glu32.lib'
#		forge.gllibs += ' glew32s.lib'
		if forge.codegen == 'debug':
			forge.gllibs += ' glew32d.lib'
		else:
			forge.gllibs += ' glew32.lib'
		if forge.pretty < 2:
			forge.cprint(forge.YELLOW,0,'downgrading warning level from /W3 to /W1 due to boost slop')
		forge.cppmap['win32_warning'] = '/W1'
		forge.cpp_11 = 1
		if forge.rtti:
			forge.cppmap['rtti'] = '/GR'
		else:
			forge.cppmap['rtti'] = '/GR-'
		forge.cppmap['woe_return'] = '/we4716'
		if forge.alignment:
			forge.cppmap['codegen'] += ' /Zp' + forge.alignment

def compiler_setup():
	compiler = os.getenv('FE_CC')
	if compiler:
		compiler_base = os.path.basename(compiler)
#		forge.cxx = compiler		overwrite setting from forge.py
		if compiler_base == 'icc':
			forge.fe_compiler = 'FE_INTEL'
		if compiler_base == 'dmc':
			forge.cxx = 'C:\\dm\\bin\\sc.exe'
			forge.fe_compiler = 'FE_DMC'

	if forge.compiler_brand == 'clang':
		forge.cppmap['pch-date-time'] = '-Wno-pch-date-time'
#		forge.cppmap['std'] = '-std=c++1z'
		forge.cppmap['std'] = '-std=c++11'
		forge.cppmap['template_depth'] = '-ftemplate-depth=1024'

		# really shouldn't hide these, but there are still a lot
		forge.cppmap['inconsistent-missing-override'] = '-Wno-inconsistent-missing-override'

		if "FE_CI_BUILD" in os.environ and os.environ["FE_CI_BUILD"] == "1":
#			forge.cppmap['unused-but-set-variable'] = '-Wno-unused-but-set-variable'
			forge.cppmap['unused-variable'] = '-Wno-unused-variable'

		# https://stackoverflow.com/questions/29293394/where-does-the-1-symbol-come-from-when-using-llvms-libc
		forge.cppmap['stdlib'] = '-stdlib=libc++'

		if forge.fe_os == 'FE_LINUX':

			# HACK for Unreal only
			forge.cppmap['std'] = '-std=c++17'
#			forge.cppmap['stdlib'] = '-stdlib=libstdc++'
#			forge.includemap['stdlib'] = '/opt/UnrealEngine/Engine/Extras/ThirdPartyNotUE/SDKs/HostLinux/Linux_x64/v20_clang-13.0.1-centos7/x86_64-unknown-linux-gnu/usr/include'
#			forge.linkmap['stdlib'] = '-L/opt/UnrealEngine/Engine/Extras/ThirdPartyNotUE/SDKs/HostLinux/Linux_x64/v20_clang-13.0.1-centos7/x86_64-unknown-linux-gnu/usr/lib'

	if forge.fe_compiler == 'FE_INTEL':
		forge.cppmap['codegen'] = ''
		if forge.codegen == 'debug':
			forge.cppmap['intel'] = '-g'
#		forge.linkmap['intel_cc'] = '-lcxa -lcprts -lunwind'
		forge.linkmap['intel_rpath'] = "-Wl,-rpath='" + os.getenv('IDB_HOME') + "/../../compiler/lib/intel64/'"

	if forge.fe_compiler == 'FE_MICROSOFT':
		if forge.codegen == 'debug':
			forge.cppmap['fe_codegen'] = '-DFE_CONSOLE_LOG'
		forge.linkmap['win32_no_cmt'] = '/NODEFAULTLIB:libcmt.lib /NODEFAULTLIB:libcmtd.lib'

	if forge.fe_compiler == 'FE_DMC':
		forge.cprint(forge.RED,1,'Digital Mars build incomplete. Awaiting full boost support')
		sys.exit(1)
		forge.objout = '-o'
		forge.exeout = '-o'
		forge.dlflags = ' -WD'
		if forge.codegen == 'debug':
			forge.cppmap['codegen'] = '-D_DEBUG -g'
			forge.dlflags += ' /DEBUG'
		elif forge.codegen == 'optimize':
			forge.cppmap['codegen'] = '-DNDEBUG'
		else:
			forge.cppmap['codegen'] = ''
		#forge.cppmap['codegen'] += ' -Aa -Ab -Ae -Ar -Aw -C -EC -Jb -Pz -3 -mn'
		forge.cppmap['codegen'] += ' -A- -Aa -Ae -Ar'
		forge.libincl = '-L'
		forge.nolink = '-c'
		forge.linker = 'c:\\dm\\bin\dmc.exe'
		forge.linkerflags = ''
		forge.ar = 'c:\\dm\\bin\\lib.exe'
		forge.arflags = '-p32'
		forge.aroutprefix = '-c '
		forge.objsuf = '.obj'
		forge.exesuf = '.exe'
		forge.libflag = ''
		forge.libpre = ''
		forge.libsuf = '.lib'
		forge.libdl = '.dll'
		forge.pymodsuf = '.pyd'
		forge.ld1 = ''
		forge.ld2 = ''
		forge.forcecxx = '-cpp'
		forge.fat1 = ''
		forge.fat2 = ''

		forge.winlibs = ''
		forge.gllibs = ''

		forge.cppmap['win32'] =''
		forge.cppmap['win32_warning'] = ''

		forge.includemap['stl'] = 'C:\dm\stlport\stlport'
		forge.linkmap['win32'] = 'kernel32.lib user32.lib gdi32.lib comdlg32.lib advapi32.lib ole32.lib oleaut32.lib uuid.lib'
		#forge.includemap['dmc'] = 'C:\dm\include'

	forge.gfxlibs = forge.winlibs + ' ' + forge.gllibs

def find_boost_include(path, collection):
	for boost in [ "boost", "hboost"]:
		collection_path = os.path.join(path, boost, collection)

		if os.path.exists(collection_path):
			return boost

	return ''

def find_boost_lib(lib_suffix, path, collection):
	for boost in [ "boost", "hboost"]:
		suffixes = [ '-mt', '' ]
		if lib_suffix != '':
			suffixes = [ lib_suffix ] + suffixes

		for suffix in suffixes:
			if os.path.exists(path + '/lib' + boost + '_' + collection + suffix + '.so'):
				return boost+ '_' + collection + suffix

		if forge.api == "x86_win32" or forge.api == "x86_win64":
			if forge.codegen == 'debug':
				tag = "mt-gd-x"
			else:
				tag = "mt-x"
			expression = re.compile("^lib" + boost + ".*" + collection + ".*" + tag + ".*\.lib$")
		else:
			expression = re.compile("^lib" + boost + ".*" + collection + ".*\.so$")

		dir_list = os.listdir(path)
		for lib in dir_list:
			if expression.match(lib):
				if forge.api == "x86_win32" or forge.api == "x86_win64":
					return lib
				abbreviated = lib.replace("lib" + boost,boost).replace(".so","")
				return abbreviated

	return ''

def find_tbb_lib(path):

	if os.path.exists(path + '/libtbb.so'):
		return 'tbb'

	return ''

def find_alembic_lib(path):

	if os.path.exists(path + '/libAlembicAbc.so'):
		return 'AlembicAbc'

	return ''

def find_alembic_static(path):

	if os.path.exists(path + '/libAlembicAbc.a'):
		return 'AlembicAbc'

	return ''

def find_alembic_combo(path):

	if os.path.exists(path + '/libAlembic.so'):
		return 'Alembic'

	if os.path.exists(path + '/libAlembic_sidefx.so'):
		return 'Alembic_sidefx'

	return ''

def get_alembic_version(path):

	alembic_version = ''

	header_path = path + '/Alembic/AbcCoreAbstract/Foundation.h'
	if os.path.exists(header_path):
		for line in open(header_path):
			if 'ALEMBIC_LIBRARY_VERSION ' in line:
				alembic_version = line.split(' ')[2].rstrip()
				break

	if alembic_version == '':
		header_path = path + '/Alembic/Util/Config.h'
		if os.path.exists(header_path):
			for line in open(header_path):
				if 'ALEMBIC_LIBRARY_VERSION ' in line:
					alembic_version = str(utility.safer_eval("".join(line.split(' ')[2:])))
					break

	return alembic_version

def find_extensions(modsetPath):

	candidates = forge.modules_found
	found = []

	for candidate in candidates:
		dir_path = os.path.join(modsetPath, candidate)
		if os.path.isdir(dir_path):
			bld_py_path = os.path.join(dir_path, 'bld.py')
			if os.path.exists(bld_py_path):
				found.append(candidate)

	return found

def print_module_build_status(module, name, color=forge.CYAN, dim=False, hide=[]):
	bold = 0
	if dim:
		bold = -1

	if not module and 'fail' not in hide:
		forge.color_on(bold, forge.RED)
		sys.stdout.write(' fail %s' % (name))
		forge.color_off()

	if module.ok != None and 'fail' not in hide:
		forge.color_on(bold, forge.RED)
		sys.stdout.write(' fail:%s' % (module.ok))
		forge.color_off()

	if module.hasDLL and 'dl' not in hide:
		forge.color_on(bold, color)
		sys.stdout.write(' dl')
		forge.color_off()

	if module.hasManifest and 'auto' not in hide:
		forge.color_on(bold, forge.MAGENTA)
		sys.stdout.write(' auto')
		forge.color_off()

	if module.hasYaml and 'yaml' not in hide:
		forge.color_on(bold, forge.MAGENTA)
		sys.stdout.write(' yaml')
		forge.color_off()

	if module.hasLib and 'lib' not in hide:
		forge.color_on(bold, color)
		sys.stdout.write(' lib')
		forge.color_off()

	if module.hasExe and 'exe' not in hide:
		forge.color_on(bold, color)
		sys.stdout.write(' exe')
		forge.color_off()

def build_extensions(module, extensions, suppress):

	module_message = {}
	extensions.sort()
	for extension in extensions:
		if not extension in suppress:
			child = module.Module(extension, True)
			if child:
				forge.module_dictionary[child.modName] = child
				if child.ok == None:
					forge.dot_modules_confirmed.append(child.modName)
				else:
					forge.modules_failed.append(child.modName.split('.')[-1])
					module_message[child.modName] = child.ok

	recheck = True
	while recheck:
		recheck = False

		forge.modules_confirmed = []

		for confirmed in forge.dot_modules_confirmed:
			forge.modules_confirmed += [ confirmed.split('.')[-1] ]

		for confirmed in forge.dot_modules_confirmed:
			if not confirmed in forge.module_dictionary:
				continue

			subModule = forge.module_dictionary[confirmed]
			if subModule == None:
				continue

			try:
				(file, pathname, description) = imp.find_module('bld', [ subModule.modPath ])
			except ImportError:
				continue

			pythons_modname = 'forge.' + subModule.modName
			py_mod = imp.load_module(pythons_modname,file,pathname,description)
			if hasattr(py_mod, "prerequisites"):
				prerequisites = py_mod.prerequisites()
				for prerequisite in prerequisites:
					if not prerequisite in forge.dot_modules_confirmed and not prerequisite in forge.modules_confirmed:
						forge.cprint(forge.YELLOW,0,"extension " + subModule.modName + " requires " + " ".join(prerequisites))
						forge.dot_modules_confirmed.remove(confirmed)
						forge.modules_confirmed.remove(confirmed.split('.')[-1])
						recheck = True
						break

	for extension in extensions:

		modName = ".".join([module.modName,extension])

		forge.color_on(0, forge.CYAN)
		sys.stdout.write('(')
		forge.color_off()
		if not modName in forge.dot_modules_confirmed:
			forge.color_on(1, forge.CYAN)
			sys.stdout.write('%s' % (extension))
			forge.color_on(1, forge.RED)
			if modName in module_message and module_message[modName] != None:
				sys.stdout.write(' fail:' + str(module_message[modName]))
			else:
				sys.stdout.write(' removed')
			forge.color_off()
		elif extension in suppress:
			forge.color_on(1, forge.YELLOW)
			sys.stdout.write('%s' % (extension))
			forge.color_off()
		elif extension in forge.modules_created:
			forge.color_on(1, forge.CYAN)
			sys.stdout.write('%s' % (extension))
			forge.color_on(1, forge.RED)
			sys.stdout.write(' dup')
			forge.color_off()
		else:
			forge.color_on(1, forge.CYAN)
			sys.stdout.write('%s' % (extension))
			forge.color_off()

			child = module.Module(extension)
			forge.modules_created.append(extension)

			child.AddManifest()
			child.AddYaml()

			print_module_build_status(child, extension)

			# Print info for test modules too.
			for dep in child.deps:
				# Check if it's a test module for this child module.
				if isinstance(dep, forge.Module) and (dep.modName == child.modName + ".test"):
					forge.color_on(-1, forge.CYAN)
					sys.stdout.write('|test')
					forge.color_off()

					dep.AddManifest()
					dep.AddYaml()

					print_module_build_status(dep, "test", dim=True, hide=['exe']) # exe is implied in tests
					# We don't need to keep searching if we already found the test module.
					break

		forge.color_on(0, forge.CYAN)
		sys.stdout.write(') ')
		forge.color_off()

	forge.color_off()
	sys.stdout.write('\n')

def defaultSetup(module, adjective, title):
	forge.includemap[module.modName] = module.modPath

	extension_list = find_extensions(module.modPath)

	forge.cprint(forge.CYAN,1, title + ': %d' % len(extension_list))

	if len(extension_list):
		build_extensions(module, extension_list, {})
	elif forge.pretty < 2:
		forge.cprint(forge.CYAN,0,
			'adjust the product list to specify ' + adjective + ' "' + module.modName + '" extensions to build')
