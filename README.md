![felogo](doc/image/FreeElectron.svg 'FE Logo'){: height="128px" width="320px"}

| Branch | CI/CD | Coverage |
| -------|:-----:|:--------:|
| main | [![pipeline status](https://gitlab.com/freeelectron_org/fe/badges/main/pipeline.svg)](https://gitlab.com/freeelectron_org/fe/-/commits/main) | [![coverage report](https://gitlab.com/freeelectron_org/fe/badges/main/coverage.svg)](https://gitlab.com/freeelectron_org/fe/-/commits/main) |
| dev | [![pipeline status](https://gitlab.com/freeelectron_org/fe/badges/dev/pipeline.svg)](https://gitlab.com/freeelectron_org/fe/-/commits/dev) | [![coverage report](https://gitlab.com/freeelectron_org/fe/badges/dev/coverage.svg)](https://gitlab.com/freeelectron_org/fe/-/commits/dev)

[![Latest Release](https://gitlab.com/freeelectron_org/fe/-/badges/release.svg)](https://gitlab.com/freeelectron_org/fe/-/releases)

# Documentation

Free Electron is an Open Source Extensible Architecture.

This readme is an intentionally brief introduction. Full project documentation
is available and maintained as a [doxygen set](http://freeelectron.org/doc/).

# Licensing

Free Electron may be used following the standard BSD 2-Clause License. A copy
should be included in this distribution. If not, please visit
[freeelectron.org](http://freeelectron.org).

All files included in this distribution are bound to this license, regardless
of whether each particular file mentions the license, except for files in which
an alternate license is explicitly stated.

Any files in the Free Electron repository that are not included in a specific
distribution do not have an implied permission to use.

# Preparing

Optionally, first install any technologies you want to use with Free Electron.
See the [tech page](http://freeelectron.org/tech/) for a partial list.

# Compiling

Examine `default.env` and add overrides to `local.env`.

~~~ python
% ./build.py
~~~

Note that this build script delegates to a python-based system called `forge`.

# Example Usage with Existing Tools

Houdini and Maya represent two examples of using the framework with other tools
that support external libraries. Assistance for launching either, with their
respective FE plugins, is included with this source install of FE.

After completing an FE build as described above, if all of the local system
prerequisites are met, the plugins will be available for immediate use.

The scripts `RUN_HOUDINI` and `RUN_MAYA` can launch Houdini or Maya directly
with the matching configuration.  It may be necessary to adjust these wrapper
scripts, as well as `SETUP_HOUDINI` and `SET_ENV`.

Alternatively, just set the appropriate environment variables.

For Houdini, you might need something like:

~~~ sh
export HOUDINI_PATH=${HOME}:${HIH}:${HFS}/houdini:${FE_LIB_PATH}/houdini${HOUDINI_VERSION}
~~~

...where `FE_LIB_PATH` is likely to be the `lib/x86_64_linux_optimize`
directory that appears within this project root directory after a build has
occurred.

For Maya, it is similar:

~~~ sh
export MAYA_PLUG_IN_PATH=${FE_LIB_PATH}/maya${MAYA_VERSION}
export MAYA_SCRIPT_PATH=${FE_LIB_PATH}/maya${MAYA_VERSION}/scripts
export XBMLANGPATH=="${FE_LIB_PATH}/maya${MAYA_VERSION}/icons/%B"
~~~
