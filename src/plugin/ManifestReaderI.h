/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __plugin_ManifestReaderI_h__
#define __plugin_ManifestReaderI_h__

namespace fe
{

class FE_DL_EXPORT ManifestReaderI:
	virtual public Component,
	public CastableAs<ManifestReaderI>
{
	public:

virtual	BWORD	load(String a_filename)										=0;
};

} /* namespace fe */

#endif /* __plugin_ManifestReaderI_h__ */
