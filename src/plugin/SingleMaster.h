/** @file */

#ifndef __plugin_SingleMaster_h__
#define __plugin_SingleMaster_h__

namespace fe
{

/**************************************************************************//**
    @brief Point of Entry for Development

	@ingroup plugin
*//***************************************************************************/
class FE_DL_PUBLIC FE_PLUGIN_PORT SingleMaster:
	public Handled<SingleMaster>,
	public CastableAs<SingleMaster>
{
	public:
									SingleMaster(void);
virtual								~SingleMaster(void);

		sp<Master>					master(void);

									/// @internal
virtual	void						release(void);

static	sp<SingleMaster> FE_CDECL	create(void);

static	sp<SingleMaster> FE_CDECL	existing(void)
									{	return ms_spSingleMaster; }

static	void						usePrefix(String a_prefix);

	protected:

static	FE_DL_PUBLIC	BWORD				ms_registered;
static	FE_DL_PUBLIC	sp<SingleMaster>	ms_spSingleMaster;

	private:

		sp<Master>	m_spMaster;
};

} /* namespace fe */

#endif /* __plugin_SingleMaster_h__ */
