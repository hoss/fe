/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __plugin_Component_h__
#define __plugin_Component_h__

#define	FE_COMPONENT_TRACK	FE_COUNTED_TRACK

namespace fe
{

/**************************************************************************//**
	@brief Base for all interfacable components

	@ingroup plugin
*//***************************************************************************/
class FE_DL_EXPORT Component:
		public Handled<Component>,
		public CastableAs<Component>,
		virtual public Initialized
{
	private:

	/// @brief Reference hub used by Component::adjoin()
	class Hub: public Handled<Hub>
	{
		public:
					Hub(void);
	virtual			~Hub(void);

	const	String&	name(void) const						{ return m_name; }
			void	setName(const String& name)				{ m_name=name; }

			void	attach(Component* pComponent,BWORD singleton);
			void	detach(Component* pComponent,BWORD singleton);

		private:
			List< sp<Component> >	m_persistList;
			String					m_name;
			U32						m_singletonCount;
	};


	public:
						Component(void);
virtual					~Component(void);

#if FE_CPLUSPLUS >= 201103L
						Component(Component&&)						=delete;
						Component(const Component&)					=delete;

		Component&		operator=(Component&&)						=delete;
		Component&		operator=(const Component&)					=delete;
#endif

						/** @fn void fe::Component::initialize(void)

							@brief Called after construction and basic
							factory initialization.

							This is an optional site for setup steps that
							use members not available during true
							construction.

							Any derived class can supply an additional
							non-virtual initialize function.

							To allow all the initialize functions to be
							called, any class @em T supplying an
							@em initialize method must also derive from
							Initialize <T>. */

						/// @brief Return the components chosen name
const	String&			name(void) const				{ return m_name; }
						/// @brief Return the annotated chosen name
const	String			verboseName(void) const			{ return name(); }
						/** @brief Rename the component to anything you want

							The default name is the implementation name
							as given by the factory. */
		void			setName(const String& name)		{ m_name=name; }

						/** @brief Get the Registry that created this
							component

							This allows any component to easily create
							other components. The Registry can also point
							you to the Master. */
		hp<Registry>	registry(void) const;

						/** @brief Tie this component's lifetime to another

							A hub is used to keep all components in a
							group alive as long as at least one of them is
							referenced (in addition to one reference each
							by the hub).  Once the reference count of every
							component in the group drops to one, the
							hub destructs and all the components
							collapse.

							Each component can only be adjoined once.
							A component can be adjoined to by any number
							of other components.

							Adjoining can happen in any order and it
							doesn't matter which existing component in a
							particular group a component adjoins to.

							fe::hp's do not count as references.  It is
							usually good practice for Components within
							the same hub to only retain fe::hp's to
							each other, not fe::sp's.  As long as
							fe::sp's exist, other than from the hub, the
							group will persist indefinitely. */
		void			adjoin(sp<Component> spComponent);

						/** Convienience function to registry()->create()
							*/
		sp<Component>	create(const String &implementation,
								BWORD quiet=FALSE) const;

						///	@brief Untie component from Hub, if connected
		void			disjoin(void);

		String			factoryName(void);

						/** @brief Store a reference to the
							library used to instantiate this component.

							Setting this value externally may be ignored or
							cause an assertion.

							This reference provide two roles:
							- It prevents this library from being unloaded
								while this component persists.
							- If this is the last component of an abandoned
								library, the end-life of this component
								will cause the library to unload.


							@internal */
		void			setLibrary(sp<Library> spLibrary);

						/** @brief Get the library that created the component.

							@internal */
		sp<Library>		library(void)	{ return m_spLibrary; }

						/** @brief Set the library's factory index

							@internal */
		void			setFactoryIndex(I32 a_factoryIndex)
						{	m_factoryIndex=a_factoryIndex; }

						/** @brief Get the library's factory index

							A negative number indicates no known factory.

							@internal */
		I32				factoryIndex(void)	{ return m_factoryIndex; }

						/** @brief Specify whether component is a singleton

							@internal */
		void			setSingleton(BWORD set)	{ m_singleton=set; }
						/// @brief Return whether component is a singleton
		BWORD			isSingleton(void)		{ return m_singleton; }

						using Counted::acquire;

						/** @brief Specialized reference increment

							@internal */
virtual	void			acquire(void);
						/** @brief Specialized reference decrement

							This virtual adds functionality to unload
							libraries under specific circumstances.

							@internal */
virtual	void			release(void);


	private:
		void			assignHub(Hub* pHub);
		void			setStray(BWORD stray);

		String			m_name;
		sp<Library>		m_spLibrary;
		hp<Hub>			m_hpHub;
		I32				m_factoryIndex;

						//* TODO combine flags
		BWORD			m_singleton;
		BWORD			m_stray;
};

namespace internal
{
/** @brief Delete the component and unload libraries where appropriate

	@relates fe::Component

	Under the right circumstances, this unloads one or more unneeded libraries.
	This function is reused by fe::Registry with pComponent=NULL.

	@internal */
FE_DL_EXPORT void FE_CDECL checkUnload(sp<Library>& pLibrary,
		Component* pComponent=NULL);
} // namespace internal

inline void Component::acquire(void)
{
//	feLog("+%p Component::acquire %d %s\n",(U32)this,count(),name().c_str());

#if FE_COUNTED_MT && FE_COMPONENT_TRACK
	RecursiveMutex::Guard guard(Counted::ms_counted_mutex);
#endif

	int refCount;
	Counted::acquire(refCount);

	if(m_hpHub.isValid())
	{
		if(refCount==2+isSingleton())
		{
			setStray(FALSE);
		}
		if(refCount==2)
		{
			m_hpHub->acquire();
			m_hpHub->trackReference(this,"Component::acquire");
		}
	}

#if FE_COMPONENT_TRACK
	if(refCount && registry().isValid())
	{
		registry()->tracker().acquire(this,name(),refCount);
	}
#endif

//	feLog("-%p Component::acquire\n",(U32)this);
}

inline void Component::release(void)
{
	int refCount;

	{
//		feLog("+%p Component::release %d %s\n",(U32)this,count(),
//				verboseName().c_str());

#if FE_COUNTED_MT && FE_COMPONENT_TRACK
		RecursiveMutex::Guard guard(Counted::ms_counted_mutex);
#endif

#if FE_COMPONENT_TRACK
		if(count() && registry().isValid())
		{
			registry()->tracker().release(this,verboseName(),count()-1);
		}
#endif

		refCount=releaseInternal();
		if(m_hpHub.isValid())
		{
			if(refCount==1+isSingleton())
			{
				setStray(TRUE);
			}

			if(refCount==1)
			{
//				feLog("Component::release release Hub from %s\n",
//						verboseName().c_str());

				m_hpHub->release();

				//* This component might now be deleted,
				//* but only if it's the last on on the Hub.
				//* Otherwise, it can be picked up again.
				return;
			}
		}
	}

	if(!refCount)
	{
		abandonHandle();
		fe::internal::checkUnload(m_spLibrary,this);
	}
}

template <class T>
sp<T> component_cast( sp<Component> spC )
{
	sp<T> spT(spC);
	if(!spT.isValid())
	{
		if(spC.isValid())
		{
			throw fe::Exception(e_invalidPointer, "Component Assertion Error",
				"%s as %s", spC->name().c_str(), FE_TYPESTRING(T).c_str());
		}
		else
		{
			throw fe::Exception(e_invalidPointer, "Component Assertion Error",
				"invalid component");
		}
	}
	return spT;
}

#if FE_COMPILER==FE_GNU
#define feCast(T, comp) (fe::Exception::stage(__FILE__,__LINE__,__PRETTY_FUNCTION__),component_cast<T>(comp))
#elif FE_COMPILER==FE_MICROSOFT
#define feCast(T, comp) (fe::Exception::stage(__FILE__,__LINE__,__FUNCTION__),component_cast<T>(comp))
#else
#define feCast(T, comp) (fe::Exception::stage(__FILE__,__LINE__,__func__),component_cast<T>(comp))
#endif

} // namespace fe

#endif /* __plugin_Component_h__ */
