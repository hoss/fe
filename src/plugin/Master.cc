/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <plugin/plugin.pmh>

namespace fe
{

RecursiveMutex Master::ms_mutex;

Master::Master(void):
	m_active(false)
{
	System::printBannerOnce();

#if FE_CODEGEN<=FE_DEBUG
	feLog("Creating FE Master %p\n", this);
#endif

	ms_mutex.lock();

	setName("Master");

	int *pp = new int;

#if FE_CODEGEN<FE_OPTIMIZE
	SystemTicker::calibrate();
#endif

#if FE_CHECK_NEW
	if(!Memory::newWorks())
	{
		feLog("Warning: not using FE version of new()\n");
#if FE_OS==FE_WIN32 || FE_OS==FE_WIN64
		feLog("  heap unification must be provided by your alternative\n");
#elif FE_MEM_ALIGNMENT && FE_OS!=FE_OSX
		feLog("  memory alignment must be provided by your alternative\n");
#endif
	}
#endif

	delete pp;

	m_spAllocator = new BasicAllocator();

	sp<TypeMaster> spTypeMaster(new TypeMaster());
	bind(spTypeMaster);

	sp<Registry> spRegistry(new Registry(this));
	spRegistry->initializeAll();
	bind(spRegistry);

	m_active=true;

	assertCore(typeMaster());
	assertPlugin(typeMaster());
	ms_mutex.unlock();

#if FE_COUNTED_TRACK
	registerRegion(this,sizeof(Master));
#endif
#if FE_CODEGEN<=FE_DEBUG
	feLog("Master created\n");
#endif
}

//~Master::Master(const Master &other)
//~{
//~	if(&other != this)
//~	{
//~		feX("fe::Master::Master","clone impossible");
//~	}
//~}

Master::~Master(void)
{
#if FE_CODEGEN<=FE_DEBUG
	feLog("Dismissing FE Master %p\n", this);
#endif

	if(m_spCatalog.isValid())
	{
#if FE_CODEGEN<=FE_DEBUG
		catalog()->catalogDump();
#endif
		catalog()->catalogClear();
#if FE_CODEGEN<=FE_DEBUG
		feLog("catalog cleared\n");
#endif
	}

	m_active=false;
	m_spTypeMaster = NULL;
	m_spCatalog=NULL;
	m_spAllocator = NULL;
#if FE_COUNTED_TRACK
	deregisterRegion(this);
#endif
	//* component tracker is tied to the registry
	m_spRegistry = NULL;
#if FE_CODEGEN<=FE_DEBUG
	feLog("~Master done\n");
#endif

#if FE_SAFE_COUNTED_MUTEX
	Memory::clearDeadPool();
#endif
}

sp<Catalog> Master::catalog(void)
{
	if(!m_active)
	{
		return sp<Catalog>();
	}
	if(!m_spCatalog.isValid())
	{
		m_spCatalog=createCatalog("Master Catalog");

		if(!m_spCatalog->cataloged("path:plugin"))
		{
			String pluginPath=String("${FE_PLUGIN}").substituteEnv();
			if(pluginPath.empty())
			{
				pluginPath="${FE_ROOT}/../plugin";
			}
			m_spCatalog->catalog<String>("path:plugin")=
					pluginPath.substituteEnv();
		}
	}
	return m_spCatalog;
}

sp<Catalog> Master::createCatalog(String a_name)
{
	if(!m_active)
	{
		return sp<Catalog>();
	}

	if(!m_spLibrary.isValid())
	{
		m_spLibrary=new Library();
		m_spLibrary->setRegistry(m_spRegistry);
	}
	if(!m_spLibrary.isValid())
	{
		return sp<Catalog>();
	}

	return Library::create<Catalog>(a_name,m_spLibrary.raw());
}

} /* namespace */
