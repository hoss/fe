/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __core_Master_h__
#define __core_Master_h__

#define FE_CAT_SPEC(name, description)	name
#define FE_CAT(name)					name

namespace fe
{

/**************************************************************************//**
	@brief Central access point for key pseudo-global objects

	@ingroup plugin
*//***************************************************************************/
class FE_DL_EXPORT Master: public Handled<Master>
{
	public:
							Master(void);
#if FE_CPLUSPLUS >= 201103L
							Master(const Master &)					=delete;
#endif

virtual						~Master(void);

		void				bind(sp<TypeMaster>& a_rspTypeMaster)
							{	m_spTypeMaster=a_rspTypeMaster; }
		sp<TypeMaster>		typeMaster(void) const
							{	return m_spTypeMaster; }

		void				bind(sp<Registry>& a_rspRegistry)
							{	m_spRegistry=a_rspRegistry; }
		sp<Registry>		registry(void) const
							{	return m_spRegistry; }

							//* instantiate a generic catalog
		sp<Catalog>			createCatalog(String a_name);

							//* return the master catalog
		sp<Catalog>			catalog(void);

		sp<Allocator>		allocator(void) const	{ return m_spAllocator; }

	private:
		sp<TypeMaster>		m_spTypeMaster;
		sp<Registry>		m_spRegistry;
		sp<Allocator>		m_spAllocator;

		sp<Library>			m_spLibrary;
		sp<Catalog>			m_spCatalog;
		bool				m_active;

static	RecursiveMutex		ms_mutex;
};

} /* namespace */

#endif /* __core_Master_h__ */

