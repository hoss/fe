/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __plugin_assertPlugin_h__
#define __plugin_assertPlugin_h__

namespace fe
{

FE_DL_EXPORT void assertPlugin(sp<TypeMaster> spTypeMaster);

} /* namespace */

#endif /* __plugin_assertPlugin_h__ */
