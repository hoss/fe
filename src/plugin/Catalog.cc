/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <plugin/plugin.pmh>

namespace fe
{

Catalog::Catalog(void):
	m_highest(0)
{
}

Catalog::~Catalog(void)
{
	catalogClear();
}

sp<TypeMaster> Catalog::typeMaster(void) const
{
	return registry()->master()->typeMaster();
}

sp<Catalog> Catalog::catalogShallowCopy(void)
{
	sp<Catalog> spClone(
			registry()->master()->createCatalog(name()+" clone"));

	spClone->m_orderMap=m_orderMap;
	spClone->m_highest=m_highest;

	for(std::map< String, InstanceMap >::iterator it=m_instanceMapMap.begin();
			it!=m_instanceMapMap.end();it++)
	{
		spClone->m_instanceMapMap[it->first]=it->second;
	}

	return spClone;
}

sp<Catalog> Catalog::catalogDeepCopy(void)
{
	sp<Catalog> spClone(
			registry()->master()->createCatalog(name()+" replicant"));

	spClone->m_orderMap=m_orderMap;
	spClone->m_highest=m_highest;

	for(std::map< String, InstanceMap >::iterator it=m_instanceMapMap.begin();
			it!=m_instanceMapMap.end();it++)
	{
		spClone->m_instanceMapMap[it->first].deepCopy(it->second);
	}

	return spClone;
}

void Catalog::catalogDump(void) const
{
	feLog("Catalog:\n");
	for(std::map< String, InstanceMap >::const_iterator it1=
			m_instanceMapMap.begin();it1!=m_instanceMapMap.end();it1++)
	{
		for(std::map< String, Instance >::const_iterator it2=
				it1->second.begin();it2!=it1->second.end();it2++)
		{
			const String value=print(it2->second);

			feLog(" \"%s\" \"%s\" 0x%x \"%.256s\"%s (%s)\n", it1->first.c_str(),
					it2->first.c_str(), it2->second.data(),value.c_str(),
					value.length()>256? "...": "",
					it2->second.type()->typeinfo().ref().name());
		}
	}
	feLog("\n");
}

U32 Catalog::catalogSize(void) const
{
	return m_orderMap.size();
}

String Catalog::catalogKey(U32 index) const
{
	U32 countdown=index;
	for(std::map< I32, String >::const_iterator it = m_orderMap.begin();
			it != m_orderMap.end(); it++)
	{
		if(!countdown--)
		{
			return it->second;
		}
	}
	return String();
}

void Catalog::catalogKeys(Array<String> &a_keys,String a_pattern) const
{
	if(a_pattern==".*")
	{
		for(std::map< I32, String >::const_iterator it = m_orderMap.begin();
				it != m_orderMap.end(); it++)
		{
			a_keys.push_back(it->second);
		}
	}
	else
	{
		Regex regex(a_pattern.c_str());

		for(std::map< I32, String >::const_iterator it = m_orderMap.begin();
				it != m_orderMap.end(); it++)
		{
			const String& rKey=it->second;
			if(regex.match(rKey.c_str()))
			{
				a_keys.push_back(rKey);
			}
		}
	}
}

void Catalog::catalogProperties(const String a_name,
	Array<String> &a_properties) const
{
	std::map<String, InstanceMap>::const_iterator it1 =
			m_instanceMapMap.find(a_name);
	if(it1 == m_instanceMapMap.end())
	{
		return;
	}

	for(std::map< String, Instance >::const_iterator it2=it1->second.begin();
			it2!=it1->second.end();it2++)
	{
		a_properties.push_back(it2->first);
	}
}

//* TODO verify that this results in a valid m_orderMap
void Catalog::catalogOverlay(sp<Catalog> a_spOtherCatalog,
	Array<String>* a_pTypeList,BWORD a_shallow)
{
	sp<BaseType> spRemoveType=typeMaster()->lookupType<Catalog::Remove>();

	for(std::map< String, InstanceMap >::iterator it=
			a_spOtherCatalog->m_instanceMapMap.begin();
			it != a_spOtherCatalog->m_instanceMapMap.end(); it++)
	{
		const String key=it->first;

		for(std::map< String, Instance >::iterator it2=it->second.begin();
				it2!=it->second.end();it2++)
		{
			const String property=it2->first;
			const Instance instance = it2->second;

			if(a_pTypeList)
			{
				sp<BaseType> spBaseType=instance.type();
				if(spBaseType.isNull())
				{
					feLog("Catalog::catalogOverlay"
							" invalid type for \"%s\" \"%s\"\n",
							key.c_str(),property.c_str());
					continue;
				}

				const String typeName=spBaseType->typeinfo().ref().name();

				const U32 nameCount=a_pTypeList->size();
				U32 nameIndex=0;
				for(;nameIndex<nameCount;nameIndex++)
				{
//					feLog("type \"%s\" vs \"%s\"\n",typeName.c_str(),
//							(*a_pTypeList)[nameIndex].c_str());

					if(typeName==(*a_pTypeList)[nameIndex])
					{
						break;
					}
				}
				if(nameIndex==nameCount)
				{
					continue;
				}
			}

			if(instance.type()==spRemoveType)
			{
				catalogRemove(key);
			}
			else
			{
				if(a_shallow)
				{
					catalogInstance(key,property)=instance;
				}
				else
				{
					//* make an independent instance
					catalogInstance(key,property).create(instance.type());
					catalogInstance(key,property).assign(instance);
				}
			}
		}
	}
}

sp<Component> Catalog::catalogComponent(String implementation,
		String a_name,String a_property)
{
//	feLog("Catalog::catalogComponent '%s' \"%s\" \"%s\"\n",
//			implementation.c_str(),a_name.c_str(),a_property.c_str());

	sp<Component> spComponent;
	if(cataloged(a_name,a_property))
	{
		Instance &instance = m_instanceMapMap[a_name][a_property];
		if(instance.is< sp<Component> >())
		{
			spComponent = instance.cast< sp<Component> >();
		}
	}
	if(!spComponent.isValid())
	{
		spComponent=registry()->create(implementation,a_name);
		createInstance< sp<Component> >(a_name,a_property)=spComponent;
	}
	return spComponent;
}

bool Catalog::catalogLookup(const String a_name, const String a_property,
	Instance &a_instance) const
{
	std::map<String, InstanceMap>::const_iterator it1 =
			m_instanceMapMap.find(a_name);
	if(it1 == m_instanceMapMap.end())
	{
		return false;
	}
	std::map<String, Instance>::const_iterator it2 =
			m_instanceMapMap.at(a_name).find(a_property);
	if(it2 == m_instanceMapMap.at(a_name).end())
	{
		return false;
	}
	a_instance = it2->second;
	return true;
}

String Catalog::catalogValue(String a_name,String a_property) const
{
	if(!cataloged(a_name,a_property))
	{
		return String();
	}

	const Instance& rInstance=
			const_cast<Catalog*>(this)->m_instanceMapMap[a_name][a_property];
	const sp<BaseType> spBaseType=rInstance.type();
	if(spBaseType.isNull())
	{
		feLog("Catalog::catalogValue bad type for \"%s\" \"%s\"\n",
				a_name.c_str(),a_property.c_str());
		return FALSE;
	}

	void *data = rInstance.data();

	std::ostringstream ostrstrm;

	if(-1 == spBaseType->getInfo()->output(ostrstrm, data,
		BaseType::Info::e_ascii))
	{
		feLog("Catalog::catalogValue ascii write failed\n");
	}

	//* remove redundant quotes
	String text=ostrstrm.str().c_str();
	const String chopText=text.prechop("\"").chop("\"");
	if(chopText.length()==text.length()-2)
	{
		text=chopText;
	}

	return text;
}

I32 Catalog::catalogBytes(String a_name,String a_property,
	Array<U8>& a_rByteArray) const
{
	if(!cataloged(a_name,a_property))
	{
		return 0;
	}

	const Instance& rInstance=
			const_cast<Catalog*>(this)->m_instanceMapMap[a_name][a_property];
	const sp<BaseType> spBaseType=rInstance.type();
	if(spBaseType.isNull())
	{
		feLog("Catalog::catalogBytes bad type for \"%s\" \"%s\"\n",
				a_name.c_str(),a_property.c_str());
		return FALSE;
	}

	void *data = rInstance.data();

	std::ostringstream ostrstrm;

	if(-1 == spBaseType->getInfo()->output(ostrstrm, data,
		BaseType::Info::e_binary))
	{
		feLog("Catalog::catalogBytes binary write failed\n");
	}

	std::string buffer=ostrstrm.str();
	const I32 byteCount=buffer.length();
	a_rByteArray.resize(byteCount);
	memcpy(a_rByteArray.data(),buffer.data(),byteCount);

	return byteCount;
}

BWORD Catalog::catalogSet(String a_name,String a_property,String a_value)
{
	if(!cataloged(a_name,a_property))
	{
		return FALSE;
	}

	Instance& rInstance=
			const_cast<Catalog*>(this)->m_instanceMapMap[a_name][a_property];
	const sp<BaseType> spBaseType=rInstance.type();
	if(spBaseType.isNull())
	{
		feLog("Catalog::catalogSet bad type for \"%s\" \"%s\"\n",
				a_name.c_str(),a_property.c_str());
		return FALSE;
	}

	std::istringstream istrstrm(a_value.c_str());
	spBaseType->getInfo()->input(istrstrm,rInstance.data(),
			BaseType::Info::e_ascii);

	// redundant?
//	catalogInstance(rInstance,a_name,a_property);

	return TRUE;
}

BWORD Catalog::catalogSet(String a_name,String a_property,
		String a_type,String a_value,Instance** a_ppInstance)
{
	Instance& rInstance=catalogInstance(a_name,a_property);

	sp<BaseType> spBaseType=rInstance.type();
	if(spBaseType.isNull())
	{
		spBaseType=typeMaster()->lookupName(a_type);
		if(spBaseType.isValid())
		{
//			feLog("Catalog::catalogSet create \"%s\" \"%s\" \"%s\" \"%s\"\n",
//					a_name.c_str(),a_property.c_str(),
//					a_type.c_str(),a_value.c_str());
			rInstance.create(spBaseType);
		}
	}
	if(spBaseType.isNull())
	{
		feLog("Catalog::catalogSet bad type \"%s\" for \"%s\" \"%s\"\n",
				a_type.c_str(),a_name.c_str(),a_property.c_str());
		return FALSE;
	}
	if(!rInstance.unique())
	{
//		feLog("Catalog::catalogSet create unique \"%s\" \"%s\" \"%s\" \"%s\"\n",
//				a_name.c_str(),a_property.c_str(),
//				a_type.c_str(),a_value.c_str());
		rInstance.create(spBaseType);
	}

	std::istringstream istrstrm(a_value.c_str());
	spBaseType->getInfo()->input(istrstrm,rInstance.data(),
			BaseType::Info::e_ascii);

	if(a_ppInstance)
	{
		*a_ppInstance= &rInstance;
	}

	return TRUE;
}

BWORD Catalog::catalogSet(String a_name,String a_property,String a_type,
		const U8* a_pRawBytes,I32 a_byteCount,Instance** a_ppInstance)
{
/*
	if(a_type=="bytearray")
	{
		Instance& rInstance=catalogInstance(a_name,a_property);

		sp<BaseType> spBaseType=rInstance.type();
		if(spBaseType.isNull())
		{
			rInstance.create< Array<U8> >(typeMaster());
			spBaseType=rInstance.type();
		}
		if(spBaseType.isNull())
		{
			feLog("Catalog::catalogSet bad type \"%s\" for \"%s\" \"%s\"\n",
					a_type.c_str(),a_name.c_str(),a_property.c_str());
			return FALSE;
		}
		if(!rInstance.unique())
		{
			rInstance.create< Array<U8> >(typeMaster());
		}

		Array<U8>& rByteArray=rInstance.cast< Array<U8> >();
		rByteArray.resize(a_byteCount);
		memcpy(rByteArray.data(),a_pRawBytes,a_byteCount);

		if(a_ppInstance)
		{
			*a_ppInstance= &rInstance;
		}

		return TRUE;
	}
*/

	Instance& rInstance=catalogInstance(a_name,a_property);

	sp<BaseType> spBaseType=rInstance.type();
	if(spBaseType.isNull())
	{
		spBaseType=typeMaster()->lookupName(a_type);
		if(spBaseType.isValid())
		{
//			feLog("Catalog::catalogSet create \"%s\" \"%s\" \"%s\" \"%s\"\n",
//					a_name.c_str(),a_property.c_str(),
//					a_type.c_str(),a_value.c_str());
			rInstance.create(spBaseType);
		}
	}
	if(spBaseType.isNull())
	{
		feLog("Catalog::catalogSet bad type \"%s\" for \"%s\" \"%s\"\n",
				a_type.c_str(),a_name.c_str(),a_property.c_str());
		return FALSE;
	}
	if(!rInstance.unique())
	{
//		feLog("Catalog::catalogSet create unique \"%s\" \"%s\" \"%s\" \"%s\"\n",
//				a_name.c_str(),a_property.c_str(),
//				a_type.c_str(),a_value.c_str());
		rInstance.create(spBaseType);
	}

	if(a_type=="bytearray")
	{
		Array<U8>& rByteArray=rInstance.cast< Array<U8> >();
		rByteArray.resize(a_byteCount);
		memcpy(rByteArray.data(),a_pRawBytes,a_byteCount);
	}
	else
	{
		MemStream memstream(a_pRawBytes,a_byteCount);
		spBaseType->getInfo()->input(memstream,rInstance.data(),
				BaseType::Info::e_binary);
	}

	if(a_ppInstance)
	{
		*a_ppInstance= &rInstance;
	}

	return TRUE;
}

} /* namespace */
