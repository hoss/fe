/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __plugin_CatalogReaderBase_h__
#define __plugin_CatalogReaderBase_h__

namespace fe
{

class FE_DL_EXPORT CatalogReaderBase:
	virtual public CatalogReaderI,
	public CastableAs<CatalogReaderBase>
{
	public:

		void	addInstance(sp<Catalog> a_spCatalog,
						const String& a_key,const String& a_property,
						const String& a_typeName,const String& a_value);
};

} /* namespace fe */

#endif /* __plugin_CatalogReaderBase_h__ */
