/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __plugin_Registry_h__
#define __plugin_Registry_h__

namespace fe
{

class Catalog;

/**************************************************************************//**
	@brief Dynamic Library Manager

	@ingroup plugin

	Libraries can only be safely loaded and unloaded in a FILO
	(first-in last-out) manner.
	These restrictions are handled internally, but it may require
	some forethought to optimize memory usage by reducing library retention.
*//***************************************************************************/
class FE_DL_EXPORT Registry:
		public Initialize<Registry>,
		public Handled<Registry>,
		public ObjectSafe<Registry>
{
	public:
						Registry(void);
						Registry(Master* pMaster);
virtual					~Registry(void);

virtual	void			initialize(void);

						/** @brief Add a path search for managed
							dynamic libraries.

							Currently, this only used on Windows. */
		Result			addPath(String a_absPath);

						/** @brief Register the factories in the named
							dynamic library

							@em adaptname will append the platform-specific
							prefix and suffix to an abbreviated @em filename,
							such as from 'MyLib' to 'libMyLib.so',

							@em manageDependencies will automatically preload
							any libraries that the given library says
							it needs. */
		Result			manage(const String &filename, bool adaptname = true,
							bool manageDependencies = true);

						/** @brief Register the factories in the Library object

							In cases where using a dso is not practical,
							the method can add factories directly from an
							existing Library object. */
		Result			manage(sp<Library>& a_rspLibrary);

						/** @brief (prototypical) Release interest in the
							named library

							This doesn't necessarily unload the library
							from memory.

							In the current system, trying to re-manage
							a library may be unpredictable. */
		Result			unmanage(const String &filename);

						/** @brief Substitute a dynamic library

							Further create() calls will load and
							use the new library. */
		void 			substituteLibrary(const String &oldDlName,
								const String &newDlName);

		Result			registerLibrary(sp<Library> spLibrary);

						/** @brief Potentially release libraries who don't
							have instantiated components */
		U32				prune(void);

						/** @brief Return a list of available implementations
							that match the string pattern */
		I32				listAvailable(const String &pattern,
								Array<String>& available) const;

						/** @brief Adjust the priority of matching a
							particular implementation */
		void			prioritize(const String &a_implementation,
							I32 a_priority);

						/** @brief Instantiate a Component of the given
							named implementation */
		sp<Component>	create(const String &a_pattern,
							BWORD quiet=FALSE) const;
		sp<Component>	create(const char* a_pattern,
							BWORD quiet=FALSE) const;
		sp<Component>	create(Regex &a_regex,
							BWORD quiet=FALSE) const;

						/** @brief Instantiate a Counted of the given
							named implementation */
		sp<Counted>		create(String implementation,String name) const;

						/// @brief Access the Master (ptr may not be valid)
		sp<Master>		master(void) const;

						/// @brief Access the table of loadable components
		sp<Catalog>		manifest(void);

						/// @brief Access the table of loadable types
		sp<Catalog>		typeManifest(void);

						/** @brief Access the component tracker

							@internal */
		Tracker&		tracker(void)					{ return m_tracker; };

const	String&			name(void) const				{ return m_name; }

		void			dump(void) const;
		void			dumpLibraries(void) const;

						/** @brief reference a Counted that is released
							just before unloading libraries

							Any number of dependents can be added. */
		void			addDependent(sp<Counted> spCounted)
						{	m_dependents.push_back(spCounted); }

	public:
	struct FactoryLocation
	{
		public:
							FactoryLocation(sp<Library> spLibrary,
									U32 factoryIndex):
									m_spLibrary(spLibrary),
									m_factoryIndex(factoryIndex),
									m_priority(0)							{}
			sp<Library>		m_spLibrary;
			U32				m_factoryIndex;
			I32				m_priority;
	};
		std::map<String,FactoryLocation*>
						&factoryMap(void)				{ return m_factoryMap; }

	private:
		void			clear(void);

		sp<Component>	createInternal(const String* a_pattern,
							Regex* a_pRegex,
							BWORD quiet=FALSE) const;

	class TypeLoader:
		public TypeMaster::TypeLoader,
		public CastableAs<TypeLoader>
	{
		public:
	virtual	Result			loadType(String a_name);
			hp<Registry>	m_hpRegistry;
	};

	class LoadedLibrary
	{
		public:
							LoadedLibrary(sp<Library> spLibrary):
									m_spLibrary(spLibrary),
									m_factoriesAccepted(0)
							{	FEASSERT(spLibrary.isValid()); }
							~LoadedLibrary(void);

			sp<Library>		m_spLibrary;
			U32				m_factoriesAccepted;
	};

	class Candidate
	{
		public:
							Candidate(void):
									m_pFactoryLocation(NULL),
									m_priority(0)							{}
							Candidate(I32):
									m_pFactoryLocation(NULL),
									m_priority(0)							{}

			FactoryLocation*	m_pFactoryLocation;
			String				m_factoryName;
			String				m_dlname;
			I32					m_priority;
	};


		I32				findIndex(const String &filename) const;
		void			unmanage(U32 index);

		String			m_name;

		//*	Ordered FILO, one DL_Loader per dynamic library.
		Array<LoadedLibrary*>					m_loaderStack;

		Array< sp<Counted> >					m_dependents;

		std::map<String,FactoryLocation*>		m_factoryMap;

		Tracker									m_tracker;

		hp<Master>								m_hpMaster;
		sp<Catalog>								m_spManifest;
		sp<Catalog>								m_spTypeManifest;
};


} /* namespace */

#endif /* __plugin_Registry_h__ */
