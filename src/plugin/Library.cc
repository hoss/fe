/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <plugin/plugin.pmh>

namespace fe
{

Library::Library(void):
		m_pLoader(NULL),
		m_strayCount(0),
		m_name("Library")
{
	suppressReport();
}

Library::~Library(void)
{
//	feLog("~Library()\n");
	releaseSingletons();
}

void Library::releaseSingletons(void)
{
	SAFEGUARD;

//	feLog("Library::releaseSingletons %s\n",name().c_str());
	U32 m;
	for(m=0;m<m_factories.size();m++)
		m_factories[m].m_spSingleton=NULL;
}

void Library::setRegistry(sp<Registry> spRegistry)
{
	SAFEGUARD;

	m_pRegistry=spRegistry.raw();
}

hp<Registry> Library::registry(void) const
{
	return m_pRegistry? m_pRegistry->getHandle(): hp<Registry>();
}

void Library::populateManifest(Array<String> &names) const
{
	SAFEGUARD;

	U32 m;
	for(m=0;m<m_factories.size();m++)
		names.push_back(m_factories[m].m_spFactory->name());
}

void Library::addFactory(BaseFactory *pFactory,BWORD singleton)
{
	SAFEGUARD;

	U32 index=m_factories.size();
	m_factories.resize(index+1);
	m_factories[index].m_spFactory=pFactory;
	m_factories[index].m_singleton=singleton;
}

sp<Component> Library::create(U32 factoryIndex)
{
	// NOTE guard shouldn't be necessary here
	SAFEGUARD;

	if(!m_factories[factoryIndex].m_singleton)
	{
		return m_factories[factoryIndex].m_spFactory->create(
				this,factoryIndex);
	}

	sp<Component> spSingleton=m_factories[factoryIndex].m_spSingleton;
	if(!spSingleton.isValid())
	{
		SAFEGUARD;

		spSingleton=m_factories[factoryIndex].m_spFactory->create(
				this,factoryIndex);
		if(spSingleton.isValid())
		{
			spSingleton->setSingleton(TRUE);
		}

		m_factories[factoryIndex].m_spSingleton=spSingleton;
	}

	return spSingleton;
}

String Library::factoryName(U32 factoryIndex)
{
	if(factoryIndex>=m_factories.size())
	{
		return "";
	}
	return m_factories[factoryIndex].m_spFactory->name();
}

U32 Library::adjustedReferences(void) const
{
	SAFEGUARD;

	U32 outstanding=count();
//	feLog("Library::adjustedReferences %s outstanding=%d strayCount=%d\n",
//			name().c_str(),outstanding,m_strayCount);

	U32 m;
	for(m=0;m<m_factories.size();m++)
	{
//		U32 count=
//				m_factories[m].m_spSingleton.isValid()?
//				m_factories[m].m_spSingleton->count(): -1;

//		feLog("  %d: singleton %d count %d %s\n",m,
//				m_factories[m].m_singleton, count,
//				m_factories[m].m_spFactory->name().c_str());

		if(m_factories[m].m_singleton &&
				m_factories[m].m_spSingleton.isValid() &&
				m_factories[m].m_spSingleton->count()==1)
		{
//			feLog("  -=1\n");
			outstanding-=1;
		}
	}

	return outstanding;
}

void Library::initComponent(sp<Component>& rspComponent,void* ptr,U32 size,
		String name,Library* pLibrary,I32 a_factoryIndex)
{
	rspComponent->registerRegion(ptr,size);
	rspComponent->setName(name);
	rspComponent->setLibrary(sp<Library>(pLibrary));
	rspComponent->setFactoryIndex(a_factoryIndex);
	rspComponent->initializeAll();
}

} /* namespace */


