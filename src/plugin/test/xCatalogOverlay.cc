/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "xCatalog.h"
#include "math.h"

using namespace fe;

int main(int argc,char** argv)
{
	UNIT_START();

	BWORD complete=FALSE;

	try
	{
		sp<Master> spMaster(new Master);

		sp<Catalog> spCatalogRoot=spMaster->createCatalog("Root");
		UNIT_TEST(spCatalogRoot.isValid());
		if(!spCatalogRoot.isValid())
		{
			feX(argv[0],"spCatalogRoot invalid");
		}

		spCatalogRoot->catalog<String>("A")="alpha";
		spCatalogRoot->catalog<Real>("C")=3.14;
		spCatalogRoot->catalog<bool>("D")=true;

		feLog("spCatalogRoot:\n");
		spCatalogRoot->catalogDump();

		sp<Catalog> spCatalogL1=spMaster->createCatalog("Level 1");
		UNIT_TEST(spCatalogL1.isValid());
		if(!spCatalogL1.isValid())
		{
			feX(argv[0],"spCatalogL1 invalid");
		}

		spCatalogL1->catalog<I32>("B")=42;
		spCatalogL1->catalog<Real>("C")=2.718;
		spCatalogL1->catalog<I32>("D")=7;	//* type change

		feLog("spCatalogL1:\n");
		spCatalogL1->catalogDump();

		sp<Catalog> spCatalogL2=spMaster->createCatalog("Level 2");
		UNIT_TEST(spCatalogL2.isValid());
		if(!spCatalogL2.isValid())
		{
			feX(argv[0],"spCatalogL2 invalid");
		}

		spCatalogL2->catalog<String>("A")="charlie";
		spCatalogL2->catalog<Catalog::Remove>("C");

		feLog("spCatalogL2:\n");
		spCatalogL2->catalogDump();

		sp<Catalog> spCatalogView=spMaster->createCatalog("View");
		UNIT_TEST(spCatalogView.isValid());
		if(!spCatalogView.isValid())
		{
			feX(argv[0],"spCatalogView invalid");
		}

		spCatalogView->catalogOverlay(spCatalogRoot);

		feLog("spCatalogView at Root:\n");
		spCatalogView->catalogDump();

		UNIT_TEST(spCatalogView->catalog<String>("A")=="alpha");
		UNIT_TEST(!spCatalogView->cataloged("B"));
		UNIT_TEST(fabs(spCatalogView->catalog<Real>("C")-3.14)<1e-6);
		UNIT_TEST(spCatalogView->catalog<bool>("D")==true);

		spCatalogView->catalogOverlay(spCatalogL1);

		feLog("spCatalogView at L1:\n");
		spCatalogView->catalogDump();

		UNIT_TEST(spCatalogView->catalog<String>("A")=="alpha");
		UNIT_TEST(spCatalogView->catalog<I32>("B")==42);
		UNIT_TEST(fabs(spCatalogView->catalog<Real>("C")-2.718)<1e-6);
		UNIT_TEST(spCatalogView->catalog<I32>("D")==7);

		spCatalogView->catalogOverlay(spCatalogL2);

		feLog("spCatalogView at L2:\n");
		spCatalogView->catalogDump();

		UNIT_TEST(spCatalogView->catalog<String>("A")=="charlie");
		UNIT_TEST(spCatalogView->catalog<I32>("B")==42);
		UNIT_TEST(!spCatalogView->cataloged("C"));
		UNIT_TEST(spCatalogView->catalog<I32>("D")==7);

		complete=TRUE;
	}
	catch(Exception &e) { e.log(); }
	catch(...)			{ feLog("uncaught exception\n"); }

	UNIT_TEST(complete);
	UNIT_TEST(!Counted::trackerCount());
	UNIT_TRACK(18);
	UNIT_RETURN();
}
