/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <plugin/plugin.pmh>

using namespace fe;

class DerivedComponent_A:
	virtual public Component,
	public CastableAs<DerivedComponent_A>
{
	public:
//				DerivedComponent_A(void)	{ feLog("DerivedComponent_A\n"); }
		int a;
};

class DerivedComponent_B:
	public DerivedComponent_A,
	public CastableAs<DerivedComponent_B>
{
	public:
//				DerivedComponent_B(void)	{ feLog("DerivedComponent_B\n"); }
		double b;
};

class DerivedComponent_C:
	virtual public Component,
	public CastableAs<DerivedComponent_C>
{
	public:
//				DerivedComponent_C(void)	{ feLog("DerivedComponent_C\n"); }
		float c;
};

class DerivedComponent_D:
	public DerivedComponent_B,
	public DerivedComponent_C,
	public CastableAs<DerivedComponent_D>
{
	public:
//				DerivedComponent_D(void)	{ feLog("DerivedComponent_D\n"); }
		String d;
};


int main(int argc,char** argv)
{
	UNIT_START();

	BWORD complete=FALSE;

	for(I32 index=0;index<10000;index++)
	{
		//* spin for 1ms
		nanoSpin(1000000);
	}

	try
	{
		const I32 createCount=10000;
		const I32 arrayCount=10;

		sp<Component> spComponent[arrayCount];

		const FE_UWORD t0=systemTick();

		for(I32 index=0;index<createCount;index++)
		{
			const I32 arrayIndex=index%arrayCount;
			spComponent[arrayIndex]=
					Library::create<DerivedComponent_D>("DerivedComponent_D");
			FEASSERT(spComponent[arrayIndex].isValid());
		}

		const FE_UWORD t1=systemTick();

		const FE_UWORD tickDiff=t1-t0;
		const Real microSeconds=tickDiff*SystemTicker::microsecondsPerTick();
		const Real perCreate=microSeconds/Real(createCount);

		feLog("creates %d microseconds %.6G per %.6G\n",
				createCount,microSeconds,perCreate);

		sp<DerivedComponent_D> castArray[arrayCount];

		const FE_UWORD t2=systemTick();

		for(I32 index=0;index<createCount;index++)
		{
			const I32 arrayIndex=index%arrayCount;
			castArray[arrayIndex]=spComponent[arrayIndex];
			FEASSERT(castArray[arrayIndex].isValid());
		}

		const FE_UWORD t3=systemTick();

		const FE_UWORD tickDiffCast=t3-t2;
		const Real microSecondsCast=
				tickDiffCast*SystemTicker::microsecondsPerTick();
		const Real perCast=microSecondsCast/Real(createCount);

		feLog("casts %d microseconds %.6G per %.6G\n",
				createCount,microSecondsCast,perCast);

		complete=TRUE;
	}
	catch(Exception &e) { e.log(); }
	catch(...)			{ feLog("uncaught exception\n"); }

	UNIT_TEST(complete);
	UNIT_TEST(!Counted::trackerCount());
	UNIT_TRACK(2);
	UNIT_RETURN();
}
