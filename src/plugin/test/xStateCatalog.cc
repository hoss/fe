/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "xCatalog.h"
#include "math.h"

using namespace fe;

class FE_DL_EXPORT MyStateCatalog:
	public StateCatalog,
	public CastableAs<MyStateCatalog>
{
	public:
virtual	Result		start(void)		{ return e_ok; }
virtual	Result		stop(void)		{ return e_ok; }
};

bool approximately_equal(Real value1,Real value2)
{
	return (fabs(value1-value2)<1e-6);
}

int main(int argc,char** argv)
{
	UNIT_START();

	BWORD complete=FALSE;

	try
	{
		sp<Master> spMaster(new Master);
		sp<TypeMaster> spTypeMaster=spMaster->typeMaster();

		sp<Library> spLibrary(new Library());
		spLibrary->setRegistry(spMaster->registry());

		sp<MyStateCatalog> spStateCatalog=
				Library::create<MyStateCatalog>("MyStateCatalog",
				spLibrary.raw());
		UNIT_TEST(spStateCatalog.isValid());
		if(!spStateCatalog.isValid())
		{
			feX(argv[0],"spStateCatalog invalid");
		}

		spStateCatalog->setState<String>("A","alpha");
		spStateCatalog->setState<Real>("C",3.14);
		spStateCatalog->setState<bool>("D",true);

		sp<StateCatalog::Snapshot> spSnapshot0;
		spStateCatalog->snapshot(spSnapshot0);
		UNIT_TEST(spSnapshot0.isValid());
		if(!spSnapshot0.isValid())
		{
			feX(argv[0],"spSnapshot0 invalid");
		}

		feLog("spStateCatalog\n");
		spStateCatalog->catalogDump();

		feLog("spSnapshot0 (serial=%d)\n",spSnapshot0->serial());
		spSnapshot0->dump();

		feLog("spSnapshot0 updates\n");
		spSnapshot0->dumpUpdates();

		feLog("update C=2.71\n");
		spStateCatalog->setState<Real>("C",2.71);
		feLog("add E=13\n");
		spStateCatalog->setState<I32>("B",13);

		feLog("\nspStateCatalog\n");
		spStateCatalog->catalogDump();

		Real valueC= -1.0;
		spStateCatalog->getState<Real>("C",valueC);
		UNIT_TEST(approximately_equal(valueC,2.71));

		I32 valueB= -1;
		spStateCatalog->getState<I32>("B",valueB);
		UNIT_TEST(valueB==13);

		String typeValueB;
		spStateCatalog->getTypeName("B",typeValueB);
		feLog("B type is '%s'\n",typeValueB.c_str());
		UNIT_TEST(spTypeMaster->lookupName(typeValueB)==
				spTypeMaster->lookupName("I32"));

		feLog("spSnapshot0 (serial=%d)\n",spSnapshot0->serial());
		spSnapshot0->dump();

		feLog("spSnapshot0 updates\n");
		spSnapshot0->dumpUpdates();

		valueC= -1.0;
		spSnapshot0->getState<Real>("C",valueC);
		UNIT_TEST(approximately_equal(valueC,3.14));

		feLog("update E=17\n");
		spStateCatalog->setState<I32>("B",17);
		feLog("serial++\n");
		spStateCatalog->incrementSerial();

		sp<StateCatalog::Snapshot> spSnapshot1;
		spStateCatalog->snapshot(spSnapshot1);
		UNIT_TEST(spSnapshot1.isValid());
		if(!spSnapshot1.isValid())
		{
			feX(argv[0],"spSnapshot1 invalid");
		}

		feLog("\nspStateCatalog\n");
		spStateCatalog->catalogDump();

		valueB= -1;
		spStateCatalog->getState<I32>("B",valueB);
		UNIT_TEST(valueB==17);

		feLog("spSnapshot0 (serial=%d)\n",spSnapshot0->serial());
		spSnapshot0->dump();

		feLog("spSnapshot0 updates\n");
		spSnapshot0->dumpUpdates();

		feLog("spSnapshot1 (serial=%d)\n",spSnapshot1->serial());
		spSnapshot1->dump();

		feLog("spSnapshot1 updates\n");
		spSnapshot1->dumpUpdates();

		valueB= -1;
		spSnapshot1->getState<I32>("B",valueB);
		UNIT_TEST(valueB==17);

		feLog("release spSnapshot0\n");
		spSnapshot0=NULL;
		feLog("update E=19\n");
		spStateCatalog->setState<I32>("B",19);
		feLog("serial++\n");
		spStateCatalog->incrementSerial();

		sp<StateCatalog::Snapshot> spSnapshot2;
		spStateCatalog->snapshot(spSnapshot2);
		UNIT_TEST(spSnapshot2.isValid());
		if(!spSnapshot2.isValid())
		{
			feX(argv[0],"spSnapshot2 invalid");
		}

		feLog("\nspStateCatalog\n");
		spStateCatalog->catalogDump();

		valueB= -1;
		spStateCatalog->getState<I32>("B",valueB);
		UNIT_TEST(valueB==19);

		feLog("spSnapshot1 (serial=%d)\n",spSnapshot1->serial());
		spSnapshot1->dump();

		feLog("spSnapshot1 updates\n");
		spSnapshot1->dumpUpdates();

		valueB= -1;
		spSnapshot1->getState<I32>("B",valueB);
		UNIT_TEST(valueB==17);

		feLog("spSnapshot2 (serial=%d)\n",spSnapshot2->serial());
		spSnapshot2->dump();

		feLog("spSnapshot2 updates\n");
		spSnapshot2->dumpUpdates();

		valueB= -1;
		spSnapshot2->getState<I32>("B",valueB);
		UNIT_TEST(valueB==19);

		valueC= -1.0;
		spSnapshot2->getState<Real>("C",valueC);
		UNIT_TEST(approximately_equal(valueC,2.71));

		String typeValueC;
		spStateCatalog->getTypeName("C",typeValueC);
		feLog("C type is '%s'\n",typeValueC.c_str());
		UNIT_TEST(spTypeMaster->lookupName(typeValueC)==
				spTypeMaster->lookupName("real"));

		{
			feLog("Atomic testing\n");

			StateCatalog::Atomic atomic(spStateCatalog);

			bool valueD(false);
			atomic.getState<bool>("D",valueD);
			UNIT_TEST(valueD==true);

			String typeValueD;
			atomic.getTypeName("D",typeValueD);
			feLog("D type is '%s'\n",typeValueD.c_str());
			UNIT_TEST(spTypeMaster->lookupName(typeValueD)==
					spTypeMaster->lookupName("boolean"));
		}

		complete=TRUE;
	}
	catch(Exception &e) { e.log(); }
	catch(...)			{ feLog("uncaught exception\n"); }

	UNIT_TEST(complete);
	UNIT_TEST(!Counted::trackerCount());
	UNIT_TRACK(19);
	UNIT_RETURN();
}
