/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <typeinfo>
#include "fe/plugin.h"
#include "xInterface.h"

using namespace fe;

class CreatorTask: public Thread::Functor
{
	public:
						CreatorTask(U32 id,U32 components,
								sp<Registry> spRegistry)
						{	m_id=id;
							m_components=components;
							m_spRegistry=spRegistry; }

virtual					~CreatorTask(void)									{}

virtual	void			operate(void);

	private:
		U32				m_id;
		U32				m_components;
		sp<Registry>	m_spRegistry;
};

inline void CreatorTask::operate(void)
{
	sp<MyComponentI>* pspMyComponentI=new sp<MyComponentI>[m_components];

	U32 m;
	for(m=0;m<m_components;m++)
	{
		pspMyComponentI[m]=m_spRegistry->create("MyComponentI");

		if(pspMyComponentI[m].isValid())
		{
			U32 square=pspMyComponentI[m]->test(m);
//			feLog("%d:%d square=%d\n",m_id,m,square);
			if(square!=m*m)
				feX("CreatorTask::operator()",
						"MyComponentI test(%d) failed",m);
		}
		else
			feX("CreatorTask::operator()","failed to create MyComponentI");
	}

	delete[] pspMyComponentI;
}

int main(int argc,char** argv)
{
	Mutex::confirm("fexBoostThread");

	UNIT_START();
	BWORD completed=FALSE;

	if(argc<3)
	{
		feLog("Syntax: %s <threads> <components>\n",argv[0]);
		exit(1);
	}

	const U32 threads=atoi(argv[1]);
	const U32 components=atoi(argv[2]);

	feLog("%d threads %d components\n",threads,components);

	try
	{
		sp<Registry> spRegistry(new Registry());
		spRegistry->initializeAll();

		Result result=spRegistry->manage("xPluginLibrary");
		UNIT_TEST(successful(result));

		Thread::Group group;

		CreatorTask** task=new CreatorTask*[threads];

		U32 m;
		for(m=0;m<threads;m++)
			task[m]=new CreatorTask(m,components,spRegistry);

		for(m=0;m<threads;m++)
			group.createThread(task[m]);

		group.joinAll();

		for(m=0;m<threads;m++)
			delete task[m];

		delete[] task;

		feLog("-- end of block --\n");
		completed=TRUE;
	}
	catch(Exception &e)	{ e.log(); }
	catch(...)			{ feLog("uncaught exception\n"); }

	feLog("-- out of block --\n");

	UNIT_TEST(completed);
	UNIT_TEST(!Counted::trackerCount());
	UNIT_TRACK(3);
	UNIT_RETURN();
}
