/** @file */

#include "plugin/plugin.pmh"

using namespace fe;

class FE_DL_EXPORT MyContext:
	public Handled<MyContext>,
	public CastableAs<MyContext>
{
	public:
						MyContext(void)
						{	feLog("MyContext::MyContext\n"); }
virtual					~MyContext(void)
						{	feLog("MyContext::~MyContext\n"); }

static	sp<MyContext>	create(void);

		sp<Master>		master(void)
						{	return m_spSingleMaster->master(); }

	private:
static	sp<SingleMaster>	m_spSingleMaster;
};

sp<SingleMaster> MyContext::m_spSingleMaster;

inline sp<MyContext> MyContext::create(void)
{
	feLog("MyContext::create\n");

	m_spSingleMaster=SingleMaster::create();
	sp<Master> spMaster=m_spSingleMaster->master();
	sp<Catalog> spCatalog=spMaster->catalog();
	sp<MyContext> spMyContext=spCatalog->catalog< sp<Counted> >("MyContext");
	if(spMyContext.isNull())
	{
		spMyContext=new MyContext();
		spCatalog->catalog< sp<Counted> >("MyContext")=spMyContext;
	}
	return spMyContext;
}

int main(int argc,char** argv)
{
	UNIT_START();

	BWORD complete=FALSE;

	try
	{
		{
			sp<MyContext> spMyContext(MyContext::create());
			const void* raw1=spMyContext.raw();
			feLog("spMyContext %p\n",raw1);

			sp<MyContext> spMyContext2(MyContext::create());
			const void* raw2=spMyContext2.raw();
			feLog("spMyContext2 %p\n",raw2);

			UNIT_TEST(raw1==raw2);

			sp<Master> spMaster=spMyContext->master();
			sp<Registry> spRegistry=spMaster->registry();
			UNIT_TEST(spRegistry.isValid());
		}

		complete=TRUE;
	}
	catch(Exception &e) { e.log(); }
	catch(...)			{ feLog("uncaught exception\n"); }

	UNIT_TEST(complete);
	UNIT_TEST(!Counted::trackerCount());
	UNIT_TRACK(4);
	UNIT_RETURN();
}
