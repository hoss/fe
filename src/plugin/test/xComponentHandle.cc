/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "fe/plugin.h"

using namespace fe;

class MyComponent: public Component, CastableAs<MyComponent>
{
};

int main(int argc,char** argv)
{
	UNIT_START();

	BWORD completed=FALSE;

	try
	{
		sp<Component> spComponent(new MyComponent());
		spComponent->setName("MyComponent");
		UNIT_TEST(spComponent.isValid());

		hp<Component> handle=spComponent;
		UNIT_TEST(handle.isValid());
		UNIT_TEST(handle.raw()==spComponent.raw());

		hp<Component> handle2=handle;
		UNIT_TEST(handle2.isValid());
		UNIT_TEST(handle2.raw()==spComponent.raw());

		feLog("name: \"%s\"\n",handle2->name().c_str());

		sp<MyComponent> spMyComponent=handle;
		UNIT_TEST(spMyComponent.isValid());
		UNIT_TEST(spComponent==spMyComponent);

		spMyComponent=NULL;
		UNIT_TEST(handle.isValid());
		UNIT_TEST(handle2.isValid());

		spComponent=NULL;
		UNIT_TEST(!handle.isValid());
		UNIT_TEST(!handle2.isValid());

		completed=TRUE;
	}
	catch(fe::Exception &e)
	{
		e.log();
	}
	catch(...)
	{
		feLog("uncaught exception\n");
	}

	UNIT_TEST(completed);
	UNIT_TRACK(12);
	UNIT_RETURN();
}
