/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __plugin_test_xInterface_h__
#define __plugin_test_xInterface_h__

class FE_DL_PUBLIC MyComponentI:
	virtual public fe::Component,
	public fe::CastableAs<MyComponentI>
{
	public:
virtual	int	test(int i)														= 0;
};

class FE_DL_PUBLIC MyComponent2I:
	virtual public fe::Component,
	fe::CastableAs<MyComponent2I>
{
	public:
virtual	int	test(int i)														= 0;
};

#endif /* __plugin_test_xInterface_h__ */
