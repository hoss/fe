/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "fe/plugin.h"

using namespace fe;

class MyComponent:
	public Component,
	public CastableAs<MyComponent>
{
	public:
				MyComponent(void)				{ feLog("MyComponent()\n"); }
virtual			~MyComponent(void)											{}
		void	setOther(sp<MyComponent> spOther)	{ m_hpOther=spOther; }

		hp<Component>	m_hpOther;
};

int main(int argc,char** argv)
{
	UNIT_START();

	BWORD completed=FALSE;

	try
	{
		sp<Registry> spRegistry(new Registry());
		spRegistry->initializeAll();

		{
			sp<MyComponent> spComponentA(new MyComponent());
			spComponentA->setName("MyComponent A");

			sp<MyComponent> spComponentB(new MyComponent());
			spComponentB->setName("MyComponent B");

			spComponentA->setOther(spComponentB);
			spComponentB->setOther(spComponentA);

			feLog("adjoin spComponentA\n");
			spComponentB->adjoin(spComponentA);
			feLog("spComponentB=NULL\n");
			spComponentB=NULL;

			UNIT_TEST(spComponentA.isValid());
			UNIT_TEST(spComponentA->m_hpOther.isValid());

			feLog("spComponentB=spComponentA->m_hpOther\n");
			spComponentB=spComponentA->m_hpOther;
			feLog("spComponentA=NULL\n");
			spComponentA=NULL;

			UNIT_TEST(spComponentB.isValid());
			UNIT_TEST(spComponentB->m_hpOther.isValid());

			feLog("hpComponentA()\n");
			hp<MyComponent> hpComponentA(spComponentB->m_hpOther);
			feLog("hpComponentB()\n");
			hp<MyComponent> hpComponentB(spComponentB);
			UNIT_TEST(hpComponentA.isValid());
			UNIT_TEST(hpComponentB.isValid());

			feLog("disjoin hpComponentA\n");
			hpComponentA->disjoin();
			UNIT_TEST(!hpComponentA.isValid());
			UNIT_TEST(hpComponentB.isValid());

			feLog("--\n");
		}

		completed=TRUE;
	}
	catch(Exception &e)	{ e.log(); }
	catch(...)			{ feLog("uncaught exception\n"); }

	UNIT_TEST(completed);
	UNIT_TEST(!Counted::trackerCount());
	UNIT_TRACK(10);
	UNIT_RETURN();
}
