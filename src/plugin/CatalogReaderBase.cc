/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <plugin/plugin.pmh>

namespace fe
{

void CatalogReaderBase::addInstance(sp<Catalog> a_spCatalog,const String& a_key,
	const String& a_property,const String& a_typeName,const String& a_value)
{
	sp<StateCatalog> spStateCatalog=a_spCatalog;
	if(spStateCatalog.isValid())
	{
		spStateCatalog->setState(a_key,a_property,a_typeName,a_value);
	}
	else
	{
		Instance* pInstance=NULL;
		a_spCatalog->catalogSet(a_key,a_property,a_typeName,a_value,&pInstance);
	}
}

} /* namespace fe */
