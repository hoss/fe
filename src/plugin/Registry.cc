/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <plugin/plugin.pmh>

#define FE_REGISTRY_MANAGE_DEBUG		FALSE
#define FE_REGISTRY_MATCH_DEBUG			FALSE
#define FE_REGISTRY_FACTORY_DEBUG		FALSE
#define FE_REGISTRY_SUBSTITUTE_DEBUG	TRUE
#define FE_REGISTRY_DUMP_LIBRARIES		(FE_CODEGEN<=FE_DEBUG)

namespace fe
{

Registry::Registry(void):
		m_name("Registry (no Master)"),
		m_tracker("Component")
{
}

Registry::Registry(Master* pMaster):
		m_name("Registry"),
		m_tracker("Component")
{
	if(pMaster)
	{
		m_hpMaster=pMaster->getHandle();
	}
}

void Registry::initialize(void)
{
	//* NOTE provoke creation of feLogger before loading a dll (Windows issue)
	feLogger();

	//* NOTE gdb backtrace() can fail after a failed dlopen(),
	//* unless called once early
	feGenerateBacktrace();

#if FE_COUNTED_TRACK
	Counted::startTracker();
#endif

	if(m_hpMaster.isValid())
	{
		sp<TypeMaster> spTypeMaster=m_hpMaster->typeMaster();

		sp<TypeLoader> spTypeLoader(new TypeLoader);
		spTypeLoader->m_hpRegistry=hp<Registry>(this);

		spTypeMaster->addTypeLoader(spTypeLoader);
	}
}

Registry::~Registry(void)
{
#if FE_REGISTRY_FACTORY_DEBUG
	feLog("~Registry\n");
	dump();
#endif

	Thread::dismiss();
	Mutex::dismiss();

	m_dependents.clear();
	m_spManifest=NULL;

#if FE_REGISTRY_DUMP_LIBRARIES
	feLog("~Registry libraries loaded:\n");
	dumpLibraries();
	feLog("\n");
#endif

	clear();
#if FE_COMPONENT_TRACK
	feLog("\nComponent %s\n\n",m_tracker.report().c_str());
#endif

	//* minimize Registry's impact on the report
	abandonHandle();
	m_hpMaster.invalidate();

#if FE_COUNTED_TRACK
	feLog("Counted %s\n\n",Counted::reportTracker().c_str());
	Counted::stopTracker();
#endif
}

Registry::LoadedLibrary::~LoadedLibrary(void)
{
	//* reuse chained library shutdown code (pComponent=NULL)
	fe::internal::checkUnload(m_spLibrary);
}

sp<Master> Registry::master(void) const
{
	if(m_hpMaster.isValid())
	{
		return m_hpMaster;
	}
	else
	{
		return sp<Master>(NULL);
	}
}

Result Registry::TypeLoader::loadType(String a_name)
{
//	feLog("Registry::TypeLoader::loadType \"%s\"\n",a_name.c_str());

	sp<Registry> spRegistry(m_hpRegistry);
	if(spRegistry.isNull())
	{
		return e_invalidPointer;
	}

	const String& dlname=
			spRegistry->typeManifest()->catalogOrDefault<String>(a_name,"");

//	feLog("Registry::TypeLoader::loadType dlName \"%s\"\n",dlname.c_str());

	if(dlname.empty())
	{
		return e_cannotFind;
	}

	Result result=spRegistry->manage(dlname);

	if(successful(result) && System::getVerbose()=="all")
	{
		feLog("autoload %s (for type \"%s\")\n",dlname.c_str(),a_name.c_str());
	}

	return result;
}

Result Registry::addPath(String a_absPath)
{
	return DL_Loader::addPath(a_absPath);
}

sp<Catalog> Registry::manifest(void)
{
	if(!m_spManifest.isValid())
	{
		m_spManifest=master()->createCatalog("Manifest");
	}
	return m_spManifest;
}

sp<Catalog> Registry::typeManifest(void)
{
	if(!m_spTypeManifest.isValid())
	{
		m_spTypeManifest=master()->createCatalog("TypeManifest");
	}
	return m_spTypeManifest;
}

U32 Registry::prune(void)
{
	SAFEGUARD;

	U32 prunings=0;

	while(!m_loaderStack.empty())
	{
		LoadedLibrary &rLoadLib=*m_loaderStack.back();
		U32 adjusted=rLoadLib.m_spLibrary->adjustedReferences();
		U32 stray=rLoadLib.m_spLibrary->strayCount();
		U32 net=adjusted-stray;

		if(net!=rLoadLib.m_factoriesAccepted+1)
		{
			feLog("Registry::prune() \"%s\" has %d referencing objects\n"
					"  (incl. %d factories, %d hub strays, and 1 Registry)\n",
					rLoadLib.m_spLibrary->name().c_str(),
					adjusted,rLoadLib.m_factoriesAccepted,stray);

#if FE_COMPONENT_TRACK
			feLog("\nComponent %s\n",m_tracker.report().c_str());
#endif
		}

		FEASSERT(net>rLoadLib.m_factoriesAccepted);
		if(net-rLoadLib.m_factoriesAccepted != 1)
		{
			return prunings;
		}

		unmanage(m_loaderStack.size()-1);
		prunings++;
	}

	return prunings;
}

I32 Registry::findIndex(const String& library) const
{
	SAFEGUARD;

	U32 size=m_loaderStack.size();
	U32 m;
	for(m=0;m<size;m++)
	{
		LoadedLibrary &rLoadLib= *(m_loaderStack[m]);
		if(rLoadLib.m_spLibrary->name()==library)
			return m;
	}
	return -1;
}

void Registry::dumpLibraries(void) const
{
	SAFEGUARD;

	U32 size=m_loaderStack.size();
	U32 m;
	for(m=0;m<size;m++)
	{
		LoadedLibrary &rLoadLib= *(m_loaderStack[m]);

		const I32 factoriesAccepted=rLoadLib.m_factoriesAccepted;
		const I32 referencingObjects=
				rLoadLib.m_spLibrary->adjustedReferences();

		feLog("  \"%s\"  (%d %s of %d %s)\n",
				rLoadLib.m_spLibrary->name().c_str(),
				factoriesAccepted,
				factoriesAccepted==1? "factory": "factories",
				referencingObjects,
				referencingObjects==1?
				"referencing object": "referencing objects");
	}
}

void Registry::dump(void) const
{
	SAFEGUARD;

	fe_printf("Registry::dump\n");
	std::map<String,FactoryLocation*>::const_iterator it=m_factoryMap.begin();
	while(it!=m_factoryMap.end())
	{
		fe_printf("  factory %p \"%s\" in \"%s\"\n",
				it->second,it->first.c_str(),
				it->second->m_spLibrary->name().c_str());
		it++;
	}
}

Result Registry::unmanage(const String& library)
{
	SAFEGUARD;

	I32 index=findIndex(library);
	if(index<0)
		return e_cannotFind;

	unmanage(index);
	return e_ok;
}

void Registry::unmanage(U32 index)
{
	SAFEGUARD;

	LoadedLibrary* pLoadLib=m_loaderStack[index];
	String filename=pLoadLib->m_spLibrary->name();

#if FE_REGISTRY_MANAGE_DEBUG
	feLog("Registry::unmanage \"%s\"\n",filename.c_str());
#endif

	//* release the factories (is this too slow?)
	std::map<String,FactoryLocation*>::iterator it=m_factoryMap.begin();
	while(it!=m_factoryMap.end())
	{
		if(it->second->m_spLibrary->name()==filename)
		{
			FactoryLocation* pFactoryLocation=it->second;

#if FE_REGISTRY_FACTORY_DEBUG
			feLog("Registry::unmanage releasing factory %p \"%s\"\n",
					pFactoryLocation,it->first.c_str());
#endif

			delete pFactoryLocation;
			std::map<String,FactoryLocation*>::iterator it2=it++;
			m_factoryMap.erase(it2);
		}
		else
		{
			it++;
		}
	}

	if(pLoadLib->m_spLibrary->adjustedReferences()-
			pLoadLib->m_spLibrary->strayCount() != 1)
	{
		feLog("Registry::unmanage abandoning \"%s\""
				" still in use (%d extra ref)\n",
				pLoadLib->m_spLibrary->name().c_str(),
				pLoadLib->m_spLibrary->adjustedReferences()-1);
	}

	pLoadLib->m_spLibrary->releaseSingletons();
	pLoadLib->m_spLibrary->setRegistry(sp<Registry>(NULL));
	delete pLoadLib;

	U32 m;
	for(m=index;m<m_loaderStack.size()-1;m++)
	{
		m_loaderStack[m]=m_loaderStack[m+1];
	}
	m_loaderStack.pop_back();
}

void Registry::clear(void)
{
	SAFEGUARD;

	prune();

#if FALSE
	//* to speed things up, wipe out all lookups to avoid selective cleanup
	std::map<String,FactoryLocation*>::iterator it=m_factoryMap.begin();
	while(it!=m_factoryMap.end())
	{
		delete it->second;
		std::map<String,FactoryLocation*>::iterator it2=it++;
		m_factoryMap.erase(it2);
	}
#endif

	while(!m_loaderStack.empty())
	{
		unmanage(m_loaderStack.size()-1);
	}
}

Result Registry::registerLibrary(sp<Library> spLibrary)
{
	for(unsigned int i = 0; i < m_loaderStack.size(); i++)
	{
		if(m_loaderStack[i]->m_spLibrary->name() == spLibrary->name())
		{
			return e_alreadyAvailable;
		}
	}

	if(!m_loaderStack.empty())
	{
		spLibrary->setChainLibrary(m_loaderStack.back()->m_spLibrary);
	}

	return manage(spLibrary);
}

Result Registry::manage(const String &filename, bool adaptname,
		bool manageDependencies)
{
	SAFEGUARD;

	if(findIndex(filename)>=0)
	{
		return e_alreadyAvailable;
	}

	DL_Loader *pLoader=new DL_Loader();
	FEASSERT(pLoader);

#if FE_REGISTRY_MANAGE_DEBUG
	feLog("Registry::manage(\"%s\")\n",filename.c_str());
#endif

	BWORD success=pLoader->openDL(filename,adaptname);
	if(!success)
	{
#if FE_REGISTRY_MANAGE_DEBUG
		feLog("Registry::manage(\"%s\") failed to load\n",filename.c_str());
#endif

		delete pLoader;
		return e_readFailed;
	}

	//* Load all dependencies first
	//* TODO prevent cycles (add current to an exclusion list)

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wconditionally-supported"

	void (*fpListDependencies)(List<String*>&);
	fpListDependencies=(void (*)(List<String*>&))
			pLoader->findDLFunction("ListDependencies");

#pragma GCC diagnostic pop

	if(manageDependencies && fpListDependencies)
	{
		//* NOTE Win32 sidestep: need to replace new'ed strings
		List<String*> preloadList;
		preloadList.setAutoDestruct(TRUE);
		fpListDependencies(preloadList);

#if TRUE
		List<String*>::Iterator iterator(preloadList);
		iterator.toHead();
		while(*iterator)
		{
			delete iterator.replace(new String((*iterator)->c_str()));
			iterator++;
		}
#endif

		BWORD unloaded=FALSE;
		String* pDependency;
		iterator.toHead();
		while((pDependency= *iterator++))
		{
#if FE_REGISTRY_MANAGE_DEBUG
			feLog("  \"%s\" requires dependency \"%s\"",filename.c_str(),
					pDependency->c_str());
#endif
			if(findIndex(*pDependency)<0)
			{
#if FE_REGISTRY_MANAGE_DEBUG
				feLog("  not loaded \n");
#endif
				if(!unloaded)
				{
					//* unload desired lib while we load dependencies
					success=pLoader->closeDL();
					FEASSERT(success);
					unloaded=TRUE;
				}
				manage(*pDependency);
			}
#if FE_REGISTRY_MANAGE_DEBUG
			else
			{
				feLog("  already loaded \n");
			}
#endif
		}

		if(unloaded)
		{
			//* reload desired lib
			success=pLoader->openDL(filename,adaptname);
			FEASSERT(success);
		}
	}

#if FE_REGISTRY_MANAGE_DEBUG
	feLog("Registry::manage \"%s\" loaded\n",filename.c_str());
#endif

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wconditionally-supported"

	Library* (*fpCreateLibrary)(sp<Master>);
	fpCreateLibrary=(Library *(*)(sp<Master>))
			pLoader->findDLFunction("CreateLibrary");

#pragma GCC diagnostic pop

	sp<Library> spLibrary;
	if(fpCreateLibrary)
	{
		spLibrary=fpCreateLibrary(master());
	}
	if(spLibrary.isNull())
	{
		success=pLoader->closeDL();
		FEASSERT(success);

#if FE_REGISTRY_MANAGE_DEBUG
	feLog("Registry::manage \"%s\" does not have CreateLibrary()\n",
			filename.c_str());
#endif

		return e_invalidFile;
	}

	spLibrary->setLoader(pLoader);

	for(unsigned int i = 0; i < m_loaderStack.size(); i++)
	{
		if(m_loaderStack[i]->m_spLibrary->name() == spLibrary->name())
		{
#if FE_REGISTRY_MANAGE_DEBUG
		feLog("Registry::manage \"%s\" already loaded\n",
				filename.c_str());
#endif
			return e_alreadyAvailable;
		}
	}

	if(!m_loaderStack.empty())
		spLibrary->setChainLibrary(m_loaderStack.back()->m_spLibrary);

	Result result=manage(spLibrary);

	if(successful(result))
	{
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wconditionally-supported"

		void (*fpInitializeLibrary)(sp<Library>);
		fpInitializeLibrary=(void (*)(sp<Library>))
				pLoader->findDLFunction("InitializeLibrary");

#pragma GCC diagnostic pop

		if(fpInitializeLibrary)
		{
			fpInitializeLibrary(spLibrary);
		}
	}

	return result;
}

Result Registry::manage(sp<Library>& a_rspLibrary)
{
	SAFEGUARD;

	a_rspLibrary->setRegistry(sp<Registry>(this));

	LoadedLibrary *pLoadedLibrary=new LoadedLibrary(a_rspLibrary);
	m_loaderStack.push_back(pLoadedLibrary);

	Array<String> manifest;
	a_rspLibrary->populateManifest(manifest);
	U32 m;
	U32 size=manifest.size();
	for(m=0;m<size;m++)
	{
		const String &name=manifest[m];

#if FE_REGISTRY_FACTORY_DEBUG
		feLog("  registering factory \"%s\"\n",name.c_str());
#endif

		std::map<String,FactoryLocation*>::iterator it=m_factoryMap.find(name);
		if(it!=m_factoryMap.end())
		{
			FactoryLocation *pLocation=it->second;
			feLog("WARNING redundant factory \"%s\"\n"
					"  found in \"%s\"\n"
					"  previously registered by \"%s\"\n",
					name.c_str(),
					a_rspLibrary->name().c_str(),
					pLocation->m_spLibrary->name().c_str());
		}
		else
		{
			FactoryLocation* pFactoryLocation=
					new FactoryLocation(a_rspLibrary,m);

			m_factoryMap[name]=pFactoryLocation;
			pLoadedLibrary->m_factoriesAccepted++;

#if FE_REGISTRY_FACTORY_DEBUG
			feLog("  %p\n",pFactoryLocation);
#endif
		}
	}

	return e_ok;
}

I32 Registry::listAvailable(const String &pattern,
	Array<String>& available) const
{
	SAFEGUARD;

	I32 count=0;

#if FE_REGISTRY_MATCH_DEBUG
	feLog("Registry::listAvailable(\"%s\") searching\n",pattern.c_str());
#endif

	std::map<String,FactoryLocation*>::const_iterator it=m_factoryMap.begin();
	while(it!=m_factoryMap.end())
	{
		const String &factoryName= it->first;

#if FE_REGISTRY_MATCH_DEBUG
		feLog("  vs. \"%s\"\n",factoryName.c_str());
#endif

		if(factoryName.dotMatch(pattern))
		{
#if FE_REGISTRY_MATCH_DEBUG
			feLog("  MATCHED\n");
#endif

			available.push_back(factoryName);
			count++;
		}
		it++;
	}

	return count;
}

void Registry::prioritize(const String &a_implementation,I32 a_priority)
{
	SAFEGUARD;

	std::map<String,FactoryLocation*>::iterator it=
			m_factoryMap.find(a_implementation);
	if(it==m_factoryMap.end())
	{
		feLog("Registry::prioritize \"%s\" not found\n",
				a_implementation.c_str());
		return;
	}
	m_factoryMap[a_implementation]->m_priority=a_priority;
}

sp<Component> Registry::create(const String& a_pattern,BWORD quiet) const
{
	SAFEGUARD;

	return createInternal(&a_pattern,NULL,quiet);
}

sp<Component> Registry::create(const char* a_pattern,BWORD quiet) const
{
	SAFEGUARD;

	const String temp(a_pattern);
	return createInternal(&temp,NULL,quiet);
}

sp<Component> Registry::create(Regex& a_regex,BWORD quiet) const
{
	SAFEGUARD;

	return createInternal(NULL,&a_regex,quiet);
}

sp<Counted> Registry::create(String implementation,String name) const
{
	sp<Component> spComponent=create(implementation);
	if(spComponent.isValid())
	{
		spComponent->setName(name);
	}
	return spComponent;
}

void Registry::substituteLibrary(const String &originalDlName,
	const String &newDlName)
{
	SAFEGUARD;

	Array<String> manifestKeys;
	m_spManifest->catalogKeys(manifestKeys);
	const U32 manifestKeyCount=manifestKeys.size();

	//* Search the manifest for references to the old DL name
	for(U32 keyIndex=0;keyIndex<manifestKeyCount;keyIndex++)
	{
		const String &manifestKey=manifestKeys[keyIndex];

		if(m_spManifest->catalog<String>(manifestKey).search(
				originalDlName+"(-\\d+)?$"))
		{
			//* Check that we aren't reloading the same DL
			if(m_spManifest->catalog<String>(manifestKey) == newDlName)
			{
				return;
			}

#if FE_REGISTRY_SUBSTITUTE_DEBUG
			feLog("Registry::substituteLibrary substituting DL %s with %s\n",
					m_spManifest->catalog<String>(manifestKey).c_str(),
					newDlName.c_str());
#endif

			//* Update the manifest to use the new DL
			m_spManifest->catalogSet(manifestKey,newDlName);

			//* Remove factory location so a new one is forced to be created
			//* (which loads the DL in the process)
			m_factoryMap.erase(manifestKey);
		}
	}
}

sp<Component> Registry::createInternal(const String* a_pPattern,
	Regex* a_pRegex,BWORD quiet) const
{
#if FE_REGISTRY_MATCH_DEBUG
	feLog("Registry::create(\"%s\") searching\n",
			a_pPattern? a_pPattern->c_str(): "<REGEX>");
	quiet=FALSE;
#endif

	List<Candidate> candidateList;

	// TODO fast check for exact match
	// TODO store each new dot-match as a potential future exact match
	std::map<String,FactoryLocation*>::const_iterator it=m_factoryMap.begin();
	while(it!=m_factoryMap.end())
	{
		const String &factoryName= it->first;

#if FE_REGISTRY_MATCH_DEBUG
		feLog("  vs. \"%s\"\n",factoryName.c_str());
#endif

		if((a_pPattern && factoryName.dotMatch(*a_pPattern)) ||
				(a_pRegex && a_pRegex->match(factoryName.c_str())))
		{
			FactoryLocation *pLocation=it->second;
			const I32 priority=pLocation->m_priority;

#if FE_REGISTRY_MATCH_DEBUG
			feLog("  MATCHED priority %d\n",priority);
#endif

			List<Candidate>::Iterator iterator(candidateList);
			iterator.toHead();
			while(!iterator.isAtTailNull())
			{
				if((*iterator).m_priority<priority)
				{
					break;
				}
				iterator++;
			}

			Candidate candidate;
			candidate.m_pFactoryLocation=pLocation;
			candidate.m_factoryName=factoryName;
			candidate.m_priority=priority;

			iterator.insertBefore(candidate);
		}
		it++;
	}

	if(m_spManifest.isValid())
	{
		Array<String> keys;
		m_spManifest->catalogKeys(keys);
		const U32 keyCount=keys.size();
		for(U32 keyIndex=0;keyIndex<keyCount;keyIndex++)
		{
			const String& key=keys[keyIndex];
			const String& dlname=m_spManifest->catalog<String>(key);

#if FE_REGISTRY_MATCH_DEBUG
			feLog("manifest \"%s\" \"%s\"\n",key.c_str(),dlname.c_str());
#endif

			//* skip components already loaded
			if(findIndex(dlname)>=0)
			{
#if FE_REGISTRY_MATCH_DEBUG
				feLog("  SKIP\n");
#endif

				continue;
			}

			const I32 priority=
					m_spManifest->catalogOrDefault<I32>(key,"priority",0);

			if((a_pPattern && key.dotMatch(*a_pPattern)) ||
					(a_pRegex && a_pRegex->match(key.c_str())))
			{
#if FE_REGISTRY_MATCH_DEBUG
				feLog("  MATCHED priority %d\n",priority);
#endif

				List<Candidate>::Iterator iterator(candidateList);
				iterator.toHead();
				while(!iterator.isAtTailNull())
				{
					if((*iterator).m_priority<priority)
					{
						break;
					}
					iterator++;
				}

				Candidate candidate;
				candidate.m_dlname=dlname;
				candidate.m_factoryName=key;
				candidate.m_priority=priority;

				iterator.insertBefore(candidate);
			}
		}
	}

	List<Candidate>::Iterator iterator(candidateList);

#if FE_REGISTRY_MATCH_DEBUG
	feLog("  %d Candidates:\n",candidateList.size());
	iterator.toHead();
	while(!iterator.isAtTailNull())
	{
		FactoryLocation *pLocation=(*iterator).m_pFactoryLocation;
		if(pLocation)
		{
			feLog("    %d \"%s\"\n",pLocation->m_priority,
					pLocation->m_spLibrary->factoryName(
					pLocation->m_factoryIndex).c_str());
		}

		iterator++;
	}
#endif

	sp<Component> spComponent;

	iterator.toHead();
	while(!iterator.isAtTailNull())
	{
		const Candidate& rCandidate=(*iterator);

		FactoryLocation *pLocation=rCandidate.m_pFactoryLocation;
		if(pLocation)
		{
			spComponent=pLocation->m_spLibrary->create(
					pLocation->m_factoryIndex);

			if(spComponent.isValid())
			{
				spComponent->setName(rCandidate.m_factoryName);
				break;
			}
		}
		else
		{
			const Result result=
					const_cast<Registry*>(this)->manage(rCandidate.m_dlname);

			//* only if just loaded
			if(result==e_ok)
			{
				if(System::getVerbose()=="all")
				{
					feLog("autoload %s (for \"%s\")\n",
							rCandidate.m_dlname.c_str(),
							a_pPattern? a_pPattern->c_str(): "<REGEX>");
				}

				return createInternal(a_pPattern,a_pRegex,quiet);
			}
			else if(failure(result))
			{
				//* TODO purge dlname from manifest

#if FE_REGISTRY_MATCH_DEBUG
				feLog("Registry::create(\"%s\") failed to manage \"%s\"\n",
						a_pPattern? a_pPattern->c_str(): "<REGEX>",
						rCandidate.m_dlname.c_str());
#endif
			}
		}
		iterator++;
	}

	if(!quiet && spComponent.isNull())
	{
		feLog("Registry::create(\"%s\") found no usable match\n",
				a_pPattern? a_pPattern->c_str(): "<REGEX>");
	}

	return spComponent;
}

} /* namespace */
