/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __plugin_CatalogWriterI_h__
#define __plugin_CatalogWriterI_h__

namespace fe
{

class FE_DL_EXPORT CatalogWriterI:
	virtual public Component,
	public CastableAs<CatalogWriterI>
{
	public:

virtual	BWORD	save(String a_filename, sp<Catalog> spCatalog)				=0;
};

} /* namespace fe */

#endif /* __plugin_CatalogWriterI_h__ */
