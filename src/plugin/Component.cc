/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <plugin/plugin.pmh>

#define	FE_COMPONENT_UNLOAD	TRUE	//* turn off to retain symbols (valgrind)

namespace fe
{

Component::Component(void):
	m_factoryIndex(-1),
	m_singleton(FALSE),
	m_stray(FALSE)
{
}

Component::~Component(void)
{
	//* check for unnamed components (not usually turned on)
//	FEASSERT(!name().empty());

//	feLog("~Component() \"%s\"\n",verboseName().c_str());

	disjoin();
	setStray(FALSE);
}

Component::Hub::Hub(void):
	m_name("Component::Hub"),
	m_singletonCount(0)
{
#if FE_COUNTED_TRACK
	suppressReport();
#endif
}

Component::Hub::~Hub(void)
{
//	feLog("~Hub \"%s\"\n",verboseName().c_str());

	abandonHandle();

#if FE_COUNTED_TRACK
	List< sp<Component> >::Iterator iterator(m_persistList);
	sp<Component> spComponent;
	iterator.toHead();
	while((spComponent= *iterator++).isValid())
	{
//		feLog("~Hub \"%s\" untrack \"%s\"\n",verboseName().c_str(),
//				spComponent->verboseName().c_str());
//		spComponent->untrackReference(static_cast<Counted*>(this));
	}

//	feLog("~Hub \"%s\" %p count=%d listed=%d\n",
//			verboseName().c_str(),this,count(),m_persistList.size());
#endif
}

void Component::Hub::attach(Component* pComponent,BWORD singleton)
{
#if FE_COUNTED_TRACK
//	sp<Component>* pspComponent =
#endif
			m_persistList.append(sp<Component>(pComponent));

#if 0 //FE_COUNTED_TRACK
	pComponent->trackReference(pspComponent,
			"sp<> Component::Hub::attach");
#endif

	if(singleton)
	{
		m_singletonCount++;
	}

	setName(name()+" "+pComponent->name());
}

void Component::Hub::detach(Component* pComponent,BWORD singleton)
{
	setName(name()+" -"+pComponent->name());

#if FE_COUNTED_TRACK
	pComponent->untrackReference(static_cast<Counted*>(this));
#endif

#if FE_CODEGEN <= FE_DEBUG
	BWORD removed=
#endif
	m_persistList.remove(sp<Component>(pComponent));
	FEASSERT(removed);

	if(singleton)
	{
		m_singletonCount--;
	}
}

void Component::setStray(BWORD stray)
{
//	feLog("Component::setStray(%d) %d %s\n",
//			stray,m_stray,verboseName().c_str());

	if(m_spLibrary.isValid())
	{
		if(stray && !m_stray)
		{
			m_spLibrary->adjustStrayCount(1);
		}
		else if(!stray && m_stray)
		{
			m_spLibrary->adjustStrayCount(-1);
		}
	}

	m_stray=stray;
}

void Component::adjoin(sp<Component> spComponent)
{
#if FE_COUNTED_MT
	//* not sure it's necessary here, but it seems like a good idea
	RecursiveMutex::Guard guard(Counted::ms_counted_mutex);
#endif

	if(!spComponent.isValid())
	{
		feLog("Component::adjoin invalid component");
		return;
	}

	if(m_hpHub.isValid())
	{
		String errmsg;
		errmsg.sPrintf("Component::adjoin \"%s\" already adjoined\n",
				verboseName().c_str());
		feX(errmsg.c_str());
	}

	Component* pOther=spComponent.raw();
	if(!pOther->m_hpHub.isValid())
	{
		Hub* pHub=new Hub();
		pHub->registerRegion(pHub,sizeof(Hub));
		pHub->setName("Component::Hub");

		pOther->assignHub(pHub);
	}

	assignHub((Hub*)pOther->m_hpHub.raw());
}

void Component::disjoin(void)
{
	if(m_hpHub.isValid())
	{
		hp<Hub> hpHub=m_hpHub;
		m_hpHub=(Hub*)NULL;

		I32 references=count();

		setStray(FALSE);

		//* NOTE deletion may occur here
		hpHub->detach(this,isSingleton());

		if(hpHub.isValid())
		{
			if(references>1)
			{
//				feLog("Component::disjoin release Hub from \"%s\"\n",
//						verboseName().c_str());
				hpHub->release();
				hpHub->untrackReference(this);
			}
		}
	}
}

void Component::assignHub(Hub* pHub)
{
	if(count()>1)
	{
		pHub->acquire();
	}
	if(count()<2+isSingleton())
	{
		setStray(TRUE);
	}

	m_hpHub=pHub;
	pHub->attach(this,isSingleton());
}

String Component::factoryName(void)
{
	if(m_spLibrary.isNull() || m_factoryIndex<0)
	{
		return "";
	}

	return m_spLibrary->factoryName(m_factoryIndex);
}

void Component::setLibrary(sp<Library> spLibrary)
{
	//* assure library lifetime while this component exists
	FEASSERT(!m_spLibrary.isValid());
	if(!m_spLibrary.isValid())
	{
		m_spLibrary=spLibrary;
	}
}

hp<Registry> Component::registry(void) const
{
	return m_spLibrary.isValid()? m_spLibrary->registry(): hp<Registry>();
}

sp<Component> Component::create(const String &implementation,
	BWORD quiet) const
{
	if(!m_spLibrary.isValid())
	{
		return sp<Component>(NULL);
	}
	sp<Component> spComponent = registry()->create(implementation,quiet);
	return spComponent;
}

namespace internal
{
void checkUnload(sp<Library>& spLibrary,Component* pComponent)
{
	sp<Library> spChainLibrary;
	DL_Loader* pLoader=NULL;
	if(spLibrary.isValid() && spLibrary->adjustedReferences()==1)
	{
		pLoader=spLibrary->getLoader();
		spChainLibrary=spLibrary->getChainLibrary();
	}

	if(pComponent)
	{
		if(spLibrary.isValid() && !spLibrary->registry().isValid())
		{
			if(System::getVerbose()=="all")
			{
				feLog("fe::internal::checkUnload \"%s\"\n"
						"  NULL Registry -> not deleting component\n",
						spLibrary->name().c_str());
			}
			return;
		}
		else
		{
			delete pComponent;
		}
	}
	else
	{
		spLibrary=NULL;
	}

	while(pLoader)
	{
#if FE_CODEGEN<=FE_DEBUG
		feLog("fe::internal::checkUnload closeDL \"%s\"\n",
				pLoader->name().c_str());
#endif

		//* TODO check if this is ever used and if spLibrary is valid

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wconditionally-supported"

		void (*fpDestroyLibrary)(sp<Master>);
		fpDestroyLibrary=(void (*)(sp<Master>))
				pLoader->findDLFunction("DestroyLibrary");

#pragma GCC diagnostic pop

		if(fpDestroyLibrary)
		{
			FEASSERT(spLibrary.isValid());
			fpDestroyLibrary(spLibrary->registry()->master());
		}

#if	FE_COMPONENT_UNLOAD
		pLoader->closeDL();
#endif

		delete pLoader;
		pLoader=NULL;

		if(spChainLibrary.isValid() && spChainLibrary->adjustedReferences()==1)
		{
			pLoader=spChainLibrary->getLoader();
			spChainLibrary=spChainLibrary->getChainLibrary();
		}
	}
}

} // namespace internal

} // namespace fe
