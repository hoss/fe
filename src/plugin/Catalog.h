/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __core_Catalog_h__
#define __core_Catalog_h__

#define FE_CAT_SPEC(name, description)	name
#define FE_CAT(name)					name

namespace fe
{

/**************************************************************************//**
	@brief Dictionary of Arbitrary Instances

	@ingroup plugin

	TODO allow reordering
	TODO allow individual removal (remember to also remove from order map)
*//***************************************************************************/
class FE_DL_EXPORT Catalog:
	virtual public Component,
	public CastableAs<Catalog>
{
	public:

	class Remove
	{
		public:
			bool	operator==(const Remove &other) const { return true; }
	};

							Catalog(void);
virtual						~Catalog(void);

							/** @brief Returns a mirror of the catalog

								All values shared references with the original.
							*/
		sp<Catalog>			catalogShallowCopy(void);

							/** @brief Returns a duplicate of the catalog

								All values are also copied by assignment.
							*/
		sp<Catalog>			catalogDeepCopy(void);

							/** @brief Returns the number of name keys

								Each name key may have any number of properties.
							*/
		U32					catalogSize(void) const;

							/** @brief Returns a key by ordered index

								By default, keys are index in creation order.
							*/
		String				catalogKey(U32 index) const;

							/** @brief Returns keys in order of creation

								Keys are appended to the given array.
								Clear the array if only want new values.

								If a regex pattern is given,
								only matching keys are returned.

								Future improvements may allow reordering.
							*/
		void				catalogKeys(Array<String> &a_keys,
								String a_pattern=".*") const;

							/** @brief Returns properties for a key

								Properties are appended to the given array.
								Clear the array if only want new values.

								Order is arbitrary.
							*/
		void				catalogProperties(String a_name,
									Array<String> &a_properties) const;

							/** @brief Returns TRUE if the given property for
								the given name has been created.

								The default property is 'value'.
							*/
		BWORD				cataloged(String a_name,
									String a_property="value") const;

							/** @brief Returns TRUE if any property for
								the given name has been created.
							*/
		BWORD				catalogedAny(String a_name) const;

							/// @brief Remove all entries.
		void				catalogClear(void);

							/** @brief Duplicates entries from another catalog.

								Optionally, a list of type names limits which
								entries are copied.
							*/
		void				catalogOverlay(sp<Catalog> a_spOtherCatalog,
									Array<String>* a_pTypeList=NULL,
									BWORD a_shallow=FALSE);

							/** @brief Returns a component entry of the given
								property for the given name.

								If the entry does not exist,
								it may be created using the given
								implementation name.

								The return value will be invalid if the
								implementation could not be instantiated.
							*/
		sp<Component>		catalogComponent(String implementation,
									String a_name,String a_property="value");

							/** @brief Returns a component entry of the given
								property for the given name.

								Regardless of whether the entry previously
								existed, it is replaced by the given Component.

								The default property if 'value'.
							*/
		sp<Component>		catalogComponent(sp<Component> a_spComponent,
									String a_name,String a_property="value");

							/** @brief Returns the literal Instance of the
								given property for the given name.

								Regardless of whether the entry previously
								existed, it is replaced by the given Instance.

								The default property is 'value'.
							*/
		Instance&			catalogInstance(Instance &instance,
									String a_name,String a_property="value");

							/** @brief Returns the literal Instance of the
								given property for the given name.

								If the entry does not exist, it will be created.

								The default property is 'value'.
							*/
		Instance&			catalogInstance(String a_name,
									String a_property="value");

							/** @brief Returns the type_info of the
								given property for the given name.

								If the entry does not exist, it will be created.

								The default property is 'value'.
							*/
const	fe_type_info&		catalogTypeInfo(String a_name,
									String a_property="value") const;
							/** @brief Returns the type name of the
								given property, determined by the TypeMaster

								If the entry does not exist, it will be created.

								The default property is 'value'.
							*/
const	String				catalogTypeName(String a_name,
									String a_property="value") const;

							/** @brief Returns TRUE if
								given catalog entry match the template type
							*/
		template <class T>
		BWORD				catalogTypeIs(String a_name,
									String a_property="value") const;

							/** @brief Returns existing catalog entry
								if already set or a default if not

								If entry does not exist, this version will
								create an entry and set it to the appropriate
								Type default.

								The default property is 'value'.
							*/
		template <class T>
		T&					catalog(String a_name,String a_property="value");

							/** @brief Returns existing catalog entry
								if already set or default if not

								If entry does not exist, this version will
								create an entry and set it to the given default.
							*/
		template <class T>
		T&					catalog(String a_name,String a_property,
									const T& a_default);
//		template <class T>
//		T&					catalog(String a_name,const T& a_default)
//							{	return catalog(a_name,"value",a_default); }

							/** @brief Returns existing catalog entry
								if already set or default if not

								If entry does not exist, this version will
								not create an entry and simply return
								the given default.

								The default property is 'value'.
							*/
		template <class T>
const	T&					catalogOrDefault(String a_name,String a_property,
									const T& a_default) const;
		template <class T>
const	T&					catalogOrDefault(String a_name,
									const T& a_default) const
							{	return catalogOrDefault(a_name,
										"value",a_default); }

							/** @brief Returns existing catalog entry,
								converted to a String

								If entry does not exist,
								an empty String is returned.

								The default property is 'value'.
							*/
		String				catalogValue(String a_name,
									String a_property="value") const;

							/** @brief Converts a catalog entry,
								into raw binary

								Returns the number of bytes in the buffer.

								If entry does not exist,
								zero bytes is returned.
							*/
		I32					catalogBytes(String a_name,
									String a_property,
									Array<U8>& a_rByteArray) const;

							/** @brief Sets the value for a catalog entry,
								converted to the existing type,
								from a given String

								If entry does not exist,
								it is not created.

								The default property is 'value'.
							*/
		BWORD				catalogSet(String a_name,
									String a_property,
									String a_value);
		BWORD				catalogSet(String a_name,
									String a_value)
							{	return catalogSet(a_name,
										"value",a_value); }

							/** @brief Sets the value for a catalog entry,
								converted to the existing type,
								from a given String

								If entry does not exist,
								it is created using the named type.

								If the Instance pointer pointer is not null.
								the contained pointer is set to point to
								the instance.
								This can save the time of immediately doing
								another map lookup, if you need the instance.
							*/
		BWORD				catalogSet(String a_name,
									String a_property,
									String a_type,
									String a_value,
									Instance** a_ppInstance=NULL);

							/** @brief Sets the value for a catalog entry,
								using a raw byte block

								If entry does not exist, it is created.

								If the Instance pointer pointer is not null.
								the contained pointer is set to point to
								the instance.
								This can save the time of immediately doing
								another map lookup, if you need the instance.
							*/
		BWORD				catalogSet(String a_name,
									String a_property,
									String a_type,
									const U8* a_pRawBytes,
									I32 a_byteCount,
									Instance** a_ppInstance=NULL);

							/** @brief Returns an existing catalog entry,
								if it exists

								If entry does not exist, this version will
								throw an exception.

								The default property is 'value'.
							*/
		template <class T>
		T&					catalogOrException(String a_name,
									String a_property) const;

							/** @brief Gets the Instance of
								an existing catalog entry, if it exists

								If entry does not exist,
								the method return false.

								The default property is 'value'.
							*/
		bool				catalogLookup(const String a_name,
									String a_property,
									Instance &a_instance) const;
		bool				catalogLookup(const String a_name,
									Instance &a_instance) const
							{	return catalogLookup(a_name,"value",
										a_instance); }

							///	@brief Move all properties for name to bottom
		void				catalogMoveToEnd(String a_name);

							///	@brief Remove all properties for a name
		void				catalogRemove(String a_name);

							///	@brief Remove one property for a name
		void				catalogRemove(String a_name,String a_property);

							/** @brief Print a description of the catalog
								to the log
							*/
		void				catalogDump(void) const;

		sp<TypeMaster>		typeMaster(void) const;

	private:

		template<class T>
		T&					createInstance(String a_name,
									String a_property="value");

		std::map< String, InstanceMap >	m_instanceMapMap;
		std::map< I32, String >			m_orderMap;
		I32								m_highest;
};

template<class T>
T& Catalog::createInstance(String a_name,String a_property)
{
	if(!catalogedAny(a_name))
	{
		m_orderMap[++m_highest]=a_name;
	}
	T& rT=m_instanceMapMap[a_name][a_property].create<T>(typeMaster());
#if FE_COUNTED_TRACK
	Counted::trackReference(&rT,this,"Catalog "+FE_TYPESTRING(T));
#endif
	return rT;
}

template<class T>
T &Catalog::catalog(String a_name,String a_property)
{
	if(!cataloged(a_name,a_property))
	{
		return createInstance<T>(a_name,a_property);
	}
	return m_instanceMapMap[a_name][a_property].cast<T>();
}

template<class T>
T &Catalog::catalog(String a_name,String a_property,const T& a_default)
{
	if(!cataloged(a_name,a_property))
	{
		createInstance<T>(a_name,a_property)=a_default;
	}
	return m_instanceMapMap[a_name][a_property].cast<T>();
}

template<class T>
const T& Catalog::catalogOrDefault(String a_name,String a_property,
	const T& a_default) const
{
	std::map<String, InstanceMap>::const_iterator it=
			m_instanceMapMap.find(a_name);
	if(it==m_instanceMapMap.end())
	{
		return a_default;
	}

	const std::map<String, Instance>& instanceMap=it->second;

	std::map<String, Instance>::const_iterator it2=
			instanceMap.find(a_property);
	if(it2==instanceMap.end())
	{
		return a_default;
	}

	return it2->second.cast<T>();
}

template<class T>
T &Catalog::catalogOrException(String a_name,String a_property) const
{
	std::map<String, InstanceMap>::const_iterator it=
			m_instanceMapMap.find(a_name);
	if(it==m_instanceMapMap.end())
	{
		feX("Catalog::catalogOrException",
				("missing key '" + a_name + "'").c_str());
	}

	const std::map<String, Instance>& instanceMap=it->second;

	std::map<String, Instance>::const_iterator it2=
			instanceMap.find(a_property);
	if(it2==instanceMap.end())
	{
		feX("Catalog::catalogOrException",
				("missing property '" + a_property +
				"' for '" + a_name + "'").c_str());
	}

	return it2->second.cast<T>();
}

inline sp<Component> Catalog::catalogComponent(sp<Component> a_spComponent,
		String a_name,String a_property)
{
	sp<Component>& rspComponent=
			createInstance< sp<Component> >(a_name,a_property);

	rspComponent=a_spComponent;

	return a_spComponent;
}

inline Instance &Catalog::catalogInstance(String a_name,String a_property)
{
	if(!catalogedAny(a_name))
	{
		m_orderMap[++m_highest]=a_name;
	}
	return m_instanceMapMap[a_name][a_property];
}

inline Instance &Catalog::catalogInstance(Instance &instance,String a_name,
		String a_property)
{
	if(!catalogedAny(a_name))
	{
		m_orderMap[++m_highest]=a_name;
	}
	m_instanceMapMap[a_name][a_property]=instance;
	return m_instanceMapMap[a_name][a_property];
}

inline const fe_type_info& Catalog::catalogTypeInfo(String a_name,
		String a_property) const
{
	std::map<String, InstanceMap>::const_iterator it=
			m_instanceMapMap.find(a_name);
	if(it==m_instanceMapMap.end())
	{
		return getTypeId<void>();
	}

	const std::map<String, Instance>& instanceMap=it->second;

	std::map<String, Instance>::const_iterator it2=
			instanceMap.find(a_property);
	if(it2==instanceMap.end())
	{
		return getTypeId<void>();
	}

	const Instance& rInstance=it2->second;
	return rInstance.type()->typeinfo().ref();
}

inline const String Catalog::catalogTypeName(String a_name,
		String a_property) const
{
//	const Instance& rInstance=catalogInstance(a_name,a_property);
	if(!cataloged(a_name,a_property))
	{
		return "";
	}
	const Instance& rInstance=m_instanceMapMap.at(a_name).at(a_property);

	const sp<BaseType> spBaseType=rInstance.type();
	if(spBaseType.isNull())
	{
		feX("Catalog::catalogTypeName","invalid Instance");
		return "<unknown>";
	}

	String typeName=rInstance.type()->typeinfo().ref().name();

	std::list<String> typeNames;
	typeMaster()->reverseLookup(spBaseType,typeNames);
	if(typeNames.size()>0)
	{
		typeName= *(typeNames.begin());
	}

	return typeName;
}

template<class T>
inline BWORD Catalog::catalogTypeIs(String a_name,
		String a_property) const
{
	return (catalogTypeInfo(a_name,a_property)==getTypeId<T>());
}

inline BWORD Catalog::catalogedAny(String a_name) const
{
	std::map<String, InstanceMap>::const_iterator it=
			m_instanceMapMap.find(a_name);

	return it!=m_instanceMapMap.end();
}

inline BWORD Catalog::cataloged(String a_name,String a_property) const
{
	std::map<String, InstanceMap>::const_iterator it=
			m_instanceMapMap.find(a_name);

	return it!=m_instanceMapMap.end() &&
			it->second.find(a_property) != it->second.end();
}

inline void Catalog::catalogMoveToEnd(String a_name)
{
	for(std::map< I32, String >::iterator it = m_orderMap.begin();
			it != m_orderMap.end(); it++)
	{
		if(it->second==a_name)
		{
			m_orderMap.erase(it->first);
			m_orderMap[++m_highest]=a_name;
			return;
		}
	}
}

inline void Catalog::catalogRemove(String a_name)
{
#if FE_COUNTED_TRACK
	std::map<String, InstanceMap>::iterator i_name=
			m_instanceMapMap.find(a_name);

	if(i_name!=m_instanceMapMap.end())
	{
		InstanceMap& rInstanceMap=i_name->second;
		for(std::map< String, Instance >::iterator i_prop=rInstanceMap.begin();
				i_prop!=rInstanceMap.end(); i_prop++)
		{
			Counted::untrackReference(i_prop->second.data(),this);
		}
	}
#endif
	m_instanceMapMap.erase(a_name);

	for(std::map< I32, String >::iterator it = m_orderMap.begin();
			it != m_orderMap.end(); it++)
	{
		if(it->second==a_name)
		{
			m_orderMap.erase(it->first);
			break;
		}
	}
}

inline void Catalog::catalogRemove(String a_name,String a_property)
{
	std::map<String, InstanceMap>::iterator i_name=
			m_instanceMapMap.find(a_name);

	if(i_name!=m_instanceMapMap.end())
	{
		InstanceMap& rInstanceMap=i_name->second;

		std::map<String, Instance>::const_iterator i_prop=
				rInstanceMap.find(a_property);
		if(i_prop!=rInstanceMap.end())
		{
#if FE_COUNTED_TRACK
			Counted::untrackReference(i_prop->second.data(),this);
#endif
			rInstanceMap.erase(a_property);
		}
	}
}

inline void Catalog::catalogClear(void)
{
#if FE_COUNTED_TRACK
	for(std::map< String, InstanceMap >::iterator i_name=
			m_instanceMapMap.begin(); i_name!=m_instanceMapMap.end(); i_name++)
	{
		InstanceMap& rInstanceMap=i_name->second;
		for(std::map< String, Instance >::iterator i_prop=rInstanceMap.begin();
				i_prop!=rInstanceMap.end(); i_prop++)
		{
			Counted::untrackReference(i_prop->second.data(),this);
		}
	}
#endif
	m_instanceMapMap.clear();
	m_orderMap.clear();
}

class FE_DL_EXPORT SafeCatalog:
	public ObjectSafeShared<SafeCatalog>,
	virtual public Catalog
{
	public:
							SafeCatalog(void)								{}
virtual						~SafeCatalog(void)								{}
};

} /* namespace */

#endif /* __core_Catalog_h__ */
