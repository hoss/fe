/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __data_h__
#define __data_h__

#include "fe/plugin.h"


#ifdef MODULE_data
#define FE_DATA_PORT FE_DL_EXPORT
#else
#define FE_DATA_PORT FE_DL_IMPORT
#endif

#define FE_SB 1
#define FE_AV 2
#define FE_DATA_STORE FE_AV
//#define FE_AV_FASTITER_ENABLE
//#define FE_MAKE_RECORD_FOR_WEAKRECORD


#include "data/Attribute.h"
#include "data/Depend.h"
#include "data/DynamicBitset.h"
#include "data/Layout.h"
#include "data/LayoutSB.h"
#include "data/LayoutAV.h"
#include "data/StoreI.h"
#include "data/RecordSB.h"
#include "data/RecordAV.h"
#include "data/Record.h"
#include "data/WeakRecordSB.h"
#include "data/WeakRecordAV.h"
#include "data/WeakRecord.h"
#include "data/PopulateI.h"

//* for watching (there should be a Watchable base class)
namespace fe
{
class RecordGroup;
}

#include "data/RecordArraySB.h"
#include "data/RecordArrayAV.h"
#include "data/RecordArray.h"
#include "data/Scope.h"
#include "data/WatcherI.h"
#include "data/RecordFactoryI.h"
#include "data/RecordCookbook.h"
#include "data/Accessor.h"
#include "data/PathAccessor.h"
#include "data/RecordMap.h"
#include "data/RecordGroup.h"
#include "data/RecordOperation.h"
#include "data/AccessorSet.h"
#include "data/SegmentStore.h"
#include "data/AccessorSets.h"
#include "data/Reader.h"
#include "data/Scanner.h"
#include "data/Writer.h"
#include "data/BinaryReader.h"
#include "data/AsciiReader.h"
#include "data/BinaryWriter.h"
#include "data/AsciiWriter.h"
#include "data/Stream.h"
#include "data/Cloner.h"
#include "data/Omega.h"

namespace fe
{
FE_DL_EXPORT Library* CreateDataLibrary(sp<Master> spMaster);
}


#endif /* __data_h__ */
