/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __data_WeakRecord_h__
#define __data_WeakRecord_h__

namespace fe
{

#if FE_DATA_STORE==FE_SB
typedef WeakRecordSB WeakRecord;
typedef WeakRecordSBInfo WeakRecordInfo;
#endif

#if FE_DATA_STORE==FE_AV
typedef WeakRecordAV WeakRecord;
typedef WeakRecordAVInfo WeakRecordInfo;
#endif

} /* namespace */

#endif /* __data_WeakRecord_h__ */

