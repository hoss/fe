/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __data_Record_h__
#define __data_Record_h__

namespace fe
{

#if FE_DATA_STORE==FE_SB
typedef RecordSB Record;
typedef RecordSBInfo RecordInfo;
#endif

#if FE_DATA_STORE==FE_AV
typedef RecordAV Record;
typedef RecordAVInfo RecordInfo;
#endif


}

#endif /* __data_Record_h__ */

