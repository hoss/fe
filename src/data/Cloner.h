/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __data_Cloner_h__
#define __data_Cloner_h__

namespace fe
{

/**	Deep cloning tool
	*/
class FE_DL_EXPORT Cloner : public fe::data::Scanner
{
	public:
		Cloner(sp<Scope> spScope);
virtual	~Cloner(void);

		Record				clone(Record r_src);
		sp<RecordGroup>		clone(sp<RecordGroup> rg_src);
		sp<RecordGroup>		cloneShallow(sp<RecordGroup> rg_src);

	private:
		void				deepclone(void);

		std::map< int, sp<RecordGroup> >	m_new_rgs;
		std::map< int, sp<RecordArray> >	m_new_ras;

		void				handleRecord(Record r_src);


		struct RecordWiringInfo
		{
			Record					m_record;
			Accessor<Record>		m_aRecord;
			FE_UWORD					m_idr;
		};
		typedef std::list<RecordWiringInfo>			t_wiring;

		struct WeakRecordWiringInfo
		{
			Record					m_record;
			Accessor<WeakRecord>	m_aRecord;
			FE_UWORD				m_idr;
		};
		typedef std::list<WeakRecordWiringInfo>			t_wk_wiring;

		t_wiring					m_wiringList;
		t_wk_wiring					m_wkWiringList;
		std::map< FE_UWORD, Record>	m_src_idr_to_dst_record;
};



} /* namespace */

#endif /* __data_Cloner_h__ */
