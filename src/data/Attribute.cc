/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <data/data.pmh>

#define FE_SUPPRESS_ATTRIBUTE	TRUE

namespace fe
{

Attribute::Attribute(void)
{
	m_name = "--unset--";
	m_spType = NULL;
	m_serialize = true;
	m_cloneable = true;
#if FE_SUPPRESS_ATTRIBUTE
	suppressReport();
#endif
#if FE_COUNTED_TRACK
	Counted::registerRegion(this,sizeof(Attribute));
#endif
}

Attribute::Attribute(const String name, sp<BaseType> type, sp<TypeMaster> spTM)
{
	if(! type.isValid())
	{
		feX("fe::Attribute::Attribute","attempt construction with NULL type");
	}
	m_spType = type;
	m_name = name;
	m_serialize = true;
	m_cloneable = true;
	m_spTypeMaster = spTM;
	m_dead.create(m_spType);
#if FE_SUPPRESS_ATTRIBUTE
	suppressReport();
#endif
#if FE_COUNTED_TRACK
	Counted::registerRegion(this,sizeof(Attribute));
#endif
}

Attribute::Attribute(const Attribute &other):
	Castable(),
	Counted(),
	ClassSafe<Attribute>()
{
	m_name = other.m_name;
	m_spType = other.m_spType;
	m_spTypeMaster = other.m_spTypeMaster;
	m_serialize = true;
	m_cloneable = true;
	m_default = other.m_default;
	m_dead.create(m_spType);
#if FE_SUPPRESS_ATTRIBUTE
	suppressReport();
#endif
#if FE_COUNTED_TRACK
	Counted::registerRegion(this,sizeof(Attribute));
#endif
}

Attribute::~Attribute(void)
{
}

Attribute &Attribute::operator=(const Attribute &other)
{
	if(&other == this) { return *this; }
	m_name = other.m_name;
	m_spType = other.m_spType;
	m_spTypeMaster = other.m_spTypeMaster;
	m_serialize = other.m_serialize;
	m_cloneable = other.m_cloneable;
	m_default = other.m_default;
	m_dead.create(m_spType);
	return *this;
}

void Attribute::peek(Peeker &peeker)
{
	peeker.str().catf("%-15s %d", m_name.c_str(), m_serialize);
}

bool Attribute::isSerialize(void)
{
	return m_serialize;
}

void Attribute::setSerialize(bool set)
{
	m_serialize = set;
}

bool Attribute::isCloneable(void)
{
	return m_cloneable;
}

void Attribute::setCloneable(bool set)
{
	m_cloneable = set;
}


} /* namespace */

