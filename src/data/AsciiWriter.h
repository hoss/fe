/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __data_AsciiWriter_h__
#define __data_AsciiWriter_h__

namespace fe
{
namespace data
{

class FE_DL_EXPORT AsciiWriter : public Scanner, public Writer
{
	public:

				AsciiWriter(sp<Scope> spScope);
virtual			~AsciiWriter(void);
virtual	void	output(std::ostream &a_ostrm, sp<RecordGroup> spRG);

	protected:

virtual	void	writeBinaryBlock(std::ostream &a_ostrm,
					const void *a_ptr, int a_size);

	private:

virtual	void	write(std::ostream &a_ostrm, Record r_out, int a_sb_id);
virtual	void	write(std::ostream &a_ostrm, sp<RecordGroup> spRG, int a_id);
virtual	void	write(std::ostream &a_ostrm, sp<RecordArray> spRA, int a_id);
virtual	void	write(std::ostream &a_ostrm, sp<Layout> spLayout, int a_id);
virtual	void	write(std::ostream &a_ostrm, sp<Attribute> spAttribute);

		U32		m_defaultRGID;
};

} /* namespace */
} /* namespace */

#endif /* __data_AsciiWriter_h__ */

