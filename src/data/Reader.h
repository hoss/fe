/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __data_Reader_h__
#define __data_Reader_h__
namespace fe
{
namespace data
{

typedef	Array<Record>						t_r;
typedef std::map<int, sp<RecordGroup> >		t_id_rg;
typedef std::map<int, sp<RecordArray> >		t_id_ra;

class FE_DL_EXPORT Reader : public Counted
{
	public:
							Reader(void):	m_name("Reader")				{}

virtual	sp<RecordGroup>		input(std::istream &istrm)						= 0;
const	String&				name(void) const	{ return m_name; }

	private:
		String				m_name;
};


} /* namespace */
} /* namespace */

#endif /* __data_Reader_h__ */
