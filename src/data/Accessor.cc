/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <data/data.pmh>

namespace fe
{

BaseAccessor::BaseAccessor(void)
{
	m_index = 0;
#ifdef FE_ACCESSOR_CACHE_LAYOUT_AND_OFFSET
	m_offset=0;
#endif
}

BaseAccessor::~BaseAccessor(void)
{
}

void BaseAccessor::initialize(sp<Scope> scope, const String &attribute)
{
	initialize(scope.raw(), attribute);
}

void BaseAccessor::initialize(Scope *pScope, const String &attribute)
{
	m_hpScope = pScope;
	if(!(pScope->findAttribute(attribute,m_index).isValid()))
	{
		feX("fe::BaseAccessor::initialize",
			"accessor init could not find valid index for attribute %s"
			, attribute.c_str());
	}

	typeCheck(attribute);
}

void BaseAccessor::initialize(sp<Scope> scope, sp<Attribute> spAttribute)
{
	/// @todo get index via Scope::attributeIndex once that is optimized
	// m_hpScope = pScope;
	// m_index = m_hpScope->attributeIndex(spAttribute);
	initialize(scope.raw(), spAttribute->name());
}

void BaseAccessor::setup(sp<Scope> scope, const String &attribute,
		const String &attributetype)
{
	m_hpScope = scope;
	if(!(scope->findAttribute(attribute,m_index).isValid()))
	{
		scope->support(attribute,attributetype);
		if(!(scope->findAttribute(attribute,m_index).isValid()))
		{
			feX("fe::BaseAccessor::setup",
				"accessor init could not find valid index for attribute %s"
				, attribute.c_str());
		}
	}

	typeCheck(attribute);
}

void BaseAccessor::typeCheck(const String &attribute)
{
	// if(m_typeInfo.isValid())
	// {
	// 	sp<Attribute> spAttribute = m_hpScope->findAttribute(attribute,m_index);
	// 	if(m_typeInfo != spAttribute->type()->typeinfo())
	// 	{
	// 		sp<BaseType> spRequestType =
	// 			m_hpScope->typeMaster()->lookupType(m_typeInfo);
	// 		feX("fe::BaseAccessor::typeCheck",
	// 			"type mismatch: \"%s\" is already \"%s\" but requesting \"%s\"",
	// 			attribute.c_str(),
	// 			spAttribute->type()->typeinfo().ref().name(),
 	// 			spRequestType->typeinfo().ref().name());
	// 	}
	// }
}

BaseAccessor &BaseAccessor::operator=(const BaseAccessor &other)
{
	if(&other != this)
	{
		m_index = other.m_index;
#ifdef FE_ACCESSOR_CACHE_LAYOUT_AND_OFFSET
		m_offset = other.m_offset;
		m_hpLayout = other.m_hpLayout;
#endif
		m_hpScope = other.m_hpScope;
		m_typeInfo = other.m_typeInfo;
	}
	return *this;
}

void BaseAccessor::populate(sp<Scope> spScope, const String &layout,
	const String &attribute)
{
	setup(spScope,attribute);
	sp<Layout> spLayout=spScope->lookupLayout(layout);
	if(!spLayout.isValid())
	{
		spLayout=spScope->declare(layout);
	}
	spLayout->populate(*this);
}

void BaseAccessor::setup(sp<Scope> spScope, const String &attribute)
{
	if(!m_typeInfo.isValid())
	{
		feX("fe::BaseAccessor::setup",
		"attempt to setup unassigned BaseAccessor for \"%s\"",
			attribute.c_str());
	}
	sp<BaseType> spBT =
		spScope->typeMaster()->lookupType(m_typeInfo);
#if FALSE
	String attributetype = spScope->typeMaster()->lookupBaseType(spBT);
#else
	std::list<String> namelist;
	spScope->typeMaster()->reverseLookup(spBT, namelist);
	if(namelist.empty())
	{
		feX("fe::BaseAccessor::setup",
			"cannot automatically find attribute type for \'%s\'",
			attribute.c_str());
	}
	String attributetype = *(namelist.begin());
#endif
	setup(spScope, attribute, attributetype);
}

sp<Scope> BaseAccessor::scope(void) const
{
#if 0
	if(!m_hpScope.isValid())
	{
		feX("fe::BaseAccessor::scope",
			"attempt to get scope from uninitialized Accessor");
	}
#endif
	return m_hpScope;
}

sp<Attribute> BaseAccessor::attribute(void) const
{
	return m_hpScope->attribute(m_index);
}

const String &BaseAccessor::name(void) const
{
	return attribute()->name();
}

};

