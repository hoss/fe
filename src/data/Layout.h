/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __data_Layout_h__
#define __data_Layout_h__

namespace fe
{

class Scope;
class BaseAccessor;

class FE_DL_EXPORT Layout: public Component, public CastableAs<Layout>
{
	public:
		Layout(void) : Component() {}
virtual	~Layout(void);

virtual	void			initialize(void) {}

		/** Convienience function to Scope::populate */
virtual	void			populate(const String &attribute_name)				= 0;
		/** Convienience function to Scope::populate */
virtual	void			populate(const String &attribute_name,
								const String &attribute_type)				= 0;
		/** Convienience function to Scope::populate.  This particular one
			does a check if the layout is locked and if so verifys the
			exsitence of the attribute. */
virtual	void			populate(const BaseAccessor &accessor)				= 0;
		/** Convienience function to Scope::populate */
virtual	void			populate(sp<Attribute> spAttribute)					= 0;

virtual	void			setName(const String &name)							= 0;
virtual	const String&	name(void) const									= 0;
virtual	const String	verboseName(void) const								= 0;

virtual	void			setConstruct(bool construct)						= 0;
virtual	const bool		&construct(void) const								= 0;

virtual	hp<Scope>&		scope(void)											= 0;


virtual	void			peek(Peeker &peeker)								= 0;

virtual	FE_UWORD		attributeCount(void) const							= 0;
virtual	sp<Attribute>	attribute(FE_UWORD localIndex)						= 0;

virtual	bool			checkAttribute(FE_UWORD a_locator) const			= 0;
virtual	bool			checkAttributeStr(const String &a_name)				= 0;
virtual	void			resizeLocatorTable(FE_UWORD a_size)					= 0;

virtual	void			notifyOfAttributeChange(sp<Depend> &depend)			= 0;

virtual bool			isExactlyBitset(const t_bitset a_bitset)			= 0;
virtual bool			containsBitset(const t_bitset a_bitset)				= 0;

		const t_bitset	&bitset(void) const { return m_bitset; }
		void			setBitset(const t_bitset &a_bs) { m_bitset = a_bs; }

	private:
		t_bitset	m_bitset;

};

} /* namespace */

#endif /* __data_Layout_h__ */

