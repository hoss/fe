/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "fe/data.h"

using namespace fe;

int main(int argc, char *argv[])
{
	UnitTest unitTest;
	BWORD completed=FALSE;

#if FE_COUNTED_TRACK
	Counted::startTracker();
#endif

	try
	{
		BWORD binary=(argc>1);

		/* ***************************************************************** */
		/* OUTPUT */
		// setup
		sp<TypeMaster> spTypeMaster(new TypeMaster());
		assertCore(spTypeMaster);
//		assertMath(spTypeMaster);

		Instance instance;
		instance.create<double>(spTypeMaster);
		double double_pi = 3.1415926535897932384626433;
		instance.set(spTypeMaster,double_pi);
		std::ostringstream ostrstrm;
		if(-1 == instance.type()->getInfo()->output(ostrstrm, instance.data(),
			BaseType::Info::e_ascii))
		{
			feLog("instance ascii write failed\n");
		}
		feLog("double pi is \"%s\"\n",ostrstrm.str().c_str());
		unitTest(ostrstrm.str()=="3.14159265358979");

		instance.create<float>(spTypeMaster);
		float float_pi = double_pi;
		instance.set(spTypeMaster,float_pi);
		std::ostringstream ostrstrm2;
		if(-1 == instance.type()->getInfo()->output(ostrstrm2, instance.data(),
			BaseType::Info::e_ascii))
		{
			feLog("instance ascii write failed\n");
		}
		feLog("float pi is \"%s\"\n",ostrstrm2.str().c_str());
		unitTest(ostrstrm2.str()=="3.141593");

		sp<Allocator> spAllocator(new BasicAllocator());
		sp<Scope> spScope(new Scope(spTypeMaster, spAllocator));
		spScope->setName("Scope out");

		Peeker peeker;
		spTypeMaster->peek(peeker);
		feLog("\nTypeMaster:%s\n",peeker.output().c_str());

#if 1
		// accessors
		Accessor<U8>			u8(spScope, "u8");
		Accessor<U16>			u16(spScope, "u16");
		Accessor<U32>			u32(spScope, "u32");
		Accessor<I32>			i32(spScope, "i32");
		Accessor<float>			x(spScope, "x");
		Accessor<float>			y(spScope, "y");
		Accessor<float>			z(spScope, "z");
		Accessor<String>		s(spScope, "s");
		Accessor<Record>		r(spScope, "rec");
		Accessor<WeakRecord>	w(spScope, "wk_rec");
//		Accessor<Vector3f>		v(spScope, "vec");
		Accessor<sp<RecordGroup> >	rg(spScope, "rg");
		Accessor<sp<RecordArray> > ra(spScope, "ra");
		Accessor< std::map<String,String> > map(spScope, "map");
		Accessor< Array<String> > vs(spScope, "vs");
		Accessor< Array<Real> > vr(spScope, "vr");
		Accessor< Array<U8> > vc(spScope, "vc");

		// layout
		sp<Layout> spLayout = spScope->declare("dummy");
		spLayout->populate(x);
		spLayout = spScope->declare("out layout");
		spLayout->populate(u8);
		spLayout->populate(u16);
		spLayout->populate(u32);
		spLayout->populate(i32);
		spLayout->populate(x);
		spLayout->populate(y);
		spLayout->populate(z);
		spLayout->populate(r);
		spLayout->populate(w);
		spLayout->populate(s);
		spLayout->populate(rg);
		spLayout->populate(ra);
		spLayout->populate(map);
		spLayout->populate(vs);
		spLayout->populate(vr);
		spLayout->populate(vc);
//		spLayout->populate(v);

		Record record = spScope->createRecord(spLayout);

		u8(record) = 13;
		u16(record) = 777;
		u32(record) = 123456;
		i32(record) = -98765;
		x(record) = 1.1;
		y(record) = 2.2;
		z(record) = 3.3;
		s(record) = "hello";
//		v(record) = Vector3f(1.2f,2.3f,3.4f);

		map(record)["key one"]="two";
		map(record)["three"]="four\tor more";

		vs(record).push_back("Alpha");
		vs(record).push_back("Bravo Beta");
		vs(record).push_back("Charlie");

		vr(record).push_back(0.12321);
		vr(record).push_back(0.2321);
		vr(record).push_back(0.321);
		vr(record).push_back(0.1232);
		vr(record).push_back(0.123);

		vc(record).push_back(0x47);
		vc(record).push_back(0x00);
		vc(record).push_back(0xbe);

		// record group
		sp<RecordGroup> spRG(new RecordGroup());
		spRG->add(record);

		Record record2 = spScope->createRecord(spLayout);
		u8(record2) = 15;
		u16(record2) = 888;
		u32(record2) = 234567;
		i32(record2) = -87654;
		x(record2) = 2.1;
		y(record2) = 3.2;
		z(record2) = 4.3;
		spRG->add(record2);

		r(record) = record2;
		w(record2) = record;

#if TRUE
		rg(record) = spRG;
#else
		sp<RecordGroup> spRG2(new RecordGroup());
		rg(record) = new RecordGroup();
		rg(record)->add(spScope->createRecord(spLayout));
#endif

		// record array
		sp<RecordArray> spRA = spScope->createRecordArray(spLayout, 2);
		ra(record) = spRA;
		u8(spRA,0) = 17;
		u8(spRA,1) = 19;
		u16(spRA,0) = 999;
		u16(spRA,1) = 1111;
		u32(spRA,0) = 345678;
		u32(spRA,1) = 456789;
		i32(spRA,0) = -76543;
		i32(spRA,1) = -65432;
		x(spRA,0) = 2.3;
		x(spRA,1) = 2.4;
		y(spRA,0) = 3.3;
		y(spRA,1) = 3.4;
		z(spRA,0) = 4.3;
		z(spRA,1) = 4.4;

		System::createParentDirectories("output/test/data/");

		// output to a file
		std::ofstream outfile("output/test/data/xSerialization_out.rg");
		if(!outfile)
		{
			feX(argv[0],"could not open test output file");
		}

		// write
		sp<data::StreamI> spStream;
		if(binary)
		{
			spStream=new data::BinaryStream(spScope);
		}
		else
		{
			spStream=new data::AsciiStream(spScope);
		}
		feLog("write to outfile\n");
		spStream->output(outfile, spRG);
		feLog("write to outfile (again)\n");
		spStream->output(outfile, spRG);

		outfile.close();
#endif

		{
			std::ofstream outfile("output/test/data/xSerialization_orig.rg");

			sp<data::StreamI> spStream;
			spStream=new data::AsciiStream(spScope);

			spStream->output(outfile, spRG);

			outfile.close();
		}
		{
			Cloner cloner(spScope);
			sp<RecordGroup> spCloneRG = cloner.clone(spRG);
			std::ofstream outfile("output/test/data/xSerialization_clone.rg");

			sp<data::StreamI> spStream;
			spStream=new data::AsciiStream(spScope);

			spStream->output(outfile, spCloneRG);

			outfile.close();
		}
		{
			Cloner cloner(spScope);
			Record r_clone = cloner.clone(record);
			sp<RecordGroup> spRG(new RecordGroup());
			spRG->add(record);
			spRG->add(r_clone);
			std::ofstream outfile("output/test/data/xSerialization_clone_r.rg");

			sp<data::StreamI> spStream;
			spStream=new data::AsciiStream(spScope);

			spStream->output(outfile, spRG);

			outfile.close();
		}

		/* ***************************************************************** */
		/* INPUT */
		// setup
		sp<TypeMaster> in_spTypeMaster(new TypeMaster());
		assertCore(in_spTypeMaster);
//		assertMath(in_spTypeMaster);
		sp<Allocator> in_spAllocator(new BasicAllocator());
		sp<Scope> in_spScope(new Scope(in_spTypeMaster, in_spAllocator));
		in_spScope->setName("Scope in");

		feLog("create input accessors\n");

		// accessors
		Accessor<U8>			in_u8(in_spScope, "u8");
		Accessor<U16>			in_u16(in_spScope, "u16");
		Accessor<U32>			in_u32(in_spScope, "u32");
		Accessor<I32>			in_i32(in_spScope, "i32");
		Accessor<float>			in_x(in_spScope, "x");
		Accessor<float>			in_y(in_spScope, "y");
		Accessor<float>			in_z(in_spScope, "z");
		Accessor<Record>		in_r(in_spScope, "rec");
		Accessor<WeakRecord>	in_w(in_spScope, "wk_rec");
		Accessor<String>		in_s(in_spScope, "s");
		Accessor<sp<RecordGroup> >	in_rg(in_spScope, "rg");
		Accessor<sp<RecordArray> > in_ra(in_spScope, "ra");
		Accessor< Array<Real> > in_vr(in_spScope, "vr");
		Accessor< Array<String> > in_vs(in_spScope, "vs");
		Accessor< Array<U8> > in_vc(in_spScope, "vc");
		Accessor< std::map<String,String> > in_map(in_spScope, "map");

		feLog("create layouts\n");

		// layout
		sp<Layout> in_spLayout = in_spScope->declare("in layout");
		//sp<Layout> in_spLayout = in_spScope->declare("out layout");
		in_spLayout->populate(in_u8);
		in_spLayout->populate(in_x);
		in_spLayout->populate(in_z);

		// record group
		sp<RecordGroup> in_spRG(new RecordGroup());

		Record in_record = in_spScope->createRecord(in_spLayout);

		feLog("input stream\n");

		// input from a file
		std::ifstream infile("output/test/data/xSerialization_out.rg");
		if(!infile)
		{
			feX(argv[0],"could not open test input file");
		}

		// read
		sp<data::StreamI> in_spStream;
		if(binary)
		{
			in_spStream=new data::BinaryStream(in_spScope);
		}
		else
		{
			in_spStream=new data::AsciiStream(in_spScope);
		}
		in_spRG = in_spStream->input(infile);
		for(RecordGroup::iterator it = in_spRG->begin();
			it != in_spRG->end(); it++)
		{
			sp<RecordArray> spRA = *it;
			for(int i = 0; i < spRA->length(); i++)
			{
				//* cut cycles
				in_rg(spRA,i)=NULL;
			}
		}
		in_spRG = in_spStream->input(infile);

		feLog("print results\n");

		/* ***************************************************************** */
		// debugging
		for(RecordGroup::iterator it = in_spRG->begin();
			it != in_spRG->end(); it++)
		{
			sp<RecordArray> spRA = *it;
			feLog("RA: %d\n", spRA->length());
			for(int i = 0; i < spRA->length(); i++)
			{
				feLog("u8 %d u16 %d u32 %d i32 %d\n",
						in_u8(spRA,i),in_u16(spRA,i),
						in_u32(spRA,i),in_i32(spRA,i));
				feLog("x %g y %g z %g [%s]\n",
						in_x(spRA,i),in_y(spRA,i),in_z(spRA,i),
						in_s(spRA,i).c_str());
				if(in_r(spRA,i).isValid())
				{
					feLog("  rec: %g\n", in_x(in_r(spRA,i)));
				}
				else
				{
					feLog("  rec: inv\n");
				}

				if(in_w(spRA,i).isValid())
				{
					feLog("  wkrec: %g\n", in_x(in_w(spRA,i)));
				}
				else
				{
					feLog("  wkrec: inv\n");
				}

				unsigned int cnt = in_vr(spRA,i).size();
				feLog("  vr: %d\n", cnt);
				for(unsigned int j = 0; j < cnt; j++)
				{
					feLog("  vr: %f\n", in_vr(spRA,i)[j]);
				}

				cnt = in_vs(spRA,i).size();
				feLog("  vs: %d\n", cnt);
				for(unsigned int j = 0; j < cnt; j++)
				{
					feLog("  vs: %s\n", in_vs(spRA,i)[j].c_str());
				}

				cnt = in_vc(spRA,i).size();
				feLog("  vc: %d\n", cnt);
				for(unsigned int j = 0; j < cnt; j++)
				{
					feLog("  vc: %p\n", in_vc(spRA,i)[j]);
				}

				feLog("map:\n");
				std::map<String,String>& rMap = in_map(spRA,i);
				std::map<String,String>::const_iterator it=rMap.begin();
				while(it!=rMap.end())
				{
					feLog("\"%s\" \"%s\"\n",
						it->first.c_str(),it->second.c_str());

					it++;
				}

				if(in_ra(spRA,i).isValid())
				{
					feLog("---- valid RA\n");
					for(int j = 0; j < in_ra(spRA,i)->length(); j++)
					{
						feLog("x %g y %g z %g\n",
							in_x(in_ra(spRA,i),j),
							in_y(in_ra(spRA,i),j),
							in_z(in_ra(spRA,i),j));
					}
					feLog("---- done\n");
				}

				//* cut cycles
				in_rg(spRA,i)=NULL;
			}
		}

		fe::Peeker peek;
		peek.bold();
		peek.cat("OUTPUT");
		peek.in();
		peek(*spScope);
		peek.out();
		peek.nl();
		feLog(peek.output().c_str());
		peek.clear();
		peek.bold();
		peek.cat("INPUT");
		peek.in();
		peek(*in_spScope);
		peek.out();
		//feLog(peek.output());

		//* cut cycles
		rg(record) = NULL;

//		spScope->shutdown();
//		in_spScope->shutdown();

		//* with a Master, the Registry would handle this
		spScope->cookbook()->clear();
		in_spScope->cookbook()->clear();

		completed=TRUE;
	}
	catch(Exception &e)
	{
		e.log();
	}
	catch(...)
	{
		feLog("unhandled exception\n");
	}

#if FE_COUNTED_TRACK
	feLog("\nCounted %s\n",Counted::reportTracker().c_str());
	Counted::stopTracker();
#endif

	unitTest(completed);
	unitTest(!Counted::trackerCount());
	return unitTest.failures();
}

