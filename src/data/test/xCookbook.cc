/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "fe/data.h"

using namespace fe;

int main(void)
{
	UNIT_START();
	BWORD complete=FALSE;
	try
	{
		fe::sp<fe::Master> spMaster(new fe::Master);
		fe::sp<fe::Scope> spScope1(new fe::Scope(*spMaster.raw()));
		spScope1->setName("Scope1");

		sp<Layout> spLayout=spScope1->declare("sack");
		spScope1->support("quantity", "integer");
		spScope1->populate("sack", "quantity");

		sp<Layout> spLayout2=spScope1->declare("tack");
		spScope1->support("bonus", "integer");
		spScope1->populate("tack", "bonus");

		Accessor<I32> aQuantity;
		aQuantity.setup(spScope1, "quantity", "integer");

		feLog("\n1\n");
		Record first=spScope1->produceRecord("sack");

		spScope1->cookbook()->add("sack","quantity","7");

		feLog("\n2\n");
		Record second=spScope1->produceRecord("sack");
		UNIT_TEST(aQuantity(second)==7);

		spScope1->cookbook()->derive("pack","sack");
		spScope1->cookbook()->add("pack","quantity","3");

		feLog("\n3\n");
		Record third=spScope1->produceRecord("pack");
		UNIT_TEST(aQuantity(third)==3);

		char filename[]="src/data/test/knack.rg";
		feLog("\nloading \"%s\"\n",filename);

		std::ifstream inFile(filename);
		if(!inFile)
		{
			feX("could not open \"%s\"\n",filename);
		}

		sp<RecordGroup> spRecordGroup;
		sp<data::StreamI> spStream(new data::AsciiStream(spScope1));
		spRecordGroup=spStream->input(inFile);
		inFile.close();

		feLog("\n4\n");
		Record fourth=spScope1->produceRecord("knack");
		UNIT_TEST(aQuantity(fourth)==5);

		feLog("\n5\n");
		Record fifth=spScope1->produceRecord("knack");
		UNIT_TEST(aQuantity(fifth)==5);

		feLog("\n6\n");
		Record sixth=spScope1->produceRecord("rack");
		UNIT_TEST(aQuantity(sixth)==13);

		feLog("\n7\n");
		Record seventh=spScope1->produceRecord("rack");
		UNIT_TEST(aQuantity(seventh)==13);

		feLog("\n8\n");
		fe::sp<fe::Scope> spScope2(new fe::Scope(*spMaster.raw()));
		spScope2->setName("Scope2");

		sp<Layout> spLayout3=spScope2->declare("sack");
		spScope2->support("quantity", "integer");
		spScope2->populate("sack", "quantity");

		sp<Layout> spLayout4=spScope2->declare("tack");
		spScope2->support("bonus", "integer");
		spScope2->populate("tack", "bonus");

		Record eighth=spScope2->produceRecord("rack");
		Accessor<I32> aQuantity2;
		aQuantity2.setup(spScope2, "quantity", "integer");
		UNIT_TEST(aQuantity2(eighth)==13);

		complete=TRUE;
	}
	catch(Exception &e) { e.log(); }
	catch(...)			{ feLog("uncaught exception\n"); }

	UNIT_TEST(complete);
	UNIT_TEST(!Counted::trackerCount());
	UNIT_TRACK(9);
	UNIT_RETURN();
}
