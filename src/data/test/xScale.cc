/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "fe/data.h"

using namespace fe;

I32 main(I32 argc,char** argv)
{
	UnitTest unitTest;

	try
	{
		I32 count=10000;
		I32 iterations=100;

		if(argc>1)
		{
			count=atoi(argv[1]);
		}
		if(argc>2)
		{
			iterations=atoi(argv[2]);
		}
		feLog("count=%d iterations=%d\n",count,iterations);

		// setup
		sp<Master> spMaster(new Master());
		sp<Scope> spScope(new Scope(*spMaster.raw()));

		spScope->setLocking(false);

		// accessors
		Accessor<Real>	x(spScope,"x");
		Accessor<Real>	y(spScope,"y");
		Accessor<Real>	z(spScope,"z");
		Accessor<Real>	vx(spScope,"vx");
		Accessor<Real>	vy(spScope,"vy");
		Accessor<Real>	vz(spScope,"vz");

		// layouts
		sp<Layout>spParticle = spScope->declare("particle");
		spParticle->populate(x);
		spParticle->populate(y);
		spParticle->populate(z);
		spParticle->populate(vx);
		spParticle->populate(vy);
		spParticle->populate(vz);

		// record group
		sp<RecordGroup> spRG(new RecordGroup());

		for(I32 i = 0; i < count; i++)
		{
			Record r;
			r = spScope->createRecord(spParticle);
			spRG->add(r);

			x(r) = 0.0;
			y(r) = 0.0;
			z(r) = 0.0;

			vx(r) = 1.0 - 2.0 * ((Real)rand()/(Real)RAND_MAX);
			vy(r) = 1.0 - 2.0 * ((Real)rand()/(Real)RAND_MAX);
			vz(r) = 1.0 + 1.0 * ((Real)rand()/(Real)RAND_MAX);
		}

		const Real dt=1.0/60.0;
		const Real g=9.81;
		const Real restitution=0.9;
		const Real fade=0.99;

		for(I32 iteration=0;iteration<iterations;iteration++)
		{
//			feLog("iteration %d\n",iteration);

			for(RecordGroup::iterator it = spRG->begin();
					it != spRG->end(); it++)
			{
				sp<RecordArray> spRA(*it);
				for(I32 i = 0; i < spRA->length(); i++)
				{
					Record r = spRA->getRecord(i);

					Real xr=x(r);
					Real yr=y(r);
					Real zr=z(r);

					Real vxr=vx(r);
					Real vyr=vy(r);
					Real vzr=vz(r);

					xr += vxr * dt;
					yr += vyr * dt;
					zr += vzr * dt;

					if(zr < 0.0)
					{
						zr *= -restitution;
						vzr *= -restitution;
					}

					vxr *= fade;
					vyr *= fade;
					vzr *= fade;

					vzr -= g * dt;

					x(r)=xr;
					y(r)=yr;
					z(r)=zr;

					vx(r)=vxr;
					vy(r)=vyr;
					vz(r)=vzr;

//					feLog("%d  %6.3f %6.3f %6.3f  %6.3f %6.3f %6.3f\n",
//							iteration,xr,yr,zr,vxr,vyr,vzr);
				}
			}
		}
	}
	catch(Exception &e)
	{
		e.log();
	}

	return unitTest.failures();
}

