/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "fe/data.h"

using namespace fe;


int main(void)
{
	UnitTest unitTest;
	try
	{

		Peeker peek;

		// setup infrastructure
		sp<TypeMaster> spTypeMaster(new TypeMaster());
		assertCore(spTypeMaster);
		sp<Allocator> spAllocator(new BasicAllocator());
		sp<Scope> spScope(new Scope(spTypeMaster, spAllocator));

		// some layouts
		spScope->declare("test layout");
		spScope->support("bogus", "integer");
		spScope->populate("test layout", "bogus");

		spScope->declare("another layout");
		spScope->populate("another layout", "dummy");
		spScope->populate("another layout", "bogus");
		spScope->share("another layout", "dummy", "bogus");
		spScope->share("", FE_USE(":CT"), "number");


		// accessors
		Accessor<I32> dummy;
		dummy.setup(spScope, "dummy", "integer");

		Accessor<I32> bogus;
		bogus.setup(spScope, "bogus", "integer");

		Accessor<I32> number;
		number.setup(spScope, "number");
		spScope->populate("another layout", "number");

		Accessor<F32> a_real;
		a_real.setup(spScope, "a_real");
		//spScope->populate("another layout", "a_real");

		Accessor<I32> loose;
		BaseAccessor ba;
		ba = loose;
		ba.setup(spScope, "loose");

		// a record group
		//sp<RecordGroup> spRG(new RecordGroup());
		sp<RecordGroup> spRG;
		spRG = new RecordGroup();

		sp<Layout> spTest = spScope->lookupLayout("test layout");
		sp<Layout> spAnother = spScope->lookupLayout("another layout");
		for(int i = 0; i < 4; i++)
		{
			Record r;
			r = spScope->createRecord(spTest);
			spRG->add(r);
			bogus(r) = i;

			r = spScope->createRecord(spAnother);
			spRG->add(r);
			bogus(r) = 1000 + i;
		}


		// iterate through record group manually
		for(RecordGroup::iterator it = spRG->begin(); it != spRG->end(); it++)
		{
			sp<RecordArray> spRA(*it);
			sp<Layout> spLayout(spRA->layout());
			feLog(spLayout->name().c_str());
			feLog("\n");
			for(int i = 0; i < spRA->length(); i++)
			{
				Record r = spRA->getRecord(i);
				feLog("  bogus %d\n", bogus(r));
			}
		}


		//peek(*spScope);
		//peek(*spTypeMaster);
		//feLog(peek.output());

//		spScope->shutdown();
	}
	catch(Exception &e)
	{
		e.log();
	}

	return unitTest.failures();
}

