/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "fe/data.h"

class myClass
{
	public:
		myClass(void) { m_int = 1; }
virtual	~myClass(void) { }

virtual	bool operator==(const myClass &other) const
		{
			return (m_int == other.m_int);
		}

		int	m_int;
};


int main(void)
{
	UNIT_START();
	try
	{
		fe::sp<fe::Master> spMaster(new fe::Master);
		fe::sp<fe::Scope> spScope(new fe::Scope(*spMaster.raw()));

		spScope->typeMaster()-> assertType<myClass>("myClassType");
		fe::sp<fe::Attribute> myAttribute =
			spScope->support("my_attribute", "myClassType");

		fe::Peeker peek;
		peek(*myAttribute);
		feLog("%s\n",peek.output().c_str());

		fe::Accessor<fe::String> hello_accessor(spScope, "hello");
		fe::Accessor<int> hello_wrong_type;
		try
		{
			hello_wrong_type.initialize(spScope, "hello");
			UNIT_TEST(true);
		}
		catch(fe::Exception &e)
		{
			UNIT_TEST(false);
		}
	}
	catch(fe::Exception &e)
	{
		e.log();
	}

	UNIT_TRACK(1);
	UNIT_RETURN();
}
