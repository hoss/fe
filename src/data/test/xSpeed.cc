/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "fe/data.h"

using namespace fe;

class Test
{
	public:
		Test(const char *filename, FE_UWORD count)
		{
			m_count = count;
			m_filename.sPrintf("output/test/data/%s_%u.dat", filename, m_count);
			System::createParentDirectories(m_filename);
			m_outfile.open(m_filename.c_str());
		}
virtual	~Test(void)
		{
			m_outfile.close();
		}
	protected:
		FE_UWORD			m_count;
		std::ofstream	m_outfile;
		String			m_filename;
};


class NullTest : public Test
{
	public:
		NullTest(FE_UWORD count) : Test("null", count) { }
virtual	~NullTest(void) { }
virtual	float operator()(FE_UWORD size)
		{
			//float f = 0.0;
			FE_UWORD t0, t1;
			t0 = systemTick();
			for(FE_UWORD i = 0; i < m_count; i++)
			{
				//f += 1.1;
			}
			t1 = systemTick();
			float per = 2 * ((float)(t1 - t0))/(float)m_count;
			m_outfile << size << " " << per << '\n';
			//fprintf(stderr,"%f\n", f);
			return per;
		}
	private:
};

class RawHeapTest : public Test
{
	public:
		RawHeapTest(FE_UWORD count) : Test("raw", count) { }
virtual	~RawHeapTest(void) { }
virtual	float operator()(FE_UWORD size)
		{
			//float f = 0.0;
			float per = 0.0;
			FE_UWORD live = size - 1;
			m_ppData = new FE_UWORD* [m_count];
			for(FE_UWORD i = 0; i < m_count; i++)
			{
				m_ppData[i] = new FE_UWORD[size];
			}
			FE_UWORD t0, t1;
			t0 = systemTick();
			for(FE_UWORD i = 0; i < m_count; i++)
			{
				m_ppData[i][live] = i;
			}
			t1 = systemTick();
			per += ((float)(t1 - t0))/(float)m_count;
			FE_UWORD accum = 0;
			t0 = systemTick();
			for(FE_UWORD i = 0; i < m_count; i++)
			{
				accum += m_ppData[i][live];
				//f += 1.1;
			}
			t1 = systemTick();
			per += ((float)(t1 - t0))/(float)m_count;
			m_outfile << size << " " << per << '\n';
			for(FE_UWORD i = 0; i < m_count; i++)
			{
				delete [] m_ppData[i];
			}
			delete [] m_ppData;
			//fprintf(stderr,"%f\n", f);
			return per;
		}
	private:
		FE_UWORD			**m_ppData;
};

class DirectRecord : public Test
{
	public:
		DirectRecord(FE_UWORD count, sp<Scope> spScope) : Test("direct", count)
		{
			m_spScope = spScope;
		}
virtual	~DirectRecord(void) { }
virtual	float operator()(FE_UWORD size)
		{
			//SystemTicker ticker("direct record");
			//float f = 0.0;
			float per = 0.0;
			String layout_name = m_filename;
			layout_name.sPrintf("direct_%u_%u", m_count, size);
			m_spL = m_spScope->declare(layout_name);
			for(FE_UWORD i = 0; i < size; i++)
			{
				String attribute_name;
				attribute_name.sPrintf("%u", i);
				m_spL->populate(attribute_name, "integer");
			}
			//ticker.log("populate");
			FE_UWORD live = size - 1;
			String live_name;
			live_name.sPrintf("%u", live);
			Accessor<int> aLive;
			aLive.initialize(m_spScope, live_name);
			m_pRecord = new Record[m_count];
			for(FE_UWORD i = 0; i < m_count; i++)
			{
				m_pRecord[i] = m_spScope->createRecord(m_spL);
			}
			FE_UWORD t0, t1;
			t0 = systemTick();
			for(FE_UWORD i = 0; i < m_count; i++)
			{
				aLive(m_pRecord[i]) = i;
			}
			t1 = systemTick();
			per += ((float)(t1 - t0))/(float)m_count;
			//ticker.log("create");
			FE_UWORD accum = 0;
			//ticker.log("pre get");
			t0 = systemTick();
			for(FE_UWORD i = 0; i < m_count; i++)
			{
				accum += aLive(m_pRecord[i]);
				//f += 1.1;
			}
			t1 = systemTick();
			//ticker.log("get");
			per += ((float)(t1 - t0))/(float)m_count;
			m_outfile << size << " " << per << '\n';
			delete [] m_pRecord;
			//fprintf(stderr,"%f\n", f);
			return per;
		}
	private:
		Record			*m_pRecord;
		sp<Layout>		m_spL;
		sp<Scope>		m_spScope;
};


int main(int argc, char *argv[])
{
	UnitTest unitTest;
	try
	{
		sp<Master> spMaster(new Master);
		sp<Scope> spScope(new Scope(*spMaster.raw()));

		//for(FE_UWORD count = 1; count < 1000; count += 100)
		FE_UWORD count = 10000;
		{
			RawHeapTest raw(count);
			DirectRecord direct(count, spScope);
			NullTest nt(count);
			fprintf(stderr,"count %lu\n", count);
			//for(FE_UWORD size = 1; size < 1000; size += 200)
			FE_UWORD size = 10;
			for(unsigned int c = 0; c < 10; c++)
			{
				fprintf(stderr,"  size %lu\n", size);
				float g;
				g = nt(size);
				fprintf(stderr,"    nt     %g\n", g);
				g = direct(size);
				fprintf(stderr,"    direct %g\n", g);
				g = raw(size);
				fprintf(stderr,"    raw    %g\n", g);

				g = nt(size);
				fprintf(stderr,"    nt     %g\n", g);
				g = direct(size);
				fprintf(stderr,"    direct %g\n", g);
				g = raw(size);
				fprintf(stderr,"    raw    %g\n", g);
			}
		}
	}
	catch(Exception &e)
	{
		e.log();
	}
	return unitTest.failures();
}

