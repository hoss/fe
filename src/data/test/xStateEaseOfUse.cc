/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "fe/data.h"

using namespace fe;

void basic_read_pattern(const sp<RecordGroup> a_spRG)
{
	// The normal
	for(RecordGroup::iterator i_rg = a_spRG->begin();
		i_rg != a_spRG->end(); i_rg++)
	{
		sp<RecordArray> spRA = *i_rg;
	}
}

int main(void)
{
	fe::UnitTest unitTest;
	try
	{

		fe::Peeker peek;

		// setup
		fe::sp<fe::Master> spMaster(new fe::Master);
		fe::sp<fe::Scope> spScope(new fe::Scope(*spMaster.raw()));

		// layout specified with strings
		fe::sp<fe::Layout> spLayout;
		spLayout = spScope->declare("first layout");
		spLayout->populate("bogus");

		// accessor
		fe::Accessor<I32> dummy;
		// automaticly looks up type for I32 and sets up support in scope
		dummy.setup(spScope, "dummy");

		// layout specified with accessors
		spLayout = spScope->declare("second layout");
		spLayout->populate(dummy);

		// creating a record will automatically resolve (lock) the layout
		fe::Record r = spScope->createRecord(spLayout);

		// set a value
		dummy(r) = 42;

		// read a value
		feLog("%d\n", dummy(r));

		peek(*spScope);
		feLog(peek.output().c_str());

//		spScope->shutdown();

	}
	catch(fe::Exception &e)
	{
		e.log();
	}

	return unitTest.failures();
}

