/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "fe/data.h"

using namespace fe;

int main(int argc, char *argv[])
{
	UnitTest unitTest;

	try
	{
		sp<SingleMaster> spSingleMaster=SingleMaster::create();
		sp<Master> spMaster=spSingleMaster->master();
		sp<TypeMaster> spTypeMaster(spMaster->typeMaster());
		assertCore(spTypeMaster);

		spMaster->registry()->manage("feDataDL");

		sp<Scope> spScope(spMaster->registry()->create("Scope"));
		FEASSERT(spScope.isValid());

		Accessor<int> i(spScope,"i");
		Accessor<Real> r(spScope,"r");

		sp<Layout> spLayout = spScope->declare("layout");

		spLayout->populate(i);
		spLayout->populate(r);

		Record recordOut = spScope->createRecord(spLayout);

		i(recordOut) = 7;
		r(recordOut) = 8.9;

		sp<BaseType> spRecordType = spTypeMaster->lookupType<Record>();

		std::ostringstream ostrm;
		spRecordType->getInfo()->output(ostrm,&recordOut,
				BaseType::Info::e_ascii);

		feLog("\noutput: \"%s\"\n",ostrm.str().c_str());

		Record recordIn;

		std::istringstream istrm(ostrm.str());
		spRecordType->getInfo()->input(istrm,&recordIn,
				BaseType::Info::e_ascii);

		feLog("\ninput: %d %.6G\n",i(recordIn),r(recordIn));
	}
	catch(Exception &e)
	{
		e.log();
		return 1;
	}
	catch(...)
	{
		feLog("unhandled exception\n");
		return 1;
	}

	return unitTest.failures();

}

