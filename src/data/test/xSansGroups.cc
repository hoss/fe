/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "fe/data.h"

using namespace fe;


class FE_DL_EXPORT AsThingOne:
	public AccessorSet,
	public Initialize<AsThingOne>
{
	public:
		void initialize(void)
		{
			add(id2,	FE_USE("test:id2"));
			add(label,	FE_USE("test:label"));
			add(id,		FE_USE("test:id"));
		}
		Accessor<I32>		id;
		Accessor<I32>		id2;
		Accessor<String>	label;
};

class FE_DL_EXPORT AsThingTwo:
	public AccessorSet,
	public Initialize<AsThingTwo>
{
	public:
		void initialize(void)
		{
			add(id,		FE_USE("test:id"));
		}
		Accessor<I32>		id;
};

class FE_DL_EXPORT AsMatter:
	public AccessorSet,
	public Initialize<AsMatter>
{
	public:
		void initialize(void)
		{
			add(mass,		FE_USE("test:mass"));
		}
		Accessor<Real>		mass;
};

int main(int argc, char *argv[])
{
	UnitTest unitTest;
#if 1
	try
	{
		sp<Master> spMaster(new Master);
		sp<Scope> spScope(new Scope(*spMaster.raw()));

		sp<Layout> l_0 = spScope->declare("L0");
		sp<Layout> l_1 = spScope->declare("L1");
		sp<Layout> l_2 = spScope->declare("L2");

		sp<RecordGroup> rg_all(new RecordGroup());

		AsThingOne asThingOne;
		asThingOne.populate(l_0);

		AsThingTwo asThingTwo;
		asThingTwo.populate(l_2);

		AsMatter asMatter;
		asMatter.populate(l_0);
		asMatter.populate(l_1);
		asMatter.populate(l_2);

		std::vector<Record> r_array;

		for(unsigned int i = 0; i < 5; i++)
		{
			Record r_a = spScope->createRecord(l_0);
			asThingOne.id(r_a) = i;
			asThingOne.id2(r_a) = 55 - i;
			asMatter.mass(r_a) = 1.1;
			r_array.push_back(r_a);
		}

		Record r_two = spScope->createRecord(l_2);
		asThingTwo.id(r_two) = 234;
		asMatter.mass(r_two) = 1.1;
		r_array.push_back(r_two);

#if 1
		{
			std::vector<Record> r_throw_away;
			for(unsigned int i = 0; i < 3; i++)
			{
				Record r_a = spScope->createRecord(l_0);
				asThingOne.id(r_a) = 100 + i;
				asMatter.mass(r_a) = 1.1;
				r_throw_away.push_back(r_a);
			}
		}

		for(unsigned int i = 5; i < 6; i++)
		{
			Record r_a = spScope->createRecord(l_0);
			asThingOne.id(r_a) = i;
			asMatter.mass(r_a) = 1.1;
			r_array.push_back(r_a);
		}

		r_array[2] = r_array[1];
#endif

#if 1
		t_layouts &layouts = spScope->layouts();
		for(unsigned int i = 0; i < layouts.size(); i++)
		{
			sp<LayoutAV> l_av = layouts[i];
			if(l_av.isValid() && asThingOne.check(l_av))
			{
				int *id_av = asThingOne.id(l_av);
				int *id2_av = asThingOne.id2(l_av);
				if(id_av != NULL && id2_av != NULL)
				{
					for(unsigned int j = 0; j < l_av->avSize(); j++)
					{
						id_av[j] = id_av[j] + id2_av[j];
					}
				}

#if 0
				fprintf(stderr, "%s avsize %d free %d idr %d\n", l_av->name().c_str(), l_av->m_avSize, l_av->m_freeList.size(), l_av->m_idr.size());
				fprintf(stderr, "  Loc Tbl:\n");
				for(unsigned int j = 0; j < l_av->m_locatorTable.size(); j++)
				{
					fprintf(stderr, "    %d\n", l_av->m_locatorTable[j]);
				}
				fprintf(stderr, "  AVs:\n");
				for(unsigned int j = 0; j < l_av->m_attributeVector.size(); j++)
				{
					fprintf(stderr, "    %d\n", l_av->m_attributeVector[j]->size());
				}
				fprintf(stderr, "  RC:SN\n");
				for(unsigned int j = 0; j < l_av->m_avSize; j++)
				{
					fprintf(stderr, "    %d: %2d:%2d\n", j, l_av->m_rc->at(j), l_av->m_sn->at(j));
				}

				fprintf(stderr, "  IDIDX: %d\n", asThingOne.id.index());
				fprintf(stderr, "  IDIDX: %d\n", asThingOne.label.index());
				fprintf(stderr, "  IDIDX: %d\n", asThingOne.id2.index());

				int id_av_idx = l_av->m_locatorTable[asThingOne.id.index()];
				int *id_array = (int *)(l_av->m_attributeVector[id_av_idx]->raw_at(0));

				fprintf(stderr, "  VAL:\n");
				for(unsigned int j = 0; j < l_av->m_avSize; j++)
				{
					fprintf(stderr, "    %d: %2d\n", j, id_array[j]);
				}

				fprintf(stderr, "  RECORDS:\n");
				for(unsigned int j = 0; j < r_array.size(); j++)
				{
					if(r_array[j].layout() == l_av)
					{
						fprintf(stderr, "    %d: %2d\n", j, asThingOne.id(r_array[j]));
					}
				}
#endif
			}
		}
#endif

	}
	catch(Exception &e)
	{
		e.log();
	}
#endif
	return unitTest.failures();
}

