/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "fe/data.h"

using namespace fe;

I32 main(I32 argc,char** argv)
{
	UnitTest unitTest;

	try
	{
		// setup
		sp<Master> spMaster(new Master());
		sp<Scope> spScope(new Scope(*spMaster.raw()));

		// accessors
		AsSystem systemAS;
		systemAS.bind(spScope);

		Accessor<Real>	x(spScope,"x");
		Accessor<Real>	y(spScope,"y");
		Accessor<Real>	z(spScope,"z");

		// layouts
		sp<Layout> sp3DLayout = spScope->declare("3dimensional");
		sp3DLayout->populate(x);
		sp3DLayout->populate(y);
		sp3DLayout->populate(z);
		sp<Layout> sp2DLayout = spScope->declare("2dimensional");
		sp2DLayout->populate(x);
		sp2DLayout->populate(y);

		t_bitset bitset(spScope->attributes().size());
		bitset.set(systemAS.count.index());
		bitset.set(systemAS.sn.index());

		bitset.set(x.index());
		unitTest(sp2DLayout->containsBitset(bitset), "2D Layout contains X");
		unitTest(sp3DLayout->containsBitset(bitset), "3D Layout contains X");
		unitTest(!sp2DLayout->isExactlyBitset(bitset), "2D Layout is not exactly X");
		unitTest(!sp3DLayout->isExactlyBitset(bitset), "2D Layout is not exactly X");

		bitset.set(y.index());
		unitTest(sp2DLayout->containsBitset(bitset), "2D Layout contains X,Y");
		unitTest(sp3DLayout->containsBitset(bitset), "3D Layout contains X,Y");
		unitTest(sp2DLayout->isExactlyBitset(bitset), "2D Layout is exactly X,Y");
		unitTest(!sp3DLayout->isExactlyBitset(bitset), "3D Layout is not exactly X,Y");

		bitset.set(z.index());
		unitTest(!sp2DLayout->containsBitset(bitset), "2D Layout does not contain X,Y,Z");
		unitTest(sp3DLayout->containsBitset(bitset), "3D Layout contains X,Y,Z");
		unitTest(!sp2DLayout->isExactlyBitset(bitset), "2D Layout is not exactly X,Y,Z");
		unitTest(sp3DLayout->isExactlyBitset(bitset), "3D Layout is exactly X,Y,Z");
	}
	catch(Exception &e)
	{
		e.log();
	}

	return unitTest.failures();
}

