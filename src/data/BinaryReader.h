/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __data_BinaryReader_h__
#define __data_BinaryReader_h__
namespace fe
{
namespace data
{


class FE_DL_EXPORT BinaryReader : public Reader
{
typedef	Array<Record>						t_r;
typedef std::map<int, sp<RecordGroup> >		t_id_rg;
typedef std::map<int, sp<RecordArray> >		t_id_ra;
	public:
							BinaryReader(sp<Scope> spScope);
virtual						~BinaryReader(void);
virtual	sp<RecordGroup>		input(std::istream &istrm);

	private:
		struct AttributeInfo
		{
			String					m_name;
			std::list<String>		m_typenames;
			IWORD					m_skipsize;
			sp<Attribute>			m_spAttribute;
			FE_UWORD				m_index;
		};
		typedef Array<AttributeInfo>				t_attrinfo;
		struct LayoutInfo
		{
			sp<Layout>					m_spLayout;
			Array<AttributeInfo>		m_attributeInfos;
		};
		typedef std::map<int, LayoutInfo>			t_id_layoutinfo;
		struct RecordWiringInfo
		{
			Record					m_record;
			Accessor<Record>		m_aRecord;
			int						m_id;
		};
		typedef std::list<RecordWiringInfo>			t_wiring;

		struct WeakRecordWiringInfo
		{
			Record					m_record;
			Accessor<WeakRecord>	m_aRecord;
			int						m_id;
		};
		typedef std::list<WeakRecordWiringInfo>			t_wk_wiring;

		U8					getCode(std::istream &istrm);
		void				readLayout(std::istream &istrm);
		void				readAttribute(std::istream &istrm);
		void				readState(std::istream &istrm);
		void				readRecord(std::istream &istrm, LayoutInfo &l_i);
		void				readRecordGroup(std::istream &istrm);
		void				readInfo(std::istream &istrm);
		void				wireRecords(void);
		void				recordGroupsToArrays(void);
		void				reset(void);
		void				skip(std::istream &istrm, int skipsize);

	private:
		t_id_layoutinfo				m_layoutInfos;
		t_id_rg						m_rgs;
		t_id_ra						m_ras;
		t_r							m_rs;
		t_attrinfo					m_attrInfos;
		t_wiring					m_wiringList;
		t_wk_wiring					m_wkWiringList;
		sp<Scope>					m_spScope;
		sp<BaseType>				m_spRecordGroupType;
		sp<BaseType>				m_spRecordArrayType;
		sp<BaseType>				m_spRecordType;
		sp<BaseType>				m_spWeakRecordType;
};


} /* namespace */
} /* namespace */

#endif /* __data_BinaryReader_h__ */
