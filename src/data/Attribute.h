/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __data_Attribute_h__
#define __data_Attribute_h__

#define FE_ATTRIBUTE(name,description)
#define FE_USE_3(name,description,maybe_use_this_later)		name
#define FE_SPEC(name,description)		name
#define FE_USE_1(name)										name
#define FE_USE_0()											"FE_USE"

#define FE_DISPATCH(name,doc) name
#define FE_SLOT(name) name
// yes, cheated, got this macro magic from stackoverflow
// this gives FE_USE multiple (unused by C++) arguments (that our dox can use)
#define FE_USE_X(x,A,B,C,name, ...) name

#define FE_USE(...)					FE_USE_X(,##__VA_ARGS__,\
									FE_USE_3(__VA_ARGS__),\
									FE_SPEC(__VA_ARGS__),\
									FE_USE_1(__VA_ARGS__),\
									FE_USE_0(__VA_ARGS__))

namespace fe
{

struct SerialFlags;


/** @brief An attribute within a Layout (record type)

	@ingroup data
	*/
class FE_DL_EXPORT Attribute : public Counted, public ClassSafe<Attribute>
{
	public:
					Attribute(const Attribute &other);
virtual				~Attribute(void);

		Attribute				&operator=(const Attribute &other);

		/**	Get the BaseType for this Attribute. */
		const sp<BaseType>		&type(void) const;
		/**	Get the TypeMaster for the BaseType for this Attribute. */
		const sp<TypeMaster>	&typeMaster(void) const;
		/** Get the name of this Attribute. */
		const String			&name(void) const;
		/** Get the long name of this Attribute. */
		const String			verboseName(void) const;

		/** Return true if attribute should be serialized. */
		bool					isSerialize(void);
		/** Set whether or not attribute should be serialized. */
		void					setSerialize(bool set);

		/** Return true if attribute should be cloned. */
		bool					isCloneable(void);
		/** Set whether or not attribute should be cloned. */
		void					setCloneable(bool set);

		Instance				&defaultInstance(void) { return m_default; }
		Instance				&deadInstance(void) { return m_dead; }

		void					peek(Peeker &peeker);

	friend class Scope; // only really want Scopes creating these things
	private:
		Attribute(const String name, sp<BaseType> type, sp<TypeMaster> spTM);
		Attribute(void);
		String						m_name;
		sp<BaseType>				m_spType;
		sp<TypeMaster>				m_spTypeMaster;
		bool						m_serialize;
		bool						m_cloneable;
		Instance					m_default;
		Instance					m_dead;
};

inline const String &Attribute::name(void) const
{
	return m_name;
}

inline const String Attribute::verboseName(void) const
{
	return "Attribute " + m_name;
}

inline const sp<BaseType> &Attribute::type(void) const
{
	return m_spType;
}

inline const sp<TypeMaster> &Attribute::typeMaster(void) const
{
	return m_spTypeMaster;
}

} /* namespace */

#endif /* __data_Attribute_h__ */
