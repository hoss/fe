/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __data_PopulateI_h__
#define __data_PopulateI_h__

namespace fe
{

/** Populate a layout with attributes */
class FE_DL_EXPORT PopulateI :
		virtual public Component,
		public CastableAs<PopulateI>
{
	public:
virtual	void	populate(sp<Layout> spLayout)								= 0;
};

} /* namespace */

#endif /* __data_PopulateI_h__ */

