/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __data_AsciiReader_h__
#define __data_AsciiReader_h__
namespace fe
{
namespace data
{


class FE_DL_EXPORT AsciiReader : public Reader
{
	public:
							AsciiReader(sp<Scope> spScope);
virtual						~AsciiReader(void);
virtual	sp<RecordGroup>		input(std::istream &istrm);

	private:
		enum BlockType
		{
			e_none,
			e_record,
			e_group,
			e_defaultgroup,
			e_template,
			e_attribute,
			e_layout,
			e_info,
			e_end,
			e_comment
		};
		class Token
		{
			public:
				Token(void)
				{
					m_pBlock = NULL;
				}
				~Token(void)
				{
					if(m_pBlock) { deallocate(m_pBlock); }
				}
				Token(const Token &other)
				{
					m_pBlock = NULL;
					copy(other);
				}
				Token &operator=(const Token &other)
				{
					if(&other == this) { return *this; }
					copy(other);
					return *this;
				}
				void create(int sz)
				{
					if(m_pBlock) { deallocate(m_pBlock); }
					m_size = sz;
					m_pBlock = allocate(m_size);
				}
				void copy(const Token &other)
				{
					if(m_pBlock) { deallocate(m_pBlock); }
					if(other.m_pBlock)
					{
						m_pBlock = allocate(other.m_size);
						m_size = other.m_size;
						memcpy(m_pBlock, other.m_pBlock, m_size);
					}
					m_string = other.m_string;
					m_args = other.m_args;
				}
				String				m_string;
				String				m_args;
				void				*m_pBlock;
				int					m_size;
				bool				isString(void) const
									{	return (m_pBlock==NULL);}
				void				clear(void)
				{
					m_string = "";
					m_args = "";
					if(m_pBlock) { deallocate(m_pBlock); }
					m_pBlock = NULL;
				}
				operator String &()
				{
					if(!isString()) { feX("token not string"); }
					return m_string;
				}
				operator const String &() const
				{
					if(!isString()) { feX("token not string"); }
					return m_string;
				}
				operator void *()
				{
					if(isString()) { feX("token is string"); }
					return m_pBlock;
				}
				const char *c_str(void)
				{
					if(isString()) { return m_string.c_str(); }
					else return "[binary]";
				}
				String args(void) const
				{
					if(!isString()) { feX("token not string"); }
					return m_args;
				}
				String &str(void)
				{
					if(!isString()) { feX("token not string"); }
					return m_string;
				}
		};

		bool			readToken(std::istream &a_istrm, Token &a_token);
		//void			readAttribute(std::istream &a_istrm);
		//void			readInfo(std::istream &a_istrm);
		void			dispatchBlock(BlockType a_type,
								const Token& token,
								Array<Token> &a_tokens);
		void			handleGroup(Array<Token> &a_tokens,
								const Token& token);
		void			handleAttribute(Array<Token> &a_tokens);
		void			handleLayout(Array<Token> &a_tokens);
		void			handleTemplate(Array<Token> &a_tokens);
		void			handleRecord(Array<Token> &a_tokens);
		void			handleInfo(Array<Token> &a_tokens);
		void			handleComment(Array<Token> &a_tokens);
		void			handleEnd(Array<Token> &a_tokens);

	protected:
		struct AttributeInfo
		{
			String					m_name;
			std::list<String>		m_typenames;
			sp<Attribute>			m_spAttribute;
			FE_UWORD					m_index;
		};
		typedef std::map<String, AttributeInfo>		t_attrinfo;
		struct LayoutInfo
		{
			sp<Layout>					m_spLayout;
			t_attrinfo					m_attributeInfos;
		};
		typedef std::map<String, LayoutInfo>		t_id_layoutinfo;
// TODO: AJW: do this back wiring in a faster way -- Accessor-per is slow
// TODO: AJW: apply new backwiring to Binary version
		struct RecordWiringInfo
		{
			Record					m_record;
			Accessor<Record>		m_aRecord;
			String					m_id;
		};
		typedef std::list<RecordWiringInfo>			t_wiring;

		struct WeakRecordWiringInfo
		{
			Record					m_record;
			Accessor<WeakRecord>	m_aRecord;
			String					m_id;
		};
		typedef std::list<WeakRecordWiringInfo>			t_wk_wiring;

#if 0
		void				readLayout(std::istream &istrm);
		void				readAttribute(std::istream &istrm);
		void				readState(std::istream &istrm);
		void				readRecord(std::istream &istrm, LayoutInfo &l_i);
		void				readRecordGroup(std::istream &istrm);
		void				wireRecords(void);
		void				recordGroupsToArrays(void);
		void				skip(std::istream &istrm, int skipsize);
#endif
		void			fail(const String &a_message);
		void			reset(void);

		void			wireRecords(void);
		void			recordGroupsToArrays(void);
#if 0
		void			tokenize(Array<String> &a_tokens,
								std::string &a_string);
#endif

typedef	std::map<String, Record>				t_str_r;
typedef std::map<String, sp<RecordGroup> >		t_id_rg;
typedef std::map<String, sp<RecordArray> >		t_id_ra;

		t_id_layoutinfo				m_layoutInfos;
		t_id_rg						m_rgs;
		t_id_ra						m_ras;
		t_str_r						m_rs;
		t_attrinfo					m_attrInfos;
		t_wiring					m_wiringList;
		t_wk_wiring					m_wkWiringList;
		sp<Scope>					m_spScope;
		sp<BaseType>				m_spRecordGroupType;
		sp<BaseType>				m_spRecordArrayType;
		sp<BaseType>				m_spRecordType;
		sp<BaseType>				m_spWeakRecordType;
		std::vector< sp<RecordGroup> >	m_spDefaultRGs;
		sp<RecordGroup>				m_spDefaultRG;

		sp<Scope>		findScope(const String &a_name);

		unsigned int				m_next_subst_id;
		String						m_subst_id;
};


} /* namespace */
} /* namespace */

#endif /* __data_AsciiReader_h__ */
