/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <data/data.pmh>

namespace fe
{

void WeakRecordAV::demandSerialNumber(void) const
{
#if FE_WR_DEMAND_SN
	if(isValid() && m_serialNumber<0)
	{
		feLog("WeakRecordSA::cacheSerialNumber layout \"%s\""
				" doesn't support \":SN\" attribute\n",
				m_hpLayout->name().c_str());
		feX("WeakRecordSA::demandSerialNumber","no SN");
	}
#endif
}

String WeakRecordAVInfo::print(void *instance)
{
	if(!instance)
		return String("<error>");

	const WeakRecordAV* pR = (WeakRecordAV*)instance;
	if(!pR->isValid())
		return String("<invalid>");

	String string;
	string.sPrintf("%u %s",pR->idr(),
			pR->layout().isValid()?
			pR->layout()->name().c_str(): "<invalid layout>");
	return string;
}

IWORD WeakRecordAVInfo::output(std::ostream &ostrm, void *instance,
		t_serialMode mode)
{
	feX(e_unsupported,
		"WeakRecordAVInfo::output",
		"record output should be done via fe::Stream");
	return 0;
}

void WeakRecordAVInfo::input(std::istream &istrm, void *instance,
		t_serialMode mode)
{
	feX(e_unsupported,
		"WeakRecordAVInfo::input",
		"record input should be done via fe::Stream");
}

IWORD WeakRecordAVInfo::iosize(void)
{
	return c_implicit;
}

bool WeakRecordAVInfo::getConstruct(void)
{
	return true;
}

void WeakRecordAVInfo::construct(void *instance)
{
	new(instance)WeakRecordAV;
}

void WeakRecordAVInfo::destruct(void *instance)
{
	((WeakRecordAV *)instance)->~WeakRecordAV();
}


} /* namespace */

