/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <data/data.pmh>

namespace fe
{

sp<RecordArray>	RecordGroup::getArray(const String layout)
{
	RecordGroup::iterator it = this->begin();
	if(it==this->end())
	{
		return sp<RecordArray>();
	}

	sp<RecordArray> spRA(*it);
	sp<Scope> spScope = spRA->layout()->scope();
	sp<Layout> spLayout=spScope->lookupLayout(layout);
	return getArray(spLayout);
}

sp<RecordArray>	RecordGroup::getArray(sp<Layout> l_query)
{
	if(l_query.isNull())
	{
		return sp<RecordArray>();
	}
	sp<RecordArray> spRA;
	t_layout_ra::iterator it = m_raMap.find(l_query);
	if(it == m_raMap.end())
	{
		if(!m_locking)
		{
			m_locking=l_query->scope()->isLocking();
		}

		RecordArray* pRA=new RecordArray();
		pRA->registerRegion(pRA,sizeof(RecordArray));
		pRA->trackReference(this,"RecordGroup");
		spRA = pRA;
		spRA->bind(sp<RecordGroup>(this));
		spRA->setLayout(l_query);
		spRA->setWeak(m_weak);
		spRA->enableDuplicates(m_duplicates);
		spRA->maintainOrder(m_maintainOrder);
		m_raMap[l_query] = spRA;
	}
	else
	{
		spRA = it->second;
	}
	return spRA;
}

void RecordGroup::watcherAdd(const Record &record)
{
	for(t_watchers::iterator tw_it = m_watchers.begin();
		tw_it != m_watchers.end(); tw_it++)
	{
		(*tw_it)->add(record);
	}
}

void RecordGroup::watcherRemove(const Record &record)
{
	for(t_watchers::iterator tw_it = m_watchers.begin();
		tw_it != m_watchers.end(); tw_it++)
	{
		(*tw_it)->remove(record);
	}
}

void RecordGroup::add(const Record &record)
{
	SAFEGUARDCLASS_IF(m_locking || !m_raMap.size());

	sp<RecordArray> spRA = getArray(record.layout());

	spRA->addCovert(record);

	watcherAdd(record);
}

void RecordGroup::add(sp<RecordArray> &records)
{
	SAFEGUARDCLASS_IF(m_locking || !m_raMap.size());

	sp<RecordArray> spRA2 = getArray(records->layout());

	for(int i = 0; i < records->length(); i++)
	{
		Record record=records->getRecord(i);
		spRA2->addCovert(record);
		watcherAdd(record);
	}
}

void RecordGroup::add(sp<RecordGroup> &records)
{
	SAFEGUARDCLASS_IF(m_locking || !m_raMap.size());

	for(RecordGroup::iterator i_rg = records->begin();
		i_rg != records->end(); i_rg++)
	{
		sp<RecordArray> spRA = *i_rg;

		sp<Layout> spLayout=spRA->layout();
		sp<RecordArray> spRA2 = getArray(spLayout);

		for(int i = 0; i < spRA->length(); i++)
		{
			Record record=spRA->getRecord(i);
			spRA2->addCovert(record);
			watcherAdd(record);
		}
	}
}

void RecordGroup::remove(sp<RecordGroup> &records)
{
#if 0
	for(RecordGroup::iterator i_rg = records->begin();
		i_rg != records->end(); i_rg++)
	{
		sp<RecordArray> spRA = *i_rg;
		for(int i = 0; i < spRA->length(); i++)
		{
			remove(spRA->getRecord(i));
		}
	}
#endif
#if 1
	SAFEGUARDCLASS_IF(m_locking);

	for(RecordGroup::iterator i_delete = records->begin();
		i_delete != records->end(); i_delete++)
	{
		sp<RecordArray> ra_delete = *i_delete;

		t_layout_ra::iterator it = m_raMap.find(ra_delete->layout());
		if(it != m_raMap.end())
		{
			sp<RecordArray> ra_this = it->second;
			for(int i = ra_delete->length()-1; i >= 0; i--)
			{
				Record r_delete = ra_delete->getRecord(i);
				ra_this->removeCovert(r_delete);
				watcherRemove(r_delete);
			}

		}
	}
#endif
}

void RecordGroup::rebuildArrays(void)
{
	sp<RecordGroup> spStash(new RecordGroup);
	sp<RecordGroup> spThis(this);
	spStash->add(spThis);
	clear();
	add(spStash);
}

bool RecordGroup::remove(const Record &record)
{
	SAFEGUARDCLASS_IF(m_locking);

	t_layout_ra::iterator it = m_raMap.find(record.layout());
	if(it == m_raMap.end())
	{
		feLog("fe::RecordGroup::remove"
				" attempt to remove non member %s record from group"
				" (layout mismatch)\n",record.layout()->name().c_str());
		return false;
	}
	else
	{
		sp<RecordArray> spRA = it->second;
		if(!spRA->removeCovert(record))
		{
/*
			feLog("fe::RecordGroup::remove"
					" attempt to remove non member %s record from group\n",
					record.layout()->name().c_str());
*/
			return false;
		}
	}

	watcherRemove(record);
	return true;
}


FE_UWORD RecordGroup::count(void)
{
	SAFEGUARDCLASS_IF(m_locking);

	FE_UWORD cnt = 0;
	for(RecordGroup::iterator i_rg = begin();
		i_rg != end(); i_rg++)
	{
		cnt += (*i_rg)->length();
	}
	return cnt;
}

void RecordGroup::all(RecordOperation &op)
{
	SAFEGUARDCLASS_IF(m_locking);

	for(t_layout_ra::iterator it = m_raMap.begin();
		it != m_raMap.end(); it++)
	{
		sp<Layout> spLayout = it->first;
		sp<RecordArray> spRA = it->second;
		bool failed_filter = false;
		for(std::list<BaseAccessor>::iterator fit = op.filters().begin();
			fit != op.filters().end(); fit++)
		{
			if(!fit->check(spRA))
			{
				failed_filter = true;
				break;
			}
		}
		if(failed_filter)
		{
			continue;
		}
		for(IWORD i = 0; i < spRA->length(); i++)
		{
			op(spRA->getRecord(i));
		}
	}
}

RecordGroup::iterator RecordGroup::begin(Array<BaseAccessor> filters)
{
	RecordGroup::iterator it;
	it.m_filters = filters;
	it.m_raMapIt = m_raMap.begin();
	it.m_endIt = m_raMap.end();
	while(!it.check() && it.m_raMapIt != it.m_endIt)
	{
		it.m_raMapIt++;
	}
	it.m_spRG = this;
	return it;
}

void RecordGroup::removeReferencesFromRecord(Record &r_from, const Record &r_rm, bool a_recurse, Info *a_info)
{
	Record r_empty;
	if(!r_from.isValid()) { return; }
	sp<Scope> spScope = r_from.layout()->scope();
	sp<BaseType> spRecordType =
		spScope->typeMaster()->lookupType<Record>();
	sp<BaseType> spRGType =
		spScope->typeMaster()->lookupType< sp<RecordGroup> >();
	unsigned int acnt = spScope->getAttributeCount();
	for(unsigned int a = 0; a < acnt; a++)
	{
		sp<Attribute> spAttr = spScope->attribute(a);
		if(spAttr->type() == spRecordType)
		{
			Accessor<Record> aRecord;
			aRecord.initialize(spScope, spAttr);
			if(aRecord.check(r_from))
			{
				if(aRecord(r_from) == r_rm)
				{
					aRecord(r_from) = r_empty;
				}
				else if(a_recurse &&
					a_info->m_rs.find(aRecord(r_from)) ==
					a_info->m_rs.end())
				{
					a_info->m_rs.insert(aRecord(r_from));
					removeReferencesFromRecord(aRecord(r_from),
						r_rm, a_recurse, a_info);
				}
			}
		}
		if(a_recurse && spAttr->type() == spRGType)
		{
			Accessor< sp<RecordGroup> > aRG;
			aRG.initialize(spScope, spAttr);
			if(aRG.check(r_from))
			{
				if(a_info->m_rgs.find(aRG(r_from)) ==
					a_info->m_rgs.end())
				{
					a_info->m_rgs.insert(aRG(r_from));
					aRG(r_from)->removeReferences(r_rm, a_recurse, a_info);
				}
			}
		}
	}
}

void RecordGroup::removeReferences(const Record &r_rm, bool a_recurse, Info *a_info)
{
	bool delete_rginfo = false;
	if(a_recurse && !a_info)
	{
		a_info = new Info();
		delete_rginfo = true;
	}
	Record r_empty;
	for(RecordGroup::iterator i_rg = this->begin();
		i_rg != this->end(); i_rg++)
	{
		sp<RecordArray> spRA(*i_rg);
		sp<Scope> spScope = spRA->layout()->scope();
		sp<BaseType> spRecordType =
			spScope->typeMaster()->lookupType<Record>();
		sp<BaseType> spRGType =
			spScope->typeMaster()->lookupType< sp<RecordGroup> >();
		unsigned int acnt = spScope->getAttributeCount();
		for(unsigned int a = 0; a < acnt; a++)
		{
			sp<Attribute> spAttr = spScope->attribute(a);
			if(spAttr->type() == spRecordType)
			{
				Accessor<Record> aRecord;
				aRecord.initialize(spScope, spAttr);
				if(aRecord.check(spRA))
				{
					for(int i = 0; i < spRA->length(); i++)
					{
						if(aRecord(spRA, i) == r_rm)
						{
							aRecord(spRA, i) = r_empty;
						}
						else if(a_recurse &&
							a_info->m_rs.find(aRecord(spRA,i)) ==
							a_info->m_rs.end())
						{
							a_info->m_rs.insert(aRecord(spRA,i));
							removeReferencesFromRecord(aRecord(spRA,i),
								r_rm, a_recurse, a_info);
						}
					}
				}
			}
			if(a_recurse && spAttr->type() == spRGType)
			{
				Accessor< sp<RecordGroup> > aRG;
				aRG.initialize(spScope, spAttr);
				if(aRG.check(spRA))
				{
					for(int i = 0; i < spRA->length(); i++)
					{
						if(a_info->m_rgs.find(aRG(spRA,i)) ==
							a_info->m_rgs.end())
						{
							a_info->m_rgs.insert(aRG(spRA,i));
							aRG(spRA,i)->removeReferences(r_rm,
								a_recurse, a_info);
						}
					}
				}
			}
		}
	}
	if(delete_rginfo)
	{
		delete a_info;
	}
}

sp<Scope> RecordGroup::getFirstScope(void)
{
	for(RecordGroup::iterator i_rg = this->begin();
		i_rg != this->end(); i_rg++)
	{
		sp<RecordArray> spRA = *i_rg;
		return spRA->layout()->scope();
	}
	return sp<Scope>();
}

// TODO: switch to single scope per RG rule -- over a decade,
// mixing never actually happened, and such would be unintuitive
// for now, adding:
sp<Scope> RecordGroup::scope(void)
{
	return getFirstScope();
}

/*--------------------------------------------------------------------------*/
/*--------------------------------------------------------------------------*/
typedef sp<RecordGroup> RecordGroupPtr;

String PtrRecordGroupInfo::print(void *instance)
{
	if(!instance)
		return String("<error>");

	const RecordGroupPtr* pspRG = (RecordGroupPtr*)instance;
	if(!(*pspRG).isValid())
		return String("<null>");

	String string;
	string.sPrintf("%d arrays:",(*pspRG)->size());
	for(RecordGroup::iterator it=(*pspRG)->begin();it!=(*pspRG)->end();it++)
	{
		sp<RecordArray> spRA= *it;
		string.catf(" %d %s",spRA->length(),spRA->layout()->name().c_str());
	}
	return string;
}

IWORD PtrRecordGroupInfo::output(std::ostream &ostrm, void *instance,
		t_serialMode mode)
{
	const RecordGroupPtr* pspRG = (RecordGroupPtr*)instance;
	sp<Scope> spScope = (*pspRG)->scope();

	sp<data::StreamI> spStream=(mode == e_binary)?
			sp<data::StreamI>(new data::BinaryStream(spScope)):
			sp<data::StreamI>(new data::AsciiStream(spScope));

	sp<RecordGroup> spRG(new RecordGroup());
	spStream->output(ostrm,*pspRG);

	return c_ascii;
}

void PtrRecordGroupInfo::input(std::istream &istrm, void *instance,
		t_serialMode mode)
{
	sp<SingleMaster> spSingleMaster=SingleMaster::create();
	sp<Master> spMaster=spSingleMaster->master();

	sp<Scope> spScope(new Scope(spMaster));

	RecordGroupPtr* pspRG = (RecordGroupPtr*)instance;

	sp<data::StreamI> spStream=(mode == e_binary)?
			sp<data::StreamI>(new data::BinaryStream(spScope)):
			sp<data::StreamI>(new data::AsciiStream(spScope));

	(*pspRG)=spStream->input(istrm);
}

IWORD PtrRecordGroupInfo::iosize(void)
{
	return c_implicit;
}

bool PtrRecordGroupInfo::getConstruct(void)
{
	return true;
}

void PtrRecordGroupInfo::construct(void *instance)
{
	new(instance)RecordGroupPtr;
}

void PtrRecordGroupInfo::destruct(void *instance)
{
	((RecordGroupPtr *)instance)->~RecordGroupPtr();
}


} /* namespace */

