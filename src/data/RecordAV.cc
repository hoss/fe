/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <data/data.pmh>

#define FE_RAV_SERIAL_TIMER		FALSE

#if FE_RAV_SERIAL_TIMER
#include <chrono>
#endif

namespace fe
{

RecordAV RecordAV::clone(void)
{
	RecordAV r_clone;
	if(!isValid()) { return r_clone; }

	sp<Scope> spScope = m_spLayout->scope();

#if FE_DATA_STORE==FE_AV
	r_clone = spScope->produceRecord(m_spLayout->name());
#else
	feX("RecordAV::clone", "cannot produce Record in SB mode");
#endif

	FE_UWORD cnt = spScope->getAttributeCount();
	for(FE_UWORD i = 0; i < cnt; i++)
	{
		if(m_spLayout->checkAttribute(i))
		{
			sp<Attribute> spAttribute=spScope->attribute(i);
			if(spAttribute->isCloneable())
			{
				sp<BaseType> spBT = spAttribute->type();
				spBT->assign(
					r_clone.rawAttribute(i),
					rawAttribute(i));
			}
		}
	}

	return r_clone;
}

bool RecordAV::extractInstance(Instance &instance, const String &attrName)
{
    FE_UWORD attrIndex;
    FEASSERT(m_spLayout.isValid());
    const sp<Attribute>& rspAttribute =
        m_spLayout->scope()->findAttribute(attrName, attrIndex);
    if(!rspAttribute.isValid())
    {
        return false;
    }

    if(!m_spLayout->checkAttribute(attrIndex))
    {
        return false;
    }

    const sp<BaseType>& rspBT = rspAttribute->type();

    void *pV = rawAttribute(attrIndex);

    std::atomic<int> *pRefCnt = NULL;

	FE_UWORD refCountIndex = m_spLayout->scope()->refCount().index();
	if(m_spLayout->checkAttribute(refCountIndex))
	{
		pRefCnt = &(accessAttribute< std::atomic<int> >(refCountIndex));
	}

    instance.set(pV, rspBT, pRefCnt);

    return true;
}

String RecordAVInfo::print(void *instance)
{
	if(!instance)
		return String("<error>");

	const RecordAV* pR = (RecordAV*)instance;
	if(!pR->isValid())
		return String("<invalid>");

	String string;
	string.sPrintf("%u %s",pR->idr(),pR->layout().isValid()?
			pR->layout()->name().c_str(): "<invalid layout>");
	return string;
}

IWORD RecordAVInfo::output(std::ostream &ostrm, void *instance,
	t_serialMode mode)
{
#if FE_RAV_SERIAL_TIMER
	std::chrono::steady_clock::time_point tp0=
			std::chrono::steady_clock::now();
#endif

	const Record& rRecord = *(Record*)instance;
	sp<Scope> spScope = rRecord.layout()->scope();

	sp<data::StreamI> spStream=(mode == e_binary)?
			sp<data::StreamI>(new data::BinaryStream(spScope)):
			sp<data::StreamI>(new data::AsciiStream(spScope));

	sp<RecordGroup> spRG(new RecordGroup());
	spRG->add(rRecord);

#if FE_RAV_SERIAL_TIMER
	std::chrono::steady_clock::time_point tp1=
			std::chrono::steady_clock::now();
#endif

	spStream->output(ostrm,spRG);

#if FE_RAV_SERIAL_TIMER
	std::chrono::steady_clock::time_point tp2=
			std::chrono::steady_clock::now();
	const Real microseconds0=1e6*
			std::chrono::duration<float>(tp1-tp0).count();
	const Real microseconds1=1e6*
			std::chrono::duration<float>(tp2-tp1).count();
	feLog("RecordAVInfo::output time %4.1f us %4.1f us\n",
			microseconds0,microseconds1);
#endif

	return c_ascii;
}

void RecordAVInfo::input(std::istream &istrm, void *instance,
	t_serialMode mode)
{
#if FE_RAV_SERIAL_TIMER
	std::chrono::steady_clock::time_point tp0=
			std::chrono::steady_clock::now();
#endif

	Record& rRecord = *(Record*)instance;
//	feLog("RecordAVInfo::input record valid %d scope %p\n",
//			rRecord.isValid(),
//			rRecord.isValid()? rRecord.layout()->scope().raw(): NULL);

	sp<Scope> spScope;
	if(rRecord.isValid())
	{
		spScope=rRecord.layout()->scope();
	}
	else
	{
		sp<SingleMaster> spSingleMaster=SingleMaster::create();
		sp<Master> spMaster=spSingleMaster->master();

		spScope=new Scope(spMaster);
		spScope->setLocking(FALSE);
	}

	sp<data::StreamI> spStream=(mode == e_binary)?
			sp<data::StreamI>(new data::BinaryStream(spScope)):
			sp<data::StreamI>(new data::AsciiStream(spScope));

#if FE_RAV_SERIAL_TIMER
	std::chrono::steady_clock::time_point tp1=
			std::chrono::steady_clock::now();
#endif

	sp<RecordGroup> spRG=spStream->input(istrm);

	sp<RecordArray> spRA= *spRG->begin();
	if(spRA.isValid() && spRA->length()>0)
	{
		rRecord=spRA->getRecord(0);
	}

#if FE_RAV_SERIAL_TIMER
	std::chrono::steady_clock::time_point tp2=
			std::chrono::steady_clock::now();
	const Real microseconds0=1e6*
			std::chrono::duration<float>(tp1-tp0).count();
	const Real microseconds1=1e6*
			std::chrono::duration<float>(tp2-tp1).count();
	feLog("RecordAVInfo::input time %4.1f us %4.1f us\n",
			microseconds0,microseconds1);
#endif
}

IWORD RecordAVInfo::iosize(void)
{
	return c_implicit;
}

bool RecordAVInfo::getConstruct(void)
{
	return true;
}

void RecordAVInfo::construct(void *instance)
{
	new(instance)RecordAV;
}

void RecordAVInfo::destruct(void *instance)
{
	((RecordAV *)instance)->~RecordAV();
}



} /* namespace */


