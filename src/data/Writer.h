/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __data_Writer_h__
#define __data_Writer_h__

namespace fe
{
namespace data
{

#define FE_KW_RECORD		"RECORD"
#define FE_KW_RECORDGROUP	"RECORDGROUP"
#define FE_KW_DEFAULTGROUP	"DEFAULTGROUP"
#define FE_KW_TEMPLATE		"TEMPLATE"
#define FE_KW_ATTRIBUTE		"ATTRIBUTE"
#define FE_KW_LAYOUT		"LAYOUT"
#define FE_KW_INFO			"INFO"
#define FE_KW_END			"END"
#define FE_KW_COMMENT		"COMMENT"
#define FE_KW_STARTBINARY	'~'
#define FE_KW_ENDBINARY		'~'

#if 0
typedef std::map<sp<Layout>, t_layout_info>	t_layout_loinfo;
typedef std::map<sp<RecordGroup>, int>			t_rg_id;
typedef std::map<sp<RecordArray>, int>			t_ra_id;
typedef std::map<void *, int>					t_sb_id;
typedef std::map<sp<Attribute>, int>			t_attr_id;
typedef Array<sp<Attribute> >					t_attr_array;
#endif

class FE_DL_EXPORT Writer : public Counted
{
	public:
					Writer(void):	m_name("Writer")						{}

virtual	void		output(std::ostream &ostrm, sp<RecordGroup> spRG)		= 0;
const	String&		name(void) const	{ return m_name; }

	private:
		String		m_name;
};

} /* namespace */
} /* namespace */

#endif /* __data_Writer_h__ */

