/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <data/data.pmh>

#define FE_SC_RECORDFACTORY_DEBUG		FALSE
#define FE_SC_PADDING_VERBOSE			FALSE

namespace fe
{

Scope::Scope(void)
{
}

void Scope::initialize(void)
{
	if(m_spTypeMaster.isValid())
		return;

	m_hpRegistry=registry();
	if(!m_hpRegistry.isValid())
	{
		feLog("Scope::Scope WARNING registry() failed\n");
		FEASSERT(m_hpRegistry.isValid());
		return;
	}
	sp<Master> spMaster=m_hpRegistry->master();
	if(!spMaster.isValid())
	{
		feLog("Scope::Scope WARNING spRegistry.master() failed\n");
		FEASSERT(spMaster.isValid());
		return;
	}

	construct(spMaster->typeMaster(), spMaster->allocator());
	prepareCookbook(spMaster);

#if FALSE
	//* WARNING memory bloat
	sp<RecordGroup> &rg_scopes =
			spMaster->catalog()->catalog< sp<RecordGroup> >("Scopes");
	if(!rg_scopes.isValid())
	{
		rg_scopes = new RecordGroup();
	}
	sp<Layout> l_component = declare("l_component");
	AsComponent asComponent;
	asComponent.populate(l_component);
	Record r_self = createRecord(l_component);
	asComponent.component(r_self) = this;
	rg_scopes->add(r_self);
#endif
}

Scope::Scope(Master &master)
{
	construct(master.typeMaster(), master.allocator());
	sp<Master> spMaster(&master);
	prepareCookbook(spMaster);
}

Scope::Scope(sp<Master> &spMaster)
{
	construct(spMaster->typeMaster(), spMaster->allocator());
	prepareCookbook(spMaster);
}

Scope::Scope(sp<TypeMaster> spTypeMaster, sp<Allocator> allocator)
{
	construct(spTypeMaster, allocator);
	sp<Master> spMaster(NULL);
	prepareCookbook(spMaster);
}

void Scope::prepareCookbook(sp<Master>& rspMaster)
{
	m_spRecordCookbook=NULL;
	if(registry().isValid())
	{
		m_spRecordCookbook=registry()->master()->catalog()->catalogComponent(
				"*.RecordCookbook","RecordCookbook");
	}
	else if(rspMaster.isValid())
	{
		sp<Component>& rspRecordCookbook=rspMaster->catalog()
				->catalog< sp<Component> >("RecordCookbook");
		if(rspRecordCookbook.isValid())
		{
			m_spRecordCookbook=rspRecordCookbook;
		}
		else
		{
			m_spRecordCookbook=
					Library::create<RecordCookbook>("RecordCookbook");
			m_spRecordCookbook->registerDependency(rspMaster->registry());
			rspRecordCookbook=m_spRecordCookbook;
		}
	}
	else
	{
		m_spRecordCookbook=Library::create<RecordCookbook>("RecordCookbook");
	}
}

StringStringsMap& Scope::factoryMultiMap(void)
{
	return m_spRecordCookbook->factoryMultiMap();
}

void Scope::setName(const String& name)
{
	Component::setName(name);

#if FALSE
	if(!registry().isValid() || !registry()->master().isValid())
	{
		return;
	}

	//* NOTE not thread-safe
	registry()->master()->catalog()->catalogComponent(
			sp<Component>(this), name);
#endif
}

void assertData(sp<TypeMaster> spTypeMaster)
{
	sp<BaseType> spBT =
		spTypeMaster->assertType<sp<RecordGroup> >("RecordGroup");
	if(!spBT->getInfo().isValid())
	{
		spBT->setInfo(new PtrRecordGroupInfo());
	}
	spBT = spTypeMaster->assertType<sp<RecordGroup> >("group");

	spBT =
		spTypeMaster->assertType<sp<RecordArray> >("RecordArray");
	if(!spBT->getInfo().isValid())
	{
		spBT->setInfo(new PtrRecordArrayInfo());
	}
	spBT = spTypeMaster->assertType<sp<RecordArray> >("array");

	spBT = spTypeMaster->assertType<Record>("record");
	if(!spBT->getInfo().isValid())
	{
		spBT->setInfo(new RecordInfo());
	}

	spBT = spTypeMaster->assertType<WeakRecord>("WeakRecord");
	if(!spBT->getInfo().isValid())
	{
		spBT->setInfo(new WeakRecordInfo());
	}

	spBT = spTypeMaster->assertPtr<Component>("component");
	spBT = spTypeMaster->assertPtr<Scope>("scope");
	spBT = spTypeMaster->assertPtr<Layout>("layout");
}

void Scope::construct(sp<TypeMaster> spTypeMaster, sp<Allocator> allocator)
{
	m_locking=true;
	m_nullAttribute = NULL;
	m_nextUniqueLayoutID = 0;
	m_nextUniqueIDR = 1;	//* NOTE idr 0 is an error condition

	setName("Scope");

	m_spTypeMaster = spTypeMaster;

	if(allocator.isValid())
	{
		m_spAllocator = allocator;
	}
	else
	{
		m_spAllocator = new BasicAllocator();
	}

	//m_spTypeMaster->assertPtr<RecordGroup>("RecordGroup");
	assertData(m_spTypeMaster);

	support(FE_USE(":CT"), "atomic_integer");
	m_paCount = new Accessor< std::atomic<int> >;
	m_paCount->initialize(this, FE_USE(":CT"));
	findAttribute(FE_USE(":CT"))->setSerialize(false);
	findAttribute(FE_USE(":CT"))->setCloneable(false);

#if FE_SCOPE_GETRECORD
	support(FE_USE(":ID"), "integer");
	m_paIDNumber = new Accessor<int>;
	m_paIDNumber->initialize(this, FE_USE(":ID"));
#endif

	sp<Attribute> spAttrSN=support(FE_USE(":SN"), "atomic_integer");
	spAttrSN->setSerialize(FALSE);
	spAttrSN->setCloneable(FALSE);
	m_paSerialNumber = new Accessor< std::atomic<int> >;
	m_paSerialNumber->initialize(this, FE_USE(":SN"));

	m_serialIndex=attributeIndex(m_paSerialNumber->attribute());

	m_looking=false;
	m_serialCount=0;
	m_storeName = "";

#if FE_COUNTED_TRACK
	Counted::registerRegion(this,sizeof(Scope));
#endif
}

Scope::Scope(const Scope &other):
	Castable(),
	Initialized(),
	Component(),
	Initialize<Scope>(),
	ObjectSafe<Scope>()
{
	FEASSERT(0);
}

Scope::~Scope(void)
{
	shutdown();
}

bool Scope::shutdown(void)
{
	SAFEGUARD;
	m_layouts.clear();
	//m_storeMap.clear();
	m_depend.clear();
#if FE_SCOPE_GETRECORD
	m_idNumbered.clear();
	m_freeIDNumbers.clear();
#endif
	m_spAllocator = NULL;
	m_spTypeMaster = NULL;
	if(m_paCount)
	{
		delete m_paCount;
		m_paCount = NULL;
	}
#if FE_SCOPE_GETRECORD
	if(m_paIDNumber)
	{
		delete m_paIDNumber;
		m_paIDNumber = NULL;
	}
#endif
	if(m_paSerialNumber)
	{
		delete m_paSerialNumber;
		m_paSerialNumber = NULL;
	}
	return true;
}

Scope &Scope::operator=(const Scope &other)
{
	FEASSERT(0);
	return *this;
}

sp<RecordCookbook> Scope::cookbook(void)
{
	return m_spRecordCookbook;
}

void Scope::layoutValidate(void)
{
	SAFEGUARD_IF(m_locking);

	for(t_depends::iterator it_outer = m_depend.begin();
			it_outer != m_depend.end();
			it_outer++)
	{
		t_depends::iterator it_inner = it_outer;
		for(	it_inner++;
				it_inner != m_depend.end();
				it_inner++)
		{
			if(**it_outer == **it_inner)
			{
				// delete duplicate
				//t_depends::iterator it_remove = it_inner;
				//it_inner--;
				sp<BaseType> inner_t =
					m_spTypeMaster->lookupName((*it_inner)->attributeType());
				feX("Scope::layoutValidate",
					"duplicates should no longer get here [%s]",
					(*it_inner)->attributeName().c_str());
				//m_depend.remove(*it_remove);
			}
			else if((*it_outer)->attributeName() == (*it_inner)->attributeName())
			{
				sp<BaseType> inner_t =
					m_spTypeMaster->lookupName((*it_inner)->attributeType());
				sp<BaseType> outer_t =
					m_spTypeMaster->lookupName((*it_outer)->attributeType());
				if(inner_t != outer_t)
				{
					if(		((*it_outer)->dependFlag() & Depend::e_available)
						&&	((*it_inner)->dependFlag() & Depend::e_available) )
					{
						Peeker peek;
						peek(*this);
						if(!inner_t.isValid())
						{
							feX("fe::Scope::layoutValidate",
								"%s: attribute type not valid: %s\n%s",
								(*it_inner)->attributeName().c_str(),
								(*it_inner)->attributeType().c_str(),
								peek.output().c_str());
						}
						if(!outer_t.isValid())
						{
							feX("fe::Scope::layoutValidate",
								"%s: attribute type not valid: %s\n%s",
								(*it_outer)->attributeName().c_str(),
								(*it_outer)->attributeType().c_str(),
								peek.output().c_str());
						}
						feX("fe::Scope::layoutValidate",
							"attribute type conflict on %s: %s != %s\n%s",
							(*it_outer)->attributeName().c_str(),
							(*it_outer)->attributeType().c_str(),
							(*it_inner)->attributeType().c_str(),
							peek.output().c_str());
					}
				}
			}
		}
	}
}

sp<Attribute> Scope::dynamicAddAttribute(const String &typeName,
		const String &attributeName)
{
#if FE_CODEGEN<=FE_DEBUG
	layoutValidate();
#endif

	SAFEGUARD_IF(m_locking);

	// if attribute already exists return
	FE_UWORD attributeIndex;
	sp<Attribute> spAttribute = findAttribute(attributeName, attributeIndex);
	if(spAttribute.isValid())
	{
		return spAttribute;
	}

#if FE_CODEGEN<=FE_DEBUG
	FE_UWORD cnt =
#endif
			getAttributeCount();

	spAttribute = addAttribute(typeName, attributeName);

#if FE_CODEGEN<=FE_DEBUG
	FE_UWORD newcnt =
#endif
			getAttributeCount();

	FEASSERT(newcnt == cnt+1);

#if 0
	FE_UWORD layoutCount = m_layouts.size();
	for(FE_UWORD index = 0; index < layoutCount; index ++)
	{
		const Offset *lastOffsetTable = m_layouts[index]->rawOffsetTable();
		if(lastOffsetTable)
		{
			m_layouts[index]->setOffsetTable(new Offset[newcnt]);
			for(FE_UWORD i = 0;i < cnt; i++)
			{
				m_layouts[index]->setOffset(i, lastOffsetTable[i]);
			}
			m_layouts[index]->setOffset(cnt, offsetNone);
/*
			feLog("Scope:dynamicAddAttribute %d %s delete[] %p %s\n",
					index,m_layouts[index]->name().c_str(),
					lastOffsetTable,attributeName.c_str());
*/
			delete[] lastOffsetTable;
		}
	}
#endif
#if 0
	for(unsigned int i = 0; i < m_offsetTables.size(); i++)
	{
		m_offsetTables[i]->resize(newcnt);
	}
#endif
	resizeLayoutLocators();

	return spAttribute;
}

sp<Attribute> Scope::addAttribute(const String &typeName,
		const String &attributeName)
{
	SAFEGUARD_IF(m_locking);

	sp<BaseType> spAttributeType;
	FEASSERT(m_spTypeMaster.isValid());
	spAttributeType = m_spTypeMaster->lookupName(typeName);
	if(!spAttributeType.isValid())
	{
		feX("fe::Scope::addAttribute",
			"could not add \'%s\'; attribute type \'%s\' not known",
			attributeName.c_str(), typeName.c_str());
	}
	sp<Attribute> spAttribute(new Attribute(attributeName,spAttributeType,
			m_spTypeMaster));
	unsigned int n = m_attributes.size();
	m_indexmap[attributeName] = n;
	//m_attrmap[spAttribute.raw()] = n;
	m_attributes.push_back(spAttribute);

#if FE_COUNTED_TRACK
	spAttribute->trackReference(this,"Scope::addAttribute");
#endif

	return spAttribute;
}

sp<Layout> Scope::declare(const String &name)
{
	FEASSERT(!name.empty());
	if(name.empty())
	{
		feLog("Scope::declare empty name\n");
		return sp<Layout>(NULL);
	}

	SAFEGUARD_IF(m_locking);

	sp<Layout> spLayout = lookupLayout(name);
	if(spLayout.isValid())
	{
		return spLayout;
	}

	populate(name, FE_USE(":CT"));
	populate(name, FE_USE(":SN"));
	//populate(name, FE_I);

	spLayout = new LayoutDefault(this, name);
	m_layouts.push_back(spLayout);
	spLayout->adjoin(sp<Scope>(this));

	return spLayout;
}

#if 0
bool Scope::lookupStore(sp<Layout> spLayout, sp<StoreI> &spStore)
{
	SAFEGUARD;
	layout_store_map::iterator it = m_storeMap.find(spLayout);
	if(it != m_storeMap.end())
	{
		spStore = it->second;
		return true;
	}

	return false;
}
#endif

#if 0
void Scope::freeIDNumber(const Record &r_old)
{
	SAFEGUARD;
	int *pID;
	if(m_paIDNumber &&
		m_paIDNumber->queryAttribute(r_old, pID))
	{
//		feLog("freeIDNumber %p %d\n",stateblock,*pID);
		m_freeIDNumbers.push_back(*pID);
		m_idNumbered[*pID] = WeakRecord();
	}

	//* piggyback
	int *pSN;
	if(m_paSerialNumber &&
		m_paSerialNumber->queryAttribute(r_old, pSN))
	{
//		feLog("freeIDNumber SN %p %d\n",stateblock,*pSN);
		*pSN= -1;
	}
}
#endif


#if FE_SCOPE_GETRECORD
Record Scope::getRecord(IWORD id)
{
	if(id >= (IWORD)(m_idNumbered.size()))
	{
		feX(e_invalidRange,
			"Scope::getRecord",
			"index out of range");
	}
	if(!m_idNumbered[id].isValid())
	{
		feX(e_invalidRange,
			"Scope::getRecord",
			"record id not valid");
	}
	Record record;
	//record.set(m_idNumbered[id].data());
	record = m_idNumbered[id];
	return record;
}
#endif

void Scope::registerFactory(String name,sp<RecordFactoryI> spRecordFactoryI)
{
	m_factoryMap[name]=spRecordFactoryI;
#if FE_COUNTED_TRACK
	if(spRecordFactoryI.isValid())
	{
		spRecordFactoryI->trackReference(this,"Scope::registerFactory");
	}
#endif
}

Record Scope::produceRecord(String name)
{
	Record record;

	Array<String> layoutList;
	m_spRecordCookbook->findLayoutList(sp<Scope>(this),name,layoutList);
#if FE_SC_RECORDFACTORY_DEBUG
	feLog("Scope::produceRecord \"%s\" layoutList size %d\n",
			name.c_str(),layoutList.size());
#endif

	if(layoutList.size()>1)
	{
		sp<Layout> spLayout=declare(name);
		for(U32 m=0;m<layoutList.size();m++)
		{
			String prerequisite=layoutList[m];

#if FE_SC_RECORDFACTORY_DEBUG
			feLog("Scope::produceRecord \"%s\" clones \"%s\"\n",
					name.c_str(),prerequisite.c_str());
#endif

			clonePopulate(prerequisite.c_str(),name);
		}
		record=createRecord(spLayout);

		// NOTE once we make the Layout, findLayoutList won't give the old ones
		factoryMultiMap()[name]=layoutList;
	}
	else if(factoryMultiMap().find(name)!=factoryMultiMap().end())
	{
		layoutList=factoryMultiMap()[name];

		sp<Layout> spLayout=lookupLayout(name);

#if FE_SC_RECORDFACTORY_DEBUG
		feLog("Scope::produceRecord \"%s\" mapped (%d)\n",
				name.c_str(),spLayout.isValid());
#endif

		if(spLayout.isValid())
		{
			record=createRecord(spLayout);
		}
	}

	for(U32 m=0;m<layoutList.size();m++)
	{
		String prerequisite=layoutList[m];

#if FE_SC_RECORDFACTORY_DEBUG
		feLog("Scope::produceRecord \"%s\" requires \"%s\"\n",
				name.c_str(),prerequisite.c_str());
#endif

		sp<RecordFactoryI> spRecordFactoryI=m_factoryMap[prerequisite];
/*
		if(!spRecordFactoryI.isValid() && registry().isValid())
		{
			String implementation="RecordFactoryI."+prerequisite;
#if FE_SC_RECORDFACTORY_DEBUG
			feLog("Scope::produceRecord \"%s\" trying \"%s\"\n",
					name.c_str(),implementation.c_str());
#endif
			spRecordFactoryI=create(implementation);
			if(spRecordFactoryI.isValid())
			{
#if FE_SC_RECORDFACTORY_DEBUG
				feLog("Scope::produceRecord \"%s\" created as \"%s\"\n",
						name.c_str(),implementation.c_str());
#endif
				m_factoryMap[prerequisite]=spRecordFactoryI;
			}
		}
*/
		if(spRecordFactoryI.isValid())
		{
			sp<Scope> spScope(this);
			spRecordFactoryI->bind(spScope);
			spRecordFactoryI->produce(record);
		}
		else if(!record.isValid())
		{
			sp<Layout> spLayout=lookupLayout(prerequisite);
			if(spLayout.isValid())
			{
				record=createRecord(spLayout);
			}
		}
	}

	if(!record.isValid())
	{
		feLog("Scope::produceRecord \"%s\" does not name a"
				" layout, factory, or recipe\n",
				name.c_str());
	}
	else
	{
		m_spRecordCookbook->cook(sp<Scope>(this),name,record);
	}
	return record;
}

void Scope::finalize(Record& rRecord)
{
	String name=rRecord.layout()->name();

	Array<String> layoutList;
	m_spRecordCookbook->findLayoutList(sp<Scope>(this),name,layoutList);

	if(layoutList.size()>1)
	{
		feX("Scope::finalize","\"%s\" has %d layouts\n",
				name.c_str(),layoutList.size());
	}
	else if(!layoutList.size())
	{
#if FE_SC_RECORDFACTORY_DEBUG
		feLog("Scope::finalize","\"%s\" has no layouts\n",
				name.c_str());
#endif
		return;
	}
	else if(factoryMultiMap().find(name)!=factoryMultiMap().end())
	{
		layoutList=factoryMultiMap()[name];
	}

	for(U32 m=0;m<layoutList.size();m++)
	{
		String prerequisite=layoutList[m];

#if FE_SC_RECORDFACTORY_DEBUG
		feLog("Scope::finalize \"%s\" uses \"%s\"\n",
				name.c_str(),prerequisite.c_str());
#endif

		sp<RecordFactoryI> spRecordFactoryI=m_factoryMap[prerequisite];
		if(spRecordFactoryI.isValid())
		{
			sp<Scope> spScope(this);
			spRecordFactoryI->bind(spScope);
			spRecordFactoryI->finalize(rRecord);
		}
#if FE_SC_RECORDFACTORY_DEBUG
		else
		{
			feLog("Scope::finalize \"%s\" does not name a factory\n",
					name.c_str());
		}
#endif
	}
}

Record Scope::createRecord(const String layout)
{
	sp<Layout> spLayout=lookupLayout(layout);
	return createRecord(spLayout);
}

sp<RecordArray> Scope::createRecordArray(const String layout,FE_UWORD count)
{
	sp<Layout> spLayout=lookupLayout(layout);
	return createRecordArray(spLayout,count);
}

Record Scope::createRecord(sp<Layout> spLayout)
{
#if FALSE
	sp<LayoutDefault> spLayoutDefault(spLayout);
#else
	//* NOTE known cast (but doesn't appear to be noticably faster)
	sp<LayoutDefault> spLayoutDefault(
			static_cast<LayoutDefault*>(spLayout.raw()));
#endif

	if(spLayoutDefault.isValid())
	{
		return spLayoutDefault->createRecord();
	}
	else
	{
		// TODO: AJW: fix this
		feX("Scope::createRecord", "can only create one version");
	}
}

sp<RecordArray> Scope::createRecordArray(sp<Layout> spLayout,FE_UWORD count)
{
	sp<RecordArray> spRA(new RecordArray(spLayout, count));
	return spRA;
}

#if 0
void Scope::freeRecord(Record record)
{
	SAFEGUARD_IF(m_locking);

	if(m_paCount->check(record))
	{
		feX("fe::Scope::freeRecord",
			"attempt to explicitly free reference counted record");
	}

#if FE_SCOPE_SUPPORT_WATCHERS
	unwatch(record);
#endif

	sp<LayoutDefault> spLayoutDefault(record.layout());
	if(spLayoutDefault.isValid())
	{
		spLayoutDefault->freeRecord(record);
	}
	else
	{
		// TODO: AJW: fix this
		feX("Scope::freeRecord", "can only free one version");
	}
}
#endif

#if 0
void Scope::lockLayout(sp<Layout> spLayout)
{
	spLayout->lock();
	return;
}
#endif

sp<Layout> Scope::lookupLayout(const String &name)
{
	SAFEGUARD_IF(m_locking);

	sp<Layout> spLayout;
	for(FE_UWORD i = 0; i < m_layouts.size(); i++)
	{
		if(m_layouts[i]->name() == name)
		{
			spLayout = m_layouts[i];
		}
	}
	if(!m_looking && !spLayout.isValid())
	{
		if(!m_factoryMap[name].isValid() && registry().isValid())
		{
			String implementation="RecordFactoryI."+name;
#if FE_SC_RECORDFACTORY_DEBUG
			feLog("Scope::lookupLayout \"%s\" trying \"%s\"\n",
					name.c_str(),implementation.c_str());
#endif
			const BWORD quiet=TRUE;
			registerFactory(name,create(implementation,quiet));
			if(m_factoryMap[name].isValid())
			{
				m_factoryMap[name]->setName(name);
			}
		}
		if(m_factoryMap[name].isValid())
		{
			m_looking=TRUE;

			sp<Scope> spScope(this);
			m_factoryMap[name]->bind(spScope);
			spLayout=m_factoryMap[name]->layout();

			m_looking=FALSE;
		}
	}

	return spLayout;
}

FE_UWORD Scope::getAttributeCount(void) const
{
	SAFEGUARD_IF(m_locking);

	const I32 count=m_attributes.size();

	return count;
}

sp<Attribute> Scope::support(const String &mayHave, const String &ofType)
{
	sp<Depend> spDepend(new Depend(mayHave));
	spDepend->attributeType() = ofType;
	spDepend->dependFlag() = Depend::e_available;
	return addDepend(spDepend);
}

bool Scope::enforce(const String &ifHas, const String &mustHave)
{
	sp<Depend> spDepend(new Depend(mustHave));
	spDepend->matchAttributes().push_back(ifHas);
	spDepend->dependFlag() = Depend::e_attribute;
	addDepend(spDepend);
	return true;
}

bool Scope::enforce(const Array<String> &ifHas, const String &mustHave)
{
	sp<Depend> spDepend(new Depend(mustHave));
	spDepend->matchAttributes() = ifHas;
	spDepend->dependFlag() = Depend::e_attribute;
	addDepend(spDepend);
	return true;
}

bool Scope::populate(const String &ifIs, const String &mustHave)
{
	sp<Depend> spDepend(new Depend(mustHave));
	spDepend->dependName() = ifIs;
	spDepend->dependFlag() = Depend::e_populate;
	addDepend(spDepend);
	return true;
}

bool Scope::share(const String &ifIs, const String &share1,
		const String &share2)
{
	sp<Depend> spDepend(new Depend(share1));
	spDepend->shareName() = share2;
	spDepend->dependName() = ifIs;
	spDepend->dependFlag() = Depend::e_share;
	addDepend(spDepend);
	return true;
}

#ifdef DEPRECATE
bool Scope::within(const String &ifIs, const String &child,
		const String &parent, const size_t &offset)
{
	sp<Depend> spDepend(new Depend(child));
	spDepend->shareName() = parent;
	spDepend->dependName() = ifIs;
	spDepend->dependFlag() = Depend::e_within;
	spDepend->offset() = offset;
	addDepend(spDepend);
	return true;
}
#endif

void Scope::clonePopulate(const String &ifThisHas, const String &thenThisDoes)
{
	t_depends::iterator i_depend;
	for(i_depend = m_depend.begin();i_depend!=m_depend.end();i_depend++)
	{
		if((*i_depend)->dependFlag() & Depend::e_populate)
		{
			if((*i_depend)->dependName() == ifThisHas)
			{
				populate(thenThisDoes, (*i_depend)->attributeName());
			}
		}
	}
}

#if 0
bool Scope::clear(const String &ifIs)
{
	SAFEGUARD;
	sp<Layout> spLayout;
	spLayout = lookupLayout(ifIs);
	if(spLayout.isValid() && spLayout->locked())
	{
		feX("fe::Scope::clear","cannot clear locked layout");
	}

	t_depends::iterator it_depend;
	for(it_depend = m_depend.begin();it_depend!=m_depend.end();/*it_depend++*/)
	{
		if((*it_depend)->dependFlag() & Depend::e_populate)
		{
			if((*it_depend)->dependName() == ifIs)
			{
				if((*it_depend)->dependFlag() & Depend::e_available)
				{
					sp<Depend> spDepend(new Depend());
					spDepend = *it_depend;
					spDepend->dependFlag() =
						(*it_depend)->dependFlag() & ~Depend::e_populate;
					m_depend.push_back(spDepend);
				}

				t_depends::iterator it_remove;
				it_remove = it_depend;
				it_depend++;
				m_depend.remove(*it_remove);
			}
			else
			{
				it_depend++;
			}
		}
		else
		{
			it_depend++;
		}
	}
	return true;
}
#endif

sp<Attribute> Scope::addDepend(sp<Depend> &depend)
{
	SAFEGUARD_IF(m_locking);

	sp<Attribute> spAttribute;
	bool duplicate = false;
	// TODO: lookup on a list -- not good.	change data structure or add index
	for(t_depends::iterator i_dep = m_depend.begin();
			i_dep != m_depend.end();
			i_dep++)
	{
		if(*depend == **i_dep)
		{
			duplicate = true;
		}
	}
	if(duplicate)
	{
		if(depend->dependFlag() & Depend::e_available)
		{
			FE_UWORD attributeIndex;
			spAttribute = findAttribute(depend->attributeName(),
					attributeIndex);
		}
		return spAttribute;
	}
	m_depend.push_back(depend);

#if 0
	if(depend->dependFlag() & Depend::e_attribute)
	{
		depAttributeCheck(depend);
	}
	if(depend->dependFlag() & Depend::e_populate)
	{
		depRecordCheck(depend);
	}
#endif
	if(depend->dependFlag() & Depend::e_available)
	{
		spAttribute = dynamicAddAttribute(depend->attributeType(),
				depend->attributeName());
	}

	// notify layouts that the attributes may have changed
	notifyLayoutsOfAttributeChange(depend);

	return spAttribute;
}

void Scope::notifyLayoutsOfAttributeChange(sp<Depend> &depend)
{
	SAFEGUARD_IF(m_locking);

	FE_UWORD layoutCount = m_layouts.size();
	for(FE_UWORD index = 0; index < layoutCount; index++)
	{
		m_layouts[index]->notifyOfAttributeChange(depend);
	}
}

#if 0
void Scope::depAttributeCheck(sp<Depend> &depend)
{
	SAFEGUARD;
	FE_UWORD layoutCount = m_layouts.size();
	for(FE_UWORD index = 0; index < layoutCount; index++)
	{
		if(m_layouts[index]->locked())
		{
			for(unsigned int i = 0; i < depend->matchAttributes().size(); i++)
			{
				if(checkAttribute(depend->matchAttributes()[i],
					m_layouts[index]))
				{
					if(!checkAttribute(depend->attributeName(),
						m_layouts[index]))
					{
						feX("fe::Scope::depAttributeCheck",
							"enforce attribute failure (already locked): "
							"record type: %s MustHave: %s IfHas: %s",
							m_layouts[index]->name().c_str(),
							depend->attributeName().c_str(),
							depend->matchAttributes()[i].c_str());
					}
				}
			}
		}
	}
}

void Scope::depRecordCheck(sp<Depend> &depend)
{
	SAFEGUARD;
	FE_UWORD layoutCount = m_layouts.size();
	for(FE_UWORD index = 0; index < layoutCount; index++)
	{
		if(m_layouts[index]->locked())
		{
			if(m_layouts[index]->name() == depend->dependName())
			{
				if(!checkAttribute(depend->attributeName(),m_layouts[index]))
				{
					feX("fe::Scope::depRecordCheck",
						"enforce attribute failure (already locked): "
						"record type: %s MustHave: %s IfIs: %s",
						m_layouts[index]->name().c_str(),
						depend->attributeName().c_str(),
						depend->dependName().c_str());
				}
			}
		}
	}
}
#endif

sp<Attribute>& Scope::findAttribute(const String &name)
{
	FE_UWORD dummy;
	return findAttribute(name, dummy);
}

sp<Attribute>& Scope::findAttribute(const String &name, FE_UWORD &index)
{
	SAFEGUARD_IF(m_locking);

#if FALSE
	// this is actually slower last time i tested it
	t_indexmap::iterator i_index = m_indexmap.find(name);
	if(i_index != m_indexmap.end())
	{
		index = i_index->second;
		return m_attributes[index];
	}
#else
	for(index = 0;index < m_attributes.size(); index++)
	{
		if(name == m_attributes[index]->name())
		{
			return m_attributes[index];
		}
	}
#endif

	return m_nullAttribute;
}

/// @todo cache m_index in Attribute itself for O(1) lookup
FE_UWORD Scope::attributeIndex(sp<Attribute> spAttribute)
{
	SAFEGUARD_IF(m_locking);

#if 0
	t_attrmap::iterator i_index = m_attrmap.find(spAttribute.raw());
	if(i_index != m_attrmap.end())
	{
		return i_index->second;
	}
#endif

#if 1
	for(FE_UWORD index = 0;index < m_attributes.size(); index++)
	{
		if(m_attributes[index] == spAttribute)
		{
			return index;
		}
	}
#endif

	feX("fe::Scope::attributeIndex","attribute scope mismatch");
	return 0;
}

sp<Attribute> Scope::attribute(FE_UWORD index)
{
	SAFEGUARD_IF(m_locking);

	return m_attributes[index];
}

void Scope::peek(Peeker &peeker)
{
	SAFEGUARD_IF(m_locking);

	peeker.nl();
	peeker.bold();
	peeker.cat("Scope:");
	peeker.normal();
	peeker.in();

	// attributes
	peeker.nl();
	peeker.cat("Attributes:");
	peeker.in();
	for(t_attributes::iterator it = m_attributes.begin();
			it != m_attributes.end(); it++)
	{
		peeker.nl();
		peeker(**it);
		peeker.cat(":");
		std::list<String> names;
		m_spTypeMaster->reverseLookup((*it)->type(),names);
		for(std::list<String>::iterator n_it = names.begin();
			n_it != names.end(); n_it++)
		{
			peeker.cat(" ");
			peeker.cat(*n_it);
		}
	}
	peeker.out();

	// layouts
	peeker.nl();
	peeker.cat("Layouts:");
	peeker.in();
	for(t_layouts::iterator it = m_layouts.begin(); it != m_layouts.end(); it++)
	{
		peeker(**it);
	}
	peeker.out();

#if 1
	// depends
	peeker.nl();
	peeker.cat("Depends:");
	peeker.in();
	for(t_depends::iterator it = m_depend.begin(); it != m_depend.end(); it++)
	{
		peeker.nl();
		peeker(**it);
	}
	peeker.out();
#endif

	peeker.out();
}

Accessor< std::atomic<int> > &Scope::refCount(void)
{
//	SAFEGUARD;
	FEASSERT(m_paCount);
	return *m_paCount;
}

void Scope::addAccessorSet(AccessorSet *a_as)
{
	SAFEGUARD_IF(m_locking);

	assert(a_as);
	m_accessorSets.insert(a_as);
}

void Scope::removeAccessorSet(AccessorSet *a_as)
{
	SAFEGUARD_IF(m_locking);

	m_accessorSets.erase(a_as);
}


#if 0
sp<StoreI> Scope::createStore(sp<Layout> spLayout) const
{
	SAFEGUARD;
	sp<StoreI> spStore;
	if(m_hpRegistry.isValid() && m_storeName != "")
	{
		spStore = m_hpRegistry->create(m_storeName);
		if(!spStore.isValid())
		{
			spStore = new SegmentStore;
		}
	}
	else
	{
		spStore = new SegmentStore;
	}
	spStore->setLayout(spLayout);

	return spStore;
}
#endif

#if 0
void Scope::specifyStore(sp<Registry> spRegistry, const String &storeName)
{
	SAFEGUARD;
	m_hpRegistry = spRegistry;
	m_storeName = storeName;
}
#endif

#if FE_SCOPE_SUPPORT_WATCHERS
void Scope::addWatcher(sp<WatcherI> spWatch)
{
	m_watchers.push_back(spWatch);
}

bool Scope::removeWatcher(sp<WatcherI> spWatch)
{
	for(t_watchers::iterator it = m_watchers.begin();
		it != m_watchers.end(); it++)
	{
		if(*it == spWatch)
		{
			m_watchers.erase(it);
			return true;
		}
	}
	return false;
}

void Scope::watch(const Record &record)
{
	for(t_watchers::iterator it = m_watchers.begin();
		it != m_watchers.end(); it++)
	{
		(*it)->add(record);
	}
}

void Scope::unwatch(const Record &record)
{
	for(t_watchers::iterator it = m_watchers.begin();
		it != m_watchers.end(); it++)
	{
		(*it)->remove(record);
	}
}
#endif

void Scope::resizeLayoutLocators(void) const
{
	FE_UWORD attr_cnt = getAttributeCount();
	for(FE_UWORD i = 0; i < m_layouts.size(); i++)
	{
		m_layouts[i]->resizeLocatorTable(attr_cnt);
	}

	for(std::set<AccessorSet *>::const_iterator i_s = m_accessorSets.begin();
		i_s != m_accessorSets.end(); i_s++)
	{
		AccessorSet *as = *i_s;
		t_bitset bitset(m_attributes.size());

		for(unsigned int j = 0; j < as->size(); j++)
		{
			bitset.set((*as)[j]->index());
		}

		as->setBitset(bitset);
	}
}

#if 0
OffsetTable::OffsetTable(void)
{
	m_table = NULL;
	m_size = 0;
	suppressReport();
#if FE_COUNTED_TRACK
	setName("OffsetTable");
#endif
}

OffsetTable::~OffsetTable(void)
{
	if(m_table) { delete [] m_table; }
}

void OffsetTable::resize(unsigned int a_size)
{
	Offset *pOffset = new Offset[a_size];
	for(unsigned int i = 0; i < a_size; i++)
	{
		pOffset[i] = offsetNone;
	}
	if(m_table != NULL)
	{
		for(unsigned int i = 0; i < m_size; i++)
		{
			pOffset[i] = m_table[i];
		}
		delete [] m_table;
	}
	m_table = pOffset;
	m_size = a_size;
	for(FE_UWORD index = 0; index < m_layouts.size(); index++)
	{
		m_layouts[index]->setOffsetTable(m_table);
	}
}

void OffsetTable::attach(sp<Layout> a_layout)
{
	m_layouts.push_back(a_layout);
	a_layout->setOffsetTable(m_table);
}
#endif

} /* namespace */

