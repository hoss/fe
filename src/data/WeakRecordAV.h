/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __data_WeakRecordAV_h__
#define __data_WeakRecordAV_h__

namespace fe
{

/**	@brief Non-persistent reference to an instance of a Layout

	@ingroup data
*/
class FE_DL_EXPORT WeakRecordAV
{
	friend class Scope;
	friend class RecordArrayAV;
	public:
					WeakRecordAV(I32 ignored=0);
					WeakRecordAV(const RecordAV &other);
					WeakRecordAV(const WeakRecordAV &other);
virtual				~WeakRecordAV(void);

					operator RecordAV(void);

		WeakRecordAV	&operator=(const WeakRecordAV &other);

		bool		operator==(const WeakRecordAV &other) const;
		bool		operator!=(const WeakRecordAV &other) const;

					/** @brief Return true if the record points to the
						original valid state block

						A record without the ":SN" field can never
						be confirmed as valid.
						You may be able access it if you are sure it
						still exists by other means. */
		bool		isValid(void) const;
		FE_UWORD		idr(void) const;
					/** Return the Layout. */
		const sp<LayoutAV>	&layout(void) const;


					/** @brief throw if the record doesn't have
						a serial number */
		void		demandSerialNumber(void) const;

		template <class T>
		T			&accessAttribute(FE_UWORD aLocator) const;

		void		*rawAttribute(FE_UWORD aLocator) const;

	private:
					/** Return a raw pointer to the Layout.
						Mainly intended for Accessor for speed. */
		LayoutAV	*rawLayout(void) const;
		void		set(FE_UWORD aArrayIndex, sp<LayoutAV> &aLayout);

	private:

		void		cacheSerialNumber(void);
		I32			readSerialNumber(void) const;

		sp<LayoutAV>	m_hpLayout;
		FE_UWORD		m_arrayIndex;
		I32			m_serialNumber;
		U32			m_serialLocator;
};

class WeakRecordAVInfo : public BaseType::Info
{
	public:
virtual	String	print(void *instance);
virtual	IWORD	output(std::ostream &ostrm, void *instance, t_serialMode mode);
virtual	void	input(std::istream &istrm, void *instance, t_serialMode mode);
virtual	IWORD	iosize(void);
virtual	bool	getConstruct(void);
virtual	void	construct(void *instance);
virtual	void	destruct(void *instance);
};

inline WeakRecordAV::WeakRecordAV(I32):
	m_arrayIndex(RecordAV::ms_invalidIndex),
	m_serialNumber(-1),
	m_serialLocator(0)
{
}

inline WeakRecordAV::WeakRecordAV(const RecordAV& other)
{
	if(other.isValid())
	{
		m_hpLayout = other.layout();
		m_arrayIndex = other.m_arrayIndex;

		cacheSerialNumber();
	}
	else
	{
		m_arrayIndex = RecordAV::ms_invalidIndex;
		m_serialNumber= -1;
	}
}

inline WeakRecordAV::WeakRecordAV(const WeakRecordAV &other)
{
	if(other.isValid())
	{
		m_hpLayout = other.m_hpLayout;
		m_arrayIndex = other.m_arrayIndex;
		m_serialLocator = other.m_serialLocator;
		m_serialNumber = other.m_serialNumber;
	}
	else
	{
		m_arrayIndex = RecordAV::ms_invalidIndex;
		m_serialNumber= -1;
	}
}

inline WeakRecordAV::~WeakRecordAV(void)
{
}

inline WeakRecordAV::operator RecordAV(void)
{
	RecordAV record;
	if(isValid())
	{
		record.set(m_arrayIndex, m_hpLayout);
	}
	return record;
}

inline I32 WeakRecordAV::readSerialNumber(void) const
{
#ifdef FE_AV_FASTITER_ENABLE
	if(!m_hpLayout->existenceCheck(m_serialLocator, m_arrayIndex))
	{
		feLog("WeakRecordAV::readSerialNumber failed %d %d\n",
				m_serialLocator, m_arrayIndex);
		return -1;
	}
#endif
	return accessAttribute<int>(m_serialLocator);
}

inline LayoutAV *WeakRecordAV::rawLayout(void) const
{
	return m_hpLayout.raw();
}

inline WeakRecordAV &WeakRecordAV::operator=(const WeakRecordAV &other)
{
	if(this != &other)
	{
		m_hpLayout = other.m_hpLayout;
		m_arrayIndex = other.m_arrayIndex;
		m_serialLocator = other.m_serialLocator;
		m_serialNumber = other.m_serialNumber;
	}
	return *this;
}

inline bool WeakRecordAV::operator==(const WeakRecordAV &other) const
{
	if (m_hpLayout != other.m_hpLayout) { return false; }
	return (m_arrayIndex == other.m_arrayIndex);
}

inline bool WeakRecordAV::operator!=(const WeakRecordAV &other) const
{
	if (m_hpLayout != other.m_hpLayout) { return true; }
	return (m_arrayIndex != other.m_arrayIndex);
}

inline bool WeakRecordAV::isValid(void) const
{
	if(m_arrayIndex != RecordAV::ms_invalidIndex)
	{
		if(m_serialNumber<0 || m_serialNumber==readSerialNumber())
		{
			return TRUE;
		}

		const_cast<WeakRecordAV*>(this)->m_arrayIndex=RecordAV::ms_invalidIndex;
		const_cast<WeakRecordAV*>(this)->m_serialNumber= -1;
	}

	return FALSE;
}

inline void WeakRecordAV::set(FE_UWORD aArrayIndex, sp<LayoutAV> &aLayout)
{
	m_arrayIndex = aArrayIndex;
	m_hpLayout = aLayout;

	// ???? Why were we making this record?
#ifdef FE_MAKE_RECORD_FOR_WEAKRECORD
	RecordAV record;
	record.set(aArrayIndex, aLayout);
#endif
	cacheSerialNumber();

	//* reset if there is no serial number
#if TRUE
	if(isValid() && m_serialNumber<0)
	{
		feLog("WeakRecordAV::set layout \"%s\" SN %d"
				" doesn't support \":SN\" attribute -> refusing assignment\n",
				m_hpLayout->name().c_str(),m_serialNumber);
		m_hpLayout=NULL;
		m_arrayIndex=RecordAV::ms_invalidIndex;
		FEASSERT(0);
	}
#endif
}

template <class T>
inline T &WeakRecordAV::accessAttribute(FE_UWORD aLocator) const
{
#if 0
	const FE_UWORD &avIndex = layout()->locatorTable()[aLocator];
	// TODO: do this without the dynamic cast if it is too slow with (hard cast like the void * below) -- RecordAV too
	TypeVector<T> &typeVector =	dynamic_cast< TypeVector<T>& >(*(layout()->attributeVector()[avIndex]));
	return typeVector.at(m_arrayIndex);
#endif
	return *(T *)(m_hpLayout->voidAccess(aLocator, m_arrayIndex));
}

inline void *WeakRecordAV::rawAttribute(FE_UWORD aLocator) const
{
#if 0
	const FE_UWORD &avIndex = layout()->locatorTable()[aLocator];
	sp<BaseTypeVector> &baseVector = layout()->attributeVector()[avIndex];
	return baseVector->raw_at(m_arrayIndex);
#endif
	return (m_hpLayout->voidAccess(aLocator, m_arrayIndex));
}

inline FE_UWORD WeakRecordAV::idr(void) const
{
	return m_hpLayout->idr(m_arrayIndex);
}

inline const sp<LayoutAV> &WeakRecordAV::layout(void) const
{
	return m_hpLayout;
}


inline void WeakRecordAV::cacheSerialNumber(void)
{
	U32 index = m_hpLayout->serialIndex();
	m_serialLocator = index;
	m_serialNumber = readSerialNumber();

#if FE_CODEGEN <= FE_DEBUG
	demandSerialNumber();
#endif
}

} /* namespace */

#endif /* __data_WeakRecordAV_h__ */

