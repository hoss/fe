/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __data_DynamicBitset_h__
#define __data_DynamicBitset_h__

//#define FE_BOOST_BITSET
#ifdef FE_BOOST_BITSET
#include "boost/dynamic_bitset.hpp"
typedef boost::dynamic_bitset<> t_bitset;
#else
// this is minmally implemented to replace boost::dynamic_bitset<> in FE's use
// do not use it in new code.
class VeryCrudeDynamicBitset
{
	public:

				VeryCrudeDynamicBitset(void)								{}

				VeryCrudeDynamicBitset(size_t a_size)
				{
					m_vec.resize(a_size);
					for(unsigned int i = 0 ; i < m_vec.size(); i++)
					{
						m_vec[i] = 0;
					}
				}

		bool	is_subset_of(const VeryCrudeDynamicBitset &a_bitset) const
				{
					unsigned int other_size_minus_one =
							(unsigned int)(a_bitset.m_vec.size()-1);
					for(unsigned int i = 0 ; i < m_vec.size(); i++)
					{
						if(m_vec[i])
						{
							if(i > other_size_minus_one) { return false; }
							if(!a_bitset.m_vec[i]) { return false; }
						}
					}
					return true;
				}

		bool	operator==(const VeryCrudeDynamicBitset &a_bitset) const
				{
					if(m_vec.size() != a_bitset.m_vec.size())
					{
						return false;
					}
					for(unsigned int i = 0 ; i < m_vec.size(); i++)
					{
						if(m_vec != a_bitset.m_vec)
						{
							return false;
						}
					}
					return true;
				}
		bool	operator<(const VeryCrudeDynamicBitset &a_bitset) const
				{
					if(m_vec.size() < a_bitset.m_vec.size())
					{
						return true;
					}
					if(m_vec.size() > a_bitset.m_vec.size())
					{
						return false;
					}
					for(unsigned int i = 0 ; i < m_vec.size(); i++)
					{
						if(m_vec > a_bitset.m_vec)
						{
							return false;
						}
						if(m_vec < a_bitset.m_vec)
						{
							return true;
						}
					}
					return false;
				}
		void	resize(unsigned int a_size)
				{
					m_vec.resize(a_size);
				}

		void	set(unsigned int a_index)
				{
					m_vec[a_index] = U8(true);
				}

		bool	operator [] (unsigned int a_index) const
				{
					return bool(m_vec[a_index]) == true;
				}

		VeryCrudeDynamicBitset
				operator & (const VeryCrudeDynamicBitset &a_other) const
				{
					VeryCrudeDynamicBitset result(m_vec.size());
					for(unsigned int i = 0; i < m_vec.size(); i++)
					{
						if(a_other.m_vec[i] && m_vec[i])
						{
							result.m_vec[i] = true;
						}
						else
						{
							result.m_vec[i] = false;
						}
					}
					return result;
				}

		VeryCrudeDynamicBitset
				operator | (const VeryCrudeDynamicBitset &a_other) const
				{
					VeryCrudeDynamicBitset result(m_vec.size());
					for(unsigned int i = 0; i < m_vec.size(); i++)
					{
						if(a_other.m_vec[i] || m_vec[i])
						{
							result.m_vec[i] = true;
						}
						else
						{
							result.m_vec[i] = false;
						}
					}
					return result;
				}

		unsigned int size(void) const { return m_vec.size(); }

	private:

		std::vector<U8>	m_vec;

};
typedef VeryCrudeDynamicBitset t_bitset;
#endif

inline bool partial_is_subset(const t_bitset &a_sub, const t_bitset &a_set)
{
	if(a_sub.size() != a_set.size())
	{
		t_bitset a_fit = a_sub;
		a_fit.resize(a_set.size());
		return a_fit.is_subset_of(a_set);
	}
	return a_sub.is_subset_of(a_set);
}

#endif /* __data_DynamicBitset_h__ */
