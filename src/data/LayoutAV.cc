/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <data/data.pmh>

//#define VALIDATE_LAYOUTAV
namespace fe
{

LayoutAV::LayoutAV(void)
{
	feX("fe::LayoutAV::LayoutAV","do not use default LayoutAV constructor");
}

LayoutAV::LayoutAV(sp<Scope> &scope)
{
	m_name.sPrintf("__%d__", scope->getNextUniqueLayoutID());
	constructor(scope);
}

LayoutAV::LayoutAV(sp<Scope> &scope, const String &name)
{
	this->setName(name);
	constructor(scope);
}

LayoutAV::LayoutAV(Scope *pScope, const String &name)
{
	this->setName(name);
	sp<Scope> spScope(pScope);
	constructor(spScope);
}

LayoutAV::LayoutAV(const LayoutAV &other):
	Castable(),
	Initialized(),
	Layout(),
	Initialize<LayoutAV>(),
	ClassSafe<GlobalHolder>()
{
	copy(other);
}

void LayoutAV::constructor(sp<Scope> &scope)
{
	FEASSERT(scope.isValid());
	m_in_use = false;
	m_need_defaults = false;
	m_hpScope = scope;
	m_avSize = 0;
	m_allocSize = 0;
	m_construct = false;
	m_serialIndex = m_hpScope->serialIndex();

	resizeLocatorTable(m_hpScope->getAttributeCount());
	addMissingAttributes();

#if FE_COUNTED_TRACK
	Counted::registerRegion(this,sizeof(LayoutAV));
#endif

}

LayoutAV::~LayoutAV(void)
{
}

void LayoutAV::initialize(void)
{
	SAFEGUARDCLASS;
	if(!m_hpScope.isValid())
	{
		feX("fe::LayoutAV::initialize",
			"cannot initialize record layout without scope");
	}
}

LayoutAV &LayoutAV::copy(const LayoutAV &other)
{
	SAFEGUARDCLASS;
	if(&other == this) { return *this; }

	m_name = other.name();

	feX("fe::LayoutAV::copy", "need to implement");

	return *this;
}

void LayoutAV::populate(const String &attribute_name)
{
	//* NOTE scope is guarded
//	SAFEGUARDCLASS;

	m_hpScope->populate(name(), attribute_name);
}

void LayoutAV::populate(const String &attribute_name,
	const String &attribute_type)
{
	//* NOTE scope is guarded
//	SAFEGUARDCLASS;

	m_hpScope->support(attribute_name, attribute_type);
	m_hpScope->populate(name(), attribute_name);
}

void LayoutAV::populate(const BaseAccessor &accessor)
{
	//* NOTE scope is guarded
//	SAFEGUARDCLASS;

	sp<Attribute> spAttribute = m_hpScope->attribute((FE_UWORD)accessor);
	m_hpScope->populate(name(), spAttribute->name());
}

void LayoutAV::populate(sp<Attribute> spAttribute)
{
	//* NOTE scope is guarded
//	SAFEGUARDCLASS;

	m_hpScope->populate(name(), spAttribute->name());
}

void LayoutAV::populate(sp<LayoutAV> spLayout)
{
	//* NOTE scope is guarded
//	SAFEGUARDCLASS;

	m_hpScope->clonePopulate(spLayout->name(), name());
}

void LayoutAV::peek(Peeker &peeker)
{
//	unsigned int n = m_locatorTable.size();
	for(FE_UWORD i = 0; i < scope()->getAttributeCount(); i++)
	{
		if(m_locatorTable[i] == locatorNone)
		{
			peeker.nl();
			peeker.cat("NONE");
			peeker.str().cat("\t");
			peeker.cat(m_hpScope->attribute(i)->name());
		}
		else
		{
			peeker.nl();
			peeker.cat("HAS");
			peeker.str().cat("\t");
			peeker.cat(m_hpScope->attribute(i)->name());
		}
	}
}

void LayoutAV::resizeLocatorTable(FE_UWORD aSize)
{
	unsigned int oldSize = m_locatorTable.size();
	m_locatorTable.resize(aSize);
	for(unsigned int i = oldSize; i < aSize; i++)
	{
		m_locatorTable[i] = locatorNone;
	}

	t_bitset bitset(scope()->attributes().size());
	for(unsigned int index = 0;index < scope()->attributes().size(); index++)
	{
		if(checkAttribute(index))
		{
			bitset.set(index);
		}
	}
	setBitset(bitset);
}

void LayoutAV::addMissingAttributes(void)
{
	// insanely expensive in debug build;even too slow for normal dev iterating:
#ifdef VALIDATE_LAYOUTAV
	m_hpScope->layoutValidate();
#endif
	FE_UWORD cnt = m_hpScope->getAttributeCount();

	sp<TypeMaster> spTypeMaster(m_hpScope->typeMaster());
	hp<LayoutAV> spLayout(this);
	Array<String> attributes;

	// cut&paste block from LayoutSB::lock
	sp<Depend> spDepend;
	String attributeName;
	t_depends::iterator it_depend;
	Array<String>::iterator it_attribute;
	Array< std::list<FE_UWORD> > shareTable;
	shareTable.resize(cnt);
	//std::map< FE_UWORD, std::pair<size_t,String> > withinMap;
	//std::map< FE_UWORD, std::list<FE_UWORD> > withinReverseMap;

	// add attributes based on Depend::e_populate dependencies
	// and build share table
	for(it_depend = m_hpScope->depends().begin();
		it_depend != m_hpScope->depends().end(); it_depend++)
	{
		if(((*it_depend)->dependName() == "")
					||	((*it_depend)->dependName() == spLayout->name()) )
		{
			if((*it_depend)->dependFlag() & Depend::e_populate)
			{
				attributes.push_back((*it_depend)->attributeName());
			}
			if((*it_depend)->dependFlag() & Depend::e_share)
			{
				FE_UWORD idx_1, idx_2;
				sp<Attribute> spT1, spT2;
				spT1 = m_hpScope->findAttribute((*it_depend)->shareName(),
						idx_1);
				spT2 = m_hpScope->findAttribute((*it_depend)->attributeName(),
						idx_2);
				if(spT1.isValid() && spT2.isValid())
				{
					if(spT1->type() != spT2->type())
					{
						std::list<String> names;
						String s1,s2;
						spTypeMaster->reverseLookup(spT1->type(), names);
						s1.cat(names," ");
						names.clear();
						spTypeMaster->reverseLookup(spT2->type(), names);
						s2.cat(names, " ");
						feX("fe::Scope::addMissingAttributes",
							"share type mismatch %s[%s] != %s[%s]",
							spT1->name().c_str(),s1.c_str(),
							spT2->name().c_str(),s2.c_str() );
					}
					shareTable[idx_1].push_back(idx_2);
					shareTable[idx_2].push_back(idx_1);
				}
			}
		}
	}

	// add attributes based on Depend::e_attribute dependencies
	t_depends depend_list;
	for(it_depend = m_hpScope->depends().begin();
		it_depend != m_hpScope->depends().end(); it_depend++)
	{
		if((*it_depend)->dependFlag() & Depend::e_attribute)
		{
			depend_list.push_back(*it_depend);
			std::sort((*it_depend)->matchAttributes().begin(),
				(*it_depend)->matchAttributes().end());
		}
	}
	bool change = true;
	while(change)
	{
		change = false;
		std::sort(attributes.begin(), attributes.end());
		for(it_depend = depend_list.begin();
				it_depend != depend_list.end();/* it_depend++ below */)
		{
			Array<String> &matchattrs = (*it_depend)->matchAttributes();

			if(std::includes<Array<String>::iterator,
				Array<String>::iterator>(
					attributes.begin(), attributes.end(),
					matchattrs.begin(), matchattrs.end()))
			{
				change = true;
				attributes.push_back((*it_depend)->attributeName());
				std::sort(attributes.begin(), attributes.end());
				sp<Depend> spDep = *it_depend;
				it_depend++;
				depend_list.remove(spDep);
			}
			else
			{
				it_depend++;
			}
		}
	}
	// end of cut&paste block to build up attributes


	sp<Attribute> spAttribute;
	FE_UWORD index;


	for(it_attribute = attributes.begin();it_attribute != attributes.end();
			it_attribute++)
	{
		spAttribute = m_hpScope->findAttribute(*it_attribute, index);
		if(!spAttribute.isValid())
		{
#if 0
			feX("fe::Scope::addMissingAttributes",
				"Layout \"%s\" not initialized because "
				"Attribute \"%s\" was not found",
				spLayout->name().c_str(), it_attribute->c_str());
#endif
			// I think we can just skip until attribute is added
			continue;
		}

		if(m_locatorTable[index] == locatorNone)
		{
			// check for share
			for(std::list<FE_UWORD>::iterator it_share
					= shareTable[index].begin();
				it_share != shareTable[index].end();
				it_share++)
			{
				FE_UWORD idx = *it_share;
				if(m_locatorTable[idx] != locatorNone)
				{
					feLog("LayoutAV share %d %d\n",
							index, idx);
					m_locatorTable[index] = m_locatorTable[idx];
					break;
				}
			}

			// if not shared, add
			if(m_locatorTable[index] == locatorNone)
			{
#ifdef FE_AV_FASTITER_ENABLE
				if(m_in_use)
				{
					feX("LayoutAV::addMissingAttributes",
						"Layout \"%s\" "
						"Attribute \"%s\" : FE_AV_FASTITER_ENABLE does not support adding attributes after create",
						spLayout->name().c_str(), it_attribute->c_str());
				}
#endif


				if(!spAttribute->type().isValid())
				{
					feX("fe::Scope::addMissingAttributes",
						"Layout \"%s\" not initialized because "
						"Attribute \"%s\" had no valid type",
						spLayout->name().c_str(), it_attribute->c_str());
				}
				// TODO: AJW: alignment?

				unsigned int new_locator = m_attributeVector.size();
				sp<BaseTypeVector> spBaseTypeVector =
						spAttribute->type()->createVector();
				spBaseTypeVector->resize(m_allocSize);
				m_attributeVector.push_back(spBaseTypeVector);
				m_locatorTable[index] = new_locator;

				if(spAttribute->defaultInstance().data())
				{
					m_need_defaults = true;
				}

#if FE_COUNTED_TRACK
				spBaseTypeVector->setName(spAttribute->name());
				spBaseTypeVector->trackReference(fe_cast<Counted>(this),
						"LayoutAV::addMissingAttributes \""+
						spAttribute->name()+"\"");
#endif

				// except if reference count is added after records created
				if(spAttribute->name() == ":CT")
				{
					if(m_avSize > 0)
					{
						feX("LayoutAV", "attempt to add reference counting to"
								" Layout with existing Records");
					}

					m_rc = fe_cast< TypeVector< std::atomic<I32> > >(&*spBaseTypeVector);
				}

				// except if serial number is added after records created
				if(spAttribute->name() == ":SN")
				{
					if(m_avSize > 0)
					{
						feX("LayoutAV", "attempt to add serial number to"
								" Layout with existing Records");
					}

					m_sn = fe_cast< TypeVector< std::atomic<I32> > >(&*spBaseTypeVector);
				}
			}

			if(spAttribute->type()->getConstruct())
			{
				spLayout->setConstruct(true);
			}
		}
	}

	m_hpScope->resizeLayoutLocators();
}


void LayoutAV::notifyOfAttributeChange(sp<Depend> &depend)
{
	// check and add necessary attributes
	if(m_in_use)
	{
		addMissingAttributes();
	}
}

void LayoutAV::startUsing(void)
{
	addMissingAttributes();
	m_in_use = true;
}

bool LayoutAV::isExactlyBitset(const t_bitset a_bitset)
{
	if(!m_in_use) { startUsing(); }

	return a_bitset == bitset();
}

bool LayoutAV::containsBitset(const t_bitset a_bitset)
{
	if(!m_in_use) { startUsing(); }

	return a_bitset.is_subset_of(bitset());
}

RecordAV LayoutAV::createRecord(void)
{
	RecordAV record;
	createRecords(record,1);

	scope()->assignIDNumber<RecordAV>(record);

#if FE_SCOPE_SUPPORT_WATCHERS
	scope()->watch(record);
#endif

	return record;
}

void LayoutAV::createRecord(RecordAV &r_new)
{
	createRecords(r_new,1);
}

void LayoutAV::createRecords(RecordAV &r_new,U32 a_count)
{
	if(!a_count)
	{
		return;
	}

	SAFEGUARDCLASS_IF(m_hpScope->isLocking());

	if(!m_in_use) { startUsing(); }

#ifndef FE_AV_FASTITER_ENABLE

	if(a_count==1 && m_freeList.size() > 0)
	{
		std::set< FE_UWORD >::iterator i_free = m_freeList.begin();
		r_new.m_arrayIndex = *i_free;
		m_freeList.erase(i_free);
	}
	else
	{
		r_new.m_arrayIndex = m_avSize;
		m_avSize+=a_count;

		if(m_allocSize<m_avSize)
		{
			m_allocSize=1.2*m_avSize;	//* TODO tweak multiplier

			const unsigned int n = m_locatorTable.size();
			for(unsigned int i = 0; i < n; i++)
			{
				if(m_locatorTable[i] != locatorNone)
				{
					m_attributeVector[m_locatorTable[i]]->resize(m_allocSize);
				}
			}
			m_idr.resize(m_allocSize);
		}
	}

	r_new.m_spLayout = this;

	const FE_UWORD firstRecordArrayIndex=r_new.m_arrayIndex;

	for(U32 index=0;index<a_count;index++)
	{
		const FE_UWORD recordArrayIndex=firstRecordArrayIndex+index;

		m_idr[recordArrayIndex] = m_hpScope->getNextUniqueIDR();

		//* NOTE after first, index is raw-acquired without Record to destruct
		acquireArrayIndex(recordArrayIndex);
	}
#else
	const FE_UWORD newslot = m_avSize;
	m_avSize+=a_count;

	if(a_count==1 && m_freeList.size() > 0)
	{
		std::set< FE_UWORD >::iterator i_free = m_freeList.begin();
		r_new.m_arrayIndex = *i_free;
		m_freeList.erase(i_free);
	}
	else
	{
		r_new.m_arrayIndex = newslot;
		m_idr.resize(m_avSize);
		m_bounce.resize(m_avSize);
	}

	m_bounceback.resize(m_avSize);

	const unsigned int n = (unsigned int)(m_locatorTable.size());
	for(unsigned int i = 0; i < n; i++)
	{
		if(m_locatorTable[i] != locatorNone)
		{
			m_attributeVector[m_locatorTable[i]]->resize(m_avSize);
		}
	}

	r_new.m_spLayout = this;

	const FE_UWORD firstRecordArrayIndex=r_new.m_arrayIndex;

	for(U32 index=0;index<a_count;index++)
	{
		const FE_UWORD recordArrayIndex=firstRecordArrayIndex+index;

		m_bounce[recordArrayIndex] = newslot+index;
		m_idr[recordArrayIndex] = m_hpScope->getNextUniqueIDR();

		m_bounceback[newslot+index] = recordArrayIndex;

		//* NOTE after first, index is raw-acquired without Record to destruct
		acquireArrayIndex(recordArrayIndex);
	}
#endif

	if(m_need_defaults)
	{
		Record record=r_new;
		for(U32 index=0;index<a_count;index++)
		{
			record.m_arrayIndex=firstRecordArrayIndex+index;
			assignDefaultAttributes(record);
		}
		record.m_arrayIndex=firstRecordArrayIndex;
	}
}

void LayoutAV::freeArrayIndex(const FE_UWORD &aArrayIndex)
{
#ifndef FE_AV_FASTITER_ENABLE
	if(m_sn.isValid())
	{
		std::atomic<int> &sn = m_sn->at(aArrayIndex);
		sn = -1;
	}

	m_freeList.insert(aArrayIndex);
#else
	if(m_sn.isValid())
	{
		std::atomic<int> &sn = m_sn->at(m_bounce[aArrayIndex]);
		sn = -1;
	}

	// still need freelist for bounce and idr
	m_freeList.insert(aArrayIndex);

	// convert incoming aArrayIndex to trueIndex
	FE_UWORD trueIndex = m_bounce[aArrayIndex];
	m_bounce[aArrayIndex] = -1;
	m_idr[aArrayIndex] = -1;
	m_avSize -= 1;
	if(trueIndex == m_avSize)
	{
		// deleting the the last one
		unsigned int n = (unsigned int)(m_locatorTable.size());
		for(unsigned int i = 0; i < n; i++)
		{
			if(m_locatorTable[i] != locatorNone)
			{
				sp<BaseTypeVector> &spAV = m_attributeVector[m_locatorTable[i]];
				spAV->resize(m_avSize);
			}
		}
	}
	else
	{
		// deleting in the middle -- move last to hole
		m_bounce[m_bounceback[m_avSize]] = trueIndex;
		m_bounceback[trueIndex] = m_bounceback[m_avSize];
		unsigned int n = (unsigned int)(m_locatorTable.size());
		for(unsigned int i = 0; i < n; i++)
		{
			if(m_locatorTable[i] != locatorNone)
			{
				sp<BaseTypeVector> &spAV = m_attributeVector[m_locatorTable[i]];
				scope()->attribute(i)->type()->assign(spAV->raw_at(trueIndex), spAV->raw_at(m_avSize));
				spAV->resize(m_avSize);
			}
		}
	}
#endif
}


#if 0
void LayoutAV::freeRecord(RecordAV &r_old)
{
// TODO: AJW: fix and enable trimming mode (in freeArrayIndex too)
	scope()->freeIDNumber<RecordAV>(r_old);

#if 0
	// trim trailing freelist items approach (broken)
	if(r_old.m_arrayIndex == m_avSize-1)
	{
		m_avSize--;
		std::set< FE_UWORD >::reverse_iterator i_free = m_freeList.rbegin();
		while((i_free != m_freeList.rend()) && (*i_free == m_avSize-1))
		{
			m_avSize--;
			std::set< FE_UWORD >::reverse_iterator i_erase = i_free;
			i_free++;

std::set< FE_UWORD >::reverse_iterator i_e2 = i_erase;
i_e2--;
			m_freeList.erase(--i_erase.base());
		}

		unsigned int n = m_locatorTable.size();
		for(unsigned int i = 0; i < n; i++)
		{
			if(m_locatorTable[i] != locatorNone)
			{
				m_attributeVector[m_locatorTable[i]]->resize(m_avSize);
			}
		}
	}
	else
	{
		m_freeList.insert(r_old.m_arrayIndex);
	}
#else
	// high water mark approach
	m_freeList.insert(r_old.m_arrayIndex);
#endif
}
#endif

void LayoutAV::assignDefaultAttributes(RecordAV &r_new)
{
	SAFEGUARDCLASS_IF(m_hpScope->isLocking());

#if 0
	if(m_construct)
	{
		unsigned int n = m_locatorTable.size();
		for(unsigned int i = 0; i < n; i++)
		{
			if(m_locatorTable[i] != locatorNone)
			{
				scope()->attribute(i)->type()->construct(
					r_new.rawAttribute(m_locatorTable[i]));
			}
		}
	}
#endif

	unsigned int n = m_locatorTable.size();
	for(unsigned int i = 0; i < n; i++)
	{
		if(m_locatorTable[i] != locatorNone)
		{
			Instance &defaultInstance =
				scope()->attribute(i)->defaultInstance();
			if(defaultInstance.data())
			{
				Instance new_instance = Instance(
					r_new.rawAttribute(i),
					defaultInstance.type(), NULL);
				new_instance.assign(defaultInstance);
			}
		}
	}
}

void LayoutAV::destructAttributes(const FE_UWORD &a_arrayIndex)
{
	SAFEGUARDCLASS_IF(m_hpScope->isLocking());

	unsigned int n = m_locatorTable.size();
	for(unsigned int i = 0; i < n; i++)
	{
		if(m_locatorTable[i] != locatorNone)
		{
			Instance &deadInstance =
					scope()->attribute(i)->deadInstance();

			if(deadInstance.data())
			{
				//void *v = (void *)(m_attributeVector[m_locatorTable[i]] ->raw_at(a_arrayIndex));
				void *v = voidAccess(i, a_arrayIndex);

				Instance new_instance =
						Instance(v, deadInstance.type(), NULL);

				new_instance.assign(deadInstance);
			}
		}
	}
	// count needs to be at zero if it exists -- deadInstance above sets
	// it to noise
	if(m_rc.isValid())
	{
#ifndef FE_AV_FASTITER_ENABLE
		std::atomic<int> &rc = m_rc->at(a_arrayIndex);
#else
		std::atomic<int> &rc = m_rc->at(m_bounce[a_arrayIndex]);
#endif
		rc = 0;
	}
#if 0
	if(m_construct)
	{
		unsigned int n = m_locatorTable.size();
		for(unsigned int i = 0; i < n; i++)
		{
			if(m_locatorTable[i] != locatorNone)
			{
				void *v = (void *)(m_attributeVector[m_locatorTable[i]]
						->raw_at(a_arrayIndex));
				scope()->attribute(i)->type()->destruct(v);
			}
		}
	}
#endif
}

FE_UWORD LayoutAV::attributeCount(void) const
{
	FE_UWORD count=0;

	unsigned int n = m_locatorTable.size();
	for(unsigned int i = 0; i < n; i++)
	{
		if(m_locatorTable[i] != locatorNone)
		{
			count++;
		}
	}

	return count;
}

sp<Attribute> LayoutAV::attribute(FE_UWORD localIndex)
{
	FE_UWORD count=0;

	unsigned int n = m_locatorTable.size();
	for(unsigned int i = 0; i < n; i++)
	{
		if(m_locatorTable[i] != locatorNone)
		{
			if(count==localIndex)
			{
				return scope()->attribute(i);
			}

			count++;
		}
	}

	return sp<Attribute>(NULL);
}


bool LayoutAV::checkAttributeStr(const String &a_name)
{
	FE_UWORD index;
	if(!scope()->findAttribute(a_name,index).isValid())
	{
		return false;
	}
	return checkAttribute(index);
}

} /* namespace */

