/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <data/data.pmh>

#define FE_WR_DEMAND_SN		TRUE

namespace fe
{

void WeakRecordSB::demandSerialNumber(void) const
{
/*
	feLog("WeakRecord::demandSerialNumber %d %d %s\n",
			isValid(),m_serialNumber,
			isValid()? m_spLayout->name().c_str(): "<invalid>");
*/
#if FE_WR_DEMAND_SN
	if(isValid() && m_serialNumber<0)
	{
		feLog("WeakRecordSB::cacheSerialNumber layout \"%s\""
				" doesn't support \":SN\" attribute\n",
				m_spLayout->name().c_str());
		feX("WeakRecordSB::demandSerialNumber","no SN");
	}
#endif
}

String WeakRecordSBInfo::print(void *instance)
{
	if(!instance)
		return String("<error>");

	const WeakRecordSB* pR = (WeakRecordSB*)instance;
	if(!pR->isValid())
		return String("<invalid>");

	String string;
	string.sPrintf("%u %s",pR->idr(),
			pR->layout().isValid()?
			pR->layout()->name().c_str(): "<invalid layout>");
	return string;
}

IWORD WeakRecordSBInfo::output(std::ostream &ostrm, void *instance,
		t_serialMode mode)
{
	feX(e_unsupported,
		"WeakRecordSBInfo::output",
		"record output should be done via fe::Stream");
	return 0;
}

void WeakRecordSBInfo::input(std::istream &istrm, void *instance,
		t_serialMode mode)
{
	feX(e_unsupported,
		"WeakRecordInfo::input",
		"record input should be done via fe::Stream");
}

IWORD WeakRecordSBInfo::iosize(void)
{
	return c_implicit;
}

bool WeakRecordSBInfo::getConstruct(void)
{
	return true;
}

void WeakRecordSBInfo::construct(void *instance)
{
	new(instance)WeakRecordSB;
}

void WeakRecordSBInfo::destruct(void *instance)
{
	((WeakRecordSB *)instance)->~WeakRecordSB();
}


} /* namespace */

