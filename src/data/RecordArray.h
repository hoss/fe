/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __data_RecordArray_h__
#define __data_RecordArray_h__


namespace fe
{

#if FE_DATA_STORE==FE_SB
typedef RecordArraySB RecordArray;
typedef PtrRecordArraySBInfo PtrRecordArrayInfo;
#endif

#if FE_DATA_STORE==FE_AV
typedef RecordArrayAV RecordArray;
typedef PtrRecordArrayAVInfo PtrRecordArrayInfo;
#endif

};

#endif /* __data_RecordArray_h__ */

