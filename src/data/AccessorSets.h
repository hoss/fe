/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __data_AccessorSets_h__
#define __data_AccessorSets_h__

namespace fe
{


/**	System reserved attributes.  Note that this collection is not intended
	to be necessarily populated as a full set. */
class AsSystem
	: public AccessorSet, public Initialize<AsSystem>
{
	public:
		AsSystem(void){}
		void initialize(void)
		{
			add(count,	FE_SPEC(":CT", "fe system reference count"));
			add(id,		FE_SPEC(":ID", "fe system id"));
			add(sn,		FE_SPEC(":SN", "fe serial number"));
			add(ttl,	FE_SPEC(":TTL", "fe system time to live"));
		}
		/// record reference count
		Accessor< std::atomic<int> >	count;
		/// record identifier
		Accessor<int>	id;
		/// record serial number
		Accessor< std::atomic<int> >	sn;
		/// signal time to live in networking
		Accessor<int>	ttl;
};

/**	Generic reusable attributes. Useful for internal unshared unpublic
	attributes. */
class AsGeneric :
	public AccessorSet,
	public Initialize<AsGeneric>
{
	public:
		void initialize(void)
		{
			add(r0,				FE_USE("generic:r_0"));
			add(r1,				FE_USE("generic:r_1"));

			add(s0,				FE_USE("generic:s_0"));

			add(i0,				FE_USE("generic:i_0"));
			add(i1,				FE_USE("generic:i_1"));

			add(g0,				FE_USE("generic:g_0"));
		}
		Accessor<Record>			r0;
		Accessor<Record>			r1;

		Accessor<String>			s0;

		Accessor<int>				i0;
		Accessor<int>				i1;

		Accessor< sp<RecordGroup> >	g0;
};

/// Sequencer signal
class AsSequenceSignal
	: public AccessorSet, public Initialize<AsSequenceSignal>
{
	public:
		AsSequenceSignal(void){}
		void initialize(void)
		{
			add(time,		FE_USE("seq:time"));
			add(interval,	FE_USE("seq:interval"));
			add(count,		FE_USE("seq:count"));
			add(periodic,	FE_USE("seq:periodic"));
		}
		///	time setting for sequencer signals
		Accessor<int>		time;
		/// interval setting for sequencer signals
		Accessor<int>		interval;
		/// count setting for sequencer signals
		Accessor<int>		count;
		/// mode setting for sequencer signals
		Accessor<int>		periodic;
};

/// just a Component
class AsComponent
	: public AccessorSet, public Initialize<AsComponent>
{
	public:
		AsComponent(void){}
		void initialize(void)
		{
			add(component,	FE_USE("component"));
		}
		/// component
		Accessor< sp<Component> >	component;
};


} /* namespace */

#endif /* __data_AccessorSets_h__ */

