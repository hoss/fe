/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __data_LayoutAV_h__
#define __data_LayoutAV_h__


namespace fe
{

class Scope;
class RecordAV;
class BaseAccessor;


/** @brief Record "type" definition

	@ingroup data

	A Layout is a definition of Attribute layout in Record instances.

	Layout objects can only be created via a Scope.  For typical use
	all operations on Layout objects are also done through Scope methods.

	@todo full data system guide
	*/
class FE_DL_EXPORT LayoutAV: public Layout, public Initialize<LayoutAV>,
		public ClassSafe<GlobalHolder>
{
friend	class RecordAV;
friend	class WeakRecordAV;
friend	class RecordArrayAV;

	public:
		LayoutAV(sp<Scope> &scope);
		LayoutAV(sp<Scope> &scope, const String &name);
		LayoutAV(Scope *pScope, const String &name);
	private:
		LayoutAV(const LayoutAV &other);
		LayoutAV &operator=(const LayoutAV &other);
	public:
virtual	~LayoutAV(void);

		LayoutAV		&copy(const LayoutAV &other);
virtual	void			initialize(void);

		/** Convienience function to Scope::populate */
virtual	void			populate(const String &attribute_name);
		/** Convienience function to Scope::populate */
virtual	void			populate(const String &attribute_name,
								const String &attribute_type);
		/** Convienience function to Scope::populate.  This particular one
			does a check if the layout is locked and if so verifys the
			exsitence of the attribute. */
virtual	void			populate(const BaseAccessor &accessor);
		/** Convienience function to Scope::populate */
virtual	void			populate(sp<Attribute> spAttribute);
		/** Convienience function to Scope::populate */
		void			populate(sp<LayoutAV> spLayout);

virtual	void			setName(const String &name);
virtual	const String&	name(void) const;
virtual	const String	verboseName(void) const;

virtual	void			setConstruct(bool construct);
virtual	const bool		&construct(void) const;

virtual	hp<Scope>&		scope(void);


virtual	void			peek(Peeker &peeker);

virtual	FE_UWORD		attributeCount(void) const;
virtual	sp<Attribute>	attribute(FE_UWORD localIndex);

virtual	bool			checkAttribute(FE_UWORD a_locator) const;
virtual	bool			checkAttributeStr(const String &a_name);
virtual	void			resizeLocatorTable(FE_UWORD a_size);

		RecordAV		createRecord(void);
		void			createRecord(RecordAV &r_new);

		//void			freeRecord(RecordAV &r_old);
		void			freeArrayIndex(const FE_UWORD &aArrayIndex);

virtual	void			notifyOfAttributeChange(sp<Depend> &depend);

		FE_UWORD		avSize(void) { return m_avSize; }
		void			*baseOfAV(FE_UWORD a_locatorIndex);

	private:
						LayoutAV(void);
		void			constructor(sp<Scope> &scope);

		void			addMissingAttributes(void);
		void			assignDefaultAttributes(RecordAV &r_new);
		void			destructAttributes(const FE_UWORD &a_arrayIndex);

		Array< sp<BaseTypeVector> >	&attributeVector(void);
		Array< FE_UWORD >				&locatorTable(void);

const	FE_UWORD		&idr(const FE_UWORD &a_index) const;

		void			acquireArrayIndex(const FE_UWORD &aArrayIndex);
		void			releaseArrayIndex(const FE_UWORD &aArrayIndex);

		unsigned int	serialIndex(void) const { return m_serialIndex; }

		void			startUsing(void);

		bool			isExactlyBitset(const t_bitset a_bitset) override;
		bool			containsBitset(const t_bitset a_bitset) override;

	private:
		void			createRecords(RecordAV &r_new,U32 a_count);

		String								m_name;
		hp<Scope>							m_hpScope;
		Array< sp<BaseTypeVector> >			m_attributeVector;
		Array< FE_UWORD >					m_locatorTable;
		Array< FE_UWORD >					m_idr;
#ifdef FE_AV_FASTITER_ENABLE
		std::vector< FE_UWORD >				m_bounce;
		std::vector< FE_UWORD >				m_bounceback;
#endif
		std::set< FE_UWORD >				m_freeList;
		FE_UWORD							m_avSize;
		FE_UWORD							m_allocSize;
		sp< TypeVector< std::atomic<int> > >	m_rc;
		sp< TypeVector< std::atomic<int> > >	m_sn;
		bool								m_construct;
		unsigned int						m_serialIndex;
		bool								m_in_use;
		bool								m_need_defaults;

	private:
		void*			voidAccess(const FE_UWORD &a_locatorIndex,
							const FE_UWORD &a_arrayIndex) const;

#ifdef FE_AV_FASTITER_ENABLE
		bool			existenceCheck(const FE_UWORD &a_locatorIndex,
							const FE_UWORD &a_arrayIndex) const;
#endif

	public:
static
const	FE_DL_PUBLIC FE_UWORD					locatorNone = (FE_UWORD)(-1);
static
const	FE_DL_PUBLIC FE_UWORD					arrayindexNone = (FE_UWORD)(-1);
};

#if FE_DATA_STORE==FE_AV
typedef LayoutAV LayoutDefault;
#endif

#ifdef FE_AV_FASTITER_ENABLE
inline void *LayoutAV::voidAccess(const FE_UWORD &a_locatorIndex,
	const FE_UWORD &a_arrayIndex) const
{
	FEASSERT(m_locatorTable[a_locatorIndex]!=locatorNone);
	FEASSERT(a_arrayIndex!=arrayindexNone);
	FEASSERT(m_bounce[a_arrayIndex]>=0);
	FEASSERT(m_locatorTable[a_locatorIndex]<m_attributeVector.size());
	return (void *)(m_attributeVector[m_locatorTable[a_locatorIndex]]->raw_at(
			m_bounce[a_arrayIndex]));
}

inline bool LayoutAV::existenceCheck(const FE_UWORD &a_locatorIndex,
	const FE_UWORD &a_arrayIndex) const
{
	if(I32(a_locatorIndex) < 0) { return FALSE; }
	if(a_locatorIndex >= m_locatorTable.size()) { return FALSE; }
	if(I32(a_arrayIndex) < 0) { return FALSE; }
	if(a_arrayIndex >= m_bounce.size()) { return FALSE; }
	if(I32(m_bounce[a_arrayIndex]) < 0) { return FALSE; }

	//* size() is type size, not array size
//	if(m_bounce[a_arrayIndex] >=
//			m_attributeVector[m_locatorTable[a_locatorIndex]]->size())
//	{	return FALSE; }

	return true;
}
#else
inline void *LayoutAV::voidAccess(const FE_UWORD &a_locatorIndex,
	const FE_UWORD &a_arrayIndex) const
{
	FEASSERT(m_locatorTable[a_locatorIndex]!=locatorNone);
	FEASSERT(a_arrayIndex!=arrayindexNone);
	return (void *)(m_attributeVector[m_locatorTable[a_locatorIndex]]->raw_at(
			a_arrayIndex));
}
#endif

inline void *LayoutAV::baseOfAV(FE_UWORD a_locatorIndex)
{
	return m_attributeVector[m_locatorTable[a_locatorIndex]]->raw_at(0);
}

inline bool LayoutAV::checkAttribute(FE_UWORD aLocator) const
{
	return (m_locatorTable[aLocator] != locatorNone);
}

inline const FE_UWORD &LayoutAV::idr(const FE_UWORD &aIndex) const
{
	return m_idr[aIndex];
}

inline Array< sp<BaseTypeVector> > &LayoutAV::attributeVector(void)
{
	return m_attributeVector;
}

inline Array< FE_UWORD > &LayoutAV::locatorTable(void)
{
	return m_locatorTable;
}

//* WARNING cross-guarding with Scope can deadlock

inline void LayoutAV::setName(const String &name)
{
//	SAFEGUARDCLASS;
	m_name = name;
}
inline const String& LayoutAV::name(void) const
{
//	SAFEGUARDCLASS;
	return m_name;
}
inline const String LayoutAV::verboseName(void) const
{
	return "Layout " + name();
}

inline void LayoutAV::setConstruct(bool construct)
{
//	SAFEGUARDCLASS;
	m_construct = construct;
}
inline const bool &LayoutAV::construct(void) const
{
//	SAFEGUARDCLASS;
	return m_construct;
}

inline hp<Scope>& LayoutAV::scope(void)
{
//	SAFEGUARDCLASS;
	return m_hpScope;
}

inline void LayoutAV::acquireArrayIndex(const FE_UWORD &aArrayIndex)
{
	if(m_rc.isValid())
	{
#ifndef FE_AV_FASTITER_ENABLE
		m_rc->at(aArrayIndex)++;
#else
		m_rc->at(m_bounce[aArrayIndex])++;
#endif
	}
	else
	{
		feX("RC is on by default, should not get here");
	}
}

inline void LayoutAV::releaseArrayIndex(const FE_UWORD &aArrayIndex)
{
//~	SAFEGUARDCLASS;

	if(m_rc.isValid())
	{
#ifndef FE_AV_FASTITER_ENABLE
		std::atomic<int> &rc = m_rc->at(aArrayIndex);
#else
		std::atomic<int> &rc = m_rc->at(m_bounce[aArrayIndex]);
#endif
		rc--;
		if(rc == 0)
		{
			destructAttributes(aArrayIndex);
			freeArrayIndex(aArrayIndex);
		}
	}
}

} /* namespace */

#endif /* __data_LayoutAV_h__ */

