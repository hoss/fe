/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <data/data.pmh>

namespace fe
{

VoidAccessor::VoidAccessor(void)
{
	m_sep = ".";
}

VoidAccessor::VoidAccessor(const String &a_path)
{
	m_sep = ".";
	setup(a_path);
}

VoidAccessor::~VoidAccessor(void)
{
}


VoidAccessor &VoidAccessor::operator=(const String &a_path)
{
	setup(a_path);
	return *this;
}



void VoidAccessor::setup(const String &a_path)
{
	m_path.clear();

#ifdef FE_BOOST_TOKENIZER
	boost::char_separator<char> sep(m_sep.c_str());
	typedef boost::tokenizer<boost::char_separator<char> > t_tokenizer;
	std::string std_str = a_path.c_str();
	t_tokenizer tokens(std_str, sep);
	for(t_tokenizer::iterator i_t = tokens.begin(); i_t != tokens.end(); ++i_t)
	{
		String s = (*i_t).c_str();
		m_path.push_back(s);
	}
#else
	String buffer=a_path.c_str();
	String token;
	while(!(token=buffer.parse("",m_sep.c_str())).empty())
	{
		m_path.push_back(token);
	}
#endif
	m_last = m_path.back();
	m_path.pop_back();

}

String VoidAccessor::path(void)
{
	String s;
	for(std::list<String>::iterator i_r = m_path.begin();
		i_r != m_path.end(); i_r++)
	{
		s.cat(i_r->c_str());
		s.cat(m_sep);
	}
	s.cat(m_last.c_str());
	return s;
}

void *VoidAccessor::access(const Record r_in, sp<Attribute> &a_rspAttr)
{
	FE_UWORD i;
	Record r_a = r_in;
	for(std::list<String>::iterator i_r = m_path.begin();
		i_r != m_path.end(); i_r++)
	{
		sp<Attribute> &attr = r_a.layout()->scope()->findAttribute(*i_r, i);
		if(!attr.isValid())
		{
			return NULL;
		}
		if(!r_a.layout()->checkAttribute(i))
		{
			return NULL;
		}
		r_a = r_a.accessAttribute<Record>(i);
		if(!r_a.isValid())
		{
			return NULL;
		}
	}
	a_rspAttr = r_a.layout()->scope()->findAttribute(m_last, i);
	if(!a_rspAttr.isValid())
	{
		return NULL;
	}
	if(!r_a.layout()->checkAttribute(i))
	{
		return NULL;
	}
	return r_a.accessAttribute<void *>(i);

}

} /* namespace */

