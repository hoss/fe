/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __data_RecordArrayAV_h__
#define __data_RecordArrayAV_h__

namespace fe
{

struct hash_av
{ FE_UWORD operator()(const FE_UWORD &u) const
	{ return u; } };

struct eq_av
{ bool operator()(FE_UWORD u1, FE_UWORD u2) const
	{ return u1 == u2; } };

typedef HashMap<FE_UWORD, IWORD, hash_av, eq_av>		t_av_map;

/**	@brief Homogeneous collection of Records

	@ingroup data

	An array of Records, or more precisely an array of state block pointers
	all pointing to state blocks of the same layout.  Therefore only a single
	reference to the layout itself is stored.  Therefore this record array
	object consumes less memory than an array of record objects.
	*/
class FE_DL_EXPORT RecordArrayAV : public Counted
{
	public:
		RecordArrayAV(void);
		RecordArrayAV(sp<LayoutAV> spLayout);
		RecordArrayAV(sp<LayoutAV> spLayout, FE_UWORD aCount);
		RecordArrayAV(const RecordArrayAV &other);
virtual	~RecordArrayAV(void);

		RecordArrayAV	&operator=(const RecordArrayAV &other);
		FE_UWORD		idr(IWORD index);
		/**	Return the length of the array. */
		IWORD			length(void);
		/** Return the Layout. */
		sp<Layout>		layout(void);
		/** Return a raw pointer to the Layout.  Mainly intended for Accessor
			for speed. */
		Layout			*rawLayout(void) const;
		/**	Add the given record to the array.	If the record array already
			has records and the Layout is different than for @em record
			the record will not be added and false will be returned.
			On success true is returned and if @em index is not NULL the index
			of the added record is return in @em index.  */
		bool			add(const RecordAV &record, IWORD *index = NULL);
		bool			add(sp<RecordArrayAV> spRA, IWORD start, IWORD size);

		/**	@internal
			Add records without triggering watchers. */
		bool			addCovert(const RecordAV &record, IWORD *index = NULL);
		/**	@internal
			Add records without triggering watchers. */
		bool			addCovert(sp<RecordArrayAV> spRA,
								IWORD start, IWORD size);

		/** create and add a record of an already set Layout.  This avoids
			the overhead of checking for duplicates. */
		bool			addCreateCovert(U32 a_count);

		/** Remove the given record from the array.  On success return
			true and if @em index is provided return the index of the removed
			record.  If the record is not in the array return false. */
		bool			remove(const RecordAV &record, IWORD *index = NULL);

						/** Remove record at given index.  The array is kept
							contiguous by moving the last element into the
							the given index after removal.	Therefore, in
							an iteration that involves deletion it is best
							to iterate backwards through the array. */
		void			remove(IWORD index);

		/**	@internal
			Remove records without triggering watchers. */
		bool			removeCovert(const RecordAV &record,IWORD *index=NULL);
		/**	@internal
			Remove records without triggering watchers. */
		void			removeCovert(IWORD index);
		///	@internal
		void			bind(sp<RecordGroup> a_spRecordGroup)
						{	m_hpRecordGroup=a_spRecordGroup; }

		/** Return the record at the given index. */
		RecordAV		getRecord(IWORD index);
		/// Return a non-persistent record for the given index
		WeakRecordAV	getWeakRecord(IWORD index);
		/**	Clear the record array. */
		void			clear(void);
		/**	Return true if @em record is in the record array.  Otherwise return
			false. */
		bool			find(const RecordAV &record);

		void			set(WeakRecordAV &record, IWORD index);
		void			set(RecordAV &record, IWORD index);

		void			setLayout(const sp<LayoutAV>& rspLayout);

						/// @brief Remove any invalid weak references
		void			prune(void);

						/** @internal

							@brief Choose weak referencing

							This should only be set by RecordGroup at
							construction. */
		void			setWeak(BWORD weak);

						/// @brief Return TRUE if using weak referencing
		BWORD			isWeak(void) const		{ return m_weak; }

						/** @internal

							@brief Don't retain a mappping from Record
							to array index.

							RecordArray::remove will be faster,
							while RecordArray::find will be slower.

							This should only be set by RecordGroup at
							construction. */
		void			enableDuplicates(bool a_bool);

						/** @internal

							@brief Don't reorder entries during removal.

							RecordArray::remove can be slower,
							particularly if enableDuplicates is false.

							This should only be set by RecordGroup at
							construction. */
		void			maintainOrder(bool a_bool);


		template <class T>
		T				&accessAttribute(FE_UWORD aLocator,
								FE_UWORD aIndex) const;
	private:
		/**	Return the arrayindex at the given index. */
		FE_UWORD		arrayindex(IWORD index) const;
		template <class T>
		T				&accessAttributeUnsafe(FE_UWORD aLocator,
								FE_UWORD aIndex) const;
	private:
		void			grow(void);
		void			copy(const RecordArrayAV &other);

		I32				readSerialNumber(IWORD index) const;

		void			trackBlock(U32 index);
		void			untrackBlock(U32 index);

		void			acquireAV(IWORD index);
		void			releaseAV(IWORD index);

	private:
		hp<RecordGroup>	m_hpRecordGroup;
		sp<LayoutAV>	m_spLayout;
		Array<FE_UWORD>	m_pArrayIndex;
		Array<IWORD>	m_pSN;
		BWORD			m_weak;
		FE_UWORD		m_serialLocator;
		bool			m_duplicates;
		bool			m_maintainOrder;
		t_av_map		m_av_map;

#if FE_COUNTED_TRACK
	public:
const	String&			name(void) const
						{	return m_spLayout.isValid()?
									m_spLayout->name(): Counted::name(); }
const	String			verboseName(void) const
						{	return "RecordArray " +
									String(m_weak? "(weak) ": "") + name(); }
#endif
};

template <class T>
inline T &RecordArrayAV::accessAttribute(FE_UWORD aLocator, FE_UWORD aIndex) const
{
//	return *(T *)(m_spLayout
//			->m_attributeVector[m_spLayout->m_locatorTable[aLocator]]
//			->raw_at(arrayindex(aIndex)));
#if 0
	return *(T *)(m_spLayout
			->m_attributeVector[m_spLayout->m_locatorTable[aLocator]]
			->raw_at(m_pArrayIndex[aIndex]));
#endif
	return *(T *)(m_spLayout->voidAccess(aLocator, m_pArrayIndex[aIndex]));
}

template <class T>
inline T &RecordArrayAV::accessAttributeUnsafe(
	FE_UWORD aLocator, FE_UWORD aIndex) const
{
#if 0
	return *(T *)(m_spLayout
			->m_attributeVector[m_spLayout->m_locatorTable[aLocator]]
			->raw_at(m_pArrayIndex[aIndex]));
#endif
	return *(T *)(m_spLayout->voidAccess(aLocator, m_pArrayIndex[aIndex]));
}


class PtrRecordArrayAVInfo : public BaseType::Info
{
	public:
virtual	IWORD	output(std::ostream &ostrm, void *instance, t_serialMode mode);
virtual	void	input(std::istream &istrm, void *instance, t_serialMode mode);
virtual	IWORD	iosize(void);
virtual	bool	getConstruct(void);
virtual	void	construct(void *instance);
virtual	void	destruct(void *instance);
};

inline void RecordArrayAV::setWeak(BWORD weak)
{
	m_weak = weak;
#if FE_COUNTED_TRACK
	//* propagate name change
	sp<RecordArrayAV> spRA(this);
#endif
}

inline void RecordArrayAV::maintainOrder(BWORD a_bool)
{
	m_maintainOrder = a_bool;
}

inline void RecordArrayAV::set(WeakRecordAV &record, IWORD index)
{
	record.set(m_pArrayIndex[index], m_spLayout);
}

inline void RecordArrayAV::set(RecordAV &record, IWORD index)
{
	record.set(m_pArrayIndex[index], m_spLayout);
}

inline IWORD RecordArrayAV::length(void)
{
	return m_pArrayIndex.size();
}

// TODO: AJW: if we choose just AV,
// go back to returning const reference of LayoutAV
inline sp<Layout> RecordArrayAV::layout(void)
{
	return m_spLayout;
}

inline RecordAV RecordArrayAV::getRecord(IWORD index)
{
	RecordAV record;
	FE_UWORD ai=arrayindex(index);
	if(ai != LayoutAV::arrayindexNone)
	{
		record.set(ai, m_spLayout);
	}
	return record;
}

inline WeakRecordAV RecordArrayAV::getWeakRecord(IWORD index)
{
	WeakRecordAV record;
	FE_UWORD ai=arrayindex(index);
	if(ai != LayoutAV::arrayindexNone)
	{
		record.set(ai, m_spLayout);
	}
	return record;
}

inline void RecordArrayAV::acquireAV(IWORD index)
{
	if(!m_weak)
	{
		m_spLayout->acquireArrayIndex(arrayindex(index));
	}
	trackBlock(index);
}

inline void RecordArrayAV::releaseAV(IWORD index)
{
	untrackBlock(index);
	if(!m_weak)
	{
		m_spLayout->releaseArrayIndex(arrayindex(index));
	}
}

inline I32 RecordArrayAV::readSerialNumber(IWORD index) const
{
	if(m_serialLocator!=LayoutAV::locatorNone &&
			m_pArrayIndex[index]!=LayoutAV::arrayindexNone)
	{
#ifdef FE_AV_FASTITER_ENABLE
		if(!m_spLayout->existenceCheck(m_serialLocator, m_pArrayIndex[index]))
		{
			return -1;
		}
#endif
		return accessAttributeUnsafe<int>(m_serialLocator, index);
	}
	else
	{
		return -1;
	}
}


inline FE_UWORD RecordArrayAV::idr(IWORD index)
{
	return m_spLayout->idr(arrayindex(index));
}

inline void RecordArrayAV::untrackBlock(U32 index)
{
#if FE_COUNTED_TRACK
//	untrackReference(this, this);
#endif
}

inline void RecordArrayAV::trackBlock(U32 index)
{
#if FE_COUNTED_TRACK
	//* NO GOOD
//	trackReference(this,this,"RecordArray");
#endif
}


inline FE_UWORD RecordArrayAV::arrayindex(IWORD index) const
{
#if FE_CODEGEN<=FE_DEBUG
	if(index<0 || index >= IWORD(m_pArrayIndex.size()))
	{
		feX("fe::RecordArray::data","index out of range");
	}
#endif
	if(m_weak && m_pSN[index]>=0 && m_pSN[index]!=readSerialNumber(index))
	{
		feLog("RecordArray::data weak element %d is not valid (%d vs %d)\n",
				index,m_pSN[index],readSerialNumber(index));
#if FE_COUNTED_TRACK
		feLog("  name \"%s\"\n",name().c_str());
#endif

		*(const_cast<FE_UWORD *>(&(m_pArrayIndex[index])))=
				LayoutAV::arrayindexNone;
		return LayoutAV::arrayindexNone;
	}
	return m_pArrayIndex[index];
}

};

#endif /* __data_RecordArrayAV_h__ */

