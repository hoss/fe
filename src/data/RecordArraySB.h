/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __data_RecordArraySB_h__
#define __data_RecordArraySB_h__

namespace fe
{

struct hash_sb_pointer
{ FE_UWORD operator()(const void *pV) const
	{ return reinterpret_cast<FE_UWORD>(pV); } };

struct eq_sb_pointer
{ bool operator()(void *pV1, void *pV2) const
	{ return pV1 == pV2; } };

#if 1
typedef HashMap<	void *,
					IWORD,
					hash_sb_pointer,
					eq_sb_pointer>		t_sb_map;
#else
typedef std::map< void *, IWORD >		t_sb_map;
#endif


/**	@brief Homogeneous collection of Records

	@ingroup data

	An array of Records, or more precisely an array of state block pointers
	all pointing to state blocks of the same layout.  Therefore only a single
	reference to the layout itself is stored.  Therefore this record array
	object consumes less memory than an array of record objects.
	*/
class FE_DL_EXPORT RecordArraySB : public Counted
{
	public:
		RecordArraySB(void);
		RecordArraySB(sp<LayoutSB> spLayout);
		RecordArraySB(sp<LayoutSB> spLayout, FE_UWORD aCount);
		RecordArraySB(const RecordArraySB &other);
virtual	~RecordArraySB(void);

		RecordArraySB	&operator=(const RecordArraySB &other);
		// /**	Return the state block at the given index. */
		//void			*operator[](IWORD index);
		FE_UWORD			idr(IWORD index);
		/**	Return the length of the array. */
		IWORD			length(void);
		/** Return the Layout. */
		sp<Layout>		layout(void);
		/** Return a raw pointer to the Layout.  Mainly intended for Accessor
			for speed. */
		//LayoutSB		*rawLayout(void) const;
		/**	Add the given record to the array.  If the record array already
			has records and the Layout is different than for @em record
			the record will not be added and false will be returned.
			On success true is returned and if @em index is not NULL the index
			of the added record is return in @em index . */
		bool			add(const RecordSB &record, IWORD *index = NULL);

		bool			add(sp<RecordArraySB> spRA, IWORD start, IWORD size);

		bool			addCovert(const RecordSB &record, IWORD *index = NULL){return false;}
		bool			addCovert(sp<RecordArraySB> spRA, IWORD start, IWORD size){return false;}

		void			enableDuplicates(bool a_bool);

		/** create and add a record of an already set Layout.  This avoids
			the overhead of checking for duplicates. */
		bool			addCreate(void);

		/** Remove the given record from the array.  On success return
			true and if @em index is provided return the index of the removed
			record.  If the record is not in the array return false. */
		bool			remove(const RecordSB &record, IWORD *index = NULL);

		// dummy due to AV
		bool			removeCovert(const RecordSB &record,IWORD *index=NULL) { return false; }
		void			removeCovert(IWORD index) {}
		void			bind(sp<RecordGroup> a_spRecordGroup) {}

		/** Return the record at the given index. */
		RecordSB		getRecord(IWORD index);
		/// Return a non-persistent record for the given index
		WeakRecordSB	getWeakRecord(IWORD index);
		/**	Clear the record array. */
		void			clear(void);
		/**	Return true if @em record is in the record array.  Otherwise return
			false. */
		bool			find(const RecordSB &record);

		void			set(WeakRecordSB &record, IWORD index);
		void			set(RecordSB &record, IWORD index);

		void			setLayout(const sp<LayoutSB>& rspLayout);

						/// @brief Remove any invalid weak references
		void			prune(void);

						/** @internal

							@brief Choose weak referencing

							This should only be set by RecordGroup at
							construction. */
		void			setWeak(BWORD weak);

						/// @brief Return TRUE if using weak referencing
		BWORD			isWeak(void) const		{ return m_weak; }

						/** Remove record at given index.  The array is kept
							contiguous by moving the last element into the
							the given index after removal.  Therefore, in
							an iteration that involves deletion it is best
							to iterate backwards through the array. */
		void			remove(IWORD index);

		template <class T>
		T				&accessAttribute(FE_UWORD aLocator, FE_UWORD aIndex) const;
	private:
		/**	Return the state block at the given index. */
		void			*data(IWORD index) const;
	private:
		void			grow(void);
		void			copy(const RecordArraySB &other);
		void			acquireSB(IWORD index);
		void			releaseSB(IWORD index);

		I32				readSerialNumber(IWORD index) const;

		void			trackBlock(U32 index);
		void			untrackBlock(U32 index);

	private:
		sp<LayoutSB>	m_spLayout;
		void			**m_ppSB;
		IWORD			*m_pSN;
		IWORD			m_allocated;
		IWORD			m_used;
		BWORD			m_weak;
		I32				m_serialOffset;
		bool			m_duplicates;
		t_sb_map		m_sb_map;

#if FE_COUNTED_TRACK
	public:
const	String&	name(void) const
				{	return m_spLayout.isValid()?
						m_spLayout->name(): Counted::name(); }
const	String	verboseName(void) const
				{	return "RecordArray " +
							String(m_weak? "(weak) ": "") + name(); }
#endif
};

template <class T>
inline T &RecordArraySB::accessAttribute(FE_UWORD aLocator, FE_UWORD aIndex) const
{
#if FE_CODEGEN<=FE_DEBUG
	return *((T *)((char *)(data(aIndex)) + m_spLayout->offsetTable()[aLocator]));
#else
	return *((T *)((char *)(data(aIndex)) + m_spLayout.raw()->offsetTable()[aLocator]));
#endif
}

class PtrRecordArraySBInfo : public BaseType::Info
{
	public:
virtual	IWORD	output(std::ostream &ostrm, void *instance, t_serialMode mode);
virtual	void	input(std::istream &istrm, void *instance, t_serialMode mode);
virtual	IWORD	iosize(void);
virtual	bool	getConstruct(void);
virtual	void	construct(void *instance);
virtual	void	destruct(void *instance);
};

inline void RecordArraySB::setWeak(BWORD weak)
{
	m_weak = weak;
#if FE_COUNTED_TRACK
	//* propagate name change
	sp<RecordArraySB> spRA(this);
#endif
}

inline void RecordArraySB::set(WeakRecordSB &record, IWORD index)
{
	record.set(m_ppSB[index]);
}

inline void RecordArraySB::set(RecordSB &record, IWORD index)
{
	record.set(m_ppSB[index]);
}

#if 0
inline LayoutSB *RecordArraySB::rawLayout(void) const
{
	return m_spLayout.raw();
}
#endif

#if 0
inline void *RecordArraySB::operator[](IWORD index)
{
	return m_ppSB[index];
}
#endif

inline IWORD RecordArraySB::length(void)
{
	return m_used;
}

inline sp<Layout> RecordArraySB::layout(void)
{
	return m_spLayout;
}

inline RecordSB RecordArraySB::getRecord(IWORD index)
{
	RecordSB record;
	void* pSB=data(index);
	if(pSB)
	{
		record.set(pSB);
	}
	return record;
}

inline WeakRecordSB RecordArraySB::getWeakRecord(IWORD index)
{
	WeakRecordSB record;
	void* pSB=data(index);
	if(pSB)
	{
		record.set(pSB);
	}
	return record;
}

inline void RecordArraySB::acquireSB(IWORD index)
{
	if(!m_weak)
	{
		FE_SB_TO_HDR(m_ppSB[index])->m_pStore->acquireSB(m_ppSB[index]);
	}
	trackBlock(index);
}

inline void RecordArraySB::releaseSB(IWORD index)
{
	untrackBlock(index);
	if(!m_weak)
	{
		FE_SB_TO_HDR(m_ppSB[index])->m_pStore->releaseSB(getRecord(index));
	}
}

inline I32 RecordArraySB::readSerialNumber(IWORD index) const
{
	return (m_serialOffset>=0 && m_ppSB[index])?
			*((I32 *)((char *)(m_ppSB[index]) + m_serialOffset)): -1;
}

inline void* RecordArraySB::data(IWORD index) const
{
#if FE_CODEGEN<=FE_DEBUG
	if(index >= m_used || index < 0)
	{
		feX("fe::RecordArray::data","index out of range");
	}
#endif
#if 0
	if(m_weak)
	{
		feLog("RecordArray::data %d SN %d vs %d\n",
				index,m_pSN[index],readSerialNumber(index));
	}
#endif

	if(m_weak && m_pSN[index]>=0 && m_pSN[index]!=readSerialNumber(index))
	{
		feLog("RecordArray::data weak element %d is not valid (%d vs %d)\n",
				index,m_pSN[index],readSerialNumber(index));
#if FE_COUNTED_TRACK
		feLog("  name \"%s\"\n",name().c_str());
#endif

		m_ppSB[index]=NULL;
		return NULL;
	}
	return m_ppSB[index];
}

inline FE_UWORD RecordArraySB::idr(IWORD index)
{
	return reinterpret_cast<FE_UWORD>(data(index));
}

inline void RecordArraySB::untrackBlock(U32 index)
{
#if FE_COUNTED_TRACK
	untrackReference(FE_SB_TO_HDR(m_ppSB[index]),this);
#endif
}

inline void RecordArraySB::trackBlock(U32 index)
{
#if FE_COUNTED_TRACK
	String blockname;
	blockname.sPrintf("block:%d %s",index,name().c_str());
	trackReference(FE_SB_TO_HDR(m_ppSB[index]),this,"RecordArray");
#endif
}

};

#endif /* __data_RecordArraySB_h__ */
