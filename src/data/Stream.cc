/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <data/data.pmh>

namespace fe
{
namespace data
{

/* ************************************************************************ */
/* Stream                                                                   */

AsciiStream::AsciiStream(void):
		m_name("AsciiStream")
{
}

AsciiStream::AsciiStream(sp<Scope> spScope):
		m_name("AsciiStream")
{
	m_spScope = spScope;
#if FE_COMPONENT_TRACK
	m_spScope->trackReference(this,"AsciiStream::AsciiStream");
#endif

	m_spWriter = new AsciiWriter(m_spScope);
	m_spReader = new AsciiReader(m_spScope);
}

AsciiStream::~AsciiStream(void)
{
#if FE_COMPONENT_TRACK
	if(m_spScope.isValid())
	{
		m_spScope->untrackReference(this);
	}
#endif
}

void AsciiStream::bind(sp<Scope> spScope)
{
#if FE_COMPONENT_TRACK
	if(m_spScope.isValid())
	{
		m_spScope->untrackReference(this);
	}
#endif

	m_spScope = spScope;

#if FE_COMPONENT_TRACK
	m_spScope->trackReference(this,"AsciiStream::AsciiStream");
#endif

	m_spWriter = new AsciiWriter(m_spScope);
	m_spReader = new AsciiReader(m_spScope);
}

void AsciiStream::output(std::ostream &ostrm, sp<RecordGroup> spRG)
{
	m_spWriter->output(ostrm, spRG);
}

sp<RecordGroup> AsciiStream::input(std::istream &istrm)
{
	return m_spReader->input(istrm);
}

BinaryStream::BinaryStream(void):
		m_name("BinaryStream")
{
}

BinaryStream::BinaryStream(sp<Scope> spScope):
		m_name("BinaryStream")
{
#if FE_COMPONENT_TRACK
	if(m_spScope.isValid())
	{
		m_spScope->untrackReference(this);
	}
#endif

	m_spScope = spScope;

#if FE_COMPONENT_TRACK
	m_spScope->trackReference(this,"BinaryStream::BinaryStream");
#endif

	m_spWriter = new BinaryWriter(m_spScope);
	m_spReader = new BinaryReader(m_spScope);
}

BinaryStream::~BinaryStream(void)
{
#if FE_COMPONENT_TRACK
	if(m_spScope.isValid())
	{
		m_spScope->untrackReference(this);
	}
#endif
}

void BinaryStream::bind(sp<Scope> spScope)
{
	m_spScope = spScope;
#if FE_COMPONENT_TRACK
	m_spScope->trackReference(this,"BinaryStream::BinaryStream");
#endif

	m_spWriter = new BinaryWriter(m_spScope);
	m_spReader = new BinaryReader(m_spScope);
}

void BinaryStream::output(std::ostream &ostrm, sp<RecordGroup> spRG)
{
	m_spWriter->output(ostrm, spRG);
}

sp<RecordGroup> BinaryStream::input(std::istream &istrm)
{
	return m_spReader->input(istrm);
}

AsciiFileStream::AsciiFileStream(void):
		m_name("AsciiFileStream")
{
}

AsciiFileStream::~AsciiFileStream(void)
{
#if FE_COMPONENT_TRACK
	if(m_spScope.isValid())
	{
		m_spScope->untrackReference(this);
	}
#endif
}

void AsciiFileStream::bind(sp<Scope> spScope)
{
#if FE_COMPONENT_TRACK
	if(m_spScope.isValid())
	{
		m_spScope->untrackReference(this);
	}
#endif
	m_spScope = spScope;
#if FE_COMPONENT_TRACK
	m_spScope->trackReference(this,"AsciiFileStream::bind");
#endif

	m_spStream = new AsciiStream(spScope);
}

void AsciiFileStream::output(const String &a_file, sp<RecordGroup> spRG)
{
	std::ofstream outfile(a_file.c_str());
	if(!outfile)
	{
		feX("AsciiFileStream::output",
			"could not open %s", a_file.c_str());
	}

	m_spStream->output(outfile, spRG);

	outfile.close();
}

sp<RecordGroup>	AsciiFileStream::input(const String &a_file)
{
	std::ifstream infile(a_file.c_str());
	if(!infile)
	{
		feX("AsciiFileStream::input",
			"could not open %s", a_file.c_str());
	}

	sp<RecordGroup> spRG;
	spRG = m_spStream->input(infile);

	infile.close();

	return spRG;
}

BinaryFileStream::BinaryFileStream(void):
		m_name("BinaryFileStream")
{
}

BinaryFileStream::~BinaryFileStream(void)
{
#if FE_COMPONENT_TRACK
	if(m_spScope.isValid())
	{
		m_spScope->untrackReference(this);
	}
#endif
}

void BinaryFileStream::bind(sp<Scope> spScope)
{
#if FE_COMPONENT_TRACK
	if(m_spScope.isValid())
	{
		m_spScope->untrackReference(this);
	}
#endif
	m_spScope = spScope;
#if FE_COMPONENT_TRACK
	m_spScope->trackReference(this,"BinaryFileStream::bind");
#endif

	m_spStream = new BinaryStream(spScope);
}

void BinaryFileStream::output(const String &a_file, sp<RecordGroup> spRG)
{
	std::ofstream outfile(a_file.c_str());
	if(!outfile)
	{
		feX("BinaryFileStream::output",
			"could not open %s", a_file.c_str());
	}

	m_spStream->output(outfile, spRG);

	outfile.close();
}

sp<RecordGroup>	BinaryFileStream::input(const String &a_file)
{
	std::ifstream infile(a_file.c_str());
	if(!infile)
	{
		feX("BinaryFileStream::input",
			"could not open %s", a_file.c_str());
	}

	sp<RecordGroup> spRG;
	spRG = m_spStream->input(infile);

	infile.close();

	return spRG;
}

} /* namespace */
} /* namespace */


