/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __data_BinaryWriter_h__
#define __data_BinaryWriter_h__

namespace fe
{
namespace data
{

class FE_DL_EXPORT BinaryWriter : public Scanner, public Writer
{
	public:
					BinaryWriter(sp<Scope> spScope);
virtual				~BinaryWriter(void);
virtual	void		output(std::ostream &ostrm, sp<RecordGroup> spRG);

	private:
virtual	void		write(std::ostream &ostrm, sp<RecordGroup> spRG, int id);
virtual	void		write(std::ostream &ostrm, sp<RecordArray> spRA, int id);
virtual	void		write(std::ostream &ostrm, sp<Layout> spLayout, int id);
virtual	void		write(std::ostream &ostrm, sp<Attribute> spAttribute);
		void		write(std::ostream &ostrm, Record record);
		void		writeInfo(std::ostream &ostrm);

		void		deepwrite(std::ostream &ostrm, sp<RecordArray> spRA);
};

} /* namespace */
} /* namespace */

#endif /* __data_Writer_h__ */

