/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __data_WatcherI_h__
#define __data_WatcherI_h__

namespace fe
{

class FE_DL_EXPORT WatcherI:
	virtual public Component,
	public CastableAs<WatcherI>
{
	public:
		/**	Add @em record to the collection.  */
virtual	void		add(const Record &record)								= 0;
		/** Remove @em record from the colletion. */
virtual	void		remove(const Record &record)							= 0;
		/** Clear the collection. @todo maybe not need this */
virtual	void		clear(void)												= 0;
};

} /* namespace */

#endif /* __data_WatcherI_h__ */

