/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <data/data.pmh>

namespace fe
{


//FE_DL_PUBLIC LayoutSB::Offset LayoutSB::offsetNone = 0xFFFFFFFF;

LayoutSB::LayoutSB(void)
{
	feX("fe::LayoutSB::LayoutSB","do not use default Layout constructor");
}

LayoutSB::LayoutSB(sp<Scope> &scope)
{
	m_name.sPrintf("__%d__", scope->getNextUniqueLayoutID());
	constructor(scope);
}

LayoutSB::LayoutSB(sp<Scope> &scope, const String &name)
{
	this->setName(name);
	constructor(scope);
}

LayoutSB::LayoutSB(Scope *pScope, const String &name)
{
	this->setName(name);
	sp<Scope> spScope(pScope);
	constructor(spScope);
}

LayoutSB::LayoutSB(const LayoutSB &other):
	Castable(),
	Initialized(),
	Layout(),
	Initialize<LayoutSB>(),
	ClassSafe<GlobalHolder>()
{
	copy(other);
}

void LayoutSB::constructor(sp<Scope> &scope)
{
	FEASSERT(scope.isValid());
	m_suppressLock = false;
	m_init = false;
	m_construct = false;
	m_pTable = NULL;
	m_hpScope = scope;
	m_size = 0;
	m_serialIndex = m_hpScope->serialIndex();
#if FE_COUNTED_TRACK
	Counted::registerRegion(this,sizeof(LayoutSB));
#endif

}

LayoutSB::~LayoutSB(void)
{
#if 0
	if(rawOffsetTable())
	{
		delete [] rawOffsetTable();
	}
#endif
}

LayoutSB &LayoutSB::operator=(const LayoutSB &other)
{
	SAFEGUARDCLASS;
	return copy(other);
}

LayoutSB &LayoutSB::copy(const LayoutSB &other)
{
	SAFEGUARDCLASS;
	if(&other == this) { return *this; }

	m_name = other.name();
	m_init = other.locked();
	m_size = other.size();
	if(m_pTable)
	{
		delete [] m_pTable;
	}

//	register
		FE_UWORD tablesize =
		sizeof(Offset) * other.m_hpScope->getAttributeCount();

	m_pTable = new Offset[other.m_hpScope->getAttributeCount()];
	memcpy((void *)offsetTable(), (void *)(other.offsetTable()), tablesize);

	return *this;
}

#if 0
FE_UWORD LayoutSB::attributeCount(void) const
{
	SAFEGUARDCLASS;
	return m_hpScope->getAttributeCount();
}
#endif

void LayoutSB::populate(const String &attribute_name)
{
	SAFEGUARDCLASS;
	m_hpScope->populate(name(), attribute_name);
}

void LayoutSB::populate(const String &attribute_name, const String &attribute_type)
{
	SAFEGUARDCLASS;
	m_hpScope->support(attribute_name, attribute_type);
	m_hpScope->populate(name(), attribute_name);
}

void LayoutSB::populate(const BaseAccessor &accessor)
{
	SAFEGUARDCLASS;
	if(locked())
	{
		if(!accessor.check(*this))
		{
			sp<Attribute> spAttribute = m_hpScope->attribute((FE_UWORD)accessor);
			feX("LayoutSB::populate",
					"locked layout \'%s\' must have \'%s\' attribute",
					name().c_str(),
					spAttribute->name().c_str());
		}
	}
	else
	{
		sp<Attribute> spAttribute = m_hpScope->attribute((FE_UWORD)accessor);
		m_hpScope->populate(name(), spAttribute->name());
	}
}

void LayoutSB::populate(sp<Attribute> spAttribute)
{
	SAFEGUARDCLASS;
	m_hpScope->populate(name(), spAttribute->name());
}

void LayoutSB::populate(sp<LayoutSB> spLayout)
{
	SAFEGUARDCLASS;
	m_hpScope->clonePopulate(spLayout->name(), name());
}

void LayoutSB::setLocked(bool locked)
{
	SAFEGUARDCLASS;
	m_init = locked;
	if (locked && !m_store.isValid())
	{
		m_store = new SegmentStore;
		sp<LayoutSB> self(this);
		m_store->setLayout(self);
	}
}


void LayoutSB::lock(void)
{
	SAFEGUARDCLASS;
	sp<LayoutSB> spLayout(this);
#if 0
	m_hpScope->lockLayout(spLayout);
	return;
#endif

	sp<TypeMaster> spTypeMaster(m_hpScope->typeMaster());

#ifdef FE_LOCK_SUPPRESSION
	if(spLayout->lockSuppressed())
	{
		feX("fe::Scope::lockLayout",
			"attempt to lock layout \'%s\' with lock suppression on",
			spLayout->name().c_str());
	}
#endif
	m_hpScope->layoutValidate();

	Array<String> attributes;

	FEASSERT(spLayout->scope().raw() == m_hpScope.raw());

	if(spLayout->locked())
	{
		feX("fe::Scope::lockLayout","attempt to lock already locked layout");
	}

	FE_UWORD cnt = m_hpScope->getAttributeCount();

	// setup and clear new table
	if(spLayout->rawOffsetTable() != NULL)
	{
		feX("fe::Scope::lockLayout","offset table not null for lock");
	}

	//sp<OffsetTable> newOffsetTable(new OffsetTable());

	m_offsetTable = new OffsetTable();
	m_offsetTable->resize(cnt);
	m_offsetTable->attach(spLayout);

	sp<Depend> spDepend;
	String attributeName;
	t_depends::iterator it_depend;
	Array<String>::iterator it_attribute;
	Array< std::list<FE_UWORD> > shareTable;
	shareTable.resize(cnt);
	//std::map< FE_UWORD, std::pair<size_t,String> > withinMap;
	//std::map< FE_UWORD, std::list<FE_UWORD> > withinReverseMap;

	// add attributes based on Depend::e_populate dependencies
	// and build share table
	for(it_depend = m_hpScope->depends().begin();
		it_depend != m_hpScope->depends().end(); it_depend++)
	{
		if(((*it_depend)->dependName() == "")
					||	((*it_depend)->dependName() == spLayout->name()) )
		{
			if((*it_depend)->dependFlag() & Depend::e_populate)
			{
				attributes.push_back((*it_depend)->attributeName());
			}
			if((*it_depend)->dependFlag() & Depend::e_share)
			{
				FE_UWORD idx_1, idx_2;
				sp<Attribute> spT1, spT2;
				spT1 = m_hpScope->findAttribute((*it_depend)->shareName(), idx_1);
				spT2 = m_hpScope->findAttribute((*it_depend)->attributeName(), idx_2);
				if(spT1.isValid() && spT2.isValid())
				{
					if(spT1->type() != spT2->type())
					{
						std::list<String> names;
						String s1,s2;
						spTypeMaster->reverseLookup(spT1->type(), names);
						s1.cat(names," ");
						names.clear();
						spTypeMaster->reverseLookup(spT2->type(), names);
						s2.cat(names, " ");
						feX("fe::Scope::lockLayout",
							"share type mismatch %s[%s] != %s[%s]",
							spT1->name().c_str(),s1.c_str(),
							spT2->name().c_str(),s2.c_str() );
					}
					shareTable[idx_1].push_back(idx_2);
					shareTable[idx_2].push_back(idx_1);
				}
			}
#if 0
			if((*it_depend)->dependFlag() & Depend::e_within)
			{
				FE_UWORD idx_p, idx_c;
				sp<Attribute> spParent, spChild;
				spParent = m_hpScope->findAttribute((*it_depend)->shareName(), idx_p);
				spChild = m_hpScope->findAttribute((*it_depend)->attributeName(), idx_c);
				if(spParent.isValid() && spChild.isValid())
				{
					std::map< FE_UWORD, std::pair<size_t,String> >::iterator
						i_within = withinMap.find(idx_p);
					if(i_within != withinMap.end())
					{
						std::list<String> names;
						String s1,s2;
						spTypeMaster->reverseLookup(spParent->type(), names);
						s1.cat(names," ");
						names.clear();
						spTypeMaster->reverseLookup(spChild->type(), names);
						s2.cat(names, " ");
						feX("fe::Scope::lockLayout",
							"cannot next withins %s[%s] %s[%s]",
							spParent->name().c_str(),s1.c_str(),
							spChild->name().c_str(),s2.c_str() );
					}
					withinMap[idx_c] =
						std::pair<size_t, String>
						((*it_depend)->offset(), spParent->name());
					withinReverseMap[idx_p].push_back(idx_c);
				}
			}
#endif
		}
	}

	// add attributes based on Depend::e_attribute dependencies
	t_depends depend_list;
	for(it_depend = m_hpScope->depends().begin();
		it_depend != m_hpScope->depends().end(); it_depend++)
	{
		if((*it_depend)->dependFlag() & Depend::e_attribute)
		{
			depend_list.push_back(*it_depend);
			std::sort((*it_depend)->matchAttributes().begin(),
				(*it_depend)->matchAttributes().end());
		}
	}
	bool change = true;
	while(change)
	{
		change = false;
		std::sort(attributes.begin(), attributes.end());
		for(it_depend = depend_list.begin();
				it_depend != depend_list.end();/* it_depend++ below */)
		{
			Array<String> &matchattrs = (*it_depend)->matchAttributes();

			if(std::includes<Array<String>::iterator,
				Array<String>::iterator>(
					attributes.begin(), attributes.end(),
					matchattrs.begin(), matchattrs.end()))
			{
				change = true;
				attributes.push_back((*it_depend)->attributeName());
				std::sort(attributes.begin(), attributes.end());
				sp<Depend> spDep = *it_depend;
				it_depend++;
				depend_list.remove(spDep);
			}
			else
			{
				it_depend++;
			}
		}
	}


	spLayout->setSize(0);
	sp<Attribute> spAttribute;
	FE_UWORD index;

	for(it_attribute = attributes.begin();it_attribute != attributes.end();
			it_attribute++)
	{
		spAttribute = m_hpScope->findAttribute(*it_attribute, index);
		if(!spAttribute.isValid())
		{
			feX("fe::Scope::lockLayout",
				"Layout \"%s\" not initialized because "
				"Attribute \"%s\" was not found",
				spLayout->name().c_str(), it_attribute->c_str());
		}
		if(spLayout->rawOffsetTable()[index] == offsetNone)
		{
#if 0
			// being a child within :) overrides getting added
			std::map< FE_UWORD, std::pair<Offset,String> >::iterator i_within =
				withinMap.find(index);
			if(i_within != withinMap.end())
			{
				bool is_within = false;
				for(Array<String>::iterator ja = attributes.begin();
					ja != attributes.end(); ja++)
				{
					if(*ja == i_within->second.second)
					{
						is_within = true;
						break;
					}
				}
				if(is_within)
				{
					continue;
				}
			}
#endif

			// check for share
			for(std::list<FE_UWORD>::iterator it_share
					= shareTable[index].begin();
				it_share != shareTable[index].end();
				it_share++)
			{
				FE_UWORD idx = *it_share;
				if(spLayout->rawOffsetTable()[idx] != offsetNone)
				{
					feLog("Scope::lockLayout share %d %p\n",
							index, spLayout->rawOffsetTable()[idx]);
					spLayout->setOffset(index, spLayout->rawOffsetTable()[idx]);
					break;
				}
			}
			// if not shared, add
			if(spLayout->rawOffsetTable()[index] == offsetNone)
			{
				if(!spAttribute->type().isValid())
				{
					feX("fe::Scope::lockLayout",
						"Layout \"%s\" not initialized because "
						"Attribute \"%s\" had no valid type",
						spLayout->name().c_str(), it_attribute->c_str());
				}
				FE_UWORD alignment=0;
				if(spAttribute->type()->getInfo().isValid())
				{
					alignment=spAttribute->type()->getInfo()->alignment();
				}
				if(alignment)
				{
					// TODO: rearrange layout for more efficient alignment
					/*** NOTE SegmentStore actually has 16 bytes already filled
						from the original allocate(), SBHeader + Segment,
						but that has no net skew. */
					FE_UWORD skew=spLayout->size()%alignment;
					if(skew)
					{
						FE_UWORD pad=alignment-skew;

#if FE_SC_PADDING_VERBOSE
						feLog("Scope::lockLayout padding %s:%s"
								" to %p at %p by %p\n",
								spLayout->name().c_str(),
								spAttribute->name().c_str(),
								alignment,spLayout->size(),pad);
#endif

						spLayout->setSize(spLayout->size()+pad);
					}
				}
#if FALSE
				feLog("Scope::lockLayout %s for %s at %p align %p\n",
						spLayout->name().c_str(),
						spAttribute->name().c_str(),
						spLayout->size(),
						alignment);
#endif
				spLayout->setOffset(index, spLayout->size());
				spLayout->setSize(spLayout->size() +
						spAttribute->type()->size());

#if 0
				// check for children within
				std::map< FE_UWORD, std::list<FE_UWORD> >::iterator i_reverse =
					withinReverseMap.find(index);
				if(i_reverse != withinReverseMap.end())
				{
					for(std::list<FE_UWORD>::iterator i_child =
						i_reverse->second.begin();
						i_child != i_reverse->second.end(); i_child++)
					{
						FE_UWORD idx_c = *i_child;
						spLayout->setOffset(idx_c,
							spLayout->rawOffsetTable()[index] +
							withinMap[idx_c].first);
					}
				}
#endif
			}
			if(spAttribute->type()->getConstruct())
			{
				spLayout->setConstruct(true);
			}
		}
	}


#if 0
#if 1
	bool append = true;
	for(unsigned int i_ot = 0; i_ot < m_offsetTables.size(); i_ot++)
	{
		bool match = true;
		FE_UWORD attr_cnt = getAttributeCount();
		for(FE_UWORD i_a = 0; i_a < attr_cnt; i_a++)
		{
			if(newOffsetTable->table()[i_a] != m_offsetTables[i_ot]->table()[i_a])
			{
				match = false;
				break;
			}
		}
		if(match)
		{
			m_offsetTables[i_ot]->attach(spLayout);
			append = false;
			break;
		}

	}
	if(append)
	{
		m_offsetTables.push_back(newOffsetTable);
	}
#else
	m_offsetTables.push_back(newOffsetTable);
#endif
#endif


	spLayout->cacheIndices();
	spLayout->setLocked(true);

	m_hpScope->resizeLayoutLocators();

#if FE_COUNTED_TRACK
//	m_store->trackReference(fe_cast<Counted>(this),"Scope::lockLayout");
#endif
}

void LayoutSB::resizeLocatorTable(FE_UWORD aSize)
{
	if(locked())
	{
		m_offsetTable->resize(aSize);
		t_bitset bitset(scope()->attributes().size());
		for(unsigned int index = 0;index < scope()->attributes().size(); index++)
		{
			if(checkAttribute(index))
			{
				bitset.set(index);
			}
		}
		setBitset(bitset);
	}
}

#ifdef FE_LOCK_SUPPRESSION
void LayoutSB::suppressLock(bool suppression)
{
	SAFEGUARDCLASS;
	if(locked())
	{
		feX("LayoutSB::suppressLock",
			"attempt to suppress locking on already locked layout \'%s\'",
			name().c_str());
	}
	m_suppressLock = suppression;
}

bool LayoutSB::lockSuppressed(void) const
{
	SAFEGUARDCLASS;
	return m_suppressLock;
}
#endif

void LayoutSB::initialize(void)
{
	SAFEGUARDCLASS;
	if(!scope().isValid())
	{
		feX("fe::LayoutSB::initialize",
			"cannot initialize record layout without scope");
	}
	if (!locked())
	{
		lock();
	}
}

void LayoutSB::cacheIndices(void)
{
	SAFEGUARDCLASS;
	m_offsetIndex.clear();
	FE_UWORD cnt = scope()->getAttributeCount();
	for(FE_UWORD i = 0; i < cnt ; i++)
	{
		if(m_pTable[i] == offsetNone) { continue; }

		m_offsetIndex.push_back(i);
	}
}

RecordSB LayoutSB::createRecord(void)
{
	SAFEGUARDCLASS;
	if(!locked())
	{
		lock();
	}

	RecordSB record;
	createRecord(record);
	scope()->assignIDNumber<RecordSB>(record);
#if FE_SCOPE_SUPPORT_WATCHERS
	scope()->watch(record);
#endif
	return record;
}

void LayoutSB::createRecord(RecordSB &r_new)
{
	r_new.m_pStateBlock = m_store->createSB();
	r_new.m_spLayout = this;
	constructAttributes(r_new.data());
	r_new.acquire();

}

void LayoutSB::freeRecord(RecordSB &r_old)
{
	scope()->freeIDNumber(r_old);
	m_store->freeSB(r_old.data());
}


void LayoutSB::constructAttributes(void *stateBlock)
{
	SAFEGUARDCLASS;

	if(m_construct)
	{
#if 0
		FE_UWORD cnt = scope()->getAttributeCount();
		for(FE_UWORD i = 0; i < cnt ; i++)
		{
			if(m_pTable[i] == offsetNone) { continue; }

			scope()->attribute(i)->type()->construct(
				(void *)(((char *)stateBlock) + m_pTable[i]));
		}
#endif

		for(FE_UWORD i = 0; i < m_offsetIndex.size(); i++)
		{
			scope()->attribute(m_offsetIndex[i])->type()->construct(
				(void *)(((char *)stateBlock) + m_pTable[m_offsetIndex[i]]));
		}
	}

	for(FE_UWORD i = 0; i < m_offsetIndex.size(); i++)
	{
		Instance &defaultInstance =
			scope()->attribute(m_offsetIndex[i])->defaultInstance();
		if(defaultInstance.data())
		{
			Instance new_instance = Instance(
				(void *)(((char *)stateBlock) + m_pTable[m_offsetIndex[i]]),
				defaultInstance.type(), NULL);

			new_instance.assign(defaultInstance);
		}
	}

}

void LayoutSB::destructAttributes(void *stateBlock)
{
	SAFEGUARDCLASS;

	if(m_construct)
	{
#if 0
		FE_UWORD cnt = scope()->getAttributeCount();
		for(FE_UWORD i = 0; i < cnt ; i++)
		{
			if(m_pTable[i] == offsetNone) { continue; }

			scope()->attribute(i)->type()->destruct(
				(void *)(((char *)stateBlock) + m_pTable[i]));
		}
#endif

		for(FE_UWORD i = 0; i < m_offsetIndex.size(); i++)
		{
			scope()->attribute(m_offsetIndex[i])->type()->destruct(
				(void *)(((char *)stateBlock) + m_pTable[m_offsetIndex[i]]));
		}
	}
}


void LayoutSB::peek(Peeker &peeker)
{
	peeker.nl();
	peeker.bold();
	peeker.str().catf("%s:", m_name.c_str());
	peeker.normal();
	peeker.in();

	peeker.nl();
	peeker.str().catf("size: %u", size());

	if(!m_init)
	{
		peeker.nl();
		peeker.red();
		peeker.cat("not yet locked");
	}

	if(m_pTable)
	{
		peeker.nl();
		peeker.cat("offset table:");
		peeker.normal();
		peeker.in();
		peeker.cyan();
		for(FE_UWORD i = 0; i < scope()->getAttributeCount(); i++)
		{
			if(m_pTable[i] == offsetNone)
			{
#if 0
				peeker.nl();
				peeker.cat("NONE");
				peeker.str().cat("\t");
				peeker.cat(m_hpScope->attribute(i)->name());
#endif
			}
			else
			{
				peeker.nl();
				peeker.str().catf("%4u", m_pTable[i]);
				peeker.str().cat("\t");
				peeker.cat(m_hpScope->attribute(i)->name());
			}
		}
		peeker.out();
	}

	peeker.base();

	peeker.out();
}

LayoutSB::OffsetTable::OffsetTable(void)
{
	m_table = NULL;
	m_size = 0;
	suppressReport();
#if FE_COUNTED_TRACK
	setName("OffsetTable");
#endif
}

LayoutSB::OffsetTable::~OffsetTable(void)
{
	if(m_table) { delete [] m_table; }
}

void LayoutSB::OffsetTable::resize(unsigned int a_size)
{
	Offset *pOffset = new Offset[a_size];
	for(unsigned int i = 0; i < a_size; i++)
	{
		pOffset[i] = offsetNone;
	}
	if(m_table != NULL)
	{
		for(unsigned int i = 0; i < m_size; i++)
		{
			pOffset[i] = m_table[i];
		}
		delete [] m_table;
	}
	m_table = pOffset;
	m_size = a_size;
	for(FE_UWORD index = 0; index < m_layouts.size(); index++)
	{
		m_layouts[index]->setOffsetTable(m_table);
	}
}

void LayoutSB::OffsetTable::attach(sp<LayoutSB> a_layout)
{
	m_layouts.push_back(a_layout);
	a_layout->setOffsetTable(m_table);
}

void LayoutSB::notifyOfAttributeChange(sp<Depend> &depend)
{
	if(depend->dependFlag() & Depend::e_attribute)
	{
		depAttributeCheck(depend);
	}
	if(depend->dependFlag() & Depend::e_populate)
	{
		depRecordCheck(depend);
	}
}

bool LayoutSB::checkAttributeStr(const String &a_name)
{
	FE_UWORD index;
	if(!scope()->findAttribute(a_name,index).isValid())
	{
		return false;
	}
	return checkAttribute(index);
}

void LayoutSB::depAttributeCheck(sp<Depend> &depend)
{
	SAFEGUARDCLASS;
	if(locked())
	{
		for(unsigned int i = 0; i < depend->matchAttributes().size(); i++)
		{
			if(checkAttributeStr(depend->matchAttributes()[i]))
			//if(fe::checkAttribute(depend->matchAttributes()[i], *this))
			{
				if(!checkAttributeStr(depend->attributeName()))
				//if(!fe::checkAttribute(depend->attributeName(), *this))
				{
					feX("fe::Scope::depAttributeCheck",
						"enforce attribute failure (already locked): "
						"record type: %s MustHave: %s IfHas: %s",
						name().c_str(),
						depend->attributeName().c_str(),
						depend->matchAttributes()[i].c_str());
				}
			}
		}
	}
}

void LayoutSB::depRecordCheck(sp<Depend> &depend)
{
	SAFEGUARDCLASS;
	if(locked())
	{
		if(name() == depend->dependName())
		{
			if(!checkAttributeStr(depend->attributeName()))
			//if(!fe::checkAttribute(depend->attributeName(),*this))
			{
				feX("fe::Scope::depRecordCheck",
					"enforce attribute failure (already locked): "
					"record type: %s MustHave: %s IfIs: %s",
					name().c_str(),
					depend->attributeName().c_str(),
					depend->dependName().c_str());
			}
		}
	}
}

} /* namespace */


