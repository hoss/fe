/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __data_SegmentStore_h__
#define __data_SegmentStore_h__

namespace fe
{

/** @brief Memory manager for state blocks.

	@ingroup data
	*/
class FE_DL_EXPORT SegmentStore : public StoreI, public ClassSafe<GlobalHolder>
{
	public:
						SegmentStore(void);
virtual					~SegmentStore(void);
virtual	void			*createSB(void);
virtual	void			*createSB(FE_UWORD count);
virtual	hp<LayoutSB>&	getLayout(void);
virtual	void			setLayout(sp<LayoutSB>& rspLayout);
virtual	FE_UWORD			skipSizeSB(void);

						/// Get the long name of this Store
		const String	verboseName(void) const;

	private:
		friend class LayoutSB;
virtual	void			acquireSB(void *stateBlock);
virtual	void			releaseSB(const RecordSB &r_old);
virtual	void			freeSB(void *stateBlock);

	private:
		void			initSB(void *stateBlock);
		void			clear(void);
		void*			stateBlockOf(U32 index,void* pSegment);
		FE_UWORD			stateBlockBytes(void);
		FE_UWORD			segmentBytes(U32 count);
		bool			queryRC(void *sb, I32 *&pCount);

		class FreeHeader
		{
			public:
				FreeHeader		*m_pNext;
		};
		class Segment
		{
			public:
				FE_UWORD			m_allocated;
				FE_UWORD			m_used;
				Segment			*m_pNext;
		};

		hp<LayoutSB>			m_hpLayout;
		Segment					*m_pSegment;
		FreeHeader				*m_pFreeList;
		//Accessor<I32>			m_aRC;
		TypeInfo				m_i32TypeInfo;
		FE_UWORD					m_i32Index;
		FE_UWORD					m_cntIndex;
};

inline const String SegmentStore::verboseName(void) const
{
	return "SegmentStore " + name();
}


} /* namespace */

#endif /* __data_SegmentStore_h__ */

