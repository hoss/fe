/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#define FE_MTX_COUNT	(FE_CODEGEN<FE_DEBUG)

#ifndef __platform_mutex_h__
#define __platform_mutex_h__

#if 0
#include "boost/thread.hpp"
#define FE_YIELD boost::thread::yield
#define FE_SLEEP(s, ns) \
	struct boost::xtime boost_time; \
	boost_time.sec=seconds; \
	boost_time.nsec=nanoseconds; \
	boost::thread::sleep(boost_time);
#endif

#if 0
#include "boost/thread.hpp"
#include "boost/thread/thread.hpp"
#include "boost/thread/tss.hpp"
#include "boost/thread/condition.hpp"
typedef boost::shared_mutex mutex;
typedef boost::recursive_mutex recursive_mutex;
#define FE_YIELD boost::thread::yield
#define FE_SLEEP(s, ns) \
	struct boost::xtime boost_time; \
	boost_time.sec=seconds; \
	boost_time.nsec=nanoseconds; \
	boost::thread::sleep(boost_time);
#define FE_LOCK(s, m) s.lock()
#define FE_UNLOCK(s) s.unlock()
#define FE_M_LOCK(m) m->lock()
#define FE_M_UNLOCK(m) m->unlock()
#define FE_M_TRY_LOCK(m) m->try_lock()
#define FE_M_LOCK_SHARED(m) m->lock_shared()
#define FE_M_UNLOCK_SHARED(m) m->unlock_shared()
#define FE_M_TRY_LOCK_SHARED(m) m->try_lock_shared()
#endif

#if 0
#include "tbb/mutex.h"
#include "tbb/recursive_mutex.h"
#include "tbb/spin_rw_mutex.h"
#include "tbb/tick_count.h"
#include "tbb/tbb_thread.h"
typedef tbb::spin_rw_mutex mutex;
typedef tbb::recursive_mutex recursive_mutex;
#define FE_YIELD tbb::this_tbb_thread::yield
#define FE_SLEEP(s, ns) \
	tbb::tick_count::interval_t t(((double)s) + (((double)ns) * 1.0e-9)); \
	tbb::this_tbb_thread::sleep(t)
#define FE_LOCK(s, m) s.acquire(m)
#define FE_UNLOCK(s) s.release()
#define FE_M_LOCK(m) m->lock()
#define FE_M_UNLOCK(m) m->unlock()
#define FE_M_TRY_LOCK(m) m->try_lock()
#define FE_M_LOCK_SHARED(m) m->lock_read()
#define FE_M_UNLOCK_SHARED(m) m->unlock()
#define FE_M_TRY_LOCK_SHARED(m) m->try_lock_read()
#endif

namespace fe
{

/*
	boost::mutex			m_jobMonitor;
	boost::mutex::scoped_lock lock(m_jobMonitor);

	boost::condition		m_jobAvailable;
	m_jobAvailable.wait(lock);
	m_jobAvailable.notify_one();
	m_barrierHit.notify_all();
*/

extern "C"
{

typedef void* (FE_CDECL MutexInitFunction)(bool a_recursive);
typedef bool (FE_CDECL MutexLockFunction)(bool a_recursive,void* a_pRawMutex,
		bool a_un,bool a_try,bool a_readOnly);
typedef void (FE_CDECL MutexFinishFunction)(bool a_recursive,void* a_pRawMutex);

typedef void* (FE_CDECL MutexGuardInitFunction)(
		bool a_recursive,void* a_pRawMutex,bool a_readOnly);
typedef bool (FE_CDECL MutexGuardLockFunction)(bool a_recursive,void* a_pRawGuard,
		bool a_un,bool a_try,bool a_readOnly);
typedef void (FE_CDECL MutexGuardFinishFunction)(bool a_recursive,void* a_pRawGuard,
		bool a_readOnly);

typedef void* (FE_CDECL MutexConditionInitFunction)(void);
typedef bool (FE_CDECL MutexConditionWaitFunction)(void* a_pRawCondition,
		bool a_recursive,void* a_pRawGuard,bool a_readOnly);
typedef bool (FE_CDECL MutexConditionNotifyFunction)(void* a_pRawCondition,bool a_all);
typedef void (FE_CDECL MutexConditionFinishFunction)(void* a_pRawCondition);

} /* extern "C" */

FE_MEM_PORT	extern	MutexInitFunction*				gs_fnMutexInit;
FE_MEM_PORT	extern	MutexLockFunction*				gs_fnMutexLock;
FE_MEM_PORT	extern	MutexFinishFunction*			gs_fnMutexFinish;

FE_MEM_PORT	extern	MutexGuardInitFunction*			gs_fnMutexGuardInit;
FE_MEM_PORT	extern	MutexGuardLockFunction*			gs_fnMutexGuardLock;
FE_MEM_PORT	extern	MutexGuardFinishFunction*		gs_fnMutexGuardFinish;

FE_MEM_PORT	extern	MutexConditionInitFunction*		gs_fnMutexConditionInit;
FE_MEM_PORT	extern	MutexConditionWaitFunction*		gs_fnMutexConditionWait;
FE_MEM_PORT	extern	MutexConditionNotifyFunction*	gs_fnMutexConditionNotify;
FE_MEM_PORT	extern	MutexConditionFinishFunction*	gs_fnMutexConditionFinish;

FE_MEM_PORT	extern	String							gs_mutexSupport;
FE_MEM_PORT	extern	BWORD							gs_mutexChecked;
FE_MEM_PORT	extern	DL_Loader*						gs_pMutexLoader;

class FE_DL_PUBLIC FE_DL_EXPORT Mutex
{
	public:
				Mutex(BWORD a_recursive=FALSE);
				~Mutex(void);

static	BWORD	supported(void)		{ return (gs_fnMutexInit!=NULL); }
static	String	support(void)		{ return gs_mutexSupport; }

		BWORD	lock(void);
		BWORD	unlock(void);
		BWORD	tryLock(void);
		BWORD	lockReadOnly(void);
		BWORD	unlockReadOnly(void);
		BWORD	tryLockReadOnly(void);

	class Condition;
	class Guard
	{
		friend class Mutex::Condition;

		public:
					Guard(Mutex& a_rMutex,BWORD a_readOnly=FALSE,
							BWORD a_truly=TRUE);
					~Guard(void);

			BWORD	lock(void);
			BWORD	unlock(void);
			BWORD	tryLock(void);

		private:
			Mutex*	m_pMutex;
			void*	m_pRawGuard;
			bool	m_readOnly;
	};

	class Condition
	{
		public:
					Condition(void);
					~Condition(void);

			BWORD	wait(Guard& a_rGuard);
			BWORD	notifyOne(void);
			BWORD	notifyAll(void);

		private:
			void*	raw(void);

			void*	m_pRawCondition;
	};

static	void	replaceInitFunction(MutexInitFunction* a_fnInit)
				{	gs_fnMutexInit=a_fnInit; }
static	void	replaceLockFunction(MutexLockFunction* a_fnLock)
				{	gs_fnMutexLock=a_fnLock; }
static	void	replaceFinishFunction(MutexFinishFunction* a_fnFinish)
				{	gs_fnMutexFinish=a_fnFinish; }

static	void	replaceGuardInitFunction(MutexGuardInitFunction* a_fnGuardInit)
				{	gs_fnMutexGuardInit=a_fnGuardInit; }
static	void	replaceGuardLockFunction(MutexGuardLockFunction* a_fnGuardLock)
				{	gs_fnMutexGuardLock=a_fnGuardLock; }
static	void	replaceGuardFinishFunction(
						MutexGuardFinishFunction* a_fnGuardFinish)
				{	gs_fnMutexGuardFinish=a_fnGuardFinish; }

static	void	replaceConditionInitFunction(
						MutexConditionInitFunction* a_fnConditionInit)
				{	gs_fnMutexConditionInit=a_fnConditionInit; }
static	void	replaceConditionWaitFunction(
						MutexConditionWaitFunction* a_fnConditionWait)
				{	gs_fnMutexConditionWait=a_fnConditionWait; }
static	void	replaceConditionNotifyFunction(
						MutexConditionNotifyFunction* a_fnConditionNotify)
				{	gs_fnMutexConditionNotify=a_fnConditionNotify; }
static	void	replaceConditionFinishFunction(
						MutexConditionFinishFunction* a_fnConditionFinish)
				{	gs_fnMutexConditionFinish=a_fnConditionFinish; }

static	BWORD	confirm(String a_hint="");
static	BWORD	clearFunctionPointers(void);
static	BWORD	dismiss(void);

	protected:

		BWORD	m_recursive;

	private:

		void*	raw(void);

		bool	m_initializing;
		void*	m_pRawMutex;

#if FE_MTX_COUNT
		I32		m_fakeCount;
		I32		m_lockCount;
#endif
};

class FE_DL_PUBLIC FE_DL_EXPORT RecursiveMutex: public Mutex
{
	public:
				RecursiveMutex(void):
					Mutex(TRUE)												{}
};

} /* namespace */

#endif /* __platform_mutex_h__ */
