/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __platform_Count_h__
#define __platform_Count_h__

#define FE_COUNT_MT		TRUE

#if FE_COUNT_MT && FE_HW==FE_X86 && FE_OS!=FE_OSX
#define FE_COUNT_ASM_IMPL
#endif

/* https://en.wikipedia.org/wiki/Fetch-and-add
static inline int fetch_and_add(int* variable, int value)
{
    __asm__ volatile("lock; xaddl %0, %1"
      : "+r" (value), "+m" (*variable) // input+output
      : // No input-only
      : "memory"
    );
    return value;
}
*/

#ifdef FE_COUNT_ASM_IMPL
#if FE_OS==FE_LINUX
static inline int __attribute__ ((__unused__))
feAsmSwapIncr(volatile int *pInt, int incr)
{
#if TRUE
    __asm__ volatile("lock; xaddl %0, %1"
      : "+r" (incr), "+m" (*pInt) // input+output
      : // No input-only
      : "memory"
    );
    return incr;
#else
	//* 20180916 remove register keyword
//	register int r;
	int r;

	__asm__ __volatile__ ("lock; xaddl %0,%2"
		: "=r" (r) : "0" (incr), "m" (*pInt) : "memory");
	return r;
#endif
}

static inline void __attribute__ ((__unused__))
feAsmIncr(volatile int* pInt, int incr)
{
	feAsmSwapIncr(pInt, incr);
#if 0
	__asm__ __volatile__ ("lock; addl %0,%1"
		: : "ir" (incr), "m" (*pInt) : "memory");
#endif
}
#endif
#if FE_OS==FE_WIN32 || FE_OS==FE_WIN64
static inline int
feAsmSwapIncr(volatile int *pInt, int incr)
{
	return InterlockedExchangeAdd(
			reinterpret_cast<volatile LONG *>(pInt), incr);
#if 0
  __asm
  {
    mov ecx, pInt
    mov eax, incr
    lock xadd dword ptr [ecx], eax
  }
#endif
}

static inline void
feAsmIncr(volatile int* pInt, int incr)
{
	feAsmSwapIncr(pInt, incr);
}
#endif
#endif

#endif /* __platform_Count_h__ */
