/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __platform_envvar_h__
#define __platform_envvar_h__

#if FE_COMPILER==FE_GNU

// __builtin_prefetch(ADDR,WRITE 0-1,LOCALITY 0-3)
#define FEPREFETCH_READ(address)	__builtin_prefetch(address,0,0)
#define FEPREFETCH_WRITE(address)	__builtin_prefetch(address,1,0)
#define FE_PREFETCHING				TRUE

#else

#define FEPREFETCH_READ(address)
#define FEPREFETCH_WRITE(address)
#define FE_PREFETCHING				FALSE

#endif

namespace fe
{

/**************************************************************************//**
	@brief System call wrappers

	@ingroup platform
*//***************************************************************************/
class FE_DL_PUBLIC System
{
	public:
						System(void);

						/** @brief Print a startup message only one time

							Additional calls will print nothing.*/
static	void FE_CDECL	printBannerOnce(void);

						/** @brief Get an environment variable

							TRUE is returned only if the variable exists.*/
static	bool FE_CDECL	getEnvironmentVariable(const String &name,
								String &value);

						/** @brief Get the active user's home directory

							TRUE is returned only if the value is found.*/
static	bool FE_CDECL	getHomePath(String &value);

						/// @brief Get general verbosity setting
static	String FE_CDECL	getVerbose(void);

						/// @brief Return the date when System.cc was compiled
static	String FE_CDECL	buildDate(void);

						/** @brief Get the full path to the running executable

							Pruning will resolve all '..' tokens in the path.*/
static	String FE_CDECL	getExePath(BWORD a_prune);

						/** @brief Get the full path to the symbol's library

							Pruning will resolve all '..' tokens in the path.*/
static	String FE_CDECL	getLoadPath(void* a_symbol,BWORD a_prune);

						/// @brief Get the path for all relative file operations
static	String FE_CDECL	getCurrentWorkingDirectory(void);

						/// @brief Set the path for all relative file operations
static	bool FE_CDECL	setCurrentWorkingDirectory(
								const String &a_path);

						/// @brief Make a directory for the given path
static	bool FE_CDECL	createDirectory(const String &a_path);

						/// @brief Make all directories parent to the given file
static	bool FE_CDECL	createParentDirectories(const String &a_filename);

						/** @brief Get all the entries in a directory

							The entries arg is populated with all the files
							and subdirectories under the given path. */
static	bool FE_CDECL	listDirectory(String a_path,
								std::vector<String>& a_rEntries);

static	String FE_CDECL	demangle(const String a_symbol,
								BWORD a_emptyDefault=FALSE);

						/// @brief Return the path of the enclosing executable
static	String FE_CDECL getExecutablePath(void);

						/// @brief Return the path of the enclosing module
static	String FE_CDECL getModulePath(void);

						/** @brief Return the binary suffix that
							the current libs were built with */
static	String FE_CDECL getBinarySuffix(void);

						/** @brief Find first instance of file among paths

							The first path with the file is returned,
							or an empty string if there is no match. */
static	String FE_CDECL findFile(String a_filename,std::vector<String> a_paths);

						/** @brief Find first instance of file among paths

							The paths argument is a colon delimited string.
							The first path with the file is returned,
							or an empty string if there is no match. */
static	String FE_CDECL findFile(String a_filename,String a_delimitedPaths,
							String a_delimiter=":");

						/** @brief Return formatted time

							The format follows strftime(), except that
							the format sequence '${MILLISECONDS}' can be used
							to indicate milliseconds. */
static	String FE_CDECL localTime(String a_format="%Y%m%d %H:%M:%S");

	private:

static	BWORD	ms_bannerPrinted;
};

} /* namespace */

#endif /* __platform_envvar_h__ */

