/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __platform_Result_h__
#define __platform_Result_h__

namespace fe
{

#define	FE_FAILURE	0x80000000

/**************************************************************************//**
	@brief Generalized return value indicating success or failure, and why

	@ingroup platform

	The functions fe::succeeded and fe::failed can quickly determine which
	category the result belongs to.
*//***************************************************************************/
/// @todo eventually grep code for usage a remove useless ones
typedef enum
{
	e_ok=0x00000000,
	e_cancel,
	e_poison,
	e_alreadyAvailable,
	e_notNeeded,
	// add new success codes here.  NOTE: be sure to update resultString()

	e_undefinedFailure=FE_FAILURE,
	e_unsupported,
	e_refused,
	e_outOfMemory,
	e_invalidFile,
	e_invalidHandle,
	e_invalidPointer,
	e_invalidRange,
	e_notInitialized,
	e_cannotChange,
	e_aborted,
	e_writeFailed,
	e_readFailed,
	e_cannotFind,
	e_cannotCreate,
	e_unsolvable,
	e_impossible,
	e_usage,
	e_system,
	e_typeMismatch,
	e_corrupt
	// add new failure codes here.  NOTE: be sure to update resultString()
} Result;

/**************************************************************************//**
	@brief Returns TRUE if the result value indicates a success

	@ingroup platform
*//***************************************************************************/
inline BWORD successful(Result result)
{
	return ((result&FE_FAILURE)==0);
}

/**************************************************************************//**
	@brief Returns TRUE if the result value indicates a failure

	@ingroup platform
*//***************************************************************************/
inline BWORD failure(Result result)
{
	return ((result&FE_FAILURE)!=0);
}

/**************************************************************************//**
	@brief Return a string for a result code

	@ingroup platform
*//***************************************************************************/
FE_DL_EXPORT const char *resultString(Result result);

} /* namespace */

#endif /* __platform_Result_h__ */
