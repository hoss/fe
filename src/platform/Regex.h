/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __platform_regex_h__
#define __platform_regex_h__

namespace fe
{

extern "C"
{

typedef void* (FE_CDECL RegexInitFunction)(const char* a_pattern);
typedef void* (FE_CDECL RegexMatchFunction)(void* a_expression,const char* a_candidate);
typedef const char* (FE_CDECL RegexResultFunction)(void* a_result,U32 a_index);
typedef void* (FE_CDECL RegexReplaceFunction)(void* a_expression,
		const char* a_candidate,const char* a_replacement);
typedef void (FE_CDECL RegexFinishFunction)(void* a_expression);
typedef void (FE_CDECL RegexReleaseFunction)(void* a_result);

} /* extern "C" */

FE_MEM_PORT	extern	RegexInitFunction*		gs_fnRegexInit;
FE_MEM_PORT	extern	RegexMatchFunction*		gs_fnRegexMatch;
FE_MEM_PORT	extern	RegexMatchFunction*		gs_fnRegexSearch;
FE_MEM_PORT	extern	RegexResultFunction*	gs_fnRegexResult;
FE_MEM_PORT	extern	RegexReplaceFunction*	gs_fnRegexReplace;
FE_MEM_PORT	extern	RegexFinishFunction*	gs_fnRegexFinish;
FE_MEM_PORT	extern	RegexReleaseFunction*	gs_fnRegexRelease;
FE_MEM_PORT	extern	BWORD					gs_regexChecked;

class FE_DL_PUBLIC FE_DL_EXPORT Regex
{
	public:
				Regex(const char* a_pattern);
				~Regex(void);
		BWORD	match(const char* a_candidate);
		BWORD	search(const char* a_candidate);
const	char*	replace(const char* a_candidate,const char* a_replacement);
const	char*	result(U32 a_index);

static	void	replaceInitFunction(RegexInitFunction* a_fnInit)
				{	gs_fnRegexInit=a_fnInit; }
static	void	replaceMatchFunction(RegexMatchFunction* a_fnMatch)
				{	gs_fnRegexMatch=a_fnMatch; }
static	void	replaceSearchFunction(RegexMatchFunction* a_fnSearch)
				{	gs_fnRegexSearch=a_fnSearch; }
static	void	replaceResultFunction(RegexResultFunction* a_fnResult)
				{	gs_fnRegexResult=a_fnResult; }
static	void	replaceReplaceFunction(RegexReplaceFunction* a_fnReplace)
				{	gs_fnRegexReplace=a_fnReplace; }
static	void	replaceFinishFunction(RegexFinishFunction* a_fnFinish)
				{	gs_fnRegexFinish=a_fnFinish; }
static	void	replaceReleaseFunction(RegexReleaseFunction* a_fnRelease)
				{	gs_fnRegexRelease=a_fnRelease; }

static	BWORD	confirm(String a_hint="");

	private:

		void*					m_pExpression;
		void*					m_pResult;
};

} /* namespace */

#endif /* __platform_regex_h__ */
