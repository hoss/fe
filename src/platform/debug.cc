/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <platform/platform.pmh>

//#include <boost/format.hpp>

#define FE_LOG_GUARD_LOGGER	TRUE
#define FE_LOG_GUARD_COUT	!FE_LOG_GUARD_LOGGER

namespace fe
{

#if FE_SAFE_COUNTED_MUTEX
Log::Log(void):
		m_cnt(0)
	{
		if(ms_mutexCount++ == 0)
		{
			FEASSERT(!ms_pMutex);
			ms_pMutex=new RecursiveMutex();
		}
		FEASSERT(ms_pMutex);
	}

Log::~Log(void)
	{
		FEASSERT(ms_pMutex);
		if(--ms_mutexCount == 0)
		{
			delete ms_pMutex;
			ms_pMutex=NULL;
		}
	}
FE_DL_PUBLIC RecursiveMutex* Log::ms_pMutex=NULL;
FE_DL_PUBLIC I32 Log::ms_mutexCount=0;
#else
Log::Log(void): m_cnt(0) {}
Log::~Log(void) {}
FE_DL_PUBLIC RecursiveMutex Log::ms_mutex;
#endif

FE_DL_PUBLIC void intrusive_ptr_add_ref(Log *a_log)
{
	if (a_log)
	{
		a_log->acquire();
	}
}

FE_DL_PUBLIC void intrusive_ptr_release(Log *a_log)
{
	if (!a_log->release())
	{
		delete a_log;
	}
}

void StdoutLog::log(const std::string &a_message,
	std::map<std::string,std::string> &a_attributes)
{
	if(!fe::gs_pPrintFunction)
	{
		feX(e_refused,"StdoutLog::log","gs_pPrintFunction is NULL");
	}

#if FE_LOG_GUARD_COUT
	RecursiveMutex::Guard guard(mutex());
#endif

	fe::gs_pPrintFunction(a_message.c_str());
}

PrefaceLog::PrefaceLog(void)
{
	String value;
	m_annotate=(System::getEnvironmentVariable("FE_ANNOTATE", value) &&
			atoi(value.c_str()));
}

void PrefaceLog::log(const std::string &a_message,
	std::map<std::string,std::string> &a_attributes)
{
	if(!fe::gs_pPrintFunction)
	{
		feX(e_refused,"PrefaceLog::log","gs_pPrintFunction is NULL");
	}

	const String replaced=String(a_message.c_str()).replace("\n(.)",
			"\n"+preface()+"$1");

#if FE_LOG_GUARD_COUT
	RecursiveMutex::Guard guard(mutex());
#endif

	std::ostringstream stream;

	stream << preface().c_str();

	if(m_annotate)
	{
		const char* filename=a_attributes["file"].c_str();
		const char* basename=strrchr(filename,'/');

//		output(stream,a_attributes["date"].c_str(),11,false);
//		stream << " ";
//		output(stream,a_attributes["time"].c_str(),8,false);
		output(stream,System::localTime(
				"%H:%M:%S.${MILLISECONDS}").c_str(),13,false);
		stream << " ";
		output(stream,a_attributes["group"].c_str(),18,false);
		stream << " ";
		output(stream,basename? basename+1: filename,20,true);
		stream << ":";
		output(stream,a_attributes["line"].c_str(),4,false);
		stream << " ";
	}

	stream << replaced.c_str();

	fe::gs_pPrintFunction(stream.str().c_str());
}

void FileLog::setFilename(String a_filename)
{
#if FE_LOG_GUARD_COUT
	RecursiveMutex::Guard guard(mutex());
#endif

	m_filename=a_filename;
	m_ofstream=std::ofstream(a_filename.c_str());
}

void FileLog::log(const std::string &a_message,
	std::map<std::string,std::string> &a_attributes)
{
#if FE_LOG_GUARD_COUT
	RecursiveMutex::Guard guard(mutex());
#endif

	m_ofstream << a_message;
	m_ofstream.flush();
}

void AnnotatedLog::output(std::ostream& stream,const char* source,
	int size,bool right)
{
	char buffer[1024];

	const int len=strlen(source);
	const int start=(len>size)? len-size: 0;
	strncpy(buffer,&source[start],1023);
	buffer[len-start]=0;
	if(len>size)
	{
		buffer[0]='^';
	}
	if(right)
	{
		stream << std::setw(size) << std::right << buffer;
	}
	else
	{
		stream << std::setw(size) << std::left << buffer;
	}
}

void AnnotatedLog::log(const std::string &a_message,
	std::map<std::string,std::string> &a_attributes)
{
#if FE_LOG_GUARD_COUT
	RecursiveMutex::Guard guard(mutex());
#endif

	std::ostringstream stream;

	if(m_newline)
	{
		const char* filename=a_attributes["file"].c_str();
		const char* basename=strrchr(filename,'/');

//		output(stream,a_attributes["date"].c_str(),11,false);
//		stream << " ";
//		output(stream,a_attributes["time"].c_str(),8,false);
		output(stream,System::localTime(
				"%H:%M:%S.${MILLISECONDS}").c_str(),13,false);
		stream << " ";
		output(stream,a_attributes["group"].c_str(),18,false);
		stream << " ";
		output(stream,basename? basename+1: filename,20,true);
		stream << ":";
		output(stream,a_attributes["line"].c_str(),4,false);
		stream << " ";
	}

	stream << a_message.c_str();

	if(fe::gs_pPrintFunction)
	{
		fe::gs_pPrintFunction(stream.str().c_str());
	}
	else
	{
		feX(e_refused,"AnnotatedLog::log","gs_pPrintFunction is NULL");
	}

	const char* message=a_message.c_str();
	const int len=strlen(message);
	m_newline=(message[len-1]=='\n');
}

void GroupLog::log(const std::string &a_message,
	std::map<std::string,std::string> &a_attributes)
{
#if FE_LOG_GUARD_COUT
	RecursiveMutex::Guard guard(mutex());
#endif

	std::ostringstream stream;

	if(m_newline)
	{
#if FE_OS!=FE_WIN32 && FE_OS!=FE_WIN64
		stream << "[34m" ;
#endif
		output(stream,a_attributes["group"].c_str(),18,false);
#if FE_OS!=FE_WIN32 && FE_OS!=FE_WIN64
		stream << "[0m" ;
#endif
		stream << " ";
	}

	stream << a_message.c_str();

	if(fe::gs_pPrintFunction)
	{
		fe::gs_pPrintFunction(stream.str().c_str());
	}
	else
	{
		feX(e_refused,"GroupLog::log","gs_pPrintFunction is NULL");
	}

	const char* message=a_message.c_str();
	const int len=strlen(message);
	m_newline=(message[len-1]=='\n');
}

void vsPrintf(std::string &target, const char* fmt, va_list ap,int& size)
{
	if(size<0)
	{
		feX("(debug)vsPrintf size<0 indicates unheeded discontinuation");
	}
	if(!size)
	{
		// start at 80 bytes, seems reasonable (1 tty line)
		size=80;
	}
	char *tmpbuf = new char[size];
	FEASSERT(NULL != tmpbuf);

	int n=FEVSNPRINTF((char *)tmpbuf, size, fmt, ap);

	if(n> -1 && n<size)
	{
		size= -1;
		target = tmpbuf;
		delete [] tmpbuf;
		return;
	}
	if(n> -1)
	{
		size=n+1;
	}
	else
	{
		size*=2;
	}

	delete [] tmpbuf;
	target="";
}
#if FALSE
void vsPrintf(std::string &target, const char *fmt, va_list ap)
{

	int sz = 80;
	char *tmp = new char[sz];
	FEASSERT(NULL != tmp);
	while(true)
	{
		int n=FEVSNPRINTF((char *)tmp, sz, fmt, ap);

		if(n> -1 && n<sz) break;
		if(n> -1) sz=n+1;
		else sz*=2;

		delete [] tmp;
		tmp = new char[sz];
		FEASSERT(NULL != tmp);

		// NOTE calling vsnprintf again without va_end,va_start is undefined
		feX("(debug)vsPrintf warning, expanding buffer\n");
	}
	target = tmp;
	delete [] tmp;
}
#endif

Logger::Logger(void)
{
#if FE_SAFE_COUNTED_MUTEX
	if(ms_mutexCount++ == 0)
	{
		FEASSERT(!ms_pMutex);
		ms_pMutex=new RecursiveMutex();
	}
	FEASSERT(ms_pMutex);
#endif

	String value;
	if(System::getEnvironmentVariable("FE_ANNOTATE", value) &&
			atoi(value.c_str()))
	{
		m_directLog = new AnnotatedLog();
	}
	else
	{
		m_directLog = new StdoutLog();
	}

	setLog("stdout", m_directLog);
	setLog("win32", new Win32Log());
	bind(".*", "stdout");
	bind(".*", "win32");
}

#if FE_SAFE_COUNTED_MUTEX
Logger::~Logger(void)
	{
		delete m_directLog;

		FEASSERT(ms_pMutex);
		if(--ms_mutexCount == 0)
		{
			delete ms_pMutex;
			ms_pMutex=NULL;
		}
	}
FE_DL_PUBLIC RecursiveMutex* Logger::ms_pMutex=NULL;
FE_DL_PUBLIC I32 Logger::ms_mutexCount=0;
#else
Logger::~Logger(void) {}
FE_DL_PUBLIC RecursiveMutex Logger::ms_mutex;
#endif

#if 0
void Logger::direct(const char *a_format, ...) const
{
	if(m_directLog.get()) { return; }

	std::string output;
	va_list ap;
	int size=0;
	while(size>=0)
	{
		va_start(ap, a_format);
		vsPrintf(output,a_format,ap,size);
		va_end(ap);
	}

	m_directLog->log(output);
}
#endif


void Logger::dump(void)
{
	fe_printf("Logger::dump\n");
	fe_printf("logs:\n");
	for(std::map<String, t_logptr>::const_iterator
		i_log = m_logs.begin(); i_log != m_logs.end(); i_log++)
	{
		fe_printf("    '%s' %p\n",i_log->first.c_str(),i_log->second);
	}
	fe_printf("bindmap:\n");
	for(std::map<String, std::vector<t_logptr> >::const_iterator
		i_bind = m_bindmap.begin(); i_bind != m_bindmap.end(); i_bind++)
	{
		fe_printf("    '%s'\n",i_bind->first.c_str());
		for(unsigned int i = 0; i < i_bind->second.size(); i++)
		{
			fe_printf("        %p\n",i_bind->second[i]);
		}
	}
	fe_printf("bindings:\n");
	for(std::map<String, std::vector<String> >::const_iterator
		i_binding = m_bindings.begin();
		i_binding != m_bindings.end(); i_binding++)
	{
		fe_printf("    '%s'\n",i_binding->first.c_str());
		for(unsigned int i = 0; i < i_binding->second.size(); i++)
		{
			fe_printf("        '%s'\n",i_binding->second[i].c_str());
		}
	}
	fe_printf("antibindings:\n");
	for(std::map<String, std::vector<String> >::const_iterator
		i_binding = m_antibindings.begin();
		i_binding != m_antibindings.end(); i_binding++)
	{
		fe_printf("    '%s'\n",i_binding->first.c_str());
		for(unsigned int i = 0; i < i_binding->second.size(); i++)
		{
			fe_printf("        '%s'\n",i_binding->second[i].c_str());
		}
	}
}

void Logger::log(const String &a_group,
	std::map<std::string,std::string> &a_attributes, const char *a_format, ...)
{
#if FE_LOG_GUARD_LOGGER
	RecursiveMutex::Guard guard(mutex());
#endif

	std::map<String, std::vector<t_logptr> >::const_iterator
		i_bind = m_bindmap.find(a_group);
	if(i_bind == m_bindmap.end() || !i_bind->second.size())
	{
		wiregroup(a_group);
		i_bind = m_bindmap.find(a_group);
		if(i_bind == m_bindmap.end() || !i_bind->second.size())
		{
			return;
		}
	}

	std::string output;
	va_list ap;
	int size=0;
	while(size>=0)
	{
		va_start(ap, a_format);
		vsPrintf(output,a_format,ap,size);
		va_end(ap);
	}

	for(unsigned int i = 0; i < i_bind->second.size();i++)
	{
		i_bind->second[i]->log(output, a_attributes);
	}
}


void Logger::bind(const String &a_group, const String &a_logname)
{
	m_bindings[a_group].push_back(a_logname);

	m_bindmap[a_group].clear(); //* ensure existance

	for(std::map<String, std::vector<t_logptr> >::const_iterator
		i_bind = m_bindmap.begin(); i_bind != m_bindmap.end(); i_bind++)
	{
		wiregroup(i_bind->first);
	}
}

void Logger::antibind(const String &a_group, const String &a_logname)
{
	m_antibindings[a_group].push_back(a_logname);

	m_bindmap[a_group].clear(); //* ensure existance

	for(std::map<String, std::vector<t_logptr> >::const_iterator
		i_bind = m_bindmap.begin(); i_bind != m_bindmap.end(); i_bind++)
	{
		wiregroup(i_bind->first);
	}
}

void Logger::clearAll(void)
{
	m_bindings.clear();
	m_antibindings.clear();
	m_logs.clear();
	m_bindmap.clear();
}

void Logger::clear(const String &a_group)
{
//X	m_bindings[a_group].clear();
	m_bindings.erase(a_group);
	m_antibindings.erase(a_group);

	Regex regex(a_group.c_str());

	for(std::map<String, std::vector<t_logptr> >::iterator
		i_bind = m_bindmap.begin(); i_bind != m_bindmap.end(); )
	{
		std::string s(i_bind->first.c_str());

		if(regex.search(s.c_str()))
		{
			std::map<String, std::vector<t_logptr> >::iterator i_erase =
				i_bind;
			i_bind++;
			m_bindmap.erase(i_erase);
		}
		else
		{
			i_bind++;
		}
	}

	for(std::map<String, std::vector<String> >::iterator
		i_binding = m_bindings.begin(); i_binding != m_bindings.end(); )
	{
		std::string s(i_binding->first.c_str());

		if(regex.search(s.c_str()))
		{
			std::map<String, std::vector<String> >::iterator i_erase =
				i_binding;
			i_binding++;
			m_bindings.erase(i_erase);
		}
		else
		{
			i_binding++;
		}
	}

	for(std::map<String, std::vector<String> >::iterator
		i_binding = m_antibindings.begin(); i_binding != m_antibindings.end(); )
	{
		std::string s(i_binding->first.c_str());

		if(regex.search(s.c_str()))
		{
			std::map<String, std::vector<String> >::iterator i_erase =
				i_binding;
			i_binding++;
			m_antibindings.erase(i_erase);
		}
		else
		{
			i_binding++;
		}
	}
}

void Logger::setLog(const String &a_logname, t_logptr a_log)
{
	m_logs[a_logname] = a_log;
}

void Logger::wiregroup(const String &a_group)
{
	//* TODO can probably omit regex patterns from the bindmap

	m_bindmap[a_group].clear();

	std::set<String> logset;
	for(std::map<String, std::vector<String> >::const_iterator
		i_binding = m_bindings.begin();
		i_binding != m_bindings.end(); i_binding++)
	{
		const char* pattern=i_binding->first.c_str();
		if(!strcmp(pattern,".*") || Regex(pattern).search(a_group.c_str()))
		{
			for(unsigned int i = 0; i < i_binding->second.size(); i++)
			{
				std::map<String, t_logptr>::const_iterator i_log =
						m_logs.find(i_binding->second[i]);
				if(i_log == m_logs.end())
				{
					continue;
				}

				logset.insert(i_log->first);
			}
		}
	}

	for(std::map<String, std::vector<String> >::const_iterator
		i_binding = m_antibindings.begin();
		i_binding != m_antibindings.end(); i_binding++)
	{
		const char* pattern=i_binding->first.c_str();
		if(!strcmp(pattern,".*") || Regex(pattern).search(a_group.c_str()))
		{
			for(unsigned int i = 0; i < i_binding->second.size(); i++)
			{
				std::map<String, t_logptr>::const_iterator i_log =
						m_logs.find(i_binding->second[i]);
				if(i_log == m_logs.end())
				{
					continue;
				}

				logset.erase(i_log->first);
			}
		}
	}

	for(std::set<String>::iterator iLS = logset.begin();
		iLS != logset.end(); iLS++)
	{
		m_bindmap[a_group].push_back(m_logs[*iLS]);
	}
}

} /* namespace */

FE_DL_PUBLIC void feLogDirect(const char *format, ...)
{
	std::string output;
	va_list ap;
	int size=0;
	while(size>=0)
	{
		va_start(ap, format);
		fe::vsPrintf(output,format,ap,size);
		va_end(ap);
	}

#if FE_USE_PRINTF
	fe_fprintf(stdout, "%s", output.c_str());
#else
	if(fe::gs_pPrintFunction)
	{
		fe::gs_pPrintFunction(output.c_str());
	}
#endif
}

FE_DL_PUBLIC void feLogError(const char *format, ...)
{
	std::string output;
	va_list ap;
	int size=0;
	while(size>=0)
	{
		va_start(ap, format);
		fe::vsPrintf(output,format,ap,size);
		va_end(ap);
	}

#if FE_USE_PRINTF
	fe_fprintf(stderr, "%s", output.c_str());
	fflush(stderr);
#else
	if(fe::gs_pPrintFunction)
	{
		fe::gs_pPrintFunction(output.c_str());
	}
#endif
}

//fe::Logger feLogger;

//fe::Logger& feLogger = fe::debugLogger;

