/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

// CORAL

#include <platform/platform.pmh>

namespace fe
{


DualString::DualString(void)
{
	m_pRep=NULL;
}

void DualString::init(void)
{
	m_pRep=newRep();
	m_pRep->m_pBuffer8=NULL;
	m_pRep->m_pBufferWide=NULL;
	m_pRep->m_stale8=true;
	m_pRep->m_staleWide=true;
}

DualString::DualString(const DualString &operand)
{
	m_pRep=NULL;
	operator=(operand);
}

DualString::DualString(const FESTRING_I8 *operand)
{
	if(!operand)
	{
		init();
		return;
	}
	const I32 bytes=FESTRLEN(operand)+1;
	m_pRep=newRep();
	m_pRep->newBuffer(bytes);
	FESTRCPY(m_pRep->m_pBuffer8,bytes,operand);
	m_pRep->m_stale8=false;
	m_pRep->m_staleWide=true;
}

DualString::DualString(const FESTRING_U8 *operand)
{
	if(!operand)
	{
		init();
		return;
	}
	const I32 bytes=FESTRLEN(operand)+1;
	m_pRep=newRep();
	m_pRep->newBuffer(bytes);
	FESTRCPY(m_pRep->m_pBuffer8,bytes,operand);
	m_pRep->m_stale8=false;
	m_pRep->m_staleWide=true;
}

DualString::DualString(const FESTRING_WIDE *operand)
{
	if(!operand)
	{
		init();
		return;
	}
	m_pRep=newRep();
	U32 len=strlenWide(operand)+1;
	m_pRep->newBuffer(len);
	memcpy(m_pRep->m_pBufferWide,operand,len*FESTRING_WIDE_SIZE);
	m_pRep->m_stale8=true;
	m_pRep->m_staleWide=false;
}

DualString::DualString(I32 operand)
{
	m_pRep=newRep();
	U32 len=12;
	m_pRep->newBuffer(len);
	FE_SNPRINTF((char *)m_pRep->m_pBuffer8,len,"%d",operand);
	m_pRep->m_stale8=false;
	m_pRep->m_staleWide=true;
}

DualString::~DualString(void)
{
	if(m_pRep && --m_pRep->m_references == 0)
	{
#if FESTRING_DEBUG
		confirm8();
		fe_fprintf(stderr,"~DualString() destroy %s\n",m_pRep->m_pBuffer8);
#endif
		deleteRep();
	}
}

void DualString::Rep::newBuffer(U32 size)
{
	//* for U8 buffer, allocate double the chars for largest possible multibyte

	if(gs_pAllocateFunction)
	{
		m_pBuffer8=(FESTRING_U8 *)allocate(2*size*sizeof(FESTRING_U8));
		m_pBufferWide=(FESTRING_WIDE *)allocate(size*sizeof(FESTRING_WIDE));
	}
	else
	{
		FEASSERT(0);
		m_pBuffer8=(FESTRING_U8 *)malloc(2*size*sizeof(FESTRING_U8));
		m_pBufferWide=(FESTRING_WIDE *)malloc(size*sizeof(FESTRING_WIDE));
	}

	m_stale8=true;
	m_staleWide=true;
}

void DualString::Rep::deleteBuffer(void)
{
	deallocateFunction *pDeallocateFunction=
			gs_pAllocateFunction? gs_pDeallocateFunction: free;

	FEASSERT(pDeallocateFunction);
	if (m_pBuffer8)
	{
		pDeallocateFunction(m_pBuffer8);
		m_pBuffer8 = NULL;
	}
	if (m_pBufferWide)
	{
		pDeallocateFunction(m_pBufferWide);
		m_pBufferWide = NULL;
	}
}

DualString::Rep *DualString::newRep(void)
{
	Rep *pRep=NULL;

	if(gs_pAllocateFunction)
	{
		pRep=(Rep *)allocate(sizeof(Rep));
	}
	else
	{
		FEASSERT(0);
		pRep=(Rep *)malloc(sizeof(Rep));
	}

	pRep->m_references=1;

	return pRep;
}

void DualString::deleteRep(void)
{
	deallocateFunction *pDeallocateFunction=
			gs_pAllocateFunction? gs_pDeallocateFunction: free;

	m_pRep->deleteBuffer();

	FEASSERT(pDeallocateFunction);
	pDeallocateFunction(m_pRep);

	m_pRep=NULL;
}

DualString &DualString::operator=(const FESTRING_U8 *operand)
{
	if(m_pRep)
	{
		// still in use elsewhere
		if(m_pRep->m_references>1)
		{
			// abandon old m_pRep (other ref will clean)
			m_pRep->m_references--;
			m_pRep=newRep();
		}
		else
		{
#if FESTRING_DEBUG
			confirm8();
			fe_fprintf(stderr,"DualString::operator= destroy %s\n",
					m_pRep->m_pBuffer8);
#endif
			m_pRep->deleteBuffer();
		}
	}
	else
		init();

	if(!operand)
	{
		deleteRep();
		return *this;
	}
	const I32 bytes=FESTRLEN(operand)+1;
	m_pRep->newBuffer(bytes);
	FESTRCPY(m_pRep->m_pBuffer8,bytes,operand);
	m_pRep->m_stale8=false;
	m_pRep->m_staleWide=true;
	return *this;
}

DualString &DualString::operator=(const FESTRING_WIDE *operand)
{
	if(m_pRep)
	{
		// still in use elsewhere
		if(m_pRep->m_references>1)
		{
			// abandon old m_pRep (other ref will clean)
			m_pRep->m_references--;
			m_pRep=newRep();
		}
		else
		{
#if FESTRING_DEBUG
			confirm8();
			fe_fprintf(stderr,"DualString::operator= destroy %s\n",
					m_pRep->m_pBuffer8);
#endif
			m_pRep->deleteBuffer();
		}
	}
	else
		init();

	if(!operand)
	{
		deleteRep();
		return *this;
	}
	U32 len=strlenWide(operand)+1;
	m_pRep->newBuffer(len);
	memcpy(m_pRep->m_pBufferWide,operand,len*FESTRING_WIDE_SIZE);
	m_pRep->m_stale8=true;
	m_pRep->m_staleWide=false;
	return *this;
}

DualString &DualString::operator=(const DualString &operand)
{
	// protect against string=string
	if(operand.m_pRep)
		operand.m_pRep->m_references++;

	if(m_pRep && --m_pRep->m_references == 0)
	{
#if FESTRING_DEBUG
		confirm8();
		fe_fprintf(stderr,"DualString::operator= destroy %s\n",
					m_pRep->m_pBuffer8);
#endif
		deleteRep();
	}

	m_pRep=operand.m_pRep;

	return *this;
}

U32 DualString::strlenWide(const FESTRING_WIDE *buffer)
{
	U32 m=0;
	if(buffer)
		while(buffer[m])
			m++;
	return m;
}

U32 DualString::length(void) const
{
	//* TODO fast version for multibyte

	if(m_pRep)
	{
		if(m_pRep->m_staleWide)
			((DualString*)this)->confirmWide();

		if(!m_pRep->m_staleWide)
			return strlenWide(m_pRep->m_pBufferWide);
	}

	return 0;
}

DualString DualString::copy(void) const
{
	DualString newDualString;
	newDualString.m_pRep=newDualString.newRep();

	U32 len=length()+1;
	newDualString.m_pRep->newBuffer(len);
	FESTRCPY(newDualString.m_pRep->m_pBuffer8,len,m_pRep->m_pBuffer8);
	memcpy(newDualString.m_pRep->m_pBufferWide,m_pRep->m_pBufferWide,
			len*FESTRING_WIDE_SIZE);

	newDualString.m_pRep->m_stale8=m_pRep->m_stale8;
	newDualString.m_pRep->m_staleWide=m_pRep->m_staleWide;
	return newDualString;
}

const FESTRING_U8 *DualString::rawU8(void) const
{
	if(!m_pRep)
		return (const FESTRING_U8 *)"";

	//* subvert const
	((DualString *)this)->confirm8();
	return m_pRep->m_pBuffer8;
}

const FESTRING_WIDE *DualString::rawWide(void) const
{
	if(!m_pRep)
		return NULL;

	//* subvert const
	((DualString *)this)->confirmWide();
	return m_pRep->m_pBufferWide;
}

void DualString::confirm8(void)
{
	if(!m_pRep)
		return;

	//* JIT conversion
	if(m_pRep->m_stale8)
	{
		if(m_pRep->m_staleWide)
			(*this)="";
		else
		{
			FESTR_WC2MB(m_pRep->m_pBuffer8,m_pRep->m_pBufferWide,
										strlenWide(m_pRep->m_pBufferWide)+1);
			m_pRep->m_stale8=false;
		}
	}
}

void DualString::confirmWide(void)
{
	if(!m_pRep)
		return;

	//* JIT conversion
	if(m_pRep->m_staleWide)
	{
		if(m_pRep->m_stale8)
			(*this)="";

		FEASSERT(!m_pRep->m_stale8);

		U32 len=FESTRLEN(m_pRep->m_pBuffer8);
		FESTR_MB2WC(m_pRep->m_pBufferWide,m_pRep->m_pBuffer8,len);
		m_pRep->m_pBufferWide[len]=0;
		m_pRep->m_staleWide=false;
	}
}

I32 DualString::compare(const DualString &operand) const
{
	if(!m_pRep)
		return operand.m_pRep? 1: 0;

	if(m_pRep->m_staleWide && operand.m_pRep->m_staleWide)
		return FESTRCMP_MB(rawU8(),operand.rawU8());

	//* subvert const
	((DualString)*this).confirmWide();
	((DualString)operand).confirmWide();

	U32 len1=DualString::strlenWide(rawWide());
	U32 len2=DualString::strlenWide(operand.rawWide());

	if(len1<len2)
		return (memcmp(rawWide(),operand.rawWide(),
										len1*FESTRING_WIDE_SIZE)>0)? 1:-1;
	if(len2<len1)
		return (memcmp(rawWide(),operand.rawWide(),
										len2*FESTRING_WIDE_SIZE)<0)? -1:1;

	return memcmp(rawWide(),operand.rawWide(),len1*FESTRING_WIDE_SIZE);
}

//* TODO FESTRING_WIDE
BWORD DualString::dotMatch(const DualString &operand) const
{
//	fe_fprintf(stderr,"dotMatch() \"%s\" \"%s\"\n",rawU8(),operand.rawU8());

	if(!m_pRep || !operand.m_pRep)
		return false;

	const_cast<DualString*>(this)->confirm8();
	const_cast<DualString*>(&operand)->confirm8();

	const FESTRING_U8* str1=rawU8();
	const FESTRING_U8* str2=operand.rawU8();

	const U32 olen1=length();
	const U32 olen2=operand.length();

	U32 len1=olen1;
	U32 len2=olen2;
	while(str2[0]=='*' && str2[1]=='.')
	{
		const FESTRING_U8* str;
		if(!(str=(const FESTRING_U8*)memchr(str1,'.',len1)))
			return false;
		str++;
		str2+=2;
		len1-=str-str1;
		len2-=2;
		str1=str;
		FEASSERT(str1<rawU8()+olen1);
		FEASSERT(str2<operand.rawU8()+olen2);

//		fe_fprintf(stderr,"  rematch \"%s\" \"%s\"\n",str1,str2);
	}

	if(FESTRLEN(str1)>=len2 && (str1[len2]==0 || str1[len2]=='.') &&
			!FESTRNCMP_MB(str1,str2,len2))
		return true;

	return false;
}

// TODO FESTRING_WIDE
DualString &DualString::sPrintf(const char* fmt, ...)
{
	va_list ap;
	va_start(ap, fmt);

	vsPrintf(fmt,ap);

	va_end(ap);

	return *this;
}

// Brute force.  Rewrite if necessary for speed or space.
// TODO FESTRING_WIDE
DualString &DualString::catf(const char* fmt, ...)
{
	va_list ap;
	va_start(ap, fmt);

	DualString tmp;
	tmp.vsPrintf(fmt,ap);

	va_end(ap);

	DualString previous(*this);
	sPrintf("%s%s",previous.c_str(),tmp.c_str());

	return *this;
}

// TODO make work w/o depending on c_str
// TODO << operator
DualString &DualString::cat(std::list<DualString> &strList, DualString sep)
{
	for(std::list<DualString>::iterator it = strList.begin();
		it != strList.end(); it++)
	{
		if(it != strList.begin())
		{
			catf("%s", sep.c_str());
		}
		catf("%s", it->c_str());
	}
	return *this;
}

// Brute force.  Rewrite if necessary for speed.
// TODO FESTRING_WIDE
DualString &DualString::vsPrintf(const char* fmt, va_list ap)
{
	// start at 80 bytes, seems reasonable (1 tty line)
	int sz = 80;
	FESTRING_U8 *tmpbuf = new FESTRING_U8[sz];
	FEASSERT(NULL != tmpbuf);
	while(true)
	{
		int n=FEVSNPRINTF((char *)tmpbuf, sz, fmt, ap);

		if(n> -1 && n<sz)
			break;
		if(n> -1)
			sz=n+1;
		else
			sz*=2;

		delete [] tmpbuf;
		tmpbuf = new FESTRING_U8[sz];
		FEASSERT(NULL != tmpbuf);

		// NOTE calling vsnprintf again without va_end,va_start is undefined
		feX("String::vsPrintf warning, expanding buffer\n");
	}
	operator=(tmpbuf);
	delete [] tmpbuf;

	return *this;
}

void DualString::output(std::ostream &ostrm) const
{
	U32 len = strlen(c_str());
	U32 nlen = htonl(len);
	ostrm.write((char *)&nlen, 4);
	ostrm.write(c_str(), len);
}

void DualString::input(std::istream &istrm)
{
	U32 len;
	istrm.read((char *)&len, 4);
	len = ntohl(len);

	// TODO remove this extra memory buffer.  input directly into c_str buffer
	char *buffer = new char[len + 1];
	istrm.read(buffer, len);
	buffer[len] = 0;
	operator=(buffer);
	delete[] buffer;
}

//* TODO optimize array usage
void DualString::forceCase(BWORD upper)
{
	if(!m_pRep)
		return;

	DualString temp=*this;
	if(!m_pRep->m_stale8)
	{
		*this=(const FESTRING_U8 *)temp;	// replicate

#ifdef FESTR_MB2UPPER
		if(upper)
			FESTR_MB2UPPER(m_pRep->m_pBuffer8);
		else
			FESTR_MB2LOWER(m_pRep->m_pBuffer8);
#else
		//* manual 8-bit version
		U32 m,len=FESTRLEN(m_pRep->m_pBuffer8);
		for(m=0;m<len;m++)
//          if(m_pRep->m_pBuffer8[m] < 0x7f)
				m_pRep->m_pBuffer8[m]=upper?
						toupper(m_pRep->m_pBuffer8[m]): tolower(
														m_pRep->m_pBuffer8[m]);
#endif
	}
	else if(!m_pRep->m_staleWide)
	{
		*this=(const FESTRING_WIDE *)temp;	// replicate

#ifdef FESTR_WC2UPPER
		if(upper)
			FESTR_WC2UPPER(m_pRep->m_pBufferWide);
		else
			FESTR_WC2LOWER(m_pRep->m_pBufferWide);
#else
		//* manual Wide version
		U32 m,len=strlenWide(m_pRep->m_pBufferWide);
		for(m=0;m<len;m++)
			if(m_pRep->m_pBufferWide[m] < 0x7f)
				m_pRep->m_pBufferWide[m]=upper?
						toupper(m_pRep->m_pBufferWide[m]):
						tolower(m_pRep->m_pBufferWide[m]);
#endif
	}
	else
		(*this)="";
}

DualString errorDualString(int error)
{
	DualString message;
#if FE_OS==FE_WIN32 || FE_OS==FE_WIN64
	// from MSDN example
	LPVOID lpMsgBuf;
	if (!FormatMessage(
			FORMAT_MESSAGE_ALLOCATE_BUFFER |
			FORMAT_MESSAGE_FROM_SYSTEM |
			FORMAT_MESSAGE_IGNORE_INSERTS,
			NULL,
			error,
			MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), // Default language
			(LPTSTR) &lpMsgBuf,
			0,
			NULL ))
		return "no error string";

	message = (char *)lpMsgBuf;

	LocalFree( lpMsgBuf );
#else
	message = strerror(error);
#endif
	return message;
}

} /* namespace */
