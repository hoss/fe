/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __platform_define_h__
#define __platform_define_h__

#ifndef FALSE
#define FALSE				0
#endif
#ifndef TRUE
#define TRUE				1
#endif

//* FE_CODEGEN
#define FE_VALIDATE			1
#define FE_DEBUG			2
#define FE_PROFILE			3
#define FE_OPTIMIZE			4
#define FE_PURIFY			5

//* FE_OS
#define FE_LINUX			1
#define FE_WIN32			2
#define FE_OSX				3
#define FE_CYGWIN			4
#define FE_WIN64			5
#define FE_FREEBSD			6

//* FE_OSVER
/* use the primary version number */

//* FE_COMPILER
#define FE_MICROSOFT		1
#define FE_ANSI				2
#define FE_GNU				3
#define FE_KR				4
#define FE_INTEL			5
#define FE_DMC				6

//* FE_HW
#define FE_X86				1
#define FE_PPC				2
#define FE_68K				3
#define FE_AXP				4
#define FE_MIPS				5
#define FE_SPARC			6
#define FE_HPPA				7

//* FE_ENDIAN
#define FE_BIG				1
#define FE_LITTLE			2

//* FE_WL
#define FE_WL_WIN32			1
#define FE_WL_XWIN			2

//* FE_2DGL
#define FE_2D_OPENGL		1
#define FE_2D_DIRECTX		2
#define FE_2D_GDI			3
#define FE_2D_X_GFX			4
#define FE_2D_OSX			5

//* FE_3DGL
#define FE_3D_OPENGL		1
#define FE_3D_DIRECT3D		2

#if FE_HW==FE_X86 || FE_HW==FE_AXP
#define FE_ENDIAN			FE_LITTLE
#else
#define FE_ENDIAN			FE_BIG
#endif

//* FE_RE
//#define FE_RE_BOOST		1
//#define FE_RE_PCRE		2

#endif /* __platform_define_h__ */

