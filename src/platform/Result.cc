/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <platform/platform.pmh>

namespace fe
{

const char *resultString(Result result)
{
	switch(result)
	{
		case e_ok:
			return "ok";
		case e_cancel:
			return "cancel";
		case e_poison:
			return "poison";
		case e_alreadyAvailable:
			return "already available";
		case e_notNeeded:
			return "not needed";

		case e_undefinedFailure:
			return "undefined failure";
		case e_unsupported:
			return "unsupported";
		case e_refused:
			return "refused";
		case e_outOfMemory:
			return "out of memory";
		case e_invalidFile:
			return "invalid file";
		case e_invalidHandle:
			return "invalid handle";
		case e_invalidPointer:
			return "invalid pointer";
		case e_invalidRange:
			return "invalid range";
		case e_notInitialized:
			return "not initialized";
		case e_cannotChange:
			return "cannot change";
		case e_aborted:
			return "aborted";
		case e_writeFailed:
			return "write failed";
		case e_readFailed:
			return "read failed";
		case e_cannotFind:
			return "cannot find";
		case e_cannotCreate:
			return "cannot create";
		case e_unsolvable:
			return "calculation can not be solved";
		case e_impossible:
			return "reached what is supposed to be impossible state";
		case e_usage:
			return "usage error";
		case e_system:
			return "failed system call";
		case e_typeMismatch:
			return "type mismatch";
		case e_corrupt:
			return "corrupt data";
		default:
			return "result with no description";
	};
}

};

