/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */


#ifndef __platform_Exception_h__
#define __platform_Exception_h__

// TODO: the use of Statics here is not safe.  Try to find a better way.

#if FE_CODEGEN>FE_DEBUG
#define feX throw ::fe::Exception
// funkiness to get __FILE__ and __LINE__ with varargs
#elif FE_COMPILER==FE_GNU
#define feX ::fe::Exception::stage(__FILE__,__LINE__,__PRETTY_FUNCTION__), throw ::fe::Exception
#elif FE_COMPILER==FE_MICROSOFT
#define feX ::fe::Exception::stage(__FILE__,__LINE__,__FUNCTION__), throw ::fe::Exception
#else
#define feX ::fe::Exception::stage(__FILE__,__LINE__,__func__), throw ::fe::Exception
#endif

#define feAssert(cond,msg) {if(!(cond)){feX(msg);}}

namespace fe
{

/**************************************************************************//**
	@brief Generic exception carrying a fe::String payload

	@ingroup platform
*//***************************************************************************/
class FE_DL_PUBLIC FE_DL_EXPORT Exception: public std::exception
{
	public:
						Exception(void) throw();
						Exception(const char *fmt, ...) throw();
						Exception(const char *location,
								const char *fmt, ...) throw();
						Exception(const Result &result,
								const char *fmt, ...) throw();
						Exception(const Result &result, const char *location,
								const char *fmt, ...) throw();
						Exception(const Result &result, const char *location,
								String fmt, ...) throw();
						Exception(const Result &result) throw();
						Exception(const Exception &other) throw();

virtual					~Exception(void) throw();

						//* as std::exception
virtual
const	char*			what(void) const throw()
						{	return m_message.c_str(); }

		Exception		&operator=(const Exception &other) throw();

						/// @brief Return the fe::String payload
const	String			&getMessage(void) const throw()
						{	return m_message; }

const	String			&getLocation(void) const throw()
						{	return m_location; }

const	String			&getFile(void) const throw()
						{	return m_file; }

const	String			&getFunction(void) const throw()
						{	return m_function; }

const	String			&getStack(void) const throw()
						{	return m_stack; }

		Result			getResult(void) const throw()
						{	return m_result; }

		int				getLine(void) const throw()
						{	return m_line; }

						/// @brief Send the message to the global log
		void			log(void) throw();

						/** @brief Assert instead of except,
							if selected

							If the env variable "FE_E_ASSERT" is set,
							exceptions cause an assertion when they create.
							They don't have to be thrown first.

							@internal */
virtual	void			checkAssert(void) throw();

						/**	Not thread safe, so exceptions thrown from
							multiple threads are in a race condition on
							file and line.
							*/
static	void			stage(const char *file, int line, const char* function)
						{
							//ms_file = file;
							//ms_line = line;
							//ms_function = function;
						}


	protected:
		String			m_message;
		String			m_location;
		Result			m_result;
		String			m_file;
		int				m_line;
		String			m_function;
		String			m_stack;
static	String			ms_file;
static	int				ms_line;
static	String			ms_function;

};

FE_DL_PUBLIC String print(const Exception& a_rException);

} /* namespace */

#endif /* __platform_Exception_h__ */
