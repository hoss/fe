/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __platform_debug_h__
#define __platform_debug_h__

namespace fe
{

FE_MEM_PORT extern String	gs_logPrefix;
FE_MEM_PORT extern BWORD	gs_bannerPrinted;

/**************************************************************************//**
	@brief Format text into a fe::String
*//***************************************************************************/
void FE_DL_EXPORT vsPrintf(std::string &target, const char *fmt, va_list ap, int &size);

/**
	Base class for logging system Log objects
	*/
class FE_DL_PUBLIC Log
{
	public:
					Log(void);
virtual				~Log(void);

virtual		void	log(const std::string &a_message)						=0;
virtual		void	log(const std::string &a_message,
						std::map<std::string,std::string> &a_attributes)
					{
						log(a_message);
					}

			void	acquire(void)
					{
						RecursiveMutex::Guard guard(mutex());
						m_cnt++;
					}
			bool	release(void)
					{
						RecursiveMutex::Guard guard(mutex());
						m_cnt--;
						return (m_cnt > 0);
					}


#if FE_SAFE_COUNTED_MUTEX
virtual	RecursiveMutex& mutex(void) const
				{ return *ms_pMutex; }

	private:
static	FE_DL_PUBLIC	I32				ms_mutexCount;
static	FE_DL_PUBLIC	RecursiveMutex*	ms_pMutex;
#else
virtual	RecursiveMutex& mutex(void) const
				{ return ms_mutex; }
	private:
static	FE_DL_PUBLIC	RecursiveMutex	ms_mutex;
#endif

		int m_cnt;
};

FE_DL_PUBLIC void intrusive_ptr_add_ref(Log *a_log);
FE_DL_PUBLIC void intrusive_ptr_release(Log *a_log);

//typedef boost::intrusive_ptr<Log> t_logptr;
typedef Log *t_logptr;

class FE_DL_PUBLIC StdoutLog : public Log
{
	public:
					StdoutLog(void)											{}
virtual				~StdoutLog(void)										{}

virtual		void	log(const std::string &a_message,
						std::map<std::string,std::string> &a_attributes);
virtual		void	log(const std::string &a_message)
					{
						std::cout << a_message.c_str();
						std::cout.flush();
					}
};

class FE_DL_PUBLIC FileLog : public StdoutLog
{
	public:
					FileLog(void)											{}
virtual				~FileLog(void)											{}

					using StdoutLog::log;

virtual		void	log(const std::string &a_message,
						std::map<std::string,std::string> &a_attributes);

			void	setFilename(String a_filename);
			String	filename(void)					{ return m_filename; }

	protected:
			String			m_filename;
			std::ofstream	m_ofstream;
};

class FE_DL_PUBLIC AnnotatedLog : public StdoutLog
{
	public:
					AnnotatedLog(void): m_newline(true)						{}
virtual				~AnnotatedLog(void)										{}

					using StdoutLog::log;

virtual		void	log(const std::string &a_message,
						std::map<std::string,std::string> &a_attributes);
	protected:
			void	output(std::ostream& stream,const char* source,
							int size,bool right);
			bool	m_newline;
};

class FE_DL_PUBLIC PrefaceLog : public AnnotatedLog
{
	public:
					PrefaceLog(void);
virtual				~PrefaceLog(void)										{}

					using AnnotatedLog::log;

virtual		void	log(const std::string &a_message,
						std::map<std::string,std::string> &a_attributes);

virtual		String	preface(void)					{ return m_logPreface; }
virtual		void	setPreface(String a_preface)	{ m_logPreface=a_preface; }

	protected:
			BWORD	m_annotate;
			String	m_logPreface;
};

//* NOTE globally shared prefix
class FE_DL_PUBLIC PrefixLog : public PrefaceLog
{
	public:
					PrefixLog(void)											{}
virtual				~PrefixLog(void)										{}

					using PrefaceLog::log;

virtual		String	preface(void)				{ return gs_logPrefix; }

static		String	prefix(void)				{ return gs_logPrefix; }
			void	setPrefix(String a_prefix)	{ gs_logPrefix=a_prefix; }
};

class FE_DL_PUBLIC GroupLog : public AnnotatedLog
{
	public:
					GroupLog(void)											{}
virtual				~GroupLog(void)											{}

					using AnnotatedLog::log;

virtual		void	log(const std::string &a_message,
						std::map<std::string,std::string> &a_attributes);
};

class FE_DL_PUBLIC NullLog : public Log
{
	public:
					NullLog(void)											{}
virtual				~NullLog(void)											{}

					using Log::log;

virtual		void	log(const std::string &a_message)
					{
						/* NOOP */
					}
};

class FE_DL_PUBLIC Win32Log : public Log
{
	public:
					Win32Log(void)											{}
virtual				~Win32Log(void)											{}

					using Log::log;

virtual		void	log(const std::string &a_message)
					{
#if FE_OS==FE_WIN32 || FE_OS==FE_WIN64
						OutputDebugStringA(a_message.c_str());
#endif
					}
};

class Logger;

/**
@page logging Logging Guide

The logging system has these concepts:
- named groups
- named Log objects (Log objects output a message somewhere)
- group name to log name relationships (bindings)

With this structure logging groups can be flexibly configured without any
hardcoded enumerations via defines, enums, or whatever.

A Static Logger object, named feLogger, is intended to be used for most logging.
However, most logging calls will actually go through the feLog* macros.

@section basic_logging Basic Logging
@code
feLog("some logging message text\n");
@endcode

Note that the trailing newline is not automatic.

The group for a basic log message is the MODULE macro, if defined, and is
otherwise "default".  FE's build system automatically sets MODULE to be
the module name (such as "src.platform").

@section explicit_logging Logging to an Explicit Group

@code
feLogGroup("my group", "some logging message text\n");
@endcode

@section direct_logging Direct Logging

@code
feLogDirect("some logging message text\n");
@endcode

Direct logging incurs minimum overhead and processing, and may be independently
\#ifdef disabled from normal logging.  Direct logging has no group.

@section control_logging Controlling Output

@subsection naming_logs Setting Up Named Log Objects
@code
feLogger.setLog("stdout", new StdoutLog());
@endcode

@subsection binding_groups Binding Groups to Log Objects
@code
feLogger.bind("src\.platform", "stdout");
@endcode


Binding can use regular expressions, which is where a lot of the flexibility
lies.  Here is an example of binding all messages to stdout:
@code
feLogger.bind(".*", "stdout");
@endcode

Here is an example of binding all debug messages following a particular
naming convention:
@code
feLogger.bind(".*\.debug", "stdout");
@endcode

Clearing a binding is done with the clear method:
@code
feLogger.clear(".*")
@endcode

@section log_objects Log Objects

Log objects take the vsprinted message and a map of attributes.  File, line
number, etc, all come in as attributes.

@section log_defaults Default feLogger bindings
As with any binding, these can be cleared and reset, but as a default,
feLogger is initialized like this in its constructor:
@code
m_directLog = new StdoutLog();
setLog("stdout", m_directLog);
setLog("win32", new Win32Log());
bind(".*", "stdout");
bind(".*", "win32");
@endcode
*/

/**************************************************************************//**
	@brief C++ portion of logging interface implementation

*//***************************************************************************/
class FE_DL_PUBLIC Logger
{
	public:
					Logger(void);
virtual				~Logger(void);

//virtual		void	direct(const char *format, ...) const;
virtual		void	log(const String &a_group,
						std::map<std::string,std::string> &a_attributes,
						const char *a_format, ...);
virtual		void	bind(const String &a_group, const String &a_logname);
virtual		void	antibind(const String &a_group, const String &a_logname);
virtual		void	clear(const String &a_group);
virtual		void	clearAll(void);
virtual		void	setLog(const String &a_logname, t_logptr a_log);

virtual		void	dump(void);

//virtual		GroupLogger operator[](const String &a_group) const;

	private:
		void wiregroup(const String &a_group);
		std::map<String, std::vector<String> >		m_bindings;
		std::map<String, std::vector<String> >		m_antibindings;
		std::map<String, t_logptr>					m_logs;
		t_logptr									m_directLog;

		std::map<String, std::vector<t_logptr> >	m_bindmap;

#if FE_SAFE_COUNTED_MUTEX
virtual	RecursiveMutex& mutex(void) const
				{ return *ms_pMutex; }

	private:
static	FE_DL_PUBLIC	I32				ms_mutexCount;
static	FE_DL_PUBLIC	RecursiveMutex*	ms_pMutex;
#else
virtual	RecursiveMutex& mutex(void) const
				{ return ms_mutex; }
	private:
static	FE_DL_PUBLIC	RecursiveMutex	ms_mutex;
#endif

};


//extern Logger debugLogger;

} /* namespace */

// This does not seem to work properly in g++ 3 if it is in a namespace
//extern FE_DL_PUBLIC fe::Logger feLogger;

#ifndef MODULE
#define MODULE ""
#endif

//#define feLog(...) /* NOOP */
//#define feLogGroup(grp, ...) /* NOOP */

#if 1
#define feLog(...) {														\
	std::stringstream out;													\
	out << __LINE__;														\
	std::map<std::string,std::string> attributes;							\
	attributes["file"] = __FILE__;											\
	attributes["line"] = out.str().c_str();									\
	attributes["date"] = __DATE__;											\
	attributes["time"] = __TIME__;											\
	attributes["group"] = MODULE;											\
	feLogger()->log(MODULE, attributes, __VA_ARGS__);						\
	}

#define feLogGroup(grp, ...) {												\
	std::stringstream out;													\
	out << __LINE__;														\
	std::map<std::string,std::string> attributes;							\
	attributes["file"] = __FILE__;											\
	attributes["line"] = out.str().c_str();									\
	attributes["date"] = __DATE__;											\
	attributes["time"] = __TIME__;											\
	attributes["group"] = grp;												\
	feLogger()->log(grp, attributes, __VA_ARGS__);							\
	}
#else
#define feLog(...) { feLogDirect(__VA_ARGS__); }
#define feLogGroup(grp,...) { feLogDirect(__VA_ARGS__); }
#endif

//#define feLogDirect(...) feLogger.direct(__VA_ARGS__)
FE_DL_PUBLIC void feLogDirect(const char *format, ...);

FE_DL_PUBLIC void feLogError(const char *format, ...);

#endif /* __platform_debug_h__ */
