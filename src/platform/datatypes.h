/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __platform_datatypes_h__
#define __platform_datatypes_h__

// NO NAMESPACE

typedef long long int			I64;
typedef int						I32;
typedef short					I16;

//* NOTE Bink/Miles may already define these (using #define)
#ifndef U64
typedef unsigned long long int	U64;
#endif
#ifndef U32
typedef unsigned int			U32;
#endif
#ifndef F64
typedef unsigned short			U16;
typedef unsigned char			U8;
typedef signed char				I8;
typedef float					F32;
typedef double					F64;
#endif

#ifndef NULL
#define NULL	0
#endif

typedef bool			BWORD;
typedef char			CHAR;

//* UWORD is already defined in win32
typedef size_t			FE_UWORD;

#if FE_OS==FE_WIN32 || FE_OS==FE_WIN64
typedef SSIZE_T			IWORD;
#else
typedef ssize_t			IWORD;
#endif

#ifndef FE_BOOST_REGEX
#define FE_BOOST_REGEX	0
#endif

#ifndef FE_BOOST_FILESYSTEM
#define FE_BOOST_FILESYSTEM	0
#endif

#ifndef FE_USE_TSS
#define FE_USE_TSS	0
#endif

namespace fe
{
#if FE_DOUBLE_REAL
	typedef	double			Real;
#else
	typedef	float			Real;
#endif

	typedef Real					t_real;
	typedef std::vector<I32>		t_vecint;
	typedef std::vector<U32>		t_vecuint;
	typedef std::vector<t_real>		t_vecreal;
}

#endif /* __platform_datatypes_h__ */

