/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __platform_dl_loader_h__
#define __platform_dl_loader_h__

namespace fe
{

/**************************************************************************//**
	@brief Raw access to a dynamic library

	@ingroup platform
*//***************************************************************************/
class FE_DL_PUBLIC DL_Loader
{
	public:
					DL_Loader(void)
					{
#if FE_OS==FE_WIN32 || FE_OS==FE_WIN64
#else
						m_handle=NULL;
#endif
					}

					/** @brief Add a path search for managed
						dynamic libraries. */
static	Result		addPath(String a_absPath);

					/** @brief Open a dynamic library

						If adaptname is TRUE, the filename is adapted
						used using local convention
						(MyLib is interpreted as libMyLib.so or MyLib.lib). */
		BWORD		openDL(String a_filename,BWORD a_adaptname=TRUE);

					/// @brief Close a dynamic library
		BWORD		closeDL(void);

					/** @brief Find the named symbol in the open library

						Returns a pointer to the named function, if it exists.
						This may only work for functions wrapped in extern "C"
						and explicitly exported. */
		void		*findDLFunction(String a_symbol);

					/// @brief Returns the name of the library as it was opened
const	String&		name(void) const					{ return m_name; }

	private:

		String		adaptName(String a_filename,
							String a_prefix,String a_format);

#if FE_OS==FE_WIN32 || FE_OS==FE_WIN64
		HINSTANCE		m_handle;
#else
		void			*m_handle;

static	FE_DL_PUBLIC	std::vector<String>	ms_pathArray;
#endif

		String			m_name;
};

} // namespace

#endif // __platform_dl_loader_h__
