/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <platform/platform.pmh>

#define FE_REGEX_DEBUG	FALSE
#define FE_REGEX_STD	(FE_CPLUSPLUS >= 201103L)

#if FE_REGEX_STD
#include <regex>
#endif

namespace fe
{

Regex::Regex(const char* a_pattern)
{
	confirm();

	m_pResult=NULL;

	if(gs_fnRegexInit)
	{
		m_pExpression=gs_fnRegexInit(a_pattern);
		return;
	}

	m_pExpression=strdup(a_pattern);
}

Regex::~Regex(void)
{
	if(m_pResult)
	{
		if(gs_fnRegexRelease)
		{
			gs_fnRegexRelease(m_pResult);
		}
		m_pResult=NULL;
	}
	if(m_pExpression)
	{
		if(gs_fnRegexFinish)
		{
			gs_fnRegexFinish(m_pExpression);
		}
		m_pExpression=NULL;
	}
}

BWORD Regex::match(const char* a_candidate)
{
	if(m_pResult)
	{
		FEASSERT(gs_fnRegexRelease);
		gs_fnRegexRelease(m_pResult);
		m_pResult=NULL;
	}

	if(gs_fnRegexMatch)
	{
		m_pResult=gs_fnRegexMatch(m_pExpression,a_candidate);
		return m_pResult!=NULL;
	}

	const char* pattern=(const char*)m_pExpression;

#if FE_REGEX_DEBUG
	fe_fprintf(stderr,"Regex::match \"%s\" with \"%s\"\n",pattern,a_candidate);
#endif

#if FE_REGEX_STD

	const std::regex regex(pattern);
	if (!std::regex_match(a_candidate,regex))
	{
#if FE_REGEX_DEBUG
		fe_fprintf(stderr,"Regex::match no match\n");
#endif

		return false;
	}

#if FE_REGEX_DEBUG
	fe_fprintf(stderr,"Regex::match matched\n");
#endif

	return true;

#else

#if FE_REGEX_DEBUG
	fe_fprintf(stderr,"Regex::match C++11 required for default regex\n");
#endif
	return false;

#endif
}

BWORD Regex::search(const char* a_candidate)
{
	if(m_pResult)
	{
		FEASSERT(gs_fnRegexRelease);
		gs_fnRegexRelease(m_pResult);
		m_pResult=NULL;
	}

	if(gs_fnRegexSearch)
	{
		m_pResult=gs_fnRegexSearch(m_pExpression,a_candidate);
		return m_pResult!=NULL;
	}

	return match(a_candidate);
}

const char* Regex::replace(const char* a_candidate,const char* a_replacement)
{
	if(m_pResult)
	{
		FEASSERT(gs_fnRegexRelease);
		gs_fnRegexRelease(m_pResult);
		m_pResult=NULL;
	}

	if(gs_fnRegexReplace)
	{
		m_pResult=gs_fnRegexReplace(m_pExpression,a_candidate,a_replacement);
		if(m_pResult)
		{
			return result(0);
		}
	}

	return a_candidate;
}

const char* Regex::result(U32 a_index)
{
	if(gs_fnRegexSearch)
	{
		FEASSERT(gs_fnRegexResult);
		return gs_fnRegexResult(m_pResult,a_index);
	}

	return "";
}

//* static
BWORD Regex::confirm(String a_hint)
{
//	fe_fprintf(stderr,"Regex::confirm \"%s\"\n",a_hint.c_str());

	if(gs_fnRegexInit)
	{
		return TRUE;
	}
	if(gs_regexChecked)
	{
		return FALSE;
	}
	gs_regexChecked=TRUE;

	const String verbose=System::getVerbose();

	System::printBannerOnce();

	DL_Loader *pLoader=new DL_Loader();
	FEASSERT(pLoader);

	const BWORD adaptname=TRUE;

	String nameOrder=FE_STRING(FE_REGEX_ORDER);

	if(!a_hint.empty())
	{
		nameOrder=a_hint+":"+nameOrder;
	}

	String name;
	while(!(name=nameOrder.parse("\"",":")).empty())
	{
		if(!pLoader->openDL(name,adaptname))
		{
			continue;
		}

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wconditionally-supported"

		bool (*regex_init)(void);
		regex_init=(bool (*)(void))pLoader->findDLFunction("regex_init");

#pragma GCC diagnostic pop

		if(!regex_init)
		{
			if(verbose!="none")
			{
				fe_fprintf(stderr,"%sregex_init not found in \"%s\"\n",
						PrefixLog::prefix().c_str(),name.c_str());
			}

			pLoader->closeDL();
			continue;
		}

		if(regex_init())
		{
			if(verbose=="all")
			{
				fe_printf("%susing %s (for regular expressions)\n",
						PrefixLog::prefix().c_str(),name.c_str());
			}

			delete pLoader;
			return TRUE;

			//* TODO retain, close, and delete pLoader later
		}
	}

	if(verbose!="none")
	{
		fe_printf("%susing simplified regex\n",PrefixLog::prefix().c_str());
	}

	delete pLoader;
	return FALSE;
}

} /* namespace */

