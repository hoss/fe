/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "fe/platform.h"

int main(void)
{
	fe::UnitTest unitTest;
	unitTest(true, "unitTest test");

	feLogDirect("feLogDirect test int: %d string %s\n",1,"one");
	unitTest(true, "feLogDirect test");

	fe::String s = "test string";
	s = "";
	s.sPrintf("%s\n", "sdlhf");

	fe::String s2("another string");
	s = s2;

	fe::String s3(s);

	s3 = s + s2;

	I32 f[2];
	I32 g[2];
	I32 h;
	I32 i;
	feLogDirect("%p %p %p\n",&f[0],&f[1],
			(unsigned long long)(&f[1])-(unsigned long long)(&f[0]));
	feLogDirect("%p %p %p\n",&g[0],&g[1],
			(unsigned long long)(&g[1])-(unsigned long long)(&g[0]));
	feLogDirect("%p %p %p\n",&h,&i,
			(unsigned long long)(&h)-(unsigned long long)(&i));

	const fe::String exePath=fe::System::getExePath(TRUE);
	feLogDirect("exePath \"%s\"\n",exePath.c_str());

	return unitTest.failures();
}

