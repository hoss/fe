/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "fe/platform.h"

using namespace fe;

int main(void)
{
	UNIT_START();

	BWORD completed=FALSE;

	try
	{
#if _WIN64
		feLogDirect("_WIN64 set\n");
#endif
#if _WIN32
		feLogDirect("_WIN32 set\n");
#endif
#if _WIN16
		feLogDirect("_WIN16 set\n");
#endif
#if _M_IA64
		feLogDirect("_M_IA64 set\n");
#endif
#if _M_IX86
		feLogDirect("_M_IX86 set\n");
#endif
#if _M_X64
		feLogDirect("_M_X64 set\n");
#endif

		feLogDirect("char\n");
		int bytes=sizeof(char);
		feLogDirect("%d\n",bytes);

		feLogDirect("short\n");
		bytes=sizeof(short);
		feLogDirect("%d\n",bytes);

		feLogDirect("int\n");
		bytes=sizeof(int);
		feLogDirect("%d\n",bytes);

		feLogDirect("long\n");
		bytes=sizeof(long);
		feLogDirect("%d\n",bytes);

		feLogDirect("long*\n");
		bytes=sizeof(long*);
		feLogDirect("%d\n",bytes);

		feLogDirect("float\n");
		bytes=sizeof(float);
		feLogDirect("%d\n",bytes);

		feLogDirect("double\n");
		bytes=sizeof(double);
		feLogDirect("%d\n",bytes);

		completed=TRUE;
	}
	catch(Exception &e)	{ e.log(); }
	catch(...)			{ feLogDirect("uncaught exception\n"); }

	UNIT_TEST(completed);
	UNIT_TRACK(1);
	UNIT_RETURN();
}
