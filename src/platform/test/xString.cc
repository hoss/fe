/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "fe/platform.h"

using namespace fe;

int main(void)
{
	UNIT_START();

	BWORD completed=FALSE;

	try
	{
		feLogDirect("\n");
		String message("Start\n");
		for(U32 m=0;m<8;m++)
		{
			String copy(message);
			message.sPrintf(
					"%sTesting    Another    Verbose    Text    Line %d\n",
					copy.c_str(),m);
		}
		feLogDirect("length=%d string:\n%s",message.length(),message.c_str());

		UNIT_TEST(String("ShortString")==String("ShortString"));
		UNIT_TEST(String("ShortString")!=String("short_string"));
		UNIT_TEST(String("ShortString")!=String("shortString"));
		UNIT_TEST(String("AMuchLongerString")==String("AMuchLongerString"));
		UNIT_TEST(String("AMuchLongerString")!=String("a_much_longer_string"));

		feLogDirect("\n");
		String shortCased="AbcDeF";
		feLogDirect("%s\n",shortCased.c_str());
		String shortUpper=shortCased;
		shortUpper.forceUppercase();
		feLogDirect("%s\n",shortUpper.c_str());
		UNIT_TEST(shortUpper=="ABCDEF");
		String shortLower=shortCased;
		shortLower.forceLowercase();
		feLogDirect("%s\n",shortLower.c_str());
		UNIT_TEST(shortLower=="abcdef");

		String longCased="This is a LONGER string.";
		feLogDirect("%s\n",longCased.c_str());
		String longUpper=longCased;
		longUpper.forceUppercase();
		feLogDirect("%s\n",longUpper.c_str());
		UNIT_TEST(longUpper=="THIS IS A LONGER STRING.");
		String longLower=longCased;
		longLower.forceLowercase();
		feLogDirect("%s\n",longLower.c_str());
		UNIT_TEST(longLower=="this is a longer string.");

		feLogDirect("\n");
		String filename("/path/to/file");
		String path=filename.pathname();
		String base=filename.basename();
		feLogDirect("filename=\"%s\" path=\"%s\" base=\"%s\"\n",
				filename.c_str(),path.c_str(),base.c_str());
		UNIT_TEST(path=="/path/to");
		UNIT_TEST(base=="file");

		String fullname("beforemiddleafter");
		String front=fullname.chop("after");
		String back=fullname.prechop("before");
		feLogDirect("fullname=\"%s\" front=\"%s\" back=\"%s\"\n",
				fullname.c_str(),front.c_str(),back.c_str());
		UNIT_TEST(front=="beforemiddle");
		UNIT_TEST(back=="middleafter");

		String original(" Can't allow the spaces! ");
		String replaced=original.substitute("Can't","Can not");
		replaced=replaced.substitute(" ","_");
		feLogDirect("original=\"%s\"\n",original.c_str());
		feLogDirect("replaced=\"%s\"\n",replaced.c_str());
		UNIT_TEST(replaced=="_Can_not_allow_the_spaces!_");

		String variables("abcd $USER efg ${HOME} "
			"hijk${TERM}lm${UNKNOWN}no\t$HOST\tpq $UNKNOWN");
		String substituted=variables.substituteEnv();
		feLogDirect("variables=\"%s\"\n",variables.c_str());
		feLogDirect("substituted=\"%s\"\n",substituted.c_str());

		std::vector<Real> vector;
//		feLogDirect("std::vector<Real> typeid \"%s\"\n",typeid(vector).name());
		feLogDirect("std::vector<Real> string \"%s\"\n",
				FE_TYPESTRING(decltype(vector)).c_str());

		completed=TRUE;
	}
	catch(Exception &e)	{ e.log(); }
	catch(...)			{ feLogDirect("uncaught exception\n"); }

	UNIT_TEST(completed);
	UNIT_TRACK(15);
	UNIT_RETURN();
}
