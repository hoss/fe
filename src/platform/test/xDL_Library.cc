/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "fe/platform.h"
#include "platform/dlCore.cc"

extern "C"
{

FE_DL_EXPORT void *DL_Begin(void)
{
	feLogDirect("DL_Begin() called\n");

//	feLogDirect("%p %s\n",typeid(F32).name(),typeid(F32).name());

	return NULL;
}

}
