/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "fe/platform.h"
#include "xstr.h"
#include <iostream>
#include <stdexcept>
#include "debug_assert.h"
#include <string>

using namespace fe;

typedef xstr::xstring<xstr::var_char_buf<4> > vchar;

void test(int str_count, int str_size)
{
	String label;
	label.sPrintf("count %d size %d", str_count, str_size);
	SystemTicker ticker(label.c_str());
	std::vector<String>			fe_a_str(str_count);
	std::vector<String>			fe_b_str(str_count);
	std::vector<std::string>	std_a_str(str_count);
	std::vector<std::string>	std_b_str(str_count);
	vchar				*xstr_a_str;
	vchar				*xstr_b_str;

	xstr_a_str = new vchar[str_count]();
	xstr_b_str = new vchar[str_count]();

	ticker.log("init time");

	for(int i = 0; i < str_count; i++)
	{
		fe_a_str[i] = "";
		for(int j = 0; j < str_size; j++)
		{
			fe_a_str[i].sPrintf("%s%c", fe_a_str[i].c_str(),
				'a' + (char)(26.0*((float)rand()/(float)RAND_MAX)));
		}
	}
	ticker.log("create random strings in fe");

	for(int i = 0; i < str_count; i++)
	{
		std_a_str[i] = fe_a_str[i].c_str();
	}
	ticker.log("copy random strings to std");

	for(int i = 0; i < str_count; i++)
	{
		xstr_a_str[i] = fe_a_str[i].c_str();
	}
	ticker.log("copy random strings to xstr");


	// FE
	for(int i = 0; i < str_count; i++)
	{
		fe_b_str[i] = fe_a_str[i].c_str();
	}
	ticker.log("FE: create from c_str");

	for(int i = 0; i < str_count; i++)
	{
		fe_b_str[i] = fe_a_str[i].c_str();
	}
	ticker.log("FE: recreate from assign");

	int same = 0;
	for(int i = 0; i < str_count; i++)
	{
		if(fe_a_str[0] == fe_b_str[i])
		{
			same++;
		}
	}
	ticker.log("FE: compare, mostly miss %d", same);

	same = 0;
	for(int i = 0; i < str_count; i++)
	{
		if(fe_a_str[i] == fe_b_str[i])
		{
			same++;
		}
	}
	ticker.log("FE: compare, mostly hit %d", same);

	// STD
	for(int i = 0; i < str_count; i++)
	{
		std_b_str[i] = std_a_str[i].c_str();
	}
	ticker.log("STD: create from c_str");

	for(int i = 0; i < str_count; i++)
	{
		std_b_str[i] = std_a_str[i];
	}
	ticker.log("STD: recreate from assign");


	same = 0;
	for(int i = 0; i < str_count; i++)
	{
		if(std_a_str[0] == std_b_str[i])
		{
			same++;
		}
	}
	ticker.log("STD: compare, mostly miss %d", same);

	same = 0;
	for(int i = 0; i < str_count; i++)
	{
		if(std_a_str[i] == std_b_str[i])
		{
			same++;
		}
	}
	ticker.log("STD: compare, mostly hit %d", same);

	// XSTR
	for(int i = 0; i < str_count; i++)
	{
		xstr_b_str[i] = xstr_a_str[i].c_str();
	}
	ticker.log("XSTR: create from c_str");

	for(int i = 0; i < str_count; i++)
	{
		xstr_b_str[i] = xstr_a_str[i];
	}
	ticker.log("XSTR: recreate from assign");


	same = 0;
	for(int i = 0; i < str_count; i++)
	{
		if(xstr_a_str[0] == xstr_b_str[i])
		{
			same++;
		}
	}
	ticker.log("XSTR: compare, mostly miss %d", same);

	same = 0;
	for(int i = 0; i < str_count; i++)
	{
		if(xstr_a_str[i] == xstr_b_str[i])
		{
			same++;
		}
	}
	ticker.log("XSTR: compare, mostly hit %d", same);

	delete [] xstr_a_str;
	delete [] xstr_b_str;

}

int main(void)
{
	test(100000, 15);
	test(100000, 16);
	test(100000, 20);
	test(100000, 100);
}
