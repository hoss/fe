/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "fe/platform.h"

#include <omp.h>

#define FE_OMX_DEBUG	FALSE
#define FE_OCD_DEBUG	FALSE
#define FE_OTH_DEBUG	FALSE

#define FE_OCD_WARN		TRUE
#define FE_OTH_WARN		TRUE

//* NOTE OpenMP doesn't have reader-writer "shared" locks

namespace fe
{

void* ompmutex_init(bool a_recursive)
{
	void* pMutex;

	if(a_recursive)
	{
		omp_nest_lock_t* pOmpLock=new omp_nest_lock_t();
		omp_init_nest_lock(pOmpLock);
		pMutex=pOmpLock;
	}
	else
	{
		omp_lock_t* pOmpLock=new omp_lock_t();
		omp_init_lock(pOmpLock);
		pMutex=pOmpLock;
	}

#if FE_OMX_DEBUG
	fe_fprintf(stderr,"ompmutex_init recursive %d -> %p\n",
			a_recursive,pMutex);
#endif

	return pMutex;
}

bool ompmutex_lock(bool a_recursive,void* a_pMutex,
	bool a_un,bool a_try,bool a_readOnly)
{
#if FE_OMX_DEBUG
	fe_fprintf(stderr,"ompmutex_lock recursive %d mutex %p"
			" un %d try %d read %d\n",
			a_recursive,a_pMutex,a_un,a_try,a_readOnly);
#endif

	FEASSERT(a_pMutex);

	if(a_recursive)
	{
		omp_nest_lock_t* pOmpLock=(omp_nest_lock_t*)a_pMutex;

		if(a_un)
		{
			omp_unset_nest_lock(pOmpLock);
			return true;
		}

		if(a_try)
		{
			return omp_test_nest_lock(pOmpLock);
		}

		omp_set_nest_lock(pOmpLock);
		return true;
	}

	omp_lock_t* pOmpLock=(omp_lock_t*)a_pMutex;

	if(a_un)
	{
		omp_unset_lock(pOmpLock);
		return true;
	}

	if(a_try)
	{
		return omp_test_lock(pOmpLock);
	}

	omp_set_lock(pOmpLock);
	return true;
}

void ompmutex_finish(bool a_recursive,void* a_pMutex)
{
#if FE_OMX_DEBUG
	fe_fprintf(stderr,"ompmutex_finish recursive %d mutex %p\n",
			a_recursive,a_pMutex);
#endif

	FEASSERT(a_pMutex);

	if(a_recursive)
	{
		omp_nest_lock_t* pOmpLock=(omp_nest_lock_t*)a_pMutex;

		omp_destroy_nest_lock(pOmpLock);

		delete pOmpLock;
		return;
	}

	omp_lock_t* pOmpLock=(omp_lock_t*)a_pMutex;

	omp_destroy_lock(pOmpLock);

	delete pOmpLock;
}

void* ompguard_init(bool a_recursive,void* a_pMutex,bool a_readOnly)
{
	FEASSERT(a_pMutex);

#if FE_OMX_DEBUG
	fe_fprintf(stderr,"ompguard_init recursive %d mutex %p read %d\n",
			a_recursive,a_pMutex,a_readOnly);
#endif

	ompmutex_lock(a_recursive,a_pMutex,false,false,a_readOnly);

	return a_pMutex;
}

bool ompguard_lock(bool a_recursive,void* a_pGuard,
	bool a_un,bool a_try,bool a_readOnly)
{
#if FE_OMX_DEBUG
	fe_fprintf(stderr,"ompguard_lock recursive %d guard %p"
			" un %d try %d read %d\n",
			a_recursive,a_pGuard,a_un,a_try,a_readOnly);
#endif

	FEASSERT(a_pGuard);

	return ompmutex_lock(a_recursive,a_pGuard,a_un,a_try,a_readOnly);
}

void ompguard_finish(bool a_recursive,void* a_pGuard,bool a_readOnly)
{
#if FE_OMX_DEBUG
	fe_fprintf(stderr,"ompguard_finish recursive %d guard %p read %d\n",
			a_recursive,a_pGuard,a_readOnly);
#endif

	FEASSERT(a_pGuard);

	ompmutex_lock(a_recursive,a_pGuard,true,false,a_readOnly);
}

void* ompcondition_init(void)
{
	void* pCondition=NULL;

#if FE_OCD_DEBUG
	fe_fprintf(stderr,"ompcondition_init -> %p\n",pCondition);
#endif

	return pCondition;
}

bool ompcondition_wait(void* a_pCondition,bool a_recursive,
	void* a_pGuard,bool a_readOnly)
{
#if FE_OCD_DEBUG
	fe_fprintf(stderr,"ompcondition_wait %p guard %p\n",a_pCondition,a_pGuard);
#endif

#if FE_OCD_WARN
	fe_fprintf(stderr,"ompcondition_wait NOT IMPLEMENTED\n");
#endif

	FEASSERT(a_pGuard);

	//* TODO
	return false;
}

bool ompcondition_notify(void* a_pCondition,bool a_all)
{
#if FE_OCD_DEBUG
	fe_fprintf(stderr,"ompcondition_notify %p all %d\n",a_pCondition,a_all);
#endif

#if FE_OCD_WARN
	fe_fprintf(stderr,"ompcondition_notify NOT IMPLEMENTED\n");
#endif

	FEASSERT(a_pCondition);

	//* TODO
	return false;
}

void ompcondition_finish(void* a_pCondition)
{
#if FE_OCD_DEBUG
	fe_fprintf(stderr,"ompcondition_finish %p\n",a_pCondition);
#endif

#if FE_OCD_WARN
	fe_fprintf(stderr,"ompcondition_finish NOT IMPLEMENTED\n");
#endif

	FEASSERT(a_pCondition);

	//* TODO
}

extern "C"
{

FE_DL_EXPORT bool omp_mutex_init(void)
{
#if FE_OCD_DEBUG
	fe_printf("omp_mutex_init()\n");
#endif

//	fe_printf("  using omp::mutex\n");

	Mutex::replaceInitFunction(ompmutex_init);
	Mutex::replaceLockFunction(ompmutex_lock);
	Mutex::replaceFinishFunction(ompmutex_finish);

	Mutex::replaceGuardInitFunction(ompguard_init);
	Mutex::replaceGuardLockFunction(ompguard_lock);
	Mutex::replaceGuardFinishFunction(ompguard_finish);

	Mutex::replaceConditionInitFunction(ompcondition_init);
	Mutex::replaceConditionWaitFunction(ompcondition_wait);
	Mutex::replaceConditionNotifyFunction(ompcondition_notify);
	Mutex::replaceConditionFinishFunction(ompcondition_finish);

	return true;
}

}

///////////////////////////////////////////////////////////////////////////////

void* ompthread_default_init(void)
{
	FEASSERT(FALSE);
	void* pThread=NULL;

#if FE_OTH_DEBUG
	fe_fprintf(stderr,"ompthread_default_init -> %p\n",pThread);
#endif

	return pThread;
}

void* ompthread_init(void* a_pFunctor)
{
	FEASSERT(a_pFunctor);

	Thread::Functor& rFunctor=*(Thread::Functor*)(a_pFunctor);

#if FE_OTH_WARN
	fe_fprintf(stderr,"ompthread_init NOT IMPLEMENTED\n");
#endif

	//* TODO
	FEASSERT(FALSE);
	void* pThread=NULL;

#if FE_OTH_DEBUG
	fe_fprintf(stderr,"ompthread_init -> %p\n",pThread);
#endif

	return pThread;
}


void ompthread_interrupt(void* a_pThread)
{
#if FE_OTH_DEBUG
	fe_fprintf(stderr,"ompthread_interrupt thread %p\n",a_pThread);
#endif

#if FE_OTH_WARN
	fe_fprintf(stderr,"ompthread_interrupt NOT IMPLEMENTED\n");
#endif

	FEASSERT(a_pThread);

	//* TODO
}

void ompthread_join(void* a_pThread)
{
#if FE_OTH_DEBUG
	fe_fprintf(stderr,"ompthread_join thread %p\n",a_pThread);
#endif

#if FE_OTH_WARN
	fe_fprintf(stderr,"ompthread_join NOT IMPLEMENTED\n");
#endif

	FEASSERT(a_pThread);

	//* TODO
}

bool ompthread_joinable(void* a_pThread)
{
#if FE_OTH_DEBUG
	fe_fprintf(stderr,"ompthread_joinable thread %p\n",a_pThread);
#endif

#if FE_OTH_WARN
	fe_fprintf(stderr,"ompthread_joinable NOT IMPLEMENTED\n");
#endif

	FEASSERT(a_pThread);

	//* TODO
	return FALSE;
}

void ompthread_finish(void* a_pThread)
{
#if FE_OTH_DEBUG
	fe_fprintf(stderr,"ompthread_finish thread %p\n",a_pThread);
#endif

#if FE_OTH_WARN
	fe_fprintf(stderr,"ompthread_finish NOT IMPLEMENTED\n");
#endif

	FEASSERT(a_pThread);

	//* TODO
}

void ompthread_interruption(void)
{
#if FE_OTH_DEBUG
	fe_fprintf(stderr,"ompthread_interruption\n");
#endif

#if FE_OTH_WARN
	fe_fprintf(stderr,"ompthread_interruption NOT IMPLEMENTED\n");
#endif

	//* TODO
}

int ompthread_concurrency(void)
{
#if FE_OTH_DEBUG
	fe_fprintf(stderr,"ompthread_concurrency\n");
#endif

	return omp_get_max_threads();
}

void* ompgroup_init(void)
{
#if FE_OTH_WARN
	fe_fprintf(stderr,"ompgroup_init NOT IMPLEMENTED\n");
#endif

	//* TODO
	void* pThreadGroup=NULL;

	FEASSERT(pThreadGroup);

#if FE_OTH_DEBUG
	fe_fprintf(stderr,"ompgroup_init -> %p\n",pThreadGroup);
#endif

	return pThreadGroup;
}

void* ompgroup_create(void* a_pThreadGroup,void* a_pFunctor)
{
	FEASSERT(a_pThreadGroup);
	FEASSERT(a_pFunctor);

//	Thread::Functor* pFunctor=(Thread::Functor*)(a_pFunctor);

#if FE_OTH_WARN
	fe_fprintf(stderr,"ompgroup_create NOT IMPLEMENTED\n");
#endif

	//* TODO
	void* pThread=NULL;
	FEASSERT(pThread);

#if FE_OTH_DEBUG
	fe_fprintf(stderr,"ompgroup_create group %p functor %p -> %p\n",
			a_pThreadGroup,a_pFunctor,pThread);
#endif

	return pThread;
}

void ompgroup_join_all(void* a_pThreadGroup)
{
#if FE_OTH_DEBUG
	fe_fprintf(stderr,"ompgroup_join_all group %p\n",a_pThreadGroup);
#endif

#if FE_OTH_WARN
	fe_fprintf(stderr,"ompgroup_join_all NOT IMPLEMENTED\n");
#endif

	FEASSERT(a_pThreadGroup);

	//* TODO

#if FE_OTH_DEBUG
	fe_fprintf(stderr,"ompgroup_join_all group %p complete\n",a_pThreadGroup);
#endif
}

void ompgroup_finish(void* a_pThreadGroup)
{
#if FE_OTH_DEBUG
	fe_fprintf(stderr,"ompgroup_finish %p\n",a_pThreadGroup);
#endif

#if FE_OTH_WARN
	fe_fprintf(stderr,"ompgroup_finish NOT IMPLEMENTED\n");
#endif

	FEASSERT(a_pThreadGroup);

	//* TODO
}

extern "C"
{

FE_DL_EXPORT bool omp_thread_init(void)
{
#if FE_OTH_DEBUG
	fe_printf("omp_thread_init()\n");
#endif

	Thread::replaceDefaultInitFunction(ompthread_default_init);
	Thread::replaceInitFunction(ompthread_init);
	Thread::replaceInterruptFunction(ompthread_interrupt);
	Thread::replaceJoinFunction(ompthread_join);
	Thread::replaceJoinableFunction(ompthread_joinable);
	Thread::replaceFinishFunction(ompthread_finish);
	Thread::replaceInterruptionFunction(ompthread_interruption);
	Thread::replaceConcurrencyFunction(ompthread_concurrency);

	Thread::replaceGroupInitFunction(ompgroup_init);
	Thread::replaceGroupCreateFunction(ompgroup_create);
	Thread::replaceGroupJoinAllFunction(ompgroup_join_all);
	Thread::replaceGroupFinishFunction(ompgroup_finish);

	return true;
}

}

} /* namespace */
