/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __platform_thread_h__
#define __platform_thread_h__

namespace fe
{

/*
	boost::thread_group*	m_pGroup;
	boost::thread**			m_ppThread;

	a_threads=boost::thread::hardware_concurrency();

	m_pGroup=new boost::thread_group();
	m_ppWorker=new WORKER*[a_threads];
	m_ppThread=new boost::thread*[a_threads];

	m_ppThread[m]=m_pGroup->create_thread(
			*m_ppWorker[m]);
	m_ppThread[n]->interrupt();
	m_pGroup->join_all();
	m_pGroup->remove_thread(m_ppThread[m]);

	boost::this_thread::interruption_point();

	boost::thread_specific_ptr< Array< sp<T> > >*	m_pTssArray;

	Array< sp<T> >* pArray;
	if(!(pArray=m_pTssArray->get()))
	{
		pArray=new Array< sp<T> >();
		m_pTssArray->reset(pArray);
*/

extern "C"
{

typedef void* (FE_CDECL ThreadDefaultInitFunction)(void);
typedef void* (FE_CDECL ThreadInitFunction)(void* a_pFunctor);
typedef bool (FE_CDECL ThreadConfigFunction)(void* a_pRawThread,String a_property,String a_value);
typedef void (FE_CDECL ThreadInterruptFunction)(void* a_pRawThread);
typedef void (FE_CDECL ThreadJoinFunction)(void* a_pRawThread);
typedef bool (FE_CDECL ThreadJoinableFunction)(void* a_pRawThread);
typedef void (FE_CDECL ThreadFinishFunction)(void* a_pRawThread);
typedef void (FE_CDECL ThreadInterruptionFunction)(void);
typedef int (FE_CDECL ThreadConcurrencyFunction)(void);

typedef void* (FE_CDECL ThreadGroupInitFunction)(void);
typedef void* (FE_CDECL ThreadGroupCreateFunction)(
		void* a_pRawThreadGroup,void* a_pFunctor);
typedef void (FE_CDECL ThreadGroupJoinAllFunction)(void* a_pRawThreadGroup);
typedef void (FE_CDECL ThreadGroupFinishFunction)(void* a_pRawThreadGroup);

} /* extern "C" */

FE_MEM_PORT	extern	ThreadDefaultInitFunction*		gs_fnThreadDefaultInit;
FE_MEM_PORT	extern	ThreadInitFunction*				gs_fnThreadInit;
FE_MEM_PORT	extern	ThreadConfigFunction*			gs_fnThreadConfig;
FE_MEM_PORT	extern	ThreadInterruptFunction*		gs_fnThreadInterrupt;
FE_MEM_PORT	extern	ThreadJoinFunction*				gs_fnThreadJoin;
FE_MEM_PORT	extern	ThreadJoinableFunction*			gs_fnThreadJoinable;
FE_MEM_PORT	extern	ThreadFinishFunction*			gs_fnThreadFinish;
FE_MEM_PORT	extern	ThreadInterruptionFunction*		gs_fnThreadInterruption;
FE_MEM_PORT	extern	ThreadConcurrencyFunction*		gs_fnThreadConcurrency;

FE_MEM_PORT	extern	ThreadGroupInitFunction*		gs_fnThreadGroupInit;
FE_MEM_PORT	extern	ThreadGroupCreateFunction*		gs_fnThreadGroupCreate;
FE_MEM_PORT	extern	ThreadGroupJoinAllFunction*		gs_fnThreadGroupJoinAll;
FE_MEM_PORT	extern	ThreadGroupFinishFunction*		gs_fnThreadGroupFinish;

FE_MEM_PORT	extern	String							gs_threadSupport;
FE_MEM_PORT	extern	BWORD							gs_threadChecked;
FE_MEM_PORT	extern	DL_Loader*						gs_pThreadLoader;

class FE_DL_PUBLIC FE_DL_EXPORT Thread
{
	public:

	class FE_DL_PUBLIC FE_DL_EXPORT Functor
	{
		public:
					Functor(void);
	virtual			~Functor(void);
	virtual	void	operate(void);

			void	operator()(void);

		protected:
			Functor*	m_pOriginal;	//* operator() 'this' may be different
	};

	class FE_DL_PUBLIC FE_DL_EXPORT Group
	{
		public:
					Group(void);
					~Group(void);

			Thread*	createThread(Functor* a_pFunctor);
			BWORD	joinAll(void);

		private:

			void*	m_pRawThreadGroup;
	};

				Thread(void);
				Thread(Functor* a_pFunctor);
				Thread(void* a_pRawThread);
				~Thread(void);

		BWORD	config(String a_property,String a_value);
		BWORD	interrupt(void);
		BWORD	join(void);
		BWORD	joinable(void);

static	BWORD	interruptionPoint(void);
static	I32		hardwareConcurrency(void);
static	String	support(void)	{ return gs_threadSupport; }

static	void	replaceDefaultInitFunction(
					ThreadDefaultInitFunction* a_fnDefaultInit)
				{	gs_fnThreadDefaultInit=a_fnDefaultInit; }
static	void	replaceInitFunction(
					ThreadInitFunction* a_fnInit)
				{	gs_fnThreadInit=a_fnInit; }
static	void	replaceConfigFunction(
					ThreadConfigFunction* a_fnConfig)
				{	gs_fnThreadConfig=a_fnConfig; }
static	void	replaceInterruptFunction(
					ThreadInterruptFunction* a_fnInterrupt)
				{	gs_fnThreadInterrupt=a_fnInterrupt; }
static	void	replaceJoinFunction(
					ThreadJoinFunction* a_fnJoin)
				{	gs_fnThreadJoin=a_fnJoin; }
static	void	replaceJoinableFunction(
					ThreadJoinableFunction* a_fnJoinable)
				{	gs_fnThreadJoinable=a_fnJoinable; }
static	void	replaceFinishFunction(
					ThreadFinishFunction* a_fnFinish)
				{	gs_fnThreadFinish=a_fnFinish; }
static	void	replaceInterruptionFunction(
					ThreadInterruptionFunction* a_fnInterruption)
				{	gs_fnThreadInterruption=a_fnInterruption; }
static	void	replaceConcurrencyFunction(
					ThreadConcurrencyFunction* a_fnConcurrency)
				{	gs_fnThreadConcurrency=a_fnConcurrency; }

static	void	replaceGroupInitFunction(
					ThreadGroupInitFunction* a_fnGroupInit)
				{	gs_fnThreadGroupInit=a_fnGroupInit; }
static	void	replaceGroupCreateFunction(
					ThreadGroupCreateFunction* a_fnGroupCreate)
				{	gs_fnThreadGroupCreate=a_fnGroupCreate; }
static	void	replaceGroupJoinAllFunction(
					ThreadGroupJoinAllFunction* a_fnGroupJoinAll)
				{	gs_fnThreadGroupJoinAll=a_fnGroupJoinAll; }
static	void	replaceGroupFinishFunction(
					ThreadGroupFinishFunction* a_fnGroupFinish)
				{	gs_fnThreadGroupFinish=a_fnGroupFinish; }

static	BWORD	clearFunctionPointers(void);
static	BWORD	dismiss(void);

	private:

static	BWORD	confirm(void);

		void*	m_pRawThread;
};

} /* namespace */

#endif /* __platform_thread_h__ */
