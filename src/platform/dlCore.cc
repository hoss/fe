/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <platform/platform.pmh>

#if FE_OS==FE_WIN32 || FE_OS==FE_WIN64

namespace fe
{
extern HINSTANCE gs_hinstDLL;
HINSTANCE gs_hinstDLL=0;
}

BWORD WINAPI DllMain(HINSTANCE hinstDLL,DWORD fdwReason,LPVOID lpvReserved)
{
	//* stop DLL_THREAD_ATTACH/DETACH notifications (reduces code & init time)
	BWORD success=DisableThreadLibraryCalls(hinstDLL);
	if(!success)
	{
		feLogDirect("DllMain() DisableThreadLibraryCalls failed\n");
	}

	fe::gs_hinstDLL=hinstDLL;
	return TRUE;
}

#endif
