/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __platform_type_name_h__
#define __platform_type_name_h__

namespace fe
{

//* WARNING having a function templated on T and then calling another
//* templated function on a standard container of T, like std::deque<T>
//* can inconsistently add the allocator arg to the container type string.

// for more complexity, see:
// https://stackoverflow.com/questions/81870/is-it-possible-to-print-a-variables-type-in-standard-c

template <class T>
fe::String FE_CDECL fe_type_name()
{
#ifdef __clang__
	fe::String signature = __PRETTY_FUNCTION__;
	return signature.prechop(35).chop(1);

#elif defined(__GNUC__)
	fe::String signature = __PRETTY_FUNCTION__;

#if FE_CPLUSPLUS < 201402L
	return signature.prechop(40).chop(1);
#else
	return signature.prechop(50).chop(1);
#endif

#elif defined(_MSC_VER)
	fe::String signature = __FUNCSIG__;
	return signature.prechop(42).chop(7).substitute("class ","").substitute("<class","<");
#endif
}

} /* namespace */

#if FE_RTTI && FE_TYPEID
#include <typeinfo>
#define	FE_TYPESTRING(T)	fe::System::demangle(typeid(T).name())
#else
#define	FE_TYPESTRING(T)	fe::fe_type_name<T>()
#endif

#endif /* __platform_type_name_h__ */
