/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __math_Mass_h__
#define __math_Mass_h__

namespace fe
{

class FE_DL_EXPORT Mass
{
	public:
		Mass(void) {}

		void	cylinder(	Axis a_axis,
							Real a_density,
							Real a_radius,
							Real a_length);

		void	tube(		Axis a_axis,
							Real a_density,
							Real a_radiusOuter,
							Real a_radiusInner,
							Real a_length);

		void	rotate(		const SpatialTransform &a_rotation);
		void	translate(	const SpatialVector &a_translation);

		Mass	&operator +=(const Mass &a_other);

		Matrix<3,3,Real>		&I(void) { return m_inertia; }
		Real					&mass(void) { return m_mass; }
		Vector<3,Real>			&cg(void) { return m_cg; }
		const Matrix<3,3,Real>	&getI(void) const { return m_inertia; }
		const Real				&getMass(void) const { return m_mass; }
		const Vector<3,Real>	&getCg(void) const { return m_cg; }

	private:
		Matrix<3,3,Real>	m_inertia;
		Real				m_mass;
		Vector<3,Real>		m_cg;
};

} /* namespace */

#endif /* __math_Mass_h__ */

