/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <math/math.pmh>

namespace fe
{

Mass &Mass::operator +=(const Mass &a_other)
{
	// CG
	Real mass_inv = (Real)1.0 / (m_mass + a_other.m_mass);
//fe_fprintf(stderr, "CG1 %f %f %f\n", m_cg[0], m_cg[1], m_cg[2]);
//fe_fprintf(stderr, "CG2 %f %f %f\n", a_other.m_cg[0], a_other.m_cg[1], a_other.m_cg[2]);
	m_cg = (m_cg*m_mass + a_other.m_cg*a_other.m_mass) * mass_inv;
//fe_fprintf(stderr, "CG3 %f %f %f\n", m_cg[0], m_cg[1], m_cg[2]);

	// overall mass
//fe_fprintf(stderr, "M1 %f\n", m_mass);
//fe_fprintf(stderr, "M2 %f\n", a_other.m_mass);
	m_mass += a_other.m_mass;
//fe_fprintf(stderr, "M3 %f\n", m_mass);

	// inertia
	add(m_inertia, m_inertia, a_other.m_inertia);

	return *this;
}

void Mass::cylinder(Axis a_axis, Real a_density, Real a_radius, Real a_length)
{
	Real rsqr = a_radius * a_radius;

	m_mass = rsqr * a_length * a_density * fe::pi;

	setAll(m_inertia, (Real)0.0);
	setAll(m_cg, (Real)0.0);

	Real mrsqr = m_mass * rsqr;
	Real no_axis = ((Real)1.0/(Real)4.0) * mrsqr
		+ ((Real)1.0/(Real)12.0) * a_length * a_length * m_mass;
	Real is_axis = (Real)1.0/(Real)2.0 * mrsqr;

	if(a_axis == e_xAxis)
	{
		m_inertia(0,0) = is_axis;
		m_inertia(1,1) = no_axis;
		m_inertia(2,2) = no_axis;
	}
	else if(a_axis == e_yAxis)
	{
		m_inertia(0,0) = no_axis;
		m_inertia(1,1) = is_axis;
		m_inertia(2,2) = no_axis;
	}
	else if(a_axis == e_zAxis)
	{
		m_inertia(0,0) = no_axis;
		m_inertia(1,1) = no_axis;
		m_inertia(2,2) = is_axis;
	}
}

void Mass::tube(Axis a_axis, Real a_density, Real a_radiusOuter,
	Real a_radiusInner, Real a_length)
{
	cylinder(a_axis, a_density, a_radiusOuter, a_length);
	Mass negativeMass;
	negativeMass.cylinder(a_axis, -a_density, a_radiusInner, a_length);
	*this += negativeMass;
}

void Mass::rotate(const SpatialTransform &a_rotation)
{
	Matrix<3,3,Real> r;
	for(unsigned int i = 0; i < 3; i++)
	{
		for(unsigned int j = 0; j < 3; j++)
		{
			r(i,j) = a_rotation(i,j);
		}
	}

	m_inertia = r * m_inertia * transpose(r);

	m_inertia(1,0) = m_inertia(0,1);
	m_inertia(2,0) = m_inertia(0,2);
	m_inertia(2,1) = m_inertia(1,2);


	transformVector(a_rotation, m_cg, m_cg);

}

void Mass::translate(const SpatialVector &a_translation)
{
	// ode does something completely different (and I think faster), but I
	// didn't derive it that way yet, so here is what I figured out quick
	// and dirty.  I get the same results as ode.
	Vector3 r0, r1;

	r0 = - m_cg;
	r1 = - a_translation - m_cg;
	Matrix<3,3,Real> m0,m1,out;
	setIdentity(m0);
	m0 *= dot(r0,r0);
	outerProduct(out,r0,r0);
	subtract(m0,m0,out);
	m0 *= m_mass;

	setIdentity(m1);
	m1 *= dot(r1,r1);
	outerProduct(out,r1,r1);
	subtract(m1,m1,out);
	m1 *= m_mass;

	subtract(m_inertia, m_inertia, m0);
	add(m_inertia, m_inertia, m1);

	m_inertia(1,0) = m_inertia(0,1);
	m_inertia(2,0) = m_inertia(0,2);
	m_inertia(2,1) = m_inertia(1,2);

	m_cg += a_translation;
}

} /* namespace */

