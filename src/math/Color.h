/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __math_Color_h__
#define __math_Color_h__


namespace fe
{

//typedef Vector<4,Real> Color;

/**************************************************************************//**
	@brief Special vector for color (RGBA)

	@ingroup math
*//***************************************************************************/
class Color: public Vector<4,Real>
{
	public:
		Color(void):
				Vector<4,Real>()											{}

		Color(Real r,Real g,Real b=(Real)0,Real a=(Real)1):
				Vector<4,Real>(r,g,b,a)										{}

		Color(const Vector<3,Real> &other)
		{	this->Vector<4,Real>::operator=(other);
			m_data[3]=1.0; }

		Color(const Vector<4,Real> &other)
		{	this->Vector<4,Real>::operator=(other); }

		template<typename U>
		Color(const Vector<3,U> &other)
		{	this->Vector<4,Real>::operator=(other);
			m_data[3]=1.0; }

		template<typename U>
		Color(const Vector<4,U> &other)
		{	this->Vector<4,Real>::operator=(other); }

		Color(const Real* array)
		{	operator=(array); }
};

inline Color& set(Color &lhs)
{
	lhs[0]=Real(0);
	lhs[1]=Real(0);
	lhs[2]=Real(0);
	lhs[3]=Real(1);
	return lhs;
}

template<typename U>
inline Color& set(Color &lhs, U r)
{
	lhs[0]=(Real)r;
	lhs[1]=Real(0);
	lhs[2]=Real(0);
	lhs[3]=Real(1);
	return lhs;
}

template<typename U, typename V>
inline Color& set(Color &lhs, U r, V g)
{
	lhs[0]=(Real)r;
	lhs[1]=(Real)g;
	lhs[2]=Real(0);
	lhs[3]=Real(1);
	return lhs;
}

template<typename U, typename V, typename W>
inline Color& set(Color &lhs, U r, V g, W b)
{
	lhs[0]=(Real)r;
	lhs[1]=(Real)g;
	lhs[2]=(Real)b;
	lhs[3]=Real(1);
	return lhs;
}

} /* namespace */

#endif /* __math_Color_h__ */
