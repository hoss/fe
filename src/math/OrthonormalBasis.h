/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __math_OrthonormalBasis_h__
#define __math_OrthonormalBasis_h__

namespace fe
{

/**************************************************************************//**
	\brief OrthonormalBasis for 3D transformations

	@ingroup math

	HACK Currently assumes Z-forward Y-up

	TODO care about specified direction and up
*//***************************************************************************/
template <typename T>
class OrthonormalBasis: public Matrix<3,4,T>
{
	public:
				OrthonormalBasis<T>(Axis a_direction,Axis a_up)
				{
					setIdentity(*this);
				}

				template <typename U>
		void	adapt(const Matrix<3,4,U>& a_rMatrix);

	private:
		Axis	m_direction;
		Axis	m_up;

};

template <typename T>
template <typename U>
inline void OrthonormalBasis<T>::adapt(const Matrix<3,4,U>& a_rMatrix)
{
	Matrix<3,4,T>::direction()=
			Vector<3,T>(
			a_rMatrix.left()[1],
			a_rMatrix.left()[2],
			a_rMatrix.left()[0]);

	Matrix<3,4,T>::up()=
			Vector<3,T>(
			a_rMatrix.direction()[1],
			a_rMatrix.direction()[2],
			a_rMatrix.direction()[0]);

	Matrix<3,4,T>::left()=
			Vector<3,T>(
			a_rMatrix.up()[1],
			a_rMatrix.up()[2],
			a_rMatrix.up()[0]);

	setTranslation((Matrix<3,4,T>&)*this,
			Vector<3,T>(
			a_rMatrix.translation()[1],
			a_rMatrix.translation()[2],
			a_rMatrix.translation()[0]));
}

typedef OrthonormalBasis<F32>	OrthonormalBasisf;
typedef OrthonormalBasis<F64>	OrthonormalBasisd;
typedef OrthonormalBasis<Real>	SpatialBasis;

} /* namespace */

#endif /* __math_OrthonormalBasis_h__ */

