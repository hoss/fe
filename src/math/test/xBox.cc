/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "math/math.h"

using namespace fe;

int main(void)
{
	UNIT_START();
	BWORD completed=FALSE;

	try
	{
		Box2i a;
		Box2i b;
		Box2i c;
		Box2i d;
		set(a,1,2,3,4);
		set(b,1,2,3,4);
		set(c,2,2,3,4);
		set(d,1,2,3,3);
		UNIT_TEST(a==b);
		UNIT_TEST(a!=c);
		UNIT_TEST(a!=d);
		UNIT_TEST(c!=d);

		Box2i i2;

		set(i2,Vector2i(1,2),Vector2i(3,4));
		feLog("%d %d %d %d\n",i2[0],i2[1],i2.size()[0],i2.size()[1]);
		UNIT_TEST(i2[0]==1);
		UNIT_TEST(i2[1]==2);
		UNIT_TEST(width(i2)==3);
		UNIT_TEST(height(i2)==4);

		set(i2,5,6,7,8);
		UNIT_TEST(i2==Vector2i(5,6));
		UNIT_TEST(i2.size()==Vector2i(7,8));

		resize(i2,9,10);
		UNIT_TEST(i2==Vector2i(5,6));
		UNIT_TEST(i2.size()==Vector2i(9,10));

		Box3f f3(Vector3f(1.2,2.3,3.4),Vector3f(4.5,5.6,6.7));

		feLog("%s  %s\n",print(f3).c_str(),print(f3.size()).c_str());

		completed=TRUE;
	}
	catch(Exception &e)	{ e.log(); }
	catch(...)			{ feLog("uncaught exception\n"); }

	UNIT_TEST(completed);
	UNIT_TRACK(13);
	UNIT_RETURN();
}

