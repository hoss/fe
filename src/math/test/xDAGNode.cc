/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "math/math.h"

using namespace fe;

int main(void)
{
	UNIT_START();
	BWORD completed=FALSE;

	try
	{
		sp<DAGNode> spNode1(new DAGNode);
		sp<DAGNode> spNode2(new DAGNode);
		sp<DAGNode> spNode3(new DAGNode);
		sp<DAGNode> spNode4(new DAGNode);

		spNode3->attachTo(spNode1);
		spNode3->attachTo(spNode2);
		spNode4->attachTo(spNode2);

		feLog("spNode1 parent %d (vs 0)\n",spNode1->parentConnectionCount());
		UNIT_TEST(spNode1->parentConnectionCount()==0);
		feLog("spNode1 child %d (vs 1)\n",spNode1->childConnectionCount());
		UNIT_TEST(spNode1->childConnectionCount()==1);
		feLog("spNode2 parent %d (vs 0)\n",spNode2->parentConnectionCount());
		UNIT_TEST(spNode2->parentConnectionCount()==0);
		feLog("spNode2 child %d (vs 2)\n",spNode2->childConnectionCount());
		UNIT_TEST(spNode2->childConnectionCount()==2);
		feLog("spNode3 parent %d (vs 2)\n",spNode3->parentConnectionCount());
		UNIT_TEST(spNode3->parentConnectionCount()==2);
		feLog("spNode3 child %d (vs 0)\n",spNode3->childConnectionCount());
		UNIT_TEST(spNode3->childConnectionCount()==0);
		feLog("spNode4 parent %d (vs 1)\n",spNode4->parentConnectionCount());
		UNIT_TEST(spNode4->parentConnectionCount()==1);
		feLog("spNode4 child %d (vs 0)\n",spNode4->childConnectionCount());
		UNIT_TEST(spNode4->childConnectionCount()==0);

		spNode3->detach();
		spNode4->detachFrom(spNode1);
		spNode4->detachFrom(spNode2);

		feLog("\nspNode1 parent %d (vs 0)\n",spNode1->parentConnectionCount());
		UNIT_TEST(!spNode1->parentConnectionCount());
		feLog("spNode1 child %d (vs 0)\n",spNode1->childConnectionCount());
		UNIT_TEST(!spNode1->childConnectionCount());
		feLog("spNode2 parent %d (vs 0)\n",spNode2->parentConnectionCount());
		UNIT_TEST(!spNode2->parentConnectionCount());
		feLog("spNode2 child %d (vs 0)\n",spNode2->childConnectionCount());
		UNIT_TEST(!spNode2->childConnectionCount());
		feLog("spNode3 parent %d (vs 0)\n",spNode3->parentConnectionCount());
		UNIT_TEST(!spNode3->parentConnectionCount());
		feLog("spNode3 child %d (vs 0)\n",spNode3->childConnectionCount());
		UNIT_TEST(!spNode3->childConnectionCount());
		feLog("spNode4 parent %d (vs 0)\n",spNode4->parentConnectionCount());
		UNIT_TEST(!spNode4->parentConnectionCount());
		feLog("spNode4 child %d (vs 0)\n",spNode4->childConnectionCount());
		UNIT_TEST(!spNode4->childConnectionCount());

		completed=TRUE;
	}
	catch(Exception &e)	{ e.log(); }
	catch(...)			{ feLog("uncaught exception\n"); }

	UNIT_TEST(completed);
	UNIT_TRACK(17);
	UNIT_RETURN();
}
