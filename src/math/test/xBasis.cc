/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "math/math.h"

using namespace fe;

int main(void)
{
	UNIT_START();
	BWORD completed=FALSE;

	try
	{
		SpatialTransform z30;
		setIdentity(z30);
		rotate(z30,30.0f*degToRad,e_zAxis);
		feLog("z30\n%s\n",c_print(z30));

		SpatialBasis yUp(e_zAxis,e_yAxis);
		yUp.adapt(z30);
		feLog("yUp\n%s\n",c_print(yUp));

#if FALSE
		setIdentity(yUp);
		SpatialTransform yTransform;
		setIdentity(yTransform);
		yTransform(0,0)=1.0;
		yTransform(1,1)=0.0;
		yTransform(1,2)= -1.0;
		yTransform(2,1)=1.0;
		yTransform(2,2)=0.0;
		SpatialTransform yInvert=yTransform;
		yTransform(1,2)=1.0;
		yTransform(2,1)= -1.0;
		(SpatialTransform&)yUp=yInvert*z30*yTransform;
		feLog("yTransform\n%s\n",c_print(yTransform));
		feLog("yUp\n%s\n",c_print(yUp));
#endif

		const SpatialVector xVector(1.0,0.0,0.0);
		const SpatialVector yVector(0.0,1.0,0.0);
		const SpatialVector zVector(0.0,0.0,1.0);

		SpatialVector yUpResult;
		SpatialVector zUpResult;

		feLog("\n");
		transformVector(z30,yVector,zUpResult);
		transformVector(yUp,xVector,yUpResult);
		feLog("z30 in zUp: yVector -> %s\n",c_print(zUpResult));
		feLog("z30 as yUp: xVector -> %s\n",c_print(yUpResult));
		UNIT_TEST(zUpResult[0]==yUpResult[2]);
		UNIT_TEST(zUpResult[1]==yUpResult[0]);
		UNIT_TEST(zUpResult[2]==yUpResult[1]);

		feLog("\n");
		transformVector(z30,zVector,zUpResult);
		transformVector(yUp,yVector,yUpResult);
		feLog("z30 in zUp: zVector -> %s\n",c_print(zUpResult));
		feLog("z30 as yUp: yVector -> %s\n",c_print(yUpResult));
		UNIT_TEST(zUpResult[0]==yUpResult[2]);
		UNIT_TEST(zUpResult[1]==yUpResult[0]);
		UNIT_TEST(zUpResult[2]==yUpResult[1]);

		feLog("\n");
		transformVector(z30,xVector,zUpResult);
		transformVector(yUp,zVector,yUpResult);
		feLog("z30 in zUp: xVector -> %s\n",c_print(zUpResult));
		feLog("z30 as yUp: zVector -> %s\n",c_print(yUpResult));
		UNIT_TEST(zUpResult[0]==yUpResult[2]);
		UNIT_TEST(zUpResult[1]==yUpResult[0]);
		UNIT_TEST(zUpResult[2]==yUpResult[1]);

		completed=TRUE;
	}
	catch(Exception &e)	{ e.log(); }
	catch(...)			{ feLog("uncaught exception\n"); }

	UNIT_TEST(completed);
	UNIT_TRACK(10);
	UNIT_RETURN();
}
