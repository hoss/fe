/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "math/math.h"

#define	TYPE1	Vector4d
#define	TYPE2	Vector4f

using namespace fe;

int main(int argc,char** argv)
{
	feLog("UNIT_START\n");
	UNIT_START();
	BWORD completed=FALSE;

	if(argc>2)
	{
		feLog("Syntax: %s [mode]\n",argv[0]);
		exit(1);
	}
	const U32 mode=(argc>1)? atoi(argv[1]): 0;

	try
	{
		sp<Profiler> spProfiler(new Profiler("Test Profile"));
		UNIT_TEST(spProfiler.isValid());

		sp<Profiler::Profile> spStep1(
				new Profiler::Profile(spProfiler,"Step 1"));
		sp<Profiler::Profile> spStep2(
				new Profiler::Profile(spProfiler,"Step 2"));

		TYPE1 sum1;
		TYPE2 sum2;

#if TRUE
		TYPE1 point1[1000];
		TYPE2 point2[1000];

//		const TYPE1* rPoint1=point1;
		const TYPE2* rPoint2=point2;
#else
		std::vector<TYPE1> point1(1000);
		std::vector<TYPE2> point2(1000);

		const std::vector<TYPE1>& rPoint1=point1;
		const std::vector<TYPE2>& rPoint2=point2;
#endif

		for(U32 m=0;m<1000;m++)
		{
			set(point1[m],m,m+1,m+2);
			set(point2[m],m,m+1,m+2);
		}

		feLog("Begin\n");

		spProfiler->begin();

		spStep1->start();

		for(U32 pass=0;pass<10000;pass++)
		{
			if(mode)
			{
				set(sum1);
				for(U32 m=0;m<1000;m++)
				{
					sum1[0]+=point1[m][0];
					sum1[1]+=point1[m][1];
					sum1[2]+=point1[m][2];
				}
			}
			else
			{
				set(sum1);
				for(U32 m=0;m<1000;m++)
				{
					sum1+=point1[m];
				}
			}
		}

		spStep2->replace(spStep1);

		for(U32 pass=0;pass<10000;pass++)
		{
//			feLog("pass %d\n",pass);
			if(mode)
			{
				set(sum2);

				for(U32 m=0;m<1000;m++)
				{
//					feLog("index %d\n",m);

					sum2[0]+=rPoint2[m][0];
					sum2[1]+=rPoint2[m][1];
					sum2[2]+=rPoint2[m][2];

//					sum2.addToElement(0,point2[m].temp()[0]);
//					sum2.addToElement(1,point2[m].temp()[1]);
//					sum2.addToElement(2,point2[m].temp()[2]);

//					sum2.addToElement(0,rPoint2[m][0]);
//					sum2.addToElement(1,rPoint2[m][1]);
//					sum2.addToElement(2,rPoint2[m][2]);

//					setAt(sum2,0,sum2[0]+rPoint2[m][0]);

//					setAt(sum2,0,rPoint2[m][0]);
//					setAt(sum2,1,rPoint2[m][1]);
//					setAt(sum2,2,rPoint2[m][2]);
				}
			}
			else
			{
				set(sum2);

				for(U32 m=0;m<1000;m++)
				{
					sum2+=point2[m];
				}
			}
		}

		spStep2->finish();

		spProfiler->end();

		feLog("End\n");

		feLog("sum1 %s\n",c_print(sum1));
		feLog("sum2 %s\n",c_print(sum2));

		feLog("\nProfiler %s\n%s\n",
				spProfiler->name().c_str(),
				spProfiler->report().c_str());

		completed=TRUE;
	}
	catch(Exception &e)
	{
		e.log();
	}

	UNIT_TEST(completed);
	UNIT_TRACK(2);
	UNIT_RETURN();
}
