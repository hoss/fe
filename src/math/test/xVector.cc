/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "math/math.h"

using namespace fe;

void printfloats(const double* array)
{
	feLog("%g %g %g %g\n",array[0],array[1],array[2],array[3]);
}

int main(void)
{
	UNIT_START();
	BWORD completed=FALSE;

	try
	{
		//Vector<2,int> i2;

		Vector<4,double> a(1.0,2.0,3.0);

		double da[] = { 3.0, 4.0, 5.0, 0.0 };
		Vector<4,double> b(da);

		Vector4d c = cross3(a, b);

		feLog("%s\n", print(a).c_str());
		feLog("%s\n", print(b).c_str());
		feLog("%s\n", print(c).c_str());

		c += a;
		feLog("%s\n", print(c).c_str());

		printfloats(c.temp());

		c[2]= -1.2;
		printfloats(c.temp());

		Vector3f f1(3.0f,4.0f,5.0f);
		Vector3f f2(6.0f,7.0f,8.0f);
		Vector3f f3=f1+f2;
		feLog("%s\n",print(f3).c_str());
		UNIT_TEST(equivalent(f3,Vector3f(9.0f,11.0f,13.0f),1e-3f));

		Vector4f v1(3.0f,4.0f,5.0f);
		Vector4f v2(6.0f,7.0f,8.0f);
		Vector4f v3=v1+v2;
		feLog("%s\n",print(v3).c_str());
		UNIT_TEST(equivalent(v3,Vector4f(9.0f,11.0f,13.0f),1e-3f));

		Vector2f v4(11.0f,12.0f);
		Vector4f v5;
		v5=v4;
		feLog("%s\n",print(v5).c_str());
		UNIT_TEST(equivalent(v5,Vector4f(11.0f,12.0f),1e-3f));

		F32 dp=dot(v1,v2);
		F32 goal=18.0f+28.0f+40.0f;
		feLog("dot %.6f vs %.6f\n",dp,goal);
		UNIT_TEST(fabs(dp-goal)<1e-3f);

		Array<Vector2f> s(4);
		s[0][0] = 0.0;
		s[1][0] = 1.0;
		s[2][0] = 2.0;
		s[3][0] = 3.0;
		s[0][1] = 1.0;
		s[1][1] = 0.0;
		s[2][1] = 1.0;
		s[3][1] = 1.0;

		float x, y, dy;
		for(x = 0.0; x <= 5.0; x += 0.4)
		{
			polyInterp(s, x, y, dy);
			feLog("%f %f\n", x, y);
		}

		completed=TRUE;
	}
	catch(Exception &e)
	{
		e.log();
	}

	UNIT_TEST(completed);
	UNIT_TRACK(5);
	UNIT_RETURN();
}

