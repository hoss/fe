/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __math_Vector2_h__
#define __math_Vector2_h__

namespace fe
{

typedef Vector<2,double>	Vector2d;
typedef Vector<2,float>		Vector2f;
typedef Vector<2,FE_UWORD>	Vector2u;
typedef Vector<2,I32>		Vector2i;
typedef Vector<2,Real>		Vector2;

template<>
inline Vector<2,F32>::Vector(void)
{
	m_data[0]=0.0f;
	m_data[1]=0.0f;
}

template<>
template<typename U>
inline Vector<2,F32>::Vector(const Vector<2,U>& other)
{
	m_data[0]=other[0];
	m_data[1]=other[1];
}

template<class T>
inline Vector<2,T>& set(Vector<2,T> &lhs)
{
	lhs[0]=(T)0.0;
	lhs[1]=(T)0.0;
	return lhs;
}

template<class T, typename U, typename V>
inline Vector<2,T>& set(Vector<2,T> &lhs, U x, V y=0.0)
{
	lhs[0]=(T)x;
	lhs[1]=(T)y;
	return lhs;
}

template<class T>
inline Vector<2,T> operator+(const Vector<2,T> &lhs,const Vector<2,T> &rhs)
{
	return Vector<2,T>(lhs[0]+rhs[0],lhs[1]+rhs[1]);
}

template<class T>
inline Vector<2,T> operator-(const Vector<2,T> &lhs,const Vector<2,T> &rhs)
{
	return Vector<2,T>(lhs[0]-rhs[0],lhs[1]-rhs[1]);
}

// from numerical recipes
template<class T>
inline void polyInterp(const Array<Vector<2,T> > &a_xy, const T &a_x,
		T &a_y, T &a_dy)
{
	I32 ns=0;
	T den, dif, dift, ho, hp, w;

	I32 n = a_xy.size();

	dif = fabs(a_x - a_xy[0][0]);
	Array<T> c(n);
	Array<T> d(n);

	for(I32 i=0; i < n; i++)
	{
		if((dift=fabs(a_x - a_xy[i][0])) < dif)
		{
			ns = i;
			dif = dift;
		}
		c[i] = a_xy[i][1];
		d[i] = a_xy[i][1];
	}
	a_y = a_xy[ns--][1];
	for(I32 m = 1; m < n; m++)
	{
		for(I32 i = 0; i < n-m; i++)
		{
			ho=a_xy[i][0]-a_x;
			hp=a_xy[i+m][0]-a_x;
			w=c[i+1]-d[i];
			if( (den=ho-hp) == 0.0 ) { feX("polyInterp failure"); }
			den=w/den;
			d[i]=hp*den;
			c[i]=ho*den;
		}
		a_y += (a_dy=( 2*ns < (n-m-1) ? c[ns+1] : d[ns--] ));
	}
}

} /* namespace */


#endif /* __math_Vector2_h__ */
