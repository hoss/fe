/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <math/math.pmh>

#define FE_DAG_DEBUG	FALSE

#define FE_DAG_VALIDATION	(FE_CODEGEN<FE_DEBUG)

#if FE_DAG_VALIDATION
#define FE_DAG_VALIDATE()	validate()
#else
#define FE_DAG_VALIDATE()
#endif

namespace fe
{

DAGNode::DAGNode(void)
{
#if FE_COUNTED_TRACK
	setName("DAGNode");
	registerRegion(this,sizeof(*this));
#endif
}

DAGNode::~DAGNode(void)
{
#if FE_COUNTED_TRACK
	deregisterRegion(this);
#endif
}

void DAGNode::validate(void) const
{
	//* verify that each parent references this node exactly once

	List< hp<Connection> >::Iterator parentIt(
			*const_cast< List< hp<Connection> >* >(&m_parentConnections));
	hp<Connection> hpConnection;
	while((hpConnection= *parentIt++).isValid())
	{
		FEASSERT(hpConnection->count()>0);

		List< sp<Connection> >::Iterator childIt(
				hpConnection->parent()->m_childConnections);
		sp<Connection> spConnection;
		U32 count=0;
		while((spConnection= *childIt++).isValid())
		{
			FEASSERT(spConnection->count()>0);

			if(spConnection->child().raw()==this)
			{
				count++;
			}
		}
		if(count!=1)
		{
			feX("DAGNode::validate",
					"parent connection has %d back references",count);
		}
	}

	List< sp<Connection> >::Iterator childIt(
			*const_cast< List< sp<Connection> >* >(&m_childConnections));
	sp<Connection> spConnection;
	while((spConnection= *childIt++).isValid())
	{
		FEASSERT(spConnection->count()>0);

		List< hp<Connection> >::Iterator parentIt(
				spConnection->child()->m_parentConnections);
		hp<Connection> hpConnection;
		U32 count=0;
		while((hpConnection= *parentIt++).isValid())
		{
			FEASSERT(hpConnection->count()>0);

			if(hpConnection->parent().raw()==this)
			{
				count++;
			}
		}
		if(count!=1)
		{
			feX("DAGNode::validate",
					"child connection has %d back references",count);
		}
	}
}

List<String>::Iterator DAGNode::parentConnector(void)
{
	FE_DAG_VALIDATE();
	return m_parentConnectors;
}

List<String>::Iterator DAGNode::childConnector(void)
{
	FE_DAG_VALIDATE();
	return m_childConnectors;
}

U32 DAGNode::parentConnectorCount(void) const
{
	FE_DAG_VALIDATE();
	return m_parentConnectors.size();
}

U32 DAGNode::childConnectorCount(void) const
{
	FE_DAG_VALIDATE();
	return m_childConnectors.size();
}

String DAGNode::parentConnector(U32 a_index)
{
	FE_DAG_VALIDATE();
	return m_parentConnectors[a_index];
}

String DAGNode::childConnector(U32 a_index)
{
	FE_DAG_VALIDATE();
	return m_childConnectors[a_index];
}

List< hp<DAGNode::Connection> >::Iterator DAGNode::parentConnection(void)
{
	FE_DAG_VALIDATE();
	return m_parentConnections;
}

List< sp<DAGNode::Connection> >::Iterator DAGNode::childConnection(void)
{
	FE_DAG_VALIDATE();
	return m_childConnections;
}

U32 DAGNode::parentConnectionCount(void) const
{
	FE_DAG_VALIDATE();
	return m_parentConnections.size();
}

U32 DAGNode::childConnectionCount(void) const
{
	FE_DAG_VALIDATE();
	return m_childConnections.size();
}

sp<DAGNode::Connection> DAGNode::parentConnection(U32 a_index) const
{
	FE_DAG_VALIDATE();
	return m_parentConnections[a_index];
}

sp<DAGNode::Connection> DAGNode::childConnection(U32 a_index) const
{
	FE_DAG_VALIDATE();
	return m_childConnections[a_index];
}

void DAGNode::addParentConnector(const String a_connector)
{
	FE_DAG_VALIDATE();
	ListCore::Context context;
	if(!m_parentConnectors.searchForElement(context,a_connector))
	{
		m_parentConnectors.append(a_connector);
	}
}

void DAGNode::addChildConnector(const String a_connector)
{
	FE_DAG_VALIDATE();
	ListCore::Context context;
	if(!m_childConnectors.searchForElement(context,a_connector))
	{
		m_childConnectors.append(a_connector);
	}
}

BWORD DAGNode::hasParentConnector(String a_connector)
{
	FE_DAG_VALIDATE();
	ListCore::Context context;
	return m_parentConnectors.searchForElement(context,a_connector);
}

BWORD DAGNode::hasChildConnector(String a_connector)
{
	FE_DAG_VALIDATE();
	ListCore::Context context;
	return m_childConnectors.searchForElement(context,a_connector);
}

BWORD DAGNode::attachTo(sp<DAGNode> spNode,
	String a_localConnector,String a_remoteConnector)
{
	FE_DAG_VALIDATE();

	List< sp<Connection> >::Iterator childIt(spNode->m_childConnections);
	sp<Connection> spConnection;
	while((spConnection= *childIt++).isValid())
	{
		if(spConnection->child()==sp<DAGNode>(this))
		{
			feLog("DAGNode::attachTo node already attached\n");
			return FALSE;
		}
	}

	sp<Connection> spNewConnection(new Connection(sp<DAGNode>(this),spNode,
			a_localConnector,a_remoteConnector));

	spNode->m_childConnections.append(spNewConnection);
	spNode->m_childMap[a_remoteConnector]=spNewConnection;

	m_parentConnections.append(spNewConnection);
	m_parentMap[a_localConnector]=spNewConnection;

	FE_DAG_VALIDATE();
#if FE_DAG_VALIDATION
	spNode->validate();
#endif
	return TRUE;
}

BWORD DAGNode::detachFrom(sp<DAGNode> spNode)
{
#if FE_DAG_DEBUG
	feLog("DAGNode::detachFrom %p of %p\n",spNode.raw(),this);
#endif

	FE_DAG_VALIDATE();

	List< sp<Connection> >::Iterator childIt(spNode->m_childConnections);
	sp<Connection> spConnection;
	while((spConnection= *childIt++).isValid())
	{
		if(spConnection->child()==sp<DAGNode>(this))
		{
			const String childConnector=spConnection->childConnector();
			spNode->m_childConnections.remove(spConnection);
			spNode->m_childMap.erase(childConnector);

#if FE_DAG_DEBUG
			feLog("  at child \"%s\"\n",childConnector.c_str());
#endif
			break;
		}
	}

	List< hp<Connection> >::Iterator parentIt(m_parentConnections);
	hp<Connection> hpConnection;
	while((hpConnection= *parentIt++).isValid())
	{
		if(hpConnection->parent()==spNode)
		{
			const String parentConnector=hpConnection->parentConnector();
			m_parentConnections.remove(hpConnection);
			m_parentMap.erase(parentConnector);

#if FE_DAG_DEBUG
			feLog("  at parent \"%s\"\n",parentConnector.c_str());
#endif
			break;
		}
	}

	FE_DAG_VALIDATE();
#if FE_DAG_VALIDATION
	spNode->validate();
#endif
	return TRUE;
}

void DAGNode::detach(void)
{
	FE_DAG_VALIDATE();
	List< hp<Connection> >::Iterator parentIt(m_parentConnections);
	hp<Connection> hpConnection;
	while((hpConnection= *parentIt++).isValid())
	{
		sp<DAGNode> spParent=hpConnection->parent();
		detachFrom(spParent);

#if FE_DAG_VALIDATION
		spParent->validate();
#endif
	}

	m_parentConnections.removeAll();
	m_parentMap.clear();

	FE_DAG_VALIDATE();
}

BWORD DAGNode::detach(String a_localConnector)
{
	std::map< String, hp<Connection> >::const_iterator it=
			m_parentMap.find(a_localConnector);
	if(it==m_parentMap.end())
	{
		return FALSE;
	}
	hp<Connection> hpConnection=it->second;
	FEASSERT(hpConnection.isValid());

	sp<DAGNode> spParent=hpConnection->parent();
	spParent->m_childConnections.remove(hpConnection);
	spParent->m_childMap.erase(hpConnection->parentConnector());

	m_parentConnections.remove(hpConnection);
	m_parentMap.erase(a_localConnector);

	FE_DAG_VALIDATE();
#if FE_DAG_VALIDATION
	spParent->validate();
#endif
	return TRUE;
}

sp<DAGNode::Connection> DAGNode::parentConnection(
	String a_parentConnector) const
{
	FE_DAG_VALIDATE();

	std::map< String, hp<Connection> >::const_iterator it=
			m_parentMap.find(a_parentConnector);
	if(it==m_parentMap.end())
	{
		return sp<Connection>(NULL);
	}

	return it->second;
}

sp<DAGNode::Connection> DAGNode::childConnection(String a_childConnector) const
{
	FE_DAG_VALIDATE();

	std::map< String, sp<Connection> >::const_iterator it=
			m_childMap.find(a_childConnector);
	if(it==m_childMap.end())
	{
		return sp<Connection>(NULL);
	}
	return it->second;
}

} // namespace
