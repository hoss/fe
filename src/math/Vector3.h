/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __math_Vector3_h__
#define __math_Vector3_h__

namespace fe
{

typedef Vector<3,double>	Vector3d;
typedef Vector<3,float>		Vector3f;
typedef Vector<3,FE_UWORD>	Vector3u;
typedef Vector<3,int>		Vector3i;
typedef Vector<3,Real>		Vector3;

template<class T>
inline Vector<3,T> &cross3(Vector<3,T> &r, const Vector<3,T> &lhs,
		const Vector<3,T> &rhs)
{
	r[0] = (lhs[1] * rhs[2] - lhs[2] * rhs[1]);
	r[1] = (lhs[2] * rhs[0] - lhs[0] * rhs[2]);
	r[2] = (lhs[0] * rhs[1] - lhs[1] * rhs[0]);
	return r;
}

template<class T>
inline Vector<3,T> cross3(const Vector<3,T> &lhs, const Vector<3,T> &rhs)
{
	Vector<3,T> v;
	return cross3(v, lhs, rhs);
}

template<class T>
inline Vector<3,T> &cross(Vector<3,T> &r, const Vector<3,T> &lhs,
		const Vector<3,T> &rhs)
{
	return cross3(r, lhs, rhs);
}

template<class T>
inline Vector<3,T> cross(const Vector<3,T> &lhs, const Vector<3,T> &rhs)
{
	return cross3(lhs, rhs);
}

template<class T>
inline Vector<3,T>& set(Vector<3,T> &lhs)
{
	lhs[0]=T(0);
	lhs[1]=T(0);
	lhs[2]=T(0);
	return lhs;
}

template<class T, typename U>
inline Vector<3,T>& set(Vector<3,T> &lhs, U x)
{
	lhs[0]=(T)x;
	lhs[1]=T(0);
	lhs[2]=T(0);
	return lhs;
}

template<class T, typename U, typename V>
inline Vector<3,T>& set(Vector<3,T> &lhs, U x, V y)
{
	lhs[0]=(T)x;
	lhs[1]=(T)y;
	lhs[2]=T(0);
	return lhs;
}

template<class T, typename U, typename V, typename W>
inline Vector<3,T>& set(Vector<3,T> &lhs, U x, V y, W z)
{
	lhs[0]=(T)x;
	lhs[1]=(T)y;
	lhs[2]=(T)z;
	return lhs;
}

//* TEMP
#if TRUE
typedef Vector<4,F64>		Vector4d;
typedef Vector<4,F32>		Vector4f;
typedef Vector<4,FE_UWORD>	Vector4u;
typedef Vector<4,int>		Vector4i;
typedef Vector<4,Real>		Vector4;

template<class T>
inline Vector<4,T> &cross3(Vector<4,T> &r, const Vector<4,T> &lhs,
		const Vector<4,T> &rhs)
{
	r[0] = (lhs[1] * rhs[2] - lhs[2] * rhs[1]);
	r[1] = (lhs[2] * rhs[0] - lhs[0] * rhs[2]);
	r[2] = (lhs[0] * rhs[1] - lhs[1] * rhs[0]);
	r[3] = T(0);
	return r;
}

template<class T>
inline Vector<4,T> cross3(const Vector<4,T> &lhs, const Vector<4,T> &rhs)
{
	Vector<4,T> v;
	return cross3(v, lhs, rhs);
}

template<class T>
inline Vector<4,T>& set(Vector<4,T> &lhs)
{
	lhs[0]=T(0);
	lhs[1]=T(0);
	lhs[2]=T(0);
	lhs[3]=T(0);
	return lhs;
}

template<class T, typename U>
inline Vector<4,T>& set(Vector<4,T> &lhs, U x)
{
	lhs[0]=(T)x;
	lhs[1]=T(0);
	lhs[2]=T(0);
	lhs[3]=T(0);
	return lhs;
}

template<class T, typename U, typename V>
inline Vector<4,T>& set(Vector<4,T> &lhs, U x, V y)
{
	lhs[0]=(T)x;
	lhs[1]=(T)y;
	lhs[2]=T(0);
	lhs[3]=T(0);
	return lhs;
}

template<class T, typename U, typename V, typename W>
inline Vector<4,T>& set(Vector<4,T> &lhs, U x, V y, W z)
{
	lhs[0]=(T)x;
	lhs[1]=(T)y;
	lhs[2]=(T)z;
	lhs[3]=T(0);
	return lhs;
}

template<class T, typename U, typename V, typename W,typename X>
inline Vector<4,T>& set(Vector<4,T> &lhs, U x, V y, W z, X w)
{
	lhs[0]=(T)x;
	lhs[1]=(T)y;
	lhs[2]=(T)z;
	lhs[3]=(T)w;
	return lhs;
}
#endif

} /* namespace */


#endif /* __math_Vector3_h__ */
