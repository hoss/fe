/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __math_Vector4_h__
#define __math_Vector4_h__

namespace fe
{

typedef Vector<4,F64>	Vector4d;
typedef Vector<4,F32>	Vector4f;
typedef Vector<4,FE_UWORD>	Vector4u;
typedef Vector<4,int>	Vector4i;
typedef Vector<4,Real>	Vector4;

/// @brief Partially specialized 4-component vector intended for spatial use
template<typename T>
class Vector<4,T>
{
	public:

	class Raw
	{
		public:
				Raw(const Vector<4,T>& vector)
				{	m_array[0]=vector[0];
					m_array[1]=vector[1];
					m_array[2]=vector[2];
					m_array[3]=vector[3]; }

				operator const T*(void) const
				{	return m_array; }

		private:
			T	m_array[4];
	};

						Vector(void)
						{
//							m_data[3] = T(0); /* make 3D assumption valid */
						}
						Vector(const Vector<4,T>& other)
						{	operator=(other); }
						template<typename U>
						Vector(const Vector<4,U>& other)
						{	operator=(other); }
						Vector(const Vector<3,T>& other)
						{	operator=(other); }
						template<typename U>
						Vector(const Vector<3,U>& other)
						{	operator=(other); }
						Vector(const Vector<2,T>& other)
						{	operator=(other); }
						template<typename U>
						Vector(const Vector<2,U>& other)
						{	operator=(other); }
						Vector(const T* array)
						{	operator=(array); }
						Vector(T x,T y,T z=T(0),T w=T(0))
						{	set4(x,y,z,w); }

		Vector<4,T>&	operator=(const Vector<4,T>& other)
#if FE_EXPLOIT_MEMCPY
						{	memcpy(m_data,other.m_data,4*sizeof(T));
							return *this; }
#else
						{	return set4(other[0],other[1],other[2],other[3]); }
#endif
		template<typename U>
		Vector<4,T>&	operator=(const Vector<4,U>& other)
						{	return set4((T)other[0],(T)other[1],(T)other[2],
									(T)other[3]); }

		Vector<4,T>&	operator=(const Vector<3,T>& other)
#if FE_EXPLOIT_MEMCPY
						{	memcpy(m_data,other.m_data,3*sizeof(T));
							m_data[3]=T(0);
							return *this; }
#else
						{	return set4(other[0],other[1],other[2],T(0)); }
#endif
		template<typename U>
		Vector<4,T>&	operator=(const Vector<3,U>& other)
						{	return set4((T)other[0],(T)other[1],
							(T)other[2],T(0)); }

		Vector<4,T>&	operator=(const Vector<2,T>& other)
#if FE_EXPLOIT_MEMCPY
						{	memcpy(m_data,other.m_data,2*sizeof(T));
							m_data[2]=T(0);
							m_data[3]=T(0);
							return *this; }
#else
						{	return set4(other[0],other[1],T(0),T(0)); }
#endif
		template<typename U>
		Vector<4,T>&	operator=(const Vector<2,U>& other)
						{	return set4((T)other[0],(T)other[1],
							(T)other[2],T(0)); }

		Vector<4,T>&	operator=(const T* array)
#if FE_EXPLOIT_MEMCPY
						{	memcpy(m_data,array,4*sizeof(T));
							return *this; }
#else
						{	return set4(array[0],array[1],array[2],array[3]); }
#endif


const	T&				operator[](U32 index) const;
		T&				operator[](U32 index);

		Vector<4,T>&	set4(T x=T(0),T y=T(0),T z=T(0),T w=T(0));

		void			get4(T array[4]);

const	Raw				temp(void) const
						{	return *this; }

	protected:
		T				m_data[4];
};

template<class T>
inline const T& Vector<4,T>::operator[](U32 index) const
{
	return m_data[index];
}

/** @brief Access internal component

	Warning, this provides internal access and should
	be limited to immediate use only. */
template<class T>
inline T &Vector<4,T>::operator[](U32 index)
{
	return m_data[index];
}

template<typename T>
inline Vector<4,T>& Vector<4,T>::set4(T x,T y,T z,T w)
{
	m_data[0]=x;
	m_data[1]=y;
	m_data[2]=z;
	m_data[3]=w;
	return *this;
}

template<typename T>
inline void Vector<4,T>::get4(T array[4])
{
	array[0]=m_data[0];
	array[1]=m_data[1];
	array[2]=m_data[2];
	array[3]=m_data[3];
}

/**	Set components
	@relates Vector<4,T>
	*/
template<typename T>
inline Vector<4,T> &set(Vector<4,T> &r)
{
	r.set4(T(0),T(0),T(0),T(0));
	return r;
}

/**	Set components
	@relates Vector<4,T>
	*/
template<typename T,typename U>
inline Vector<4,T> &set(Vector<4,T> &r,U x)
{
	r.set4(x,T(0),T(0),T(0));
	return r;
}

/**	Set components
	@relates Vector<4,T>
	*/
template<typename T,typename U,typename V>
inline Vector<4,T> &set(Vector<4,T> &r,U x,V y)
{
	r.set4(x,y,T(0),T(0));
	return r;
}

/**	Set components
	@relates Vector<4,T>
	*/
template<typename T,typename U,typename V,typename W>
inline Vector<4,T> &set(Vector<4,T> &r,U x,V y,W z)
{
	r.set4(x,y,z,T(0));
	return r;
}

/**	Set components
	@relates Vector<4,T>
	*/
template<typename T,typename U,typename V,typename W,typename X>
inline Vector<4,T> &set(Vector<4,T> &r,U x,V y,W z,X w)
{
	r.set4(x,y,z,w);
	return r;
}

/**	Set indexed component
	@relates Vector<4,T>
	*/
template<typename T,typename U>
inline Vector<4,T> &setAt(Vector<4,T> &lhs,U32 index,U value)
{
	lhs[index]=value;
	return lhs;

//	T array[4];
//	lhs.get4(array);
//	array[index]=value;
//	return lhs=array;
}

/** Return the number of elements
	@relates Vector<4,T>
	*/
template<typename T>
inline U32 size(const Vector<4,T> &lhs)
{
	return 4;
}

/**	Add to Vector in place
	@relates Vector<4,T>
	*/
template<typename T>
inline Vector<4,T> &operator+=(Vector<4,T> &lhs,const Vector<4,T> &rhs)
{
	return set(lhs,lhs[0]+rhs[0],lhs[1]+rhs[1],lhs[2]+rhs[2],lhs[3]+rhs[3]);
}

/**	Subtract from Vector in place
	@relates Vector<4,T>
	*/
template<typename T>
inline Vector<4,T> &operator-=(Vector<4,T> &lhs,const Vector<4,T> &rhs)
{
	return set(lhs,lhs[0]-rhs[0],lhs[1]-rhs[1],lhs[2]-rhs[2],lhs[3]-rhs[3]);
}

/**	Negate the Vector
	@relates Vector<4,T>
	*/
template<typename T>
inline Vector<4,T> operator-(const Vector<4,T> &rhs)
{
	Vector<4,T> v;
	return set(v,-rhs[0],-rhs[1],-rhs[2],-rhs[3]);
}

/**	Independently scale components in place
	@relates Vector<4,T>
	*/
template<typename T>
inline Vector<4,T> &operator*=(Vector<4,T> &lhs,const Vector<4,T> &rhs)
{
	return set(lhs,lhs[0]*rhs[0],lhs[1]*rhs[1],lhs[2]*rhs[2],lhs[3]*rhs[3]);
}

/**	Uniformly scale components in place
	@relates Vector<4,T>
	*/
template<typename T,typename U>
inline Vector<4,T> &operator*=(Vector<4,T> &lhs,U scale)
{
	return set(lhs,lhs[0]*scale,lhs[1]*scale,lhs[2]*scale,lhs[3]*scale);
}

/**	Return dot product
	@relates Vector<4,T>
	*/
template<typename T>
inline T dot(const Vector<4,T> &lhs,const Vector<4,T> &rhs)
{
	return lhs[0]*rhs[0]+lhs[1]*rhs[1]+lhs[2]*rhs[2]+lhs[3]*rhs[3];
}

/**	Return square of the Vector length
	@relates Vector<4,T>
	*/
template<typename T>
inline T magnitudeSquared(const Vector<4,T> &rhs)
{
	return rhs[0]*rhs[0]+rhs[1]*rhs[1]+rhs[2]*rhs[2]+rhs[3]*rhs[3];
}

/**	Return the Vector length
	@relates Vector<4,T>
	*/
template<typename T>
inline T magnitude(const Vector<4,T> &rhs)
{
	return sqrt(magnitudeSquared(rhs));
}

/**	Return the Vector direction scaled to unit length
	@relates Vector<4,T>
	*/
template<typename T>
inline Vector<4,T> unit(const Vector<4,T> &vector)
{
	T mag=magnitude(vector);
	if(mag==T(0))
	{
		feX("unit(Vector<4,T>)","attempt to normalize zero magnitude vector");
	}
	return vector*T(1.0/mag);
}

/**	Return the Vector direction scaled to unit length with zero check
	@relates Vector<4,T>
	*/
template<typename T>
inline Vector<4,T> unitSafe(const Vector<4,T> &vector)
{
	T mag=magnitude(vector);
	if(mag>T(0))
	{
		return vector*(T(1)/mag);
	}
	return vector;
}

/**	Scale Vector to unit length
	@relates Vector<4,T>
	*/
template<typename T>
inline Vector<4,T> &normalize(Vector<4,T> &vector)
{
	T mag=magnitude(vector);
	if(mag==T(0))
		feX("normal","attempt to normalize zero magnitude vector");
	return vector*=T(1.0/mag);
}

/**	Scale Vector to unit length with zero check
	@relates Vector<4,T>
	*/
template<typename T>
inline Vector<4,T> &normalizeSafe(Vector<4,T> &vector)
{
	T mag=magnitude(vector);
	if(mag>T(0))
	{
		vector*=T(1.0/mag);
	}
	return vector;
}

/**	Return text describing the Vector's state
	@relates Vector<4,T>
	*/
template<typename T>
inline String print(const Vector<4,T> &vector)
{
	String s;
	s.sPrintf("%g %g %g %g",vector[0],vector[1],vector[2],vector[3]);
	return s;
}

/**	Return sum of Vectors
	@relates Vector<4,T>
	*/
template<typename T>
inline Vector<4,T> operator+(const Vector<4,T> &lhs,const Vector<4,T> &rhs)
{
	Vector<4,T> v;
	return set(v,lhs[0]+rhs[0],lhs[1]+rhs[1],lhs[2]+rhs[2],lhs[3]+rhs[3]);
}

/**	Return difference of Vectors
	@relates Vector<4,T>
	*/
template<typename T>
inline Vector<4,T> operator-(const Vector<4,T> &lhs,const Vector<4,T> &rhs)
{
	Vector<4,T> v;
	return set(v,lhs[0]-rhs[0],lhs[1]-rhs[1],lhs[2]-rhs[2],lhs[3]-rhs[3]);
}

/**	Return a Vector of products of each component
	@relates Vector<4,T>
	*/
template<typename T>
inline Vector<4,T> operator*(const Vector<4,T> &lhs,const Vector<4,T> &rhs)
{
	Vector<4,T> v;
	return set(v,lhs[0]*rhs[0],lhs[1]*rhs[1],lhs[2]*rhs[2],lhs[3]*rhs[3]);
}

/**	Return a uniformly scale Vector (pre)
	@relates Vector<4,T>
	*/
template<typename T,typename U>
inline
typename boost::enable_if<boost::is_arithmetic<U>,Vector<4,T> >::type
operator*(const U lhs, const Vector<4,T> &rhs)
{
	Vector<4,T> v;
	return set(v,lhs*rhs[0],lhs*rhs[1],lhs*rhs[2],lhs*rhs[3]);
}

/**	Return a uniformly scale Vector (post)
	@relates Vector<4,T>
	*/
template<typename T,typename U>
inline
typename boost::enable_if<boost::is_arithmetic<U>,Vector<4,T> >::type
operator*(const Vector<4,T> &lhs, const U rhs)
{
	Vector<4,T> v;
	return set(v,lhs[0]*rhs,lhs[1]*rhs,lhs[2]*rhs,lhs[3]*rhs);
}

/**	Return a cross product of Vectors
	@relates Vector<4,T>
	*/
template<typename T>
inline Vector<4,T> &cross3(Vector<4,T> &r, const Vector<4,T> &lhs,
		const Vector<4,T> &rhs)
{
	set(r,	lhs[1] * rhs[2] - lhs[2] * rhs[1],
			lhs[2] * rhs[0] - lhs[0] * rhs[2],
			lhs[0] * rhs[1] - lhs[1] * rhs[0]);
	return r;
}

/**	Set the Vector as a cross product of Vectors
	@relates Vector<4,T>
	*/
template<typename T>
inline Vector<4,T> cross3(const Vector<4,T> &lhs, const Vector<4,T> &rhs)
{
	Vector<4,T> v;
	return cross3(v, lhs, rhs);
}

} /* namespace */

#endif /* __math_Vector4_h__ */

