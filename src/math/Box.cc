/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <math/math.pmh>

namespace fe
{

//* cylinder
const Box3 aabb(const Vector3f& location,const Vector3f& span,
		F32 baseRadius,F32 endRadius)
{
	const Vector3f end(location+span);

	Vector3f min;
	Vector3f max;
	set(min);
	set(max);

	const F32 scale=1.0f/maximum(0.01f,magnitude(span));

	const F32 cosa0=scale*magnitude(Vector2f(span[1],span[2]));
	const F32 bc0=baseRadius*cosa0;
	const F32 ec0=endRadius*cosa0;
	min[0]=minimum(location[0]-bc0,end[0]-ec0);
	max[0]=maximum(location[0]+bc0,end[0]+ec0);

	const F32 cosa1=scale*magnitude(Vector2f(span[0],span[2]));
	const F32 bc1=baseRadius*cosa1;
	const F32 ec1=endRadius*cosa1;
	min[1]=minimum(location[1]-bc1,end[1]-ec1);
	max[1]=maximum(location[1]+bc1,end[1]+ec1);

	const F32 cosa2=scale*magnitude(Vector2f(span[0],span[1]));
	const F32 bc2=baseRadius*cosa2;
	const F32 ec2=endRadius*cosa2;
	min[2]=minimum(location[2]-bc2,end[2]-ec2);
	max[2]=maximum(location[2]+bc2,end[2]+ec2);

	return Box3(min,max-min);
}

//* sphere
const Box3 aabb(const Vector3f& location,F32 radius)
{
	F32 radius2=radius+radius;
	return Box3(Vector3f(location[0]-radius,location[1]-radius,
			location[2]-radius),Vector3f(radius2,radius2,radius2));
}

} // namespace
