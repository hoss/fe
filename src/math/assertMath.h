/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __math_AssertMath_h__
#define __math_AssertMath_h__

namespace fe
{

FE_DL_EXPORT void FE_CDECL assertMath(sp<TypeMaster> spTypeMaster);

} /* namespace */

#endif /* __math_AssertMath_h__ */
