/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __math_Vector_gnu_h__
#define __math_Vector_gnu_h__

#if FE_SSE>=1 && FE_COMPILER==FE_GNU

#ifdef __GNUC__
#if __GNUC__ > 3 || ( __GNUC__ == 3 && __GNUC_MINOR__ >= 4)

#if FE_MEM_ALIGNMENT<16
#error FE_SSE requires FE_MEM_ALIGNMENT>=16
#endif

namespace fe
{

//typedef float v4sf __attribute__ ((mode(V4SF)));
typedef float v4sf __attribute__ ((vector_size (16)));

namespace {
inline void v4sf_assertAlignment(const v4sf& value)
{
#if FE_CODEGEN<FE_PROFILE
	if(long(&value)%16)
	{
		feX("v4sf_assertAlignment","v4sf misalignment");
	}
#endif
}
inline void v4sf_setSame(v4sf& value,const F32 element)
{
	reinterpret_cast<F32*>(&value)[0]=element;
	value=__builtin_ia32_shufps(value,value,0x00);
}
} // namespace

inline void v4sf_log(String text,const v4sf& value);
} // namespace fe

#define FE_VDIM	3
#include "VectorNf_gnu.h"
#undef FE_VDIM

#define FE_VDIM	4
#include "VectorNf_gnu.h"
#undef FE_VDIM

namespace fe
{
namespace {
inline void v4sf_log(String text,const v4sf& value)
{
	const F32* v=reinterpret_cast<const F32*>(&value);
	feLog("%s %s\n",print(Vector<4,F32>(v)).c_str(),text.c_str());
}
} // namespace
} // namespace fe

#endif /* __GNUC__ >= 3 && __GNUC_MINOR__ >= 4 */
#endif /* __GNUC__ */

#endif // FE_SSE>=1 && FE_COMPILER==FE_GNU

#endif /* __math_Vector_gnu_h__ */
