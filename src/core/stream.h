/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __core_stream_h__
#define __core_stream_h__

namespace fe
{

#if 0
void FE_DL_PUBLIC FE_CORE_PORT skip(std::istream &istrm, FE_UWORD cnt);

void FE_DL_PUBLIC FE_CORE_PORT output(std::ostream &ostrm, const String &s);
void FE_DL_PUBLIC FE_CORE_PORT output(std::ostream &ostrm, U8 u8);
void FE_DL_PUBLIC FE_CORE_PORT output(std::ostream &ostrm, U32 u32);
void FE_DL_PUBLIC FE_CORE_PORT output(std::ostream &ostrm, I32 i32);

void FE_DL_PUBLIC FE_CORE_PORT input(std::istream &istrm, String &s);
void FE_DL_PUBLIC FE_CORE_PORT input(std::istream &istrm, U8 &u8);
void FE_DL_PUBLIC FE_CORE_PORT input(std::istream &istrm, U32 &u32);
void FE_DL_PUBLIC FE_CORE_PORT input(std::istream &istrm, I32 &i32);
#endif

inline
void skip(std::istream &istrm, FE_UWORD cnt)
{
	char c;
	while(cnt--)
	{
		istrm.get(c);
	}
}

inline
void output(std::ostream &ostrm, const String &s)
{
	s.output(ostrm);
}

inline
void output(std::ostream &ostrm, U8 u8)
{
	ostrm.write((char *)&u8, sizeof(U8));
}

inline
void output(std::ostream &ostrm, U32 u32)
{
	U32 n_ul = htonl(u32);
	ostrm.write((char *)&n_ul, sizeof(U32));
}

inline
void output(std::ostream &ostrm, I32 i32)
{
	U32 n_ul = htonl(*(U32 *)&i32);
	ostrm.write((char *)&n_ul, sizeof(U32));
}

inline
void input(std::istream &istrm, String &s)
{
	s.input(istrm);
}

inline
void input(std::istream &istrm, U8 &u8)
{
	istrm.read((char *)&u8, sizeof(U8));
}

inline
void input(std::istream &istrm, U32 &u32)
{
	U32 n_ul;
	istrm.read((char *)&n_ul, sizeof(U32));
	u32 = ntohl(n_ul);
}

inline
void input(std::istream &istrm, I32 &i32)
{
	U32 n_ul;
	istrm.read((char *)&n_ul, sizeof(U32));
	n_ul = ntohl(n_ul);
	i32 = *(I32 *)&n_ul;
}

} /* namespace */


#endif /* __core_stream_h__ */

