/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __core_Array_h__
#define __core_Array_h__

namespace fe
{

/**************************************************************************//**
	@brief Wrapper for std::vector

	@ingroup core

	Allows bounds-checking just for debug builds.
*//***************************************************************************/
template<typename T>
class FE_DL_EXPORT Array: public std::vector<T>
{
	public:

explicit		Array(void)													{}

explicit		Array(U32 a_size):
						std::vector<T>(a_size)								{}

explicit		Array(U32 a_size,const T& a_value):
						std::vector<T>(a_size,a_value)						{}

				Array(const Array& a_other):
						std::vector<T>(a_other)								{}

				Array(const std::vector<T>& a_other):
						std::vector<T>(a_other)								{}

		Array&	operator=(const Array& a_other)
				{	return (Array&)std::vector<T>::operator=(a_other); }

#if FE_CODEGEN<=FE_DEBUG
const		T&	operator[](I32 index) const
			{
				try
				{
					return std::vector<T>::at(index);
				}
				catch(const std::exception& std_e)
				{
					String location;
					location.sPrintf("Array<%s>::operator[%d] const",
							FE_TYPESTRING(T).c_str(),index);
					feX(location.c_str(),std_e.what());
				}
				catch(...)
				{
					feX("Array::operator[] const","unrecognized exception");
				}
				//* NOTE should never get this far
				return std::vector<T>::at(index);
			}

			T&	operator[](I32 index)
			{
				try
				{
					return std::vector<T>::at(index);
				}
				catch(const std::exception& std_e)
				{
					String location;
					location.sPrintf("Array<%s>::operator[%d]",
							FE_TYPESTRING(T).c_str(),index);
					feX(location.c_str(),std_e.what());
				}
				catch(...)
				{
					feX("Array::operator[]","unrecognized exception");
				}
				//* NOTE should never get this far
				return std::vector<T>::at(index);
			}
#endif

};

} // namespace

#endif // __core_Array_h__
