/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __core_Tracker_h__
#define __core_Tracker_h__

namespace fe
{

/**************************************************************************//**
	@brief Watches the usage of reference counted objects

	@ingroup core

	Counted has debugging mechanisms that manipulate a Tracker
	during validation.  It can be printed using Counted::reportTracker().

	A major singleton (like Registry in the plugin mode) may
	set up and maintain another Tracker automatically
	(such as for Component) while in an appropiate debugging mode.
*//***************************************************************************/
class FE_DL_EXPORT Tracker: public ObjectSafe<Tracker>
{
	public:
							Tracker(String name): m_name(name)				{}
virtual						~Tracker(void);

		void				suppress(void *pPtr);

		void				acquire(void *pPtr,String name,U32 newcount)
							{	change(FALSE,pPtr,name,newcount); }
		void				release(void *pPtr,String name,U32 newcount)
							{	change(TRUE,pPtr,name,newcount); }

		void				addReference(void* pPtr,void* pReference,
									String what)
							{	changeReference(FALSE,pPtr,pReference,what); }
		void				removeReference(void* pPtr,void* pReference)
							{	changeReference(TRUE,pPtr,pReference,
										String()); }

							/** @brief Register a block of memory

							pPtr is the object as a common form,
							such as Counted.
							pT is the start of the actually allocated block. */
		void				registerRegion(void* pPtr,void* pT,U32 bytes,
									String name);
		void				deregisterRegion(void* pPtr);

							/** @brief Generate a report of current
								object usage */
		String				report(void) const;
		U32					totalCount(void) const;

	private:

	class FE_DL_PUBLIC Track
	{
		public:
							Track(void):
									m_count(0),
									m_base(0x0),
									m_size(0),
									m_suppress(false),
									m_referencesFound(0)		{}

			U32						m_count;
			void*					m_base;
			U32						m_size;
			BWORD					m_suppress;
			U32						m_referencesFound;
			String					m_name;
			std::map<void*,String>	m_references;
	};

		void				change(BWORD release,void *pPtr,
									String name,U32 newcount);
		void				changeReference(BWORD remove,void* pPtr,
									void* pReference,String what);
		String				reportAt(std::map<void*,Track>::const_iterator it,
									std::map<void*,Track>::const_iterator
									itRoot,U32 depth,BWORD& rCycled) const;

		std::map<void*,Track>	m_trackMap;
		String					m_name;
};

} /* namespace */

#endif /* __core_Tracker_h__ */
