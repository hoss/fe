/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <core/core.pmh>

namespace fe
{

FE_DL_PUBLIC const IWORD BaseType::Info::c_implicit = -1;
FE_DL_PUBLIC const IWORD BaseType::Info::c_explicit = -2;
FE_DL_PUBLIC const IWORD BaseType::Info::c_noascii = -1;
FE_DL_PUBLIC const IWORD BaseType::Info::c_ascii = -2;

BaseType::BaseType(void)
{
#if FE_OS==FE_LINUX && FE_CHEAP_CHECKABLE
	U8 current_stack;
	if((void*)this > (void*)&current_stack)
	{
		feX(	"fe::BaseType::BaseType",
				"attempt to create Type object on stack %p > %p",
				this,&current_stack);
	}
#endif
	suppressReport();
}

BaseType::~BaseType(void)
{
}

void BaseType::setInfo(const sp<BaseType::Info> &spInfo)
{
	SAFEGUARDCLASS;
	m_spInfo=spInfo;
}

void BaseType::setInfo(Info *pInfo)
{
	SAFEGUARDCLASS;
	m_spInfo=pInfo;
}

sp<BaseType::Info> BaseType::getInfo(void)
{
	SAFEGUARDCLASS;
	return m_spInfo;
}

bool BaseType::getConstruct(void) const
{
	if(m_spInfo.isValid())
	{
		return m_spInfo->getConstruct();
	}
	return false;
}

void BaseType::construct(void *instance)
{
	if(m_spInfo.isValid())
	{
		m_spInfo->construct(instance);
	}
}

void BaseType::destruct(void *instance)
{
	if(m_spInfo.isValid())
	{
		m_spInfo->destruct(instance);
	}
}

BWORD BaseType::copy(void *instance,const void* source)
{
	if(!m_spInfo.isValid())
	{
		return FALSE;
	}
	return m_spInfo->copy(instance,source);
}

TypeInfo &BaseType::typeinfo(void)
{
	return m_typeInfo;
}


} /* namespace */

FE_DL_EXPORT fe::Logger* gs_feLogger=NULL;

