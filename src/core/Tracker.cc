/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <core/core.pmh>

#define	FE_TRACK_TRACE			FALSE
#define	FE_TRACK_DEBUG			FALSE

#define	FE_TRACK_EXCEPTION		TRUE
#define	FE_TRACK_SUPPRESSIONS	TRUE
#define	FE_ASSERT_NAMES			FALSE	// turn on to find unnamed entries

namespace fe
{

Tracker::~Tracker(void)
{
}

U32 Tracker::totalCount(void) const
{
	SAFEGUARD;
	U32 sum=0;

	std::map<void*,Track>::const_iterator it=m_trackMap.begin();
	while(it!=m_trackMap.end())
	{
		if(!it->second.m_suppress)
		{
			sum+=it->second.m_count;
		}
		it++;
	}

	return sum;
}

void Tracker::suppress(void *pPtr)
{
#if FE_TRACK_SUPPRESSIONS
	SAFEGUARD;
	std::map<void*,Track>::iterator it=m_trackMap.find(pPtr);
	if(it==m_trackMap.end())
	{
		change(false,pPtr,"",0);
		it=m_trackMap.find(pPtr);
	}
	if(it!=m_trackMap.end())
		it->second.m_suppress=true;
#endif
}

void Tracker::change(BWORD release,void *pPtr,String name,U32 newcount)
{
	SAFEGUARD;

	std::map<void*,Track>::iterator it=m_trackMap.find(pPtr);
	if(it==m_trackMap.end())
	{
#if FE_TRACK_TRACE==FALSE
		if(newcount>2)
#endif
			feLog("%s Tracker %p new%s%2d \"%s\"\n",m_name.c_str(),pPtr,
					release? "--": "++",newcount,name.c_str());
		if(!release || newcount)
		{
			Track& track=m_trackMap[pPtr];
			track.m_name=name;
			track.m_count=newcount;
		}
	}
	else
	{
#if FE_TRACK_TRACE==FALSE
		if(it->second.m_count && newcount &&
				it->second.m_count+(release? -1: 1) != newcount)
#endif
			feLog("%s Tracker %p %2d %s%2d (%d) \"%s\"\n",m_name.c_str(),pPtr,
					it->second.m_count,
					release? "--": "++",newcount,
					it->second.m_references.size(),name.c_str());
#if FE_TRACK_EXCEPTION
		if(it->second.m_count && newcount &&
				it->second.m_count+(release? -1: 1) != newcount)
			feX("Tracker::change","mistrack (thread collision?)");
#endif
		it->second.m_name=name;
		it->second.m_count=newcount;
		if(release && !newcount)
			m_trackMap.erase(it);

#if FE_ASSERT_NAMES
	//* Please supply a name for your Counted object!
	if(release)
		FEASSERT(!name.empty() && name!="Counted");
#endif
	}

//	if(name=="Scope in")
//	{
//		feLogBacktrace();
//	}
}

void Tracker::registerRegion(void* pPtr,void* pT,U32 size,String name)
{
	SAFEGUARD;
	Track& track=m_trackMap[pPtr];
	track.m_base=pT;
	track.m_size=size;
	track.m_name=name;
}

void Tracker::deregisterRegion(void* pPtr)
{
	SAFEGUARD;
	std::map<void*,Track>::iterator it=m_trackMap.find(pPtr);
	if(it!=m_trackMap.end())
		m_trackMap.erase(it);
}

void Tracker::changeReference(BWORD remove,void* pPtr,void* pReference,
		String what)
{
	SAFEGUARD;
	std::map<void*,Track>::iterator it=m_trackMap.find(pPtr);
	if(it==m_trackMap.end())
		return;

#if FE_TRACK_TRACE
	feLog("Tracker::changeReference %p -> %d (%d%s) ref %p \"%s\"\n",
			pPtr,it->second.m_count, it->second.m_references.size(),
			remove? "--": "++",pReference,
			remove? it->second.m_references[pReference].c_str(): what.c_str());
#endif

	String name=remove? it->second.m_references[pReference].c_str():
			what.c_str();
//	if(name=="sp<> Scope in")
//	{
//		feLogBacktrace();
//	}

	if(remove)
		it->second.m_references.erase(pReference);
	else
		it->second.m_references[pReference]=what;
}

String Tracker::report(void) const
{
	SAFEGUARD;
	if(m_trackMap.empty())
		return "Tracker: all clear\n";

	String message="Tracker's outstanding objects:\n";
	String suppressions;

	U32 suppressed=0;
	U32 outstanding=0;
	std::map<void*,Track>::const_iterator it=m_trackMap.begin();
	while(it!=m_trackMap.end())
	{
		U32& referencesFound=
				*const_cast<U32*>(&it->second.m_referencesFound);
		referencesFound=0;

		if(it->second.m_suppress)
		{
			if(it->second.m_count)
			{
				suppressions.catf("\"%s\"\n",it->second.m_name.c_str());
				suppressed++;
			}
			it++;
			continue;
		}

		outstanding++;

		message.catf("%08p ",it->first);
		if(it->second.m_references.size()!=it->second.m_count)
			message.catf("%d/",it->second.m_references.size());
		message.catf("%d \"%s\"",it->second.m_count,it->second.m_name.c_str());
		if(it->second.m_size)
			message.catf(" (%p+%d bytes)",it->second.m_base,it->second.m_size);
		message.cat("\n");
		const std::map<void*,String>& references=it->second.m_references;
		std::map<void*,String>::const_iterator itRef=references.begin();
		while(itRef!=references.end())
		{
			void* pReference=itRef->first;
			BWORD found=false;

			std::map<void*,Track>::const_iterator it2=m_trackMap.begin();
			while(it2!=m_trackMap.end())
			{
				if( pReference==it2->first ||
						(pReference>=it2->second.m_base &&
						(FE_UWORD)pReference<
						(FE_UWORD)(it2->second.m_base)+
						(FE_UWORD)(it2->second.m_size)))
				{
					if(!found)
						message.catf("  %p (%s)",
							pReference,itRef->second.c_str());
					message.catf(" -> %p+%d \"%s\" (%s)",
							it2->second.m_base,
							it2->second.m_size,it2->second.m_name.c_str(),
							itRef->second.c_str());
					found=true;
					referencesFound++;
				}
				it2++;
			}
			if(found)
				message.cat("\n");
#if TRUE
			else
				message.catf("  %p (%s)\n",pReference,itRef->second.c_str());
#endif
			itRef++;
		}

		it++;
	}
	if(!outstanding)
	{
		message.cat("(none)\n");
	}

	message.catf("\nSuppressions: %d\n",suppressed);
	if(suppressed)
	{
		message.cat(suppressions);
	}
	else
	{
		message.cat("(none)\n");
	}

	message.cat("\nHoldouts:\n");

	String holdouts;
	it=m_trackMap.begin();
	while(it!=m_trackMap.end())
	{
#if FE_TRACK_DEBUG
		feLog("Tracker::report %d %s\n",
				it->second.m_referencesFound,
				it->second.m_name.c_str());
#endif
		BWORD cycled=FALSE;
		holdouts.cat(reportAt(it,it,0,cycled));
		it++;
	}
	if(holdouts.empty())
	{
		message.cat("(none)\n");
	}
	else
	{
		message.cat(holdouts);
	}

	return message;
}

String Tracker::reportAt(std::map<void*,Track>::const_iterator it,
		std::map<void*,Track>::const_iterator itRoot,
		U32 depth,BWORD& rCycled) const
{
	SAFEGUARD;
	if(it->second.m_suppress)
		return "";

#if FE_TRACK_DEBUG
	feLog("Tracker::reportAt %s\n",it->second.m_name.c_str());
#endif

	String message;
	BWORD found=false;
	std::map<void*,Track>::const_iterator it2=m_trackMap.begin();
	while(it2!=m_trackMap.end())
	{
		if(it2->second.m_suppress)
		{
			it2++;
			continue;
		}

		const std::map<void*,String>& references=it2->second.m_references;
		std::map<void*,String>::const_iterator itRef=references.begin();
		while(itRef!=references.end())
		{
#if FE_TRACK_DEBUG
			feLog("Tracker::reportAt %p vs %p+%d %s for %p %s\n",
					it->first,
					it2->second.m_base,
					it2->second.m_size,
					it2->second.m_name.c_str(),
					itRef->first,
					itRef->second.c_str());
#endif

			void* pReference=itRef->first;
			if( pReference==it->first ||
					(pReference>=it->second.m_base &&
					(FE_UWORD)pReference<
					(FE_UWORD)(it->second.m_base)+
					(FE_UWORD)(it->second.m_size)))
			{
#if FE_TRACK_DEBUG
				feLog("Tracker::reportAt hit %d %d\n",depth,found);
#endif

				if(!depth && !found)
				{
					message.catf("%d \"%s\" %p %p+%d\n",
							it->second.m_count,
							it->second.m_name.c_str(),
							it->first,
							it->second.m_base,
							it->second.m_size);
				}
				message.catf("%*c%d \"%s\" %d %p %p+%d\n",4*(depth+1),' ',
						it2->second.m_count,
						it2->second.m_name.c_str(),
						it2->second.m_referencesFound,
						it2->first,
						it2->second.m_base,
						it2->second.m_size);
				found=true;

				if( it2->first==itRoot->first ||
						(it2->first>=itRoot->second.m_base &&
						(FE_UWORD)it2->first<
						(FE_UWORD)(itRoot->second.m_base)+
						(FE_UWORD)(itRoot->second.m_size)))
				{
					message.catf("%*c*CYCLE DETECTED*\n",4*(depth+1),' ');
					rCycled=TRUE;
				}
				else if(it2->second.m_referencesFound<2 || depth<4)
				{
					BWORD cycled=FALSE;
					const String subMessage=
							reportAt(it2,itRoot,depth+1,cycled);

					if(it2->second.m_referencesFound<2 || cycled)
					{
						message.cat(subMessage);
					}

					if(cycled)
					{
						rCycled=TRUE;
					}
				}
			}
			itRef++;
		}
		it2++;
	}

	return message;
}

} /* namespace */
