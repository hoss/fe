/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __core_StdAllocator_h__
#define __core_StdAllocator_h__

namespace fe
{

/**************************************************************************//**
	@brief Replacement for std::allocator using the FE memory system

*//***************************************************************************/
template <class T>
class StdAllocator
{
	public:
	typedef FE_UWORD			size_type;
	typedef ptrdiff_t		difference_type;
	typedef T*				pointer;
	typedef const T*		const_pointer;
	typedef T&				reference;
	typedef const T&		const_reference;
	typedef T				value_type;

							StdAllocator() {}
							StdAllocator(const StdAllocator&) {}

		pointer				allocate(size_type n, const void * = 0)
							{
								T* t = (T*) fe::allocate(n * sizeof(T));
								feLog("StdAllocator::allocate %p\n",t);
								return t;
							}

		void				deallocate(void* p, size_type)
							{
								if (p)
								{
//									feLog("StdAllocator::deallocate %p\n",p);
									fe::deallocate(p);
								}
							}

		pointer				address(reference x) const
							{	return &x; }

		const_pointer		address(const_reference x) const
							{	return &x; }

		StdAllocator<T>&	operator=(const StdAllocator&)
							{	return *this; }

		void				construct(pointer p, const T& val)
							{	new ((T*) p) T(val); }

		void				destroy(pointer p)
							{	p->~T(); }

		size_type			max_size() const
							{	return FE_UWORD(-1); }

	template <class U>
	struct rebind
	{ typedef StdAllocator<U> other; };

							template <class U>
							StdAllocator(const StdAllocator<U>&)			{}

							template <class U>
		StdAllocator&		operator=(const StdAllocator<U>&)
							{	return *this; }
};

} /* namespace */

#endif /* __core_StdAllocator_h__ */

