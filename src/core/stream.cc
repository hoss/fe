/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <core/core.pmh>

namespace fe
{

#if 0
void skip(std::istream &istrm, FE_UWORD cnt)
{
	char c;
	while(cnt--)
	{
		istrm.get(c);
	}
}

void output(std::ostream &ostrm, const String &s)
{
	s.output(ostrm);
}

void output(std::ostream &ostrm, U8 u8)
{
	ostrm.write((char *)&u8, sizeof(U8));
}

void output(std::ostream &ostrm, U32 u32)
{
	U32 n_ul = htonl(u32);
	ostrm.write((char *)&n_ul, sizeof(U32));
}

void output(std::ostream &ostrm, I32 i32)
{
	U32 n_ul = htonl(*(U32 *)&i32);
	ostrm.write((char *)&n_ul, sizeof(U32));
}

void input(std::istream &istrm, String &s)
{
	s.input(istrm);
}

void input(std::istream &istrm, U8 &u8)
{
	istrm.read((char *)&u8, sizeof(U8));
}

void input(std::istream &istrm, U32 &u32)
{
	U32 n_ul;
	istrm.read((char *)&n_ul, sizeof(U32));
	u32 = ntohl(n_ul);
}

void input(std::istream &istrm, I32 &i32)
{
	U32 n_ul;
	istrm.read((char *)&n_ul, sizeof(U32));
	n_ul = ntohl(n_ul);
	i32 = *(I32 *)&n_ul;
}
#endif

} /* namespace */

