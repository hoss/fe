/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "fe/core.h"

using namespace fe;

int main(void)
{
	UNIT_START();

	BWORD completed=FALSE;

	try
	{
		FileLog* pFileLog=new FileLog();
		pFileLog->setFilename("output/test/core/xFileLog.log");

		feLogger()->clear(".*");
		feLogger()->clear("file");
		feLogger()->setLog("filelog", pFileLog);
		feLogger()->bind("file", "filelog");

		feLogGroup("file","Begin Log\n");
		feLogGroup("file","End Log\n");

		completed=TRUE;
	}
	catch(Exception &e)	{ e.log(); }
	catch(...)			{ feLogDirect("uncaught exception\n"); }

	UNIT_TEST(completed);
	UNIT_TRACK(1);
	UNIT_RETURN();
}
