/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "fe/core.h"

using namespace fe;

int main(void)
{
	UNIT_START();

	BWORD completed=FALSE;

	try
	{
		std::string stdString("ABCDE0123456789");
		feLogDirect("\"%s\"\n",stdString.c_str());
		stdString="hello world";
		const char* buffer=stdString.c_str();
		feLogDirect("\"%s\"\n",buffer);
		for(I32 m=0;m<15;m++)
		{
			feLogDirect("%p ",buffer[m]);
		}
		feLogDirect("\n\n");

		const I32 loops(1000);
		const I32 arrayCount(100000);

		String feStringArray[arrayCount];
		std::string stdStringArray[arrayCount];

		SystemTicker ticker;

		const char* long_text=
				"This is quite a long line of text."
				"  This is another long line of text."
				"  This is another long line of text."
				"  This is another long line of text."
				"  This is another long line of text."
				"  This is another long line of text.";
		std::string longStdString(long_text);
		String longFeString(long_text);

		U32 time1=ticker.timeMS();
		for(I32 loop=0;loop<loops;loop++)
		{
			for(I32 m=0;m<arrayCount;m++)
			{
				stdStringArray[m]=longStdString;
			}
		}
		U32 time2=ticker.timeMS();
		U32 deltaMS=time2-time1;
		feLogDirect("std copy ms=%d\n",deltaMS);

		time1=ticker.timeMS();
		for(I32 loop=0;loop<loops;loop++)
		{
			for(I32 m=0;m<arrayCount;m++)
			{
				feStringArray[m]=longFeString;
			}
		}
		time2=ticker.timeMS();
		deltaMS=time2-time1;
		feLogDirect("fe copy ms=%d\n",deltaMS);

		for(I32 m=0;m<arrayCount;m++)
		{
			feStringArray[m]="Hello World!";
			stdStringArray[m]="Hello World!";
		}

		for(I32 matchPass=0;matchPass<3;matchPass++)
		{
			const String testName=(matchPass==2)? "resized":
					(matchPass? "unmatching": "matching");

			const String feStringKey=(matchPass==2)? "Hello World":
					(matchPass? "hello world?": "Hello World!");

			const std::string stdStringKey=feStringKey.c_str();


			I32 matches=0;
			time1=ticker.timeMS();
			for(I32 loop=0;loop<loops;loop++)
			{
				for(I32 m=0;m<arrayCount;m++)
				{
					if(stdStringArray[m]==stdStringKey)
					{
						matches++;
					}
				}
			}
			time2=ticker.timeMS();
			deltaMS=time2-time1;
			feLogDirect("std %s matches=%d ms=%d\n",
					testName.c_str(), matches,deltaMS);

			matches=0;
			time1=ticker.timeMS();
			for(I32 loop=0;loop<loops;loop++)
			{
				for(I32 m=0;m<arrayCount;m++)
				{
					if(feStringArray[m]==feStringKey)
					{
						matches++;
					}
				}
			}
			time2=ticker.timeMS();
			deltaMS=time2-time1;
			feLogDirect("fe %s matches=%d ms=%d\n",
					testName.c_str(), matches,deltaMS);
		}

		completed=TRUE;
	}
	catch(Exception &e)	{ e.log(); }
	catch(...)			{ feLogDirect("uncaught exception\n"); }

	UNIT_TEST(completed);
	UNIT_TRACK(1);
	UNIT_RETURN();
}
