/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "fe/core.h"

using namespace fe;

int main(void)
{
	bool completed=false;

	UNIT_START();

	try
	{

		std::vector<I32> std_vector(3);
		std_vector[0]=1;
		std_vector[1]=2;
		std_vector[2]=3;

		Array<I32> fe_array(std_vector);

		fe_array=std_vector;

		std::vector<I32> std_vector2=fe_array;
		UNIT_TEST(std_vector2[0]==1);
		UNIT_TEST(std_vector2[1]==2);
		UNIT_TEST(std_vector2[2]==3);

		completed=true;
	}
	catch(fe::Exception &e)
	{
		e.log();
	}

	UNIT_TEST(completed);
	UNIT_TRACK(4);
	UNIT_RETURN();
}

