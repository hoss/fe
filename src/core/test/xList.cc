/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "fe/core.h"

using namespace fe;

class myAbstract
{
	public:
virtual			~myAbstract(void)											=0;
virtual	void	method(void)												=0;
};

static U32 gs_count=0;
class myProbe
{
	public:
				~myProbe(void)	{ gs_count++; }
};

class myCounted : public Counted
{
	public:
		int a;
};


int main()
{
	UNIT_START();

#if FE_COUNTED_TRACK
	Counted::startTracker();
#endif

	BWORD completed=FALSE;

	try
	{
		const U32 ordering[2][5]= { { 1,2,3,4,5 }, { 1,3,4,2,5 } };

		List<myAbstract*> abstractList;

		feLog("delete probe\n");
		{
			List<myProbe*> list;
			list.setAutoDestruct(TRUE);
			list.append(new myProbe);
		}
		UNIT_TEST(gs_count==1);
		feLog("\n");

		List<U32*> list;
		U32 one=1;
		U32 two=2;
		U32 three=3;
		U32 four=4;
		U32 five=5;

		list.append(&one);
		list.append(&two);
		list.append(&three);
		list.append(&four);
		list.append(&five);

		ListCore::Context context,context2;
		U32 *pEntry;

		UNIT_TEST(list.size()==5);
		U32 m=0;
		list.toHead(context);
		while((pEntry=list.postIncrement(context))!=NULL)
		{
			feLog("%p %d\n",pEntry,*pEntry);
			UNIT_TEST(*pEntry==ordering[0][m++]);
		}

		list.toHead(context);
		list.postIncrement(context);

		list.toHead(context2);
		list.postIncrement(context2);
		list.postIncrement(context2);
		list.postIncrement(context2);

		list.moveNodeAfter(context,context2);

		UNIT_TEST(list.size()==5);
		m=0;
		feLog("\n");
		list.toHead(context);
		while((pEntry=list.postIncrement(context))!=NULL)
		{
			feLog("%p %d\n",pEntry,*pEntry);
			UNIT_TEST(*pEntry==ordering[1][m++]);
		}

		feLog("\niterator\n");
		List<U32*>::Iterator iterator(list);
		iterator.toHead();
		while((pEntry= *iterator++)!=NULL)
			feLog("%08p %d\n",pEntry,*pEntry);
		feLog("\n");
		iterator.toHead();
		pEntry= *iterator;
		feLog("%08p %d\n",pEntry,**iterator);
		pEntry= *(++iterator);
		feLog("%08p %d\n",pEntry,*pEntry);
		**iterator=7;
		pEntry= *iterator--;
		feLog("%08p %d\n",pEntry,*pEntry);
		pEntry= *iterator--;
		feLog("%08p %d\n",pEntry,*pEntry);
		pEntry= *iterator--;
		feLog("%08p\n",pEntry);

		feLog("\nnew List\n");
		List<U32*> *pList=new List<U32*>;
		feLog("append 1\n");
		U32** p1=pList->append(&one);
		feLog("append 2\n");
		U32** p2=pList->append(&two);
		feLog("to head\n");
		pList->toHead(context);
		feLog("delete\n");
		delete pList;

		feLog("\nnew List\n");
		pList=new List<U32*>;
		feLog("append 3\n");
		U32** p3=pList->append(&three);
		feLog("append 4\n");
		U32** p4=pList->append(&four);
		feLog("to tail\n");
		pList->toTail(context);
		feLog("delete\n");
		delete pList;

		feLog("p1=%p p2=%p p3=%p p4=%p\n",p1,p2,p3,p4);
		UNIT_TEST(p1!=p2);
		UNIT_TEST(p1!=p3);
		UNIT_TEST(p1!=p4);
		UNIT_TEST(p2!=p3);
		UNIT_TEST(p2!=p4);
		UNIT_TEST(p3!=p4);

		feLog("\nsp<U32> List (no pointer)\n");
		List<U32> directList;

		directList.append(1);
		directList.append(2);
		directList.append(3);
		directList.append(4);
		directList.remove(2);
		UNIT_TEST(directList.size()==3);

		List<U32>::Iterator diter(directList);
		diter.toHead();
		while(!diter.isAtTailNull())
			feLog("%d\n",*(diter++));

		feLog("\nsp<myCounted> List\n");
		List< sp<myCounted> > ptrList;
		sp<myCounted> spCnt1(new myCounted());
		sp<myCounted> spCnt2(new myCounted());
		sp<myCounted> spCnt3(new myCounted());
		feLog("append\n");
		ptrList.append(spCnt1);
		ptrList.append(spCnt2);
		ptrList.append(spCnt3);
		ptrList.remove(spCnt2);
		UNIT_TEST(ptrList.size()==2);

		feLog("iterator\n");
		List< sp<myCounted> >::Iterator ptrIterator(ptrList);
		sp<Counted> spCounted;
		feLog("toHead\n");
		ptrIterator.toHead();
		feLog("remove\n");
		while(ptrIterator.remove()) {};

		feLog("\n--\n");

		completed=TRUE;
	}
	catch(Exception &e)	{ e.log(); }
	catch(...)			{ feLog("uncaught exception\n"); }

#if FE_COUNTED_TRACK
	feLog("Counted %s\n",Counted::reportTracker().c_str());
	Counted::stopTracker();
#endif

	UNIT_TEST(completed);
	UNIT_TEST(!Counted::trackerCount());
	UNIT_TRACK(23);
	UNIT_RETURN();
}
