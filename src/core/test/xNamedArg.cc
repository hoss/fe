/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "fe/core.h"

using namespace fe;

class Arg
{
	public:
				Arg(char const* a_t,String a_name="")
				{
					m_instance.create<String>(ms_spTypeMaster)=a_t;
					m_name=a_name;
				}
				template<class T>
				Arg(T a_t,String a_name="")
				{
					m_instance.create<T>(ms_spTypeMaster)=a_t;
					m_name=a_name;
				}

		Instance		m_instance;
		String			m_name;
static	sp<TypeMaster>	ms_spTypeMaster;

};

sp<TypeMaster> Arg::ms_spTypeMaster;

void function(Arg* args)
{
	U32 arg=0;
	while(true)
	{
		if(args[arg].m_instance.is<void*>())
		{
			break;
		}
		if(args[arg].m_instance.is<Real>())
		{
			printf("%.6G",args[arg].m_instance.cast<Real>());
		}
		else if(args[arg].m_instance.is<I32>())
		{
			printf("%d",args[arg].m_instance.cast<I32>());
		}
		else if(args[arg].m_instance.is<String>())
		{
			printf("\"%s\"",args[arg].m_instance.cast<String>().c_str());
		}
		else
		{
			printf("UNKNOWN");
		}
		printf(" \"%s\"\n", args[arg].m_name.c_str());
		arg++;
	}
}

int main()
{
	UNIT_START();

#if FE_COUNTED_TRACK
	Counted::startTracker();
#endif

	BWORD completed=FALSE;

	try
	{
		sp<TypeMaster> spTypeMaster(new TypeMaster);
		assertCore(spTypeMaster);
		Arg::ms_spTypeMaster=spTypeMaster;

		function((Arg[]){
				{1.2f,"min"},
				3.4f,
				{5.6f,"max"},
				{"text","label"},
				{7,"count"},
				(void*)NULL });

		completed=TRUE;
	}
	catch(Exception &e)	{ e.log(); }
	catch(...)			{ feLog("uncaught exception\n"); }

#if FE_COUNTED_TRACK
	feLog("Counted %s\n",Counted::reportTracker().c_str());
	Counted::stopTracker();
#endif

	UNIT_TEST(completed);
	UNIT_TEST(!Counted::trackerCount());
	UNIT_TRACK(2);
	UNIT_RETURN();
}
