/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "fe/core.h"

#define PROFILER_FRAMES		100

using namespace fe;

int main(int argc,char** argv)
{
	UNIT_START();

	BWORD completed=FALSE;

	U32 frames=PROFILER_FRAMES;
	if(argc>1)
	{
		frames=atoi(argv[1]);
	}

	try
	{
		sp<Profiler> spProfiler(new Profiler("Test Profile"));
		UNIT_TEST(spProfiler.isValid());

		sp<Profiler::Profile> spStep1(
				new Profiler::Profile(spProfiler,"Step 1"));
		sp<Profiler::Profile> spStep2(
				new Profiler::Profile(spProfiler,"Step 2"));
		sp<Profiler::Profile> spStep3(
				new Profiler::Profile(spProfiler,"Step 3"));
		sp<Profiler::Profile> spStep4(
				new Profiler::Profile(spProfiler,"Step 4"));

		for(U32 m=0;m<frames;m++)
		{
			spProfiler->begin();

			spStep1->start();

			milliSleep(1);

			spStep2->replace(spStep1);

			milliSleep(2);

			spStep2->finish();

			{
				Profiler::Stage stage(spStep3);
				milliSleep(3);
			}

			{
				Profiler::Stage stage(spStep4);
				nanoSpin(U32(100e3));
			}

			spProfiler->end();
		}

		feLog("Profiler %s\n%s\n",spProfiler->name().c_str(),
				spProfiler->report().c_str());

#if FE_CODEGEN<=FE_PROFILE
		const F64 per1=spStep1->microseconds()/spStep1->count();
		const F64 per2=spStep2->microseconds()/spStep2->count();
		const F64 per3=spStep3->microseconds()/spStep3->count();
		const F64 per4=spStep4->microseconds()/spStep4->count();

#if FE_OS==FE_WIN32 || FE_OS==FE_WIN64
		// Win32 timing is pretty sloppy
		UNIT_TEST(per1>900.0 && per1<2900.0);
		UNIT_TEST(per2>1900.0 && per2<3900.0);
		UNIT_TEST(per3>2900.0 && per3<4900.0);
		UNIT_TEST(per4>100.0 && per4<105.0);
#else
		UNIT_TEST(per1>950.0 && per1<1100.0);
		UNIT_TEST(per2>1900.0 && per2<2200.0);
		UNIT_TEST(per3>2850.0 && per3<3300.0);
		UNIT_TEST(per4>100.0 && per4<102.0);
#endif
#endif

		completed=TRUE;
	}
	catch(fe::Exception &e) { e.log(); }
	catch(...) { feLog("uncaught exception\n"); }

	UNIT_TEST(completed);
	UNIT_TRACK(FE_CODEGEN<=FE_PROFILE? 6: 2);
	UNIT_RETURN();
}

