/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "fe/core.h"

using namespace fe;

int main()
{
	UNIT_START();

#if FE_COUNTED_TRACK
	Counted::startTracker();
#endif

	BWORD completed=FALSE;

	try
	{
		sp<TypeMaster> spTypeMaster(new TypeMaster);
		assertCore(spTypeMaster);

		InstanceMap instanceMap(spTypeMaster);

		instanceMap.property<String>("label")="Float Value";
		instanceMap.property<Real>("value")=1.2;

		printf("label = \"%s\"\n",
				instanceMap.property<String>("label").c_str());
		UNIT_TEST(instanceMap.property<String>("label")=="Float Value");

		printf("value = \"%.6G\"\n",
				instanceMap.property<Real>("value"));
		UNIT_TEST(instanceMap.property<Real>("value")-1.2 < 1e-6);

		completed=TRUE;
	}
	catch(Exception &e)	{ e.log(); }
	catch(...)			{ feLog("uncaught exception\n"); }

#if FE_COUNTED_TRACK
	feLog("Counted %s\n",Counted::reportTracker().c_str());
	Counted::stopTracker();
#endif

	UNIT_TEST(completed);
	UNIT_TEST(!Counted::trackerCount());
	UNIT_TRACK(4);
	UNIT_RETURN();
}
