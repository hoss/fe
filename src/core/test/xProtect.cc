/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "fe/core.h"

using namespace fe;

class MyProtect:
	public Protectable,
	public CastableAs<MyProtect>
{
	public:
					MyProtect(void):
						m_data(0)											{}

virtual	MyProtect*	clone(Protectable* pInstance=NULL)
					{
						MyProtect* pMyProtect=
								pInstance? fe_cast<MyProtect>(pInstance):
								new MyProtect();

						Protectable::clone(pMyProtect);
						pMyProtect->m_data=m_data;
						return pMyProtect;
					}

		I32			m_data;
};

#define TEST_PROTECTED(SAME,PROTECTED,COPY)									\
		printf("protect %p %d:%d %d  copy %p %d:%d %d\n",					\
				cpProtect.raw(),cpProtect.protection(),						\
				cpProtect->protection(),									\
				cpProtect->m_data,											\
				cpCopy.raw(),cpCopy.protection(),							\
				cpCopy->protection(),										\
				cpCopy->m_data);											\
		UNIT_TEST(SAME==(cpProtect.raw()==cpCopy.raw()));					\
		UNIT_TEST(cpProtect->m_data==PROTECTED);							\
		UNIT_TEST(cpCopy->m_data==COPY);


int main(void)
{
	bool completed=false;

	UNIT_START();

	try
	{
		cp<MyProtect> cpProtect(new MyProtect());
		cp<MyProtect> cpCopy(cpProtect);
		TEST_PROTECTED(TRUE,0,0);

//		cpCopy->m_data=7;		compile error

		cpCopy.writable()->m_data=7;
		TEST_PROTECTED(TRUE,7,7);

		cpProtect.protect();
		cpProtect.writable()->m_data=5;
		TEST_PROTECTED(TRUE,5,5);

		cpCopy.writable()->m_data=3;
		TEST_PROTECTED(FALSE,5,3);

		cpCopy=cpProtect;
		TEST_PROTECTED(TRUE,5,5);

		const MyProtect* pWas=cpCopy.raw();
		cpProtect=new MyProtect();
		TEST_PROTECTED(FALSE,0,5);

		cpCopy.writable()->m_data=9;
		TEST_PROTECTED(FALSE,0,9);
		UNIT_TEST(cpCopy.raw()==pWas);

		completed=true;
	}
	catch(Exception &e)
	{
		e.log();
	}

	UNIT_TEST(completed);
	UNIT_TRACK(23);
	UNIT_RETURN();
}
