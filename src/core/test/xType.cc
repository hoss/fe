/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "fe/core.h"

class DummyClass
{
	public:
		DummyClass(void)
		{
			m_data = 123;
		}
virtual	~DummyClass(void)
		{
		}
		bool operator == (const DummyClass &other) const
		{
			return (m_data == other.m_data);
		}
		FE_UWORD	m_data;
};

using namespace fe;

int main(void)
{
	fe::UnitTest unitTest;

	fe::sp<fe::BaseType> pType(new fe::Type<DummyClass>);

	fe::sp<fe::BaseType::Info> pDangle(
		new TypeInfoMemcpyIO<DummyClass>);
	fe::sp<fe::BaseType::Info> pInfoer(
		new TypeInfoConstructable<DummyClass>);

	pType->setInfo(pDangle);
	pType->setInfo(pInfoer);

	void *instance = fe::allocate(pType->size());

	DummyClass *pDummy = static_cast<DummyClass *>(instance);

	// this trips up valgrind, and I do not want to suppression it
	//feLog("before construct %u\n", pDummy->m_data);

	if(pType->getConstruct())
	{
		pType->construct(instance);
	}

	unitTest(123 == pDummy->m_data, "construction test");

	fe::deallocate(instance);

	return unitTest.failures();
}

