/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "fe/core.h"

int main(void)
{
	fe::UnitTest unitTest;

	fe::RecursiveMutex mutex;
	{
		fe::RecursiveMutex::Guard guard(mutex);
	}

	unitTest(true,"guard");

	return unitTest.failures();
}

