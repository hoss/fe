/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "fe/core.h"

using namespace fe;

class A
{
	public:
		A(void)
		{
			m_data = 123;
		}
virtual	~A(void)
		{
		}
		bool operator == (const A &other) const
		{
			return (m_data == other.m_data);
		}
		FE_UWORD	m_data;
};

class B : public A
{
};

class BToA : public TypeConversion
{
	public:
		InstanceAllocation	operator()(void *in,void *&out)
		{
			out = new A();
			((A *)out)->m_data = ((B *)in)->m_data;
			return e_copy;
		}
};

int main(void)
{
	UnitTest unitTest;
	BWORD completed=false;

	try
	{
		sp<TypeMaster> typeMaster(new TypeMaster);

		sp<BaseType> spTypeA = typeMaster->assertType<A>("A");
		sp<BaseType> spTypeA2 = typeMaster->assertType<A>("A2");
		sp<BaseType> spTypeB = typeMaster->assertType<B>("B");
		typeMaster->assertType<I32>("integer");
		typeMaster->assertType<U32>("unsigned");
		typeMaster->assertType<F32>("float");
		typeMaster->assertType<F32>("float");
		typeMaster->assertType<F32>("real");
		typeMaster->assertType<F32>();

		// convienient conversion registration
		typeMaster->registerConversion<BToA,B,A>();

		// raw conversion registration
		typeMaster->registerConversion(sp<TypeConversion>(new BToA()),
				spTypeB,spTypeA);

		// default converter
		typeMaster->registerConversion<TypeConversion,U32,I32>();

		// lookup conversion that isnt there (should leave an INVALID in dump)
		typeMaster->lookupConversion(spTypeA,spTypeB);

		sp<TypeConversion> spConverter =
				typeMaster->lookupConversion<U32,I32>();

		I32 i = 10;
		U32 u = 20;
		U32 *pU = &u;
		(*spConverter)((void *)&i,(void *&)pU);
		feLog("should be 10: %d\n", *pU);
		unitTest(10 == *pU, "conversion");

		// register a native conversion
		typeMaster->nativeConversion<F32,I32>();

		I32 *pI;
		F32 f = 42.1;
		spConverter = typeMaster->lookupConversion<F32,I32>();
		TypeConversion::InstanceAllocation rc =
				(*spConverter)((void *)&f,(void *&)pI);
		feLog("should be 42: %d\n", *pI);
		unitTest(42 == *pI, "conversion");
		if( rc == TypeConversion::e_copy )
		{
			delete pI;
		}

		Instance instance;
		instance.create<int>(typeMaster);
		float a_float = 1.23;
		instance.set(typeMaster, a_float);
		feLog("should be 1.23: %f\n", instance.cast<float>());

		Peeker peek;
		peek(*typeMaster);
		feLog(peek.output().c_str());
		completed=true;
	}
	catch(Exception& e)
	{
		e.log();
	}

	unitTest(completed,"completed");
	return unitTest.failures();
}

