/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "fe/core.h"

using namespace fe;

#define CNT 100000

class Item
{
	public:
		Item(void)
		{
			m_val = 17;
		}
		void set(int val)
		{
			m_val = val;
		}
		int get(void)
		{
			return m_val;
		}
	private:
		int m_val;
};

template<typename t_allocator, typename T>
bool validity_test(void)
{
	bool valid = true;
	t_allocator xallocator;
	Allocator &allocator = xallocator;
	Array<T *> v(CNT);

	for(int i = 0; i < CNT; i++)
	{
		v[i] = new(allocator.allocate(sizeof(T))) T;
		v[i]->set(i);
	}

	for(int i = 0; i < CNT; i++)
	{
		if(i != v[i]->get())
		{
			valid = false;
		}
	}

	for(int i = 0; i < CNT; i++)
	{
		allocator.deallocate(v[i]);
	}

	return valid;
}


template<typename t_allocator, typename T>
bool speed_test(const String &a_name)
{
	SystemTicker ticker(a_name.c_str());
	ticker.log("pre");

	t_allocator *allocator = new t_allocator();
	ticker.log("allocator construction");

	Array<T *> v(CNT);
	ticker.log();

	for(int i = 0; i < CNT; i++)
	{
		v[i] = new(allocator->allocate(sizeof(T))) T;
		v[i]->set(i);
	}
	ticker.log("allocate");

	for(int i = 0; i < CNT; i++)
	{
		allocator->deallocate(v[i]);
	}
	ticker.log("deallocate");

	delete allocator;
	ticker.log("allocator destruction");


	static Array<int> g_test_array;
	allocator = new t_allocator();
	if(g_test_array.size() == 0)
	{
		for(int i = 0; i < 20; i++)
		{
			g_test_array.push_back(
				(int)((float)CNT*((float)rand()/(float)RAND_MAX)));
		}
	}
	ticker.log();

	for(int i_ta = (int)g_test_array.size() - 1; i_ta >= 0; i_ta--)
	{
		for(int i = 0; i < g_test_array[i_ta]; i++)
		{
			v[i] = new(allocator->allocate(sizeof(T))) T;
			v[i]->set(i);
		}
		for(int i = 0; i < g_test_array[i_ta]; i++)
		{
			allocator->deallocate(v[i]);
		}
	}
	ticker.log("usage simulation");

	delete allocator;
	ticker.log("--------------------");

	return true;
}


int main(void)
{
	bool completed=false;

	UNIT_START();

	try
	{
		UNIT_TEST(true);

		UNIT_TEST((validity_test<PoolAllocator, Item>()));

		UNIT_TEST((speed_test<PoolAllocator, Item>("pool")));

		UNIT_TEST((speed_test<BasicAllocator, Item>("basic")));


		completed=true;
	}
	catch(fe::Exception &e)
	{
		e.log();
	}

	UNIT_TEST(completed);
	UNIT_TRACK(5);
	UNIT_RETURN();
}

