/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include "fe/core.h"

class A : public fe::Counted, fe::CastableAs<A>
{
	public:
		int a;
};

class B : public A, fe::CastableAs<B>
{
	public:
		double b;
};

class C : public fe::Counted, fe::CastableAs<C>
{
	public:
		float c;
};

int main(void)
{
	const bool heap_check=FE_PTR_STACK_CHECK;
	bool completed=false;

	UNIT_START();

	try
	{
		fe::sp<A> spA(new A);
		spA->a=10;
		UNIT_TEST(spA.isValid());
		UNIT_TEST(spA.raw()->count()==1);

		std::list<void *> plist;

		plist.push_back( (void *)spA.raw() );

		fe::sp<A> spA2;
		std::list<void *>::iterator it = plist.begin();
		UNIT_TEST(!spA2.isValid());
		spA2=reinterpret_cast<A *>(*it);
		UNIT_TEST(spA2.isValid());
		UNIT_TEST(spA.raw()->count()==2);

		UNIT_TEST(spA->a==10);
		spA2->a=20;
		UNIT_TEST(spA->a==20);

		spA=NULL;
		UNIT_TEST(!spA.isValid());
		UNIT_TEST(spA2.isValid());
		UNIT_TEST(spA2.raw()->count()==1);

		fe::sp<B> spB(new B);
		UNIT_TEST(spB.isValid());
		UNIT_TEST(spB.raw()->count()==1);
		spA=spB;
		UNIT_TEST(spA.isValid());
		UNIT_TEST(spB.raw()->count()==2);

		fe::sp<B> spB2(spA);
		UNIT_TEST(spB2.isValid());
		UNIT_TEST(spB2.raw()->count()==3);
		spB2=NULL;

		spB=NULL;
		UNIT_TEST(!spB.isValid());
		UNIT_TEST(spA.raw()->count()==1);
		spB=spA2;
		UNIT_TEST(!spB.isValid());
		UNIT_TEST(spA2.raw()->count()==1);

		//* needs to be a compile error
#if FALSE
		A* pA=new A;
		spB=pA;
#endif

		spA=NULL;
		spA2=NULL;

		if(heap_check)
		{
			feLog("Heap check: exception expected\n");
			bool excepted=false;
			try
			{
				A stackA;
				spA=&stackA;
			}
			catch(fe::Exception &e)
			{
				feLog("FE exception\n");
				excepted=true;
			}
			catch(...)
			{
				feLog("generic exception\n");
				excepted=true;
			}
			UNIT_TEST(excepted);
			UNIT_TEST(!spA.isValid());
		}

		B stackB;
		stackB.b=7;
		UNIT_TEST(stackB.b==7);

		completed=true;
	}
	catch(fe::Exception &e)
	{
		e.log();
	}

	UNIT_TEST(completed);
	UNIT_TRACK(22+2*heap_check);
	UNIT_RETURN();
}

