/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <core/core.pmh>

namespace fe
{

PoolAllocator::PoolAllocator()
{
	m_max = 256;
	m_stride = sizeof(FE_UWORD);
	m_nfList = new void*[m_max+1];

	for(U32 i = 0;i<=m_max;i++)
	{
		m_nfList[i] = NULL;
	}
}

PoolAllocator::PoolAllocator(FE_UWORD stride, FE_UWORD slots)
{
	m_max = slots;
	m_stride = stride;
	m_nfList = new void*[m_max+1];

	for(U32 i = 0;i<=m_max;i++)
	{
		m_nfList[i] = NULL;
	}
}

PoolAllocator::~PoolAllocator(void)
{
	for(U32 i = 0;i<=m_max;i++)
	{
		while(m_nfList[i])
		{
			void *tmp = ((FE_PA_FREE *)(m_nfList[i]))->m_next;
			fe::deallocate((m_nfList[i]));
			m_nfList[i] = tmp;
		}
	}
	delete [] m_nfList;
}


} /* namespace */

