/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */


/**************************************************************************//**
	@brief Implementation of std::istream for raw byte buffer

	@ingroup core

	Usage:
	@code
		MemStream memstream(pBuffer, bufferSize);
	@endcode

	See https://tuttlem.github.io/2014/08/18/getting-istream-to-work-off-a-byte-array.html
*//***************************************************************************/
class MemStream: public std::istream
{
	private:

	class MemBuf: public std::basic_streambuf<char>
	{
		public:
					MemBuf(const uint8_t *p, size_t l)
					{
						setg((char*)p, (char*)p, (char*)p + l);
					}

	};

	public:
				MemStream(const uint8_t *p, size_t l):
					std::istream(&m_memBuf),
					m_memBuf(p, l)
				{
					rdbuf(&m_memBuf);
				}

		MemBuf m_memBuf;
};
