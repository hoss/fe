/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __core_Instance_h__
#define __core_Instance_h__

namespace fe
{

/**	@brief Smart pointer used with types represented by BaseType

	@ingroup core

	This highly specialized smart pointer is designed to deal with instances
	with types represented by BaseType.
	In particular, intended for record attribute
	instances with respect to the script binding.

	Whenever dealing directly with Instances be very careful about reference
	count management.  Instance cannot deal with reference counting unless it
	is explicitly given a pointer to the count itself (via set() or
	construction).  So be sure to manage the memory properly if this cannot be
	done.
	*/
class FE_DL_EXPORT Instance
{
	public:
						Instance(void);
						Instance(void *instance, const sp<BaseType>& rspBT,
								std::atomic<int> *pRefCnt);
						Instance(const Instance &other);
						~Instance(void);

		Instance		&operator=(const Instance &other);

		bool			operator==(const Instance &other);

		void			acquire(void) const;
		void			release(void);
		void			assign(const Instance &rvalue);

		template<class T>
		T				&cast(void) const;

		template<class T>
		bool			is(void) const;

		sp<BaseType>	type(void) const;
		void			*data(void) const;

		void			set(void *instance, const sp<BaseType>& rspBT,
								std::atomic<int> *pRefCnt);
		template<class T>
		T				&set(sp<TypeMaster> spTM, T &a_t);

		template<class T>
		T				&create(sp<TypeMaster> spTM);
		void			create(sp<BaseType> spBT);

		BWORD			unique(void) const
						{	return m_pRefCnt? ((*m_pRefCnt)<2): TRUE; }

	private:
		sp<BaseType>	m_spType;
		void			*m_pData;
		std::atomic<int>				*m_pRefCnt;
};

template<class T>
T &Instance::cast(void) const
{
	if(m_spType.isNull())
	{
		feX("fe::Instance::cast","unknown type");
	}
	if(TypeInfo(getTypeId<T>()) != m_spType->typeinfo())
	{
		feX("fe::Instance::cast",
				"(template '%s') type mismatch casting to '%s' from '%s'",
				FE_TYPESTRING(T).c_str(),
				TypeInfo(getTypeId<T>()).ref().name(),
				m_spType->typeinfo().ref().name());
	}
	return *(reinterpret_cast<T *>(m_pData));
}

template<class T>
bool Instance::is(void) const
{
	if(m_spType.isNull())
	{
		return false;
	}
	if(TypeInfo(getTypeId<T>()) != m_spType->typeinfo())
	{
		return false;
	}
	return true;
}

template<class T>
T &Instance::set(sp<TypeMaster> spTM, T &a_t)
{
	release();
	m_spType = spTM->lookupType(TypeInfo(getTypeId<T>()));
	if(m_spType.isNull())
	{
		feX("fe::Instance::cast","unknown type");
	}
	m_pData = &a_t;
	acquire();
	return a_t;
}

template<class T>
T &Instance::create(sp<TypeMaster> spTM)
{
	release();
	m_spType = spTM->lookupType(TypeInfo(getTypeId<T>()));
	if(m_spType.isNull())
	{
		feX("fe::Instance::cast","unknown type");
	}
	m_pData = allocate(sizeof(T) + sizeof(std::atomic<int>));
	m_pRefCnt = (std::atomic<int> *)((char *)m_pData + sizeof(T));
	*m_pRefCnt = 0;
	if(m_spType->getConstruct())
	{
		m_spType->construct(m_pData);
	}
	acquire();

	return *(reinterpret_cast<T *>(m_pData));
}

/**	Instance print
	@relates Instance
	*/
inline String print(const Instance& a_instance)
{
	return a_instance.type()->getInfo()->print(a_instance.data());
}

} /* namespace */
#endif /* __core_Instance_h__ */

