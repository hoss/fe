/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */


/**************************************************************************//**
	@brief Base class that prevents further derivation

	@ingroup core

	Usage:
	@code
		class MyFinal: virtual Final< MyFinal >
		{
		}
	@endcode

	By making it virtual, the most derived class calls the constructor,
	which is hidden from all but the immediate derived class.

	Note that this requires changes to the standard that may still be pending.
	Otherwise, it is an error.
*//***************************************************************************/
template<typename T>
class Final
{
			Final(void)														{}

friend	class T;
};
