/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __core_Poison_h__
#define __core_Poison_h__

namespace fe
{

/**	Death pill for threads.
	*/
class FE_DL_EXPORT Poison : public ObjectSafe<Poison>
{
	public:
		Poison(void);
virtual	~Poison(void);

		bool	active(void);
		void	start(void);
		void	stop(void);

	private:
		bool	m_active;
};

/**	An exception to be thrown when active Poison has been detected. */
class FE_DL_EXPORT Poisoned //: public Exception
{
	public:
		Poisoned(void);
virtual	~Poisoned(void);
};


} /* namespace */


#endif /* __core_Poison_h__ */

