/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __core_Handled_h__
#define __core_Handled_h__

namespace fe
{

class FE_DL_EXPORT HandleToken: public Counted
{
	public:
							HandleToken(void):
#if FE_COUNTED_TRACK
									m_name("HandleToken"),
#endif
									m_valid(TRUE)
							{	suppressReport(); }

virtual						~HandleToken(void)								{}

		BWORD				valid(void)						{ return m_valid; }
		void				invalidate(void)
							{	suppressReport();
								m_valid=FALSE; }
#if FE_COUNTED_TRACK
	public:
const	String&				name(void) const				{ return m_name; }
virtual	void				setName(String name)
							{
								RecursiveMutex::Guard guard(
										ms_counted_mutex);
								m_name.sPrintf("HandleToken %s",name.c_str());
							}
	private:
		String				m_name;
#endif
	private:
		BWORD				m_valid;
};

/**************************************************************************//**
	@brief Safe handle for shared pointer

	@ingroup core

	Unlike sp<>, the pointed to object can easily become invalid
	since the handle's existance doesn't prop up the object's existance.
	Unless there is another mechanism, such as a known sp<>,
	that is known to guarantee the object's persistence,
	it would be wise to judiciously verify currency with isValid().

	A hp<> can be automatically cast to a sp<> (smart pointer).

	A sp<> based on a Handled object can be automatically cast to a hp<>.
*//***************************************************************************/
template<class T>
class FE_DL_EXPORT hp
{
	public:
					hp(void)											{}

					hp(const T* pT)
					{
						operator=(pT);
					}

					template<class X>
					hp(const X* pX)
					{
						operator=(pX);
					}

					hp(const hp<T>& rhpT)
					{	m_spHandleToken=rhpT.m_spHandleToken;
						m_pObject=rhpT.m_pObject; }

					template<class X>
					hp(const sp<X>& rspX)
					{	operator=(rspX->getHandle()); }

					template<class X>
					hp(const hp<X>& rhpX)
					{	m_spHandleToken=rhpX.handleToken();
						m_pObject=fe_cast<T>(rhpX.raw()); }

					/** @brief Initialize during Handled construction

						@internal */
		void		initialize(T* pObject)
					{	m_spHandleToken=new HandleToken();
						m_pObject=pObject; }

					/** @brief Converts from a smart pointer of any type

						If the object in the smart pointer does not have
						a method @em getHandle returning a hp<>,
						the conversion should fail to compile. */
					template<class X>
const	hp<T>&		operator=(const sp<X>& rspX)
					{
						if(rspX.isValid())
							operator=(rspX->getHandle());
						else
						{
							m_spHandleToken=NULL;
							m_pObject=NULL;
						}
						return *this;
					}

//*					NOTE no auto-conversion from hp<X> to hp<T> (use an sp<X>)
/*					template<class X>
const	hp<T>&		operator=(const hp<X>& rhpX)
					{	operator=(fe_cast<T>(rhpX.raw()));
						return *this; }
*/

					template<class X>
const	hp<T>&		operator=(const X* pX)
					{
						if(pX)
							operator=(pX->getHandle());
						else
						{
							m_spHandleToken=NULL;
							m_pObject=NULL;
						}
						return *this;
					}
		hp<T>&		operator=(const hp<T>& rhpT)
					{	m_spHandleToken=rhpT.m_spHandleToken;
						m_pObject=rhpT.m_pObject;
						return *this; }

					// this is so ptr can be used as a std::map key
		BWORD		operator<(const hp<T>& rhpT) const
					{	return (FE_UWORD)(m_pObject) < (FE_UWORD)(rhpT.m_pObject); }

					/// @brief Returns the handled pointer, potentially NULL
		T*			raw(void) const
					{	if(!handleToken() || !handleToken()->valid())
							return NULL;
						return m_pObject; }

					/// @brief Use the handled pointer
		T*			operator->(void) const
					{
						if(!handleToken() || !handleToken()->valid())
							feX(e_invalidHandle,("fe::hp< "+FE_TYPESTRING(T)+
									" >::operator->").c_str(),
									"attempt to use invalid Handle");
						FEASSERT(m_pObject);
						return m_pObject;
					}

					/// @brief Dereference to the handled pointer
		T&			operator*(void) const
					{
						if(!handleToken() || !handleToken()->valid())
							feX(e_invalidHandle,("fe::hp< "+FE_TYPESTRING(T)+
									" >::operator->").c_str(),
									"attempt to use invalid Handle");
						FEASSERT(m_pObject);
						return *m_pObject;
					}

					/** @brief Returns a smart pointer of the same
						templated type

						You usually don't need to use this method directly
						since sp<> can auto-convert. */
		sp<T>		object(void) const	{ return sp<T>(raw()); }

					/// @brief Returns true if the pointer is set
		BWORD		isValid(void) const
					{	return handleToken() && handleToken()->valid(); }

					/// @brief Returns true if the pointer is not set
		BWORD		isNull(void) const
					{	return !isValid(); }

					/** @brief Nulls the shared pointer

						@internal */
		void		invalidate(void)
					{	if(handleToken())
							handleToken()->invalidate();
						m_spHandleToken=NULL; }

					/** @brief Access the shared token

						@internal */
		HandleToken*	handleToken(void) const
					{	return m_spHandleToken.raw(); }

	private:
		sp<HandleToken>		m_spHandleToken;
		T*					m_pObject;
};

/**************************************************************************//**
	@brief Base class providing an fe::Handle to the derived class
*//***************************************************************************/
template <class T>
class FE_DL_EXPORT Handled: public Protectable
{
	public:
					Handled(void)			{ m_handle.initialize((T*)this); }
virtual				~Handled(void)			{ abandonHandle(); }

					/** @brief Get a safe handle to this object

						Unlike the smart pointer sp<>, this handle does not
						force the object to persist.  Instead, it can be
						checked to determine if it is still valid. */
const	hp<T>&		getHandle(void)	const
					{
#if FE_COUNTED_TRACK
						m_handle.handleToken()->setName(name());
#endif
						return m_handle;
					}

	protected:
		void		abandonHandle(void)		{ m_handle.invalidate(); }

	private:
		hp<T>		m_handle;
};

///	@relates hp
template <class T,class X>
inline BWORD operator==(const hp<T>& rhpLeft,const hp<X>& rhpRight)
{
	return (rhpLeft.handleToken()==rhpRight.handleToken());
}

///	@relates hp
template <class T,class X>
inline BWORD operator==(const hp<T>& rhpLeft,const sp<X>& rspRight)
{
	return (rhpLeft.raw()==rspRight.raw());
}

///	@relates hp
template <class T,class X>
inline BWORD operator==(const sp<X>& rspLeft,const hp<T>& rhpRight)
{
	return (rspLeft.raw()==rhpRight.raw());
}

///	@relates hp
template <class T>
inline BWORD operator!=(const hp<T>& rhpLeft,const hp<T>& rhpRight)
{
	return (rhpLeft.handleToken()!=rhpRight.handleToken());
}

///	@relates hp
template <class T,class X>
inline BWORD operator!=(const hp<T>& rhpLeft,const sp<X>& rspRight)
{
	return (rhpLeft.raw()!=rspRight.raw());
}

///	@relates hp
template <class T,class X>
inline BWORD operator!=(const sp<X>& rspLeft,const hp<T>& rhpRight)
{
	return (rspLeft.raw()!=rhpRight.raw());
}

template<class T>
struct hash_hp
{
		FE_UWORD	operator()(const hp<T>& rhpT) const
				{	return reinterpret_cast<FE_UWORD>(rhpT.raw()); }
};

template<class T>
struct eq_hp
{
		bool	operator()(const hp<T>& rhpT1, const hp<T>& rhpT2) const
				{	return rhpT1 == rhpT2; }
};

template<typename T, typename U>
class AutoHashMap< hp<T>, U >:
	public HashMap< hp<T>, U, hash_hp<T>, eq_hp<T> >						{};

} /* namespace */

#endif /* __core_Handled_h__ */

