/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __core_Delete_h__
#define __core_Delete_h__

namespace fe
{

/**************************************************************************//**
	@fn deleteByType
	@brief Contingent deletion of contents, based on type

	@ingroup core

	Usage:
	@code
		int				x;
		int*			pX=new int();
		int**			array=new int[10]();
		sp<Counted>		counted=new Counted();

		deleteByType(x);				// does nothing
		deleteByType(pX);				// calls delete pX
		deleteByType(array);			// calls delete[] array
		deleteByType(counted);			// does nothing
	@endcode
*//***************************************************************************/

template<typename T>
inline
void	deleteByType(T t)		{ }

inline
void	deleteByType(void* pT)
		{	feLog("deleteByType() deleting void* is undefined\n"); }

template<typename T>
inline
void	deleteByType(T* pT)		{ delete pT; }

template<typename T>
inline
void	deleteByType(T** ppT)	{ delete[] ppT; }

/* NOTE don't actually work, falls back on T
template<typename T>
inline
void	deleteByType(sp<T> spT)	{ }

template<typename T>
inline
void	deleteByType(hp<T> hpT)	{ }
*/

/**************************************************************************//**
	@fn defaultByType
	@brief Returns NULL or closely equivalent value, based on type

	@ingroup core

	Usage:
	@code
		int				x;
		int*			pX=new int();
		int**			array=new int[10]();
		sp<Counted>		counted=new Counted();

		defaultByType(x);				// returns 0
		defaultByType(pX);				// returns NULL
		defaultByType(array);			// returns NULL
		defaultByType(counted);			// returns sp<Counted>()

	@endcode

	The argument is required for proper template matching during
	compilation, but it is ignored during runtime.

	If a non-pointer type can not be initialized with 0,
	an additional overload is required.
*//***************************************************************************/

template<typename T>
inline
T		defaultByType(T t)			{ return T(0); }

template<typename T>
inline
T*		defaultByType(T* pT)		{ return NULL; }

/* NOTE don't actually work, falls back on T
template<typename T>
inline
sp<T>	defaultByType(sp<T> spT)	{ return sp<T>(); }

template<typename T>
inline
hp<T>	defaultByType(hp<T> hpT)	{ return hp<T>(); }
*/

} // namespace

#endif // __core_Delete_h__
