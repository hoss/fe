/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __core_assertCore_h__
#define __core_assertCore_h__

namespace fe
{

FE_DL_EXPORT void assertCore(sp<TypeMaster> spTypeMaster);

template <class T>
class FE_DL_EXPORT TypeInfoInteger : public BaseType::Info
{
	public:

virtual	String	print(void *instance)
				{
					T value= *(T *)instance;
					String string;
					string.sPrintf("%d",value);
					return string;
				}

virtual	IWORD	output(std::ostream &ostrm, void *instance, t_serialMode mode)
				{
					if(mode == e_binary)
					{
						T value = *(T *)instance;
						ostrm.write((char *)&value, sizeof(T));
						return sizeof(T);
					}
					else
					{
						T value= *(T *)instance;
						ostrm << std::dec << value;
						return c_ascii;
					}
				}

virtual	void	input(std::istream &istrm, void *instance, t_serialMode mode)
				{
					T value;
					if(mode == e_binary)
					{
						istrm.read((char *)&value, sizeof(T));
						*(T *)instance = value;
					}
					else
					{
						istrm >> value;
						*(T *)instance = value;
					}
				}

virtual	void	construct(void *instance)	{ *(T *)instance=0; }

virtual	IWORD	iosize(void)				{ return sizeof(T); }
};

class FE_DL_EXPORT InfoU8 : public TypeInfoInteger<U8>
{
	public:
virtual	IWORD	output(std::ostream &ostrm, void *instance, t_serialMode mode);
virtual	void	input(std::istream &istrm, void *instance, t_serialMode mode);
};

class FE_DL_EXPORT InfoU16 : public TypeInfoInteger<U16>
{
	public:
virtual	IWORD	output(std::ostream &ostrm, void *instance, t_serialMode mode);
virtual	void	input(std::istream &istrm, void *instance, t_serialMode mode);
};

class FE_DL_EXPORT InfoI16 : public TypeInfoInteger<I16>
{
	public:
virtual	IWORD	output(std::ostream &ostrm, void *instance, t_serialMode mode);
virtual	void	input(std::istream &istrm, void *instance, t_serialMode mode);
};

class FE_DL_EXPORT InfoU32 : public TypeInfoInteger<U32>
{
	public:
virtual	IWORD	output(std::ostream &ostrm, void *instance, t_serialMode mode);
virtual	void	input(std::istream &istrm, void *instance, t_serialMode mode);
};

class FE_DL_EXPORT InfoI32 : public TypeInfoInteger<I32>
{
	public:
virtual	IWORD	output(std::ostream &ostrm, void *instance, t_serialMode mode);
virtual	void	input(std::istream &istrm, void *instance, t_serialMode mode);
};

class FE_DL_EXPORT InfoU64 : public TypeInfoInteger<U64>
{
	public:
virtual	String	print(void *instance);
};

class FE_DL_EXPORT InfoI64 : public TypeInfoInteger<I64>
{
	public:
virtual	String	print(void *instance);
};

class FE_DL_EXPORT InfoIWORD : public TypeInfoInteger<IWORD>
{
	public:
virtual	IWORD	output(std::ostream &ostrm, void *instance, t_serialMode mode);
virtual	void	input(std::istream &istrm, void *instance, t_serialMode mode);
};

class FE_DL_EXPORT InfoF32 : public BaseType::Info
{
	public:
virtual	IWORD	output(std::ostream &ostrm, void *instance, t_serialMode mode);
virtual	void	input(std::istream &istrm, void *instance, t_serialMode mode);
virtual	String	print(void *instance);
virtual	void	construct(void *instance);
virtual	IWORD	iosize(void);
};

class FE_DL_EXPORT InfoF64 : public BaseType::Info
{
	public:
virtual	IWORD	output(std::ostream &ostrm, void *instance, t_serialMode mode);
virtual	void	input(std::istream &istrm, void *instance, t_serialMode mode);
virtual	String	print(void *instance);
virtual	void	construct(void *instance);
virtual	IWORD	iosize(void);
};

class FE_DL_EXPORT InfoBool : public BaseType::Info
{
	public:
virtual	IWORD	output(std::ostream &ostrm, void *instance, t_serialMode mode);
virtual	void	input(std::istream &istrm, void *instance, t_serialMode mode);
virtual	String	print(void *instance);
virtual	void	construct(void *instance);
virtual	IWORD	iosize(void);
};

class FE_DL_EXPORT InfoVoid : public BaseType::Info
{
	public:
virtual	IWORD	output(std::ostream &ostrm, void *instance, t_serialMode mode);
virtual	void	input(std::istream &istrm, void *instance, t_serialMode mode);
virtual	String	print(void *instance);
virtual	void	construct(void *instance);
virtual	IWORD	iosize(void);
};

class FE_DL_EXPORT InfoVoidStar : public BaseType::Info
{
	public:
virtual	IWORD	output(std::ostream &ostrm, void *instance, t_serialMode mode);
virtual	void	input(std::istream &istrm, void *instance, t_serialMode mode);
virtual	String	print(void *instance);
virtual	void	construct(void *instance);
virtual	IWORD	iosize(void);
};


} /* namespace */

#endif /* __core_assertCore_h__ */
