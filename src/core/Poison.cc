/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <core/core.pmh>

namespace fe
{

Poison::Poison(void)
{
	m_active = false;
}

Poison::~Poison(void)
{
}

bool Poison::active(void)
{
	SAFEGUARD;
	return m_active;
}

void Poison::start(void)
{
	SAFEGUARD;
	m_active = true;
}

void Poison::stop(void)
{
	SAFEGUARD;
	m_active = false;
}

Poisoned::Poisoned(void)
//	: Exception(e_poison, "Poisoned")
{
}

Poisoned::~Poisoned(void)
{
}


} /* namespace */

