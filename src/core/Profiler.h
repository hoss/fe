/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __core_Profiler_h__
#define __core_Profiler_h__

#define FE_PROFILER_ON		(FE_CODEGEN<=FE_PROFILE)
#define FE_PROFILER_DEBUG	FALSE

namespace fe
{

/**************************************************************************//**
	@brief Group of tick-based precision profilers

	@ingroup core

*//***************************************************************************/
class FE_DL_EXPORT Profiler: public Handled<Profiler>
{
	public:
						Profiler(String name): m_name(name)
						{
#if FE_PROFILER_ON
							SystemTicker::calibrate();
#if FE_PROFILER_DEBUG
							feLog("Profiler calibrate %.6G us/tick\n",
									SystemTicker::microsecondsPerTick());
#endif
#endif
						}
virtual					~Profiler(void)										{}

	/// @brief Tick-based precision time meter
	class FE_DL_PUBLIC Profile: public Handled<Profile>
	{
		public:
						Profile(sp<Profiler> spProfiler,String name):
#if FE_PROFILER_ON
								m_spProfiler(spProfiler),
#endif
								m_name(name),
								m_count(0),
								m_us(0.0)
						{
#if FE_PROFILER_ON
							hp<Profile> hpProfile(this);
							spProfiler->registerProfile(hpProfile);
#endif
						}

	const	String&		name(void) const			{ return m_name; }
			F64			microseconds(void) const	{ return m_us; }
			U32			count(void) const			{ return m_count; }

			void		start(void)
						{
#if FE_PROFILER_ON
							m_startTick=systemTick();
							m_count++;
#if FE_PROFILER_DEBUG
							feLog("Profile %s start %u\n",m_name.c_str(),
									m_startTick);
#endif
#endif
						}
			void		finish(void)
						{
#if FE_PROFILER_ON
							unsigned long tick=systemTick();
							m_us+=SystemTicker::microsecondsPerTick()*
									SystemTicker::tickDifference(
									m_startTick,tick);

#if FE_PROFILER_DEBUG
							unsigned long diff=tick-m_startTick;
							F64 us=diff*SystemTicker::microsecondsPerTick();
							feLog("Profile %s finish %u diff %u\n",
									m_name.c_str(),
									tick,tick-m_startTick,us);
#endif
#endif
						}
			void		replace(sp<Profile>& rspProfile)
						{
#if FE_PROFILER_ON
							m_startTick=systemTick();
							m_count++;

							rspProfile->m_us+=
									SystemTicker::microsecondsPerTick()*
									SystemTicker::tickDifference(
									rspProfile->m_startTick,m_startTick);

#if FE_PROFILER_DEBUG
							unsigned long diff=
									m_startTick-rspProfile->m_startTick;
							F64 us=diff*SystemTicker::microsecondsPerTick();
							feLog("Profile %s replace %s %u diff %u"
									" (%.6G us)\n",
									m_name.c_str(),
									rspProfile->name().c_str(),
									m_startTick,
									m_startTick-rspProfile->m_startTick,us);
#endif
#endif
						}
			String		report(void) const;

		private:
#if FE_PROFILER_ON
			sp<Profiler>	m_spProfiler;
#endif
			String			m_name;
			U32				m_count;
			F64				m_us;
			unsigned long	m_startTick;
	};

	/// @brief Guard-style scope control for a Profiler
	class FE_DL_PUBLIC Stage
	{
		public:
						Stage(sp<Profile>& rspProfile)
#if FE_PROFILER_ON
								:m_spProfile(rspProfile)
#endif
						{
#if FE_PROFILER_ON
							m_spProfile->start();
#endif
						}
						~Stage(void)
						{
#if FE_PROFILER_ON
							m_spProfile->finish();
#endif
						}

		private:
#if FE_PROFILER_ON
			sp<Profile>	m_spProfile;
#endif
	};

const	String&			name(void) const				{ return m_name; }

#if FE_PROFILER_ON
		void			registerProfile(hp<Profile> hpProfile)
						{	m_profileList.append(hpProfile); }
#endif

		void			begin(void)
						{
#if FE_PROFILER_DEBUG
							feLog("Profiler %s begin\n",m_name.c_str());
#endif
						}
		void			end(void)
						{
#if FE_PROFILER_DEBUG
							feLog("Profiler %s end\n",m_name.c_str());
#endif
						}

						/// @brief Generate a report of current profiles
		String			report(void);

	private:

#if FE_PROFILER_ON
		List< hp<Profile> >		m_profileList;
#endif

		String					m_name;
};

} /* namespace */

#endif /* __core_Profiler_h__ */

