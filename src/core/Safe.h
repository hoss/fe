/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __core_Safe_h__
#define __core_Safe_h__

//#define FE_DISABLE_THREAD_SAFE

#define SAFEGUARD				FEASSERT(fe_thread_mutex());				\
								SafeGuard __fe_thread_safeguard(			\
								fe_thread_mutex());
#define SAFEGUARD_IF(X)			FEASSERT(fe_thread_mutex());				\
								SafeGuard __fe_thread_safeguard(			\
								fe_thread_mutex(),X);
#define SAFEUNLOCK				__fe_thread_safeguard.unlock();
#define SAFELOCK				__fe_thread_safeguard.lock();

#define SAFEGUARDCLASS			FEASSERT(fe_class_thread_mutex());			\
								SafeGuard __fe_class_thread_safeguard(		\
								fe_class_thread_mutex());
#define SAFEGUARDCLASS_IF(X)		FEASSERT(fe_class_thread_mutex());		\
								SafeGuard __fe_class_thread_safeguard(		\
								fe_class_thread_mutex(),X);
#define SAFEUNLOCKCLASS			__fe_class_thread_safeguard.unlock();
#define SAFELOCKCLASS			__fe_class_thread_safeguard.lock();

namespace fe
{

class SafeBase
{
	public:
virtual							~SafeBase(void)								{}
		// returns a pointer because NotSafe does not require a mutex
virtual	RecursiveMutex*			fe_thread_mutex(void) const					=0;
};

class ClassSafeBase
{
	public:
virtual							~ClassSafeBase(void)						{}
		// returns a pointer because NotSafe does not require a mutex
virtual	RecursiveMutex*			fe_class_thread_mutex(void) const			=0;
};

template <typename T>
class FE_DL_EXPORT trueClassSafe : public ClassSafeBase
{
	public:
		class FE_DL_PUBLIC SafeGuard
		{
			public:
						SafeGuard(RecursiveMutex *mutex,BWORD a_truly=TRUE):
								m_guard(*mutex,FALSE,a_truly)				{}
				void	lock(void)		{ m_guard.lock(); }
				void	unlock(void)	{ m_guard.unlock(); }

				RecursiveMutex::Guard&
						guard(void)		{ return m_guard; }

			private:
				RecursiveMutex::Guard m_guard;
		};

#if FE_SAFE_COUNTED_MUTEX
				trueClassSafe(void)
				{
//					feLog("%p trueClassSafe<%s> %d %p %p\n",
//							this,FE_TYPESTRING(T).c_str(),
//							ms_mutexCount,&ms_pMutex,ms_pMutex);

//					if(ms_mutexCount++ == 0)
					if(!feAsmSwapIncr(&ms_mutexCount,1) && !ms_pMutex)
					{
						ms_pMutex=new RecursiveMutex;
						Memory::markForDeath(ms_pMutex,&ms_mutexCount);
					}
					FEASSERT(ms_pMutex);
				}

virtual			~trueClassSafe(void)
				{
//					feLog("%p ~trueClassSafe<%s> %d %p %p\n",
//							this,FE_TYPESTRING(T).c_str(),
//							ms_mutexCount,&ms_pMutex,ms_pMutex);

					FEASSERT(ms_pMutex);

//					if(--ms_mutexCount == 0)
					if(!(feAsmSwapIncr(&ms_mutexCount,-1)-1))
					{
						//* NOTE marked for death
//						delete ms_pMutex;
//						ms_pMutex=NULL;
					}
				}

virtual	RecursiveMutex* fe_class_thread_mutex(void) const
				{
//					feLog("%p trueClassSafe<%s>::fe_class_thread_mutex"
//							" %d %p %p\n",
//							this,FE_TYPESTRING(T).c_str(),
//							ms_mutexCount,&ms_pMutex,ms_pMutex);
					return ms_pMutex;
				}

	private:
static	FE_DL_PUBLIC	I32				ms_mutexCount;
static	FE_DL_PUBLIC	RecursiveMutex*	ms_pMutex;
#else
virtual	RecursiveMutex* fe_class_thread_mutex(void) const
				{
//					feLog("%p fe_class_thread_mutex %p\n",this,&ms_mutex);
					return &ms_mutex;
				}
	private:
static	FE_DL_PUBLIC	RecursiveMutex	ms_mutex;
#endif
};

template <typename T>
class FE_DL_EXPORT trueObjectSafe : public SafeBase
{
	public:
		class FE_DL_PUBLIC SafeGuard
		{
			public:
						SafeGuard(RecursiveMutex *mutex,BWORD a_truly=TRUE):
								m_guard(*mutex,FALSE,a_truly)				{}
				void	lock(void)		{ m_guard.lock(); }
				void	unlock(void)	{ m_guard.unlock(); }

				RecursiveMutex::Guard& guard(void)
						{	return m_guard; }
			private:
				RecursiveMutex::Guard m_guard;
		};

				trueObjectSafe(void)
				{
					//* force initialization
					SafeGuard guard(&m_fe_mutex);
				}
virtual			~trueObjectSafe(void)										{}

virtual	RecursiveMutex	*fe_thread_mutex(void) const
		{
			/*	cast away const here so that ObjectSafe can be used on const
				accessors that would otherwise be vulnerable to non-const
				mutators */
			return const_cast<RecursiveMutex*>(&m_fe_mutex);
		}

	private:
		RecursiveMutex	m_fe_mutex;
};

template <typename T>
class FE_DL_EXPORT trueNotSafe : public SafeBase
{
	public:
		class FE_DL_PUBLIC SafeGuard
		{
			public:
						SafeGuard(RecursiveMutex *mutex,BWORD a_truly=TRUE)	{}
				void	lock(void)											{}
				void	unlock(void)										{}
		};

				trueNotSafe(void)											{}
virtual			~trueNotSafe(void)											{}

virtual	RecursiveMutex* fe_thread_mutex(void) const			{ return NULL; }
};

template <typename T>
class FE_DL_EXPORT trueNotClassSafe : public ClassSafeBase
{
	public:
		class FE_DL_PUBLIC SafeGuard
		{
			public:
						SafeGuard(RecursiveMutex *mutex,BWORD a_truly=TRUE)	{}
				void	lock(void)											{}
				void	unlock(void)										{}
		};

				trueNotClassSafe(void)										{}
virtual			~trueNotClassSafe(void)										{}

virtual	RecursiveMutex* fe_class_thread_mutex(void) const	{ return NULL; }
};

#if FE_SAFE_COUNTED_MUTEX
template<typename T>
FE_DL_PUBLIC RecursiveMutex* trueClassSafe<T>::ms_pMutex=NULL;
template<typename T>
FE_DL_PUBLIC I32 trueClassSafe<T>::ms_mutexCount=0;
#else
template<typename T>
FE_DL_PUBLIC RecursiveMutex trueClassSafe<T>::ms_mutex;
#endif

class FE_DL_EXPORT GlobalHolder:
	public trueClassSafe<GlobalHolder>		{};

#ifndef FE_DISABLE_THREAD_SAFE
/**	Class level locking for thread safety
	*/
template<typename T> class FE_DL_EXPORT ClassSafe:
	public trueClassSafe<T>		{};
///	Object level locking for thread safety
template<typename T> class FE_DL_EXPORT ObjectSafe:
	public trueObjectSafe<T>	{};
///	No locking for thread safety
template<typename T> class FE_DL_EXPORT NotSafe:
	public trueNotSafe<T>		{};
#else
///	Class level locking for thread safety
template<typename T> class FE_DL_EXPORT ClassSafe:
	public trueNotClassSafe<T>		{};
///	Object level locking for thread safety
template<typename T> class FE_DL_EXPORT ObjectSafe:
	public trueNotSafe<T>		{};
///	No locking for thread safety
template<typename T> class FE_DL_EXPORT NotSafe:
	public trueNotSafe<T>		{};
#endif

} /* namespace */

#endif /* __core_Safe_h__ */
