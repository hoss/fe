/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __core_Peeker_h__
#define __core_Peeker_h__

namespace fe
{

class FE_DL_EXPORT Peeker
{
	public:
		Peeker(void);
virtual	~Peeker(void);

virtual	void	clear(void);
virtual	String	output(void);

virtual	void	base(void);
virtual	void	bold(void);
virtual	void	normal(void);
virtual	void	red(void);
virtual	void	green(void);
virtual	void	yellow(void);
virtual	void	blue(void);
virtual	void	magenta(void);
virtual	void	cyan(void);
virtual	void	white(void);

virtual	void	in(void);
virtual	void	out(void);
virtual	void	indent(void);
virtual	void	nl(void);

virtual	void	cat(const String &s);
virtual	void	cat(const char *s);
virtual	String	&str(void);

template <class T>
		void	operator()(T &t);

	protected:
		String	m_string;
		int		m_indent;
};

template <class T>
inline void Peeker::operator()(T &t)
{
	t.peek(*this);
}

} /* namespace */

#endif /* __core_Peeker_h__ */

