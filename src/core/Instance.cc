/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <core/core.pmh>

namespace fe
{

Instance::Instance(void)
{
	m_pData = NULL;
	m_pRefCnt = NULL;
}

Instance::Instance(void *instance, const sp<BaseType>& rspBT, std::atomic<int> *pRefCnt)
{
	m_pData = instance;
	m_spType = rspBT;
	m_pRefCnt = pRefCnt;
	acquire();
}

void Instance::set(void *instance, const sp<BaseType>& rspBT, std::atomic<int> *pRefCnt)
{
	release();
	m_pData = instance;
	m_spType = rspBT;
	m_pRefCnt = pRefCnt;
	acquire();
}

Instance::Instance(const Instance &other)
{
	m_pData = other.m_pData;
	m_spType = other.m_spType;
	m_pRefCnt = other.m_pRefCnt;
	acquire();
}

Instance::~Instance(void)
{
	release();
}

Instance &Instance::operator=(const Instance &other)
{
	if(this != &other)
	{
		other.acquire();
		release();
		m_pData = other.m_pData;
		m_spType = other.m_spType;
		m_pRefCnt = other.m_pRefCnt;
	}
	return *this;
}

bool Instance::operator==(const Instance &other)
{
	if(m_pData == other.m_pData)
	{
		return true;
	}
	return false;
}

void Instance::acquire(void) const
{
	if(m_pRefCnt)
	{
		(*m_pRefCnt)++;
	}
}

void Instance::release(void)
{
	if(m_pRefCnt && (*m_pRefCnt > 0))
	{
		(*m_pRefCnt)--;
		if(*m_pRefCnt == 0)
		{
			if(m_spType->getConstruct())
			{
				m_spType->destruct(m_pData);
			}
			deallocate(m_pData);
			m_pRefCnt = NULL;
			m_pData = NULL;
		}
	}
}

void Instance::create(sp<BaseType> spBT)
{
	release();
	m_spType = spBT;
	m_pData = allocate(spBT->size() + sizeof(std::atomic<int>));
	m_pRefCnt = (std::atomic<int> *)((char *)m_pData + spBT->size());
	*m_pRefCnt = 0;
	if(m_spType->getConstruct())
	{
		m_spType->construct(m_pData);
	}
	acquire();
}

fe::sp<BaseType> Instance::type(void) const
{
	return m_spType;
}

void *Instance::data(void) const
{
	return m_pData;
}

void Instance::assign(const Instance &rvalue)
{
	if(type()->typeinfo() != rvalue.type()->typeinfo())
	{
		feX("fe::Instance::assign","fe::Instance type conversion mismatch");
	}

#if FALSE
	String typeText;
#if FE_TYPE_STORE_NAME
	typeText="'"+rvalue.type()->verboseName()+"' ";
#endif
	const String valueText=rvalue.type()->getInfo().isValid()?
			rvalue.type()->getInfo()->print(
			const_cast<void*>(rvalue.data())):
			"<NULL Info>";
	feLog("Instance::assign %s\"%s\" <<<<<<<<<<<<<<<<\n",
			typeText,valueText.c_str());
#endif

	type()->assign(data(),rvalue.data());
}


} /* namespace */

