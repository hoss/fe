/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __core_TypeMaster_h__
#define __core_TypeMaster_h__

namespace fe
{


/**************************************************************************//**
*//***************************************************************************/
class FE_DL_EXPORT TypeConversion : public Counted
{
	public:
		typedef enum
		{
			e_cast,			///< 'out' points to pre-existing instance
			e_copy			///< 'out' points to newly allocated instance
		} InstanceAllocation;

virtual			~TypeConversion(void)										{}

virtual	InstanceAllocation	operator()(void *in,void *&out)
		{
			out = in;
			return e_cast;
		}

#if FE_COUNTED_TRACK
				TypeConversion(void): m_name("TypeConversion")				{}
const	String& name(void) const	{ return m_name; }
	private:
		String	m_name;
#endif

};

template<class In, class Out>
class FE_DL_EXPORT NativeConversion : public TypeConversion
{
	public:
		InstanceAllocation operator()(void *in,void *&out)
		{
			out = new Out;
			*reinterpret_cast<Out *>(out)=
				static_cast<Out>(*reinterpret_cast<In *>(in));
			return e_copy;
		}
};

typedef AutoHashMap< TypeInfo, sp<BaseType> > type_info_map;
typedef AutoHashMap< String, sp<BaseType> > string_type_map;
					//* reverse map: just one name of potentially many
typedef AutoHashMap< sp<BaseType>, String > type_string_map;
typedef AutoHashMap< sp<BaseType>, sp<TypeConversion> > out_converter_map;
typedef AutoHashMap< sp<BaseType>, out_converter_map > in_out_converter_map;

/**	@brief Run time C++ type management

	@ingroup core
	*/
class FE_DL_EXPORT TypeMaster : public Counted, public ClassSafe<TypeMaster>
{
	private:
	class TypeMasterEntry
	{
		public:
			BaseType				*type(void) {return m_pType;}
			void					addType(BaseType *pType) {m_pType = pType;}
		private:
			BaseType				*m_pType;
	};

	public:

	class TypeLoader:
		virtual public Counted,
		public CastableAs<TypeLoader>
	{
		public:
						/** @brief called when a requested type is not found
							*/
	virtual	Result		loadType(String a_name)								=0;
	};

							TypeMaster(void);
							~TypeMaster(void);

		template<class T>
		sp<BaseType>		lookupType(void);
		sp<BaseType>		lookupType(const TypeInfo &typeInfo);
		void				dumpTypes(void) const;

		Result				addTypeLoader(sp<TypeLoader> a_spTypeLoader);
		Result				loadType(String a_name);

		template<class Converter, class In, class Out>
		void				registerConversion(void);

		template<class In, class Out>
		void				nativeConversion(void);

		void				registerConversion(
								sp<TypeConversion> spConv,
								sp<BaseType> spInType,
								sp<BaseType> spOutType);


		sp<BaseType>		lookupName(const String &name);
							//* without autoload
		sp<BaseType>		lookupNameLoaded(const String &name);

		String				lookupBaseType(const sp<BaseType>& type);

		template<class In, class Out>
		sp<TypeConversion>	lookupConversion(void);

		void				reverseLookup(sp<BaseType> basetype,
								std::list<String> &names);
		String				reverseLookup(sp<BaseType> basetype);

							/// @internal
		sp<TypeConversion>	lookupConversion(sp<BaseType> spInType,
								sp<BaseType> spOutType);

		void				peek(Peeker &peeker);

		template<class T>
		sp<BaseType>		assertType(const String &name);

		template<class T>
		sp<BaseType>		assertType(void);

		template<class T, class Info>
		sp<BaseType>		assertTypeInfo(const String &name);


		template<class T>
		sp<BaseType>		assertPtr(const String &name);

const	String&				name(void) const	{ return m_name; }

		sp<BaseType>		assertTypeInternal(const TypeInfo &typeInfo,
								sp<BaseType> spT, const String &name);

							/// @brief serialize String buffer into value
		template<class T>
		Result				input(String a_buffer,T& a_value);

							/// @brief serialize value into String buffer
		template<class T>
		Result				output(T& a_value,String& a_buffer);

	private:
		type_info_map							m_hTypeidMap;
		string_type_map							m_hNameMap;
		type_string_map							m_hTypeMap;
		in_out_converter_map					m_hConverter;
		String									m_name;
		Array< sp<TypeLoader> >					m_typeLoaderArray;
};

template<class T>
inline sp<BaseType> TypeMaster::lookupType(void)
{
	return lookupType(TypeInfo(getTypeId<T>()));
}

template<class Converter, class In, class Out>
void TypeMaster::registerConversion(void)
{
	sp<TypeConversion> spConv(new Converter);
	sp<BaseType> spInType = lookupType<In>();
	sp<BaseType> spOutType = lookupType<Out>();
	if(spInType.isValid() && spOutType.isValid())
	{
		registerConversion(spConv,spInType,spOutType);
	}
	else
	{
		feX("fe::TypeMaster::registerConversion",
			"cannot convert unknown types");
	}
}

template<class In, class Out>
void TypeMaster::nativeConversion(void)
{
	registerConversion< NativeConversion<In,Out>, In, Out>();
}

template<class In, class Out>
sp<TypeConversion>	TypeMaster::lookupConversion(void)
{
	sp<BaseType> spInType = lookupType<In>();
	sp<BaseType> spOutType = lookupType<Out>();
	if(spInType.isValid() && spOutType.isValid())
	{
		return lookupConversion(spInType,spOutType);
	}
	else
	{
		feX("fe::TypeMaster::lookupConversion",
			"cannot convert unknown types");
	}
}

template<class T>
inline sp<BaseType> assertType(sp<TypeMaster> spTypeMaster, const String &name)
{
	sp<BaseType> spType(Type<T>::create());
	return spTypeMaster->assertTypeInternal(TypeInfo(getTypeId<T>()), spType, name);
}

template<class T>
inline sp<BaseType> TypeMaster::assertType(const String &name)
{
	sp<BaseType> spType(Type<T>::create());
	return assertTypeInternal(TypeInfo(getTypeId<T>()), spType, name);
}

template<class T>
sp<BaseType> TypeMaster::assertType(void)
{
	String name = "local_rtti_";
	name.cat(FE_TYPESTRING(T));
	sp<BaseType> spType(Type<T>::create());
	return assertTypeInternal(TypeInfo(getTypeId<T>()), spType, name);
}

template<class T, class Info>
sp<BaseType> TypeMaster::assertTypeInfo(const String &name)
{
	sp<BaseType> spType(Type<T>::create());

	sp<BaseType> spReturn;
	spReturn = assertTypeInternal(TypeInfo(getTypeId<T>()), spType, name);
	spReturn->setInfo(new Info());
	return spReturn;
}

template<class T>
class PtrInfo : public TypeInfoConstructable<sp<T> >
{
};
#if 0
template<class T>
class PtrInfo : public BaseType::Info
{
	public:
		virtual	bool	getConstruct(void)
		{
			return true;
		}
		virtual	void	construct(void *instance)
		{
			new(instance)sp<T>;
		}
		virtual	void	destruct(void *instance)
		{
			((sp<T> *)instance)->~sp<T>();
		}
};
#endif

template<class T>
sp<BaseType> TypeMaster::assertPtr(const String &name)
{
	sp<BaseType> spT;
	spT = assertType<sp<T> >(name);
	spT->setInfo(new PtrInfo<T>());
	return spT;
}

template<class T>
Result TypeMaster::input(String a_buffer,T& a_value)
{
	const sp<BaseType> spBaseType=lookupType<T>();
	if(spBaseType.isNull())
	{
		feLog("TypeMaster::output bad type\n");
		return e_unsupported;
	}

	void* data = &a_value;

	std::istringstream istrstrm(a_buffer.c_str());
	spBaseType->getInfo()->input(istrstrm,data,
			BaseType::Info::e_ascii);

	return e_ok;
}

template<class T>
Result TypeMaster::output(T& a_value,String& a_buffer)
{
	const sp<BaseType> spBaseType=lookupType<T>();
	if(spBaseType.isNull())
	{
		feLog("TypeMaster::output bad type\n");
		return e_unsupported;
	}

	void* data = &a_value;

	std::ostringstream ostrstrm;
	if(-1 == spBaseType->getInfo()->output(ostrstrm, data,
		BaseType::Info::e_ascii))
	{
		feLog("TypeMaster::output ascii write failed\n");
		return e_writeFailed;
	}

	a_buffer=ostrstrm.str().c_str();
	return e_ok;
}

} /* namespace */

#endif /* ___core_TypeMaster_h__ */
