/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __core_Counted_h__
#define __core_Counted_h__

#define FE_COUNTED_MT		FE_COUNT_MT

#if FE_CODEGEN<FE_DEBUG
#define FE_COUNTED_TRACK	TRUE
#else
#define FE_COUNTED_TRACK	FALSE
#endif

//* if you wanted cross-debug/optimize linking, this shouldn't vary by CODEGEN
#define FE_COUNTED_STORE_TRACKER	FE_COUNTED_TRACK

#if FE_COUNTED_TRACK && !FE_COUNTED_STORE_TRACKER
#error Cannot have FE_COUNTED_TRACK without FE_COUNTED_STORE_TRACKER
#endif

namespace fe
{

/**************************************************************************//**
	@brief Heap-based support for classes participating in fe::ptr <>

	@ingroup core

	acquire() and release() are required methods for fe::ptr <>.
*//***************************************************************************/
class FE_DL_PUBLIC FE_CORE_PORT Counted: public CastableAs<Counted>
{
	public:
				Counted(void):
					CastableAs<Counted>(),
					m_count(0)
				{
#if FE_COUNTED_STORE_TRACKER
					m_countedName="Counted";
#endif
#if FE_COUNTED_TRACK
					m_zeroed=FALSE;
#endif
				}

virtual			~Counted(void)
				{
#if FE_CODEGEN<=FE_DEBUG
					if(m_count)
					{
						feLog("Counted::~Counted invalid m_count %d\n",
								m_count);
						feLogBacktrace();
					}
#endif
					FEASSERT(!m_count);
				}

				/// @brief Increment the reference count
virtual	void	acquire(void)
				{
#if FE_COUNTED_MT && (FE_COUNTED_TRACK || !defined(FE_COUNT_ASM_IMPL))
					RecursiveMutex::Guard guard(ms_counted_mutex);
#endif

#ifdef FE_COUNT_ASM_IMPL
					feAsmIncr(&m_count, 1);
#else /* FE_COUNT_ASM_IMPL */
#if FE_COUNTED_TRACK
					FEASSERT(!m_zeroed);
#endif
					m_count++;
#endif /* FE_COUNT_ASM_IMPL */

#if FE_COUNTED_TRACK
					if(ms_pTracker)
					{
						ms_pTracker->acquire(this,verboseName(),m_count);
					}
#endif
				}

				/// @brief Increment the reference count (and get the count)
virtual	void	acquire(int& rCount)
				{
#if FE_COUNTED_MT && (FE_COUNTED_TRACK || !defined(FE_COUNT_ASM_IMPL))
					RecursiveMutex::Guard guard(ms_counted_mutex);
#endif

#if FE_COUNTED_TRACK
					FEASSERT(!m_zeroed);
#endif

#ifdef FE_COUNT_ASM_IMPL
					rCount=feAsmSwapIncr(&m_count, 1) + 1;
#else
					rCount= ++m_count;
#endif

#if FE_COUNTED_TRACK
					if(ms_pTracker)
					{
						ms_pTracker->acquire(this,verboseName(),rCount);
					}
#endif
				}

				/** @brief Decrement the reference count

					When the count reaches zero, the object is deleted. */
virtual	void	release(void)
				{
					if(!releaseInternal())
						delete this;
				}

				/** @brief Decrement the reference count

					Do not delete, even if count reaches zero.
					This is used to abandon a Counted from reference counting,
					although it remains valid to be picked up by reference
					counting again. */
virtual	void	abandon(void)
				{
#if FE_COUNTED_MT && (FE_COUNTED_TRACK || !defined(FE_COUNT_ASM_IMPL))
					RecursiveMutex::Guard guard(ms_counted_mutex);
#endif
#if FE_COUNTED_TRACK
					if(ms_pTracker)
					{
						ms_pTracker->release(this,verboseName(),m_count-1);
					}
#endif
#if FE_CODEGEN<=FE_DEBUG
					if(m_count<=0)
					{
						feX(e_corrupt,"Counted::abandon",
								"invalid m_count %d\n",m_count);
					}
#endif
					FEASSERT(m_count>0);
#ifdef FE_COUNT_ASM_IMPL
					feAsmIncr(&m_count, -1);
#else
					/* not releaseInternal since object is presumably still
						'live' even at count 0 */
					m_count--;
#endif
				}

				/// @brief Return the count of references
		int		count(void) const
				{
#if FE_COUNTED_MT
					RecursiveMutex::Guard guard(ms_counted_mutex);
#endif
					return m_count;
				}

	protected:
				/** @brief Decrement the reference count

					Returns the count after decrementing the references.
					The intent is that a substitute release() can call
					releaseInternal(), then conditionally delete the object
					after doing some custom cleanup that is not suitable
					for putting in the destructor. */
		int		releaseInternal(void)
				{
#if FE_COUNTED_MT && (FE_COUNTED_TRACK || !defined(FE_COUNT_ASM_IMPL))
					RecursiveMutex::Guard guard(ms_counted_mutex);
#endif

#if FE_COUNTED_TRACK
					if(ms_pTracker)
					{
						ms_pTracker->release(this,verboseName(),m_count-1);
					}
					if(m_count==1)
					{
						m_zeroed=TRUE;
					}
#endif
#if FE_CODEGEN<=FE_DEBUG
					if(m_count<=0)
					{
						feX(e_corrupt,"Counted::releaseInternal",
								"invalid m_count %d\n",m_count);
					}
#endif
					FEASSERT(m_count>0);
#ifdef FE_COUNT_ASM_IMPL
					return feAsmSwapIncr(&m_count, -1) - 1;
#else
					return --m_count;
#endif
				}

		void	suppressReport(void)
				{
#if FE_COUNTED_TRACK
					if(ms_pTracker)
					{
						ms_pTracker->suppress(this);
					}
#endif
				}

#if (FE_COUNTED_MT || FE_COUNTED_TRACK)
	protected:
static	FE_DL_PUBLIC	RecursiveMutex	ms_counted_mutex;
#endif

	private:
		int			m_count;

#if FE_COUNTED_STORE_TRACKER
		String		m_countedName;
		BWORD		m_zeroed;

static	FE_DL_PUBLIC	Tracker*	ms_pTracker;
static	FE_DL_PUBLIC	U32			ms_trackerCount;
	public:
					/***	NOTE name methods are only virtual if
							FE_COUNTED_STORE_TRACKER is TRUE */
		void		setName(const String& name)		{ m_countedName=name; }
virtual
const	String		verboseName(void) const			{ return name(); }
virtual
const	String&		name(void) const				{ return m_countedName; }
#else
	public:
		void		setName(const String&)									{}
#endif


	public:
#if FE_COUNTED_TRACK
static	void		startTracker(void)
					{	ms_pTracker=new Tracker("Counted  "); }
static	void		stopTracker(void)
					{	ms_trackerCount=trackerCount();
						delete ms_pTracker;
						ms_pTracker=NULL; }
static	String		reportTracker(void)
					{	FEASSERT(ms_pTracker);
						return ms_pTracker->report(); }
static	U32			trackerCount(void)
					{	return ms_pTracker?
								ms_pTracker->totalCount(): ms_trackerCount; }

					//* non-Counted references
static	void		trackReference(void* pPtr,void* pReference,String what)
					{	if(ms_pTracker)
							ms_pTracker->addReference(pPtr,pReference,what); }
static	void		untrackReference(void* pPtr,void* pReference)
					{	if(ms_pTracker)
							ms_pTracker->removeReference(pPtr,pReference); }

					//* non-Counted related regions
static	void		registerRegion(void* pPtr,U32 bytes,String name)
					{	if(ms_pTracker)
							ms_pTracker->registerRegion(pPtr,pPtr,bytes,name); }
static	void		deregisterRegion(void* pPtr)
					{	if(ms_pTracker)
							ms_pTracker->deregisterRegion(pPtr); }

		void		trackReference(void* pReference,String what)
					{	if(ms_pTracker)
							ms_pTracker->addReference(this,pReference,what); }
		void		untrackReference(void* pReference)
					{	if(ms_pTracker)
							ms_pTracker->removeReference(this,pReference); }

		void		acquireRegion(void* pPtr,String name,int count)
					{	if(ms_pTracker)
							ms_pTracker->acquire(pPtr,name,count); }
		void		releaseRegion(void* pPtr,String name,int count)
					{	if(ms_pTracker)
							ms_pTracker->release(pPtr,name,count); }

					//* Counted regions
		void		registerRegion(void* pT,U32 bytes)
					{
						if(ms_pTracker)
						{
							ms_pTracker->registerRegion(this,pT,bytes,
									verboseName());
						}
					}
#else
static	String		reportTracker(void)					{ return ""; }
static	U32			trackerCount(void)					{ return 0; }
		void		trackReference(void* pReference,String what)			{}
		void		untrackReference(void* pReference)						{}
		void		registerRegion(void* pT,U32 bytes)						{}
#endif
};

} /* namespace */

#endif /* __core_Counted_h__ */
