/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __core_AutoHashMap_h__
#define __core_AutoHashMap_h__

namespace fe
{

struct eq_string
{
	bool operator()(const String &s1, const String &s2) const
	{
		return s1 == s2;
	}
};

struct hash_int
{
	FE_UWORD operator()(const I32 &i) const
	{
		return I32(i);
	}
};

struct hash_string
{
	FE_UWORD operator()(const String &s) const
	{
		FE_UWORD hash = 0;
		const unsigned char *buffer = s.rawU8();

		for(FE_UWORD i = 0; i < s.length(); i++)
		{
			hash = (hash << 3) + buffer[i];
		}

		return hash;
	}
};

template<typename T>
struct hash_auto
{
		FE_UWORD	operator()(const T t) const
				{	return reinterpret_cast<FE_UWORD>(t); }
};
template<typename T>
struct eq_auto
{
		bool	operator()(T t1, T t2) const
				{	return t1 == t2; }
};

template<typename T, typename U>
class AutoHashMap:
	public HashMap< T, U, hash_auto<T>, eq_auto<T> >						{};

template<typename U>
class AutoHashMap< I32, U >:
	public HashMap< I32, U, hash_int, eq_string >							{};

template<typename U>
class AutoHashMap< String, U >:
	public HashMap< String, U, hash_string, eq_string >						{};

} /* namespace */

#endif /* __core_AutoHashMap_h__ */
