/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#ifndef __core_sp_h__
#define __core_sp_h__

#if FE_CODEGEN<=FE_DEBUG
#define FE_PTR_NULL_CHECK	TRUE
#else
#define FE_PTR_NULL_CHECK	FALSE
#endif

#define FE_PTR_STACK_CHECK	FE_CHECK_HEAP

#if FE_CODEGEN<FE_DEBUG
#define FE_PTR_TRACK	TRUE
#else
#define FE_PTR_TRACK	FALSE
#endif

#define FE_PTR_DEBUG	FALSE

namespace fe
{

template<typename T> class hp;
/**************************************************************************//**
	@brief Intrusive Smart Pointer

	@ingroup core

	In the spectrum of "smart" pointers, this is a shared pointer.  It is
	held from several points and let go when all contacts have released it.
	The elimination of the pointed-to object is left to the object itself.
	It is presumed that the object holds a reference count which tracks
	each acquire and release.  A common resolution is self-deletion.

	For an interchangable weak reference, see hp<>.

	FE doesn't currently have direct support for a scoped or auto pointer
	with just one owner.

	The reason this class breaks fe naming convention is for clarity during
	usage.  The short lowercase 'sp' name obscures the pointed-to class less.

	Attempt to reference a pointer on the stack is illegal and should cause
	an exception.
*//***************************************************************************/
template<typename T>
class sp
{
	public:
				sp(void): m_tPtr(NULL)										{}

explicit		sp(T* pT): m_tPtr(pT)
				{
#if FE_PTR_STACK_CHECK
					if(pT && !Memory::isHeapOrCannotTell(pT))
						feX(("fe::sp< "+FE_TYPESTRING(T)+" >::sp").c_str(),
								"attempt to reference stack value");
#endif
					if(m_tPtr)
						m_tPtr->acquire();
					track();
				}

				sp(const sp<T>& rspT)
				{
					if(rspT.m_tPtr)
						rspT.m_tPtr->acquire();
					m_tPtr = rspT.m_tPtr;
					track();
				}

				template<typename X>
				sp(const sp<X>& rspX): m_tPtr(NULL)
				{	operator=(rspX); }

				template<typename X>
				sp(const hp<X>& rhpX): m_tPtr(NULL)
				{	operator=(rhpX.object()); }

#if FE_PTR_TRACK
virtual			~sp(void)
#else
				~sp(void)
#endif
				{
					untrack();
					if(m_tPtr)
						m_tPtr->release();
				}

		sp<T>	&operator=(T* pT)
				{
					if(pT!=raw())
					{
#if FE_PTR_STACK_CHECK
						if(pT && !Memory::isHeapOrCannotTell(pT))
						{
							feX(("fe::sp< "+FE_TYPESTRING(T)+
									" >::operator=").c_str(),
									"attempt to reference stack value");
						}
#endif
						if(pT)
							pT->acquire();
						untrack();
						if(m_tPtr)
							m_tPtr->release();
						m_tPtr = pT;
						track();
					}
					return *this;
				}

		sp<T>	&operator=(const sp<T>& rspT)
				{
					if(rspT.raw() != raw())
					{
						if(rspT.m_tPtr)
							rspT.m_tPtr->acquire();
						untrack();
						if(m_tPtr)
							m_tPtr->release();
						m_tPtr = rspT.m_tPtr;
						track();
					}
					return *this;
				}

				template<typename X>
		sp<T>	&operator=(const sp<X>& rspX)
				{
#if FE_PTR_DEBUG
					T* pT=fe_cast<T>(rspX.raw());

					if(!pT && rspX.raw())
					{
						feLog("[33m>> sp<%s> could not convert %p"
								" from \"%s\" [0m\n",
								FE_TYPESTRING(T).c_str(),
								rspX.raw(),
								FE_TYPESTRING(X).c_str());
//						FEASSERT(0);
					}
					return operator=(pT);
#else
					return operator=(fe_cast<T>(rspX.raw()));
#endif
				}

				template<typename X>
		bool	is(void)
				{
					return (fe_cast<X>(raw()) != NULL);
				}

				template<typename X>
		sp<T>	&operator=(const hp<X>& rhpX)
				{
					untrack();
					if(m_tPtr)
						m_tPtr->release();
					m_tPtr = NULL;
					operator=(rhpX.object());
					return *this;
				}

		T&		operator*(void)
				{
#if FE_PTR_NULL_CHECK
					if(!m_tPtr)
						feX(("fe::sp< "+FE_TYPESTRING(T)+
								" >::operator*").c_str(),
								"attempt to dereference invalid sp<>");
#endif
					FEASSERT(m_tPtr);
					return *m_tPtr;
				}

				//* is this meaningful?  The sp is const, not the T.
const	T&		operator*(void) const
				{
#if FE_PTR_NULL_CHECK
					if(!m_tPtr)
						feX(("fe::sp< "+FE_TYPESTRING(T)+
								" >::operator*").c_str(),
								"attempt to dereference invalid sp<>");
#endif
					FEASSERT(m_tPtr);
					return *m_tPtr;
				}

		T*		operator->(void) const
				{
#if FE_PTR_NULL_CHECK
					if(!m_tPtr)
						feX(("fe::sp< "+FE_TYPESTRING(T)+
								" >::operator->").c_str(),
								("attempt to use invalid sp<"+
								FE_TYPESTRING(T)+">").c_str());
#endif
					FEASSERT(m_tPtr);
					return m_tPtr;
				}

const	T*		asConst(void) const
				{
#if FE_PTR_NULL_CHECK
					if(!m_tPtr)
						feX(("fe::sp< "+FE_TYPESTRING(T)+
								" >::asConst").c_str(),
								("attempt to use invalid sp<"+
								FE_TYPESTRING(T)+">").c_str());
#endif
					FEASSERT(m_tPtr);
					return m_tPtr;
				}

		T*		abandon(void)
				{
#if FE_PTR_NULL_CHECK
					if(!m_tPtr)
						feX(("fe::sp< "+FE_TYPESTRING(T)+
								" >::abandon").c_str(),
								("attempt to use invalid sp<"+
								FE_TYPESTRING(T)+">").c_str());
#endif
					FEASSERT(m_tPtr);
					untrack();
					T *pT = m_tPtr;
					m_tPtr->abandon();
					m_tPtr = NULL;
					return pT;
				}
		T*		raw(void) const				{ return m_tPtr; }
		BWORD	isValid(void) const			{ return m_tPtr != NULL; }
		BWORD	isNull(void) const			{ return m_tPtr == NULL; }

	private:
#if FE_PTR_TRACK
				//* NOTE only virtual if tracking is on
virtual	void	track(void)
				{
					if(m_tPtr)
					{
						m_tPtr->trackReference(this,"sp<> "+m_tPtr->name());
					}
				}
		void	untrack(void)
				{
					if(m_tPtr)
					{
						m_tPtr->untrackReference(this);
					}
				}
#else
		void	track(void)													{}
		void	untrack(void)												{}
#endif
		T*		m_tPtr;
};

///	@relates sp
template <class T,class X>
inline BWORD operator==(const sp<T>& rspLeft,const sp<X>& rspRight)
{
	return (rspLeft.raw() == sp<T>(rspRight).raw());
}

///	@relates sp
template <class T,class X>
inline BWORD operator!=(const sp<T>& rspLeft,const sp<X>& rspRight)
{
	return (rspLeft.raw() != sp<T>(rspRight).raw());
}

// this is so an sp<> can be used as a std::map key
///	@relates sp
template <class T,class X>
inline BWORD operator<(const sp<T>& rspLeft,const sp<X>& rspRight)
{
	return (FE_UWORD)(rspLeft.raw()) < (FE_UWORD)(sp<T>(rspRight).raw());
}

template<class T>
struct hash_sp
{
		FE_UWORD	operator()(const sp<T>& rspT) const
				{	return reinterpret_cast<FE_UWORD>(rspT.raw()); }
};

template<class T>
struct eq_sp
{
		bool	operator()(const sp<T>& rspT1, const sp<T>& rspT2) const
				{	return rspT1 == rspT2; }
};

template<typename T, typename U>
class AutoHashMap< sp<T>, U >:
	public HashMap< sp<T>, U, hash_sp<T>, eq_sp<T> >						{};

} /* namespace */

#endif /* __core_sp_h__ */
