/*	Copyright (C) 2003-2021 Free Electron Organization
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit freeelectron.org. */

/** @file */

#include <autoload/autoload.pmh>

#include "platform/dlCore.cc"

using namespace fe;

extern "C"
{

FE_DL_EXPORT void LoadYamlManifests(sp<Registry> spRegistry)
{
	sp<ManifestReaderI> spManifestReaderI=
			spRegistry->create("ManifestReaderI");
	if(spManifestReaderI.isNull())
	{
		feLog("AutoLoad could not create ManifestReaderI\n");
		return;
	}

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wconditionally-supported"

	const String loadPath=System::getLoadPath((void*)LoadYamlManifests,TRUE);
	const String libPath=loadPath.pathname();

#pragma GCC diagnostic pop

//	feLog("LoadYamlManifests loadPath \"%s\"\n",loadPath.c_str());
//	feLog("LoadYamlManifests libPath \"%s\"\n",libPath.c_str());

	Array<String> entries;
	System::listDirectory(libPath,entries);

	Regex regex("^.*DL\\.yaml$");

	const I32 entryCount=entries.size();
	for(I32 entryindex=0;entryindex<entryCount;entryindex++)
	{
		const String& rEntry=entries[entryindex];
		if(regex.match(rEntry.c_str()))
		{
//			feLog("entry %d/%d \"%s\"\n",entryindex,entryCount,
//					rEntry.c_str());
			spManifestReaderI->load(libPath+"/"+rEntry);
		}
	}
}

FE_DL_EXPORT void ListDependencies(List<String*>& list)
{
}

FE_DL_EXPORT Library* CreateLibrary(sp<Master> spMaster)
{
	Library *pLibrary=new Library();

	return pLibrary;
}

FE_DL_EXPORT void InitializeLibrary(sp<Library> spLibrary)
{
	sp<Catalog> spManifest=spLibrary->registry()->manifest();
	sp<Catalog> spTypeManifest=spLibrary->registry()->typeManifest();

#include "autoload/manifests.cc"

	LoadYamlManifests(spLibrary->registry());

//	feLog("Component manifest:\n");
//	spManifest->catalogDump();

//	feLog("Type manifest:\n");
//	spTypeManifest->catalogDump();
}

}
