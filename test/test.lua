io.write("hello from lua\n")

-- manage("fexNativeWindowDL");
a = {};
a[1] = 10
sig.a = 10;
a[2] = sig.a;
io.write(sig.a);
sig.r = sig;
io.write("\n------\n");
io.write(sig.r.a);
sig.r = nil;
layout = sig:layout()
scope = layout:scope()
scope = sig:layout():scope()
layout = scope:declare("my_layout")
layout:populate("c", "real")
layout:populate("v", "vector3")
layout:populate("sv", "spatial_vector")
layout:populate("signaler", "component")
sig.r = layout:createRecord()
sig.r = scope:createRecord(layout)
sig.r.v[1] = 1.0
sig.r.sv[1] = sig.r.v[1]
sig.r.v[1] = sig.r.sv[1]
c = sig:check("a");
io.write("\n------\n");
if c then io.write("found\n")
else io.write("not found\n")
end

function setrecs()
	for cv,ra in sig.rg() do
		io.write("\n--RA--\n");
		if ra:check("a") then
			io.write(ra:length())
			for i = 0,ra:length()-1 do
				ra[i].a = i
			end
		end
	end
end

setrecs()

for cv,ra in sig.rg() do
	io.write("\nread -RA--\n");
	if ra:check("a") then
		io.write(ra:length())
		for i = 0,ra:length()-1 do
			io.write("\nread --R--\n");
			io.write(ra[i].a)
			io.write("\n");
		end
	end
end



signaler = create("SignalerI")
io.write(signaler:name())
io.write("\n")

handler = create("HandlerI")
io.write(handler:name())
io.write("\n")

signaler:insert(handler, layout)

signaler:signal(sig.r)

handler:handle(sig.r)

sig.r.signaler = signaler

sig.r.signaler:signal(sig.r)

--[[
io.write("pre dispatch\n")
rec = layout:createRecord()
dispatch = create("DispatchI.TestDispatch")
rec, i = dispatch:my_function(rec, 17)
io.write(i)
io.write(" post dispatch\n")

io.write(dispatch:sqr(17))
io.write("\n")
]]



-- window:getEventContext()


