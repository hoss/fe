if normaledSurface == nil then
	count = 0

	if modelfile == nil then
		modelfile = "/home/symhost/sym/proj/gitlab/fe/scene/HumanFemale/HumanFemale.usd"
	end

	if modelnode == nil then
		modelnode = "/HumanFemale_Group/HumanFemale/Geom/Body/Body_sbdv"
	end

	print("modelfile: " .. modelfile)
	print("modelnode: " .. modelnode)

	importOp = createOperator("ImportOp")
	importOp["loadfile"] = modelfile
	importOp["extractNodes"] = modelnode
	importedSurface = importOp()
	print("ImportOp summary: " .. importOp["summary"])

	surfaceNormalOp = createOperator("SurfaceNormalOp")
	surfaceNormalOp["Clockwise"] = false
	normaledSurface = surfaceNormalOp(importedSurface)
	print("SurfaceNormalOp summary: " .. surfaceNormalOp["summary"])

	bloatOp = createOperator("BloatOp")

	exportOp = createOperator("ExportOp")
	exportOp["savefile"] = "output/exported.json"
	exportOp(normaledSurface)
	exportOp["savefile"] = "output/exported.yaml"
	exportOp(normaledSurface)
end

bloat = 2.0 * (1.0 - math.abs(math.cos(0.01 * count)))
print("count=" .. count .. " bloat=" .. bloat)

bloatOp["Bloat"] = bloat
bloatedSurface = bloatOp(normaledSurface)
print("BloatOp summary: " .. bloatOp["summary"])

count = count + 1
output = bloatedSurface

bloatedSurface = nil

print("complete using " .. string.format("%.1f", collectgarbage("count")) .. " KB")
--collectgarbage("collect")
