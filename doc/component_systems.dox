/**

@page component_systems Component Systems

At its core, Free Electron is plugin system designed
around a component model,
meant to be as simple as possible, but no simpler.
But the term "component" can mean different things to different people.

@section CBSE Component-Based Software Engineering

General Free Electron components adhere to the classic concept
in software engineering.

https://en.wikipedia.org/wiki/Component-based_software_engineering

In many ways, the Free Electron component model is similar to COM.
But it makes several design choices to attain additional
simplicity and efficiency.

- support C++ only as a first class language, and others through bindings
- do not require an IDL
- do not require a broker or independent run-time
- use RTTI, but be judicious for dynamic casts
- use multiple inheritance, for a small set of specific design patterns

Components must be:

- encapsulated
- substitutable
- packageable

Free Electron is not a constrained set of design patterns.
It is a component substrate to be used for component packaging,
loading, lookup, and binding.
There should be no restriction of patterns within any exposed system.

@section ECS Entity Component System

Another popular use of the term "component" refers to
the functional aspects of coarse game objects.

https://en.wikipedia.org/wiki/Entity_component_system

Free Electron is not limited to the focused role of facilitating
just "entity" components, in this sense of an ECS architecture.
However, the ECS concepts already align with capabilities in Free Electron.
The coarse grained game Entity could be a Free Electron entity component
associated with a list of contained Free Electron aspect components,
all implementing appropriate Free Electron interfaces.
The entity component data could be stored naively in the
actual component member data,
but would probably be more efficient in a RecordGroup data objects
(or some other data store), either per component,
or perhaps optimized in some per-Entity or per-System layout.
ECS Systems could be a implementations of some Free Electron interface,
with each System probably holding a list the current aspect components
which that particular System services.

The different usages of the terminology might provoke some confusion.
But briefly, "ECS components" are just aspects of coarse game objects,
while "Free Electron components" can be almost any modestly coarse object.

*/
