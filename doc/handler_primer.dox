/**

@page handler_primer Handler Primer and Guide

Any Component implementing the HandlerI interface is considered a handler
for the purposes of this style guide.

A handler is therefore a component that handles arbitrary signals in the
form of Records, or at least that is all the HandlerI::handle() interface
says it does.  In practice a handler needs more information than some
arbitrary incoming record alone in order to perform its function.




@section handler_signal_info Cacheing Signal and Signaler Information

Deriving from the BindI interface provides a callback 
from the Signaler when the handler is registered
with the signaler.  This calls provides an opportunty to cache information
obtainable via the signaler or the signal type (Layout).

Note that it is possible to have record attributes pointing to records in
a different scope than the parent record.  In this case care must be taken
when cacheing scope.




@section handler_access_info Dynamic State

Using Fixed or Well-Known Attributes on the input signal fe::Record is
the preferred method of handing per-signal input state to a handler as
well as output state.

<b>If a handler is changing state while handling a signal, that
state should be handed to the Handler in the signal itself (via its
attributes).</b>


fe::Accessor and fe::AccessorSet provide for accessing attributes with known names.
This is useful for standardized notions of data (see the classes derived from
fe::AccessorSet).

Note that the FE_USE macro is available to facilatate automatic documentation
of used attribute names.




@section handler_config_info (Mostly) Static State

State that corresponds to the configuration of the handler itself, which
will not change often or at all, should be set using the
fe::ConfigI interface.

The fe::Config mechanism is preferred to using class functions when
possible since Config based state is easily runtime inspectable via
generic mechanisms.

An object's config can be parented to
other objects, and even the system's Catalog system in fe::Master.

fe::Config based configuration is automatically exposed to scripting,

The fe::Config mechanism is really just a string dictionary with convienience
functions for the aforementioned purpose.  Therefore, Config can be used
to communicate and key,value String pair to a Component.





@section handler_dispatch_info Exposing Arbitrary Functionality

When none of the above fits and scripting exposure is necessary, the
DispatchI interface may be used.  ( See @ref component_binding )

If exposure to scripting is not necessary, typical C++ interfaces should
be fine.



*/

