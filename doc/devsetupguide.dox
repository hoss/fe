
/**

@page devsetupguide Developer Setup Guide

<b>OUTDATED.  THIS PAGE SEVERELY NEEDS UPDATING.</b>

\htmlonly

<SCRIPT LANGUAGE="JavaScript">
<!--
	
	function PrintLastModifiedDate()
	{
		lastmodstr = document.lastModified		// get last modified date
		modval = Date.parse(lastmodstr)			// convert string to date
		if(modval == 0)                			// unknown date (or January 1, 1970 GMT)
		{
	   		document.write("Modified: Unknown")
   		}
		else
		{
   			document.write("Modified: " + lastmodstr)
		}
	}	// end PrintLastModifiedDate()
   	
// -->
</SCRIPT>

<P>This guide describes the process of creating a development environment
	for the Free Electron codebase, from scratch.  Note that this is a 
	*developer* setup guide, not a *user* setup guide.
	There are will be sections for each platform supported by Free Electron.
  	The following assumptions are made regarding the state of the target system:</P>

<UL>
	<LI>Functioning OS installation
	<LI>Current drivers installed for all hardware
	<LI>A network connection (for retrieving the code)
</UL>

<BR>

<P>Please refer to the section associated with your system:</P>

<UL>
	<LI><A href="#linux">GNU/Linux</A>
	<LI><A href="#win32">Win32</A>
	<LI><A href="#mac">Mac</A>
</UL>

<HR>

<H3><A name="common">Common</A></H3>

<OL class=header3>

<LI>get svn access:
	<UL class=plain>
		<LI>email <A href="mailto:orang@imonk.com">AJ</A> and get an imonk account (you'll use this for svn access, and for web access)

	</UL>
	<BR><BR>


<LI>

	The following environment variables are required:<BR><BR>

	<TABLE WIDTH=75% VALIGN=TOP BORDER=1 class=plain>
		<TR class=tableheader>
			<TD class=tableheader WIDTH=15%>Variable</TD>
			<TD class=tableheader WIDTH=15%>Value</TD>
			<TD class=tableheader WIDTH=35%>Example</TD>
			<TD class=tableheader WIDTH=35%>Description</TD>
		</TR>

		<TR>
			<TD>FE_ROOT</TD>
			<TD>&#60path to local FE svn repo&#62</TD>
			<TD>D:\projects\fe\subversion</TD>
			<TD>A convenience variable leveraged by several others</TD>
		</TR>
	
		<TR>
			<TD>CODEGEN</TD>
			<TD><I>buildtype</I> [debug,optimize,profile]</TD>
			<TD>debug</TD>
			<TD>Used by Forge to dictate the nature of the build.</TD>
		</TR>
			
		<TR>
			<TD>FE_LIB</TD>
			<TD>&#60path to desired lib directory for generated binaries&#62</TD>
			<TD>D:\projects\fe</TD>
			<TD>Instructs Forge to create and use a <I>lib</I> directory at the specified location for
		   		all generated libs.  If not specified, Forge will use $FE_ROOT instead.</TD>
		</TR>
			
	</TABLE>

	<BR><BR>
	The following environment variables are recommended but aren't
	technically necessary:<BR><BR>

	<TABLE WIDTH=75% VALIGN=TOP BORDER=1 class=plain>
		<TR class=tableheader>
			<TD class=tableheader WIDTH=15%>Variable</TD>
			<TD class=tableheader WIDTH=15%>Value</TD>
			<TD class=tableheader WIDTH=35%>Example</TD>
			<TD class=tableheader WIDTH=35%>Description</TD>
		</TR>

		<TR>
			<TD>PRETTY</TD>
			<TD><I>anything</I></TD>
			<TD>1</TD>
			<TD>Used to colorize Forge's shell output.  The value of this var doesn't matter,
		   		as Forge only checks whether or not it exists.</TD>
		</TR>
	
		<TR>
			<TD>CCACHE_DIR</TD>
			<TD>&#60path to local ccache temp&#62</TD>
			<TD>/usr/tmp/ccache</TD>
			<TD>Instructs Forge to leverage ccache, and specifies the ccache temp directory.</TD>
		</TR>
	
		<TR>
			<TD>FE_E_ASSERT</TD>
			<TD><I>non-zero value</I></TD>
			<TD>1</TD>
			<TD>Causes an assert to occur whenever an exception would have been thrown.</TD>
		</TR>

		<TR>
			<TD>FE_OBJ</TD>
			<TD>&#60path to desired obj directory for generated object files&#62</TD>
			<TD>D:\projects\fe</TD>
			<TD>Instructs Forge to create and use a <I>obj</I> directory at the specified location for
		   		all generated object files.  If not specified, Forge will use $FE_LIB instead.</TD>
		</TR>
	
	</TABLE>
	<BR><BR>
	
<HR>

<H3><A name="linux">Linux</A></H3>

<P>pending...</P>

<HR>

<H3><A name="win32">Win32</A></H3>

<OL class=header3>

<LI>Do everything stated in the <A href="#common">Common</A> section above.
	<BR><BR>

<LI>The following environment variables are required:<BR><BR>

	<TABLE WIDTH=75% VALIGN=TOP BORDER=1 class=plain>
		<TR class=tableheader>
			<TD class=tableheader WIDTH=15%>Variable</TD>
			<TD class=tableheader WIDTH=15%>Value</TD>
			<TD class=tableheader WIDTH=35%>Example</TD>
			<TD class=tableheader WIDTH=35%>Description</TD>
		</TR>

		<TR>
			<TD>FE_PATH</TD>
			<TD>&#60paths to local FE binaries&#62</TD>
			<TD>%FE_ROOT%\bin;%FE_LIB%\lib\x86_win32_%CODEGEN%;<BR>%FE_LIB%\lib\x86_win32_%CODEGEN%\fe;</TD>
			<TD>A convenience variable used to make the generated binaries easy to execute.</TD>
		</TR>
	
		<TR>
			<TD>HASH_MAP_OVERRIDE</TD>
			<TD><I>non zero value</I></TD>
			<TD>1</TD>
			<TD>Causes std::map to appear as ext::hash_map in the code.  Necessary because Microsoft suxors.</TD>
		</TR>
		
	</TABLE>
	<BR><BR>

<LI>Install MSVC++:
	<UL class=plain>
		<LI>NOTE: only Microsoft Visual Studio .NET (v7.1) is officially supported. MSVC++ v6.0 is not
		sufficiently C++ compliant.
		<LI>Ensure that the MS Visual C++ tools (linker and compiler) are able to run
		from a shell (command line).  In order for them to do so, certain environment variables
		must be set.  The MSVC++ installer normally asks whether or not these variables
		should be set.  If you didn't let the MSVC++ installer set the variables, or you're not sure, then you need to run
		the VCVARS32.BAT file that sets the variables.  This bat file should exist in your MSVC++ folder
		(typically, <CODE><B>C:\Program Files\Microsoft Visual Studio\VC98\Bin</B></CODE>).
		It's preferrable to run the version
		of this file that exists on your system, as it will be configured correctly for the directories on your
		system.  If this file doesn't exist for some reason, there is a copy available in the svn repository:
		<CODE><B>bin/vsvars32.bat</B></CODE>
		<BR><BR>
		If you use that batch file, you must edit it to ensure
		that it references the directory where MSVC++ was installed on your system.  After you edit it, just
		create a shortcut to it, and set the 'Start in:' directory to be where ever you want -- it acts just 
		like the regular shell shortcut.
	</UL>
	<BR><BR>

<LI>Install Python
	<UL class=plain>
		<LI>If you already have <A href="http://python.org" target="_blank">Python</A>
			on your system, please verify that it is <B>v2.2.3</B> (you can do this by just running
			IDLE, the python console, which will display the version at the top).
			If it is not, we make no guarantees regarding FE functionality.  A non-debug build of FE might
			work on a different Python version, but you're asking for trouble.
		<LI>go to the <A HREF="http://python.org/download/" TARGET="_blank">Python downloads page</A>
		<LI>download the win32 install.  Note that we are currently using
			<B><A href="http://python.org/ftp/python/2.2.3/Python-2.2.3.exe">v2.2.3</A></B>.
			Even if newer versions of Python are available, you should use <B>v2.2.3</B> until this
			document indicates otherwise.  
			The reason that the version is important is because Python maintains its own shell variables, and
			they need to sync up with the python debug binaries that we provide which had to be built
			with a particular version of the python source.
		<LI>install it.
		<LI>set the following environment variables:

			<TABLE WIDTH=75% VALIGN=TOP BORDER=1 class=plain>
				<TR class=tableheader>
					<TD class=tableheader WIDTH=15%>Variable</TD>
					<TD class=tableheader WIDTH=15%>Value</TD>
					<TD class=tableheader WIDTH=35%>Example</TD>
					<TD class=tableheader WIDTH=35%>Description</TD>
				</TR>

				<TR>
					<TD>PYTHON_ROOT</TD>
					<TD>&#60root directory of your python installation&#62</TD>
					<TD>"C:\Program Files\Python22"</TD>
					<TD>Used by scripts to locate python binaries, modules, etc..</TD>
				</TR>
	
				<TR>
					<TD>PYTHON_VERSION</TD>
					<TD><I>major.minor python version</I></TD>
					<TD>2.2</TD>
					<TD>The major.minor version number of the pyton installation.  NOTE - it is very important
						that you DO NOT include the third version number (use 2.2 NOT 2.2.3)</TD>
				</TR>
						
				<TR>
					<TD>PYTHONPATH</TD>
					<TD>&#60directory of your FE binaries&#62</TD>
					<TD>%FE_PATH%</TD>
					<TD>An additional set of directories that the Python interpreter considers as part of its $PATH.
						NOTE - if this variable already exists after you install Python, then just append the 
						FE part to it.</TD>
				</TR>
		
			</TABLE>
			<BR><BR>

		<LI>Python is FE's primary supported scripting language.
			Test scripts, demos, tools, etc. are all actively being developed with Python.
		<LI>If you would like color output from the build system, install the WConio package (http://newcenturycomputers.net/projects/wconio.html)
	</UL>
	<BR><BR>

<LI>Install Boost
	<UL class=plain>
		<LI>If you already have <A href="http://boost.org" target="_blank">Boost</A>
			installed on your system you can skip this section.
		<LI>go to the <A HREF="http://sourceforge.net/project/showfiles.php?group_id=7586" TARGET="_blank">Boost files page</A>
		<LI>download a boost package file (zip/bz2/gz).  We're currently using
			<A href="http://prdownloads.sourceforge.net/boost/boost_1_31_0.zip?download">v1.31.0</A>.
		<LI>unzip the boost package someplace (semi-permanent).  Note that unzipping will create a
		<CODE><B>boost_1_31_0</B></CODE> directory.  If that annoys you as much as it does me, just rename it to 'Boost'.
		That way, if you were to upgrade boost, you wouldn't have to track down references to the version-ized 
		directory name.  Whatever you call it, all subsequent directory references as relative to this root directory.
		<LI>run <CODE><B>boost\tools\build\jam_src\build.bat</B></CODE>
		(this builds boost.jam, which is boost's build tool)
		<LI>NOTE: if that completes successfully, not only will you have built boost.jam, but you'll also have verified
		that the MSVC++ environment variables are established correctly to allow command line compiling and linking.
		<LI>That created a directory called <CODE><B>bin.ntx86</B></CODE>, which contains the boost.jam executables.
		Add the path for these executables to your $PATH environment variable (or, if you don't care, you can just copy
		the executables to somewhere that is already in the $PATH, such as your $WINDIR\system32 directory).
		<LI>ensure that the appropriate Python environment variables have been set (see the Python section above).
		Boost relies on them to build the Boost.Python library.
		<LI>edit <CODE><B>boost\boost\config\user.hpp</B></CODE> such that it contains: <BR><BR>
			<CODE><B>#define BOOST_THREAD_NO_LIB</B></CODE><BR><BR>
			There's a section for this 
			kind of directive at the bottom of user.hpp, so just read the comment and add the line.
		<LI>Now, you're ready to build the Boost libs.  Open a console in the Boost root directory (where you unzipped
		the boost files (e.g. <CODE><B>C:\projects\boost_1_31_0</B></CODE>), and run the following command:<BR>
			<CODE><B>bjam "-sTOOLS=vc7.1" --prefix=D:\projects\boost_1_31_0 install</B></CODE>
			<BR><BR>
		This takes quite awhile, so go get a drink or something.  Upon completion, you should see something like,
		<I>"...updated 3121 targets..."</I>  The '--prefix' argument allows you to specify where boost installs itself.
		Thus, if like me you didn't want the 'boost_1_31_0' directory, change the argument to reflect whatever you named
		the root boost directory.
		<LI>set the following environment variables:

			<TABLE WIDTH=75% VALIGN=TOP BORDER=1 class=plain>
				<TR class=tableheader>
					<TD class=tableheader WIDTH=15%>Variable</TD>
					<TD class=tableheader WIDTH=15%>Value</TD>
					<TD class=tableheader WIDTH=35%>Example</TD>
					<TD class=tableheader WIDTH=35%>Description</TD>
				</TR>

				<TR>
					<TD>BOOST_VERSION</TD>
					<TD><I>specially formatted boost version number</I></TD>
					<TD>1_31</TD>
					<TD>Used by Forge to correctly identify boost binaries.  NOTE -- this variable MUST NOT
						exist *BEFORE* you build boost, only afterwards.  If you're rebuilding boost,
						delete this variable first, and then replace it after the boost build finishes.
						If you don't, boost installs itself using '1__1' which is bad.</TD>
				</TR>
	
				<TR>
					<TD>BOOST_INCLUDE</TD>
					<TD>&#60directory of the boost header files&#62</TD>
					<TD>D:\projects\Boost\include\boost-%BOOST_VERSION%</TD>
					<TD>Used by Forge to allow the correct inclusion of boost header files.</TD>
				</TR>
						
				<TR>
					<TD>BOOST_LIB</TD>
					<TD>&#60root directory of the generated boost binaries&#62</TD>
					<TD>D:\projects\Boost\lib</TD>
					<TD>Used by Forge to allow correct linking to the boost binaries.</TD>
				</TR>

				<TR>
					<TD>BOOST_STRUCTURE</TD>
					<TD><I>installtype</I> [install,bin-stage]</TD>
					<TD>install</TD>
					<TD>Used by Forge to allow correct usage of boost files.
						Not sure exactly what this does...</TD>
				</TR>
	
			</TABLE>
			<BR><BR>

			<LI>FE uses Boost for uh, horribly obfuscating our own build process,
				for the purpose of alienating anyone who would otherwise be inclined to
				help on the project.  If you're curious about Boost, read the 
				<A href="http://www.boost.org/libs/libraries.htm" target="_blank">boost documentation</A>.
				We'll eventually provide a specific list of the boost libraries used by FE.

	</UL>
	<BR><BR>

<LI>Install DevIL
	<UL class=plain>
		<LI>FE requires <A href="http://openil.sourceforge.net/" target="_blank">DevIL</A>
		v1.6.5 or greater.  Even if you already have DevIL on your system, please verify the version,
		and that the appropriate environment variables are set.
		<LI>go to the <A HREF="http://openil.sourceforge.net/download.php" TARGET="_blank">DevIL downloads page</A>
		<LI>download the 
		<A href="http://prdownloads.sourceforge.net/openil/DevIL-SDK-1.6.5.zip">windows SDK</A> (currently v1.6.5)
		<LI>unzip it someplace (semi-permanent).  Note, it will create its own 'DevIL' directory when unzipped.
		<LI>set the following environment variable:

			<TABLE WIDTH=75% VALIGN=TOP BORDER=1 class=plain>
				<TR class=tableheader>
					<TD class=tableheader WIDTH=15%>Variable</TD>
					<TD class=tableheader WIDTH=15%>Value</TD>
					<TD class=tableheader WIDTH=35%>Example</TD>
					<TD class=tableheader WIDTH=35%>Description</TD>
				</TR>

				<TR>
					<TD>DEVIL</TD>
					<TD>&#60directory of the boost header files&#62</TD>
					<TD>D:\projects\DevIL</TD>
					<TD>Convenience variable leveraged when appending to some other env vars.</TD>
				</TR>
	
			</TABLE>
			<BR><BR>
			
		<LI>FE uses DevIL for some image manipulation.  If you're curious about DevIL, read the 
		<A href="http://openil.sourceforge.net/docs/index.php" target="_blank">documentation</A>.

	</UL>
	<BR><BR>
	
<LI>Install SDL (Simple DirectMedia Layer)
	<UL class=plain>
		<LI>FE requires <A href="http://www.libsdl.org/" target="_blank">SDL</A>
		v1.2.7 or greater.  Even if you already have SDL on your system, please verify the version,
		and that the appropriate environment variables are set.
		<LI>go to the <A HREF="http://www.libsdl.org/download-1.2.php" TARGET="_blank">SDL downloads page</A>
		<LI>download the 
		<A href="http://www.libsdl.org/release/SDL-devel-1.2.7-VC6.zip">win32 development libraries</A> (currently v1.2.7)
		<LI>unzip the SDL package someplace (semi-permanent).  Note that unzipping will create a
		<CODE><B>SDL-1.2.7</B></CODE> directory.  If that annoys you as much as it does me, just rename it to 'SDL'.
		That way, if you were to upgrade SDL, you wouldn't have to track down references to the version-ized 
		directory name.  Whatever you call it, all subsequent directory references as relative to this root directory.
		<LI>FE uses SDL for stuff.  If you're curious about SDL, read the 
		<A href="http://www.libsdl.org/intro.php" target="_blank">introduction</A>.

	</UL>
	<BR><BR>

<LI>Install Subversion
	<UL class=plain>
		<LI>If you already have <A href="http://subversion.tigris.org" target="_blank">Subversion</A>
			installed on your system you can skip this section.
		<LI>go to the <A HREF="http://subversion.tigris.org/getting_subversion.html" TARGET="_blank">Subversion downloads page</A>
		<LI>follow the instructions for installing Subversion via a source tarball, or get one of the 
			<A href="http://subversion.tigris.org/servlets/ProjectDocumentList?folderID=91" target="_blank">pre-packaged
			setup files</A> for win32.  We're currently using
			<A href="http://subversion.tigris.org/files/documents/15/14089/svn-1.0.5-setup.exe">v1.0.5</A>.
		<LI>FE uses Subversion for revision control. You are highly encouraged to peruse the
			<A href="subversion_book.html" target="_blank">Subversion book</A> if you aren't familiar with Subversion.	
	</UL>
	<BR><BR>

<LI>Get the codebase using Subversion
	<UL class=plain>
		<LI>using a console window, change into (or create) the directory that you want to 
		contain your local FE repository.  NOTE -- this must be the same directory that
		you specify as <CODE><B>$FE_ROOT</B></CODE>
		<LI>execute the following subversion command to retrieve the FE repository:<BR>
			<CODE><B>svn co https://imonk.com/svn/fe .</B></CODE><BR><BR>
			NOTE: the '.' tells svn to use the current directory.  If you don't specify a target directory,
			you'll likely wind up with an extra 'fe/' that you didn't intend to have.
		<LI>you'll get a notice about the imonk SSL certificate.  Press 'p' to have subversion accept the
		certificate permanently.
		<LI>you'll be prompted to enter your imonk password.  If you don't have an imonk account,
		email AJ to get one.
		<LI>once that finishes, you'll possess a local, subversion managed copy of the FE codebase.
	</UL>
	<BR><BR>

	<LI>Finish setting up the environment variables: 
	<UL class=plain>
	
		<LI>Add the following to the <B>INCLUDE</B> variable:
	
			<UL class=plain>
				<LI><CODE>%DEVIL%\include</CODE>
			</UL>

		<LI>Add the following to the <B>LIB</B> variable:
	
			<UL class=plain>
				<LI><CODE>%DEVIL%\lib</CODE>
			</UL>

		<LI>Add the following to the <B>PATH</B> variable:
	
			<UL class=plain>
				<LI><CODE>%PYTHON_ROOT%</CODE>
				<LI><CODE>%PYTHONPATH%</CODE>
				<LI><CODE>%BOOST_LIB%</CODE>
				<LI><CODE>%DEVIL%\lib</CODE>
				<LI><CODE>%FE_PATH%</CODE>
			</UL>
	
	</UL>
	<BR><BR>

<LI>You can now generate any targets:
	<UL class=plain>
		<LI><CODE>cd $FE_ROOT</CODE>
		<LI>in a console window, enter: <CODE>forge.py -d [target]</CODE>  (e.g.: <CODE>forge.py -d root</CODE>)
		<LI>If you used 'forge.py -d root' as your first target (and it's recommended that you do),
			then you'll know whether or not things
		    worked correctly by observing the results of the suite of unit tests (both python scripts and 
			executables) which runs automatically.
		<LI>You can run '<CODE>forge.py -h</CODE>' to see a brief summary of forge.py command line usage.
		<LI>Eventually, one of us will get around to providing a nice home online for Forge, and we would
			subsequently provide a link to it here.	
	</UL>
	<BR><BR>

<LI>Build the IDE workspace and project files (if you wish to use the MSVC IDE):
	<UL class=plain>
		<LI><B>NOTE: MSVC project files are currently NOT supported!</B>  We will address this issue
			as soon as we can afford to do so, but it simply is not a high priority at this time.  We fully intend
			to provide complete support for MSVC .NET project files.  We will NEVER
			generate MSVC 6.0 project files (as stated above, MSVC 6.0 is not sufficiently C++ compliant
			for our purposes).
		<LI><B>The auto-generated MSVC files should be considered read-only.  Changes made to
		    the project settings within the IDE will not be reflected in the official build
			process.  All official build settings must be managed through the associated forge files.</B>
	</UL>
	<BR><BR>


</OL>

<H2>If you want to do local builds of the documentation:</H2>

<OL class=header3>

<LI>Install doxygen
	<UL class=plain>

		<LI>If you already have <A href="http://doxygen.org" target="_blank">Doxygen</A>
			installed on your system you can skip this section.
		<LI>go to the <A HREF="http://www.stack.nl/~dimitri/doxygen/download.html#latestsrc" TARGET="_blank">Doxygen downloads page</A>
		<LI>follow the instructions for installing Doxygen via a source tarball, or get the 
			<A href="ftp://ftp.stack.nl/pub/users/dimitri/doxygen-1.3.7-setup.exe" target="_blank">pre-packaged win32
			setup</A> (currently at v1.3.7).
		<LI>doxygen is an automated documentation tool.  It uses its own syntax for embedded markups in source code.  It's 
		the best thing since sliced bread, and if you write software you should use it.
	</UL>

<LI>Install graphviz
	<UL class=plain>
		<LI>If you already have <A href="http://www.research.att.com/sw/tools/graphviz/" target="_blank">Graphviz</A>
			installed on your system you can skip this section.
		<LI>go to the <A HREF="http://www.research.att.com/sw/tools/graphviz/download.html" TARGET="_blank">Graphviz downloads page</A>
		<LI>If you're feeling really adventurous, feel free to attempt an install from source, but there's no reason not
			to just grab the 
			<A href="http://www.graphviz.org/pub/graphviz/ARCHIVE/graphviz-1.12.exe" target="_blank">pre-packaged win32
			setup</A> (currently at v1.12).
		<LI>graphviz includes utilities for generating graphs from text descriptions.  It is leveraged by Doxygen.
	</UL>

<LI>build the doxygen documentation files:
	<UL class=plain>
		<LI><CODE>cd $FE_ROOT\doc</CODE>
		<LI>in a console window, enter: <CODE><B>doxygen</B></CODE>
		<LI>NOTE: if it complains about 'dot', that means that Graphviz isn't properly installed.
		<LI>if you want to create a shortcut to run doxygen, make sure that the working directory
		    is correctly specified, because the doxygen configuration file is inferred.
		<LI>after it's done, you can open up <CODE><B>$FE_ROOT\doc\html\index.html</B></CODE>
		<LI>It is highly recommended that you peruse the 'Related Pages' section first.	
		<LI>The official doxygen-generated Free Electron documentation can always be found
			<A HREF="" TARGET="_blank">online</A>.
	</UL>

</OL>

<HR>

<H3><A name="mac">Mac</A></H3>

<pre>
These are prliminary notes taken while trying to setup a Mac Mini OSX 10.4

Install Fink.  This is technically not required, but makes life much easier.
http://fink.sourceforge.net

Install subversion (I did via the fink svn-client package...using dselect).

Get FE source:
svn co http://imonk.com/svn/fe/main

Install boost.  There is a fink package in 'unstable', but I did it from source.



</pre>


\endhtmlonly
@endcode
*/

