/**
	@mainpage Free Electron Reference Manual

	Free Electron is a C++ framework facilitating reuse and integration
	for R&D projects such as simulation, AI, and visual effects.

	Core systems include dynamic plugins, a strong component model,
	and a fast runtime database, all highly extensible.

	Simple build files for each module are written directly in Python.
	The build system supports distcc, ccache, and precompiled headers.

	Integration has been demonstrated with Alembic, Arnold, Boost, FBX,
	Houdini, JSON, Katana, Lua, Maya, OpenAL, OpenCL, ODE, OpenGL, OpenIL,
	OpenImageIO, OpenMP, OpenSceneGraph, OpenSubdiv, OpenVDB, PCRE, Ptex,
	SDL, TBB, Unreal, USD/Hydra, and XGen, as well as many proprietary tools.

	@section install Installation

	- @subpage installation

	@section architect Architecture and Usage

	- @subpage summary

	- @subpage component_systems

	- @subpage framework_basics

	- @subpage statics

	- @subpage component_guide

	- @subpage plugin_howto

	- @subpage data_primer

	- @subpage handler_primer

	- @subpage serial_format

	- @subpage window_design

	- @subpage viewer_design

	- @subpage logging

	- @subpage nethost
*/

/*
	<center>
	<table border=0>
	<tr>
	<td>@image html FreeElectron.svg</td>
	</tr>
	</table>
	</center>
*/

/*
	- @subpage component_binding

	- @subpage network_design

	@section collaborate Collaboration

	- @subpage attribute_names

	- @subpage catalog_names

	- @subpage issue_tracking

	@copydoc developer

	@copydoc versioninfo
*/
