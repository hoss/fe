/**

@page summary Feature Summary

@section abstract Abstract
Free Electron is a C++ framework facilitating reuse and integration
for R&D projects such as simulation, AI, and visual effects.

@section features Features

A core set of libraries provides functionality that will be used
in nearly all systems.  On top of this, there is a wide set of extensible
and potentially replaceable modules.

Builds are done in Python following a simple template for each module.

Platform abstraction allows virtually identical use on Linux, OSX, and Win32.

Multithreading has been a design imperative from the beginning.
A balance is made to provide the tools to write safe code
but without forcing locks at too fine a level.
Coarse common operations, such as component construction,
are built as thread safe, but components themselves can choose to
participate in mechanisms that make them thread safe.
This avoids potentially hugely wasteful locking where
access collisions aren't really possible.
Multithreading is demonstrated and proven in several extensions.

A simple, yet powerful, component model hides implementations
behind clean interfaces.  Implementations can be extended
with new implementations, even across dynamic libaries,
providing full functional extensibility.

Smart pointers, smart handles, smart hubs, and copy-on-write pointers
allow an entire application to be built up purely on references,
all propped up by a single context instance.
Elegant dependency destruction permits graceful resource deallocation
without any explicit shutdown or release calls.
Conversions between pointer and handle references,
as well as RTTI supported casting, is completely automatic.

Dynamic plugins allow libraries to be compiled independently
and provide new components without requiring recompilation
of core systems or other components.

A highly efficient database stores primary application data as records
in well organized backing stores instead of class instances.
Shared properties are implemented with coincidental
attribute names instead of multiple inheritance.
This avoids picking up unwanted data or functionality
from base cases classes in order to get desired properties.
Handlers can efficiently cull out subsets from a pool of records
in order to operate only on those with the relevant attributes.

Records are often propagated as signals.
Signals are records sent to handlers and often contain
groups of other records.
The payload could be any type of information,
such as a single window event or a huge cluster of particles.
Signals can be networked to other processes or even other machines.

Serialization stores and retrieves data in human-readable
neatly formatted ascii form or as tight binary buffers.

Instead of clinging to a strategy of avoiding other people's work
and creating our own versions of everything,
we cherry pick outside technologies and assimilate them as modules,
usually with our own interfaces.
When the technolgies we need are not readily available,
we build our own.
*/
